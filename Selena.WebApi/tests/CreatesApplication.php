<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        /***   TEST  ***/
        $variables_test = [
            'DB_CONNECTION' => env('DB_CONNECTION_TEST', 'data_test'),
            'DB_HOST' => env('DB_HOST_TEST', 'postgres'),
            'DB_PORT' => env('DB_PORT_TEST', '5432'),
            'DB_DATABASE' => env('DB_DATABASE_TEST', 'selena_test'),
            'DB_USERNAME' => env('DB_USERNAME_TEST', 'selena_test'),
            'DB_PASSWORD' => env('DB_PASSWORD_TEST', '!T3cD3v!'),
        ];

        foreach ($variables_test as $key => $value) {
            putenv("$key=$value");
        }

        /***   LOG   ***/
        $variables_log = [
            'DB_CONNECTION_LOG' => env('DB_CONNECTION_TEST_LOG', 'log_test'),
            // 'DB_HOST_LOG' => env('DB_HOST_TEST_LOG', 'mysql'),
            // 'DB_PORT_LOG' => env('DB_PORT_TEST_LOG', '3306'),
            'DB_DATABASE_LOG' => env('DB_DATABASE_TEST_LOG', 'selena_test_log'),
            'DB_USERNAME_LOG' => env('DB_USERNAME_TEST_LOG', 'selena_test_log'),
            'DB_PASSWORD_LOG' => env('DB_PASSWORD_TEST_LOG', '!T3cD3v!'),
        ];

        foreach ($variables_log as $key => $value) {
            putenv("$key=$value");
        }

        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }
}

<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\BusinessLogic\UserBL;
use Laravel\Passport\Passport;

// class UserTest extends TestCase
// {
//     #region GET

//     /*
//      * v1 / language / getSelect ?search= {description}
//      */
//     public function testLanguageGetSelect()
//     {
//         $user = User::where('name', 'admin')->first();
//         $description = substr($user, 0, 2);
//         $data = response()->getSelect(UserBL::getSelect($description));

//         Passport::actingAs($user);
//         $response = $this->get('/api/v1/user/select?search=' . $description);
//         $response->assertStatus(200)
//             ->assertSuccessful()
//             ->assertSee(json_encode($data->original));
//     }

//     #region GET
// }

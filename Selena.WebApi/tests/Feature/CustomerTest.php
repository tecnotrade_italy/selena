<?php

namespace Tests\Feature;

use App\BusinessLogic\LanguageBL;
use App\Customer;
use App\DtoModel\DtoCustomer;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Language;
use App\PostalCode;
use App\User;
use App\VatType;
use Carbon\Carbon;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CustomerTest extends TestCase
{
    #region GET

    /**
     * Get all
     *
     * @return void
     */
    public function testGetAll()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $postalCodeGetAll = new PostalCode();
        $postalCodeGetAll->code = '40017';
        $postalCodeGetAll->created_id = $user->id;
        $postalCodeGetAll->save();

        $vatTypeGetAll = new VatType();
        $vatTypeGetAll->rate = 20;
        $vatTypeGetAll->created_id = $user->id;
        $vatTypeGetAll->save();

        $customerTecnotradeGetAll = new Customer();
        $customerTecnotradeGetAll->company = true;
        $customerTecnotradeGetAll->business_name = 'TecnotradeGetAll srl';
        $customerTecnotradeGetAll->name = 'Simone';
        $customerTecnotradeGetAll->surname = 'Resca';
        $customerTecnotradeGetAll->vat_number = '1234567890';
        $customerTecnotradeGetAll->fiscal_code = 'SMNRSC75D17F257K';
        $customerTecnotradeGetAll->address = 'Via Giulio Cesare Costa 18';
        $customerTecnotradeGetAll->postal_code_id = $postalCodeGetAll->id;
        $customerTecnotradeGetAll->telephone_number = '051123456';
        $customerTecnotradeGetAll->fax_number = '051123457';
        $customerTecnotradeGetAll->email = 'simone@tecnotrade.com';
        $customerTecnotradeGetAll->website = 'tecnotrade.com';
        $customerTecnotradeGetAll->language_id = $user->language_id;
        $customerTecnotradeGetAll->vat_type_id = $vatTypeGetAll->id;
        $customerTecnotradeGetAll->enable_from = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $customerTecnotradeGetAll->enable_to = $customerTecnotradeGetAll->enable_from;
        $customerTecnotradeGetAll->created_id = $user->id;
        $customerTecnotradeGetAll->send_newsletter = true;
        $customerTecnotradeGetAll->error_newsletter = false;
        $customerTecnotradeGetAll->save();

        #endregion PARAMETER

        $response = $this->get('/api/v1/customer/all');

        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        $this->assertEquals(Customer::all()->map(function ($model) {
            $postalCode = PostalCode::find($model->postal_code_id);
            $language = Language::find($model->language_id);
            $vatType = VatType::find($model->vat_type_id);

            return [
                'id' => $model->id,
                'company' => $model->company,
                'businessName' => $model->business_name,
                'name' => $model->name,
                'surname' => $model->surname,
                'vatNumber' => $model->vat_number,
                'fiscalCode' => $model->fiscal_code,
                'address' => $model->address,
                'idPostalCode' => $model->postal_code_id,
                'postalcodeDescription' => $postalCode->code,
                'postalCodeDescription' => null,
                'telephoneNumber' => $model->telephone_number,
                'faxNumber' => $model->fax_number,
                'email' => $model->email,
                'website' => $model->website,
                'idLanguage' => $model->language_id,
                'languageDescription' => $language->description,
                'idVatType' => $model->vat_type_id,
                'vattypeDescription' => $vatType->rate,
                'vatTypeDescription' => null,
                'enableFrom' => $model->enable_from,
                'enableTo' => $model->enable_to,
                'sendNewsletter' => $model->send_newsletter,
                'errorNewsletter' => $model->error_newsletter,
            ];
        })->toArray(), $response->json()['data']);

        #region PARAMETER

        $customerTecnotradeGetAll->delete();
        $vatTypeGetAll->delete();
        $postalCodeGetAll->delete();

        #endregion PARAMETER
    }

    /**
     * Get by id
     *
     * @return void
     */
    public function testGetById()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $postalCodeById = new PostalCode();
        $postalCodeById->code = '40017';
        $postalCodeById->created_id = $user->id;
        $postalCodeById->save();

        $vatTypeById = new VatType();
        $vatTypeById->rate = 20;
        $vatTypeById->created_id = $user->id;
        $vatTypeById->save();

        $customerTecnotradeById = new Customer();
        $customerTecnotradeById->company = true;
        $customerTecnotradeById->business_name = 'TecnotradeById srl';
        $customerTecnotradeById->name = 'Simone';
        $customerTecnotradeById->surname = 'Resca';
        $customerTecnotradeById->vat_number = '1234567890';
        $customerTecnotradeById->fiscal_code = 'SMNRSC75D17F257K';
        $customerTecnotradeById->address = 'Via Giulio Cesare Costa 18';
        $customerTecnotradeById->postal_code_id = $postalCodeById->id;
        $customerTecnotradeById->telephone_number = '051123456';
        $customerTecnotradeById->fax_number = '051123457';
        $customerTecnotradeById->email = 'simone@tecnotrade.com';
        $customerTecnotradeById->website = 'tecnotrade.com';
        $customerTecnotradeById->language_id = $user->language_id;
        $customerTecnotradeById->vat_type_id = $vatTypeById->id;
        $customerTecnotradeById->enable_from = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $customerTecnotradeById->enable_to = $customerTecnotradeById->enable_from;
        $customerTecnotradeById->created_id = $user->id;
        $customerTecnotradeById->send_newsletter = true;
        $customerTecnotradeById->error_newsletter = false;
        $customerTecnotradeById->save();

        $dtoCustomerTecnotradeById = new DtoCustomer();
        $dtoCustomerTecnotradeById->id = $customerTecnotradeById->id;
        $dtoCustomerTecnotradeById->company = $customerTecnotradeById->company;
        $dtoCustomerTecnotradeById->businessName = $customerTecnotradeById->business_name;
        $dtoCustomerTecnotradeById->name = $customerTecnotradeById->name;
        $dtoCustomerTecnotradeById->surname = $customerTecnotradeById->surname;
        $dtoCustomerTecnotradeById->vatNumber = $customerTecnotradeById->vat_number;
        $dtoCustomerTecnotradeById->fiscalCode = $customerTecnotradeById->fiscal_code;
        $dtoCustomerTecnotradeById->address = $customerTecnotradeById->address;
        $dtoCustomerTecnotradeById->idPostalCode = $postalCodeById->id;
        $dtoCustomerTecnotradeById->postalcodeDescription = $postalCodeById->code;
        $dtoCustomerTecnotradeById->telephoneNumber = $customerTecnotradeById->telephone_number;
        $dtoCustomerTecnotradeById->faxNumber = $customerTecnotradeById->fax_number;
        $dtoCustomerTecnotradeById->email = $customerTecnotradeById->email;
        $dtoCustomerTecnotradeById->website = $customerTecnotradeById->website;
        $dtoCustomerTecnotradeById->idLanguage = $user->language_id;
        $dtoCustomerTecnotradeById->languageDescription = Language::find($user->language_id)->description;
        $dtoCustomerTecnotradeById->idVatType = $vatTypeById->id;
        $dtoCustomerTecnotradeById->vattypeDescription = $vatTypeById->rate;
        $dtoCustomerTecnotradeById->enableFrom = $customerTecnotradeById->enable_from;
        $dtoCustomerTecnotradeById->enableTo = $customerTecnotradeById->enable_to;
        $dtoCustomerTecnotradeById->sendNewsletter = $customerTecnotradeById->send_newsletter;
        $dtoCustomerTecnotradeById->errorNewsletter = $customerTecnotradeById->error_newsletter;

        #endregion PARAMETER

        $response = $this->get('/api/v1/customer/' . $customerTecnotradeById->id);
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        #region PARAMETER

        #endregion PARAMETER

        $this->assertEquals($dtoCustomerTecnotradeById->toArray(), $response->json()['data']);

        #region PARAMETER

        $customerTecnotradeById->delete();
        $vatTypeById->delete();
        $postalCodeById->delete();

        #endregion PARAMETER
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @return void
     */
    public function testInsert()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $postalCodeInsert = new PostalCode();
        $postalCodeInsert->code = '40017';
        $postalCodeInsert->created_id = $user->id;
        $postalCodeInsert->save();

        $vatTypeInsert = new VatType();
        $vatTypeInsert->rate = 20;
        $vatTypeInsert->created_id = $user->id;
        $vatTypeInsert->save();

        $dtoCustomerTecnotradeInsertBefore = new DtoCustomer();
        $dtoCustomerTecnotradeInsertBefore->company = true;
        $dtoCustomerTecnotradeInsertBefore->businessName = 'TecnotradeInsert srl';
        $dtoCustomerTecnotradeInsertBefore->name = 'Simone';
        $dtoCustomerTecnotradeInsertBefore->surname = 'Resca';
        $dtoCustomerTecnotradeInsertBefore->vatNumber = '1234567890';
        $dtoCustomerTecnotradeInsertBefore->fiscalCode = 'SMNRSC75D17F257K';
        $dtoCustomerTecnotradeInsertBefore->address = 'Via Giulio Cesare Costa 18';
        $dtoCustomerTecnotradeInsertBefore->idPostalCode = $postalCodeInsert->id;
        $dtoCustomerTecnotradeInsertBefore->telephoneNumber = '051123456';
        $dtoCustomerTecnotradeInsertBefore->faxNumber = '051123457';
        $dtoCustomerTecnotradeInsertBefore->email = 'simone@tecnotrade.com';
        $dtoCustomerTecnotradeInsertBefore->website = 'tecnotrade.com';
        $dtoCustomerTecnotradeInsertBefore->idLanguage = $user->language_id;
        $dtoCustomerTecnotradeInsertBefore->idVatType = $vatTypeInsert->id;
        $dtoCustomerTecnotradeInsertBefore->enableFrom = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $dtoCustomerTecnotradeInsertBefore->enableTo = $dtoCustomerTecnotradeInsertBefore->enableFrom;
        $dtoCustomerTecnotradeInsertBefore->sendNewsletter = true;
        $dtoCustomerTecnotradeInsertBefore->errorNewsletter = false;

        #endregion PARAMETER

        $response = $this->putJson('/api/v1/customer', $dtoCustomerTecnotradeInsertBefore->toArray());
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        #region PARAMETER

        $customerAfterInsert = Customer::where('business_name', 'TecnotradeInsert srl')->first();

        $dtoCustomerTecnotradeInsertAfter = new DtoCustomer();
        $dtoCustomerTecnotradeInsertAfter->company = $customerAfterInsert->company;
        $dtoCustomerTecnotradeInsertAfter->businessName = $customerAfterInsert->business_name;
        $dtoCustomerTecnotradeInsertAfter->name = $customerAfterInsert->name;
        $dtoCustomerTecnotradeInsertAfter->surname = $customerAfterInsert->surname;
        $dtoCustomerTecnotradeInsertAfter->vatNumber = $customerAfterInsert->vat_number;
        $dtoCustomerTecnotradeInsertAfter->fiscalCode = $customerAfterInsert->fiscal_code;
        $dtoCustomerTecnotradeInsertAfter->address = $customerAfterInsert->address;
        $dtoCustomerTecnotradeInsertAfter->idPostalCode = $customerAfterInsert->postal_code_id;
        $dtoCustomerTecnotradeInsertAfter->telephoneNumber = $customerAfterInsert->telephone_number;
        $dtoCustomerTecnotradeInsertAfter->faxNumber = $customerAfterInsert->fax_number;
        $dtoCustomerTecnotradeInsertAfter->email = $customerAfterInsert->email;
        $dtoCustomerTecnotradeInsertAfter->website = $customerAfterInsert->website;
        $dtoCustomerTecnotradeInsertAfter->idLanguage = $customerAfterInsert->language_id;
        $dtoCustomerTecnotradeInsertAfter->idVatType = $customerAfterInsert->vat_type_id;
        $dtoCustomerTecnotradeInsertAfter->enableFrom = $customerAfterInsert->enable_from;
        $dtoCustomerTecnotradeInsertAfter->enableTo = $customerAfterInsert->enable_to;
        $dtoCustomerTecnotradeInsertAfter->sendNewsletter = $customerAfterInsert->send_newsletter;
        $dtoCustomerTecnotradeInsertAfter->errorNewsletter = $customerAfterInsert->error_newsletter;

        #endregion PARAMETER

        $this->assertEquals($dtoCustomerTecnotradeInsertBefore, $dtoCustomerTecnotradeInsertAfter);

        #region PARAMETER

        $customerAfterInsert->delete();
        $vatTypeInsert->delete();
        $postalCodeInsert->delete();

        #endregion PARAMETER
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @return void
     */
    public function testUpdate()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $postalCodeUpdate = new PostalCode();
        $postalCodeUpdate->code = '40017';
        $postalCodeUpdate->created_id = $user->id;
        $postalCodeUpdate->save();

        $postalCode2Update = new PostalCode();
        $postalCode2Update->code = '40018';
        $postalCode2Update->created_id = $user->id;
        $postalCode2Update->save();

        $vatTypeUpdate = new VatType();
        $vatTypeUpdate->rate = 20;
        $vatTypeUpdate->created_id = $user->id;
        $vatTypeUpdate->save();

        $vatType2Update = new VatType();
        $vatType2Update->rate = 10;
        $vatType2Update->created_id = $user->id;
        $vatType2Update->save();

        $languageUpdate = new Language();
        $languageUpdate->description = 'Italianoo';
        $languageUpdate->default = false;
        $languageUpdate->created_id = $user->id;
        $languageUpdate->date_format = 'd/m/Y';
        $languageUpdate->date_time_format = 'd/m/Y H:i';
        $languageUpdate->date_format_client = 'DD/MM/YYYY';
        $languageUpdate->date_time_format_client = 'DD/MM/YYYY HH:mm';
        $languageUpdate->save();

        $customerTecnotradeUpdate = new Customer();
        $customerTecnotradeUpdate->company = true;
        $customerTecnotradeUpdate->business_name = 'TecnotradeUpdate srl';
        $customerTecnotradeUpdate->name = 'Simone';
        $customerTecnotradeUpdate->surname = 'Resca';
        $customerTecnotradeUpdate->vat_number = '1234567890';
        $customerTecnotradeUpdate->fiscal_code = 'SMNRSC75D17F257K';
        $customerTecnotradeUpdate->address = 'Via Giulio Cesare Costa 18';
        $customerTecnotradeUpdate->postal_code_id = $postalCodeUpdate->id;
        $customerTecnotradeUpdate->telephone_number = '051123456';
        $customerTecnotradeUpdate->fax_number = '051123457';
        $customerTecnotradeUpdate->email = 'simone@tecnotrade.com';
        $customerTecnotradeUpdate->website = 'tecnotrade.com';
        $customerTecnotradeUpdate->language_id = $user->language_id;
        $customerTecnotradeUpdate->vat_type_id = $vatTypeUpdate->id;
        $customerTecnotradeUpdate->enable_from = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $customerTecnotradeUpdate->enable_to = $customerTecnotradeUpdate->enable_from;
        $customerTecnotradeUpdate->created_id = $user->id;
        $customerTecnotradeUpdate->send_newsletter = true;
        $customerTecnotradeUpdate->error_newsletter = false;
        $customerTecnotradeUpdate->save();

        $dtoCustomerTecnotradeUpdateBefore = new DtoCustomer();
        $dtoCustomerTecnotradeUpdateBefore->id = $customerTecnotradeUpdate->id;
        $dtoCustomerTecnotradeUpdateBefore->company = false;
        $dtoCustomerTecnotradeUpdateBefore->businessName = 'TecnotradeUpdatee srll';
        $dtoCustomerTecnotradeUpdateBefore->name = 'Simonee';
        $dtoCustomerTecnotradeUpdateBefore->surname = 'Rescaa';
        $dtoCustomerTecnotradeUpdateBefore->vatNumber = '12345678900';
        $dtoCustomerTecnotradeUpdateBefore->fiscalCode = 'SMNRSC75D17F257KK';
        $dtoCustomerTecnotradeUpdateBefore->address = 'Via Giulio Cesare Costa 188';
        $dtoCustomerTecnotradeUpdateBefore->idPostalCode = $postalCode2Update->id;
        $dtoCustomerTecnotradeUpdateBefore->telephoneNumber = '0511234567';
        $dtoCustomerTecnotradeUpdateBefore->faxNumber = '0511234577';
        $dtoCustomerTecnotradeUpdateBefore->email = 'simone@tecnotradee.com';
        $dtoCustomerTecnotradeUpdateBefore->website = 'tecnotradee.com';
        $dtoCustomerTecnotradeUpdateBefore->idLanguage = $languageUpdate->id;
        $dtoCustomerTecnotradeUpdateBefore->idVatType = $vatType2Update->id;
        $dtoCustomerTecnotradeUpdateBefore->enableFrom = $customerTecnotradeUpdate->enable_from;
        $dtoCustomerTecnotradeUpdateBefore->enableTo = $dtoCustomerTecnotradeUpdateBefore->enableFrom;
        $dtoCustomerTecnotradeUpdateBefore->sendNewsletter = false;
        $dtoCustomerTecnotradeUpdateBefore->errorNewsletter = true;

        #endregion PARAMETER

        $response = $this->postJson('/api/v1/customer', $dtoCustomerTecnotradeUpdateBefore->toArray());
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        #region PARAMETER

        $customerAfterInsert = Customer::where('business_name', 'TecnotradeUpdatee srll')->first();

        $dtoCustomerTecnotradeUpdateAfter = new DtoCustomer();
        $dtoCustomerTecnotradeUpdateAfter->id = $customerAfterInsert->id;
        $dtoCustomerTecnotradeUpdateAfter->company = $customerAfterInsert->company;
        $dtoCustomerTecnotradeUpdateAfter->businessName = $customerAfterInsert->business_name;
        $dtoCustomerTecnotradeUpdateAfter->name = $customerAfterInsert->name;
        $dtoCustomerTecnotradeUpdateAfter->surname = $customerAfterInsert->surname;
        $dtoCustomerTecnotradeUpdateAfter->vatNumber = $customerAfterInsert->vat_number;
        $dtoCustomerTecnotradeUpdateAfter->fiscalCode = $customerAfterInsert->fiscal_code;
        $dtoCustomerTecnotradeUpdateAfter->address = $customerAfterInsert->address;
        $dtoCustomerTecnotradeUpdateAfter->idPostalCode = $customerAfterInsert->postal_code_id;
        $dtoCustomerTecnotradeUpdateAfter->telephoneNumber = $customerAfterInsert->telephone_number;
        $dtoCustomerTecnotradeUpdateAfter->faxNumber = $customerAfterInsert->fax_number;
        $dtoCustomerTecnotradeUpdateAfter->email = $customerAfterInsert->email;
        $dtoCustomerTecnotradeUpdateAfter->website = $customerAfterInsert->website;
        $dtoCustomerTecnotradeUpdateAfter->idLanguage = $customerAfterInsert->language_id;
        $dtoCustomerTecnotradeUpdateAfter->idVatType = $customerAfterInsert->vat_type_id;
        $dtoCustomerTecnotradeUpdateAfter->enableFrom = $customerAfterInsert->enable_from;
        $dtoCustomerTecnotradeUpdateAfter->enableTo = $customerAfterInsert->enable_to;
        $dtoCustomerTecnotradeUpdateAfter->sendNewsletter = $customerAfterInsert->send_newsletter;
        $dtoCustomerTecnotradeUpdateAfter->errorNewsletter = $customerAfterInsert->error_newsletter;

        #endregion PARAMETER

        $this->assertEquals($dtoCustomerTecnotradeUpdateBefore, $dtoCustomerTecnotradeUpdateAfter);

        #region PARAMETER

        $customerAfterInsert->delete();
        $languageUpdate->delete();
        $vatType2Update->delete();
        $vatTypeUpdate->delete();
        $postalCode2Update->delete();
        $postalCodeUpdate->delete();

        #endregion PARAMETER
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Update
     *
     * @return void
     */
    public function testDelete()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $postalCodeDelete = new PostalCode();
        $postalCodeDelete->code = '40017';
        $postalCodeDelete->created_id = $user->id;
        $postalCodeDelete->save();

        $vatTypeDelete = new VatType();
        $vatTypeDelete->rate = 20;
        $vatTypeDelete->created_id = $user->id;
        $vatTypeDelete->save();

        $customerTecnotradeDelete = new Customer();
        $customerTecnotradeDelete->company = true;
        $customerTecnotradeDelete->business_name = 'TecnotradeDelete srl';
        $customerTecnotradeDelete->name = 'Simone';
        $customerTecnotradeDelete->surname = 'Resca';
        $customerTecnotradeDelete->vat_number = '1234567890';
        $customerTecnotradeDelete->fiscal_code = 'SMNRSC75D17F257K';
        $customerTecnotradeDelete->address = 'Via Giulio Cesare Costa 18';
        $customerTecnotradeDelete->postal_code_id = $postalCodeDelete->id;
        $customerTecnotradeDelete->telephone_number = '051123456';
        $customerTecnotradeDelete->fax_number = '051123457';
        $customerTecnotradeDelete->email = 'simone@tecnotrade.com';
        $customerTecnotradeDelete->website = 'tecnotrade.com';
        $customerTecnotradeDelete->language_id = $user->language_id;
        $customerTecnotradeDelete->vat_type_id = $vatTypeDelete->id;
        $customerTecnotradeDelete->enable_from = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $customerTecnotradeDelete->enable_to = $customerTecnotradeDelete->enable_from;
        $customerTecnotradeDelete->created_id = $user->id;
        $customerTecnotradeDelete->send_newsletter = true;
        $customerTecnotradeDelete->error_newsletter = false;
        $customerTecnotradeDelete->save();

        #endregion PARAMETER

        $response = $this->delete('/api/v1/customer/' . $customerTecnotradeDelete->id);
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');
        $this->assertNull(Customer::find($customerTecnotradeDelete->id));

        #region PARAMETER

        $vatTypeDelete->delete();
        $postalCodeDelete->delete();

        #endregion PARAMETER
    }

    #endregion DELETE
}

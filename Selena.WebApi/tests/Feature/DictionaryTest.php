<?php

namespace Tests\Feature;

use App\BusinessLogic\DictionaryBL;
use App\Helpers\LogHelper;
use App\Language;
use Tests\TestCase;

// class DictionaryTest extends TestCase
// {
//     #region V1

//     /*
//      * v1 / dictionary / getByLanguage
//      */
//     public function testDictionaryGetByLanguage()
//     {
//         $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;

//         $response = $this->get('/api/v1/dictionary/getByLanguage/' . $idLanguage);
//         $response->assertStatus(200)
//             ->assertSuccessful()
//             ->assertSeeText('data');

//         $this->assertEquals(DictionaryBL::getByLanguage($idLanguage)->map(function ($model) {
//             return [
//                 $model->code => $model->description
//             ];
//         })->toArray(), $response->json()['data']);
//     }

//     #endregion V1
// }

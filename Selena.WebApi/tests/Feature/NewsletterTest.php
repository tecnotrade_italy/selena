<?php

namespace Tests\Feature;

use App\DtoModel\DtoNewsletter;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Newsletter;
use App\User;
use Carbon\Carbon;
use Laravel\Passport\Passport;
use Tests\TestCase;

class NewsletterTest extends TestCase
{
    #region GET

    /**
     * Get all
     *
     * @return void
     */
    public function testGetAll()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $newsletter = new Newsletter();
        $newsletter->description = 'NewsletterGetAll';
        $newsletter->html = '<body>NewsletterGetAll</body>';
        $newsletter->draft = true;
        $newsletter->url = '/newsletter/all';
        $newsletter->schedule_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->start_send_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->end_send_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->created_id = $user->id;
        $newsletter->title = 'NewsletterGetAll';
        $newsletter->save();

        $dtoNewsletter = new DtoNewsletter();
        $dtoNewsletter->id = $newsletter->id;
        $dtoNewsletter->description = $newsletter->description;
        $dtoNewsletter->html = $newsletter->html;
        $dtoNewsletter->draft = $newsletter->draft;
        $dtoNewsletter->url = $newsletter->url;
        $dtoNewsletter->scheduleDateTime = $newsletter->schedule_date_time;
        $dtoNewsletter->startSendDateTime = $newsletter->start_send_date_time;
        $dtoNewsletter->endSendDateTime = $newsletter->end_send_date_time;
        $dtoNewsletter->title = $newsletter->title;

        #endregion PARAMETER

        $response = $this->get('/api/v1/newsletter/all');
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');
        $this->assertEquals(Newsletter::all()->map(function ($model) {
            return [
                'id' => $model->id,
                'description' => $model->description,
                'html' => $model->html,
                'draft' => $model->draft,
                'url' => $model->url,
                'scheduleDateTime' => $model->schedule_date_time,
                'startSendDateTime' => $model->start_send_date_time,
                'endSendDateTime' => $model->end_send_date_time,
                'title' => $model->title,
            ];
        })->toArray(), $response->json()['data']);

        #region PARAMETER

        $newsletter->delete();

        #endregion PARAMETER
    }

    /**
     * Get by Id
     *
     * @return void
     */
    public function testGetById()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $newsletter = new Newsletter();
        $newsletter->description = 'NewsletterGetById';
        $newsletter->html = '<body>NewsletterGetById</body>';
        $newsletter->draft = true;
        $newsletter->url = '/newsletter/byId';
        $newsletter->schedule_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->start_send_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->end_send_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->created_id = $user->id;
        $newsletter->title = 'NewsletterGetById';
        $newsletter->save();

        $dtoNewsletter = new DtoNewsletter();
        $dtoNewsletter->id = $newsletter->id;
        $dtoNewsletter->description = $newsletter->description;
        $dtoNewsletter->html = $newsletter->html;
        $dtoNewsletter->draft = $newsletter->draft;
        $dtoNewsletter->url = $newsletter->url;
        $dtoNewsletter->scheduleDateTime = $newsletter->schedule_date_time;
        $dtoNewsletter->startSendDateTime = $newsletter->start_send_date_time;
        $dtoNewsletter->endSendDateTime = $newsletter->end_send_date_time;
        $dtoNewsletter->title = $newsletter->title;

        #endregion PARAMETER

        $response = $this->get('/api/v1/newsletter/' . $newsletter->id);
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');
        $this->assertEquals($dtoNewsletter->toArray(), $response->json()['data']);

        #region PARAMETER

        $newsletter->delete();

        #endregion PARAMETER
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @return void
     */
    public function testInsert()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $dtoNewsletterBefore = new DtoNewsletter();
        $dtoNewsletterBefore->description = 'NewsletterInsert';
        $dtoNewsletterBefore->html = '<body>NewsletterInsert</body>';
        $dtoNewsletterBefore->draft = true;
        $dtoNewsletterBefore->url = '/newsletter/byId';
        $dtoNewsletterBefore->scheduleDateTime = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $dtoNewsletterBefore->startSendDateTime = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $dtoNewsletterBefore->endSendDateTime = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $dtoNewsletterBefore->title = 'NewsletterInsert';

        #endregion PARAMETER

        $response = $this->put('/api/v1/newsletter', $dtoNewsletterBefore->toArray());
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        #region PARAMETER

        $newsletter = Newsletter::where('description', 'NewsletterInsert')->first();

        $dtoNewsletterAfter = new DtoNewsletter();
        $dtoNewsletterAfter->description = $newsletter->description;
        $dtoNewsletterAfter->html = $newsletter->html;
        $dtoNewsletterAfter->draft = $newsletter->draft;
        $dtoNewsletterAfter->url = $newsletter->url;
        $dtoNewsletterAfter->scheduleDateTime = $newsletter->schedule_date_time;
        $dtoNewsletterAfter->startSendDateTime = $newsletter->start_send_date_time;
        $dtoNewsletterAfter->endSendDateTime = $newsletter->end_send_date_time;
        $dtoNewsletterAfter->title = $newsletter->title;

        #endregion PARAMETER

        $this->assertEquals($dtoNewsletterBefore, $dtoNewsletterAfter);

        #region PARAMETER

        $newsletter->delete();

        #endregion PARAMETER
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @return void
     */
    public function testUpdate()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $newsletter = new Newsletter();
        $newsletter->description = 'NewsletterUpdate';
        $newsletter->html = '<body>NewsletterUpdate</body>';
        $newsletter->draft = true;
        $newsletter->url = '/newsletter/update';
        $newsletter->schedule_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->start_send_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->end_send_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->created_id = $user->id;
        $newsletter->title = 'NewsletterUpdate';
        $newsletter->save();

        $dtoNewsletterBefore = new DtoNewsletter();
        $dtoNewsletterBefore->id = $newsletter->id;
        $dtoNewsletterBefore->description = 'NewsletterUpdatee';
        $dtoNewsletterBefore->html = '<body>NewsletterUpdatee</body>';
        $dtoNewsletterBefore->draft = false;
        $dtoNewsletterBefore->url = '/newsletter/updatee';
        $dtoNewsletterBefore->scheduleDateTime = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $dtoNewsletterBefore->startSendDateTime = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $dtoNewsletterBefore->endSendDateTime = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $dtoNewsletterBefore->title = 'NewsletterUpdatee';

        #endregion PARAMETER

        $response = $this->post('/api/v1/newsletter', $dtoNewsletterBefore->toArray());
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        #region PARAMETER

        $newsletterAfter = Newsletter::find($newsletter->id);

        $dtoNewsletterAfter = new DtoNewsletter();
        $dtoNewsletterAfter->id = $newsletterAfter->id;
        $dtoNewsletterAfter->description = $newsletterAfter->description;
        $dtoNewsletterAfter->html = $newsletterAfter->html;
        $dtoNewsletterAfter->draft = $newsletterAfter->draft;
        $dtoNewsletterAfter->url = $newsletterAfter->url;
        $dtoNewsletterAfter->scheduleDateTime = $newsletterAfter->schedule_date_time;
        $dtoNewsletterAfter->startSendDateTime = $newsletterAfter->start_send_date_time;
        $dtoNewsletterAfter->endSendDateTime = $newsletterAfter->end_send_date_time;
        $dtoNewsletterAfter->title = $newsletterAfter->title;

        #endregion PARAMETER

        $this->assertEquals($dtoNewsletterBefore, $dtoNewsletterAfter);

        #region PARAMETER

        $newsletter->delete();

        #endregion PARAMETER
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @return void
     */
    public function testDelete()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $newsletter = new Newsletter();
        $newsletter->description = 'NewsletterDelete';
        $newsletter->html = '<body>NewsletterDelete</body>';
        $newsletter->draft = true;
        $newsletter->url = '/newsletter/delete';
        $newsletter->schedule_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->start_send_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->end_send_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->created_id = $user->id;
        $newsletter->title = 'NewsletterDelete';
        $newsletter->save();

        #endregion PARAMETER

        $response = $this->delete('/api/v1/newsletter/' . $newsletter->id);
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');
        $this->assertNull(Newsletter::find($newsletter->id));
    }

    #endregion DELETE
}

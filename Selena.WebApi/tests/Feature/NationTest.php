<?php

namespace Tests\Feature;

use App\DtoModel\DtoNation;
use App\Helpers\LogHelper;
use App\Language;
use App\LanguageNation;
use App\Nation;
use App\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class NationTest extends TestCase
{
    #region GET

    /**
     * Get all
     *
     * @return void
     */
    public function testGetAll()
    {
        #region PARAMETER

        // $user = new User();
        // $user->name = 'admin';
        // $user->email = 'admin@tecnotrade.com';
        // $user->password = bcrypt('password1');
        // $user->save();

        // $idUser = User::first()->id;

        // $language = new Language();
        // $language->description = 'Italiano';
        // $language->default = true;
        // $language->created_id = $idUser;
        // $language->date_format = 'd/m/Y';
        // $language->date_time_format = 'd/m/Y H:i';
        // $language->date_format_client = 'DD/MM/YYYY';
        // $language->date_time_format_client = 'DD/MM/YYYY HH:mm';
        // $language->save();

        // $user = User::first();
        // $user->language_id = $language->id;
        // $user->save();

        // $user = new User();
        // $user->name = 'newsletter';
        // $user->email = 'admin@tecnotrade.com';
        // $user->password = bcrypt('password1');
        // $user->save();

        // $user = User::first();
        // $user->language_id = $language->id;
        // $user->save();

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;

        $nation = new Nation();
        $nation->cee = true;
        $nation->abbreviation = 'IT';
        $nation->created_id = $user->id;
        $nation->save();

        $languageNation = new LanguageNation();
        $languageNation->language_id = $idLanguage;
        $languageNation->nation_id = $nation->id;
        $languageNation->description = 'Italia';
        $languageNation->created_id = $user->id;
        $languageNation->save();

        #endregion PARAMETER

        $response = $this->get('/api/v1/nation/all/' . $idLanguage);

        $response->assertStatus(200)
            ->assertSuccessful()
            ->assertSeeText('data');

        $this->assertEquals(Nation::all()->map(function ($model) {
            return [
                'id' => $model->id,
                'cee' => $model->cee,
                'abbreviation' => $model->abbreviation,
                'description' => LanguageNation::where('language_id', '=', Language::where('description', 'Italiano')->first()->id)->first()->description,
            ];
        })->toArray(), $response->json()['data']);

        #region PARAMETER

        $languageNation->delete();
        $nation->delete();

        #endregion PARAMETER
    }

    /**
     * Get by id
     *
     * @return void
     */
    public function testGetById()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;

        $nation = new Nation();
        $nation->cee = true;
        $nation->abbreviation = 'IT';
        $nation->created_id = $user->id;
        $nation->save();

        $languageNation = new LanguageNation();
        $languageNation->language_id = $idLanguage;
        $languageNation->nation_id = $nation->id;
        $languageNation->description = 'Italia';
        $languageNation->created_id = $user->id;
        $languageNation->save();

        $dtoNation = new DtoNation();
        $dtoNation->id = $nation->id;
        $dtoNation->cee = $nation->cee;
        $dtoNation->abbreviation = $nation->abbreviation;
        $dtoNation->description = $languageNation->description;

        #endregion PARAMETER

        $response = $this->get('/api/v1/nation/' . $nation->id);
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');
        $this->assertEquals($dtoNation->toArray(), $response->json()['data']);

        #region PARAMETER

        $languageNation->delete();
        $nation->delete();

        #endregion PARAMETER
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @return void
     */
    public function testInsert()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;

        $nation = new Nation();
        $nation->cee = true;
        $nation->abbreviation = 'IT';
        $nation->created_id = $user->id;

        $languageNation = new LanguageNation();
        $languageNation->language_id = $idLanguage;
        $languageNation->nation_id = $nation->id;
        $languageNation->description = 'Italia';
        $languageNation->created_id = $user->id;

        $dtoNationBefore = new DtoNation();
        $dtoNationBefore->cee = $nation->cee;
        $dtoNationBefore->abbreviation = $nation->abbreviation;
        $dtoNationBefore->description = $languageNation->description;

        #endregion PARAMETER

        $response = $this->putJson('/api/v1/nation', $dtoNationBefore->toArray());
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        #region PARAMETER

        $nationAfter = Nation::where('abbreviation', 'IT')->first();
        $nationLanguageAfter = LanguageNation::where('nation_id', $nationAfter->id)->first();

        $dtoNationAfter = new DtoNation();
        $dtoNationAfter->cee = $nationAfter->cee;
        $dtoNationAfter->abbreviation = $nationAfter->abbreviation;
        $dtoNationAfter->description = $nationLanguageAfter->description;

        #endregion PARAMETER
        $this->assertEquals($nationAfter->id, $response->json()['data']);
        $this->assertEquals($dtoNationAfter->toArray(), $dtoNationBefore->toArray());

        #region PARAMETER
        $nationLanguageAfter->delete();
        $nationAfter->delete();

        #endregion PARAMETER
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @return void
     */
    public function testUpdate()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;

        $nation = new Nation();
        $nation->cee = true;
        $nation->abbreviation = 'IT';
        $nation->created_id = $user->id;
        $nation->save();

        $languageNation = new LanguageNation();
        $languageNation->language_id = $idLanguage;
        $languageNation->nation_id = $nation->id;
        $languageNation->description = 'Italia';
        $languageNation->created_id = $user->id;
        $languageNation->save();


        $dtoNationBefore = new DtoNation();
        $dtoNationBefore->id = $nation->id;
        $dtoNationBefore->cee = false;
        $dtoNationBefore->abbreviation = 'ITAA';
        $dtoNationBefore->description = 'IALIANO';


        #endregion PARAMETER

        $response = $this->postJson('/api/v1/nation', $dtoNationBefore->toArray());

        #region PARAMETER

        $nationAfter = Nation::find($nation->id);
        $nationLanguageAfter = LanguageNation::find($nationAfter->id)->first();

        $dtoNationAfter = new DtoNation();
        $dtoNationAfter->id = $nationAfter->id;
        $dtoNationAfter->cee = $nationAfter->cee;
        $dtoNationAfter->abbreviation = $nationAfter->abbreviation;
        $dtoNationAfter->description = $nationLanguageAfter->description;

        #endregion PARAMETER

        $response->assertStatus(200)->assertSuccessful();

        $this->assertEquals($dtoNationAfter, $dtoNationBefore);

        #region PARAMETER

        $languageNation->delete();
        $nation->delete();

        #endregion PARAMETER
    }

    #endregion UPDATE


    #region DELETE

    /**
     * Delete
     *
     * @return void
     */
    public function testDelete()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;

        $nation = new Nation();
        $nation->cee = true;
        $nation->abbreviation = 'IT';
        $nation->created_id = $user->id;
        $nation->save();

        $languageNation = new LanguageNation();
        $languageNation->language_id = $idLanguage;
        $languageNation->nation_id = $nation->id;
        $languageNation->description = 'Italia';
        $languageNation->created_id = $user->id;
        $languageNation->save();

        #endregion PARAMETER

        $response = $this->delete('/api/v1/nation/' . $nation->id);

        $response->assertStatus(200)
            ->assertSuccessful();

        $this->assertNull(Nation::find($nation->id));
        $this->assertNull(LanguageNation::where('nation_id', $nation->id)->first());
    }

    #endregion DELETE


}

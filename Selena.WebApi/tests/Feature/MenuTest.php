<?php

namespace Tests\Feature;

use App\BusinessLogic\MenuBL;
use App\DtoModel\DtoLanguageMenu;
use App\DtoModel\DtoMenu;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenu;
use App\User;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Passport;
use Tests\TestCase;
use App\Helpers\LogHelper;

// class MenuTest extends TestCase
// {
//     #region V1

//     #region GET

//     /*
//      * v1 / menu / getSettings {Link}
//      */
//     public function testMenuGetSettingsLink()
//     {
//         Passport::actingAs(User::where('name', '=', 'admin')->first());
//         $menuLink = LanguageMenu::whereNotNull('link')->first();

//         if (isset($menuLink)) {
//             $response = $this->get('/api/v1/menu/getSettings/' . $menuLink->menu_id);
//             $response->assertStatus(200)
//                 ->assertSuccessful()
//                 ->assertSeeText('data')
//                 ->assertSee(json_encode(MenuBL::getSettings($menuLink->menu_id)));
//         }
//     }

    /*
     * v1 / menu / getSettings {Folder}
     */
    // public function testMenuGetSettingsFolder()
    // {
    //     Passport::actingAs(User::where('name', '=', 'admin')->first());
    //     $menuFolder = DB::select('  SELECT menus.id
    //                                 FROM menus
    //                                 INNER JOIN languages_menus ON menus.id = languages_menus.menu_id
    //                                 WHERE menus.page_id IS NULL
    //                                 AND languages_menus.link IS NULL');

    //     if (isset($menuFolder) && count($menuFolder) > 0) {
    //         $response = $this->get('/api/v1/menu/getSettings/' . $menuFolder[0]->id);
    //         $response->assertStatus(200)
    //             ->assertSuccessful()
    //             ->assertSeeText('data')
    //             ->assertSee(json_encode(MenuBL::getSettings($menuFolder[0]->id)));
    //     }
    // }

    // #endregion GET

    // #region PUT

    // /*
    //  * PUT /api/v1/menu/link
    //  */
    // public function testMenuPutLink()
    // {
    //     $icon = Icon::first();
    //     $idIcon = isset($icon) ? $icon->id : null;

    //     $languageMenu = new DtoLanguageMenu();
    //     $languageMenu->idLanguage = Language::first()->id;
    //     $languageMenu->title = 'folderDiTest';
    //     $languageMenu->url = 'www.google.it';

    //     $data = new DtoMenu();
    //     $data->idFunctionality = Functionality::where('code', 'Pages')->first()->id;
    //     $data->languagesMenus->push($languageMenu);
    //     $data->idIcon = $idIcon;
    //     $data->newWindow = true;
    //     $data->hideMobile = true;

    //     Passport::actingAs(User::where('name', '=', 'admin')->first());
    //     $response = $this->putJson('/api/v1/menu/link', $data->toArray());

    //     $response->assertStatus(200)
    //         ->assertSuccessful()
    //         ->assertSeeText('data');

    //     $dtoResult = MenuBL::getSettings($response->json()['data']);
    //     $data->id = $dtoResult->id;
    //     $data->idFunctionality = null;
    //     $data->newWindow = 1;
    //     $data->hideMobile = 1;
    //     $data->order = $dtoResult->order;
    //     $this->assertEquals($data, $dtoResult);
    // }

    // /*
    //  * PUT /api/v1/menu/folder
    //  */
    // public function testMenuPutFolder()
    // {
    //     $icon = Icon::first();
    //     $idIcon = isset($icon) ? $icon->id : null;

    //     $languageMenu = new DtoLanguageMenu();
    //     $languageMenu->idLanguage = Language::first()->id;
    //     $languageMenu->title = 'folderDiTest';

    //     $data = new DtoMenu();
    //     $data->idFunctionality = Functionality::where('code', 'Pages')->first()->id;
    //     $data->languagesMenus->push($languageMenu);
    //     $data->idIcon = $idIcon;
    //     $data->hideMobile = true;

    //     Passport::actingAs(User::where('name', '=', 'admin')->first());
    //     $response = $this->putJson('/api/v1/menu/folder', $data->toArray());

    //     $response->assertStatus(200)
    //         ->assertSuccessful()
    //         ->assertSeeText('data');

    //     $dtoResult = MenuBL::getSettings($response->json()['data']);
    //     $data->id = $dtoResult->id;
    //     $data->idFunctionality = null;
    //     $data->newWindow = 0;
    //     $data->hideMobile = 1;
    //     $data->order = $dtoResult->order;
    //     $this->assertEquals($data, $dtoResult);
    // }

    // #endregion PUT

    // #region POST

    /*
     * POST /api/v1/menu/link
     */
    // public function testMenuPostLink()
    // {
    //     $dtoMenu = MenuBL::getSettings(LanguageMenu::whereNotNull('link')->first()->menu_id);
    //     $dtoMenu->idFunctionality = Functionality::where('code', 'Pages')->first()->id;
    //     foreach ($dtoMenu->languagesMenus as $languageMenu) {
    //         $languageMenu->title = 'titoloLinkAggiornato' . $languageMenu->idLanguage;
    //         $languageMenu->url = 'linkAggiornato1';
    //     }

    //     Passport::actingAs(User::where('name', '=', 'admin')->first());
    //     $response = $this->postJson('/api/v1/menu/link', $dtoMenu->toArray());

    //     $response->assertStatus(200)
    //         ->assertSuccessful()
    //         ->assertSeeText('data');

    //     $dtoResult = MenuBL::getSettings($dtoMenu->id);
    //     $dtoMenu->idFunctionality = null;
    //     $this->assertEquals($dtoMenu, $dtoResult);
    // }

    // /*
    //  * POST /api/v1/menu/folder
    //  */
    // public function testMenuPostFolder()
    // {
    //     $idsMenu = DB::select(' SELECT menus.id
    //                             FROM menus
    //                             INNER JOIN languages_menus ON menus.id = languages_menus.menu_id
    //                             WHERE menus.page_id IS NULL
    //                             AND languages_menus.link IS NULL');

    //     if (!is_null($idsMenu) && count($idsMenu)) {
    //         $dtoMenu = MenuBL::getSettings($idsMenu[0]->id);

    //         $dtoMenu->idFunctionality = Functionality::where('code', 'Pages')->first()->id;
    //         foreach ($dtoMenu->languagesMenus as $languageMenu) {
    //             $languageMenu->title = 'titoloFolderAggiornato' . $languageMenu->idLanguage;
    //         }

    //         Passport::actingAs(User::where('name', '=', 'admin')->first());
    //         $response = $this->postJson('/api/v1/menu/folder', $dtoMenu->toArray());

    //         $response->assertStatus(200)
    //             ->assertSuccessful()
    //             ->assertSeeText('data');

    //         $dtoResult = MenuBL::getSettings($dtoMenu->id);
    //         $dtoMenu->idFunctionality = null;
    //         $this->assertEquals($dtoMenu, $dtoResult);
    //     }
    // }

    // #endregion POST

    // #endregion V1
// }

<?php

namespace Tests\Feature;

use App\Contact;
use App\ContactRequest;
use App\DtoModel\DtoContactRequest;
use App\Helpers\HttpHelper;
use App\User;
use Carbon\Carbon;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ContactRequestTest extends TestCase
{
    #region GET

    /**
     * Get all
     *
     * @return void
     */
    public function testGetAll()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $contactAndreaGetAll = new Contact();
        $contactAndreaGetAll->name = 'AndreaGetAll';
        $contactAndreaGetAll->surname = 'Prandini';
        $contactAndreaGetAll->business_name = 'Tecnotrade srl';
        $contactAndreaGetAll->email = 'admin@tecnotrade.com';
        $contactAndreaGetAll->phone_number = '051123456';
        $contactAndreaGetAll->send_newsletter = true;
        $contactAndreaGetAll->error_newsletter = false;
        $contactAndreaGetAll->created_id = $user->id;
        $contactAndreaGetAll->save();

        $contactRequestGetAll = new ContactRequest();
        $contactRequestGetAll->contact_id = $contactAndreaGetAll->id;
        $contactRequestGetAll->description = "ContactRequestGetAll";
        $contactRequestGetAll->created_id = $user->id;
        $contactRequestGetAll->save();

        $contactRequestGetAll_1 = new ContactRequest();
        $contactRequestGetAll_1->contact_id = $contactAndreaGetAll->id;
        $contactRequestGetAll_1->description = "ContactRequestGetAll_1";
        $contactRequestGetAll_1->created_id = $user->id;
        $contactRequestGetAll_1->save();

        #endregion PARAMETER

        $response = $this->get('/api/v1/contactrequest/all');
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        $this->assertEquals(ContactRequest::all()->map(function ($model) {
            $contact = Contact::find($model->contact_id);

            return [
                'id' => $model->id,
                'name' => $contact->name,
                'surname' => $contact->surname,
                'email' => $contact->email,
                'phoneNumber' => $contact->phone_number,
                'date' => Carbon::parse($model->created_at)->format(HttpHelper::getLanguageDateTimeFormat()),
                'description' => $model->description
            ];
        })->toArray(), $response->json()['data']);

        #region PARAMETER

        $contactRequestGetAll->delete();
        $contactRequestGetAll_1->delete();
        $contactAndreaGetAll->delete();

        #endregion PARAMETER
    }

    /**
     * Get by id
     *
     * @return void
     */
    public function testGetById()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $contactAndreaGetById = new Contact();
        $contactAndreaGetById->name = 'AndreaGetById';
        $contactAndreaGetById->surname = 'Prandini';
        $contactAndreaGetById->business_name = 'Tecnotrade srl';
        $contactAndreaGetById->email = 'admin@tecnotrade.com';
        $contactAndreaGetById->phone_number = '051123456';
        $contactAndreaGetById->send_newsletter = true;
        $contactAndreaGetById->error_newsletter = false;
        $contactAndreaGetById->created_id = $user->id;
        $contactAndreaGetById->save();

        $contactRequestGetById = new ContactRequest();
        $contactRequestGetById->contact_id = $contactAndreaGetById->id;
        $contactRequestGetById->description = "ContactRequestGetById";
        $contactRequestGetById->created_id = $user->id;
        $contactRequestGetById->save();

        $dtoContactRequestGetById = new DtoContactRequest();
        $dtoContactRequestGetById->id = $contactRequestGetById->id;
        $dtoContactRequestGetById->name = $contactAndreaGetById->name;
        $dtoContactRequestGetById->surname = $contactAndreaGetById->surname;
        $dtoContactRequestGetById->email = $contactAndreaGetById->email;
        $dtoContactRequestGetById->phoneNumber = $contactAndreaGetById->phone_number;
        $dtoContactRequestGetById->date = Carbon::parse($contactRequestGetById->created_at)->format(HttpHelper::getLanguageDateTimeFormat());
        $dtoContactRequestGetById->description = $contactRequestGetById->description;

        #endregion PARAMETER

        $response = $this->get('/api/v1/contactrequest/' . $contactRequestGetById->id);
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');
        $this->assertEquals($dtoContactRequestGetById->toArray(), $response->json()['data']);

        #region PARAMETER

        $contactRequestGetById->delete();
        $contactAndreaGetById->delete();

        #endregion PARAMETER
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @return void
     */
    public function testInsert()
    {
        #region PARAMETER

        // $user = User::where('name', 'admin')->first();
        // Passport::actingAs($user);

        $dtoContactRequestBefore = new DtoContactRequest();
        $dtoContactRequestBefore->name = 'AndreaInsert';
        $dtoContactRequestBefore->surname = 'Prandini';
        $dtoContactRequestBefore->email = 'admin@tecnotrade.com';
        $dtoContactRequestBefore->phoneNumber = '051123456';
        $dtoContactRequestBefore->description = 'ContactRequestInsert';

        #endregion PARAMETER

        $response = $this->putJson('/api/v1/contactrequest', $dtoContactRequestBefore->toArray());
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        #region PARAMETER

        $contactAfter = Contact::where('name', 'AndreaInsert')->first();
        $contactRequestAfter = ContactRequest::where('contact_id', $contactAfter->id)->where('description', 'ContactRequestInsert')->first();

        $dtoContactRequestAfter = new DtoContactRequest();
        $dtoContactRequestAfter->name = $contactAfter->name;
        $dtoContactRequestAfter->surname = $contactAfter->surname;
        $dtoContactRequestAfter->email = $contactAfter->email;
        $dtoContactRequestAfter->phoneNumber = $contactAfter->phone_number;
        $dtoContactRequestAfter->description = $contactRequestAfter->description;

        #endregion PARAMETER

        $this->assertEquals($contactRequestAfter->id, $response->json()['data']);
        $this->assertEquals($dtoContactRequestBefore, $dtoContactRequestAfter);

        #region PARAMETER

        $contactRequestAfter->delete();
        $contactAfter->delete();

        #endregion PARAMETER
    }

    #endregion INSERT
}

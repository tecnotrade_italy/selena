<?php

namespace Tests\Feature;

use App\Contact;
use App\DtoModel\DtoContact;
use App\Helpers\LogHelper;
use App\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ContactTest extends TestCase
{
    #region GET

    /**
     * Get all
     *
     * @return void
     */
    public function testGetAll()
    {
        #region PARAMETER
        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $contactAndrea = new Contact();
        $contactAndrea->name = 'Andrea';
        $contactAndrea->surname = 'Prandini';
        $contactAndrea->business_name = 'Tecnotrade srl';
        $contactAndrea->email = 'admin@tecnotrade.com';
        $contactAndrea->phone_number = '051123456';
        $contactAndrea->send_newsletter = true;
        $contactAndrea->error_newsletter = false;
        $contactAndrea->created_id = $user->id;
        $contactAndrea->save();

        $contactFilippo = new Contact();
        $contactFilippo->name = 'Filippo';
        $contactFilippo->surname = 'Caiumi';
        $contactFilippo->business_name = 'Tecnotrade srl';
        $contactFilippo->email = 'filippo@tecnotrade.com';
        $contactFilippo->phone_number = '051654321';
        $contactFilippo->send_newsletter = false;
        $contactFilippo->error_newsletter = true;
        $contactFilippo->created_id = $user->id;
        $contactFilippo->save();

        #endregion PARAMETER

        $response = $this->get('/api/v1/contact/all');

        $response->assertStatus(200)
            ->assertSuccessful()
            ->assertSeeText('data');

        $this->assertEquals(Contact::all()->map(function ($model) {
            return [
                'id' => $model->id,
                'name' => $model->name,
                'surname' => $model->surname,
                'businessName' => $model->business_name,
                'email' => $model->email,
                'phoneNumber' => $model->phone_number,
                'sendNewsletter' => $model->send_newsletter,
                'errorNewsletter' => $model->error_newsletter,
            ];
        })->toArray(), $response->json()['data']);

        #region PARAMETER

        $contactAndrea->delete();
        $contactFilippo->delete();

        #endregion PARAMETER
    }

    /**
     * Get by id
     *
     * @return void
     */
    public function testGetById()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $contactAndrea = new Contact();
        $contactAndrea->name = 'Andrea';
        $contactAndrea->surname = 'Prandini';
        $contactAndrea->business_name = 'Tecnotrade srl';
        $contactAndrea->email = 'admin@tecnotrade.com';
        $contactAndrea->phone_number = '051123456';
        $contactAndrea->send_newsletter = true;
        $contactAndrea->error_newsletter = false;
        $contactAndrea->created_id = $user->id;
        $contactAndrea->save();

        $dtoContact = new DtoContact();
        $dtoContact->id = $contactAndrea->id;
        $dtoContact->name = $contactAndrea->name;
        $dtoContact->surname = $contactAndrea->surname;
        $dtoContact->businessName = $contactAndrea->business_name;
        $dtoContact->email = $contactAndrea->email;
        $dtoContact->phoneNumber = $contactAndrea->phone_number;
        $dtoContact->sendNewsletter = $contactAndrea->send_newsletter;
        $dtoContact->errorNewsletter = $contactAndrea->error_newsletter;

        #endregion PARAMETER

        $response = $this->get('/api/v1/contact/' . $contactAndrea->id);
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');
        $this->assertEquals($dtoContact->toArray(), $response->json()['data']);

        #region PARAMETER

        $contactAndrea->delete();

        #endregion PARAMETER
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @return void
     */
    public function testInsert()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $dtoContactBefore = new DtoContact();
        $dtoContactBefore->name = 'AndreaInsert';
        $dtoContactBefore->surname = 'Prandini';
        $dtoContactBefore->businessName = 'Tecnotrade srl';
        $dtoContactBefore->email = 'admin@tecnotrade.com';
        $dtoContactBefore->phoneNumber = '051123456';
        $dtoContactBefore->sendNewsletter = true;
        $dtoContactBefore->errorNewsletter = false;

        #endregion PARAMETER

        $response = $this->putJson('/api/v1/contact', $dtoContactBefore->toArray());
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        #region PARAMETER

        $contactAfter = Contact::where('name', 'AndreaInsert')->first();

        $dtoContactAfter = new DtoContact();
        $dtoContactAfter->name = $contactAfter->name;
        $dtoContactAfter->surname = $contactAfter->surname;
        $dtoContactAfter->businessName = $contactAfter->business_name;
        $dtoContactAfter->email = $contactAfter->email;
        $dtoContactAfter->phoneNumber = $contactAfter->phone_number;
        $dtoContactAfter->sendNewsletter = $contactAfter->send_newsletter;
        $dtoContactAfter->errorNewsletter = $contactAfter->error_newsletter;

        #endregion PARAMETER

        $this->assertEquals($contactAfter->id, $response->json()['data']);
        $this->assertEquals($dtoContactAfter, $dtoContactBefore);

        #region PARAMETER

        $contactAfter->delete();

        #endregion PARAMETER
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @return void
     */
    public function testUpdate()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $contactAndrea = new Contact();
        $contactAndrea->name = 'Andrea';
        $contactAndrea->surname = 'Prandini';
        $contactAndrea->business_name = 'Tecnotrade srl';
        $contactAndrea->email = 'admin@tecnotrade.com';
        $contactAndrea->phone_number = '051123456';
        $contactAndrea->send_newsletter = true;
        $contactAndrea->error_newsletter = false;
        $contactAndrea->created_id = $user->id;
        $contactAndrea->save();

        $dtoContactBefore = new DtoContact();
        $dtoContactBefore->id = $contactAndrea->id;
        $dtoContactBefore->name = 'Andreaa';
        $dtoContactBefore->surname = 'Prandinii';
        $dtoContactBefore->businessName = 'Tecnotrade srll';
        $dtoContactBefore->email = 'padmin@tecnotrade.com';
        $dtoContactBefore->phoneNumber = '0511234567';
        $dtoContactBefore->sendNewsletter = false;
        $dtoContactBefore->errorNewsletter = true;

        #endregion PARAMETER

        $response = $this->postJson('/api/v1/contact', $dtoContactBefore->toArray());

        #region PARAMETER

        $contactAfter = Contact::find($contactAndrea->id);

        $dtoContactAfter = new DtoContact();
        $dtoContactAfter->id = $contactAfter->id;
        $dtoContactAfter->name = $contactAfter->name;
        $dtoContactAfter->surname = $contactAfter->surname;
        $dtoContactAfter->businessName = $contactAfter->business_name;
        $dtoContactAfter->email = $contactAfter->email;
        $dtoContactAfter->phoneNumber = $contactAfter->phone_number;
        $dtoContactAfter->sendNewsletter = $contactAfter->send_newsletter;
        $dtoContactAfter->errorNewsletter = $contactAfter->error_newsletter;

        #endregion PARAMETER

        $response->assertStatus(200)
            ->assertSuccessful();

        $this->assertEquals($dtoContactAfter, $dtoContactBefore);

        #region PARAMETER

        $contactAndrea->delete();

        #endregion PARAMETER
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @return void
     */
    public function testDelete()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $contactAndrea = new Contact();
        $contactAndrea->name = 'Andrea';
        $contactAndrea->surname = 'Prandini';
        $contactAndrea->business_name = 'Tecnotrade srl';
        $contactAndrea->email = 'admin@tecnotrade.com';
        $contactAndrea->phone_number = '051123456';
        $contactAndrea->send_newsletter = true;
        $contactAndrea->error_newsletter = false;
        $contactAndrea->created_id = $user->id;
        $contactAndrea->save();

        #endregion PARAMETER

        $response = $this->delete('/api/v1/contact/' . $contactAndrea->id);

        $response->assertStatus(200)
            ->assertSuccessful();

        $this->assertNull(Contact::find($contactAndrea->id));
    }

    #endregion DELETE
}

<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\BusinessLogic\MenuAdminBL;
use App\Language;
use App\User;
use Laravel\Passport\Passport;
use App\Helpers\LogHelper;
use App\Helpers\ArrayHelper;
use function GuzzleHttp\json_encode;
use App\Functionality;
use App\BusinessLogic\FieldUserRoleBL;

// class AdminConfigurationTest extends TestCase
// {
//     #region V1

//     /*
//      * v1 / adminConfiguration / getMenu
//      */
//     public function testAdminConfigurationGetMenu()
//     {
//         $user = User::where('name', '=', 'admin')->first();
//         Passport::actingAs($user);

//         $response = $this->get('/api/v1/adminConfiguration/getMenu');

//         $response->assertStatus(200)
//             ->assertSuccessful()
//             ->assertSeeText('data')
//             ->assertSee(json_encode(MenuAdminBL::getDto($user->id, Language::find($user->language_id)->id)));
//     }

//     public function testAdminConfigurationGetFunctionality()
//     {
//         $user = User::where('name', '=', 'admin')->first();
//         $idFunctionality = Functionality::where('code', 'Pages')->first()->id;
//         Passport::actingAs($user);

//         $response = $this->get('/api/v1/adminConfiguration/getFunctionality/' . $idFunctionality);

//         $response->assertStatus(200)
//             ->assertSuccessful()
//             ->assertSeeText('data')
//             ->assertSee(json_encode(FieldUserRoleBL::getConfiguration($idFunctionality, $user->id, Language::find($user->language_id)->id)));
//     }

//     #endregion V1
// }

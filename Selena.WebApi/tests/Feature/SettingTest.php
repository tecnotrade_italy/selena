<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\BusinessLogic\SettingBL;
use App\User;
use Laravel\Passport\Passport;
use App\Enums\SettingEnum;
use App\Setting;
use App\Helpers\LogHelper;

// class SettingTest extends TestCase
// {
//     #region GET

//     // /*
//     //  * v1 / setting / getAll
//     //  */
//     public function testSettingGetAll()
//     {
//         $user = User::where('name', 'admin')->first();
//         Passport::actingAs($user);

//         $response = $this->get('/api/v1/setting/getAll');
//         $response->assertStatus(200)
//             ->assertSuccessful()
//             ->assertSeeText('data')
//             ->assertSee(json_encode(SettingBL::getAll()));
//     }

//     /*
//      * v1 / setting / getAll
//      */
//     public function testSettingGetCodes()
//     {
//         $user = User::where('name', 'admin')->first();
//         Passport::actingAs($user);

//         $response = $this->get('/api/v1/setting/getCodes');
//         $response->assertStatus(200)
//             ->assertSuccessful()
//             ->assertSeeText('data')
//             ->assertSee(json_encode(SettingBL::getCodes()));
//     }

//     #endregion GET

//     #region PUT

    /*
     * v1 / setting / insert
     */
    // public function testSettingInsert()
    // {
    //     $setting = new Setting();
    //     $setting->code = SettingEnum::CopySettingDefaultLanguage;
    //     $setting->value = true;

    //     $user = User::where('name', 'admin')->first();
    //     Passport::actingAs($user);

    //     $response = $this->putJson('/api/v1/setting/dto', $setting->toArray());
    //     $response->assertStatus(200)
    //         ->assertSuccessful()
    //         ->assertSeeText('data');

    //     $this->assertNotNull(SettingBL::getAll()->find($response->json()['data']));
    // }

    // #endregion PUT

    // #region POST

    /*
     * v1 / setting / update
     */
    // public function testSettingUpdate()
    // {
    //     $setting = Setting::where('code', SettingEnum::CopySettingDefaultLanguage)->first();
    //     $setting->value = false;

    //     $user = User::where('name', 'admin')->first();
    //     Passport::actingAs($user);

    //     $response = $this->postJson('/api/v1/setting/dto', $setting->toArray());
    //     $response->assertStatus(200)
    //         ->assertSuccessful()
    //         ->assertSeeText('data');
    // }

    // #endregion POST

    // #region DELETE

    /*
     * v1 / setting / delete
     */
    // public function testSettingDelete()
    // {
    //     $setting = Setting::where('code', SettingEnum::CopySettingDefaultLanguage)->first();

    //     $user = User::where('name', 'admin')->first();
    //     Passport::actingAs($user);

    //     $response = $this->delete('/api/v1/setting/' . $setting->id);
    //     $response->assertStatus(200)
    //         ->assertSuccessful()
    //         ->assertSeeText('data');

    //     $this->assertNull(Setting::find($setting->id));
    // }

    // #endregion DELETE
// }

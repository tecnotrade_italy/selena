<?php

namespace Tests\Feature;

use App\DtoModel\DtoGroupNewsletter;
use App\GroupNewsletter;
use App\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class GroupNewsletterTest extends TestCase
{
    #region GET

    /**
     * Get all
     *
     * @return void
     */
    public function testGetAll()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $groupNewsletterMailinglist = new GroupNewsletter();
        $groupNewsletterMailinglist->description = 'GetAll';
        $groupNewsletterMailinglist->created_id = $user->id;
        $groupNewsletterMailinglist->save();

        $groupNewsletterMailinglist_1 = new GroupNewsletter();
        $groupNewsletterMailinglist_1->description = 'GetAll_1';
        $groupNewsletterMailinglist_1->created_id = $user->id;
        $groupNewsletterMailinglist_1->save();

        #endregion PARAMETER

        $response = $this->get('/api/v1/groupnewsletter/all');

        $response->assertStatus(200)
            ->assertSuccessful()
            ->assertSeeText('data');

        $this->assertEquals(GroupNewsletter::all()->map(function ($model) {
            return [
                'id' => $model->id,
                'description' => $model->description
            ];
        })->toArray(), $response->json()['data']);

        #region PARAMETER

        $groupNewsletterMailinglist->delete();
        $groupNewsletterMailinglist_1->delete();

        #endregion PARAMETER
    }

    /**
     * Get by id
     *
     * @return void
     */
    public function testGetById()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $groupNewsletterMailinglist = new GroupNewsletter();
        $groupNewsletterMailinglist->description = 'GetById';
        $groupNewsletterMailinglist->created_id = $user->id;
        $groupNewsletterMailinglist->save();

        $dtoGroupNewsletter = new DtoGroupNewsletter();
        $dtoGroupNewsletter->id = $groupNewsletterMailinglist->id;
        $dtoGroupNewsletter->description = $groupNewsletterMailinglist->description;

        #endregion PARAMETER

        $response = $this->get('/api/v1/groupnewsletter/' . $groupNewsletterMailinglist->id);

        $response->assertStatus(200)
            ->assertSuccessful()
            ->assertSeeText('data');

        $this->assertEquals($dtoGroupNewsletter->toArray(), $response->json()['data']);

        #region PARAMETER

        $groupNewsletterMailinglist->delete();

        #endregion PARAMETER
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @return void
     */
    public function testInsert()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $dtoGroupNewsletterBefore = new DtoGroupNewsletter();
        $dtoGroupNewsletterBefore->description = 'Insert';

        #endregion PARAMETER

        $response = $this->putJson('/api/v1/groupnewsletter',  $dtoGroupNewsletterBefore->toArray());

        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        #region PARAMETER

        $groupNewsletterMailinglist = GroupNewsletter::where('description', 'Insert')->first();

        $dtoGroupNewsletterAfter = new DtoGroupNewsletter();
        $dtoGroupNewsletterAfter->description = $groupNewsletterMailinglist->description;

        #endregion PARAMETER

        $this->assertEquals($groupNewsletterMailinglist->id, $response->json()['data']);
        $this->assertEquals($dtoGroupNewsletterBefore, $dtoGroupNewsletterAfter);

        #region PARAMETER

        $groupNewsletterMailinglist->delete();

        #endregion PARAMETER
    }

    #endregion INSERT

    // #region UPDATE

    /**
     * Update
     *
     * @return void
     */
    public function testUpdate()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $groupNewsletterMailinglist = new GroupNewsletter();
        $groupNewsletterMailinglist->description = 'update';
        $groupNewsletterMailinglist->created_id = $user->id;
        $groupNewsletterMailinglist->save();

        $dtoGroupNewsletterBefore = new DtoGroupNewsletter();
        $dtoGroupNewsletterBefore->id = $groupNewsletterMailinglist->id;
        $dtoGroupNewsletterBefore->description = $groupNewsletterMailinglist->description;

        #endregion PARAMETER

        $response = $this->postJson('/api/v1/groupnewsletter',  $dtoGroupNewsletterBefore->toArray());

        $response->assertStatus(200)
            ->assertSuccessful()
            ->assertSeeText('data');

        #region PARAMETER

        $groupNewsletterMailinglistAfter = GroupNewsletter::where('description', 'update')->first();

        $dtoGroupNewsletterAfter = new DtoGroupNewsletter();
        $dtoGroupNewsletterAfter->id = $groupNewsletterMailinglistAfter->id;
        $dtoGroupNewsletterAfter->description = $groupNewsletterMailinglistAfter->description;

        #endregion PARAMETER

        $this->assertEquals($dtoGroupNewsletterBefore, $dtoGroupNewsletterAfter);

        #region PARAMETER

        $groupNewsletterMailinglist->delete();

        #endregion PARAMETER
    }

    // #endregion UPDATE

    // #region DELETE

    /**
     * Delete
     *
     * @return void
     */
    public function testDelete()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $groupNewsletterMailinglist = new GroupNewsletter();
        $groupNewsletterMailinglist->description = 'delete';
        $groupNewsletterMailinglist->created_id = $user->id;
        $groupNewsletterMailinglist->save();

        #endregion PARAMETER

        $response = $this->delete('/api/v1/groupnewsletter/' .  $groupNewsletterMailinglist->id);

        $response->assertStatus(200)->assertSuccessful();
        $this->assertNull(GroupNewsletter::find($groupNewsletterMailinglist->id));
    }

    // #endregion DELETE
}

<?php

namespace Tests\Feature;

use App\BusinessLogic\LanguagePageBL;
use App\BusinessLogic\PageBL;
use App\DtoModel\DtoLanguagePageSetting;
use App\DtoModel\DtoPageSetting;
use App\Functionality;
use App\Helpers\LogHelper;
use App\Icon;
use App\Language;
use App\LanguagePage;
use App\Page;
use App\PageCategory;
use App\PageShareType;
use App\User;
use Carbon\Carbon;
use Laravel\Passport\Passport;
use Tests\TestCase;
use App\DtoModel\DtoUrlListDetail;
use App\Helpers\HttpHelper;

// class PageTest extends TestCase
// {
#region PUT

// public function testPageInsert()
// {
//     $icon = Icon::first();
//     $idIcon = isset($icon) ? $icon->id : null;
//     $pageCategory = PageCategory::first();
//     $idPageCategory = isset($pageCategory) ? $pageCategory->id : null;
//     $pageShareType = PageShareType::first();
//     $idPageShareType = isset($pageShareType) ? $pageShareType->id : null;
//     $user = User::where('name', 'admin')->first();

//     $languagePageSetting = new DtoLanguagePageSetting();
//     $languagePageSetting->content = '<b>Pagina di test</b>';
//     $languagePageSetting->idLanguage = Language::first()->id;
//     $languagePageSetting->seoDescription = "Descrizione SEO";
//     $languagePageSetting->seoKeyword = "Key SEO";
//     $languagePageSetting->seoTitle = "Titolo SEO";
//     $languagePageSetting->shareDescription = "Descrizione condivisione";
//     $languagePageSetting->shareTitle = "Titolo condivisione";
//     $languagePageSetting->hideBreadcrumb = true;
//     $languagePageSetting->hideFooter = false;
//     $languagePageSetting->hideHeader = false;
//     $languagePageSetting->title = "Pagina di test";
//     $languagePageSetting->url = "/tmp";
//     $languagePageSetting->urlCoverImage = "/tmp.png";

//     $dtoPageSetting = new DtoPageSetting();
//     $dtoPageSetting->fixed = true;
//     // $dtoPageSetting->fixedEnd = Carbon::now()->addDay(1)->format('d/m/Y H:i');
//     $dtoPageSetting->hideMenu = false;
//     $dtoPageSetting->hideMenuMobile = false;
//     $dtoPageSetting->hideSearchEngine = false;
//     $dtoPageSetting->homePage = true;
//     $dtoPageSetting->idAuthor = $user->id;
//     $dtoPageSetting->idFunctionality = Functionality::where('code', 'Pages')->first()->id;
//     $dtoPageSetting->idIcon = $idIcon;
//     $dtoPageSetting->idPageCategory = $idPageCategory;
//     $dtoPageSetting->idPageShareType = $idPageShareType;
//     $dtoPageSetting->languagePageSetting->push($languagePageSetting);
//     $dtoPageSetting->online = true;
//     $dtoPageSetting->publish = Carbon::now()->format('d/m/Y H:i');
//     // $dtoPageSetting->visibleEnd = Carbon::now()->addDay(3)->format('d/m/Y H:i');
//     $dtoPageSetting->visibleFrom = Carbon::now()->addDay(2)->format('d/m/Y H:i');

//     Passport::actingAs($user);
//     $response = $this->putJson('/api/v1/page/dto', $dtoPageSetting->toArray());
//     $response->assertStatus(200)
//         ->assertSuccessful()
//         ->assertSeeText('data');

//     $this->assertNotNull(PageBL::getSetting($response->json()['data']));
// }

// #endregion PUT

// #region GET

/*
     * v1 / page / getMenuList
     */
// public function testPageGetMenuList()
// {
//     $user = User::where('name', 'admin')->first();
//     Passport::actingAs($user);

//     $response = $this->get('/api/v1/page/getMenuList');
//     $response->assertStatus(200)
//         ->assertSuccessful()
//         ->assertSeeText('data')
//         ->assertSee(json_encode(PageBL::getMenuList($user->language_id)));
// }

// /*
//  * v1 / page / getSetting
//  */
// public function testPageGetSetting()
// {
//     $page = Page::first();
//     if (!is_null($page)) {
//         $user = User::where('name', 'admin')->first();
//         Passport::actingAs($user);

//         $response = $this->get('/api/v1/page/getSetting/' . $page->id);
//         $response->assertStatus(200)
//             ->assertSuccessful()
//             ->assertSeeText('data')
//             ->assertSee(json_encode(PageBL::getSetting($page->id)));
//     }
// }

// /*
//  * v1 / page / getHome
//  */
// public function testPageGetHome()
// {
//     $idLanguage = Language::first()->id;
//     $response = $this->get('/api/v1/page/getHome/' . $idLanguage);
//     $response->assertStatus(200)
//         ->assertSuccessful()
//         ->assertSeeText('data')
//         ->assertSee(json_encode(PageBL::getHome($idLanguage)));
// }

/*
     * v1 / page / url
     */
// public function testPageGetUrl()
// {
//     $user = User::where('name', 'admin')->first();
//     Passport::actingAs($user);
//     $response = $this->get('/api/v1/page/url');
//     $response->assertStatus(200)
//         ->assertSuccessful()
//         ->assertSeeText('data')
//         ->assertSee(json_encode(PageBL::getUrl($user->language_id)));
// }

// #endregion GET

// #region POST

// /*
//  * v1 / page / getRender
//  */
// public function testPageGetRender()
// {
//     $languagePage = LanguagePage::whereNotNull('url')->first();

//     if (!is_null($languagePage)) {
//         $response = $this->postJson('/api/v1/page/getRender', ['url' => $languagePage->url]);
//         $response->assertStatus(200)
//             ->assertSuccessful()
//             ->assertSeeText('data')
//             ->assertSee(json_encode(PageBL::getRender($languagePage->url)));
//     }
// }

// /*
//  * v1 / page / checkExistUrl
//  */
// public function testPageCheckExistUrl()
// {
//     $languagePage = LanguagePage::whereNotNull('url')->first();

//     if (!is_null($languagePage)) {

//         $user = User::where('name', 'admin')->first();
//         Passport::actingAs($user);

//         $responseUrl = $this->postJson('/api/v1/page/checkExistUrl', ['url' => $languagePage->url]);
//         $responseUrl->assertStatus(200)
//             ->assertSuccessful()
//             ->assertSeeText('data')
//             ->assertSee(json_encode(LanguagePageBL::checkExistUrl($languagePage->url)));

//         $responseUrlId = $this->postJson('/api/v1/page/checkExistUrl/', ['url' => $languagePage->url, 'id' => $languagePage->page_id]);
//         $responseUrlId->assertStatus(200)
//             ->assertSuccessful()
//             ->assertSeeText('data')
//             ->assertSee(json_encode(LanguagePageBL::checkExistUrl($languagePage->url, $languagePage->page_id)));

//         $responseNotUrl = $this->postJson('/api/v1/page/checkExistUrl/', ['url' => $languagePage->url . 'a']);
//         $responseNotUrl->assertStatus(200)
//             ->assertSuccessful()
//             ->assertSeeText('data')
//             ->assertSee(json_encode(LanguagePageBL::checkExistUrl($languagePage->url . 'a')));
//     }
// }

// public function testPageUpdate()
// {
//     $languagePage = LanguagePage::where('url', "/tmp");

//     if ($languagePage->count()) {
//         $icon = Icon::first();
//         $idIcon = isset($icon) ? $icon->id : null;
//         $pageCategory = PageCategory::first();
//         $idPageCategory = isset($pageCategory) ? $pageCategory->id : null;
//         $pageShareType = PageShareType::first();
//         $idPageShareType = isset($pageShareType) ? $pageShareType->id : null;
//         $user = User::where('name', 'admin')->first();

//         $dtoPageSetting = PageBL::getSetting($languagePage->first()->page_id);
//         $dtoPageSetting->fixed = false;
//         // $dtoPageSetting->fixedEnd = Carbon::now()->addDay(2)->format('d/m/Y H:i');
//         $dtoPageSetting->hideMenu = true;
//         $dtoPageSetting->hideMenuMobile = true;
//         $dtoPageSetting->hideSearchEngine = true;
//         $dtoPageSetting->homePage = false;
//         $dtoPageSetting->id = $languagePage->first()->page_id;
//         $dtoPageSetting->idAuthor = $user->id;
//         $dtoPageSetting->idFunctionality = Functionality::where('code', 'Pages')->first()->id;
//         $dtoPageSetting->idIcon = $idIcon;
//         $dtoPageSetting->idPageCategory = $idPageCategory;
//         $dtoPageSetting->idPageShareType = $idPageShareType;

//         $dtoPageSetting->languagePageSetting->first()->content = '<b>Pagina di test aggiornata</b>';
//         $dtoPageSetting->languagePageSetting->first()->idLanguage = Language::first()->id;
//         $dtoPageSetting->languagePageSetting->first()->seoDescription = "Descrizione aggiornata SEO";
//         $dtoPageSetting->languagePageSetting->first()->seoKeyword = "Key aggiornata SEO";
//         $dtoPageSetting->languagePageSetting->first()->seoTitle = "Titolo aggiornato SEO";
//         $dtoPageSetting->languagePageSetting->first()->shareDescription = "Descrizione aggiornata condivisione";
//         $dtoPageSetting->languagePageSetting->first()->shareTitle = "Titolo aggiornato condivisione";
//         $dtoPageSetting->languagePageSetting->first()->hideBreadcrumb = false;
//         $dtoPageSetting->languagePageSetting->first()->hideFooter = true;
//         $dtoPageSetting->languagePageSetting->first()->hideHeader = true;
//         $dtoPageSetting->languagePageSetting->first()->title = "Pagina di test aggiornata";
//         $dtoPageSetting->languagePageSetting->first()->url = "/tmpAgg";
//         $dtoPageSetting->languagePageSetting->first()->urlCoverImage = "/tmpAgg.png";

//         $dtoPageSetting->online = false;
//         $dtoPageSetting->publish = Carbon::now()->addDay(1)->format('d/m/Y H:i');
//         $dtoPageSetting->visibleEnd = Carbon::now()->addDay(4)->format('d/m/Y H:i');
//         $dtoPageSetting->visibleFrom = Carbon::now()->addDay(3)->format('d/m/Y H:i');

//         Passport::actingAs($user);
//         $response = $this->postJson('/api/v1/page/dto', $dtoPageSetting->toArray());
//         $response->assertStatus(200)
//             ->assertSuccessful()
//             ->assertSeeText('data');
//     }
// }

/**
 * v1 / page / url
 */
    // public function testPagePostUrl()
    // {
    //     $idLanguage = Language::first()->id;
    //     $user = User::where('name', 'admin')->first();

    //     $dtoUrlLists = PageBL::getUrl($idLanguage);
    //     if ($dtoUrlLists->count() > 0) {
    //         if ($dtoUrlLists[0]->urlListDetail->count() > 0) {
    //             $dtoUrlLists[0]->urlListDetail[0]->url = '/tmpL';
    //         } else {
    //             $dtoUrlLists[0]->urlListDetail[0] = new DtoUrlListDetail();
    //             $dtoUrlLists[0]->urlListDetail[0]->idLanguage = Language::where('id', '<>', $idLanguage)->first()->id;
    //             $dtoUrlLists[0]->urlListDetail[0]->url = '/tmpL';
    //         }
    //     }

    //     Passport::actingAs($user);
    //     $response = $this->postJson('/api/v1/page/url', $dtoUrlLists->toArray());
    //     $response->assertStatus(200)
    //         ->assertSuccessful()
    //         ->assertSeeText('data');
    // }

    // #endregion POST

    // #region DELETE

    // public function testPageDelete()
    // {
    //     $languagePage = LanguagePage::where('url', "/tmpAgg");
    //     if ($languagePage->count()) {
    //         $idPage = $languagePage->first()->page_id;
    //         $user = User::where('name', 'admin')->first();
    //         Passport::actingAs($user);

    //         $response = $this->delete('/api/v1/page/' . $idPage);
    //         $response->assertStatus(200)
    //             ->assertSuccessful()
    //             ->assertSeeText('data');
    //         $this->assertNull(Page::find($idPage));
    //     }
    // }

//     #endregion DELETE
// }

<?php

namespace Tests\Feature;

use App\DtoModel\DtoFeedbackNewsletter;
use App\DtoModel\DtoNewsletter;
use App\FeedbackNewsletter;
use App\Helpers\HttpHelper;
use App\Newsletter;
use App\NewsletterRecipient;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Passport;
use Tests\TestCase;

class FeedbackNewsletterTest extends TestCase
{
    #region GET

    /**
     * Get all
     *
     * @return void
     */
    public function testGetAllByNewsletter()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $newsletter = new Newsletter();
        $newsletter->description = 'NewsletterGetAllByNewsletter';
        $newsletter->html = '<body>NewsletterGetAllByNewsletter</body>';
        $newsletter->draft = false;
        $newsletter->url = '/newsletter/allByNewsletter';
        $newsletter->schedule_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->start_send_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->end_send_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->created_id = $user->id;
        $newsletter->title = 'NewsletterGetAllByNewsletter';
        $newsletter->save();

        $newsletterRecipient = new NewsletterRecipient();
        $newsletterRecipient->newsletter_id = $newsletter->id;
        $newsletterRecipient->name = 'Simone Resca';
        $newsletterRecipient->email = 'simone@tecnotrade.com';
        $newsletterRecipient->created_id = $user->id;
        $newsletterRecipient->save();

        $feedbackNewsletter = new FeedbackNewsletter();
        $feedbackNewsletter->newsletter_recipient_id = $newsletterRecipient->id;
        $feedbackNewsletter->code = 'SENT';
        $feedbackNewsletter->created_id = $user->id;
        $feedbackNewsletter->save();

        #endregion PARAMETER

        $response = $this->get('/api/v1/feedbacknewsletter/allByNewsletter/' . $newsletter->id);
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        $result = collect();

        foreach (DB::select(
            '   SELECT feedback_newsletters.id, feedback_newsletters.code, newsletters_recipients.name, newsletters_recipients.email
                FROM newsletters_recipients
                INNER JOIN feedback_newsletters ON newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
                WHERE newsletters_recipients.newsletter_id = :idNewsletter',
            ['idNewsletter' => $newsletter->id]
        ) as $feedback) {
            $dtoFeedbackNewsletter = array();
            $dtoFeedbackNewsletter['id'] = $feedback->id;
            $dtoFeedbackNewsletter['code'] = $feedback->code;
            $dtoFeedbackNewsletter['name'] = $feedback->name;
            $dtoFeedbackNewsletter['email'] = $feedback->email;
            $result->push($dtoFeedbackNewsletter);
        }

        $this->assertEquals($result->toArray(), $response->json()['data']);

        #region PARAMETER

        $feedbackNewsletter->delete();
        $newsletterRecipient->delete();
        $newsletter->delete();

        #endregion PARAMETER
    }

    #endregion GET

    #region INSERT

    /**
     * Get all
     *
     * @return void
     */
    public function testInsert()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $newsletter = new Newsletter();
        $newsletter->description = 'NewsletterGetAllByNewsletter';
        $newsletter->html = '<body>NewsletterGetAllByNewsletter</body>';
        $newsletter->draft = false;
        $newsletter->url = '/newsletter/allByNewsletter';
        $newsletter->schedule_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->start_send_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->end_send_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
        $newsletter->created_id = $user->id;
        $newsletter->title = 'NewsletterGetAllByNewsletter';
        $newsletter->save();

        $newsletterRecipient = new NewsletterRecipient();
        $newsletterRecipient->newsletter_id = $newsletter->id;
        $newsletterRecipient->name = 'Simone Resca';
        $newsletterRecipient->email = 'simone@tecnotrade.com';
        $newsletterRecipient->created_id = $user->id;
        $newsletterRecipient->save();

        #endregion PARAMETER

        $response = $this->get('/api/v1/feedbacknewsletter/' . $newsletterRecipient->id . '/OPEN');
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        $feedbackNewsletter = FeedbackNewsletter::where('newsletter_recipient_id', $newsletterRecipient->id)->where('code', 'OPEN')->first();
        $this->assertNotNull($feedbackNewsletter);

        #region PARAMETER

        $feedbackNewsletter->delete();
        $newsletterRecipient->delete();
        $newsletter->delete();

        #endregion PARAMETER
    }

    #endregion INSERT
}

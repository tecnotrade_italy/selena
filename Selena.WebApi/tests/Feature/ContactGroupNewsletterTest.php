<?php

namespace Tests\Feature;

use App\Contact;
use App\ContactGroupNewsletter;
use App\DtoModel\DtoContactGroupNewsletter;
use App\GroupNewsletter;
use App\User;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ContactGroupNewsletterTest extends TestCase
{
    #region GET

    /**
     * Get included by contact
     *
     * @return void
     */
    public function testGetIncludedByContact()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $contactAndreaGetIncludedByContact = new Contact();
        $contactAndreaGetIncludedByContact->name = 'AndreaGetIncludedByContact';
        $contactAndreaGetIncludedByContact->surname = 'Prandini';
        $contactAndreaGetIncludedByContact->business_name = 'Tecnotrade srl';
        $contactAndreaGetIncludedByContact->email = 'admin@tecnotrade.com';
        $contactAndreaGetIncludedByContact->phone_number = '051123456';
        $contactAndreaGetIncludedByContact->send_newsletter = true;
        $contactAndreaGetIncludedByContact->error_newsletter = false;
        $contactAndreaGetIncludedByContact->created_id = $user->id;
        $contactAndreaGetIncludedByContact->save();

        $groupNewsletterMailinglistGetIncludedByContact = new GroupNewsletter();
        $groupNewsletterMailinglistGetIncludedByContact->description = 'MailinglistGetIncludedByContact';
        $groupNewsletterMailinglistGetIncludedByContact->created_id = $user->id;
        $groupNewsletterMailinglistGetIncludedByContact->save();

        $groupNewsletterMailinglistGetIncludedByContact_1 = new GroupNewsletter();
        $groupNewsletterMailinglistGetIncludedByContact_1->description = 'MailinglistGetIncludedByContact_1';
        $groupNewsletterMailinglistGetIncludedByContact_1->created_id = $user->id;
        $groupNewsletterMailinglistGetIncludedByContact_1->save();

        $contactGroupNewsletterAMGetIncludedByContact = new ContactGroupNewsletter();
        $contactGroupNewsletterAMGetIncludedByContact->contact_id = $contactAndreaGetIncludedByContact->id;
        $contactGroupNewsletterAMGetIncludedByContact->group_newsletter_id = $groupNewsletterMailinglistGetIncludedByContact->id;
        $contactGroupNewsletterAMGetIncludedByContact->created_id = $user->id;
        $contactGroupNewsletterAMGetIncludedByContact->save();

        $contactGroupNewsletterAM1GetIncludedByContact = new ContactGroupNewsletter();
        $contactGroupNewsletterAM1GetIncludedByContact->contact_id = $contactAndreaGetIncludedByContact->id;
        $contactGroupNewsletterAM1GetIncludedByContact->group_newsletter_id = $groupNewsletterMailinglistGetIncludedByContact_1->id;
        $contactGroupNewsletterAM1GetIncludedByContact->created_id = $user->id;
        $contactGroupNewsletterAM1GetIncludedByContact->save();

        #endregion PARAMETER

        $response = $this->get('/api/v1/contactgroupnewsletter/included/' . $contactAndreaGetIncludedByContact->id);

        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        $result = collect();

        foreach (DB::select(
            '   SELECT cgn.id, gn.description
                FROM contacts_groups_newsletters cgn
                INNER JOIN groups_newsletters gn ON cgn.group_newsletter_id = gn.id
                WHERE cgn.contact_id = :idContact',
            ['idContact' => $contactAndreaGetIncludedByContact->id]
        ) as $groupNewsletter) {
            $dtoGroupNewsletter = array();
            $dtoGroupNewsletter['id'] = $groupNewsletter->id;
            $dtoGroupNewsletter['description'] = $groupNewsletter->description;
            $dtoGroupNewsletter['name'] = null;
            $dtoGroupNewsletter['surname'] = null;
            $dtoGroupNewsletter['businessName'] = null;
            $dtoGroupNewsletter['email'] = null;
            $result->push($dtoGroupNewsletter);
        }

        $this->assertEquals($result->toArray(), $response->json()['data']);

        #region PARAMETER

        $contactGroupNewsletterAMGetIncludedByContact->delete();
        $contactGroupNewsletterAM1GetIncludedByContact->delete();
        $groupNewsletterMailinglistGetIncludedByContact->delete();
        $groupNewsletterMailinglistGetIncludedByContact_1->delete();
        $contactAndreaGetIncludedByContact->delete();

        #endregion PARAMETER
    }

    /**
     * Get excluded by contact
     *
     * @return void
     */
    public function testGetExcludedByContact()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $contactAndreaGetExcludedByContact = new Contact();
        $contactAndreaGetExcludedByContact->name = 'AndreaGetExcludedByContact';
        $contactAndreaGetExcludedByContact->surname = 'Prandini';
        $contactAndreaGetExcludedByContact->business_name = 'Tecnotrade srl';
        $contactAndreaGetExcludedByContact->email = 'admin@tecnotrade.com';
        $contactAndreaGetExcludedByContact->phone_number = '051123456';
        $contactAndreaGetExcludedByContact->send_newsletter = true;
        $contactAndreaGetExcludedByContact->error_newsletter = false;
        $contactAndreaGetExcludedByContact->created_id = $user->id;
        $contactAndreaGetExcludedByContact->save();

        $groupNewsletterMailinglistGetExcludedByContact = new GroupNewsletter();
        $groupNewsletterMailinglistGetExcludedByContact->description = 'MailinglistGetExcludedByContact';
        $groupNewsletterMailinglistGetExcludedByContact->created_id = $user->id;
        $groupNewsletterMailinglistGetExcludedByContact->save();

        $groupNewsletterMailinglistGetExcludedByContact_1 = new GroupNewsletter();
        $groupNewsletterMailinglistGetExcludedByContact_1->description = 'MailinglistGetExcludedByContact_1';
        $groupNewsletterMailinglistGetExcludedByContact_1->created_id = $user->id;
        $groupNewsletterMailinglistGetExcludedByContact_1->save();

        $contactGroupNewsletterAMGetExcludedByContact = new ContactGroupNewsletter();
        $contactGroupNewsletterAMGetExcludedByContact->contact_id = $contactAndreaGetExcludedByContact->id;
        $contactGroupNewsletterAMGetExcludedByContact->group_newsletter_id = $groupNewsletterMailinglistGetExcludedByContact->id;
        $contactGroupNewsletterAMGetExcludedByContact->created_id = $user->id;
        $contactGroupNewsletterAMGetExcludedByContact->save();

        #endregion PARAMETER

        $response = $this->get('/api/v1/contactgroupnewsletter/excluded/' . $contactAndreaGetExcludedByContact->id);

        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        $result = collect();

        foreach (DB::select(
            '   SELECT id, description
                FROM groups_newsletters
                WHERE id NOT IN (SELECT group_newsletter_id FROM contacts_groups_newsletters WHERE contact_id = :idContact)',
            ['idContact' => $contactAndreaGetExcludedByContact->id]
        ) as $groupNewsletter) {
            $dtoGroupNewsletter = array();
            $dtoGroupNewsletter['id'] = $groupNewsletter->id;
            $dtoGroupNewsletter['description'] = $groupNewsletter->description;
            $dtoGroupNewsletter['name'] = null;
            $dtoGroupNewsletter['surname'] = null;
            $dtoGroupNewsletter['businessName'] = null;
            $dtoGroupNewsletter['email'] = null;
            $result->push($dtoGroupNewsletter);
        }

        $this->assertEquals($result->toArray(), $response->json()['data']);

        #region PARAMETER

        $contactGroupNewsletterAMGetExcludedByContact->delete();
        $groupNewsletterMailinglistGetExcludedByContact->delete();
        $groupNewsletterMailinglistGetExcludedByContact_1->delete();
        $contactAndreaGetExcludedByContact->delete();

        #endregion PARAMETER
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @return void
     */
    public function testInsert()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $contactAndreaInsert = new Contact();
        $contactAndreaInsert->name = 'AndreaInsert';
        $contactAndreaInsert->surname = 'Prandini';
        $contactAndreaInsert->business_name = 'Tecnotrade srl';
        $contactAndreaInsert->email = 'admin@tecnotrade.com';
        $contactAndreaInsert->phone_number = '051123456';
        $contactAndreaInsert->send_newsletter = true;
        $contactAndreaInsert->error_newsletter = false;
        $contactAndreaInsert->created_id = $user->id;
        $contactAndreaInsert->save();

        $groupNewsletterMailinglistInsert = new GroupNewsletter();
        $groupNewsletterMailinglistInsert->description = 'MailinglistInsert';
        $groupNewsletterMailinglistInsert->created_id = $user->id;
        $groupNewsletterMailinglistInsert->save();

        $dtoContactGroupNewsletterInsert = new DtoContactGroupNewsletter();
        $dtoContactGroupNewsletterInsert->idContact = $contactAndreaInsert->id;
        $dtoContactGroupNewsletterInsert->idGroupNewsletter = $groupNewsletterMailinglistInsert->id;

        #endregion PARAMETER

        $response = $this->putJson('/api/v1/contactgroupnewsletter',  $dtoContactGroupNewsletterInsert->toArray());

        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        #region PARAMETER

        $contactGroupNewsletterInsert = ContactGroupNewsletter::where('contact_id', $contactAndreaInsert->id)->where('group_newsletter_id', $groupNewsletterMailinglistInsert->id)->first();

        #endregion PARAMETER

        $this->assertEquals($contactGroupNewsletterInsert->id, $response->json()['data']);

        #region PARAMETER

        $contactGroupNewsletterInsert->delete();
        $groupNewsletterMailinglistInsert->delete();
        $contactAndreaInsert->delete();

        #endregion PARAMETER
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @return void
     */
    public function testUpdate()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $contactAndreaUpdate = new Contact();
        $contactAndreaUpdate->name = 'AndreaUpdate';
        $contactAndreaUpdate->surname = 'Prandini';
        $contactAndreaUpdate->business_name = 'Tecnotrade srl';
        $contactAndreaUpdate->email = 'admin@tecnotrade.com';
        $contactAndreaUpdate->phone_number = '051123456';
        $contactAndreaUpdate->send_newsletter = true;
        $contactAndreaUpdate->error_newsletter = false;
        $contactAndreaUpdate->created_id = $user->id;
        $contactAndreaUpdate->save();

        $groupNewsletterMailinglistUpdate = new GroupNewsletter();
        $groupNewsletterMailinglistUpdate->description = 'MailinglistUpdate';
        $groupNewsletterMailinglistUpdate->created_id = $user->id;
        $groupNewsletterMailinglistUpdate->save();

        $groupNewsletterMailinglistUpdate_1 = new GroupNewsletter();
        $groupNewsletterMailinglistUpdate_1->description = 'MailinglistUpdate_1';
        $groupNewsletterMailinglistUpdate_1->created_id = $user->id;
        $groupNewsletterMailinglistUpdate_1->save();

        $contactGroupNewsletterBefore = new ContactGroupNewsletter();
        $contactGroupNewsletterBefore->contact_id = $contactAndreaUpdate->id;
        $contactGroupNewsletterBefore->group_newsletter_id = $groupNewsletterMailinglistUpdate->id;
        $contactGroupNewsletterBefore->created_id = $user->id;
        $contactGroupNewsletterBefore->save();

        $dtoContactGroupNewsletterBefore = new DtoContactGroupNewsletter();
        $dtoContactGroupNewsletterBefore->id = $contactGroupNewsletterBefore->id;
        $dtoContactGroupNewsletterBefore->idContact = $contactAndreaUpdate->id;
        $dtoContactGroupNewsletterBefore->idGroupNewsletter = $groupNewsletterMailinglistUpdate_1->id;

        #endregion PARAMETER

        $response = $this->postJson('/api/v1/contactgroupnewsletter',  $dtoContactGroupNewsletterBefore->toArray());
        $response->assertStatus(200)->assertSuccessful()->assertSeeText('data');

        #region PARAMETER

        $groupNewsletterMailinglistAfter = ContactGroupNewsletter::find($contactGroupNewsletterBefore->id);

        $dtoContactGroupNewsletterAfter = new DtoContactGroupNewsletter();
        $dtoContactGroupNewsletterAfter->id = $groupNewsletterMailinglistAfter->id;
        $dtoContactGroupNewsletterAfter->idContact = $groupNewsletterMailinglistAfter->contact_id;
        $dtoContactGroupNewsletterAfter->idGroupNewsletter = $groupNewsletterMailinglistAfter->group_newsletter_id;

        #endregion PARAMETER

        $this->assertEquals($dtoContactGroupNewsletterBefore, $dtoContactGroupNewsletterAfter);

        #region PARAMETER

        $groupNewsletterMailinglistAfter->delete();
        $groupNewsletterMailinglistUpdate->delete();
        $groupNewsletterMailinglistUpdate_1->delete();
        $contactAndreaUpdate->delete();

        #endregion PARAMETER
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @return void
     */
    public function testDelete()
    {
        #region PARAMETER

        $user = User::where('name', 'admin')->first();
        Passport::actingAs($user);

        $contactAndreaDelete = new Contact();
        $contactAndreaDelete->name = 'AndreaDelete';
        $contactAndreaDelete->surname = 'Prandini';
        $contactAndreaDelete->business_name = 'Tecnotrade srl';
        $contactAndreaDelete->email = 'admin@tecnotrade.com';
        $contactAndreaDelete->phone_number = '051123456';
        $contactAndreaDelete->send_newsletter = true;
        $contactAndreaDelete->error_newsletter = false;
        $contactAndreaDelete->created_id = $user->id;
        $contactAndreaDelete->save();

        $groupNewsletterMailinglistDelete = new GroupNewsletter();
        $groupNewsletterMailinglistDelete->description = 'MailinglistDelete';
        $groupNewsletterMailinglistDelete->created_id = $user->id;
        $groupNewsletterMailinglistDelete->save();

        $contactGroupNewsletterDelete = new ContactGroupNewsletter();
        $contactGroupNewsletterDelete->contact_id = $contactAndreaDelete->id;
        $contactGroupNewsletterDelete->group_newsletter_id = $groupNewsletterMailinglistDelete->id;
        $contactGroupNewsletterDelete->created_id = $user->id;
        $contactGroupNewsletterDelete->save();

        #endregion PARAMETER

        $response = $this->delete('/api/v1/contactgroupnewsletter/' .  $contactGroupNewsletterDelete->id);
        $response->assertStatus(200)->assertSuccessful();
        $this->assertNull(ContactGroupNewsletter::find($contactGroupNewsletterDelete->id));

        #region PARAMETER

        $groupNewsletterMailinglistDelete->delete();
        $contactAndreaDelete->delete();

        #endregion PARAMETER
    }

    #endregion DELETE
}

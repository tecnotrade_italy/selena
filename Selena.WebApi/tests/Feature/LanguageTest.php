<?php

namespace Tests\Feature;

use App\BusinessLogic\LanguageBL;
use App\Language;
use App\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

// class LanguageTest extends TestCase
// {
//     #region V1

//     /*
//      * v1 / language / getDefault
//      */
//     public function testLanguageGetDefault()
//     {
//         Passport::actingAs(User::where('name', '=', 'admin')->first());

//         $response = $this->get('/api/v1/language/getDefault');
//         $response->assertStatus(200)
//             ->assertSuccessful()
//             ->assertJson(['data' => LanguageBL::getDefault()->toArray()]);
//     }

//     /*
//      * v1 / language / get / {id}
//      */
//     public function testLanguageGetById()
//     {
//         $language = Language::first();

//         $response = $this->get('/api/v1/language/get/' . $language->id);
//         $response->assertStatus(200)
//             ->assertSuccessful()
//             ->assertJson(['data' => LanguageBL::get($language->id)->toArray()]);
//     }

//     /*
//      * v1 / language / getSelect ?search= {description}
//      */
//     public function testLanguageGetSelect()
//     {
//         $description = substr(Language::first()->description, 0, 2);
//         $data = response()->getSelect(LanguageBL::getSelect($description));

//         $response = $this->get('/api/v1/language/select?search=' . $description);
//         $response->assertStatus(200)
//             ->assertSuccessful()
//             ->assertSee(json_encode($data->original));
//     }

//     /*
//      * v1 / language / all
//      */
//     public function testLanguageAll()
//     {
//         $response = $this->get('/api/v1/language/all');
//         $response->assertStatus(200)
//             ->assertSuccessful()
//             ->assertSee(json_encode(LanguageBL::getAll()));
//     }

//     #endregion V1
// }

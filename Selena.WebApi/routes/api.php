<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Enable CORS
Route::group(['middleware' => 'cors'], function () {

    #region LOGIN
    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'AuthController@login');
        Route::post('loginSalesman', 'AuthController@loginSalesman');
        Route::post('signup', 'AuthController@signup');
        Route::put('insertLoginForFacebook', 'AuthController@insertLoginForFacebook');
        Route::put('insertLoginForGoogle', 'AuthController@insertLoginForGoogle');

        
        Route::group(['middleware' => 'auth:api'], function () {
            Route::get('logout', 'AuthController@logout');
            Route::get('user', 'AuthController@user');
            

        });
    });
    #endregion LOGIN

    //Route::group(['prefix' => 'paypal'], function () {
    Route::post('payment/{cart_id}', 'PaymentController@charge')->where('cart_id', '[0-9]+');
    Route::get('paymentsuccess', 'PaymentController@payment_success');
    Route::get('paymentbookingoffline/{cart_id}/{language_id}', 'PaymentController@paymentbookingoffline')->where(['cart_id' => '[0-9]+', 'language_id' => '[0-9]+']);
    Route::get('paymenterror', 'PaymentController@payment_error');
    //});

    #region V1
    Route::group(['prefix' => 'v1'], function () {

        #region ADMIN CONFIGURATION

        Route::group(['prefix' => 'adminConfiguration', 'middleware' => 'auth:api'], function () {
            Route::get('getMenu', 'Api\v1\AdminConfigurationController@getMenu');
            Route::get('getFunctionality/{id}', 'Api\v1\AdminConfigurationController@getFunctionality')->where('id', '[0-9]+');
        });

        #endregion ADMIN CONFIGURATION

        Route::group(['prefix' => 'menuAdmin', 'middleware' => 'auth:api'], function () {
            Route::get('allCustom/{idLanguage}', 'Api\v1\MenuAdminController@getAllCustom')->where('idLanguage', '[0-9]+');
            Route::get('{id}', 'Api\v1\MenuAdminController@getById')->where('id', '[0-9]+');
            Route::get('getByCode/{code}', 'Api\v1\MenuAdminController@getByCode');
            Route::put('', 'Api\v1\MenuAdminController@insert');
            Route::post('', 'Api\v1\MenuAdminController@update');
            Route::delete('{id}', 'Api\v1\MenuAdminController@delete')->where('id', '[0-9]+');
            Route::get('getByRole/{idRole}/', 'Api\v1\MenuAdminController@getByRole')->where('role_id', '[0-9]+');

            Route::post('disable/{menu_admin_id}/{role_id}', 'Api\v1\MenuAdminController@disable')->where(['menu_admin_id' => '[0-9]+', 'role_id' => '[0-9]+']);
            Route::post('enable/{menu_admin_id}/{role_id}', 'Api\v1\MenuAdminController@enable')->where(['menu_admin_id' => '[0-9]+', 'role_id' => '[0-9]+']);
        });

        #region AUTOMATION

        Route::group(['prefix' => 'automation', 'middleware' => 'auth:api'], function () {
            Route::get('getAll', 'Api\v1\AutomationController@getAll');
            Route::put('', 'Api\v1\AutomationController@insert');
            Route::post('', 'Api\v1\AutomationController@update');
        });

        #endregion AUTOMATION

        #region CARRIAGE
        Route::group(['prefix' => 'carriage'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('getByCustomer/{idCustomer}/{idFunctionality}', 'Api\v1\CarriageController@getByCustomer')->where(['idCustomer' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
                Route::get('select', 'Api\v1\CarriageController@getSelect');
                Route::get('all/{idLanguage}', 'Api\v1\CarriageController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\CarriageController@getById')->where('id', '[0-9]+');
                Route::put('', 'Api\v1\CarriageController@insert');
                Route::post('', 'Api\v1\CarriageController@update');
                Route::post('updateCheck', 'Api\v1\CarriageController@updateCheck');
                Route::delete('{id}', 'Api\v1\CarriageController@delete')->where('id', '[0-9]+');
            });
            Route::get('select', 'Api\v1\CarriageController@getSelect');
            Route::get('getAllCarriageByCartId/{idLanguage}/{cartId}', 'Api\v1\CarriageController@getAllCarriageByCartId')->where(['idLanguage' => '[0-9]+', 'cartId' => '[0-9]+']);;
        });
        #endregion CARRIAGE

        #endregion CARRIER
         Route::group(['prefix' => 'carrier'], function () {
                Route::group(['middleware' => 'auth:api'], function () {
                    Route::get('getAll', 'Api\v1\CarrierController@getAll');
                    Route::get('getCarrierArea/{id}/{idItem}', 'Api\v1\CarrierController@getCarrierArea')->where(['id' => '[0-9]+', 'idItem' => '[0-9]+']);
                    Route::get('getByCustomer/{idCustomer}/{idFunctionality}', 'Api\v1\CarrierController@getByCustomer')->where(['idCustomer' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
                    Route::put('', 'Api\v1\CarrierController@insert');
                    Route::put('insertCarrierArea', 'Api\v1\CarrierController@insertCarrierArea');
                    Route::post('', 'Api\v1\CarrierController@update');
                    Route::post('updateCarrierRangeArea', 'Api\v1\CarrierController@updateCarrierRangeArea');
                    Route::delete('{id}', 'Api\v1\CarrierController@delete')->where('id', '[0-9]+');
                    Route::delete('deleteRangeDetailArea/{id}', 'Api\v1\CarrierController@deleteRangeDetailArea')->where('id', '[0-9]+');
                    Route::post('deleteZoneRangeDetailArea', 'Api\v1\CarrierController@deleteZoneRangeDetailArea');
                });
                Route::get('select', 'Api\v1\CarrierController@getSelect');
                Route::get('selectCarrier', 'Api\v1\CarrierController@selectCarrier');
                Route::get('getAllCarrierByCartId/{idLanguage}/{cartId}', 'Api\v1\CarrierController@getAllCarrierByCartId')->where(['idLanguage' => '[0-9]+', 'cartId' => '[0-9]+']);;
            });

            Route::group(['prefix' => 'carrierstype'], function () {
                Route::group(['middleware' => 'auth:api'], function () {
                Route::get('selectCarrierType', 'Api\v1\CarrierTypeController@selectCarrierType'); 
            });
        });


        #region CATEGORIES

        //Route::group(['prefix' => 'Categories', 'middleware' => 'auth:api'], function () {
        Route::group(['prefix' => 'Categories'], function () {
            Route::group(['middleware' => ['auth:api']], function () {
                Route::get('all/{idLanguage}', 'Api\v1\CategoriesController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('getAllListNavigation/{idLanguage}/{idItem}', 'Api\v1\CategoriesController@getAllListNavigation')->where(['idLanguage' => '[0-9]+', 'idItem' => '[0-9]+']);
                Route::get('getAllListNavigationFather/{idLanguage}/{idItem}', 'Api\v1\CategoriesController@getAllListNavigationFather')->where(['idLanguage' => '[0-9]+', 'idItem' => '[0-9]+']);
                Route::get('getAllDetailListCategories/{idLanguage}/{idItem}', 'Api\v1\CategoriesController@getAllDetailListCategories')->where(['idLanguage' => '[0-9]+', 'idItem' => '[0-9]+']);
                Route::get('getAllDetailListCategoriesFather/{idLanguage}/{idCategories}', 'Api\v1\CategoriesController@getAllDetailListCategoriesFather')->where(['idLanguage' => '[0-9]+', 'idCategories' => '[0-9]+']);
                Route::get('{id}', 'Api\v1\CategoriesController@getById')->where('id', '[0-9]+');

                Route::get('getImages/{idCategories}', 'Api\v1\CategoriesController@getImages')->where('idCategories', '[0-9]+');
                Route::get('getAllImagesInFolder/{id}', 'Api\v1\CategoriesController@getAllImagesInFolder')->where('id', '[0-9]+');

                Route::get('getCategoriesLanguages/{idCategories}', 'Api\v1\CategoriesController@getCategoriesLanguages')->where('idCategories', '[0-9]+');
                Route::get('getByTranslateDefault/{id}/{idLanguage}/{idItem}', 'Api\v1\CategoriesController@getByTranslateDefault')->where(['id' => '[0-9]+', 'idItem' => '[0-9]+', 'idLanguage' => '[0-9]+']);
                Route::get('getByTranslateDefaultNew/{idLanguage}/{idItem}', 'Api\v1\CategoriesController@getByTranslateDefaultNew')->where(['idItem' => '[0-9]+', 'idLanguage' => '[0-9]+']);
                Route::get('getByTranslate/{id}', 'Api\v1\CategoriesController@getByTranslate')->where('id', '[0-9]+');

                Route::put('', 'Api\v1\CategoriesController@insert');
                Route::post('', 'Api\v1\CategoriesController@update');
                Route::post('updateOrderCategoryItem', 'Api\v1\CategoriesController@updateOrderCategoryItem');
                Route::post('updateOrderCategoryFather', 'Api\v1\CategoriesController@updateOrderCategoryFather');
                Route::post('updateOrderCategoriesTable', 'Api\v1\CategoriesController@updateOrderCategoriesTable');
                Route::post('getList', 'Api\v1\CategoriesController@getList');
                Route::put('insertCategoriesFather', 'Api\v1\CategoriesController@insertCategoriesFather');

                Route::post('updateOnlineCategories', 'Api\v1\CategoriesController@updateOnlineCategories');
                Route::post('updateImageName', 'Api\v1\CategoriesController@updateImageName');
                Route::post('updateCategoriesImages', 'Api\v1\CategoriesController@updateCategoriesImages');
                Route::put('insertTranslate', 'Api\v1\CategoriesController@insertTranslate');

                Route::post('updateTranslate', 'Api\v1\CategoriesController@updateTranslate');
                Route::delete('{id}', 'Api\v1\CategoriesController@delete')->where('id', '[0-9]+');
                Route::delete('getCancelTranslate/{id}', 'Api\v1\CategoriesController@getCancelTranslate')->where('id', '[0-9]+');
                Route::delete('deleteCategoriesFather/{idCategories}/{idCategoriesFather}', 'Api\v1\CategoriesController@deleteCategoriesFather')->where(['idCategories' => '[0-9]+', 'idCategoriesFather' => '[0-9]+']);
                Route::put('clone/{id}/{idFunctionality}', 'Api\v1\CategoriesController@clone')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            });
            Route::get('select', 'Api\v1\CategoriesController@getSelect');
            Route::get('selectSubCategories', 'Api\v1\CategoriesController@getSelectSubCategories');
            Route::get('selectPrincipalCategories', 'Api\v1\CategoriesController@getSelectPrincipalCategories');
        });
      
        #endregion CATEGORIES

        #region CATEGORY TYPE

        Route::group(['prefix' => 'CategoryType', 'middleware' => 'auth:api'], function () {
            Route::get('select', 'Api\v1\CategoryTypeController@getSelect');
            Route::get('all/{idLanguage}', 'Api\v1\CategoryTypeController@getAll')->where('idLanguage', '[0-9]+');
            Route::get('{id}', 'Api\v1\UserController@getById')->where('id', '[0-9]+');
            Route::put('', 'Api\v1\CategoryTypeController@insert');
            Route::post('', 'Api\v1\CategoryTypeController@update');
            Route::delete('{id}', 'Api\v1\CategoryTypeController@delete')->where('id', '[0-9]+');
        });

        #endregion CATEGORY TYPE


        #region CONTACT

        Route::group(['prefix' => 'contact'], function () {
            Route::group(['middleware' => ['auth:api']], function () {
                Route::get('allRequest/{idFunctionality}', 'Api\v1\ContactController@getAllRequest')->where('idFunctionality', '[0-9]+');
                Route::get('{id}', 'Api\v1\ContactController@getById')->where('id', '[0-9]+');
                Route::get('all', 'Api\v1\ContactController@getAll');
                Route::put('', 'Api\v1\ContactController@insert');
                Route::post('importData', 'Api\v1\ContactController@importData');
                Route::post('', 'Api\v1\ContactController@update');
                Route::delete('{id}', 'Api\v1\ContactController@delete')->where('id', '[0-9]+');
            });

            Route::get('select', 'Api\v1\ContactController@getSelect');
            Route::post('unsubscribe', 'Api\v1\ContactController@unsubscribe');
            Route::post('unsubscribeNewsletter', 'Api\v1\ContactController@unsubscribeNewsletter');
            Route::put('subscribe', 'Api\v1\ContactController@subscribe');
            Route::put('insertRequest', 'Api\v1\ContactController@insertRequest');
        });

        #endregion CONTACT

        #region CONTACT GROUP NEWSLETTER

        Route::group(['prefix' => 'contactgroupnewsletter', 'middleware' => 'auth:api'], function () {
            Route::get('included/{id}', 'Api\v1\ContactGroupNewsletterController@getIncludedByContact')->where('id', '[0-9]+');
            Route::get('excluded/{id}', 'Api\v1\ContactGroupNewsletterController@getExcludedByContact')->where('id', '[0-9]+');
            Route::get('includedgroup/{id}', 'Api\v1\ContactGroupNewsletterController@getIncludedByContactGroup')->where('id', '[0-9]+');
            Route::get('excludedgroup/{id}', 'Api\v1\ContactGroupNewsletterController@getExcludedByContactGroup')->where('id', '[0-9]+');
            Route::put('', 'Api\v1\ContactGroupNewsletterController@insert');
            Route::post('', 'Api\v1\ContactGroupNewsletterController@update');
            Route::delete('{id}', 'Api\v1\ContactGroupNewsletterController@delete')->where('id', '[0-9]+');
        });

        #endregion CONTACT  GROUP NEWSLETTER

        #region CONTACT REQUEST

        Route::group(['prefix' => 'contactrequest'], function () {
            Route::group(['middleware' => ['auth:api']], function () {
                Route::get('{id}', 'Api\v1\ContactRequestController@getById')->where('id', '[0-9]+');
                Route::get('all', 'Api\v1\ContactRequestController@getAll');
            });
            Route::post('insertworkforus', 'Api\v1\ContactRequestController@insertWorkForUs');
            Route::put('', 'Api\v1\ContactRequestController@insert');
            Route::post('insertTourAvailable', 'Api\v1\ContactRequestController@insertTourAvailable');
            Route::post('insertRequestLanguage', 'Api\v1\ContactRequestController@insertRequestLanguage');
            Route::put('insertLandingeBookPcb', 'Api\v1\ContactRequestController@insertLandingeBookPcb');
            Route::put('insertLogsContacts', 'Api\v1\ContactRequestController@insertLogsContacts');
        });

        #endregion CONTACT REQUEST

        #region CONTENT

        Route::group(['prefix' => 'content'], function () {
            Route::group(['middleware' => ['auth:api']], function () {
                Route::get('getAll/{idFunctionality}', 'Api\v1\ContentController@getAll')->where('idFunctionality', '[0-9]+');
                Route::get('getByLanguage/{idContent}/{idLanguage}/{idFunctionality}', 'Api\v1\ContentController@getByLanguage')->where(['idContent' => '[0-9]+', 'idLanguage' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
                Route::get('getTagByType/{typeContent}', 'Api\v1\ContentController@getTagByType');
                Route::put('', 'Api\v1\ContentController@insert');
                Route::post('', 'Api\v1\ContentController@update');
                Route::delete('{id}/{idFunctionality}', 'Api\v1\ContentController@delete')->where(['id' => '[0-9]+', 'idLanguage' => '[0-9]+']);
                Route::delete('deleteLanguage/{idContent}/{idLanguage}/{idFunctionality}', 'Api\v1\ContentController@deleteLanguage')->where(['idContent' => '[0-9]+', 'idLanguage' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            });
            Route::get('getByCode/{code}/{idLanguage?}', 'Api\v1\ContentController@getByCode')->where('idLanguage', '[0-9]+');
            Route::post('getByCodeUser', 'Api\v1\ContentController@getByCodeUser');
            Route::post('getByCodeRender', 'Api\v1\ContentController@getByCodeRender');
            Route::post('getBreadcrumbs', 'Api\v1\ContentController@getBreadcrumbs');
        });

        #endregion CONTENT

        #region CUSTOMER

        Route::group(['prefix' => 'customer', 'middleware' => 'auth:api'], function () {
            Route::get('selectBeforeDDTSixten', 'Api\v1\CustomerController@getSelectBeforeDDTSixten');

            Route::get('{id}', 'Api\v1\CustomerController@getById')->where('id', '[0-9]+');
            Route::get('all', 'Api\v1\CustomerController@getAll');
            Route::get('select', 'Api\v1\CustomerController@getSelect');
            Route::put('', 'Api\v1\CustomerController@insert');
            Route::post('', 'Api\v1\CustomerController@update');
            Route::delete('{id}', 'Api\v1\CustomerController@delete')->where('id', '[0-9]+');
        });

        #endregion CUSTOMER    

        #region CUSTOMER GROUP NEWSLETTER

        Route::group(['prefix' => 'customergroupnewsletter', 'middleware' => 'auth:api'], function () {
            Route::get('included/{id}', 'Api\v1\CustomerGroupNewsletterController@getIncludedByCustomer')->where('id', '[0-9]+');
            Route::get('excluded/{id}', 'Api\v1\CustomerGroupNewsletterController@getExcludedByCustomer')->where('id', '[0-9]+');
            Route::get('includedgroup/{id}', 'Api\v1\CustomerGroupNewsletterController@getIncludedByCustomerGroup')->where('id', '[0-9]+');
            Route::get('excludedgroup/{id}', 'Api\v1\CustomerGroupNewsletterController@getExcludedByCustomerGroup')->where('id', '[0-9]+');
            Route::put('', 'Api\v1\CustomerGroupNewsletterController@insert');
            Route::post('', 'Api\v1\CustomerGroupNewsletterController@update');
            Route::delete('{id}', 'Api\v1\CustomerGroupNewsletterController@delete')->where('id', '[0-9]+');
        });

        #endregion CUSTOMER  GROUP NEWSLETTER

        #region DICTIONARY

        Route::group(['prefix' => 'dictionary'], function () {
            Route::get('getByLanguage/{idLanguage}', 'Api\v1\DictionaryController@getByLanguage')->where('idLanguage', '[0-9]+');
        });

        #endregion DICTIONARY

        #region DOCUMENT HEAD

        Route::group(['prefix' => 'documenthead', 'middleware' => 'auth:api'], function () {
            Route::get('getSixtenOrderConfirmed/{idFunctionality}', 'Api\v1\DocumentHeadController@getSixtenOrderConfirmed')->where('idFunctionality', '[0-9]+');
            Route::get('selectMonthBeforeDDTSixten/{idCustomer}/{idFunctionality}', 'Api\v1\DocumentHeadController@getSelectMonthBeforeDDTSixten')->where(['idCustomer' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            Route::post('confirmBeforeDDTSixten', 'Api\v1\DocumentHeadController@confirmBeforeDDTSixten');

            #region MDMICRODETECTORS SHIPPING
            Route::get('getMdMicrodetectorsShippingList/{idFunctionality}', 'Api\v1\DocumentHeadController@getMdMicrodetectorsShippingList')->where('idFunctionality', '[0-9]+');
            #endregion MDMICRODETECTORS SHIPPING
        });

        #endregion DOCUMENT HEAD

        #region DOCUMENT ROW

        Route::group(['prefix' => 'documentrow', 'middleware' => 'auth:api'], function () {
            Route::get('getSixtenByHead/{idHead}/{idFunctionality}', 'Api\v1\DocumentRowController@getSixtenByHead')->where(['idHead' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            Route::get('getByErp/{idErp}/{idFunctionality}', 'Api\v1\DocumentRowController@getByErp')->where(['idErp' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            Route::get('getNumberPackageDDT/{idCustomer}/{idMonth}/{idFunctionality}', 'Api\v1\DocumentRowController@getNumberPackageDDT')->where(['idCustomer' => '[0-9]+', 'idMonth' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            Route::put('insertBeforeDDT', 'Api\v1\DocumentRowController@insertBeforeDDT');

            #region MDMICRODETECTORS SHIPPING

            Route::get('getMdMicrodetectorsByHead/{idHead}', 'Api\v1\DocumentRowController@getMdMicrodetectorsByHead');
            Route::post('updateMdMicrodetectorsShippingRow', 'Api\v1\DocumentRowController@updateMdMicrodetectorsShippingRow');
            Route::post('updateMdMicrodetectorsResetShippingRow', 'Api\v1\DocumentRowController@updateMdMicrodetectorsResetShippingRow');
            #endregion MDMICRODETECTORS SHIPPING
        });

        #endregion DOCUMENT ROW

        #region FEEDBACK NEWSLETTER

        Route::group(['prefix' => 'reportnewsletter'], function () {
            Route::group(['middleware' => ['auth:api']], function () {
                Route::get('allByNewsletter/{idNewsletter}', 'Api\v1\FeedbackNewsletterController@getAllByNewslletter')->where('idNewsletter', '[0-9]+');
                Route::get('getChart/{idNewsletter}', 'Api\v1\FeedbackNewsletterController@getChart')->where('idNewsletter', '[0-9]+');
                Route::get('getChartGraphics/{idNewsletter}', 'Api\v1\FeedbackNewsletterController@getChartGraphics')->where('idNewsletter', '[0-9]+');
                Route::get('getChartGraphicsforTime/{idNewsletter}', 'Api\v1\FeedbackNewsletterController@getChartGraphicsforTime')->where('idNewsletter', '[0-9]+');
            });
                Route::get('{idNewsletterRecipient}/{code}', 'Api\v1\FeedbackNewsletterController@insert')->where('idNewsletterRecipient', '[0-9]+');
        });

        #endregion FEEDBACK NEWSLETTER

        #region FILE

        Route::group(['prefix' => 'file', 'middleware' => 'auth:api'], function () {
            Route::post('', 'Api\v1\FileController@add');
            Route::post('checkUploadFileEditor', 'Api\v1\FileController@checkUploadFileEditor');
        });

        #endregion FILE

        #region GOOGLE ANALYTICS

        Route::group(['prefix' => 'googleAnalytics', 'middleware' => 'auth:api'], function () {
            Route::get('getUserActiveRealTime', 'Api\v1\GoogleAnalyticsController@getUserActiveRealTime');

            Route::post('getPagesViews', 'Api\v1\GoogleAnalyticsController@getPagesViews');
            Route::post('getVisitorsViews', 'Api\v1\GoogleAnalyticsController@getVisitorsViews');
            Route::post('getTotalUser', 'Api\v1\GoogleAnalyticsController@getTotalUsers');
            Route::post('getTotalSession', 'Api\v1\GoogleAnalyticsController@getTotalSession');
            Route::post('getTotalPageView', 'Api\v1\GoogleAnalyticsController@getTotalPageView');
            Route::post('getMostVisitedPages', 'Api\v1\GoogleAnalyticsController@getMostVisitedPages');
            Route::post('getPageViewPerSession', 'Api\v1\GoogleAnalyticsController@getPageViewPerSession');
            Route::post('getAvgSessionDuration', 'Api\v1\GoogleAnalyticsController@getAvgSessionDuration');
            Route::post('getBounceRate', 'Api\v1\GoogleAnalyticsController@getBounceRate');
            Route::post('getSourceSession', 'Api\v1\GoogleAnalyticsController@getSourceSession');
            Route::post('getReturningVisitors', 'Api\v1\GoogleAnalyticsController@getReturningVisitors');
            Route::post('getDeviceSession', 'Api\v1\GoogleAnalyticsController@getDeviceSession');

            Route::post('storeJsonKey', 'Api\v1\GoogleAnalitycsController@storeJsonKey');
        });

        #endregion GOOGLE ANALYTICS

        #region GROUP DISPLAY TECHNICAL SPECIFICATION

        Route::group(['prefix' => 'groupdisplaytechnicalspecification', 'middleware' => 'auth:api'], function () {
            Route::get('getDtoTable', 'Api\v1\GroupDisplayTechnicalSpecificationController@getDtoTable');
            Route::get('getTechnicalSpecification/{id}', 'Api\v1\GroupDisplayTechnicalSpecificationController@getTechnicalSpecification')->where('id', '[0-9]+');
            Route::get('checkExists/{description}/{idLanguage}/{id?}', 'Api\v1\GroupDisplayTechnicalSpecificationController@checkExists')->where(['idLanguage' => '[0-9]+', 'id' => '[0-9]+']);
            Route::put('', 'Api\v1\GroupDisplayTechnicalSpecificationController@insert');
            Route::put('addTechnicalSpecification/{id}/{idTechnicalSpecification}/{idFunctionality}', 'Api\v1\GroupDisplayTechnicalSpecificationController@addTechnicalSpecification')->where(['id' => '[0-9]+', 'idTechnicalSpecification' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            Route::post('', 'Api\v1\GroupDisplayTechnicalSpecificationController@update');
            Route::post('updateTechnicalSpecification/{id}/{order}/{idFunctionality}', 'Api\v1\GroupDisplayTechnicalSpecificationController@updateTechnicalSpecification')->where(['id' => '[0-9]+', 'order' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            Route::delete('{id}/{idFunctionality}', 'Api\v1\GroupDisplayTechnicalSpecificationController@delete')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);;
            Route::delete('deleteGroupDisplayTechnicalSpecificationTechnicalSpecification/{id}/{idFunctionality}', 'Api\v1\GroupDisplayTechnicalSpecificationController@deleteGroupDisplayTechnicalSpecificationTechnicalSpecification')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
        });

        #endregion GROUP DISPLAY TECHNICAL SPECIFICATION

        #region GROUP NEWSLETTER
          Route::group(['prefix' => 'groupnewsletter'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
            Route::get('{id}', 'Api\v1\GroupNewsletterController@getById')->where('id', '[0-9]+');
            Route::get('all', 'Api\v1\GroupNewsletterController@getAll');
            Route::put('', 'Api\v1\GroupNewsletterController@insert');
            Route::post('', 'Api\v1\GroupNewsletterController@update');
            Route::delete('{id}', 'Api\v1\GroupNewsletterController@delete')->where('id', '[0-9]+');
            Route::post('getAllByUser', 'Api\v1\GroupNewsletterController@getAllByUser');
            Route::post('getAllByContact', 'Api\v1\GroupNewsletterController@getAllByContact');
            Route::post('getUserSearch', 'Api\v1\GroupNewsletterController@getUserSearch');
            Route::post('getAllByUserGroup', 'Api\v1\GroupNewsletterController@getAllByUserGroup');
            Route::get('select', 'Api\v1\GroupNewsletterController@getSelect');

        });
            Route::put('insertUser', 'Api\v1\GroupNewsletterController@insertUser');
            Route::delete('delUser/{IdCustomer}/{rowIdSelected}', 'Api\v1\GroupNewsletterController@delUser')->where(['IdCustomer' => '[0-9]+', 'rowIdSelected' => '[0-9]+']);
            Route::put('insertContact', 'Api\v1\GroupNewsletterController@insertContact');
            Route::delete('delContact/{IdContact}/{rowIdSelected}', 'Api\v1\GroupNewsletterController@delContact')->where(['IdContact' => '[0-9]+', 'rowIdSelected' => '[0-9]+']);
            Route::delete('delGroupUser/{idGroupUser}/{rowIdSelected}', 'Api\v1\GroupNewsletterController@delGroupUser')->where(['idGroupUser' => '[0-9]+', 'rowIdSelected' => '[0-9]+']);
            Route::put('insertGroupUser', 'Api\v1\GroupNewsletterController@insertGroupUser');

        });
        #endregion GROUP NEWSLETTER

        #region GROUP TECHNICAL SPECIFICATION

        Route::group(['prefix' => 'grouptechnicalspecification', 'middleware' => 'auth:api'], function () {
            Route::get('getDtoTable', 'Api\v1\GroupTechnicalSpecificationController@getDtoTable');
            Route::get('getGraphic', 'Api\v1\GroupTechnicalSpecificationController@getGraphic');
            Route::get('getItems/{id}', 'Api\v1\GroupTechnicalSpecificationController@getItems')->where('id', '[0-9]+');
            Route::get('getTechnicalSpecification/{id}', 'Api\v1\GroupTechnicalSpecificationController@getTechnicalSpecification')->where('id', '[0-9]+');
            Route::get('checkExists/{description}/{idLanguage}/{id?}', 'Api\v1\GroupTechnicalSpecificationController@checkExists')->where(['idLanguage' => '[0-9]+', 'id' => '[0-9]+']);
            Route::put('', 'Api\v1\GroupTechnicalSpecificationController@insert');
            Route::put('addItem/{id}/{idItem}/{idFunctionality}', 'Api\v1\GroupTechnicalSpecificationController@addItem')->where(['id' => '[0-9]+', 'idItem' => '[0-9]+', 'idFunctionality' => '[0-9]+']);;
            Route::put('addTechnicalSpecification/{id}/{idTechnicalSpecification}/{idFunctionality}', 'Api\v1\GroupTechnicalSpecificationController@addTechnicalSpecification')->where(['id' => '[0-9]+', 'idTechnicalSpecification' => '[0-9]+', 'idFunctionality' => '[0-9]+']);;
            Route::post('', 'Api\v1\GroupTechnicalSpecificationController@update');
            Route::delete('{id}/{idFunctionality}', 'Api\v1\GroupTechnicalSpecificationController@delete')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            Route::delete('deleteItemGroupTechnicalSpecification/{id}/{idFunctionality}', 'Api\v1\GroupTechnicalSpecificationController@deleteItemGroupTechnicalSpecification')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            Route::delete('deleteGroupTechnicalSpecificationTechnicalSpecification/{id}/{idFunctionality}', 'Api\v1\GroupTechnicalSpecificationController@deleteGroupTechnicalSpecificationTechnicalSpecification')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
        });

        #endregion GROUP TECHNICAL SPECIFICATION

        #region ICON

        Route::group(['prefix' => 'icon'], function () {
            Route::get('select', 'Api\v1\IconController@getSelect');
            Route::get('{id}', 'Api\v1\IconController@getById')->where('id', '[0-9]+');
        });

        #endregion ICON

        #region IMAGE

        Route::group(['prefix' => 'image', 'middleware' => 'auth:api'], function () {
            Route::get('all', 'Api\v1\ImageController@getAll');
            Route::post('', 'Api\v1\ImageController@add');
            Route::delete('', 'Api\v1\ImageController@delete');
        });

        #endregion IMAGE

        #region ITEM

        Route::group(['prefix' => 'item'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('withGroupTechnicalSpecification', 'Api\v1\ItemController@getWithGroupTechnicalSpecification');
                Route::get('getGraphic', 'Api\v1\ItemController@getGraphic');
                Route::get('getImages/{idItem}', 'Api\v1\ItemController@getImages')->where('idItem', '[0-9]+');
                Route::post('getAllItemsWithoutCategory', 'Api\v1\ItemController@getAllItemsWithoutCategory');

                
                Route::get('getAllImagesInFolder/{id}', 'Api\v1\ItemController@getAllImagesInFolder')->where('id', '[0-9]+');
                Route::get('count', 'Api\v1\ItemController@count');
                Route::get('', 'Api\v1\ItemController@get');
                Route::put('', 'Api\v1\ItemController@insert');
                Route::put('insertRelated', 'Api\v1\ItemController@insertRelated');
                Route::put('insertTranslate', 'Api\v1\ItemController@insertTranslate');
                Route::post('getAllRelatedSearch', 'Api\v1\ItemController@getAllRelatedSearch');
                Route::delete('deleteItemCategories/{idCategories}/{idItem}', 'Api\v1\ItemController@deleteItemCategories')->where(['idCategories' => '[0-9]+', 'idItem' => '[0-9]+']);
                Route::put('insertItemCategories', 'Api\v1\ItemController@insertItemCategories');
                Route::put('insertItemImages', 'Api\v1\ItemController@insertItemImages');
                Route::post('updateImageName', 'Api\v1\ItemController@updateImageName');
                Route::post('updateOrderCategoriesItemTable', 'Api\v1\ItemController@updateOrderCategoriesItemTable');

                
                Route::post('updateOrderImamePhotos', 'Api\v1\ItemController@updateOrderImamePhotos');
                Route::post('updateItemImages', 'Api\v1\ItemController@updateItemImages');
                Route::post('updateItemCheckAssociation', 'Api\v1\ItemController@updateItemCheckAssociation');
                Route::post('updateOnlineItem', 'Api\v1\ItemController@updateOnlineItem');
                Route::post('updateOnlineItemCheck', 'Api\v1\ItemController@updateOnlineItemCheck');
                Route::post('updateAnnouncementsItemCheck', 'Api\v1\ItemController@updateAnnouncementsItemCheck');
                Route::post('updatePromotionItemCheck', 'Api\v1\ItemController@updatePromotionItemCheck');
                Route::delete('delete/{id}', 'Api\v1\ItemController@delete')->where('id', '[0-9]+');
                Route::delete('deleteItemsPhotos/{id}', 'Api\v1\ItemController@deleteItemsPhotos')->where('id', '[0-9]+');
            });
            Route::get('all/{idLanguage}', 'Api\v1\ItemController@getAll')->where('idLanguage', '[0-9]+');
            Route::get('select', 'Api\v1\ItemController@getSelect');
            Route::get('{id}', 'Api\v1\ItemController@getById')->where('id', '[0-9]+');
            Route::post('', 'Api\v1\ItemController@update');
            Route::delete('{id}', 'Api\v1\ItemController@RemoveFileDb')->where('id', '[0-9]+');
            Route::delete('RemoveFileDb/{id}', 'Api\v1\ItemController@RemoveFileDb')->where('id', '[0-9]+');
            Route::delete('removeFile/{id}', 'Api\v1\ItemController@removeFile')->where('id', '[0-9]+');
            Route::get('getByFile/{id}', 'Api\v1\ItemController@getByFile')->where('id', '[0-9]+');
            Route::get('getByFileNoId', 'Api\v1\ItemController@getByFileNoId');
            Route::get('getItemLanguages/{idItem}', 'Api\v1\ItemController@getItemLanguages')->where('idItem', '[0-9]+');
            Route::put('clone/{id}/{idFunctionality}', 'Api\v1\ItemController@clone')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            Route::get('getListItems/{id}', 'Api\v1\ItemController@getListItems')->where('id', '[0-9]+');
            Route::get('getItemById/{id}', 'Api\v1\ItemController@getItemById')->where('id', '[0-9]+');
            Route::get('getByPriceList/{id}', 'Api\v1\ItemController@getByPriceList')->where('id', '[0-9]+');
            Route::get('getByImgAgg/{id}', 'Api\v1\ItemController@getByImgAgg')->where('id', '[0-9]+');
            Route::get('getDocumentInformationById/{id}', 'Api\v1\ItemController@getDocumentInformationById')->where('id', '[0-9]+');
            Route::delete('getCancelTranslate/{id}', 'Api\v1\ItemController@getCancelTranslate')->where('id', '[0-9]+');
            Route::get('getAllRelated/{id}/{idLanguage}', 'Api\v1\ItemController@getAllRelated')->where(['id' => '[0-9]+', 'idLanguage' => '[0-9]+']);
            Route::get('getByTranslate/{id}', 'Api\v1\ItemController@getByTranslate')->where('id', '[0-9]+');
            Route::get('getByTranslateDefault/{id}/{idLanguage}/{idItem}', 'Api\v1\ItemController@getByTranslateDefault')->where(['id' => '[0-9]+', 'idItem' => '[0-9]+', 'idLanguage' => '[0-9]+']);
            Route::get('getByTranslateDefaultNew/{idLanguage}/{idItem}', 'Api\v1\ItemController@getByTranslateDefaultNew')->where(['idItem' => '[0-9]+', 'idLanguage' => '[0-9]+']);
            Route::post('getItemsUserShop', 'Api\v1\ItemController@getItemsUserShop');
            Route::post('downloadfile', 'Api\v1\ItemController@downloadfile');
            Route::post('getItemsCategoriesFilter', 'Api\v1\ItemController@getItemsCategoriesFilter');
            Route::post('updateNoAuth', 'Api\v1\ItemController@updateNoAuth');
            Route::post('updateTranslate', 'Api\v1\ItemController@updateTranslate');
            Route::post('updateImages', 'Api\v1\ItemController@updateImages');
            Route::post('addFavoritesItem', 'Api\v1\ItemController@addFavoritesItem');
            Route::get('movePhoto', 'Api\v1\ItemController@movePhoto');
            Route::get('moveFile', 'Api\v1\ItemController@moveFile');
            Route::post('updateImageAggFile', 'Api\v1\ItemController@updateImageAggFile'); 
            Route::delete('delRelated/{idItems}/{rowIdSelected}', 'Api\v1\ItemController@delRelated')->where(['idItems' => '[0-9]+', 'rowIdSelected' => '[0-9]+']);
            Route::post('getPriceItemSupport', 'Api\v1\ItemController@getPriceItemSupport');
            Route::post('getPriceItemSupportAndAddParams', 'Api\v1\ItemController@getPriceItemSupportAndAddParams');

        });
        #endregion ITEM

        #region ITEM GROUP TECHNICAL SPECIFICATION

        Route::group(['prefix' => 'itemgrouptechnicalspecification', 'middleware' => 'auth:api'], function () {
            Route::get('getItemWithoutGroupTechnicalSpecification', 'Api\v1\ItemGroupTechnicalSpecificationController@getItemWithoutGroupTechnicalSpecification');
        });

        #endregion ITEM GROUP TECHNICAL SPECIFICATION

        #region ITEM TECHNICAL SPECIFICATION
        Route::group(['prefix' => 'itemtechnicalspecification', 'middleware' => 'auth:api'], function () {
            Route::get('getConfiguration/{idItem}/{idFunctionality}/{idLanguage?}', 'Api\v1\ItemTechnicalSpecificationController@getConfiguration')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+', 'idLanguage' => '[0-9]+']);
            Route::get('getVersionConfiguration/{idItem}/{version}/{idFunctionality}/{idLanguage?}', 'Api\v1\ItemTechnicalSpecificationController@getVersionConfiguration')->where(['id' => '[0-9]+', 'version' => '[0-9]+', 'idFunctionality' => '[0-9]+', 'idLanguage' => '[0-9]+']);
            Route::get('getVersion/{idItem}/{idFunctionality}/{idLanguage?}', 'Api\v1\ItemTechnicalSpecificationController@getVersion')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+', 'idLanguage' => '[0-9]+']);
            Route::get('getCompletedStatByLanguage/{idLanguage}', 'Api\v1\ItemTechnicalSpecificationController@getCompletedStatByLanguage')->where(['idLanguage' => '[0-9]+']);
            Route::get('{idItem}/{idFunctionality}/{idLanguage?}/{version?}', 'Api\v1\ItemTechnicalSpecificationController@get')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+', 'idLanguage' => '[0-9]+', 'version' => '[0-9]+']);
            Route::put('', 'Api\v1\ItemTechnicalSpecificationController@insert');
            Route::post('complete', 'Api\v1\ItemTechnicalSpecificationController@complete');
        });

        #endregion ITEM TECHNICAL SPECIFICATION

        #region JS
        Route::group(['prefix' => 'js', 'middleware' => 'auth:api'], function () {
            Route::get('{idFunctionality}', 'Api\v1\JSController@get')->where('idFunctionality', '[0-9]+');
            Route::post('', 'Api\v1\JSController@update');
            Route::post('updateHead', 'Api\v1\JSController@updateHead');
        });
        #endregion JS

          #region Robots
          Route::group(['prefix' => 'robots', 'middleware' => 'auth:api'], function () {
            Route::get('{idFunctionality}', 'Api\v1\RobotsSettingController@get')->where('idFunctionality', '[0-9]+');
            Route::post('updateRobots', 'Api\v1\RobotsSettingController@updateRobots');
        });
        #endregion Robots

        #region LANGUAGE
        Route::group(['prefix' => 'language'], function () {
            Route::group(['middleware' => ['auth:api']], function () {
                Route::get('getConfiguration', 'Api\v1\LanguageController@getConfiguration');
                Route::get('selectlanguagestranslate', 'Api\v1\LanguageController@selectlanguagestranslate');

                Route::put('', 'Api\v1\LanguageController@insert');
                Route::post('', 'Api\v1\LanguageController@update');
                
            });
            Route::get('selectlanguagescategoriestranslate', 'Api\v1\LanguageController@selectlanguagescategoriestranslate');
            Route::get('selectlanguagesitemstranslate', 'Api\v1\LanguageController@selectlanguagesitemstranslate');
            Route::get('getDefault', 'Api\v1\LanguageController@getDefault');
            Route::get('get/{id}', 'Api\v1\LanguageController@get')->where('id', '[0-9]+');
            Route::get('select', 'Api\v1\LanguageController@getSelect');
            Route::get('all', 'Api\v1\LanguageController@getAll');

            Route::post('getUrlTranslate', 'Api\v1\LanguageController@getUrlTranslate');
        });
        #endregion LANGUAGE

        #region MANAGE STYLE

        Route::group(['prefix' => 'managestyle', 'middleware' => 'auth:api'], function () {
            Route::get('{idFunctionality}', 'Api\v1\ManageStyleController@get')->where('idFunctionality', '[0-9]+');
            Route::post('', 'Api\v1\ManageStyleController@update');
            Route::get('getCssEmail/{idFunctionality}', 'Api\v1\ManageStyleController@getCssEmail')->where('idFunctionality', '[0-9]+');
            Route::get('getCssHead/{idFunctionality}', 'Api\v1\ManageStyleController@getCssHead')->where('idFunctionality', '[0-9]+');
            Route::post('updateCssEmail', 'Api\v1\ManageStyleController@updateCssEmail');
            Route::post('updateSaveGlobal', 'Api\v1\ManageStyleController@updateSaveGlobal'); 
            Route::post('updateSaveHead', 'Api\v1\ManageStyleController@updateSaveHead'); 
        });

        #endregion MANAGE STYLE

        #region MENU

        Route::group(['prefix' => 'menu', 'middleware' => 'auth:api'], function () {
            Route::get('getSettings/{id}', 'Api\v1\MenuController@getSettings')->where('id', '[0-9]+');
            Route::put('link', 'Api\v1\MenuController@link');
            Route::put('folder', 'Api\v1\MenuController@folder');
            Route::post('link', 'Api\v1\MenuController@link');
            Route::post('newlink', 'Api\v1\MenuController@newlink');
            Route::post('updateNewlink', 'Api\v1\MenuController@updateNewlink');
            Route::post('folder', 'Api\v1\MenuController@folder');
        });

        #endregion MENU

        #region NATION
        Route::group(['prefix' => 'nation'], function () {
            Route::group(['middleware' => ['auth:api']], function () {
                Route::get('all/{idLanguage}', 'Api\v1\NationController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\NationController@getById')->where('id', '[0-9]+');
                Route::put('', 'Api\v1\NationController@insert');
                Route::post('', 'Api\v1\NationController@update');
                Route::delete('{id}', 'Api\v1\NationController@delete')->where('id', '[0-9]+');
            });
            Route::get('select', 'Api\v1\NationController@getSelect');
        });
        #endregion NATION

         #region REDIRECT
         Route::group(['prefix' => 'redirect'], function () {
            Route::group(['middleware' => ['auth:api']], function () {
                Route::get('all', 'Api\v1\RedirectController@getAll');
                Route::get('{id}', 'Api\v1\RedirectController@getById')->where('id', '[0-9]+');
                Route::put('', 'Api\v1\RedirectController@insert');
                Route::post('', 'Api\v1\RedirectController@update');
                Route::delete('{id}', 'Api\v1\RedirectController@delete')->where('id', '[0-9]+');
                Route::post('updateTable', 'Api\v1\RedirectController@updateTable');
                Route::put('insertRedirectCheck', 'Api\v1\RedirectController@insertRedirectCheck');
            });
                Route::get('select', 'Api\v1\RedirectController@getSelect');
        });
        #endregion REDIRECT

        

        Route::group(['prefix' => 'categoriespages'], function () {
            Route::group(['middleware' => ['auth:api']], function () {

                Route::get('all/{idLanguage}', 'Api\v1\CategoriesPagesController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\CategoriesPagesController@getById')->where('id', '[0-9]+');
                Route::put('', 'Api\v1\CategoriesPagesController@insert');
                Route::post('', 'Api\v1\CategoriesPagesController@update');
                Route::delete('{id}', 'Api\v1\CategoriesPagesController@delete')->where('id', '[0-9]+');
            });
                Route::get('select', 'Api\v1\CategoriesPagesController@getSelect');
        });

        #region NEGOTIATION

        Route::group(['prefix' => 'negotiation'], function () {
            Route::get('getByUser/{id}', 'Api\v1\NegotiationController@getByUser')->where('id', '[0-9]+');
            Route::get('getDetailById/{id}/{idUser}', 'Api\v1\NegotiationController@getDetailById')->where(['id' => '[0-9]+', 'idUser' => '[0-9]+']);
            Route::put('', 'Api\v1\NegotiationController@insert');
            Route::put('insertDetail', 'Api\v1\NegotiationController@insertDetail');
            Route::delete('deleteRow/{id}', 'Api\v1\NegotiationController@delete')->where('id', '[0-9]+');
        });

        #endregion NEGOTIATION

        #region NEWSLETTER
        Route::group(['prefix' => 'newsletter', 'middleware' => 'auth:api'], function () {
            Route::get('all', 'Api\v1\NewsletterController@getAll');
            Route::get('getAllCampaign', 'Api\v1\NewsletterController@getAllCampaign');
            Route::get('{id}', 'Api\v1\NewsletterController@getById')->where('id', '[0-9]+');
            Route::get('getemail/{id}', 'Api\v1\NewsletterController@getemail')->where('id', '[0-9]+');
            Route::get('getemailusers/{id}', 'Api\v1\NewsletterController@getemailusers')->where('id', '[0-9]+');
            Route::put('', 'Api\v1\NewsletterController@insert');
            Route::put('insertCampaign', 'Api\v1\NewsletterController@insertCampaign');
            Route::post('', 'Api\v1\NewsletterController@update');
            Route::delete('{id}', 'Api\v1\NewsletterController@delete')->where('id', '[0-9]+');
            Route::get('getListEmailByGroup/{id}', 'Api\v1\NewsletterController@getListEmailByGroup')->where('id', '[0-9]+');
            Route::get('getByListDateForId/{id}', 'Api\v1\NewsletterController@getByListDateForId')->where('id', '[0-9]+');   
            Route::post('updateCampaign', 'Api\v1\NewsletterController@updateCampaign');
            Route::get('getAllviewlistmailerror/{id}', 'Api\v1\NewsletterController@getAllviewlistmailerror')->where('id', '[0-9]+');  
            Route::get('getAllviewlistmailopen/{id}', 'Api\v1\NewsletterController@getAllviewlistmailopen')->where('id', '[0-9]+');
            Route::get('getListEmailQueryGroupNewsletter/{id}', 'Api\v1\NewsletterController@getListEmailQueryGroupNewsletter')->where('id', '[0-9]+');  
            Route::post('confirmSendMailTestSender', 'Api\v1\NewsletterController@confirmSendMailTestSender'); 
            Route::post('updateOnlineNewsletterCheck', 'Api\v1\NewsletterController@updateOnlineNewsletterCheck');
            Route::post('unsubscribefornewsletter', 'Api\v1\NewsletterController@unsubscribefornewsletter');
            Route::post('unsubscribeallfornewsletter', 'Api\v1\NewsletterController@unsubscribeallfornewsletter'); 
            Route::post('updateemailnewsletter', 'Api\v1\NewsletterController@updateemailnewsletter');
            Route::put('clone', 'Api\v1\NewsletterController@clone');
        });
        #endregion NEWSLETTER

        #region NEWSLETTER RECIPIENT

        Route::group(['prefix' => 'newsletterrecipient', 'middleware' => 'auth:api'], function () {
            Route::get('contact/{idNewsletter}', 'Api\v1\NewsletterRecipientController@getContactByNewsletter')->where('idNewsletter', '[0-9]+');
            Route::get('customer/{idNewsletter}', 'Api\v1\NewsletterRecipientController@getCustomerByNewsletter')->where('idNewsletter', '[0-9]+');
            Route::get('groupnewsletter/{idNewsletter}', 'Api\v1\NewsletterRecipientController@getGroupByNewsletter')->where('idNewsletter', '[0-9]+');
            Route::get('getNumberRecipients/{id}', 'Api\v1\NewsletterRecipientController@getNumberRecipients')->where('id', '[0-9]+');
            Route::put('contact', 'Api\v1\NewsletterRecipientController@insertContact');
            Route::put('customer', 'Api\v1\NewsletterRecipientController@insertCustomer');
            Route::put('groupnewsletter', 'Api\v1\NewsletterRecipientController@insertGroup');
            Route::delete('{id}', 'Api\v1\NewsletterRecipientController@delete')->where('id', '[0-9]+');
            Route::delete('groupnewsletter/{idNewsletter}/{idGroup}', 'Api\v1\NewsletterRecipientController@deleteGroup')->where(['idNewsletter' => '[0-9]+', 'idGroup' => '[0-9]+']);
        });

        #endregion NEWSLETTER RECIPIENT

        #region TYPOLOGY KEYWORDS

        Route::group(['prefix' => 'typologyKeywords', 'middleware' => 'auth:api'], function () {
            Route::get('all/{id}', 'Api\v1\TypologyKeywordsController@getAll')->where('id', '[0-9]+');
            Route::get('getAllSelect', 'Api\v1\TypologyKeywordsController@getAllSelect');
            Route::put('', 'Api\v1\TypologyKeywordsController@insert');
            Route::post('', 'Api\v1\TypologyKeywordsController@update');
            Route::delete('{id}', 'Api\v1\TypologyKeywordsController@delete')->where('id', '[0-9]+');
        });

        #endregion TYPOLOGY KEYWORDS

         #region LIST KEYWORDS

         Route::group(['prefix' => 'listKeywords', 'middleware' => 'auth:api'], function () {
            Route::get('all/{id}', 'Api\v1\ListKeywordsController@getAll')->where('id', '[0-9]+');
            Route::put('', 'Api\v1\ListKeywordsController@insert');
            Route::post('', 'Api\v1\ListKeywordsController@update');
            Route::delete('{id}', 'Api\v1\ListKeywordsController@delete')->where('id', '[0-9]+');
        });

        #endregion LIST KEYWORDS

        #region OPTIONAL

        Route::group(['prefix' => 'optional', 'middleware' => 'auth:api'], function () {
            Route::get('all/{id}', 'Api\v1\OptionalController@getAll')->where('id', '[0-9]+');
            Route::get('{id}', 'Api\v1\OptionalController@getById')->where('id', '[0-9]+');
            Route::put('', 'Api\v1\OptionalController@insert');
            Route::put('insertItemOptional', 'Api\v1\OptionalController@insertItemOptional');
            Route::post('', 'Api\v1\OptionalController@update');
            Route::delete('{id}', 'Api\v1\OptionalController@delete')->where('id', '[0-9]+');
            Route::delete('delItemOptional/{idOptional}/{idItem}', 'Api\v1\OptionalController@delItemOptional')->where(['idOptional' => '[0-9]+', 'idItem' => '[0-9]+']);
        });

        #endregion OPTIONAL

        #region QUOTES

        Route::group(['prefix' => 'quotes'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('{id}', 'Api\v1\QuotesController@getById')->where('id', '[0-9]+');
            });

            Route::get('getByUserId/{id}', 'Api\v1\QuotesController@getByUserId')->where('id', '[0-9]+');
            //Route::put('', 'Api\v1\QuotesController@insertByCartId');
            Route::post('insertByCartId', 'Api\v1\QuotesController@insertByCartId');
            Route::post('', 'Api\v1\QuotesController@update');
            Route::post('transformIntoCart', 'Api\v1\QuotesController@transformIntoCart');
            Route::delete('delquotes/{id}', 'Api\v1\QuotesController@delete')->where('id', '[0-9]+');
        });

        #endregion QUOTES

        #region FAQ

        Route::group(['prefix' => 'faq', 'middleware' => 'auth:api'], function () {
            Route::get('all/{id}', 'Api\v1\FaqController@getAll')->where('id', '[0-9]+');
            Route::put('', 'Api\v1\FaqController@insert');
            Route::post('', 'Api\v1\FaqController@update');
            Route::delete('{id}', 'Api\v1\FaqController@delete')->where('id', '[0-9]+');
        });

        #endregion FAQ

        #region SCHEDULER

        Route::group(['prefix' => 'scheduler', 'middleware' => 'auth:api'], function () {
            Route::get('all/{id}', 'Api\v1\SchedulerController@getAll')->where('id', '[0-9]+');

            Route::get('typeScheduler', 'Api\v1\SchedulerController@typeScheduler');
            Route::get('repeatHourScheduler', 'Api\v1\SchedulerController@repeatHourScheduler');
            Route::get('repeatMinutesScheduler', 'Api\v1\SchedulerController@repeatMinutesScheduler');

            Route::put('', 'Api\v1\SchedulerController@insert');
            Route::post('', 'Api\v1\SchedulerController@update');
            Route::post('updateCheckActive', 'Api\v1\SchedulerController@updateCheckActive');
            Route::post('updateCheckNotification', 'Api\v1\SchedulerController@updateCheckNotification');
            Route::delete('{id}', 'Api\v1\SchedulerController@delete')->where('id', '[0-9]+');
        });

        #endregion SCHEDULER

        #region FORM BUILDER

        Route::group(['prefix' => 'formbuilder'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{id}', 'Api\v1\FormBuilderController@getAll')->where('id', '[0-9]+');
                Route::get('all/getAllRequest', 'Api\v1\FormBuilderController@getAllRequest');
                Route::get('getAllById/{ln}/{id}', 'Api\v1\FormBuilderController@getAllById')->where(['ln' => '[0-9]+', 'id' => '[0-9]+']);
                Route::put('', 'Api\v1\FormBuilderController@insert');
                Route::post('', 'Api\v1\FormBuilderController@update');

                Route::put('insertFormBuilderField', 'Api\v1\FormBuilderController@insertFormBuilderField');
                Route::post('updateFormBuilderField', 'Api\v1\FormBuilderController@updateFormBuilderField');


                Route::put('clone/{id}/{idFunctionality}', 'Api\v1\FormBuilderController@clone')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
               

                Route::delete('{id}', 'Api\v1\FormBuilderController@delete')->where('id', '[0-9]+');
                Route::delete('delByContactAndFormId/{id}/{idForm}', 'Api\v1\FormBuilderController@delByContactAndFormId')->where(['id' => '[0-9]+', 'idForm' => '[0-9]+']);
                Route::delete('deleteFormBuilderField/{id}', 'Api\v1\FormBuilderController@deleteFormBuilderField')->where('id', '[0-9]+');
                Route::delete('deleteValueFormBuilderField/{id}', 'Api\v1\FormBuilderController@deleteValueFormBuilderField')->where('id', '[0-9]+');
                Route::delete('deleteValueFatherFormBuilderField/{id}', 'Api\v1\FormBuilderController@deleteValueFatherFormBuilderField')->where('id', '[0-9]+');
            });
            Route::post('insertContactFormBuilder', 'Api\v1\FormBuilderController@insertContactFormBuilder');
            Route::get('select', 'Api\v1\FormBuilderController@getSelect');
        });

        #endregion FORM BUILDER

        #region FORM BUILDER TYPE

        Route::group(['prefix' => 'formbuildertype'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{id}', 'Api\v1\FormBuilderTypeController@getAll')->where('id', '[0-9]+');
                Route::put('', 'Api\v1\FormBuilderTypeController@insert');
                Route::post('', 'Api\v1\FormBuilderTypeController@update');
                Route::delete('{id}', 'Api\v1\FormBuilderTypeController@delete')->where('id', '[0-9]+');
            });
            Route::get('select', 'Api\v1\FormBuilderTypeController@getSelect');
        });

        #endregion FORM BUILDER TYPE

        #region SALESMAN

        Route::group(['prefix' => 'salesman'], function () {
            Route::post('getAllUsersBySalesman', 'Api\v1\SalesmanController@getAllUsersBySalesman');
            Route::post('activeUserById', 'Api\v1\SalesmanController@activeUserById');
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{id}', 'Api\v1\SalesmanController@getAll')->where('id', '[0-9]+');
                Route::put('', 'Api\v1\SalesmanController@insert');
                Route::post('insertSalesmanUsers', 'Api\v1\SalesmanController@insertSalesmanUsers');
                Route::post('removeSalesmanUsers', 'Api\v1\SalesmanController@removeSalesmanUsers');
                Route::post('', 'Api\v1\SalesmanController@update');
                Route::delete('{id}', 'Api\v1\SalesmanController@delete')->where('id', '[0-9]+');
            });
        });

        #endregion SALESMAN

        #region ITEM ATTACHMENT TYPE

        Route::group(['prefix' => 'itemAttachmentType'], function () {
            Route::get('select', 'Api\v1\ItemAttachmentTypeController@getSelect');
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{id}', 'Api\v1\ItemAttachmentTypeController@getAll')->where('id', '[0-9]+');
                Route::get('{id}', 'Api\v1\ItemAttachmentTypeController@getById')->where('id', '[0-9]+');
                Route::put('', 'Api\v1\ItemAttachmentTypeController@insert');
                Route::post('', 'Api\v1\ItemAttachmentTypeController@update');
                Route::delete('{id}', 'Api\v1\ItemAttachmentTypeController@delete')->where('id', '[0-9]+');
            });
        });
        
        #endregion ITEM ATTACHMENT TYPE

        #region ITEM ATTACHMENT FILE
       

        Route::group(['prefix' => 'itemAttachmentFile'], function () {
            //Route::group(['prefix' => 'itemAttachmentFile', 'middleware' => 'auth:api'], function () {
            Route::get('select', 'Api\v1\ItemAttachmentFileController@getSelect');
            Route::get('all/{id}', 'Api\v1\ItemAttachmentFileController@getAll')->where('id', '[0-9]+');
            Route::get('getDetail/{id}/{ln}', 'Api\v1\ItemAttachmentFileController@getDetail')->where(['id' => '[0-9]+', 'ln' => '[0-9]+']);

            Route::get('{id}', 'Api\v1\ItemAttachmentFileController@getById')->where('id', '[0-9]+');
            Route::post('getByFile', 'Api\v1\ItemAttachmentFileController@getByFile');
            Route::post('deleteFromItemAttachmentFileItem', 'Api\v1\ItemAttachmentFileController@deleteFromItemAttachmentFileItem');

            
            Route::post('getItemByIdAttachment', 'Api\v1\ItemAttachmentFileController@getItemByIdAttachment')->where('id', '[0-9]+');
            Route::put('', 'Api\v1\ItemAttachmentFileController@insert');

            Route::put('insertFromAlbero', 'Api\v1\ItemAttachmentFileController@insertFromAlbero');
            Route::post('', 'Api\v1\ItemAttachmentFileController@update');
            Route::delete('{id}', 'Api\v1\ItemAttachmentFileController@delete')->where('id', '[0-9]+');
            Route::delete('delTranslation/{id}', 'Api\v1\ItemAttachmentFileController@delTranslation')->where('id', '[0-9]+');
            // Route::get('getAllByAttachmentUser/{id}/{idLanguage}', 'Api\v1\ItemAttachmentFileController@getAllByAttachmentUser')->where(['id' => '[0-9]+', 'idLanguage' => '[0-9]+']);
            Route::post('getAllByAttachmentUser', 'Api\v1\ItemAttachmentFileController@getAllByAttachmentUser');   

        });
        Route::group(['middleware' => 'auth:api'], function () {

        });


        
        #endregion ITEM ATTACHMENT FILE

        #region SUPPORTS

        Route::group(['prefix' => 'supports', 'middleware' => 'auth:api'], function () {
            Route::get('all/{id}', 'Api\v1\SupportsController@getAll')->where('id', '[0-9]+');
            Route::get('allPrices/{id}', 'Api\v1\SupportsController@getAllPricesSupports')->where('id', '[0-9]+');
            Route::put('', 'Api\v1\SupportsController@insert');
            Route::put('insertSupportPrice', 'Api\v1\SupportsController@insertSupportPrice');
            Route::put('insertItemSupport', 'Api\v1\SupportsController@insertItemSupport');
            Route::post('', 'Api\v1\SupportsController@update');
            Route::post('updateSupportPrice', 'Api\v1\SupportsController@updateSupportPrice');
            Route::post('updateUmSupportPrices', 'Api\v1\SupportsController@updateUmSupportPrices');
            Route::delete('{id}', 'Api\v1\SupportsController@delete')->where('id', '[0-9]+');
            Route::delete('deleteRangeDetailSupportsPrice/{id}', 'Api\v1\SupportsController@deleteRangeDetailSupportsPrice')->where('id', '[0-9]+');
        });
        #endregion SUPPORTS

        #region PRICELIST GENERATOR
        Route::group(['prefix' => 'pricelistgenerator'], function () {
            Route::group(['middleware' => ['auth:api']], function () {
                Route::get('all/{idLanguage}', 'Api\v1\PriceListGeneratorController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\PriceListGeneratorController@getById')->where('id', '[0-9]+');
                Route::put('', 'Api\v1\PriceListGeneratorController@insert');
                Route::post('', 'Api\v1\PriceListGeneratorController@update');
                Route::post('getAllByUser', 'Api\v1\PriceListGeneratorController@getAllByUser');
                Route::post('getAllByGroup', 'Api\v1\PriceListGeneratorController@getAllByGroup');
                Route::post('getAllByCategories', 'Api\v1\PriceListGeneratorController@getAllByCategories');
                Route::post('getAllByDestinationUsers', 'Api\v1\PriceListGeneratorController@getAllByDestinationUsers');
                Route::post('getAllByDestinationGroup', 'Api\v1\PriceListGeneratorController@getAllByDestinationGroup');
                Route::post('getAllByDestinationCategories', 'Api\v1\PriceListGeneratorController@getAllByDestinationCategories');
                Route::post('getAllByCategoriesNewList', 'Api\v1\PriceListGeneratorController@getAllByCategoriesNewList');
                Route::post('getByExecuteResult', 'Api\v1\PriceListGeneratorController@getByExecuteResult');
                Route::delete('{id}', 'Api\v1\PriceListGeneratorController@delete')->where('id', '[0-9]+');
                Route::post('updateOnlineCheck', 'Api\v1\PriceListGeneratorController@updateOnlineCheck');

            });
                Route::get('select', 'Api\v1\PriceListGeneratorController@getSelect');
        });
        #endregion PRICELIST GENERATOR

        #region PACKAGE

        Route::group(['prefix' => 'package', 'middleware' => 'auth:api'], function () {
            Route::get('selectByErp', 'Api\v1\PackageController@selectByErp');
            Route::get('getMaxByErp/{idErp}/{idFunctionality}', 'Api\v1\PackageController@getMaxByErp')->where('idFunctionality', '[0-9]+');
            Route::put('createByErp', 'Api\v1\PackageController@createByErp');
        });

        #endregion PACKAGE

        #region PAGE
        Route::group(['prefix' => 'page'], function () {
            Route::get('getMenuList/{id}/{pageType}', 'Api\v1\PageController@getMenuList')->where('id', '[0-9]+');
            Route::get('getSetting/{id}/{pageType}', 'Api\v1\PageController@getSetting')->where('id', '[0-9]+');
            Route::get('getByUserDescription/{sUserId}', 'Api\v1\PageController@getByUserDescription')->where('id', '[0-9]+');
            Route::get('getHome/{idLanguage?}', 'Api\v1\PageController@getHome');
            Route::get('getLastActivity/{idLanguage?}', 'Api\v1\PageController@getLastActivity');
            Route::post('getRender', 'Api\v1\PageController@getRender');
            Route::post('getRenderUser', 'Api\v1\PageController@getRenderUser');
            Route::post('getByFile', 'Api\v1\PageController@getByFile');
            Route::post('searchFile', 'Api\v1\PageController@searchFile');
            
            Route::group(['middleware' => ['auth:api', 'permission:pages']], function () {
                Route::get('url', 'Api\v1\PageController@getUrl');
                Route::get('getByFileEditorAll', 'Api\v1\PageController@getByFileEditorAll');
                Route::put('', 'Api\v1\PageController@insert');
                Route::put('clone/{id}/{idFunctionality}', 'Api\v1\PageController@clone')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
                Route::post('', 'Api\v1\PageController@update');
                Route::post('checkExistUrl', 'Api\v1\PageController@checkExistUrl');
                Route::post('url', 'Api\v1\PageController@postUrl');
                Route::post('menuList', 'Api\v1\PageController@postMenuList');
                Route::post('onlinePage', 'Api\v1\PageController@postOnlinePage');
                Route::post('insertImage', 'Api\v1\PageController@insertImage');
                Route::post('deleteImage', 'Api\v1\PageController@deleteImage');
                Route::post('convertImageToWebp', 'Api\v1\PageController@convertImageToWebp');
                Route::delete('{id}', 'Api\v1\PageController@delete')->where('id', '[0-9]+');
                Route::put('insertItemCategoriesPages', 'Api\v1\PageController@insertItemCategoriesPages');
                Route::delete('deleteItemCategoriesPages/{idCategories}/{idPage}', 'Api\v1\PageController@deleteItemCategoriesPages')->where(['idCategories' => '[0-9]+', 'idPage' => '[0-9]+']);
            });
        });
        #endregion PAGE

        #region PAGE CATEGORIES

        Route::group(['prefix' => 'pageCategory', 'middleware' => 'auth:api'], function () {
            Route::get('select', 'Api\v1\LanguagePageCategoryController@getSelect');
            Route::get('{id}', 'Api\v1\LanguagePageCategoryController@getById')->where('id', '[0-9]+');
            Route::get('getAllByPageId/{id}', 'Api\v1\LanguagePageCategoryController@getAllByPageId')->where('id', '[0-9]+');
            Route::get('getAllListCategories/{idLanguage}/{idPages}', 'Api\v1\LanguagePageCategoryController@getAllListCategories')->where(['idLanguage' => '[0-9]+', 'idPages' => '[0-9]+']);
        });

        #endregion PAGE CATEGORIES

        #region PACKAGING TYPE SIXTEN

        Route::group(['prefix' => 'packagingTypeSixten', 'middleware' => 'auth:api'], function () {
            Route::get('select', 'Api\v1\PackagingTypeSixtenController@getselect');
        });

        #endregion PACKAGING TYPE SIXTEN

        #region PAYMENT TYPE
        Route::group(['prefix' => 'paymenttype'], function () {
            Route::get('getAllByCartId/{idLanguage}/{cartId}', 'Api\v1\PaymentTypeController@getAllByCartId')->where(['idLanguage' => '[0-9]+', 'cartId' => '[0-9]+']);;
            Route::get('select', 'Api\v1\PaymentTypeController@getselect'); 
        });
        #endregion PAGE

        #region POSTAL CODE

        Route::group(['prefix' => 'postalcode', 'middleware' => 'auth:api'], function () {
            Route::get('select', 'Api\v1\PostalCodeController@getSelect');
            Route::get('{id}', 'Api\v1\PostalCodeController@getById')->where('id', '[0-9]+');
            Route::get('all', 'Api\v1\PostalCodeController@getAll');
            Route::put('', 'Api\v1\PostalCodeController@insert');
            Route::post('', 'Api\v1\PostalCodeController@update');
            Route::delete('{id}', 'Api\v1\PostalCodeController@delete')->where('id', '[0-9]+');
        });

        #endregion POSTAL CODE


        #region PROVINCE

        Route::group(['prefix' => 'provinces'], function () {
            Route::get('select', 'Api\v1\ProvinceController@getSelect');
        });

        #endregion PROVINCE

        #region TECHNICAL SPECIFICATION

        Route::group(['prefix' => 'technicalSpecification', 'middleware' => 'auth:api'], function () {
            Route::get('getDtoTable', 'Api\v1\TechnicalSpecificationController@getDtoTable');
            Route::get('getGroupDisplayTechnicalSpecification/{id}', 'Api\v1\TechnicalSpecificationController@getGroupDisplayTechnicalSpecification')->where('id', '[0-9]+');
            Route::get('getGroupTechnicalSpecification/{id}', 'Api\v1\TechnicalSpecificationController@getGroupTechnicalSpecification')->where('id', '[0-9]+');
            Route::get('checkExists/{description}/{idLanguage}/{id?}', 'Api\v1\TechnicalSpecificationController@checkExists')->where(['idLanguage' => '[0-9]+', 'id' => '[0-9]+']);
            Route::put('', 'Api\v1\TechnicalSpecificationController@insert');
            Route::put('addGroupDisplayTechnicalSpecification/{id}/{idGroupTechnicalSpecification}/{idFunctionality}', 'Api\v1\TechnicalSpecificationController@addGroupDisplayTechnicalSpecification')->where(['id' => '[0-9]+', 'idGroupTechnicalSpecification' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            Route::put('addGroupTechnicalSpecification/{id}/{idGroupTechnicalSpecification}/{idFunctionality}', 'Api\v1\TechnicalSpecificationController@addGroupTechnicalSpecification')->where(['id' => '[0-9]+', 'idGroupTechnicalSpecification' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            Route::post('', 'Api\v1\TechnicalSpecificationController@update');
            Route::delete('{id}/{idFunctionality}', 'Api\v1\TechnicalSpecificationController@delete')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
        });

        #endregion TECHNICAL SPECIFICATION

        #region ROLE


         Route::group(['prefix' => 'role'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
            Route::get('getByUser/{id}/{idFunctionality}', 'Api\v1\RoleController@getByUser')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            Route::post('getAllByRole', 'Api\v1\RoleController@getAllByRole'); 
            Route::post('getUserName', 'Api\v1\RoleController@getUserName');  

        });
            Route::put('insertRole', 'Api\v1\RoleController@insertRole'); 
            Route::delete('delRoleUser/{idRoleUser}/{user_id}', 'Api\v1\RoleController@delRoleUser')->where(['idRoleUser' => '[0-9]+', 'user_id' => '[0-9]+']);
        });

        #endregion ROLE

        #region ROLE USER

        Route::group(['prefix' => 'roleuser', 'middleware' => 'auth:api'], function () {
            Route::put('', 'Api\v1\RoleUserController@insert');
            Route::delete('{id}/{idFunctionality}', 'Api\v1\RoleUserController@delete')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
        });

        #endregion ROLE USER

        #region REGION

        Route::group(['prefix' => 'regions'], function () {
            Route::get('select', 'Api\v1\RegionController@getSelect');
        });

        #endregion REGION

        #region SETTING

        Route::group(['prefix' => 'setting'], function () {
            Route::get('getAll', 'Api\v1\SettingController@getAll');
            Route::get('getCodes', 'Api\v1\SettingController@getCodes');
            Route::get('getRenderMenu', 'Api\v1\SettingController@getRenderMenu');
            Route::get('getRenderBlog', 'Api\v1\SettingController@getRenderBlog');
            Route::get('getRenderNews', 'Api\v1\SettingController@getRenderNews');
            Route::get('getByCode/{code}', 'Api\v1\SettingController@getByCode');
            


            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('getLastSync', 'Api\v1\SettingController@getLastSync');
                Route::put('', 'Api\v1\SettingController@insert');
                Route::post('', 'Api\v1\SettingController@update');
                Route::post('updateImageNameFavicon', 'Api\v1\SettingController@updateImageNameFavicon');
                Route::delete('{id}', 'Api\v1\SettingController@delete')->where('id', '[0-9]+');
            });
        });

        #endregion SETTING

        #region SHARE TYPE

        Route::group(['prefix' => 'pageShareType', 'middleware' => 'auth:api'], function () {
            Route::get('select', 'Api\v1\LanguagePageShareTypeController@getSelect');
            Route::get('{id}', 'Api\v1\LanguagePageShareTypeController@getById')->where('id', '[0-9]+');
        });

        #endregion SHARE TYPE

        #region SHIPMENT CODE SIXTEN

        Route::group(['prefix' => 'shipmentCodeSixten', 'middleware' => 'auth:api'], function () {
            Route::get('select', 'Api\v1\ShipmentCodeSixtenController@getselect');
            Route::get('getByCustomerSixten/{idCustomer}/{idFunctionality}', 'Api\v1\ShipmentCodeSixtenController@getByCustomerSixten')->where(['idCustomer' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
        });

        #endregion SHIPMENT CODE SIXTEN

        #region SELENA VIEWS

        Route::group(['prefix' => 'selenaview', 'middleware' => 'auth:api'], function () {
            Route::get('all', 'Api\v1\SelenaViewsController@getAll');
            Route::get('{id}', 'Api\v1\SelenaViewsController@getById')->where('id', '[0-9]+');
            Route::put('', 'Api\v1\SelenaViewsController@insert');
            Route::post('', 'Api\v1\SelenaViewsController@update');
            Route::delete('{id}', 'Api\v1\SelenaViewsController@delete')->where('id', '[0-9]+');
        });

        #endregion SELENA VIEWS

        #region IMAGE RESIZE

        Route::group(['prefix' => 'imageresize', 'middleware' => 'auth:api'], function () {
            Route::get('all', 'Api\v1\ImageResizeController@getAll');
            Route::get('{id}', 'Api\v1\ImageResizeController@getById')->where('id', '[0-9]+');
            Route::put('', 'Api\v1\ImageResizeController@insert');
            Route::post('', 'Api\v1\ImageResizeController@update');
            Route::delete('{id}', 'Api\v1\ImageResizeController@delete')->where('id', '[0-9]+');
        });

        #endregion IMAGE RESIZE

         #region querygroupnewsletter
         Route::group(['prefix' => 'querygroupnewsletter'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
            Route::delete('{id}', 'Api\v1\QueryGroupNewsletterController@delete')->where('id', '[0-9]+');
            });
            Route::get('all', 'Api\v1\QueryGroupNewsletterController@getAll');
            Route::get('{id}', 'Api\v1\QueryGroupNewsletterController@getById')->where('id', '[0-9]+');
            Route::put('', 'Api\v1\QueryGroupNewsletterController@insert');
            Route::post('', 'Api\v1\QueryGroupNewsletterController@update');
            Route::get('select', 'Api\v1\QueryGroupNewsletterController@getSelect');
            Route::post('executequeryresult', 'Api\v1\QueryGroupNewsletterController@executequeryresult'); 
            Route::delete('deleteRow/{id}', 'Api\v1\QueryGroupNewsletterController@deleteRow')->where('id', '[0-9]+');         
        });
        #endregion querygroupnewsletter

          
        

        #region MESSAGES

        Route::group(['prefix' => 'message'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{idLanguage}', 'Api\v1\MessagesController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\MessagesController@getById')->where('id', '[0-9]+');
                Route::put('', 'Api\v1\MessagesController@insert');
                Route::post('', 'Api\v1\MessagesController@update');
                Route::delete('{id}', 'Api\v1\MessagesController@delete')->where('id', '[0-9]+');

            });
            Route::get('getByCode/{code}', 'Api\v1\MessagesController@getByCode');
            Route::get('select', 'Api\v1\MessagesController@getSelect');
        });

        #region cartRules
        Route::group(['prefix' => 'cartRules', 'middleware' => 'auth:api'], function () {
            Route::get('all', 'Api\v1\CartRulesController@getAll');
            Route::get('{id}', 'Api\v1\CartRulesController@getById')->where('id', '[0-9]+');
            Route::put('', 'Api\v1\CartRulesController@insert');
            Route::post('', 'Api\v1\CartRulesController@update');
            Route::delete('{id}', 'Api\v1\CartRulesController@delete')->where('id', '[0-9]+');
            Route::get('getUserSelectedAndNot/{id}', 'Api\v1\CartRulesController@getUserSelectedAndNot')->where('id', '[0-9]+');
            Route::put('insertUserCartRoleUser', 'Api\v1\CartRulesController@insertUserCartRoleUser');
            Route::delete('deleteUserCartRoleUser/{id}', 'Api\v1\CartRulesController@deleteUserCartRoleUser')->where('id', '[0-9]+');
            Route::post('getUserCartRolesSearch', 'Api\v1\CartRulesController@getUserCartRolesSearch');    
            Route::post('getUserSearchBusinessNameCartRuleUser', 'Api\v1\CartRulesController@getUserSearchBusinessNameCartRuleUser');
            Route::get('getGroupSelectedAndNot/{id}', 'Api\v1\CartRulesController@getGroupSelectedAndNot')->where('id', '[0-9]+');
            Route::put('insertCartRoleGroup', 'Api\v1\CartRulesController@insertCartRoleGroup');     
            Route::delete('deleteCartRoleGroup/{id}', 'Api\v1\CartRulesController@deleteCartRoleGroup')->where('id', '[0-9]+');     
            Route::post('getGroupCartRolesSearch', 'Api\v1\CartRulesController@getGroupCartRolesSearch');  
            Route::post('getGroupSearchNameCartRuleGroup', 'Api\v1\CartRulesController@getGroupSearchNameCartRuleGroup');  
            Route::get('getCarriersSelectedAndNot/{id}', 'Api\v1\CartRulesController@getCarriersSelectedAndNot')->where('id', '[0-9]+');
            Route::put('insertCarriersCartRole', 'Api\v1\CartRulesController@insertCarriersCartRole');     
            Route::delete('deleteCartRoleCarriers/{id}', 'Api\v1\CartRulesController@deleteCartRoleCarriers')->where('id', '[0-9]+');     
            Route::post('getCarriersCartRolesSearch', 'Api\v1\CartRulesController@getCarriersCartRolesSearch');  
            Route::post('getCarrierSearchNameCartRuleCarriers', 'Api\v1\CartRulesController@getCarrierSearchNameCartRuleCarriers');  
            Route::get('getItemsSelectedAndNot/{id}', 'Api\v1\CartRulesController@getItemsSelectedAndNot')->where('id', '[0-9]+');
            Route::get('getCategoriesSelectedAndNot/{id}', 'Api\v1\CartRulesController@getCategoriesSelectedAndNot')->where('id', '[0-9]+');
            Route::get('getProducersSelectedAndNot/{id}', 'Api\v1\CartRulesController@getProducersSelectedAndNot')->where('id', '[0-9]+');
            Route::get('getSuppliersSelectedAndNot/{id}', 'Api\v1\CartRulesController@getSuppliersSelectedAndNot')->where('id', '[0-9]+');
            Route::get('getSpecificProductsSelectedAndNot/{id}', 'Api\v1\CartRulesController@getSpecificProductsSelectedAndNot')->where('id', '[0-9]+');
            Route::put('insertItemCartRole', 'Api\v1\CartRulesController@insertItemCartRole');     
            Route::delete('deleteCartRoleItem/{id}', 'Api\v1\CartRulesController@deleteCartRoleItem')->where('id', '[0-9]+');   
            Route::post('getItemsCartRolesSearch', 'Api\v1\CartRulesController@getItemsCartRolesSearch');
             Route::post('getItemsSearchDescriptionCartRuleItems', 'Api\v1\CartRulesController@getItemsSearchDescriptionCartRuleItems');
             Route::put('insertCategoriesCartRoleCategories', 'Api\v1\CartRulesController@insertCategoriesCartRoleCategories'); 
             Route::put('insertProducersCartRoleProducers', 'Api\v1\CartRulesController@insertProducersCartRoleProducers');  
             Route::put('insertSuppliersCartRoleSuppliers', 'Api\v1\CartRulesController@insertSuppliersCartRoleSuppliers');    
             Route::put('insertSpecificItemsCartRole', 'Api\v1\CartRulesController@insertSpecificItemsCartRole');  
             Route::put('updateSpecificProductsValue', 'Api\v1\CartRulesController@updateSpecificProductsValue'); 
            Route::delete('deleteCategoriesCartRoleCategories/{id}', 'Api\v1\CartRulesController@deleteCategoriesCartRoleCategories')->where('id', '[0-9]+'); 
            Route::delete('deleteProducersCartRoleProducers/{id}', 'Api\v1\CartRulesController@deleteProducersCartRoleProducers')->where('id', '[0-9]+'); 
            Route::delete('deleteSuppliersCartRoleSuppliers/{id}', 'Api\v1\CartRulesController@deleteSuppliersCartRoleSuppliers')->where('id', '[0-9]+'); 
              
            Route::delete('deleteSpecificProductsCartRole/{id}', 'Api\v1\CartRulesController@deleteSpecificProductsCartRole')->where('id', '[0-9]+'); 
             Route::post('getCategoriesCartRolesSearch', 'Api\v1\CartRulesController@getCategoriesCartRolesSearch');
             Route::post('getProducersCartRolesSearch', 'Api\v1\CartRulesController@getProducersCartRolesSearch');
             Route::post('getSuppliersCartRolesSearch', 'Api\v1\CartRulesController@getSuppliersCartRolesSearch');
             Route::post('getSearchDescriptionCartRuleCategories', 'Api\v1\CartRulesController@getSearchDescriptionCartRuleCategories');
             Route::post('getSearchDescriptionCartRuleProducers', 'Api\v1\CartRulesController@getSearchDescriptionCartRuleProducers'); 
             Route::post('getSearchDescriptionCartRuleSuppliers', 'Api\v1\CartRulesController@getSearchDescriptionCartRuleSuppliers');   
             Route::post('getSpecificProductCartRolesSearch', 'Api\v1\CartRulesController@getSpecificProductCartRolesSearch'); 
             Route::post('getSearchSpecificProductsSelectedCartRule', 'Api\v1\CartRulesController@getSearchSpecificProductsSelectedCartRule');

        });

        #endregion cartRules

        #region VOUCHERS

        Route::group(['prefix' => 'voucher', 'middleware' => 'auth:api'], function () {
            Route::get('all', 'Api\v1\VoucherController@getAll');
            Route::get('{id}', 'Api\v1\VoucherController@getById')->where('id', '[0-9]+');
            Route::get('getByCode/{code}', 'Api\v1\VoucherController@getByCode');
            Route::put('', 'Api\v1\VoucherController@insert');
            Route::post('', 'Api\v1\VoucherController@update');
            Route::delete('{id}', 'Api\v1\VoucherController@delete')->where('id', '[0-9]+');
        });
        #endregion VOUCHER

        #region VOUCHER TYPE

        Route::group(['prefix' => 'VoucherType', 'middleware' => 'auth:api'], function () {
            Route::get('all/{idLanguage}', 'Api\v1\VoucherTypeController@getAll')->where('idLanguage', '[0-9]+');
            Route::get('select', 'Api\v1\VoucherTypeController@getSelect');
            Route::put('', 'Api\v1\VoucherTypeController@insert');
            Route::post('', 'Api\v1\VoucherTypeController@update');
            Route::get('{id}', 'Api\v1\VoucherTypeController@getById')->where('id', '[0-9]+');
             
        });

           #region document Statuses
           Route::group(['prefix' => 'documentStatuses'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
            Route::get('all/{idLanguage}', 'Api\v1\DocumentStatusesController@getAll')->where('idLanguage', '[0-9]+');
            Route::put('', 'Api\v1\DocumentStatusesController@insert');
            Route::post('', 'Api\v1\DocumentStatusesController@update');
            Route::get('{id}', 'Api\v1\DocumentStatusesController@getById')->where('id', '[0-9]+');
            Route::delete('{id}', 'Api\v1\DocumentStatusesController@delete')->where('id', '[0-9]+');
        });
            Route::get('select', 'Api\v1\DocumentStatusesController@getSelect');
    });

            

        #endregion VOUCHER TYPE

        #region MESSAGE TYPE
        Route::group(['prefix' => 'MessageType', 'middleware' => 'auth:api'], function () {
            Route::get('select', 'Api\v1\MessageTypeController@getSelect');
            Route::get('{id}', 'Api\v1\MessageTypeController@getById')->where('id', '[0-9]+');
        });
        #endregion MESSAGE TYPE


        #region SEARCH
        Route::group(['prefix' => 'search'], function () {
            Route::post('', 'Api\v1\SearchController@search');
            Route::post('searchImplemented', 'Api\v1\SearchController@searchImplemented');
            Route::post('searchGeneral', 'Api\v1\SearchController@searchGeneral');
        });
        #endregion SEARCH


        #region SEARCH DATA SHEET
          Route::group(['prefix' => 'searchdatasheet'], function () {
            Route::post('searchDataSheet', 'Api\v1\SearchDataSheetController@searchDataSheet');
            Route::post('searchPaginatorDataSheet', 'Api\v1\SearchDataSheetController@searchPaginatorDataSheet');
        });
        # END REGION SEARCH DATA SHEET


        #region SEARCHSHOP
        Route::group(['prefix' => 'searchshop'], function () {
            //Route::post('', 'Api\v1\SearchShopController@search');
            Route::post('searchImplemented', 'Api\v1\SearchShopController@searchImplemented');
        });
        #endregion SEARCHSHOP

        #region SEARCHLIUTAIO
        Route::group(['prefix' => 'searchliutaio'], function () {
            Route::post('searchImplemented', 'Api\v1\SearchLiutaioController@searchImplemented');
        });
        #endregion SEARCHLIUTAIO 

        #region SEARCHSALAREGISTRAZIONE
        Route::group(['prefix' => 'searchsalaregistrazione'], function () {
            Route::post('searchImplemented', 'Api\v1\SearchSalaRegistrazioneController@searchImplemented');
        });
        #endregion SEARCHSALAREGISTRAZIONE 

         #region SEARCHSCHOOLMUSIC
        Route::group(['prefix' => 'searchschoolmusic'], function () {
            Route::post('searchImplemented', 'Api\v1\SearchSchoolMusicController@searchImplemented');
        });
        #endregion SEARCHSCHOOLMUSIC 
        

        #region SEARCHINTERVENTION
        Route::group(['prefix' => 'searchintervention'], function () {
            Route::post('searchImplemented', 'Api\v1\SearchInterventionController@searchImplemented');
        });
        #endregion SEARCHINTERVENTION

        #region searchuser
        Route::group(['prefix' => 'searchuser'], function () {
            Route::post('searchImplemented', 'Api\v1\SearchUserController@searchImplemented');
            Route::post('SearchViewPrivates', 'Api\v1\SearchUserController@SearchViewPrivates');
            Route::post('SearchViewTrueSubscriber', 'Api\v1\SearchUserController@SearchViewTrueSubscriber');
            Route::post('SearchViewFalseSubscriber', 'Api\v1\SearchUserController@SearchViewFalseSubscriber');

        });
        #endregion searchuser

        #region searchitems
        Route::group(['prefix' => 'searchitems'], function () {
            Route::post('searchImplemented', 'Api\v1\SearchItemsController@searchImplemented');
        });
        #endregion searchitems

        
        #region searchitemsGr
        Route::group(['prefix' => 'searchitemsGr'], function () {
            Route::post('searchImplementedItemsGr', 'Api\v1\SearchItemsListGrController@searchImplementedItemsGr');
            Route::get('getResultDetail/{id}', 'Api\v1\SearchItemsListGrController@getResultDetail')->where('id', '[0-9]+');
            Route::post('searchImplementedItemsGlobal', 'Api\v1\SearchItemsListGrController@searchImplementedItemsGlobal');

        });
        #endregion searchitemsGr


        #region TEMPLATE EDITOR

        Route::group(['prefix' => 'templateeditor', 'middleware' => 'auth:api'], function () {
            Route::get('getDto/{idFunctionality}', 'Api\v1\TemplateEditorController@getDto')->where('idFunctionality', '[0-9]+');
            Route::get('getTemplateById/{id}', 'Api\v1\TemplateEditorController@getTemplateById')->where('id', '[0-9]+');
            Route::put('', 'Api\v1\TemplateEditorController@insert');
            Route::put('clone/{id}/{idFunctionality}', 'Api\v1\TemplateEditorController@clone')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            Route::post('', 'Api\v1\TemplateEditorController@update');
            Route::post('table', 'Api\v1\TemplateEditorController@updateTable');
            Route::delete('{id}/{idFunctionality}', 'Api\v1\TemplateEditorController@delete')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            Route::get('getTemplateDefaultValue', 'Api\v1\TemplateEditorController@getTemplateDefaultValue');
            Route::get('getTemplateDefaultValueSelect', 'Api\v1\TemplateEditorController@getTemplateDefaultValueSelect');

        });

        #endregion TEMPLATE EDITOR

        #region TEMPLATE EDITOR GROUP

        Route::group(['prefix' => 'templateeditorgroup', 'middleware' => 'auth:api'], function () {
            Route::get('{id}', 'Api\v1\TemplateEditorGroupController@getById')->where('id', '[0-9]+');
            Route::get('getDto/{idFunctionality}', 'Api\v1\TemplateEditorGroupController@getDto')->where('idFunctionality', '[0-9]+');
            Route::get('select', 'Api\v1\TemplateEditorGroupController@getSelect');
            Route::get('selectGroupTemplate', 'Api\v1\TemplateEditorGroupController@selectGroupTemplate');
            Route::put('', 'Api\v1\TemplateEditorGroupController@insert');
            Route::post('', 'Api\v1\TemplateEditorGroupController@update');
            Route::put('clone/{id}/{idFunctionality}', 'Api\v1\TemplateEditorGroupController@clone')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            Route::delete('{id}/{idFunctionality}', 'Api\v1\TemplateEditorGroupController@delete')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
        });

        #endregion TEMPLATE EDITOR GROUP

        #region TEMPLATE EDITOR TYPE

        Route::group(['prefix' => 'templateeditortype', 'middleware' => 'auth:api'], function () {
            Route::get('{id}', 'Api\v1\TemplateEditorTypeController@getById')->where('id', '[0-9]+');
            Route::get('select', 'Api\v1\TemplateEditorTypeController@getSelect');
        });

        #endregion TEMPLATE EDITOR TYPE

        #region TYPE OPTIONAL

            Route::group(['prefix' => 'typeoptional'], function () {
                Route::group(['middleware' => 'auth:api'], function () {

            Route::get('all/{id}', 'Api\v1\TypeOptionalController@getAll')->where('id', '[0-9]+');
            Route::get('getAllTypeAndOptional/{id}/{idItem}', 'Api\v1\TypeOptionalController@getAllTypeAndOptional')->where(['id' => '[0-9]+', 'idItem' => '[0-9]+']);
            Route::get('{id}', 'Api\v1\TypeOptionalController@getById')->where('id', '[0-9]+');
            //Route::get('select', 'Api\v1\TypeOptionalController@getSelect');
            Route::put('', 'Api\v1\TypeOptionalController@insert');
            Route::put('insertItemTypeOptional', 'Api\v1\TypeOptionalController@insertItemTypeOptional');
            Route::post('', 'Api\v1\TypeOptionalController@update');
            Route::delete('{id}', 'Api\v1\TypeOptionalController@delete')->where('id', '[0-9]+');
            Route::delete('delItemTypeOptional/{idTypeOptional}/{idItem}', 'Api\v1\TypeOptionalController@delItemTypeOptional')->where(['idTypeOptional' => '[0-9]+', 'idItem' => '[0-9]+']);
        });
            Route::get('select', 'Api\v1\TypeOptionalController@getSelect');
    });


        #endregion TYPE OPTIONAL


        #region USER

        Route::group(['prefix' => 'user'], function () {
            Route::group(['middleware' => 'auth:api'], function () {

                Route::get('getAllUser/{idLanguage}', 'Api\v1\UserController@getAllUser')->where('idLanguage', '[0-9]+');
                Route::get('getGraphic', 'Api\v1\UserController@getGraphic');
                Route::get('all/{idFunctionality}', 'Api\v1\UserController@getAll')->where('idFunctionality', '[0-9]+');
                Route::get('getByIdResult/{id}', 'Api\v1\UserController@getByIdResult')->where('id', '[0-9]+');
                Route::get('getAllUserSales/{id}', 'Api\v1\UserController@getAllUserSales')->where('id', '[0-9]+');
                Route::get('getAllDetailValueSalesUser/{id}', 'Api\v1\UserController@getAllDetailValueSalesUser')->where('id', '[0-9]+');
                Route::get('count', 'Api\v1\UserController@count');
                Route::get('{id}', 'Api\v1\UserController@getById')->where('id', '[0-9]+');
                Route::put('', 'Api\v1\UserController@insert');
                Route::post('checkExistUsername', 'Api\v1\UserController@checkExistUsername');
                Route::post('checkGroupUser', 'Api\v1\UserController@checkGroupUser');
                Route::post('checkExistEmail', 'Api\v1\UserController@checkExistEmail');
                Route::post('', 'Api\v1\UserController@update');
                Route::post('changePassword', 'Api\v1\UserController@changePassword');
                Route::delete('{id}/{idFunctionality}', 'Api\v1\UserController@delete')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);

            });

            Route::get('getByUsername/{username}', 'Api\v1\UserController@getByUsername')->where('username', '[a-z]+');
            Route::get('getCommunityData/{id}', 'Api\v1\UserController@getCommunityData')->where(['id' => '[0-9]+']);
            Route::post('availableCheck/{id}', 'Api\v1\UserController@availableCheck')->where(['id' => '[0-9]+']);
            Route::put('insertNoAuth', 'Api\v1\UserController@insertNoAuth');
            Route::get('getUserData/{id}', 'Api\v1\UserController@getUserData')->where('id', '[0-9]+');
            Route::post('updateNoAuth', 'Api\v1\UserController@updateNoAuth');
            Route::post('rescuePassword', 'Api\v1\UserController@rescuePassword');
            Route::post('ResetConfermationEmail', 'Api\v1\UserController@ResetConfermationEmail');
            Route::put('insertUser', 'Api\v1\UserController@insertUser');
        
            Route::post('updateUser', 'Api\v1\UserController@updateUser');
            Route::post('updateFieldById', 'Api\v1\UserController@updateFieldById');
            Route::post('updateVatNumber', 'Api\v1\UserController@updateVatNumber');
            Route::post('changePasswordRescued', 'Api\v1\UserController@changePasswordRescued');
            Route::post('updateProfileImage', 'Api\v1\UserController@updateProfileImage');
            Route::post('uploadImageCoverProfile', 'Api\v1\UserController@uploadImageCoverProfile');
            Route::get('selectUserPages', 'Api\v1\UserController@getSelectUserPages');
            Route::get('select', 'Api\v1\UserController@getSelect');
            Route::get('selectProv', 'Api\v1\UserController@getSelectProv');
            Route::get('selectIntervention', 'Api\v1\UserController@getSelectIntervention');
            Route::delete('{id}', 'Api\v1\UserController@deleteRow')->where('id', '[0-9]+');
            Route::get('getDifferentDestination/{id}', 'Api\v1\UserController@getDifferentDestination')->where('id', '[0-9]+');
            Route::get('getDetailUsers/{id}', 'Api\v1\UserController@getDetailUsers')->where('id', '[0-9]+');
            
            Route::post('updateorcreateUsersAdresses', 'Api\v1\UserController@updateorcreateUsersAdresses');
            Route::post('updateorcreateDestinationBusinessName', 'Api\v1\UserController@updateorcreateDestinationBusinessName');
            Route::get('getAllAddress/{id}', 'Api\v1\UserController@getAllAddress')->where('id', '[0-9]+');
            Route::get('getbyIdAddress/{id}', 'Api\v1\UserController@getbyIdAddress')->where('id', '[0-9]+');
            Route::post('checkUserLoggedToUserUrl', 'Api\v1\UserController@checkUserLoggedToUserUrl');
            Route::post('updateUserAddress', 'Api\v1\UserController@updateUserAddress');
            Route::put('insertUserAddress', 'Api\v1\UserController@insertUserAddress');
            Route::put('insertComment', 'Api\v1\UserController@insertComment');
            Route::post('updateCheckOnlineAddress', 'Api\v1\UserController@updateCheckOnlineAddress');
            Route::post('updateCheckDetailDefaultAddress', 'Api\v1\UserController@updateCheckDetailDefaultAddress');
            Route::post('updateCheckOnlineReferentDetail', 'Api\v1\UserController@updateCheckOnlineReferentDetail');
            Route::get('getAllUserHistorical/{id}', 'Api\v1\UserController@getAllUserHistorical')->where('id', '[0-9]+');
            Route::get('getAllDetailTimeLine/{id}', 'Api\v1\UserController@getAllDetailTimeLine')->where('id', '[0-9]+');
            
            Route::put('insertSalesUser', 'Api\v1\UserController@insertSalesUser');
            Route::post('updateSalesUser', 'Api\v1\UserController@updateSalesUser');
            Route::delete('deleteAddress/{id}', 'Api\v1\UserController@deleteAddress')->where('id', '[0-9]+');   
        });
        #endregion USER

        #region USER ADDRESS
        Route::group(['prefix' => 'useraddress'], function () {
            Route::post('', 'Api\v1\UserAddressController@insert');
            Route::post('activeAddressByUser', 'Api\v1\UserAddressController@activeAddressByUser');
            Route::get('{id}', 'Api\v1\UserAddressController@getAllByUser')->where('id', '[0-9]+');
            Route::delete('deleteAddressById/{id}', 'Api\v1\UserAddressController@deleteAddressById')->where('id', '[0-9]+');
        });
       #endregion USER ADDRESS


            #region SALES ORDER 
            Route::group(['prefix' => 'salesorder'], function () {
                Route::group(['middleware' => 'auth:api'], function () {
                    Route::get('BySalesOrderAdmin/{id}', 'Api\v1\SalesOrderController@getBySalesOrderAdmin')->where('id', '[0-9]+');  
                    Route::get('DetailPopupSelect/{id}', 'Api\v1\SalesOrderController@DetailPopupSelect')->where('id', '[0-9]+');  
                    Route::delete('{id}', 'Api\v1\SalesOrderController@delete')->where('id', '[0-9]+');
                });
                Route::get('{id}/{idLanguage}', 'Api\v1\SalesOrderController@getAllBySales')->where(['id', '[0-9]+', 'idLanguage' => '[0-9]+']);
                Route::post('getAllBySalesmanId', 'Api\v1\SalesOrderController@getAllBySalesmanId');   
                Route::get('getByDetailSaleOrder/{id}/{idLanguage}', 'Api\v1\SalesOrderController@getByDetailSaleOrder')->where(['id', '[0-9]+', 'idLanguage' => '[0-9]+']); 
                Route::post('updateSalesOrderDetail', 'Api\v1\SalesOrderController@updateSalesOrderDetail');
                Route::delete('deleteOrderDetail/{id}', 'Api\v1\SalesOrderController@deleteOrderDetail')->where('id', '[0-9]+');  
                Route::post('updateTrackingShipment', 'Api\v1\SalesOrderController@updateTrackingShipment');
                Route::post('SendMailCustomer', 'Api\v1\SalesOrderController@SendMailCustomer');
                Route::post('SendShipment', 'Api\v1\SalesOrderController@SendShipment'); 
                Route::put('insertMessages', 'Api\v1\SalesOrderController@insertMessages'); 
                Route::post('updateNote', 'Api\v1\SalesOrderController@updateNote');
                Route::post('insertAndSendMessages', 'Api\v1\SalesOrderController@insertAndSendMessages'); 
                Route::post('insertFileOrderSales', 'Api\v1\SalesOrderController@insertFileOrderSales'); 
                Route::post('sendEmailDetailSummaryOrder', 'Api\v1\SalesOrderController@sendEmailDetailSummaryOrder');
                Route::post('saveChangeStatus', 'Api\v1\SalesOrderController@saveChangeStatus');
                Route::get('getAllSalesOrder', 'Api\v1\SalesOrderController@getAllSalesOrder');

                
            });
            #endregion SALES ORDER

        #region INTERVENTION
        Route::group(['prefix' => 'intervention'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{idLanguage}', 'Api\v1\InterventionController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('getAllIntervention/{idLanguage}', 'Api\v1\InterventionController@getAllIntervention')->where('idLanguage', '[0-9]+');
                Route::get('getAllRepairArriving/{idLanguage}', 'Api\v1\InterventionController@getAllRepairArriving')->where('idLanguage', '[0-9]+');
                Route::get('select', 'Api\v1\InterventionController@getSelect');
                Route::get('getLastId', 'Api\v1\InterventionController@getLastId');
                Route::get('getLastIdLib', 'Api\v1\InterventionController@getLastIdLib');
                Route::get('getDefaultValueSelectStates', 'Api\v1\InterventionController@getDefaultValueSelectStates');
                Route::post('', 'Api\v1\InterventionController@update');
                Route::delete('{id}', 'Api\v1\InterventionController@delete')->where('id', '[0-9]+');
                Route::put('clone/{id}/{idFunctionality}', 'Api\v1\InterventionController@clone')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            });
            Route::get('getAllRepairArrivedNumberMenuGlobal/{idLanguage}', 'Api\v1\InterventionController@getAllRepairArrivedNumberMenuGlobal')->where('idLanguage', '[0-9]+');
            Route::get('{id}', 'Api\v1\InterventionController@getById')->where('id', '[0-9]+');
            Route::get('getnumbertelephone/{id}', 'Api\v1\InterventionController@getnumbertelephone')->where('id', '[0-9]+');
            Route::get('getdescription/{id}', 'Api\v1\InterventionController@getdescription')->where('id', '[0-9]+');
            Route::get('getdescriptionbynameproduct/{id}', 'Api\v1\InterventionController@getdescriptionbynameproduct')->where('id', '[0-9]+');   
            Route::get('getByViewValue/{id}', 'Api\v1\InterventionController@getByViewValue')->where('id', '[0-9]+');
            Route::get('getByViewQuotes/{id}/{idUser}', 'Api\v1\InterventionController@getByViewQuotes')->where(['id', '[0-9]+', 'idUser' => '[0-9]+']);

            Route::get('getByModuleGuarantee/{id}', 'Api\v1\InterventionController@getByModuleGuarantee')->where('id', '[0-9]+');
            Route::get('getByFaultModule/{id}', 'Api\v1\InterventionController@getByFaultModule')->where('id', '[0-9]+');
            Route::get('getByViewExternalRepair/{id}', 'Api\v1\InterventionController@getByViewExternalRepair')->where('id', '[0-9]+');
            Route::get('getBySelectReferent/{id}', 'Api\v1\InterventionController@getBySelectReferent')->where('id', '[0-9]+');
            Route::get('getByIdUser/{user_id}', 'Api\v1\InterventionController@getByIdUser')->where('user_id', '[0-9]+');
            Route::get('getDescriptionAndPriceProcessingSheet/{id}', 'Api\v1\InterventionController@getDescriptionAndPriceProcessingSheet')->where('id', '[0-9]+');
            Route::post('updateIntervention', 'Api\v1\InterventionController@updateIntervention');
            Route::post('updateSelectReferent', 'Api\v1\InterventionController@updateSelectReferent');
            Route::post('updateInterventionUpdateProcessingSheet', 'Api\v1\InterventionController@updateInterventionUpdateProcessingSheet');
            Route::post('updateInterventionUpdateViewQuotes', 'Api\v1\InterventionController@updateInterventionUpdateViewQuotes');
            Route::post('updateUser', 'Api\v1\InterventionController@updateUser');
            Route::post('updateExternalRepair', 'Api\v1\InterventionController@updateExternalRepair');
            Route::put('', 'Api\v1\InterventionController@insert');
            Route::put('insertModule', 'Api\v1\InterventionController@insertModule');
            Route::post('updateCheck', 'Api\v1\InterventionController@updateCheck');   
            Route::post('updateCheckRepairArrived', 'Api\v1\InterventionController@updateCheckRepairArrived'); 
            Route::post('SearchOpen', 'Api\v1\InterventionController@SearchOpen');
            Route::post('SearchClose', 'Api\v1\InterventionController@SearchClose'); 
            Route::post('SearchTrue', 'Api\v1\InterventionController@SearchTrue'); 
            Route::get('getByInterventionFU/{id}', 'Api\v1\InterventionController@getByInterventionFU')->where('id', '[0-9]+');
            Route::post('updateInterventionFU', 'Api\v1\InterventionController@updateInterventionFU');
        });
        #endregion INTERVENTION

         #region interventioncustomer
                        Route::group(['prefix' => 'interventioncustomer'], function () {
                        Route::group(['middleware' => 'auth:api'], function () { 
                        Route::get('getAllInterventionCustomer/{idUser}', 'Api\v1\InterventionCustomerController@getAllInterventionCustomer')->where('idUser', '[0-9]+');          
                        Route::post('updateInterventionUpdateViewCustomerQuotes', 'Api\v1\InterventionCustomerController@updateInterventionUpdateViewCustomerQuotes');
                        Route::get('getByViewQuotesCustomer/{id}', 'Api\v1\InterventionCustomerController@getByViewQuotesCustomer')->where('id', '[0-9]+');
                        Route::get('getByViewModuleCustomer/{id}', 'Api\v1\InterventionCustomerController@getByViewModuleCustomer')->where('id', '[0-9]+');
                        Route::get('getByViewModuleValidationGuarantee/{id}', 'Api\v1\InterventionCustomerController@getByViewModuleValidationGuarantee')->where('id', '[0-9]+');
                        Route::post('updateModuleValidationGuarantee', 'Api\v1\InterventionCustomerController@updateModuleValidationGuarantee'); 
                        Route::post('updateSendFaultModule', 'Api\v1\InterventionCustomerController@updateSendFaultModule');                        
                        Route::put('insertMessageCustomerQuotes', 'Api\v1\InterventionCustomerController@insertMessageCustomerQuotes');
                    
                    }); 
                        Route::post('getAllInterventionCustomerSearch', 'Api\v1\InterventionCustomerController@getAllInterventionCustomerSearch');

                    });
                #end region interventioncustomer           

                # region internalrepair
                        Route::group(['prefix' => 'internalrepair'], function () {
                        Route::group(['middleware' => 'auth:api'], function () { 
                        Route::get('getAllInternalRepair/{idLanguage}', 'Api\v1\InternalRepairController@getAllInternalRepair')->where('idLanguage', '[0-9]+');          
                        Route::delete('{id}', 'Api\v1\InternalRepairController@delete')->where('id', '[0-9]+');
                    });
                        Route::get('getLastId', 'Api\v1\InternalRepairController@getLastId');
                        Route::put('', 'Api\v1\InternalRepairController@insert');
                        Route::get('getByInternalRepair/{id}', 'Api\v1\InternalRepairController@getByInternalRepair')->where('id', '[0-9]+');
                        Route::post('updateInternalRepair', 'Api\v1\InternalRepairController@updateInternalRepair');
                        Route::post('getdescription', 'Api\v1\InternalRepairController@getdescription');
                        Route::post('getdescriptioninsert', 'Api\v1\InternalRepairController@getdescriptioninsert');   
                        Route::post('getAllInternalRepairSearch', 'Api\v1\InternalRepairController@getAllInternalRepairSearch'); 
                        Route::post('getinternalRepairSearchViewOpen', 'Api\v1\InternalRepairController@getinternalRepairSearchViewOpen');
                        Route::post('getinternalRepairSearchViewClose', 'Api\v1\InternalRepairController@getinternalRepairSearchViewClose');
                        Route::get('getLastIdIntRepair', 'Api\v1\InternalRepairController@getLastIdIntRepair');
                        Route::post('getAllInternalExternalRepairSearch', 'Api\v1\InternalRepairController@getAllInternalExternalRepairSearch');

                    });
                # end region internalrepair

                #region fasturgencies
                    Route::group(['prefix' => 'fasturgencies'], function () {
                        Route::group(['middleware' => 'auth:api'], function () {
                                    
                        });
                        Route::put('', 'Api\v1\FastController@insert');
                        Route::get('getUserData/{id}', 'Api\v1\FastController@getUserData')->where('id', '[0-9]+');
                        Route::post('updateFast', 'Api\v1\FastController@updateFast');
                        Route::delete('{id}', 'Api\v1\FastController@delete')->where('id', '[0-9]+');
                    });
                #end region fasturgencies

        #region externalrepair
        Route::group(['prefix' => 'externalrepair'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::delete('{id}', 'Api\v1\ExternalRepairController@delete')->where('id', '[0-9]+');
                Route::get('getAllexternalRepair/{idLanguage}', 'Api\v1\ExternalRepairController@getAllexternalRepair')->where('idLanguage', '[0-9]+');
                Route::get('getByRepair/{id}', 'Api\v1\ExternalRepairController@getByRepair')->where('id', '[0-9]+');
                Route::post('updateRepairExternal', 'Api\v1\ExternalRepairController@updateRepairExternal');     
                Route::post('getbyFaultModule/{id}', 'Api\v1\ExternalRepairController@getbyFaultModule')->where('id', '[0-9]+'); 
            });
                Route::post('getAllexternalRepairSearch', 'Api\v1\ExternalRepairController@getAllexternalRepairSearch');
                Route::post('getexternalRepairSearchViewOpen', 'Api\v1\ExternalRepairController@getexternalRepairSearchViewOpen');
                Route::post('getexternalRepairSearchViewClose', 'Api\v1\ExternalRepairController@getexternalRepairSearchViewClose');
        });

        #region accountvision
 Route::group(['prefix' => 'accountvision'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::delete('{id}', 'Api\v1\AccountVisionController@delete')->where('id', '[0-9]+');
                Route::get('getAllAccountVision/{idLanguage}', 'Api\v1\AccountVisionController@getAllAccountVision')->where('idLanguage', '[0-9]+');
                Route::get('getById/{id}', 'Api\v1\AccountVisionController@getById')->where('id', '[0-9]+');
                Route::post('update', 'Api\v1\AccountVisionController@update'); 
        });
                Route::put('', 'Api\v1\AccountVisionController@insert');
                Route::get('getLastId', 'Api\v1\AccountVisionController@getLastId');
                Route::get('getLastIdContoVisione', 'Api\v1\AccountVisionController@getLastIdContoVisione');
                Route::post('SearchAccountVision', 'Api\v1\AccountVisionController@SearchAccountVision');
                Route::post('SearchAccountVisionOpen', 'Api\v1\AccountVisionController@SearchAccountVisionOpen');
                Route::post('SearchAccountVisionClose', 'Api\v1\AccountVisionController@SearchAccountVisionClose');
        });

        #region viewquotes
        Route::group(['prefix' => 'viewquotes'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::delete('{id}', 'Api\v1\QuotesViewController@delete')->where('id', '[0-9]+');
            });

            Route::get('getByViewQuotes/{id}/{idUser}', 'Api\v1\QuotesViewController@getByViewQuotes')->where(['id', '[0-9]+', 'idUser' => '[0-9]+']);
            Route::post('updateInterventionUpdateViewQuotes', 'Api\v1\QuotesViewController@updateInterventionUpdateViewQuotes');
            Route::get('getAllViewQuote/{idLanguage}', 'Api\v1\QuotesViewController@getAllViewQuote')->where('idLanguage', '[0-9]+');
            Route::post('updateandSendEmailQuotes', 'Api\v1\QuotesViewController@updateandSendEmailQuotes');
            Route::post('SearchQuotes', 'Api\v1\QuotesViewController@SearchQuotes');
            Route::put('insertMessageQuotes', 'Api\v1\QuotesViewController@insertMessageQuotes');
            Route::put('insertReferent', 'Api\v1\QuotesViewController@insertReferent');
            
         //  Route::get('pdfviewquotes/{id}', array('as'=>'pdfviewquotes', 'uses'=>'GeneratePdfController@pdfviewquotes'))->where('id', '[0-9]+');
        });

        Route::group(['prefix' => 'processingview'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('getAllProcessingView/{idLanguage}', 'Api\v1\ProcessingViewController@getAllProcessingView')->where('idLanguage', '[0-9]+');
                Route::get('getAllProcessingSearch/{idLanguage}', 'Api\v1\ProcessingViewController@getAllProcessingSearch')->where('idLanguage', '[0-9]+');
                Route::get('getAllViewQuoteSearch/{idLanguage}', 'Api\v1\ProcessingViewController@getAllViewQuoteSearch')->where('idLanguage', '[0-9]+');
                Route::delete('{id}', 'Api\v1\ProcessingViewController@delete')->where('id', '[0-9]+');
            });
            Route::post('getSearchFdl', 'Api\v1\ProcessingViewController@getSearchFdl');
            Route::post('updateInterventionUpdateProcessingSheet', 'Api\v1\ProcessingViewController@updateInterventionUpdateProcessingSheet');
            Route::get('getDescriptionAndPriceProcessingSheet/{id}', 'Api\v1\ProcessingViewController@getDescriptionAndPriceProcessingSheet')->where('id', '[0-9]+');
            Route::get('getByViewValue/{id}', 'Api\v1\ProcessingViewController@getByViewValue')->where('id', '[0-9]+');
        });


        Route::group(['prefix' => 'paymentGr'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
            });
            Route::get('selectpaymentGr', 'Api\v1\PaymentGrController@selectpaymentGr');
        });

        Route::group(['prefix' => 'StatesQuotesGr'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
            });
            Route::get('selectStatesQuotesGr', 'Api\v1\StatesQuotesGrController@selectStatesQuotesGr');
        });

        #region UNIT MEASURE

        Route::group(['prefix' => 'UnitMeasure'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{idLanguage}', 'Api\v1\UnitMeasureController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\UnitMeasureController@getById')->where('id', '[0-9]+');
                Route::put('', 'Api\v1\UnitMeasureController@insert');
                Route::post('', 'Api\v1\UnitMeasureController@update');
                Route::delete('{id}', 'Api\v1\UnitMeasureController@delete')->where('id', '[0-9]+');
            });
            Route::get('select', 'Api\v1\UnitMeasureController@getSelect');
        });


        Route::group(['prefix' => 'list'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{idLanguage}', 'Api\v1\ListController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\ListController@getById')->where('id', '[0-9]+');
                Route::put('', 'Api\v1\ListController@insert');
                Route::post('', 'Api\v1\ListController@update');
                Route::delete('{id}', 'Api\v1\ListController@delete')->where('id', '[0-9]+');
            });
            Route::get('select', 'Api\v1\ListController@getSelect');
        });

        #endregion UNIT MEASURE


        #region VAT TYPE

        Route::group(['prefix' => 'vattype'], function () {
            Route::group(['middleware' => 'auth:api'], function () {

                Route::get('{id}', 'Api\v1\VatTypeController@getById')->where('id', '[0-9]+');
                Route::get('all', 'Api\v1\VatTypeController@getAll');
                Route::put('', 'Api\v1\VatTypeController@insert');
                Route::post('', 'Api\v1\VatTypeController@update');
                Route::delete('{id}', 'Api\v1\VatTypeController@delete')->where('id', '[0-9]+');
            });

            Route::get('select', 'Api\v1\VatTypeController@getSelect');
        });

        #endregion VAT TYPE

        #region People

        Route::group(['prefix' => 'people'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{idLanguage}', 'Api\v1\PeopleController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\PeopleController@getById')->where('id', '[0-9]+');
                Route::get('getByIdReferent/{id}', 'Api\v1\PeopleController@getByIdReferent')->where('id', '[0-9]+');
                Route::get('getAllReferent/{id}/{idFunctionality}', 'Api\v1\PeopleController@getAllReferent')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
                Route::get('getPeopleUser/{id}', 'Api\v1\PeopleController@getPeopleUser')->where('id', '[0-9]+');
                
                Route::post('', 'Api\v1\PeopleController@update');
                
                Route::delete('{id}', 'Api\v1\PeopleController@delete')->where('id', '[0-9]+');
                Route::put('clone/{id}/{idFunctionality}', 'Api\v1\PeopleController@clone')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            });
            Route::get('select', 'Api\v1\PeopleController@getSelect');
            Route::get('getSelectExternalReferent', 'Api\v1\PeopleController@getSelectExternalReferent');
            Route::get('getPeopleUserWeb/{id}', 'Api\v1\PeopleController@getPeopleUserWeb')->where('id', '[0-9]+');

            Route::get('getAllUteByUserSecurPoint/{id}', 'Api\v1\PeopleController@getAllUteByUserSecurPoint')->where('id', '[0-9]+'); 
            
            Route::put('', 'Api\v1\PeopleController@insert');
            Route::put('insertNoAuth', 'Api\v1\PeopleController@insertNoAuth');
            Route::post('insertUteByUserSecurPoint', 'Api\v1\PeopleController@insertUteByUserSecurPoint');
            Route::post('updateReferent', 'Api\v1\PeopleController@updateReferent');
            Route::post('updateReferentWeb', 'Api\v1\PeopleController@updateReferentWeb');
            Route::put('insertReferentWeb', 'Api\v1\PeopleController@insertReferentWeb');     
        });

        #endregion People

        #region places
        Route::group(['prefix' => 'places'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{idLanguage}', 'Api\v1\PlacesController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\PlacesController@getById')->where('id', '[0-9]+');
                Route::post('', 'Api\v1\PlacesController@update');
                Route::delete('{id}', 'Api\v1\PlacesController@delete')->where('id', '[0-9]+');
                Route::put('clone/{id}/{idFunctionality}', 'Api\v1\PlacesController@clone')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            });
                Route::get('select', 'Api\v1\PlacesController@getSelect');
                Route::put('', 'Api\v1\PlacesController@insert');
        });
        #endregion places

        #region bookingsettings
        Route::group(['prefix' => 'bookingsettings'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{idLanguage}', 'Api\v1\BookingSettingsController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\BookingSettingsController@getById')->where('id', '[0-9]+');
                Route::post('', 'Api\v1\BookingSettingsController@update');
                Route::delete('{id}', 'Api\v1\BookingSettingsController@delete')->where('id', '[0-9]+');
                Route::put('clone/{id}/{idFunctionality}', 'Api\v1\BookingSettingsController@clone')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            });
                Route::get('select', 'Api\v1\BookingSettingsController@getSelect');
                Route::put('', 'Api\v1\BookingSettingsController@insert');
        });
        #endregion bookingsettings

          #region employees
        Route::group(['prefix' => 'employees'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{idLanguage}', 'Api\v1\EmployeesController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\EmployeesController@getById')->where('id', '[0-9]+');
                Route::post('', 'Api\v1\EmployeesController@update');
                Route::delete('{id}', 'Api\v1\EmployeesController@delete')->where('id', '[0-9]+');
                Route::put('clone/{id}/{idFunctionality}', 'Api\v1\EmployeesController@clone')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            });
                Route::get('select', 'Api\v1\EmployeesController@getSelect');
                Route::put('', 'Api\v1\EmployeesController@insert');
        });
            #endregion employees

            #region appointments
        Route::group(['prefix' => 'appointments'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{idLanguage}', 'Api\v1\AppointmentsController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('getAllResultCalendar/{idLanguage}', 'Api\v1\AppointmentsController@getAllResultCalendar')->where('idLanguage', '[0-9]+');
                Route::post('getAllAppointmentsSearch', 'Api\v1\AppointmentsController@getAllAppointmentsSearch');
                Route::get('getByIdEvent/{id}', 'Api\v1\AppointmentsController@getByIdEvent')->where('id', '[0-9]+');
            });
                Route::get('select', 'Api\v1\AppointmentsController@getSelect');
        });
            #endregion appointments

                #region bookingstates
        Route::group(['prefix' => 'bookingstates'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
            });
                Route::get('select', 'Api\v1\BookingStatesController@getSelect');
        });
            #endregion bookingstates

            #region connections
        Route::group(['prefix' => 'connections'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{idLanguage}', 'Api\v1\ConnectionsController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\ConnectionsController@getById')->where('id', '[0-9]+');
                Route::post('', 'Api\v1\ConnectionsController@update');
                Route::delete('{id}', 'Api\v1\ConnectionsController@delete')->where('id', '[0-9]+');
                Route::put('clone/{id}/{idFunctionality}', 'Api\v1\ConnectionsController@clone')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            });
                Route::get('select', 'Api\v1\ConnectionsController@getSelect');
                Route::post('getByIdConnections', 'Api\v1\ConnectionsController@getByIdConnections');
                Route::post('getByEventResultForParameter', 'Api\v1\ConnectionsController@getByEventResultForParameter');
                Route::put('', 'Api\v1\ConnectionsController@insert');
                Route::put('insertPrenotazioneForCliBooking', 'Api\v1\ConnectionsController@insertPrenotazioneForCliBooking');
        });
            #endregion connections


        #region exceptions
        Route::group(['prefix' => 'exceptions'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{idLanguage}', 'Api\v1\ExceptionsController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\ExceptionsController@getById')->where('id', '[0-9]+');
                Route::post('', 'Api\v1\ExceptionsController@update');
                Route::delete('{id}', 'Api\v1\ExceptionsController@delete')->where('id', '[0-9]+');
                Route::put('clone/{id}/{idFunctionality}', 'Api\v1\ExceptionsController@clone')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            });
                Route::get('select', 'Api\v1\ExceptionsController@getSelect');
                Route::put('', 'Api\v1\ExceptionsController@insert');
        });
        #endregion exceptions

        #region bookingservices
        Route::group(['prefix' => 'bookingservices'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{idLanguage}', 'Api\v1\BookingServicesController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\BookingServicesController@getById')->where('id', '[0-9]+');
                Route::post('', 'Api\v1\BookingServicesController@update');
                Route::delete('{id}', 'Api\v1\BookingServicesController@delete')->where('id', '[0-9]+');
                Route::put('clone/{id}/{idFunctionality}', 'Api\v1\BookingServicesController@clone')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            });
                Route::get('select', 'Api\v1\BookingServicesController@getSelect');
                Route::put('', 'Api\v1\BookingServicesController@insert');
        });
        #endregion bookingservices

        
        Route::group(['prefix' => 'itemGr'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{idLanguage}', 'Api\v1\ItemGrController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\ItemGrController@getById')->where('id', '[0-9]+');
                Route::post('', 'Api\v1\ItemGrController@update');
                Route::delete('{id}', 'Api\v1\ItemGrController@delete')->where('id', '[0-9]+');
                Route::put('insertNoAuth', 'Api\v1\ItemGrController@insertNoAuth');
                Route::post('updateExploded', 'Api\v1\ItemGrController@updateExploded');
                Route::delete('deleteExploded/{id}', 'Api\v1\ItemGrController@deleteExploded')->where('id', '[0-9]+');
            });
            Route::get('select', 'Api\v1\ItemGrController@getSelect');
            Route::get('getProduct/{id}', 'Api\v1\ItemGrController@getProduct')->where('id', '[0-9]+');            
            Route::get('selectnameproduct', 'Api\v1\ItemGrController@selectnameproduct');
            Route::get('getSelectListArticle', 'Api\v1\ItemGrController@getSelectListArticle');
            Route::get('selectitemGr', 'Api\v1\ItemGrController@getselectitemGr');
            Route::get('getBarcodeGrForTypology/{idTipology}', 'Api\v1\ItemGrController@getBarcodeGrForTypology')->where('idTipology', '[0-9]+');
            Route::get('getBarcodeGrForPlantId/{idSelectplant_id}', 'Api\v1\ItemGrController@getBarcodeGrForPlantId')->where('idSelectplant_id', '[0-9]+');
            Route::get('getByCmp/{id}', 'Api\v1\ItemGrController@getByCmp')->where('id', '[0-9]+');            
        });



        Route::group(['prefix' => 'technicalviewgr'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('getAllProcessingTechnical/{idUser}', 'Api\v1\TechnicalViewController@getAllProcessingTechnical')->where('idUser', '[0-9]+'); 
                Route::get('getAllQuotesTechnical/{idUser}', 'Api\v1\TechnicalViewController@getAllQuotesTechnical')->where('idUser', '[0-9]+'); 
            });
        });


        #region SMS

         Route::group(['prefix' => 'sendSms'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::delete('{id}', 'Api\v1\sendSmsController@delete')->where('id', '[0-9]+');
            });
            Route::put('insertSms', 'Api\v1\sendSmsController@insertSms'); 
            Route::get('getnumbertelephone/{id}', 'Api\v1\sendSmsController@getnumbertelephone')->where('id', '[0-9]+');
            Route::get('getListContactByGroup/{id}', 'Api\v1\sendSmsController@getListContactByGroup')->where('id', '[0-9]+');
            Route::get('getListContactByUser/{id}', 'Api\v1\sendSmsController@getListContactByUser')->where('id', '[0-9]+');
            Route::get('all', 'Api\v1\sendSmsController@getAll');
            Route::get('getListTelephoneQueryGroup/{id}', 'Api\v1\sendSmsController@getListTelephoneQueryGroup')->where('id', '[0-9]+');
        });
        #endregion SMS

        #region GROUP SMS

        Route::group(['prefix' => 'groupSms'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::delete('{id}', 'Api\v1\groupSmsController@delete')->where('id', '[0-9]+');
            });
            Route::get('select', 'Api\v1\groupSmsController@getSelect');
            Route::put('insertGroupSms', 'Api\v1\groupSmsController@insertGroupSms');
            Route::post('updateGroupSms', 'Api\v1\groupSmsController@updateGroupSms'); 
            Route::get('all', 'Api\v1\groupSmsController@getAll');

        });
        #endregion GROUP SMS
        
                #region REGION
                Route::group(['prefix' => 'regions'], function () {
                    Route::group(['middleware' => ['auth:api']], function () {
                        Route::get('all/{idLanguage}', 'Api\v1\RegionController@getAll')->where('idLanguage', '[0-9]+');
                        Route::get('{id}', 'Api\v1\RegionController@getById')->where('id', '[0-9]+');
                        Route::put('', 'Api\v1\RegionController@insert');
                        Route::post('', 'Api\v1\RegionController@update');
                        Route::delete('{id}', 'Api\v1\RegionController@delete')->where('id', '[0-9]+');
                    });
                    Route::get('select', 'Api\v1\RegionController@getSelect');
                });
                #endregion REGION

                #region PROVINCE
                Route::group(['prefix' => 'provinces'], function () {
                    Route::group(['middleware' => ['auth:api']], function () {
                        Route::get('all/{idLanguage}', 'Api\v1\ProvinceController@getAll')->where('idLanguage', '[0-9]+');
                        Route::get('{id}', 'Api\v1\ProvinceController@getById')->where('id', '[0-9]+');
                        Route::put('', 'Api\v1\ProvinceController@insert');
                        Route::post('', 'Api\v1\ProvinceController@update');
                        Route::delete('{id}', 'Api\v1\ProvinceController@delete')->where('id', '[0-9]+');
                    });
                    Route::get('select', 'Api\v1\ProvinceController@getSelect');
                });
                #endregion PROVINCE


                #region CITIES
                Route::group(['prefix' => 'cities'], function () {
                    Route::group(['middleware' => ['auth:api']], function () {
                        Route::get('all/{idLanguage}', 'Api\v1\CitiesController@getAll')->where('idLanguage', '[0-9]+');
                        Route::get('{id}', 'Api\v1\CitiesController@getById')->where('id', '[0-9]+');
                        Route::put('', 'Api\v1\CitiesController@insert');
                        Route::post('', 'Api\v1\CitiesController@update');
                        Route::delete('{id}', 'Api\v1\CitiesController@delete')->where('id', '[0-9]+');
                    });
                    Route::get('select', 'Api\v1\CitiesController@getSelect');
                });
                #endregion CITIES

                #region ZONE
                Route::group(['prefix' => 'zone'], function () {
                    Route::group(['middleware' => ['auth:api']], function () {
                        Route::get('all/{idLanguage}', 'Api\v1\ZoneController@getAll')->where('idLanguage', '[0-9]+');
                        Route::get('{id}', 'Api\v1\ZoneController@getById')->where('id', '[0-9]+');
                        Route::put('', 'Api\v1\ZoneController@insert');
                        Route::post('', 'Api\v1\ZoneController@update');
                        Route::delete('{id}', 'Api\v1\ZoneController@delete')->where('id', '[0-9]+');
                    });
                    Route::get('select', 'Api\v1\ZoneController@getSelect');
                });
                #endregion ZONE

                Route::group(['prefix' => 'brand'], function () {
                    Route::group(['middleware' => 'auth:api'], function () {
                    });
                    Route::get('select', 'Api\v1\BrandController@getSelect');
                });

        Route::group(['prefix' => 'businessnamesupplier'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
            });
            Route::get('select', 'Api\v1\BusinessNameController@getSelect');
            Route::post('getAllSupplierRegistrySearch', 'Api\v1\BusinessNameController@getAllSupplierRegistrySearch');
            Route::get('getById/{id}', 'Api\v1\BusinessNameController@getById')->where('id', '[0-9]+');
            Route::get('getByIdUp/{id}', 'Api\v1\BusinessNameController@getByIdUp')->where('id', '[0-9]+');
            Route::post('updatedetailsupplierregistry', 'Api\v1\BusinessNameController@updatedetailsupplierregistry');
            Route::post('insertsupplierregistry', 'Api\v1\BusinessNameController@insertsupplierregistry');
            Route::delete('{id}', 'Api\v1\BusinessNameController@delete')->where('id', '[0-9]+');
        });

          Route::group(['prefix' => 'dealer'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
            });
            Route::get('select', 'Api\v1\DealerController@getSelect');
        });


        Route::group(['prefix' => 'tipology'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
            });
            Route::get('select', 'Api\v1\TipologyController@getSelect');
        });

        Route::group(['prefix' => 'plant'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
            });
            Route::get('select', 'Api\v1\PlantController@getSelect');
        });

        #region STATES
        Route::group(['prefix' => 'States'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{idLanguage}', 'Api\v1\StatesController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\StatesController@getById')->where('id', '[0-9]+');
                Route::put('', 'Api\v1\StatesController@insert');
                Route::post('', 'Api\v1\StatesController@update');
                Route::delete('{id}', 'Api\v1\StatesController@delete')->where('id', '[0-9]+');
            });
            Route::get('select', 'Api\v1\StatesController@getSelect');
            Route::post('updateNoAuth', 'Api\v1\StatesController@updateNoAuth');
            Route::put('insertNoAuth', 'Api\v1\StatesController@insertNoAuth');
        });
        #endregion STATES

        #region Roles
        Route::group(['prefix' => 'roles'], function () {
            Route::group(['middleware' => 'auth:api'], function () {


                Route::get('getAllWithoutAdmin', 'Api\v1\RolesController@getAllWithoutAdmin');
                Route::get('all', 'Api\v1\RolesController@getAll');
                Route::get('{id}', 'Api\v1\RolesController@getById')->where('id', '[0-9]+');
                Route::post('', 'Api\v1\RolesController@update');
                Route::delete('{id}', 'Api\v1\RolesController@delete')->where('id', '[0-9]+');
            });
            Route::get('select', 'Api\v1\RolesController@getSelect');
            Route::put('', 'Api\v1\RolesController@insert');
            Route::put('insertNoAuth', 'Api\v1\RolesController@insertNoAuth');
            Route::post('updateNoAuth', 'Api\v1\RolesController@updateNoAuth');
            Route::get('getByIdRoles/{id}', 'Api\v1\RolesController@getByIdRoles')->where('id', '[0-9]+');
            Route::get('allNoAuth', 'Api\v1\RolesController@getAllNoAuth');
        });
        #endregion Roles

        #region VIDEO
        Route::group(['prefix' => 'video', 'middleware' => 'auth:api'], function () {
            Route::post('', 'Api\v1\VideoController@add');
        });
        #endregion VIDEO

        #region PRODUCER
        Route::group(['prefix' => 'producer'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('', 'Api\v1\ProducerController@get');
                Route::get('all/{idLanguage}', 'Api\v1\ProducerController@getAll')->where('idLanguage', '[0-9]+');
                Route::get('{id}', 'Api\v1\ProducerController@getById')->where('id', '[0-9]+');
                Route::put('', 'Api\v1\ProducerController@insert');
                Route::post('', 'Api\v1\ProducerController@update');
                Route::delete('{id}', 'Api\v1\ProducerController@delete')->where('id', '[0-9]+');
                Route::put('clone/{id}/{idFunctionality}', 'Api\v1\ProducerController@clone')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
            });
            Route::get('select', 'Api\v1\ProducerController@getSelect');
            Route::get('selectUserShop', 'Api\v1\ProducerController@getSelectUserShop');
            Route::get('selectConditionUserShop', 'Api\v1\ProducerController@getSelectConditionUserShop');
            Route::get('selectCategoriesFilter', 'Api\v1\ProducerController@getSelectCategoriesFilter');
            Route::get('selectConditionCategoriesFilter', 'Api\v1\ProducerController@getSelectConditionCategoriesFilter');
        });

        #endregion PRODUCER

        Route::group(['prefix' => 'Amazon'], function () {
            Route::post('getHtml', 'Api\v1\AmazonController@getHtml');
        });


        #region FAULTMODULES
        Route::group(['prefix' => 'FaultModules', 'middleware' => 'auth:api'], function () {
            Route::get('all/{idLanguage}', 'Api\v1\FaultModulesController@getAll')->where('idLanguage', '[0-9]+');
            Route::get('select', 'Api\v1\FaultModulesController@getSelect');
            Route::get('{id}', 'Api\v1\FaultModulesController@getById')->where('id', '[0-9]+');
            Route::get('getLastId', 'Api\v1\FaultModulesController@getLastId');
            Route::put('', 'Api\v1\FaultModulesController@insert');
            Route::post('', 'Api\v1\FaultModulesController@update');
            Route::delete('{id}', 'Api\v1\FaultModulesController@delete')->where('id', '[0-9]+');
            Route::put('clone/{id}/{idFunctionality}', 'Api\v1\FaultModulesController@clone')->where(['id' => '[0-9]+', 'idFunctionality' => '[0-9]+']);
        });

        #region CART
        Route::group(['prefix' => 'Cart'], function () {
            Route::get('getById/{idCart}', 'Api\v1\CartController@getById')->where('idCart', '[0-9]+');
            Route::get('getAll', 'Api\v1\CartController@getAll');
            Route::get('getTotalAmount/{idCart}', 'Api\v1\CartController@getTotalAmount')->where('idCart', '[0-9]+');
            Route::get('getNrProductRows/{idCart}', 'Api\v1\CartController@getNrProductRows')->where('idCart', '[0-9]+');
            Route::get('getNrRows/{idCart}', 'Api\v1\CartController@getNrRows')->where('idCart', '[0-9]+');
            Route::get('getNoteByCartId/{idCart}', 'Api\v1\CartController@getNoteByCartId')->where('idCart', '[0-9]+');
            
            Route::put('', 'Api\v1\CartController@insert');
            Route::post('', 'Api\v1\CartController@update');
            Route::post('updatePaymentType', 'Api\v1\CartController@updatePaymentType');
            Route::post('updateNote', 'Api\v1\CartController@updateNote');
            Route::post('updateCarriage', 'Api\v1\CartController@updateCarriage');
            Route::post('updateCarrier', 'Api\v1\CartController@updateCarrier');
            Route::delete('{id}', 'Api\v1\CartController@delete')->where('id', '[0-9]+');
        });
        #endregion CART

        #region CART DETAIL
        Route::group(['prefix' => 'CartDetail'], function () {
            Route::get('getAllByCart/{idCart}', 'Api\v1\CartDetailController@getAllByCart')->where('idCart', '[0-9]+');
            Route::get('{idutente}', 'Api\v1\CartDetailController@getNumberCartItems')->where('idutente', '[0-9]+');
            Route::put('', 'Api\v1\CartDetailController@insert');
            Route::put('insertRequest', 'Api\v1\CartDetailController@insertRequest');
            Route::post('', 'Api\v1\CartDetailController@update');
            Route::delete('{id}', 'Api\v1\CartDetailController@delete')->where('id', '[0-9]+');
            Route::post('modifyQuantity', 'Api\v1\CartDetailController@modifyQuantity');
        });
        #endregion CART DETAIL

        #region COMMUNITY IMAGE

        Route::group(['prefix' => 'CommunityFavoriteImage'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                
            });
            Route::post('updateOrInsert', 'Api\v1\CommunityFavoriteImageController@updateOrInsert');
        });

        Route::group(['prefix' => 'CommunityImageComment'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                
            });
            Route::post('insert', 'Api\v1\CommunityImageCommentController@insert');
        });

        Route::group(['prefix' => 'CommunityAction'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                
            });
            Route::post('insert', 'Api\v1\CommunityActionController@insert');
        });

        Route::group(['prefix' => 'CommunityImage'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all', 'Api\v1\CommunityImageController@getAll');
                Route::get('getGallery/{type}', 'Api\v1\CommunityImageController@getGallery')->where('type', '[a-z]+');
                Route::post('', 'Api\v1\CommunityImageController@update');
                Route::delete('{id}', 'Api\v1\CommunityImageController@delete')->where('id', '[0-9]+');
                Route::get('{id}', 'Api\v1\CommunityImageController@getById')->where('id', '[0-9]+');
                Route::put('insertNewImages', 'Api\v1\CommunityImageController@insertNewImages');
                Route::post('updateOnlineCheckImages', 'Api\v1\CommunityImageController@updateOnlineCheckImages');
                Route::put('insertToWebP', 'Api\v1\CommunityImageController@insertToWebP');
            });
            Route::put('insert', 'Api\v1\CommunityImageController@insert');
            Route::get('countUserUpload/{id}', 'Api\v1\CommunityImageController@countUserUpload')->where('id', '[0-9]+');
            Route::post('getPreviousNextUrlByType', 'Api\v1\CommunityImageController@getPreviousNextUrlByType');
            Route::post('getGalleryPaginator', 'Api\v1\CommunityImageController@getGalleryPaginator');
            Route::post('deleteImage', 'Api\v1\CommunityImageController@deleteImage');
            Route::post('getAllSearch', 'Api\v1\CommunityImageController@getAllSearch');
        });

        Route::group(['prefix' => 'CommunityImageReportType', 'middleware' => 'auth:api'], function () {
            Route::get('all/{idLanguage}', 'Api\v1\CommunityImageReportTypeController@getAll')->where('idLanguage', '[0-9]+');
            Route::get('select', 'Api\v1\CommunityImageReportTypeController@getSelect');
            Route::put('', 'Api\v1\CommunityImageReportTypeController@insert');
            Route::post('', 'Api\v1\CommunityImageReportTypeController@update');
            Route::delete('{id}', 'Api\v1\CommunityImageReportTypeController@delete')->where('id', '[0-9]+');
            Route::get('{id}', 'Api\v1\CommunityImageReportTypeController@getById')->where('id', '[0-9]+');
        });

        Route::group(['prefix' => 'CommunityImageCategory'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all/{idLanguage}', 'Api\v1\CommunityImageCategoryController@getAll')->where('idLanguage', '[0-9]+');
                Route::put('', 'Api\v1\CommunityImageCategoryController@insert');
                Route::post('', 'Api\v1\CommunityImageCategoryController@update');
                Route::delete('{id}', 'Api\v1\CommunityImageCategoryController@delete')->where('id', '[0-9]+');
                Route::get('{id}', 'Api\v1\CommunityImageCategoryController@getById')->where('id', '[0-9]+');
                Route::get('getImages/{id}', 'Api\v1\CommunityImageController@getImages')->where('id', '[0-9]+');
                Route::post('updateImageName', 'Api\v1\CommunityImageController@updateImageName');


            });
            Route::get('select', 'Api\v1\CommunityImageCategoryController@getSelect');
        });

        /*Route::group(['prefix' => 'CommunityImage'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
                Route::get('all', 'Api\v1\CommunityImageController@getAll');
                Route::get('getGallery/{type}', 'Api\v1\CommunityImageController@getGallery')->where('type', '[a-z]+');
                Route::post('', 'Api\v1\CommunityImageController@update');
                Route::delete('{id}', 'Api\v1\CommunityImageController@delete')->where('id', '[0-9]+');
                Route::get('{id}', 'Api\v1\CommunityImageController@getById')->where('id', '[0-9]+');
            });
            Route::put('insert', 'Api\v1\CommunityImageController@insert');
            Route::get('countUserUpload/{id}', 'Api\v1\CommunityImageController@countUserUpload')->where('id', '[0-9]+');
        });*/
        #endregion COMMUNITY IMAGE

        

        Route::group(['prefix' => 'AdvertisingArea'], function () {
            Route::group(['middleware' => 'auth:api'], function () {
            Route::get('all', 'Api\v1\AdvertisingAreaController@getAll');
            Route::get('getByCode/{code}', 'Api\v1\AdvertisingAreaController@getByCode');
            Route::get('select', 'Api\v1\AdvertisingAreaController@getSelect');
            Route::put('', 'Api\v1\AdvertisingAreaController@insert');
            Route::post('', 'Api\v1\AdvertisingAreaController@update');
            Route::delete('{id}', 'Api\v1\AdvertisingAreaController@delete')->where('id', '[0-9]+');
            Route::get('{id}', 'Api\v1\AdvertisingAreaController@getById')->where('id', '[0-9]+');
        });
            Route::post('getHtmlBanner', 'Api\v1\AdvertisingAreaController@getHtmlBanner');
        });

        Route::group(['prefix' => 'AdvertisingBanner', 'middleware' => 'auth:api'], function () {
            Route::get('all', 'Api\v1\AdvertisingBannerController@getAll');
            Route::put('', 'Api\v1\AdvertisingBannerController@insert');
            Route::post('', 'Api\v1\AdvertisingBannerController@update');
            Route::delete('{id}', 'Api\v1\AdvertisingBannerController@delete')->where('id', '[0-9]+');
            Route::get('{id}', 'Api\v1\AdvertisingBannerController@getById')->where('id', '[0-9]+');
        });

        Route::group(['prefix' => 'AmazonTemplate', 'middleware' => 'auth:api'], function () {
            Route::get('all', 'Api\v1\AmazonTemplateController@getAll');
            Route::get('getByCode/{code}', 'Api\v1\AmazonTemplateController@getByCode');
            Route::get('select', 'Api\v1\AmazonTemplateController@getSelect');
            Route::put('', 'Api\v1\AmazonTemplateController@insert');
            Route::post('', 'Api\v1\AmazonTemplateController@update');
            Route::delete('{id}', 'Api\v1\AmazonTemplateController@delete')->where('id', '[0-9]+');
            Route::get('{id}', 'Api\v1\AmazonTemplateController@getById')->where('id', '[0-9]+');
        });

        #region SPECIAL FORMAGGI
        Route::group(['prefix' => 'specialformaggi'], function () {
            Route::get('importItems', 'Api\v1\SpecialFormaggiController@importItems');
            Route::get('importUsers', 'Api\v1\SpecialFormaggiController@importUsers');
            Route::get('importGroupAgenti', 'Api\v1\SpecialFormaggiController@importGroupAgenti');
            Route::get('importCanaliVendita', 'Api\v1\SpecialFormaggiController@importCanaliVendita');
            Route::get('importCategorieCliente', 'Api\v1\SpecialFormaggiController@importCategorieCliente');
            Route::get('importZoneCommerciali', 'Api\v1\SpecialFormaggiController@importZoneCommerciali');
        });
        #endregion SPECIAL FORMAGGI
    });
 
 
    Route::get('/storage/sitemap.xml', 'SitemapController@index');
    Route::get('/storage/sitemap.xml/blog', 'SitemapController@blog');
    Route::get('/storage/sitemap.xml/news', 'SitemapController@news');
    Route::get('/storage/sitemap.xml/pages', 'SitemapController@pages');
    Route::get('/storage/sitemap.xml/items', 'SitemapController@items');
    Route::get('/storage/sitemap.xml/pricesItems', 'SitemapController@pricesItems');
    Route::get('/storage/sitemap.xml/photoItems', 'SitemapController@photoItems');
    Route::get('/storage/sitemap.xml/categories', 'SitemapController@categories');
    Route::get('/storage/sitemap.xml/linkcategories', 'SitemapController@linkcategories');
    Route::get('/storage/sitemap.xml/users', 'SitemapController@users');
    Route::get('/storage/sitemap.xml/keywords', 'SitemapController@keywords');
    Route::get('/storage/sitemap.xml/compareItem/{page}', 'SitemapController@compareItem')->where('page', '[0-9]+');
    Route::get('/storage/sitemap.xml/communityImage/{page}', 'SitemapController@communityImage')->where('page', '[0-9]+');
    #endregion CATEGORIES

       
    #region GENERATE PDF
    Route::get('pdfview', array('as'=>'pdfview', 'uses'=>'GeneratePdfController@pdfview'));
    Route::get('pdfviewquotes/{id}', array('as'=>'pdfviewquotes', 'uses'=>'GeneratePdfController@pdfviewquotes'))->where('id', '[0-9]+');
    Route::get('pdfviewprocessingsheet/{id}', array('as'=>'pdfviewprocessingsheet', 'uses'=>'GeneratePdfController@pdfviewprocessingsheet'))->where('id', '[0-9]+');
    
    #endregion GENERATE PDF

    Route::post('feed', 'RssFeedController@feed');
    Route::get('frs', 'RssFeedController@feedRss');
    
    //Route::get('/rss', 'RssFeedController@feed');

    #endregion V1
});

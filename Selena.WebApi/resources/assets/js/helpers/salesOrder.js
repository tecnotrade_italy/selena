/***   salesOrderHelpers   ***/

export function getAllBySales(idToAppend){
spinnerHelpers.show()
  if(functionHelpers.isValued(storageData.sUserId())){
    salesOrderServices.getAllBySales(storageData.sUserId(),storageData.sIdLanguage(), function (result) {
      $('#' + idToAppend).html(result.message)
      $('.list-sale-sorder').on('click',function(){
        salesOrderHelpers.getByDetailSaleOrder($(this).attr('data-id-list-sales-order'), 'body-modal-list-view-order-sales')
        window.$('#Modal-View-Order').modal('show')
      })
      spinnerHelpers.hide()
    })
  }else{
    window.location.href = '/login'
  }
}

export function getByDetailSaleOrder(id,idToAppend){
  spinnerHelpers.show()
  if(functionHelpers.isValued(id)){
    salesOrderServices.getByDetailSaleOrder(id, storageData.sIdLanguage(), function (result) {
      $('#' + idToAppend).html(result.message)
      spinnerHelpers.hide()
    })
  }else{
    window.location.href = '/login'
  }
}

export function insertFileOrderSales(data) {
  spinnerHelpers.show()

  if (data.imgName == "" || data.imgName == null) {
    notificationHelpers.error("Attenzione!Non hai inserito il file pdf!")
    spinnerHelpers.hide()
  } else {
    if (data.imgName.includes('jpg') || data.imgName.includes('png') || data.imgName.includes('gif')) {
      notificationHelpers.error("Attenzione!Puoi solo inserire file con estensione .pdf!")
      spinnerHelpers.hide()
    } else { 
       salesOrderServices.insertFileOrderSales(data, function () {
      $("#upload-file-detail #created_id").val("")
      $("#upload-file-detail #imgName").val("")
      $("#upload-file-detail #sales_order_detail_id").val("")
      $("#upload-file-detail #created_id").val("")
      $("#upload-file-detail #imgBase64").val("")
      notificationHelpers.success("Inserimento avvenuto correttamente!")
      spinnerHelpers.hide()
    })
    }
  }
}
export function sendEmailDetailSummaryOrder(data) {
    spinnerHelpers.show()
  
        salesOrderServices.sendEmailDetailSummaryOrder(data, function () {
        notificationHelpers.success("Email inviata con successo!")
        spinnerHelpers.hide()
      });
 } 


export function getAllBySalesmanId(data){
  spinnerHelpers.show()
  if(functionHelpers.isValued(data.salesmanId)){
    salesOrderServices.getAllBySalesmanId(data, function (result) {
      $('#' + data.idToAppend).html(result.message);
      $('.list-sale-sorder').on('click',function(){
        salesOrderHelpers.getByDetailSaleOrder($(this).attr('data-id-list-sales-order'), 'body-modal-list-view-order-sales')
        window.$('#Modal-View-Order').modal('show')
      })
      spinnerHelpers.hide()
    })
  }else{
    window.location.href = '/login-agente'
  }
}

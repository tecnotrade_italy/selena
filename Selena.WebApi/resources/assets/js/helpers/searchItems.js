/** *   searchItemsHelpers   ***/
/** *   POST   ***/

export function searchImplemented(
  tipology,
  note,
  original_code,
  code_product,
  code_gr,
  plant,
  brand,
  business_name_supplier,
  code_supplier,
  price_purchase,
  price_list,
  repair_price,
  price_new,
  update_date,
  usual_supplier,
  pr_rip_conc
) {
  spinnerHelpers.show()
  const data = functionHelpers.formToJson(
    tipology,
    note,
    original_code,
    code_product,
    code_gr,
    plant,
    brand,
    business_name_supplier,
    code_supplier,
    price_purchase,
    price_list,
    repair_price,
    price_new,
    update_date,
    usual_supplier,
    pr_rip_conc
  )
  data.idLanguage = storageData.sIdLanguage()
  const html = ''
  searchItemsServices.searchImplemented(data, function (response) {
    for (let i = 0; i < response.data.length; i++) {
      let newList = ''
      newList += '<tr>'
      newList +=
        '<td><button class="btn btn-info" onclick="itemsGrHelpers.getById(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil"/></button>'
      newList +=
        '<button class="btn btn-danger" onclick="itemsGrHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash"/></button></td>'
      newList += '<td>' + response.data[i].id + '</td>'
      newList += '<td>' + response.data[i].tipology + '</td>'
      newList += '<td>' + response.data[i].note + '</td>'
      newList +=
        '<div class="container"><div id="lightgallery" style="display:flex">'
      newList +=
        "<td><a href='" +
        response.data[i].img +
        "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></td></div></div>"
      newList += '</td>'
      // newList += "<td><a href='" + response.data[i].img + "'</a>></td>";
      newList += '<td>' + response.data[i].original_code + '</td>'
      newList += '<td>' + response.data[i].code_product + '</td>'
      newList += '<td>' + response.data[i].code_gr + '</td>'
      newList += '<td>' + response.data[i].plant + '</td>'
      newList += '<td>' + response.data[i].brand + '</td>'
      newList += '<td>' + response.data[i].business_name_supplier + '</td>'
      newList += '<td>' + response.data[i].code_supplier + '</td>'
      newList += '<td>' + response.data[i].price_purchase + '</td>'
      newList += '<td>' + response.data[i].price_list + '</td>'
      newList += '<td>' + response.data[i].repair_price + '</td>'
      newList += '<td>' + response.data[i].price_new + '</td>'
      newList += '<td>' + response.data[i].update_date + '</td>'
      newList += '<td>' + response.data[i].usual_supplier + '</td>'
      newList += '<td>' + response.data[i].pr_rip_conc + '</td>'

      newList += '</tr>'

      $('#table_list_searchitems').show()
      $('#table_list_itemsGR').hide()
      $('#table_list_searchitems tbody').append(newList)
    }
    lightGallery(document.getElementById('lightgallery'))
  })

  spinnerHelpers.hide()
}

/** *   END POST   ***/

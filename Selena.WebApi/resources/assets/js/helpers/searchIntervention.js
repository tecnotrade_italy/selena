/** *   searchInterventionHelpers   ***/
/** *   POST   ***/

export function searchImplemented(
  create_quote,
  IdIntervention,
  nr_ddt,
  user_id,
  items
) {
  spinnerHelpers.show()
  const data = functionHelpers.formToJson(
    create_quote,
    IdIntervention,
    nr_ddt,
    user_id,
    items
  )
  data.idLanguage = storageData.sIdLanguage()
  const html = ''
  searchInterventionServices.searchImplemented(data, function (response) {
    $('#table_list_searchintervention tbody').html('')
    for (let i = 0; i < response.data.length; i++) {
      let newList = ''
      if (response.data[i].ready == true) {
        newList += "<tr style='background-color:#FFA500'>"
      } else {
        newList += "<tr style=''>"
      }

      if (
        response.data[i].nr_ddt_gr != null &&
        response.data[i].nr_ddt_gr != '-'
      ) {
        newList += "<tr style='background-color:#fff'>"
      }

      if (response.data[i].ready == true) {
        newList +=
          '<td><input type="checkbox"  id="form-control ready' +
          i +
          '" value="' +
          response.data[i].ready +
          '" name="ready' +
          i +
          '" class="switch-input' +
          i +
          '" checked="true' +
          i +
          '" onclick="interventionHelpers.updateCheck(' +
          response.data[i].id +
          ')"></td>'
      } else {
        newList +=
          '<td><input type="checkbox" id="form-control ready' +
          i +
          '" value="' +
          response.data[i].ready +
          '" name="ready' +
          i +
          '" class="switch-input' +
          i +
          '" onclick="interventionHelpers.updateCheck(' +
          response.data[i].id +
          ')"></td>'
      }

      newList +=
        '<td><button class="btn btn-info" onclick="interventionHelpers.getById(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil"/></button></td>'
      newList +=
        '<td><button class="btn btn-light" onclick="interventionHelpers.getByViewValue(' +
        response.data[i].id +
        ')"><i class="fa fa-circle-o"/></button></td>'
      newList +=
        '<td><button class="btn btn-dark" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-circle-o"/></button></td>'
      newList += '<td>' + response.data[i].id + '</td>'
      newList +=
        '<td><a style="cursor:pointer;" onclick="interventionHelpers.getByIdUser(' +
        response.data[i].user_id +
        ')">' +
        response.data[i].descriptionCustomer +
        '</a></td>'
      newList += '<td>' + response.data[i].date_ddt + '</td>'
      newList += '<td>' + response.data[i].nr_ddt + '</td>'
      // newList += "<td><a href='" + response.data[i].imgmodule + "'</a>Visualizza</td>";
      newList += '<td>' + response.data[i].description + '</td>'
      newList += '<td>' + response.data[i].defect
      if (response.data[i].imgmodule != '') {
        newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">'
        newList +=
          "<a href='" +
          response.data[i].imgmodule +
          "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>"
      }
      newList += '</td>'
      newList += '<td>' + response.data[i].codeGr + '</td>'
      newList += '<td>' + response.data[i].nr_ddt_gr + '</td>'
      newList += '<td>' + response.data[i].date_ddt_gr + '</td>'
      if (response.data[i].idInterventionExternalRepair == null) {
        newList +=
          '<td><button class="btn btn-green" style="color:#d7e7fe;font-size:38px;" onclick="interventionHelpers.getByViewExternalRepair(' +
          response.data[i].id +
          ')"><i class="fa fa-truck"/></button></td>'
      }
      if (response.data[i].idInterventionExternalRepair != null) {
        newList +=
          '<td><button class="btn btn-green" style="color:#02784d;font-size:38px;" onclick="interventionHelpers.getByViewExternalRepair(' +
          response.data[i].id +
          ')"><i class="fa fa-truck"/></button></td>'
      }
      if (
        response.data[i].ddt_supplier != null &&
        response.data[i].ddt_supplier != '-'
      ) {
        newList +=
          '<button class="btn btn-green" style="color:#02784d;font-size:38px;" onclick="interventionHelpers.getByViewExternalRepair(' +
          response.data[i].id +
          ')"><i class="fa fa-reply" aria-hidden="true"></i></button></td>'
      }
      newList += '<td>' + response.data[i].create_quote + '</td>'
      newList += '<td>' + response.data[i].descriptionStates + '</td>'
      newList += '<td>' + response.data[i].codeTracking + '</td>'
      newList +=
        '<td><button class="btn btn-danger" onclick="interventionHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash"/></button></td>'
      newList += '</tr>'

      $('#table_list_searchintervention').show()
      $('#table_list_searchintervention tbody').append(newList)
      $('#table_list_intervention').hide()
    }
  })
  spinnerHelpers.hide()
}

/** *   END POST   ***/

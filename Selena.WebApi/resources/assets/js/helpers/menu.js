/** *   menuHelpers   ***/

export function getIdByAdminUrl() {
  let id

  $.each(storageData.sMenu(), function () {
    if (this.url == pageHelpers.getUrl()) {
      id = this.id
      return true
    } else if (
      !functionHelpers.isValued(this.nextLevel) &&
      this.nextLevel.length > 0
    ) {
      $.each(this.nextLevel, function () {
        if (this.url == pageHelpers.getUrl()) {
          id = this.id
          return true
        }
      })

      if (id != 'undefined') {
        return true
      }
    }
  })

  return id
}

export function getIdFunctionByAdminUrl(routeParameter) {
  let idFunction

  $.each(storageData.sMenu(), function () {
    if (this.url == pageHelpers.getUrl(routeParameter)) {
      idFunction = this.idFunctionality
      return true
    } else if (
      functionHelpers.isValued(this.nextLevel) &&
      this.nextLevel.length > 0
    ) {
      $.each(this.nextLevel, function () {
        if (this.url == pageHelpers.getUrl(routeParameter)) {
          idFunction = this.idFunctionality
          return true
        }

        if (
          functionHelpers.isValued(this.nextLevel) &&
          this.nextLevel.length > 0
        ) {
          $.each(this.nextLevel, function () {
            if (this.url == pageHelpers.getUrl(routeParameter)) {
              idFunction = this.idFunctionality
              return true
            }
          })

          if (idFunction != 'undefined') {
            return true
          }
        }
      })

      if (idFunction != 'undefined') {
        return true
      }
    }
  })

  return idFunction
}

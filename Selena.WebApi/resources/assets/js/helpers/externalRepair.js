//  externalRepairHelpers

export function getAllexternalRepair() {
  spinnerHelpers.show()
  externalRepairServices.getAllexternalRepair(
    storageData.sIdLanguage(),
    function (response) {
      for (let i = 0; i < response.data.length; i++) {
      let newList = "";
        newList += '<tr>'
        newList +=
          '<td><button class="btn btn-info" onclick="externalRepairHelpers.getByRepair(' +
          response.data[i].id +
          ')"><i class="fa fa-pencil"/></button></td>'
        newList += '<td>' + response.data[i].id_intervention + '</td>'
        newList += '<td>' + response.data[i].date + '</td>'
        newList += '<td>' + response.data[i].ddt_exit + '</td>'
        newList += '<td>' + response.data[i].id_business_name_supplier + '</td>'
        newList += '<td>' + response.data[i].description + '</td>'
        newList += '<td>'
        if (response.data[i].imgmodule != '') {
          newList +=
            '<div class="container"><div id="lightgallery" style="display:flex">'
          newList +=
            "<a href='" +
            response.data[i].imgmodule +
            "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>"
        }
        newList += '</td>'
        newList += '<td>' + response.data[i].id_intervention + '</td>'
        newList += '<td>' + response.data[i].code_gr + '</td>'
        newList +=
          '<td>' +
          response.data[i].date_return_product_from_the_supplier +
          '</td>'
        newList += '<td>' + response.data[i].ddt_supplier + '</td>'
        newList += '<td>' + response.data[i].reference_supplier + '</td>'
        newList += '<td>' + response.data[i].notes_from_repairman + '</td>'
        newList +=
          '<td><button class="btn btn-danger" onclick="externalRepairHelpers.deleteRow(' +
          response.data[i].id +
          ')"><i class="fa fa-trash"/></button></td>'
        newList += '</tr>'
        $('#table_list_external_repair tbody').append(newList)
        $('#table_list_external_repair').show()
        $('#frmVieUpdatewRepair').hide()
      }
      lightGallery(document.getElementById('lightgallery'))
    }
  )

  spinnerHelpers.hide()
}

export function getByRepair(id) {
  externalRepairServices.getByRepair(id, function (response) {

    let searchParam = ''
    $.each(response.data, function (i, val) {
      if (val == true) {
        if (i == 'id') {
          $('#frmVieUpdatewRepair #' + i).html(val)
        } else {
          $('#frmVieUpdatewRepair #' + i).attr('checked', 'checked')
        }
      } else if (i == "imgName") {
          if (val != "") {
           $("#frmVieUpdatewRepair #imgPreview").attr('src', val);
            $("#frmVieUpdatewRepair #imgName").attr('src', val);
            $("#frmVieUpdatewRepair #imgNameLink").attr('href', val);
          } else {
             $("#frmVieUpdatewRepair #imgName").remove();
          }
        } else {
          $("#frmVieUpdatewRepair #" + i).val(val);
          $("#frmVieUpdatewRepair #" + i).html(val);
        }

      var selectFaultModule = response.data.selectFaultModule[0]
      $('#div-cont-module').html('')
      var html = ''
      html += '<h3 ' + '">Anteprima Modulo Guasti ' + '</h3>&nbsp;'
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-3">'
      html += '<label for="referent' + '">Referente ' + '</label>&nbsp;'
      html +=
        '<input type="text" class="form-control" name="referent" id="referent" placeholder="Referente" readonly value="' +
        selectFaultModule.referent +
        '">'
      html += '</div>'
      html += '<div class="col-3">'
      html += '<label for="nr_ddt' + '">Numero DDT ' + '</label>&nbsp;'
      html +=
        '<input type="text" class="form-control" name="nr_ddt" id="nr_ddt" placeholder="Numero DDT" readonly value="' +
        selectFaultModule.nr_ddt +
        '">'
      html += '</div>'
      html += '<div class="col-3">'
      html += '<label for="date_ddt' + '">Data DDT ' + '</label>&nbsp;'
      html +=
        '<input type="text" class="form-control" name="referent" id="date_ddt" placeholder="Data DDT" readonly value="' +
        selectFaultModule.date_ddt +
        '">'
      html += '</div>'
      html += '</div>'
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-3">'
      html +=
        '<label for="trolley_type' + '">Modello Carrello ' + '</label>&nbsp;'
      html +=
        '<input type="text" class="form-control" name="trolley_type" id="trolley_type" placeholder="Modello Carrello" readonly value="' +
        selectFaultModule.trolley_type +
        '">'
      html += '</div>'
      html += '<div class="col-3">'
      html += '<label for="series' + '">Matricola ' + '</label>&nbsp;'
      html +=
        '<input type="text" class="form-control" name="series" id="series" placeholder="Matricola" readonly value="' +
        selectFaultModule.series +
        '">'
      html += '</div>'
      html += '<div class="col-3">'
      html += '<label for="voltage' + '">Voltaggio ' + '</label>&nbsp;'
      html +=
        '<input type="text" class="form-control" name="voltage" id="voltage" placeholder="Voltaggio" readonly value="' +
        selectFaultModule.voltage +
        '">'
      html += '</div>'
      html += '<div class="col-3">'
      html += '<label for="plant_type' + '">Tipo Impianto ' + '</label>&nbsp;'
      html +=
        '<input type="text" class="form-control" name="plant_type" id="plant_type" placeholder="Tipo Impianto" readonly value="' +
        selectFaultModule.plant_type +
        '">'
      html += '</div>'
      html += '</div>'

      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-6">'
      html += '<label for="defect' + '">Difetto ' + '</label>&nbsp;'
      html +=
        '<textarea class="form-control" id="defect" readonly>' +
        selectFaultModule.defect +
        '</textarea>'
      html += '</div>'
      html += '<div class="col-6">'
      html += '<label for="exit_notes' + '">Note ' + '</label>&nbsp;'
      html +=
        '<textarea class="form-control" id="exit_notes" readonly>' +
        selectFaultModule.exit_notes +
        '</textarea>'
      html += '</div>'
      html += '</div>'
      $('#div-cont-module').append(html)

      var AggExternalRepair = response.data
      $('#div-cont-upd-external-repair').html('')
      var html = ''
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-4">'
      html += '<label for="id_intervention' + '">Numero ' + '</label>&nbsp;'
      html +=
        '<input type="text" class="form-control" name="id_intervention" id="id_intervention" placeholder="Numero" value="' +
        AggExternalRepair.id_intervention +
        '">'
      html += '</div>'
      html += '<div class="col-4">'
      html += '<label for="date' + '">Data ' + '</label>&nbsp;'
      html +=
        '<input type="text" class="form-control" name="date" id="date" placeholder="Data" value="' +
        AggExternalRepair.date +
        '">'
      html += '</div>'
      html += '<div class="col-4">'
      html += '<label for="ddt_exit' + '">DDT Uscita ' + '</label>&nbsp;'
      html +=
        '<input type="text" class="form-control" name="ddt_exit" id="ddt_exit" placeholder="DDT Uscita" value="' +
        AggExternalRepair.ddt_exit +
        '">'
      html += '</div>'
      html += '</div>'
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-6">'
      html +=
        '<label for="id_business_name_supplier' +
        '">Fornitore ' +
        '</label>&nbsp;'
      html +=
        '<select class="form-control select-graphic custom-select id_business_name_supplier" id="id_business_name_supplier' +
        '" value="' +
        AggExternalRepair.id_business_name_supplier +
        '" name="id_business_name_supplier' +
        '"></select>'
      html += '</div>'
      html += '</div>'
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-6">'
      html += '<label for="description' + '">Descrizione ' + '</label>&nbsp;'
      html +=
        '<input type="text" class="form-control" name="description" id="description" placeholder="Descrizione" value="' +
        AggExternalRepair.description +
        '">'
      html += '</div>'
      html += '</div>'
      html += '<div class="row">'
      html += '<div class="col-6">'
      html += '<label for="code_gr' + '">Codice GR ' + '</label>&nbsp;'
      html +=
        '<input type="text" class="form-control" name="code_gr" id="code_gr" placeholder="Codice GR" value="' +
        AggExternalRepair.code_gr +
        '">'
      html += '</div>'
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-6">'
      html +=
        '<input type="hidden" class="form-control" name="id" id="id" placeholder="id" value="' +
        AggExternalRepair.id +
        '">'
      html += '</div>'
      html += '</div>'
      $('#div-cont-upd-external-repair').append(html)

      var AggExternalRepair = response.data
      console.log(AggExternalRepair);
      $('#div-cont-upd-rientro-external-repair').html('')
      var html = ''
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-4">'
      html +=
        '<label for="date_return_product_from_the_supplier' +
        '">Data Entrata ' +
        '</label>&nbsp;'
      html +=
        '<input type="text" class="form-control" name="date_return_product_from_the_supplier" id="date_return_product_from_the_supplier" placeholder="Data Entrata" value="' +
        AggExternalRepair.date_return_product_from_the_supplier +
        '">'
      html += '</div>'
      html += '<div class="col-4">'
      html += '<label for="ddt_supplier' + '">DDT Fornitore ' + '</label>&nbsp;'
      html +=
        '<input type="text" class="form-control" name="ddt_supplier" id="ddt_supplier" placeholder="DDT Fornitore" value="' +
        AggExternalRepair.ddt_supplier +
        '">'
      html += '</div>'
      html += '<div class="col-4">'
      html +=
        '<label for="reference_supplier' + '">Rif Fornitore ' + '</label>&nbsp;'
      html +=
        '<input type="text" class="form-control" name="reference_supplier" id="reference_supplier" placeholder="Rif Fornitore" value="' +
        AggExternalRepair.reference_supplier +
        '">'
      html += '</div>'
      html += '</div>'
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-12">'
      html +=
        '<label for="notes_from_repairman' +
        '">Note Fornitore ' +
        '</label>&nbsp;'
      html +=
        '<textarea class="form-control" id="notes_from_repairman" placeholder="Note Fornitore">' +
        AggExternalRepair.notes_from_repairman +
        '</textarea>'

      html += '</div>'
      html += '</div>'
      $('#div-cont-upd-rientro-external-repair').append(html)

      var searchParam = ''
      let initials = []
      initials.push({
        id: AggExternalRepair.id_business_name_supplier,
        text: AggExternalRepair.Description_business_name_supplier,
      })
      $('#id_business_name_supplier').select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl +
            '/api/v1/businessnamesupplier/select',
          data(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
          },
        }
      });

      var searchParam = ''
      $('#id_business_name_supplier').select2({
        ajax: {
          url:
            configData.wsRootServicesUrl +
            '/api/v1/businessnamesupplier/select',
          data(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
          },
        },
      })
    })
    $('#table_list_external_repair').show()
    $('#frmVieUpdatewRepair').modal('show')
  })
}

export function updateRepairExternal() {
  spinnerHelpers.show()
  let data = {
    id: $('#frmVieUpdatewRepair #id').val(),
    id_intervention: $('#frmVieUpdatewRepair #id_intervention').val(),
    date: $('#frmVieUpdatewRepair #date').val(),
    ddt_exit: $('#frmVieUpdatewRepair #ddt_exit').val(),
    id_business_name_supplier: $(
      '#frmVieUpdatewRepair #id_business_name_supplier'
    ).val(),
    date_return_product_from_the_supplier: $(
      '#frmVieUpdatewRepair #date_return_product_from_the_supplier'
    ).val(),
    ddt_supplier: $('#frmVieUpdatewRepair #ddt_supplier').val(),
    reference_supplier: $('#frmVieUpdatewRepair #reference_supplier').val(),
    notes_from_repairman: $('#frmVieUpdatewRepair #notes_from_repairman').val(),
  }
  externalRepairServices.updateRepairExternal(data, function () {
    $('#frmVieUpdatewRepair #date').val('')
    $('#frmVieUpdatewRepair #ddt_exit').val('')
    $('#frmVieUpdatewRepair #id_business_name_supplier').val('')
    $('#frmVieUpdatewRepair #date_return_product_from_the_supplier').val('')
    $('#frmVieUpdatewRepair #ddt_supplier').val('')
    $('#frmVieUpdatewRepair #reference_supplier').val('')
    $('#frmVieUpdatewRepair #notes_from_repairman').val('')
    notificationHelpers.success('Modifica avvenuta con successo')
    spinnerHelpers.hide()
    $('#table_list_external_repair').show()
    $('#frmVieUpdatewRepair').hide()
  })
}

export function deleteRow(id) {
  if (confirm('Sei sicuro di voler eliminare la riparazione?')) {
    spinnerHelpers.show()
    externalRepairServices.del(id, function (response) {
      notificationHelpers.success(
        dictionaryHelpers.getDictionary('DeleteCompleted')
      )
      spinnerHelpers.hide()
      window.location.reload()
    })
  }
}

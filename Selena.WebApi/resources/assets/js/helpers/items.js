/** *   itemsHelpers   ***/

export function getListItems() {
  spinnerHelpers.show()
  itemServices.getListItems(storageData.sUserId(), function (response) {
    for (let i = 0; i < response.data.length; i++) {
      let newList = ''
      newList += '<tr>'
      newList +=
        '<td><a class="btn btn-info" onclick="itemsHelpers.getItemById(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil"></i></a>'
      newList +=
        '<a class="btn btn-danger" onclick="itemsHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash"></i></a></td>'
      newList += "<td><img src='" + response.data[i].img + "'></td>"
      newList += '<td>' + response.data[i].description + '</td>'
      newList += '<td>' + response.data[i].producer_id + '</td>'
      newList += '<td>€ ' + response.data[i].price + '</td>'
      newList += '</tr>'

      $('#table_list_announcement').show()
      $('#frmUpdateAds').hide()
      $('#table_list_announcement tbody').append(newList)
    }
  })

  spinnerHelpers.hide()
}

export function getItemsUserShop(data) {
  spinnerHelpers.show()

  itemServices.getItemsUserShop(data, function (response) {
    let html = ''
    let htmlPaginator = ''
    let minus10Page = 1
    let plus10Page = 1
    let minus1Page = 1
    let plus1Page = 1
    const pageSelected = parseInt(response.data.pageSelected)
    const maxPage = response.data.totalPages
    let msgSearch = 'tutto il catalogo'
    if (
      $('#tabCategoryUserCont .nav-item .active').attr('data-id-category') !=
      undefined
    ) {
      msgSearch = $('#tabCategoryUserCont .nav-item .active').text()
    }
    if (response.data.html != '') {
      $('#resultItem').html('')
      html +=
        '<div class="row"><div class="col-12"><h3>Stai cercando in: <b>' +
        msgSearch +
        '</b></h3></div></div>'
      html += '<div class="row">' + response.data.html + '</div>'
      $('#resultItem').html(html)

      /* funzione gestione paginatore */
      if (response.data.paginationActive) {
        htmlPaginator += "<ul class='pagination'>"
        /* controllo freccia -10 pagine */
        if (pageSelected - 10 <= 1) {
          minus10Page = 1
        } else {
          minus10Page = pageSelected - 10
        }

        htmlPaginator +=
          '<li class="page-item" data-page="' +
          minus10Page +
          '"><a><i class="fa fa-angle-double-left"></i></a></li>'
        /* end controllo freccia -10 pagine */
        /* controllo freccia -1 pagine */
        if (pageSelected - 1 <= 1) {
          minus1Page = 1
        } else {
          minus1Page = pageSelected - 1
        }

        htmlPaginator +=
          '<li class="page-item" data-page="' +
          minus1Page +
          '"><a><i class="fa fa-angle-left"></i></a></li>'
        /* end controllo freccia -1 pagine */

        /* controllo per pagine intermedie */
        if (maxPage > 4) {
          if (pageSelected > 1) {
            if (pageSelected + 2 > maxPage) {
              if (pageSelected == maxPage) {
                for (var j = pageSelected - 3; j <= maxPage; j++) {
                  if (pageSelected == j) {
                    htmlPaginator +=
                      '<li class="page-item active" data-page="' +
                      j +
                      '"><a>' +
                      j +
                      '</a></li>'
                  } else {
                    htmlPaginator +=
                      '<li class="page-item" data-page="' +
                      j +
                      '"><a>' +
                      j +
                      '</a></li>'
                  }
                }
              } else {
                for (var j = pageSelected - 2; j <= maxPage; j++) {
                  if (pageSelected == j) {
                    htmlPaginator +=
                      '<li class="page-item active" data-page="' +
                      j +
                      '"><a>' +
                      j +
                      '</a></li>'
                  } else {
                    htmlPaginator +=
                      '<li class="page-item" data-page="' +
                      j +
                      '"><a>' +
                      j +
                      '</a></li>'
                  }
                }
              }
            } else {
              for (var j = pageSelected - 1; j <= pageSelected + 2; j++) {
                if (pageSelected == j) {
                  htmlPaginator +=
                    '<li class="page-item active" data-page="' +
                    j +
                    '"><a>' +
                    j +
                    '</a></li>'
                } else {
                  htmlPaginator +=
                    '<li class="page-item" data-page="' +
                    j +
                    '"><a>' +
                    j +
                    '</a></li>'
                }
              }
            }
          } else {
            for (var j = 1; j <= 4; j++) {
              if (pageSelected == j) {
                htmlPaginator +=
                  '<li class="page-item active" data-page="' +
                  j +
                  '"><a>' +
                  j +
                  '</a></li>'
              } else {
                htmlPaginator +=
                  '<li class="page-item" data-page="' +
                  j +
                  '"><a>' +
                  j +
                  '</a></li>'
              }
            }
          }
        } else {
          for (var j = 1; j <= maxPage; j++) {
            if (pageSelected == j) {
              htmlPaginator +=
                '<li class="page-item active" data-page="' +
                j +
                '"><a>' +
                j +
                '</a></li>'
            } else {
              htmlPaginator +=
                '<li class="page-item" data-page="' +
                j +
                '"><a>' +
                j +
                '</a></li>'
            }
          }
        }
        /* end controllo per pagine intermedie */

        /* controllo freccia + 1 pagine */
        if (pageSelected + 1 >= maxPage) {
          plus1Page = maxPage
        } else {
          plus1Page = pageSelected + 1
        }

        htmlPaginator +=
          '<li class="page-item" data-page="' +
          plus1Page +
          '"><a><i class="fa fa-angle-right"></i></a></li>'
        /* end controllo freccia + 1 pagine */
        /* controllo freccia + 10 pagine */
        if (pageSelected + 10 >= maxPage) {
          plus10Page = maxPage
        } else {
          plus10Page = pageSelected + 10
        }

        htmlPaginator +=
          '<li class="page-item" data-page="' +
          plus10Page +
          '"><a><i class="fa fa-angle-double-right"></i></a></li>'
        /* end controllo freccia + 10 pagine */

        htmlPaginator += '</ul>'
        $('html,body').animate(
          { scrollTop: $('.mainPages').offset().top },
          'slow'
        )
        $('.paginationCont').html(htmlPaginator)
      } else {
        $('.paginationCont').html('')
      }
      $('.totItems').html(response.data.totItems)
      /* end funzione gestione paginatore */
      $('.pagination li').on('click', function () {
        const data = {
          url: window.location.pathname,
          description: $('#description').val(),
          priceFrom: $('#priceUserShopFrom').val(),
          priceTo: $('#priceUserShopTo').val(),
          condition: $('#conditionUserShop').val(),
          producer: $('#producerUserShop').val(),
          idCategory: $('#tabCategoryUserCont .nav-item .active').attr(
            'data-id-category'
          ),
          pageSelected: $(this).attr('data-page'),
        }
        itemsHelpers.getItemsUserShop(data)
      })
    } else {
      $('#resultItem').html('')
      $('#resultItem').html('Nessun articolo trovato')
    }
  })

  spinnerHelpers.hide()
}

export function deleteRow(id) {
  if (confirm("Sei sicuro di voler eliminare l'annuncio?")) {
    spinnerHelpers.show()
    itemServices.del(id, function (response) {
      notificationHelpers.success(
        dictionaryHelpers.getDictionary('DeleteCompleted')
      )
      setTimeout(function () {
        location.reload()
        spinnerHelpers.hide()
      }, 2000)
    })
  }
}

export function getItemsCategoriesFilter(data) {
  spinnerHelpers.show()

  itemServices.getItemsCategoriesFilter(data, function (response) {
    let html = ''
    let htmlPaginator = ''
    let minus10Page = 1
    let plus10Page = 1
    let minus1Page = 1
    let plus1Page = 1
    const pageSelected = parseInt(response.data.pageSelected)
    const maxPage = response.data.totalPages

    if (response.data.html != '') {
      $('#resultItem').html('')
      html += response.data.html
      $('#resultItem').html(html)

      /* funzione gestione paginatore */
      if (response.data.paginationActive) {
        htmlPaginator += "<ul class='pagination'>"
        /* controllo freccia -10 pagine */
        if (pageSelected - 10 <= 1) {
          minus10Page = 1
        } else {
          minus10Page = pageSelected - 10
        }

        htmlPaginator +=
          '<li class="page-item" data-page="' +
          minus10Page +
          '"><a><i class="fa fa-angle-double-left"></i></a></li>'
        /* end controllo freccia -10 pagine */
        /* controllo freccia -1 pagine */
        if (pageSelected - 1 <= 1) {
          minus1Page = 1
        } else {
          minus1Page = pageSelected - 1
        }

        htmlPaginator +=
          '<li class="page-item" data-page="' +
          minus1Page +
          '"><a><i class="fa fa-angle-left"></i></a></li>'
        /* end controllo freccia -1 pagine */

        /* controllo per pagine intermedie */
        if (maxPage > 4) {
          if (pageSelected > 1) {
            if (pageSelected + 2 > maxPage) {
              if (pageSelected == maxPage) {
                for (var j = pageSelected - 3; j <= maxPage; j++) {
                  if (pageSelected == j) {
                    htmlPaginator +=
                      '<li class="page-item active" data-page="' +
                      j +
                      '"><a>' +
                      j +
                      '</a></li>'
                  } else {
                    htmlPaginator +=
                      '<li class="page-item" data-page="' +
                      j +
                      '"><a>' +
                      j +
                      '</a></li>'
                  }
                }
              } else {
                for (var j = pageSelected - 2; j <= maxPage; j++) {
                  if (pageSelected == j) {
                    htmlPaginator +=
                      '<li class="page-item active" data-page="' +
                      j +
                      '"><a>' +
                      j +
                      '</a></li>'
                  } else {
                    htmlPaginator +=
                      '<li class="page-item" data-page="' +
                      j +
                      '"><a>' +
                      j +
                      '</a></li>'
                  }
                }
              }
            } else {
              for (var j = pageSelected - 1; j <= pageSelected + 2; j++) {
                if (pageSelected == j) {
                  htmlPaginator +=
                    '<li class="page-item active" data-page="' +
                    j +
                    '"><a>' +
                    j +
                    '</a></li>'
                } else {
                  htmlPaginator +=
                    '<li class="page-item" data-page="' +
                    j +
                    '"><a>' +
                    j +
                    '</a></li>'
                }
              }
            }
          } else {
            for (var j = 1; j <= 4; j++) {
              if (pageSelected == j) {
                htmlPaginator +=
                  '<li class="page-item active" data-page="' +
                  j +
                  '"><a>' +
                  j +
                  '</a></li>'
              } else {
                htmlPaginator +=
                  '<li class="page-item" data-page="' +
                  j +
                  '"><a>' +
                  j +
                  '</a></li>'
              }
            }
          }
        } else {
          for (var j = 1; j <= maxPage; j++) {
            if (pageSelected == j) {
              htmlPaginator +=
                '<li class="page-item active" data-page="' +
                j +
                '"><a>' +
                j +
                '</a></li>'
            } else {
              htmlPaginator +=
                '<li class="page-item" data-page="' +
                j +
                '"><a>' +
                j +
                '</a></li>'
            }
          }
        }
        /* end controllo per pagine intermedie */

        /* controllo freccia + 1 pagine */
        if (pageSelected + 1 >= maxPage) {
          plus1Page = maxPage
        } else {
          plus1Page = pageSelected + 1
        }

        htmlPaginator +=
          '<li class="page-item" data-page="' +
          plus1Page +
          '"><a><i class="fa fa-angle-right"></i></a></li>'
        /* end controllo freccia + 1 pagine */
        /* controllo freccia + 10 pagine */
        if (pageSelected + 10 >= maxPage) {
          plus10Page = maxPage
        } else {
          plus10Page = pageSelected + 10
        }

        htmlPaginator +=
          '<li class="page-item" data-page="' +
          plus10Page +
          '"><a><i class="fa fa-angle-double-right"></i></a></li>'
        /* end controllo freccia + 10 pagine */

        htmlPaginator += '</ul>'

        $('.paginationCont').html(htmlPaginator)
        $('html,body').animate(
          { scrollTop: $('.mainPages').offset().top },
          'slow'
        )
      } else {
        $('.paginationCont').html('')
      }
      $('.totItems').html(response.data.totItems)
      /* end funzione gestione paginatore */
      $('.pagination li').on('click', function () {
        const data = {
          url: window.location.pathname,
          priceFrom: $('#priceCategoriesFilterFrom').val(),
          priceTo: $('#priceCategoriesFilterTo').val(),
          condition: $('#conditionCategoriesFilter').val(),
          producer: $('#producerCategoriesFilter').val(),
          order: $('#orderCategoriesFilter').val(),
          description :$('#searchItemsCategory').val(),
          pageSelected: $(this).attr('data-page'),
        }
        itemsHelpers.getItemsCategoriesFilter(data)
      })
    } else {
      $('#resultItem').html('')
      $('#resultItem').html('Nessun articolo trovato')
    }
  })

  spinnerHelpers.hide()
}

export function getItemById(id) {
  // spinnerHelpers.show();
  itemServices.getItemById(id, function (response) {
    let searchParam = ''


    /* SELECT PRODUCER */
    const initials = []
    initials.push({
      id: response.data.producer_id,
      text: response.data.descriptionProducer,
    })
    $('#producer_id').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/producer/select',
        data(params) {
          if (params.term == '') {
            searchParam = '*'
          } else {
            searchParam = params.term
          }
          const query = {
            search: searchParam,
          }
          return query
        },
        processResults(data) {
          const dataParse = JSON.parse(data)

          return {
            results: dataParse.data,
          }
        },
      },
    })
    /* END SELECT PRODUCER */

    /* SELECT CATEGORIES FATHER */
    const initialsCategoryFather = []
    initialsCategoryFather.push({
      id: response.data.category_father_id,
      text: response.data.categoryFatherDescription,
    })
    $('#category_father_id').select2({
      data: initialsCategoryFather,
      ajax: {
        url:
          configData.wsRootServicesUrl +
          '/api/v1/Categories/selectPrincipalCategories',
        data(params) {
          if (params.term == '') {
            searchParam = '*'
          } else {
            searchParam = params.term
          }
          const query = {
            search: searchParam,
          }
          return query
        },
        processResults(data) {
          const dataParse = JSON.parse(data)

          return {
            results: dataParse.data,
          }
        },
      },
    })

    $('#category_father_id').on('change', function () {
      $('#category_id').val('')
      let searchParam = ''
      const catFather = $(this).val()
      $('#category_id').select2({
        ajax: {
          url:
            configData.wsRootServicesUrl +
            '/api/v1/Categories/selectSubCategories',
          data(params) {
            if (params.term == '') {
              searchParam = '*'
            } else {
              searchParam = params.term
            }
            const query = {
              search: searchParam,
              idCatFather: catFather,
            }
            return query
          },
          processResults(data) {
            const dataParse = JSON.parse(data)
            return {
              results: dataParse.data,
            }
          },
        },
      })
    })
    /* END SELECT CATEGORIES FATHER */

    /* SELECT CATEGORIES */
    const initialsCategory = []
    initialsCategory.push({
      id: response.data.category_id,
      text: response.data.categoryDescription,
    })
    $('#category_id').select2({
      data: initialsCategory,
      ajax: {
        url:
          configData.wsRootServicesUrl +
          '/api/v1/Categories/selectSubCategories',
        data(params) {
          if (params.term == '') {
            searchParam = '*'
          } else {
            searchParam = params.term
          }
          const query = {
            search: searchParam,
            idCatFather: response.data.category_father_id,
          }
          return query
        },
        processResults(data) {
          const dataParse = JSON.parse(data)
          return {
            results: dataParse.data,
          }
        },
      },
    })
    /* END SELECT CATEGORIES */

    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        $('#' + i).attr('checked', 'checked')
        // $("#" + i).val(val);
      } else if (i == 'img') {
        $('#img-preview').attr('src', val)
      } else {
        $('#' + i).val(val)
      }
    })

    let cont = 0
    const imgAggItem = response.data.imgAgg
    if (imgAggItem.length > 0) {
      for (let j = 0; j < imgAggItem.length; j++) {
        let html = ''
        if (cont < 10) {
          cont = cont + 1
          html +=
            '<div class="row" id="divcont-' +
            cont +
            '" data-id-img-agg="' +
            cont +
            '">'
          html += '<div class="col-8">'
          html +=
            '<label for="img' +
            cont +
            '">Immagine Aggiuntiva ' +
            cont +
            '</label>&nbsp;'
          html +=
            '<input type="hidden" id="imgBase64' +
            cont +
            '" name="imgBase64' +
            cont +
            '">'
          html +=
            '<input type="hidden" id="imgName' +
            cont +
            '" name="imgName' +
            cont +
            '" value="' +
            imgAggItem[j].img +
            '">'
          html +=
            '<input type="file" accept="image/jpeg, image/jpg, image/png, image/gif" id="img' +
            cont +
            '" class="imgAggInput" name="img' +
            cont +
            '">'
          html += '</div>'
          html += '<div class="col-2">'
          html +=
            '<img src="' + imgAggItem[j].img + '" style="max-width:50px;">'
          html += '</div>'
          html += '<div class="col-2">'
          html +=
            '<i class="fa fa-times del-img-agg" data-id-cont="' +
            cont +
            '"></i>'
          html += '</div>'
          html += '</div>'
          $('#div-cont-img-agg').append(html)
        }
      }
    }

    $('#frmUpdateAds .del-img-agg').on('click', function (e) {
      const idDel = $(this).attr('data-id-cont')
      const el = $(this)
        .parent('div')
        .parent('div')
        .attr('data-id-img-agg', idDel)

      cont = cont - 1
      if (cont < 0) {
        cont = 0
      }
      el.remove()
    })

    /* IMG AGG */
    $('#frmUpdateAds #add-img-agg').on('click', function (e) {
      let html = ''
      if (cont < 10) {
        cont = cont + 1
        html +=
          '<div class="row" id="divcont-' +
          cont +
          '" data-id-img-agg="' +
          cont +
          '">'
        html += '<div class="col-10">'
        html +=
          '<label for="img' +
          cont +
          '">Immagine Aggiuntiva ' +
          cont +
          '</label>&nbsp;'
        html +=
          '<input type="hidden" id="imgBase64' +
          cont +
          '" name="imgBase64' +
          cont +
          '">'
        html +=
          '<input type="hidden" id="imgName' +
          cont +
          '" name="imgName' +
          cont +
          '">'
        html +=
          '<input type="file" accept="image/jpeg, image/jpg, image/png, image/gif" id="img' +
          cont +
          '" class="imgAggInput" name="img' +
          cont +
          '">'
        html += '</div>'
        html += '<div class="col-2">'
        html +=
          '<i class="fa fa-times del-img-agg" data-id-cont="' + cont + '"></i>'
        html += '</div>'
        html += '</div>'
        $('#div-cont-img-agg').append(html)
      }

      $('#frmUpdateAds .imgAggInput').on('change', function (e) {
        const el = $(this).parent('div').parent('div').attr('data-id-img-agg')
        const reader = new FileReader()
        const files = e.target.files || e.dataTransfer.files
        const jsonImgAgg = {}
        if (!files.length) return
        reader.onload = (e) => {
          $('#imgName' + el).val(files[0].name)
          $('#imgBase64' + el).val(e.target.result)
        }
        reader.readAsDataURL(files[0])
      })

      $('#frmUpdateAds .del-img-agg').on('click', function (e) {
        const idDel = $(this).attr('data-id-cont')
        const el = $(this)
          .parent('div')
          .parent('div')
          .attr('data-id-img-agg', idDel)

        cont = cont - 1
        if (cont < 0) {
          cont = 0
        }
        el.remove()
      })
    })
    /* END IMG AGG */
  })

  $('#frmUpdateAds').show()
  $('#table_list_announcement').hide()
}

export function updateNoAuth() {
  spinnerHelpers.show()
  const data = {
    user_id: storageData.sUserId(),
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmUpdateAds #id').val(),
    external_link: $('#frmUpdateAds #external_link').val(),
    description: $('#frmUpdateAds #description').val(),
    meta_tag_characteristic: $('#frmUpdateAds #meta_tag_characteristic').val(),
    price: $('#frmUpdateAds #price').val().replace(',', '.'),
    available: $('#frmUpdateAds #available').val(),
    producer_id: $('#frmUpdateAds #producer_id').val(),
    category_id: $('#frmUpdateAds #category_id').val(),
    free_shipment: $('#frmUpdateAds #free_shipment').val(),
    new: $('#frmUpdateAds #new').val(),
    img: $('#frmUpdateAds #imgBase64').val(),
    imgName: $('#frmUpdateAds #imgName').val(),
  }

  const arrayImgAgg = []
  for (let i = 0; i < 10; i++) {
    if (functionHelpers.isValued($('#imgName' + i).val())) {
      arrayImgAgg.push(
        JSON.parse(
          '{"imgBase64' +
            i +
            '":"' +
            $('#imgBase64' + i).val() +
            '", "imgName' +
            i +
            '":"' +
            $('#imgName' + i).val() +
            '"}'
        )
      )
    }
  }
  data.imgAgg = arrayImgAgg

  itemServices.updateNoAuth(data, function () {
    notificationHelpers.success('Annuncio modificato correttamente!')
    setTimeout(function () {
      location.reload()
      spinnerHelpers.hide()
    }, 2000)
  })
}

export function addFavoritesItem(id) {
  if (storageData.sUserId() == null || storageData.sUserId() == '') {
    notificationHelpers.error(
      'Prima di inserire un articolo nei preferiti è necessario effettuare il login!'
    )
  } else {
    spinnerHelpers.show()
    const data = {
      idItem: id,
      user_id: storageData.sUserId(),
    }
    itemServices.addFavoritesItem(data, function (response) {
      if (response.data == '0') {
        // se articolo era già presente gestisco il fatto che è stato rimosso mostrando il cuore vuoto
        $("i[data-id-item-fav='" + id + "']").removeClass('fa-heart')
        $("i[data-id-item-fav='" + id + "']").addClass('fa-heart-o')
        notificationHelpers.success('Articolo rimosso dai preferiti!')
      } else {
        // altrimenti gestisco il fatto che è stato aggiunto ai preferiti
        $("i[data-id-item-fav='" + id + "']").addClass('fa-heart')
        $("i[data-id-item-fav='" + id + "']").removeClass('fa-heart-o')
        notificationHelpers.success('Articolo aggiunto ai preferiti!')
      }
      spinnerHelpers.hide()
    })
  }
}

/** *   salesmanHelpers   ***/

export function getAllUsersBySalesman(idToAppend) {
  spinnerHelpers.show()
  if (functionHelpers.isValued(storageData.sSalesmanId())) {
    var data = {
      id : storageData.sSalesmanId(),
      business_name: $('#search_business_name').val(),
      vat_number: $('#search_vat_number').val(),
      code: $('#search_code').val(),
      province: $('#search_province').val(),
    }
    salesmanServices.getAllUsersBySalesman(data, function (result) {
      $('#' + idToAppend).html(result.message)

      /* gestione indirizzi di spedizione */
      $('#' + idToAppend + ' .btnActiveUser').on('click', function () {
        const userId = $(this).attr('data-id')

        if (functionHelpers.isValued(userId)) {
          const data = {
            user_id: userId,
            salesman_id: storageData.sSalesmanId()
          }

          salesmanServices.activeUserById(data, function (result) {
            window.$('#modal-user').modal('hide');
            if (result.data != '') {
              storageData.setTokenKey('Bearer ' + result.data.original.access_token)
              storageData.setRefreshTokenKey(result.data.original.refresh_token)
              storageData.setExpiresTokenKey(result.data.original.expires_at)
              window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000)

              storageData.setUserName(result.data.original.username)
              storageData.setUserDescription(result.data.original.user_description)
              storageData.setUserId(result.data.original.id)
              storageData.setRole(result.data.original.role)

              //storageData.removeIdLanguage()
              window.localStorage.setItem('sIdLanguage', result.data.original.idLanguage)
              storageData.setCookie('sIdLanguage', result.data.original.idLanguage , 365);
              
              storageData.setCartId(result.data.original.cart_id)
              window.location.reload(true);
              notificationHelpers.success('Utente attivato correttamente!')
              
              
            } else {
              spinnerHelpers.hide()
              notificationHelpers.error('Ops, qualcosa è andato storto!')
            }
          })
        } else {
          notificationHelpers.error('Ops, qualcosa è andato storto!')
        }
      })

      /* end gestione indirizzi di spedizione */

      spinnerHelpers.hide()
    })
  } else {
    window.location.href = '/login-agente'
  }
}


export function getAllSearch() {
  spinnerHelpers.show();
	var data = {
	 	userId: storageData.sUserId(),
    image_category_id: $("#data-image-category-id").val(),
    search: $("#search").val(),
  }
  console.log(data)

  communityImageServices.getAllSearch(data, function (response) {
    console.log(response)
    $("#lightgallery").html(response.message)
  lightGallery(document.getElementById('lightgallery'))
})
  spinnerHelpers.hide()
}

/***   attachmentFileUserHelpers   ***/

  export function getAllByAttachmentUser(data){
    spinnerHelpers.show()
    if(functionHelpers.isValued(data.UserId)){
      itemAttachmentFileServices.getAllByAttachmentUser(data, function (result) {
        $('#' + data.idToAppend).html(result.message);

        spinnerHelpers.hide()
      });
    }else{
      window.location.href = '/login'
    }
  }



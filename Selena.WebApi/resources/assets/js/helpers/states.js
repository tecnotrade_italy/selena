/** *   statesHelpers   ***/
// statesServices

export function getAll() {
  spinnerHelpers.show()
  statesServices.getAll(storageData.sIdLanguage(), function (response) {
    for (let i = 0; i < response.data.length; i++) {
      let newList = ''
      newList += '<tr>'
      newList +=
        '<td><button class="btn btn-info" onclick="statesHelpers.getById(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil"/></button>'
      newList +=
        '<button class="btn btn-danger" onclick="statesHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash"/></button></td>'
      newList += '<td>' + response.data[i].id + '</td>'
      newList += '<td>' + response.data[i].description + '</td>'
      newList += '</tr>'

      $('#table_list_states').show()
      $('#frmUpdateStates').hide()
      $('#table_list_states tbody').append(newList)
    }
  })

  spinnerHelpers.hide()
}

export function getById(id) {
  // spinnerHelpers.show();
  statesServices.getById(id, function (response) {
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        $('#' + i).attr('checked', 'checked')
      } else {
        $('#' + i).val(val)
      }
    })
  })

  $('#frmUpdateStates').show()
  $('#table_list_states').hide()
  $('#id').css('display', 'none')
  $('#description').css('display', 'block')
}

export function updateNoAuth() {
  spinnerHelpers.show()
  const data = {
    // id: storageData.sUserId(),
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmUpdateStates #id').val(),
    description: $('#frmUpdateStates #description').val(),
  }
  statesServices.updateNoAuth(data, function () {
    $('#frmUpdateStates #id').val('')
    $('#frmUpdateStates #description').val('')

    notificationHelpers.success(
      dictionaryHelpers.getDictionary('ContactRequestCompleted')
    )
    spinnerHelpers.hide()
    $('#table_list_states').show()
    $('#frmUpdateStates').hide()
    window.location.reload()
  })
}

export function deleteRow(id) {
  if (confirm('Vuoi eliminare lo stato?')) {
    spinnerHelpers.show()
    statesServices.del(id, function (response) {
      notificationHelpers.success(
        dictionaryHelpers.getDictionary('DeleteCompleted')
      )
      spinnerHelpers.hide()
      window.location.reload()
    })
  }
}

export function insertNoAuth() {
  spinnerHelpers.show()

  const data = {
    created_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmAddStates #id').val(),
    description: $('#frmAddStates #description').val(),
  }

  statesServices.insertNoAuth(data, function () {
    $('#frmAddStates #id').val('')
    $('#frmAddStates #description').val('')
    notificationHelpers.success(
      dictionaryHelpers.getDictionary('ContactRequestCompleted')
    )
    spinnerHelpers.hide()
  })
}

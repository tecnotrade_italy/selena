/** *   personaldataHelpers   ***/

export function getUserData() {
  // spinnerHelpers.show();
  userServices.getUserData(storageData.sUserId(), function (response) {
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        $('#' + i).attr('checked', 'checked')
        // $("#" + i).val(val);
      } else if (i == 'img') {
        $('#img-preview').attr('src', val)
      } else {
        $('#' + i).val(val)
      }
    })

    var searchParam = ''
    var initials = []
    initials.push({
      id: response.data.province_id,
      text: response.data.Province_id,
    })
    window.$('#province_id').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/provinces/select',
        data(params) {
          if (params.term == '') {
            searchParam = '*'
          } else {
            searchParam = params.term
          }
          const query = {
            search: searchParam,
          }
          return query
        },
        processResults(data) {
          const dataParse = JSON.parse(data)
          return {
            results: dataParse.data,
          }
        },
      },
    })

    var searchParam = ''
    var initials = []
    initials.push({
      id: response.data.region_id,
      text: response.data.Region_id,
    })
    window.$('#region_id').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/regions/select',
        data(params) {
          if (params.term == '') {
            searchParam = '*'
          } else {
            searchParam = params.term
          }
          const query = {
            search: searchParam,
          }
          return query
        },
        processResults(data) {
          const dataParse = JSON.parse(data)
          return {
            results: dataParse.data,
          }
        },
      },
    })

    var searchParam = ''
    var initials = []
    initials.push({
      id: response.data.nation_id,
      text: response.data.Nation_id,
    })
    window.$('#nation_id').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/nation/select',
        data(params) {
          if (params.term == '') {
            searchParam = '*'
          } else {
            searchParam = params.term
          }
          const query = {
            search: searchParam,
          }
          return query
        },
        processResults(data) {
          const dataParse = JSON.parse(data)
          return {
            results: dataParse.data,
          }
        },
      },
    })

    // view result
    var cont = 0
    const imgAggItem = response.data.imgAgg
    if (imgAggItem !== undefined) {
      if (imgAggItem.length > 0) {
        for (var j = 0; j < imgAggItem.length; j++) {
          var html = ''
          if (cont < 10) {
            cont = cont + 1
            html +=
              '<div class="row" id="divcont-' +
              cont +
              '" data-id-img-agg="' +
              cont +
              '">'
            html += '<div class="col-8">'
            html +=
              '<label for="img' +
              cont +
              '">Immagine Aggiuntiva ' +
              cont +
              '</label>&nbsp;'
            html +=
              '<input type="hidden" id="imgBase64' +
              cont +
              '" name="imgBase64' +
              cont +
              '">'
            html +=
              '<input type="hidden" id="imgName' +
              cont +
              '" name="imgName' +
              cont +
              '" value="' +
              imgAggItem[j].img +
              '">'
            html +=
              '<input type="file" accept="image/jpeg, image/jpg, image/png, image/gif" id="img' +
              cont +
              '" class="imgAggInput" name="img' +
              cont +
              '">'
            html += '</div>'
            html += '<div class="col-2">'
            html +=
              '<img src="' + imgAggItem[j].img + '" style="max-width:50px;">'
            html += '</div>'
            html += '<div class="col-2">'
            html +=
              '<i class="fa fa-times del-img-agg" data-id-cont="' +
              cont +
              '"></i>'
            html += '</div>'
            html += '</div>'
            $('#div-cont-img-agg').append(html)
          }
        }
      }
    }

    $('#frmUpdateDate .del-img-agg').on('click', function (e) {
      const idDel = $(this).attr('data-id-cont')
      const el = $(this)
        .parent('div')
        .parent('div')
        .attr('data-id-img-agg', idDel)

      cont = cont - 1
      if (cont < 0) {
        cont = 0
      }
      el.remove()
    })
    // end view result

    /* IMG AGG */
    $('#frmUpdateDate #add-img-agg').on('click', function (e) {
      let html = ''
      if (cont < 10) {
        cont = cont + 1
        html +=
          '<div class="row" id="divcont-' +
          cont +
          '" data-id-img-agg="' +
          cont +
          '">'
        html += '<div class="col-10">'
        html +=
          '<label for="img' +
          cont +
          '">Immagine Aggiuntiva ' +
          cont +
          '</label>&nbsp;'
        html +=
          '<input type="hidden" id="imgBase64' +
          cont +
          '" name="imgBase64' +
          cont +
          '">'
        html +=
          '<input type="hidden" id="imgName' +
          cont +
          '" name="imgName' +
          cont +
          '">'
        html +=
          '<input type="file" accept="image/jpeg, image/jpg, image/png, image/gif" id="img' +
          cont +
          '" class="imgAggInput" name="img' +
          cont +
          '">'
        html += '</div>'
        html += '<div class="col-2">'
        html +=
          '<i class="fa fa-times del-img-agg" data-id-cont="' + cont + '"></i>'
        html += '</div>'
        html += '</div>'
        $('#div-cont-img-agg').append(html)
      }

      $('#frmUpdateDate .imgAggInput').on('change', function (e) {
        const el = $(this).parent('div').parent('div').attr('data-id-img-agg')
        const reader = new FileReader()
        const files = e.target.files || e.dataTransfer.files
        const jsonImgAgg = {}
        if (!files.length) return
        reader.onload = (e) => {
          $('#imgName' + el).val(files[0].name)
          $('#imgBase64' + el).val(e.target.result)
        }
        reader.readAsDataURL(files[0])
      })

      $('#frmUpdateDate .del-img-agg').on('click', function (e) {
        const idDel = $(this).attr('data-id-cont')
        const el = $(this)
          .parent('div')
          .parent('div')
          .attr('data-id-img-agg', idDel)

        cont = cont - 1
        if (cont < 0) {
          cont = 0
        }
        el.remove()
      })
    })
    /* END IMG AGG */

    // view result MAPS
    var cont = 0
    const AggMaps = response.data.AggMaps
    if (AggMaps !== undefined) {
      if (AggMaps.length > 0) {
        for (var j = 0; j < AggMaps.length; j++) {
          var html = ''
          if (cont < 10) {
            cont = cont + 1
            html +=
              '<div class="row" id="divcontmaps-' +
              cont +
              '" data-id-maps-agg="' +
              cont +
              '">'
            html += '<div class="col-5">'
            html += '<div class="form-group">'
            html +=
              '<label for="maps_map' +
              cont +
              '">Mappe ' +
              cont +
              '</label>&nbsp;'
            html +=
              '<textarea class="form-control" id="maps_map' +
              cont +
              '" name="maps_map' +
              cont +
              '">' +
              AggMaps[j].maps_map +
              '</textarea>'
            html += '</div>'
            html += '</div>'
            html += '<div class="col-3">'
            html += '<div>'
            html += '<div class="form-group">'
            html +=
              '<label for="address_maps' +
              cont +
              '">Indirizzo ' +
              cont +
              '</label>&nbsp;'
            html +=
              '<input type="text" class="form-control" id="address_maps' +
              cont +
              '" name="address_maps' +
              cont +
              '" value="' +
              AggMaps[j].address_maps +
              '">'
            html += '</div>'
            html += '<div class="form-group">'
            html +=
              '<label for="codepostal_maps' +
              cont +
              '">Codice Postale ' +
              cont +
              '</label>&nbsp;'
            html +=
              '<input type="text" class="form-control" id="codepostal_maps' +
              cont +
              '" name="codepostal_maps' +
              cont +
              '" value="' +
              AggMaps[j].codepostal_maps +
              '">'
            html += '</div>'
            html += '</div>'
            html += '</div>'
            html += '<div class="col-3">'
            html += '<div>'
            html += '<div class="form-group">'
            html +=
              '<label for="country_maps' +
              cont +
              '">Località ' +
              cont +
              '</label>&nbsp;'
            html +=
              '<input type="text" class="form-control" id="country_maps' +
              cont +
              '" name="country_maps' +
              cont +
              '" value="' +
              AggMaps[j].country_maps +
              '">'
            html += '</div>'
            html += '<div class="form-group">'
            html +=
              '<label for="province_maps' +
              cont +
              '">Provincia ' +
              cont +
              '</label>&nbsp;'
            html +=
              '<input type="text" class="form-control" id="province_maps' +
              cont +
              '" name="province_maps' +
              cont +
              '" value="' +
              AggMaps[j].province_maps +
              '">'
            html += '</div>'
            html += '</div>'
            html += '</div>'
            html += '<div class="col-1">'
            html += '<div class="form-group">'
            html +=
              '<i class="fa fa-times del-maps-agg" data-id-cont="' +
              cont +
              '"></i>'
            html += '</div>'
            html += '</div>'
            html += '<div class="form-group">'
            html +=
              '<input type="hidden" class="form-control" id="id_map' +
              cont +
              '" name="id_map' +
              cont +
              '" value="' +
              AggMaps[j].id_map +
              '">'
            html += '</div>'
            html += '</div>'
            $('#div-cont-maps-agg').append(html)
          }
        }
      }
    }

    $('#frmUpdateDate .del-img-agg').on('click', function (e) {
      const idDel = $(this).attr('data-id-cont')
      const el = $(this)
        .parent('div')
        .parent('div')
        .attr('data-id-img-agg', idDel)

      cont = cont - 1
      if (cont < 0) {
        cont = 0
      }
      el.remove()
    })
    // end view result MAPS

    /* AGG MAPS */
    $('#frmUpdateDate #add-img-maps').on('click', function (e) {
      let html = ''
      if (cont < 10) {
        cont = cont + 1
        html +=
          '<div class="row" id="divcontmaps-' +
          cont +
          '" data-id-maps-agg="' +
          cont +
          '">'
        html += '<div class="col-5">'
        html += '<div class="form-group">'
        html +=
          '<label for="maps_map' + cont + '">Mappe ' + cont + '</label>&nbsp;'
        html +=
          '<textarea class="form-control" id="maps_map' +
          cont +
          '" name="maps_map' +
          cont +
          '"></textarea>'
        html += '</div>'
        html += '</div>'
        html += '<div class="col-3">'
        html += '<div>'
        html += '<div class="form-group">'
        html +=
          '<label for="address_maps' +
          cont +
          '">Indirizzo ' +
          cont +
          '</label>&nbsp;'
        html +=
          '<input type="text" class="form-control" id="address_maps' +
          cont +
          '" name="address_maps' +
          cont +
          '">'
        html += '</div>'
        html += '<div class="form-group">'
        html +=
          '<label for="codepostal_maps' +
          cont +
          '">Codice Postale ' +
          cont +
          '</label>&nbsp;'
        html +=
          '<input type="text" class="form-control" id="codepostal_maps' +
          cont +
          '" name="codepostal_maps' +
          cont +
          '">'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '<div class="col-3">'
        html += '<div>'
        html += '<div class="form-group">'
        html +=
          '<label for="country_maps' +
          cont +
          '">Località ' +
          cont +
          '</label>&nbsp;'
        html +=
          '<input type="text" class="form-control" id="country_maps' +
          cont +
          '" name="country_maps' +
          cont +
          '">'
        html += '</div>'
        html += '<div class="form-group">'
        html +=
          '<label for="province_maps' +
          cont +
          '">Provincia ' +
          cont +
          '</label>&nbsp;'
        html +=
          '<input type="text" class="form-control" id="province_maps' +
          cont +
          '" name="province_maps' +
          cont +
          '">'
        html += '</div>'
        html += '<div class="form-group">'
        html +=
          '<input type="hidden" class="form-control" id="id_map' +
          cont +
          '" name="id_map' +
          cont +
          '">'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '<div class="col-1">'
        html += '<div class="form-group">'
        html +=
          '<i class="fa fa-times del-maps-agg" data-id-cont="' + cont + '"></i>'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        $('#div-cont-maps-agg').append(html)
      }

      $('#frmUpdateDate .del-maps-agg').on('click', function (e) {
        const idDel = $(this).attr('data-id-cont')
        const el = $(this)
          .parent('div')
          .parent('div')
          .attr('data-id-maps-agg', idDel)

        cont = cont - 1
        if (cont < 0) {
          cont = 0
        }
        el.remove()
      })
    })
    /* END MAPS */
  })
}

export function update() {
  spinnerHelpers.show()

  const data = {
    id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    email: $('#frmUpdateDate #email').val(),
    business_name: $('#frmUpdateDate #business_name').val(),
    name: $('#frmUpdateDate #name').val(),
    surname: $('#frmUpdateDate #surname').val(),
    vat_number: $('#frmUpdateDate #vat_number').val(),
    fiscal_code: $('#frmUpdateDate #fiscal_code').val(),
    address: $('#frmUpdateDate #address').val(),
    postal_code: $('#frmUpdateDate #postal_code').val(),
    telephone_number: $('#frmUpdateDate #telephone_number').val(),
    website: $('#frmUpdateDate #website').val(),
    vat_type_id: $('#frmUpdateDate #vat_type_id').val(),
    country: $('#frmUpdateDate #country').val(),
    province: $('#frmUpdateDate #province').val(),
    description: $('#frmUpdateDate #description').val(),
    accepted_payments: $('#frmUpdateDate #accepted_payments').val(),
    terms_of_sale: $('#frmUpdateDate #terms_of_sale').val(),
    shop_time: $('#frmUpdateDate #shop_time').val(),
    maps: $('#frmUpdateDate #maps').val(),
    img: $('#frmUpdateDate #imgBase64').val(),
    monday: $('#frmUpdateDate #monday').val(),
    tuesday: $('#frmUpdateDate #tuesday').val(),
    wednesday: $('#frmUpdateDate #wednesday').val(),
    thursday: $('#frmUpdateDate #thursday').val(),
    friday: $('#frmUpdateDate #friday').val(),
    saturday: $('#frmUpdateDate #saturday').val(),
    sunday: $('#frmUpdateDate #sunday').val(),
    province_id: $('#frmUpdateDate #province_id').val(),
    nation_id: $('#frmUpdateDate #nation_id').val(),
    region_id: $('#frmUpdateDate #region_id').val(),
  }

  const arrayImgAgg = []
  for (var i = 0; i < 10; i++) {
    if (functionHelpers.isValued($('#imgName' + i).val())) {
      arrayImgAgg.push(
        JSON.parse(
          '{"imgBase64' +
            i +
            '":"' +
            $('#imgBase64' + i).val() +
            '", "imgName' +
            i +
            '":"' +
            $('#imgName' + i).val() +
            '"}'
        )
      )
    }
  }
  data.imgAgg = arrayImgAgg

  // SEZIONE MAPS
  const arrayMapAgg = []
  for (var i = 0; i < 10; i++) {
    const jsonMapAgg = {}
    let check = false
    if (functionHelpers.isValued($('#id_map' + i).val())) {
      check = true
      jsonMapAgg.id_map = $('#id_map' + i).val()
    }
    if (functionHelpers.isValued($('#maps_map' + i).val())) {
      check = true
      jsonMapAgg.maps_map = $('#maps_map' + i).val()
    }
    if (functionHelpers.isValued($('#address_maps' + i).val())) {
      check = true
      jsonMapAgg.address_maps = $('#address_maps' + i).val()
    }
    if (functionHelpers.isValued($('#codepostal_maps' + i).val())) {
      check = true
      jsonMapAgg.codepostal_maps = $('#codepostal_maps' + i).val()
    }
    if (functionHelpers.isValued($('#country_maps' + i).val())) {
      check = true
      jsonMapAgg.country_maps = $('#country_maps' + i).val()
    }
    if (functionHelpers.isValued($('#province_maps' + i).val())) {
      check = true
      jsonMapAgg.province_maps = $('#province_maps' + i).val()
    }
    if (check) {
      arrayMapAgg.push(jsonMapAgg)
    }
  }
  data.AggMaps = arrayMapAgg

  userServices.updateNoAuth(data, function () {
    notificationHelpers.success('Modifica dati effettuata con successo!')
    setTimeout(function () {
      location.reload()
    }, 2000)
    spinnerHelpers.hide()
  })
}

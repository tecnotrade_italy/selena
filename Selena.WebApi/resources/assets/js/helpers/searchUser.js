/** *   searchUserHelpers   ***/
/** *   POST   ***/

export function searchImplemented(
  name,
  business_name,
  email,
  country,
  province,
  telephone_number
) {
  spinnerHelpers.show()
  const data = functionHelpers.formToJson(
    name,
    business_name,
    email,
    country,
    province,
    telephone_number
  )
  data.idLanguage = storageData.sIdLanguage()
  const html = ''
  searchUserServices.searchImplemented(data, function (response) {
    $('#table_list_searchuser tbody').html('')
    for (let i = 0; i < response.data.length; i++) {
      var newList = ''
      newList += '<tr>'
      var newList = ''
      newList += '<tr>'
      newList +=
        '<td><button class="btn btn-info" onclick="userHelpers.getByIdResult(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil"/></button></td>'
      newList += '<td>' + response.data[i].id + '</td>'
      newList += '<td>' + response.data[i].subscriber + '</td>'
      newList += '<td>' + response.data[i].expiration_date + '</td>'
      newList += '<td>' + response.data[i].subscription_date + '</td>'
      newList += '<td>' + response.data[i].business_name + '</td>'
      newList += '<td>' + response.data[i].name + '</td>'
      newList += '<td>' + response.data[i].telephone_number + '</td>'
      newList += '<td>' + response.data[i].mobile_number + '</td>'
      newList += '<td>' + response.data[i].fax_number + '</td>'
      newList += '<td>' + response.data[i].email + '</td>'
      newList +=
        '<td><button class="btn btn-danger" onclick="userHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash"/></button></td>'
      newList += '</tr>'

      $('#table_list_searchuser').show()
      $('#table_list_user').hide()
      $('#table_list_searchuser tbody').append(newList)
      // $("#table_list_intervention").hide();
    }
  })

  spinnerHelpers.hide()
}

/** *   END POST   ***/

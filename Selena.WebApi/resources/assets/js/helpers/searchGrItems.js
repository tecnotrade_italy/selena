/***   searchGrItemsHelpers   ***/
/***   POST   ***/

export function searchImplementedItemsGr(id,brand,tipology,code_gr) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(id,brand,tipology,code_gr);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  searchItemsGrServices.searchImplementedItemsGr(data, function (response) {
    $("#table_list_searchitemsGrlist tbody").html('');
    if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_searchitemsGrlist").show();
      $("#table_list_searchitemsGrlist tbody").append(newList);
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        newList += "<tr>";
        newList += "<td><a href='/dettaglio-articolo?id=" + response.data[i].id + "'><img style='width: 150px;max-width: 150px;' src = '" + response.data[i].imageitems + "'></a></td> ";
        newList += "<td>" + response.data[i].tipology + "</td>";
        // newList += "<td>" + response.data[i].plant + "</td>";
        newList += "<td>" + response.data[i].original_code + "</td>";
        newList += "<td>" + response.data[i].brand + "</td>";
        // newList += "<td>" + response.data[i].code_product + "</td>";
        newList += "<td>" + response.data[i].code_gr + "</td>";
        newList += "<td><a href='" + response.data[i].img + "'><i class='fa fa-file-pdf' class='fr-fic fr-dii' style = 'color:#efb70d;'></i></a></td></div></div>";
        newList += "</td>";
        newList += "</tr>";
        $("#table_list_searchitemsGrlist").show();
        $("#table_list_searchitemsGrlist tbody").append(newList);
        //$("#frmSearchItems").hide();
      }
    }
    lightGallery(document.getElementById('lightgallery'));
    });
    
    spinnerHelpers.hide(); 
}
    


export function getResultDetail(id) {
  searchItemsGrServices.getResultDetail(id, function (response) {
    var searchParam = "";
    jQuery.each(response.data, function (i, val) {
      
      if (val == true) {
        if (i == "id") {
          $("#frmViewDetailItems #" + i).html(val);
        } else {
          $('#frmViewDetailItems #' + i).attr('checked', 'checked');
        }
      } else {
        if (i == "imageitems") {
          if (val != "") {
            $("#frmViewDetailItems #imgPreview").attr('src', val);
              $("#frmViewDetailItems #imgName").attr('href', val);
            $("#frmViewDetailItems #imageitems").attr('src', val);
          } else {
            $("#frmViewDetailItems #imgName").remove();
          }
        } else {
          $("#frmViewDetailItems #" + i).val(val);
          $("#frmViewDetailItems #" + i).html(val);
        }
      }
   });
    $("#frmViewDetailItems").show();
  });
}

export function searchImplementedItemsGlobal(idForm) {
  spinnerHelpers.show();
   var data = {
     code_gr: $("#frmSearchItemsGlobal #code_gr").val(),
     idLanguage: storageData.sIdLanguage(),
};
  
  //var data = functionHelpers.formToJson(idForm);
  //data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  searchItemsGrServices.searchImplementedItemsGlobal(data, function (response) {
    html += "<div class='container div-cont-search'>";
    html += "<div class='row'>";
    html += response.message;
    html += "</div>";
    html += "</div>";

    $('#result').html(html);
    spinnerHelpers.hide();
  });
}


/***   END POST   ***/

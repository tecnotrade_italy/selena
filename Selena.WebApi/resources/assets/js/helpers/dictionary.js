/** *   dictionaryHelpers   ***/

export function getDictionary(property) {
  let value = _getDictionary(property)

  if (!functionHelpers.isValued(value)) {
    dictionaryServices.setByLanguage(function (result) {
      storageData.setDictionary(result.data)

      value = _getDictionary(property)
    })
  }

  return value
}

export function _getDictionary(property) {
  const propertySplit = property.split('.')
  const propertyLength = propertySplit.length
  let value

  $.each(propertySplit, function (i, item) {
    if (i == propertyLength - 1) {
      $.each(storageData.sDictionary(), function (x, dictionary) {
        if (functionHelpers.isValued(dictionary[item])) {
          value = dictionary[item]
          return true
        }
      })
      return true
    }
  })

  return value
}

/** *   searchLiutaioHelpers   ***/

/** *   POST   ***/

export function searchImplemented(idForm) {
  spinnerHelpers.show()
  const data = functionHelpers.formToJson(idForm)
  data.idLanguage = storageData.sIdLanguage()
  let html = ''
  searchLiutaioServices.searchImplemented(data, function (response) {
    html += "<div class='container div-cont-search'>"
    html += "<div class='row'>"
    html += response.message
    html += '</div>'
    html += '</div>'

    $('#result').html(html)
    spinnerHelpers.hide()
  })
}

/** *   END POST   ***/

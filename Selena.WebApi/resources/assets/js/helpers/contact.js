/***   contactHelpers   ***/

export function insertLogsContacts(id, url_file){
  console.log(url_file);
  console.log(id);

     var data = {
     idUser: storageData.sUserId(),
     idContact: id,
     url_file: url_file,
    };
    console.log(data);
    contactRequestServices.insertLogsContacts(data, function (result) {
      console.log(result);
    });
}

export function insertLandingeBookPcb() { 
  spinnerHelpers.show();
    var processing_of_personal_data = false;
    if ($("#contactRequestLandingeBookPcb #processing_of_personal_data").is(":checked")) {
      processing_of_personal_data = true;
    } else {
      processing_of_personal_data = false;
    }

    var data = {
      idLanguage: storageData.sIdLanguage(),
      name: $('#contactRequestLandingeBookPcb #name').val(),
      surname: $('#contactRequestLandingeBookPcb #surname').val(),
      email: $('#contactRequestLandingeBookPcb #email').val(),
      business_name: $('#contactRequestLandingeBookPcb #business_name').val(),
      role: $('#contactRequestLandingeBookPcb #role').val(),
      processing_of_personal_data: processing_of_personal_data,
    };
    if (functionHelpers.isValued($('#contactRequestLandingeBookPcb #emailToSend')) && functionHelpers.isValued($('#contactRequestLandingeBookPcb #emailToSend').val())) {
      data.emailToSend = $('#contactRequestLandingeBookPcb #emailToSend').val();
    }
    contactRequestServices.insertLandingeBookPcb(data, function () {
      $('#contactRequestLandingeBookPcb #name').val();
      $('#contactRequestLandingeBookPcb #surname').val();
      $('#contactRequestLandingeBookPcb #email').val();
      $('#contactRequestLandingeBookPcb #business_name').val();
      $('#contactRequestLandingeBookPcb #role').val('');
      $('#contactRequestLandingeBookPcb #processing_of_personal_data').val('');
      notificationHelpers.success("Richiesta di contatto inviata con successo");
      spinnerHelpers.hide();
    });
}

export function insertRequest() {
  spinnerHelpers.show();

  var checkprivacy_cookie_policy = false;
  if ($("#contactRequest #checkprivacy_cookie_policy").is(":checked")) {
    checkprivacy_cookie_policy = true;
  } else {
    checkprivacy_cookie_policy = false;
  }

  var processing_of_personal_data = false;
  if ($("#contactRequest #processing_of_personal_data").is(":checked")) {
    processing_of_personal_data = true;
  } else {
    processing_of_personal_data = false;
  }
var newsletter_info = false;
  if ($("#contactRequest #newsletter_info").is(":checked")) {
    newsletter_info = true;
  } else {
    newsletter_info = false;
  }
  var newsletter_mailing = false;
  if ($("#contactRequest #newsletter_mailing").is(":checked")) {
    newsletter_mailing = true;
  } else {
    newsletter_mailing = false;
  }

  var data = {
    name: $('#contactRequest #name').val(),
    surname: $('#contactRequest #surname').val(),
    email: $('#contactRequest #email').val(),
    description: $('#contactRequest #description').val(),
    businessName: $('#contactRequest #businessName').val(),
    phoneNumber: $('#contactRequest #phoneNumber').val(),
    faxNumber: $('#contactRequest #faxNumber').val(),
    province: $('#contactRequest #province').val(),
    address: $('#contactRequest #address').val(),
    vatNumber: $('#contactRequest #vatNumber').val(),
    object: $('#contactRequest #object').val(),
    city: $('#contactRequest #city').val(),
    postalCode: $('#contactRequest #postalCode').val(),
    countryofresidence: $('#contactRequest #countryofresidence').val(),
    checkprivacy_cookie_policy: checkprivacy_cookie_policy,
    processing_of_personal_data: processing_of_personal_data,
    newsletter_info:newsletter_info,
    newsletter_mailing:newsletter_mailing,
  };

  if (functionHelpers.isValued($('#contactRequest #emailToSend')) && functionHelpers.isValued($('#contactRequest #emailToSend').val())) {
      data.emailToSend = $('#contactRequest #emailToSend').val();
  }

  contactRequestServices.insert(data, function () {
    $('#contactRequest #object').val();
    $('#contactRequest #vatNumber').val();
    $('#contactRequest #businessName').val();
    $('#contactRequest #postalCode').val();
    $('#contactRequest #city').val();
    $('#contactRequest #address').val();
    $('#contactRequest #province').val();
    $('#contactRequest #faxNumber').val();
    $('#contactRequest #phoneNumber').val();
    $('#contactRequest #name').val('');
    $('#contactRequest #surname').val('');
    $('#contactRequest #email').val('');
    $('#contactRequest #description').val('');
    $('#contactRequest #telefono').val('');
    $('#contactRequest #checkprivacy_cookie_policy').val('');
    $('#contactRequest #processing_of_personal_data').val('');
    $('#contactRequest #countryofresidence').val('');
    notificationHelpers.success("Richiesta di contatto inviata con successo");
    spinnerHelpers.hide();
  });
}

export function insertRequestLanguage() {
  spinnerHelpers.show();

   var checkprivacy_cookie_policy = false;
  if ($("#RequestContactLanguage #checkprivacy_cookie_policy").is(":checked")) {
    checkprivacy_cookie_policy = true;
  } else {
    checkprivacy_cookie_policy = false;
  }

  var processing_of_personal_data = false;
  if ($("#RequestContactLanguage #processing_of_personal_data").is(":checked")) {
    processing_of_personal_data = true;
  } else {
    processing_of_personal_data = false;
  }
var newsletter_info = false;
  if ($("#RequestContactLanguage #newsletter_info").is(":checked")) {
    newsletter_info = true;
  } else {
    newsletter_info = false;
  }
  var newsletter_mailing = false;
  if ($("#RequestContactLanguage #newsletter_mailing").is(":checked")) {
    newsletter_mailing = true;
  } else {
    newsletter_mailing = false;
  }

  var data = {
    idLanguage: storageData.sIdLanguage(),
    name: $('#RequestContactLanguage #name').val(),
    surname: $('#RequestContactLanguage #surname').val(),
    email: $('#RequestContactLanguage #email').val(),
    description: $('#RequestContactLanguage #description').val(),
    businessName: $('#RequestContactLanguage #businessName').val(),
    phoneNumber: $('#RequestContactLanguage #phoneNumber').val(),
    faxNumber: $('#RequestContactLanguage #faxNumber').val(),
    province: $('#RequestContactLanguage #province').val(),
    vatNumber: $('#RequestContactLanguage #vatNumber').val(),
    object: $('#RequestContactLanguage #object').val(),
    city: $('#RequestContactLanguage #city').val(),
    postalCode: $('#RequestContactLanguage #postalCode').val(),
    countryofresidence: $('#RequestContactLanguage #countryofresidence').val(),
    checkprivacy_cookie_policy: checkprivacy_cookie_policy,
    processing_of_personal_data: processing_of_personal_data,
    newsletter_info:newsletter_info,
    newsletter_mailing:newsletter_mailing,
  };

  contactRequestServices.insertRequestLanguage(data, function () {
    $('#RequestContactLanguage #object').val();
    $('#RequestContactLanguage #vatNumber').val();
    $('#RequestContactLanguage #businessName').val();
    $('#RequestContactLanguage #postalCode').val();
    $('#RequestContactLanguage #city').val();
    $('#RequestContactLanguage #province').val();
    $('#RequestContactLanguage #faxNumber').val();
    $('#RequestContactLanguage #phoneNumber').val();
    $('#RequestContactLanguage #name').val('');
    $('#RequestContactLanguage #surname').val('');
    $('#RequestContactLanguage #email').val('');
    $('#RequestContactLanguage #description').val('');
    $('#RequestContactLanguage #telefono').val('');
    $('#RequestContactLanguage #checkprivacy_cookie_policy').val('');
    $('#RequestContactLanguage #processing_of_personal_data').val('');
    $('#RequestContactLanguage #countryofresidence').val('');

    
    notificationHelpers.success(dictionaryHelpers.getDictionary("ContactRequestCompleted"));
    //window.location.href = '/contact-request';
    spinnerHelpers.hide();
  });
}

export function inserTourAvailable() {
  spinnerHelpers.show();
  var data = {
    idLanguage: storageData.sIdLanguage(),
    date: $('#form_tour_availability #date').val(),
    course_id: $('#form_tour_availability #course_id').val(),
    adult_nr: $('#form_tour_availability #adult_nr').val(),
    kids_nr: $('#form_tour_availability #kids_nr').val(),
    babies_nr: $('#form_tour_availability #babies_nr').val(),
    total_amount: $('#form_tour_availability #total_amount').val(),
    name: $('#form_tour_availability #name').val(),
    surname: $('#form_tour_availability #surname').val(),
    email: $('#form_tour_availability #email').val(),
    telephone: $('#form_tour_availability #telephone').val(),
    notes: $('#form_tour_availability #notes').val()
  };

 /* if (functionHelpers.isValued($('#contactRequest #emailToSend')) && functionHelpers.isValued($('#contactRequest #emailToSend').val())) {
    data.emailToSend = $('#contactRequest #emailToSend').val();
  }*/

  contactRequestServices.insertTourAvailable(data, function () {
    $('#form_tour_availability #date').val();
    $('#form_tour_availability #course_id').val();
    $('#form_tour_availability #adult_nr').val();
    $('#form_tour_availability #kids_nr').val();
    $('#form_tour_availability #babies_nr').val();
    $('#form_tour_availability #total_amount').val('');
    $('#form_tour_availability #name').val('');
    $('#form_tour_availability #surname').val('');
    $('#form_tour_availability #email').val('');
    $('#form_tour_availability #telephone').val('');
    $('#form_tour_availability #notes').val('');
    notificationHelpers.success("Richiesta inviata con successo");
    spinnerHelpers.hide();
    $('#paypalform').submit();
  });
}




export function insertRequestWorkForUs() {
  spinnerHelpers.show();
  var checkprivacy_cookie_policy = false;
  if ($("#workForUs #checkprivacy_cookie_policy").is(":checked")) {
    checkprivacy_cookie_policy = true;
  } else {
    checkprivacy_cookie_policy = false;
  }

  
  /*var data = {
    name: $('#workForUs #name').val(),
    surname: $('#workForUs #surname').val(),
    email: $('#workForUs #email').val(),
    phoneNumber: $('#workForUs #phone').val(),
    gender: $('#workForUs #gender').val(),
    date: $('#workForUs #date').val(),
    position: $('#workForUs #position').val(),
    nation: $('#workForUs #nation').val(),
    city: $('#workForUs #city').val(),
    info: $('#workForUs #info').val(),
    actualCompany: $('#workForUs #actualCompany').val(),
    file: $("#myFile").prop('files')[0]
  };*/
  var file_data = $('#myFile').prop('files')[0],
    name = $('#workForUs #name').val(),
    surname = $('#workForUs #surname').val(),
    email = $('#workForUs #email').val(),
    phoneNumber = $('#workForUs #phone').val(),
    gender = $('#workForUs #gender').val(),
    date = $('#workForUs #date').val(),
    position = $('#workForUs #position').val(),
    nation = $('#workForUs #nation').val(),
    city = $('#workForUs #city').val(),
    info = $('#workForUs #info').val(),
    cityofborn = $('#workForUs #cityofborn').val(),
    address = $('#workForUs #address').val(),
    cap = $('#workForUs #cap').val(),
    province = $('#workForUs #province').val(),
    countryofresidence = $('#workForUs #countryofresidence').val(),
   	regionofdomicily = $('#workForUs #regionofdomicily').val(),
		cityofresidence = $('#workForUs #cityofresidence').val(),		
		fiscalcode = $('#workForUs #fiscalcode').val(),		
		website = $('#workForUs #website').val(),		
		license = $('#workForUs #license').val(),	
		meansoftransport = $('#workForUs #meansoftransport').val(),	
	  countryofbirth = $('#workForUs #countryofbirth').val(),	
    actualCompany = $('#workForUs #actualCompany').val();
    checkprivacy_cookie_policy = checkprivacy_cookie_policy;
  
    var form_data = new FormData();
    form_data.append('file', file_data);
    form_data.append('name', name);
    form_data.append('surname', surname);
    form_data.append('email', email);
    form_data.append('phoneNumber', phoneNumber);
    form_data.append('gender', gender);
    form_data.append('date', date);
    form_data.append('position', position);
    form_data.append('nation', nation);
    form_data.append('city', city);
    form_data.append('info', info);
    form_data.append('cityofborn', cityofborn);
    form_data.append('address', address);
    form_data.append('cap', cap);
    form_data.append('province', province);
    form_data.append('actualCompany', actualCompany);
    form_data.append('checkprivacy_cookie_policy', checkprivacy_cookie_policy);
    form_data.append('countryofresidence', countryofresidence);
    form_data.append('regionofdomicily', regionofdomicily);
    form_data.append('cityofresidence', cityofresidence);
    form_data.append('fiscalcode', fiscalcode);
    form_data.append('website', website);
    form_data.append('license', license);
    form_data.append('meansoftransport', meansoftransport);
    form_data.append('countryofbirth', countryofbirth);
  

  $.ajax({
    url: configData.wsRootServicesUrl + "/api/v1/contactrequest/insertworkforus", // point to server-side PHP script
    data: form_data,
    type: 'POST',
    contentType: false, // The content type used when sending data to the server.
    cache: false, // To unable request pages to be cached
    processData: false,
    success: function(data) {
      $('#workForUs #name').val('');
      $('#workForUs #surname').val('');
      $('#workForUs #email').val('');
      $('#workForUs #phone').val('');
      $('#workForUs #gender').val('');
      $('#workForUs #date').val('');
      $('#workForUs #position').val('');
      $('#workForUs #nation').val('');
      $('#workForUs #city').val('');
      $('#workForUs #info').val('');
      $('#workForUs #actualCompany').val('');
      $('#workForUs #checkprivacy_cookie_policy').val('');
      $('#workForUs #countryofresidence').val('');
      $('#workForUs #regionofdomicily').val('');
      $('#workForUs #cityofresidence').val('');
      $('#workForUs #fiscalcode').val('');
      $('#workForUs #website').val('');
      $('#workForUs #license').val('');
      $('#workForUs #meansoftransport').val('');
      $('#workForUs #countryofbirth').val('');
      $("#myFile").val('');
      notificationHelpers.success('Modulo lavora con noi spedito correttamente!');
      spinnerHelpers.hide();
      setTimeout(function(){
        window.location.href = "/";
      }, 1500);
    }
});

  /*contactRequestServices.insertWorkForUs(form_data, function () {
    $('#workForUs #name').val('');
    $('#workForUs #surname').val('');
    $('#workForUs #email').val('');
    $('#workForUs #phone').val('');
    $('#workForUs #gender').val('');
    $('#workForUs #date').val('');
    $('#workForUs #position').val('');
    $('#workForUs #nation').val('');
    $('#workForUs #city').val('');
    $('#workForUs #info').val('');
    $('#workForUs #actualCompany').val('');
    $("#myFile").val('');
    notificationHelpers.success(dictionaryHelpers.getDictionary("ContactRequestCompleted"));
    spinnerHelpers.hide();
  });*/
}


export function unsubscribeContact() {
  spinnerHelpers.show()

  var data = {
    email: $('#contactUnsubscribe #email').val()
  }

  contactServices.unsubscribe(data, function () {
    $('#contactUnsubscribe #email').val('')
    notificationHelpers.success(dictionaryHelpers.getDictionary("UnsubscribeContactCompleted"))
    spinnerHelpers.hide()
  })
}

export function unsubscribeNewsletter() {
  spinnerHelpers.show();

  var data = {
    email: $('#unsubscribeNewsletter #email').val()
  };

  contactServices.unsubscribeNewsletter(data, function (response) {
  
    $('#unsubscribeNewsletter #email').val('');
    $("#div_html_newsletter").html("");
    var html = "";
    html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
    html += '<div class="col-12" style="text-align:center;">';
    html += '<h2 for="' + '" style="color: #3f8ec5;">Cancellazione Dalla Newsletter' + "</h2>";
    html += "</div>";
    html += '<div class="col-12" style="text-align:center;">';
    html += '<h3 for="">' + response.message  + "</h3>";
    html += "</div>";
    html += "</div>";
    $("#div_html_newsletter").append(html);
    window.$("#unsubscribeNewsletterA").modal("show")
   //notificationHelpers.success(dictionaryHelpers.getDictionary("UnsubscribeContactCompleted"));
    spinnerHelpers.hide();
  });
}


export function subscribeContact() {
  spinnerHelpers.show()

  var processing_of_personal_data = false;
  if ($("#contactSubscribe #processing_of_personal_data").is(":checked")) {
    processing_of_personal_data = true;
  } else {
    processing_of_personal_data = false;
  }

  console.log(data);
  var data = {
    email: $('#contactSubscribe #emailnewsletter').val(),
    processing_of_personal_data: processing_of_personal_data,
  }

  contactServices.subscribe(data, function () {
    $('#contactSubscribe #emailnewsletter').val(''),
    $('#contactSubscribe #processing_of_personal_data').val('')
    var cname = "newsletterPopUp"
    var cvalue = "true"
    var exdays = 7
    var d = new Date()
    d.setTime(d.getTime() + (exdays*24*60*60*1000))
    var expires = "expires="+ d.toUTCString()
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/"
    window.$('.modal').modal('hide')
    
    notificationHelpers.success(dictionaryHelpers.getDictionary("SubscribeContactCompleted"))
    spinnerHelpers.hide()
  });
}

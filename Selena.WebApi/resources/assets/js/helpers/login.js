/** *   loginHelpers   ***/

export function login() {
  spinnerHelpers.show()
  $('#loginPasswordError').hide()
  const sSubmitFormButton = $('#btnLogin')
  const username = $('#loginUsername').val()
  const password = $('#loginPassword').val()

  if (functionHelpers.isValued(sSubmitFormButton)) {
    sSubmitFormButton.text('Login...')
    sSubmitFormButton.prop('disabled', true)
  }

  if ($('#chkSaveLogin').is(':checked')) {
    storageData.setCredetial({
      username,
      password,
    })
  } else {
    storageData.removeCredetial()
  }

  loginServices.login(
    username,
    password,
    function (result) {
      console.log(result);
      if (result.code == '200') {
        storageData.setTokenKey('Bearer ' + result.access_token)
        storageData.setRefreshTokenKey(result.refresh_token)
        storageData.setExpiresTokenKey(result.expires_at)
        window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000)
        storageData.setUserName(result.username)
        storageData.setUserDescription(result.user_description)
        storageData.setUserId(result.id)
        storageData.setRole(result.role)

        storageData.removeIdLanguage()
        window.localStorage.setItem('sIdLanguage', result.idLanguage)
        storageData.setCookie('sIdLanguage', result.idLanguage , 365);
        
        storageData.setCartId(result.cart_id)

        if (pageHelpers.isAdminPage() || result.username == 'admin') {
          storageData.setIdLanguage(result.idLanguage);
          adminConfigurationServices.getMenu(
            function (result) {
              storageData.setMenu(result.data)
              if (functionHelpers.isValued(storageData.sHoldUrl())) {
                window.location = '/?' + storageData.sHoldUrl()
                storageData.removeHoldUrl()
              } else if (pageHelpers.isAdminPage()) {
                window.location = '/admin'
              } else {
                window.location = '/'
              }
            },
            function (response) {
              if (functionHelpers.isValued(response)) {
                $('#loginPasswordError').text(response)
                $('#loginPasswordError').show()
                notificationHelpers.error(response)
              }

              const sSubmitFormButton = $('#btnLogin')
              sSubmitFormButton.text('Login')
              sSubmitFormButton.prop('disabled', false)
              spinnerHelpers.hide()
            }
          )
        } else if (functionHelpers.isValued(storageData.sHoldUrl())) {
          window.location = '/?' + storageData.sHoldUrl()
          storageData.removeHoldUrl()
        } else {
          window.location = '/'
        }
      } else {
        if (result.code == '404') {
          confirm(result.message)
        } else {
          confirm('Credenziali errate')
        }
        const sSubmitFormButton = $('#btnLogin')
        sSubmitFormButton.text('Login')
        sSubmitFormButton.prop('disabled', false)
        spinnerHelpers.hide()
      }
    },
    function (response) {
      if (response.status == 0) {
        $('#loginPasswordError').text(
          dictionaryHelpers.getDictionary('UnableToConnectToServer')
        )
      } else if (
        functionHelpers.isValued(response.responseText) &&
        jQuery.parseJSON(response.responseText).error_description != undefined
      ) {
        $('#loginPasswordError').text(
          jQuery.parseJSON(response.responseText).error_description
        )
      }
      $('#loginPasswordError').show()
      const sSubmitFormButton = $('#btnLogin')
      sSubmitFormButton.text('Login')
      sSubmitFormButton.prop('disabled', false)
      spinnerHelpers.hide()
    }
  )
}

export function disconnect() {
  const credential = storageData.sCredetial()
  storageData.removeCredentialData()
  storageData.setCredetial(credential)
  storageData.setCartId('')
  if (window.location.hash != '#logout') {
    storageData.setHoldUrl(window.location.hash)
  }

  /*var auth2 = gapi.auth2.getAuthInstance();
  auth2.signOut().then(function () {
    console.log('User signed out.');
  });*/

  if (pageHelpers.isAdminPage()) {
    window.location = '/admin/login'
  } else {
   
    window.location = '/login'
  }
}

export function refreshToken(refresh) {
  const diff = Math.round(
    (new Date(storageData.sExpiresTokenKey()).getTime() -
      new Date().getTime()) /
      1000 /
      60
  )

  if (diff < 0) {
    loginHelpers.disconnect()
  } else if (diff <= 5) {
    loginServices.refreshToken(function (result) {
      storageData.setTokenKey('bearer ' + result.access_token)
      storageData.setRefreshTokenKey(result.refresh_token)
      storageData.setExpiresTokenKey(result['.expires'])
      window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000)
    })
  } else if (functionHelpers.isValued(refresh) && refresh) {
    window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000)
  }
}

export function loginSalesman() {
  spinnerHelpers.show()
  $('#loginPasswordError').hide()
  const sSubmitFormButton = $('#btnLoginSalesman');
  const username = $('#loginUsernameSalesman').val()
  const password = $('#loginPasswordSalesman').val()

  if (functionHelpers.isValued(sSubmitFormButton)) {
    sSubmitFormButton.text('Login...')
    sSubmitFormButton.prop('disabled', true)
  }

  if ($('#chkSaveLogin').is(':checked')) {
    storageData.setCredetial({
      username,
      password,
    })
  } else {
    storageData.removeCredetial()
  }

  loginServices.loginSalesman(username,password, function (result) {
    if (result.code == '200') {
      storageData.setSalesmanId(result.id);
      const sSubmitFormButton = $('#btnLoginSalesman')
      sSubmitFormButton.text('Login')
      sSubmitFormButton.prop('disabled', false)
      spinnerHelpers.hide()
      window.location = '/area-agenti';
    }else {
      if (result.code == '404') {
        confirm(result.message)
      } else {
        confirm('Credenziali errate')
      }
      const sSubmitFormButton = $('#btnLoginSalesman')
      sSubmitFormButton.text('Login')
      sSubmitFormButton.prop('disabled', false)
      spinnerHelpers.hide()
    }
  });

}


export function disconnectUserBySalesman() {
  const credential = storageData.sCredetial()
  storageData.removeCredentialData()
  storageData.setCredetial(credential)
  storageData.setCartId('')
  if (window.location.hash != '#logout') {
    storageData.setHoldUrl(window.location.hash)
  }
  window.location.reload(true);

}

export function disconnectSalesman() {
  window.localStorage.removeItem('sTokenKey')
  window.localStorage.removeItem('sRefreshTokenKey')
  window.localStorage.removeItem('sExpiresTokenKey')
  window.localStorage.removeItem('sCredetial')
  window.localStorage.removeItem('sUserName')
  window.localStorage.removeItem('sUserId')
  window.localStorage.removeItem('sSalesmanId')
  window.localStorage.removeItem('sUserDescription')
  window.localStorage.removeItem('sNotificationData')
  window.localStorage.removeItem('sRouteParameter')
  window.localStorage.removeItem('sFilterParameter')
  window.localStorage.removeItem('sMenu')

  storageData.delCookie('sUserId');
  storageData.delCookie('sUserDescription');
  storageData.delCookie('sSalesmanId');
  storageData.delCookie('sUserName');
  storageData.delCookie('sRole');
  storageData.delCookie('sCartId');
  window.location.reload(true);
}


 /*
 export function insertLoginForGoogle(profile) {
  spinnerHelpers.show(); 

  var data = {
    id_google: profile.getId(),
    name: profile.getGivenName(),
    surname: profile.getFamilyName(),
    name_and_surname: profile.getName(),
    email: profile.getEmail(),
  };

 loginServices.insertLoginForGoogle(data, function (result) {
    if (result.data.original.code == '200') {
      storageData.setTokenKey('Bearer ' + result.data.original.access_token)
      storageData.setRefreshTokenKey(result.data.original.refresh_token)
      storageData.setExpiresTokenKey(result.data.original.expires_at)
      window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000)
      storageData.setUserName(result.data.original.username)
      storageData.setUserDescription(result.data.original.user_description)
      storageData.setUserId(result.data.original.id)
      storageData.setRole(result.data.original.role)
      storageData.removeIdLanguage()
      window.localStorage.setItem('sIdLanguage', result.data.original.idLanguage)
      storageData.setCookie('sIdLanguage', result.data.original.idLanguage , 365);
      storageData.setCartId(result.data.original.cart_id)

      if (result.data.original.redirect == true) {
        window.location = '/modifica-i-tuoi-dati';
      }else{
        window.location = '/area-privata';
      }
    }else{
      if (result.data.original.code == '404') {
        confirm(result.data.original.message);
      } else {
        confirm('Credenziali errate');
      }
    }
  });
}

export function insertLoginForFacebook(response2) {
  spinnerHelpers.show(); 

  var data = {
    email: response2.email,
    first_name: response2.first_name,
    name: response2.name,
    id_facebook: response2.id,
    surname: response2.last_name,
    idUser: storageData.sUserId(),
  };

  loginServices.insertLoginForFacebook(data, function (result) {
    if (result.data.original.code == '200') {
      storageData.setTokenKey('Bearer ' + result.data.original.access_token)
      storageData.setRefreshTokenKey(result.data.original.refresh_token)
      storageData.setExpiresTokenKey(result.data.original.expires_at)
      window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000)
      storageData.setUserName(result.data.original.username)
      storageData.setUserDescription(result.data.original.user_description)
      storageData.setUserId(result.data.original.id)
      storageData.setRole(result.data.original.role)
      storageData.removeIdLanguage()
      window.localStorage.setItem('sIdLanguage', result.data.original.idLanguage)
      storageData.setCookie('sIdLanguage', result.data.original.idLanguage , 365);
      storageData.setCartId(result.data.original.cart_id)

      if (result.data.original.redirect == true) {
        window.location = '/modifica-i-tuoi-dati';
      }else{
        window.location = '/area-privata';
      }
    }else{
      if (result.data.original.code == '404') {
        confirm(result.data.original.message);
      } else {
        confirm('Credenziali errate');
      }
    }
  });
}*/

export function insertLoginForGoogle(profile) {
  spinnerHelpers.show(); 

  var data = {
    id_google: profile.getId(),
    name: profile.getGivenName(),
    surname: profile.getFamilyName(),
    name_and_surname: profile.getName(),
    email: profile.getEmail(),
  };

  //console.log(data);

  loginServices.insertLoginForGoogle(data, function (result) {
   if (result.data.original.code == '200') {
       storageData.setTokenKey('Bearer ' + result.data.original.access_token)
       storageData.setRefreshTokenKey(result.data.original.refresh_token)
       storageData.setExpiresTokenKey(result.data.original.expires_at)
       window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000)
       storageData.setUserName(result.data.original.username)
       storageData.setUserDescription(result.data.original.user_description)
       storageData.setUserId(result.data.original.id)
       storageData.setRole(result.data.original.role)
       storageData.removeIdLanguage()
       window.localStorage.setItem('sIdLanguage', result.data.original.idLanguage)
       storageData.setCookie('sIdLanguage', result.data.original.idLanguage , 365);
       storageData.setCartId(result.data.original.cart_id)

       if (result.data.original.redirect == true || result.data.original.redirect == 'true') {
           window.location = '/modifica-i-tuoi-dati';
         }else{
          window.location = '/area-privata';
         }
      }
});
}

export function insertLoginForFacebook(response2) {
  spinnerHelpers.show(); 

    var data = {
      email: response2.email,
      first_name: response2.first_name,
      name: response2.name,
      id_facebook: response2.id,
      surname: response2.last_name,
      idUser: storageData.sUserId(),
    };

    loginServices.insertLoginForFacebook(data, function (result) {
    
    /* if (functionHelpers.isValued(result.data.message)){
      notificationHelpers.success("Accesso avvenuto con successo!");
      spinnerHelpers.hide();
     } else {
        window.location = '/completa-i-tuoi-dati';
     }*/
    if (result.data.original.code == '200') {
        storageData.setTokenKey('Bearer ' + result.data.original.access_token)
        storageData.setRefreshTokenKey(result.data.original.refresh_token)
        storageData.setExpiresTokenKey(result.data.original.expires_at)
        window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000)
        storageData.setUserName(result.data.original.username)
        storageData.setUserDescription(result.data.original.user_description)
        storageData.setUserId(result.data.original.id)
        storageData.setRole(result.data.original.role)
        storageData.removeIdLanguage()
        window.localStorage.setItem('sIdLanguage', result.data.original.idLanguage)
        storageData.setCookie('sIdLanguage', result.data.original.idLanguage , 365);
        storageData.setCartId(result.data.original.cart_id)

        if (result.data.original.redirect == true || result.data.original.redirect == 'true') {
          console.log(result);
            window.location = '/modifica-i-tuoi-dati';
          }else{
            window.location = '/area-privata';
          }
    }
});

}





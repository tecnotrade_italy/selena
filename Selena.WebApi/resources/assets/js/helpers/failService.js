/** *   failService   ***/

export function fail(response, fnError, id) {
  if (response.status == 401) {
    alert(401)
  }

  if (response.status == 400 && response.responseText == '{"error":"invalid_grant"}') {
    // || response.status == 401) {
    loginHelpers.disconnect()
  } else if (response.status == 0) {
    notificationHelpers.error(dictionaryHelpers.getDictionary('UnableToConnectToServer'))
  } else if (functionHelpers.isValued(response.responseText) && ($.parseJSON(response.responseText).message != undefined || $.parseJSON(response.responseText).error_description != undefined)) {
    if ($.parseJSON(response.responseText).message != undefined) {
      if ($.isFunction(fnError)) {
        if (functionHelpers.isValued(id)) {
          fnError($.parseJSON(response.responseText).message, id)
        } else {
          fnError($.parseJSON(response.responseText).message)
        }
      } else {
        notificationHelpers.error($.parseJSON(response.responseText).message)
      }
    } else if ($.isFunction(fnError)) {
      if (functionHelpers.isValued(id)) {
        fnError($.parseJSON(response.responseText).error_description, id);
      } else {
        fnError($.parseJSON(response.responseText).error_description);
      }
    } else {
      notificationHelpers.error($.parseJSON(response.responseText).error_description);
    }
  }else if ($.isFunction(fnError)) {
    fnError(response.responseText);
  } else {
    notificationHelpers.error(response.responseText);
  }
  spinnerHelpers.hide()
}

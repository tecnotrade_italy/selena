/** *   interventioncustomerHelpers   ***/

export function getAllInterventionCustomer() {
  spinnerHelpers.show()
  interventionCustomerServices.getAllInterventionCustomer(
    storageData.sUserId(),
    function (response) {
      for (let i = 0; i < response.data.length; i++) {
        let newList = ''
        newList += '<tr>'
        newList += '<td>' + response.data[i].id + '</td>'
        newList += '<td>' + response.data[i].date_ddt + '</td>'
        newList += '<td>' + response.data[i].plant_type + '</td>'
        newList += '<td>' + response.data[i].defect
        if (response.data[i].imgmodule != '') {
          newList +=
            '<div class="container"><div id="lightgallery" style="display:flex">'
          newList +=
            "<a href='" +
            response.data[i].imgmodule +
            "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>"
        }
        newList += '</td>'
        newList += '<td>' + response.data[i].states_quotes_description + '</td>'
        newList += '<td>' + response.data[i].states_description + '</td>'

        newList += '<td>'
        if (
          response.data[i].states_quotes_description != 'Accettato' &&
          response.data[i].states_quotes_description != 'Rifiutato'
        ) {
          newList +=
            '<button class="btn btn-link" onclick="interventioncustomerHelpers.getByViewQuotesCustomer(' +
            response.data[i].id +
            ')"><i class="fa fa-eur"/></button>'
        }
        if (response.data[i].states_quotes_description == 'Accettato') {
          newList +=
            '<button class="btn btn-green" style="color:green;" onclick="interventioncustomerHelpers.getByViewQuotesCustomer(' +
            response.data[i].id +
            ')"><i class="fa fa-eur" style="color:green!important;"/></button>'
        }
        if (response.data[i].states_quotes_description == 'Rifiutato') {
          newList +=
            '<button class="btn btn-green" style="color:red;" onclick="interventioncustomerHelpers.getByViewQuotesCustomer(' +
            response.data[i].id +
            ')"><i class="fa fa-eur" style="color:red!important;"/></button>'
        }
        ;('</td>')

        newList +=
          '<td><button class="btn btn-link" onclick="interventioncustomerHelpers.getByViewModuleCustomer(' +
          response.data[i].id +
          ')"><i class="fa fa-wrench"/></button></td>'
        newList += '</tr>'
        $('#table_list_customerintervention tbody').append(newList)
        $('#table_list_customerintervention').show()
        $('#frmViewQuotesCustomer').hide()
        $('#ViewFaultModule').hide()
      }
      lightGallery(document.getElementById('lightgallery'))
    }
  )
  spinnerHelpers.hide()
}

export function getByViewModuleCustomer(id) {
  interventionCustomerServices.getByViewModuleCustomer(id, function (response) {
    const searchParam = ''
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        if (i == 'id') {
          $('#ViewFaultModule #' + i).html(val)
        } else {
          $('#ViewFaultModule #' + i).attr('checked', 'checked')
        }
      } else if (i == 'imgName') {
        if (val != '') {
          $('#ViewFaultModule #imgPreview').attr('src', val)
        } else {
          $('#ViewFaultModule #imgName').remove()
        }
      } else {
        $('#ViewFaultModule #' + i).val(val)
        $('#ViewFaultModule #' + i).html(val)
      }
    })
    $('#ViewFaultModule').show()
    $('#frmViewQuotesCustomer').hide()
    $('#table_list_customerintervention').hide()
  })
}

export function getByViewQuotesCustomer(id) {
  interventionCustomerServices.getByViewQuotesCustomer(id, function (response) {
    const searchParam = ''
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        if (i == 'id') {
          $('#frmViewQuotesCustomer #' + i).html(val)
        } else {
          $('#frmViewQuotesCustomer #' + i).attr('checked', 'checked')
        }
      } else if (i == 'imgName') {
        if (val != '') {
          $('#frmViewQuotesCustomer #imgPreview').attr('src', val)
        } else {
          $('#frmViewQuotesCustomer #imgName').remove()
        }
      } else {
        $('#frmViewQuotesCustomer #' + i).val(val)
        $('#frmViewQuotesCustomer #' + i).html(val)
      }

      let searchParam = ''
      const initials = []
      initials.push({
        id: response.data.id_states_quote,
        text: response.data.States_Description,
      })
      $('#id_states_quote').select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl +
            '/api/v1/StatesQuotesGr/selectStatesQuotesGr',
          data(params) {
            if (params.term == '') {
              searchParam = '*'
            } else {
              searchParam = params.term
            }
            const query = {
              search: searchParam,
            }
            return query
          },
          processResults(data) {
            const dataParse = JSON.parse(data)
            return {
              results: dataParse.data,
            }
          },
        },
      })

      const AggArticlesQuotes = response.data.Articles
      $('#div-cont-codegr_description-agg').html('')
      if (AggArticlesQuotes.length > 0) {
        for (let j = 0; j < AggArticlesQuotes.length; j++) {
          let html = ''
          html +=
            '<div class="row no-gutters" data-id-row="' +
            '" id="divcont-' +
            '" >'
          html += '<div class="col-4">'
          html +=
            '<label for="description">Descrizione:<br><span class="description">' +
            AggArticlesQuotes[j].description +
            '</span></label>'
          html += '</div>'
          html += '<div class="col-4">'
          html +=
            '<label for="code_gr">Codice Articolo:<br><span class="code_gr">' +
            AggArticlesQuotes[j].code_gr +
            '</span></label>'
          html += '</div>'
          html += '</div>'
          $('#div-cont-codegr_description-agg').append(html)
        }
      }
    })
    $('#frmViewQuotesCustomer').show()
    $('#ViewFaultModule').hide()
    $('#table_list_customerintervention').hide()
  })
}

export function updateInterventionUpdateViewCustomerQuotes() {
  spinnerHelpers.show()
  const data = {
    updated_id: storageData.sUserId(),
    // idLanguage: storageData.sIdLanguage(),
    id_intervention: $('#frmViewQuotesCustomer #id').val(),
    // id_states_quote: $('#frmViewQuotesCustomer #id_states_quote').val(),
    id_states_quote: $('#frmViewQuotesCustomer #rifiutato').val(),
  }
  if (confirm('Vuoi confermare lo stato del tuo preventivo?')) {
    interventionCustomerServices.updateInterventionUpdateViewCustomerQuotes(
      data,
      function () {
        notificationHelpers.success('Modifica avvenuta con successo')
        spinnerHelpers.hide()
        $('#table_list_customerintervention').show()
        $('#frmViewQuotesCustomer').hide()
      }
    )
  }
}
export function updateInterventionUpdateViewCustomerQuotesAccept() {
  spinnerHelpers.show()
  const data = {
    updated_id: storageData.sUserId(),
    // idLanguage: storageData.sIdLanguage(),
    id_intervention: $('#frmViewQuotesCustomer #id').val(),
    // id_states_quote: $('#frmViewQuotesCustomer #id_states_quote').val(),
    id_states_quote: $('#frmViewQuotesCustomer #accettato').val(),
  }
  if (confirm('Vuoi confermare lo stato del tuo preventivo?')) {
    interventionCustomerServices.updateInterventionUpdateViewCustomerQuotes(
      data,
      function () {
        notificationHelpers.success('Modifica avvenuta con successo')
        spinnerHelpers.hide()
        $('#table_list_customerintervention').show()
        $('#frmViewQuotesCustomer').hide()
      }
    )
  }
}

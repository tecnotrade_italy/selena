/** *   peopleHelpers   ***/

export function insert() {
  spinnerHelpers.show()

  const data = {
    user_id: storageData.sUserId(),
    name: $('#people #name').val(),
    surname: $('#people #surname').val(),
    email: $('#people #email').val(),
    phone_number: $('#people #phone_number').val(),
    roles_id: $('#people #roles_id').val(),
  }

  peopleServices.insert(data, function () {
    $('#people #name').val('')
    $('#people #surname').val('')
    $('#people #email').val('')
    $('#people #phone_number').val('')
    $('#people #roles_id').val('')
    notificationHelpers.success(
      dictionaryHelpers.getDictionary('ContactRequestCompleted')
    )
    spinnerHelpers.hide()
  })
}

export function getPeopleUser(id) {
  spinnerHelpers.show()
  peopleServices.getPeopleUser(id, function (response) {
    for (let i = 0; i < response.data.length; i++) {
      let newList = ''
      newList += '<tr>'
      newList +=
        '<td><button class="btn btn-info" onclick="peopleHelpers.getByIdReferent(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil"/></button>'
      newList +=
        '<button class="btn btn-danger" onclick="peopleHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash"/></button></td>'
      newList += '<td>' + response.data[i].id + '</td>'
      newList += '<td>' + response.data[i].name + '</td>'
      newList += '<td>' + response.data[i].surname + '</td>'
      newList += '<td>' + response.data[i].email + '</td>'
      newList += '<td>' + response.data[i].phone_number + '</td>'
      newList += '<td>' + response.data[i].roles_id + '</td>'
      newList += '</tr>'

      $('#table_list_people tbody').append(newList)
      $('#table_list_people').show()
      $('#frmUpdateReferent').hide()
    }
  })
  spinnerHelpers.hide()
}

export function getByIdReferent(id) {
  // spinnerHelpers.show();
  peopleServices.getByIdReferent(id, function (response) {
    let searchParam = ''

    // select roles_id
    const initials = []
    initials.push({
      id: response.data.roles_id,
      text: response.data.descriptionRoleId,
    })
    $('#roles_id').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/roles/select',
        data(params) {
          if (params.term == '') {
            searchParam = '*'
          } else {
            searchParam = params.term
          }
          const query = {
            search: searchParam,
          }
          return query
        },
        processResults(data) {
          const dataParse = JSON.parse(data)

          return {
            results: dataParse.data,
          }
        },
      },
    })

    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        $('#frmUpdateReferent #' + i).attr('checked', 'checked')
      } else {
        $('#frmUpdateReferent #' + i).val(val)
      }
    })
    $('#frmUpdateReferent').show()
    $('#frmAddReferent').hide()
    $('#table_list_people').hide()
  })
}

export function updateReferent() {
  spinnerHelpers.show()
  const data = {
    // id: storageData.sUserId(),
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmUpdateReferent #id').val(),
    name: $('#frmUpdateReferent #names').val(),
    surname: $('#frmUpdateReferent #surnames').val(),
    email: $('#frmUpdateReferent #emails').val(),
    phone_number: $('#frmUpdateReferent #phone_numbers').val(),
    roles_id: $('#frmUpdateReferent #roles_id').val(),
  }
  peopleServices.updateReferent(data, function () {
    $('#frmUpdateReferent #id').val('')
    $('#frmUpdateReferent #names').val('')
    $('#frmUpdateReferent #surnames').val('')
    $('#frmUpdateReferent #emails').val('')
    $('#frmUpdateReferent #phone_numbers').val('')
    $('#frmUpdateReferent #roles_id').val('')

    notificationHelpers.success('Referente aggiornato correttamente!')
    spinnerHelpers.hide()
    $('#table_list_people').show()
    $('#frmUpdateReferent').hide()
  })
}

export function insertNoAuth() {
  spinnerHelpers.show()

  const data = {
    created_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    // id: $('#frmAddReferent #id').val(),
    name: $('#frmAddReferent #name').val(),
    surname: $('#frmAddReferent #surname').val(),
    email: $('#frmAddReferent #email').val(),
    phone_number: $('#frmAddReferent #phone_number').val(),
    roles_id: $('#frmAddReferent #add_roles_id').val(),
    user_id: $('#frmAddReferent #user_id').val(),
  }

  peopleServices.insertNoAuth(data, function () {
    // $('#frmAddReferent #id').val('');
    $('#frmAddReferent #name').val('')
    $('#frmAddReferent #surname').val('')
    $('#frmAddReferent #email').val('')
    $('#frmAddReferent #phone_number').val('')
    $('#frmAddReferent #add_roles_id').val('')
    $('#frmAddReferent #user_id').val('')

    $('#frmAddReferent').hide()

    $('#table_list_people').show()

    notificationHelpers.success('Referente inserito correttamente!')

    spinnerHelpers.hide()
  })
}

export function deleteRow(id) {
  spinnerHelpers.show()
  peopleServices.del(id, function (response) {
    notificationHelpers.success(
      dictionaryHelpers.getDictionary('DeleteCompleted')
    )
    window.location.reload()
    $('#table_list_people').show()
    $('#table_list_user').hide()
  })
  spinnerHelpers.hide()
}

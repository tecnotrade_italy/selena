/** *   processingviewHelpers   ***/

export function getAllProcessingView() {
  spinnerHelpers.show()
  processingviewServices.getAllProcessingView(
    storageData.sIdLanguage(),
    function (response) {
      for (let i = 0; i < response.data.length; i++) {
      let newList = "";
        newList += '<tr>'
        newList +=
          '<td><button class="btn btn-light" onclick="processingviewHelpers.getByViewValue(' +
          response.data[i].id +
          ')"><i class="fa fa-circle-o"/></button></td>'
        newList += '<td>' + response.data[i].id + '</td>'
        newList += '<td>' + response.data[i].date_aggs + '</td>'
        newList += '<td>' + response.data[i].descriptionCustomer + '</td>'
        newList += '<td>' + response.data[i].defect
        if (response.data[i].imgmodule != '') {
          newList +=
            '<div class="container"><div id="lightgallery" style="display:flex">'
          newList +=
            "<a href='" +
            response.data[i].imgmodule +
            "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>"
        }
        newList += '</td>'
        newList +=
          '<td><button class="btn btn-danger" onclick="processingviewHelpers.deleteRow(' +
          response.data[i].id +
          ')"><i class="fa fa-trash"/></button></td>'
        newList += '</tr>'
        $('#table_list_processingView').show()
        $('#frmViewProcessingSheet').hide()

        $('#table_list_processingView tbody').append(newList)
      }
      lightGallery(document.getElementById('lightgallery'))
    }
  )

  spinnerHelpers.hide()
}

export function getByViewValue(id) {
  // $('#frmViewProcessingSheet #id').val(id);
  processingviewServices.getByViewValue(id, function (response) {
    let searchParam = ''
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        if (i == 'id') {
          $('#frmViewProcessingSheet #' + i).html(val)
        } else {
          $('#frmViewProcessingSheet #' + i).attr('checked', 'checked')
        }
      } else if (i == "imgName") {
          if (val != "") {
            $("#frmViewProcessingSheet #imgPreview").attr('src', val);
            $("#frmViewProcessingSheet #imgName").attr('src', val);
            $("#frmViewProcessingSheet #imgNameLink").attr('href', val);

          } else {
            $("#frmViewProcessingSheet #imgName").remove();
          }
        } else {
          $("#frmViewProcessingSheet #" + i).val(val);
          $("#frmViewProcessingSheet #" + i).html(val);
          $("#frmViewProcessingSheet #defect").prop("disabled", true);
          //$("#frmViewProcessingSheet #imgName" + i).val(val);  

        }

      let data = new Date()
      let gg, mm, aaaa
      gg = data.getDate() + '/'
      mm = data.getMonth() + 1 + '/'
      aaaa = data.getFullYear()

      let HH = data.getHours()
      let MM = data.getMinutes()
      let SS = data.getSeconds()
      parseInt(MM) < 10 ? (MM = '0' + MM) : null
      parseInt(SS) < 10 ? (SS = '0' + SS) : null

      $('#div-cont-date_aggs').html('')
      var html = ''
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-12">'
      html +=
        '<label id="date_aggs" for="date_aggs' +
        '">Data: ' +
        gg +
        mm +
        aaaa +
        ' ' +
        HH +
        ':' +
        MM +
        ':' +
        SS +
        '</label>&nbsp;'
      // html += '<label for="date_aggs' + '">Data: ' + '</label>&nbsp;';
      // html += '<input type="text" class="form-control" name="date_aggs" id="date_aggs" value="'+ gg + mm + aaaa + ' ' + HH + ":" + MM + ":" + SS + '">';
      html += '</div>'
      html += '</div>'
      $('#div-cont-date_aggs').html(html)

      let AggArticles = response.data.Articles
      if (AggArticles.length > 0) {
        let cont = AggArticles.length
        $('#div-cont-img-agg').html('')
        for (var j = 0; j < AggArticles.length; j++) {
          var html = ''
          if (j < 15) {
            // cont = cont + 1;
            html +=
              '<div class="row" data-id-row="' +
              j +
              '" id="divcont-' +
              j +
              '" >'
            html += '<div class="col-2">'
            if (j == 0) {
              html += '<label for="code_gr' + j + '">Articolo</label>&nbsp;'
            }
            html +=
              '<select class="custom-select code_gr_select" id="code_gr' +
              j +
              '" value="' +
              AggArticles[j].code_gr +
              '" name="code_gr' +
              j +
              '"></select>'
            html += '</div>'
            html += '<div class="col-5">'
            if (j == 0) {
              html +=
                '<label for="description' + j + '">Descrizione</label>&nbsp;'
            }
            html +=
              '<input type="text"  id="description' +
              j +
              '" value="' +
              AggArticles[j].description +
              '" name="description' +
              j +
              '" class="form-control description' +
              j +
              '">'
            html += '</div>'
            html += '<div class="col-1">'
            html += '<div class="form-group">'
            html += '<div class="form-check">'
            if (j == 0) {
              html +=
                '<label class="col col-form-label nopadding" for="n_u' +
                j +
                '">N/U</label>&nbsp;'
            }
            html +=
              '<label class="switch switch-pill switch-primary" style="margin: 2px; vertical-align:bottom;"></label>&nbsp;'
            html +=
              '<input type="checkbox" id="form-control n_u' +
              j +
              '" value="' +
              AggArticles[j].n_u +
              '" name="n_u' +
              j +
              '" class="switch-input' +
              j +
              '">'
            html += '</div>'
            html += '</div>'
            html += '</div>'
            html += '<div class="col-1">'
            if (j == 0) {
              html += '<label for="qta' + j + '">Quantita</label>&nbsp;'
            }
            html +=
              '<input type="text" class="form-control qta" id="qta' +
              j +
              '" value="' +
              AggArticles[j].qta +
              '" name="qta' +
              j +
              '">'
            html += '</div>'
            html += '<div class="col-1">'
            if (j == 0) {
              html += '<label for="price_list' + j + '">Pr.Unitario</label>'
            }
            html +=
              '<input type="text" class="form-control price_list" id="price_list' +
              j +
              '" value="' +
              AggArticles[j].price_list +
              '" name="price_list' +
              j +
              '">'
            html += '</div>'
            html += '<div class="col-1">'
            if (j == 0) {
              html += '<label for="tot' + j + '">Totale</label>&nbsp;'
            }
            html +=
              '<input type="text" class="form-control tot" id="tot' +
              j +
              '" value="' +
              AggArticles[j].tot +
              '" name="tot' +
              j +
              '">'
            html += '</div>'
            html += '<div class="col-1 text-right">'
            html +=
              '<i class="fa fa-times del-img-agg" data-id-cont="' + j + '"></i>'
            html += '</div>'
            html += '</div>'
            $('#div-cont-img-agg').append(html)
          }

          $('#frmViewProcessingSheet .tot').on('keyup', function () {
            let contRow = j
            let tot = 0

            $('#frmViewProcessingSheet .tot').each(function (index) {
              tot = parseFloat(tot) + parseFloat($(this).val())
            })
            let consumables = $('#frmViewProcessingSheet #consumables').val()
            let tot_value_config = $(
              '#frmViewProcessingSheet #tot_value_config'
            ).val()
            // CalcolaTotale(tot, tot_value_config, consumables, contRow);

            let totaleFinale =
              parseFloat(tot_value_config) +
              parseFloat(consumables) +
              parseFloat(tot)
            $('#frmViewProcessingSheet .tot_final').val(totaleFinale)
          })

          // $.getScript("https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js").done(function(){
          var searchParam = ''
          let initials = []
          initials.push({
            id: AggArticles[j].code_gr,
            text: AggArticles[j].description_codeGr,
          })
          $('#code_gr' + j).select2({
            data: initials,
            ajax: {
              url: configData.wsRootServicesUrl + '/api/v1/itemGr/selectitemGr',
              data (params) {
                if (params.term == '') {
                  searchParam = '*';
                } else {
                  searchParam = params.term;
                }
                var query = {
                  search: searchParam,
                }
                return query;
              },
              processResults (data) {
                var dataParse = JSON.parse(data);
                return {
                  results: dataParse.data
                };
              }
              },
            },
          })
          //  });
        }
      }

      $('#frmViewProcessingSheet .qta').on('keyup', function () {
        let contRow = $(this).parent('div').parent('div').attr('data-id-row')
        let qta = $(this).val()
        let price_list = $(
          '#frmViewProcessingSheet #price_list' + contRow
        ).val()
        CalcolaTotaleRiga(qta, price_list, contRow)
      })
      $('#frmViewProcessingSheet .price_list').on('keyup', function () {
        let contRow = $(this).parent('div').parent('div').attr('data-id-row')
        let price_list = $(this).val()
        let qta = $('#frmViewProcessingSheet #qta' + contRow).val()

        CalcolaTotaleRiga(qta, price_list, contRow)
      })

      $('#frmViewProcessingSheet .code_gr_select').on('change', function () {
        let id = $(this).val()
        let contRow = $(this).parent('div').parent('div').attr('data-id-row')
        interventionHelpers.getDescriptionAndPriceProcessingSheet(id, contRow)
      })

      function CalcolaTotaleRiga(qta, price_list, contRow) {
        let tot = qta * price_list
        $('#frmViewProcessingSheet #tot' + contRow).val(tot)
      }
    })

    $('#frmViewProcessingSheet').show()
    $('#table_list_processingView').hide()
    $('#row-table-fld-pre').hide()
  })
}

export function updateInterventionUpdateProcessingSheet() {
  spinnerHelpers.show()

  let to_call = false
  if ($('#frmViewProcessingSheet #to_call').is(':checked')) {
    to_call = true
  } else {
    to_call = false
  }

let search_product = false;
  if ($('#frmViewProcessingSheet #search_product').is(':checked')) {
    search_product = true
  } else {
    search_product = false
  }

  let create_quote = false
  if ($('#frmViewProcessingSheet #create_quote').is(':checked')) {
    create_quote = true
  } else {
    create_quote = false
  }
  let escape_from = false
  if ($('#frmViewProcessingSheet #escape_from').is(':checked')) {
    escape_from = true
  } else {
    escape_from = false
  }
  let working = false
  if ($('#frmViewProcessingSheet #working').is(':checked')) {
    working = true
  } else {
    working = false
  }
  if ($('#frmViewProcessingSheet #n_u').is(':checked')) {
    n_u = true
  }

  let data = {
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmViewProcessingSheet #id').val(),
    flaw_detection: $('#frmViewProcessingSheet #flaw_detection').val(),
    done_works: $('#frmViewProcessingSheet #done_works').val(),
    hour: $('#frmViewProcessingSheet #hour').val(),
    value_config_hour: $('#frmViewProcessingSheet #value_config_hour').val(),
    tot_value_config: $('#frmViewProcessingSheet #tot_value_config').val(),
    consumables: $('#frmViewProcessingSheet #consumables').val(),
    tot_final: $('#frmViewProcessingSheet #tot_final').val(),
    date_aggs: $('#frmViewProcessingSheet #date_aggs').val(),
    total_calculated: $('#frmViewProcessingSheet #total_calculated').val(),
    n_u: $('#frmViewProcessingSheet #n_u').val(),
    // n_u: n_u,
    to_call,
    search_product,
    create_quote,
    escape_from,
    working,
    id_intervention: $('#frmViewProcessingSheet #id').val(),
    description: $('#frmViewProcessingSheet #description').val(),
    qta: $('#frmViewProcessingSheet #qta').val(),
    id_gr_items: $('#frmViewProcessingSheet #code_gr').val(),
    price_list: $('#frmViewProcessingSheet #price_list').val(),
    tot: $('#frmViewProcessingSheet #tot').val(),
  }

  let arrayArticles = []
  for (let i = 0; i < 15; i++) {
    let jsonArticles = {}
    let check = false
    if (functionHelpers.isValued($('#description' + i).val())) {
      check = true
      jsonArticles.description = $('#description' + i).val()
    }
    if (functionHelpers.isValued($('#code_gr' + i).val())) {
      check = true
      jsonArticles.code_gr = $('#code_gr' + i).val()
    }
    if (functionHelpers.isValued($('#qta' + i).val())) {
      check = true
      jsonArticles.qta = $('#qta' + i).val()
    }
    if (functionHelpers.isValued($('#price_list' + i).val())) {
      check = true
      jsonArticles.price_list = $('#price_list' + i).val()
    }

    // if(functionHelpers.isValued($('#n_u'+ i).val())){
    // check = true;
    // jsonArticles.push($('#n_u' + i).val());
    // }

    // if(functionHelpers.isValued($('#n_u'+ i).val())){
    // check = true;
    // jsonArticles['n_u'] = $('#n_u' + i).val();
    // }

    if (functionHelpers.isValued($('#tot' + i).val())) {
      check = true
      jsonArticles.tot = $('#tot' + i).val()
    }
    if (check) {
      arrayArticles.push(jsonArticles)
    }
  }

  data.Articles = arrayArticles

  processingviewServices.updateInterventionUpdateProcessingSheet(
    data,
    function () {
      $('#frmViewProcessingSheet #create_quote').val('')
      $('#frmViewProcessingSheet #escape_from').val('')
      $('#frmViewProcessingSheet #working').val('')
      $('#frmViewProcessingSheet #to_call').val('')
      $('#frmViewProcessingSheet #search_product').val('')
      $('#frmViewProcessingSheet #flaw_detection').val('')
      $('#frmViewProcessingSheet #done_works').val('')
      $('#frmViewProcessingSheet #hour').val('')
      $('#frmViewProcessingSheet #value_config_hour').val('')
      $('#frmViewProcessingSheet #tot_value_config').val('')
      $('#frmViewProcessingSheet #consumables').val('')
      $('#frmViewProcessingSheet #tot_final').val('')
      $('#frmViewProcessingSheet #date_aggs').val('')
      $('#frmViewProcessingSheet #total_calculated').val('')
      $('#frmViewProcessingSheet #n_u').val('')
      $('#frmViewProcessingSheet #description').val('')
      $('#frmViewProcessingSheet #qta').val('')
      $('#frmViewProcessingSheet #code_gr').val('')
      $('#frmViewProcessingSheet #price_list').val('')
      $('#frmViewProcessingSheet #tot').val('')

      notificationHelpers.success(
        dictionaryHelpers.getDictionary('ContactRequestCompleted')
      )
      spinnerHelpers.hide()
      window.location.reload()
      $('#table_list_processingView').show()
      $('#frmViewProcessingSheet').hide()
    }
  )
}

export function deleteRow(id) {
  if (confirm('Vuoi eliminare il foglio di lavorazione?')) {
    spinnerHelpers.show()
    processingviewServices.del(id, function (response) {
      notificationHelpers.success(
        dictionaryHelpers.getDictionary('DeleteCompleted')
      )
      spinnerHelpers.hide()
      // window.location.reload();
    })
  }
}

export function getDescriptionAndPriceProcessingSheet(id, contRow) {
  processingviewServices.getDescriptionAndPriceProcessingSheet(
    id,
    function (response) {
      $('#frmViewProcessingSheet #description' + contRow).val(
        response.data.description
      )
      $('#frmViewProcessingSheet #price_list' + contRow).val(
        response.data.price_list
      )
    }
  )
}

/** *   Gestione Materiali   Tabella Fogli di Lavorazione ***/
// inizio sezione fogli di lavorazione su gestione materiali
export function getAllProcessingSearch() {
  spinnerHelpers.show()
  processingviewServices.getAllProcessingSearch(
    storageData.sIdLanguage(),
    function (response) {
      for (let i = 0; i < response.data.length; i++) {
      let newList = "";
        if (
          response.data[i].ready == true ||
          response.data[i].working == true
        ) {
          newList += "<tr style='background-color:#FFA500'>"
        } else {
          newList += "<tr style=''>"
        }
        if (
          (response.data[i].nr_ddt_gr != null &&
            response.data[i].nr_ddt_gr != '-') ||
          (response.data[i].date_ddt_gr != null &&
            response.data[i].date_ddt_gr != '-')
        ) {
          newList += "<tr style='background-color:#fff'>"
        }
        newList += '<td>'
        if (response.data[i].escape_from == true) {
          newList +=
            '<button class="btn btn-green" style="color:#8f00ff;font-size:15px;"><i class="fa fa-circle" style="color:#8f00ff!important;"/></button>'
        }
        if (response.data[i].to_call == true) {
          newList +=
            '<button class="btn btn-green" style="color:#03876e;font-size:15px;"><i class="fa fa-circle" style="color:#03876e!important;"/></button>'
        }
        if (response.data[i].search_product == true) {
          newList +=
            '<button class="btn btn-green" style="color:#00bfff;font-size:15px;"><i class="fa fa-circle" style="color:#00bfff!important;"/></button>'
        }
        newList +=
          '</td><td><button class="btn btn-primary-quotes" onclick="processingviewHelpers.getByViewValue(' +
          response.data[i].id +
          ')"><i class="fa fa-pencil-square-o"/></button></td>'
        newList += '<td>' + response.data[i].id + '</td>'
        newList += '<td>' + response.data[i].descriptionCustomer + '</td>'
        newList += '<td>' + response.data[i].defect
        if (response.data[i].imgmodule != '') {
          newList +=
            '<div class="container"><div id="lightgallery" style="display:flex">'
          newList +=
            "<a href='" +
            response.data[i].imgmodule +
            "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>"
        }
        newList += '<td>' + response.data[i].referent
        newList += '</td>'

        newList +=
          '<td><button class="btn btn-primary-quotes" style="color:black" onclick="processingviewHelpers.getByViewQuotes(' +
          response.data[i].id +
          ')"><i class="fa fa-file-archive-o"/></button></td>'
        newList += '</tr>'

        $('#row-table-fld-pre').show()
        $('#frmViewProcessingSheet').hide()
        $('#table_list_processingSearch tbody').append(newList)
      }
      lightGallery(document.getElementById('lightgallery'))
    }
  )
  spinnerHelpers.hide()
}
// visualizzazione dettaglio preventivo su fdl
export function getByViewQuotes(id) {
  quotesServices.getByViewQuotes(id, function (response) {
    let searchParam = ''
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        if (i == 'id') {
          $('#frmViewQuotes #' + i).html(val)
        } else {
          $('#frmViewQuotes #' + i).attr('checked', 'checked')
        }
      } else if (i == "imgName") {
          if (val != "") {
            $("#frmViewQuotes #imgPreview").attr('src', val);
          } else {
            $("#frmViewQuotes #imgName").remove();
          }
        } else {
          $("#frmViewQuotes #" + i).val(val);
          $("#frmViewQuotes #" + i).html(val);
        }

      let AggNoteBeforeTheQuotes = response.data

      $('#div-cont-note_before_the_quote').html('')
      var html = ''
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-12">'
      html +=
        '<textarea class="form-control" id="note_before_the_quote">' +
        'In riferimento al Vs DDT n. ' +
        AggNoteBeforeTheQuotes.nr_ddt +
        '  del ' +
        AggNoteBeforeTheQuotes.date_ddt +
        ' con la presente siamo ad inviarVi il seguente preventivo di riparazione relativo al Vs Impianto: ' +
        AggNoteBeforeTheQuotes.description +
        '</textarea>'
      html += '</div>'
      html += '</div>'
      $('#div-cont-note_before_the_quote').append(html)

      let AggTotale = response.data

      $('#div-cont-totale').html('')
      var html = ''
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-6">'
      html += '<label for="tot_quote' + '">Totale: ' + '</label>&nbsp;'
      // if(AggTotale.tot_quote==''){
      html +=
        '<input type="text" class="form-control" name="tot_quote" id="tot_quote" value="' +
        '€ ' +
        AggTotale.tot_quote +
        ' Netto + IVA">'
      // }else{
      // html += '<input type="text" class="form-control" name="tot_quote" id="tot_quote" value="'+ AggTotale.tot_quote + '">';
      // }
      html += '</div>'
      html += '</div>'
      $('#div-cont-totale').append(html)

      let data = new Date()
      let gg, mm, aaaa
      gg = data.getDate() + '/'
      mm = data.getMonth() + 1 + '/'
      aaaa = data.getFullYear()

      $('#div-cont-date_agg').html('')
      var html = ''
      html +=
        '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-12">'
      html += '<label for="date_agg' + '">Data: ' + '</label><br>'
      html +=
        '<input type="text" class="form-control" name="date_agg" id="date_agg" value="' +
        gg +
        mm +
        aaaa +
        '">'
      html += '</div>'
      html += '</div>'
      $('#div-cont-date_agg').append(html)

      let AggPaymentQuotes = response.data

      $('#div-cont-payment_quote').html('')
      var html = ''
      html +=
        '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-12">'
      html +=
        '<label for="description_payment' +
        '">Descrizione Pagamento ' +
        '</label>&nbsp;'
      html +=
        '<select class="form-control select-graphic custom-select code_gr_select" id="description_payment' +
        '" value="' +
        AggPaymentQuotes.description_payment +
        '" name="description_payment' +
        '"></select>'
      html += '</div>'
      html += '</div>'
      $('#div-cont-payment_quote').append(html)

      var searchParam = ''
      var initials = []
      // initials.push({id: AggPaymentQuotes.description_payment, text: AggPaymentQuotes.Description_payment});
      $('#description_payment').select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl + '/api/v1/paymentGr/selectpaymentGr',
          data (params) {
            if (params.term == '') {
              searchParam = '*';
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam,
            }
            return query;
          },
          processResults (data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        },
      })

      var searchParam = ''
      $('#description_payment').select2({
        ajax: {
          url:
            configData.wsRootServicesUrl + '/api/v1/paymentGr/selectpaymentGr',
          data (params) {
            if (params.term == '') {
              searchParam = '*';
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam,
            }
            return query;
          },
          processResults (data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
          },
        },
      })

      var searchParam = ''
      var initials = []
      initials.push({
        id: AggPaymentQuotes.id_states_quote,
        text: AggPaymentQuotes.States_Description,
      })
      $('#id_states_quote').select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl +
            '/api/v1/StatesQuotesGr/selectStatesQuotesGr',
          data (params) {
            if (params.term == '') {
              searchParam = '*';
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam,
            }
            return query;
          },
          processResults (data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        },
      })

      let AggArticlesQuotes = response.data.Articles

      $('#div-cont-codegr_description-agg').html('')
      if (AggArticlesQuotes.length > 0) {
        for (let j = 0; j < AggArticlesQuotes.length; j++) {
          var html = ''
          html +=
            '<div class="row no-gutters" data-id-row="' +
            '" id="divcont-' +
            '" >'
          html += '<div class="col-4">'
          // html += '<label for="description' + '">Descrizione: ' + '<span class="description' + '" id="description' + '" value="' + AggArticlesQuotes[j].description + '"</span></label>&nbsp;';
          // html += '<input type="text"  id="description' + '" value="' + AggArticlesQuotes[j].description + '" name="description' + '">';
          html +=
            '<label for="description">Descrizione:<br><span class="description">' +
            AggArticlesQuotes[j].description +
            '</span></label>'
          html += '</div>'
          html += '<div class="col-4">'
          html +=
            '<label for="code_gr">Codice Articolo:<br><span class="code_gr">' +
            AggArticlesQuotes[j].code_gr +
            '</span></label>'
          // html += '<label for="code_gr' + '">Codice Articolo ' + '</label>&nbsp;';
          // html += '<input type="text"  id="code_gr' + '" value="' + AggArticlesQuotes[j].code_gr + '" name="code_gr' + '">';
          html += '</div>'
          html += '</div>'
          $('#div-cont-codegr_description-agg').append(html)
        }
      }
    })
    $('#frmViewQuotes').show()
    $('#row-table-fld-pre').hide()
  })
}
// update sezione gestiona materiali fogli di lavorazione
export function updateInterventionUpdateViewQuotes() {
  spinnerHelpers.show()
  let data = {
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmViewQuotes #id').val(),
    email: $('#frmViewQuotes #email').val(),
    id_states_quote: $('#frmViewQuotes #id_states_quote').val(),
    code_intervention_gr: $('#frmViewQuotes #code_intervention_gr').val(),
    object: $('#frmViewQuotes #object').val(),
    date_agg: $('#frmViewQuotes #date_agg').val(),
    note_before_the_quote: $('#frmViewQuotes #note_before_the_quote').val(),
    note: $('#frmViewQuotes #note').val(),
    description_payment: $('#frmViewQuotes #description_payment').val(),
    tot_quote: $('#frmViewQuotes #tot_quote').val(),
  }

  quotesServices.updateInterventionUpdateViewQuotes(data, function () {
    $('#frmViewQuotes #email').val('')
    $('#frmViewQuotes #id_states_quote').val('')
    $('#frmViewQuotes #object').val('')
    $('#frmViewQuotes #date_agg').val('')
    $('#frmViewQuotes #id_states_quote').val('')
    $('#frmViewQuotes #note_before_the_quote').val('')
    $('#frmViewQuotes #description_payment').val('')
    $('#frmViewQuotes #note').val('')
    $('#frmViewQuotes #tot_quote').val('')
    notificationHelpers.success('Modifica avvenuta con successo')
    spinnerHelpers.hide()
    window.location.reload()
    $('#table_list_quotes').show()
    $('#frmViewQuotes').hide()
  })
}
// end sezione fogli di lavorazione

/* Tabella Preventivi gestione materiali  */
export function getAllViewQuoteSearch() {
  spinnerHelpers.show()
  processingviewServices.getAllViewQuoteSearch(
    storageData.sIdLanguage(),
    function (response) {
      for (let i = 0; i < response.data.length; i++) {
      let newList = "";
        if (
          response.data[i].ready == true ||
          response.data[i].working == true
        ) {
          newList += "<tr style='background-color:#FFA500'>"
        } else {
          newList += "<tr style=''>"
        }
        if (
          (response.data[i].nr_ddt_gr != null &&
            response.data[i].nr_ddt_gr != '-') ||
          (response.data[i].date_ddt_gr != null &&
            response.data[i].date_ddt_gr != '-')
        ) {
          newList += "<tr style='background-color:#fff'>"
        }
        newList +=
          '<td><button class="btn btn-primary-quotes" onclick="processingviewHelpers.getByViewQuotesTable(' +
          response.data[i].id +
          ')"><i class="fa fa-pencil-square-o"/></button></td>'
        newList += '<td>' + response.data[i].id + '</td>'
        newList += '<td>'
        if (response.data[i].imgmodule != '') {
          newList +=
            '<div class="container"><div id="lightgallery" style="display:flex">'
          newList +=
            "<a href='" +
            response.data[i].imgmodule +
            "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>"
        }
        ;+'</td>'

        newList += '<td>' + response.data[i].descriptionCustomer + '</td>'
        newList += '<td>' + response.data[i].description + '</td>'

        newList += '<td>'
        if (response.data[i].id_states_quote == 1) {
          newList +=
            '<button class="btn btn-green" style="color:#ffd700;font-size:15px;"><i class="fa fa-flag-o"/></button>'
        }
        if (response.data[i].id_states_quote == 7) {
          newList +=
            '<button class="btn btn-green" style="color:green;font-size:15px;"><i class="fa fa-flag-o"/></button>'
        }
        if (response.data[i].id_states_quote == 5) {
          newList +=
            '<button class="btn btn-green" style="color:red;font-size:15px;"><i class="fa fa-flag-o"/></button>'
        }
        ;('</td>')
        newList += '</td>'
        newList += '</tr>'

        $('#row-table-fld-pre').show()
        $('#table_list_quotesSearch').show()
        $('#frmViewQuotes').hide()
        $('#table_list_quotesSearch tbody').append(newList)
      }
      lightGallery(document.getElementById('lightgallery'))
    }
  )
  spinnerHelpers.hide()
}

export function getByViewQuotesTable(id) {
  quotesServices.getByViewQuotes(id, function (response) {
    let searchParam = ''
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        if (i == 'id') {
          $('#frmViewQuotes #' + i).html(val)
        } else {
          $('#frmViewQuotes #' + i).attr('checked', 'checked')
        }
      } else if (i == "imgName") {
          if (val != "") {
            $("#frmViewQuotes #imgPreview").attr('src', val);
          } else {
            $("#frmViewQuotes #imgName").remove();
          }
        } else {
          $("#frmViewQuotes #" + i).val(val);
          $("#frmViewQuotes #" + i).html(val);
        }

      let AggNoteBeforeTheQuotes = response.data
      $('#div-cont-note_before_the_quote').html('')
      var html = ''
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-12">'
      html +=
        '<textarea class="form-control" id="note_before_the_quote">' +
        'In riferimento al Vs DDT n. ' +
        AggNoteBeforeTheQuotes.nr_ddt +
        '  del ' +
        AggNoteBeforeTheQuotes.date_ddt +
        ' con la presente siamo ad inviarVi il seguente preventivo di riparazione relativo al Vs Impianto: ' +
        AggNoteBeforeTheQuotes.description +
        '</textarea>'
      html += '</div>'
      html += '</div>'
      $('#div-cont-note_before_the_quote').append(html)

      let AggTotale = response.data
      $('#div-cont-totale').html('')
      var html = ''
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-6">'
      html += '<label for="tot_quote' + '">Totale: ' + '</label>&nbsp;'
      // if(AggTotale.tot_quote==''){
      html +=
        '<input type="text" class="form-control" name="tot_quote" id="tot_quote" value="' +
        '€ ' +
        AggTotale.tot_quote +
        ' Netto + IVA">'
      // }else{
      // html += '<input type="text" class="form-control" name="tot_quote" id="tot_quote" value="'+ AggTotale.tot_quote + '">';
      // }
      html += '</div>'
      html += '</div>'
      $('#div-cont-totale').append(html)

      let data = new Date()
      let gg, mm, aaaa
      gg = data.getDate() + '/'
      mm = data.getMonth() + 1 + '/'
      aaaa = data.getFullYear()

      $('#div-cont-date_agg').html('')
      var html = ''
      html +=
        '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-12">'
      html += '<label for="date_agg' + '">Data: ' + '</label><br>'
      html +=
        '<input type="text" class="form-control" name="date_agg" id="date_agg" value="' +
        gg +
        mm +
        aaaa +
        '">'
      html += '</div>'
      html += '</div>'
      $('#div-cont-date_agg').append(html)

      let AggPaymentQuotes = response.data
      $('#div-cont-payment_quote').html('')
      var html = ''
      html +=
        '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-12">'
      html +=
        '<label for="description_payment' +
        '">Descrizione Pagamento ' +
        '</label>&nbsp;'
      html +=
        '<select class="form-control select-graphic custom-select code_gr_select" id="description_payment' +
        '" value="' +
        AggPaymentQuotes.description_payment +
        '" name="description_payment' +
        '"></select>'
      html += '</div>'
      html += '</div>'
      $('#div-cont-payment_quote').append(html)

      var searchParam = ''
      var initials = []
      initials.push({
        id: AggPaymentQuotes.description_payment,
        text: AggPaymentQuotes.Description_payment,
      })
      $('#description_payment').select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl + '/api/v1/paymentGr/selectpaymentGr',
          data (params) {
            if (params.term == '') {
              searchParam = '*';
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam,
            }
            return query;
          },
          processResults (data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        },
      })

      var searchParam = ''
      $('#description_payment').select2({
        ajax: {
          url:
            configData.wsRootServicesUrl + '/api/v1/paymentGr/selectpaymentGr',
          data (params) {
            if (params.term == '') {
              searchParam = '*';
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam,
            }
            return query;
          },
          processResults (data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        },
      })

      var searchParam = ''
      var initials = []
      initials.push({
        id: AggPaymentQuotes.id_states_quote,
        text: AggPaymentQuotes.States_Description,
      })
      $('#id_states_quote').select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl +
            '/api/v1/StatesQuotesGr/selectStatesQuotesGr',
          data (params) {
            if (params.term == '') {
              searchParam = '*';
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam,
            }
            return query;
          },
          processResults (data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        },
      })

      let AggArticlesQuotes = response.data.Articles
      $('#div-cont-codegr_description-agg').html('')
      if (AggArticlesQuotes.length > 0) {
        for (let j = 0; j < AggArticlesQuotes.length; j++) {
          var html = ''
          html +=
            '<div class="row no-gutters" data-id-row="' +
            '" id="divcont-' +
            '" >'
          html += '<div class="col-4">'
          // html += '<label for="description' + '">Descrizione: ' + '<span class="description' + '" id="description' + '" value="' + AggArticlesQuotes[j].description + '"</span></label>&nbsp;';
          // html += '<input type="text"  id="description' + '" value="' + AggArticlesQuotes[j].description + '" name="description' + '">';
          html +=
            '<label for="description">Descrizione:<br><span class="description">' +
            AggArticlesQuotes[j].description +
            '</span></label>'
          html += '</div>'
          html += '<div class="col-4">'
          html +=
            '<label for="code_gr">Codice Articolo:<br><span class="code_gr">' +
            AggArticlesQuotes[j].code_gr +
            '</span></label>'
          // html += '<label for="code_gr' + '">Codice Articolo ' + '</label>&nbsp;';
          // html += '<input type="text"  id="code_gr' + '" value="' + AggArticlesQuotes[j].code_gr + '" name="code_gr' + '">';
          html += '</div>'
          html += '</div>'
          $('#div-cont-codegr_description-agg').append(html)
        }
      }
    })
    $('#frmViewQuotes').show()
    $('#row-table-fld-pre').hide()
  })
}
/* fine tabella Preventivi gestione materiali  */

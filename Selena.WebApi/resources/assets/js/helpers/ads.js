/** *   adsHelpers   ***/

export function insert() {
  spinnerHelpers.show()
  const link = $('#ads #description').val()
  const a =
    'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
  const b =
    'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------'
  const p = new RegExp(a.split('').join('|'), 'g')

  const linkrewrite = link
    .toString()
    .toLowerCase()
    .replace(/\s+/g, '-') // Replace spaces with -
    .replace(p, (c) => b.charAt(a.indexOf(c))) // Replace special characters
    .replace(/&/g, '-and-') // Replace & with 'and'
    .replace(/[^\w\-]+/g, '') // Remove all non-word characters
    .replace(/\-\-+/g, '-') // Replace multiple - with single -
    .replace(/^-+/, '') // Trim - from start of text
    .replace(/-+$/, '')

  const urlRewrite = '/' + linkrewrite + '-' + storageData.sUserId()

  const data = {
    customer_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    description: $('#ads #description').val(),
    meta_tag_link: urlRewrite,
    price: $('#ads #price').val().replace(',', '.'),
    available: $('#ads #available').val(),
    external_link: $('#ads #external_link').val(),
    new: $('#ads #new').val(),
    free_shipment: $('#ads #free_shipment').val(),
    shipping_price: $('#ads #shipping_price').val(),
    producer_id: $('#ads #producer_id').val(),
    meta_tag_characteristic: $('#ads #meta_tag_characteristic').val(),
    category_id: $('#ads #category_id').val(),
    img: $('#ads #imgBase64').val(),
    imgName: $('#ads #imgName').val(),
  }

  const arrayImgAgg = []
  for (let i = 0; i < 10; i++) {
    if (
      functionHelpers.isValued($('#imgBase64' + i).val()) &&
      functionHelpers.isValued($('#imgName' + i).val())
    ) {
      arrayImgAgg.push(
        JSON.parse(
          '{"imgBase64' +
            i +
            '":"' +
            $('#imgBase64' + i).val() +
            '", "imgName' +
            i +
            '":"' +
            $('#imgName' + i).val() +
            '"}'
        )
      )
    }
  }

  // if(arrayImgAgg.length>0){
  data.imgAgg = arrayImgAgg
  // }

  itemServices.insert(data, function () {
    notificationHelpers.success('Annuncio caricato con successo!')
    setTimeout(function () {
      window.location = '/i-tuoi-annunci'
    }, 2000)
    spinnerHelpers.hide()
  })
}

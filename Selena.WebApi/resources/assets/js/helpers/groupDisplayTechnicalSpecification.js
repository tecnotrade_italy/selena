/** *   groupDisplayTechnicalSpecificationHelpers   ***/

export function onChangeTable(index, value, id, field) {
  const idTable = $('#' + id).closest('table')[0].id
  if (functionHelpers.isValued(value)) {
    groupDisplayTechnicalSpecificationServices.checkExists(
      value,
      field,
      $('#' + idTable).bootstrapTable('getData')[index].id,
      function (result) {
        if (result.data) {
          if (!$('#' + id).hasClass('error')) {
            $('#' + id).addClass('error')
          }
          $('#' + idTable).bootstrapTable('uncheck', index)
          notificationHelpers.error(
            dictionaryHelpers.getDictionary(
              'GroupDisplayTechnicalSpecificationAlreadyExists'
            )
          )
        } else if ($('#' + id).hasClass('error')) {
          $('#' + id).removeClass('error')
        }
      }
    )
  } else if (field == storageData.sIdLanguage()) {
    if (!$('#' + id).hasClass('error')) {
      $('#' + id).addClass('error')
    }
    $('#' + idTable).bootstrapTable('uncheck', index)
    notificationHelpers.error(
      dictionaryHelpers.getDictionary('ThisFieldIsRequired')
    )
  } else if ($('#' + id).hasClass('error')) {
    $('#' + id).removeClass('error')
  }
}

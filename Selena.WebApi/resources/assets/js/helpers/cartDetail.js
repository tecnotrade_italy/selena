export function del(id) {
  spinnerHelpers.show()

  cartDetailServices.del(id, function (result) {
    spinnerHelpers.hide()
    window.location.reload()
  })
}

export function modifyQuantity(id, newQuantity) {
  spinnerHelpers.show()
  const data = {
    id,
    newQuantity,
  }

  cartDetailServices.modifyQuantity(data, function (result) {
    spinnerHelpers.hide()
    window.location.reload()
  })
}

/** *   userAddressHelpers   ***/

export function insert(idForm) {
  spinnerHelpers.show()

  if (functionHelpers.isValued(storageData.sUserId())) {
    const data = functionHelpers.formToJson(idForm)
    data.user_id = storageData.sUserId()
    data.cart_id = storageData.sCartId()

    userAddressServices.insert(data, function () {
      notificationHelpers.success('Indirizzo inserito correttamente!')
      window.$('#modal-add-address').modal('hide')
      spinnerHelpers.hide()
      setTimeout(function () {
        location.reload(true)
      }, 1000)
    })
  } else {
    window.location.href = '/login'
  }
}

export function getAllByUser(idToAppend) {
  spinnerHelpers.show()
  if (functionHelpers.isValued(storageData.sUserId())) {
    userAddressServices.getAllByUser(storageData.sUserId(), function (result) {
      $('#' + idToAppend).html(result.message)

      /* gestione indirizzi di spedizione */
      $('#' + idToAppend + ' .fa-check').on('click', function () {
        const idAddressActive = $(this).attr('data-id-address')

        if (functionHelpers.isValued(idAddressActive)) {
          const data = {
            idAddress: idAddressActive,
            idCart: storageData.sCartId(),
          }
          userAddressServices.activeAddressByUser(data, function (result) {
            window.$('#modal-list-address').modal('hide')
            if (result.data != '') {
              notificationHelpers.success('Indirizzo attivato correttamente!')
              spinnerHelpers.hide()
              setTimeout(function () {
                location.reload(true)
              }, 1000)
            } else {
              spinnerHelpers.hide()
              notificationHelpers.error('Ops, qualcosa è andato storto!')
            }
          })
        } else {
          notificationHelpers.error('Ops, qualcosa è andato storto!')
        }
      })

      $('#' + idToAppend + ' .fa-trash').on('click', function () {
        const idAddressActive = $(this).attr('data-id-address')
        if (
          confirm("Sei sicuro di voler eliminare l'indirizzo di spedizione?")
        ) {
          if (functionHelpers.isValued(idAddressActive)) {
            userAddressServices.deleteAddressById(
              idAddressActive,
              function (result) {
                window.$('#modal-list-address').modal('hide')
                if (result.data != '') {
                  notificationHelpers.success(
                    'Indirizzo eliminato correttamente!'
                  )
                  spinnerHelpers.hide()
                  setTimeout(function () {
                    location.reload(true)
                  }, 1000)
                } else {
                  spinnerHelpers.hide()
                  notificationHelpers.error('Ops, qualcosa è andato storto!')
                }
              }
            )
          } else {
            notificationHelpers.error('Ops, qualcosa è andato storto!')
          }
        }
      })

      /* end gestione indirizzi di spedizione */

      spinnerHelpers.hide()
    })
  } else {
    window.location.href = '/login'
  }
}



export function getAllUteByUserSecurPoint(idToAppend) {
  spinnerHelpers.show()
  if (functionHelpers.isValued(storageData.sUserId())) {
    userAddressServices.getAllUteByUserSecurPoint(storageData.sUserId(), function (result) {
      $('#' + idToAppend).html(result.message);
      spinnerHelpers.hide()
    })
  } else {
    window.location.href = '/login'
  }
}

export function insertUteByUserSecurPoint(idForm) {
  spinnerHelpers.show()

  if (functionHelpers.isValued(storageData.sUserId())) {
    const data = functionHelpers.formToJson(idForm)
    data.user_id = storageData.sUserId()
    data.cart_id = storageData.sCartId()

    userAddressServices.insertUteByUserSecurPoint(data, function () {
      notificationHelpers.success('Utente inserito correttamente!')
      window.$('#modal-add-address').modal('hide')
      spinnerHelpers.hide()
      setTimeout(function () {
        location.reload(true)
      }, 1000)
    })
  } else {
    window.location.href = '/login'
  }
}

export function resetAddressByCartId() {
  spinnerHelpers.show()

  if (functionHelpers.isValued(storageData.sUserId())) {
    var data = {
      user_id: storageData.sUserId(),
      cart_id: storageData.sCartId()
    }

    userAddressServices.resetAddressByCartId(data, function () {
      spinnerHelpers.hide()
      setTimeout(function () {
        location.reload(true)
      }, 1000)
    })
  } else {
    window.location.href = '/login'
  }
}

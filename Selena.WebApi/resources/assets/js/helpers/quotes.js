/** *   quotesHelpers   ***/

export function getAllViewQuote() {
  spinnerHelpers.show()
  quotesServices.getAllViewQuote(
    storageData.sIdLanguage(),
    function (response) {
      for (let i = 0; i < response.data.length; i++) {
      let newList = "";
        newList += '<tr>'
        newList +=
          '<td><button class="btn btn-light" onclick="quotesHelpers.getByViewQuotes(' +
          response.data[i].id +
          ')"><i class="fa fa-circle"/></button></td>'
        newList += '<td>' + response.data[i].id + '</td>'
        newList += '<td>' + response.data[i].descriptionCustomer + '</td>'
        newList += '<td>' + response.data[i].date_aggs + '</td>'
        newList += '<td>' + response.data[i].description + '</td>'
        newList += '<td>' + response.data[i].note + '</td>'
        newList += '<td>' + response.data[i].States_Description + '</td>'
        newList += '</td>'
        newList +=
          '<td><button class="btn btn-danger" onclick="quotesHelpers.deleteRow(' +
          response.data[i].id +
          ')"><i class="fa fa-trash"/></button></td>'
        newList += '</tr>'
        $('#table_list_quotes').show()
        $('#frmViewQuotes').hide()
        $('#table_list_quotes tbody').append(newList)
      }
      lightGallery(document.getElementById('lightgallery'))
    }
  )

  spinnerHelpers.hide()
}

export function getByViewQuotes(id) {
  quotesServices.getByViewQuotes(id, function (response) {
    let searchParam = ''
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        if (i == 'id') {
          $('#frmViewQuotes #' + i).html(val)
        } else {
          $('#frmViewQuotes #' + i).attr('checked', 'checked')
        }
      } else if (i == "imgName") {
          if (val != "") {
            $("#frmViewQuotes #imgPreview").attr('src', val);
          } else {
            $("#frmViewQuotes #imgName").remove();
          }
        } else {
          $("#frmViewQuotes #" + i).val(val);
          $("#frmViewQuotes #" + i).html(val);
        }

      let AggNoteBeforeTheQuotes = response.data
      $('#div-cont-note_before_the_quote').html('')
      var html = ''
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-12">'
      html +=
        '<textarea class="form-control" id="note_before_the_quote">' +
        'In riferimento al Vs DDT n. ' +
        AggNoteBeforeTheQuotes.nr_ddt +
        '  del ' +
        AggNoteBeforeTheQuotes.date_ddt +
        ' con la presente siamo ad inviarVi il seguente preventivo di riparazione relativo al Vs Impianto: ' +
        AggNoteBeforeTheQuotes.description +
        '</textarea>'
      html += '</div>'
      html += '</div>'
      $('#div-cont-note_before_the_quote').append(html)

      let AggTotale = response.data
      $('#div-cont-totale').html('')
      var html = ''
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-6">'
      html += '<label for="tot_quote' + '">Totale: ' + '</label>&nbsp;'
      // if(AggTotale.tot_quote==''){
      html +=
        '<input type="text" class="form-control" name="tot_quote" id="tot_quote" value="' +
        '€ ' +
        AggTotale.tot_quote +
        ' Netto + IVA">'
      // }else{
      // html += '<input type="text" class="form-control" name="tot_quote" id="tot_quote" value="'+ AggTotale.tot_quote + '">';
      // }
      html += '</div>'
      html += '</div>'
      $('#div-cont-totale').append(html)

      let data = new Date()
      let gg, mm, aaaa
      gg = data.getDate() + '/'
      mm = data.getMonth() + 1 + '/'
      aaaa = data.getFullYear()

      $('#div-cont-date_agg').html('')
      var html = ''
      html +=
        '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-12">'
      html += '<label for="date_agg' + '">Data: ' + '</label><br>'
      html +=
        '<input type="text" class="form-control" name="date_agg" id="date_agg" value="' +
        gg +
        mm +
        aaaa +
        '">'
      html += '</div>'
      html += '</div>'
      $('#div-cont-date_agg').append(html)

      let AggPaymentQuotes = response.data
      $('#div-cont-payment_quote').html('')
      var html = ''
      html +=
        '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >'
      html += '<div class="col-12">'
      html +=
        '<label for="description_payment' +
        '">Descrizione Pagamento ' +
        '</label>&nbsp;'
      html +=
        '<select class="form-control select-graphic custom-select code_gr_select" id="description_payment' +
        '" value="' +
        AggPaymentQuotes.description_payment +
        '" name="description_payment' +
        '"></select>'
      html += '</div>'
      html += '</div>'
      $('#div-cont-payment_quote').append(html)

      var searchParam = ''
      var initials = []
      // initials.push({id: AggPaymentQuotes.description_payment, text: AggPaymentQuotes.Description_payment});
      $('#description_payment').select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl + '/api/v1/paymentGr/selectpaymentGr',
          data (params) {
            if (params.term == '') {
              searchParam = '*';
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam,
            }
            return query;
          },
          processResults (data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        },
      })

      var searchParam = ''
      $('#description_payment').select2({
        ajax: {
          url:
            configData.wsRootServicesUrl + '/api/v1/paymentGr/selectpaymentGr',
          data (params) {
            if (params.term == '') {
              searchParam = '*';
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam,
            }
            return query;
          },
          processResults (data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        },
      })

      var searchParam = ''
      var initials = []
      initials.push({
        id: AggPaymentQuotes.id_states_quote,
        text: AggPaymentQuotes.States_Description,
      })
      $('#id_states_quote').select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl +
            '/api/v1/StatesQuotesGr/selectStatesQuotesGr',
          data (params) {
            if (params.term == '') {
              searchParam = '*';
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam,
            }
            return query;
          },
          processResults (data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
          },
        },
      })

      let AggArticlesQuotes = response.data.Articles
      $('#div-cont-codegr_description-agg').html('')
      if (AggArticlesQuotes.length > 0) {
        for (let j = 0; j < AggArticlesQuotes.length; j++) {
          var html = ''
          html +=
            '<div class="row no-gutters" data-id-row="' +
            '" id="divcont-' +
            '" >'
          html += '<div class="col-4">'
          // html += '<label for="description' + '">Descrizione: ' + '<span class="description' + '" id="description' + '" value="' + AggArticlesQuotes[j].description + '"</span></label>&nbsp;';
          // html += '<input type="text"  id="description' + '" value="' + AggArticlesQuotes[j].description + '" name="description' + '">';
          html +=
            '<label for="description">Descrizione:<br><span class="description">' +
            AggArticlesQuotes[j].description +
            '</span></label>'
          html += '</div>'
          html += '<div class="col-4">'
          html +=
            '<label for="code_gr">Codice Articolo:<br><span class="code_gr">' +
            AggArticlesQuotes[j].code_gr +
            '</span></label>'
          // html += '<label for="code_gr' + '">Codice Articolo ' + '</label>&nbsp;';
          // html += '<input type="text"  id="code_gr' + '" value="' + AggArticlesQuotes[j].code_gr + '" name="code_gr' + '">';
          html += '</div>'
          html += '</div>'
          $('#div-cont-codegr_description-agg').append(html)
        }
      }
    })
    $('#frmViewQuotes').show()
    $('#table_list_quotes').hide()
  })
}

export function updateInterventionUpdateViewQuotes() {
  spinnerHelpers.show()
  let data = {
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmViewQuotes #id').val(),
    email: $('#frmViewQuotes #email').val(),
    id_states_quote: $('#frmViewQuotes #id_states_quote').val(),
    code_intervention_gr: $('#frmViewQuotes #code_intervention_gr').val(),  
    object: $('#frmViewQuotes #object').val(),
    date_agg: $('#frmViewQuotes #date_agg').val(),
    note_before_the_quote: $('#frmViewQuotes #note_before_the_quote').val(),
    note: $('#frmViewQuotes #note').val(),
    description_payment: $('#frmViewQuotes #description_payment').val(),
    tot_quote: $('#frmViewQuotes #tot_quote').val(),
  }

  quotesServices.updateInterventionUpdateViewQuotes(data, function () {
    $('#frmViewQuotes #email').val('')
    $('#frmViewQuotes #id_states_quote').val('')
    $('#frmViewQuotes #object').val('')
    $('#frmViewQuotes #date_agg').val('')
    $('#frmViewQuotes #id_states_quote').val('')
    $('#frmViewQuotes #note_before_the_quote').val('')
    $('#frmViewQuotes #description_payment').val('')
    $('#frmViewQuotes #note').val('')
    $('#frmViewQuotes #tot_quote').val('')
    notificationHelpers.success('Modifica avvenuta con successo')
    spinnerHelpers.hide()
    $('#table_list_quotes').show()
    $('#frmViewQuotes').hide()
  })
}

export function deleteRow(id) {
  if (confirm('Vuoi eliminare il preventivo?')) {
    spinnerHelpers.show()
    quotesServices.del(id, function (response) {
      notificationHelpers.success(
        dictionaryHelpers.getDictionary('DeleteCompleted')
      )
      spinnerHelpers.hide()
      window.location.reload()
    })
  }
}

/** *   END POST   ***/

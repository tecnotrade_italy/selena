/** *   userHelpers   ***/

export function rescuePassword() {
  spinnerHelpers.show()
  const email = $('#inputEmail').val()
  const data = {
    email,
  }
  if (functionHelpers.isValued(email)) {
    userServices.rescuePassword(data, function (result) {
      if (result.message == '') {
        notificationHelpers.error('Email non presente nel nostro sistema!')
      } else {
        notificationHelpers.success(
          "Abbiamo inviato all'indirizzo email indicato il link per resettare la password."
        )
      }
      spinnerHelpers.hide()
    })
  }
}

export function changePassword(id) {
  spinnerHelpers.show()
  const password = $('#password').val()
  const confirmPassword = $('#confirmPassword').val()
  const data = {
    password,
    confirmPassword,
    id,
  }
  if (password == confirmPassword) {
    userServices.changePasswordRescued(data, function (result) {
      if (result.message == '') {
        notificationHelpers.error(
          'Attenzione la pagina non è più valida o la password è stata già resettata!'
        )
      } else {
        notificationHelpers.success('Password cambiata correttamente!')
      }
      spinnerHelpers.hide()
    })
  } else {
    spinnerHelpers.hide()
    notificationHelpers.error('Le password non coincidono!')
  }
}

export function checkExistUsername(field, value, rowIndex, id, rowId) {
  spinnerHelpers.show()

  if (functionHelpers.isValued(rowIndex)) {
    window.localStorage.setItem('userHelperId', id)
    if (value != 'admin') {
      userServices.checkExistUsernameNotForThisPage(
        rowId,
        value,
        function (result) {
          if (result.data) {
            validationHelpers.setTableError(
              window.localStorage.getItem('userHelperId'),
              dictionaryHelpers.getDictionary('UsernameAlreadyExist')
            )
          } else {
            $('#' + field).removeClass('error')
          }

          window.localStorage.setItem('userHelperId', '')
        }
      )
    }
    spinnerHelpers.hide()
  } else {
    window.localStorage.setItem('userHelperId', field)
    if (value != 'admin') {
      userServices.checkExistUsername(value, function (result) {
        if (result.data) {
          validationHelpers.setDetailError(
            window.localStorage.getItem('userHelperId'),
            dictionaryHelpers.getDictionary('UsernameAlreadyExist')
          )
        } else {
          $('#' + field).removeClass('error')
        }

        window.localStorage.setItem('userHelperId', '')
      })
    }
    spinnerHelpers.hide()
  }
}

export function checkExistEmail(field, value, rowIndex, id, rowId) {
  spinnerHelpers.show()

  if (functionHelpers.isValued(rowIndex)) {
    window.localStorage.setItem('userHelperId', id)
    userServices.checkExistEmailNotForThisPage(rowId, value, function (result) {
      if (result.data) {
        validationHelpers.setTableError(
          window.localStorage.getItem('userHelperId'),
          dictionaryHelpers.getDictionary('EmailAlreadyExist')
        )
      }

      window.localStorage.setItem('userHelperId', '')
      spinnerHelpers.hide()
    })
  } else {
    window.localStorage.setItem('userHelperId', field)
    userServices.checkExistEmail(value, function (result) {
      if (result.data) {
        validationHelpers.setDetailError(
          window.localStorage.getItem('userHelperId'),
          dictionaryHelpers.getDictionary('EmailAlreadyExist')
        )
      }

      window.localStorage.setItem('userHelperId', '')
      spinnerHelpers.hide()
    })
  }
}

export function availableCheck(id) {
  // spinnerHelpers.show();
  userServices.availableCheck(id, function (result) {
    notificationHelpers.success('Registrazione completata con successo!')
  })
}

/* CREATE USER */
export function insertNoAuth(idForm) {
  spinnerHelpers.show()

  const data = functionHelpers.formToJson(idForm)
  data["idLanguage"] = storageData.sIdLanguage()
  const arrayImgAgg = []

  /* //format orari settimanali
  var html = "";
  html += '<table>'; 
  html += '<tbody>'; 
  html += '<tr>'; 
  html += '<th colspan="4"></th>';
  html += '<tr>'; 
  html += '<td>Lunedì</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#monday").val() + '</td>';
  html += '</tr>'; 
  html += '<tr>'; 
  html += '<td>Martedì</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'; 
  html += '<td>' + $("#tuesday").val() + '</td>';
  html += '</tr>'; 
  html += '<tr>'; 
  html += '<td>Mercoledì</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#wednesday").val() + '</td>';
  html += '</tr>';
  html += '<tr>'; 
  html += '<td>Giovedì</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#thursday").val() + '</td>';
  html += '</tr>';
  html += '<tr>'; 
  html += '<td>Venerdì</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#friday").val() + '</td>';
  html += '</tr>';
  html += '<tr>'; 
  html += '<td>Sabato</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#saturday").val() + '</td>';
  html += '</tr>';
  html += '<tr>'; 
  html += '<td>Domenica</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#sunday").val() + '</td>';
  html += '</tr>';
  html += '</tr>'; 
  html += '</tbody>'; 
  html += '</table>';  

   data.shop_time = html;
  //end orari settimanali */

  // ciclo img. aggiuntive
  for (var i = 0; i < 10; i++) {
    if (
      functionHelpers.isValued(data['imgBase64' + i]) &&
      functionHelpers.isValued(data['imgName' + i])
    ) {
      arrayImgAgg.push(
        JSON.parse(
          '{"imgBase64' +
            i +
            '":"' +
            data['imgBase64' + i] +
            '", "imgName' +
            i +
            '":"' +
            data['imgName' + i] +
            '"}'
        )
      )
    }
  }
  data.imgAgg = arrayImgAgg
  // end ciclo img.aggiuntive

  // SEZIONE MAPS
  const arrayMapAgg = []
  for (var i = 0; i < 5; i++) {
    const jsonMapAgg = {}
    let check = false
    if (functionHelpers.isValued($('#user_id' + i).val())) {
      check = true
      jsonMapAgg.user_id = $('#user_id' + i).val()
    }
    if (functionHelpers.isValued($('#maps' + i).val())) {
      check = true
      jsonMapAgg.maps = $('#maps' + i).val()
    }
    if (functionHelpers.isValued($('#address' + i).val())) {
      check = true
      jsonMapAgg.address = $('#address' + i).val()
    }
    if (functionHelpers.isValued($('#codepostal' + i).val())) {
      check = true
      jsonMapAgg.codepostal = $('#codepostal' + i).val()
    }
    if (functionHelpers.isValued($('#country' + i).val())) {
      check = true
      jsonMapAgg.country = $('#country' + i).val()
    }
    if (functionHelpers.isValued($('#province' + i).val())) {
      check = true
      jsonMapAgg.province = $('#province' + i).val()
    }
    if (check) {
      arrayMapAgg.push(jsonMapAgg)
    }
  }
  data.MapAgg = arrayMapAgg

  userServices.insertNoAuth(data, function (result) {
    notificationHelpers.success('Registrazione completata con successo!')
    setTimeout(function () {
      window.location = '/notification-of-receipt-of-user-confirmation-email'
    }, 2000)
    spinnerHelpers.hide()
  })
}
// END SEZIONE MAPS

/* END CREATE USER */

/* GET ALL USER */
export function getAllUser() {
  spinnerHelpers.show()
  userServices.getAllUser(storageData.sIdLanguage(), function (response) {
    for (let i = 0; response.data.length; i++) {
      let newList = ''
      newList += '<response.data.leng<tr>'
      newList +=
        '<td><button class="btn btn-info" onclick="userHelpers.getByIdResult(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil"/></button></td>'
      newList += '<td>' + response.data[i].id + '</td>'
      newList += '<td>' + response.data[i].subscriber + '</td>'
      newList += '<td>' + response.data[i].expiration_date + '</td>'
      newList += '<td>' + response.data[i].subscription_date + '</td>'
      newList += '<td>' + response.data[i].business_name + '</td>'
      newList += '<td>' + response.data[i].name + '</td>'
      newList += '<td>' + response.data[i].telephone_number + '</td>'
      newList += '<td>' + response.data[i].mobile_number + '</td>'
      newList += '<td>' + response.data[i].fax_number + '</td>'
      newList += '<td>' + response.data[i].email + '</td>'

      // newList += "<td>" + response.data[i].username + "</td>";
      // newList += "<td>" + response.data[i].password + "</td>";
      // newList += "<td>" + response.data[i].language_id + "</td>";
      // newList += "<td>" + response.data[i].surname + "</td>";
      // newList += "<td>" + response.data[i].vat_number + "</td>";
      // newList += "<td>" + response.data[i].fiscal_code + "</td>";
      // newList += "<td>" + response.data[i].address + "</td>";
      // newList += "<td>" + response.data[i].codepostal + "</td>";
      // newList += "<td>" + response.data[i].country + "</td>";
      // newList += "<td>" + response.data[i].province + "</td>";
      // newList += "<td>" + response.data[i].website + "</td>";
      // newList += "<td>" + response.data[i].vat_type_id + "</td>";
      //  newList += "<td>" + response.data[i].pec + "</td>";
      newList +=
        '<td><button class="btn btn-danger" onclick="userHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash"/></button></td>'
      newList += '</tr>'

      $('#table_list_user tbody').append(newList)
      $('#table_list_user').show()
      $('#frmAddReferent').hide()
      $('#table_list_searchuser').hide()
      $('#frmUpdateUser').hide()
      $('#frmUpdateReferent').hide()
      $('#table_list_people').hide()
      $('#nav-tab').hide()
      $('.title-modifica-utente').hide()
    }
  })
  spinnerHelpers.hide()
}

export function getByIdResult(id) {
  // spinnerHelpers.show();
  userServices.getByIdResult(id, function (response) {
    $('#frmSearchUser').hide()
    $('#table_list_searchuser').hide()
    let searchParam = ''

    // select vattype
    const initials = []
    initials.push({
      id: response.data.vat_type_id,
      text: response.data.descriptionVatType,
    })
    $('#vat_type_id').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/vattype/select',
        data(params) {
          if (params.term == '') {
            searchParam = '*'
          } else {
            searchParam = params.term
          }
          const query = {
            search: searchParam,
          }
          return query
        },
        processResults(data) {
          const dataParse = JSON.parse(data)

          return {
            results: dataParse.data,
          }
        },
      },
    })
    // get all user
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        $('#frmUpdateUser #subscriber' + i).attr('checked', 'checked')
      } else {
        $('#frmUpdateUser #' + i).val(val)
      }
    })

    peopleHelpers.getPeopleUser(id)
    $('#frmAddReferent #user_id').val(id)
    $('#frmUpdateReferent #user_id').val(id)
    $('#frmUpdateUser').show()
    $('#frmAddReferent').hide()
    $('#nav-tab').show()
    $('#table_list_user').hide()
    $('#btnadd').hide()

    $('.title-modifica-utente').show()
    $('.title-modifica-cliente').hide()
    // $("#frmAddReferent").hide();
  })

  // peopleHelpers.getPeopleUser($("#id").val());
}

export function updateUser() {
  spinnerHelpers.show()
  const data = {
    // id: storageData.sUserId(),
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmUpdateUser #id').val(),
    username: $('#frmUpdateUser #username').val(),
    email: $('#frmUpdateUser #email').val(),
    password: $('#frmUpdateUser #password').val(),
    business_name: $('#frmUpdateUser #business_name').val(),
    name: $('#frmUpdateUser #name').val(),
    surname: $('#frmUpdateUser #surname').val(),
    vat_number: $('#frmUpdateUser #vat_number').val(),
    fiscal_code: $('#frmUpdateUser #fiscal_code').val(),
    address: $('#frmUpdateUser #address').val(),
    codepostal: $('#frmUpdateUser #codepostal').val(),
    country: $('#frmUpdateUser #country').val(),
    province: $('#frmUpdateUser #province').val(),
    website: $('#frmUpdateUser #website').val(),
    vat_type_id: $('#frmUpdateUser #vat_type_id').val(),
    telephone_number: $('#frmUpdateUser #telephone_number').val(),
    fax_number: $('#frmUpdateUser #fax_number').val(),
    mobile_number: $('#frmUpdateUser #mobile_number').val(),
    pec: $('#frmUpdateUser #pec').val(),
    subscriber: $('#frmUpdateUser #subscriber').val(),
    subscription_date: $('#frmUpdateUser #subscription_date').val(),
    expiration_date: $('#frmUpdateUser #expiration_date').val(),
  }

  userServices.updateUser(data, function () {
    notificationHelpers.success('Utente modificato con successo!')
    spinnerHelpers.hide()
    $('#table_list_user').show()
    $('#frmUpdateUser').hide()
    // window.location.reload();
  })
}

export function insertUser() {
  spinnerHelpers.show()

  const data = {
    created_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmAddUser #id').val(),
    username: $('#frmAddUser #username').val(),
    email: $('#frmAddUser #email').val(),
    password: $('#frmAddUser #password').val(),
    business_name: $('#frmAddUser #business_name').val(),
    name: $('#frmAddUser #name').val(),
    surname: $('#frmAddUser #surname').val(),
    vat_number: $('#frmAddUser #vat_number').val(),
    fiscal_code: $('#frmAddUser #fiscal_code').val(),
    address: $('#frmAddUser #address').val(),
    codepostal: $('#frmAddUser #codepostal').val(),
    country: $('#frmAddUser #country').val(),
    province: $('#frmAddUser #province').val(),
    website: $('#frmAddUser #website').val(),
    vat_type_id: $('#frmAddUser #vat_type_id').val(),
    telephone_number: $('#frmAddUser #telephone_number').val(),
    fax_number: $('#frmAddUser #fax_number').val(),
    mobile_number: $('#frmAddUser #mobile_number').val(),
    pec: $('#frmAddUser #pec').val(),
    subscriber: $('#frmAddUser #subscriber').val(),
    subscription_date: $('#frmAddUser #subscription_date').val(),
    expiration_date: $('#frmAddUser #expiration_date').val(),
  }

  userServices.insertUser(data, function (result) {
    $('#frmAddUser #id').val('')
    $('#frmAddUser #username').val('')
    $('#frmAddUser #email').val('')
    $('#frmAddUser #password').val('')
    $('#frmAddUser #business_name').val('')
    $('#frmAddUser #name').val('')
    $('#frmAddUser #surname').val('')
    $('#frmAddUser #vat_number').val('')
    $('#frmAddUser #fiscal_code').val('')
    $('#frmAddUser #address').val('')
    $('#frmAddUser #codepostal').val('')
    $('#frmAddUser #country').val('')
    $('#frmAddUser #province').val('')
    $('#frmAddUser #website').val('')
    $('#frmAddUser #vat_type_id').val('')
    $('#frmAddUser #telephone_number').val('')
    $('#frmAddUser #fax_number').val('')
    $('#frmAddUser #mobile_number').val('')
    $('#frmAddUser #pec').val('')
    $('#frmAddUser #subscriber').val('')
    $('#frmAddUser #subscription_date').val('')
    $('#frmAddUser #expiration_date').val('')
    notificationHelpers.success('Registrazione completata con successo!')
    spinnerHelpers.hide()
  })
}

export function deleteRow(id) {
  spinnerHelpers.show()
  userServices.delet(id, function (response) {
    notificationHelpers.success(
      dictionaryHelpers.getDictionary('DeleteCompleted')
    )
    spinnerHelpers.hide()
    window.location.reload()
  })
}

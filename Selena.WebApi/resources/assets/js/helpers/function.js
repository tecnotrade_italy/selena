/** *   functionHelpers   ***/

// Return if element is valued
export function isValued(value) {
  if (
    value !== undefined &&
    value !== '' &&
    value !== null &&
    value !== 'undefined' &&
    typeof value !== 'undefined'
  ) {
    return true
  } else {
    return false
  }
}

// Return if element is function
export function isFunction(value) {
  if ($.isFunction(value)) {
    return true
  } else {
    return false
  }
}

// Return json without null value
export function clearNullOnJson(object) {
  $.each(object, function (i, value) {
    if (value == null) {
      delete object[i]
    }
  })

  return object
}

// Return if element is numeric
export function isNumeric(value) {
  return !isNaN(value)
}

// Replace all
export function replaceAll(stringValue, stringSearch, stringReplace) {
  return stringValue.replace(new RegExp(stringSearch, 'g'), stringReplace)
}

// Remove quotes
export function stringRemoveQuotes(value) {
  if (functionHelpers.isNumeric(value)) {
    return value
  } else {
    return value.replace(new RegExp("#\\'#", 'g'), "'")
  }
}

// Date formatter
export function dateFormatter(value) {
  if (functionHelpers.isValued(value)) {
    return DateFormat.format.date(value, storageData.sDatePickerFormat())
  }
}

// Date time formatter
export function dateTimeFormatter(value) {
  if (functionHelpers.isValued(value)) {
    return DateFormat.format.date(value, storageData.sDateTimePickerFormat())
  }
}

// Date from IT to EN
export function dateFromITtoEN(date) {
  if (functionHelpers.isValued(date)) {
    if (date.split(' ').length > 1) {
      var d = date.split(' ')[0].split('/')
      const t = date.split(' ')[1].split(':')
      var day = '' + d[0]
      var month = '' + d[1]
      var year = d[2]
      const hour = t[0]
      const minute = t[1]
      const second = t[2]

      if (month.length < 2) month = '0' + month
      if (day.length < 2) day = '0' + day

      return (
        [year, month, day].join('-') + 'T' + [hour, minute, second].join(':')
      )
    } else {
      var d = date.split('/')
      var day = '' + d[0]
      var month = '' + d[1]
      var year = d[2]

      if (month.length < 2) month = '0' + month
      if (day.length < 2) day = '0' + day

      return [year, month, day].join('-')
    }
  } else {
  }
}

// Date to JSON formatter
export function dateToJsonFormatter(date) {
  if (functionHelpers.isValued(date)) {
    return functionHelpers.dateFromITtoEN(date)
  } else {
    return null
  }
}

export function scrollToTop(scrollDuration) {
  const scrollStep = -window.scrollY / (scrollDuration / 15)
  var scrollInterval = setInterval(function () {
    if (window.scrollY != 0) {
      window.scrollBy(0, scrollStep)
    } else clearInterval(scrollInterval)
  }, 15)
}

// Validate email address
export function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}

export function formToJson(form) {
  const dataForm = $('#' + form).serializeArray()
  const data = {}
  $.map(dataForm, function (n, i) {
    data[n.name] = n.value
  })
  return data
}

export function insertPaginator(form) {}

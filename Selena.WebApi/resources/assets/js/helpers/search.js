/** *   searchHelpers   ***/

/** *   POST   ***/

export function search(pageActive) {
  spinnerHelpers.show()
  let page = 1
  if (pageActive != '' || pageActive != undefined) {
    page = pageActive
  }
  const data = {
    search: $('#frmSearch #search').val(),
    idLanguage: storageData.sIdLanguage(),
    autocomplete: false,
    paginator: page,
  }

  let html = ''

  searchServices.search(data, function (response) {
    let htmlPaginator = ''
    let minus10Page = 1
    let plus10Page = 1
    let minus1Page = 1
    let plus1Page = 1
    const pageSelected = parseInt(response.data.pageSelected)
    const maxPage = response.data.totalPages
    window.history.pushState(
      'ricerca',
      'Ricerca',
      '/ricerca?r=' + $('#frmSearch #search').val() + '&p=' + page
    )

    html += "<div class='container div-cont-search'>"
    html += "<div class='row'>"
    html += "<div class='col-12'>"
    html += "<h1 class='div-title-search'>Ricerca</h1>"
    html += '</div>'
    html += '</div>'
    html += '<div class="row no-gutters div-paginator">'
    html += '<div class="col-12">'
    html += '<hr>'
    html += '</div>'
    html += '<div class="col-md-6 col-sm-12">'
    html += '<h5>Articoli presenti: <b class="totItems">&nbsp;</b></h5>'
    html += '</div>'
    html += '<div class="col-md-6 col-sm-12">'
    html += '<div class="paginationCont" style="float:right;">'
    html += '<br>'
    html += '</div>'
    html += '</div>'
    html += '</div>'
    html += "<div class='row'>"
    html += response.data.html
    html += '</div>'
    html += '</div>'

    $('.mainPages').html(html)

    /* funzione gestione paginatore */
    if (response.data.paginationActive) {
      htmlPaginator += "<ul class='pagination'>"
      /* controllo freccia -10 pagine */
      if (pageSelected - 10 <= 1) {
        minus10Page = 1
      } else {
        minus10Page = pageSelected - 10
      }

      htmlPaginator +=
        '<li class="page-item" data-page="' +
        minus10Page +
        '"><a><i class="fa fa-angle-double-left"></i></a></li>'
      /* end controllo freccia -10 pagine */
      /* controllo freccia -1 pagine */
      if (pageSelected - 1 <= 1) {
        minus1Page = 1
      } else {
        minus1Page = pageSelected - 1
      }

      htmlPaginator +=
        '<li class="page-item" data-page="' +
        minus1Page +
        '"><a><i class="fa fa-angle-left"></i></a></li>'
      /* end controllo freccia -1 pagine */

      /* controllo per pagine intermedie */
      if (maxPage > 4) {
        if (pageSelected > 1) {
          if (pageSelected + 2 > maxPage) {
            if (pageSelected == maxPage) {
              for (var j = pageSelected - 3; j <= maxPage; j++) {
                if (pageSelected == j) {
                  htmlPaginator +=
                    '<li class="page-item active" data-page="' +
                    j +
                    '"><a>' +
                    j +
                    '</a></li>'
                } else {
                  htmlPaginator +=
                    '<li class="page-item" data-page="' +
                    j +
                    '"><a>' +
                    j +
                    '</a></li>'
                }
              }
            } else {
              for (var j = pageSelected - 2; j <= maxPage; j++) {
                if (pageSelected == j) {
                  htmlPaginator +=
                    '<li class="page-item active" data-page="' +
                    j +
                    '"><a>' +
                    j +
                    '</a></li>'
                } else {
                  htmlPaginator +=
                    '<li class="page-item" data-page="' +
                    j +
                    '"><a>' +
                    j +
                    '</a></li>'
                }
              }
            }
          } else {
            for (var j = pageSelected - 1; j <= pageSelected + 2; j++) {
              if (pageSelected == j) {
                htmlPaginator +=
                  '<li class="page-item active" data-page="' +
                  j +
                  '"><a>' +
                  j +
                  '</a></li>'
              } else {
                htmlPaginator +=
                  '<li class="page-item" data-page="' +
                  j +
                  '"><a>' +
                  j +
                  '</a></li>'
              }
            }
          }
        } else {
          for (var j = 1; j <= 4; j++) {
            if (pageSelected == j) {
              htmlPaginator +=
                '<li class="page-item active" data-page="' +
                j +
                '"><a>' +
                j +
                '</a></li>'
            } else {
              htmlPaginator +=
                '<li class="page-item" data-page="' +
                j +
                '"><a>' +
                j +
                '</a></li>'
            }
          }
        }
      } else {
        for (var j = 1; j <= maxPage; j++) {
          if (pageSelected == j) {
            htmlPaginator +=
              '<li class="page-item active" data-page="' +
              j +
              '"><a>' +
              j +
              '</a></li>'
          } else {
            htmlPaginator +=
              '<li class="page-item" data-page="' +
              j +
              '"><a>' +
              j +
              '</a></li>'
          }
        }
      }
      /* end controllo per pagine intermedie */

      /* controllo freccia + 1 pagine */
      if (pageSelected + 1 >= maxPage) {
        plus1Page = maxPage
      } else {
        plus1Page = pageSelected + 1
      }

      htmlPaginator +=
        '<li class="page-item" data-page="' +
        plus1Page +
        '"><a><i class="fa fa-angle-right"></i></a></li>'
      /* end controllo freccia + 1 pagine */
      /* controllo freccia + 10 pagine */
      if (pageSelected + 10 >= maxPage) {
        plus10Page = maxPage
      } else {
        plus10Page = pageSelected + 10
      }

      htmlPaginator +=
        '<li class="page-item" data-page="' +
        plus10Page +
        '"><a><i class="fa fa-angle-double-right"></i></a></li>'
      /* end controllo freccia + 10 pagine */

      htmlPaginator += '</ul>'
      $('.paginationCont').html(htmlPaginator)
    } else {
      $('.paginationCont').html('')
    }
    $('.totItems').html(response.data.totItems)
    /* end funzione gestione paginatore */
    $('.pagination li').on('click', function () {
      searchHelpers.search($(this).attr('data-page'))
    })

    spinnerHelpers.hide()
    $('#searchAutocomplete').hide('fast')
  })
}

export function searchAutocomplete() {
  const data = {
    search: $('#frmSearch #search').val(),
    idLanguage: storageData.sIdLanguage(),
    autocomplete: true,
  }

  searchServices.search(data, function (response) {
    $('#searchAutocomplete').html(
      "<i class='fa fa-times icon-close-search'></i><div class='row'>" +
        response.data.html +
        '</div>'
    )
    $('#searchAutocomplete').show()
    $('#searchAutocomplete .icon-close-search').on('click', function () {
      $('#searchAutocomplete').hide()
    })
  })
}

export function searchImplemented(idForm) {
  spinnerHelpers.show()
  const data = functionHelpers.formToJson(idForm)
  data.idLanguage = storageData.sIdLanguage()
  let html = ''
  searchServices.searchImplemented(data, function (response) {
    // window.history.pushState('ricerca', 'Ricerca', '/ricerca-avanzata?r=' + $('#frmSearch #search').val());
    html += "<div class='container div-cont-search'>"
    html += "<div class='row'>"
    html += response.message
    html += '</div>'
    html += '</div>'

    $('#result').html(html)
    spinnerHelpers.hide()
  })
}

export function searchGeneral(data) {

  searchServices.searchGeneral(data, function (response) {
    console.log(response)
    $('#searchAutocomplete').html("<i class='fa fa-times icon-close-search'></i><div class='row'>" + response.message + "</div>")
    $('#searchAutocomplete').show()
    $('#searchAutocomplete .icon-close-search').on('click',function(){
    $('#searchAutocomplete').hide()
    });
  });
}


/** *   END POST   ***/

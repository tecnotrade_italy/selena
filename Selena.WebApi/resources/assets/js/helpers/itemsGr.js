export function getAll(id) {
  spinnerHelpers.show()
  itemGrServices.getAll(id, function (response) {
    for (let i = 0; i < response.data.length; i++) {
      let newList = ''
      newList += '<tr>'
      newList +=
        '<td><button class="btn btn-info" onclick="itemsGrHelpers.getById(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil"/></button>'
      newList +=
        '<button class="btn btn-danger" onclick="itemsGrHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash"/></button></td>'
      newList += '<td>' + response.data[i].id + '</td>'
      newList += '<td>' + response.data[i].tipology + '</td>'
      newList += '<td>' + response.data[i].note + '</td>'
      // newList += "<td><a href='" + response.data[i].img + "'</a>></td>";
      newList +=
        '<div class="container"><div id="lightgallery" style="display:flex">'
      newList +=
        "<td><a href='" +
        response.data[i].img +
        "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></td></div></div>"
      newList += '</td>'
      newList += '<td>' + response.data[i].original_code + '</td>'
      newList += '<td>' + response.data[i].code_product + '</td>'
      newList += '<td>' + response.data[i].code_gr + '</td>'
      newList += '<td>' + response.data[i].plant + '</td>'
      newList += '<td>' + response.data[i].brand + '</td>'
      newList += '<td>' + response.data[i].business_name_supplier + '</td>'
      newList += '<td>' + response.data[i].code_supplier + '</td>'
      newList += '<td>' + response.data[i].price_purchase + '</td>'
      newList += '<td>' + response.data[i].price_list + '</td>'
      newList += '<td>' + response.data[i].repair_price + '</td>'
      newList += '<td>' + response.data[i].price_new + '</td>'
      newList += '<td>' + response.data[i].update_date + '</td>'
      newList += '<td>' + response.data[i].usual_supplier + '</td>'
      newList += '<td>' + response.data[i].pr_rip_conc + '</td>'

      newList += '</tr>'

      $('#table_list_itemsGR tbody').append(newList)
      $('#frmUpdateItemsGr').hide()
      $('#table_list_searchitems').hide()
      $('#table_list_itemsGR').show()
    }
    lightGallery(document.getElementById('lightgallery'))
  })
  spinnerHelpers.hide()
}

export function getById(id) {
  $('#frmUpdateItemsGr #id').val(id)
  // spinnerHelpers.show();
  itemGrServices.getById(id, function (response) {
    let searchParam = ''

    /* SELECT BRAND */
    var initials = []
    initials.push({ id: response.data.brand, text: response.data.Brand })
    $('#brand').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/brand/select',
        data(params) {
          if (params.term == '') {
            searchParam = '*'
          } else {
            searchParam = params.term
          }
          const query = {
            search: searchParam,
          }
          return query
        },
        processResults(data) {
          const dataParse = JSON.parse(data)

          return {
            results: dataParse.data,
          }
        },
      },
    })

    var initials = []
    $('#brand').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/brand/select',
        data(params) {
          if (params.term == '') {
            searchParam = '*'
          } else {
            searchParam = params.term
          }
          const query = {
            search: searchParam,
          }
          return query
        },
        processResults(data) {
          const dataParse = JSON.parse(data)

          return {
            results: dataParse.data,
          }
        },
      },
    })
    /* END SELECT brand */

    /* SELECT business_name_supplier */
    var initials = []
    initials.push({
      id: response.data.business_name_supplier,
      text: response.data.BusinessName,
    })
    $('#business_name_supplier').select2({
      data: initials,
      ajax: {
        url:
          configData.wsRootServicesUrl + '/api/v1/businessnamesupplier/select',
        data(params) {
          if (params.term == '') {
            searchParam = '*'
          } else {
            searchParam = params.term
          }
          const query = {
            search: searchParam,
          }
          return query
        },
        processResults(data) {
          const dataParse = JSON.parse(data)

          return {
            results: dataParse.data,
          }
        },
      },
    })

    var initials = []
    $('#business_name_supplier').select2({
      data: initials,
      ajax: {
        url:
          configData.wsRootServicesUrl + '/api/v1/businessnamesupplier/select',
        data(params) {
          if (params.term == '') {
            searchParam = '*'
          } else {
            searchParam = params.term
          }
          const query = {
            search: searchParam,
          }
          return query
        },
        processResults(data) {
          const dataParse = JSON.parse(data)

          return {
            results: dataParse.data,
          }
        },
      },
    })
    /* END SELECT business_name_supplier */

    /* SELECT tipology */
    var initials = []
    initials.push({ id: response.data.tipology, text: response.data.Tipology })
    $('#tipology').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/tipology/select',
        data(params) {
          if (params.term == '') {
            searchParam = '*'
          } else {
            searchParam = params.term
          }
          const query = {
            search: searchParam,
          }
          return query
        },
        processResults(data) {
          const dataParse = JSON.parse(data)

          return {
            results: dataParse.data,
          }
        },
      },
    })

    var initials = []
    $('#tipology').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/tipology/select',
        data(params) {
          if (params.term == '') {
            searchParam = '*'
          } else {
            searchParam = params.term
          }
          const query = {
            search: searchParam,
          }
          return query
        },
        processResults(data) {
          const dataParse = JSON.parse(data)

          return {
            results: dataParse.data,
          }
        },
      },
    })
    /* END SELECT tipology */

    /*  SELECT ItemsGr */
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        $('#frmUpdateItemsGr #' + i).attr('checked', 'checked')
      } else if (i == 'imgName') {
        if (val != '') {
          $('#frmUpdateItemsGr #imgPreview').attr('src', val)
        } else {
          $('#frmUpdateItemsGr #imgPreview').remove()
        }
      } else {
        $('#frmUpdateItemsGr #' + i).val(val)
      }
    })
    $('#frmUpdateItemsGr').show()
    $('#table_list_itemsGR').hide()
    $('#frmSearchItems').hide()
    $('#table_list_searchitems').hide()
  })
}

export function update() {
  spinnerHelpers.show()
  const data = {
    // id: storageData.sUserId(),
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmUpdateItemsGr #id').val(),
    tipology: $('#frmUpdateItemsGr #tipology').val(),
    note: $('#frmUpdateItemsGr #note').val(),
    original_code: $('#frmUpdateItemsGr #original_code').val(),
    code_product: $('#frmUpdateItemsGr #code_product').val(),
    code_gr: $('#frmUpdateItemsGr #code_gr').val(),
    plant: $('#frmUpdateItemsGr #plant').val(),
    brand: $('#frmUpdateItemsGr #brand').val(),
    business_name_supplier: $(
      '#frmUpdateItemsGr #business_name_supplier'
    ).val(),
    code_supplier: $('#frmUpdateItemsGr #code_supplier').val(),
    price_purchase: $('#frmUpdateItemsGr #price_purchase').val(),
    price_list: $('#frmUpdateItemsGr #price_list').val(),
    repair_price: $('#frmUpdateItemsGr #repair_price').val(),
    price_new: $('#frmUpdateItemsGr #price_new').val(),
    update_date: $('#frmUpdateItemsGr #update_date').val(),
    usual_supplier: $('#frmUpdateItemsGr #usual_supplier').val(),
    pr_rip_conc: $('#frmUpdateItemsGr #pr_rip_conc').val(),
    img: $('#frmUpdateItemsGr #imgBase64').val(),
    imgName: $('#frmUpdateItemsGr #imgName').val(),
  }
  itemGrServices.update(data, function () {
    $('#frmUpdateItemsGr #id').val('')
    $('#frmUpdateItemsGr #tipology').val('')
    $('#frmUpdateItemsGr #note').val('')
    $('#frmUpdateItemsGr #original_code').val('')
    $('#frmUpdateItemsGr #code_product').val('')
    $('#frmUpdateItemsGr #code_gr').val('')
    $('#frmUpdateItemsGr #plant').val('')
    $('#frmUpdateItemsGr #brand').val('')
    $('#frmUpdateItemsGr #business_name_supplier').val('')
    $('#frmUpdateItemsGr #code_supplier').val('')
    $('#frmUpdateItemsGr #price_purchase').val('')
    $('#frmUpdateItemsGr #price_list').val('')
    $('#frmUpdateItemsGr #repair_price').val('')
    $('#frmUpdateItemsGr #price_new').val('')
    $('#frmUpdateItemsGr #update_date').val('')
    $('#frmUpdateItemsGr #usual_supplier').val('')
    $('#frmUpdateItemsGr #imgBase64').val('')
    $('#frmUpdateItemsGr #pr_rip_conc').val('')

    notificationHelpers.success(
      dictionaryHelpers.getDictionary('ContactRequestCompleted')
    )
    spinnerHelpers.hide()
    window.location.reload()
    $('#table_list_itemsGR').show()
    $('#frmUpdateItemsGr').hide()
  })
}

export function deleteRow(id) {
  if (confirm("Vuoi eliminare l'articolo?")) {
    spinnerHelpers.show()
    itemGrServices.del(id, function (response) {
      notificationHelpers.success(
        dictionaryHelpers.getDictionary('DeleteCompleted')
      )
      spinnerHelpers.hide()
      window.location.reload()
    })
  }
}

export function insertNoAuth() {
  spinnerHelpers.show()

  const data = {
    created_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    img: $('#frmAddItemsGr #imgBase64').val(),
    imgName: $('#frmAddItemsGr #imgName').val(),
    tipology: $('#frmAddItemsGr #tipology').val(),
    note: $('#frmAddItemsGr #note').val(),
    original_code: $('#frmAddItemsGr #original_code').val(),
    code_product: $('#frmAddItemsGr #code_product').val(),
    code_gr: $('#frmAddItemsGr #code_gr').val(),
    plant: $('#frmAddItemsGr #plant').val(),
    brand: $('#frmAddItemsGr #brand').val(),
    business_name_supplier: $('#frmAddItemsGr #business_name_supplier').val(),
    code_supplier: $('#frmAddItemsGr #code_supplier').val(),
    price_purchase: $('#frmAddItemsGr #price_purchase').val(),
    price_list: $('#frmAddItemsGr #price_list').val(),
    repair_price: $('#frmAddItemsGr #repair_price').val(),
    price_new: $('#frmAddItemsGr #price_new').val(),
    update_date: $('#frmAddItemsGr #update_date').val(),
    usual_supplier: $('#frmAddItemsGr #usual_supplier').val(),
    pr_rip_conc: $('#frmAddItemsGr #pr_rip_conc').val(),
  }

  itemGrServices.insertNoAuth(data, function () {
    $('#frmAddItemsGr #imgBase64').val('')
    $('#frmAddItemsGr #tipology').val('')
    $('#frmAddItemsGr #note').val('')
    $('#frmAddItemsGr #original_code').val('')
    $('#frmAddItemsGr #code_product').val('')
    $('#frmAddItemsGr #code_gr').val('')
    $('#frmAddItemsGr #plant').val('')
    $('#frmAddItemsGr #brand').val('')
    $('#frmAddItemsGr #business_name_supplier').val('')
    $('#frmAddItemsGr #code_supplier').val('')
    $('#frmAddItemsGr #price_purchase').val('')
    $('#frmAddItemsGr #price_list').val('')
    $('#frmAddItemsGr #repair_price').val('')
    $('#frmAddItemsGr #price_new').val('')
    $('#frmAddItemsGr #update_date').val('')
    $('#frmAddItemsGr #usual_supplier').val('')
    $('#frmAddItemsGr #pr_rip_conc').val('')
    notificationHelpers.success('Inserimento completato con successo!')
    spinnerHelpers.hide()
  })
}

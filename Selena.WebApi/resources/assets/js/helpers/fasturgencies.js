//  fasturgenciesHelpers

// addFastUrgenze

export function addFastUrgenze() {
  spinnerHelpers.show()

  const data = {
    user_id: $('#frmfasturgencies #user_id').val(),
    date: $('#frmfasturgencies #date').val(),
    object: $('#frmfasturgencies #object').val(),
    description: $('#frmfasturgencies #description').val(),
  }

  const arrayfasturgencies = []
  for (let i = 0; i < 15; i++) {
    const jsonfasturgencies = {}
    let check = false

    if (functionHelpers.isValued($('#user_id' + i).val())) {
      check = true
      jsonfasturgencies.user_id = $('#user_id' + i).val()
    }
    if (functionHelpers.isValued($('#date' + i).val())) {
      check = true
      jsonfasturgencies.date = $('#date' + i).val()
    }
    if (functionHelpers.isValued($('#object' + i).val())) {
      check = true
      jsonfasturgencies.object = $('#object' + i).val()
    }
    if (functionHelpers.isValued($('#description' + i).val())) {
      check = true
      jsonfasturgencies.description = $('#description' + i).val()
    }
    if (check) {
      arrayfasturgencies.push(jsonfasturgencies)
    }
  }

  data.fasturgencies = arrayfasturgencies

  fasturgenciesServices.insert(data, function () {
    $('#intervention #user_id').val('')
    $('#intervention #date').val('')
    $('#intervention #object').val('')
    $('#intervention #description').val('')
    notificationHelpers.success('Inserimento avvenuto correttamente!')
    spinnerHelpers.hide()
  })
}

export function getUserData() {
  // spinnerHelpers.show();
  fasturgenciesServices.getUserData(storageData.sUserId(), function (response) {
    // view result FAST URGENZE
    let cont = 0
    const Aggfasturgencies = response.data
    if (Aggfasturgencies !== undefined) {
      if (Aggfasturgencies.length > 0) {
        for (let j = 0; j < Aggfasturgencies.length; j++) {
          let html = ''
          html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >'
          html += '<div class="col-3">'
          html +=
            '<select class="custom-select user_id_select" id="user_id' +
            Aggfasturgencies[j].id +
            '" name="user_id' +
            '" value="' +
            Aggfasturgencies[j].user_id +
            '"></select>'
          html += '</div>'
          html += '<div class="col-2">'
          html +=
            '<input type="text" id="date' +
            Aggfasturgencies[j].id +
            '" name="date' +
            '" class="form-control date' +
            '" value="' +
            Aggfasturgencies[j].date +
            '">'
          html += '</div>'
          html += '<div class="col-2">'
          html +=
            '<input type="text" class="form-control object" id="object' +
            Aggfasturgencies[j].id +
            '" name="object' +
            '" value="' +
            Aggfasturgencies[j].object +
            '">'
          html += '</div>'
          html += '<div class="col-3">'
          html +=
            '<input type="text" class="form-control description" id="description' +
            Aggfasturgencies[j].id +
            '" name="description' +
            '" value="' +
            Aggfasturgencies[j].description +
            '">'
          html += '</div>'
          html += '<div class="col-1">'
          html +=
            '<button type="button" class="btn btn-outline-success" onclick="fasturgenciesHelpers.updateFast(' +
            Aggfasturgencies[j].id +
            ',false)">Salva</button>'
          html += '</div>'
          html += '<div class="col-1">'
          html +=
            '<button type="button" class="btn btn-outline-danger" id="btndelete' +
            '" onclick="btndelete' +
            '">Cancella</button>'
          html += '</div>'
          html += '<div class="col-0">'
          html +=
            '<input type="hidden" class="form-control id" id="id' +
            Aggfasturgencies[j].id +
            '" name="id' +
            '" value="' +
            Aggfasturgencies[j].id +
            '">'
          html += '</div>'
          html += '</div>'
          $('#div-cont-fast-urgenze').append(html)

          // $.getScript("https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js").done(function(){
          var searchParam = ''
          const initials = []
          initials.push({
            id: Aggfasturgencies[j].user_id,
            text: Aggfasturgencies[j].description_user_id,
          })
          $('#user_id' + Aggfasturgencies[j].id).select2({
            data: initials,
            ajax: {
              url: configData.wsRootServicesUrl + '/api/v1/user/select',
              data(params) {
                if (params.term == '') {
                  searchParam = '*'
                } else {
                  searchParam = params.term
                }
                const query = {
                  search: searchParam,
                }
                return query
              },
              processResults(data) {
                const dataParse = JSON.parse(data)
                return {
                  results: dataParse.data,
                }
              },
            },
          })
          // });
        }
      }
    }

    // end view FAST URGENZE

    /* AGG FAST URGENZE */
    $('#frmFastUrgenze #add-fast-urgenze').on('click', function (e) {
      let html = ''
      if (cont < 10) {
        cont = cont + 1
        html +=
          '<div class="row" data-id-row="' +
          cont +
          '" id="divcont-' +
          cont +
          '" >'
        html += '<div class="col-3">'
        html +=
          '<select class="custom-select user_id_select" id="user_id' +
          cont +
          '" name="user_id' +
          cont +
          '"></select>'
        html += '</div>'
        html += '<div class="col-2">'
        html +=
          '<input type="text" id="date' +
          cont +
          '" name="date' +
          cont +
          '" class="form-control date' +
          cont +
          '">'
        html += '</div>'
        html += '<div class="col-2">'
        html +=
          '<input type="text" class="form-control object" id="object' +
          cont +
          '" name="object' +
          cont +
          '">'
        html += '</div>'
        html += '<div class="col-3">'
        html +=
          '<input type="text" class="form-control description" id="description' +
          cont +
          '" name="description' +
          cont +
          '">'
        html += '</div>'
        html += '<div class="col-1">'
        html +=
          '<button type="button" class="btn btn-outline-success" id="btnsave' +
          '" onclick="fasturgenciesHelpers.updateFast(' +
          cont +
          ',true)">Salva</button>'
        html += '</div>'
        html += '<div class="col-1">'
        html +=
          '<button type="button" class="btn btn-outline-danger" id="btndelete' +
          '" onclick="btndelete' +
          '">Cancella</button>'
        html += '</div>'
        html += '<div class="col-0">'
        html +=
          '<input type="hidden" class="form-control id" id="id' +
          cont +
          '" name="id' +
          '" value="' +
          cont +
          '">'
        html += '</div>'
        html += '</div>'
        $('#div-cont-fast-urgenze').append(html)

        $.getScript(
          'https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js'
        ).done(function () {
          let searchParam = ''
          $('#user_id' + cont).select2({
            ajax: {
              url: configData.wsRootServicesUrl + '/api/v1/user/select',
              data(params) {
                if (params.term == '') {
                  searchParam = '*'
                } else {
                  searchParam = params.term
                }
                const query = {
                  search: searchParam,
                }
                return query
              },
              processResults(data) {
                const dataParse = JSON.parse(data)
                return {
                  results: dataParse.data,
                }
              },
            },
          })
        })
      }
    })
    /* END FAST URGENZE */
  })
}

export function updateFast(id, isNew) {
  let data = {
    id,
    isNew,
  }

  const arrayFast = []
  const jsonFast = {}
  let check = false
  if (functionHelpers.isValued($('#date' + id).val())) {
    check = true
    jsonFast.date = $('#date' + id).val()
  }
  if (functionHelpers.isValued($('#id' + id).val())) {
    check = true
    jsonFast.id = $('#id' + id).val()
  }
  jsonFast.isNew = isNew

  if (functionHelpers.isValued($('#object' + id).val())) {
    check = true
    jsonFast.object = $('#object' + id).val()
  }
  if (functionHelpers.isValued($('#description' + id).val())) {
    check = true
    jsonFast.description = $('#description' + id).val()
  }
  if (functionHelpers.isValued($('#user_id' + id).val())) {
    check = true
    jsonFast.user_id = $('#user_id' + id).val()
  }

  if (check) {
    arrayFast.push(jsonFast)
  }

  data = arrayFast
  fasturgenciesServices.updateFast(data, function () {
    notificationHelpers.success('Modifica dati effettuata con successo!')
  })
}

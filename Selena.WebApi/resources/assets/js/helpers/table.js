/** *   tableHelpers   ***/

export function addInput(tableSelector, configurations, isDisabled) {
  $.each(configurations, function (i, configuration) {
    if (
      functionHelpers.isValued(configuration.visibleOnTable) &&
      configuration.visibleOnTable
    ) {
      if (
        functionHelpers.isValued(configuration.inputType) &&
        functionHelpers.isValued(configuration.field)
      ) {
        const editable = {
          mode: 'inline',
          emptytext: '-',
        }

        if (functionHelpers.isValued(isDisabled) && isDisabled) {
          editable.disabled = true
        }

        switch (configuration.inputType) {
          case 'DatePicker':
            editable.type = 'date'
            editable.format = storageData.sDatePickerFormat()
            break
          case 'Input':
          case 'EmailText':
            editable.type = 'text'
            break
          case 'Password':
            editable.type = 'password'
            break
          case 'Label':
            editable.disabled = true
            break
          case 'Select':
            editable.type = 'select'
            break
        }

        if (functionHelpers.isValued(configuration.onChange)) {
          editable.onSave = configuration.onChange
        } else {
          switch (configuration.inputType) {
            case 'Numeric':
              editable.onSave = 'tableHelpers.onChangeCheckNumber'
              break
          }
        }

        editable.validate = function (value) {
          if (
            functionHelpers.isValued(configuration.required) &&
            configuration.required &&
            $.trim(value) == ''
          ) {
            return dictionaryHelpers.getDictionary('ThisFieldIsRequired')
          }

          if (
            functionHelpers.isValued(configuration.digits) &&
            configuration.digits &&
            !$.isNumeric(value) &&
            functionHelpers.isValued(value)
          ) {
            return dictionaryHelpers.getDictionary('OnlyNumbersAreAllowed')
          }

          if (
            functionHelpers.isValued(configuration.maxLength) &&
            value.length > $(this).attr('data-maxlength')
          ) {
            return dictionaryHelpers
              .getDictionary('MaxLenght')
              .replace('{0}', $(this).attr('data-maxlength'))
          }

          if (
            configuration.inputType == 'EmailText' &&
            !functionHelpers.validateEmail(value)
          ) {
            return dictionaryHelpers.getDictionary('EmailNotValid')
          }

          // if (functionHelpers.isValued(configuration.onChange)) {
          //   var field = $(this).attr('field');
          //   var value = $(this).closest('td').find('.form-control').val();
          //   var rowIndex = $(this).data('rowindex');
          //   var id = $(this)[0].id;
          //   var rowId = id.substr(field.length + 4);
          //   return window[configuration.onChange.split('.')[0]][configuration.onChange.split('.')[1]](field,value,rowIndex,id,rowId);
          // }
        }

        $.each(
          $(tableSelector).find('a[id^="ti_' + configuration.field + '_"]'),
          function (i, input) {
            if (functionHelpers.isValued(configuration.maxLength)) {
              $(tableSelector)
                .find('#' + this.id)
                .attr('data-maxlength', configuration.maxLength)
            }

            if (editable.type == 'select') {
              switch (configuration.service) {
                case 'languages':
                  editable.select2 = languageHelpers.getSelect2ForBootstrapTable()
                  break

                // case 'templateEditorType':
                //   editable.select2 = templateEditorTypeServices.getSelect2ForBootstrapTable();
                //   break;

                default:
                  editable.select2 = window[
                    configuration.service + 'Services'
                  ].getSelect2ForBootstrapTable(configuration.dropDownParam)
                  break
              }

              editable.select2.width = 200
              editable.tpl = '<select style="width:150px;">'

              editable.success = function (response, newValue) {
                const editable = $(this).data('editable')
                const option = editable.input.$input.find(
                  'option[value="VALUE"]'.replace('VALUE', newValue)
                )
                const newText = option.text()
                $(this).attr('data-text', newText)
                $(this).attr('data-value', newValue)
              }

              $(tableSelector)
                .find('#' + this.id)
                .editable(editable)
            } else {
              $(tableSelector)
                .find('#' + this.id)
                .editable(editable)
            }

            $(tableSelector)
              .find('#' + this.id)
              .on('save', function (e, params) {
                if (
                  !$(tableSelector)
                    .find('#' + this.id)
                    .hasClass('error')
                ) {
                  if ($(tableSelector).find('.bs-checkbox').length > 0) {
                    $(tableSelector).bootstrapTable(
                      'check',
                      $(this).attr('data-rowindex')
                    )
                  }
                }

                if (functionHelpers.isValued(configuration.onSelect2Save)) {
                  eval(configuration.onSelect2Save)(this, e, params)
                }
              })
          }
        )
      }
    }
  })

  // $(tableSelector).bootstrapTable('resetView');
  spinnerHelpers.hide()
}

/** *   ONCHANGE   ***/

export function onChangeEvent(el, functionToCall) {
  const field = $('#' + $(el).closest('td').find('a')[0].id).attr('field')
  const value = $(el).closest('form').find('.form-control').val()
  const rowIndex = $(el).closest('td').find('a').data('rowindex')
  const id = $(el).closest('td').find('a')[0].id
  const rowId = id.substr(field.length + 4)
  functionToCall(field, value, rowIndex, id, rowId)
}

export function onChange(index, value, id, field) {
  switch (pageHelpers.getUrl()) {
    case '/admin/managementTechinicalSpecification':
      technicalSpecificationHelpers.onChangeTable(index, value, id, field)
      break
    case '/admin/groupsTechinicalSpecification':
      groupTechnicalSpecificationHelpers.onChangeTable(index, value, id, field)
      break
    case '/admin/groupsDisplayTechinicalSpecification':
      groupDisplayTechnicalSpecificationHelpers.onChangeTable(
        index,
        value,
        id,
        field
      )
      break
  }
}

export function onChangeCheckNumber(index, value, id, field) {
  const idTable = $('#' + id).closest('table')[0].id
  if (functionHelpers.isValued(value)) {
    if (functionHelpers.isNumeric(value)) {
      if ($('#' + id).hasClass('error')) {
        $('#' + id).removeClass('error')
      }
    } else {
      if (!$('#' + id).hasClass('error')) {
        $('#' + id).addClass('error')
      }
      $('#' + idTable).bootstrapTable('uncheck', index)
      notificationHelpers.error(
        dictionaryHelpers.getDictionary('PleaseEnterAValidNumber')
      )
    }
  } else if ($('#' + id).hasClass('error')) {
    $('#' + id).removeClass('error')
  }
}

/** *   END ONCHANGE   ***/

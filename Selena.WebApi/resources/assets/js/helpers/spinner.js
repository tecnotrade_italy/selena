/** *   spinnerHelpers   ***/

export function show() {
  $('.modal-backdrop').not('.fade').css('z-index', '9000')
  window.$('#spinnerView').modal('show')
}

export function hide() {
  window.$('#spinnerView').modal('hide')
}

/***   searchDataSheetHelpers   ***/

/***   POST   ***/
export function search(description,categories_id,producer_id) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(description,categories_id,producer_id);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  searchDataSheetServices.search(data, function (response) {
 
  html += "<div class='container div-cont-search'>";
    html += "<div class='row'>";
    html += response.message;
    html += "</div>";
    html += "</div>";
    $('#result').html(html);
    spinnerHelpers.hide();
  });
}


export function searchPaginatorDataSheet(pageActive) {
  spinnerHelpers.show();
  var page = pageActive;

  var data = {
    search: $('#frmSearchDatasSheet #search').val(),
    description: $('#frmSearchDatasSheet #description').val(),
    categories_id: $('#frmSearchDatasSheet #categories_id').val(),
    producer_id: $('#frmSearchDatasSheet #producer_id').val(),
    idLanguage : storageData.sIdLanguage(),
    autocomplete : false,
    orderby: $('#frmSearchDatasSheet #order').val(),
    paginator: page
  };
  var TextProd = "";
  if($('#frmSearchDatasSheet #producer_id').val()!= '' && $('#frmSearchDatasSheet #producer_id').val()!= null && $('#frmSearchDatasSheet #producer_id').val() != undefined){
    TextProd = window.$('#frmSearchDatasSheet #producer_id').select2('data')[0]['text'];
  }

  var TextCat = "";
  if($('#frmSearchDatasSheet #categories_id').val()!= '' && $('#frmSearchDatasSheet #categories_id').val()!= null && $('#frmSearchDatasSheet #categories_id').val() != undefined){
    TextCat = window.$('#frmSearchDatasSheet #categories_id').select2('data')[0]['text'];
  }

  
  var html = "";
  searchDataSheetServices.searchPaginatorDataSheet(data, function (response) {
    var htmlPaginator = "";
    var minus10Page = 1;
    var plus10Page = 1;
    var minus1Page = 1;
    var plus1Page = 1;
    var pageSelected = parseInt(response.data.pageSelected);
    var maxPage = response.data.totalPages;
    window.history.pushState('ricerca scheda tecnica', 'Ricerca', '/schede-tecniche?prod=' + $('#frmSearchDatasSheet #producer_id').val() + '&proddesc=' +  TextProd + '&cat=' + $('#frmSearchDatasSheet #categories_id').val()  + '&catdesc=' +  TextCat + '&description=' + $('#frmSearchDatasSheet #description').val() + '&order=' + $('#frmSearchDatasSheet #order').val() + '&p=' + page);
    html += "<div class='container div-cont-search'>";
    html += "<div class='row'>";
    html += "<div class='col-12'>";
    html += "<h1 class='div-title-search'>Ricerca</h1>";
    html += "</div>";
    html += "</div>";
	  html += '<div class="row no-gutters div-paginator">';
    html += '<div class="col-12">';
    html += '<hr>';
    html += '</div>';
    html += '<div class="col-md-6 col-sm-12">';
    html += '<h5>Articoli presenti: <b class="totItems">&nbsp;</b></h5>';
    html += '</div>';
    html += '<div class="col-md-6 col-sm-12">';
    html += '<div class="paginationCont" style="float:right;">';
    html += '<br>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += "<div class='row'>";
    html += response.data.html; 
    html += "</div>";
    html += "</div>";
    $('#result').html(html);


    /* funzione gestione paginatore */
    if (response.data.paginationActive) {
      htmlPaginator += "<ul class='pagination'>";
      /* controllo freccia -10 pagine */
      if ((pageSelected - 10) <= 1) {
        minus10Page = 1;
      } else {
        minus10Page = pageSelected - 10;
      }

      htmlPaginator += '<li class="page-item" data-page="' + minus10Page + '"><a><i class="fa fa-angle-double-left"></i></a></li>';
      /* end controllo freccia -10 pagine */
      /* controllo freccia -1 pagine */
      if ((pageSelected - 1) <= 1) {
        minus1Page = 1;
      } else {
        minus1Page = pageSelected - 1;
      }

      htmlPaginator += '<li class="page-item" data-page="' + minus1Page + '"><a><i class="fa fa-angle-left"></i></a></li>';
      /* end controllo freccia -1 pagine */

      /* controllo per pagine intermedie */
      if (maxPage > 4) {
        if (pageSelected > 1) {
          if ((pageSelected + 2) > maxPage) {
            if (pageSelected == maxPage) {
              for (var j = (pageSelected - 3); j <= maxPage; j++) {
                if (pageSelected == j) {
                  htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
                } else {
                  htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
                }
              }
            } else {
              for (var j = (pageSelected - 2); j <= maxPage; j++) {
                if (pageSelected == j) {
                  htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
                } else {
                  htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
                }
              }
            }
          } else {
            for (var j = (pageSelected - 1); j <= (pageSelected + 2); j++) {
              if (pageSelected == j) {
                htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
              } else {
                htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
              }
            }
          }
        } else {
          for (var j = 1; j <= 4; j++) {
            if (pageSelected == j) {
              htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
            } else {
              htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
            }
          }
        }
      } else {
        for (var j = 1; j <= maxPiage; j++) {
          if (pageSelected == j) {
            htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
          } else {
            htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
          }
        }
      }
      /* end controllo per pagine intermedie */

      /* controllo freccia + 1 pagine */
      if (pageSelected + 1 >= maxPage) {
        plus1Page = maxPage;
      } else {
        plus1Page = pageSelected + 1;
      }

      htmlPaginator += '<li class="page-item" data-page="' + plus1Page + '"><a><i class="fa fa-angle-right"></i></a></li>';
      /* end controllo freccia + 1 pagine */
      /* controllo freccia + 10 pagine */
      if (pageSelected + 10 >= maxPage) {
        plus10Page = maxPage;
      } else {
        plus10Page = pageSelected + 10;
      }

      htmlPaginator += '<li class="page-item" data-page="' + plus10Page + '"><a><i class="fa fa-angle-double-right"></i></a></li>';
      /* end controllo freccia + 10 pagine */

      htmlPaginator += "</ul>";
      $('.paginationCont').html(htmlPaginator);
    } else {
      $('.paginationCont').html('');
    }
    $('.totItems').html(response.data.totItems);
    /* end funzione gestione paginatore */
    $('.pagination li').on('click', function () {
      searchDataSheetHelpers.searchPaginatorDataSheet($(this).attr('data-page'));
    });

    spinnerHelpers.hide();
    $('#searchAutocomplete').hide('fast');
  });
}




/***   END POST   ***/

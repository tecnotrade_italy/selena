/** *   storageData   ***/

/** *   WINDOW STORAGE   ***/

export function removeCredentialData() {
  window.localStorage.removeItem('sTokenKey')
  window.localStorage.removeItem('sRefreshTokenKey')
  window.localStorage.removeItem('sExpiresTokenKey')
  window.localStorage.removeItem('sCredetial')
  window.localStorage.removeItem('sUserName')
  window.localStorage.removeItem('sUserId')
  //window.localStorage.removeItem('sSalesmanId')
  window.localStorage.removeItem('sUserDescription')
  window.localStorage.removeItem('sNotificationData')
  window.localStorage.removeItem('sRouteParameter')
  window.localStorage.removeItem('sFilterParameter')
  window.localStorage.removeItem('sMenu')
  

  storageData.delCookie('sUserId');
  storageData.delCookie('sUserDescription');
  //storageData.delCookie('sSalesmanId');
  storageData.delCookie('sUserName');
  storageData.delCookie('sRole');
  storageData.delCookie('sCartId');
}

/** * CONFIGURATION ***/

export function sMenu() {
  if (functionHelpers.isValued(window.localStorage.getItem('sMenu'))) {
    return JSON.parse(window.localStorage.getItem('sMenu'))
  }
}

export function setMenu(value) {
  window.localStorage.setItem('sMenu', JSON.stringify(value))
}

/** * END CONFIGURATION ***/

/** * CREDENTIAL ***/

export function sTokenKey() {
  return window.localStorage.getItem('sTokenKey')
}

export function setTokenKey(value) {
  window.localStorage.setItem('sTokenKey', value)
}

export function sRefreshTokenKey() {
  return window.localStorage.getItem('sRefreshTokenKey')
}

export function setRefreshTokenKey(value) {
  window.localStorage.setItem('sRefreshTokenKey', value)
}

export function sExpiresTokenKey() {
  return window.localStorage.getItem('sExpiresTokenKey')
}

export function setExpiresTokenKey(value) {
  window.localStorage.setItem('sExpiresTokenKey', value)
}

export function sCredetial() {
  return JSON.parse(window.localStorage.getItem('sCredetial'))
}

export function setCredetial(value) {
  window.localStorage.setItem('sCredetial', JSON.stringify(value))
}

export function removeCredetial() {
  window.localStorage.removeItem('sCredetial')
}

/** * END CREDENTIAL ***/

/** * LANGUAGE ***/

export function sIdLanguage() {
  if (functionHelpers.isValued(window.localStorage.getItem('sIdLanguage'))) {
    return window.localStorage.getItem('sIdLanguage')
  } else {
    languageServices.getDefault(function (result) {
      window.localStorage.setItem('sIdLanguage', result.data.id)
      storageData.setDescriptionLanguage(result.data.description)
      storageData.setDatePickerFormat(result.data.date_format_client)
      storageData.setDateTimePickerFormat(result.data.date_time_format_client)
      dictionaryServices.setByLanguage(function (result) {
        storageData.setDictionary(result.data)
      })
      return result.data.id
    })

    return 0
  }
}

export function setIdLanguage(id) {
  window.localStorage.setItem('sIdLanguage', id)
  storageData.setCookie('sIdLanguage', id , 365);

  languageServices.get(id, function (result) {
    storageData.setDescriptionLanguage(result.data.description)
    storageData.setDatePickerFormat(result.data.date_format_client)
    storageData.setDateTimePickerFormat(result.data.date_time_format_client)
    dictionaryServices.setByLanguage(function (result) {
      storageData.setDictionary(result.data)
    })
    return result.data.id
  })
}

export function removeIdLanguage() {
  window.localStorage.removeItem('sIdLanguage')
  window.localStorage.removeItem('sDictionary')

  storageData.delCookie('sIdLanguage');
}

export function sDescriptionLanguage() {
  return window.localStorage.getItem('sDescriptionLanguage')
}

export function setDescriptionLanguage(value) {
  window.localStorage.setItem('sDescriptionLanguage', value)
}

export function sDictionary() {
  if (functionHelpers.isValued(window.localStorage.getItem('sDictionary'))) {
    return JSON.parse(window.localStorage.getItem('sDictionary'))
  } else {
    dictionaryServices.setByLanguage(function (result) {
      storageData.setDictionary(result.data)
    })
  }
}

export function setDictionary(value) {
  window.localStorage.setItem('sDictionary', JSON.stringify(value))
}

export function sDatePickerFormat() {
  return window.localStorage.getItem('sDatePickerFormat')
}

export function setDatePickerFormat(value) {
  window.localStorage.setItem('sDatePickerFormat', value)
}

export function sDateTimePickerFormat() {
  return window.localStorage.getItem('sDateTimePickerFormat')
}

export function setDateTimePickerFormat(value) {
  window.localStorage.setItem('sDateTimePickerFormat', value)
}

/** * END LANGUAGE ***/

/** * PAGE ***/

export function sHoldUrl() {
  return window.localStorage.getItem('sHoldUrl')
}

export function setHoldUrl(value) {
  window.localStorage.setItem('sHoldUrl', value)
}

export function removeHoldUrl() {
  window.localStorage.removeItem('sHoldUrl')
}

export function sNotificationData() {
  if (
    functionHelpers.isValued(window.localStorage.getItem('sNotificationData'))
  ) {
    return JSON.parse(window.localStorage.getItem('sNotificationData'))
  }
}

export function setNotificationData(value) {
  window.localStorage.setItem('sNotificationData', JSON.stringify(value))
}

export function sRouteParameter() {
  return window.localStorage.getItem('sRouteParameter')
}

export function setRouteParameter(value) {
  window.localStorage.setItem('sRouteParameter', value)
}

export function removeRouteParameter() {
  window.localStorage.removeItem('sRouteParameter')
}

export function sFilterParameter(view) {
  if (functionHelpers.isValued(view)) {
    const filterParameter = JSON.parse(
      window.localStorage.getItem('sFilterParameter')
    )

    if (
      functionHelpers.isValued(filterParameter) &&
      functionHelpers.isValued(filterParameter[view])
    ) {
      return filterParameter[view]
    } else {
    }
  } else {
  }
}

export function setFilterParameter(view, value) {
  if (functionHelpers.isValued(view)) {
    let filterParameter = JSON.parse(
      window.localStorage.getItem('sFilterParameter')
    )

    if (!functionHelpers.isValued(filterParameter)) {
      filterParameter = {}
    }

    filterParameter[view] = value
    window.localStorage.setItem(
      'sFilterParameter',
      JSON.stringify(filterParameter)
    )
  }
}

export function removeFilterParameter(view) {
  if (functionHelpers.isValued(view)) {
    let filterParameter = JSON.parse(
      window.localStorage.getItem('sFilterParameter')
    )

    if (
      functionHelpers.isValued(filterParameter) &&
      functionHelpers.isValued(filterParameter[view])
    ) {
      filterParameter = delete filterParameter[view]
      if (filterParameter.toString() == '[object Object]') {
        window.localStorage.setItem(
          'sFilterParameter',
          JSON.stringify(filterParameter)
        )
      } else {
        window.localStorage.removeItem('sFilterParameter')
      }
    }
  } else {
    window.localStorage.removeItem('sFilterParameter')
  }
}

/** * END PAGE ***/

/** * SETTING ***/

// settingServices

export function sSetting() {
  if (functionHelpers.isValued(window.localStorage.getItem('sSetting'))) {
    return JSON.parse(window.localStorage.getItem('sSetting'))
  } else {
    settingService.getAll(function (result) {
      window.localStorage.setItem('sSetting', JSON.stringify(result))
      return JSON.parse(window.localStorage.getItem('sSetting'))
    })
  }
}

/** * END SETTING ***/

/** * USER ***/

export function sUserName() {
  return window.localStorage.getItem('sUserName')
}

export function setUserName(value) {
  window.localStorage.setItem('sUserName', value)
  storageData.setCookie('sUserName', value , 365);
}

export function setRole(value) {
  window.localStorage.setItem('sRole', value)
  storageData.setCookie('sRole', value , 365);
}
export function sRole(value) {
  return window.localStorage.getItem('sRole')
}

export function sUserId(value) {
  return window.localStorage.getItem('sUserId')
}

export function setUserId(value) {
  window.localStorage.setItem('sUserId', value)
  storageData.setCookie('sUserId', value , 365);
}

export function sUserDescription(value) {
  return window.localStorage.getItem('sUserDescription')
}

export function setUserDescription(value) {
  window.localStorage.setItem('sUserDescription', value)
  storageData.setCookie('sUserDescription', value , 365);
}

export function sSalesmanId(value) {
  return window.localStorage.getItem('sSalesmanId')
}

export function setSalesmanId(value) {
  window.localStorage.setItem('sSalesmanId', value)
  storageData.setCookie('sSalesmanId', value , 365);
}

export function sCartId(value) {
  return window.localStorage.getItem('sCartId')
}
export function setCartId(value) {
  window.localStorage.setItem('sCartId', value)
  storageData.setCookie('sCartId', value , 365);
}

/** * END USER ***/

/** * GESTIONE COOKIE  ***/

export function setCookie(name,value,days) {
  var expires = "";
  if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days*24*60*60*1000));
      expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

export function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}

export function delCookie(name) {   
  document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

/** * END GESTIONE COOKIE ***/

/** * END WINDOW STORAGE ***/

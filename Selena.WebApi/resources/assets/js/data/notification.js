/** *   notificationData   ***/

export function data() {
  if (!functionHelpers.isValued(storageData.sNotificationData())) {
  } else {
    return storageData.sNotificationData()
  }
}

export function put(type, message) {
  let _notification = this.data()
  if (!functionHelpers.isValued(_notification)) {
    _notification = [{ type: '', message: '', read: true }]
    _notification.splice(0, 1)
  }

  _notification.unshift({ type, message, read: false })
  storageData.setNotificationData(_notification)
}

export function readAll() {
  const _notification = this.data()

  $.each(_notification, function (i, item) {
    item.read = true
  })

  storageData.setNotificationData(_notification)
  setTimeout(function () {
    notificationHelpers.readNotification()
  }, 2000)
}

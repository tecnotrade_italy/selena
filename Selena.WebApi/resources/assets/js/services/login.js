/** *   loginService   ***/

/** *   POST   ***/

export function login(username, password, fnSuccess, fnError) {
  const session_token = storageData.sTokenKey()

  const data = {
    // grant_type: 'password',
    name: username,
    password,
    remember_me: true,
    session_token,
  }

  serviceHelpers.postNoJson('/api/auth/login', data, fnSuccess, fnError)
}

export function loginSalesman(username, password, fnSuccess, fnError) {
  const session_token = storageData.sTokenKey()

  const data = {
    // grant_type: 'password',
    name: username,
    password,
    remember_me: true,
    session_token,
  }

  serviceHelpers.postNoJson('/api/auth/loginSalesman', data, fnSuccess, fnError)
}

export function refreshToken(fnSuccess, fnError) {
  serviceHelpers.postWithoutData(
    '/Token?grant_type=refresh_token&refresh_token=' +
      storageData.sRefreshTokenKey() +
      '&username=' +
      storageData.sUserName(),fnSuccess,fnError)
}

export function insertLoginForFacebook(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/auth/insertLoginForFacebook', data, fnSuccess, fnError);
}

export function insertLoginForGoogle(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/auth/insertLoginForGoogle', data, fnSuccess, fnError);
}


/** *   END POST   ***/

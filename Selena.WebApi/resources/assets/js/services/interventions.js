/** *   InterventionsServices   ***/

/** *   GET   ***/

export function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/intervention/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey(),
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term),
        }
      },
      processResults(result) {
        return {
          results: result.data,
        }
      },
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty'),
  }
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/intervention/select?search=' + search,
    fnSuccess,
    fnError
  )
}
export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/intervention/' + id,
    fnSuccess,
    fnError
  )
}

export function getByViewValue(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/intervention/getByViewValue/' + id,
    fnSuccess,
    fnError
  )
}
export function getByViewQuotes(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/intervention/getByViewQuotes/' + id,
    fnSuccess,
    fnError
  )
}
export function getByViewExternalRepair(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/intervention/getByViewExternalRepair/' + id,
    fnSuccess,
    fnError
  )
}

export function getByIdUser(user_id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/intervention/getByIdUser/' + user_id,
    fnSuccess,
    fnError
  )
}

export function getDescriptionAndPriceProcessingSheet(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/intervention/getDescriptionAndPriceProcessingSheet/' + id,
    fnSuccess,
    fnError
  )
}
export function getnumbertelephone(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/intervention/getnumbertelephone/' + id,
    fnSuccess,
    fnError
  )
}
export function getLastId(fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/intervention/getLastId',
    fnSuccess,
    fnError
  )
}

export function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/intervention/all/' + idLanguage,
    fnSuccess,
    fnError
  )
}

export function getAllIntervention(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/intervention/getAllIntervention/' + idLanguage,
    fnSuccess,
    fnError
  )
}

export function get(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/intervention', fnSuccess, fnError)
}

export function getWithGroupTechnicalSpecification(fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/intervention/withGroupTechnicalSpecification',
    fnSuccess,
    fnError
  )
}

export function getGraphic(fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/intervention/getGraphic',
    fnSuccess,
    fnError
  )
}

export function count(fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/intervention/count',
    fnSuccess,
    fnError
  )
}

/** *   END GET   ***/

/** *   PUT   ***/

export function insert(DtoInterventions, fnSuccess, fnError) {
  serviceHelpers.put(
    '/api/v1/intervention',
    DtoInterventions,
    fnSuccess,
    fnError
  )
}

export function insertModule(DtoInterventions, fnSuccess, fnError) {
  serviceHelpers.put(
    '/api/v1/intervention/insertModule',
    DtoInterventions,
    fnSuccess,
    fnError
  )
}
/** *   END PUT   ***/

/** *   POST   ***/

export function update(DtoInterventions, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/intervention',
    DtoInterventions,
    fnSuccess,
    fnError
  )
}

export function updateIntervention(DtoInterventions, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/intervention/updateIntervention',
    DtoInterventions,
    fnSuccess,
    fnError
  )
}

export function updateInterventionUpdateProcessingSheet(
  DtoInterventions,
  fnSuccess,
  fnError
) {
  serviceHelpers.postAutenticate(
    '/api/v1/intervention/updateInterventionUpdateProcessingSheet',
    DtoInterventions,
    fnSuccess,
    fnError
  )
}

export function updateInterventionUpdateViewQuotes(
  DtoQuotes,
  fnSuccess,
  fnError
) {
  serviceHelpers.postAutenticate(
    '/api/v1/intervention/updateInterventionUpdateViewQuotes',
    DtoQuotes,
    fnSuccess,
    fnError
  )
}

export function updateExternalRepair(DtoExternalRepair, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/intervention/updateExternalRepair',
    DtoExternalRepair,
    fnSuccess,
    fnError
  )
}
export function updateCheck(DtoInterventions, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/intervention/updateCheck',
    DtoInterventions,
    fnSuccess,
    fnError
  )
}
/** *   END POST   ***/

export function updateUser(DtoUser, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/intervention/updateUser',
    DtoUser,
    fnSuccess,
    fnError
  )
}

/** * DELETE ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate(
    '/api/v1/intervention/' + id,
    fnSuccess,
    fnError
  )
}

/** * DELETE ***/

/** *   clone ***/
export function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/intervention/clone/' +
      id +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    null,
    fnSuccess,
    fnError
  )
}
/** *   END  clone ***/

/** *   userServices   ***/

/** *   GET   ***/

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/user/select?search' + search,
    fnSuccess,
    fnError
  )
}

export function getByIdResult(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/user/getByIdResult/' + id,
    fnSuccess,
    fnError
  )
}

export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/' + id, fnSuccess, fnError)
}
export function availableCheck(id, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/user/availableCheck/' + id,
    fnSuccess,
    fnError
  )
}

export function getUserData(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/user/getUserData/' + id, fnSuccess, fnError)
}

export function getByUsername(username, fnSuccess, fnError) {
  serviceHelpers.get(
    '/api/v1/user/getByUsername/' + username,
    fnSuccess,
    fnError
  )
}

export function getGraphic(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/getGraphic', fnSuccess, fnError)
}

export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/user/all/' + menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )
}

export function getAllUser(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/user/getAllUser/' + idLanguage,
    fnSuccess,
    fnError
  )
}

export function count(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/count', fnSuccess, fnError)
}

/** *   END GET   ***/

/** *   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/user', data, fnSuccess, fnError)
}

export function insertNoAuth(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/user/insertNoAuth', data, fnSuccess, fnError)
}

export function insertUser(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/user/insertUser', data, fnSuccess, fnError)
}

/** *   END PUT   ***/

/** *   POST   ***/

export function changePassword(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/user/changePassword',
    data,
    fnSuccess,
    fnError
  )
}

export function changePasswordRescued(data, fnSuccess, fnError) {
  serviceHelpers.post(
    '/api/v1/user/changePasswordRescued',
    data,
    fnSuccess,
    fnError
  )
}

export function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/user', data, fnSuccess, fnError)
}

export function updateNoAuth(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/updateNoAuth', data, fnSuccess, fnError)
}

export function updateUser(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/updateUser', data, fnSuccess, fnError)
}

export function rescuePassword(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/rescuePassword', data, fnSuccess, fnError)
}

export function checkExistUsername(username, fnSuccess, fnError) {
  const data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    username,
  }

  serviceHelpers.postAutenticate(
    '/api/v1/user/checkExistUsername',
    data,
    fnSuccess,
    fnError
  )
}

export function checkGroupUser(username, fnSuccess, fnError) {
  const data = {
    username,
  }

  serviceHelpers.postAutenticate(
    '/api/v1/user/checkGroupUser',
    data,
    fnSuccess,
    fnError
  )
}

export function checkExistUsernameNotForThisPage(
  id,
  username,
  fnSuccess,
  fnError
) {
  const data = {
    id,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    username,
  }

  serviceHelpers.postAutenticate(
    '/api/v1/user/checkExistUsername',
    data,
    fnSuccess,
    fnError
  )
}

export function checkExistEmail(email, fnSuccess, fnError) {
  const data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    email,
  }

  serviceHelpers.postAutenticate(
    '/api/v1/user/checkExistEmail',
    data,
    fnSuccess,
    fnError
  )
}

export function checkExistEmailNotForThisPage(id, email, fnSuccess, fnError) {
  const data = {
    id,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    email,
  }

  serviceHelpers.postAutenticate(
    '/api/v1/user/checkExistEmail',
    data,
    fnSuccess,
    fnError
  )
}

/** *   END POST   ***/

/** *   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate(
    '/api/v1/user/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )
}

/** *   END DELETE   ***/

export function delet(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/user/' + id, fnSuccess, fnError)
}

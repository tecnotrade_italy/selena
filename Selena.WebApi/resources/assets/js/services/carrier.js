/** *  carrierServices   ***/

/** *   GET   ***/

export function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/carrier/getAll', fnSuccess, fnError)
}

export function getByCustomer(idCustomer, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/carrier/getByCustomer/' +
      idCustomer +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )
}

export function getSelect(routeParams, fnSuccess, fnError) {
  const data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(routeParams),
  }

  serviceHelpers.getAutenticateWithData(
    '/api/v1/carrier/select',
    data,
    fnSuccess,
    fnError
  )
}

export function getCarrierArea(idCarrier, idZone, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/carrier/getCarrierArea/' + idCarrier + '/' + idZone,
    fnSuccess,
    fnError
  )
}

/** *   END GET   ***/

/** *   INSERT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/carrier', data, fnSuccess, fnError)
}

export function insertCarrierArea(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/carrier/insertCarrierArea',
    data,
    fnSuccess,
    fnError
  )
}

/** *   END INSERT   ***/

/** *   POST   ***/

export function update(request, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/carrier', request, fnSuccess, fnError)
}

export function updateCarrierRangeArea(request, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/carrier/updateCarrierRangeArea',
    request,
    fnSuccess,
    fnError
  )
}

/** *   END POST   ***/

/** * DELETE ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/carrier/' + id, fnSuccess, fnError)
}

export function deleteRangeDetailArea(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate(
    '/api/v1/carrier/deleteRangeDetailArea/' + id,
    fnSuccess,
    fnError
  )
}

export function deleteZoneRangeDetailArea(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/carrier/deleteZoneRangeDetailArea',
    data,
    fnSuccess,
    fnError
  )
}

/** * DELETE ***/

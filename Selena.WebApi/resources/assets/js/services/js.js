/** *   jsServices   ***/

/** *   GET   ***/

export function get(fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/js/' + menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )
}

/** *   END GET   ***/

/** *   POST   ***/

export function update(css, fnSuccess, fnError) {
  const data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    css,
  }

  serviceHelpers.postAutenticate('/api/v1/js', data, fnSuccess, fnError)
}

/** *   END POST   ***/

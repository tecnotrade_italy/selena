/** *   searchUserServices   ***/

/** *   POST   ***/

export function searchImplemented(search, fnSuccess, fnError) {
  serviceHelpers.post(
    '/api/v1/searchuser/searchImplemented',
    search,
    fnSuccess,
    fnError
  )
}
/** *   END POST   ***/

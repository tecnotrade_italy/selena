/** *   searchItemsServices   ***/

/** *   POST   ***/

export function searchImplemented(search, fnSuccess, fnError) {
  serviceHelpers.post(
    '/api/v1/searchitems/searchImplemented',
    search,
    fnSuccess,
    fnError
  )
}
/** *   END POST   ***/

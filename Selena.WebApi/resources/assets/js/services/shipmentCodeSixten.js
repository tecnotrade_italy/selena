/** *   shipmentCodeSixtenServices   ***/

/** *   GET   ***/

export function getSelect(routeParams, fnSuccess, fnError) {
  const data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(routeParams),
  }

  serviceHelpers.getAutenticateWithData(
    '/api/v1/shipmentCodeSixten/select',
    data,
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/shipmentCodeSixten/select',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: "application/x-www-form-urlencoded; charset=UTF-8",
  //   data: data
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function getByCustomerSixten(
  idCustomer,
  routeParams,
  fnSuccess,
  fnError
) {
  serviceHelpers.getAutenticate(
    '/api/v1/shipmentCodeSixten/getByCustomerSixten/' +
      idCustomer +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(routeParams),
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/shipmentCodeSixten/getByCustomerSixten/' + idCustomer + '/' + menuHelpers.getIdFunctionByAdminUrl(routeParams),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END GET   ***/

/** *  dictionaryServices   ***/

/** *   GET   ***/

export function setByLanguage(fnSuccess, fnError) {
  if (functionHelpers.isValued(storageData.sIdLanguage())) {
    serviceHelpers.get(
      '/api/v1/dictionary/getByLanguage/' + storageData.sIdLanguage(),
      fnSuccess,
      fnError
    )
  }

  // $.ajax({
  //     type: 'GET',
  //     url: configData.wsRootServicesUrl + '/api/v1/dictionary/getByLanguage/' + storageData.sIdLanguage(),
  //     dataType: 'json',
  //     headers: { Accept: "application/json" },
  //     contentType: 'application/x-www-form-urlencoded; charset=UTF-8'
  // }).done(function (result) {
  //     /*** DICTIONARY */
  //     storageData.setDictionary(result.data);
  //     /*** MESSAGGE VALIDATION ***/
  //     if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //         fnSuccess();
  //     }
  // }).fail(function (response) {
  //     failServiceHelpers.fail(response);
  // });
}

/** *   END GET   ***/

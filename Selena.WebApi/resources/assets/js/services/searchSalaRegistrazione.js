/** *   searchSalaRegistrazioneServices   ***/

/** *   POST   ***/
export function searchImplemented(search, fnSuccess, fnError) {
  serviceHelpers.post(
    '/api/v1/searchsalaregistrazione/searchImplemented',
    search,
    fnSuccess,
    fnError
  )
}
/** *   END POST   ***/

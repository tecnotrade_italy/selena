/** *   interventionCustomerServices   ***/

export function getAllInterventionCustomer(idUser, fnSuccess, fnError) {
  if (idUser === undefined || idUser == '') {
    idUser = storageData.idUser()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/interventioncustomer/getAllInterventionCustomer/' + idUser,
    fnSuccess,
    fnError
  )
}

export function getByViewQuotesCustomer(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/interventioncustomer/getByViewQuotesCustomer/' + id,
    fnSuccess,
    fnError
  )
}

export function updateInterventionUpdateViewCustomerQuotes(
  DtoQuotes,
  fnSuccess,
  fnError
) {
  serviceHelpers.postAutenticate(
    '/api/v1/interventioncustomer/updateInterventionUpdateViewCustomerQuotes',
    DtoQuotes,
    fnSuccess,
    fnError
  )
}

export function getByViewModuleCustomer(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/interventioncustomer/getByViewModuleCustomer/' + id,
    fnSuccess,
    fnError
  )
}

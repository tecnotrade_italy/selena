/** *  carriageServices   ***/

/** *   GET   ***/

export function getByCustomer(idCustomer, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/carriage/getByCustomer/' +
      idCustomer +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )
}

export function getSelect(routeParams, fnSuccess, fnError) {
  const data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(routeParams),
  }

  serviceHelpers.getAutenticateWithData(
    '/api/v1/carriage/select',
    data,
    fnSuccess,
    fnError
  )
}

/** *   END GET   ***/

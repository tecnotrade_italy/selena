/** *   searchShopServices   ***/

/** *   POST   ***/

export function searchImplemented(search, fnSuccess, fnError) {
  serviceHelpers.post(
    '/api/v1/searchshop/searchImplemented',
    search,
    fnSuccess,
    fnError
  )
}

/** *   END POST   ***/

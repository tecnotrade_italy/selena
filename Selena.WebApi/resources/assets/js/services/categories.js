/** *   categoriesServices   ***/

/** *   GET   */

export function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/Categories/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey(),
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term),
        }
      },
      processResults(result) {
        return {
          results: result.data,
        }
      },
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty'),
  }
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/Categories/select?search=' + search,
    fnSuccess,
    fnError
  )
}

export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Categories/' + id, fnSuccess, fnError)
}

export function getList(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/Categories/getList',
    data,
    fnSuccess,
    fnError
  )
}

export function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Categories/' + id, fnSuccess, fnError)
}

export function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/Categories/all/' + idLanguage,
    fnSuccess,
    fnError
  )
}

export function getAllListNavigation(idLanguage, idItem, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/Categories/getAllListNavigation/' + idLanguage + '/' + idItem,
    fnSuccess,
    fnError
  )
}

export function getAllListNavigationFather(
  idLanguage,
  idItem,
  fnSuccess,
  fnError
) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/Categories/getAllListNavigationFather/' +
      idLanguage +
      '/' +
      idItem,
    fnSuccess,
    fnError
  )
}

export function getAllDetailListCategories(
  idLanguage,
  idItem,
  fnSuccess,
  fnError
) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/Categories/getAllDetailListCategories/' +
      idLanguage +
      '/' +
      idItem,
    fnSuccess,
    fnError
  )
}

export function getAllDetailListCategoriesFather(
  idLanguage,
  idCategories,
  fnSuccess,
  fnError
) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/Categories/getAllDetailListCategoriesFather/' +
      idLanguage +
      '/' +
      idCategories,
    fnSuccess,
    fnError
  )
}

export function getImages(idCategories, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/Categories/getImages/' + idCategories,
    fnSuccess,
    fnError
  )
}

export function getAllImagesInFolder(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/Categories/getAllImagesInFolder/' + id,
    fnSuccess,
    fnError
  )
}

export function getCategoriesLanguages(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/Categories/getCategoriesLanguages/' + id,
    fnSuccess,
    fnError
  )
}

export function getByTranslateDefault(
  id,
  idItem,
  idLanguage,
  fnSuccess,
  fnError
) {
  serviceHelpers.getAutenticate(
    '/api/v1/Categories/getByTranslateDefault/' +
      id +
      '/' +
      idItem +
      '/' +
      idLanguage,
    fnSuccess,
    fnError
  )
}

export function getByTranslateDefaultNew(
  idItem,
  idLanguage,
  fnSuccess,
  fnError
) {
  serviceHelpers.getAutenticate(
    '/api/v1/Categories/getByTranslateDefaultNew/' + idItem + '/' + idLanguage,
    fnSuccess,
    fnError
  )
}

/** *   END GET   */

/** *   PUT   ***/

export function insert(DtoCategories, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/Categories',
    DtoCategories,
    fnSuccess,
    fnError
  )
}

export function insertCategoriesFather(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/Categories/insertCategoriesFather',
    data,
    fnSuccess,
    fnError
  )
}

/** *   END PUT   ***/

/** *   POST   ***/

export function update(DtoCategories, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/Categories',
    DtoCategories,
    fnSuccess,
    fnError
  )
}

export function updateOrderCategoryItem(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/Categories/updateOrderCategoryItem',
    data,
    fnSuccess,
    fnError
  )
}

export function updateOrderCategoryFather(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/Categories/updateOrderCategoryFather',
    data,
    fnSuccess,
    fnError
  )
}

export function updateOnlineCategories(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/Categories/updateOnlineCategories',
    data,
    fnSuccess,
    fnError
  )
}

export function updateImageName(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/Categories/updateImageName',
    data,
    fnSuccess,
    fnError
  )
}

export function updateCategoriesImages(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/Categories/updateCategoriesImages',
    data,
    fnSuccess,
    fnError
  )
}

/** *   END POST   ***/

/** * DELETE ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/Categories/' + id, fnSuccess, fnError)
}

export function deleteCategoriesFather(
  idCategories,
  idCategoriesFather,
  fnSuccess,
  fnError
) {
  serviceHelpers.delAutenticate(
    '/api/v1/Categories/deleteCategoriesFather/' +
      idCategories +
      '/' +
      idCategoriesFather,
    fnSuccess,
    fnError
  )
}

/** * END DELETE ***/

/** *   clone ***/
export function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/Categories/clone/' +
      id +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    null,
    fnSuccess,
    fnError
  )
}
/** *   END  clone ***/

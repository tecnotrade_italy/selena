/** *   newsletterServices   ***/

/** *   GET   ***/

export function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletter/' + id, fnSuccess, fnError)
}

export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletter/all', fnSuccess, fnError)
}

/** *   END GET   ***/

/** *   PUT   ***/

export function insert(dtoNewsletter, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/newsletter',
    dtoNewsletter,
    fnSuccess,
    fnError
  )
}

/** *   END PUT   ***/

/** *   POST   ***/

export function update(dtoNewsletter, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/newsletter',
    dtoNewsletter,
    fnSuccess,
    fnError
  )
}

/** *   END POST   ***/

/** *   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/newsletter/' + id, fnSuccess, fnError)
}

/** *   END DELETE   ***/

/** *   templateEditorGroupServices   ***/

/** *   GET   ***/

export function getDto(fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/templateeditorgroup/getDto/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )
}

export function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/templateeditorgroup/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey(),
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term),
        }
      },
      processResults(result) {
        return {
          results: result.data,
        }
      },
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty'),
  }
}

export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/templateeditorgroup/' + id,
    fnSuccess,
    fnError
  )
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/templateeditorgroup/select?idFunctionality=' +
      menuHelpers.getIdFunctionByAdminUrl() +
      '&search' +
      search,
    fnSuccess,
    fnError
  )
}

export function selectGroupTemplate(search, type, fnSuccess, fnError) {
  if (functionHelpers.isValued(type)) {
    serviceHelpers.getAutenticate(
      '/api/v1/templateeditorgroup/selectGroupTemplate?idFunctionality=' +
        menuHelpers.getIdFunctionByAdminUrl() +
        '&type=' +
        type +
        '&search' +
        search,
      fnSuccess,
      fnError
    )
  } else {
    return {
      data: [],
    }
  }
}

/** *   END GET   ***/

/** *   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/templateeditorgroup',
    data,
    fnSuccess,
    fnError
  )
}

/** *   END PUT   ***/

/** *   POST   ***/

export function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/templateeditorgroup',
    data,
    fnSuccess,
    fnError
  )
}

/** *   END POST   ***/

/** *   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate(
    '/api/v1/templateeditorgroup/' +
      id +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )
}

/** *   END DELETE   ***/

/** *   technicalSpecificationServices   ***/

/** *   GET   ***/

export function getDtoTable(fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/technicalSpecification/getDtoTable',
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/technicalSpecification/getDtoTable',
  //   dataType: 'json',
  //   headers: { Authorization: storageData.sTokenKey() },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function getGroupDisplayTechnicalSpecification(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/technicalSpecification/getGroupDisplayTechnicalSpecification/' +
      id,
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/technicalSpecification/getGroupDisplayTechnicalSpecification/' + id,
  //   dataType: 'json',
  //   headers: { Authorization: storageData.sTokenKey() },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function getGroupTechnicalSpecification(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/technicalSpecification/getGroupTechnicalSpecification/' + id,
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/technicalSpecification/getGroupTechnicalSpecification/' + id,
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function checkExists(description, idLanguage, id, fnSuccess, fnError) {
  let url =
    configData.wsRootServicesUrl +
    '/api/v1/technicalSpecification/checkExists/' +
    description +
    '/' +
    idLanguage

  if (functionHelpers.isValued(id)) {
    url = url + '/' + id
  }

  serviceHelpers.getAutenticate(url, fnSuccess, fnError)

  // $.ajax({
  //   type: 'GET',
  //   url: url,
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END GET   ***/

/** *   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/technicalSpecification',
    data,
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/technicalSpecification',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(dtoTechnicalSpecification))
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function addGroupDisplayTechnicalSpecification(
  id,
  idGroupDisplayTechnicalSpecification,
  fnSuccess,
  fnError
) {
  serviceHelpers.putAutenticateWithoutData(
    '/api/v1/technicalSpecification/addGroupDisplayTechnicalSpecification/' +
      id +
      '/' +
      idGroupDisplayTechnicalSpecification +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/technicalSpecification/addGroupDisplayTechnicalSpecification/' + id + '/' + idGroupDisplayTechnicalSpecification + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function addGroupTechnicalSpecification(
  id,
  idGroupTechnicalSpecification,
  fnSuccess,
  fnError
) {
  serviceHelpers.putAutenticateWithoutData(
    '/api/v1/technicalSpecification/addGroupTechnicalSpecification/' +
      id +
      '/' +
      idGroupTechnicalSpecification +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/technicalSpecification/addGroupTechnicalSpecification/' + id + '/' + idGroupTechnicalSpecification + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END PUT   ***/

/** *   POST   ***/

export function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/technicalSpecification',
    data,
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'POST',
  //   url: configData.wsRootServicesUrl + '/api/v1/technicalSpecification',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(dtoTechnicalSpecification))
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

/** *   END POST   ***/

/** *   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate(
    '/api/v1/technicalSpecification/' +
      id +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'DELETE',
  //   url: configData.wsRootServicesUrl + '/api/v1/technicalSpecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

/** *   END DELETE   ***/

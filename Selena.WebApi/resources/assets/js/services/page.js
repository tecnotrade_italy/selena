/** *   pageServices   ***/

/** * GET ***/

export function getMenuList(id, pageType, fnSuccess, fnError) {
  if (id == '' || id === undefined) {
    id = storageData.sIdLanguage()
  }
  if (pageType == '' || pageType === undefined) {
    pageType = 'Page'
  }

  serviceHelpers.getAutenticate(
    '/api/v1/page/getMenuList/' + id + '/' + pageType,
    fnSuccess,
    fnError
  )
}

export function getSetting(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/page/getSetting/' + id,
    fnSuccess,
    fnError
  )
}

export function getHome(fnSuccess, fnError) {
  serviceHelpers.get(
    '/api/v1/page/getHome/' + storageData.sIdLanguage(),
    fnSuccess,
    fnError
  )
}

export function getLastActivity(fnSuccess, fnError) {
  serviceHelpers.get(
    '/api/v1/page/getLastActivity/' + storageData.sIdLanguage(),
    fnSuccess,
    fnError
  )
}

/** * END GET ***/

/** * PUT ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/page', data, fnSuccess, fnError)
}

export function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/page/clone/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(),
    null,
    fnSuccess,
    fnError
  )
}

/** * END PUT ***/

/** * POST ***/

export function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/page', data, fnSuccess, fnError)
}

export function postMenuList(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/page/menuList',
    data,
    fnSuccess,
    fnError
  )
}

export function postOnlinePage(valueOnline, id, fnSuccess, fnError) {
  const data = {
    online: valueOnline,
    id,
  }

  serviceHelpers.postAutenticate(
    '/api/v1/page/onlinePage',
    data,
    fnSuccess,
    fnError
  )
}

export function getRender(url, fnSuccess, fnError) {
  const data = {
    url,
    userId: storageData.sUserId(),
  }

  serviceHelpers.post('/api/v1/page/getRender', data, fnSuccess, fnError)
}

export function checkExistUrl(url, fnSuccess, fnError) {
  const data = {
    url,
  }

  serviceHelpers.postAutenticate(
    '/api/v1/page/checkExistUrl',
    data,
    fnSuccess,
    fnError
  )
}

export function checkExistUrlNotForThisPage(url, idPage, fnSuccess, fnError) {
  const data = {
    url,
    idPage,
  }

  serviceHelpers.postAutenticate(
    '/api/v1/page/checkExistUrl',
    data,
    fnSuccess,
    fnError
  )
}

export function checkExistUrlNotForThisNewsletter(
  url,
  idNewsletter,
  fnSuccess,
  fnError
) {
  const data = {
    url,
    idNewsletter,
  }

  serviceHelpers.postAutenticate(
    '/api/v1/page/checkExistUrl',
    data,
    fnSuccess,
    fnError
  )
}

/** * END POST ***/

/** * DELETE ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/page/' + id, fnSuccess, fnError)
}

/** * END DELETE ***/

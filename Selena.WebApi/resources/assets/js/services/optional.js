/** *   optionalServices   ***/

/** *   GET   ***/

export function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/optional/all/' + ln,
    fnSuccess,
    fnError
  )
}

/** *   END GET   ***/

/** *   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/optional', data, fnSuccess, fnError)
}

export function insertItemOptional(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/optional/insertItemOptional',
    data,
    fnSuccess,
    fnError
  )
}

/** *   END PUT   ***/

/** *   POST   ***/

export function update(dtoNation, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/optional',
    dtoNation,
    fnSuccess,
    fnError
  )
}

/** *   END POST   ***/

/** *   DELETE   ***/

export function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/optional/' + data, fnSuccess, fnError)
}

export function delItemOptional(idOptional, idItem, fnSuccess, fnError) {
  serviceHelpers.delAutenticate(
    '/api/v1/optional/delItemOptional/' + idOptional + '/' + idItem,
    fnSuccess,
    fnError
  )
}

/** *   END DELETE   ***/

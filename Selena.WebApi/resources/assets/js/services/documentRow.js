/** *  documentRowServices   ***/

/** *   GET   ***/

export function getSixtenByHead(idHead, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/documentrow/getSixtenByHead/' +
      idHead +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )

  // $.ajax({
  //     type: "GET",
  //     url: configData.wsRootServicesUrl +
  //       "/api/v1/documentrow/getSixtenByHead/" +
  //       idHead +
  //       "/" +
  //       menuHelpers.getIdFunctionByAdminUrl(),
  //     dataType: "json",
  //     headers: {
  //       Authorization: storageData.sTokenKey()
  //     },
  //     contentType: "application/json"
  //   })
  //   .done(function (result) {
  //     if (
  //       functionHelpers.isValued(fnSuccess) &&
  //       functionHelpers.isFunction(fnSuccess)
  //     ) {
  //       fnSuccess(result);
  //     }
  //   })
  //   .fail(function (response) {
  //     failServiceHelpers.fail(response);
  //   });
}

export function getMdMicrodetectorsByHead(idHead, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/documentrow/getMdMicrodetectorsByHead/' + idHead,
    fnSuccess,
    fnError
  )

  // $.ajax({
  //     type: "GET",
  //     url: configData.wsRootServicesUrl +
  //       "/api/v1/documentrow/getSixtenByHead/" +
  //       idHead +
  //       "/" +
  //       menuHelpers.getIdFunctionByAdminUrl(),
  //     dataType: "json",
  //     headers: {
  //       Authorization: storageData.sTokenKey()
  //     },
  //     contentType: "application/json"
  //   })
  //   .done(function (result) {
  //     if (
  //       functionHelpers.isValued(fnSuccess) &&
  //       functionHelpers.isFunction(fnSuccess)
  //     ) {
  //       fnSuccess(result);
  //     }
  //   })
  //   .fail(function (response) {
  //     failServiceHelpers.fail(response);
  //   });
}

export function getByErp(idErp, routeParameter, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/documentrow/getByErp/' +
      idErp +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(routeParameter),
    fnSuccess,
    fnError
  )

  // $.ajax({
  //     type: "GET",
  //     url: configData.wsRootServicesUrl + "/api/v1/documentrow/getByErp/" + idErp + "/" + menuHelpers.getIdFunctionByAdminUrl(routeParameter),
  //     dataType: "json",
  //     headers: {
  //       Authorization: storageData.sTokenKey()
  //     },
  //     contentType: "application/json"
  //   })
  //   .done(function (result) {
  //     if (
  //       functionHelpers.isValued(fnSuccess) &&
  //       functionHelpers.isFunction(fnSuccess)
  //     ) {
  //       fnSuccess(result);
  //     }
  //   })
  //   .fail(function (response) {
  //     failServiceHelpers.fail(response);
  //   });
}

export function getNumberPackageDDT(idCustomer, idMonth, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/documentrow/getNumberPackageDDT/' +
      idCustomer +
      '/' +
      idMonth +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )

  // $.ajax({
  //     type: "GET",
  //     url: configData.wsRootServicesUrl + "/api/v1/documentrow/getNumberPackageDDT/" + idCustomer + '/' + idMonth + "/" + menuHelpers.getIdFunctionByAdminUrl(),
  //     dataType: "json",
  //     headers: {
  //       Authorization: storageData.sTokenKey()
  //     },
  //     contentType: "application/json"
  //   })
  //   .done(function (result) {
  //     if (
  //       functionHelpers.isValued(fnSuccess) &&
  //       functionHelpers.isFunction(fnSuccess)
  //     ) {
  //       fnSuccess(result);
  //     }
  //   })
  //   .fail(function (response) {
  //     failServiceHelpers.fail(response);
  //   });
}

/** *   END GET   ***/

/** *   INSERT   ***/

export function insertBeforeDDT(
  idErp,
  quantity,
  idPackage,
  routeParameter,
  fnSuccess,
  fnError
) {
  const data = {
    idErp,
    idPackage,
    quantity,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(routeParameter),
  }

  serviceHelpers.putAutenticate(
    '/api/v1/documentrow/insertBeforeDDT',
    data,
    fnSuccess,
    fnError
  )

  // $.ajax({
  //     type: "PUT",
  //     url: configData.wsRootServicesUrl + "/api/v1/documentrow/insertBeforeDDT",
  //     dataType: "json",
  //     headers: {
  //       Authorization: storageData.sTokenKey()
  //     },
  //     contentType: "application/json",
  //     data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  //   })
  //   .done(function (result) {
  //     if (
  //       functionHelpers.isValued(fnSuccess) &&
  //       functionHelpers.isFunction(fnSuccess)
  //     ) {
  //       fnSuccess(result);
  //     }
  //   })
  //   .fail(function (response) {
  //     failServiceHelpers.fail(response);
  //   });
}

/** *   END INSERT   ***/

/** *   UPDATE   ***/

export function updateMdMicrodetectorsShippingRow(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/documentrow/updateMdMicrodetectorsShippingRow',
    data,
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'POST',
  //   url: configData.wsRootServicesUrl + '/api/v1/documenthead/confirmBeforeDDTSixten',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function updateMdMicrodetectorsResetShippingRow(
  data,
  fnSuccess,
  fnError
) {
  serviceHelpers.postAutenticate(
    '/api/v1/documentrow/updateMdMicrodetectorsResetShippingRow',
    data,
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'POST',
  //   url: configData.wsRootServicesUrl + '/api/v1/documenthead/confirmBeforeDDTSixten',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END UPDATE   ***/

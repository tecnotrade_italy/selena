/** *   processingviewServices   ***/

/** *   GET   ***/

export function getByViewValue(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/processingview/getByViewValue/' + id,
    fnSuccess,
    fnError
  )
}

export function updateInterventionUpdateProcessingSheet(
  DtoInterventions,
  fnSuccess,
  fnError
) {
  serviceHelpers.postAutenticate(
    '/api/v1/processingview/updateInterventionUpdateProcessingSheet',
    DtoInterventions,
    fnSuccess,
    fnError
  )
}

// export function getAllProcessingView(id, fnSuccess, fnError) {
// serviceHelpers.getAutenticate('/api/v1/processingview/getAllProcessingView/' + id, fnSuccess, fnError);
// }

export function getDescriptionAndPriceProcessingSheet(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/processingview/getDescriptionAndPriceProcessingSheet/' + id,
    fnSuccess,
    fnError
  )
}

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate(
    '/api/v1/processingview/' + id,
    fnSuccess,
    fnError
  )
}

export function getAllProcessingView(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/processingview/getAllProcessingView/' + idLanguage,
    fnSuccess,
    fnError
  )
}

/* ----- Gestione Materiali ----------- */

export function getAllProcessingSearch(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/processingview/getAllProcessingSearch/' + idLanguage,
    fnSuccess,
    fnError
  )
}

export function getAllViewQuoteSearch(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/processingview/getAllViewQuoteSearch/' + idLanguage,
    fnSuccess,
    fnError
  )
}

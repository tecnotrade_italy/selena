/** *   newsletterRecipientServices   ***/

/** *   GET   ***/

export function getContact(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/newsletterrecipient/contact/' + id,
    fnSuccess,
    fnError
  )
}

export function getCustomer(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/newsletterrecipient/customer/' + id,
    fnSuccess,
    fnError
  )
}

export function getGroupNewsletter(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/newsletterrecipient/groupnewsletter/' + id,
    fnSuccess,
    fnError
  )
}

export function getNumberRecipients(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/newsletterrecipient/getNumberRecipients/' + id,
    fnSuccess,
    fnError
  )
}

/** *   END GET   ***/

/** *   PUT   ***/

export function insertContact(dtoNewsletterRecipient, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/newsletterrecipient/contact',
    dtoNewsletterRecipient,
    fnSuccess,
    fnError
  )
}

export function insertCustomer(dtoNewsletterRecipient, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/newsletterrecipient/customer',
    dtoNewsletterRecipient,
    fnSuccess,
    fnError
  )
}

export function insertGroupNewsletter(
  dtoNewsletterRecipient,
  fnSuccess,
  fnError
) {
  serviceHelpers.putAutenticate(
    '/api/v1/newsletterrecipient/groupnewsletter',
    dtoNewsletterRecipient,
    fnSuccess,
    fnError
  )
}

/** *   END PUT   ***/

/** *   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate(
    '/api/v1/newsletterrecipient/' + id,
    fnSuccess,
    fnError
  )
}

export function delGroupNewsletter(idNewsletter, idGroup, fnSuccess, fnError) {
  serviceHelpers.delAutenticate(
    '/api/v1/newsletterrecipient/groupnewsletter/' +
      idNewsletter +
      '/' +
      idGroup,
    fnSuccess,
    fnError
  )
}

/** *   END DELETE   ***/

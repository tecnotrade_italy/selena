/** *   roleServices   ***/

/** * GET ***/

export function getByUser(idUser, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/role/getByUser/' +
      idUser +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/role/getByUser/' + idUser + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     "Accept": "application/json",
  //     "Authorization": storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** * END GET ***/

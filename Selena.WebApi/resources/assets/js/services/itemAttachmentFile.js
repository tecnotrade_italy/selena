/***   itemAttachmentFileServices   ***/

/***   GET   ***/

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/itemAttachmentFile/select?search=' + search, fnSuccess, fnError);
}

export function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/itemAttachmentFile/all/' + ln, fnSuccess, fnError);
}

export function getItemByIdAttachment(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/itemAttachmentFile/getItemByIdAttachment', data, fnSuccess, fnError);
}

/*export function getAllByAttachmentUser(id, idLanguage, fnSuccess, fnError) {

  serviceHelpers.get('/api/v1/itemAttachmentFile/getAllByAttachmentUser/' + id + '/' + idLanguage, fnSuccess, fnError);
  console.log('prova2');
}*/

export function getAllByAttachmentUser(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/itemAttachmentFile/getAllByAttachmentUser', data, fnSuccess, fnError);
}
/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/itemAttachmentFile', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(dtoNation, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/itemAttachmentFile', dtoNation, fnSuccess, fnError);
}

export function getByFile(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/itemAttachmentFile/getByFile', data, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/itemAttachmentFile/' +  data, fnSuccess, fnError);
}

export function delTranslation(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/itemAttachmentFile/delTranslation/' +  id, fnSuccess, fnError);
}



/***   END DELETE   ***/

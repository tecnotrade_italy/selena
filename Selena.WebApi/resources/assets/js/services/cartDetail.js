export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/CartDetail/' + id, fnSuccess, fnError)
}

export function getAllByCart(idCart, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/CartDetail/getAllByCart/' + idCart,
    fnSuccess,
    fnError
  )
}

export function insertRequest(
  cart_id,
  user_id,
  session_token,
  idlanguage,
  quantity,
  item_id,
  fnSuccess,
  fnError
) {
  if (session_token == null || session_token == '') {
    session_token =
      'Anonymous ' +
      Math.random().toString(36).substring(2, 15) +
      Math.random().toString(36).substring(2, 15)
    storageData.setTokenKey(session_token)
  }

  const data = {
    cart_id,
    user_id,
    session_token,
    idlanguage,
    quantity,
    item_id,
  }

  serviceHelpers.putAutenticate(
    '/api/v1/CartDetail/insertRequest',
    data,
    fnSuccess,
    fnError
  )
}

export function update(DtoCategories, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/CartDetail',
    DtoCartDetail,
    fnSuccess,
    fnError
  )
}

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/CartDetail/' + id, fnSuccess, fnError)
}

export function modifyQuantity(data, fnSuccess, fnError) {
  serviceHelpers.post(
    '/api/v1/CartDetail/modifyQuantity',
    data,
    fnSuccess,
    fnError
  )
}

/** *   groupTechnicalSpecificationServices   ***/

/** *   GET   ***/

export function getDtoTable(fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/grouptechnicalspecification/getDtoTable',
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/getDtoTable',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function getItems(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/grouptechnicalspecification/getItems/' + id,
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/getItems/' + id,
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function getTechnicalSpecification(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/grouptechnicalspecification/getTechnicalSpecification/' + id,
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/getTechnicalSpecification/' + id,
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function getGraphic(fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/grouptechnicalspecification/getGraphic',
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/getGraphic',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function checkExists(description, idLanguage, id, fnSuccess, fnError) {
  let url =
    configData.wsRootServicesUrl +
    '/api/v1/grouptechnicalspecification/checkExists/' +
    description +
    '/' +
    idLanguage

  if (functionHelpers.isValued(id)) {
    url = url + '/' + id
  }

  serviceHelpers.getAutenticate(url, fnSuccess, fnError)

  // $.ajax({
  //   type: 'GET',
  //   url: url,
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END GET   ***/

/** *   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/grouptechnicalspecification',
    data,
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(dtoGroupTechnicalSpecification))
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function addItem(id, idItem, fnSuccess, fnError) {
  serviceHelpers.putAutenticateWithoutData(
    '/api/v1/grouptechnicalspecification/addItem/' +
      id +
      '/' +
      idItem +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/addItem/' + id + '/' + idItem + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function addTechnicalSpecification(
  id,
  idTechnicalSpecification,
  fnSuccess,
  fnError
) {
  serviceHelpers.putAutenticateWithoutData(
    '/api/v1/grouptechnicalspecification/addTechnicalSpecification/' +
      id +
      '/' +
      idTechnicalSpecification +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/addTechnicalSpecification/' + id + '/' + idTechnicalSpecification + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END PUT   ***/

/** *   POST   ***/

export function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/grouptechnicalspecification/addTechnicalSpecification',
    data,
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'POST',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(dtoGroupTechnicalSpecification))
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

/** *   END POST   ***/

/** *   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate(
    '/api/v1/grouptechnicalspecification/' +
      id +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'DELETE',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

export function deleteItemGroupTechnicalSpecification(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate(
    '/api/v1/grouptechnicalspecification/deleteItemGroupTechnicalSpecification/' +
      id +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'DELETE',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/deleteItemGroupTechnicalSpecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

export function deleteGroupTechnicalSpecificationTechnicalSpecification(
  id,
  fnSuccess,
  fnError
) {
  serviceHelpers.delAutenticate(
    '/api/v1/grouptechnicalspecification/deleteGroupTechnicalSpecificationTechnicalSpecification/' +
      id +
      '/' +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  )

  // $.ajax({
  //   type: 'DELETE',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/deleteGroupTechnicalSpecificationTechnicalSpecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

/** *   END DELETE   ***/

/** *   processingviewServices   ***/

/** *   GET   ***/

export function updateInterventionUpdateViewQuotes(
  DtoQuotes,
  fnSuccess,
  fnError
) {
  serviceHelpers.postAutenticate(
    '/api/v1/viewquotes/updateInterventionUpdateViewQuotes',
    DtoQuotes,
    fnSuccess,
    fnError
  )
}

export function getByViewQuotes(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/viewquotes/getByViewQuotes/' + id,
    fnSuccess,
    fnError
  )
}

export function getAllViewQuote(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/viewquotes/getAllViewQuote/' + idLanguage,
    fnSuccess,
    fnError
  )
}

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/viewquotes/' + id, fnSuccess, fnError)
}

/** *   END POST   ***/

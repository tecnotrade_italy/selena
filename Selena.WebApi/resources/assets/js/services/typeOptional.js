/** *   typeOptionalServices   ***/

/** *   GET   ***/

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/typeoptional/select?search=' + search,
    fnSuccess,
    fnError
  )
}

export function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/typeoptional/all/' + ln,
    fnSuccess,
    fnError
  )
}

export function getAllTypeAndOptional(ln, idItem, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/typeoptional/getAllTypeAndOptional/' + ln + '/' + idItem,
    fnSuccess,
    fnError
  )
}

/** *   END GET   ***/

/** *   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/typeoptional',
    data,
    fnSuccess,
    fnError
  )
}

export function insertItemTypeOptional(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/typeoptional/insertItemTypeOptional',
    data,
    fnSuccess,
    fnError
  )
}

/** *   END PUT   ***/

/** *   POST   ***/

export function update(dtoNation, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/typeoptional',
    dtoNation,
    fnSuccess,
    fnError
  )
}

/** *   END POST   ***/

/** *   DELETE   ***/

export function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate(
    '/api/v1/typeoptional/' + data,
    fnSuccess,
    fnError
  )
}

export function delItemTypeOptional(
  idTypeOptional,
  idItem,
  fnSuccess,
  fnError
) {
  serviceHelpers.delAutenticate(
    '/api/v1/typeoptional/delItemTypeOptional/' + idTypeOptional + '/' + idItem,
    fnSuccess,
    fnError
  )
}

/** *   END DELETE   ***/

/** *   MenuAdminService   ***/

/** *   GET   ***/

export function getAllCustom(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }

  serviceHelpers.getAutenticate(
    '/api/v1/menuAdmin/allCustom/' + idLanguage,
    fnSuccess,
    fnError
  )
}

export function getByCode(code, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/menuAdmin/getByCode/' + code,
    fnSuccess,
    fnError
  )
}

export function getByRole(idRole, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/menuAdmin/getByRole/' + idRole,
    fnSuccess,
    fnError
  )
}

/** *   END GET   ***/

/** *   PUT   ***/

export function insert(dtoMenuAdmin, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/menuAdmin',
    dtoMenuAdmin,
    fnSuccess,
    fnError
  )
}

/** *   END PUT   ***/

/** *   POST   ***/

export function update(dtoMenuAdmin, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/menuAdmin',
    dtoMenuAdmin,
    fnSuccess,
    fnError
  )
}

/** *   END POST   ***/

/** *   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/menuAdmin/' + id, fnSuccess, fnError)
}

/** *   END DELETE   ***/

export function disable(menu_admin_id, role_id, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/menuAdmin/disable/' + menu_admin_id + '/' + role_id,
    fnSuccess,
    fnError
  )
}

export function enable(menu_admin_id, role_id, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/menuAdmin/enable/' + menu_admin_id + '/' + role_id,
    fnSuccess,
    fnError
  )
}

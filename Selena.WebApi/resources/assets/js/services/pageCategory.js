/** *   pageCategoryServices   ***/

/** *   GET   ***/

export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/pageCategory/' + id,
    fnSuccess,
    fnError
  )
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/pageCategory/select?search' + search,
    fnSuccess,
    fnError
  )
}

/** *   END GET   ***/

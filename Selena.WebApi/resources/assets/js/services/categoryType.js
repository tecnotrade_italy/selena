/** *   categoryTypeServices   ***/

/** *   GET   */

export function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/CategoryType/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey(),
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term),
        }
      },
      processResults(result) {
        return {
          results: result.data,
        }
      },
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty'),
  }
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/CategoryType/select?search=' + search,
    fnSuccess,
    fnError
  )
}
export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/CategoryType/' + id,
    fnSuccess,
    fnError
  )
}

export function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/CategoryType/' + id,
    fnSuccess,
    fnError
  )
}

export function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/CategoryType/all/' + idLanguage,
    fnSuccess,
    fnError
  )
}

/** *   END GET   */

/** *   PUT   ***/

export function insert(DtoCategoryType, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/CategoryType',
    DtoCategoryType,
    fnSuccess,
    fnError
  )
}

/** *   END PUT   ***/

/** *   POST   ***/

export function update(DtoCategoryType, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/CategoryType',
    DtoCategoryType,
    fnSuccess,
    fnError
  )
}

/** *   END POST   ***/

/** *   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate(
    '/api/v1/CategoryType/' + id,
    fnSuccess,
    fnError
  )
}

/** *   END DELETE   ***/

/** *   categoryTypeServices   ***/

/** *   GET   */

export function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/MessageType/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey(),
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function (params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term),
        }
      },
      processResults (result) {
          return {
            results: result.data
          };
        }
      },
    }
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/MessageType/select?search=' + search,
    fnSuccess,
    fnError
  )
}
export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/MessageType/' + id, fnSuccess, fnError)
}

export function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/MessageType/' + id, fnSuccess, fnError)
}

export function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/MessageType/all/' + idLanguage,
    fnSuccess,
    fnError
  )
}

/** *   END GET   */

/** *   voucherServices   ***/

/** *   GET   ***/

export function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/voucher/all', fnSuccess, fnError)
}

export function getByCode(code, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/voucher/getByCode/' + code,
    fnSuccess,
    fnError
  )
}

/** *   END GET   ***/

/** *   PUT   ***/

export function insert(dtoVoucher, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/voucher',
    dtoVoucher,
    fnSuccess,
    fnError
  )
}

/** *   END PUT   ***/

/** *   POST   ***/

export function update(dtoVoucher, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/voucher',
    dtoVoucher,
    fnSuccess,
    fnError
  )
}

/** *   END POST   ***/

/** *   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/voucher/' + id, fnSuccess, fnError)
}

/** *   END DELETE   ***/

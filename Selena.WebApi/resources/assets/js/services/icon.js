/** *   iconServices   ***/

/** *   GET   ***/

export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/icon/' + id, fnSuccess, fnError)
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/icon/select?search' + search,
    fnSuccess,
    fnError
  )
}

/** *   END GET   ***/

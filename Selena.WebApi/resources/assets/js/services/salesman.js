/** *   salesmanServices   ***/

/** *   POST   ***/

export function activeUserById(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/salesman/activeUserById', data, fnSuccess, fnError)
}

/** *   END POST   ***/

/** *   GET   ***/

export function getAllUsersBySalesman(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/salesman/getAllUsersBySalesman', data, fnSuccess, fnError)
}


/** *   END GET   ***/


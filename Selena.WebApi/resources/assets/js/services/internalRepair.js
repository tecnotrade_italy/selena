/** *   internalRepairServices   ***/

/** *   GET   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/internalrepair', data, fnSuccess, fnError)
}
export function getAllInternalRepair(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/internalrepair/getAllInternalRepair/' + idLanguage,
    fnSuccess,
    fnError
  )
}
export function getByInternalRepair(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/internalrepair/getByInternalRepair/' + id,
    fnSuccess,
    fnError
  )
}
export function updateInternalRepair(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/internalrepair/updateInternalRepair',
    data,
    fnSuccess,
    fnError
  )
}
/** * DELETE ***/
export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate(
    '/api/v1/internalrepair/' + id,
    fnSuccess,
    fnError
  )
}

export function getLastId(fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/internalrepair/getLastId/',
    fnSuccess,
    fnError
  )
}

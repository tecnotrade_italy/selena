/***   salesOrderServices   ***/

/***   GET   ***/
export function getAllBySales(id, idLanguage, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/salesorder/' + id + '/' + idLanguage, fnSuccess, fnError);
}



export function getAllBySalesmanId(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/salesorder/getAllBySalesmanId/', data, fnSuccess, fnError);
}
/***   END GET   ***/


/***   GET   ***/
export function getByDetailSaleOrder(id, idLanguage, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/salesorder/getByDetailSaleOrder/' + id + '/' + idLanguage, fnSuccess, fnError);
}
/***   END GET   ***/


export function getAllSalesOrder(ln, fnSuccess, fnError) {
    serviceHelpers.getAutenticate('/api/v1/salesorder/getAllSalesOrder', fnSuccess, fnError);
}
  
export function getBySalesOrder(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/salesorder/getBySalesOrder/' + id, fnSuccess, fnError);
}

export function updateSalesOrderDetail(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesorder/updateSalesOrderDetail/', data, fnSuccess, fnError);
}

export function updateTrackingShipment(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesorder/updateTrackingShipment/', data, fnSuccess, fnError);
}
export function updateNote(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesorder/updateNote/', data, fnSuccess, fnError);
}

export function insertAndSendMessages(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesorder/insertAndSendMessages/', data, fnSuccess, fnError);
}

export function SendMailCustomer(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesorder/SendMailCustomer/', data, fnSuccess, fnError);
}
export function SendShipment(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesorder/SendShipment/', data, fnSuccess, fnError);
}
export function insertMessages(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/salesorder/insertMessages/', data, fnSuccess, fnError);
}
export function insertFileOrderSales(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesorder/insertFileOrderSales/', data, fnSuccess, fnError);
}
export function deleteOrderDetail(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/salesorder/deleteOrderDetail/' + id, fnSuccess, fnError);
}
export function sendEmailDetailSummaryOrder(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesorder/sendEmailDetailSummaryOrder/', data, fnSuccess, fnError);
}


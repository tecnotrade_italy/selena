/** *   feedbackNewsletterServices   ***/

/** *   GET   ***/

export function getAllByNewsletter(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/reportnewsletter/allByNewsletter/' + id,
    fnSuccess,
    fnError
  )
}

export function getChart(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/reportnewsletter/getChart/' + id,
    fnSuccess,
    fnError
  )
}

/** *   END GET   ***/

/***   contactRequestServices   ***/

/***   GET   ***/

export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/contactrequest/all', fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/contactrequest', data, fnSuccess, fnError);
}


export function insertTourAvailable(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/contactrequest/insertTourAvailable', data, fnSuccess, fnError);
}

export function insertRequestLanguage(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/contactrequest/insertRequestLanguage', data, fnSuccess, fnError);
}

export function insertWorkForUs(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/contactrequest/insertworkforus', data, fnSuccess, fnError);
}

export function insertLandingeBookPcb(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/contactrequest/insertLandingeBookPcb', data, fnSuccess, fnError);
}

export function insertLogsContacts(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/contactrequest/insertLogsContacts', data, fnSuccess, fnError);
}

/***   END PUT   ***/

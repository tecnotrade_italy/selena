/** *   googleAnalyticsServices   ***/

/** *   GET   ***/

/** *   TOTALI ***/

export function getTotalUser(startDate, endDate, fnSuccess, fnError) {
  const data = { start: startDate, end: endDate }
  serviceHelpers.postAutenticate(
    '/api/v1/googleAnalytics/getTotalUser',
    data,
    fnSuccess,
    fnError
  )
}

export function getTotalSession(startDate, endDate, fnSuccess, fnError) {
  const data = { start: startDate, end: endDate }
  serviceHelpers.postAutenticate(
    '/api/v1/googleAnalytics/getTotalSession',
    data,
    fnSuccess,
    fnError
  )
}

export function getTotalPageView(startDate, endDate, fnSuccess, fnError) {
  const data = { start: startDate, end: endDate }
  serviceHelpers.postAutenticate(
    '/api/v1/googleAnalytics/getTotalPageView',
    data,
    fnSuccess,
    fnError
  )
}

export function getPageViewPerSession(startDate, endDate, fnSuccess, fnError) {
  const data = { start: startDate, end: endDate }
  serviceHelpers.postAutenticate(
    '/api/v1/googleAnalytics/getPageViewPerSession',
    data,
    fnSuccess,
    fnError
  )
}

export function getAvgSessionDuration(startDate, endDate, fnSuccess, fnError) {
  const data = { start: startDate, end: endDate }
  serviceHelpers.postAutenticate(
    '/api/v1/googleAnalytics/getAvgSessionDuration',
    data,
    fnSuccess,
    fnError
  )
}

export function getBounceRate(startDate, endDate, fnSuccess, fnError) {
  const data = { start: startDate, end: endDate }
  serviceHelpers.postAutenticate(
    '/api/v1/googleAnalytics/getBounceRate',
    data,
    fnSuccess,
    fnError
  )
}

export function getUserActiveRealTime(fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/googleAnalytics/getUserActiveRealTime',
    fnSuccess,
    fnError
  )
}

/** *   END TOTALI   ***/

export function getMostVisitedPages(startDate, endDate, fnSuccess, fnError) {
  const data = { start: startDate, end: endDate }
  serviceHelpers.postAutenticate(
    '/api/v1/googleAnalytics/getMostVisitedPages',
    data,
    fnSuccess,
    fnError
  )
}

export function getPagesViews(startDate, endDate, fnSuccess, fnError) {
  const data = { start: startDate, end: endDate }

  serviceHelpers.postAutenticate(
    '/api/v1/googleAnalytics/getPagesViews',
    data,
    fnSuccess,
    fnError
  )
}

export function getVisitorsViews(startDate, endDate, fnSuccess, fnError) {
  const data = { start: startDate, end: endDate }
  serviceHelpers.postAutenticate(
    '/api/v1/googleAnalytics/getVisitorsViews',
    data,
    fnSuccess,
    fnError
  )
}

export function getSourceSession(startDate, endDate, fnSuccess, fnError) {
  const data = { start: startDate, end: endDate }
  serviceHelpers.postAutenticate(
    '/api/v1/googleAnalytics/getSourceSession',
    data,
    fnSuccess,
    fnError
  )
}

export function getReturningVisitors(startDate, endDate, fnSuccess, fnError) {
  const data = { start: startDate, end: endDate }
  serviceHelpers.postAutenticate(
    '/api/v1/googleAnalytics/getReturningVisitors',
    data,
    fnSuccess,
    fnError
  )
}

export function getDeviceSession(startDate, endDate, fnSuccess, fnError) {
  const data = { start: startDate, end: endDate }
  serviceHelpers.postAutenticate(
    '/api/v1/googleAnalytics/getDeviceSession',
    data,
    fnSuccess,
    fnError
  )
}

/** *   END GET   ***/

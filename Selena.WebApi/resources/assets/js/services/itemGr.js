/** *   itemGrServices   ***/

/** *   GET   ***/

export function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage()
  }
  serviceHelpers.getAutenticate(
    '/api/v1/itemGr/all/' + idLanguage,
    fnSuccess,
    fnError
  )
}

export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/itemGr/' + id, fnSuccess, fnError)
}

export function insertNoAuth(DtoItemGr, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/itemGr/insertNoAuth',
    DtoItemGr,
    fnSuccess,
    fnError
  )
}

export function update(DtoItemGr, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/itemGr',
    DtoItemGr,
    fnSuccess,
    fnError
  )
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/itemGr/select?search' + search,
    fnSuccess,
    fnError
  )
}

/** * DELETE ***/
export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/itemGr/' + id, fnSuccess, fnError)
}
/** * DELETE ***/

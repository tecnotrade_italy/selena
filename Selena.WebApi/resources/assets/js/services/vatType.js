/** *   vatTypeServices   ***/

/** *   GET   */

export function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/vattype/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey(),
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term),
        }
      },
      processResults(result) {
        return {
          results: result.data,
        }
      },
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty'),
  }
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/vattype/select?search=' + search,
    fnSuccess,
    fnError
  )
}
export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/vattype/' + id, fnSuccess, fnError)
}

export function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/vattype/' + id, fnSuccess, fnError)
}

export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/vattype/all', fnSuccess, fnError)
}

/** *   END GET   */

/** *   PUT   ***/

export function insert(dtoVatType, fnSuccess, fnError) {
  serviceHelpers.putAutenticate(
    '/api/v1/vattype',
    dtoVatType,
    fnSuccess,
    fnError
  )
}

/** *   END PUT   ***/

/** *   POST   ***/

export function update(dtoVatType, fnSuccess, fnError) {
  serviceHelpers.postAutenticate(
    '/api/v1/vattype',
    dtoVatType,
    fnSuccess,
    fnError
  )
}

/** *   END POST   ***/

/** *   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/vattype/' + id, fnSuccess, fnError)
}

/** *   END DELETE   ***/

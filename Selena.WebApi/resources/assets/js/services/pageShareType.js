/** *   pageShareTypeServices   ***/

/** *   GET   ***/

export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/pageShareType/' + id,
    fnSuccess,
    fnError
  )
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    '/api/v1/pageShareType/select?search' + search,
    fnSuccess,
    fnError
  )
}

/** *   END GET   ***/

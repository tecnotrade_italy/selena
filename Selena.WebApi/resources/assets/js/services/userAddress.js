/** *   userAddressServices   ***/

/** *   POST   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/useraddress', data, fnSuccess, fnError)
}

export function activeAddressByUser(data, fnSuccess, fnError) {
  serviceHelpers.post(
    '/api/v1/useraddress/activeAddressByUser',
    data,
    fnSuccess,
    fnError
  )
}

/** *   END POST   ***/

/** *   GET   ***/

export function getAllByUser(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/useraddress/' + id, fnSuccess, fnError)
}

export function getAllUteByUserSecurPoint(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/people/getAllUteByUserSecurPoint/' + id, fnSuccess, fnError)
}

export function insertUteByUserSecurPoint(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/people/insertUteByUserSecurPoint', data, fnSuccess, fnError)
}

export function resetAddressByCartId(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/useraddress/resetAddressByCartId', data, fnSuccess, fnError)
}


/** *   END GET   ***/

/** *   DELETE   ***/

export function deleteAddressById(id, fnSuccess, fnError) {
  serviceHelpers.del(
    '/api/v1/useraddress/deleteAddressById/' + id,
    fnSuccess,
    fnError
  )
}

/** *   END DELETE   ***/

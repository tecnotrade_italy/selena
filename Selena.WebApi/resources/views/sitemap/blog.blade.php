<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($blog as $b)
        <url>
            <loc>{{$host}}{{ $b->url }}</loc>
            <lastmod><?php
                $data = explode(" ", $b->updated_at);
                echo $data[0]
            ?></lastmod>
        </url>
    @endforeach
</urlset>


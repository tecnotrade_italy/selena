<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($keywords as $u)
        <url>
            <loc>{{$host}}{{ $u->url }}</loc>
            <lastmod><?php
                $data = explode(" ", $u->updated_at);
                echo $data[0]
            ?></lastmod>
        </url>
    @endforeach
</urlset>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @if($showNews)
    <sitemap>
        <loc>{{$host}}/sitemap.xml/news</loc>
    </sitemap>
    @endif

    @if($showBlog)
    <sitemap>
        <loc>{{$host}}/sitemap.xml/blog</loc>
    </sitemap>
    @endif

    @if($showPages)
    <sitemap>
        <loc>{{$host}}/sitemap.xml/pages</loc>
    </sitemap>
    @endif

    @if($showItems)
    <sitemap>
        <loc>{{$host}}/sitemap.xml/items</loc>
    </sitemap>
    @endif

    @if($showPricesItems)
    <sitemap>
        <loc>{{$host}}/sitemap.xml/pricesItems</loc>
    </sitemap>
    @endif

    @if($showPhotoItems)
    <sitemap>
        <loc>{{$host}}/sitemap.xml/photoItems</loc>
    </sitemap>
    @endif

    @if($showCategories)
    <sitemap>
        <loc>{{$host}}/sitemap.xml/categories</loc>
    </sitemap>
    @endif

    @if($showLinkcategories)
    <sitemap>
        <loc>{{$host}}/sitemap.xml/linkcategories</loc>
    </sitemap>
    @endif

    @if($showUser)
    <sitemap>
        <loc>{{$host}}/sitemap.xml/users</loc>
    </sitemap>
    @endif

    @if($showKeywords)
    <sitemap>
        <loc>{{$host}}/sitemap.xml/keywords</loc>
    </sitemap>
    @endif

    @for($x=0; $x<$communityImage; $x++)
    <sitemap>
        <loc>{{$host}}/sitemap.xml/communityImage/{{$x}}</loc>
    </sitemap>
    @endfor
</sitemapindex>
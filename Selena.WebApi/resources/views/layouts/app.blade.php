<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="{{ $hideSearchEngine }}">
        <title>{{ $seoTitle }}</title>
        <meta name="description" content="{{ $seoDescription }}">
        <meta name="keywords" content="{{ $seoKeyword }}">


        <meta property="og:url" content="{{ url()->current() }}">
        <meta property="og:title" content="{{ $shareTitle }}">
        <meta property="og:description" content="{{ $seoDescription }}">
        <meta property="og:image" content="{{ $shareUrlCoverImage }}">
        <meta property="og:image:url" content="{{ $shareUrlCoverImage }}">
        <meta property="og:image:secure_url" content="{{ $shareUrlCoverImage }}">
        <meta name="twitter:card" content="summary">
        <meta property="og:type" content="article">

        <link rel="canonical" href="{{ url()->current() }}">

        <link rel="stylesheet" href="/storage/css/web.css">
        <link rel="stylesheet" href="/storage/css/webhead.css">
        <link rel="stylesheet" href="{{ asset('/style/css/style.css') }}">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{$analytics}}"></script>
        <script>
        {!! $stringAnalitycs !!}
        </script>
        
        <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/spinkit/2.0.1/spinkit.min.css"-->
        <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"-->
        <!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/justifiedGallery/3.8.1/css/justifiedGallery.min.css"-->
        
    </head>
    <body>
        
        <div id="header">
        {!! $header !!}
        </div>
        
        @yield('content')
        <div class="mainPages">
            {!! $body !!}
        </div>

        <div id="footer">
            {!! $footer !!}
        </div>
        
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'">
        
        <script src="{{ asset('/script/js/app.js') }}" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" type="text/javascript"></script>
        <script src=https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js type="text/javascript"></script>
        <script src="/storage/js/sitehead.js" type="text/javascript"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" type="text/javascript"></script>

        <script src="/storage/js/site.js" type="text/javascript"></script>
        
        <script type="text/javascript">
            {!! $js !!}
        </script>
    </body>
</html>

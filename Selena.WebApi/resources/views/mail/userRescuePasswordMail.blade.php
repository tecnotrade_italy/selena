<!doctype html><HTML>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<TITLE>Recupero password</TITLE>
<style>
*,html,body, label,  a, .button	{ 
	font-family: 'Open Sans', serif !important;
	color:#787878;
}
html	{
	font-size: 12px;
}
a.button { color: #fff }
h1, h2, h3, h4, h5, h6	{
	font-family: 'Open Sans', serif !important;
	font-weight:300; 
	color:black
}
h1	{
	font-size:2rem;
	text-align:center
}
h2	{
	font-size:1.7rem;
	text-transform: capitalize;
	margin:0
} 
h3	{
	font-size:1rem
}
h4	{
	font-size:0.8rem
}
h5	{
	font-size:0.5rem
}
h6	{
	font-size:0.3rem
}
h6	{
	line-height:1.3em;
	color:#787878;
	text-align:justify
}
b	{
	font-weight:700
	}

a	{  
	display: inline-block;
	position: relative;
	padding-bottom: 5px;
}

a:hover{ text-decoration:underline}

.button {
    color: #fff;
    background: #410098;
    padding: 10px 20px;
    margin: 25px;
    font-size: 0.9rem;
    text-transform: capitalize;
    border-radius: 5px;
    display: inline-block;
    text-align: center;
}
.TabColore1 {
	background:#235F96;
	color:#fff;
}
.TabColore2 {
	background:#E9E9E9;
	color:#333;
	
}
.style2 {font-size: 12px}

.tblDati {
	border: 1px solid #aaa;
}
.tblDati thead tr {
	background: #ddd;
}
.tblDati tbody tr:nth-child(even) {
	background: #efefef;
}
.tblDati tbody tr:nth-child(odd) {
	background: #fff;
}
{{ $cssEmail }}
</style>


</head>

<body bgcolor="#FFFFFF">
<div style="width:950px; margin:0 auto">

<table width="100%" cellspacing="5" cellpadding="5" style="margin:0 0 1rem 0">
  <tr> 
    <td height="1" width="1"></td>
    <td height="1" ></td>
    <td height="1" width="1" ></td>
  </tr>
  <tr> 
    <td width="1"></td>
    <td >
      <table width="100%" cellspacing="5" cellpadding="5" style="margin:0 0 1rem 0">
        <tr align="center"> 
          <td colspan="2">
          <h1>
          Ciao {{ $name }},<br>
            Cliccando sul link sotto potrai resettare la tua password<br>
            <a href="{{ $url }}/resetpassword?i={{ $id }}">cambia password</a>            
          </h1>
        
        </td>
        </tr>
      </table>
    </td>
    <td width="1"></td>
  </tr>
  <tr> 
    <td width="1" height="1" ></td>
    <td height="1" ></td>
    <td width="1" height="1" ></td>
  </tr>
  </table>
  </div>

</BODY>
</HTML>
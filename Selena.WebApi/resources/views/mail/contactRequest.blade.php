<!doctype html>
<HTML>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <style>
           {{ $cssEmail }}
        </style>
    </head>
    <body>
        <p>Richiesta di contatto da {{ $name }} {{ $surname }} - {{$email}} - {{$telefono}}</p><br>
        <p>{{$businessName}}</p><br>
        <p>{{$address}}</p><br>
        <p>{{$postalCode}} {{$city}} ({{$province}})</p><br>
        <p>{{$description}}</p><br>
        <p>{{$vatNumber}}</p><br>
        <p>{{$processing_of_personal_data}}</p><br>
    </body>
</HTML>
<!doctype html>
<HTML>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <style>
           {{ $cssEmail }}
        </style>
    </head>
    <body>
                    <p>Richiesta lavora con noi da:</p><br> 
                    <p>Nome: {{ $name }}</p><br>     
                    <p>Cognome: {{ $surname }}</p><br> 
                    <p>Data di nascita: {{ $date }}</p><br> 
                    <p>Luogo di nascita: {{ $cityofborn }}</p><br> 
                    <p>Paese di nascita: {{$countryofbirth}}</p>
                    <p>Email: {{ $email }}</p><br> 
                    <p>Sesso: {{$gender}}</p><br> 
                    <p>Indirizzo: {{ $address }}</p><br> 
                    <p>cap: {{ $cap }}</p><br> 
                    <p>Città: {{ $city }}</p><br> 
                    <p>Paese di domicilio: {{ $countryofresidence }}</p>
                    <p>Regione di domicilio: {{ $regionofdomicily }}</p>
                    <p>Citta di domicilio: {{ $cityofresidence }}</p>
                    <p>Provincia: {{ $province }}</p><br> 
                    <p>Nazione: {{ $nation }}</p><br> 
                    <p>Telefono: {{ $phoneNumber }}</p><br> 
                    <p>Codice Fiscale: {{ $fiscalcode }}</p>
                    <p>Candidatura per: {{ $position }}</p><br> 
                    <p>Sito Web: {{ $website }}</p>
                    <p>Patente: {{ $license }}</p>
                    <p>Mezzo di Trasporto: {{ $meansoftransport }}</p>
                    <p>Come ho avuto informazioni di questa candidatura: {{ $info }}</p><br>
                    <p>Attualmente lavoro per: {{ $actualCompany }}</p>      	
				
    </body>
</HTML>





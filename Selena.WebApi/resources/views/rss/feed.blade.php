<?=
'<?xml version="1.0" encoding="UTF-8"?>'
//<guid>https://cameranation.it {!! $post['url'] !!}</guid>
/*
<link>https://cameranation.it {{ $post['url'] }}</link>
                <description><![CDATA[{{ $post['text'] }}]]></description>
                <category>{{ $post['category'] }}</category>
                <author><![CDATA[info@cameranation.it (Camera Nation)]]></author>
                
                <pubDate>{{ $post['dateString'] }}</pubDate>
                <content:encoded><![CDATA[{!! $post['content'] !!}]]></content:encoded>*/
?>
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/"  xmlns:dc="http://purl.org/dc/elements/1.1/"  xmlns:atom="http://www.w3.org/2005/Atom"  xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/">
    <channel>
        <title>Camera Nation</title>
        <link>https://cameranation.it</link>
        <description>Il portale della fotografia nato per condividere la passione per questa meravigliosa arte. </description>
        <language>it-IT</language>
        <lastBuildDate>{{ date("D, d M Y H:i:s +0000", strtotime(date("Y-m-d H:i:s")) - 60) }}</lastBuildDate>
        <sy:updatePeriod>hourly</sy:updatePeriod>
	    <sy:updateFrequency>1</sy:updateFrequency>

        @foreach ($posts as $post)
            <item>
                <title><![CDATA[{!! $post->title !!}]]></title>
                <link>https://cameranation.it{!! str_replace('’','%E2%80%99', str_replace('€','%E2%82%AC', str_replace('à','%C3%A0', str_replace(':','%3A', $post->url)))) !!}</link>
                <guid>https://cameranation.it{!! str_replace('’','%E2%80%99', str_replace('€','%E2%82%AC', str_replace('à','%C3%A0', str_replace(':','%3A', $post->url)))) !!}</guid>
                <enclosure url="{!! str_replace(' ','%20', $post->urlCoverImage) !!}" length="10000" type="image/jpeg"></enclosure>
                <description><![CDATA[{{ $post->text }}]]></description>
                <author><![CDATA[info@cameranation.it (Camera Nation)]]></author>
                <pubDate>{{ $post->dateString }}</pubDate>
                <content:encoded><![CDATA[{!! $post->content !!}]]></content:encoded>
            </item>
        @endforeach
    </channel>
</rss>
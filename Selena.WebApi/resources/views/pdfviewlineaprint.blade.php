
<img src="{{$itemPdfDimension->imgPdf}}" style="margin-top:{{$itemPdfDimension->height}}mm;min-width:{{$itemPdfDimension->width}}mm;width:{{$itemPdfDimension->width}}mm;max-width:{{$itemPdfDimension->width}}mm; height:auto">
<div>
            <img src="https://www.lineaprint.it/storage/images/logo.png" style="max-width:200px;">
</div>
    <div>
            <h6 style="font-weight:bold;font-family: 'Helvetica';font-size:14px;">ASTUCCIO {{ $itemPdfDimension->A }}b x {{ $itemPdfDimension->B }}p x {{ $itemPdfDimension->H }}h mm</h6>
    </div>
<div>

            <p style="font-size:16px;font-family: 'Helvetica';">
Per la creazione della grafica utilizzare il livello Grafica o crearne dei nuovi a seconda delle esigenze, considerando che il livello "Fustella" indica il taglio e le pieghe della scatolina.<br>
Se si vuole evitare che testi o immagini vengano sulle pieghe o addirittura sul taglio, mantenete una distanza di almeno 3 mm da queste linee.<br>
Per evitare che rimangano bordi bianchi dopo la fustellatura eccedere di almeno 3 mm dalle linee di fustelle.<br>
Una volta ultimata la creazione della grafica, convertire i Fonts in tracciati e creare il pdf (senza attivare i segni di taglio, le abbondanze e i crocini).</p>
        </div>
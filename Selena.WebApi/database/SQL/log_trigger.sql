CREATE OR REPLACE FUNCTION public.log_trigger()
	RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 
AS $BODY$
DECLARE
	tableName text;
	triggerName text;
BEGIN
	FOR tableName IN EXECUTE 'SELECT DISTINCT(table_name) FROM information_schema.columns WHERE table_schema = ''public'' AND table_name != ''migrations'' AND table_name NOT LIKE ''oauth%'''
	LOOP
		triggerName = format('log_trigger_%s', tableName);
		EXECUTE format('DROP TRIGGER IF EXISTS %s ON %s', triggerName, tableName);
		EXECUTE format('CREATE TRIGGER %s
						AFTER INSERT OR DELETE OR UPDATE 
						ON public.%s
						FOR EACH ROW
							EXECUTE PROCEDURE public.log_function()'
					  , triggerName, tableName);
	END LOOP;
END $BODY$;

-- # FOR EXECUTE
-- SELECT log_trigger();
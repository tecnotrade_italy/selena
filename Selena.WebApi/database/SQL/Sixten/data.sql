-- CUSTOMER
SELECT 'INSERT INTO customers (company, business_name, vat_number, fiscal_code, address, erp_id, created_id, language_id, carrier_id, carriage_id)
values (true, ''' + rtrim(ltrim(replace(Descrizione, '''',''''''))) + ''', ''' + rtrim(ltrim(PartitaIva)) + ''', ''' + rtrim(ltrim(CodiceFiscale)) + ''', ''' + rtrim(ltrim(replace(Indirizzo, '''',''''''))) + ''', ' + CAST(Id_CF AS varchar(10)) + ', (SELECT id FROM users WHERE name = ''prando''), (SELECT id FROM languages WHERE "default" = true), (SELECT id FROM carriers WHERE code = ''' + LTRIM(rtrim(ISNULL(Cd_DOPorto, ''))) + '''), (SELECT id FROM carriages WHERE code = ''' + LTRIM(rtrim(ISNULL(Cd_DOVettore, ''))) + '''));'
FROM CF
WHERE Cd_CF in (SELECT TOP 10
    Cd_CF
FROM DOTes
WHERE Cd_DO = 'OC'
ORDER BY Id_DoTes DESC);

-- CARRIAGES
SELECT 'INSERT INTO carriages (code, created_id) VALUES (''' + LTRIM(RTRIM(Cd_DOPorto)) + ''', (SELECT id FROM users WHERE name = ''prando''));
INSERT INTO carriages_languages (carriage_id, language_id, description, created_id) VALUES ((SELECT id FROM carriages WHERE code = ''' + LTRIM(RTRIM(Cd_DOPorto)) + '''), (SELECT id FROM languages WHERE "default" = true), ''' + LTRIM(RTRIM(Descrizione)) + ''', (SELECT id FROM users WHERE name = ''prando''));'
FROM DOPorto

-- CARRIERS
SELECT 'INSERT INTO carriers (code, created_id) VALUES (''' + LTRIM(RTRIM(Cd_DoVettore)) + ''', (SELECT id FROM users WHERE name = ''prando''));
INSERT INTO carriers_languages (carrier_id, language_id, description, created_id) VALUES ((SELECT id FROM carriers WHERE code = ''' + LTRIM(RTRIM(Cd_DoVettore)) + '''), (SELECT id FROM languages WHERE "default" = true), ''' + LTRIM(RTRIM(Descrizione)) + ''', (SELECT id FROM users WHERE name = ''prando''));'
FROM DOVettore
WHERE Cd_DoVettore IN (SELECT TOP 10
    Cd_DoVettore_1
FROM DOTes
WHERE Cd_DO = 'OC'
ORDER BY Id_DoTes DESC);

-- CURRENCIES
SELECT 'INSERT INTO currencies (code, symbol, created_id) VALUES (''' + LTRIM(RTRIM(Cd_VL)) + ''', ''' + LTRIM(RTRIM(Simbolo)) + ''', (SELECT id FROM users WHERE name = ''prando''));
INSERT INTO currencies_languages (currency_id, language_id, description, created_id) VALUES ((SELECT id FROM currencies WHERE code = ''' + LTRIM(RTRIM(Cd_VL)) + '''), (SELECT id FROM languages WHERE "default" = true), ''' + LTRIM(RTRIM(Descrizione)) + ''', (SELECT id FROM users WHERE name = ''prando''));'
FROM VL
WHERE Cd_VL IN (SELECT TOP 10
    Cd_VL
FROM DOTes
WHERE Cd_DO = 'OC'
ORDER BY Id_DoTes DESC);

-- PAYMENTS_TYPES
SELECT 'INSERT INTO payments_types (code, erp_id, created_id) VALUES (''' + LTRIM(RTRIM(Cd_PG)) + ''', ''' + LTRIM(RTRIM(Id_PG)) + ''', (SELECT id FROM users WHERE name = ''prando''));
INSERT INTO languages_payments_types (language_id, payment_type_id, description, created_id)
VALUES ((SELECT id FROM languages WHERE "default" = true), (SELECT id FROM payments_types WHERE erp_id = ''' + LTRIM(RTRIM(Id_PG)) + '''), ''' + LTRIM(RTRIM(Descrizione)) + ''',(SELECT id FROM users WHERE name = ''prando''));'
FROM PG
WHERE Cd_PG IN (SELECT TOP 10
    Cd_PG
FROM DOTes
WHERE Cd_DO = 'OC'
ORDER BY Id_DoTes DESC);

-- UNITS OF MEASURES
SELECT 'INSERT INTO units_of_measures (code, created_id) VALUES (''' + LTRIM(RTRIM(Cd_ARMisura)) + ''', (SELECT id FROM users WHERE name = ''prando''));
INSERT INTO languages_units_of_measures (language_id, unit_of_measure_id, description, created_id)
VALUES((SELECT id FROM languages WHERE "default" = true), 
(SELECT id FROM units_of_measures WHERE code = ''' + LTRIM(RTRIM(Cd_ARMisura)) + '''),
''' + LTRIM(RTRIM(Descrizione)) + ''', (SELECT id FROM users WHERE name = ''prando''));'
FROM ARMisura
WHERE Cd_ARMisura IN (SELECT Cd_ARMisura
FROM AR
WHERE Cd_AR IN (SELECT Cd_AR
FROM DORig
WHERE Id_DOTes IN (SELECT TOP 10
    Id_DOTes
FROM DOTes
WHERE Cd_DO = 'OC'
ORDER BY Id_DoTes DESC)));

-- ITEMS
SELECT 'INSERT INTO Items (internal_code, document_row_type_id, unit_of_measure_id, height, width, depth, weight, created_id)
VALUES (''' + LTRIM(RTRIM(Cd_AR)) + ''', (SELECT id FROM documents_rows_types WHERE code = ''Item''),
(SELECT id FROM units_of_measures WHERE code = ''' + LTRIM(RTRIM(Cd_ARMisura)) + '''), ' + CAST(Altezza as varchar)
+ ', ' + CAST(Larghezza as varchar) + ', ' + CAST(Lunghezza as varchar) + ', ' + CAST(PesoNetto as varchar)
+ ', (SELECT id FROM users WHERE name = ''prando''));
INSERT INTO items_languages (item_id, language_id, description, created_id)
VALUES ((SELECT id FROM items WHERE internal_code = ''' + LTRIM(RTRIM(Cd_AR)) + ''')
, (SELECT id FROM languages WHERE "default" = true), ''' + LTRIM(RTRIM(Descrizione)) + '''
, (SELECT id FROM users WHERE name = ''prando''));'
FROM AR
WHERE Cd_AR IN (SELECT Cd_AR
FROM DORig
WHERE Id_DOTes IN (SELECT TOP 10
    Id_DOTes
FROM DOTes
WHERE Cd_DO = 'OC'
ORDER BY Id_DoTes DESC));

-- DOCUMENTS HEADS
SELECT TOP 10
    'INSERT INTO documents_heads (customer_id, document_type_id, reference_code, date, expected_shipping_date, currency_id, carriage_id, payment_type_id, carrier_id, code, erp_id, created_id)
VALUES ((SELECT id FROM customers where erp_id = ''' +  CAST((SELECT CF.Id_CF
    FROM CF
    WHERE CF.Cd_CF = DOTes.Cd_CF) as varchar) + '''), 
(SELECT id FROM documents_types WHERE code = ''Order''),
''' + LTRIM(RTRIM(ISNULL(NumeroDocRif, ''))) + ''', ''' + CONVERT(varchar, DataDoc, 111) + ''', ''' + CONVERT(varchar, DataConsegna, 111)  + ''',
(SELECT id FROM currencies WHERE code = ''' + LTRIM(RTRIM(ISNULL(Cd_VL, ''))) + '''), 
(SELECT id FROM carriages WHERE code = ''' + LTRIM(RTRIM(ISNULL(Cd_PG, ''))) + '''), 
(SELECT id FROM payments_types WHERE code = ''' + LTRIM(RTRIM(ISNULL(Cd_DOPorto, ''))) + '''), 
(SELECT id FROM carriers WHERE code = ''' + LTRIM(RTRIM(ISNULL(Cd_DoVettore_1, ''))) + '''), 
''' + LTRIM(RTRIM(NumeroDoc)) + ''', ' + CAST(Id_DoTes AS varchar(10)) + ', (SELECT id FROM users WHERE name = ''prando''));'
FROM DOTes
WHERE Cd_DO = 'OC'
ORDER BY Id_DoTes DESC;


-- DOCUMENTS ROWS
SELECT 'INSERT INTO documents_rows (document_head_id, document_row_type_id, item_id, unit_of_measure_id, quantity, erp_id, created_id)
VALUES ((SELECT id FROM documents_heads WHERE erp_id = ''' + LTRIM(RTRIM(Id_DOTes)) + '''),
(SELECT id FROM documents_rows_types WHERE code = ''Item''),
(SELECT id FROM items WHERE internal_code = ''' + LTRIM(RTRIM(ISNULL(Cd_AR, ''))) + '''),
(SELECT id FROM units_of_measures WHERE code = ''' + LTRIM(RTRIM(ISNULL(Cd_ARMisura, ''))) + '''),' + CAST(Qta AS varchar) + ','
+ LTRIM(RTRIM(Id_DORig)) + ', (SELECT id FROM users WHERE name = ''prando''));'
FROM DORig
WHERE Id_DOTes IN (SELECT TOP 10
    Id_DOTes
FROM DOTes
WHERE Cd_DO = 'OC'
ORDER BY Id_DoTes DESC);

-- PACKAGING TYPES SIXTEN
insert into packaging_types_sixten
    (erp_id, description, created_id, created_at)
values
    ('001', 'CARTONE', (select id
        from users), now());
insert into packaging_types_sixten
    (erp_id, description, created_id, created_at)
values
    ('002', 'BUSTA', (select id
        from users), now());
insert into packaging_types_sixten
    (erp_id, description, created_id, created_at)
values
    ('003', 'BARRA', (select id
        from users), now());
insert into packaging_types_sixten
    (erp_id, description, created_id, created_at)
values
    ('004', 'CONTENITORE', (select id
        from users), now());
insert into packaging_types_sixten
    (erp_id, description, created_id, created_at)
values
    ('005', 'A VISTA', (select id
        from users), now());
insert into packaging_types_sixten
    (erp_id, description, created_id, created_at)
values
    ('006', 'CASSA', (select id
        from users), now());
insert into packaging_types_sixten
    (erp_id, description, created_id, created_at)
values
    ('007', 'BIDONE', (select id
        from users), now());
insert into packaging_types_sixten
    (erp_id, description, created_id, created_at)
values
    ('008', 'BANCALE', (select id
        from users), now());
insert into packaging_types_sixten
    (erp_id, description, created_id, created_at)
values
    ('009', 'ROTOLO', (select id
        from users), now());
insert into packaging_types_sixten
    (erp_id, description, created_id, created_at)
values
    ('010', 'TUBO', (select id
        from users), now());
insert into packaging_types_sixten
    (erp_id, description, created_id, created_at)
values
    ('011', 'BOBINA', (select id
        from users), now());

-- SHIPMENT CODE SIXTEN
insert into shipments_codes_sixtens
    (erp_id, description, created_id, created_at)
values
    ('001', 'VETTORE', (select id
        from users), now());
insert into shipments_codes_sixtens
    (erp_id, description, created_id, created_at)
values
    ('002', 'MITTENTE', (select id
        from users), now());
insert into shipments_codes_sixtens
    (erp_id, description, created_id, created_at)
values
    ('999', 'DESTINATARIO', (select id
        from users), now());

-- CUSTOMER SIXTEN
select 'INSERT INTO customers_sixten (customer_id, shipment_code_sixten_id, created_id) values (
(SELECT id FROM customers WHERE erp_id = ''' + LTRIM(RTRIM(Cd_CF)) + '''), 
(SELECT id FROM shipments_codes_sixtens WHERE erp_id = ''' + LTRIM(RTRIM(Cd_DOSped)) + '''),
(SELECT id FROM users));'
from CF
where Cd_CF IN ('C000032','C000193','C000841','C001001','C001034','C001106','C001110','C001269','C002089','C002628','C002660','C000325');
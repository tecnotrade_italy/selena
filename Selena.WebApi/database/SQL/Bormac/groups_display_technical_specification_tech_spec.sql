select 'INSERT INTO groups_display_technical_specification_tech_spec (group_display_technical_specification_id, technical_specification_id, created_id, "order") values ((SELECT group_display_technical_specification_id from groups_display_technicals_specifications_languages where description = ''' || replace(gdtsl.description, '''', '''''') || '''), (SELECT technical_specification_id from languages_technicals_specifications where description = ''' || replace(lts.description, '''', '''''') || '''), (SELECT id from users where name = ''prando''), ' || gdtsts.order || ');'
from groups_display_technical_specification_tech_spec gdtsts
inner join groups_display_technicals_specifications gdts on gdtsts.group_display_technical_specification_id = gdts.id
inner join groups_display_technicals_specifications_languages gdtsl on gdts.id = gdtsl.group_display_technical_specification_id
inner join technicals_specifications ts on gdtsts.technical_specification_id = ts.id
inner join languages_technicals_specifications lts on ts.id = lts.technical_specification_id
where lts.language_id = (select id from languages l where "default" = true)
and gdtsl.language_id = (select id from languages l where "default" = true);
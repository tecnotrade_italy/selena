CREATE OR REPLACE FUNCTION public.log_function()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF 
AS $BODY$
DECLARE
	dblink_name text;
	i int;
	u text;
	op text;
	col text;
	data_type text;
	oldVal text;
	newVal text;
	tmp text;
BEGIN
	dblink_name = 'selena_log_link' || TG_TABLE_NAME || '_' || txid_current();
	
	PERFORM dblink_connect(dblink_name,'selena_log_remote');

	IF TG_OP = 'INSERT' THEN
		u = NEW.created_id;
		i = NEW.id;
		op = 'INSERT';
	ELSE
		IF TG_OP = 'DELETE' THEN
			u = OLD.created_id;
			i = OLD.id;
			
			op = 'DELETE';
		ELSE
			IF to_jsonb(NEW) ? 'deleted_at' THEN
				u = OLD.created_id;
				i = OLD.id;
				op = 'SOFDEL';
			ELSE
				u = NEW.updated_id;
				i = NEW.id;
				op = 'UPDATE';
			END IF;
		END IF;
	END IF;
											
	PERFORM dblink(dblink_name, format('INSERT INTO logs_headers (action, key_id, table_name, user_id, created_at, transaction_id)
											VALUES (''%s'', %s,  ''%s'', %s, ''%s'', %s)'
											, op, i, TG_TABLE_NAME, u, now(), txid_current()));
	IF TG_OP != 'DELETE' THEN		
		FOR col IN EXECUTE format('SELECT column_name FROM information_schema.columns WHERE table_name = ''%s'' AND column_name <> ''id''', TG_TABLE_NAME)
		LOOP
			EXECUTE 'SELECT ($1).' || col || '::text' INTO oldVal USING OLD;
			EXECUTE 'SELECT ($1).' || col || '::text' INTO newVal USING NEW;
			EXECUTE 'SELECT data_type FROM information_schema.columns WHERE table_name = ''' || TG_TABLE_NAME || ''' AND column_name = ''' || col || '''' INTO data_type;
			IF coalesce(oldVal, '') != coalesce(newVal, '') THEN
				IF data_type = 'text' THEN				
				PERFORM dblink(dblink_name, format('INSERT INTO logs_details_text (header_id, column_name, value)
														VALUES (
																	( SELECT MAX(id) FROM logs_headers WHERE table_name = ''%s'' AND transaction_id = %s )
																	, ''%s'', ''%s'')'
																	, TG_TABLE_NAME, txid_current(), col, REPLACE(newVal, '''','''''')));
				ELSE				
				PERFORM dblink(dblink_name, format('INSERT INTO logs_details (header_id, column_name, value)
														VALUES (
																	( SELECT MAX(id) FROM logs_headers WHERE table_name = ''%s'' AND transaction_id = %s )
																	, ''%s'', ''%s'')'
																	, TG_TABLE_NAME, txid_current(), col, REPLACE(newVal, '''','''''')));
				END IF;
			END IF;
		END LOOP;
	END IF;
	
	PERFORM dblink_disconnect(dblink_name);
	RETURN NEW;
end $BODY$;
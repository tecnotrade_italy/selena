<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentHeadSixtensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_heads_sixten', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('document_head_id');
            $table->unsignedBigInteger('packaging_type_sixten_id')->nullable();
            $table->unsignedBigInteger('shipment_code_sixten_id')->nullable();
            $table->integer('weight')->nullable();
            $table->integer('packagingNumber')->nullable();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('document_head_id')->references('id')->on('documents_heads');
            $table->foreign('packaging_type_sixten_id')->references('id')->on('packaging_types_sixten');
            $table->foreign('shipment_code_sixten_id')->references('id')->on('shipments_codes_sixtens');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });
   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents_heads_sixten');
      
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternalRepairTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('internal_repair', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('date', 255)->nullable();
            $table->string('ddt_exit', 255)->nullable();
            $table->unsignedBigInteger('supplier')->nullable();
            $table->string('description', 255)->nullable();
            $table->string('code_gr', 255)->nullable();
            $table->timestamps();
            $table->foreign('supplier')->references('id')->on('business_name_supplier');
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
        });

    }
  

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('internal_repair');
    }
}

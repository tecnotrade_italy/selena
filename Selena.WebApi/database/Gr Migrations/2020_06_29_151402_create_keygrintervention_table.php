<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeygrinterventionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gr_interventions', function (Blueprint $table) {
            $table->unsignedBigInteger('customers_id')->nullable();
            $table->foreign('customers_id')->references('id')->on('users');
        });

      /*  Schema::connection(env('DB_CONNECTION_TEST', 'data_test'))->table('gr_interventions', function (Blueprint $table) {
            $table->unsignedBigInteger('customers_id')->nullable();
            $table->foreign('customers_id')->references('id')->on('users');
        });*/

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gr_interventions', function (Blueprint $table) {
            $table->dropColumn('customers_id');
        });

      /*  Schema::connection(env('DB_CONNECTION_TEST', 'data_test'))->table('gr_interventions', function (Blueprint $table) {
            $table->dropColumn('customers_id');
        });*/
    }
}


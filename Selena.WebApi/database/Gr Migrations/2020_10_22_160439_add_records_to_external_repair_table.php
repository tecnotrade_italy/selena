<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordsToExternalRepairTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('external_repair', function (Blueprint $table) {
            $table->string('date_return_product_from_the_supplier', 255)->nullable();
            $table->string('ddt_supplier', 255)->nullable();
            $table->string('reference_supplier', 255)->nullable();
            $table->string('notes_from_repairman', 255)->nullable();
        });
     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('external_repair', function (Blueprint $table) {
            //
        });
     
    }
}

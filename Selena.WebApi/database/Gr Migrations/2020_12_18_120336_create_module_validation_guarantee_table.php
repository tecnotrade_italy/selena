<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleValidationGuaranteeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_validation_guarantee', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_user');
            $table->unsignedBigInteger('referent');
            $table->unsignedBigInteger('id_intervention');
            $table->string('date_mount', 255);
            $table->string('trolley_type', 255);
            $table->string('plant_type', 255);
            $table->string('voltage', 255); 
            $table->boolean('traction');
            $table->boolean('lift');
            $table->string('insulation_value_tewards_machine_frame_positive', 255);
            $table->string('insulation_value_tewards_machine_frame_negative', 255);
            $table->string('single_motor_with_traction_motor_range', 255);
            $table->string('single_motor_with_brush_traction_motor', 255);
            $table->string('single_motor_u_v_w_traction_phase_motor', 255);
            $table->string('single_motor_u_v_w_motor_lifting_phase', 255);
            $table->string('note', 255);
            $table->string('bi_engine_with_traction_motor_range', 255);
            $table->string('bi_engine_with_brush_traction_motor', 255);
            $table->string('bi_engine_u_v_w_traction_phase_motor', 255);
            $table->string('bi_engine_u_v_w_motor_lifting_phase', 255);    

            $table->string('lift_with_field_brush_motor_raised', 255);
            $table->string('lift_with_u_v_w_motor_lift_pharse', 255);
            $table->string('battery_voltage', 255);
            $table->string('traction_limit_current_consumption', 255);
            $table->string('lift_limit_current_absorption', 255);
           

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('referent')->references('id')->on('people');
            $table->foreign('id_intervention')->references('id')->on('gr_interventions');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');




        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_validation_guarantee');
    }
}

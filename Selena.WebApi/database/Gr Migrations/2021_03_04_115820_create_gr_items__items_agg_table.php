<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrItemsItemsAggTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gr_items_ItemsAgg', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('business_name_supplier', 255)->nullable();    
            $table->string('code_supplier', 255)->nullable();    
            $table->string('price_purchase', 255)->nullable();    
            $table->string('update_date', 255)->nullable();    
            $table->string('usual_supplier', 255)->nullable();    
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->bigInteger('id_gritems')->nullable();
            $table->timestamps(); 
            $table->foreign('id_gritems')->references('id')->on('gr_items');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gr_items_ItemsAgg');
    }
}

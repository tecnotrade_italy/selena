<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrInterventionProcessing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gr_intervention_processing', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description', 255)->nullable();
            $table->string('qta', 255)->nullable();
            $table->boolean('n_u')->nullable();
            $table->unsignedBigInteger('id_intervention')->nullable();
            $table->unsignedBigInteger('id_gr_items')->nullable();
	        $table->string('price_list', 255)->nullable();
	        $table->string('tot', 255)->nullable();
            $table->timestamps();

            $table->foreign('id_intervention')->references('id')->on('gr_interventions');
            $table->foreign('id_gr_items')->references('id')->on('gr_items');
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gr_intervention_processing');
       
    }
}

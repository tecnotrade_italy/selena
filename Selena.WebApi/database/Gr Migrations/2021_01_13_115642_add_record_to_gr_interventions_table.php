<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordToGrInterventionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gr_interventions', function (Blueprint $table) {
            $table->string('note_internal_gr', 255)->nullable();
            $table->bigInteger('referent_customer_id')->nullable();
            $table->foreign('referent_customer_id')->references('id')->on('people');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gr_interventions', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrUserDifferentDestination extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gr_user_different_destination', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('business_name_plus', 255)->nullable();    
            $table->string('address', 255)->nullable();    
            $table->string('codepostal', 255)->nullable();  
            $table->string('province', 255)->nullable();      
            $table->string('country', 255)->nullable();  
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->timestamps();
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gr_user_different_destination');
    }
}

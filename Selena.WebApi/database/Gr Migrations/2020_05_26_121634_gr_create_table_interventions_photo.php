<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GrCreateTableInterventionsPhoto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gr_intervention_photo', function (Blueprint $table) {     
                $table->bigIncrements('id');
                $table->string('intervention_id', 255)->nullable();
                $table->string('img', 255)->nullable();
                $table->unsignedBigInteger('created_id');
                $table->unsignedBigInteger('updated_id')->nullable();
                $table->timestamps();
        });
        
       
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gr_intervention_photo');
      
    }
}

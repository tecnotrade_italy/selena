<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExternalRepairTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('external_repair', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_intervention')->nullable();
            $table->string('date', 255)->nullable();
            $table->string('ddt_exit', 255)->nullable();
            $table->string('nr', 255)->nullable();
            $table->unsignedBigInteger('id_business_name_supplier')->nullable();
            $table->timestamps();

            $table->foreign('id_intervention')->references('id')->on('gr_interventions');
            $table->foreign('id_business_name_supplier')->references('id')->on('business_name_supplier');
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('external_repair');
     
    }
}

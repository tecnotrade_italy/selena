<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordExternalReferentToGrInterventionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gr_interventions', function (Blueprint $table) {
            $table->bigInteger('external_referent_id')->nullable();
            $table->foreign('external_referent_id')->references('id')->on('people');    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gr_interventions', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountVisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_vision', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('date', 255)->nullable();
            $table->string('ddt_gr', 255)->nullable();
            $table->string('code_gr', 255)->nullable();
            $table->unsignedBigInteger('supplier')->nullable();
            $table->string('description', 255)->nullable();
            $table->string('note', 255)->nullable();
            $table->string('ddt_return', 255)->nullable();
            $table->string('note_return', 255)->nullable();
            $table->string('date_return', 255)->nullable();
            $table->string('soll', 255)->nullable();
            $table->timestamps();
            
            $table->foreign('supplier')->references('id')->on('business_name_supplier');
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();      
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_vision');
      
    }
}

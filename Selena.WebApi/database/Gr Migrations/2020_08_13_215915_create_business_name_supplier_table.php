<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessNameSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_name_supplier', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255)->nullable();
            $table->string('surname', 255)->nullable();
            $table->string('business_name', 255)->nullable();
            $table->string('address', 255)->nullable();
            $table->string('code_postal', 255)->nullable();
            $table->string('country', 255)->nullable();
            $table->string('province', 255)->nullable();
            $table->string('fiscal_code', 255)->nullable();
            $table->string('vat_number', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('telephone_number', 255)->nullable();
            $table->timestamps();
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_name_supplier');
    }
}

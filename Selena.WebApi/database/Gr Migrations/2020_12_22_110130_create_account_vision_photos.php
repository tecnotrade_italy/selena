<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountVisionPhotos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_vision_photos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('account_vision_id');
            $table->string('img', 255)->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->timestamps();
            $table->foreign('account_vision_id')->references('id')->on('account_vision');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_vision_photos');
    }
}

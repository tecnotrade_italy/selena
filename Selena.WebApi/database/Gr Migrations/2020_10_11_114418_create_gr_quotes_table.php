<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gr_quotes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('object', 255)->nullable();
            $table->string('note_before_the_quote', 255)->nullable();
            $table->string('note', 255)->nullable();
            $table->unsignedBigInteger('description_payment')->nullable();
            $table->unsignedBigInteger('id_states_quote')->nullable();
            $table->unsignedBigInteger('email')->nullable();
            $table->timestamps();

            $table->foreign('id_states_quote')->references('id')->on('gr_states_quotes');
            $table->foreign('description_payment')->references('id')->on('gr_payments');
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
        });

    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gr_quotes');
     
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gr_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tipology', 255)->nullable();   
            $table->string('note', 255)->nullable();
            $table->string('img_schemes', 255)->nullable();
            $table->string('original_code', 255)->nullable();
            $table->string('code_product', 255)->nullable();
            $table->string('code_gr', 255)->nullable();
            $table->string('plant', 255)->nullable();
            $table->string('brand', 255)->nullable();
            $table->string('business_name_supplier', 255)->nullable();
            $table->string('code_supplier', 255)->nullable();
            $table->decimal('price_purchase', 10, 4)->nullable();
            $table->decimal('price_list', 10, 4)->nullable();
            $table->decimal('repair_price', 10, 4)->nullable();
            $table->decimal('price_new', 10, 4)->nullable();
            $table->dateTime('update_date')->nullable();
            $table->string('usual_supplier', 255)->nullable();
            $table->string('pr_rip_conc', 255)->nullable();
            $table->unsignedBigInteger('item_id')->nullable();
            $table->timestamps();
          
            $table->foreign('item_id')->references('id')->on('items');
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();

        });

           
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gr_items');
       
    }
}

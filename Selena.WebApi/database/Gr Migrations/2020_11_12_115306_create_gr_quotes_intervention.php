<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrQuotesIntervention extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gr_quotes_intervention', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_intervention')->nullable();
            $table->unsignedBigInteger('id_quotes')->nullable();
            $table->timestamps();

            $table->foreign('id_quotes')->references('id')->on('gr_quotes');
            $table->foreign('id_intervention')->references('id')->on('gr_interventions');
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gr_quotes_intervention');
     
    }
}

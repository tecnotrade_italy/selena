<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGrInterventions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gr_interventions', function (Blueprint $table) { 
            $table->bigIncrements('id');
            $table->string('code_product', 255)->nullable();
            $table->string('nr_ddt', 255)->nullable();
            $table->dateTime('date_ddt')->nullable();
            $table->string('img', 255)->nullable();
            $table->string('description', 255)->nullable();
            $table->boolean('unrepairable')->nullable();
            $table->boolean('send_to_customer')->nullable();
            $table->dateTime('create_quote')->nullable();
            $table->string('defect', 255)->nullable();
            $table->string('nr_ddt_gr', 255)->nullable();
            $table->dateTime('date_ddt_gr')->nullable();
            $table->string('exit_notes',255 )->nullable();
            $table->string('phone_calls', 255)->nullable();
            $table->unsignedBigInteger('states_id')->nullable();
            $table->unsignedBigInteger('items_id')->nullable();
            $table->unsignedBigInteger('customers_id')->nullable();
            $table->unsignedBigInteger('language_id')->nullable();
            //$table->foreign('states_id')->references('id')->on('gr_states');
            //$table->foreign('items_id')->references('id')->on('items');
            //$table->foreign('customers_id')->references('id')->on('customers');
            //$table->foreign('language_id')->references('id')->on('languages');
            $table->string('referent', 255)->nullable();
            $table->string('plant_type', 255)->nullable();
            $table->string('trolley_type', 255)->nullable();
            $table->string('series', 255)->nullable();
            $table->string('voltage', 255)->nullable();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

       /* Schema::connection(env('DB_CONNECTION_TEST', 'data_test'))->create('gr_interventions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code_product', 255)->nullable();
            $table->string('nr_ddt', 255)->nullable();
            $table->dateTime('date_ddt')->nullable();
            $table->string('img', 255)->nullable();
            $table->string('description', 255)->nullable();
            $table->boolean('unrepairable')->nullable();
            $table->boolean('send_to_customer')->nullable();
            $table->dateTime('create_quote')->nullable();
            $table->string('defect', 255)->nullable();
            $table->string('nr_ddt_gr', 255)->nullable();
            $table->dateTime('date_ddt_gr')->nullable();
            $table->string('exit_notes',255 )->nullable();
            $table->string('phone_calls', 255)->nullable();
            $table->unsignedBigInteger('states_id')->nullable();
            $table->unsignedBigInteger('customers_id')->nullable();
            $table->unsignedBigInteger('language_id')->nullable();*/
           // $table->foreign('states_id')->references('id')->on('gr_states');
            //$table->foreign('items_id')->references('id')->on('items');
            //$table->foreign('customers_id')->references('id')->on('customers');
            //$table->foreign('language_id')->references('id')->on('languages');
          /*  $table->string('referent', 255)->nullable();
            $table->string('plant_type', 255)->nullable();
            $table->string('trolley_type', 255)->nullable();
            $table->string('series', 255)->nullable();
            $table->string('voltage', 255)->nullable();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });*/

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gr_interventions');
        //Schema::connection(env('DB_CONNECTION_TEST', 'data_test'))->dropIfExists('gr_interventions');
       
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordToGrQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gr_quotes', function (Blueprint $table) {
          $table->string('kind_attention', 255)->nullable();    
           $table->boolean('checksendmail')->nullable();    

            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gr_quotes', function (Blueprint $table) {
            //
        });
    }
}

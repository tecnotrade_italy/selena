<?php

use App\Language;
use App\TemplateEditor;
use App\TemplateEditorGroup;
use App\TemplateEditorType;
use App\User;
use Illuminate\Database\Seeder;

class TemplateEditorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idUser = User::first()->id;

        #region TEMPLATE EDITOR TYPE

        $templateEditoTypeHeader = new TemplateEditorType();
        $templateEditoTypeHeader->code = 'Header';
        $templateEditoTypeHeader->description = 'Header';
        $templateEditoTypeHeader->created_id = $idUser;
        $templateEditoTypeHeader->save();

        $templateEditoTypeFooter = new TemplateEditorType();
        $templateEditoTypeFooter->code = 'Footer';
        $templateEditoTypeFooter->description = 'Footer';
        $templateEditoTypeFooter->created_id = $idUser;
        $templateEditoTypeFooter->save();

        $templateEditoTypePages = new TemplateEditorType();
        $templateEditoTypePages->code = 'Pages';
        $templateEditoTypePages->description = 'Pagine';
        $templateEditoTypePages->created_id = $idUser;
        $templateEditoTypePages->save();

        $templateEditoTypeEmail = new TemplateEditorType();
        $templateEditoTypeEmail->code = 'Email';
        $templateEditoTypeEmail->description = 'Email';
        $templateEditoTypeEmail->created_id = $idUser;
        $templateEditoTypeEmail->save();

        $templateEditoTypeBlog = new TemplateEditorType();
        $templateEditoTypeBlog->code = 'Blog';
        $templateEditoTypeBlog->description = 'Blog';
        $templateEditoTypeBlog->created_id = $idUser;
        $templateEditoTypeBlog->save();

        #endregion TEMPLATE EDITOR TYPE

        #region TEMPLATE EDITOR GROUP

        $templateEditorGroupBase = new TemplateEditorGroup();
        $templateEditorGroupBase->code = 'Base';
        $templateEditorGroupBase->description = 'Base';
        $templateEditorGroupBase->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupBase->order = 10;
        $templateEditorGroupBase->created_id = $idUser;
        $templateEditorGroupBase->save();

        $templateEditorGroupHeader = new TemplateEditorGroup();
        $templateEditorGroupHeader->code = 'Header';
        $templateEditorGroupHeader->description = 'Header';
        $templateEditorGroupHeader->template_editor_type_id = $templateEditoTypeHeader->id;
        $templateEditorGroupHeader->order = 10;
        $templateEditorGroupHeader->created_id = $idUser;
        $templateEditorGroupHeader->save();

        $templateEditorGroupImmagini = new TemplateEditorGroup();
        $templateEditorGroupImmagini->code = 'Immagini';
        $templateEditorGroupImmagini->description = 'Immagini';
        $templateEditorGroupImmagini->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupImmagini->order = 20;
        $templateEditorGroupImmagini->created_id = $idUser;
        $templateEditorGroupImmagini->save();

        $templateEditorGroupNewsletter = new TemplateEditorGroup();
        $templateEditorGroupNewsletter->code = 'Newsletter';
        $templateEditorGroupNewsletter->description = 'Newsletter';
        $templateEditorGroupNewsletter->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupNewsletter->order = 30;
        $templateEditorGroupNewsletter->created_id = $idUser;
        $templateEditorGroupNewsletter->save();

        $templateEditorGroupPulsante = new TemplateEditorGroup();
        $templateEditorGroupPulsante->code = 'Pulsante';
        $templateEditorGroupPulsante->description = 'Call to action';
        $templateEditorGroupPulsante->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupPulsante->order = 40;
        $templateEditorGroupPulsante->created_id = $idUser;
        $templateEditorGroupPulsante->save();

        $templateEditorGroupVideo = new TemplateEditorGroup();
        $templateEditorGroupVideo->code = 'Video';
        $templateEditorGroupVideo->description = 'Video';
        $templateEditorGroupVideo->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupVideo->order = 50;
        $templateEditorGroupVideo->created_id = $idUser;
        $templateEditorGroupVideo->save();

        $templateEditorGroupTeams = new TemplateEditorGroup();
        $templateEditorGroupTeams->code = 'Teams';
        $templateEditorGroupTeams->description = 'Teams';
        $templateEditorGroupTeams->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupTeams->order = 60;
        $templateEditorGroupTeams->created_id = $idUser;
        $templateEditorGroupTeams->save();

        $templateEditorGroupContatti = new TemplateEditorGroup();
        $templateEditorGroupContatti->code = 'Contatti';
        $templateEditorGroupContatti->description = 'Contatti';
        $templateEditorGroupContatti->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupContatti->order = 70;
        $templateEditorGroupContatti->created_id = $idUser;
        $templateEditorGroupContatti->save();

        $templateEditorGroupMappa = new TemplateEditorGroup();
        $templateEditorGroupMappa->code = 'Mappa';
        $templateEditorGroupMappa->description = 'Mappe';
        $templateEditorGroupMappa->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupMappa->order = 80;
        $templateEditorGroupMappa->created_id = $idUser;
        $templateEditorGroupMappa->save();

        $templateEditorGroupTestimonial = new TemplateEditorGroup();
        $templateEditorGroupTestimonial->code = 'Testimonial';
        $templateEditorGroupTestimonial->description = 'Testimonial';
        $templateEditorGroupTestimonial->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupTestimonial->order = 90;
        $templateEditorGroupTestimonial->created_id = $idUser;
        $templateEditorGroupTestimonial->save();

        $templateEditorGroupProcesso = new TemplateEditorGroup();
        $templateEditorGroupProcesso->code = 'Processo';
        $templateEditorGroupProcesso->description = 'Processi';
        $templateEditorGroupProcesso->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupProcesso->order = 110;
        $templateEditorGroupProcesso->created_id = $idUser;
        $templateEditorGroupProcesso->save();

        $templateEditorGroupPrezzo = new TemplateEditorGroup();
        $templateEditorGroupPrezzo->code = 'Prezzo';
        $templateEditorGroupPrezzo->description = 'Prezzi';
        $templateEditorGroupPrezzo->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupPrezzo->order = 120;
        $templateEditorGroupPrezzo->created_id = $idUser;
        $templateEditorGroupPrezzo->save();

        #endregion TEMPLATE EDITOR GROUP

        #region TEMPLATE EDITOR

        $templateEditorModulo_1 = new TemplateEditor();
        $templateEditorModulo_1->description = 'Modulo 1';
        $templateEditorModulo_1->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-01.png';
        $templateEditorModulo_1->html = '<section style="padding:3rem 0;"><div class="container"><div class="row justify-content-center"><div class="col col-md-8 text-center"><h1>Una azienda incredibile</h1></div></div></div></section>';
        $templateEditorModulo_1->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorModulo_1->order = 10;
        $templateEditorModulo_1->created_id = $idUser;
        $templateEditorModulo_1->save();
        
$templateEditorModulo_2_100 = new TemplateEditor();
        $templateEditorModulo_2_100->description = 'Modulo 2 (100%)';
        $templateEditorModulo_2_100->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-02-1.png';
        $templateEditorModulo_2_100->html = '<section style="padding:3rem 0;"><div class="container"><div class="row justify-content-center"><div class="col col-md-12 text-center"><p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean1</p></div></div></div></section>';
        $templateEditorModulo_2_100->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorModulo_2_100->order = 20;
        $templateEditorModulo_2_100->created_id = $idUser;
        $templateEditorModulo_2_100->save();
        
$templateEditorModulo_2_70 = new TemplateEditor();
        $templateEditorModulo_2_70->description = 'Modulo 2 (70%)';
        $templateEditorModulo_2_70->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-02-2.png';
        $templateEditorModulo_2_70->html = '<section style="padding:3rem 0;"><div class="container"><div class="row justify-content-center"><div class="col col-md-8 text-center"><p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p></div></div></div></section>';
        $templateEditorModulo_2_70->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorModulo_2_70->order = 21;
        $templateEditorModulo_2_70->created_id = $idUser;
        $templateEditorModulo_2_70->save();
        
$templateEditorModulo_3 = new TemplateEditor();
        $templateEditorModulo_3->description = 'Modulo 3';
        $templateEditorModulo_3->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-03.png';
        $templateEditorModulo_3->html = '<section style="padding:3rem 0;"><div class="container"><div class="row justify-content-center"><div class="col col-md-8 text-center"><h1>Una azienda incredibile</h1><p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far <a href="#">World of Grammar</a>.</p></div></div></div></section>';
        $templateEditorModulo_3->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorModulo_3->order = 30;
        $templateEditorModulo_3->created_id = $idUser;
        $templateEditorModulo_3->save();
        
$templateEditorModulo_4 = new TemplateEditor();
        $templateEditorModulo_4->description = 'Modulo 4';
        $templateEditorModulo_4->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-04.png';
        $templateEditorModulo_4->html = '<section style="padding:3rem 0;"><div class="container"><div class="row"><div class="col col-sm-10 col-md-8 text-left"><p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p></div></div></div></section>';
        $templateEditorModulo_4->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorModulo_4->order = 40;
        $templateEditorModulo_4->created_id = $idUser;
        $templateEditorModulo_4->save();
        
$templateEditorModulo_5 = new TemplateEditor();
        $templateEditorModulo_5->description = 'Modulo 5';
        $templateEditorModulo_5->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-05.png';
        $templateEditorModulo_5->html = '<section style="padding:3rem 0;"><div class="container"><div class="row justify-content-end"><div class="col col-sm-10 col-md-8 text-left"><p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far <a href="#">World of Grammar</a>.</p></div></div></div></section>';
        $templateEditorModulo_5->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorModulo_5->order = 50;
        $templateEditorModulo_5->created_id = $idUser;
        $templateEditorModulo_5->save();
        
$templateEditorModulo_6 = new TemplateEditor();
        $templateEditorModulo_6->description = 'Modulo 6';
        $templateEditorModulo_6->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-06.png';
        $templateEditorModulo_6->html = '<section style="padding:3rem 0;"><div class="container"><div class="row"><div class="col text-left"><h2>Far far away...</h2><p>Far far away, behind the word mountains, far from the countries <a href="#">Vokalia</a> and <a href="#">Consonantia</a>, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p><p>On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word &quot;and&quot; and the <a href="#">Little Blind Text</a> should turn around and return to its own, safe country. But nothing the copy said could convince her and so it didn&rsquo;t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their.</p></div></div></div></section>';
        $templateEditorModulo_6->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorModulo_6->order = 60;
        $templateEditorModulo_6->created_id = $idUser;
        $templateEditorModulo_6->save();
        
$templateEditorModulo_7 = new TemplateEditor();
        $templateEditorModulo_7->description = 'Modulo 7';
        $templateEditorModulo_7->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-07.png';
        $templateEditorModulo_7->html = '<section style="padding:3rem 0;"><div class="container"><div class="row"><div class="col text-center"><h1>Una azienda incredibile</h1><div class="row text-left pt-4"><div class="col-12 col-md-6"><p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place far far away.</p></div><div class="col-12 col-md-6"><p>Separated they live in Bookmarksgrove right at the coast of the Semantics, far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast.</p></div></div></div></div></div></section>';
        $templateEditorModulo_7->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorModulo_7->order = 70;
        $templateEditorModulo_7->created_id = $idUser;
        $templateEditorModulo_7->save();
        
$templateEditorModulo_8_fit = new TemplateEditor();
        $templateEditorModulo_8_fit->description = 'Modulo 8 (fit)';
        $templateEditorModulo_8_fit->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-08.png';
        $templateEditorModulo_8_fit->html = '<section style="padding:3rem 0;"><div class="container"><div class="row align-items-center"><div class="col-12 col-md-6 col-lg-5"><h1>Titolo</h1><p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p></div><div class="col-12 col-md-6 ml-md-auto mt-4 mt-md-0"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/shapes/2.svg"></div></div></div></section>';
        $templateEditorModulo_8_fit->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorModulo_8_fit->order = 80;
        $templateEditorModulo_8_fit->created_id = $idUser;
        $templateEditorModulo_8_fit->save();
        
$templateEditorModulo_9_fit = new TemplateEditor();
        $templateEditorModulo_9_fit->description = 'Modulo 9 (fit)';
        $templateEditorModulo_9_fit->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-09.png';
        $templateEditorModulo_9_fit->html = '<section style="padding:3rem 0;"><div class="container"><div class="row align-items-center"><div class="col-12 col-md-6 mb-4 mb-md-0"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/shapes/2.svg"></div><div class="col-12 col-md-6 col-lg-5 ml-md-auto text-left"><h1>Titolo</h1><p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p></div></div></div></section>';
        $templateEditorModulo_9_fit->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorModulo_9_fit->order = 90;
        $templateEditorModulo_9_fit->created_id = $idUser;
        $templateEditorModulo_9_fit->save();
        
$templateEditorModulo_10_100 = new TemplateEditor();
        $templateEditorModulo_10_100->description = 'Modulo 10 (100%)';
        $templateEditorModulo_10_100->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-10.png';
        $templateEditorModulo_10_100->html = '<section><div class="container-fluid"><div class="row align-items-center"><div class="col-12 col-md-6 mb-4 mb-md-0"><img alt="image" class="img-fluid fr-fic fr-dii" src="https://selena.tecnotrade.com/api/storage/images/rose.jpg"></div><div class="col-11 col-md-6 col-lg-5 ml-md-auto offset-sm-1 text-left"><h1>Titolo</h1><p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p></div></div></div></section>';
        $templateEditorModulo_10_100->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorModulo_10_100->order = 100;
        $templateEditorModulo_10_100->created_id = $idUser;
        $templateEditorModulo_10_100->save();
        
$templateEditorModulo_11_100 = new TemplateEditor();
        $templateEditorModulo_11_100->description = 'Modulo 11 (100%)';
        $templateEditorModulo_11_100->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-11.png';
        $templateEditorModulo_11_100->html = '<section><div class="container-fluid"><div class="row align-items-center"><div class="col-11 col-md-6 col-lg-6 ml-md-auto offset-sm-1 text-left"><h1>Titolo</h1><p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p></div><div class="col-12 col-md-6 mb-4 mb-md-0"><img alt="image" class="img-fluid fr-fic fr-dii" src="https://selena.tecnotrade.com/api/storage/images/rose.jpg"></div></div></div></section>';
        $templateEditorModulo_11_100->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorModulo_11_100->order = 110;
        $templateEditorModulo_11_100->created_id = $idUser;
        $templateEditorModulo_11_100->save();
        
$templateEditorSpaziatore_60_px = new TemplateEditor();
        $templateEditorSpaziatore_60_px->description = 'Spaziatore (60 px)';
        $templateEditorSpaziatore_60_px->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-12.png';
        $templateEditorSpaziatore_60_px->html = '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12"><div style="height:60px;"><br></div></div></div></div></div><div><br></div>';
        $templateEditorSpaziatore_60_px->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorSpaziatore_60_px->order = 120;
        $templateEditorSpaziatore_60_px->created_id = $idUser;
        $templateEditorSpaziatore_60_px->save();
        
$templateEditorSpaziatore_120_px = new TemplateEditor();
        $templateEditorSpaziatore_120_px->description = 'Spaziatore (120 px)';
        $templateEditorSpaziatore_120_px->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-121.png';
        $templateEditorSpaziatore_120_px->html = '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12"><div style="height:120px;"><br></div></div></div></div></div><div><br></div>';
        $templateEditorSpaziatore_120_px->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorSpaziatore_120_px->order = 121;
        $templateEditorSpaziatore_120_px->created_id = $idUser;
        $templateEditorSpaziatore_120_px->save();
        
$templateEditorSpaziatore_240_px = new TemplateEditor();
        $templateEditorSpaziatore_240_px->description = 'Spaziatore (240 px)';
        $templateEditorSpaziatore_240_px->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-122.png';
        $templateEditorSpaziatore_240_px->html = '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12"><div style="height:240px;"><br></div></div></div></div></div><div><br></div>';
        $templateEditorSpaziatore_240_px->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorSpaziatore_240_px->order = 122;
        $templateEditorSpaziatore_240_px->created_id = $idUser;
        $templateEditorSpaziatore_240_px->save();
        
$templateEditorLinea_orizzontale_fit = new TemplateEditor();
        $templateEditorLinea_orizzontale_fit->description = 'Linea orizzontale (fit)';
        $templateEditorLinea_orizzontale_fit->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-13.png';
        $templateEditorLinea_orizzontale_fit->html = '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12"><hr style="background: none; background-color: transparent; border: none; border-top: rgba(0, 0, 0, 0.18) 1px solid; margin: 30px 0 25px; padding: 5px;"><br></div></div></div></div><div><br></div>';
        $templateEditorLinea_orizzontale_fit->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorLinea_orizzontale_fit->order = 130;
        $templateEditorLinea_orizzontale_fit->created_id = $idUser;
        $templateEditorLinea_orizzontale_fit->save();
        
$templateEditorIcone_social = new TemplateEditor();
        $templateEditorIcone_social->description = 'Icone social';
        $templateEditorIcone_social->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-15.png';
        $templateEditorIcone_social->html = '<section><div class="container"><div class="row"><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-facebook fa-stack" style="color:#222;font-size:40px;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-youtube-play fa-stack" style="color:#222;font-size:40px;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-instagram fa-stack" style="color:#222;font-size:40px;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-linkedin fa-stack" style="color:#222;font-size:40px;"></i></a></div></div></div></section>';
        $templateEditorIcone_social->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorIcone_social->order = 150;
        $templateEditorIcone_social->created_id = $idUser;
        $templateEditorIcone_social->save();
        
$templateEditorIcone_social_colorate = new TemplateEditor();
        $templateEditorIcone_social_colorate->description = 'Icone social colorate';
        $templateEditorIcone_social_colorate->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-16.png';
        $templateEditorIcone_social_colorate->html = '<section><div class="container"><div class="row"><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-facebook fa-stack" style="color:#fff;font-size:40px;background:#1877f2;border-radius:50%;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-youtube-play fa-stack" style="color:#fff;font-size:40px;background:#ff0000;border-radius:50%;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-instagram fa-stack" style="color:#fff;font-size:40px;background:#c32aa3;border-radius:50%;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-linkedin fa-stack" style="color:#fff;font-size:40px;background:#007bb5;border-radius:50%;"></i></a></div></div></div></section>';
        $templateEditorIcone_social_colorate->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorIcone_social_colorate->order = 160;
        $templateEditorIcone_social_colorate->created_id = $idUser;
        $templateEditorIcone_social_colorate->save();
        
$templateEditorImmagine_fit = new TemplateEditor();
        $templateEditorImmagine_fit->description = 'Immagine (fit)';
        $templateEditorImmagine_fit->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/immagine-01.png';
        $templateEditorImmagine_fit->html = '<div class="section-pages"><div class="container"><img src="/api/storage/images/sea.jpg" style="width: 100%;" class="fr-fic fr-dii"></div></div><div><br></div>';
        $templateEditorImmagine_fit->template_editor_group_id = $templateEditorGroupImmagini->id;
        $templateEditorImmagine_fit->order = 10;
        $templateEditorImmagine_fit->created_id = $idUser;
        $templateEditorImmagine_fit->save();
        
$templateEditorImmagine_100 = new TemplateEditor();
        $templateEditorImmagine_100->description = 'Immagine (100%)';
        $templateEditorImmagine_100->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/immagine-02.png';
        $templateEditorImmagine_100->html = '<div class="section-pages"><div class="container-fluid"><img src="/api/storage/images/sea.jpg" style="width: 100%;" class="fr-fic fr-dii"></div></div><div><br></div>';
        $templateEditorImmagine_100->template_editor_group_id = $templateEditorGroupImmagini->id;
        $templateEditorImmagine_100->order = 20;
        $templateEditorImmagine_100->created_id = $idUser;
        $templateEditorImmagine_100->save();
        
$templateEditor2_immagini_fit = new TemplateEditor();
        $templateEditor2_immagini_fit->description = '2 immagini (fit)';
        $templateEditor2_immagini_fit->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/immagine-03.png';
        $templateEditor2_immagini_fit->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 col-md-6"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-6"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></div>';
        $templateEditor2_immagini_fit->template_editor_group_id = $templateEditorGroupImmagini->id;
        $templateEditor2_immagini_fit->order = 30;
        $templateEditor2_immagini_fit->created_id = $idUser;
        $templateEditor2_immagini_fit->save();
        
$templateEditor2_immagini_senza_margini_100 = new TemplateEditor();
        $templateEditor2_immagini_senza_margini_100->description = '2 immagini senza margini (100%)';
        $templateEditor2_immagini_senza_margini_100->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/immagine-32.png';
        $templateEditor2_immagini_senza_margini_100->html = '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12 col-md-6"><img src="/api/storage/images/rose.jpg" class="fr-fic fr-dii"></div><div class="col-12 col-md-6"><img src="/api/storage/images/grass.jpg" class="fr-fic fr-dii"></div></div></div></div><div><br></div>';
        $templateEditor2_immagini_senza_margini_100->template_editor_group_id = $templateEditorGroupImmagini->id;
        $templateEditor2_immagini_senza_margini_100->order = 32;
        $templateEditor2_immagini_senza_margini_100->created_id = $idUser;
        $templateEditor2_immagini_senza_margini_100->save();
        
$templateEditor3_immagini_fit = new TemplateEditor();
        $templateEditor3_immagini_fit->description = '3 immagini (fit)';
        $templateEditor3_immagini_fit->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/immagine-04.png';
        $templateEditor3_immagini_fit->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></div>';
        $templateEditor3_immagini_fit->template_editor_group_id = $templateEditorGroupImmagini->id;
        $templateEditor3_immagini_fit->order = 40;
        $templateEditor3_immagini_fit->created_id = $idUser;
        $templateEditor3_immagini_fit->save();
        
$templateEditor3_immagini_senza_margini_100 = new TemplateEditor();
        $templateEditor3_immagini_senza_margini_100->description = '3 immagini senza margini (100%)';
        $templateEditor3_immagini_senza_margini_100->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/immagine-44.png';
        $templateEditor3_immagini_senza_margini_100->html = '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12 col-md-4"><img src="/api/storage/images/rose.jpg" class="fr-fic fr-dii"></div><div class="col-12 col-md-4"><img src="/api/storage/images/grass.jpg" class="fr-fic fr-dii"></div><div class="col-12 col-md-4"><img src="https://selena.tecnotrade.com/api/storage/images/flower.jpg" class="fr-fic fr-dii"></div></div></div></div><div><br></div>';
        $templateEditor3_immagini_senza_margini_100->template_editor_group_id = $templateEditorGroupImmagini->id;
        $templateEditor3_immagini_senza_margini_100->order = 44;
        $templateEditor3_immagini_senza_margini_100->created_id = $idUser;
        $templateEditor3_immagini_senza_margini_100->save();
        
$templateEditor4_immagini_fit = new TemplateEditor();
        $templateEditor4_immagini_fit->description = '4 immagini (fit)';
        $templateEditor4_immagini_fit->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/immagine-05.png';
        $templateEditor4_immagini_fit->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 col-md-6 col-lg-3"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-6 col-lg-3"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-6 col-lg-3"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-6 col-lg-3"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></div><div><br></div>';
        $templateEditor4_immagini_fit->template_editor_group_id = $templateEditorGroupImmagini->id;
        $templateEditor4_immagini_fit->order = 50;
        $templateEditor4_immagini_fit->created_id = $idUser;
        $templateEditor4_immagini_fit->save();
        
$templateEditor4_immagini_senza_margini_100 = new TemplateEditor();
        $templateEditor4_immagini_senza_margini_100->description = '4 immagini senza margini (100%)';
        $templateEditor4_immagini_senza_margini_100->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/immagine-16.png';
        $templateEditor4_immagini_senza_margini_100->html = '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12 col-md-6 col-lg-3"><img src="/api/storage/images/flowers.jpg" class="fr-fic fr-dii"></div><div class="col-12 col-md-6 col-lg-3"><img src="/api/storage/images/rocks.jpg" class="fr-fic fr-dii"></div><div class="col-12 col-md-6 col-lg-3"><img src="/api/storage/images/skate.jpg" class="fr-fic fr-dii"></div><div class="col-12 col-md-6 col-lg-3"><img src="/api/storage/images/reading.jpg" class="fr-fic fr-dii"></div></div></div><div><br></div></div><div><br></div>';
        $templateEditor4_immagini_senza_margini_100->template_editor_group_id = $templateEditorGroupImmagini->id;
        $templateEditor4_immagini_senza_margini_100->order = 55;
        $templateEditor4_immagini_senza_margini_100->created_id = $idUser;
        $templateEditor4_immagini_senza_margini_100->save();
        
$templateEditor2_immagini_con_titolo_e_testo = new TemplateEditor();
        $templateEditor2_immagini_con_titolo_e_testo->description = '2 immagini con titolo e testo';
        $templateEditor2_immagini_con_titolo_e_testo->img = '/api/storage/images/TemplateEditor/Previews/portfolio-2-blocchi.png';
        $templateEditor2_immagini_con_titolo_e_testo->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 text-center"><h1 style="color:#222;font-size:25px;">LOREM IPSUM</h1></div></div><div class="row"><div class="col-12 text-center"><hr></div></div><div class="row"><div class="col-12 col-md-6"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-6"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div></div></div></div>';
        $templateEditor2_immagini_con_titolo_e_testo->template_editor_group_id = $templateEditorGroupImmagini->id;
        $templateEditor2_immagini_con_titolo_e_testo->order = 60;
        $templateEditor2_immagini_con_titolo_e_testo->created_id = $idUser;
        $templateEditor2_immagini_con_titolo_e_testo->save();
        
$templateEditor3_immagini_con_titolo_e_testo = new TemplateEditor();
        $templateEditor3_immagini_con_titolo_e_testo->description = '3 immagini con titolo e testo';
        $templateEditor3_immagini_con_titolo_e_testo->img = '/api/storage/images/TemplateEditor/Previews/portfolio-3-blocchi.png';
        $templateEditor3_immagini_con_titolo_e_testo->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 text-center"><h1 style="color:#222;font-size:25px;">LOREM IPSUM</h1></div></div><div class="row"><div class="col-12 text-center"><hr></div></div><div class="row"><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div></div></div></div>';
        $templateEditor3_immagini_con_titolo_e_testo->template_editor_group_id = $templateEditorGroupImmagini->id;
        $templateEditor3_immagini_con_titolo_e_testo->order = 70;
        $templateEditor3_immagini_con_titolo_e_testo->created_id = $idUser;
        $templateEditor3_immagini_con_titolo_e_testo->save();
        
$templateEditor4_blocchi_con_titolo_e_testo = new TemplateEditor();
        $templateEditor4_blocchi_con_titolo_e_testo->description = '4 blocchi con titolo e testo';
        $templateEditor4_blocchi_con_titolo_e_testo->img = '/api/storage/images/TemplateEditor/Previews/portfolio-4-blocchi.png';
        $templateEditor4_blocchi_con_titolo_e_testo->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 text-center"><h1 style="color:#222;font-size:25px;">LOREM IPSUM</h1></div></div><div class="row"><div class="col-12 text-center"><hr></div></div><div class="row"><div class="col-12 col-md-3"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-3"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-3"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-3"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div></div></div></div>';
        $templateEditor4_blocchi_con_titolo_e_testo->template_editor_group_id = $templateEditorGroupImmagini->id;
        $templateEditor4_blocchi_con_titolo_e_testo->order = 80;
        $templateEditor4_blocchi_con_titolo_e_testo->created_id = $idUser;
        $templateEditor4_blocchi_con_titolo_e_testo->save();
        
$templateEditorModulo_1 = new TemplateEditor();
        $templateEditorModulo_1->description = 'Modulo 1';
        $templateEditorModulo_1->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/newsletter-01.png';
        $templateEditorModulo_1->html = '<section class="fdb-block"><div class="container" style="padding: 7.5rem 0;background-position: right;background-repeat: no-repeat; background-size: contain;"><div class="row justify-content-center"><div class="col-12 col-md-8 col-lg-6 text-center"><h1>Iscriviti</h1><div class="input-group mt-4 mb-4"><input type="text" class="form-control" placeholder="Inserisci il tuo indirizzo email"><div class="input-group-append"><button class="btn btn-primary" type="button">Invia</button></div></div><p class="h4">Seguici su <a href="#">Facebook</a> e su <a href="#">Instagram</a>.</p></div></div></div></section>';
        $templateEditorModulo_1->template_editor_group_id = $templateEditorGroupNewsletter->id;
        $templateEditorModulo_1->order = 10;
        $templateEditorModulo_1->created_id = $idUser;
        $templateEditorModulo_1->save();
        
$templateEditorModulo_2 = new TemplateEditor();
        $templateEditorModulo_2->description = 'Modulo 2';
        $templateEditorModulo_2->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/newsletter-02.png';
        $templateEditorModulo_2->html = '<section class="fdb-block" style="background-image: url(/api/storage/images/hero/blue.svg);padding: 7.5rem 0;background-position: right;background-repeat: no-repeat; background-size: cover;"><div class="container"><div class="fdb-box"><div class="row justify-content-center align-items-center" style="background: #FFFFFF; padding: 3.75rem 2.5rem; overflow: hidden; color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><div class="col-12 col-lg-6"><h2>Unisciti a noi!</h2><p class="lead">Iscriviti alla nostra newsletter e rimani aggiornato su tutte le nostre novit&agrave;, offerte e promozioni!</p></div><div class="col-12 col-lg-5 text-center"><div class="input-group mt-4"><input type="text" class="form-control" placeholder="Inserisci il tuo indirizzo email"><div class="input-group-append"><button class="btn btn-primary" style="margin-left: -1px; padding: 0.475rem 1.625rem 0.575rem; margin-top: 0px;" type="button">Invia</button></div></div></div></div></div></div></section>';
        $templateEditorModulo_2->template_editor_group_id = $templateEditorGroupNewsletter->id;
        $templateEditorModulo_2->order = 20;
        $templateEditorModulo_2->created_id = $idUser;
        $templateEditorModulo_2->save();
        
$templateEditorModulo_3 = new TemplateEditor();
        $templateEditorModulo_3->description = 'Modulo 3';
        $templateEditorModulo_3->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/newsletter-03.png';
        $templateEditorModulo_3->html = '<section style="background-color:#F4F7FE;padding: 7.5rem 0;"><div class="container"><div class="row justify-content-center"><div class="col-12  col-md-10 col-lg-8 col-xl-6 text-center"><img alt="image" height="40" src="/api/storage/images/icons/layers.svg" style="margin-bottom:20px;" class="fr-fic fr-dii"><h1>Iscriviti alla newsletter!</h1><p class="lead">Ricevi le nostre comunicazioni periodiche e rimani aggiornato su tutte le nostre novit&agrave;, offerte e promozioni!</p><div class="input-group mt-4 mb-4"><input type="text" class="form-control" placeholder="Inserisci il tuo indirizzo email"><div class="input-group-append"><button class="btn btn-primary" style="border: 0.125rem solid transparent; padding: 0.45rem 2.625rem; margin-left: -1px; margin-top: 0px;" type="button">Invia</button></div></div><p class="h5"><em>*Il tuo indirizzo &egrave; al sicuro, non lo condivideremo con nessuno.</em></p></div></div></div></section>';
        $templateEditorModulo_3->template_editor_group_id = $templateEditorGroupNewsletter->id;
        $templateEditorModulo_3->order = 30;
        $templateEditorModulo_3->created_id = $idUser;
        $templateEditorModulo_3->save();
        
$templateEditorModulo_4 = new TemplateEditor();
        $templateEditorModulo_4->description = 'Modulo 4';
        $templateEditorModulo_4->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/newsletter-04.png';
        $templateEditorModulo_4->html = '<section style="padding: 7.5rem 0;"><div class="container"><div class="row"><div class="col-12 col-md-6 m-md-auto ml-lg-0 col-lg-5"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/draws/group-chat.svg"></div><div class="col-12 col-md-10 col-lg-6 mt-4 mt-lg-0 ml-auto mr-auto ml-lg-auto text-left"><div class="row"><div class="col"><h1>Iscriviti</h1><p class="lead">Ricevi le nostre comunicazioni periodiche e rimani aggiornato su tutte le nostre novit&agrave;, offerte e promozioni!</p></div></div><div class="row mt-4"><div class="col"><div class="input-group"><input type="text" class="form-control" placeholder="Inserisci il tuo indirizzo email"><div class="input-group-append"><button class="btn btn-primary" style="padding: .475rem 3rem .575rem; margin-left: -1px;margin-top: 1px;" type="button">Invia</button></div></div></div></div><div class="row"><div class="col"><p><em>*Il tuo indirizzo &egrave; al sicuro, non lo condivideremo con nessuno.</em></p></div></div></div></div></div></section>';
        $templateEditorModulo_4->template_editor_group_id = $templateEditorGroupNewsletter->id;
        $templateEditorModulo_4->order = 40;
        $templateEditorModulo_4->created_id = $idUser;
        $templateEditorModulo_4->save();
        
$templateEditorModulo_5 = new TemplateEditor();
        $templateEditorModulo_5->description = 'Modulo 5';
        $templateEditorModulo_5->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/newsletter-05.png';
        $templateEditorModulo_5->html = '<section style="background-image: url(/api/storage/images/shapes/1.svg);background-size: cover; background-position: center; background-repeat: no-repeat; padding: 7.5rem 0;"><div class="container"><div class="row justify-content-end"><div class="col-12 col-md-9 col-lg-8"><div style="background: #FFFFFF; padding: 3.75rem 2.5rem;overflow: hidden;color: #444444;border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-top: solid 0.3125rem #329ef7;"><div class="row justify-content-center"><div class="col-12 col-lg-10"><h1>Iscriviti alla newsletter</h1><p>Ricevi le nostre comunicazioni periodiche e rimani aggiornato su tutte le nostre novit&agrave;, offerte e promozioni!</p></div></div><div class="row justify-content-center mt-4"><div class="col-12 col-lg-10"><div class="input-group"><input type="text" class="form-control" placeholder="Inserisci il tuo indirizzo email"><div class="input-group-append"><button class="btn btn-primary" style="padding: .475rem 3rem .575rem; margin-left: -1px;margin-top: 1px;" type="button">Invia</button></div></div></div></div></div></div></div></div></section>';
        $templateEditorModulo_5->template_editor_group_id = $templateEditorGroupNewsletter->id;
        $templateEditorModulo_5->order = 50;
        $templateEditorModulo_5->created_id = $idUser;
        $templateEditorModulo_5->save();
        
$templateEditorPulsante_1 = new TemplateEditor();
        $templateEditorPulsante_1->description = 'Pulsante 1';
        $templateEditorPulsante_1->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-01.png';
        $templateEditorPulsante_1->html = '<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0;"><div class="container"><div class="row justify-content-center"><div class="col-12 col-md-8 text-center"><p class="lead">&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.&quot;</p><p class="mt-5 mt-sm-4"><a class="btn btn-primary" href="#">Download</a></p></div></div></div></section>';
        $templateEditorPulsante_1->template_editor_group_id = $templateEditorGroupPulsante->id;
        $templateEditorPulsante_1->order = 10;
        $templateEditorPulsante_1->created_id = $idUser;
        $templateEditorPulsante_1->save();
        
$templateEditorPulsante_2 = new TemplateEditor();
        $templateEditorPulsante_2->description = 'Pulsante 2';
        $templateEditorPulsante_2->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-02.png';
        $templateEditorPulsante_2->html = '<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0;"><div class="container"><div class="row justify-content-center"><div class="col-12 col-md-6 text-center"><h1>Call to Action</h1><p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><p class="mt-5 mt-sm-4"><a class="btn btn-secondary" href="#">Download</a></p></div></div></div></section>';
        $templateEditorPulsante_2->template_editor_group_id = $templateEditorGroupPulsante->id;
        $templateEditorPulsante_2->order = 20;
        $templateEditorPulsante_2->created_id = $idUser;
        $templateEditorPulsante_2->save();
        
$templateEditorPulsante_3 = new TemplateEditor();
        $templateEditorPulsante_3->description = 'Pulsante 3';
        $templateEditorPulsante_3->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-03.png';
        $templateEditorPulsante_3->html = '<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0;"><div class="container"><div class="row justify-content-center"><div class="col-12 col-sm-10 col-md-8 col-lg-4 text-center"><p class="lead">Seguici la nostra attivit&agrave; sui social:</p><p class="h2"><a class="mx-2" href="#"><i class="fa fa-facebook fa-stack" style="color:#329ef7;font-size:30px;"></i></a> <a class="mx-2" href="#"><i class="fa fa-instagram fa-stack" style="color:#329ef7;font-size:30px;"></i></a> <a class="mx-2" href="#"><i class="fa fa-linkedin fa-stack" style="color:#329ef7;font-size:30px;"></i></a> <a class="mx-2" href="#"><i class="fa fa-youtube fa-stack" style="color:#329ef7;font-size:30px;"></i></a></p></div></div></div></section>';
        $templateEditorPulsante_3->template_editor_group_id = $templateEditorGroupPulsante->id;
        $templateEditorPulsante_3->order = 30;
        $templateEditorPulsante_3->created_id = $idUser;
        $templateEditorPulsante_3->save();
        
$templateEditorPulsante_4 = new TemplateEditor();
        $templateEditorPulsante_4->description = 'Pulsante 4';
        $templateEditorPulsante_4->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-04.png';
        $templateEditorPulsante_4->html = '<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0;"><div class="container"><div class="row text-center pb-0 pb-lg-4"><div class="col-12"><h1>Call to action</h1></div></div><div class="row text-center pt-4 pt-md-5"><div class="col-12 col-sm-10 col-md-6 col-lg-4 m-sm-auto"><img alt="image" class="fdb-icon fr-fic fr-dii" src="/api/storage/images/icons/gift.svg"><h3>Prima Azione</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><p class="mt-3"><a class="btn btn-light sl-1" href="#">Button</a></p></div><div class="col-12 col-sm-10 col-md-6 col-lg-4 ml-sm-auto mr-sm-auto mt-5 mt-md-0"><img alt="image" class="fdb-icon fr-fic fr-dii" src="/api/storage/images/icons/cloud.svg"><h3>Seconda Azione</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><p class="mt-3"><a class="btn btn-light sl-1" href="#">Button</a></p></div></div></div></section>';
        $templateEditorPulsante_4->template_editor_group_id = $templateEditorGroupPulsante->id;
        $templateEditorPulsante_4->order = 40;
        $templateEditorPulsante_4->created_id = $idUser;
        $templateEditorPulsante_4->save();
        
$templateEditorPulsante_5 = new TemplateEditor();
        $templateEditorPulsante_5->description = 'Pulsante 5';
        $templateEditorPulsante_5->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-05.png';
        $templateEditorPulsante_5->html = '<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0; background-size: cover; background-position: center; background-repeat: no-repeat;"><div class="container py-5 my-5" style="background-image: url(/api/storage/images/shapes/2.svg);background-size: contain; background-position: center; background-repeat: no-repeat;"><div class="row justify-content-center py-5"><div class="col-12 col-md-10 col-lg-8 text-center"><div style="background: #FFFFFF; padding: 3.75rem 2.5rem; overflow: hidden; color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><h1>Call to Action</h1><p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;</p><p class="mt-4"><a class="btn btn-primary" href="#">Download</a></p></div></div></div></div></section>';
        $templateEditorPulsante_5->template_editor_group_id = $templateEditorGroupPulsante->id;
        $templateEditorPulsante_5->order = 50;
        $templateEditorPulsante_5->created_id = $idUser;
        $templateEditorPulsante_5->save();
        
$templateEditorPulsante_6 = new TemplateEditor();
        $templateEditorPulsante_6->description = 'Pulsante 6';
        $templateEditorPulsante_6->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-06.png';
        $templateEditorPulsante_6->html = '<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 5rem 0;"><div class="container py-5 my-5 bg-r" style="background-image: url(/api/storage/images/shapes/4.svg);background-size: contain; background-position: center; background-repeat: no-repeat;"><div class="row justify-content-end py-5"><div class="col-12 col-md-8 col-lg-6 mr-5 text-center"><div style="background: #FFFFFF; padding: 3.75rem 2.5rem; overflow: hidden; color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><h1>Call to Action</h1><p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><p class="mt-4"><a class="btn btn-secondary" href="#">Download</a></p></div></div></div></div></section>';
        $templateEditorPulsante_6->template_editor_group_id = $templateEditorGroupPulsante->id;
        $templateEditorPulsante_6->order = 60;
        $templateEditorPulsante_6->created_id = $idUser;
        $templateEditorPulsante_6->save();
        
$templateEditorPulsante_7 = new TemplateEditor();
        $templateEditorPulsante_7->description = 'Pulsante 7';
        $templateEditorPulsante_7->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-07.png';
        $templateEditorPulsante_7->html = '<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0;"><div class="container"><div class="row align-items-center justify-content-center"><div class="col-auto"><h2>Clicca qui per scaricare la nostra presentazione</h2></div><div class="col-auto mt-4 mt-sm-0"><a class="btn btn-primary" href="#">Download</a></div></div></div></section>';
        $templateEditorPulsante_7->template_editor_group_id = $templateEditorGroupPulsante->id;
        $templateEditorPulsante_7->order = 70;
        $templateEditorPulsante_7->created_id = $idUser;
        $templateEditorPulsante_7->save();
        
$templateEditorPulsante_8 = new TemplateEditor();
        $templateEditorPulsante_8->description = 'Pulsante 8';
        $templateEditorPulsante_8->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-08.png';
        $templateEditorPulsante_8->html = '<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0;"><div class="container"><div class="row align-items-center justify-content-center"><div class="col-auto text-center"><a class="btn btn-outline-secondary" href="#">Download</a></div><div class="col-auto mt-4 mt-sm-0"><h2>Clicca qui per scaricare la nostra presentazione</h2></div></div></div></section>';
        $templateEditorPulsante_8->template_editor_group_id = $templateEditorGroupPulsante->id;
        $templateEditorPulsante_8->order = 80;
        $templateEditorPulsante_8->created_id = $idUser;
        $templateEditorPulsante_8->save();
        
$templateEditorPulsante_9_blu = new TemplateEditor();
        $templateEditorPulsante_9_blu->description = 'Pulsante 9 (blu)';
        $templateEditorPulsante_9_blu->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-09-blue.png';
        $templateEditorPulsante_9_blu->html = '<section style="background-image: url(/api/storage/images/hero/blue.svg);min-height: calc(100% - 2 * 7.5rem);padding: 7.5rem 0;background-position: center; background-repeat: no-repeat; background-size: cover;"><div class="container justify-content-center align-items-center d-flex"><div class="row justify-content-center text-center"><div class="col-12 col-md-8"><img alt="image" class="fr-fic fr-dii" src="/api/storage/images/icons/coffee.svg" style="width: 3.75rem;"><h1 style="color:#fff;">Prendiamoci un caff&egrave;</h1><p style="color:#fff;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p class="mt-5"><a class="btn btn-dark" href="#">Clicca qui per...</a></p></div></div></div></section>';
        $templateEditorPulsante_9_blu->template_editor_group_id = $templateEditorGroupPulsante->id;
        $templateEditorPulsante_9_blu->order = 90;
        $templateEditorPulsante_9_blu->created_id = $idUser;
        $templateEditorPulsante_9_blu->save();
        
$templateEditorPulsante_9_rosso = new TemplateEditor();
        $templateEditorPulsante_9_rosso->description = 'Pulsante 9 (rosso)';
        $templateEditorPulsante_9_rosso->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-09-red.png';
        $templateEditorPulsante_9_rosso->html = '<section style="background-image: url(/api/storage/images/hero/red.svg);min-height: calc(100% - 2 * 7.5rem);padding: 7.5rem 0;background-position: center; background-repeat: no-repeat; background-size: cover;"><div class="container justify-content-center align-items-center d-flex"><div class="row justify-content-center text-center"><div class="col-12 col-md-8"><img alt="image" class="fr-fic fr-dii" src="/api/storage/images/icons/coffee.svg" style="width: 3.75rem;"><h1 style="color:#fff;">Prendiamoci un caff&egrave;</h1><p style="color:#fff;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p class="mt-5"><a class="btn btn-dark" href="#">Clicca qui per...</a></p></div></div></div></section>';
        $templateEditorPulsante_9_rosso->template_editor_group_id = $templateEditorGroupPulsante->id;
        $templateEditorPulsante_9_rosso->order = 91;
        $templateEditorPulsante_9_rosso->created_id = $idUser;
        $templateEditorPulsante_9_rosso->save();
        
$templateEditorPulsante_9_giallo = new TemplateEditor();
        $templateEditorPulsante_9_giallo->description = 'Pulsante 9 (giallo)';
        $templateEditorPulsante_9_giallo->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-09-yellow.png';
        $templateEditorPulsante_9_giallo->html = '<section style="background-image: url(/api/storage/images/hero/yellow.svg);min-height: calc(100% - 2 * 7.5rem);padding: 7.5rem 0;background-position: center; background-repeat: no-repeat; background-size: cover;"><div class="container justify-content-center align-items-center d-flex"><div class="row justify-content-center text-center"><div class="col-12 col-md-8"><img alt="image" class="fr-fic fr-dii" src="/api/storage/images/icons/coffee.svg" style="width: 3.75rem;"><h1>Prendiamoci un caff&egrave;</h1><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p class="mt-5"><a class="btn btn-dark" href="#">Clicca qui per...</a></p></div></div></div></section>';
        $templateEditorPulsante_9_giallo->template_editor_group_id = $templateEditorGroupPulsante->id;
        $templateEditorPulsante_9_giallo->order = 92;
        $templateEditorPulsante_9_giallo->created_id = $idUser;
        $templateEditorPulsante_9_giallo->save();
        
$templateEditorPulsante_9_viola = new TemplateEditor();
        $templateEditorPulsante_9_viola->description = 'Pulsante 9 (viola)';
        $templateEditorPulsante_9_viola->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-09.png';
        $templateEditorPulsante_9_viola->html = '<section style="background-image: url(/api/storage/images/hero/purple.svg);min-height: calc(100% - 2 * 7.5rem);padding: 7.5rem 0;background-position: center; background-repeat: no-repeat; background-size: cover;"><div class="container justify-content-center align-items-center d-flex"><div class="row justify-content-center text-center"><div class="col-12 col-md-8"><img alt="image" class="fr-fic fr-dii" src="/api/storage/images/icons/coffee.svg" style="width: 3.75rem;"><h1 style="color:#fff;">Prendiamoci un caff&egrave;</h1><p style="color:#fff;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p class="mt-5"><a class="btn btn-dark" href="#">Clicca qui per...</a></p></div></div></div></section>';
        $templateEditorPulsante_9_viola->template_editor_group_id = $templateEditorGroupPulsante->id;
        $templateEditorPulsante_9_viola->order = 93;
        $templateEditorPulsante_9_viola->created_id = $idUser;
        $templateEditorPulsante_9_viola->save();
        
$templateEditorPulsante_10 = new TemplateEditor();
        $templateEditorPulsante_10->description = 'Pulsante 10';
        $templateEditorPulsante_10->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-10.png';
        $templateEditorPulsante_10->html = '<section class="fdb-block py-0"><div class="container bg-r py-5 my-5" style="background-image: url(/api/storage/images/shapes/1.svg);padding: 7.5rem 0;background-position: right;background-repeat: no-repeat; background-size: contain;"><div class="row py-5"><div class="col-12 col-sm-10 col-md-8 col-lg-6 text-left"><h1>Call to Action</h1><p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p class="mt-4"><a class="btn btn-primary" href="#">Download</a></p></div></div></div></section>';
        $templateEditorPulsante_10->template_editor_group_id = $templateEditorGroupPulsante->id;
        $templateEditorPulsante_10->order = 100;
        $templateEditorPulsante_10->created_id = $idUser;
        $templateEditorPulsante_10->save();
        
$templateEditorYouTube_video_fit = new TemplateEditor();
        $templateEditorYouTube_video_fit->description = 'YouTube video (fit)';
        $templateEditorYouTube_video_fit->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/video-00.png';
        $templateEditorYouTube_video_fit->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 videoWrapper"><iframe width="100%" height="315" src="https://www.youtube.com/embed/mkggXE5e2yk" frameborder="0" allowfullscreen=""></iframe></div></div></div></div>';
        $templateEditorYouTube_video_fit->template_editor_group_id = $templateEditorGroupVideo->id;
        $templateEditorYouTube_video_fit->order = 10;
        $templateEditorYouTube_video_fit->created_id = $idUser;
        $templateEditorYouTube_video_fit->save();
        
$templateEditorYouTube_video_100 = new TemplateEditor();
        $templateEditorYouTube_video_100->description = 'YouTube video (100%)';
        $templateEditorYouTube_video_100->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/video-01.png';
        $templateEditorYouTube_video_100->html = '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12 videoWrapper"><iframe width="100%" height="315" src="https://www.youtube.com/embed/mkggXE5e2yk" frameborder="0" allowfullscreen=""></iframe></div></div></div></div>';
        $templateEditorYouTube_video_100->template_editor_group_id = $templateEditorGroupVideo->id;
        $templateEditorYouTube_video_100->order = 20;
        $templateEditorYouTube_video_100->created_id = $idUser;
        $templateEditorYouTube_video_100->save();
        
$templateEditorModulo_1 = new TemplateEditor();
        $templateEditorModulo_1->description = 'Modulo 1';
        $templateEditorModulo_1->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/teams-01.png';
        $templateEditorModulo_1->html = '<section style="padding: 7.5rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-8"><h1>Team</h1></div></div><div class="row text-center mt-5"><div class="col-4"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/9.jpg"><h3 style="margin-top:1.5rem;"><strong>Mario Rossi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">Separated they live in Bookmarksgrove right at the coast of the Semantics.</p></div><div class="col-4"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/1.jpg"><h3 style="margin-top:1.5rem;"><strong>Carlo Bianchi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">One morning, when Gregor Samsa woke from troubled dreams.</p></div><div class="col-4"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/3.jpg"><h3 style="margin-top:1.5rem;"><strong>Lucia Verdi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">A small river named Duden flows by their place and supplies it.</p></div></div></div></section>';
        $templateEditorModulo_1->template_editor_group_id = $templateEditorGroupTeams->id;
        $templateEditorModulo_1->order = 10;
        $templateEditorModulo_1->created_id = $idUser;
        $templateEditorModulo_1->save();
        
$templateEditorModulo_2 = new TemplateEditor();
        $templateEditorModulo_2->description = 'Modulo 2';
        $templateEditorModulo_2->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/teams-02.png';
        $templateEditorModulo_2->html = '<section style="padding: 7.5rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-8"><h1>Team</h1></div></div><div class="row text-center mt-5"><div class="col-lg-3 col-sm-12 col-md-6"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/9.jpg"><h3 style="margin-top:1.5rem;"><strong>Mario Rossi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">Separated they live in Bookmarksgrove right at the coast of the Semantics.</p></div><div class="col-lg-3 col-sm-12 col-md-6"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/1.jpg"><h3 style="margin-top:1.5rem;"><strong>Carlo Bianchi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">One morning, when Gregor Samsa woke from troubled dreams.</p></div><div class="col-lg-3 col-sm-12 col-md-6"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/3.jpg"><h3 style="margin-top:1.5rem;"><strong>Lucia Verdi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">A small river named Duden flows by their place and supplies it.</p></div><div class="col-lg-3 col-sm-12 col-md-6"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/6.jpg"><h3 style="margin-top:1.5rem;"><strong>Claudia Aranci</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">A small river named Duden flows by their place and supplies it.</p></div></div></div></section>';
        $templateEditorModulo_2->template_editor_group_id = $templateEditorGroupTeams->id;
        $templateEditorModulo_2->order = 20;
        $templateEditorModulo_2->created_id = $idUser;
        $templateEditorModulo_2->save();
        
$templateEditorModulo_3 = new TemplateEditor();
        $templateEditorModulo_3->description = 'Modulo 3';
        $templateEditorModulo_3->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/teams-03.png';
        $templateEditorModulo_3->html = '<section style="padding: 7.5rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-8"><h1>Team</h1></div></div><div class="row text-center mt-5"><div class="col-lg-4 col-sm-12"><div style="box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-radius: 0.25rem;"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/people/1.jpg"><div style="padding: 1rem!important;"><h3 style="margin-top:1.5rem;"><strong>Carlo Bianchi</strong></h3><p style="color: #8892a0;">Ruolo</p></div></div></div><div class="col-lg-4 col-sm-12"><div style="box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-radius: 0.25rem;"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/people/3.jpg"><div style="padding: 1rem!important;"><h3 style="margin-top:1.5rem;"><strong>Lucia Verdi</strong></h3><p style="color: #8892a0;">Ruolo</p></div></div></div><div class="col-lg-4 col-sm-12"><div style="box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-radius: 0.25rem;"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/people/6.jpg"><div style="padding: 1rem!important;"><h3 style="margin-top:1.5rem;"><strong>Claudia Aranci</strong></h3><p style="color: #8892a0;">Ruolo</p></div></div></div></div></div></section>';
        $templateEditorModulo_3->template_editor_group_id = $templateEditorGroupTeams->id;
        $templateEditorModulo_3->order = 30;
        $templateEditorModulo_3->created_id = $idUser;
        $templateEditorModulo_3->save();
        
$templateEditorModulo_4 = new TemplateEditor();
        $templateEditorModulo_4->description = 'Modulo 4';
        $templateEditorModulo_4->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/teams-04.png';
        $templateEditorModulo_4->html = '<section style="padding: 7.5rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-8"><h1>Team</h1></div></div><div class="row text-center mt-5"><div class="col-lg-3 col-sm-12 col-md-6"><div style="box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-radius: 0.25rem;"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/people/9.jpg"><div style="padding: 1rem!important;"><h3 style="margin-top:1.5rem;"><strong>Mario Rossi</strong></h3><p style="color: #8892a0;">Ruolo</p></div></div></div><div class="col-lg-3 col-sm-12 col-md-6"><div style="box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-radius: 0.25rem;"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/people/1.jpg"><div style="padding: 1rem!important;"><h3 style="margin-top:1.5rem;"><strong>Carlo Bianchi</strong></h3><p style="color: #8892a0;">Ruolo</p></div></div></div><div class="col-lg-3 col-sm-12 col-md-6"><div style="box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-radius: 0.25rem;"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/people/3.jpg"><div style="padding: 1rem!important;"><h3 style="margin-top:1.5rem;"><strong>Lucia Verdi</strong></h3><p style="color: #8892a0;">Ruolo</p></div></div></div><div class="col-lg-3 col-sm-12 col-md-6"><div style="box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-radius: 0.25rem;"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/people/6.jpg"><div style="padding: 1rem!important;"><h3 style="margin-top:1.5rem;"><strong>Claudia Aranci</strong></h3><p style="color: #8892a0;">Ruolo</p></div></div></div></div></div></section>';
        $templateEditorModulo_4->template_editor_group_id = $templateEditorGroupTeams->id;
        $templateEditorModulo_4->order = 40;
        $templateEditorModulo_4->created_id = $idUser;
        $templateEditorModulo_4->save();
        
$templateEditorModulo_5 = new TemplateEditor();
        $templateEditorModulo_5->description = 'Modulo 5';
        $templateEditorModulo_5->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/teams-05.png';
        $templateEditorModulo_5->html = '<section style="padding: 7.5rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-8"><h1>Team</h1></div></div><div class="row text-center mt-5"><div class="col-sm-3 m-sm-auto"><img alt="image" class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/9.jpg"><h3 style="margin-top:1.5rem;"><strong>Mario Rossi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">Separated they live in Bookmarksgrove right at the coast of the Semantics.</p></div><div class="col-sm-3 m-sm-auto"><img alt="image" class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/1.jpg"><h3 style="margin-top:1.5rem;"><strong>Carlo Bianchi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">One morning, when Gregor Samsa woke from troubled dreams.</p></div><div class="col-sm-3 m-sm-auto"><img alt="image" class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/3.jpg"><h3 style="margin-top:1.5rem;"><strong>Lucia Verdi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">A small river named Duden flows by their place and supplies it.</p></div></div></div></section>';
        $templateEditorModulo_5->template_editor_group_id = $templateEditorGroupTeams->id;
        $templateEditorModulo_5->order = 50;
        $templateEditorModulo_5->created_id = $idUser;
        $templateEditorModulo_5->save();
        
$templateEditorModulo_6 = new TemplateEditor();
        $templateEditorModulo_6->description = 'Modulo 6';
        $templateEditorModulo_6->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/teams-06.png';
        $templateEditorModulo_6->html = '<section style="padding: 7.5rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-8"><h1>Team</h1></div></div><div class="row text-center mt-5"><div class="col-sm-2 m-sm-auto"><img alt="image" class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/9.jpg"><h3 style="margin-top:1.5rem;"><strong>Mario</strong><br><strong>Rossi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">Separated they live in Bookmarksgrove right at the coast of the Semantics.</p></div><div class="col-sm-2 m-sm-auto"><img alt="image" class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/1.jpg"><h3 style="margin-top:1.5rem;"><strong>Carlo</strong><br><strong>Bianchi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">One morning, when Gregor Samsa woke from troubled dreams.</p></div><div class="col-sm-2 m-sm-auto"><img alt="image" class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/3.jpg"><h3 style="margin-top:1.5rem;"><strong>Lucia</strong><br><strong>Verdi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">A small river named Duden flows by their place and supplies it.</p></div><div class="col-sm-2 m-sm-auto"><img alt="image" class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/6.jpg"><h3 style="margin-top:1.5rem;"><strong>Claudia</strong><br><strong>Gialli</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">A small river named Duden flows by their place and supplies it.</p></div></div></div></section>';
        $templateEditorModulo_6->template_editor_group_id = $templateEditorGroupTeams->id;
        $templateEditorModulo_6->order = 60;
        $templateEditorModulo_6->created_id = $idUser;
        $templateEditorModulo_6->save();
        
$templateEditorModulo_7 = new TemplateEditor();
        $templateEditorModulo_7->description = 'Modulo 7';
        $templateEditorModulo_7->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/teams-07.png';
        $templateEditorModulo_7->html = '<section style="padding: 7.5rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-8"><h1>Il Nostro Team</h1><p class="lead">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p></div></div><div style="height:6rem;"><br></div><div class="row justify-content-center text-left"><div class="col-sm-6"><div class="row align-items-center"><div class="col-4"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/6.jpg"></div><div class="col-8"><h3><strong>Claudia Bianchi</strong></h3><p class="lead">Ruolo</p><p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p></div></div></div><div class="col-sm-6"><div class="row align-items-center"><div class="col-4"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/3.jpg"></div><div class="col-8"><h3><strong>Lucia Rossi</strong></h3><p class="lead">Ruolo</p><p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p></div></div></div></div><div class="row-70"><br></div><div class="row justify-content-center text-left"><div class="col-sm-6"><div class="row align-items-center"><div class="col-4"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/1.jpg"></div><div class="col-8"><h3><strong>Mario Verdi</strong></h3><p class="lead">Ruolo</p><p>One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p></div></div></div><div class="col-sm-6"><div class="row align-items-center"><div class="col-4"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/9.jpg"></div><div class="col-8"><h3><strong>Fabio Pollici</strong></h3><p class="lead">Ruolo</p><p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life.</p></div></div></div></div></div></section>';
        $templateEditorModulo_7->template_editor_group_id = $templateEditorGroupTeams->id;
        $templateEditorModulo_7->order = 70;
        $templateEditorModulo_7->created_id = $idUser;
        $templateEditorModulo_7->save();
        
$templateEditorModulo_1 = new TemplateEditor();
        $templateEditorModulo_1->description = 'Modulo 1';
        $templateEditorModulo_1->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-01.png';
        $templateEditorModulo_1->html = '<section><div class="container-fluid p-0 pb-md-5"><iframe class="map" src="https://maps.google.com/maps?hl=en&q=piazza maggiore+(Bologna)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" width="100%" height="300px"></iframe></div><div class="container"><div class="row mt-5"><div class="col-12 col-md-6 col-lg-5"><h2>Contattaci</h2><p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove.</p><p class="lead">It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p><p class="h3 mt-5"><strong>Email:</strong> <a href="#">hello@website.com</a></p><p class="lead"><strong>Phone:</strong> <a href="#">+39 123 123 1232</a></p></div><div class="col-12 col-md-6 ml-auto pt-5 pt-md-0"><div class="row"><div class="col"><input type="text" class="form-control" placeholder="Nome"></div><div class="col"><input type="text" class="form-control" placeholder="Cognome"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Inserisci la tua mail"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Oggetto"></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="3" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col"><button class="btn btn-primary" type="submit">Invia</button></div></div></div></div></div></section>';
        $templateEditorModulo_1->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorModulo_1->order = 10;
        $templateEditorModulo_1->created_id = $idUser;
        $templateEditorModulo_1->save();
        
$templateEditorModulo_2_blu = new TemplateEditor();
        $templateEditorModulo_2_blu->description = 'Modulo 2 (blu)';
        $templateEditorModulo_2_blu->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-02.png';
        $templateEditorModulo_2_blu->html = '<section class="fdb-block bg-dark" style="background-image: url(/api/storage/images/hero/blue.svg);background-size: cover;padding: 7rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-12 col-md-8 col-lg-7"><h1>Contattaci</h1><h2>Vorremmo avere tue notizie!</h2></div></div><div class="row pt-4"><div class="col-12"><div class="row"><div class="col-12 col-md"><input type="text" class="form-control" placeholder="Name"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Email"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Telefono (opzionale)"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Oggetto"></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="3" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col text-center"><button class="btn btn-dark" type="submit">Invia</button></div></div></div></div></div></section>';
        $templateEditorModulo_2_blu->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorModulo_2_blu->order = 20;
        $templateEditorModulo_2_blu->created_id = $idUser;
        $templateEditorModulo_2_blu->save();
        
$templateEditorModulo_2_rosso = new TemplateEditor();
        $templateEditorModulo_2_rosso->description = 'Modulo 2 (rosso)';
        $templateEditorModulo_2_rosso->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-02-red.png';
        $templateEditorModulo_2_rosso->html = '<section style="background-image: url(/api/storage/images/hero/red.svg);background-size: cover;padding: 7rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-12 col-md-8 col-lg-7"><h1>Contattaci</h1><h2>Vorremmo avere tue notizie!</h2></div></div><div class="row pt-4"><div class="col-12"><div class="row"><div class="col-12 col-md"><input type="text" class="form-control" placeholder="Name"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Email"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Telefono (opzionale)"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Oggetto"></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="3" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col text-center"><button class="btn btn-dark" type="submit">Invia</button></div></div></div></div></div></section>';
        $templateEditorModulo_2_rosso->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorModulo_2_rosso->order = 21;
        $templateEditorModulo_2_rosso->created_id = $idUser;
        $templateEditorModulo_2_rosso->save();
        
$templateEditorModulo_2_giallo = new TemplateEditor();
        $templateEditorModulo_2_giallo->description = 'Modulo 2 (giallo)';
        $templateEditorModulo_2_giallo->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-02-yellow.png';
        $templateEditorModulo_2_giallo->html = '<section style="background-image: url(/api/storage/images/hero/yellow.svg);background-size: cover;padding: 7rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-12 col-md-8 col-lg-7"><h1 style="color:#333;">Contattaci</h1><h2 style="color:#333;">Vorremmo avere tue notizie!</h2></div></div><div class="row pt-4"><div class="col-12"><div class="row"><div class="col-12 col-md"><input type="text" class="form-control" placeholder="Name"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Email"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Telefono (opzionale)"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Oggetto"></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="3" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col text-center"><button class="btn btn-dark" type="submit">Invia</button></div></div></div></div></div></section>';
        $templateEditorModulo_2_giallo->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorModulo_2_giallo->order = 22;
        $templateEditorModulo_2_giallo->created_id = $idUser;
        $templateEditorModulo_2_giallo->save();
        
$templateEditorModulo_2_vola = new TemplateEditor();
        $templateEditorModulo_2_vola->description = 'Modulo 2 (vola)';
        $templateEditorModulo_2_vola->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-02-viola.png';
        $templateEditorModulo_2_vola->html = '<section style="background-image: url(/api/storage/images/hero/purple.svg);background-size: cover;padding: 7rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-12 col-md-8 col-lg-7"><h1>Contattaci</h1><h2>Vorremmo avere tue notizie!</h2></div></div><div class="row pt-4"><div class="col-12"><div class="row"><div class="col-12 col-md"><input type="text" class="form-control" placeholder="Name"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Email"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Telefono (opzionale)"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Oggetto"></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="3" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col text-center"><button class="btn btn-dark" type="submit">Invia</button></div></div></div></div></div></section>';
        $templateEditorModulo_2_vola->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorModulo_2_vola->order = 23;
        $templateEditorModulo_2_vola->created_id = $idUser;
        $templateEditorModulo_2_vola->save();
        
$templateEditorModulo_3 = new TemplateEditor();
        $templateEditorModulo_3->description = 'Modulo 3';
        $templateEditorModulo_3->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-03.png';
        $templateEditorModulo_3->html = '<section style="padding:4rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-12 col-md-8 col-lg-7"><h1>Contattaci</h1><p class="lead">Lorem Ipsum decided to leave for the far World of Grammar.</p></div></div><div class="row pt-4"><div class="col-12 col-md-6"><iframe class="map" src="https://maps.google.com/maps?hl=en&q=piazza maggiore+(Bologna)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" width="100%" height="350px"></iframe></div><div class="col-12 col-md-6 pt-5 pt-md-0"><div class="row"><div class="col"><input type="email" class="form-control" placeholder="Inserisci la tua mail"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Oggetto"></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="3" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col"><button class="btn btn-primary" type="submit">Invia</button></div></div></div></div></div></section>';
        $templateEditorModulo_3->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorModulo_3->order = 30;
        $templateEditorModulo_3->created_id = $idUser;
        $templateEditorModulo_3->save();
        
$templateEditorModulo_4 = new TemplateEditor();
        $templateEditorModulo_4->description = 'Modulo 4';
        $templateEditorModulo_4->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-04.png';
        $templateEditorModulo_4->html = '<section style="background-image: url(/api/storage/images/shapes/9.svg);background-size: contain; background-position: center; background-repeat: no-repeat; padding: 3rem 0;"><div class="container py-5 my-5"><div class="row py-5"><div class="col py-5"><div class="fdb-box fdb-touch" style="background: #FFFFFF; padding: 3.75rem 2.5rem; overflow: hidden; color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-top: solid 0.3125rem #329ef7;"><div class="row text-center justify-content-center"><div class="col-12 col-md-9 col-lg-7"><h1>Contattaci</h1><p class="lead">Lorem Ipsum dolor sit amet, Lorem Ipsum dolor sit amet, Lorem Ipsum dolor sit amet, Lorem Ipsum dolor sit amet, .</p></div></div><div class="row justify-content-center pt-4"><div class="col-12 col-md-8"><div class="row"><div class="col-12 col-md"><input type="text" class="form-control" placeholder="Nome"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Email"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Oggetto"></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="3" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col text-center"><button class="btn btn-primary" type="submit">Invia</button></div></div></div></div></div></div></div></div></section>';
        $templateEditorModulo_4->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorModulo_4->order = 40;
        $templateEditorModulo_4->created_id = $idUser;
        $templateEditorModulo_4->save();
        
$templateEditorModulo_5 = new TemplateEditor();
        $templateEditorModulo_5->description = 'Modulo 5';
        $templateEditorModulo_5->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-05.png';
        $templateEditorModulo_5->html = '<section><div class="container-fluid p-0 pb-3"><iframe class="map" src="https://maps.google.com/maps?hl=en&q=piazza maggiore+(Bologna)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" width="100%" height="300" allowfullscreen=""></iframe></div><div class="container"><div class="row text-center justify-content-center pt-5"><div class="col-12 col-md-7"><h1>Contattaci</h1></div></div><div class="row justify-content-center pt-4"><div class="col-12 col-md-7"><div class="row"><div class="col"><input type="text" class="form-control" placeholder="Email"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Oggetto"></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="3" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col text-center"><button class="btn btn-primary" type="submit">Invia</button></div></div></div></div><div class="row-100"><br></div></div><div class="bg-dark"><div class="container"><div class="row-50"><br></div><div class="row justify-content-center text-center"><div class="col-12 col-md mr-auto ml-auto"><img alt="image" height="40" class="mb-2 fr-fic fr-dii" src="/api/storage/images/icons/phone.svg"><p class="lead">+39 112 123 752</p></div><div class="col-12 col-md pt-4 pt-md-0 mr-auto ml-auto"><img alt="image" height="40" class="mb-2 fr-fic fr-dii" src="/api/storage/images/icons/navigation.svg"><p class="lead">Piazza Maggiore 1<br>Bologna, BO 40100 Italy</p></div><div class="col-12 col-md pt-4 pt-md-0 mr-auto ml-auto"><img alt="image" height="40" class="mb-2 fr-fic fr-dii" src="/api/storage/images/icons/mail.svg"><p class="lead">supporto@azienda.com</p></div></div><div class="row-50"><br></div></div></div><div class="container"><div class="row-70"><br></div><div class="row text-center"><div class="col"><p class="h2"><a class="mx-2" href="#"></a> <a class="mx-2" href="#"></a> <a class="mx-2" href="#"></a> <a class="mx-2" href="#"></a> <a class="mx-2" href="#"></a></p></div></div></div></section>';
        $templateEditorModulo_5->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorModulo_5->order = 50;
        $templateEditorModulo_5->created_id = $idUser;
        $templateEditorModulo_5->save();
        
$templateEditorModulo_6 = new TemplateEditor();
        $templateEditorModulo_6->description = 'Modulo 6';
        $templateEditorModulo_6->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-06.png';
        $templateEditorModulo_6->html = '<section><div class="container-fluid p-0 pb-5"><iframe class="map" src="https://maps.google.com/maps?hl=en&q=piazza maggiore+(Bologna)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" width="100%" height="300" allowfullscreen=""></iframe></div><div class="container"><div class="row pt-5"><div class="col-12"><div class="row"><div class="col-12 col-md"><label>Nome</label> &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" class="form-control"></div><div class="col-12 col-md mt-4 mt-md-0"><label>Cognome</label> &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" class="form-control"></div></div><div class="row mt-4"><div class="col"><label>Il tuo indirizzo email</label> &nbsp;&nbsp;&nbsp;&nbsp;<input type="email" class="form-control"></div></div><div class="row mt-4"><div class="col"><label>Come possiamo aiutarti?</label> <textarea class="form-control" name="message" rows="3"></textarea></div></div><div class="row mt-4 text-center"><div class="col"><button class="btn btn-primary" type="submit">Invia</button></div></div></div></div></div></section>';
        $templateEditorModulo_6->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorModulo_6->order = 60;
        $templateEditorModulo_6->created_id = $idUser;
        $templateEditorModulo_6->save();
        
$templateEditorModulo_7 = new TemplateEditor();
        $templateEditorModulo_7->description = 'Modulo 7';
        $templateEditorModulo_7->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-07.png';
        $templateEditorModulo_7->html = '<section style="background-image: url(/api/storage/images/shapes/6.svg);background-size: contain; background-position: center; background-repeat: no-repeat;"><div class="container py-5"><div class="row text-center justify-content-center"><div class="col-12 col-md-8 col-lg-7"><h1>Contattaci</h1><p>Lasciaci i tuoi dati e sar&agrave; nostra premura contattarti al pi&ugrave; presto!</p></div></div><div class="row-50"><br></div><div class="row justify-content-center"><div class="col-12 col-md-8 col-lg-7"><div class="row"><div class="col"><label>Il tuo indirizzo email</label> &nbsp;&nbsp;&nbsp;<input type="text" class="form-control"></div></div><div class="row mt-4"><div class="col"><label>Oggetto</label> &nbsp;&nbsp;&nbsp;<input type="email" class="form-control"></div></div><div class="row mt-4"><div class="col"><label>Come possiamo aiutarti?</label> <textarea class="form-control" name="message" rows="3"></textarea></div></div><div class="row mt-4"><div class="col text-right"><button class="btn btn-dark" type="submit">Invia</button></div></div></div></div><div class="row-100"><br></div></div><div class="container-fluid p-0"><iframe class="map" src="https://maps.google.com/maps?hl=en&q=piazza maggiore+(Bologna)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" width="100%" height="300" allowfullscreen=""></iframe></div></section>';
        $templateEditorModulo_7->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorModulo_7->order = 70;
        $templateEditorModulo_7->created_id = $idUser;
        $templateEditorModulo_7->save();
        
$templateEditorModulo_8 = new TemplateEditor();
        $templateEditorModulo_8->description = 'Modulo 8';
        $templateEditorModulo_8->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-08.png';
        $templateEditorModulo_8->html = '<section><div style="background-color: #F4F7FE;"><div class="container"><div class="row-100"><br></div><div class="row text-left"><div class="col-8"><h1>Contattaci</h1><p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate</p></div></div><div class="row-100"><br></div></div></div><div class="container bg-r"><div class="row-100"><br></div><div class="row"><div class="col-12 col-md-6 col-lg-5"><h2>Contatti e recapiti</h2><p class="text-large">Supporto, vendite e servizi sono disponibili ai seguenti recapiti</p><p class="h3 mt-4 mt-lg-5"><strong>Supporto</strong></p><p>+800 800 800</p><p><a href="#">Contatta il Supporto</a></p><p>Il nostro supporto tecnico &egrave; disponibile per telefono o email dalle 9.00 alle 17.00, dal Luned&igrave; al Venerd&igrave;.</p><p class="h3 mt-4 mt-lg-5"><strong>Vendite</strong></p><p>+800 800 800</p><p><a href="#">Contata il reparto commerciale</a></p><p>Il nostro supporto commerciale &egrave; disponibile per telefono o email dalle 9.00 alle 17.00, dal Luned&igrave; al Venerd&igrave;.</p><p class="h3 mt-4 mt-lg-5"><strong>Domande Generiche</strong></p><p><a href="#">hello@website.com</a></p></div><div class="col-12 col-md-6 ml-auto"><h2>Fornisci maggiori informazioni</h2><div class="row"><div class="col"><input type="text" class="form-control" placeholder="Nome"></div><div class="col"><input type="text" class="form-control" placeholder="Cognome"></div></div><div class="row mt-4"><div class="col"><input type="text" class="form-control" placeholder="Azienda"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Email"></div></div><div class="row mt-4"><div class="col"><input type="text" class="form-control" placeholder="Telefono"></div><div class="col"><input type="text" class="form-control" placeholder="Località"></div></div><div class="row mt-4"><div class="col"><select class="form-control" required="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<option value="">Seleziona il dipartimento</option> <option value="1">Supporto Tecnico</option> <option value="2">Vendite</option> <option value="3">Amministrazione</option>&nbsp;</select></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="5" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col"><button class="btn btn-primary" type="submit">Invia</button></div></div></div></div></div></section>';
        $templateEditorModulo_8->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorModulo_8->order = 80;
        $templateEditorModulo_8->created_id = $idUser;
        $templateEditorModulo_8->save();
        
$templateEditorModulo_9 = new TemplateEditor();
        $templateEditorModulo_9->description = 'Modulo 9';
        $templateEditorModulo_9->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-09.png';
        $templateEditorModulo_9->html = '<section style="padding: 7.5rem 0;background-color:#F4F7FE;"><div class="container"><div class="row text-center justify-content-center"><div class="col-12 col-md-8 col-lg-7"><p class="h2">info@azienda.com</p><p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><p><br></p><div class="row"><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-facebook fa-stack" style="color:#222;font-size:40px;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-youtube-play fa-stack" style="color:#222;font-size:40px;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-instagram fa-stack" style="color:#222;font-size:40px;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-linkedin fa-stack" style="color:#222;font-size:40px;"></i></a></div></div></div></div></div></section>';
        $templateEditorModulo_9->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorModulo_9->order = 90;
        $templateEditorModulo_9->created_id = $idUser;
        $templateEditorModulo_9->save();
        

$templateEditorMappa_fit = new TemplateEditor();
        $templateEditorMappa_fit->description = 'Mappa (fit)';
        $templateEditorMappa_fit->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/mappa-01.png';
        $templateEditorMappa_fit->html = '<section><div class="container p-0 pb-md-5"><iframe class="map" src="https://maps.google.com/maps?hl=en&q=piazza maggiore+(Bologna)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" width="100%" height="500px"></iframe></div></section>';
        $templateEditorMappa_fit->template_editor_group_id = $templateEditorGroupMappa->id;
        $templateEditorMappa_fit->order = 10;
        $templateEditorMappa_fit->created_id = $idUser;
        $templateEditorMappa_fit->save();
        
$templateEditorMappa_100 = new TemplateEditor();
        $templateEditorMappa_100->description = 'Mappa (100%)';
        $templateEditorMappa_100->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/mappa-02.png';
        $templateEditorMappa_100->html = '<section><div class="container-fluid p-0 pb-md-5"><iframe class="map" src="https://maps.google.com/maps?hl=en&q=piazza maggiore+(Bologna)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" width="100%" height="500px"></iframe></div></section>';
        $templateEditorMappa_100->template_editor_group_id = $templateEditorGroupMappa->id;
        $templateEditorMappa_100->order = 20;
        $templateEditorMappa_100->created_id = $idUser;
        $templateEditorMappa_100->save();
        
$templateEditorModulo_1 = new TemplateEditor();
        $templateEditorModulo_1->description = 'Modulo 1';
        $templateEditorModulo_1->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/testimonial-01.png';
        $templateEditorModulo_1->html = '<section style="padding:3rem 0;"><div class="container"><div class="row align-items-center justify-content-center"><div class="col-12 col-md-10 col-lg-8"><p class="lead">&quot;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&quot;</p><p class="lead"><strong>Mario Rossi</strong> <em class="ml-4">CEO e Titolare</em></p></div><div class="col-8 col-sm-6 col-md-2 col-lg-3 col-xl-2 mt-4 mt-md-0 ml-auto mr-auto mr-md-0"><img class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/1.jpg"></div></div></div></section>';
        $templateEditorModulo_1->template_editor_group_id = $templateEditorGroupTestimonial->id;
        $templateEditorModulo_1->order = 10;
        $templateEditorModulo_1->created_id = $idUser;
        $templateEditorModulo_1->save();
        
$templateEditorModulo_2 = new TemplateEditor();
        $templateEditorModulo_2->description = 'Modulo 2';
        $templateEditorModulo_2->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/testimonial-02.png';
        $templateEditorModulo_2->html = '<section style="padding:3rem 0;"><div class="container"><div class="row align-items-center justify-content-center"><div class="col-10 col-sm-6 col-md-4 col-lg-3 col-xl-2 m-auto"><img alt="image" class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/3.jpg"></div><div class="col-12 col-md-8 ml-auto mr-auto mt-4 mt-md-0"><p class="lead">&quot;Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.&quot;</p><p class="h3 mt-4 mt-lg-5"><strong>Claudia Bianchi</strong></p><p><em>Direttore Marketing</em></p></div></div></div></section>';
        $templateEditorModulo_2->template_editor_group_id = $templateEditorGroupTestimonial->id;
        $templateEditorModulo_2->order = 20;
        $templateEditorModulo_2->created_id = $idUser;
        $templateEditorModulo_2->save();
        
$templateEditorModulo_3 = new TemplateEditor();
        $templateEditorModulo_3->description = 'Modulo 3';
        $templateEditorModulo_3->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/testimonial-03.png';
        $templateEditorModulo_3->html = '<section style="padding: 7.5rem 0;background-image: url(/api/storage/images/hero/red.svg);background-size: cover; background-position: center; background-repeat: no-repeat;"><div class="container" style="background: #FFFFFF; padding: 3.75rem 2.5rem;border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><div class="row align-items-center justify-content-center"><div class="col-12 col-md-10 col-lg-8"><p class="lead">&quot;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&quot;</p><p class="lead"><strong>Mario Rossi</strong> <em class="ml-4">CEO e Titolare</em></p></div><div class="col-8 col-sm-6 col-md-2 col-lg-3 col-xl-2 mt-4 mt-md-0 ml-auto mr-auto mr-md-0"><img class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/1.jpg"></div></div></div></section>';
        $templateEditorModulo_3->template_editor_group_id = $templateEditorGroupTestimonial->id;
        $templateEditorModulo_3->order = 30;
        $templateEditorModulo_3->created_id = $idUser;
        $templateEditorModulo_3->save();
        
$templateEditorModulo_4 = new TemplateEditor();
        $templateEditorModulo_4->description = 'Modulo 4';
        $templateEditorModulo_4->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/testimonial-04.png';
        $templateEditorModulo_4->html = '<section style="padding: 3rem 0;"><div class="container"><div class="row align-items-center justify-content-center"><div class="col-md-6"><div style="border-top: solid 0.3125rem #329ef7;padding: 3.75rem 2.5rem;color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><p class="h3 mb-4">&quot;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.&quot;</p><p><img alt="image" height="50" class="rounded-circle fr-fic fr-dii" src="/api/storage/images/people/3.jpg"> <strong class="ml-3">Claudia Bianchi</strong></p></div></div><div class="col-md-6 mt-4 mt-md-0"><div style="border-top: solid 0.3125rem #329ef7;padding: 3.75rem 2.5rem;color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><p class="h3 mb-4">&quot;A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.&quot;</p><p><img alt="image" height="50" class="rounded-circle fr-fic fr-dii" src="/api/storage/images/people/9.jpg"> <strong class="ml-3">Mario Rossi</strong></p></div></div></div></div></section>';
        $templateEditorModulo_4->template_editor_group_id = $templateEditorGroupTestimonial->id;
        $templateEditorModulo_4->order = 40;
        $templateEditorModulo_4->created_id = $idUser;
        $templateEditorModulo_4->save();
        
$templateEditorModulo_5 = new TemplateEditor();
        $templateEditorModulo_5->description = 'Modulo 5';
        $templateEditorModulo_5->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/testimonial-05.png';
        $templateEditorModulo_5->html = '<section style="background-image: url(/api/storage/images/shapes/9.svg);padding: 7.5rem 0; position: relative; background-size: cover; background-position: center; background-repeat: no-repeat; background-color: #FFFFFF;"><div class="container"><div class="row text-center justify-content-center"><div class="col-md-10 col-lg-8 col-xl-7"><h1>Dicono di noi</h1><p class="lead">A small river named Duden flows by their place and supplies it with the necessary regelialia. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p></div></div><div class="row mt-5 align-items-center justify-content-center"><div class="col-md-8 col-lg-4"><div style="background: #FFFFFF; padding: 3.75rem 2.5rem; overflow: hidden; color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><div class="row no-gutters align-items-center"><div class="col-3"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/1.jpg"></div><div class="col-8 ml-auto"><p><strong>Mario Rossi</strong><br><em>Co-founder at Company</em></p></div></div><div class="row mt-4"><div class="col-12"><p class="lead">&quot;Even the all-powerful Pointing has no control about the blind texts it is an small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.&quot;</p></div></div></div></div><div class="col-md-8 col-lg-4 mt-4 mt-lg-0"><div style="background: #FFFFFF; padding: 3.75rem 2.5rem; overflow: hidden; color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><div class="row no-gutters align-items-center"><div class="col-3"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/3.jpg"></div><div class="col-8 ml-auto"><p><strong>Teresa Bianchi</strong><br><em>Co-founder at Company</em></p></div></div><div class="row mt-4"><div class="col-12"><p class="lead">&quot;Far far away, behind the word mountains, far from the countries Vokalia. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&quot;</p></div></div></div></div><div class="col-md-8 col-lg-4 mt-4 mt-lg-0"><div style="background: #FFFFFF; padding: 3.75rem 2.5rem; overflow: hidden; color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><div class="row no-gutters align-items-center"><div class="col-3"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/6.jpg"></div><div class="col-8 ml-auto"><p><strong>Lucia Verdi</strong><br><em>Co-founder at Company</em></p></div></div><div class="row mt-4"><div class="col-12"><p class="lead">&quot;Separated they live in Bookmarksgrove right at the coast of the Semantics, the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.&quot;</p></div></div></div></div></div></div></section>';
        $templateEditorModulo_5->template_editor_group_id = $templateEditorGroupTestimonial->id;
        $templateEditorModulo_5->order = 50;
        $templateEditorModulo_5->created_id = $idUser;
        $templateEditorModulo_5->save();
        
$templateEditorModulo_6 = new TemplateEditor();
        $templateEditorModulo_6->description = 'Modulo 6';
        $templateEditorModulo_6->img = 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/testimonial-06.png';
        $templateEditorModulo_6->html = '<section><div class="container"><div class="row text-center justify-content-center"><div class="col-md-10 col-lg-8 col-xl-7"><h1>Dicono di noi</h1><p class="lead">A small river named Duden flows by their place and supplies it with the necessary regelialia. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p></div></div><div class="row mt-5 justify-content-center"><div class="col-md-10 col-lg-3 ml-auto mr-auto text-center"><p class="h3 mb-4 mb-lg-5">&quot;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.&quot;</p><p><img alt="image" height="50" class="rounded-circle fr-fic fr-dii" src="/api/storage/images/people/6.jpg"></p><p class="lead"><strong>Claudia Bianchi</strong></p><p>Co-founder, Company</p></div><div class="col-md-10 col-lg-3 pt-5 pt-lg-0 ml-auto mr-auto text-center"><p class="h3 mb-4 mb-lg-5">&quot;Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&quot;</p><p><img alt="image" height="50" class="rounded-circle fr-fic fr-dii" src="/api/storage/images/people/3.jpg"></p><p class="lead"><strong>Teresa Rossi</strong></p><p>Co-founder, Company</p></div><div class="col-md-10 col-lg-3 pt-5 pt-lg-0 ml-auto mr-auto text-center"><p class="h3 mb-4 mb-lg-5">&quot;A small river named Duden flows by their place and supplies it with the necessary regelialia.&quot;</p><p><img alt="image" height="50" class="rounded-circle fr-fic fr-dii" src="/api/storage/images/people/9.jpg"></p><p class="lead"><strong>Mario Verdi</strong></p><p>Co-founder, Company</p></div></div></div></section>';
        $templateEditorModulo_6->template_editor_group_id = $templateEditorGroupTestimonial->id;
        $templateEditorModulo_6->order = 60;
        $templateEditorModulo_6->created_id = $idUser;
        $templateEditorModulo_6->save();
        
        #endregion TEMPLATE EDITOR
    }
}

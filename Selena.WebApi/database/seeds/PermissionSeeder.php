<?php

use App\Permission;
use App\Role;
use Illuminate\Database\Seeder;
use App\User;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pages = new Permission();
        $pages->name = 'pages';
        $pages->display_name = 'Edit pages';
        $pages->description = 'Edit pages admin role';
        $pages->created_id = User::where('name', 'admin')->first()->id;
        $pages->save();

        $admin = Role::where('name', 'admin')->first();
        $admin->attachPermission($pages);
    }
}

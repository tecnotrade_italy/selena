<?php

use App\MessageType;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Icon;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Tab;
use Illuminate\Database\Seeder;
use App\Language;
use App\Role;
use App\User;
use App\Functionality;

class MessagesTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idUser = User::first()->id;

        $messageType = new MessageType();
        $messageType->id = 1;
        $messageType->description = "Label";
        $messageType->created_id = $idUser;
        $messageType->save();

        $messageType = new MessageType();
        $messageType->id = 2;
        $messageType->description = "Message";
        $messageType->created_id = $idUser;
        $messageType->save();

        $messageType = new MessageType();
        $messageType->id = 3;
        $messageType->description = "Mail";
        $messageType->created_id = $idUser;
        $messageType->save();

    }
}

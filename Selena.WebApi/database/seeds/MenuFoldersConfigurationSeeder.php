<?php

use App\Enums\PageTypesEnum;
use Illuminate\Database\Seeder;

use App\Language;
use App\Role;
use App\User;
use App\Functionality;
use App\Tab;
use App\LanguageTab;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;

class MenuFoldersConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idFunctionality = Functionality::where('code', 'Pages')->first()->id;
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        $tabFolder = new Tab();
        $tabFolder->code = 'Folder';
        $tabFolder->functionality_id = $idFunctionality;
        $tabFolder->order = 30;
        $tabFolder->created_id = $idUser;
        $tabFolder->save();

        $languageTabFolder = new LanguageTab();
        $languageTabFolder->language_id = $idLanguage;
        $languageTabFolder->tab_id = $tabFolder->id;
        $languageTabFolder->description = 'Cartella';
        $languageTabFolder->created_id = $idUser;
        $languageTabFolder->save();

        #region TitlePage

        $fieldTitlePage = new Field();
        $fieldTitlePage->code = 'TitleFolder';
        $fieldTitlePage->tab_id = $tabFolder->id;
        $fieldTitlePage->field = 'languagesMenus.title';
        $fieldTitlePage->data_type = 1;
        $fieldTitlePage->required = true;
        $fieldTitlePage->max_length = 255;
        $fieldTitlePage->on_change = 'changeTitle';
        $fieldTitlePage->created_id = $idUser;
        $fieldTitlePage->save();

        $fieldLanguageTitlePage = new FieldLanguage();
        $fieldLanguageTitlePage->field_id = $fieldTitlePage->id;
        $fieldLanguageTitlePage->language_id = $idLanguage;
        $fieldLanguageTitlePage->description = 'Titolo';
        $fieldLanguageTitlePage->created_id = $idUser;
        $fieldLanguageTitlePage->save();

        $fieldUserRoleTitlePage = new FieldUserRole();
        $fieldUserRoleTitlePage->field_id = $fieldTitlePage->id;
        $fieldUserRoleTitlePage->role_id = $idRole;
        $fieldUserRoleTitlePage->enabled = true;
        $fieldUserRoleTitlePage->input_type = 0;
        $fieldUserRoleTitlePage->pos_x = 10;
        $fieldUserRoleTitlePage->pos_y = 10;
        $fieldUserRoleTitlePage->required = true;
        $fieldUserRoleTitlePage->colspan = 12;
        $fieldUserRoleTitlePage->created_id = $idUser;
        $fieldUserRoleTitlePage->save();

        #endregion TitlePage

        #region Icon

        $fieldIcon = new Field();
        $fieldIcon->code = 'IconFolder';
        $fieldIcon->tab_id = $tabFolder->id;
        $fieldIcon->field = 'idIcon';
        $fieldIcon->data_type = 0;
        $fieldIcon->service = 'icons';
        $fieldIcon->created_id = $idUser;
        $fieldIcon->save();

        $fieldLanguageIcon = new FieldLanguage();
        $fieldLanguageIcon->field_id = $fieldIcon->id;
        $fieldLanguageIcon->language_id = $idLanguage;
        $fieldLanguageIcon->description = 'Icona';
        $fieldLanguageIcon->created_id = $idUser;
        $fieldLanguageIcon->save();

        $fieldUserRoleIcon = new FieldUserRole();
        $fieldUserRoleIcon->field_id = $fieldIcon->id;
        $fieldUserRoleIcon->role_id = $idRole;
        $fieldUserRoleIcon->enabled = true;
        $fieldUserRoleIcon->input_type = 2;
        $fieldUserRoleIcon->pos_x = 10;
        $fieldUserRoleIcon->pos_y = 20;
        $fieldUserRoleIcon->colspan = 12;
        $fieldUserRoleIcon->created_id = $idUser;
        $fieldUserRoleIcon->save();

        #endregion Icon

        #region HideMobile

        $fieldHideMobile = new Field();
        $fieldHideMobile->code = 'HideMobileFolder';
        $fieldHideMobile->tab_id = $tabFolder->id;
        $fieldHideMobile->field = 'hideMobile';
        $fieldHideMobile->data_type = 3;
        $fieldHideMobile->default_value = true;
        $fieldHideMobile->created_id = $idUser;
        $fieldHideMobile->save();

        $fieldLanguageHideMobile = new FieldLanguage();
        $fieldLanguageHideMobile->field_id = $fieldHideMobile->id;
        $fieldLanguageHideMobile->language_id = $idLanguage;
        $fieldLanguageHideMobile->description = 'Nascondi da mobile';
        $fieldLanguageHideMobile->created_id = $idUser;
        $fieldLanguageHideMobile->save();

        $fieldUserRoleHideMobile = new FieldUserRole();
        $fieldUserRoleHideMobile->field_id = $fieldHideMobile->id;
        $fieldUserRoleHideMobile->role_id = $idRole;
        $fieldUserRoleHideMobile->enabled = true;
        $fieldUserRoleHideMobile->input_type = 5;
        $fieldUserRoleHideMobile->pos_x = 10;
        $fieldUserRoleHideMobile->pos_y = 30;
        $fieldUserRoleHideMobile->colspan = 12;
        $fieldUserRoleHideMobile->created_id = $idUser;
        $fieldUserRoleHideMobile->save();

        #endregion HideMobile

        #region IdLanguage

        $fieldIdLanguage = new Field();
        $fieldIdLanguage->code = 'IdLanguageFolder';
        $fieldIdLanguage->tab_id = $tabFolder->id;
        $fieldIdLanguage->field = 'languagesMenus.idLanguage';
        $fieldIdLanguage->data_type = 0;
        $fieldIdLanguage->created_id = $idUser;
        $fieldIdLanguage->save();

        $fieldLanguageIdLanguage = new FieldLanguage();
        $fieldLanguageIdLanguage->field_id = $fieldIdLanguage->id;
        $fieldLanguageIdLanguage->language_id = $idLanguage;
        $fieldLanguageIdLanguage->description = 'Lingua';
        $fieldLanguageIdLanguage->created_id = $idUser;
        $fieldLanguageIdLanguage->save();

        $fieldUserRoleIdLanguage = new FieldUserRole();
        $fieldUserRoleIdLanguage->field_id = $fieldIdLanguage->id;
        $fieldUserRoleIdLanguage->role_id = $idRole;
        $fieldUserRoleIdLanguage->enabled = true;
        $fieldUserRoleIdLanguage->input_type = 19;
        $fieldUserRoleIdLanguage->pos_x = 10;
        $fieldUserRoleIdLanguage->pos_y = 10;
        $fieldUserRoleIdLanguage->created_id = $idUser;
        $fieldUserRoleIdLanguage->save();

        #endregion IdLanguage

        $fieldPageType = new Field();
        $fieldPageType->code = 'PageType';
        $fieldPageType->tab_id = $tabFolder->id;
        $fieldPageType->field = 'pageType';
        $fieldPageType->default_value = PageTypesEnum::Page;
        $fieldPageType->data_type = 0;
        $fieldPageType->created_id = $idUser;
        $fieldPageType->save();

        $fieldLanguagePageType = new FieldLanguage();
        $fieldLanguagePageType->field_id = $fieldPageType->id;
        $fieldLanguagePageType->language_id = $idLanguage;
        $fieldLanguagePageType->description = 'Tipo pagina';
        $fieldLanguagePageType->created_id = $idUser;
        $fieldLanguagePageType->save();

        $fieldUserRolePageType = new FieldUserRole();
        $fieldUserRolePageType->field_id = $fieldPageType->id;
        $fieldUserRolePageType->role_id = $idRole;
        $fieldUserRolePageType->enabled = true;
        $fieldUserRolePageType->input_type = 12;
        $fieldUserRolePageType->pos_x = 10;
        $fieldUserRolePageType->pos_y = 10;
        $fieldUserRolePageType->created_id = $idUser;
        $fieldUserRolePageType->save();
    }
}

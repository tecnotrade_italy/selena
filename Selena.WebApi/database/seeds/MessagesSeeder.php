<?php

use App\Message;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Icon;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Tab;
use Illuminate\Database\Seeder;
use App\Language;
use App\Role;
use App\User;
use App\Functionality;

class MessagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $idMenuAdminSetting = MenuAdmin::where('code', 'Setting')->first()->id;

        #region VATTYPE

        #region ICON

        /*$iconIdCard = new Icon();
        $iconIdCard->description = 'Bookmark';
        $iconIdCard->css = 'fa fa-bookmark-o';
        $iconIdCard->created_id = $idUser;
        $iconIdCard->save();*/

        #endregion ICON

        #region FUNCTIONALITY

        $functionalityMessage = new Functionality();
        $functionalityMessage->code = 'Message';
        $functionalityMessage->created_id = $idUser;
        $functionalityMessage->save();

        #endregion FUNCTIONALITY

        #region MENU

        /*$menuAdminMessage = new MenuAdmin();
        $menuAdminMessage->code = 'Message';
        $menuAdminMessage->functionality_id = $functionalityMessage->id;
        $menuAdminMessage->icon_id = $iconIdCard->id;
        $menuAdminMessage->url = '/admin/setting/messages';
        $menuAdminMessage->created_id = $idUser;
        $menuAdminMessage->save();

        $languageMenuAdminMessage = new LanguageMenuAdmin();
        $languageMenuAdminMessage->language_id = $idLanguage;
        $languageMenuAdminMessage->menu_admin_id = $menuAdminMessage->id;
        $languageMenuAdminMessage->description = 'Messaggi';
        $languageMenuAdminMessage->created_id = $idUser;
        $languageMenuAdminMessage->save();

        $menuAdminUserRoleMessage = new MenuAdminUserRole();
        $menuAdminUserRoleMessage->menu_admin_id = $menuAdminMessage->id;
        $menuAdminUserRoleMessage->menu_admin_father_id = $idMenuAdminSetting;
        $menuAdminUserRoleMessage->role_id = $idRole;
        $menuAdminUserRoleMessage->enabled = true;
        $menuAdminUserRoleMessage->order = 30;
        $menuAdminUserRoleMessage->created_id = $idUser;
        $menuAdminUserRoleMessage->save();*/

        #endregion MENU

        #region TABLE

        #region TAB

        $tabMessageTable = new Tab();
        $tabMessageTable->code = 'Table';
        $tabMessageTable->functionality_id = $functionalityMessage->id;
        $tabMessageTable->order = 10;
        $tabMessageTable->created_id = $idUser;
        $tabMessageTable->save();

        $languageTabMessageTable = new LanguageTab();
        $languageTabMessageTable->language_id = $idLanguage;
        $languageTabMessageTable->tab_id = $tabMessageTable->id;
        $languageTabMessageTable->description = 'Table';
        $languageTabMessageTable->created_id = $idUser;
        $languageTabMessageTable->save();

        #endregion TAB

         #region CHECKBOX

         $fieldMessageCheckBox = new Field();
         $fieldMessageCheckBox->tab_id = $tabMessageTable->id;
         $fieldMessageCheckBox->code = 'TableCheckBox';
         $fieldMessageCheckBox->created_id = $idUser;
         $fieldMessageCheckBox->save();
 
         $fieldLanguageMessageCheckBox = new FieldLanguage();
         $fieldLanguageMessageCheckBox->field_id = $fieldMessageCheckBox->id;
         $fieldLanguageMessageCheckBox->language_id = $idLanguage;
         $fieldLanguageMessageCheckBox->description = 'CheckBox';
         $fieldLanguageMessageCheckBox->created_id = $idUser;
         $fieldLanguageMessageCheckBox->save();
 
         $fieldUserRoleMessageCheckBox = new FieldUserRole();
         $fieldUserRoleMessageCheckBox->field_id = $fieldMessageCheckBox->id;
         $fieldUserRoleMessageCheckBox->role_id = $idRole;
         $fieldUserRoleMessageCheckBox->enabled = true;
         $fieldUserRoleMessageCheckBox->table_order = 10;
         $fieldUserRoleMessageCheckBox->input_type = 13;
         $fieldUserRoleMessageCheckBox->created_id = $idUser;
         $fieldUserRoleMessageCheckBox->save();
 
         #endregion CHECKBOX
 
         #region FORMATTER
 
          $fieldMessageFormatter = new Field();
         $fieldMessageFormatter->code = 'MessageFormatter';
         $fieldMessageFormatter->formatter = 'MessageFormatter';
         $fieldMessageFormatter->tab_id = $tabMessageTable->id;
         $fieldMessageFormatter->created_id = $idUser;
         $fieldMessageFormatter->save();
 
         $fieldLanguageMessageFormatter = new FieldLanguage();
         $fieldLanguageMessageFormatter->field_id = $fieldMessageFormatter->id;
         $fieldLanguageMessageFormatter->language_id = $idLanguage;
         $fieldLanguageMessageFormatter->description = '';
         $fieldLanguageMessageFormatter->created_id = $idUser;
         $fieldLanguageMessageFormatter->save();
 
         $fieldUserRoleMessageFormatter = new FieldUserRole();
         $fieldUserRoleMessageFormatter->field_id = $fieldMessageFormatter->id;
         $fieldUserRoleMessageFormatter->role_id = $idRole;
         $fieldUserRoleMessageFormatter->enabled = true;
         $fieldUserRoleMessageFormatter->table_order = 20;
         $fieldUserRoleMessageFormatter->input_type = 14;
         $fieldUserRoleMessageFormatter->created_id = $idUser;
         $fieldUserRoleMessageFormatter->save();
 
         #endregion FORMATTER
 
         #region CODE
 
         $fieldMessageTable = new Field();
         $fieldMessageTable->code = 'code';
         $fieldMessageTable->tab_id = $tabMessageTable->id;
         $fieldMessageTable->field = 'code';
         $fieldMessageTable->data_type = 1;
         $fieldMessageTable->created_id = $idUser;
         $fieldMessageTable->save();
 
         $fieldLanguageMessageTable = new FieldLanguage();
         $fieldLanguageMessageTable->field_id = $fieldMessageTable->id;
         $fieldLanguageMessageTable->language_id = $idLanguage;
         $fieldLanguageMessageTable->description = 'Codice';
         $fieldLanguageMessageTable->created_id = $idUser;
         $fieldLanguageMessageTable->save();
 
         $fieldUserRoleMessageTable = new FieldUserRole();
         $fieldUserRoleMessageTable->field_id = $fieldMessageTable->id;
         $fieldUserRoleMessageTable->role_id = $idRole;
         $fieldUserRoleMessageTable->enabled = true;
         $fieldUserRoleMessageTable->table_order = 30;
         $fieldUserRoleMessageTable->input_type = 0;
         $fieldUserRoleMessageTable->created_id = $idUser;
         $fieldUserRoleMessageTable->save();
         
         #endregion ORDER

                 #region DESCRIPTION

        $fieldContactTableBusinessName = new Field();
        $fieldContactTableBusinessName->code = 'content';
        $fieldContactTableBusinessName->tab_id = $tabMessageTable->id;
        $fieldContactTableBusinessName->field = 'content';
        $fieldContactTableBusinessName->data_type = 1;
        $fieldContactTableBusinessName->created_id = $idUser;
        $fieldContactTableBusinessName->save();

        $fieldLanguageContactTableBusinessName = new FieldLanguage();
        $fieldLanguageContactTableBusinessName->field_id = $fieldMessageTable->id;
        $fieldLanguageContactTableBusinessName->language_id = $idLanguage;
        $fieldLanguageContactTableBusinessName->description = 'Testo';
        $fieldLanguageContactTableBusinessName->created_id = $idUser;
        $fieldLanguageContactTableBusinessName->save();

        $fieldUserRoleContactTableBusinessName = new FieldUserRole();
        $fieldUserRoleContactTableBusinessName->field_id = $fieldMessageTable->id;
        $fieldUserRoleContactTableBusinessName->role_id = $idRole;
        $fieldUserRoleContactTableBusinessName->enabled = true;
        $fieldUserRoleContactTableBusinessName->table_order = 40;
        $fieldUserRoleContactTableBusinessName->input_type = 0;
        $fieldUserRoleContactTableBusinessName->created_id = $idUser;
        $fieldUserRoleContactTableBusinessName->save();

        #endregion DESCRIPTION
        
        
         #region DETAIL

        #region TAB

        $tabMessageDetail = new Tab();
        $tabMessageDetail->code = 'Detail';
        $tabMessageDetail->functionality_id = $functionalityMessage->id;
        $tabMessageDetail->order = 20;
        $tabMessageDetail->created_id = $idUser;
        $tabMessageDetail->save();

        $languageTabMessageDetail = new LanguageTab();
        $languageTabMessageDetail->language_id = $idLanguage;
        $languageTabMessageDetail->tab_id = $tabMessageDetail->id;
        $languageTabMessageDetail->description = 'Detail';
        $languageTabMessageDetail->created_id = $idUser;
        $languageTabMessageDetail->save();

        #endregion TAB

        #region CODE

        $fieldMessageDetailCompany = new Field();
        $fieldMessageDetailCompany->code = 'code';
        $fieldMessageDetailCompany->tab_id = $tabMessageDetail->id;
        $fieldMessageDetailCompany->field = 'code';
        $fieldMessageDetailCompany->data_type = 1;
        $fieldMessageDetailCompany->created_id = $idUser;
        $fieldMessageDetailCompany->save();

        $fieldLanguageMessageDetailCompany = new FieldLanguage();
        $fieldLanguageMessageDetailCompany->field_id = $fieldMessageDetailCompany->id;
        $fieldLanguageMessageDetailCompany->language_id = $idLanguage;
        $fieldLanguageMessageDetailCompany->description = 'Codice';
        $fieldLanguageMessageDetailCompany->created_id = $idUser;
        $fieldLanguageMessageDetailCompany->save();

        $fieldUserRoleMessageDetailCompany = new FieldUserRole();
        $fieldUserRoleMessageDetailCompany->field_id = $fieldMessageDetailCompany->id;
        $fieldUserRoleMessageDetailCompany->role_id = $idRole;
        $fieldUserRoleMessageDetailCompany->enabled = true;
        $fieldUserRoleMessageDetailCompany->pos_x = 10;
        $fieldUserRoleMessageDetailCompany->pos_y = 10;
        $fieldUserRoleMessageDetailCompany->input_type = 0;
        $fieldUserRoleMessageDetailCompany->created_id = $idUser;
        $fieldUserRoleMessageDetailCompany->save();
        
        #endregion CODE

        #region CHARACTERISTIC

        $fieldMessageDetail = new Field();
        $fieldMessageDetail->code = 'content';
        $fieldMessageDetail->tab_id = $tabMessageDetail->id;
        $fieldMessageDetail->field = 'content';
        $fieldMessageDetail->data_type = 1;
        $fieldMessageDetail->created_id = $idUser;
        $fieldMessageDetail->save();

        $fieldLanguageMessageDetail = new FieldLanguage();
        $fieldLanguageMessageDetail->field_id = $fieldMessageDetail->id;
        $fieldLanguageMessageDetail->language_id = $idLanguage;
        $fieldLanguageMessageDetail->description = 'Testo';
        $fieldLanguageMessageDetail->created_id = $idUser;
        $fieldLanguageMessageDetail->save();

        $fieldUserRoleMessageDetail = new FieldUserRole();
        $fieldUserRoleMessageDetail->field_id = $fieldMessageDetail->id;
        $fieldUserRoleMessageDetail->role_id = $idRole;
        $fieldUserRoleMessageDetail->enabled = true;
        $fieldUserRoleMessageDetail->pos_x = 10;
        $fieldUserRoleMessageDetail->pos_y = 20;
        $fieldUserRoleMessageDetail->input_type = 17;
        $fieldUserRoleMessageDetail->created_id = $idUser;
        $fieldUserRoleMessageDetail->save();
        
        #endregion CHARACTERISTIC

    
        
        #region MESSAGE TYPE

        $fieldMessageDetailName = new Field();
        $fieldMessageDetailName->code = 'message_type_id';
        $fieldMessageDetailName->tab_id = $tabMessageDetail->id;
        $fieldMessageDetailName->field = 'message_type_id';
        $fieldMessageDetailName->data_type = 1;
        $fieldMessageDetailName->service = 'messagetype';
        $fieldMessageDetailName->created_id = $idUser;
        $fieldMessageDetailName->save();

        $fieldLanguageMessageDetailName = new FieldLanguage();
        $fieldLanguageMessageDetailName->field_id = $fieldMessageDetailName->id;
        $fieldLanguageMessageDetailName->language_id = $idLanguage;
        $fieldLanguageMessageDetailName->description = 'Tipo messaggio';
        $fieldLanguageMessageDetailName->created_id = $idUser;
        $fieldLanguageMessageDetailName->save();

        $fieldUserRoleMessageDetailName = new FieldUserRole();
        $fieldUserRoleMessageDetailName->field_id = $fieldMessageDetailName->id;
        $fieldUserRoleMessageDetailName->role_id = $idRole;
        $fieldUserRoleMessageDetailName->enabled = true;
        $fieldUserRoleMessageDetailName->pos_x = 10;
        $fieldUserRoleMessageDetailName->pos_y = 10;
        $fieldUserRoleMessageDetailName->input_type = 2;
        $fieldUserRoleMessageDetailName->created_id = $idUser;
        $fieldUserRoleMessageDetailName->save();

        #endregion MESSAGE TYPE

 

      
    }
}

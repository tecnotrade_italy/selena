<?php

use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class CustomerConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $idMenuAdminCRM = MenuAdmin::where('code', 'CRM')->first()->id;

        #region CUSTOMER

        #region ICON

        $iconIdCard = new Icon();
        $iconIdCard->description = 'IdCard';
        $iconIdCard->css = 'fa fa-id-card';
        $iconIdCard->created_id = $idUser;
        $iconIdCard->save();

        #endregion ICON

        #region FUNCTIONALITY

        $functionalityCustomer = new Functionality();
        $functionalityCustomer->code = 'Customer';
        $functionalityCustomer->created_id = $idUser;
        $functionalityCustomer->save();

        #endregion FUNCTIONALITY

        #region MENU

        $menuAdminCustomer = new MenuAdmin();
        $menuAdminCustomer->code = 'Customer';
        $menuAdminCustomer->functionality_id = $functionalityCustomer->id;
        $menuAdminCustomer->icon_id = $iconIdCard->id;
        $menuAdminCustomer->url = '/admin/crm/customer';
        $menuAdminCustomer->created_id = $idUser;
        $menuAdminCustomer->save();

        $languageMenuAdminCustomer = new LanguageMenuAdmin();
        $languageMenuAdminCustomer->language_id = $idLanguage;
        $languageMenuAdminCustomer->menu_admin_id = $menuAdminCustomer->id;
        $languageMenuAdminCustomer->description = 'Clienti';
        $languageMenuAdminCustomer->created_id = $idUser;
        $languageMenuAdminCustomer->save();

        $menuAdminUserRoleCustomer = new MenuAdminUserRole();
        $menuAdminUserRoleCustomer->menu_admin_id = $menuAdminCustomer->id;
        $menuAdminUserRoleCustomer->menu_admin_father_id = $idMenuAdminCRM;
        $menuAdminUserRoleCustomer->role_id = $idRole;
        $menuAdminUserRoleCustomer->enabled = true;
        $menuAdminUserRoleCustomer->order = 30;
        $menuAdminUserRoleCustomer->created_id = $idUser;
        $menuAdminUserRoleCustomer->save();

        #endregion MENU

        #region TABLE

        #region TAB

        $tabCustomerTable = new Tab();
        $tabCustomerTable->code = 'Table';
        $tabCustomerTable->functionality_id = $functionalityCustomer->id;
        $tabCustomerTable->order = 10;
        $tabCustomerTable->created_id = $idUser;
        $tabCustomerTable->save();

        $languageTabCustomerTable = new LanguageTab();
        $languageTabCustomerTable->language_id = $idLanguage;
        $languageTabCustomerTable->tab_id = $tabCustomerTable->id;
        $languageTabCustomerTable->description = 'Table';
        $languageTabCustomerTable->created_id = $idUser;
        $languageTabCustomerTable->save();

        #endregion TAB

        #region CHECKBOX

        $fieldCustomerCheckBox = new Field();
        $fieldCustomerCheckBox->tab_id = $tabCustomerTable->id;
        $fieldCustomerCheckBox->code = 'TableCheckBox';
        $fieldCustomerCheckBox->created_id = $idUser;
        $fieldCustomerCheckBox->save();

        $fieldLanguageCustomerCheckBox = new FieldLanguage();
        $fieldLanguageCustomerCheckBox->field_id = $fieldCustomerCheckBox->id;
        $fieldLanguageCustomerCheckBox->language_id = $idLanguage;
        $fieldLanguageCustomerCheckBox->description = 'CheckBox';
        $fieldLanguageCustomerCheckBox->created_id = $idUser;
        $fieldLanguageCustomerCheckBox->save();

        $fieldUserRoleCustomerCheckBox = new FieldUserRole();
        $fieldUserRoleCustomerCheckBox->field_id = $fieldCustomerCheckBox->id;
        $fieldUserRoleCustomerCheckBox->role_id = $idRole;
        $fieldUserRoleCustomerCheckBox->enabled = true;
        $fieldUserRoleCustomerCheckBox->table_order = 10;
        $fieldUserRoleCustomerCheckBox->input_type = 13;
        $fieldUserRoleCustomerCheckBox->created_id = $idUser;
        $fieldUserRoleCustomerCheckBox->save();

        #endregion CHECKBOX

        #region FORMATTER

        $fieldCustomerFormatter = new Field();
        $fieldCustomerFormatter->code = 'CustomerFormatter';
        $fieldCustomerFormatter->formatter = 'customerFormatter';
        $fieldCustomerFormatter->tab_id = $tabCustomerTable->id;
        $fieldCustomerFormatter->created_id = $idUser;
        $fieldCustomerFormatter->save();

        $fieldLanguageCustomerFormatter = new FieldLanguage();
        $fieldLanguageCustomerFormatter->field_id = $fieldCustomerFormatter->id;
        $fieldLanguageCustomerFormatter->language_id = $idLanguage;
        $fieldLanguageCustomerFormatter->description = '';
        $fieldLanguageCustomerFormatter->created_id = $idUser;
        $fieldLanguageCustomerFormatter->save();

        $fieldUserRoleCustomerFormatter = new FieldUserRole();
        $fieldUserRoleCustomerFormatter->field_id = $fieldCustomerFormatter->id;
        $fieldUserRoleCustomerFormatter->role_id = $idRole;
        $fieldUserRoleCustomerFormatter->enabled = true;
        $fieldUserRoleCustomerFormatter->table_order = 20;
        $fieldUserRoleCustomerFormatter->input_type = 14;
        $fieldUserRoleCustomerFormatter->created_id = $idUser;
        $fieldUserRoleCustomerFormatter->save();

        #endregion FORMATTER

        #region COMPANY

        $fieldCustomerTableCompany = new Field();
        $fieldCustomerTableCompany->code = 'Company';
        $fieldCustomerTableCompany->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableCompany->field = 'company';
        $fieldCustomerTableCompany->data_type = 3;
        $fieldCustomerTableCompany->created_id = $idUser;
        $fieldCustomerTableCompany->save();

        $fieldLanguageCustomerTableCompany = new FieldLanguage();
        $fieldLanguageCustomerTableCompany->field_id = $fieldCustomerTableCompany->id;
        $fieldLanguageCustomerTableCompany->language_id = $idLanguage;
        $fieldLanguageCustomerTableCompany->description = 'Ditta';
        $fieldLanguageCustomerTableCompany->created_id = $idUser;
        $fieldLanguageCustomerTableCompany->save();

        $fieldUserRoleCustomerTableCompany = new FieldUserRole();
        $fieldUserRoleCustomerTableCompany->field_id = $fieldCustomerTableCompany->id;
        $fieldUserRoleCustomerTableCompany->role_id = $idRole;
        $fieldUserRoleCustomerTableCompany->enabled = true;
        $fieldUserRoleCustomerTableCompany->table_order = 30;
        $fieldUserRoleCustomerTableCompany->input_type = 5;
        $fieldUserRoleCustomerTableCompany->created_id = $idUser;
        $fieldUserRoleCustomerTableCompany->save();

        #endregion COMPANY

        #region BUSINNES NAME

        $fieldCustomerTableBusinessName = new Field();
        $fieldCustomerTableBusinessName->code = 'BusinessName';
        $fieldCustomerTableBusinessName->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableBusinessName->field = 'businessName';
        $fieldCustomerTableBusinessName->data_type = 1;
        $fieldCustomerTableBusinessName->created_id = $idUser;
        $fieldCustomerTableBusinessName->save();

        $fieldLanguageCustomerTableBusinessName = new FieldLanguage();
        $fieldLanguageCustomerTableBusinessName->field_id = $fieldCustomerTableBusinessName->id;
        $fieldLanguageCustomerTableBusinessName->language_id = $idLanguage;
        $fieldLanguageCustomerTableBusinessName->description = 'Società';
        $fieldLanguageCustomerTableBusinessName->created_id = $idUser;
        $fieldLanguageCustomerTableBusinessName->save();

        $fieldUserRoleCustomerTableBusinessName = new FieldUserRole();
        $fieldUserRoleCustomerTableBusinessName->field_id = $fieldCustomerTableBusinessName->id;
        $fieldUserRoleCustomerTableBusinessName->role_id = $idRole;
        $fieldUserRoleCustomerTableBusinessName->enabled = true;
        $fieldUserRoleCustomerTableBusinessName->table_order = 40;
        $fieldUserRoleCustomerTableBusinessName->input_type = 0;
        $fieldUserRoleCustomerTableBusinessName->created_id = $idUser;
        $fieldUserRoleCustomerTableBusinessName->save();

        #endregion BUSINNES NAME

        #region NAME

        $fieldCustomerTableName = new Field();
        $fieldCustomerTableName->code = 'Name';
        $fieldCustomerTableName->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableName->field = 'name';
        $fieldCustomerTableName->data_type = 1;
        $fieldCustomerTableName->created_id = $idUser;
        $fieldCustomerTableName->save();

        $fieldLanguageCustomerTableName = new FieldLanguage();
        $fieldLanguageCustomerTableName->field_id = $fieldCustomerTableName->id;
        $fieldLanguageCustomerTableName->language_id = $idLanguage;
        $fieldLanguageCustomerTableName->description = 'Nome';
        $fieldLanguageCustomerTableName->created_id = $idUser;
        $fieldLanguageCustomerTableName->save();

        $fieldUserRoleCustomerTableName = new FieldUserRole();
        $fieldUserRoleCustomerTableName->field_id = $fieldCustomerTableName->id;
        $fieldUserRoleCustomerTableName->role_id = $idRole;
        $fieldUserRoleCustomerTableName->enabled = true;
        $fieldUserRoleCustomerTableName->table_order = 50;
        $fieldUserRoleCustomerTableName->input_type = 0;
        $fieldUserRoleCustomerTableName->created_id = $idUser;
        $fieldUserRoleCustomerTableName->save();

        #endregion NAME

        #region SURNAME

        $fieldCustomerTableSurname = new Field();
        $fieldCustomerTableSurname->code = 'Surname';
        $fieldCustomerTableSurname->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableSurname->field = 'surname';
        $fieldCustomerTableSurname->data_type = 1;
        $fieldCustomerTableSurname->created_id = $idUser;
        $fieldCustomerTableSurname->save();

        $fieldLanguageCustomerTableSurname = new FieldLanguage();
        $fieldLanguageCustomerTableSurname->field_id = $fieldCustomerTableSurname->id;
        $fieldLanguageCustomerTableSurname->language_id = $idLanguage;
        $fieldLanguageCustomerTableSurname->description = 'Cognome';
        $fieldLanguageCustomerTableSurname->created_id = $idUser;
        $fieldLanguageCustomerTableSurname->save();

        $fieldUserRoleCustomerTableSurname = new FieldUserRole();
        $fieldUserRoleCustomerTableSurname->field_id = $fieldCustomerTableSurname->id;
        $fieldUserRoleCustomerTableSurname->role_id = $idRole;
        $fieldUserRoleCustomerTableSurname->enabled = true;
        $fieldUserRoleCustomerTableSurname->table_order = 60;
        $fieldUserRoleCustomerTableSurname->input_type = 0;
        $fieldUserRoleCustomerTableSurname->created_id = $idUser;
        $fieldUserRoleCustomerTableSurname->save();

        #endregion SURNAME

        #region VAT NUMBER

        $fieldCustomerTableVatNumber = new Field();
        $fieldCustomerTableVatNumber->code = 'VatNumber';
        $fieldCustomerTableVatNumber->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableVatNumber->field = 'vatNumber';
        $fieldCustomerTableVatNumber->data_type = 1;
        $fieldCustomerTableVatNumber->created_id = $idUser;
        $fieldCustomerTableVatNumber->save();

        $fieldLanguageCustomerTableVatNumber = new FieldLanguage();
        $fieldLanguageCustomerTableVatNumber->field_id = $fieldCustomerTableVatNumber->id;
        $fieldLanguageCustomerTableVatNumber->language_id = $idLanguage;
        $fieldLanguageCustomerTableVatNumber->description = 'Partita IVA';
        $fieldLanguageCustomerTableVatNumber->created_id = $idUser;
        $fieldLanguageCustomerTableVatNumber->save();

        $fieldUserRoleCustomerTableVatNumber = new FieldUserRole();
        $fieldUserRoleCustomerTableVatNumber->field_id = $fieldCustomerTableVatNumber->id;
        $fieldUserRoleCustomerTableVatNumber->role_id = $idRole;
        $fieldUserRoleCustomerTableVatNumber->enabled = true;
        $fieldUserRoleCustomerTableVatNumber->table_order = 60;
        $fieldUserRoleCustomerTableVatNumber->input_type = 0;
        $fieldUserRoleCustomerTableVatNumber->created_id = $idUser;
        $fieldUserRoleCustomerTableVatNumber->save();

        #endregion VAT NUMBER

        #region FISCAL CODE

        $fieldCustomerTableFiscalCode = new Field();
        $fieldCustomerTableFiscalCode->code = 'FiscalCode';
        $fieldCustomerTableFiscalCode->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableFiscalCode->field = 'fiscalCode';
        $fieldCustomerTableFiscalCode->data_type = 1;
        $fieldCustomerTableFiscalCode->created_id = $idUser;
        $fieldCustomerTableFiscalCode->save();

        $fieldLanguageCustomerTableFiscalCode = new FieldLanguage();
        $fieldLanguageCustomerTableFiscalCode->field_id = $fieldCustomerTableFiscalCode->id;
        $fieldLanguageCustomerTableFiscalCode->language_id = $idLanguage;
        $fieldLanguageCustomerTableFiscalCode->description = 'Codice Fiscale';
        $fieldLanguageCustomerTableFiscalCode->created_id = $idUser;
        $fieldLanguageCustomerTableFiscalCode->save();

        $fieldUserRoleCustomerTableFiscalCode = new FieldUserRole();
        $fieldUserRoleCustomerTableFiscalCode->field_id = $fieldCustomerTableFiscalCode->id;
        $fieldUserRoleCustomerTableFiscalCode->role_id = $idRole;
        $fieldUserRoleCustomerTableFiscalCode->enabled = true;
        $fieldUserRoleCustomerTableFiscalCode->table_order = 70;
        $fieldUserRoleCustomerTableFiscalCode->input_type = 0;
        $fieldUserRoleCustomerTableFiscalCode->created_id = $idUser;
        $fieldUserRoleCustomerTableFiscalCode->save();

        #endregion FISCAL CODE

        #region ADDRESS

        $fieldCustomerTableAddress = new Field();
        $fieldCustomerTableAddress->code = 'Address';
        $fieldCustomerTableAddress->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableAddress->field = 'address';
        $fieldCustomerTableAddress->data_type = 1;
        $fieldCustomerTableAddress->created_id = $idUser;
        $fieldCustomerTableAddress->save();

        $fieldLanguageCustomerTableAddress = new FieldLanguage();
        $fieldLanguageCustomerTableAddress->field_id = $fieldCustomerTableAddress->id;
        $fieldLanguageCustomerTableAddress->language_id = $idLanguage;
        $fieldLanguageCustomerTableAddress->description = 'Indirizzo';
        $fieldLanguageCustomerTableAddress->created_id = $idUser;
        $fieldLanguageCustomerTableAddress->save();

        $fieldUserRoleCustomerTableAddress = new FieldUserRole();
        $fieldUserRoleCustomerTableAddress->field_id = $fieldCustomerTableAddress->id;
        $fieldUserRoleCustomerTableAddress->role_id = $idRole;
        $fieldUserRoleCustomerTableAddress->enabled = true;
        $fieldUserRoleCustomerTableAddress->table_order = 80;
        $fieldUserRoleCustomerTableAddress->input_type = 0;
        $fieldUserRoleCustomerTableAddress->created_id = $idUser;
        $fieldUserRoleCustomerTableAddress->save();

        #endregion ADDRESS

        #region POSTAL CODE

        $fieldCustomerTablePostalCode = new Field();
        $fieldCustomerTablePostalCode->code = 'PostalCode';
        $fieldCustomerTablePostalCode->tab_id = $tabCustomerTable->id;
        $fieldCustomerTablePostalCode->field = 'idPostalCode';
        $fieldCustomerTablePostalCode->data_type = 0;
        $fieldCustomerTablePostalCode->service = 'postalCode';
        $fieldCustomerTablePostalCode->created_id = $idUser;
        $fieldCustomerTablePostalCode->save();

        $fieldLanguageCustomerTablePostalCode = new FieldLanguage();
        $fieldLanguageCustomerTablePostalCode->field_id = $fieldCustomerTablePostalCode->id;
        $fieldLanguageCustomerTablePostalCode->language_id = $idLanguage;
        $fieldLanguageCustomerTablePostalCode->description = 'Codice postale';
        $fieldLanguageCustomerTablePostalCode->created_id = $idUser;
        $fieldLanguageCustomerTablePostalCode->save();

        $fieldUserRoleCustomerTablePostalCode = new FieldUserRole();
        $fieldUserRoleCustomerTablePostalCode->field_id = $fieldCustomerTablePostalCode->id;
        $fieldUserRoleCustomerTablePostalCode->role_id = $idRole;
        $fieldUserRoleCustomerTablePostalCode->enabled = true;
        $fieldUserRoleCustomerTablePostalCode->table_order = 90;
        $fieldUserRoleCustomerTablePostalCode->input_type = 2;
        $fieldUserRoleCustomerTablePostalCode->created_id = $idUser;
        $fieldUserRoleCustomerTablePostalCode->save();

        #endregion POSTAL CODE

        #region TELEPHONE NUMBER

        $fieldCustomerTableTelephoneNumber = new Field();
        $fieldCustomerTableTelephoneNumber->code = 'TelephoneNumber';
        $fieldCustomerTableTelephoneNumber->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableTelephoneNumber->field = 'telephoneNumber';
        $fieldCustomerTableTelephoneNumber->data_type = 1;
        $fieldCustomerTableTelephoneNumber->created_id = $idUser;
        $fieldCustomerTableTelephoneNumber->save();

        $fieldLanguageCustomerTableTelephoneNumber = new FieldLanguage();
        $fieldLanguageCustomerTableTelephoneNumber->field_id = $fieldCustomerTableTelephoneNumber->id;
        $fieldLanguageCustomerTableTelephoneNumber->language_id = $idLanguage;
        $fieldLanguageCustomerTableTelephoneNumber->description = 'Telefono';
        $fieldLanguageCustomerTableTelephoneNumber->created_id = $idUser;
        $fieldLanguageCustomerTableTelephoneNumber->save();

        $fieldUserRoleCustomerTableTelephoneNumber = new FieldUserRole();
        $fieldUserRoleCustomerTableTelephoneNumber->field_id = $fieldCustomerTableTelephoneNumber->id;
        $fieldUserRoleCustomerTableTelephoneNumber->role_id = $idRole;
        $fieldUserRoleCustomerTableTelephoneNumber->enabled = true;
        $fieldUserRoleCustomerTableTelephoneNumber->table_order = 100;
        $fieldUserRoleCustomerTableTelephoneNumber->input_type = 8;
        $fieldUserRoleCustomerTableTelephoneNumber->created_id = $idUser;
        $fieldUserRoleCustomerTableTelephoneNumber->save();

        #endregion TELEPHONE NUMBER

        #region FAX NUMBER

        $fieldCustomerTableFaxNumber = new Field();
        $fieldCustomerTableFaxNumber->code = 'FaxNumber';
        $fieldCustomerTableFaxNumber->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableFaxNumber->field = 'faxNumber';
        $fieldCustomerTableFaxNumber->data_type = 1;
        $fieldCustomerTableFaxNumber->created_id = $idUser;
        $fieldCustomerTableFaxNumber->save();

        $fieldLanguageCustomerTableFaxNumber = new FieldLanguage();
        $fieldLanguageCustomerTableFaxNumber->field_id = $fieldCustomerTableFaxNumber->id;
        $fieldLanguageCustomerTableFaxNumber->language_id = $idLanguage;
        $fieldLanguageCustomerTableFaxNumber->description = 'Fax';
        $fieldLanguageCustomerTableFaxNumber->created_id = $idUser;
        $fieldLanguageCustomerTableFaxNumber->save();

        $fieldUserRoleCustomerTableFaxNumber = new FieldUserRole();
        $fieldUserRoleCustomerTableFaxNumber->field_id = $fieldCustomerTableFaxNumber->id;
        $fieldUserRoleCustomerTableFaxNumber->role_id = $idRole;
        $fieldUserRoleCustomerTableFaxNumber->enabled = true;
        $fieldUserRoleCustomerTableFaxNumber->table_order = 110;
        $fieldUserRoleCustomerTableFaxNumber->input_type = 8;
        $fieldUserRoleCustomerTableFaxNumber->created_id = $idUser;
        $fieldUserRoleCustomerTableFaxNumber->save();

        #endregion FAX NUMBER

        #region EMAIL

        $fieldCustomerTableEmail = new Field();
        $fieldCustomerTableEmail->code = 'Email';
        $fieldCustomerTableEmail->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableEmail->field = 'email';
        $fieldCustomerTableEmail->data_type = 1;
        $fieldCustomerTableEmail->created_id = $idUser;
        $fieldCustomerTableEmail->save();

        $fieldLanguageCustomerTableEmail = new FieldLanguage();
        $fieldLanguageCustomerTableEmail->field_id = $fieldCustomerTableEmail->id;
        $fieldLanguageCustomerTableEmail->language_id = $idLanguage;
        $fieldLanguageCustomerTableEmail->description = 'Email';
        $fieldLanguageCustomerTableEmail->created_id = $idUser;
        $fieldLanguageCustomerTableEmail->save();

        $fieldUserRoleCustomerTableEmail = new FieldUserRole();
        $fieldUserRoleCustomerTableEmail->field_id = $fieldCustomerTableEmail->id;
        $fieldUserRoleCustomerTableEmail->role_id = $idRole;
        $fieldUserRoleCustomerTableEmail->enabled = true;
        $fieldUserRoleCustomerTableEmail->table_order = 120;
        $fieldUserRoleCustomerTableEmail->input_type = 9;
        $fieldUserRoleCustomerTableEmail->created_id = $idUser;
        $fieldUserRoleCustomerTableEmail->save();

        #endregion EMAIL

        #region WEBSITE

        $fieldCustomerTableWebsite = new Field();
        $fieldCustomerTableWebsite->code = 'Website';
        $fieldCustomerTableWebsite->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableWebsite->field = 'website';
        $fieldCustomerTableWebsite->data_type = 1;
        $fieldCustomerTableWebsite->created_id = $idUser;
        $fieldCustomerTableWebsite->save();

        $fieldLanguageCustomerTableWebsite = new FieldLanguage();
        $fieldLanguageCustomerTableWebsite->field_id = $fieldCustomerTableWebsite->id;
        $fieldLanguageCustomerTableWebsite->language_id = $idLanguage;
        $fieldLanguageCustomerTableWebsite->description = 'Sito';
        $fieldLanguageCustomerTableWebsite->created_id = $idUser;
        $fieldLanguageCustomerTableWebsite->save();

        $fieldUserRoleCustomerTableWebsite = new FieldUserRole();
        $fieldUserRoleCustomerTableWebsite->field_id = $fieldCustomerTableWebsite->id;
        $fieldUserRoleCustomerTableWebsite->role_id = $idRole;
        $fieldUserRoleCustomerTableWebsite->enabled = true;
        $fieldUserRoleCustomerTableWebsite->table_order = 130;
        $fieldUserRoleCustomerTableWebsite->input_type = 0;
        $fieldUserRoleCustomerTableWebsite->created_id = $idUser;
        $fieldUserRoleCustomerTableWebsite->save();

        #endregion WEBSITE

        #region LANGUAGE

        $fieldCustomerTableLanguage = new Field();
        $fieldCustomerTableLanguage->code = 'Language';
        $fieldCustomerTableLanguage->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableLanguage->field = 'idLanguage';
        $fieldCustomerTableLanguage->data_type = 0;
        $fieldCustomerTableLanguage->service = 'languages';
        $fieldCustomerTableLanguage->created_id = $idUser;
        $fieldCustomerTableLanguage->save();

        $fieldLanguageCustomerTableLanguage = new FieldLanguage();
        $fieldLanguageCustomerTableLanguage->field_id = $fieldCustomerTableLanguage->id;
        $fieldLanguageCustomerTableLanguage->language_id = $idLanguage;
        $fieldLanguageCustomerTableLanguage->description = 'Lingua';
        $fieldLanguageCustomerTableLanguage->created_id = $idUser;
        $fieldLanguageCustomerTableLanguage->save();

        $fieldUserRoleCustomerTableLanguage = new FieldUserRole();
        $fieldUserRoleCustomerTableLanguage->field_id = $fieldCustomerTableLanguage->id;
        $fieldUserRoleCustomerTableLanguage->role_id = $idRole;
        $fieldUserRoleCustomerTableLanguage->enabled = true;
        $fieldUserRoleCustomerTableLanguage->table_order = 140;
        $fieldUserRoleCustomerTableLanguage->input_type = 2;
        $fieldUserRoleCustomerTableLanguage->created_id = $idUser;
        $fieldUserRoleCustomerTableLanguage->save();

        #endregion LANGUAGE

        #region VAT TYPE

        $fieldCustomerTableVatType = new Field();
        $fieldCustomerTableVatType->code = 'VatType';
        $fieldCustomerTableVatType->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableVatType->field = 'idVatType';
        $fieldCustomerTableVatType->data_type = 0;
        $fieldCustomerTableVatType->service = 'vatType';
        $fieldCustomerTableVatType->created_id = $idUser;
        $fieldCustomerTableVatType->save();

        $fieldLanguageCustomerTableVatType = new FieldLanguage();
        $fieldLanguageCustomerTableVatType->field_id = $fieldCustomerTableVatType->id;
        $fieldLanguageCustomerTableVatType->language_id = $idLanguage;
        $fieldLanguageCustomerTableVatType->description = 'Tipo IVA';
        $fieldLanguageCustomerTableVatType->created_id = $idUser;
        $fieldLanguageCustomerTableVatType->save();

        $fieldUserRoleCustomerTableVatType = new FieldUserRole();
        $fieldUserRoleCustomerTableVatType->field_id = $fieldCustomerTableVatType->id;
        $fieldUserRoleCustomerTableVatType->role_id = $idRole;
        $fieldUserRoleCustomerTableVatType->enabled = true;
        $fieldUserRoleCustomerTableVatType->table_order = 150;
        $fieldUserRoleCustomerTableVatType->input_type = 2;
        $fieldUserRoleCustomerTableVatType->created_id = $idUser;
        $fieldUserRoleCustomerTableVatType->save();

        #endregion VAT TYPE

        #region ENABLE FROM

        $fieldCustomerTableEnableFrom = new Field();
        $fieldCustomerTableEnableFrom->code = 'EnableFrom';
        $fieldCustomerTableEnableFrom->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableEnableFrom->field = 'enableFrom';
        $fieldCustomerTableEnableFrom->data_type = 2;
        $fieldCustomerTableEnableFrom->created_id = $idUser;
        $fieldCustomerTableEnableFrom->save();

        $fieldLanguageCustomerTableEnableFrom = new FieldLanguage();
        $fieldLanguageCustomerTableEnableFrom->field_id = $fieldCustomerTableEnableFrom->id;
        $fieldLanguageCustomerTableEnableFrom->language_id = $idLanguage;
        $fieldLanguageCustomerTableEnableFrom->description = 'Valido dal';
        $fieldLanguageCustomerTableEnableFrom->created_id = $idUser;
        $fieldLanguageCustomerTableEnableFrom->save();

        $fieldUserRoleCustomerTableEnableFrom = new FieldUserRole();
        $fieldUserRoleCustomerTableEnableFrom->field_id = $fieldCustomerTableEnableFrom->id;
        $fieldUserRoleCustomerTableEnableFrom->role_id = $idRole;
        $fieldUserRoleCustomerTableEnableFrom->enabled = true;
        $fieldUserRoleCustomerTableEnableFrom->table_order = 160;
        $fieldUserRoleCustomerTableEnableFrom->input_type = 4;
        $fieldUserRoleCustomerTableEnableFrom->created_id = $idUser;
        $fieldUserRoleCustomerTableEnableFrom->save();

        #endregion ENABLE FROM

        #region ENABLE TO

        $fieldCustomerTableEnableTo = new Field();
        $fieldCustomerTableEnableTo->code = 'EnableTo';
        $fieldCustomerTableEnableTo->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableEnableTo->field = 'enableTo';
        $fieldCustomerTableEnableTo->data_type = 2;
        $fieldCustomerTableEnableTo->created_id = $idUser;
        $fieldCustomerTableEnableTo->save();

        $fieldLanguageCustomerTableEnableTo = new FieldLanguage();
        $fieldLanguageCustomerTableEnableTo->field_id = $fieldCustomerTableEnableTo->id;
        $fieldLanguageCustomerTableEnableTo->language_id = $idLanguage;
        $fieldLanguageCustomerTableEnableTo->description = 'Valido al';
        $fieldLanguageCustomerTableEnableTo->created_id = $idUser;
        $fieldLanguageCustomerTableEnableTo->save();

        $fieldUserRoleCustomerTableEnableTo = new FieldUserRole();
        $fieldUserRoleCustomerTableEnableTo->field_id = $fieldCustomerTableEnableTo->id;
        $fieldUserRoleCustomerTableEnableTo->role_id = $idRole;
        $fieldUserRoleCustomerTableEnableTo->enabled = true;
        $fieldUserRoleCustomerTableEnableTo->table_order = 170;
        $fieldUserRoleCustomerTableEnableTo->input_type = 4;
        $fieldUserRoleCustomerTableEnableTo->created_id = $idUser;
        $fieldUserRoleCustomerTableEnableTo->save();

        #endregion ENABLE TO

        #region SEND NEWSLETTER

        $fieldCustomerTableSendNewsletter = new Field();
        $fieldCustomerTableSendNewsletter->code = 'SendNewsletter';
        $fieldCustomerTableSendNewsletter->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableSendNewsletter->field = 'sendNewsletter';
        $fieldCustomerTableSendNewsletter->data_type = 3;
        $fieldCustomerTableSendNewsletter->created_id = $idUser;
        $fieldCustomerTableSendNewsletter->save();

        $fieldLanguageCustomerTableSendNewsletter = new FieldLanguage();
        $fieldLanguageCustomerTableSendNewsletter->field_id = $fieldCustomerTableSendNewsletter->id;
        $fieldLanguageCustomerTableSendNewsletter->language_id = $idLanguage;
        $fieldLanguageCustomerTableSendNewsletter->description = 'Invia newsletter';
        $fieldLanguageCustomerTableSendNewsletter->created_id = $idUser;
        $fieldLanguageCustomerTableSendNewsletter->save();

        $fieldUserRoleCustomerTableSendNewsletter = new FieldUserRole();
        $fieldUserRoleCustomerTableSendNewsletter->field_id = $fieldCustomerTableSendNewsletter->id;
        $fieldUserRoleCustomerTableSendNewsletter->role_id = $idRole;
        $fieldUserRoleCustomerTableSendNewsletter->enabled = true;
        $fieldUserRoleCustomerTableSendNewsletter->table_order = 180;
        $fieldUserRoleCustomerTableSendNewsletter->input_type = 5;
        $fieldUserRoleCustomerTableSendNewsletter->created_id = $idUser;
        $fieldUserRoleCustomerTableSendNewsletter->save();

        #endregion SEND NEWSLETTER

        #region ERROR NEWSLETTER

        $fieldCustomerTableErrorNewsletter = new Field();
        $fieldCustomerTableErrorNewsletter->code = 'ErrorNewsletter';
        $fieldCustomerTableErrorNewsletter->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableErrorNewsletter->field = 'errorNewsletter';
        $fieldCustomerTableErrorNewsletter->data_type = 3;
        $fieldCustomerTableErrorNewsletter->created_id = $idUser;
        $fieldCustomerTableErrorNewsletter->save();

        $fieldLanguageCustomerTableErrorNewsletter = new FieldLanguage();
        $fieldLanguageCustomerTableErrorNewsletter->field_id = $fieldCustomerTableErrorNewsletter->id;
        $fieldLanguageCustomerTableErrorNewsletter->language_id = $idLanguage;
        $fieldLanguageCustomerTableErrorNewsletter->description = 'Errore newsletter';
        $fieldLanguageCustomerTableErrorNewsletter->created_id = $idUser;
        $fieldLanguageCustomerTableErrorNewsletter->save();

        $fieldUserRoleCustomerTableErrorNewsletter = new FieldUserRole();
        $fieldUserRoleCustomerTableErrorNewsletter->field_id = $fieldCustomerTableErrorNewsletter->id;
        $fieldUserRoleCustomerTableErrorNewsletter->role_id = $idRole;
        $fieldUserRoleCustomerTableErrorNewsletter->enabled = true;
        $fieldUserRoleCustomerTableErrorNewsletter->table_order = 190;
        $fieldUserRoleCustomerTableErrorNewsletter->input_type = 5;
        $fieldUserRoleCustomerTableErrorNewsletter->created_id = $idUser;
        $fieldUserRoleCustomerTableErrorNewsletter->save();

        #endregion ERROR NEWSLETTER

        #endregion TABLE















        #region DETAIL

        #region TAB

        $tabCustomerDetail = new Tab();
        $tabCustomerDetail->code = 'Detail';
        $tabCustomerDetail->functionality_id = $functionalityCustomer->id;
        $tabCustomerDetail->order = 20;
        $tabCustomerDetail->created_id = $idUser;
        $tabCustomerDetail->save();

        $languageTabCustomerDetail = new LanguageTab();
        $languageTabCustomerDetail->language_id = $idLanguage;
        $languageTabCustomerDetail->tab_id = $tabCustomerDetail->id;
        $languageTabCustomerDetail->description = 'Detail';
        $languageTabCustomerDetail->created_id = $idUser;
        $languageTabCustomerDetail->save();

        #endregion TAB

        #region COMPANY

        $fieldCustomerDetailCompany = new Field();
        $fieldCustomerDetailCompany->code = 'Company';
        $fieldCustomerDetailCompany->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailCompany->field = 'company';
        $fieldCustomerDetailCompany->data_type = 3;
        $fieldCustomerDetailCompany->created_id = $idUser;
        $fieldCustomerDetailCompany->save();

        $fieldLanguageCustomerDetailCompany = new FieldLanguage();
        $fieldLanguageCustomerDetailCompany->field_id = $fieldCustomerDetailCompany->id;
        $fieldLanguageCustomerDetailCompany->language_id = $idLanguage;
        $fieldLanguageCustomerDetailCompany->description = 'Ditta';
        $fieldLanguageCustomerDetailCompany->created_id = $idUser;
        $fieldLanguageCustomerDetailCompany->save();

        $fieldUserRoleCustomerDetailCompany = new FieldUserRole();
        $fieldUserRoleCustomerDetailCompany->field_id = $fieldCustomerDetailCompany->id;
        $fieldUserRoleCustomerDetailCompany->role_id = $idRole;
        $fieldUserRoleCustomerDetailCompany->enabled = true;
        $fieldUserRoleCustomerDetailCompany->pos_x = 10;
        $fieldUserRoleCustomerDetailCompany->pos_y = 10;
        $fieldUserRoleCustomerDetailCompany->input_type = 5;
        $fieldUserRoleCustomerDetailCompany->created_id = $idUser;
        $fieldUserRoleCustomerDetailCompany->save();

        #endregion COMPANY

        #region BUSINNES NAME

        $fieldCustomerDetailBusinessName = new Field();
        $fieldCustomerDetailBusinessName->code = 'BusinessName';
        $fieldCustomerDetailBusinessName->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailBusinessName->field = 'businessName';
        $fieldCustomerDetailBusinessName->data_type = 1;
        $fieldCustomerDetailBusinessName->created_id = $idUser;
        $fieldCustomerDetailBusinessName->save();

        $fieldLanguageCustomerDetailBusinessName = new FieldLanguage();
        $fieldLanguageCustomerDetailBusinessName->field_id = $fieldCustomerDetailBusinessName->id;
        $fieldLanguageCustomerDetailBusinessName->language_id = $idLanguage;
        $fieldLanguageCustomerDetailBusinessName->description = 'Società';
        $fieldLanguageCustomerDetailBusinessName->created_id = $idUser;
        $fieldLanguageCustomerDetailBusinessName->save();

        $fieldUserRoleCustomerDetailBusinessName = new FieldUserRole();
        $fieldUserRoleCustomerDetailBusinessName->field_id = $fieldCustomerDetailBusinessName->id;
        $fieldUserRoleCustomerDetailBusinessName->role_id = $idRole;
        $fieldUserRoleCustomerDetailBusinessName->enabled = true;
        $fieldUserRoleCustomerDetailBusinessName->pos_x = 20;
        $fieldUserRoleCustomerDetailBusinessName->pos_y = 10;
        $fieldUserRoleCustomerDetailBusinessName->input_type = 0;
        $fieldUserRoleCustomerDetailBusinessName->created_id = $idUser;
        $fieldUserRoleCustomerDetailBusinessName->save();

        #endregion BUSINNES NAME

        #region NAME

        $fieldCustomerDetailName = new Field();
        $fieldCustomerDetailName->code = 'Name';
        $fieldCustomerDetailName->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailName->field = 'name';
        $fieldCustomerDetailName->data_type = 1;
        $fieldCustomerDetailName->created_id = $idUser;
        $fieldCustomerDetailName->save();

        $fieldLanguageCustomerDetailName = new FieldLanguage();
        $fieldLanguageCustomerDetailName->field_id = $fieldCustomerDetailName->id;
        $fieldLanguageCustomerDetailName->language_id = $idLanguage;
        $fieldLanguageCustomerDetailName->description = 'Nome';
        $fieldLanguageCustomerDetailName->created_id = $idUser;
        $fieldLanguageCustomerDetailName->save();

        $fieldUserRoleCustomerDetailName = new FieldUserRole();
        $fieldUserRoleCustomerDetailName->field_id = $fieldCustomerDetailName->id;
        $fieldUserRoleCustomerDetailName->role_id = $idRole;
        $fieldUserRoleCustomerDetailName->enabled = true;
        $fieldUserRoleCustomerDetailName->pos_x = 30;
        $fieldUserRoleCustomerDetailName->pos_y = 10;
        $fieldUserRoleCustomerDetailName->input_type = 0;
        $fieldUserRoleCustomerDetailName->created_id = $idUser;
        $fieldUserRoleCustomerDetailName->save();

        #endregion NAME

        #region SURNAME

        $fieldCustomerDetailSurname = new Field();
        $fieldCustomerDetailSurname->code = 'Surname';
        $fieldCustomerDetailSurname->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailSurname->field = 'surname';
        $fieldCustomerDetailSurname->data_type = 1;
        $fieldCustomerDetailSurname->created_id = $idUser;
        $fieldCustomerDetailSurname->save();

        $fieldLanguageCustomerDetailSurname = new FieldLanguage();
        $fieldLanguageCustomerDetailSurname->field_id = $fieldCustomerDetailSurname->id;
        $fieldLanguageCustomerDetailSurname->language_id = $idLanguage;
        $fieldLanguageCustomerDetailSurname->description = 'Cognome';
        $fieldLanguageCustomerDetailSurname->created_id = $idUser;
        $fieldLanguageCustomerDetailSurname->save();

        $fieldUserRoleCustomerDetailSurname = new FieldUserRole();
        $fieldUserRoleCustomerDetailSurname->field_id = $fieldCustomerDetailSurname->id;
        $fieldUserRoleCustomerDetailSurname->role_id = $idRole;
        $fieldUserRoleCustomerDetailSurname->enabled = true;
        $fieldUserRoleCustomerDetailSurname->pos_x = 40;
        $fieldUserRoleCustomerDetailSurname->pos_y = 10;
        $fieldUserRoleCustomerDetailSurname->input_type = 0;
        $fieldUserRoleCustomerDetailSurname->created_id = $idUser;
        $fieldUserRoleCustomerDetailSurname->save();

        #endregion SURNAME

        #region VAT NUMBER

        $fieldCustomerDetailVatNumber = new Field();
        $fieldCustomerDetailVatNumber->code = 'VatNumber';
        $fieldCustomerDetailVatNumber->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailVatNumber->field = 'vatNumber';
        $fieldCustomerDetailVatNumber->data_type = 1;
        $fieldCustomerDetailVatNumber->created_id = $idUser;
        $fieldCustomerDetailVatNumber->save();

        $fieldLanguageCustomerDetailVatNumber = new FieldLanguage();
        $fieldLanguageCustomerDetailVatNumber->field_id = $fieldCustomerDetailVatNumber->id;
        $fieldLanguageCustomerDetailVatNumber->language_id = $idLanguage;
        $fieldLanguageCustomerDetailVatNumber->description = 'Partita IVA';
        $fieldLanguageCustomerDetailVatNumber->created_id = $idUser;
        $fieldLanguageCustomerDetailVatNumber->save();

        $fieldUserRoleCustomerDetailVatNumber = new FieldUserRole();
        $fieldUserRoleCustomerDetailVatNumber->field_id = $fieldCustomerDetailVatNumber->id;
        $fieldUserRoleCustomerDetailVatNumber->role_id = $idRole;
        $fieldUserRoleCustomerDetailVatNumber->enabled = true;
        $fieldUserRoleCustomerDetailVatNumber->pos_x = 10;
        $fieldUserRoleCustomerDetailVatNumber->pos_y = 20;
        $fieldUserRoleCustomerDetailVatNumber->input_type = 0;
        $fieldUserRoleCustomerDetailVatNumber->created_id = $idUser;
        $fieldUserRoleCustomerDetailVatNumber->save();

        #endregion VAT NUMBER

        #region FISCAL CODE

        $fieldCustomerDetailFiscalCode = new Field();
        $fieldCustomerDetailFiscalCode->code = 'FiscalCode';
        $fieldCustomerDetailFiscalCode->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailFiscalCode->field = 'fiscalCode';
        $fieldCustomerDetailFiscalCode->data_type = 1;
        $fieldCustomerDetailFiscalCode->created_id = $idUser;
        $fieldCustomerDetailFiscalCode->save();

        $fieldLanguageCustomerDetailFiscalCode = new FieldLanguage();
        $fieldLanguageCustomerDetailFiscalCode->field_id = $fieldCustomerDetailFiscalCode->id;
        $fieldLanguageCustomerDetailFiscalCode->language_id = $idLanguage;
        $fieldLanguageCustomerDetailFiscalCode->description = 'Codice Fiscale';
        $fieldLanguageCustomerDetailFiscalCode->created_id = $idUser;
        $fieldLanguageCustomerDetailFiscalCode->save();

        $fieldUserRoleCustomerDetailFiscalCode = new FieldUserRole();
        $fieldUserRoleCustomerDetailFiscalCode->field_id = $fieldCustomerDetailFiscalCode->id;
        $fieldUserRoleCustomerDetailFiscalCode->role_id = $idRole;
        $fieldUserRoleCustomerDetailFiscalCode->enabled = true;
        $fieldUserRoleCustomerDetailFiscalCode->pos_x = 20;
        $fieldUserRoleCustomerDetailFiscalCode->pos_y = 20;
        $fieldUserRoleCustomerDetailFiscalCode->input_type = 0;
        $fieldUserRoleCustomerDetailFiscalCode->created_id = $idUser;
        $fieldUserRoleCustomerDetailFiscalCode->save();

        #endregion FISCAL CODE

        #region ADDRESS

        $fieldCustomerDetailAddress = new Field();
        $fieldCustomerDetailAddress->code = 'Address';
        $fieldCustomerDetailAddress->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailAddress->field = 'address';
        $fieldCustomerDetailAddress->data_type = 1;
        $fieldCustomerDetailAddress->created_id = $idUser;
        $fieldCustomerDetailAddress->save();

        $fieldLanguageCustomerDetailAddress = new FieldLanguage();
        $fieldLanguageCustomerDetailAddress->field_id = $fieldCustomerDetailAddress->id;
        $fieldLanguageCustomerDetailAddress->language_id = $idLanguage;
        $fieldLanguageCustomerDetailAddress->description = 'Indirizzo';
        $fieldLanguageCustomerDetailAddress->created_id = $idUser;
        $fieldLanguageCustomerDetailAddress->save();

        $fieldUserRoleCustomerDetailAddress = new FieldUserRole();
        $fieldUserRoleCustomerDetailAddress->field_id = $fieldCustomerDetailAddress->id;
        $fieldUserRoleCustomerDetailAddress->role_id = $idRole;
        $fieldUserRoleCustomerDetailAddress->enabled = true;
        $fieldUserRoleCustomerDetailAddress->pos_x = 30;
        $fieldUserRoleCustomerDetailAddress->pos_y = 20;
        $fieldUserRoleCustomerDetailAddress->input_type = 0;
        $fieldUserRoleCustomerDetailAddress->created_id = $idUser;
        $fieldUserRoleCustomerDetailAddress->save();

        #endregion ADDRESS

        #region POSTAL CODE

        $fieldCustomerDetailPostalCode = new Field();
        $fieldCustomerDetailPostalCode->code = 'PostalCode';
        $fieldCustomerDetailPostalCode->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailPostalCode->field = 'idPostalCode';
        $fieldCustomerDetailPostalCode->data_type = 0;
        $fieldCustomerDetailPostalCode->service = 'postalCode';
        $fieldCustomerDetailPostalCode->created_id = $idUser;
        $fieldCustomerDetailPostalCode->save();

        $fieldLanguageCustomerDetailPostalCode = new FieldLanguage();
        $fieldLanguageCustomerDetailPostalCode->field_id = $fieldCustomerDetailPostalCode->id;
        $fieldLanguageCustomerDetailPostalCode->language_id = $idLanguage;
        $fieldLanguageCustomerDetailPostalCode->description = 'Codice postale';
        $fieldLanguageCustomerDetailPostalCode->created_id = $idUser;
        $fieldLanguageCustomerDetailPostalCode->save();

        $fieldUserRoleCustomerDetailPostalCode = new FieldUserRole();
        $fieldUserRoleCustomerDetailPostalCode->field_id = $fieldCustomerDetailPostalCode->id;
        $fieldUserRoleCustomerDetailPostalCode->role_id = $idRole;
        $fieldUserRoleCustomerDetailPostalCode->enabled = true;
        $fieldUserRoleCustomerDetailPostalCode->pos_x = 40;
        $fieldUserRoleCustomerDetailPostalCode->pos_y = 20;
        $fieldUserRoleCustomerDetailPostalCode->input_type = 2;
        $fieldUserRoleCustomerDetailPostalCode->created_id = $idUser;
        $fieldUserRoleCustomerDetailPostalCode->save();

        #endregion POSTAL CODE

        #region TELEPHONE NUMBER

        $fieldCustomerDetailTelephoneNumber = new Field();
        $fieldCustomerDetailTelephoneNumber->code = 'TelephoneNumber';
        $fieldCustomerDetailTelephoneNumber->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailTelephoneNumber->field = 'telephoneNumber';
        $fieldCustomerDetailTelephoneNumber->data_type = 1;
        $fieldCustomerDetailTelephoneNumber->created_id = $idUser;
        $fieldCustomerDetailTelephoneNumber->save();

        $fieldLanguageCustomerDetailTelephoneNumber = new FieldLanguage();
        $fieldLanguageCustomerDetailTelephoneNumber->field_id = $fieldCustomerDetailTelephoneNumber->id;
        $fieldLanguageCustomerDetailTelephoneNumber->language_id = $idLanguage;
        $fieldLanguageCustomerDetailTelephoneNumber->description = 'Telefono';
        $fieldLanguageCustomerDetailTelephoneNumber->created_id = $idUser;
        $fieldLanguageCustomerDetailTelephoneNumber->save();

        $fieldUserRoleCustomerDetailTelephoneNumber = new FieldUserRole();
        $fieldUserRoleCustomerDetailTelephoneNumber->field_id = $fieldCustomerDetailTelephoneNumber->id;
        $fieldUserRoleCustomerDetailTelephoneNumber->role_id = $idRole;
        $fieldUserRoleCustomerDetailTelephoneNumber->enabled = true;
        $fieldUserRoleCustomerDetailTelephoneNumber->pos_x = 10;
        $fieldUserRoleCustomerDetailTelephoneNumber->pos_y = 30;
        $fieldUserRoleCustomerDetailTelephoneNumber->input_type = 8;
        $fieldUserRoleCustomerDetailTelephoneNumber->created_id = $idUser;
        $fieldUserRoleCustomerDetailTelephoneNumber->save();

        #endregion TELEPHONE NUMBER

        #region FAX NUMBER

        $fieldCustomerDetailFaxNumber = new Field();
        $fieldCustomerDetailFaxNumber->code = 'FaxNumber';
        $fieldCustomerDetailFaxNumber->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailFaxNumber->field = 'faxNumber';
        $fieldCustomerDetailFaxNumber->data_type = 1;
        $fieldCustomerDetailFaxNumber->created_id = $idUser;
        $fieldCustomerDetailFaxNumber->save();

        $fieldLanguageCustomerDetailFaxNumber = new FieldLanguage();
        $fieldLanguageCustomerDetailFaxNumber->field_id = $fieldCustomerDetailFaxNumber->id;
        $fieldLanguageCustomerDetailFaxNumber->language_id = $idLanguage;
        $fieldLanguageCustomerDetailFaxNumber->description = 'Fax';
        $fieldLanguageCustomerDetailFaxNumber->created_id = $idUser;
        $fieldLanguageCustomerDetailFaxNumber->save();

        $fieldUserRoleCustomerDetailFaxNumber = new FieldUserRole();
        $fieldUserRoleCustomerDetailFaxNumber->field_id = $fieldCustomerDetailFaxNumber->id;
        $fieldUserRoleCustomerDetailFaxNumber->role_id = $idRole;
        $fieldUserRoleCustomerDetailFaxNumber->enabled = true;
        $fieldUserRoleCustomerDetailFaxNumber->pos_x = 20;
        $fieldUserRoleCustomerDetailFaxNumber->pos_y = 30;
        $fieldUserRoleCustomerDetailFaxNumber->input_type = 8;
        $fieldUserRoleCustomerDetailFaxNumber->created_id = $idUser;
        $fieldUserRoleCustomerDetailFaxNumber->save();

        #endregion FAX NUMBER

        #region EMAIL

        $fieldCustomerDetailEmail = new Field();
        $fieldCustomerDetailEmail->code = 'Email';
        $fieldCustomerDetailEmail->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailEmail->field = 'email';
        $fieldCustomerDetailEmail->data_type = 1;
        $fieldCustomerDetailEmail->created_id = $idUser;
        $fieldCustomerDetailEmail->save();

        $fieldLanguageCustomerDetailEmail = new FieldLanguage();
        $fieldLanguageCustomerDetailEmail->field_id = $fieldCustomerDetailEmail->id;
        $fieldLanguageCustomerDetailEmail->language_id = $idLanguage;
        $fieldLanguageCustomerDetailEmail->description = 'Email';
        $fieldLanguageCustomerDetailEmail->created_id = $idUser;
        $fieldLanguageCustomerDetailEmail->save();

        $fieldUserRoleCustomerDetailEmail = new FieldUserRole();
        $fieldUserRoleCustomerDetailEmail->field_id = $fieldCustomerDetailEmail->id;
        $fieldUserRoleCustomerDetailEmail->role_id = $idRole;
        $fieldUserRoleCustomerDetailEmail->enabled = true;
        $fieldUserRoleCustomerDetailEmail->pos_x = 30;
        $fieldUserRoleCustomerDetailEmail->pos_y = 30;
        $fieldUserRoleCustomerDetailEmail->input_type = 9;
        $fieldUserRoleCustomerDetailEmail->created_id = $idUser;
        $fieldUserRoleCustomerDetailEmail->save();

        #endregion EMAIL

        #region WEBSITE

        $fieldCustomerDetailWebsite = new Field();
        $fieldCustomerDetailWebsite->code = 'Website';
        $fieldCustomerDetailWebsite->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailWebsite->field = 'website';
        $fieldCustomerDetailWebsite->data_type = 1;
        $fieldCustomerDetailWebsite->created_id = $idUser;
        $fieldCustomerDetailWebsite->save();

        $fieldLanguageCustomerDetailWebsite = new FieldLanguage();
        $fieldLanguageCustomerDetailWebsite->field_id = $fieldCustomerDetailWebsite->id;
        $fieldLanguageCustomerDetailWebsite->language_id = $idLanguage;
        $fieldLanguageCustomerDetailWebsite->description = 'Sito';
        $fieldLanguageCustomerDetailWebsite->created_id = $idUser;
        $fieldLanguageCustomerDetailWebsite->save();

        $fieldUserRoleCustomerDetailWebsite = new FieldUserRole();
        $fieldUserRoleCustomerDetailWebsite->field_id = $fieldCustomerDetailWebsite->id;
        $fieldUserRoleCustomerDetailWebsite->role_id = $idRole;
        $fieldUserRoleCustomerDetailWebsite->enabled = true;
        $fieldUserRoleCustomerDetailWebsite->pos_x = 40;
        $fieldUserRoleCustomerDetailWebsite->pos_y = 30;
        $fieldUserRoleCustomerDetailWebsite->input_type = 0;
        $fieldUserRoleCustomerDetailWebsite->created_id = $idUser;
        $fieldUserRoleCustomerDetailWebsite->save();

        #endregion WEBSITE

        #region LANGUAGE

        $fieldCustomerDetailLanguage = new Field();
        $fieldCustomerDetailLanguage->code = 'Language';
        $fieldCustomerDetailLanguage->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailLanguage->field = 'idLanguage';
        $fieldCustomerDetailLanguage->data_type = 0;
        $fieldCustomerDetailLanguage->service = 'languages';
        $fieldCustomerDetailLanguage->created_id = $idUser;
        $fieldCustomerDetailLanguage->save();

        $fieldLanguageCustomerDetailLanguage = new FieldLanguage();
        $fieldLanguageCustomerDetailLanguage->field_id = $fieldCustomerDetailLanguage->id;
        $fieldLanguageCustomerDetailLanguage->language_id = $idLanguage;
        $fieldLanguageCustomerDetailLanguage->description = 'Lingua';
        $fieldLanguageCustomerDetailLanguage->created_id = $idUser;
        $fieldLanguageCustomerDetailLanguage->save();

        $fieldUserRoleCustomerDetailLanguage = new FieldUserRole();
        $fieldUserRoleCustomerDetailLanguage->field_id = $fieldCustomerDetailLanguage->id;
        $fieldUserRoleCustomerDetailLanguage->role_id = $idRole;
        $fieldUserRoleCustomerDetailLanguage->enabled = true;
        $fieldUserRoleCustomerDetailLanguage->pos_x = 10;
        $fieldUserRoleCustomerDetailLanguage->pos_y = 40;
        $fieldUserRoleCustomerDetailLanguage->input_type = 2;
        $fieldUserRoleCustomerDetailLanguage->created_id = $idUser;
        $fieldUserRoleCustomerDetailLanguage->save();

        #endregion LANGUAGE

        #region VAT TYPE

        $fieldCustomerDetailVatType = new Field();
        $fieldCustomerDetailVatType->code = 'VatType';
        $fieldCustomerDetailVatType->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailVatType->field = 'idVatType';
        $fieldCustomerDetailVatType->data_type = 0;
        $fieldCustomerDetailLanguage->service = 'vatType';
        $fieldCustomerDetailVatType->created_id = $idUser;
        $fieldCustomerDetailVatType->save();

        $fieldLanguageCustomerDetailVatType = new FieldLanguage();
        $fieldLanguageCustomerDetailVatType->field_id = $fieldCustomerDetailVatType->id;
        $fieldLanguageCustomerDetailVatType->language_id = $idLanguage;
        $fieldLanguageCustomerDetailVatType->description = 'Tipo IVA';
        $fieldLanguageCustomerDetailVatType->created_id = $idUser;
        $fieldLanguageCustomerDetailVatType->save();

        $fieldUserRoleCustomerDetailVatType = new FieldUserRole();
        $fieldUserRoleCustomerDetailVatType->field_id = $fieldCustomerDetailVatType->id;
        $fieldUserRoleCustomerDetailVatType->role_id = $idRole;
        $fieldUserRoleCustomerDetailVatType->enabled = true;
        $fieldUserRoleCustomerDetailVatType->pos_x = 20;
        $fieldUserRoleCustomerDetailVatType->pos_y = 40;
        $fieldUserRoleCustomerDetailVatType->input_type = 2;
        $fieldUserRoleCustomerDetailVatType->created_id = $idUser;
        $fieldUserRoleCustomerDetailVatType->save();

        #endregion VAT TYPE

        #region ENABLE FROM

        $fieldCustomerDetailEnableFrom = new Field();
        $fieldCustomerDetailEnableFrom->code = 'EnableFrom';
        $fieldCustomerDetailEnableFrom->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailEnableFrom->field = 'enableFrom';
        $fieldCustomerDetailEnableFrom->data_type = 2;
        $fieldCustomerDetailEnableFrom->created_id = $idUser;
        $fieldCustomerDetailEnableFrom->save();

        $fieldLanguageCustomerDetailEnableFrom = new FieldLanguage();
        $fieldLanguageCustomerDetailEnableFrom->field_id = $fieldCustomerDetailEnableFrom->id;
        $fieldLanguageCustomerDetailEnableFrom->language_id = $idLanguage;
        $fieldLanguageCustomerDetailEnableFrom->description = 'Valido dal';
        $fieldLanguageCustomerDetailEnableFrom->created_id = $idUser;
        $fieldLanguageCustomerDetailEnableFrom->save();

        $fieldUserRoleCustomerDetailEnableFrom = new FieldUserRole();
        $fieldUserRoleCustomerDetailEnableFrom->field_id = $fieldCustomerDetailEnableFrom->id;
        $fieldUserRoleCustomerDetailEnableFrom->role_id = $idRole;
        $fieldUserRoleCustomerDetailEnableFrom->enabled = true;
        $fieldUserRoleCustomerDetailEnableFrom->pos_x = 30;
        $fieldUserRoleCustomerDetailEnableFrom->pos_y = 40;
        $fieldUserRoleCustomerDetailEnableFrom->input_type = 4;
        $fieldUserRoleCustomerDetailEnableFrom->created_id = $idUser;
        $fieldUserRoleCustomerDetailEnableFrom->save();

        #endregion ENABLE FROM

        #region ENABLE TO

        $fieldCustomerDetailEnableTo = new Field();
        $fieldCustomerDetailEnableTo->code = 'EnableTo';
        $fieldCustomerDetailEnableTo->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailEnableTo->field = 'enableTo';
        $fieldCustomerDetailEnableTo->data_type = 2;
        $fieldCustomerDetailEnableTo->created_id = $idUser;
        $fieldCustomerDetailEnableTo->save();

        $fieldLanguageCustomerDetailEnableTo = new FieldLanguage();
        $fieldLanguageCustomerDetailEnableTo->field_id = $fieldCustomerDetailEnableTo->id;
        $fieldLanguageCustomerDetailEnableTo->language_id = $idLanguage;
        $fieldLanguageCustomerDetailEnableTo->description = 'Valido al';
        $fieldLanguageCustomerDetailEnableTo->created_id = $idUser;
        $fieldLanguageCustomerDetailEnableTo->save();

        $fieldUserRoleCustomerDetailEnableTo = new FieldUserRole();
        $fieldUserRoleCustomerDetailEnableTo->field_id = $fieldCustomerDetailEnableTo->id;
        $fieldUserRoleCustomerDetailEnableTo->role_id = $idRole;
        $fieldUserRoleCustomerDetailEnableTo->enabled = true;
        $fieldUserRoleCustomerDetailEnableTo->pos_x = 40;
        $fieldUserRoleCustomerDetailEnableTo->pos_y = 40;
        $fieldUserRoleCustomerDetailEnableTo->input_type = 4;
        $fieldUserRoleCustomerDetailEnableTo->created_id = $idUser;
        $fieldUserRoleCustomerDetailEnableTo->save();

        #endregion ENABLE TO

        #region SEND NEWSLETTER

        $fieldCustomerDetailSendNewsletter = new Field();
        $fieldCustomerDetailSendNewsletter->code = 'SendNewsletter';
        $fieldCustomerDetailSendNewsletter->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailSendNewsletter->field = 'sendNewsletter';
        $fieldCustomerDetailSendNewsletter->data_type = 3;
        $fieldCustomerDetailSendNewsletter->created_id = $idUser;
        $fieldCustomerDetailSendNewsletter->save();

        $fieldLanguageCustomerDetailSendNewsletter = new FieldLanguage();
        $fieldLanguageCustomerDetailSendNewsletter->field_id = $fieldCustomerDetailSendNewsletter->id;
        $fieldLanguageCustomerDetailSendNewsletter->language_id = $idLanguage;
        $fieldLanguageCustomerDetailSendNewsletter->description = 'Invia newsletter';
        $fieldLanguageCustomerDetailSendNewsletter->created_id = $idUser;
        $fieldLanguageCustomerDetailSendNewsletter->save();

        $fieldUserRoleCustomerDetailSendNewsletter = new FieldUserRole();
        $fieldUserRoleCustomerDetailSendNewsletter->field_id = $fieldCustomerDetailSendNewsletter->id;
        $fieldUserRoleCustomerDetailSendNewsletter->role_id = $idRole;
        $fieldUserRoleCustomerDetailSendNewsletter->enabled = true;
        $fieldUserRoleCustomerDetailSendNewsletter->pos_x = 10;
        $fieldUserRoleCustomerDetailSendNewsletter->pos_y = 50;
        $fieldUserRoleCustomerDetailSendNewsletter->input_type = 5;
        $fieldUserRoleCustomerDetailSendNewsletter->created_id = $idUser;
        $fieldUserRoleCustomerDetailSendNewsletter->save();

        #endregion SEND NEWSLETTER

        #region ERROR NEWSLETTER

        $fieldCustomerDetailErrorNewsletter = new Field();
        $fieldCustomerDetailErrorNewsletter->code = 'ErrorNewsletter';
        $fieldCustomerDetailErrorNewsletter->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailErrorNewsletter->field = 'errorNewsletter';
        $fieldCustomerDetailErrorNewsletter->data_type = 3;
        $fieldCustomerDetailErrorNewsletter->created_id = $idUser;
        $fieldCustomerDetailErrorNewsletter->save();

        $fieldLanguageCustomerDetailErrorNewsletter = new FieldLanguage();
        $fieldLanguageCustomerDetailErrorNewsletter->field_id = $fieldCustomerDetailErrorNewsletter->id;
        $fieldLanguageCustomerDetailErrorNewsletter->language_id = $idLanguage;
        $fieldLanguageCustomerDetailErrorNewsletter->description = 'Errore newsletter';
        $fieldLanguageCustomerDetailErrorNewsletter->created_id = $idUser;
        $fieldLanguageCustomerDetailErrorNewsletter->save();

        $fieldUserRoleCustomerDetailErrorNewsletter = new FieldUserRole();
        $fieldUserRoleCustomerDetailErrorNewsletter->field_id = $fieldCustomerDetailErrorNewsletter->id;
        $fieldUserRoleCustomerDetailErrorNewsletter->role_id = $idRole;
        $fieldUserRoleCustomerDetailErrorNewsletter->enabled = true;
        $fieldUserRoleCustomerDetailErrorNewsletter->pos_x = 20;
        $fieldUserRoleCustomerDetailErrorNewsletter->pos_y = 50;
        $fieldUserRoleCustomerDetailErrorNewsletter->input_type = 5;
        $fieldUserRoleCustomerDetailErrorNewsletter->created_id = $idUser;
        $fieldUserRoleCustomerDetailErrorNewsletter->save();

        #endregion ERROR NEWSLETTER

        #endregion DETAIL

        #endregion CUSTOMER
    }
}

<?php

use Illuminate\Database\Seeder;
use App\User;

class DeleteDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages_sixten')->delete();
        DB::table('documents_rows_languages')->delete();
        DB::table('documents_rows')->delete();
        DB::table('documents_heads_notes')->delete();
        DB::table('documents_heads_sixten')->delete();
        DB::table('documents_heads')->delete();
        DB::table('customers_sixten')->delete();
        DB::table('customers_groups_newsletters')->delete();
        DB::table('contacts_groups_newsletters')->delete();
        DB::table('feedback_newsletters')->delete();
        DB::table('newsletters_recipients')->delete();
        DB::table('customers')->delete();
        DB::table('contacts_requests')->delete();
        DB::table('contacts')->delete();
        DB::table('carriages_languages')->delete();
        DB::table('carriages')->delete();
        DB::table('carriers_languages')->delete();
        DB::table('carriers')->delete();
        DB::table('categories_languages')->delete();
        DB::table('categories')->delete();
        DB::table('currencies_languages')->delete();
        DB::table('currencies')->delete();
        DB::table('dictionaries')->delete();
        DB::table('districts_languages')->delete();
        DB::table('districts')->delete();
        DB::table('groups_display_technical_specification_tech_spec')->delete();
        DB::table('groups_display_technicals_specifications_languages')->delete();
        DB::table('groups_display_technicals_specifications')->delete();
        DB::table('items_groups_technicals_specifications')->delete();
        DB::table('groups_technicals_specifications_languages')->delete();
        DB::table('groups_technicals_specifications_technicals_specifications')->delete();
        DB::table('groups_technicals_specifications')->delete();
        DB::table('items_technicals_specifications_revisions')->delete();
        DB::table('items_technicals_specifications')->delete();
        DB::table('items_languages')->delete();
        DB::table('items')->delete();
        DB::table('documents_rows_types_languages')->delete();
        DB::table('documents_rows_types')->delete();
        DB::table('documents_statuses_languages')->delete();
        DB::table('documents_statuses')->delete();
        DB::table('documents_types_languages')->delete();
        DB::table('documents_types')->delete();
        DB::table('languages_menus')->delete();
        DB::table('menus')->delete();
        DB::table('languages_menu_admins')->delete();
        DB::table('menu_admins_users_roles')->delete();
        DB::table('menu_admins')->delete();
        DB::table('languages_nations')->delete();
        DB::table('nations')->delete();
        DB::table('languages_page_share_types')->delete();
        DB::table('languages_payments_types')->delete();
        DB::table('payments_types')->delete();
        DB::table('languages_provinces')->delete();
        DB::table('provinces')->delete();
        DB::table('languages_sales_types')->delete();
        DB::table('sales_types')->delete();
        DB::table('languages_technicals_specifications')->delete();
        DB::table('technicals_specifications')->delete();
        DB::table('languages_units_of_measures')->delete();
        DB::table('units_of_measures')->delete();
        DB::table('languages_vat_types')->delete();
        DB::table('vat_types')->delete();
        DB::table('settings')->delete();
        DB::table('languages_pages')->delete();
        DB::table('pages')->delete();
        DB::table('page_share_types')->delete();
        DB::table('languages_page_categories')->delete();
        DB::table('page_categories')->delete();
        DB::table('fields_languages')->delete();
        DB::table('fields_users_roles')->delete();
        DB::table('fields')->delete();
        DB::table('languages_tabs')->delete();
        DB::table('tabs')->delete();
        DB::table('functionalities')->delete();
        DB::table('icons')->delete();
        DB::table('permissions')->delete();
        DB::table('groups_newsletters')->delete();
        DB::table('roles')->delete();
        DB::statement('TRUNCATE languages CASCADE');
        DB::table('users')->delete();
    }
}

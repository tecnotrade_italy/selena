   <?php

    use Illuminate\Database\Seeder;
    use Carbon\Carbon;
    use App\MenuAdmin;
    use App\Permission;
    use App\LanguageMenuAdmin;
    use App\Enums\PageTypesEnum;
    use App\MenuAdminUserRole;
    use App\Language;
    use App\Icon;
    use App\Functionality;
    use App\Field;
    use App\FieldLanguage;
    use App\FieldUserRole;
    use App\Role;
    use App\User;
    use App\Helpers\LogHelper;
    use App\Tab;
    use App\LanguageTab;
    use App\Setting;
    use App\Content;
    use App\ContentLanguage;
    use Illuminate\Support\Facades\DB;
    use App\TemplateEditorType;
    use App\TemplateEditorGroup;
    use App\TemplateEditor;
    use App\DocumentTypeLanguage;
    use App\DocumentType;
    use App\DocumentRowTypeLanguage;
    use App\DocumentRowType;
    use App\Enums\ContentEnum;
    use App\Enums\DocumentRowTypeEnum;
    use App\Enums\DocumentTypeEnum;


    class NewsletterNewAggSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            
            $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
            $idRole = Role::where('name', '=', 'admin')->first()->id;
            $idUser = User::where('name', '=', 'admin')->first()->id;


            $tabDetailNewsletterCampaign = Tab::updateOrCreate(
                [
                    'functionality_id' => Functionality::where('code', 'Newsletter')->first()->id,
                    'code' => 'DetailHtml'
                ],
                [
                    'order' => 20,
                    'created_id' => $idUser
                ]
            );
            $tabsDetailNewsletterCampaign = Tab::where('functionality_id', Functionality::where('code', 'Newsletter')->first()->id)->where('code', 'DetailHtml')->first();
            $LanguageTabs = LanguageTab::updateOrCreate(
                ['tab_id' => $tabDetailNewsletterCampaign->id],
                [
                    'language_id' => $idLanguage,
                    'description' => 'DetailHtml',
                    'created_id' => $idUser
                ]
            );


            $fieldDetailHtmlNewsletterCampaign = Field::updateOrCreate(
                [
                    'tab_id' => $tabDetailNewsletterCampaign->id,
                    'code' => 'html'
                ],
                [
                    'field' => 'html',
                    'data_type' => '1',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldDetailHtmlNewsletterCampaign->id, 'language_id' => $idLanguage],
                [
                    'created_id' => $idUser
                ]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldDetailHtmlNewsletterCampaign->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '17',
                    'pos_x' => '10',
                    'pos_y' => '10',
                    'created_id' => $idUser
                ]
            );

            $tabTableNewsletters = Tab::updateOrCreate(
                [
                    'functionality_id' => Functionality::where('code', 'Newsletter')->first()->id,
                    'code' => 'Table'
                ],
                [
                    'order' => 10,
                    'created_id' => $idUser
                ]
            );
            $tabsTableNewsletters = Tab::where('functionality_id', Functionality::where('code', 'Newsletter')->first()->id)->where('code', 'Table')->first();
            $LanguageTabs = LanguageTab::updateOrCreate(
                ['tab_id' => $tabTableNewsletters->id],
                [
                    'language_id' => $idLanguage,
                    'description' => 'Tabella',
                    'created_id' => $idUser
                ]
            );


            $fieldTableNewsletterCheckBox = Field::updateOrCreate(
                [
                    'tab_id' => $tabTableNewsletters->id,
                    'code' => 'TableCheckBox'
                ],
                [
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldTableNewsletterCheckBox->id, 'language_id' => $idLanguage],
                [
                    'description' => 'CheckBox',
                    'created_id' => $idUser
                ]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldTableNewsletterCheckBox->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '13',
                    'table_order' => '10',
                    'created_id' => $idUser
                ]
            );

            $fieldTableNewsletterFormatter = Field::updateOrCreate(
                [
                    'tab_id' => $tabTableNewsletters->id,
                    'code' => 'NewsletterFormatter'
                ],
                [
                    'formatter' => 'newsletterFormatter',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldTableNewsletterFormatter->id, 'language_id' => $idLanguage],
                [
                    'description' => 'NewsletterFormatter',
                    'created_id' => $idUser
                ]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldTableNewsletterFormatter->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '14',
                    'table_order' => '20',
                    'created_id' => $idUser
                ]
            );


            $fieldtitleTableNewsletterCampaign = Field::updateOrCreate(
                [
                    'tab_id' => $tabTableNewsletters->id,
                    'code' => 'Title'
                ],
                [
                    'field' => 'title',
                    'data_type' => '1',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldtitleTableNewsletterCampaign->id, 'language_id' => $idLanguage],
                [
                    'description' => 'Titolo',
                    'created_id' => $idUser
                ]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldtitleTableNewsletterCampaign->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '1',
                    'table_order' => '30',
                    'created_id' => $idUser
                ]
            );


            $fieldschedule_date_timeTableNewsletterCampaign = Field::updateOrCreate(
                [
                    'tab_id' => $tabTableNewsletters->id,
                    'code' => 'schedule_date_time'
                ],
                [
                    'field' => 'schedule_date_time',
                    'data_type' => '1',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldschedule_date_timeTableNewsletterCampaign->id, 'language_id' => $idLanguage],
                [
                    'description' => 'Data',
                    'created_id' => $idUser
                ]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldschedule_date_timeTableNewsletterCampaign->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '1',
                    'table_order' => '40',
                    'created_id' => $idUser
                ]
            );

            $fieldsenderTableNewsletterCampaign = Field::updateOrCreate(
                [
                    'tab_id' => $tabTableNewsletters->id,
                    'code' => 'sender'
                ],
                [
                    'field' => 'sender',
                    'data_type' => '1',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldsenderTableNewsletterCampaign->id, 'language_id' => $idLanguage],
                [
                    'description' => 'Mittente',
                    'created_id' => $idUser
                ]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldsenderTableNewsletterCampaign->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '1',
                    'table_order' => '50',
                    'created_id' => $idUser
                ]
            );

            $fieldsender_emailTableNewsletterCampaign = Field::updateOrCreate(
                [
                    'tab_id' => $tabTableNewsletters->id,
                    'code' => 'sender_email'
                ],
                [
                    'field' => 'sender_email',
                    'data_type' => '3',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldsender_emailTableNewsletterCampaign->id, 'language_id' => $idLanguage],
                [
                    'description' => 'Email Mittente',
                    'created_id' => $idUser
                ]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldsender_emailTableNewsletterCampaign->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '15',
                    'table_order' => '60',
                    'created_id' => $idUser
                ]
            );

            $fieldobjectTableNewsletterCampaign = Field::updateOrCreate(
                [
                    'tab_id' => $tabTableNewsletters->id,
                    'code' => 'object'
                ],
                [
                    'field' => 'object',
                    'data_type' => '2',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldobjectTableNewsletterCampaign->id, 'language_id' => $idLanguage],
                [
                    'description' => 'Oggetto',
                    'created_id' => $idUser
                ]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldobjectTableNewsletterCampaign->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '15',
                    'table_order' => '70',
                    'created_id' => $idUser
                ]
            );

            $fieldpreview_objectTableNewsletterCampaign = Field::updateOrCreate(
                [
                    'tab_id' => $tabTableNewsletters->id,
                    'code' => 'preview_object'
                ],
                [
                    'field' => 'preview_object',
                    'data_type' => '2',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldpreview_objectTableNewsletterCampaign->id, 'language_id' => $idLanguage],
                [
                    'description' => 'Anteprima',
                    'created_id' => $idUser
                ]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldpreview_objectTableNewsletterCampaign->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '15',
                    'table_order' => '80',
                    'created_id' => $idUser
                ]
            );


            $fieldstart_send_date_timeTableNewsletterCampaign = Field::updateOrCreate(
                [
                    'tab_id' => $tabTableNewsletters->id,
                    'code' => 'start_send_date_time'
                ],
                [
                    'field' => 'start_send_date_time',
                    'data_type' => '1',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldstart_send_date_timeTableNewsletterCampaign->id, 'language_id' => $idLanguage],
                [
                    'description' => 'Data Inizio spedizione',
                    'created_id' => $idUser
                ]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldstart_send_date_timeTableNewsletterCampaign->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '1',
                    'table_order' => '90',
                    'created_id' => $idUser
                ]
            );

            $fieldend_send_date_timeTableNewsletterCampaign = Field::updateOrCreate(
                [
                    'tab_id' => $tabTableNewsletters->id,
                    'code' => 'end_send_date_time'
                ],
                [
                    'field' => 'end_send_date_time',
                    'data_type' => '1',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldend_send_date_timeTableNewsletterCampaign->id, 'language_id' => $idLanguage],
                [
                    'description' => 'Data Fine Spedizione',
                    'created_id' => $idUser
                ]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldend_send_date_timeTableNewsletterCampaign->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '1',
                    'table_order' => '100',
                    'created_id' => $idUser
                ]
            );


            $tabstableQueryGroupNewsletter = Tab::updateOrCreate(
                [
                    'functionality_id' => Functionality::where('code', 'QueryGroupNewsletter')->first()->id,
                    'code' => 'Table'
                ],
                [
                    'order' => 10,
                    'created_id' => $idUser
                ]
            );

            $tabtableQueryGroupNewsletter = Tab::where('functionality_id', Functionality::where('code', 'QueryGroupNewsletter')->first()->id)->where('code', 'Table')->first();
            $LanguageTabs = LanguageTab::updateOrCreate(
                ['tab_id' => $tabstableQueryGroupNewsletter->id],
                [
                    'language_id' => $idLanguage,
                    'description' => 'Table',
                    'created_id' => $idUser
                ]
            );

            $fieldTableCheckBoxQueryGroupNewsletter = Field::updateOrCreate(
                [
                    'tab_id' => $tabstableQueryGroupNewsletter->id,
                    'code' => 'TableCheckBox'
                ],
                [
                    'field' => 'TableCheckBox',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldTableCheckBoxQueryGroupNewsletter->id, 'language_id' => $idLanguage],
                ['description' => 'CheckBox', 'created_id' => $idUser]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldTableCheckBoxQueryGroupNewsletter->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '13',
                    'table_order' => '10',
                    'created_id' => $idUser
                ]
            );

            $fieldTableFormatterQueryGroupNewsletter = Field::updateOrCreate(
                [
                    'tab_id' => $tabstableQueryGroupNewsletter->id,
                    'code' => 'QueryGroupNewsletterFormatter'
                ],
                [
                    'field' => 'QueryGroupNewsletterFormatter',
                    'formatter' => 'QueryGroupNewsletterFormatter',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldTableFormatterQueryGroupNewsletter->id, 'language_id' => $idLanguage],
                ['description' => '', 'created_id' => $idUser]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldTableFormatterQueryGroupNewsletter->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '14',
                    'table_order' => '20',
                    'created_id' => $idUser
                ]
            );

            $fieldNameQueryGroupNewsletter = Field::updateOrCreate(
                [
                    'tab_id' => $tabstableQueryGroupNewsletter->id,
                    'code' => 'NameView'
                ],
                [
                    'data_type' => '1',
                    'field' => 'nameView',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldNameQueryGroupNewsletter->id, 'language_id' => $idLanguage],
                ['description' => 'Nome View', 'created_id' => $idUser]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldNameQueryGroupNewsletter->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '0',
                    'table_order' => '30',
                    'created_id' => $idUser
                ]
            );

            $fieldDescriptionQueryGroupNewsletter = Field::updateOrCreate(
                [
                    'tab_id' => $tabstableQueryGroupNewsletter->id,
                    'code' => 'Description'
                ],
                [
                    'data_type' => '1',
                    'field' => 'description',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldDescriptionQueryGroupNewsletter->id, 'language_id' => $idLanguage],
                ['description' => 'Decrizione view', 'created_id' => $idUser]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldDescriptionQueryGroupNewsletter->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '0',
                    'table_order' => '40',
                    'created_id' => $idUser
                ]
            );


            $tabsDetailQueryGroupNewsletter = Tab::updateOrCreate(
                [
                    'functionality_id' => Functionality::where('code', 'QueryGroupNewsletter')->first()->id,
                    'code' => 'detailQueryGroupNewsl'
                ],
                [
                    'order' => 20,
                    'created_id' => $idUser
                ]
            );

            $tabDetailQueryGroupNewsletter = Tab::where('functionality_id', Functionality::where('code', 'QueryGroupNewsletter')->first()->id)->where('code', 'detailQueryGroupNewsl')->first();
            $LanguageTabs = LanguageTab::updateOrCreate(
                ['tab_id' => $tabsDetailQueryGroupNewsletter->id],
                [
                    'language_id' => $idLanguage,
                    'description' => 'detailQueryGroupNewsl',
                    'created_id' => $idUser
                ]
            );

            $fieldDetailNameQueryGroupNewsletter = Field::updateOrCreate(
                [
                    'tab_id' => $tabsDetailQueryGroupNewsletter->id,
                    'code' => 'NameView'
                ],
                [
                    'field' => 'nameView',
                    'data_type' => '1',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldDetailNameQueryGroupNewsletter->id, 'language_id' => $idLanguage],
                ['description' => 'Nome View', 'created_id' => $idUser]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldDetailNameQueryGroupNewsletter->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '0',
                    'pos_x' => '10',
                    'pos_y' => '10',
                    'created_id' => $idUser
                ]
            );

            $fieldDetailDescriptionQueryGroupNewsletter = Field::updateOrCreate(
                [
                    'tab_id' => $tabsDetailQueryGroupNewsletter->id,
                    'code' => 'Description'
                ],
                [
                    'field' => 'description',
                    'data_type' => '1',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldDetailDescriptionQueryGroupNewsletter->id, 'language_id' => $idLanguage],
                ['description' => 'Descrizione View', 'created_id' => $idUser]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldDetailDescriptionQueryGroupNewsletter->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '0',
                    'pos_x' => '20',
                    'pos_y' => '10',
                    'created_id' => $idUser
                ]
            );


            $fieldDetailSelectQueryGroupNewsletter = Field::updateOrCreate(
                [
                    'tab_id' => $tabsDetailQueryGroupNewsletter->id,
                    'code' => 'Select'
                ],
                [
                    'field' => 'select',
                    'data_type' => '1',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldDetailSelectQueryGroupNewsletter->id, 'language_id' => $idLanguage],
                ['description' => 'Select', 'created_id' => $idUser]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldDetailSelectQueryGroupNewsletter->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '0',
                    'pos_x' => '10',
                    'pos_y' => '20',
                    'created_id' => $idUser
                ]
            );

            $fieldDetailFromQueryGroupNewsletter = Field::updateOrCreate(
                [
                    'tab_id' => $tabsDetailQueryGroupNewsletter->id,
                    'code' => 'From'
                ],
                [
                    'field' => 'from',
                    'data_type' => '1',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldDetailFromQueryGroupNewsletter->id, 'language_id' => $idLanguage],
                ['description' => 'From', 'created_id' => $idUser]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldDetailFromQueryGroupNewsletter->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '0',
                    'pos_x' => '10',
                    'pos_y' => '30',
                    'created_id' => $idUser
                ]
            );

            $fieldDetailWhereQueryGroupNewsletter = Field::updateOrCreate(
                [
                    'tab_id' => $tabsDetailQueryGroupNewsletter->id,
                    'code' => 'Where'
                ],
                [
                    'field' => 'where',
                    'data_type' => '1',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldDetailWhereQueryGroupNewsletter->id, 'language_id' => $idLanguage],
                ['description' => 'Where', 'created_id' => $idUser]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldDetailWhereQueryGroupNewsletter->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '0',
                    'pos_x' => '10',
                    'pos_y' => '40',
                    'created_id' => $idUser
                ]
            );

            $fieldDetailOrderByQueryGroupNewsletter = Field::updateOrCreate(
                [
                    'tab_id' => $tabsDetailQueryGroupNewsletter->id,
                    'code' => 'OrderBy'
                ],
                [
                    'field' => 'orderBy',
                    'data_type' => '1',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldDetailOrderByQueryGroupNewsletter->id, 'language_id' => $idLanguage],
                ['description' => 'Order by', 'created_id' => $idUser]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldDetailOrderByQueryGroupNewsletter->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '0',
                    'pos_x' => '10',
                    'pos_y' => '50',
                    'created_id' => $idUser
                ]
            );

            $fieldDetailGroupByQueryGroupNewsletter = Field::updateOrCreate(
                [
                    'tab_id' => $tabsDetailQueryGroupNewsletter->id,
                    'code' => 'GroupBy'
                ],
                [
                    'field' => 'groupBy',
                    'data_type' => '1',
                    'created_id' => $idUser
                ]
            );

            $fieldLanguage = FieldLanguage::updateOrCreate(
                ['field_id' => $fieldDetailGroupByQueryGroupNewsletter->id, 'language_id' => $idLanguage],
                ['description' => 'Group by', 'created_id' => $idUser]
            );

            $fieldUserRole = FieldUserRole::updateOrCreate(
                ['field_id' => $fieldDetailGroupByQueryGroupNewsletter->id, 'role_id' => $idRole],
                [
                    'enabled' => 't',
                    'input_type' => '0',
                    'pos_x' => '10',
                    'pos_y' => '60',
                    'created_id' => $idUser
                ]
            );
        }
    }

<?php

use App\Dictionary;
use App\Enums\SettingEnum;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Setting;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class PrandoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        $this->call([]);

        #region DICTIONARY

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'BackGroundVisitorsViewsEmpty';
        $dictionary->description = 'Colore di sfondo di Google Analytics pagine visitatori non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'BorderVisitorsViewsEmpty';
        $dictionary->description = 'Linea di Google Analytics visitatori non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'BackGroundPagesViewsEmpty';
        $dictionary->description = 'Colore di sfondo di Google Analytics pagine visitate non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'BorderPagesViewsEmpty';
        $dictionary->description = 'Linea di Google Analytics pagine visitate non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'BackGroundDeviceSessionEmpty';
        $dictionary->description = 'Colore di sfondo di Google Analytics sessioni dispositivi non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'BackGroundSourceSessionEmpty';
        $dictionary->description = 'Colore di sfondo di Google Analytics sorgente sessioni non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        #endregion DICTIONARY

        $setting = new Setting();
        $setting->code = SettingEnum::BackgroundColorChartVisitorsViews;
        $setting->value = "#3e95cd";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::BorderColorChartVisitorsViews;
        $setting->value = "rgba(62,149,205,0.4)";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::BackgroundColorChartPagesViews;
        $setting->value = "rgb(255, 99, 132)";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::BorderColorChartPagesViews;
        $setting->value = "rgb(255, 99, 132)";
        $setting->created_id = $idUser;
        $setting->save();
        $setting = new Setting();
        $setting->code = SettingEnum::BackgroundColorChartDeviceSession;
        $setting->value = "#0080ff, #de2727, #2eb85c";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::BackgroundColorChartSourceSession;
        $setting->value = "#2819ae, #b2b8c1, #248f48, #d69405, #de2727, #0080ff, #cfd4d8, #4d5666";
        $setting->created_id = $idUser;
        $setting->save();
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Dictionary;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;

class FaultModuleinterventionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()

    {


         $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $idMenuAdminCRM = MenuAdmin::where('code', 'CRM')->first()->id;


        $iconIdCard = new Icon();
        $iconIdCard->description = 'Bookmark';
        $iconIdCard->css = 'fa fa-bookmark-o';
        $iconIdCard->created_id = $idUser;
        $iconIdCard->save();
          #region FUNCTIONALITY

        $functionalityContact = new Functionality();
        $functionalityContact->code = 'Interventionss';
        $functionalityContact->created_id = $idUser;
        $functionalityContact->save();

        #endregion FUNCTIONALITY

        

        #region DETAIL

        #region TAB
        $tabContactDetail = new Tab();
        $tabContactDetail->code = 'Detail';
        $tabContactDetail->functionality_id = $functionalityContact->id;
        $tabContactDetail->order = 20;
        $tabContactDetail->created_id = $idUser;
        $tabContactDetail->save();

        $languageTabContactDetail = new LanguageTab();
        $languageTabContactDetail->language_id = $idLanguage;
        $languageTabContactDetail->tab_id = $tabContactDetail->id;
        $languageTabContactDetail->description = 'Detail';
        $languageTabContactDetail->created_id = $idUser;
        $languageTabContactDetail->save();
        #endregion TAB

        #region Vs Referente
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'referent';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'referent';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Vs Referente';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 20;
        $fieldUserRoleContactDetailCompany->pos_y = 10;
        $fieldUserRoleContactDetailCompany->input_type = 0;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion Vs Referente

        #region numero ddt
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'nr_ddt';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'nr_ddt';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Numero DDT';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 30;
        $fieldUserRoleSurnameContactDetail->pos_y = 10;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion numero ddt

        #region  tipo impianto
        $fieldContactDetailBusinessName = new Field();
        $fieldContactDetailBusinessName->code = 'plant_type';
        $fieldContactDetailBusinessName->tab_id = $tabContactDetail->id;
        $fieldContactDetailBusinessName->field = 'plant_type';
        $fieldContactDetailBusinessName->data_type = 1;
        $fieldContactDetailBusinessName->created_id = $idUser;
        $fieldContactDetailBusinessName->save();

        $fieldLanguageContactDetailBusinessName = new FieldLanguage();
        $fieldLanguageContactDetailBusinessName->field_id = $fieldContactDetailBusinessName->id;
        $fieldLanguageContactDetailBusinessName->language_id = $idLanguage;
        $fieldLanguageContactDetailBusinessName->description = 'Tipo Impianto';
        $fieldLanguageContactDetailBusinessName->created_id = $idUser;
        $fieldLanguageContactDetailBusinessName->save();

        $fieldUserRoleContactDetailBusinessName = new FieldUserRole();
        $fieldUserRoleContactDetailBusinessName->field_id = $fieldContactDetailBusinessName->id;
        $fieldUserRoleContactDetailBusinessName->role_id = $idRole;
        $fieldUserRoleContactDetailBusinessName->enabled = true;
        $fieldUserRoleContactDetailBusinessName->pos_x = 40;
        $fieldUserRoleContactDetailBusinessName->pos_y = 50;
        $fieldUserRoleContactDetailBusinessName->input_type = 0;
        $fieldUserRoleContactDetailBusinessName->created_id = $idUser;
        $fieldUserRoleContactDetailBusinessName->save();
        #endregion tipo impianto

        #region tipo carrello
        $fieldContactDetailName = new Field();
        $fieldContactDetailName->code = 'trolley_type';
        $fieldContactDetailName->tab_id = $tabContactDetail->id;
        $fieldContactDetailName->field = 'trolley_type';
        $fieldContactDetailName->data_type = 1;
        $fieldContactDetailName->created_id = $idUser;
        $fieldContactDetailName->save();

        $fieldLanguageContactDetailName = new FieldLanguage();
        $fieldLanguageContactDetailName->field_id = $fieldContactDetailName->id;
        $fieldLanguageContactDetailName->language_id = $idLanguage;
        $fieldLanguageContactDetailName->description = 'Tipo Carrello';
        $fieldLanguageContactDetailName->created_id = $idUser;
        $fieldLanguageContactDetailName->save();

        $fieldUserRoleContactDetailName = new FieldUserRole();
        $fieldUserRoleContactDetailName->field_id = $fieldContactDetailName->id;
        $fieldUserRoleContactDetailName->role_id = $idRole;
        $fieldUserRoleContactDetailName->enabled = true;
        $fieldUserRoleContactDetailName->pos_x = 40;
        $fieldUserRoleContactDetailName->pos_y = 50;
        $fieldUserRoleContactDetailName->input_type = 0;
        $fieldUserRoleContactDetailName->created_id = $idUser;
        $fieldUserRoleContactDetailName->save();
        #endregion tipo carrello

        #region serie
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'series';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'series';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Serie';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 50;
        $fieldUserRoleContactDetailCompany->pos_y = 50;
        $fieldUserRoleContactDetailCompany->input_type = 0;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion serie


        #region difetto lamentato
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'defect';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'defect';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Difetto Lamentato';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 60;
        $fieldUserRoleSurnameContactDetail->pos_y = 70;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion difetto lamentato 


        #region voltaggio
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'voltage';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'voltage';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Voltaggio';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 70;
        $fieldUserRoleSurnameContactDetail->pos_y = 70;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion voltaggio


         #region note
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'exit_notes';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'exit_notes';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Note';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 80;
        $fieldUserRoleSurnameContactDetail->pos_y = 80;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion note

 		#region customer id 
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'customer_id';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'customer_id';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Committente';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 10;
        $fieldUserRoleSurnameContactDetail->pos_y = 10;
        $fieldUserRoleSurnameContactDetail->input_type = 2;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion customer id 

        #region DATA DDT
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'date_ddt';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'date_ddt';
        $fieldSurnameContactDetail->data_type = 2;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Data DDT';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 40;
        $fieldUserRoleSurnameContactDetail->pos_y = 10;
        $fieldUserRoleSurnameContactDetail->input_type = 4;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion DATA DDT 

         #region DATA DDT
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'create_quote';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'create_quote';
        $fieldSurnameContactDetail->data_type = 3;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Genera Preventivo';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 40;
        $fieldUserRoleSurnameContactDetail->pos_y = 10;
        $fieldUserRoleSurnameContactDetail->input_type = 5;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion DATA DDT 

         #endregion DETAIL

        #endregion GrInterventions    

    }
}

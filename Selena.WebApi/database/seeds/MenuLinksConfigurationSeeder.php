<?php

use App\Enums\PageTypesEnum;
use Illuminate\Database\Seeder;

use App\Language;
use App\Role;
use App\User;
use App\Functionality;
use App\Tab;
use App\LanguageTab;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;

class MenuLinksConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idFunctionality = Functionality::where('code', 'Pages')->first()->id;
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        $tabLink = new Tab();
        $tabLink->code = 'Link';
        $tabLink->functionality_id = $idFunctionality;
        $tabLink->order = 20;
        $tabLink->created_id = $idUser;
        $tabLink->save();

        $languageTabLink = new LanguageTab();
        $languageTabLink->language_id = $idLanguage;
        $languageTabLink->tab_id = $tabLink->id;
        $languageTabLink->description = 'Link';
        $languageTabLink->created_id = $idUser;
        $languageTabLink->save();

        #region TitlePage

        $fieldTitlePage = new Field();
        $fieldTitlePage->code = 'TitleLink';
        $fieldTitlePage->tab_id = $tabLink->id;
        $fieldTitlePage->field = 'languagesMenus.title';
        $fieldTitlePage->data_type = 1;
        $fieldTitlePage->required = true;
        $fieldTitlePage->max_length = 255;
        $fieldTitlePage->on_change = 'changeTitle';
        $fieldTitlePage->created_id = $idUser;
        $fieldTitlePage->save();

        $fieldLanguageTitlePage = new FieldLanguage();
        $fieldLanguageTitlePage->field_id = $fieldTitlePage->id;
        $fieldLanguageTitlePage->language_id = $idLanguage;
        $fieldLanguageTitlePage->description = 'Titolo';
        $fieldLanguageTitlePage->created_id = $idUser;
        $fieldLanguageTitlePage->save();

        $fieldUserRoleTitlePage = new FieldUserRole();
        $fieldUserRoleTitlePage->field_id = $fieldTitlePage->id;
        $fieldUserRoleTitlePage->role_id = $idRole;
        $fieldUserRoleTitlePage->enabled = true;
        $fieldUserRoleTitlePage->input_type = 0;
        $fieldUserRoleTitlePage->pos_x = 10;
        $fieldUserRoleTitlePage->pos_y = 10;
        $fieldUserRoleTitlePage->required = true;
        $fieldUserRoleTitlePage->colspan = 12;
        $fieldUserRoleTitlePage->created_id = $idUser;
        $fieldUserRoleTitlePage->save();

        #endregion TitlePage

        #region Link

        $fieldLink = new Field();
        $fieldLink->code = 'UrlLink';
        $fieldLink->tab_id = $tabLink->id;
        $fieldLink->field = 'languagesMenus.url';
        $fieldLink->data_type = 1;
        $fieldLink->required = true;
        $fieldLink->max_length = 255;
        $fieldLink->created_id = $idUser;
        $fieldLink->save();

        $fieldLanguageLink = new FieldLanguage();
        $fieldLanguageLink->field_id = $fieldLink->id;
        $fieldLanguageLink->language_id = $idLanguage;
        $fieldLanguageLink->description = 'Url';
        $fieldLanguageLink->created_id = $idUser;
        $fieldLanguageLink->save();

        $fieldUserRoleTitleLink = new FieldUserRole();
        $fieldUserRoleTitleLink->field_id = $fieldLink->id;
        $fieldUserRoleTitleLink->role_id = $idRole;
        $fieldUserRoleTitleLink->enabled = true;
        $fieldUserRoleTitleLink->input_type = 0;
        $fieldUserRoleTitleLink->pos_x = 10;
        $fieldUserRoleTitleLink->pos_y = 20;
        $fieldUserRoleTitleLink->colspan = 12;
        $fieldUserRoleTitleLink->created_id = $idUser;
        $fieldUserRoleTitleLink->save();

        #endregion Link

        #region NewWindow

        $fieldNewWindow = new Field();
        $fieldNewWindow->code = 'NewWindowLink';
        $fieldNewWindow->tab_id = $tabLink->id;
        $fieldNewWindow->field = 'newWindow';
        $fieldNewWindow->data_type = 3;
        $fieldNewWindow->default_value = true;
        $fieldNewWindow->created_id = $idUser;
        $fieldNewWindow->save();

        $fieldLanguageNewWindow = new FieldLanguage();
        $fieldLanguageNewWindow->field_id = $fieldNewWindow->id;
        $fieldLanguageNewWindow->language_id = $idLanguage;
        $fieldLanguageNewWindow->description = 'Apri una nuova pagina';
        $fieldLanguageNewWindow->created_id = $idUser;
        $fieldLanguageNewWindow->save();

        $fieldUserRoleTitleNewWindow = new FieldUserRole();
        $fieldUserRoleTitleNewWindow->field_id = $fieldNewWindow->id;
        $fieldUserRoleTitleNewWindow->role_id = $idRole;
        $fieldUserRoleTitleNewWindow->enabled = true;
        $fieldUserRoleTitleNewWindow->input_type = 5;
        $fieldUserRoleTitleNewWindow->pos_x = 10;
        $fieldUserRoleTitleNewWindow->pos_y = 30;
        $fieldUserRoleTitleNewWindow->colspan = 12;
        $fieldUserRoleTitleNewWindow->created_id = $idUser;
        $fieldUserRoleTitleNewWindow->save();

        #endregion NewWindow

        #region Icon

        $fieldIcon = new Field();
        $fieldIcon->code = 'IconLink';
        $fieldIcon->tab_id = $tabLink->id;
        $fieldIcon->field = 'idIcon';
        $fieldIcon->data_type = 0;
        $fieldIcon->service = 'icons';
        $fieldIcon->created_id = $idUser;
        $fieldIcon->save();

        $fieldLanguageIcon = new FieldLanguage();
        $fieldLanguageIcon->field_id = $fieldIcon->id;
        $fieldLanguageIcon->language_id = $idLanguage;
        $fieldLanguageIcon->description = 'Icona';
        $fieldLanguageIcon->created_id = $idUser;
        $fieldLanguageIcon->save();

        $fieldUserRoleIcon = new FieldUserRole();
        $fieldUserRoleIcon->field_id = $fieldIcon->id;
        $fieldUserRoleIcon->role_id = $idRole;
        $fieldUserRoleIcon->enabled = true;
        $fieldUserRoleIcon->input_type = 2;
        $fieldUserRoleIcon->pos_x = 10;
        $fieldUserRoleIcon->pos_y = 40;
        $fieldUserRoleIcon->colspan = 12;
        $fieldUserRoleIcon->created_id = $idUser;
        $fieldUserRoleIcon->save();

        #endregion Icon

        #region HideMobile

        $fieldHideMobile = new Field();
        $fieldHideMobile->code = 'HideMobileLink';
        $fieldHideMobile->tab_id = $tabLink->id;
        $fieldHideMobile->field = 'hideMobile';
        $fieldHideMobile->data_type = 3;
        $fieldHideMobile->default_value = true;
        $fieldHideMobile->created_id = $idUser;
        $fieldHideMobile->save();

        $fieldLanguageHideMobile = new FieldLanguage();
        $fieldLanguageHideMobile->field_id = $fieldHideMobile->id;
        $fieldLanguageHideMobile->language_id = $idLanguage;
        $fieldLanguageHideMobile->description = 'Nascondi da mobile';
        $fieldLanguageHideMobile->created_id = $idUser;
        $fieldLanguageHideMobile->save();

        $fieldUserRoleHideMobile = new FieldUserRole();
        $fieldUserRoleHideMobile->field_id = $fieldHideMobile->id;
        $fieldUserRoleHideMobile->role_id = $idRole;
        $fieldUserRoleHideMobile->enabled = true;
        $fieldUserRoleHideMobile->input_type = 5;
        $fieldUserRoleHideMobile->pos_x = 10;
        $fieldUserRoleHideMobile->pos_y = 50;
        $fieldUserRoleHideMobile->colspan = 12;
        $fieldUserRoleHideMobile->created_id = $idUser;
        $fieldUserRoleHideMobile->save();

        #endregion HideMobile

        #region IdLanguage

        $fieldIdLanguage = new Field();
        $fieldIdLanguage->code = 'IdLanguageLink';
        $fieldIdLanguage->tab_id = $tabLink->id;
        $fieldIdLanguage->field = 'languagesMenus.idLanguage';
        $fieldIdLanguage->data_type = 0;
        $fieldIdLanguage->created_id = $idUser;
        $fieldIdLanguage->save();

        $fieldLanguageIdLanguage = new FieldLanguage();
        $fieldLanguageIdLanguage->field_id = $fieldIdLanguage->id;
        $fieldLanguageIdLanguage->language_id = $idLanguage;
        $fieldLanguageIdLanguage->description = 'Lingua';
        $fieldLanguageIdLanguage->created_id = $idUser;
        $fieldLanguageIdLanguage->save();

        $fieldUserRoleIdLanguage = new FieldUserRole();
        $fieldUserRoleIdLanguage->field_id = $fieldIdLanguage->id;
        $fieldUserRoleIdLanguage->role_id = $idRole;
        $fieldUserRoleIdLanguage->enabled = true;
        $fieldUserRoleIdLanguage->input_type = 19;
        $fieldUserRoleIdLanguage->pos_x = 10;
        $fieldUserRoleIdLanguage->pos_y = 10;
        $fieldUserRoleIdLanguage->created_id = $idUser;
        $fieldUserRoleIdLanguage->save();

        #endregion IdLanguage


        $fieldPageType = new Field();
        $fieldPageType->code = 'PageType';
        $fieldPageType->tab_id = $tabLink->id;
        $fieldPageType->field = 'pageType';
        $fieldPageType->default_value = PageTypesEnum::Page;
        $fieldPageType->data_type = 0;
        $fieldPageType->created_id = $idUser;
        $fieldPageType->save();

        $fieldLanguagePageType = new FieldLanguage();
        $fieldLanguagePageType->field_id = $fieldPageType->id;
        $fieldLanguagePageType->language_id = $idLanguage;
        $fieldLanguagePageType->description = 'Tipo pagina';
        $fieldLanguagePageType->created_id = $idUser;
        $fieldLanguagePageType->save();

        $fieldUserRolePageType = new FieldUserRole();
        $fieldUserRolePageType->field_id = $fieldPageType->id;
        $fieldUserRolePageType->role_id = $idRole;
        $fieldUserRolePageType->enabled = true;
        $fieldUserRolePageType->input_type = 12;
        $fieldUserRolePageType->pos_x = 10;
        $fieldUserRolePageType->pos_y = 10;
        $fieldUserRolePageType->created_id = $idUser;
        $fieldUserRolePageType->save();
    }
}

<?php

use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class TemplateEditorConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idUser = User::first()->id;
        $idLanguage = Language::first()->id;
        $idRole = Role::where('name', 'admin')->first()->id;
        $idMenuAdminSetting = MenuAdmin::where('code', 'Setting')->first()->id;

        #region TEMPLATE

        $iconListAlt = new Icon();
        $iconListAlt->description = 'ListAlt';
        $iconListAlt->css = 'fa fa-list-alt';
        $iconListAlt->created_id = $idUser;
        $iconListAlt->save();

        $menuAdminTemplate = new MenuAdmin();
        $menuAdminTemplate->code = 'Template';
        $menuAdminTemplate->icon_id = $iconListAlt->id;
        $menuAdminTemplate->created_id = $idUser;
        $menuAdminTemplate->save();

        $languageMenuAdminTemplate = new LanguageMenuAdmin();
        $languageMenuAdminTemplate->language_id = $idLanguage;
        $languageMenuAdminTemplate->menu_admin_id = $menuAdminTemplate->id;
        $languageMenuAdminTemplate->description = 'Blocchi';
        $languageMenuAdminTemplate->created_id = $idUser;
        $languageMenuAdminTemplate->save();

        $menuAdminUserRoleTemplate = new MenuAdminUserRole();
        $menuAdminUserRoleTemplate->menu_admin_id = $menuAdminTemplate->id;
        $menuAdminUserRoleTemplate->menu_admin_father_id = $idMenuAdminSetting;
        $menuAdminUserRoleTemplate->role_id = $idRole;
        $menuAdminUserRoleTemplate->enabled = true;
        $menuAdminUserRoleTemplate->order = 10;
        $menuAdminUserRoleTemplate->created_id = $idUser;
        $menuAdminUserRoleTemplate->save();

        #endregion TEMPLATE

        #region TEMPLATE EDITOR

        #region FUNZIONE

        $functionalityTemplateEditor = new Functionality();
        $functionalityTemplateEditor->code = 'TemplateEditor';
        $functionalityTemplateEditor->created_id = $idUser;
        $functionalityTemplateEditor->save();

        #endregion FUNZIONE

        #region ICONA

        $iconTasks = new Icon();
        $iconTasks->description = 'Tasks';
        $iconTasks->css = 'fa fa-tasks';
        $iconTasks->created_id = $idUser;
        $iconTasks->save();

        #endregion ICONA

        #region MENU

        $menuAdminTemplateEditor = new MenuAdmin();
        $menuAdminTemplateEditor->code = 'TemplateEditor';
        $menuAdminTemplateEditor->functionality_id = $functionalityTemplateEditor->id;
        $menuAdminTemplateEditor->icon_id = $iconTasks->id;
        $menuAdminTemplateEditor->url = '/admin/setting/templateEditor';
        $menuAdminTemplateEditor->created_id = $idUser;
        $menuAdminTemplateEditor->save();

        $languageMenuAdminTemplateEditor = new LanguageMenuAdmin();
        $languageMenuAdminTemplateEditor->language_id = $idLanguage;
        $languageMenuAdminTemplateEditor->menu_admin_id = $menuAdminTemplateEditor->id;
        $languageMenuAdminTemplateEditor->description = 'Gestione blocchi';
        $languageMenuAdminTemplateEditor->created_id = $idUser;
        $languageMenuAdminTemplateEditor->save();

        $menuAdminUserRoleTemplateEditor = new MenuAdminUserRole();
        $menuAdminUserRoleTemplateEditor->menu_admin_id = $menuAdminTemplateEditor->id;
        $menuAdminUserRoleTemplateEditor->menu_admin_father_id = $menuAdminTemplate->id;
        $menuAdminUserRoleTemplateEditor->role_id = $idRole;
        $menuAdminUserRoleTemplateEditor->enabled = true;
        $menuAdminUserRoleTemplateEditor->order = 100;
        $menuAdminUserRoleTemplateEditor->created_id = $idUser;
        $menuAdminUserRoleTemplateEditor->save();

        #endregion MENU

        #region TAB TEMPLATE EDITOR

        $tabTemplateEditor = new Tab();
        $tabTemplateEditor->code = 'TemplateEditor';
        $tabTemplateEditor->functionality_id = $functionalityTemplateEditor->id;
        $tabTemplateEditor->order = 10;
        $tabTemplateEditor->created_id = $idUser;
        $tabTemplateEditor->save();

        $languageTabTemplateEditor = new LanguageTab();
        $languageTabTemplateEditor->language_id = $idLanguage;
        $languageTabTemplateEditor->tab_id = $tabTemplateEditor->id;
        $languageTabTemplateEditor->description = 'Blocchi';
        $languageTabTemplateEditor->created_id = $idUser;
        $languageTabTemplateEditor->save();

        #endregion TAB TEMPLATE EDITOR

        #region TABLE

        #region CHECKBOX

        $fieldTemplateEditorCheckBox = new Field();
        $fieldTemplateEditorCheckBox->tab_id = $tabTemplateEditor->id;
        $fieldTemplateEditorCheckBox->code = 'TableCheckBox';
        $fieldTemplateEditorCheckBox->created_id = $idUser;
        $fieldTemplateEditorCheckBox->save();

        $fieldLanguageTemplateEditorCheckBox = new FieldLanguage();
        $fieldLanguageTemplateEditorCheckBox->field_id = $fieldTemplateEditorCheckBox->id;
        $fieldLanguageTemplateEditorCheckBox->language_id = $idLanguage;
        $fieldLanguageTemplateEditorCheckBox->description = 'CheckBox';
        $fieldLanguageTemplateEditorCheckBox->created_id = $idUser;
        $fieldLanguageTemplateEditorCheckBox->save();

        $fieldUserRoleTemplateEditorCheckBox = new FieldUserRole();
        $fieldUserRoleTemplateEditorCheckBox->field_id = $fieldTemplateEditorCheckBox->id;
        $fieldUserRoleTemplateEditorCheckBox->role_id = $idRole;
        $fieldUserRoleTemplateEditorCheckBox->enabled = true;
        $fieldUserRoleTemplateEditorCheckBox->table_order = 10;
        $fieldUserRoleTemplateEditorCheckBox->input_type = 13;
        $fieldUserRoleTemplateEditorCheckBox->created_id = $idUser;
        $fieldUserRoleTemplateEditorCheckBox->save();

        #endregion CHECKBOX

        #region FORMATTER

        $fieldTemplateEditorFormatter = new Field();
        $fieldTemplateEditorFormatter->code = 'TemplateEditorFormatter';
        $fieldTemplateEditorFormatter->formatter = 'templateEditorFormatter';
        $fieldTemplateEditorFormatter->tab_id = $tabTemplateEditor->id;
        $fieldTemplateEditorFormatter->created_id = $idUser;
        $fieldTemplateEditorFormatter->save();

        $fieldLanguageTemplateEditorFormatter = new FieldLanguage();
        $fieldLanguageTemplateEditorFormatter->field_id = $fieldTemplateEditorFormatter->id;
        $fieldLanguageTemplateEditorFormatter->language_id = $idLanguage;
        $fieldLanguageTemplateEditorFormatter->description = '';
        $fieldLanguageTemplateEditorFormatter->created_id = $idUser;
        $fieldLanguageTemplateEditorFormatter->save();

        $fieldUserRoleTemplateEditorFormatter = new FieldUserRole();
        $fieldUserRoleTemplateEditorFormatter->field_id = $fieldTemplateEditorFormatter->id;
        $fieldUserRoleTemplateEditorFormatter->role_id = $idRole;
        $fieldUserRoleTemplateEditorFormatter->enabled = true;
        $fieldUserRoleTemplateEditorFormatter->table_order = 20;
        $fieldUserRoleTemplateEditorFormatter->input_type = 14;
        $fieldUserRoleTemplateEditorFormatter->created_id = $idUser;
        $fieldUserRoleTemplateEditorFormatter->save();

        #endregion FORMATTER

        #region DESCRIPTION

        $fieldTemplateEditorDescription = new Field();
        $fieldTemplateEditorDescription->code = 'Description';
        $fieldTemplateEditorDescription->field = 'description';
        $fieldTemplateEditorDescription->data_type = 1;
        $fieldTemplateEditorDescription->tab_id = $tabTemplateEditor->id;
        $fieldTemplateEditorDescription->created_id = $idUser;
        $fieldTemplateEditorDescription->save();

        $fieldLanguageTemplateEditorDescription = new FieldLanguage();
        $fieldLanguageTemplateEditorDescription->field_id = $fieldTemplateEditorDescription->id;
        $fieldLanguageTemplateEditorDescription->language_id = $idLanguage;
        $fieldLanguageTemplateEditorDescription->description = 'Descrizione';
        $fieldLanguageTemplateEditorDescription->created_id = $idUser;
        $fieldLanguageTemplateEditorDescription->save();

        $fieldUserRoleTemplateEditorDescription = new FieldUserRole();
        $fieldUserRoleTemplateEditorDescription->field_id = $fieldTemplateEditorDescription->id;
        $fieldUserRoleTemplateEditorDescription->role_id = $idRole;
        $fieldUserRoleTemplateEditorDescription->enabled = true;
        $fieldUserRoleTemplateEditorDescription->table_order = 30;
        $fieldUserRoleTemplateEditorDescription->input_type = 0;
        $fieldUserRoleTemplateEditorDescription->required = true;
        $fieldUserRoleTemplateEditorDescription->created_id = $idUser;
        $fieldUserRoleTemplateEditorDescription->save();

        #endregion DESCRIPTION

        #region IMAGE

        $fieldTemplateEditorImage = new Field();
        $fieldTemplateEditorImage->code = 'Image';
        $fieldTemplateEditorImage->field = 'img';
        $fieldTemplateEditorImage->data_type = 1;
        $fieldTemplateEditorImage->tab_id = $tabTemplateEditor->id;
        $fieldTemplateEditorImage->created_id = $idUser;
        $fieldTemplateEditorImage->save();

        $fieldLanguageTemplateEditorImage = new FieldLanguage();
        $fieldLanguageTemplateEditorImage->field_id = $fieldTemplateEditorImage->id;
        $fieldLanguageTemplateEditorImage->language_id = $idLanguage;
        $fieldLanguageTemplateEditorImage->description = 'Immagine';
        $fieldLanguageTemplateEditorImage->created_id = $idUser;
        $fieldLanguageTemplateEditorImage->save();

        $fieldUserRoleTemplateEditorImage = new FieldUserRole();
        $fieldUserRoleTemplateEditorImage->field_id = $fieldTemplateEditorImage->id;
        $fieldUserRoleTemplateEditorImage->role_id = $idRole;
        $fieldUserRoleTemplateEditorImage->enabled = true;
        $fieldUserRoleTemplateEditorImage->table_order = 40;
        $fieldUserRoleTemplateEditorImage->input_type = 18;
        $fieldUserRoleTemplateEditorImage->required = true;
        $fieldUserRoleTemplateEditorImage->created_id = $idUser;
        $fieldUserRoleTemplateEditorImage->save();

        #endregion IMAGE

        #region GROUP

        $fieldTemplateEditorGroup = new Field();
        $fieldTemplateEditorGroup->code = 'Group';
        $fieldTemplateEditorGroup->field = 'idTemplateEditorGroup';
        $fieldTemplateEditorGroup->data_type = 0;
        $fieldTemplateEditorGroup->service = 'templateEditorGroup';
        $fieldTemplateEditorGroup->tab_id = $tabTemplateEditor->id;
        $fieldTemplateEditorGroup->created_id = $idUser;
        $fieldTemplateEditorGroup->save();

        $fieldLanguageTemplateEditorGroup = new FieldLanguage();
        $fieldLanguageTemplateEditorGroup->field_id = $fieldTemplateEditorGroup->id;
        $fieldLanguageTemplateEditorGroup->language_id = $idLanguage;
        $fieldLanguageTemplateEditorGroup->description = 'Gruppo';
        $fieldLanguageTemplateEditorGroup->created_id = $idUser;
        $fieldLanguageTemplateEditorGroup->save();

        $fieldUserRoleTemplateEditorGroup = new FieldUserRole();
        $fieldUserRoleTemplateEditorGroup->field_id = $fieldTemplateEditorGroup->id;
        $fieldUserRoleTemplateEditorGroup->role_id = $idRole;
        $fieldUserRoleTemplateEditorGroup->enabled = true;
        $fieldUserRoleTemplateEditorGroup->table_order = 50;
        $fieldUserRoleTemplateEditorGroup->input_type = 2;
        $fieldUserRoleTemplateEditorGroup->required = true;
        $fieldUserRoleTemplateEditorGroup->created_id = $idUser;
        $fieldUserRoleTemplateEditorGroup->save();

        #endregion GROUP

        #region ORDER

        $fieldTemplateEditorOrder = new Field();
        $fieldTemplateEditorOrder->code = 'Order';
        $fieldTemplateEditorOrder->field = 'order';
        $fieldTemplateEditorOrder->data_type = 0;
        $fieldTemplateEditorOrder->tab_id = $tabTemplateEditor->id;
        $fieldTemplateEditorOrder->created_id = $idUser;
        $fieldTemplateEditorOrder->save();

        $fieldLanguageTemplateEditorOrder = new FieldLanguage();
        $fieldLanguageTemplateEditorOrder->field_id = $fieldTemplateEditorOrder->id;
        $fieldLanguageTemplateEditorOrder->language_id = $idLanguage;
        $fieldLanguageTemplateEditorOrder->description = 'Ordinamento';
        $fieldLanguageTemplateEditorOrder->created_id = $idUser;
        $fieldLanguageTemplateEditorOrder->save();

        $fieldUserRoleTemplateEditorOrder = new FieldUserRole();
        $fieldUserRoleTemplateEditorOrder->field_id = $fieldTemplateEditorOrder->id;
        $fieldUserRoleTemplateEditorOrder->role_id = $idRole;
        $fieldUserRoleTemplateEditorOrder->enabled = true;
        $fieldUserRoleTemplateEditorOrder->table_order = 60;
        $fieldUserRoleTemplateEditorOrder->input_type = 7;
        $fieldUserRoleTemplateEditorOrder->created_id = $idUser;
        $fieldUserRoleTemplateEditorOrder->save();

        #endregion ORDER

        #endregion TABLE

        #region DETAIL

        #region DESCRIPTION

        $fieldTemplateEditorDescriptionDetail = new Field();
        $fieldTemplateEditorDescriptionDetail->code = 'Description';
        $fieldTemplateEditorDescriptionDetail->field = 'description';
        $fieldTemplateEditorDescriptionDetail->data_type = 1;
        $fieldTemplateEditorDescriptionDetail->tab_id = $tabTemplateEditor->id;
        $fieldTemplateEditorDescriptionDetail->created_id = $idUser;
        $fieldTemplateEditorDescriptionDetail->save();

        $fieldLanguageTemplateEditorDescriptionDetail = new FieldLanguage();
        $fieldLanguageTemplateEditorDescriptionDetail->field_id = $fieldTemplateEditorDescriptionDetail->id;
        $fieldLanguageTemplateEditorDescriptionDetail->language_id = $idLanguage;
        $fieldLanguageTemplateEditorDescriptionDetail->description = 'Descrizione';
        $fieldLanguageTemplateEditorDescriptionDetail->created_id = $idUser;
        $fieldLanguageTemplateEditorDescriptionDetail->save();

        $fieldUserRoleTemplateEditorDescriptionDetail = new FieldUserRole();
        $fieldUserRoleTemplateEditorDescriptionDetail->field_id = $fieldTemplateEditorDescriptionDetail->id;
        $fieldUserRoleTemplateEditorDescriptionDetail->role_id = $idRole;
        $fieldUserRoleTemplateEditorDescriptionDetail->enabled = true;
        $fieldUserRoleTemplateEditorDescriptionDetail->pos_x = 10;
        $fieldUserRoleTemplateEditorDescriptionDetail->pos_y = 10;
        $fieldUserRoleTemplateEditorDescriptionDetail->input_type = 0;
        $fieldUserRoleTemplateEditorDescriptionDetail->required = true;
        $fieldUserRoleTemplateEditorDescriptionDetail->created_id = $idUser;
        $fieldUserRoleTemplateEditorDescriptionDetail->save();

        #endregion DESCRIPTION

        #region IMAGE

        $fieldTemplateEditorImageDetail = new Field();
        $fieldTemplateEditorImageDetail->code = 'Image';
        $fieldTemplateEditorImageDetail->field = 'img';
        $fieldTemplateEditorImageDetail->data_type = 1;
        $fieldTemplateEditorImageDetail->tab_id = $tabTemplateEditor->id;
        $fieldTemplateEditorImageDetail->created_id = $idUser;
        $fieldTemplateEditorImageDetail->save();

        $fieldLanguageTemplateEditorImageDetail = new FieldLanguage();
        $fieldLanguageTemplateEditorImageDetail->field_id = $fieldTemplateEditorImageDetail->id;
        $fieldLanguageTemplateEditorImageDetail->language_id = $idLanguage;
        $fieldLanguageTemplateEditorImageDetail->description = 'Immagine';
        $fieldLanguageTemplateEditorImageDetail->created_id = $idUser;
        $fieldLanguageTemplateEditorImageDetail->save();

        $fieldUserRoleTemplateEditorImageDetail = new FieldUserRole();
        $fieldUserRoleTemplateEditorImageDetail->field_id = $fieldTemplateEditorImageDetail->id;
        $fieldUserRoleTemplateEditorImageDetail->role_id = $idRole;
        $fieldUserRoleTemplateEditorImageDetail->enabled = true;
        $fieldUserRoleTemplateEditorImageDetail->pos_x = 20;
        $fieldUserRoleTemplateEditorImageDetail->pos_y = 10;
        $fieldUserRoleTemplateEditorImageDetail->input_type = 18;
        $fieldUserRoleTemplateEditorImageDetail->required = true;
        $fieldUserRoleTemplateEditorImageDetail->created_id = $idUser;
        $fieldUserRoleTemplateEditorImageDetail->save();

        #endregion IMAGE

        #region GROUP

        $fieldTemplateEditorGroupDetail = new Field();
        $fieldTemplateEditorGroupDetail->code = 'Group';
        $fieldTemplateEditorGroupDetail->field = 'idTemplateEditorGroup';
        $fieldTemplateEditorGroupDetail->data_type = 0;
        $fieldTemplateEditorGroupDetail->service = 'templateEditorGroup';
        $fieldTemplateEditorGroupDetail->tab_id = $tabTemplateEditor->id;
        $fieldTemplateEditorGroupDetail->created_id = $idUser;
        $fieldTemplateEditorGroupDetail->save();

        $fieldLanguageTemplateEditorGroupDetail = new FieldLanguage();
        $fieldLanguageTemplateEditorGroupDetail->field_id = $fieldTemplateEditorGroupDetail->id;
        $fieldLanguageTemplateEditorGroupDetail->language_id = $idLanguage;
        $fieldLanguageTemplateEditorGroupDetail->description = 'Gruppo';
        $fieldLanguageTemplateEditorGroupDetail->created_id = $idUser;
        $fieldLanguageTemplateEditorGroupDetail->save();

        $fieldUserRoleTemplateEditorGroupDetail = new FieldUserRole();
        $fieldUserRoleTemplateEditorGroupDetail->field_id = $fieldTemplateEditorGroupDetail->id;
        $fieldUserRoleTemplateEditorGroupDetail->role_id = $idRole;
        $fieldUserRoleTemplateEditorGroupDetail->enabled = true;
        $fieldUserRoleTemplateEditorGroupDetail->pos_x = 30;
        $fieldUserRoleTemplateEditorGroupDetail->pos_y = 10;
        $fieldUserRoleTemplateEditorGroupDetail->input_type = 2;
        $fieldUserRoleTemplateEditorGroupDetail->required = true;
        $fieldUserRoleTemplateEditorGroupDetail->created_id = $idUser;
        $fieldUserRoleTemplateEditorGroupDetail->save();

        #endregion GROUP

        #region ORDER

        $fieldTemplateEditorOrderDetail = new Field();
        $fieldTemplateEditorOrderDetail->code = 'Order';
        $fieldTemplateEditorOrderDetail->field = 'order';
        $fieldTemplateEditorOrderDetail->data_type = 0;
        $fieldTemplateEditorOrderDetail->tab_id = $tabTemplateEditor->id;
        $fieldTemplateEditorOrderDetail->created_id = $idUser;
        $fieldTemplateEditorOrderDetail->save();

        $fieldLanguageTemplateEditorOrderDetail = new FieldLanguage();
        $fieldLanguageTemplateEditorOrderDetail->field_id = $fieldTemplateEditorOrderDetail->id;
        $fieldLanguageTemplateEditorOrderDetail->language_id = $idLanguage;
        $fieldLanguageTemplateEditorOrderDetail->description = 'Ordinamento';
        $fieldLanguageTemplateEditorOrderDetail->created_id = $idUser;
        $fieldLanguageTemplateEditorOrderDetail->save();

        $fieldUserRoleTemplateEditorOrderDetail = new FieldUserRole();
        $fieldUserRoleTemplateEditorOrderDetail->field_id = $fieldTemplateEditorOrderDetail->id;
        $fieldUserRoleTemplateEditorOrderDetail->role_id = $idRole;
        $fieldUserRoleTemplateEditorOrderDetail->enabled = true;
        $fieldUserRoleTemplateEditorOrderDetail->pos_x = 40;
        $fieldUserRoleTemplateEditorOrderDetail->pos_y = 10;
        $fieldUserRoleTemplateEditorOrderDetail->input_type = 7;
        $fieldUserRoleTemplateEditorOrderDetail->required = true;
        $fieldUserRoleTemplateEditorOrderDetail->created_id = $idUser;
        $fieldUserRoleTemplateEditorOrderDetail->save();

        #endregion ORDER

        #region HTML

        $fieldTemplateEditorHtmlDetail = new Field();
        $fieldTemplateEditorHtmlDetail->code = 'Html';
        $fieldTemplateEditorHtmlDetail->field = 'html';
        $fieldTemplateEditorHtmlDetail->data_type = 1;
        $fieldTemplateEditorHtmlDetail->tab_id = $tabTemplateEditor->id;
        $fieldTemplateEditorHtmlDetail->created_id = $idUser;
        $fieldTemplateEditorHtmlDetail->save();

        $fieldLanguageTemplateEditorHtmlDetail = new FieldLanguage();
        $fieldLanguageTemplateEditorHtmlDetail->field_id = $fieldTemplateEditorHtmlDetail->id;
        $fieldLanguageTemplateEditorHtmlDetail->language_id = $idLanguage;
        $fieldLanguageTemplateEditorHtmlDetail->description = 'Html';
        $fieldLanguageTemplateEditorHtmlDetail->created_id = $idUser;
        $fieldLanguageTemplateEditorHtmlDetail->save();

        $fieldUserRoleTemplateEditorHtmlDetail = new FieldUserRole();
        $fieldUserRoleTemplateEditorHtmlDetail->field_id = $fieldTemplateEditorHtmlDetail->id;
        $fieldUserRoleTemplateEditorHtmlDetail->role_id = $idRole;
        $fieldUserRoleTemplateEditorHtmlDetail->enabled = true;
        $fieldUserRoleTemplateEditorHtmlDetail->pos_x = 10;
        $fieldUserRoleTemplateEditorHtmlDetail->pos_y = 20;
        $fieldUserRoleTemplateEditorHtmlDetail->input_type = 17;
        $fieldUserRoleTemplateEditorHtmlDetail->required = true;
        $fieldUserRoleTemplateEditorHtmlDetail->created_id = $idUser;
        $fieldUserRoleTemplateEditorHtmlDetail->save();

        #endregion HTML

        #endregion DETAIL

        #endregion TEMPLATE EDITOR

        #region TEMPLATE EDITOR GROUP

        #region FUNZIONE

        $functionalityTemplateEditorGroup = new Functionality();
        $functionalityTemplateEditorGroup->code = 'TemplateEditorGroup';
        $functionalityTemplateEditorGroup->created_id = $idUser;
        $functionalityTemplateEditorGroup->save();

        #endregion FUNZIONE

        #region ICONA

        $iconServer = new Icon();
        $iconServer->description = 'Server';
        $iconServer->css = 'fa fa-server';
        $iconServer->created_id = $idUser;
        $iconServer->save();

        #endregion ICONA

        #region MENU

        $menuAdminTemplateEditorGroup = new MenuAdmin();
        $menuAdminTemplateEditorGroup->code = 'TemplateEditorGroup';
        $menuAdminTemplateEditorGroup->functionality_id = $functionalityTemplateEditorGroup->id;
        $menuAdminTemplateEditorGroup->icon_id = $iconServer->id;
        $menuAdminTemplateEditorGroup->url = '/admin/setting/templateEditorGroup';
        $menuAdminTemplateEditorGroup->created_id = $idUser;
        $menuAdminTemplateEditorGroup->save();

        $languageMenuAdminTemplateEditorGroup = new LanguageMenuAdmin();
        $languageMenuAdminTemplateEditorGroup->language_id = $idLanguage;
        $languageMenuAdminTemplateEditorGroup->menu_admin_id = $menuAdminTemplateEditorGroup->id;
        $languageMenuAdminTemplateEditorGroup->description = 'Gruppi blocchi';
        $languageMenuAdminTemplateEditorGroup->created_id = $idUser;
        $languageMenuAdminTemplateEditorGroup->save();

        $menuAdminUserRoleTemplateEditorGroup = new MenuAdminUserRole();
        $menuAdminUserRoleTemplateEditorGroup->menu_admin_id = $menuAdminTemplateEditorGroup->id;
        $menuAdminUserRoleTemplateEditorGroup->menu_admin_father_id = $menuAdminTemplate->id;
        $menuAdminUserRoleTemplateEditorGroup->role_id = $idRole;
        $menuAdminUserRoleTemplateEditorGroup->enabled = true;
        $menuAdminUserRoleTemplateEditorGroup->order = 110;
        $menuAdminUserRoleTemplateEditorGroup->created_id = $idUser;
        $menuAdminUserRoleTemplateEditorGroup->save();

        #endregion MENU

        #region TAB TEMPLATE EDITOR GROUP

        $tabTemplateEditorGroup = new Tab();
        $tabTemplateEditorGroup->code = 'TemplateEditor';
        $tabTemplateEditorGroup->functionality_id = $functionalityTemplateEditorGroup->id;
        $tabTemplateEditorGroup->order = 10;
        $tabTemplateEditorGroup->created_id = $idUser;
        $tabTemplateEditorGroup->save();

        $languageTabTemplateEditorGroup = new LanguageTab();
        $languageTabTemplateEditorGroup->language_id = $idLanguage;
        $languageTabTemplateEditorGroup->tab_id = $tabTemplateEditorGroup->id;
        $languageTabTemplateEditorGroup->description = 'Blocchi';
        $languageTabTemplateEditorGroup->created_id = $idUser;
        $languageTabTemplateEditorGroup->save();

        #endregion TAB TEMPLATE EDITOR GROUP

        #region TABLE

        #region CHECKBOX

        $fieldTemplateEditorGroupCheckBox = new Field();
        $fieldTemplateEditorGroupCheckBox->tab_id = $tabTemplateEditorGroup->id;
        $fieldTemplateEditorGroupCheckBox->code = 'TableCheckBox';
        $fieldTemplateEditorGroupCheckBox->created_id = $idUser;
        $fieldTemplateEditorGroupCheckBox->save();

        $fieldLanguageTemplateEditorGroupCheckBox = new FieldLanguage();
        $fieldLanguageTemplateEditorGroupCheckBox->field_id = $fieldTemplateEditorGroupCheckBox->id;
        $fieldLanguageTemplateEditorGroupCheckBox->language_id = $idLanguage;
        $fieldLanguageTemplateEditorGroupCheckBox->description = 'CheckBox';
        $fieldLanguageTemplateEditorGroupCheckBox->created_id = $idUser;
        $fieldLanguageTemplateEditorGroupCheckBox->save();

        $fieldUserRoleTemplateEditorGroupCheckBox = new FieldUserRole();
        $fieldUserRoleTemplateEditorGroupCheckBox->field_id = $fieldTemplateEditorGroupCheckBox->id;
        $fieldUserRoleTemplateEditorGroupCheckBox->role_id = $idRole;
        $fieldUserRoleTemplateEditorGroupCheckBox->enabled = true;
        $fieldUserRoleTemplateEditorGroupCheckBox->table_order = 10;
        $fieldUserRoleTemplateEditorGroupCheckBox->input_type = 13;
        $fieldUserRoleTemplateEditorGroupCheckBox->created_id = $idUser;
        $fieldUserRoleTemplateEditorGroupCheckBox->save();

        #endregion CHECKBOX

        #region FORMATTER

        $fieldTemplateEditorGroupFormatter = new Field();
        $fieldTemplateEditorGroupFormatter->code = 'TemplateEditorGroupFormatter';
        $fieldTemplateEditorGroupFormatter->tab_id = $tabTemplateEditorGroup->id;
        $fieldTemplateEditorGroupFormatter->created_id = $idUser;
        $fieldTemplateEditorGroupFormatter->save();

        $fieldLanguageTemplateEditorGroupFormatter = new FieldLanguage();
        $fieldLanguageTemplateEditorGroupFormatter->field_id = $fieldTemplateEditorGroupFormatter->id;
        $fieldLanguageTemplateEditorGroupFormatter->language_id = $idLanguage;
        $fieldLanguageTemplateEditorGroupFormatter->description = '';
        $fieldLanguageTemplateEditorGroupFormatter->created_id = $idUser;
        $fieldLanguageTemplateEditorGroupFormatter->save();

        $fieldUserRoleTemplateEditorGroupFormatter = new FieldUserRole();
        $fieldUserRoleTemplateEditorGroupFormatter->field_id = $fieldTemplateEditorGroupFormatter->id;
        $fieldUserRoleTemplateEditorGroupFormatter->role_id = $idRole;
        $fieldUserRoleTemplateEditorGroupFormatter->enabled = true;
        $fieldUserRoleTemplateEditorGroupFormatter->table_order = 20;
        $fieldUserRoleTemplateEditorGroupFormatter->input_type = 14;
        $fieldUserRoleTemplateEditorGroupFormatter->created_id = $idUser;
        $fieldUserRoleTemplateEditorGroupFormatter->save();

        #endregion FORMATTER

        #region CODE

        $fieldTemplateEditorGroupCode = new Field();
        $fieldTemplateEditorGroupCode->code = 'Code';
        $fieldTemplateEditorGroupCode->field = 'code';
        $fieldTemplateEditorGroupCode->data_type = 1;
        $fieldTemplateEditorGroupCode->tab_id = $tabTemplateEditorGroup->id;
        $fieldTemplateEditorGroupCode->created_id = $idUser;
        $fieldTemplateEditorGroupCode->save();

        $fieldLanguageTemplateEditorGroupCode = new FieldLanguage();
        $fieldLanguageTemplateEditorGroupCode->field_id = $fieldTemplateEditorGroupCode->id;
        $fieldLanguageTemplateEditorGroupCode->language_id = $idLanguage;
        $fieldLanguageTemplateEditorGroupCode->description = 'Codice';
        $fieldLanguageTemplateEditorGroupCode->created_id = $idUser;
        $fieldLanguageTemplateEditorGroupCode->save();

        $fieldUserRoleTemplateEditorGroupCode = new FieldUserRole();
        $fieldUserRoleTemplateEditorGroupCode->field_id = $fieldTemplateEditorGroupCode->id;
        $fieldUserRoleTemplateEditorGroupCode->role_id = $idRole;
        $fieldUserRoleTemplateEditorGroupCode->enabled = true;
        $fieldUserRoleTemplateEditorGroupCode->table_order = 30;
        $fieldUserRoleTemplateEditorGroupCode->input_type = 0;
        $fieldUserRoleTemplateEditorGroupCode->required = true;
        $fieldUserRoleTemplateEditorGroupCode->created_id = $idUser;
        $fieldUserRoleTemplateEditorGroupCode->save();

        #endregion CODE

        #region DESCRIPTION

        $fieldTemplateEditorGroupDescription = new Field();
        $fieldTemplateEditorGroupDescription->code = 'Description';
        $fieldTemplateEditorGroupDescription->field = 'description';
        $fieldTemplateEditorGroupDescription->data_type = 1;
        $fieldTemplateEditorGroupDescription->tab_id = $tabTemplateEditorGroup->id;
        $fieldTemplateEditorGroupDescription->created_id = $idUser;
        $fieldTemplateEditorGroupDescription->save();

        $fieldLanguageTemplateEditorGroupDescription = new FieldLanguage();
        $fieldLanguageTemplateEditorGroupDescription->field_id = $fieldTemplateEditorGroupDescription->id;
        $fieldLanguageTemplateEditorGroupDescription->language_id = $idLanguage;
        $fieldLanguageTemplateEditorGroupDescription->description = 'Descrizione';
        $fieldLanguageTemplateEditorGroupDescription->created_id = $idUser;
        $fieldLanguageTemplateEditorGroupDescription->save();

        $fieldUserRoleTemplateEditorGroupDescription = new FieldUserRole();
        $fieldUserRoleTemplateEditorGroupDescription->field_id = $fieldTemplateEditorGroupDescription->id;
        $fieldUserRoleTemplateEditorGroupDescription->role_id = $idRole;
        $fieldUserRoleTemplateEditorGroupDescription->enabled = true;
        $fieldUserRoleTemplateEditorGroupDescription->table_order = 40;
        $fieldUserRoleTemplateEditorGroupDescription->input_type = 0;
        $fieldUserRoleTemplateEditorGroupDescription->required = true;
        $fieldUserRoleTemplateEditorGroupDescription->created_id = $idUser;
        $fieldUserRoleTemplateEditorGroupDescription->save();

        #endregion DESCRIPTION

        #region ORDER

        $fieldTemplateEditorGroupOrder = new Field();
        $fieldTemplateEditorGroupOrder->code = 'Order';
        $fieldTemplateEditorGroupOrder->field = 'order';
        $fieldTemplateEditorGroupOrder->data_type = 0;
        $fieldTemplateEditorGroupOrder->tab_id = $tabTemplateEditorGroup->id;
        $fieldTemplateEditorGroupOrder->created_id = $idUser;
        $fieldTemplateEditorGroupOrder->save();

        $fieldLanguageTemplateEditorGroupOrder = new FieldLanguage();
        $fieldLanguageTemplateEditorGroupOrder->field_id = $fieldTemplateEditorGroupOrder->id;
        $fieldLanguageTemplateEditorGroupOrder->language_id = $idLanguage;
        $fieldLanguageTemplateEditorGroupOrder->description = 'Ordinamento';
        $fieldLanguageTemplateEditorGroupOrder->created_id = $idUser;
        $fieldLanguageTemplateEditorGroupOrder->save();

        $fieldUserRoleTemplateEditorGroupOrder = new FieldUserRole();
        $fieldUserRoleTemplateEditorGroupOrder->field_id = $fieldTemplateEditorGroupOrder->id;
        $fieldUserRoleTemplateEditorGroupOrder->role_id = $idRole;
        $fieldUserRoleTemplateEditorGroupOrder->enabled = true;
        $fieldUserRoleTemplateEditorGroupOrder->table_order = 50;
        $fieldUserRoleTemplateEditorGroupOrder->input_type = 7;
        $fieldUserRoleTemplateEditorGroupOrder->required = true;
        $fieldUserRoleTemplateEditorGroupOrder->created_id = $idUser;
        $fieldUserRoleTemplateEditorGroupOrder->save();

        #endregion ORDER

        #region TEMPLATE EDITOR TYPE

        $fieldTemplateEditorGroupType = new Field();
        $fieldTemplateEditorGroupType->code = 'Type';
        $fieldTemplateEditorGroupType->field = 'idTemplateEditorType';
        $fieldTemplateEditorGroupType->data_type = 0;
        $fieldTemplateEditorGroupType->service = 'templateEditorType';
        $fieldTemplateEditorGroupType->tab_id = $tabTemplateEditorGroup->id;
        $fieldTemplateEditorGroupType->created_id = $idUser;
        $fieldTemplateEditorGroupType->save();

        $fieldLanguageTemplateEditorGroupType = new FieldLanguage();
        $fieldLanguageTemplateEditorGroupType->field_id = $fieldTemplateEditorGroupType->id;
        $fieldLanguageTemplateEditorGroupType->language_id = $idLanguage;
        $fieldLanguageTemplateEditorGroupType->description = 'Tipologia';
        $fieldLanguageTemplateEditorGroupType->created_id = $idUser;
        $fieldLanguageTemplateEditorGroupType->save();

        $fieldUserRoleTemplateEditorGroupType = new FieldUserRole();
        $fieldUserRoleTemplateEditorGroupType->field_id = $fieldTemplateEditorGroupType->id;
        $fieldUserRoleTemplateEditorGroupType->role_id = $idRole;
        $fieldUserRoleTemplateEditorGroupType->enabled = true;
        $fieldUserRoleTemplateEditorGroupType->table_order = 50;
        $fieldUserRoleTemplateEditorGroupType->input_type = 2;
        $fieldUserRoleTemplateEditorGroupType->required = true;
        $fieldUserRoleTemplateEditorGroupType->created_id = $idUser;
        $fieldUserRoleTemplateEditorGroupType->save();

        #endregion TEMPLATE EDITOR TYPE

        #endregion TABLE

        #region DETAIL

        #region CODE

        $fieldTemplateEditorGroupCodeDetail = new Field();
        $fieldTemplateEditorGroupCodeDetail->code = 'Code';
        $fieldTemplateEditorGroupCodeDetail->field = 'code';
        $fieldTemplateEditorGroupCodeDetail->data_type = 1;
        $fieldTemplateEditorGroupCodeDetail->tab_id = $tabTemplateEditorGroup->id;
        $fieldTemplateEditorGroupCodeDetail->created_id = $idUser;
        $fieldTemplateEditorGroupCodeDetail->save();

        $fieldLanguageTemplateEditorGroupCodeDetail = new FieldLanguage();
        $fieldLanguageTemplateEditorGroupCodeDetail->field_id = $fieldTemplateEditorGroupCodeDetail->id;
        $fieldLanguageTemplateEditorGroupCodeDetail->language_id = $idLanguage;
        $fieldLanguageTemplateEditorGroupCodeDetail->description = 'Codice';
        $fieldLanguageTemplateEditorGroupCodeDetail->created_id = $idUser;
        $fieldLanguageTemplateEditorGroupCodeDetail->save();

        $fieldUserRoleTemplateEditorGroupCodeDetail = new FieldUserRole();
        $fieldUserRoleTemplateEditorGroupCodeDetail->field_id = $fieldTemplateEditorGroupCodeDetail->id;
        $fieldUserRoleTemplateEditorGroupCodeDetail->role_id = $idRole;
        $fieldUserRoleTemplateEditorGroupCodeDetail->enabled = true;
        $fieldUserRoleTemplateEditorGroupCodeDetail->pos_x = 10;
        $fieldUserRoleTemplateEditorGroupCodeDetail->pos_y = 10;
        $fieldUserRoleTemplateEditorGroupCodeDetail->input_type = 0;
        $fieldUserRoleTemplateEditorGroupCodeDetail->required = true;
        $fieldUserRoleTemplateEditorGroupCodeDetail->created_id = $idUser;
        $fieldUserRoleTemplateEditorGroupCodeDetail->save();

        #endregion CODE

        #region DESCRIPTION

        $fieldTemplateEditorGroupDescriptionDetail = new Field();
        $fieldTemplateEditorGroupDescriptionDetail->code = 'Description';
        $fieldTemplateEditorGroupDescriptionDetail->field = 'description';
        $fieldTemplateEditorGroupDescriptionDetail->data_type = 1;
        $fieldTemplateEditorGroupDescriptionDetail->tab_id = $tabTemplateEditorGroup->id;
        $fieldTemplateEditorGroupDescriptionDetail->created_id = $idUser;
        $fieldTemplateEditorGroupDescriptionDetail->save();

        $fieldLanguageTemplateEditorGroupDescriptionDetail = new FieldLanguage();
        $fieldLanguageTemplateEditorGroupDescriptionDetail->field_id = $fieldTemplateEditorGroupDescriptionDetail->id;
        $fieldLanguageTemplateEditorGroupDescriptionDetail->language_id = $idLanguage;
        $fieldLanguageTemplateEditorGroupDescriptionDetail->description = 'Descrizione';
        $fieldLanguageTemplateEditorGroupDescriptionDetail->created_id = $idUser;
        $fieldLanguageTemplateEditorGroupDescriptionDetail->save();

        $fieldUserRoleTemplateEditorGroupDescriptionDetail = new FieldUserRole();
        $fieldUserRoleTemplateEditorGroupDescriptionDetail->field_id = $fieldTemplateEditorGroupDescriptionDetail->id;
        $fieldUserRoleTemplateEditorGroupDescriptionDetail->role_id = $idRole;
        $fieldUserRoleTemplateEditorGroupDescriptionDetail->enabled = true;
        $fieldUserRoleTemplateEditorGroupDescriptionDetail->pos_x = 20;
        $fieldUserRoleTemplateEditorGroupDescriptionDetail->pos_y = 10;
        $fieldUserRoleTemplateEditorGroupDescriptionDetail->input_type = 0;
        $fieldUserRoleTemplateEditorGroupDescriptionDetail->required = true;
        $fieldUserRoleTemplateEditorGroupDescriptionDetail->created_id = $idUser;
        $fieldUserRoleTemplateEditorGroupDescriptionDetail->save();

        #endregion DESCRIPTION

        #region TYPE

        $fieldTemplateEditorGroupTypeDetail = new Field();
        $fieldTemplateEditorGroupTypeDetail->code = 'Type';
        $fieldTemplateEditorGroupTypeDetail->field = 'idTemplateEditorType';
        $fieldTemplateEditorGroupTypeDetail->data_type = 0;
        $fieldTemplateEditorGroupTypeDetail->service = 'templateEditorType';
        $fieldTemplateEditorGroupTypeDetail->tab_id = $tabTemplateEditorGroup->id;
        $fieldTemplateEditorGroupTypeDetail->created_id = $idUser;
        $fieldTemplateEditorGroupTypeDetail->save();

        $fieldLanguageTemplateEditorGroupTypeDetail = new FieldLanguage();
        $fieldLanguageTemplateEditorGroupTypeDetail->field_id = $fieldTemplateEditorGroupTypeDetail->id;
        $fieldLanguageTemplateEditorGroupTypeDetail->language_id = $idLanguage;
        $fieldLanguageTemplateEditorGroupTypeDetail->description = 'Tipologia';
        $fieldLanguageTemplateEditorGroupTypeDetail->created_id = $idUser;
        $fieldLanguageTemplateEditorGroupTypeDetail->save();

        $fieldUserRoleTemplateEditorGroupTypeDetail = new FieldUserRole();
        $fieldUserRoleTemplateEditorGroupTypeDetail->field_id = $fieldTemplateEditorGroupTypeDetail->id;
        $fieldUserRoleTemplateEditorGroupTypeDetail->role_id = $idRole;
        $fieldUserRoleTemplateEditorGroupTypeDetail->enabled = true;
        $fieldUserRoleTemplateEditorGroupTypeDetail->pos_x = 30;
        $fieldUserRoleTemplateEditorGroupTypeDetail->pos_y = 10;
        $fieldUserRoleTemplateEditorGroupTypeDetail->input_type = 2;
        $fieldUserRoleTemplateEditorGroupTypeDetail->required = true;
        $fieldUserRoleTemplateEditorGroupTypeDetail->created_id = $idUser;
        $fieldUserRoleTemplateEditorGroupTypeDetail->save();

        #endregion TYPE

        #region ORDER

        $fieldTemplateEditorGroupOrderDetail = new Field();
        $fieldTemplateEditorGroupOrderDetail->code = 'Order';
        $fieldTemplateEditorGroupOrderDetail->field = 'order';
        $fieldTemplateEditorGroupOrderDetail->data_type = 0;
        $fieldTemplateEditorGroupOrderDetail->tab_id = $tabTemplateEditorGroup->id;
        $fieldTemplateEditorGroupOrderDetail->created_id = $idUser;
        $fieldTemplateEditorGroupOrderDetail->save();

        $fieldLanguageTemplateEditorOrderTypeDetail = new FieldLanguage();
        $fieldLanguageTemplateEditorOrderTypeDetail->field_id = $fieldTemplateEditorGroupOrderDetail->id;
        $fieldLanguageTemplateEditorOrderTypeDetail->language_id = $idLanguage;
        $fieldLanguageTemplateEditorOrderTypeDetail->description = 'Ordinamento';
        $fieldLanguageTemplateEditorOrderTypeDetail->created_id = $idUser;
        $fieldLanguageTemplateEditorOrderTypeDetail->save();

        $fieldUserRoleTemplateEditorGroupOrderDetail = new FieldUserRole();
        $fieldUserRoleTemplateEditorGroupOrderDetail->field_id = $fieldTemplateEditorGroupOrderDetail->id;
        $fieldUserRoleTemplateEditorGroupOrderDetail->role_id = $idRole;
        $fieldUserRoleTemplateEditorGroupOrderDetail->enabled = true;
        $fieldUserRoleTemplateEditorGroupOrderDetail->pos_x = 40;
        $fieldUserRoleTemplateEditorGroupOrderDetail->pos_y = 10;
        $fieldUserRoleTemplateEditorGroupOrderDetail->input_type = 7;
        $fieldUserRoleTemplateEditorGroupOrderDetail->required = true;
        $fieldUserRoleTemplateEditorGroupOrderDetail->created_id = $idUser;
        $fieldUserRoleTemplateEditorGroupOrderDetail->save();

        #endregion ORDER

        #endregion DETAIL

        #endregion TEMPLATE EDITOR GROUP
    }
}

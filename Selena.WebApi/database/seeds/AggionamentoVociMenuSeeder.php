<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\MenuAdmin;
use App\Permission;
use App\LanguageMenuAdmin;
use App\Enums\PageTypesEnum;
use App\MenuAdminUserRole;
use App\Language;
use App\Icon;
use App\Functionality;
use App\Field;
use App\FieldLanguage;
use App\FaqAdmin;
use App\GroupNewsletter;
use App\FieldUserRole;
use App\Role;
use App\User;
use App\Helpers\LogHelper;
use App\Tab;
use App\LanguageTab;
use App\Setting;
use App\Dictionary;
use App\Content;
use App\ContentLanguage;
use Illuminate\Support\Facades\DB;
use App\TemplateEditorType;
use App\TemplateEditorGroup;
use App\TemplateEditor;
use App\DocumentTypeLanguage;
use App\DocumentType;
use App\DocumentRowTypeLanguage;
use App\DocumentRowType;
use App\Enums\ContentEnum;
use App\Enums\DocumentRowTypeEnum;
use App\Enums\DocumentTypeEnum;
use App\Carriage;
use App\CarriageLanguage;
use App\Message;
use App\MessageLanguage;
use App\ImageResize;
use App\FormBuilderFieldType;

class AggiornamentoVociMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
      
        $ConfigurationSettings= Setting::firstOrCreate(['code'   => 'Script Cookiebot'],['value'   => '-', 'description' => 'Script Cookiebot']);
        $ConfigurationSettings= Setting::firstOrCreate(['code'   => 'Favicon Configuration'],['value'   => '-', 'description' => 'Favicon Configuration']);
        $ConfigurationSettings= Setting::firstOrCreate(['code'   => 'SidebarWidthMedium'],['value'   => '4', 'description' => 'Larghezza della sidebar per dispositivi Tablet (numero di colonne)']);
        $ConfigurationSettings= Setting::firstOrCreate(['code'   => 'SidebarWidthSmall'],['value'   => '12', 'description' => 'Larghezza della sidebar per dispositivi Smartphone (numero di colonne)']);

        $ConfigurationSettings= Setting::firstOrCreate(['code'   => 'UsernameProviderMessaggi'],
        ['value'   => 'tecnotrade', 'description' => 'Username per la gestione invio SMS']);

        $ConfigurationSettings= Setting::firstOrCreate(['code'   => 'PasswordProviderMessaggi'],
        ['value'   => 'tecnotrade', 'description' => 'Password per la gestione invio SMS']);

        $ConfigurationSettings= Setting::firstOrCreate(['code'   => 'SenderProviderMessaggi'],
        ['value'   => 'tecnotrade', 'description' => 'Mittente per la gestione invio SMS']);

        $ConfigurationSettings= Setting::firstOrCreate(['code'   => 'TokenAPITinyurl'],
        ['value'   => 'tRw1K9s4u4fdkbMuBmMZDGhKV3Gt7EmvydDJluNhi3f9TtL0LQf9qIrHSoXB', 'description' => 'Chiave di accesso per API TinyUrl']);

        $parameterNewsletterBlockDelay= Setting::firstOrCreate(['code'   => 'NewsletterBlockDelay'],
        ['value'   => '65', 'description' => 'Parametro impostazione tempo in secondi']);

         $parameterNewsletterBlockItems= Setting::firstOrCreate(['code'   => 'NewsletterBlockItems'],
        ['value'   => '2', 'description' => 'Parametro impostazione range invio mail']);


        $ClientIdPayPal= Setting::firstOrCreate(['code'   => 'ClientIdPayPal'],
        ['value'   => 'AaHYlKDmrvm9OYdgi5798WWsiS-bDQuRP10NdcIAgnLFBK8fOllaq8USc_MofmoFqicz7Q7NYIZfSjXA', 'description' => 'Client ID account pay pal']);

        $SecretPayPal= Setting::firstOrCreate(['code'   => 'SecretPayPal'],
        ['value'   => 'EG6xEdNRTKprjckIzBELKxqhzuB9-tGvgNIw3MZ8xiQYWCd3WY3_yuQ7qfO4KpECAvd0V5PAnrgo-LBg', 'description' => 'Secret key account pay pal']);

        $TestModePayPal= Setting::firstOrCreate(['code'   => 'TestModePayPal'],
        ['value'   => 'true', 'description' => 'Messa online account paypal (se false il pagamento viene reindirizzato su Sandbox la piattaforma di test di paypal)']);

        $DisplayPrice= Setting::firstOrCreate(['code'   => 'DisplayPrice'],
        ['value'   => 'BARRATO', 'description' => 'Metodologia di visualizzazione prezzo in catalogo: NESSUNA (Viene visualizzato il prezzo in anagrafica) - BARRATO (Viene visualizzato il prezzo base e il prezzo scontato se diverso) - CALCOLATO (Viene visualizzato solo il prezzo calcolato)']);

        $DateFormat= Setting::firstOrCreate(['code'   => 'DateFormat'],
        ['value'   => 'd/m/Y', 'description' => 'Formattazione data)']);

        $HomeBlogList= Setting::firstOrCreate(['code'   => 'HomeBlogList'],
        ['value'   => '4', 'description' => 'HomeBlogList']);

        $BlogList= Setting::firstOrCreate(['code'   => 'BlogList'],
        ['value'   => '10', 'description' => 'BlogList)']);

        $NewsList= Setting::firstOrCreate(['code'   => 'NewsList'],
        ['value'   => '10', 'description' => 'NewsList']);

        $BlogRender= Setting::firstOrCreate(['code'   => 'BlogRender'],
        ['value'   => 'Box', 'description' => 'BlogRender']);

        $AppIdFacebook= Setting::firstOrCreate(['code'   => 'AppIdFacebook'],
        ['value'   => '327798609408884', 'description' => 'AppIdFacebook']);

        $AppIdGoogle= Setting::firstOrCreate(['code'   => 'AppIdGoogle'],
        ['value'   => '916287020959-2p494jgr2019hv7rrtiud6s0b8ncgb6i', 'description' => 'AppIdGoogle']);

        $HomeBlogList= Setting::firstOrCreate(['code'   => 'HomeBlogList'],
        ['value'   => '4', 'description' => 'HomeBlogList']);

        $ForceRedirectOnLogin= Setting::firstOrCreate(['code'   => 'Force Redirect On Login'],
        ['value'   => 'true', 'description' => 'Force Redirect On Login']);


        $GroupNewsletterInglese= GroupNewsletter::firstOrCreate(['description' => 'PaperInglese'], ['created_id' => $idUser]);

        $DictionariesBn= Dictionary::firstOrCreate(['code'   => 'BusinessNameNotValued'], ['language_id' => $idLanguage, 'description' => 'Ragione Sociale non valorizzata', 'created_id' => $idUser]);
        $DictionariesCheck= Dictionary::firstOrCreate(['code'   => 'CheckProcessingOfPersonalDataNotValued'], ['language_id' => $idLanguage, 'description' => 'Check non valorizzato', 'created_id' => $idUser]);
        $DictionariesCheck= Dictionary::firstOrCreate(['code'   => 'PhoneNotValued'], ['language_id' => $idLanguage, 'description' => 'Telefono non valorizzato', 'created_id' => $idUser]);

   
        /* CONTENT PER INVIO EMAIL ALL'UTENTE PER SCARICARE IL DOWLOAD DEL PAPER */
        $message = Message::updateOrCreate(['code'   => 'EMAIL_DOWNLOAD_PAPER_TEXT'],
        [ 
            'message_type_id' => 1,
            'created_id' => $idUser
        ]);
        $messageLanguage = MessageLanguage::firstOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<p><img src="/storage/images/2021/11/logo.webp" class="fr-fic fr-dib"></p>
            <h3 style="text-align: center;"><strong>Gentile #name# #surname#,</strong></h3>
            <h3 style="text-align: center;"><strong>Grazie per la tua richiesta, scarica subito il tuo paper gratuito!</strong></h3>
            <h3 style="text-align: center;"><a class="button" href="#link_paper#">CLICCA QUI PER SCARICARE IL PAPER</a></h3>',
            'created_id' => $idUser
        ]);

        $message = Message::updateOrCreate(['code'   => 'CONTENT_NOT_FOUND'],
        [ 
            'message_type_id' => 1,
            'created_id' => $idUser
        ]);
        $messageLanguage = MessageLanguage::firstOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row"><div class="col-12"><h4>Nessuna informazione trovata</h4></div></div>',
            'created_id' => $idUser
        ]);
        

        $message = Message::updateOrCreate(['code'   => 'EMAIL_DOWNLOAD_PAPER_OBJECT'],
        [ 
            'message_type_id' => 1,
            'created_id' => $idUser
        ]);
        $messageLanguage = MessageLanguage::firstOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => 'Scarica subito il paper',
            'created_id' => $idUser
        ]);
    /* CONTENT PER INVIO EMAIL ALL'UTENTE PER SCARICARE IL DOWLOAD DEL PAPER */


    /* SECTION INVIO EMAIL CV */
        $message = Message::updateOrCreate(['code'   => 'CONFIRM_EMAIL_RECEIVED_CV_TEXT'],
        [ 
            'message_type_id' => 1,
            'created_id' => $idUser
        ]);
        $messageLanguage = MessageLanguage::firstOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'description' => 'Email di conferma e ringraziamento utente per la ricezione del suo personale cv',
            'content'     => '<p>Gentile #email#,&nbsp;</p>
            <p>&nbsp;grazie per averci inoltrato il tuo Curriculum Vitae.&nbsp;</p>
            <p>Cordiali Saluti</p>',
            'created_id' => $idUser
        ]);
    

        $message = Message::updateOrCreate(['code'   => 'CONFIRM_EMAIL_RECEIVED_CV_OBJECT'],
        [ 
            'message_type_id' => 1,
            'created_id' => $idUser
        ]);
        $messageLanguage = MessageLanguage::firstOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<p>Conferma invio CV</p>',
            'created_id' => $idUser
        ]);
        /* SECTION INVIO EMAIL CV */

        
        /*   Inserimento email messaggio invio stato cambio ordine section admin selena sezione ordini  */ 
        $message = Message::updateOrCreate(['code'   => 'ORDER_STATUS_MAIL_OBJECT'],
        [ 
            'message_type_id' => 1,
            'created_id' => $idUser
        ]);
        $messageLanguage = MessageLanguage::firstOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<p>Aggiornamenti sul tuo ordine nr.#nr_order#</p>',
            'created_id' => $idUser
        ]);

        $message = Message::updateOrCreate(['code'   => 'ORDER_STATUS_CHANGE_TEXT'],
        [ 
            'message_type_id' => 1,
            'created_id' => $idUser
        ]);
        $messageLanguage = MessageLanguage::firstOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'content' => '<div style="width:100%;background:#ededed;margin:0 !important;padding:20px 20px 60px 20px; text-align: center;"><img src="https://selena.tecnotrade.com/storage/images/logo.png" style="width:350px;" class="fr-fic fr-dib">
            <table align="center" open="" style="border-collapse:collapse; background:#fff;border-radius:.4rem; margin-top:1rem; font-family:\'Open Sans\';" width="640">
                <tbody>
                    <tr>
                        <td>
                            <div style="background: rgb(119, 136, 153); border-radius: 0.4rem 0.4rem 0px 0px; max-height: 75px; height: 75px; width: 640px; position: relative; text-align: center;">
                                <p style="color: rgb(255, 255, 255); font-size: 23px; position: absolute; left: 120px; display: inline-block; text-align: center;"><span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Ordine nr. #nr_order#<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;del #date_order#</span></p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="padding:40px;color:#5a5a5a;">
                                <p style="font-size:16px;">Gentile #emailuser#,</p>
                                <p style="font-size:16px;">lo stato dell&#39;ordine numero #nr_order# &nbsp;risulta : #statesOrder#</p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        ',
            'created_id' => $idUser
        ]);
     /*   Inserimento email messaggio invio stato cambio ordine section admin selena sezione ordini */ 



     /* richieste di contatto form invio email testo su messaggi configurabile come si vuole traduzioni in italiano e inglese */
       $idLanguageInglese = Language::where('description', '=', 'English')->first()->id;
       $message = Message::updateOrCreate(['code'   => 'CONFIRM_REQUEST_CONTACT_TEXT'],
       [ 
           'message_type_id' => 1,
           'created_id' => $idUser
       ]);
       $messageLanguage = MessageLanguage::firstOrCreate(
       [
           'message_id'   => $message->id, 
           'language_id' => $idLanguageInglese
       ],
       [
           'content'     => '<p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; color: rgb(65, 65, 65); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">Hi <strong style="box-sizing: border-box; font-weight: 700;">#name# #surname#,</strong></p><p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; color: rgb(65, 65, 65); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;">Thanks for writing to us, we will contact you shortly!</p>',
           'created_id' => $idUser
       ]);

       $message = Message::updateOrCreate(['code'   => 'CONFIRM_REQUEST_CONTACT_OBJECT'],
       [ 
           'message_type_id' => 1,
           'created_id' => $idUser
       ]);
       $messageLanguage = MessageLanguage::firstOrCreate(
       [
           'message_id'   => $message->id, 
           'language_id' => $idLanguageInglese
       ],
       [
           'content'     => '<p>Contact request from the site</p>',
           'created_id' => $idUser
       ]);

       $message = Message::updateOrCreate(['code'   => 'CONFIRM_REQUEST_INFORMATION_CONTACT_TEXT'],
       [ 
           'message_type_id' => 1,
           'created_id' => $idUser
       ]);
       $messageLanguage = MessageLanguage::firstOrCreate(
       [
           'message_id'   => $message->id, 
           'language_id' => $idLanguageInglese
       ],
       [
           'content'     => '<p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; color: rgb(65, 65, 65); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><strong style="box-sizing: border-box; font-weight: 700;">A contact request has arrived from:</strong></p>
           <p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; color: rgb(65, 65, 65); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><strong style="box-sizing: border-box; font-weight: 700;">#name# #surname#&nbsp;</strong></p>
           <p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; color: rgb(65, 65, 65); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><strong style="box-sizing: border-box; font-weight: 700;">#email#</strong></p>
           <p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; color: rgb(65, 65, 65); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><strong style="box-sizing: border-box; font-weight: 700;">#object#</strong></p>
           <p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; color: rgb(65, 65, 65); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;"><strong style="box-sizing: border-box; font-weight: 700;">#description#</strong>.</p>',
           'created_id' => $idUser
       ]);

       $message = Message::updateOrCreate(['code'   => 'CONFIRM_REQUEST_INFORMATION_CONTACT_OBJECT'],
       [ 
           'message_type_id' => 1,
           'created_id' => $idUser
       ]);
       $messageLanguage = MessageLanguage::firstOrCreate(
       [
           'message_id'   => $message->id, 
           'language_id' => $idLanguageInglese
       ],
       [
           'content'     => '<p>Contact request from: #name# #surname# - #email#</p>',
           'created_id' => $idUser
       ]);
        /* richieste di contatto form invio email testo su messaggi  */

    
   /* html messaggi su richieste contatto in italiano */
       $message = Message::updateOrCreate(['code'   => 'CONFIRM_REQUEST_CONTACT_TEXT'],
       [ 
           'message_type_id' => 1,
           'created_id' => $idUser
       ]);
       $messageLanguage = MessageLanguage::firstOrCreate(
       [
           'message_id'   => $message->id, 
           'language_id' => $idLanguage
       ],
       [
           'content'     => '<p>Ciao <strong>#name# #surname#,</strong></p><p>Grazie per averci scritto, a breve ti ricontatteremo!</p>',
           'created_id' => $idUser
       ]);

       $message = Message::updateOrCreate(['code'   => 'CONFIRM_REQUEST_INFORMATION_CONTACT_TEXT'],
       [ 
           'message_type_id' => 1,
           'created_id' => $idUser
       ]);
       $messageLanguage = MessageLanguage::firstOrCreate(
       [
           'message_id'   => $message->id, 
           'language_id' => $idLanguage
       ],
       [
           'content'     => '<p>E&#39; arrivata una richiesta di contatto da:&nbsp;</p>
           <p><strong>#name# #surname#&nbsp;</strong></p>
           <p><strong>#email#</strong></p>  
           <p><strong>#object#</strong></p>          
           <p><strong>#description#</strong>.&nbsp;</p>',
           'created_id' => $idUser
       ]);
      
       $message = Message::updateOrCreate(['code'   => 'CONFIRM_REQUEST_INFORMATION_CONTACT_OBJECT'],
       [ 
           'message_type_id' => 1,
           'created_id' => $idUser
       ]);
       $messageLanguage = MessageLanguage::firstOrCreate(
       [
           'message_id'   => $message->id, 
           'language_id' => $idLanguage
       ],
       [
           'content'     => '<p>Richiesta di contatto da #name# #surname# - #email#</p>',
           'created_id' => $idUser
       ]);

       $message = Message::updateOrCreate(['code'   => 'CONFIRM_REQUEST_CONTACT_OBJECT'],
       [ 
           'message_type_id' => 1,
           'created_id' => $idUser
       ]);
       $messageLanguage = MessageLanguage::firstOrCreate(
       [
           'message_id'   => $message->id, 
           'language_id' => $idLanguage
       ],
       [
           'content'     => '<p>Richiesta di contatto dal sito</p>',
           'created_id' => $idUser
       ]);
   /* html messaggi su richieste contatto in italiano */


/* SECTION CONTENT */ 
        $content = Content::firstOrCreate(['code'   => 'BlogListCategory'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="col-12 col-md-4 text-left blog">
            <a href="#url#"><img src="#url_cover_image#" alt="#title#" class="fr-fic fr-dii"></a>
            <div class="row">
                <div class="col-12 text-left">
                    <a href="#url#">
        
                        <h3 style="color:#333333;padding-right: 37px;">#title#</h3>
                    </a></div></div>
            <div class="row">
                <div class="col-12" style="margin-top:12px;"><a class="bottone-blog" href="#url#">Leggi di pi&ugrave; <i class="fa fa-angle-double-right"></i></a></div></div></div>',
            'created_id' => $idUser
        ]); 


        $content = Content::firstOrCreate(['code'   => 'BlogArticlesCategoryList'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '#list_sub_category_blog#',
            'created_id' => $idUser
        ]); 

        $content = Content::firstOrCreate(['code'   => 'ContentHtmlBlogArticlesCategoryList'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="col-12 col-md-3"><div class="row"><div class="col-12" style="padding: 1rem !important;">#list_category_article_page_blog#</div></div></div>',
            'created_id' => $idUser
        ]);

        $content = Content::firstOrCreate(['code'   => 'UserList'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="col-12 col-md-3"><div class="row"><div class="col-12" style="padding: 1rem !important;">#business_name#</div></div></div>',
            'created_id' => $idUser
        ]);


        $content = Content::firstOrCreate(['code'   => 'CartDetailOptional'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '#title_tipology_optional#: #description_id_optionalPrint#',
            'created_id' => $idUser
        ]); 


        $content = Content::firstOrCreate(['code'   => 'SalesDetailOptional'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '#title_tipology_optional#: #description_id_optionalPrint#',
            'created_id' => $idUser
        ]); 

        // Content per l'estrazione degli articoli di blog
        /*$content = Content::updateOrCreate(['code'   => 'ArticlesList'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="col-12 col-md-4"><img src="#url_cover_image#" style="width: 100%;" class="fr-fic fr-dii"><div class="row"><div class="col-12"><h3 style="font-weight:bold;">#title#</h3><p>#seo_description#</p><div class="text-right"><a href="#url#">Scopri di pi&ugrave;</a></div></div></div></div>',
            'created_id' => $idUser
        ]); */

        $content = Content::firstOrCreate(['code'   => 'GroupTechnicalsSpecifications'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => 'GroupTechnicalsSpecifications',
            'created_id' => $idUser
        ]);

        

        


        $content = Content::firstOrCreate(['code'   => 'ItemGroupTechnicalsSpecifications'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => 'ItemGroupTechnicalsSpecifications',
            'created_id' => $idUser
        ]);

        $content = Content::firstOrCreate(['code'   => 'BlogArticlesPage'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>#description#</h1>
                </div>
                <div class="col-12">#breadcrumbs#</div></div>
            <div class="row">
                <div class="col-12">
                    <div class="row">#list_article_blog#</div></div></div>
            <div class="row">
                <div class="col-4">
                    <div class="row">
                        <div class="col-12">#list_category_article_page_blog#</div>
                        </div>
                   </div>
                <div class="col-8">
                    <div class="row">#list_sub_category_blog#</div></div></div></div>',
            'created_id' => $idUser
        ]); 
    
        $content = Content::firstOrCreate(['code'   => 'ItemAttachmentType'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="container"><div class="row"><div class="col-12"><h3>#description#</h3></div><div class="col-12"><div class="row">#item_attachment_file#</div></div></div></div>',
            'created_id' => $idUser
        ]);

        $content = Content::firstOrCreate(['code'   => 'ItemAttachmentFile'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="container"><div class="row"><div class="col-12"><a href="#url#">#description#</a>#filesize#</div></div></div>',
            'created_id' => $idUser
        ]);


        $content = Content::firstOrCreate(['code'   => 'CategoriesFilter'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="container"><div class="row"><div class="col-12"><label>#description#</label>#input_filter#</div></div></div>',
            'created_id' => $idUser
        ]);



        
/* END SECTION CONTENT */ 




         // User Seeder
         /*    $user = User::updateOrCreate(
        [
            'name'   => 'admin', 
            'email' => 'support@tecnotrade.com'
        ],
        [
            'password' => bcrypt('admin'),
            'online' => true,
            'created_id' => $idUser
        ]);*/

           $userSyncro = User::updateOrCreate(
        [
            'name'   => 'syncro', 
            'email' => 'supporto@tecnotrade.com'
        ],
        [
            'password' => bcrypt('#5yNcR0$upP0rT0!'),
            'online' => true,
            'created_id' => $idUser
        ]);

          $userNewsletter = User::updateOrCreate(
        [
            'name'   => 'newsletter', 
            'email' => 'newsletter@tecnotrade.com'
        ],
        [
            'password' => bcrypt('$N3w5l3tt3r$upP0rT0)'),
            'online' => true,
            'created_id' => $idUser
        ]);
        // end User Seeder  

        // role seeder 
        $adminrole = Role::updateOrCreate(
        [
            'name'   => 'admin', 
            'display_name' => 'User Administrator'
        ],
        [
            'description' => 'User is allowed to manage and edit other users',
            'created_id' => $idUser
        ]);

         $customerrole = Role::updateOrCreate(
        [
            'name'   => 'customer', 
            'display_name' => 'User Customer'
        ],
        [
            'description' => 'User is allowed to access',
            'created_id' => User::where('name', '=', 'admin')->first()->id
        ]);

        $user = User::where('name', '=', 'admin')->firstOrFail();
        $user->attachRole($adminrole);
         // end role seeder

        $ConfigurationListNewsletterSubscruber= GroupNewsletter::updateorCreate(['description'   => 'SubscribedToTheNewsletter'],['created_id' => $idUser]);
        $ConfigurationListNewsletterSubscruber= GroupNewsletter::updateorCreate(['description'   => 'SubscribedToTheNewsletterInfo'],['created_id' => $idUser]);

          // PermissionSeeder 
    /*    $pages = Permission::updateOrCreate(
        [
            'name'   => 'pages', 
            'display_name' => 'Edit pages'
        ],
        [
            'description' => 'Edit pages admin role',
            'created_id' => User::where('name', 'admin')->first()->id
        ]);

            $admin = Role::where('name', 'admin')->first();
            $admin->attachPermission($pages);*/
         // end PermissionSeeder

         // LanguageSeeder
            $idUser = User::first()->id;

           $language = Language::firstOrCreate(
        [
            'description'   => 'Italiano', 
        ],
        [
            'default' => true,
            'date_format' => 'd/m/Y',
            'date_time_format' => 'd/m/Y H:i',
            'date_format_client' => 'DD/MM/YYYY',
            'date_time_format_client' => 'DD/MM/YYYY HH:mm',
            'created_id' => $idUser
        ]);  

        $user = User::first();
        $user->language_id = $language->id;
        $user->save();

        $user = User::where('name', 'syncro')->first();
        $user->language_id = $language->id;
        $user->save();

        $user = User::where('name', 'newsletter')->first();
        $user->language_id = $language->id;
        $user->save();

          $language = Language::firstOrCreate(
        [
            'description'   => 'English', 
        ],
        [
            'default' => false,
            'date_format' => 'Y/m/d',
            'date_time_format' => 'Y/m/d H:i',
            'date_format_client' => 'YYYY/MM/DD',
            'date_time_format_client' => 'YYYY/MM/DD HH:mm',
            'created_id' => $idUser
        ]);  
         // end LanguageSeeder


         // DOCUMENT TYPE SEEDER
        $documentTypeOffer = DocumentType::updateorCreate(['code'   => DocumentTypeEnum::Offer],[ 'created_id' => $idUser]);
        $documentTypeLanguageOffer = DocumentTypeLanguage::updateOrCreate(['document_type_id'   => $documentTypeOffer->id],
        ['description' => 'Offerte', 
        'language_id'=> $idLanguage, 
        'created_id' => $idUser]);

        $documentTypeOrder = DocumentType::updateorCreate(['code'   => DocumentTypeEnum::Order],[ 'created_id' => $idUser]);
        $documentTypeLanguageOrder = DocumentTypeLanguage::updateOrCreate(['document_type_id'   => $documentTypeOrder->id],
        ['description' => 'Ordine', 
        'language_id'=> $idLanguage, 
        'created_id' => $idUser]);

        $documentTypeBeforeDDT = DocumentType::updateorCreate(['code'   => DocumentTypeEnum::BeforeDDT],[ 'created_id' => $idUser]);
        $documentTypeLanguageBeforeDDT = DocumentTypeLanguage::updateOrCreate(['document_type_id'   => $documentTypeBeforeDDT->id],
        ['description' => 'Pre Bolla', 
        'language_id'=> $idLanguage, 
        'created_id' => $idUser]);

        $documentTypeDDT = DocumentType::updateorCreate(['code'   => DocumentTypeEnum::DDT],[ 'created_id' => $idUser]);
        $documentTypeLanguageDDT = DocumentTypeLanguage::updateOrCreate(['document_type_id'   => $documentTypeDDT->id],
        ['description' => 'Bolla', 
        'language_id'=> $idLanguage, 
        'created_id' => $idUser]);
        // END DOCUMENT TYPE SEEDER

        
        $groupNewsletter = GroupNewsletter::firstOrCreate(['description'   => 'SubscribedToTheNewsletter'],
        [ 'created_id' => $idUser]);

        //DOCUMENT ROW TYPE SEEDER
        $documentRowTypeItem = DocumentRowType::updateorCreate(['code'   => DocumentRowTypeEnum::Item],[ 'created_id' => $idUser]);        
        $documentRowTypeLanguageItem = DocumentRowTypeLanguage::updateOrCreate(['document_row_type_id'   => $documentRowTypeItem->id],
        ['description' => 'Articolo', 
        'language_id'=> $idLanguage, 
        'created_id' => $idUser]);
        //END DOCUMENT ROW TYPE SEEDER 

        //-----  section popular message   ----- //

        //PAGE NOT FOUND 404
        $message = Message::firstOrCreate(['code'   => 'ERROR_404_PAGE_NOT_FOUND'],
        [ 
            
            'message_type_id' => 1,
            'created_id' => $idUser
        ]);

        $messageLanguage = MessageLanguage::firstOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'description' => 'Contenuto visualizzato quando viene navigato un url non trovato',
            'content'     => '<section data-id-template-selena="36"><section style="padding: 7.5rem 0;"><div class="container"><div class="row justify-content-center"><div class="col-12 col-md-6 text-center"><h1>404<br>Ops... Non abbiamo trovato la pagina che stavi cercando.</h1></div></div></div></section></section>',
            'created_id' => $idUser
        ]);

        $message = Message::updateOrCreate(['code'   => 'message_user_not_enabled'],
        [ 
            
            'message_type_id' => 1,
            'created_id' => $idUser
        ]);

        $messageLanguage = MessageLanguage::updateOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'description' => 'messaggio di testo su popup personalizzato visibile quando provi ad eseguire il login',
            'content'     => 'Utente non abilitato',
            'created_id' => $idUser
        ]);

        $message = Message::updateOrCreate(['code'   => 'message_unconfirmed_account'],
        [ 
            
            'message_type_id' => 1,
            'created_id' => $idUser
        ]);

        $messageLanguage = MessageLanguage::updateOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'description' => 'messaggio di testo su popup personalizzato visibile quando provi ad eseguire il login',
            'content'     => 'Attenzione,non hai ancora confermato il tuo account',
            'created_id' => $idUser
        ]);

            $message = Message::updateOrCreate(['code'   => 'message_unauthorized_user'],
        [ 
            
            'message_type_id' => 1,
            'created_id' => $idUser
        ]);

        $messageLanguage = MessageLanguage::updateOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'description' => 'messaggio di testo su popup personalizzato visibile quando provi ad eseguire il login',
            'content'     => 'Utente non autorizzato',
            'created_id' => $idUser
        ]);
        //----- end section popular message   ----- //


        /*****************************************************************************/
        /* MENU CONTENUTI - PAGINE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Pages'],
        [ 
            'functionality_id' => Functionality::where('code', 'Pages')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-list-alt')->first()->id,
            'url'     => '/admin/pages',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Pagine',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'WebContents')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CONTENUTI - BLOG */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Blog'],
        [ 
            'functionality_id' => Functionality::where('code', 'Blog')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-newspaper-o')->first()->id,
            'url'     => '/admin/blog',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Blog',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'WebContents')->first()->id,
            'created_id' => $idUser
        ]);

    
        /************************************************************************ *****/
        /* MENU CONTENUTI - NEWS */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'News'],
        [ 
            'icon_id' => Icon::where('css', 'fa fa-television')->first()->id,
            'url'     => '/admin/news',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'News',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 40,
            'menu_admin_father_id' => MenuAdmin::where('code', 'WebContents')->first()->id,
            'created_id' => $idUser
        ]);


         // INIZIO CREAZIONE INPUT DATI PER SEZIONE PAGINE :  pageAccordion 
        $tabsTablePages= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Pages')->first()->id, 
            'code' => 'Content'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);           
        $tabsTablePagess = LanguageTab::updateOrCreate(['tab_id' => $tabsTablePages->id, 
       'language_id' => $idLanguage
        ],
        [ 
        'description' => 'Contenuto',
        'created_id' => $idUser]);     
       
        $field = Field::updateOrCreate(['tab_id'   => $tabsTablePages->id, 'code' => 'Content'
        ],
        [
        'field' => 'languagePageSetting.content', 
        'data_type' => 1,
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 
        'language_id' => $idLanguage],
        ['description' => 'Contenuto', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 1 ,
        'table_order'=> 10 ,
        'pos_x'=> 10 ,
        'pos_y'=> 10 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 
        

         $tabsTabAttribute= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Pages')->first()->id, 
            'code' => 'TabAttribute'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);           
       $tabsTableAttribute = LanguageTab::updateOrCreate(['tab_id' => $tabsTabAttribute->id, 
       'language_id' => $idLanguage
       ],
       [
        'description' => 'TabAttributi',
       'created_id' => $idUser]);     

        $fieldTitle= Field::updateOrCreate(['tab_id'   => $tabsTabAttribute->id,
        'code' => 'TitlePage',
        'field' => 'languagePageSetting.title',
        ],
        [
        'data_type' => 1,
        'required' => true, 
        'max_length' => 255,  
        'on_change' => 'changeTitle',
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitle->id, 
        'language_id' => $idLanguage
        ],
        [
        'description' => 'Titolo', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitle->id,
        'role_id' => $idRole
        ],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'table_order'=> 10 ,
        'pos_x'=> 10 ,
        'pos_y'=> 10 ,
        'colspan'=> 12,
        'required'=> true,
        'created_id' => $idUser]); 

      

        $fieldUrl = Field::updateOrCreate(['tab_id'   => $tabsTabAttribute->id,
        'code' => 'Url',
        'field' => 'languagePageSetting.url' 
        ],
        [
        'data_type' => 1,
        'max_length' => 255,  
        'on_change' => 'checkUrl', 
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUrl->id, 
        'language_id' => $idLanguage
        ],
        [
        'description' => 'Url', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUrl->id,
        'role_id' => $idRole
        ],
        [
        'enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> 10 ,
        'pos_y'=> 30 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 

        $fieldUrlCanonical = Field::updateOrCreate(['tab_id'   => $tabsTabAttribute->id,
        'code' => 'UrlCanonical',
        'field' => 'languagePageSetting.urlCanonical' 
        ],
        [ 
        'data_type' => '1',
        'max_length' => 255,  
        'on_change' => 'checkUrl', 
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUrlCanonical->id, 
        'language_id' => $idLanguage],
        [
        'description' => 'UrlCanonical',
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUrlCanonical->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=> 10 ,
        'pos_y'=> 40 ,
        'table_order'=> 40 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 

        $fieldseoTitle = Field::updateOrCreate(['tab_id'   => $tabsTabAttribute->id,
        'code' => 'seoTitle'
        ],
        [
        'data_type' => '1',
        'max_length' => 230, 
        'field' => 'languagePageSetting.seoTitle', 
        'on_change' => 'changeSeoTitle',
        'on_keyup' => 'keyupSeoTitle',  
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldseoTitle->id, 
        'language_id' => $idLanguage],
        ['description' => 'Titolo SEO', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldseoTitle->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0' ,
        'pos_x'=> 10 ,
        'pos_y'=> 50 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 

        $fieldseoDescription = Field::updateOrCreate(['tab_id'   => $tabsTabAttribute->id,
        'code' => 'SeoDescription'
        ],
        [
        'data_type' =>'1',
        'max_length' => 230, 
        'field' => 'languagePageSetting.seoDescription', 
        'on_change' => 'changeSeoDescription', 
        'on_keyup' => 'keyupSeoDescription',  
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldseoDescription->id, 
        'language_id' => $idLanguage],
        ['description' => 'Descrizione SEO', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldseoDescription->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '1' ,
        'pos_x'=> 10 ,
        'pos_y'=> 50 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 


        $fieldseoKeyword = Field::updateOrCreate(['tab_id'   => $tabsTabAttribute->id,
        'code' => 'SeoKeyword',
        'field' => 'languagePageSetting.SeoKeyword'
        ],
        [ 
        'data_type' => 1,
        'max_length' => 255,  
        'on_change' => 'checkUrl', 
        //'on_keyup' => 'keyupSeoKeyword',
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldseoKeyword->id, 
        'language_id' => $idLanguage],
        ['description' => 'Parole chiave SEO', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldseoKeyword->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> 10 ,
        'pos_y'=> 60 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 


        $tabsTabPublication= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Pages')->first()->id, 
            'code' => 'TabPublication'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);           
       $tabsTabPbblication = LanguageTab::updateOrCreate(['tab_id' => $tabsTabPublication->id, 
       'language_id' => $idLanguage],
       ['description' => 'TabPubblicazione',
       'created_id' => $idUser]);     
       

        $fielVisibleFrom = Field::updateOrCreate(['tab_id'   => $tabsTabPublication->id,
        'code' => 'VisibleFrom'
        ],
        [
        'field' => 'visibleFrom', 
        'data_type' => 2, 
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fielVisibleFrom->id, 
        'language_id' => $idLanguage],
        ['description' => 'Visibile dal', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fielVisibleFrom->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 4 ,
        'pos_x'=> 10 ,
        'pos_y'=> 40 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 
        
        $fielVisibleEnd = Field::updateOrCreate(['tab_id'   => $tabsTabPublication->id,
        'code' => 'VisibleEnd'
        ],
        [
        'field' => 'visibleEnd', 
        'data_type' => 2, 
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fielVisibleEnd->id, 
        'language_id' => $idLanguage],
        ['description' => 'Fino al', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fielVisibleEnd->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 4 ,
        'pos_x'=> 10 ,
        'pos_y'=> 50 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 

        $fieldHomePage = Field::updateOrCreate(['tab_id'   => $tabsTabPublication->id,     
        'code' => 'HomePage'
        ],
        [
        'field' => 'homePage', 
        'default_value' => 'true',
        'data_type' => 3, 
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldHomePage->id, 
        'language_id' => $idLanguage],
        ['description' => 'Imposta come home page', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldHomePage->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 5 ,
        'pos_x'=> 10 ,
        'pos_y'=> 70 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 

         $fieldHideHeader = Field::updateOrCreate(['tab_id'   => $tabsTabPublication->id,     
        'code' => 'HideHeader'
        ],
        [
        'field' => 'languagePageSetting.hideHeader', 
        'default_value' => 'true',
        'data_type' => 3, 
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldHideHeader->id, 
        'language_id' => $idLanguage],
        ['description' => 'Nascondi header', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldHideHeader->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 5 ,
        'pos_x'=> 10 ,
        'pos_y'=> 80 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 

       $fieldHideFooter = Field::updateOrCreate(['tab_id'   => $tabsTabPublication->id,     
        'code' => 'HideFooter'],
        [
        'field' => 'languagePageSetting.hideFooter', 
        'default_value' => 'true',
        'data_type' => 3,
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldHideFooter->id, 
        'language_id' => $idLanguage],
        ['description' => 'Nascondi footer', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldHideFooter->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 5 ,
        'pos_x'=> 10 ,
        'pos_y'=> 90 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 

         $fieldHideBreadcrumbs = Field::updateOrCreate(['tab_id'   => $tabsTabPublication->id,     
        'code' => 'HideBreadcrumb'
        ],
        [
        'field' => 'languagePageSetting.hideBreadcrumb', 
        'default_value' => 'true',
        'data_type' => 3,
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldHideBreadcrumbs->id, 
        'language_id' => $idLanguage],
        ['description' => 'Nascondi breadcrumbs', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldHideBreadcrumbs->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 5 ,
        'pos_x'=> 10 ,
        'pos_y'=> 100 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 

        $fieldHideMenu = Field::updateOrCreate(['tab_id'   => $tabsTabPublication->id,     
        'code' => 'HideMenu'],
        [
        'field' => 'hideMenu', 
        'default_value' => 'true',
        'data_type' => 3,
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldHideMenu->id, 
        'language_id' => $idLanguage],
        ['description' => 'Nascondi menu', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldHideMenu->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 5 ,
        'pos_x'=> 10 ,
        'pos_y'=> 110 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 
       

        $fieldHideMobileMenu = Field::updateOrCreate(['tab_id'   => $tabsTabPublication->id,     
        'code' => 'HideMobileMenu'
        ],
        [
        'field' => 'HideMobileMenu', 
        'default_value' => 'true',
        'data_type' => 3,
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldHideMobileMenu->id, 
        'language_id' => $idLanguage
        ],
        [
            'description' => 'Nascondi menu mobile', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldHideMobileMenu->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 5 ,
        'pos_x'=> 10 ,
        'pos_y'=> 120 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 

        $fieldHideFromSearchEngines = Field::updateOrCreate(['tab_id'   => $tabsTabPublication->id,     
        'code' => 'HideFromSearchEngines' ],
        [
        'field' => 'hideSearchEngine', 
        'default_value' => 'true',
        'data_type' => 3,
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldHideMobileMenu->id, 
        'language_id' => $idLanguage],
        ['description' => 'Nascondi dai motori di ricerca', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldHideMobileMenu->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 5 ,
        'pos_x'=> 10 ,
        'pos_y'=> 160 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 

      
        $TabSocialShare= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Pages')->first()->id, 
            'code' => 'TabSocialShare'
        ],
        [
            'order' => 30,
            'created_id' => $idUser
        ]);           
       $TabsSocialShare = LanguageTab::updateOrCreate(['tab_id' => $TabSocialShare->id, 
       'language_id' => $idLanguage],
       ['description' => 'TabSocialShare',
       'created_id' => $idUser]);     


        $fieldShareTitle= Field::updateOrCreate(['tab_id'   => $TabSocialShare->id,     
        'code' => 'ShareTitle'
        ],
        [
        'field' => 'languagePageSetting.shareTitle', 
        'default_value' => 'true',
        'data_type' => 1,
        'max_length' => 60,  
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldShareTitle->id, 
        'language_id' => $idLanguage
        ],
        ['description' => 'Titolo condivisione', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldShareTitle->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=> 10 ,
        'pos_y'=> 170 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 

        $fieldShareDescription= Field::updateOrCreate(['tab_id'   => $TabSocialShare->id,     
        'code' => 'ShareDescription'],
        [
        'field' => 'languagePageSetting.shareDescription', 
        'default_value' => 'true',
        'data_type' => 1,
        'max_length' => 230,  
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldShareDescription->id, 
        'language_id' => $idLanguage],
        ['description' => 'Descrizione condivisione', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldShareDescription->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=> 10 ,
        'pos_y'=> 180 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 

        $fieldCoverImage= Field::updateOrCreate(['tab_id'   => $TabSocialShare->id,     
        'code' => 'CoverImage' ],
        [
        'field' => 'languagePageSetting.urlCoverImage', 
        'default_value' => 'true',
        'data_type' => 1,
        'max_length' => 255,  
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCoverImage->id, 
        'language_id' => $idLanguage],
        ['description' => 'Url immagine condivisione', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCoverImage->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=> 10 ,
        'pos_y'=> 150 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 
        
        $TabSettings= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Pages')->first()->id, 
            'code' => 'Settings'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);           
        $TabsTabSettings = LanguageTab::updateOrCreate(['tab_id' => $TabSettings->id, 
        'language_id' => $idLanguage],
        ['description' => 'Impostazioni',
        'created_id' => $idUser]);      

        $fieldIdLanguage= Field::updateOrCreate(['tab_id'   => $TabSettings->id,     
        'code' => 'IdLanguage' ],
        [
        'field' => 'languagePageSetting.idLanguage', 
        'default_value' => 'true',
        'data_type' => 0,
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldIdLanguage->id, 
        'language_id' => $idLanguage],
        ['description' => 'Lingua', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldIdLanguage->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 19 ,
        'pos_x'=> 10 ,
        'pos_y'=> 10 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 

        $fieldCommandTable= Field::updateOrCreate(['tab_id'   => $TabSettings->id,     
        'code' => 'CommandTable' ],
        [
        'default_value' => 'true',
        'formatter' => 'tableFormatter',
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCommandTable->id, 
        'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCommandTable->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 14 ,
        'pos_x'=> 10 ,
        'pos_y'=> 10 ,
        'table_order'=> 10 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 

            $TabsFunctionality= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Pages')->first()->id, 
            'code' => 'TabFunctionality'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);           
        $TabsTabFunctionality = LanguageTab::updateOrCreate(['tab_id' => $TabsFunctionality->id, 
        'language_id' => $idLanguage],
        ['description' => 'Funzionalità',
        'created_id' => $idUser]);      

  
        $fieldEnableJS= Field::updateOrCreate(['tab_id'   => $TabsFunctionality->id,     
        'code' => 'EnableJS' ],
        [
         'field' => 'enableJS',   
        'default_value' => '0',
        'data_type' => 3,
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldEnableJS->id, 
        'language_id' => $idLanguage],
        ['description' => 'Abilita Javascript', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldEnableJS->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 5,
        'pos_x'=> 10,
        'pos_y'=> 1000,
        'created_id' => $idUser]); 


        $fieldProtected= Field::updateOrCreate(['tab_id'   => $TabsFunctionality->id,     
        'code' => 'Protected' ],
        [
        'field' => 'protected', 
        'default_value' => '0',
        'data_type' => 3,
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldProtected->id, 
        'language_id' => $idLanguage],
        ['description' => 'Richiedi accesso', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldProtected->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 5,
        'pos_x'=> 10,
        'pos_y'=> 1010,
        'created_id' => $idUser]); 

        $fieldPageType= Field::updateOrCreate(['tab_id'   => $tabsTabAttribute->id,     
        'code' => 'PageType' ],
        [
        'field' => 'pageType', 
        'default_value' => PageTypesEnum::Page,
        'data_type' => 0,
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldPageType->id, 
        'language_id' => $idLanguage],
        ['description' => 'Tipo pagina',
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldPageType->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 12,
        'pos_x'=> 10,
        'pos_y'=> 10,
        'created_id' => $idUser]); 
         // end pages 
         
         
        //blog
        $tabContentBlog = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Blog')->first()->id, 
            'code' => 'Content'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);    
        $tabTableContentBlog = LanguageTab::updateOrCreate(['tab_id' => $tabContentBlog->id,'language_id' => $idLanguage],
        ['description' => 'Contenuto','created_id' => $idUser]);  

        $tabAttributeBlog = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Blog')->first()->id, 
            'code' => 'TabAttribute'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);    
        $tabtabAttributeBlog = LanguageTab::updateOrCreate(['tab_id' => $tabAttributeBlog->id,'language_id' => $idLanguage],
        ['description' => 'TabAttributi','created_id' => $idUser]);  

        $fieldContentBlog = Field::updateOrCreate(['tab_id'   => $tabContentBlog->id, 
        'code' => 'Content'],
        ['created_id' => $idUser,
        'required'=> 't',        
        'field'=> 'languagePageSetting.content', 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldContentBlog->id,
         'language_id' => $idLanguage],
         ['description' => 'Contenuto', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldContentBlog->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '19',
        'pos_x'=> '10',
        'pos_y'=> '10',
        'colspan'=> '12',
        'created_id' => $idUser]);    

        $fieldTitlePageBlog = Field::updateOrCreate(['tab_id'   => $tabAttributeBlog->id, 
        'code' => 'TitlePage'],
        ['created_id' => $idUser,
        'formatter'=> 'titleFormatter', 
        'on_change'=> 'changeTitle', 
        'required'=> 't', 
        'field'=> 'languagePageSetting.title', 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitlePageBlog->id,
         'language_id' => $idLanguage],
         ['description' => 'Titolo', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitlePageBlog->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'required' => 't',
        'table_order' => 20,
        'pos_x' => '10',
        'pos_y' => '10',
        'colspan' => '12',
        'created_id' => $idUser]);  

        $fieldBlogseoTitle = Field::updateOrCreate(['tab_id'   => $tabAttributeBlog->id,
        'code' => 'seoTitle'
        ],
        [
        'data_type' => '1',
        'max_length' => 230, 
        'field' => 'languagePageSetting.seoTitle', 
        'on_change' => 'changeSeoTitle',
        'on_keyup' => 'keyupSeoTitle',
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldBlogseoTitle->id, 
        'language_id' => $idLanguage],
        ['description' => 'Titolo SEO', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldBlogseoTitle->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> 10 ,
        'pos_y'=> 20 ,
        'colspan'=> 12,
        'created_id' => $idUser]); 
        

       $fieldTitleSeoDescription = Field::updateOrCreate(['tab_id'   => $tabAttributeBlog->id, 
        'code' => 'SeoDescription'],
        ['created_id' => $idUser,
        'on_change'=> 'changeSeoDescription', 
        'on_keyup' => 'keyupSeoDescription',
        'required'=> 't', 
        'field'=> 'languagePageSetting.seoDescription', 
        'max_length'=> 230, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleSeoDescription->id,
         'language_id' => $idLanguage],
         ['description' => 'Descrizione SEO', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleSeoDescription->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '1',
        'required' => 't',
       // 'table_order' => 20,
        'pos_x' => '10',
        'pos_y' => '30',
        'colspan' => '12',
        'created_id' => $idUser]);  

        $fieldTitleSeoUrl = Field::updateOrCreate(['tab_id'   => $tabAttributeBlog->id, 
        'code' => 'Url'],
        ['created_id' => $idUser,
         'formatter'=> 'urlFormatter', 
         'on_change'=> 'checkUrl', 
        'required'=> 't', 
        'field'=> 'languagePageSetting.url', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleSeoUrl->id,
         'language_id' => $idLanguage],
         ['description' => 'Url', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleSeoUrl->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'required' => 't',
        'table_order' => 30,
        'pos_x' => '10',
        'pos_y' => '40',
        'colspan' => '12',
        'created_id' => $idUser]);  

        $fieldTitleSeoUrlCanonical = Field::updateOrCreate(['tab_id'   => $tabAttributeBlog->id, 
        'code' => 'UrlCanonical'],
        ['created_id' => $idUser,
         'formatter'=> 'urlFormatter', 
         'on_change'=> 'checkUrl', 
        'required'=> 't',     
        'field'=> 'languagePageSetting.urlCanonical', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleSeoUrlCanonical->id,
         'language_id' => $idLanguage],
         ['description' => 'Url Canonical', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleSeoUrlCanonical->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '50',
        'colspan' => '12',
        'class'=> 'urlcanonical', 
        'created_id' => $idUser]);  

        $fieldSeoKeyword= Field::updateOrCreate(['tab_id'   => $tabAttributeBlog->id, 
        'code' => 'SeoKeyword'],
        ['created_id' => $idUser,
        'required'=> 't', 
        'field'=> 'languagePageSetting.SeoKeyword', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldSeoKeyword->id,
         'language_id' => $idLanguage],
         ['description' => 'Parole chiave SEO', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldSeoKeyword->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'required' => 't',
       // 'table_order' => 50,
        'pos_x' => '10',
        'pos_y' => '60',
        'colspan' => '12',
        'created_id' => $idUser]);  


         $tabPubblicazioneBlog = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Blog')->first()->id, 
            'code' => 'TabPublication'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);    
        $tabtabPubblicazioneBlog = LanguageTab::updateOrCreate(['tab_id' => $tabPubblicazioneBlog->id,'language_id' => $idLanguage],
        ['description' => 'TabPubblicazione','created_id' => $idUser]);  

     /*   $fieldAuthor= Field::updateOrCreate(['tab_id'   => $tabPubblicazioneBlog->id, 
        'code' => 'Author'],
        ['created_id' => $idUser,
        'field'=> 'idAuthor', 
        'service'=> 'users', 
        'data_type' => '0']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldAuthor->id,
         'language_id' => $idLanguage],
         ['description' => 'Autore', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldAuthor->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '2',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '60',
        'colspan' => '12',
        'created_id' => $idUser]);  */

        /*$fieldPageCategory= Field::updateOrCreate(['tab_id'   => $tabPubblicazioneBlog->id, 
        'code' => 'PageCategory'],
        ['created_id' => $idUser,
        'field'=> 'idPageCategory', 
        'service'=> 'pageCategory', 
        'data_type' => '0']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldPageCategory->id,
         'language_id' => $idLanguage],
         ['description' => 'Categoria', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldPageCategory->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '2',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '70',
        'colspan' => '12',
        'created_id' => $idUser]);  */

        $fieldPublish= Field::updateOrCreate(['tab_id'   => $tabPubblicazioneBlog->id, 
        'code' => 'Publish'],
        ['created_id' => $idUser,
        'field'=> 'publish',  
        'data_type' => '2']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldPublish->id,
         'language_id' => $idLanguage],
         ['description' => 'Data di pubblicazione', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldPublish->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '4',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '80',
        'table_order' => 50,
        'colspan' => '12',
        'created_id' => $idUser]);  

        $fieldVisibleFrom= Field::updateOrCreate(['tab_id'   => $tabPubblicazioneBlog->id, 
        'code' => 'VisibleFrom'],
        ['created_id' => $idUser,
        'field'=> 'visibleFrom',  
        'data_type' => '2']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldVisibleFrom->id,
         'language_id' => $idLanguage],
         ['description' => 'Visibile dal', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldVisibleFrom->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '4',
        'required' => 't',
        'table_order' => 40,
        'pos_x' => '10',
        'pos_y' => '90',
        'colspan' => '12',
        'created_id' => $idUser]); 

        $fieldVisibleEnd= Field::updateOrCreate(['tab_id'   => $tabPubblicazioneBlog->id, 
        'code' => 'VisibleEnd'],
        ['created_id' => $idUser,
        'field'=> 'visibleEnd',  
        'data_type' => '2']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldVisibleEnd->id,
         'language_id' => $idLanguage],
         ['description' => 'Fino al', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldVisibleEnd->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '4',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '100',
        'colspan' => '12',
        'created_id' => $idUser]); 

        $fieldFixedPost= Field::updateOrCreate(['tab_id'   => $tabPubblicazioneBlog->id, 
        'code' => 'VisibleEnd'],
        ['created_id' => $idUser,
        'field'=> 'visibleEnd',  
        'data_type' => '2']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldFixedPost->id,
         'language_id' => $idLanguage],
         ['description' => 'Fino al', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldFixedPost->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '4',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '110',
        'colspan' => '12',
        'created_id' => $idUser]); 

        $fieldFixedPostEndDate= Field::updateOrCreate(['tab_id'   => $tabPubblicazioneBlog->id, 
        'code' => 'FixedEnd'],
        ['created_id' => $idUser,
        'field'=> 'fixedEnd',  
        'service'=> 'users', 
        'data_type' => '2']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldFixedPostEndDate->id,
         'language_id' => $idLanguage],
         ['description' => 'Data di fine post in alto', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldFixedPostEndDate->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '4',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '120',
        'colspan' => '12',
        'created_id' => $idUser]);

        $tabSocialShareBlog = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Blog')->first()->id, 
            'code' => 'TabSocialShare'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);    
        $tabTableSocialShareBlog = LanguageTab::updateOrCreate(['tab_id' => $tabSocialShareBlog->id,'language_id' => $idLanguage],
        ['description' => 'TabSocialShare','created_id' => $idUser]);  


        $fieldShareTitle= Field::updateOrCreate(['tab_id'   => $tabSocialShareBlog->id, 
        'code' => 'ShareTitle'],
        ['created_id' => $idUser,
        'field'=> 'languagePageSetting.shareTitle',  
        'max_length'=> '60', 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldShareTitle->id,
         'language_id' => $idLanguage],
         ['description' => 'Titolo condivisione', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldShareTitle->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '130',
        'colspan' => '12',
        'created_id' => $idUser]);

        $fieldShareDescription= Field::updateOrCreate(['tab_id'   => $tabSocialShareBlog->id, 
        'code' => 'ShareDescription'],
        ['created_id' => $idUser,
        'field'=> 'languagePageSetting.shareDescription',  
        'max_length'=> '230', 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldShareDescription->id,
         'language_id' => $idLanguage],
         ['description' => 'Descrizione condivisione', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldShareDescription->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '140',
        'colspan' => '12',
        'created_id' => $idUser]);

        $fieldCoverImage= Field::updateOrCreate(['tab_id'   => $tabSocialShareBlog->id, 
        'code' => 'CoverImage'],
        ['created_id' => $idUser,
        'field'=> 'languagePageSetting.urlCoverImage',  
        'max_length'=> '230', 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCoverImage->id,
         'language_id' => $idLanguage],
         ['description' => 'Url immagine condivisione', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCoverImage->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'required' => 't',
        'pos_x' => '0',
        'pos_y' => '0',
        'colspan' => '12',
        'created_id' => $idUser]);

         $tabSettingsBlog = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Blog')->first()->id, 
            'code' => 'Settings'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);    
        $tabTableSettingsBlog = LanguageTab::updateOrCreate(['tab_id' => $tabSettingsBlog->id,'language_id' => $idLanguage],
        ['description' => 'Impostazioni','created_id' => $idUser]);  


        $fieldIdLanguage= Field::updateOrCreate(['tab_id'   => $tabSettingsBlog->id, 
        'code' => 'IdLanguage'],
        ['created_id' => $idUser,
        'field'=> 'languagePageSetting.idLanguage',  
        'data_type' => '0']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldIdLanguage->id,
         'language_id' => $idLanguage],
         ['description' => 'Lingua', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldIdLanguage->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '19',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '160',
        'colspan' => '12',
        'created_id' => $idUser]);

        $fieldCommandNews= Field::updateOrCreate(['tab_id'   => $tabAttributeBlog->id, 
        'code' => 'CommandTable'],
        ['created_id' => $idUser,
        'formatter'=> 'tableFormatter',  
        'data_type' => '0']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCommandNews->id,
         'language_id' => $idLanguage],
         ['description' => 'Lingua', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCommandNews->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '14',
        'required' => 't',
        'table_order' => '10',
        'created_id' => $idUser]);


        /* table blog 09/04/20212 */
        $tabTableBlog = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Blog')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);    
        $tabsTableBlog = LanguageTab::updateOrCreate(['tab_id' => $tabTableBlog->id,'language_id' => $idLanguage],
        ['description' => 'Table','created_id' => $idUser]); 

        $fieldCommandTableBlog= Field::updateOrCreate(['tab_id'   => $tabTableBlog->id, 
        'code' => 'CodeTable'],
        ['created_id' => $idUser,
        'formatter'=> 'tableFormatter',  
        'data_type' => '0']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCommandTableBlog->id,
            'language_id' => $idLanguage],
            ['description' => '', 
            'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCommandTableBlog->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '14',
        'table_order' => '10',
        'created_id' => $idUser]); 

        $fieldTitleTableBlog = Field::updateOrCreate(['tab_id'   => $tabTableBlog->id, 
        'code' => 'title'],
        ['created_id' => $idUser,
        'formatter'=> '', 
        'field'=> 'title', 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleTableBlog->id,
            'language_id' => $idLanguage],
            ['description' => 'Titolo', 
            'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleTableBlog->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'table_order' => 20,
        'created_id' => $idUser]);   

        $fieldTitleTableSeoUrl = Field::updateOrCreate(['tab_id'   => $tabTableBlog->id, 
        'code' => 'Url'],
        ['created_id' => $idUser,
            'formatter'=> '',
        'field'=> 'url', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleTableSeoUrl->id,
            'language_id' => $idLanguage],
            ['description' => 'Url', 
            'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleTableSeoUrl->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'table_order' => 30,
        'created_id' => $idUser]);  

        $fieldTitleTableVisibleFrom = Field::updateOrCreate(['tab_id'   => $tabTableBlog->id, 
        'code' => 'visible_from'],
        ['created_id' => $idUser,
            'formatter'=> '', 
        'field'=> 'visible_from', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleTableVisibleFrom->id,
            'language_id' => $idLanguage],
            ['description' => 'Visibile dal', 
            'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleTableVisibleFrom->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'table_order' => 40,
        'created_id' => $idUser]); 

        $fieldTitleTablePublish = Field::updateOrCreate(['tab_id'   => $tabTableBlog->id,    
        'code' => 'publish'],
        ['created_id' => $idUser,
            'formatter'=> '', 
        'field'=> 'publish', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleTablePublish->id,
            'language_id' => $idLanguage],
            ['description' => 'Pubblicazione', 
            'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleTablePublish->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'table_order' => 50,
        'created_id' => $idUser]); 

        $fieldTitleTableSeoTitle = Field::updateOrCreate(['tab_id'   => $tabTableBlog->id,    
        'code' => 'seotitle'],
        ['created_id' => $idUser,
        'formatter'=> 'SeoTitleFormatter', 
        'field'=> 'seotitle', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleTableSeoTitle->id,
            'language_id' => $idLanguage],
            ['description' => 'Titolo Seo', 
            'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleTableSeoTitle->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'table_order' => 60,
        'created_id' => $idUser]); 

        $fieldTitleTableSeoDescription = Field::updateOrCreate(['tab_id'   => $tabTableBlog->id,    
        'code' => 'seodescription'],
        ['created_id' => $idUser,
        'formatter'=> 'SeoDescriptionFormatter', 
        'field'=> 'seodescription', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleTableSeoDescription->id,
            'language_id' => $idLanguage],
            ['description' => 'Descrizione Seo', 
            'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleTableSeoDescription->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'table_order' => 70,
        'created_id' => $idUser]); 

        $fieldTitleTableSeoKeyword = Field::updateOrCreate(['tab_id'   => $tabTableBlog->id,    
        'code' => 'seokeyword'],
        ['created_id' => $idUser,
        'field'=> 'seokeyword', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleTableSeoKeyword->id,
            'language_id' => $idLanguage],
            ['description' => 'Keywords', 
            'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleTableSeoKeyword->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '15',
        'table_order' => 80,
        'created_id' => $idUser]); 



        /* end table blog 09/04/2021 */
        //end blog ; 


        //News
          $tabContentBlog = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'News')->first()->id, 
            'code' => 'Content'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);    
        $tabTableContentBlog = LanguageTab::updateOrCreate(['tab_id' => $tabContentBlog->id,'language_id' => $idLanguage],
        ['description' => 'Contenuto','created_id' => $idUser]);  

        $tabAttributeBlog = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'News')->first()->id, 
            'code' => 'TabAttribute'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);    
        $tabtabAttributeBlog = LanguageTab::updateOrCreate(['tab_id' => $tabAttributeBlog->id,'language_id' => $idLanguage],
        ['description' => 'TabAttributi','created_id' => $idUser]);  

        $fieldContentBlog = Field::updateOrCreate(['tab_id'   => $tabContentBlog->id, 
        'code' => 'Content'],
        ['created_id' => $idUser,
        'required'=> 't',        
        'field'=> 'languagePageSetting.content', 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldContentBlog->id,
         'language_id' => $idLanguage],
         ['description' => 'Contenuto', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldContentBlog->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '19',
        'pos_x'=> '10',
        'pos_y'=> '90',
        'colspan'=> '12',
        'created_id' => $idUser]);    

        $fieldTitlePageBlog = Field::updateOrCreate(['tab_id'   => $tabAttributeBlog->id, 
        'code' => 'TitlePage'],
        ['created_id' => $idUser,
        'formatter'=> 'titleFormatter', 
        'on_change'=> 'changeTitle', 
        'required'=> 't', 
        'field'=> 'languagePageSetting.title', 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitlePageBlog->id,
         'language_id' => $idLanguage],
         ['description' => 'Titolo', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitlePageBlog->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'required' => 't',
       // 'table_order' => 20,
        'pos_x' => '10',
        'pos_y' => '10',
        'colspan' => '12',
        'created_id' => $idUser]);  

        $fieldNewsseoTitle = Field::updateOrCreate(['tab_id'   => $tabAttributeBlog->id,
        'code' => 'seoTitle'
        ],
        [
        'data_type' => '1',
        'max_length' => 230, 
        'field' => 'languagePageSetting.seoTitle', 
        'on_change' => 'changeSeoTitle',
        'on_keyup' => 'keyupSeoTitle',
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldNewsseoTitle->id, 
        'language_id' => $idLanguage],
        ['description' => 'Titolo SEO', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldNewsseoTitle->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> '10' ,
        'pos_y'=> '20' ,
        'colspan'=> 12,
        'created_id' => $idUser]); 
    

        $fieldTitleSeoDescription = Field::updateOrCreate(['tab_id'   => $tabAttributeBlog->id, 
        'code' => 'SeoDescription'],
        ['created_id' => $idUser,
        'required'=> 't', 
        'field'=> 'languagePageSetting.seoDescription', 
        'on_change'=> 'changeSeoDescription', 
        'on_keyup' => 'keyupSeoDescription',
        'max_length'=> 230, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleSeoDescription->id,
         'language_id' => $idLanguage],
         ['description' => 'Descrizione SEO', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleSeoDescription->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '1',
        'required' => 't',
        //'table_order' => 20,
        'pos_x' => '10',
        'pos_y' => '30',
        'colspan' => '12',
        'created_id' => $idUser]);  

        $fieldTitleSeoUrl = Field::updateOrCreate(['tab_id'   => $tabAttributeBlog->id, 
        'code' => 'Url'],
        ['created_id' => $idUser,
         'formatter'=> 'urlFormatter', 
         'on_change'=> 'checkUrl', 
        'required'=> 't', 
        'field'=> 'languagePageSetting.url', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleSeoUrl->id,
         'language_id' => $idLanguage],
         ['description' => 'Url', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleSeoUrl->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'required' => 't',
       // 'table_order' => 30,
        'pos_x' => '10',
        'pos_y' => '40',
        'colspan' => '12',
        'created_id' => $idUser]);  

        $fieldTitleSeoUrlCanonical = Field::updateOrCreate(['tab_id'   => $tabAttributeBlog->id, 
        'code' => 'UrlCanonical'],
        ['created_id' => $idUser,
         'formatter'=> 'urlFormatter', 
         'on_change'=> 'checkUrl', 
        'required'=> 't', 
        'field'=> 'languagePageSetting.urlCanonical', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleSeoUrlCanonical->id,
         'language_id' => $idLanguage],
         ['description' => 'Url Canonical', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleSeoUrlCanonical->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'required' => 't',
       // 'table_order' => 40,
        'pos_x' => '10',
        'pos_y' => '50',
        'colspan' => '12',
        'created_id' => $idUser]);  

        $fieldSeoKeyword= Field::updateOrCreate(['tab_id'   => $tabAttributeBlog->id, 
        'code' => 'SeoKeyword'],
        ['created_id' => $idUser,
        'required'=> 't', 
        'field'=> 'languagePageSetting.SeoKeyword', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldSeoKeyword->id,
         'language_id' => $idLanguage],
         ['description' => 'Parola chiave SEO', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldSeoKeyword->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '15',
        'required' => 't',
        //'table_order' => 50,
        'pos_x' => '10',
        'pos_y' => '60',
        'colspan' => '12',
        'created_id' => $idUser]);  


         $tabPubblicazioneBlog = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'News')->first()->id, 
            'code' => 'TabPublication'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);    
        $tabtabPubblicazioneBlog = LanguageTab::updateOrCreate(['tab_id' => $tabPubblicazioneBlog->id,'language_id' => $idLanguage],
        ['description' => 'TabPubblicazione','created_id' => $idUser]);  

       /* $fieldAuthor= Field::updateOrCreate(['tab_id'   => $tabPubblicazioneBlog->id, 
        'code' => 'Author'],
        ['created_id' => $idUser,
        'field'=> 'idAuthor', 
        'service'=> 'users', 
        'data_type' => '0']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldAuthor->id,
         'language_id' => $idLanguage],
         ['description' => 'Autore', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldAuthor->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '2',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '60',
        'colspan' => '12',
        'created_id' => $idUser]);  */

        $fieldPageCategory= Field::updateOrCreate(['tab_id'   => $tabPubblicazioneBlog->id, 
        'code' => 'PageCategory'],
        ['created_id' => $idUser,
        'field'=> 'idPageCategory', 
        'service'=> 'pageCategory', 
        'data_type' => '0']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldPageCategory->id,
         'language_id' => $idLanguage],
         ['description' => 'Categoria', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldPageCategory->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '2',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '70',
        'colspan' => '12',
        'created_id' => $idUser]);  

        $fieldPublish= Field::updateOrCreate(['tab_id'   => $tabPubblicazioneBlog->id, 
        'code' => 'Publish'],
        ['created_id' => $idUser,
        'field'=> 'publish',  
        'data_type' => '2']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldPublish->id,
         'language_id' => $idLanguage],
         ['description' => 'Data di pubblicazione', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldPublish->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '4',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '80',
        'colspan' => '12',
        'created_id' => $idUser]);  

        $fieldVisibleFrom= Field::updateOrCreate(['tab_id'   => $tabPubblicazioneBlog->id, 
        'code' => 'VisibleFrom'],
        ['created_id' => $idUser,
        'field'=> 'visibleFrom',  
        'data_type' => '2']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldVisibleFrom->id,
         'language_id' => $idLanguage],
         ['description' => 'Visibile dal', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldVisibleFrom->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '4',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '90',
        'colspan' => '12',
        'created_id' => $idUser]); 

        $fieldVisibleEnd= Field::updateOrCreate(['tab_id'   => $tabPubblicazioneBlog->id, 
        'code' => 'VisibleEnd'],
        ['created_id' => $idUser,
        'field'=> 'visibleEnd',  
        'data_type' => '2']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldVisibleEnd->id,
         'language_id' => $idLanguage],
         ['description' => 'Fino al', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldVisibleEnd->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '4',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '100',
        'colspan' => '12',
        'created_id' => $idUser]); 

        $fieldFixedPost= Field::updateOrCreate(['tab_id'   => $tabPubblicazioneBlog->id, 
        'code' => 'VisibleEnd'],
        ['created_id' => $idUser,
        'field'=> 'visibleEnd',  
        'data_type' => '2']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldFixedPost->id,
         'language_id' => $idLanguage],
         ['description' => 'Fissa il post nella parte superiore della pagina', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldFixedPost->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '4',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '110',
        'colspan' => '12',
        'created_id' => $idUser]); 

        $fieldFixedPostEndDate= Field::updateOrCreate(['tab_id'   => $tabPubblicazioneBlog->id, 
        'code' => 'FixedEnd'],
        ['created_id' => $idUser,
        'field'=> 'fixedEnd',  
        'service'=> 'users', 
        'data_type' => '2']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldFixedPostEndDate->id,
         'language_id' => $idLanguage],
         ['description' => 'Data di fine post in alto', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldFixedPostEndDate->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '4',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '120',
        'colspan' => '12',
        'created_id' => $idUser]);

        $tabSocialShareBlog = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'News')->first()->id, 
            'code' => 'TabSocialShare'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);    
        $tabTableSocialShareBlog = LanguageTab::updateOrCreate(['tab_id' => $tabSocialShareBlog->id,'language_id' => $idLanguage],
        ['description' => 'TabSocialShare','created_id' => $idUser]);  


        $fieldShareTitle= Field::updateOrCreate(['tab_id'   => $tabSocialShareBlog->id, 
        'code' => 'ShareTitle'],
        ['created_id' => $idUser,
        'field'=> 'languagePageSetting.shareTitle',  
        'max_length'=> '60', 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldShareTitle->id,
         'language_id' => $idLanguage],
         ['description' => 'Titolo condivisione', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldShareTitle->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '130',
        'colspan' => '12',
        'created_id' => $idUser]);

        $fieldShareDescription= Field::updateOrCreate(['tab_id'   => $tabSocialShareBlog->id, 
        'code' => 'ShareDescription'],
        ['created_id' => $idUser,
        'field'=> 'languagePageSetting.shareDescription',  
        'max_length'=> '230', 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldShareDescription->id,
         'language_id' => $idLanguage],
         ['description' => 'Descrizione condivisione', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldShareDescription->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '140',
        'colspan' => '12',
        'created_id' => $idUser]);

        $fieldCoverImage= Field::updateOrCreate(['tab_id'   => $tabSocialShareBlog->id, 
        'code' => 'CoverImage'],
        ['created_id' => $idUser,
        'field'=> 'languagePageSetting.urlCoverImage',  
        'max_length'=> '230', 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCoverImage->id,
         'language_id' => $idLanguage],
         ['description' => 'Url immagine condivisione', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCoverImage->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'required' => 't',
        'pos_x' => '0',
        'pos_y' => '0',
        'colspan' => '12',
        'created_id' => $idUser]);

         $tabSettingsBlog = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'News')->first()->id, 
            'code' => 'Settings'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);    
        $tabTableSettingsBlog = LanguageTab::updateOrCreate(['tab_id' => $tabSettingsBlog->id,'language_id' => $idLanguage],
        ['description' => 'Impostazioni','created_id' => $idUser]);  


        $fieldIdLanguage= Field::updateOrCreate(['tab_id'   => $tabSettingsBlog->id, 
        'code' => 'IdLanguage'],
        ['created_id' => $idUser,
        'field'=> 'languagePageSetting.idLanguage',  
        'data_type' => '0']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldIdLanguage->id,
         'language_id' => $idLanguage],
         ['description' => 'Lingua', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldIdLanguage->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '19',
        'required' => 't',
        'pos_x' => '10',
        'pos_y' => '160',
        'colspan' => '12',
        'created_id' => $idUser]);

        $fieldCommandNews= Field::updateOrCreate(['tab_id'   => $tabAttributeBlog->id, 
        'code' => 'CommandTable'],
        ['created_id' => $idUser,
        'formatter'=> 'tableFormatter',  
        'data_type' => '0']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCommandNews->id,
         'language_id' => $idLanguage],
         ['description' => 'Lingua', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCommandNews->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '14',
        'required' => 't',
        'table_order' => '10',
        'created_id' => $idUser]);
        //end News ;


        /* table news 25/06/2021 */
        $tabTableNews = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'News')->first()->id, 
            'code' => 'tableNews'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);    
        $tabsTableNews = LanguageTab::updateOrCreate(['tab_id' => $tabTableNews->id,'language_id' => $idLanguage],
        ['description' => 'tableNews','created_id' => $idUser]); 

        $fieldCommandTableNews= Field::updateOrCreate(['tab_id'   => $tabTableNews->id, 
        'code' => 'CodeTable'],
        ['created_id' => $idUser,
        'formatter'=> 'tableFormatter',  
        'data_type' => '0']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCommandTableNews->id,
            'language_id' => $idLanguage],
            ['description' => '', 
            'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCommandTableNews->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '14',
        'table_order' => '10',
        'created_id' => $idUser]); 

        $fieldTitleTableNews = Field::updateOrCreate(['tab_id'   => $tabTableNews->id, 
        'code' => 'title'],
        ['created_id' => $idUser,
        'formatter'=> '', 
        'field'=> 'title', 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleTableNews->id,
            'language_id' => $idLanguage],
            ['description' => 'Titolo', 
            'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleTableNews->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'table_order' => 20,
        'created_id' => $idUser]);   

        $fieldTitleTableNewsSeoUrl = Field::updateOrCreate(['tab_id'   => $tabTableNews->id, 
        'code' => 'Url'],
        ['created_id' => $idUser,
            'formatter'=> '',
        'field'=> 'url', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleTableNewsSeoUrl->id,
            'language_id' => $idLanguage],
            ['description' => 'Url', 
            'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleTableNewsSeoUrl->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'table_order' => 30,
        'created_id' => $idUser]);  

        $fieldTitleTableNewsVisibleFrom = Field::updateOrCreate(['tab_id'   => $tabTableNews->id, 
        'code' => 'visible_from'],
        ['created_id' => $idUser,
            'formatter'=> '', 
        'field'=> 'visible_from', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleTableNewsVisibleFrom->id,
            'language_id' => $idLanguage],
            ['description' => 'Visibile dal', 
            'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleTableNewsVisibleFrom->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'table_order' => 40,
        'created_id' => $idUser]); 

        $fieldTitleTableNewsPublish = Field::updateOrCreate(['tab_id'   => $tabTableNews->id,    
        'code' => 'publish'],
        ['created_id' => $idUser,
            'formatter'=> '', 
        'field'=> 'publish', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleTableNewsPublish->id,
            'language_id' => $idLanguage],
            ['description' => 'Pubblicazione', 
            'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleTableNewsPublish->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'table_order' => 50,
        'created_id' => $idUser]); 

        $fieldTitleTableSeoTitle = Field::updateOrCreate(['tab_id'   => $tabTableNews->id,    
        'code' => 'seotitle'],
        ['created_id' => $idUser,
        'formatter'=> 'SeoTitleFormatter', 
        'field'=> 'seotitle', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleTableSeoTitle->id,
            'language_id' => $idLanguage],
            ['description' => 'Titolo Seo', 
            'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleTableSeoTitle->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'table_order' => 60,
        'created_id' => $idUser]); 

        $fieldTitleTableSeoDescription = Field::updateOrCreate(['tab_id'   => $tabTableNews->id,    
        'code' => 'seodescription'],
        ['created_id' => $idUser,
        'formatter'=> 'SeoDescriptionFormatter', 
        'field'=> 'seodescription', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleTableSeoDescription->id,
            'language_id' => $idLanguage],
            ['description' => 'Descrizione Seo', 
            'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleTableSeoDescription->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'table_order' => 70,
        'created_id' => $idUser]); 


        $fieldTitleTableSeoKeyword = Field::updateOrCreate(['tab_id'   => $tabTableNews->id,    
        'code' => 'seokeyword'],
        ['created_id' => $idUser,
        'field'=> 'seokeyword', 
        'max_length'=> 255, 
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleTableSeoKeyword->id,
            'language_id' => $idLanguage],
            ['description' => 'Keywords', 
            'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleTableSeoKeyword->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'table_order' => 80,
        'created_id' => $idUser]); 

        /* end table news 09/04/2021 */

        /*****************************************************************************/
        /* MENU CATALOGO - LISTINI */
        /*****************************************************************************/
        $funzionalita = Functionality::updateOrCreate(['code'   => 'List'],[ 'created_id' => $idUser]);

        $menu = MenuAdmin::updateOrCreate(['code'   => 'PriceLists'],
        [ 
            'functionality_id'=>Functionality::where('code', 'List')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-eur')->first()->id,
            'url'     => '/admin/crm/list',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Listini',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 40,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Catalogo')->first()->id,
            'created_id' => $idUser
        ]);

        //Listino
           $tabListTable= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'List')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);           
        $tabsTableListTable = LanguageTab::updateOrCreate(['tab_id' => $tabListTable->id, 
       'language_id' => $idLanguage
        ],
        [ 
        'description' => 'Table',
        'created_id' => $idUser]);     
       
        $fieldCHECKBOX = Field::updateOrCreate(['tab_id'   => $tabListTable->id, 
        'code' => 'TableCheckBox'
        ],
        [
        'data_type' => 1,
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCHECKBOX->id, 
        'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCHECKBOX->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 13 ,
        'table_order'=> 10 ,
        'created_id' => $idUser]); 

         $fieldListFormatter = Field::updateOrCreate(['tab_id'   => $tabListTable->id, 
         'code' => 'listFormatter'
        ],
        [
        'formatter' => 'listFormatter',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldListFormatter->id, 
        'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]); 
     
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldListFormatter->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 14 ,
        'table_order'=>  20 ,
        'created_id' => $idUser]); 


        $fieldListTableCodeList= Field::updateOrCreate(['tab_id'   => $tabListTable->id, 
        'code' => 'code_list'],
        ['created_id' => $idUser,
        'field'=> 'code_list',  
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldListTableCodeList->id,
         'language_id' => $idLanguage],
         ['description' => 'Codice Lista', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldListTableCodeList->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'required' => 't',
        'table_order' => 30,
        'created_id' => $idUser]);

        $fieldDescriptionList= Field::updateOrCreate(['tab_id'   => $tabListTable->id, 
        'code' => 'description'],
        ['created_id' => $idUser,
        'field'=> 'description',  
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDescriptionList->id,
         'language_id' => $idLanguage],
         ['description' => 'Descrizione', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDescriptionList->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'required' => 't',
        'table_order' => 40,
        'created_id' => $idUser]);  

       $fieldbeginning_validity= Field::updateOrCreate(['tab_id'   => $tabListTable->id, 
        'code' => 'beginning_validity'],
        ['created_id' => $idUser,
        'field'=> 'beginning_validity',  
        'data_type' => '2']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldbeginning_validity->id,
         'language_id' => $idLanguage],
         ['description' => 'Inizio Validità', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldbeginning_validity->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '4',
        'required' => 't',
        'table_order' => 50,
        'created_id' => $idUser]);  

        $fieldend_validity= Field::updateOrCreate(['tab_id'   => $tabListTable->id, 
        'code' => 'end_validity'],
        ['created_id' => $idUser,
        'field'=> 'end_validity',  
        'data_type' => '2']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldend_validity->id,
         'language_id' => $idLanguage],
         ['description' => 'Fine Validità', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldend_validity->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '4',
        'required' => 't',
        'table_order' => 60,
        'created_id' => $idUser]);  

         $field_value_list= Field::updateOrCreate(['tab_id'   => $tabListTable->id, 
        'code' => 'value_list'],
        ['created_id' => $idUser,
        'field'=> 'value_list',  
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field_value_list->id,
         'language_id' => $idLanguage],
         ['description' => 'Valuta Listino', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field_value_list->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'required' => 't',
        'table_order' => 70,
        'created_id' => $idUser]);  

      $tabDetailTableList = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'List')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);    
        $tabsTableDetailTableList = LanguageTab::updateOrCreate(['tab_id' => $tabDetailTableList->id, 'language_id' => $idLanguage],
        ['description' => 'Detail','created_id' => $idUser]);     

        $field_code_lists= Field::updateOrCreate(['tab_id'   => $tabDetailTableList->id, 
        'code' => 'code_list'],
        ['created_id' => $idUser,
        'field'=> 'code_list',  
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field_code_lists->id,
         'language_id' => $idLanguage],
         ['description' => 'Codice Listino', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field_code_lists->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'pos_x' => '10',
        'pos_y' => '10',
        'required' => 't',
        'created_id' => $idUser]);  

        $field_descriptions= Field::updateOrCreate(['tab_id'   => $tabDetailTableList->id, 
        'code' => 'description'],
        ['created_id' => $idUser,
        'field'=> 'description',  
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field_descriptions->id,
         'language_id' => $idLanguage],
         ['description' => 'Descrizione', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field_descriptions->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'pos_x' => '20',
        'pos_y' => '10',
        'required' => 't',
        'created_id' => $idUser]);  

       $field_beg_validity= Field::updateOrCreate(['tab_id'   => $tabDetailTableList->id, 
        'code' => 'beginning_validity'],
        ['created_id' => $idUser,
        'field'=> 'beginning_validity',  
        'data_type' => '2']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field_beg_validity->id,
         'language_id' => $idLanguage],
         ['description' => 'Inizio Validità', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field_beg_validity->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '4',
        'pos_x' => '30',
        'pos_y' => '10',
        'required' => 't',
        'created_id' => $idUser]);  

        $field_end_validity= Field::updateOrCreate(['tab_id'   => $tabDetailTableList->id, 
        'code' => 'end_validity'],
        ['created_id' => $idUser,
        'field'=> 'end_validity',  
        'data_type' => '2']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field_end_validity->id,
         'language_id' => $idLanguage],
         ['description' => 'Fine Validità', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field_end_validity->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '4',
        'pos_x' => '40',
        'pos_y' => '10',
        'required' => 't',
        'created_id' => $idUser]);  

        $field_value_lists= Field::updateOrCreate(['tab_id'   => $tabDetailTableList->id, 
        'code' => 'value_list'],
        ['created_id' => $idUser,
        'field'=> 'value_list',  
        'data_type' => '1']);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field_value_lists->id,
         'language_id' => $idLanguage],
         ['description' => 'Valore Listino', 
         'created_id' => $idUser]); 

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field_value_lists->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type' => '0',
        'pos_x' => '50',
        'pos_y' => '10',
        'required' => 't',
        'created_id' => $idUser]);  
        // end List Seeder



        //Produttori Seeder
        $tabsTableProducers = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Producers')->first()->id, 
            'code' => 'table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabtableProducer =  Tab::where('functionality_id', Functionality::where('code', 'Producers')->first()->id)->where('code', 'table')->first();

        $fieldCheckBoxProducers = Field::updateOrCreate(['tab_id'   => $tabsTableProducers->id, 
        'code' => 'TableCheckBox'],
        ['field'=>'TableCheckBox',  
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCheckBoxProducers->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCheckBoxProducers->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldProducersFormatter = Field::updateOrCreate(['tab_id'   => $tabsTableProducers->id, 
        'code' => 'ProducersFormatter'],
        ['field'=>'ProducersFormatter',  
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldProducersFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldProducersFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

        $fieldProducersCode = Field::updateOrCreate(['tab_id'   => $tabsTableProducers->id, 
        'code' => 'Code'],
        ['field'=>'Code',  
        'data_type'=> '1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldProducersCode->id, 'language_id' => $idLanguage],
        ['description' => 'Codice', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldProducersCode->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

        $fieldBusinessName = Field::updateOrCreate(['tab_id'   => $tabsTableProducers->id, 
        'code' => 'BusinessName'],
        ['field'=>'BusinessName',  
        'data_type'=> '1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldBusinessName->id, 'language_id' => $idLanguage],
        ['description' => 'Ragione Sociale', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldBusinessName->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

        $fieldAddress = Field::updateOrCreate(['tab_id'   => $tabsTableProducers->id, 
        'code' => 'Address'],
        ['field'=>'Address', 
        'data_type'=> '1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldAddress->id, 'language_id' => $idLanguage],
        ['description' => 'Indirizzo', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldAddress->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '50', 
        'created_id' => $idUser]);

        $fieldResort = Field::updateOrCreate(['tab_id'   => $tabsTableProducers->id, 
        'code' => 'Resort'],
        ['field'=>'Resort', 
        'data_type'=> '1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldResort->id, 'language_id' => $idLanguage],
        ['description' => 'Località', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldResort->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '60', 
        'created_id' => $idUser]);
        
          $fieldProvince= Field::updateOrCreate(['tab_id'   => $tabsTableProducers->id, 
        'code' => 'Province'],
        ['field'=>'Province', 
        'data_type'=> '1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldProvince->id, 'language_id' => $idLanguage],
        ['description' => 'Provincia', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldProvince->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '70', 
        'created_id' => $idUser]);

        $fieldPostalCode= Field::updateOrCreate(['tab_id'   => $tabsTableProducers->id, 
        'code' => 'PostalCode'],
        ['field'=>'PostalCode', 
        'data_type'=> '1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldPostalCode->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Postale', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldPostalCode->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '80', 
        'created_id' => $idUser]);

        $fieldEmail= Field::updateOrCreate(['tab_id'   => $tabsTableProducers->id, 
        'code' => 'Email'],
        ['field'=>'Email', 
        'data_type'=> '1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldEmail->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Postale', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldEmail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '90', 
        'created_id' => $idUser]);


        $tabsDetailProducers = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Producers')->first()->id, 
            'code' => 'DetailProducers'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        
        $tabDetailProducer =  Tab::where('functionality_id', Functionality::where('code', 'Producers')->first()->id)->where('code', 'DetailProducers')->first();

        $fieldDetailcodeProducer= Field::updateOrCreate(['tab_id'   => $tabsDetailProducers->id, 
        'code' => 'code'],
        ['field'=>'code', 
        'data_type'=> '1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailcodeProducer->id, 'language_id' => $idLanguage],
        ['description' => 'Codice', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailcodeProducer->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

        $fieldDetailbusinessnameProducer= Field::updateOrCreate(['tab_id'   => $tabsDetailProducers->id, 
        'code' => 'business_name'],
        ['field'=>'business_name', 
        'data_type'=> '1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailbusinessnameProducer->id, 'language_id' => $idLanguage],
        ['description' => 'Ragione Sociale', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailbusinessnameProducer->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '20', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

         $fieldDetailaddressProducer= Field::updateOrCreate(['tab_id'   => $tabsDetailProducers->id, 
        'code' => 'address'],
        ['field'=>'address', 
        'data_type'=> '1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailaddressProducer->id, 'language_id' => $idLanguage],
        ['description' => 'Indirizzo', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailaddressProducer->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '30', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

        $fieldDetailresortProducer= Field::updateOrCreate(['tab_id'   => $tabsDetailProducers->id, 
        'code' => 'resort'],
        ['field'=>'resort', 
        'data_type'=> '1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailresortProducer->id, 'language_id' => $idLanguage],
        ['description' => 'Località', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailresortProducer->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '40', 
        'pos_y'=> '20',
        'created_id' => $idUser]);

        $fieldDetailprovinceProducer= Field::updateOrCreate(['tab_id'   => $tabsDetailProducers->id, 
        'code' => 'province'],
        ['field'=>'province', 
        'data_type'=> '1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailprovinceProducer->id, 'language_id' => $idLanguage],
        ['description' => 'Provincia', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailprovinceProducer->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '50', 
        'pos_y'=> '20',
        'created_id' => $idUser]);

        $fieldDetailpostal_code_id_Producer= Field::updateOrCreate(['tab_id'   => $tabsDetailProducers->id, 
        'code' => 'postal_code_id'],
        ['field'=>'postal_code_id', 
        'data_type'=> '1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailpostal_code_id_Producer->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Postale', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailpostal_code_id_Producer->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'pos_x'=> '60', 
        'pos_y'=> '20',
        'created_id' => $idUser]);

        $fieldDetail_Telephone_Number_Producer= Field::updateOrCreate(['tab_id'   => $tabsDetailProducers->id, 
        'code' => 'telephone_number'],
        ['field'=>'telephone_number', 
        'data_type'=> '1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetail_Telephone_Number_Producer->id, 'language_id' => $idLanguage],
        ['description' => ' Telefono', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetail_Telephone_Number_Producer->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '8', 
        'pos_x'=> '70', 
        'pos_y'=> '30',
        'created_id' => $idUser]);

       $fieldDetail_Email_Producer= Field::updateOrCreate(['tab_id'   => $tabsDetailProducers->id, 
        'code' => 'email'],
        ['field'=>'email', 
        'data_type'=> '1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetail_Email_Producer->id, 'language_id' => $idLanguage],
        ['description' => ' Email', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetail_Email_Producer->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '9', 
        'pos_x'=> '80', 
        'pos_y'=> '30',
        'created_id' => $idUser]);

        $fieldDetail_online_Producer= Field::updateOrCreate(['tab_id'   => $tabsDetailProducers->id, 
        'code' => 'Online'],
        ['field'=>'online', 
        'data_type'=> '3',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetail_Email_Producer->id, 'language_id' => $idLanguage],
        ['description' => ' Online', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetail_Email_Producer->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'pos_x'=> '90', 
        'pos_y'=> '30',
        'created_id' => $idUser]);
        // end producers 



       //vat type seeder
         $tabstableVatType = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'VatType')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabTableVatType =  Tab::where('functionality_id', Functionality::where('code', 'VatType')->first()->id)->where('code', 'Table')->first();
        $LanguageTab= LanguageTab::updateOrCreate(['tab_id'   => $tabstableVatType->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Table',
        'created_id' => $idUser]); 
        

        $fieldTableCheckBox= Field::updateOrCreate(['tab_id'   => $tabstableVatType->id, 
        'code' => 'TableCheckBox'],
        ['field'=>'TableCheckBox', 
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'table_order'=> '10',
        'input_type'=> '13', 
        'created_id' => $idUser]);
       
        $fieldTableVatTypeFormatter= Field::updateOrCreate(['tab_id'   => $tabstableVatType->id, 
        'code' => 'VatTypeFormatter'],
        ['formatter'=>'vatTypeFormatter', 
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableVatTypeFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableVatTypeFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'table_order'=> '20', 
        'input_type'=> '14',
        'created_id' => $idUser]);

         
        $fieldTableRate= Field::updateOrCreate(['tab_id'   => $tabstableVatType->id, 
        'code' => 'Rate'],
        ['field'=>'rate', 
        'data_type'=>'1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableRate->id, 'language_id' => $idLanguage],
        ['description' => 'Iva', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableRate->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'table_order'=> '30',
        'input_type'=> '0',
        'created_id' => $idUser]);


        $tabsDetailVatType = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'VatType')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        
        $tabDetailVatType =  Tab::where('functionality_id', Functionality::where('code', 'VatType')->first()->id)->where('code', 'Detail')->first();
        $LanguageTab= LanguageTab::updateOrCreate(['tab_id'   => $tabsDetailVatType->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Detail',
        'created_id' => $idUser]); 


        $fieldDetailRate= Field::updateOrCreate(['tab_id'   => $tabsDetailVatType->id, 
        'code' => 'Rate'],
        ['field'=>'rate', 
        'data_type'=>'1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailRate->id, 'language_id' => $idLanguage],
        ['description' => 'Iva', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailRate->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '10',
        'created_id' => $idUser]);
       //end vat type seeder

       //unit Measure Seeder 
         $tatableUnitMeasure = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'UnitMeasure')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tastableUnitMeasure =  Tab::where('functionality_id', Functionality::where('code', 'UnitMeasure')->first()->id)->where('code', 'Table')->first();
        $LanguageTab= LanguageTab::updateOrCreate(['tab_id'   => $tatableUnitMeasure->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Unità di Misura',
        'created_id' => $idUser]); 

        $fieldTableCheckBox= Field::updateOrCreate(['tab_id'   => $tatableUnitMeasure->id, 
        'code' => 'TableCheckBox'],
        ['field'=>'TableCheckBox', 
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);    

        $fieldTableFormatterUnitMeasures= Field::updateOrCreate(['tab_id'   => $tatableUnitMeasure->id, 
        'code' => 'UnitMeasureFormatter'],
        ['formatter'=>'UnitMeasureFormatter', 
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableFormatterUnitMeasures->id, 'language_id' => $idLanguage],
        ['description' => '', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableFormatterUnitMeasures->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);    

        $fieldTableCodeUnitMeasure= Field::updateOrCreate(['tab_id'   => $tatableUnitMeasure->id, 
        'code' => 'code'],
        ['field'=>'code', 
        'data_type'=>'1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableCodeUnitMeasure->id, 'language_id' => $idLanguage],
        ['description' => 'Unità di Misura', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableCodeUnitMeasure->id, 'role_id' => $idRole],
        ['enabled' => 't',  
        'table_order'=> '30', 
        'input_type'=> '0',
        'created_id' => $idUser]);   

        $fieldTabledescriptionUnitMeasure= Field::updateOrCreate(['tab_id'   => $tatableUnitMeasure->id, 
        'code' => 'description'],
        ['field'=>'description', 
        'data_type'=>'1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTabledescriptionUnitMeasure->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTabledescriptionUnitMeasure->id, 'role_id' => $idRole],
        ['enabled' => 't',  
        'table_order'=> '40', 
        'input_type'=> '0',
        'created_id' => $idUser]);   

         $tabsDetailUnitMeasures = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'UnitMeasure')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        
        $tadetailUnitMeasure =  Tab::where('functionality_id', Functionality::where('code', 'UnitMeasure')->first()->id)->where('code', 'Detail')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabsDetailUnitMeasures->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Detail',
        'created_id' => $idUser]); 

        $fieldDetailCodeUnitMeasure= Field::updateOrCreate(['tab_id'   => $tabsDetailUnitMeasures->id, 
        'code' => 'code'],
        ['field'=>'code', 
        'data_type'=>'1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCodeUnitMeasure->id, 'language_id' => $idLanguage],
        ['description' => 'Codice', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCodeUnitMeasure->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '10',
        'created_id' => $idUser]);   

        $fieldDetailDescriptionUnitMeasure= Field::updateOrCreate(['tab_id'   => $tabsDetailUnitMeasures->id, 
        'code' => 'description'],
        ['field'=>'description', 
        'data_type'=>'1',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailDescriptionUnitMeasure->id, 'language_id' => $idLanguage],
        ['description' => 'Unità di Misura', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailDescriptionUnitMeasure->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '20',
        'created_id' => $idUser]);
       // end unit Measure Seeder 


        /// CATEGORYTYPE SEEDER 
        
        $tabsTableCategoryType = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Category')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabTableCategoryType =  Tab::where('functionality_id', Functionality::where('code', 'Category')->first()->id)->where('code', 'Table')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabsTableCategoryType->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Table',
        'created_id' => $idUser]); 

        $fieldtableCheckBoxCategory= Field::updateOrCreate(['tab_id'   => $tabsTableCategoryType->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableCheckBoxCategory->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableCheckBoxCategory->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);   

         $fieldtableformatterCategory= Field::updateOrCreate(['tab_id'   => $tabsTableCategoryType->id, 
        'code' => 'CategoryFormatter'],
        [
        'formatter' => 'CategoryFormatter',
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableformatterCategory->id, 'language_id' => $idLanguage],
        ['description' => '', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableformatterCategory->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

        $fieldtableorderCategory= Field::updateOrCreate(['tab_id'   => $tabsTableCategoryType->id, 
        'code' => 'order'],
        [
        'field' => 'order',   
        'data_type' => '1',  
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableorderCategory->id, 'language_id' => $idLanguage],
        ['description' => 'Ordinamento', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableorderCategory->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'created_id' => $idUser]);


        $fieldtabledescriptionCategory= Field::updateOrCreate(['tab_id'   => $tabsTableCategoryType->id, 
        'code' => 'description'],
        [
        'field' => 'description',   
        'data_type' => '1',  
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabledescriptionCategory->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabledescriptionCategory->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

        $tabsDetailCategoryType = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Category')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        
        $tabDetailCategoryType =  Tab::where('functionality_id', Functionality::where('code', 'Category')->first()->id)->where('code', 'Detail')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabsDetailCategoryType->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Detail',
        'created_id' => $idUser]); 

        $fieldDetailorderCategory= Field::updateOrCreate(['tab_id'   => $tabsDetailCategoryType->id, 
        'code' => 'order'],
        [
        'field' => 'order',
        'data_type' => '1',   
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailorderCategory->id, 'language_id' => $idLanguage],
        ['description' => 'Ordinamento', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailorderCategory->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);   
 
        $fieldDetaildescriptionCategory= Field::updateOrCreate(['tab_id'   => $tabsDetailCategoryType->id, 
        'code' => 'description'],
        [
        'field' => 'description',  
        'data_type' => '1',   
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetaildescriptionCategory->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetaildescriptionCategory->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '20', 
        'created_id' => $idUser]);   
        //// END CATEGORYTYPE SEEDER 


        /// SELENA SQL VIEW SEEDER 
          $tabstableSelenaSqlView = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'SelenaViews')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabtableSelenaSqlView =  Tab::where('functionality_id', Functionality::where('code', 'SelenaViews')->first()->id)->where('code', 'Table')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabstableSelenaSqlView->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Table',
        'created_id' => $idUser]); 

        $fieldTableCheckBoxSelenaSqlView= Field::updateOrCreate(['tab_id'   => $tabstableSelenaSqlView->id, 
        'code' => 'TableCheckBox'],
        [
        'field' => 'TableCheckBox',  
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableCheckBoxSelenaSqlView->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableCheckBoxSelenaSqlView->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);    

       $fieldTableFormatterSelenaSqlView= Field::updateOrCreate(['tab_id'   => $tabstableSelenaSqlView->id, 
        'code' => 'SelenaViewsFormatter'],
        [
        'field' => 'SelenaViewsFormatter',  
        'formatter' => 'SelenaViewsFormatter',  
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableFormatterSelenaSqlView->id, 'language_id' => $idLanguage],
        ['description' => '', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableFormatterSelenaSqlView->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);   

        $fieldNameViewSelenaSqlView= Field::updateOrCreate(['tab_id'   => $tabstableSelenaSqlView->id, 
        'code' => 'NameView'],
        [
        'data_type' => '1',  
        'field' => 'nameView',  
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldNameViewSelenaSqlView->id, 'language_id' => $idLanguage],
        ['description' => 'Nome View', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldNameViewSelenaSqlView->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'created_id' => $idUser]);   

        $fieldDescriptionSelenaSqlView= Field::updateOrCreate(['tab_id'   => $tabstableSelenaSqlView->id, 
        'code' => 'Description'],
        [
        'data_type' => '1',  
        'field' => 'description',  
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDescriptionSelenaSqlView->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione view', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDescriptionSelenaSqlView->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'created_id' => $idUser]);   


          $tabsDetailSelenaSqlView = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'SelenaViews')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabDetailSelenaSqlView =  Tab::where('functionality_id', Functionality::where('code', 'SelenaViews')->first()->id)->where('code', 'Detail')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabsDetailSelenaSqlView->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Detail',
        'created_id' => $idUser]); 

        $fieldDetailNameViewSelenaSqlView= Field::updateOrCreate(['tab_id'   => $tabsDetailSelenaSqlView->id, 
        'code' => 'NameView'],
        [
        'field' => 'nameView', 
        'data_type' => '1',  
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailNameViewSelenaSqlView->id, 'language_id' => $idLanguage],
        ['description' => 'Nome View', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailNameViewSelenaSqlView->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10',
        'pos_y'=> '10', 
        'created_id' => $idUser]);    

         $fieldDetailDescriptionViewSelenaSqlView= Field::updateOrCreate(['tab_id'   => $tabsDetailSelenaSqlView->id, 
        'code' => 'Description'],
        [
        'field' => 'description', 
        'data_type' => '1',  
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailDescriptionViewSelenaSqlView->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione View', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailDescriptionViewSelenaSqlView->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '20',
        'pos_y'=> '10', 
        'created_id' => $idUser]);    


        $fieldDetailSelectViewSelenaSqlView= Field::updateOrCreate(['tab_id'   => $tabsDetailSelenaSqlView->id, 
        'code' => 'Select'],
        [
        'field' => 'select', 
        'data_type' => '1',  
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailSelectViewSelenaSqlView->id, 'language_id' => $idLanguage],
        ['description' => 'Select', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailSelectViewSelenaSqlView->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10',
        'pos_y'=> '20', 
        'created_id' => $idUser]); 

        $fieldDetailFromViewSelenaSqlView= Field::updateOrCreate(['tab_id'   => $tabsDetailSelenaSqlView->id, 
        'code' => 'From'],
        [
        'field' => 'from', 
        'data_type' => '1',  
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailFromViewSelenaSqlView->id, 'language_id' => $idLanguage],
        ['description' => 'From', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailFromViewSelenaSqlView->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10',
        'pos_y'=> '30', 
        'created_id' => $idUser]); 

         $fieldDetailWhereViewSelenaSqlView= Field::updateOrCreate(['tab_id'   => $tabsDetailSelenaSqlView->id, 
        'code' => 'Where'],
        [
        'field' => 'where', 
        'data_type' => '1',  
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailWhereViewSelenaSqlView->id, 'language_id' => $idLanguage],
        ['description' => 'Where', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailWhereViewSelenaSqlView->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10',
        'pos_y'=> '40', 
        'created_id' => $idUser]); 

        $fieldDetailOrderByViewSelenaSqlView= Field::updateOrCreate(['tab_id'   => $tabsDetailSelenaSqlView->id, 
        'code' => 'OrderBy'],
        [
        'field' => 'orderBy', 
        'data_type' => '1',  
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailOrderByViewSelenaSqlView->id, 'language_id' => $idLanguage],
        ['description' => 'Order by', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailOrderByViewSelenaSqlView->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10',
        'pos_y'=> '50', 
        'created_id' => $idUser]); 

        $fieldDetailGroupByViewSelenaSqlView= Field::updateOrCreate(['tab_id'   => $tabsDetailSelenaSqlView->id, 
        'code' => 'GroupBy'],
        [
        'field' => 'groupBy', 
        'data_type' => '1',  
        'created_id' => $idUser]); 

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailGroupByViewSelenaSqlView->id, 'language_id' => $idLanguage],
        ['description' => 'Group by', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailGroupByViewSelenaSqlView->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10',
        'pos_y'=> '60', 
        'created_id' => $idUser]);
        ///END SELENA SQL VIEW SEEDER 

        //ROLE SEEDER 
           $tabsTableRoles = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Roles')->first()->id, 
            'code' => 'TableRoles'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabTableRoles =  Tab::where('functionality_id', Functionality::where('code', 'Roles')->first()->id)->where('code', 'TableRoles')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabsTableRoles->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'TableRoles',
        'created_id' => $idUser]); 

        $fieldtableRolescheckbox= Field::updateOrCreate(['tab_id'   => $tabsTableRoles->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableRolescheckbox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableRolescheckbox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10',
        'created_id' => $idUser]);

         $fieldtableRolesFormatter= Field::updateOrCreate(['tab_id'   => $tabsTableRoles->id, 
        'code' => 'RolesFormatter'],
        [
        'formatter' => 'RolesFormatter',
        'created_id' => $idUser]); 
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableRolesFormatter->id, 'language_id' => $idLanguage],
        [
            'description' => '',
            'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableRolesFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20',
        'created_id' => $idUser]);

        $fieldtableRolesName= Field::updateOrCreate(['tab_id'   => $tabsTableRoles->id, 
        'code' => 'name'],
        [
        'field' => 'name',
        'data_type' => '1',
        'created_id' => $idUser]); 

         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableRolesName->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableRolesName->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30',
        'created_id' => $idUser]);

        $fieldtableRolesdisplay_name= Field::updateOrCreate(['tab_id'   => $tabsTableRoles->id, 
        'code' => 'display_name'],
        [
        'field' => 'display_name',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableRolesdisplay_name->id, 'language_id' => $idLanguage],
        ['description' => 'Display Nome', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableRolesdisplay_name->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40',
        'created_id' => $idUser]);


        
        $tabsDetailRoles = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Roles')->first()->id, 
            'code' => 'DetailRoles'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        
        $tabTableRoles =  Tab::where('functionality_id', Functionality::where('code', 'Roles')->first()->id)->where('code', 'DetailRoles')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabsDetailRoles->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'DetailRoles',
        'created_id' => $idUser]); 

        $fieldtabsDetailRolesname= Field::updateOrCreate(['tab_id'   => $tabsDetailRoles->id, 
        'code' => 'name'],
        [
        'field' => 'name',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsDetailRolesname->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsDetailRolesname->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);

        $fieldtabsDetailRolesdisplay_name= Field::updateOrCreate(['tab_id'   => $tabsDetailRoles->id, 
        'code' => 'display_name'],
        [
        'field' => 'display_name',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsDetailRolesdisplay_name->id, 'language_id' => $idLanguage],
        ['description' => 'Display Nome', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsDetailRolesdisplay_name->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '20', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);

         $fieldtabsDetailRolesdescription= Field::updateOrCreate(['tab_id'   => $tabsDetailRoles->id, 
        'code' => 'description'],
        [
        'field' => 'description',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsDetailRolesdescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsDetailRolesdescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '30', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);
        //END ROLE SEEDER 

        // Request Contacts Seeder 

         $tabsDetailRequestContacts = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'RequestContacts')->first()->id, 
            'code' => 'Content'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabTableRequestContacts =  Tab::where('functionality_id', Functionality::where('code', 'RequestContacts')->first()->id)->where('code', 'Content')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabsDetailRequestContacts->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Contenuto',
        'created_id' => $idUser]); 

        $fieldtabsfieldRequestContactsName= Field::updateOrCreate(['tab_id'   => $tabsDetailRequestContacts->id, 
        'code' => 'name'],
        [
        'field' => 'name',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsfieldRequestContactsName->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsfieldRequestContactsName->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '10', 
        'created_id' => $idUser]);


        $fieldtabsfieldRequestContactsSurname= Field::updateOrCreate(['tab_id'   => $tabsDetailRequestContacts->id, 
        'code' => 'surname'],
        [
        'field' => 'surname',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsfieldRequestContactsSurname->id, 'language_id' => $idLanguage],
        ['description' => 'Cognome', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsfieldRequestContactsSurname->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

        $fieldtabsfieldRequestContactsEmail= Field::updateOrCreate(['tab_id'   => $tabsDetailRequestContacts->id, 
        'code' => 'Email'],
        [
        'field' => 'email',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsfieldRequestContactsEmail->id, 'language_id' => $idLanguage],
        ['description' => 'Email', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsfieldRequestContactsEmail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

        $fieldtabsfieldRequestContactEmailToSend= Field::updateOrCreate(['tab_id'   => $tabsDetailRequestContacts->id, 
        'code' => 'EmailToSend'],
        [
        'field' => 'emailToSend',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsfieldRequestContactEmailToSend->id, 'language_id' => $idLanguage],
        ['description' => 'Email di destinazione', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsfieldRequestContactEmailToSend->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '60', 
        'created_id' => $idUser]);

        $fieldtabsfieldRequestContactPhoneNumber= Field::updateOrCreate(['tab_id'   => $tabsDetailRequestContacts->id, 
        'code' => 'PhoneNumber'],
        [
        'field' => 'phoneNumber',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsfieldRequestContactPhoneNumber->id, 'language_id' => $idLanguage],
        ['description' => 'Telefono', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsfieldRequestContactPhoneNumber->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '35', 
        'created_id' => $idUser]);

        $fieldtabsfieldRequestContactData= Field::updateOrCreate(['tab_id'   => $tabsDetailRequestContacts->id, 
        'code' => 'Date'],
        [
        'field' => 'date',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsfieldRequestContactData->id, 'language_id' => $idLanguage],
        ['description' => 'Date', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsfieldRequestContactData->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '40', 
        'created_id' => $idUser]);


        $fieldtabsfieldRequestContactDescription= Field::updateOrCreate(['tab_id'   => $tabsDetailRequestContacts->id, 
        'code' => 'Description'],
        [
        'field' => 'description',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsfieldRequestContactDescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsfieldRequestContactDescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '50', 
        'created_id' => $idUser]);


        // CustomerConfigurationSeeder
         $tabstableCustomer = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Customer')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabtableCustomer =  Tab::where('functionality_id', Functionality::where('code', 'Customer')->first()->id)->where('code', 'Table')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabstableCustomer->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Table',
        'created_id' => $idUser]); 

        $fieldtabsfieldTableCheckBox= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsfieldTableCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsfieldTableCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

         $fieldtabsfieldCustomerFormatter= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'CustomerFormatter'],
        [
        'formatter' => 'customerFormatter',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsfieldCustomerFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsfieldCustomerFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

        $fieldtabsTableCompany= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'Company'],
        [
        'data_type' => '3',
        'field' => 'Company',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsTableCompany->id, 'language_id' => $idLanguage],
        ['description' => 'Ditta', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsTableCompany->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

         $fieldCustomerTableBusinessName= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'BusinessName'],
        [
        'data_type' => '1',
        'field' => 'businessName',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerTableBusinessName->id, 'language_id' => $idLanguage],
        ['description' => 'Società', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerTableBusinessName->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

        $fieldCustomerTableName= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'Nome'],
        [
        'data_type' => '1',
        'field' => 'name',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerTableName->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerTableName->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '50', 
        'created_id' => $idUser]);

        $fieldCustomerTableSurname= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'Surname'],
        [
        'data_type' => '1',
        'field' => 'surname',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerTableSurname->id, 'language_id' => $idLanguage],
        ['description' => 'Cognome', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerTableSurname->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '60', 
        'created_id' => $idUser]);

        $fieldCustomerTableVatNumber= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'VatNumber'],
        [
        'data_type' => '1',
        'field' => 'vatNumber',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerTableVatNumber->id, 'language_id' => $idLanguage],
        ['description' => 'Partita IVA', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerTableVatNumber->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '60', 
        'created_id' => $idUser]);

        $fieldCustomerTableFiscalCode= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'FiscalCode'],
        [
        'data_type' => '1',
        'field' => 'fiscalCode',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerTableFiscalCode->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Fiscale', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerTableFiscalCode->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '70', 
        'created_id' => $idUser]);

        $fieldCustomerTableAddress= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'Address'],
        [
        'data_type' => '1',
        'field' => 'address',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerTableAddress->id, 'language_id' => $idLanguage],
        ['description' => 'Indirizzo', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerTableAddress->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '80', 
        'created_id' => $idUser]);


        $fieldCustomerTableCodePostale= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'PostalCode'],
        [
        'data_type' => '0',
        'service' => 'postalCode',

        'field' => 'idPostalCode',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerTableCodePostale->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Postale', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerTableCodePostale->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '90', 
        'created_id' => $idUser]);

        $fieldCustomerTableTelephoneNumber= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'TelephoneNumber'],
        [
        'data_type' => '1',
        'field' => 'telephoneNumber',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerTableTelephoneNumber->id, 'language_id' => $idLanguage],
        ['description' => 'Telefono', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerTableTelephoneNumber->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '8', 
        'table_order'=> '100', 
        'created_id' => $idUser]);

        $fieldCustomerTableFaxNumber= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'FaxNumber'],
        [
        'data_type' => '1',
        'field' => 'faxNumber',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerTableFaxNumber->id, 'language_id' => $idLanguage],
        ['description' => 'Fax', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerTableFaxNumber->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '8', 
        'table_order'=> '110', 
        'created_id' => $idUser]);

       $fieldCustomerTableEmail= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'Email'],
        [
        'data_type' => '1',
        'field' => 'email',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerTableEmail->id, 'language_id' => $idLanguage],
        ['description' => 'Email', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerTableEmail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '9', 
        'table_order'=> '120', 
        'created_id' => $idUser]);


        $fieldCustomerTableWebsite= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'Website'],
        [
        'data_type' => '1',
        'field' => 'website',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerTableWebsite->id, 'language_id' => $idLanguage],
        ['description' => 'Sito', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerTableWebsite->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '130', 
        'created_id' => $idUser]);

        $fieldCustomerTableLanguage= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'Language'],
        [
        'data_type' => '0',
        'field' => 'idLanguage',
        'service' => 'languages',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerTableLanguage->id, 'language_id' => $idLanguage],
        ['description' => 'Lingua', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerTableLanguage->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'table_order'=> '140', 
        'created_id' => $idUser]);

      $fieldCustomerTableVatType= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'VatType'],
        [
        'data_type' => '0',
        'field' => 'idVatType',
        'service' => 'vatType',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerTableVatType->id, 'language_id' => $idLanguage],
        ['description' => 'Tipo IVA', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerTableVatType->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'table_order'=> '150', 
        'created_id' => $idUser]);

         $fieldCustomerTableEnableFrom= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'EnableFrom'],
        [
        'data_type' => '2',
        'field' => 'enableFrom',
        'service' => 'enableFrom',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerTableEnableFrom->id, 'language_id' => $idLanguage],
        ['description' => 'Valido dal', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerTableEnableFrom->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '4', 
        'table_order'=> '160', 
        'created_id' => $idUser]);

         $fieldCustomerTableEnableTo= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'EnableTo'],
        [
        'data_type' => '2',
        'field' => 'enableTo',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerTableEnableTo->id, 'language_id' => $idLanguage],
        ['description' => 'Valido al', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerTableEnableTo->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '4', 
        'table_order'=> '170', 
        'created_id' => $idUser]);

        $fieldCustomerTableSendNewsletter= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'SendNewsletter'],
        [
        'data_type' => '3',
        'field' => 'sendNewsletter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerTableSendNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Invia newsletter', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerTableSendNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'table_order'=> '180', 
        'created_id' => $idUser]);

        $fieldUserRoleCustomerTableErrorNewsletter= Field::updateOrCreate(['tab_id'   => $tabstableCustomer->id, 
        'code' => 'ErrorNewsletter'],
        [
        'data_type' => '3',
        'field' => 'errorNewsletter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserRoleCustomerTableErrorNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Errore newsletter', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserRoleCustomerTableErrorNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'table_order'=> '190', 
        'created_id' => $idUser]);
        // End CustomerConfigurationSeeder


        $tabsDetailCustomer = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Customer')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        
        $tabDetailCustomer =  Tab::where('functionality_id', Functionality::where('code', 'Customer')->first()->id)->where('code', 'Detail')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Detail',
        'created_id' => $idUser]);   

 
        $fieldtabsDetailCompany= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'Company'],
        [
        'data_type' => '3',
        'field' => 'Company',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsDetailCompany->id, 'language_id' => $idLanguage],
        ['description' => 'Ditta', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsDetailCompany->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'pos_x'=> '10', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

         $fieldCustomerDetailBusinessName= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'BusinessName'],
        [
        'data_type' => '1',
        'field' => 'businessName',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerDetailBusinessName->id, 'language_id' => $idLanguage],
        ['description' => 'Società', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerDetailBusinessName->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '20', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);

        $fieldCustomerDetailName= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'Nome'],
        [
        'data_type' => '1',
        'field' => 'name',
        'created_id' => $idUser]); 
        
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerDetailName->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerDetailName->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '30', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

        $fieldCustomerDetailSurname= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'Surname'],
        [
        'data_type' => '1',
        'field' => 'surname',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerDetailSurname->id, 'language_id' => $idLanguage],
        ['description' => 'Cognome', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerDetailSurname->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '40', 
        'pos_y'=> '10',  
        'created_id' => $idUser]);

        $fieldCustomerDetailVatNumber= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'VatNumber'],
        [
        'data_type' => '1',
        'field' => 'vatNumber',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerDetailVatNumber->id, 'language_id' => $idLanguage],
        ['description' => 'Partita IVA', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerDetailVatNumber->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '20',  
        'created_id' => $idUser]);

        $fieldCustomerDetailFiscalCode= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'FiscalCode'],
        [
        'data_type' => '1',
        'field' => 'fiscalCode',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerDetailFiscalCode->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Fiscale', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerDetailFiscalCode->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '20', 
        'pos_y'=> '20',  
        'created_id' => $idUser]);

        $fieldCustomerDetailAddress= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'Address'],
        [
        'data_type' => '1',
        'field' => 'address',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerDetailAddress->id, 'language_id' => $idLanguage],
        ['description' => 'Indirizzo', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerDetailAddress->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '30', 
        'pos_y'=> '20', 
        'created_id' => $idUser]);


        $fieldCustomerDetailCodePostale= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'PostalCode'],
        [
        'data_type' => '0',
        'service' => 'postalCode',

        'field' => 'idPostalCode',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerDetailCodePostale->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Postale', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerDetailCodePostale->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '40', 
        'pos_y'=> '20',  
        'created_id' => $idUser]);

        $fieldCustomerDetailTelephoneNumber= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'TelephoneNumber'],
        [
        'data_type' => '1',
        'field' => 'telephoneNumber',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerDetailTelephoneNumber->id, 'language_id' => $idLanguage],
        ['description' => 'Telefono', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerDetailTelephoneNumber->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '8', 
        'pos_x'=> '10', 
        'pos_y'=> '30',  
        'created_id' => $idUser]);

        $fieldCustomerDetailFaxNumber= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'FaxNumber'],
        [
        'data_type' => '1',
        'field' => 'faxNumber',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerDetailFaxNumber->id, 'language_id' => $idLanguage],
        ['description' => 'Fax', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerDetailFaxNumber->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '8', 
        'pos_x'=> '20', 
        'pos_y'=> '30', 
        'created_id' => $idUser]);

       $fieldCustomerDetailEmail= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'Email'],
        [
        'data_type' => '1',
        'field' => 'email',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerDetailEmail->id, 'language_id' => $idLanguage],
        ['description' => 'Email', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerDetailEmail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '9', 
        'pos_x'=> '30', 
        'pos_y'=> '30',  
        'created_id' => $idUser]);


        $fieldCustomerDetailWebsite= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'Website'],
        [
        'data_type' => '1',
        'field' => 'website',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerDetailWebsite->id, 'language_id' => $idLanguage],
        ['description' => 'Sito', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerDetailWebsite->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '40', 
        'pos_y'=> '30', 
        'created_id' => $idUser]);

        $fieldCustomerDetailLanguage= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'Language'],
        [
        'data_type' => '0',
        'field' => 'idLanguage',
        'service' => 'languages',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerDetailLanguage->id, 'language_id' => $idLanguage],
        ['description' => 'Lingua', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerDetailLanguage->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'pos_x'=> '10', 
        'pos_y'=> '40', 
        'created_id' => $idUser]);

      $fieldCustomerDetailVatType= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'VatType'],
        [
        'data_type' => '0',
        'field' => 'idVatType',
        'service' => 'vatType',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerDetailVatType->id, 'language_id' => $idLanguage],
        ['description' => 'Tipo IVA', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerDetailVatType->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'pos_x'=> '20', 
        'pos_y'=> '40', 
        'created_id' => $idUser]);

         $fieldCustomerDetailEnableFrom= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'EnableFrom'],
        [
        'data_type' => '2',
        'field' => 'enableFrom',
        'service' => 'enableFrom',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerDetailEnableFrom->id, 'language_id' => $idLanguage],
        ['description' => 'Valido dal', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerDetailEnableFrom->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '4', 
        'pos_x'=> '30', 
        'pos_y'=> '40',  
        'created_id' => $idUser]);

         $fieldCustomerDetailEnableTo= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'EnableTo'],
        [
        'data_type' => '2',
        'field' => 'enableTo',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerDetailEnableTo->id, 'language_id' => $idLanguage],
        ['description' => 'Valido al', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerDetailEnableTo->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '4', 
        'pos_x'=> '40', 
        'pos_y'=> '40',  
        'created_id' => $idUser]);

        $fieldCustomerDetailSendNewsletter= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'SendNewsletter'],
        [
        'data_type' => '3',
        'field' => 'sendNewsletter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCustomerDetailSendNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Invia newsletter', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCustomerDetailSendNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'pos_x'=> '10', 
        'pos_y'=> '50', 
        'created_id' => $idUser]);

        $fieldUserRoleCustomerDetailErrorNewsletter= Field::updateOrCreate(['tab_id'   => $tabsDetailCustomer->id, 
        'code' => 'ErrorNewsletter'],
        [
        'data_type' => '3',
        'field' => 'errorNewsletter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserRoleCustomerDetailErrorNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Errore newsletter', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserRoleCustomerDetailErrorNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'pos_x'=> '20', 
        'pos_y'=> '50',  
        'created_id' => $idUser]);
        // end customer 

        // contact seeder 

          $tabsTableContact = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Contact')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);

        $tabTableContact =  Tab::where('functionality_id', Functionality::where('code', 'Contact')->first()->id)->where('code', 'Table')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabsTableContact->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Table',
        'created_id' => $idUser]);  


        $fieldtabsTableContactCheckBox= Field::updateOrCreate(['tab_id'   => $tabsTableContact->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsTableContactCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsTableContactCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldtabsTableContactFormatter= Field::updateOrCreate(['tab_id'   => $tabsTableContact->id, 
        'code' => 'ContactFormatter'],
        [
        'formatter' => 'ContactFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsTableContactFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsTableContactFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

        $fieldtabsTableContactName= Field::updateOrCreate(['tab_id'   => $tabsTableContact->id, 
        'code' => 'Name'],
        [
        'field' => 'name',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsTableContactName->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsTableContactName->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

         $fieldtabsTableContacSurname= Field::updateOrCreate(['tab_id'   => $tabsTableContact->id, 
        'code' => 'Surname'],
        [
        'field' => 'surname',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsTableContacSurname->id, 'language_id' => $idLanguage],
        ['description' => 'Cognome', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsTableContacSurname->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

        $fieldtabsTableContacBusinessName= Field::updateOrCreate(['tab_id'   => $tabsTableContact->id, 
        'code' => 'BusinessName'],
        [
        'field' => 'businessName',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsTableContacBusinessName->id, 'language_id' => $idLanguage],
        ['description' => 'Ragione Sociale', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsTableContacBusinessName->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '50', 
        'created_id' => $idUser]);

        $fieldtabsTableContactEmail= Field::updateOrCreate(['tab_id'   => $tabsTableContact->id, 
        'code' => 'Email'],
        [
        'field' => 'email',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsTableContactEmail->id, 'language_id' => $idLanguage],
        ['description' => 'Email', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsTableContactEmail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '60', 
        'created_id' => $idUser]);

        $fieldtabsTableContactPhoneNumber= Field::updateOrCreate(['tab_id'   => $tabsTableContact->id, 
        'code' => 'PhoneNumber'],
        [
        'field' => 'phoneNumber',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsTableContactPhoneNumber->id, 'language_id' => $idLanguage],
        ['description' => 'Telefono', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsTableContactPhoneNumber->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '70', 
        'created_id' => $idUser]);

        $fieldContactTableSendNewsletter= Field::updateOrCreate(['tab_id'   => $tabsTableContact->id, 
        'code' => 'SendNewsletter'],
        [
        'field' => 'sendNewsletter',
        'data_type' => '3',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldContactTableSendNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Invia newsletter', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldContactTableSendNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '80', 
        'created_id' => $idUser]);


        $fieldContactTableErrorNewsletter= Field::updateOrCreate(['tab_id'   => $tabsTableContact->id, 
        'code' => 'ErrorNewsletter'],
        [
        'field' => 'errorNewsletter',
        'data_type' => '3',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldContactTableErrorNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Errore newsletter', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldContactTableErrorNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '90', 
        'created_id' => $idUser]);

        $tabsDetailContact = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Contact')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabDetailContact =  Tab::where('functionality_id', Functionality::where('code', 'Contact')->first()->id)->where('code', 'Detail')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabsDetailContact->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Detail',
        'created_id' => $idUser]);  


        $fieldtabsDetailContactName= Field::updateOrCreate(['tab_id'   => $tabsDetailContact->id, 
        'code' => 'Name'],
        [
        'field' => 'name',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsDetailContactName->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsDetailContactName->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

         $fieldtabsDetailContacSurname= Field::updateOrCreate(['tab_id'   => $tabsDetailContact->id, 
        'code' => 'Surname'],
        [
        'field' => 'surname',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsDetailContacSurname->id, 'language_id' => $idLanguage],
        ['description' => 'Cognome', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsDetailContacSurname->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '20', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

        $fieldtabsDetailContacBusinessName= Field::updateOrCreate(['tab_id'   => $tabsDetailContact->id, 
        'code' => 'BusinessName'],
        [
        'field' => 'businessName',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsDetailContacBusinessName->id, 'language_id' => $idLanguage],
        ['description' => 'Ragione Sociale', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsDetailContacBusinessName->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '30', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

        $fieldtabsDetailContactEmail= Field::updateOrCreate(['tab_id'   => $tabsDetailContact->id, 
        'code' => 'Email'],
        [
        'field' => 'email',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsDetailContactEmail->id, 'language_id' => $idLanguage],
        ['description' => 'Email', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsDetailContactEmail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '40', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);

        $fieldtabsDetailContactPhoneNumber= Field::updateOrCreate(['tab_id'   => $tabsDetailContact->id, 
        'code' => 'PhoneNumber'],
        [
        'field' => 'phoneNumber',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabsDetailContactPhoneNumber->id, 'language_id' => $idLanguage],
        ['description' => 'Telefono', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabsDetailContactPhoneNumber->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '20', 
        'created_id' => $idUser]);

        $fieldContactDetailSendNewsletter= Field::updateOrCreate(['tab_id'   => $tabsDetailContact->id, 
        'code' => 'SendNewsletter'],
        [
        'field' => 'sendNewsletter',
        'data_type' => '3',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldContactDetailSendNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Invia newsletter', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldContactDetailSendNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'pos_x'=> '20', 
        'pos_y'=> '20', 
        'created_id' => $idUser]);


        $fieldContactDetailErrorNewsletter= Field::updateOrCreate(['tab_id'   => $tabsDetailContact->id, 
        'code' => 'ErrorNewsletter'],
        [
        'field' => 'errorNewsletter',
        'data_type' => '3',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldContactDetailErrorNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Errore newsletter', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldContactDetailErrorNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'pos_x'=> '30', 
        'pos_y'=> '20',
        'created_id' => $idUser]);

        $fieldGroup_id_Sms = Field::updateOrCreate(['tab_id'   => $tabsDetailContact->id, 'code' => 'group_sms_id'],
        ['field'=>'group_sms_id', 'service' => 'groupSms', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldGroup_id_Sms->id, 'language_id' => $idLanguage],
        ['description' => 'Gruppo SMS', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldGroup_id_Sms->id, 'role_id' => $idRole],
        ['enabled' => 't', 'input_type'=> '2', 'pos_x'=>'40', 'pos_y'=>'20', 'created_id' => $idUser]);
        //end contact seeder

        //Setting Configuration Seeder
          $tabsTableUser = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'UserSetting')->first()->id, 
            'code' => 'Users'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabTableUser =  Tab::where('functionality_id', Functionality::where('code', 'UserSetting')->first()->id)->where('code', 'Detail')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabsTableUser->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Utenti',
        'created_id' => $idUser]);  


        $fieldUserCheckBox= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);  

        $fieldUserSettingFormatter= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'UserSettingFormatter'],
        [
        'formatter' => 'userFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserSettingFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserSettingFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);  

        $fieldUserUsername= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'Username'],
        [
        'field' => 'username',
        'data_type' => '1',
        'on_change' => 'userHelpers.checkExistUsername',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserUsername->id, 'language_id' => $idLanguage],
        ['description' => 'Username', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserUsername->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> null,
        'pos_x'=> null, 
        'pos_y'=> null,  
        'colspan'=> '4',
        'required' => 't', 
        'created_id' => $idUser]);  

        $fieldUserEmail= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'Email'],
        [
        'field' => 'email',
        'data_type' => '1',
        'on_change' => 'userHelpers.checkExistEmail',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserEmail->id, 'language_id' => $idLanguage],
        ['description' => 'Email', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserEmail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '9', 
        'table_order'=> '60', 
        'pos_x'=> '30', 
        'pos_y'=> '30', 
        'colspan'=> '3',
        'required' => 't', 
        'created_id' => $idUser]);  

        $fieldUserPassword= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'Password'],
        [
        'field' => 'password',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserPassword->id, 'language_id' => $idLanguage],
        ['description' => 'Password', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserPassword->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '16',
        'table_order'=> null, 
        'pos_x'=> null, 
        'pos_y'=> null, 
        'colspan'=> '4',
        'required' => 't', 
        'created_id' => $idUser]);

        $fieldUserPassword= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'confirm_password'],
        [
        'field' => 'confirm_password',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserPassword->id, 'language_id' => $idLanguage],
        ['description' => 'Conferma Password', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserPassword->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '16', 
        'table_order'=> null,
        'pos_x'=> null, 
        'pos_y'=> null, 
        'colspan'=> '4',
        'required' => 't', 
        'created_id' => $idUser]);

         $fieldUserLanguage= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'Language'],
        [
        'field' => 'idLanguage',
        'data_type' => '1',
        'service' => 'languages',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserLanguage->id, 'language_id' => $idLanguage],
        ['description' => 'Lingua', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserLanguage->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'table_order'=> null,
        'pos_x'=> '50', 
        'pos_y'=> '40', 
        'colspan'=> '2',
        'required' => 't', 
        'created_id' => $idUser]);
        
        $fieldUserCompany= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'company'],
        [
        'field' => 'company',
        'data_type' => '3',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserCompany->id, 'language_id' => $idLanguage],
        ['description' => 'Ditta', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserCompany->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'table_order'=> null,
        'pos_x'=> '40', 
        'pos_y'=> '10', 
        'colspan'=> '1',
        'required' => 't', 
        'created_id' => $idUser]);

         $fieldUsersend_newsletter= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'send_newsletter'],
        [
        'field' => 'send_newsletter',
        'data_type' => '3',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUsersend_newsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Newsletter', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUsersend_newsletter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'table_order'=> null,
        'pos_x'=> '50', 
        'pos_y'=> '10', 
        'colspan'=> '1',
        'required' => 't', 
        'created_id' => $idUser]);

        $fieldUseronline= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'online'],
        [
        'field' => 'online',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUseronline->id, 'language_id' => $idLanguage],
        ['description' => 'Online', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUseronline->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'table_order'=> null,
        'pos_x'=> '60', 
        'pos_y'=> '10', 
        'colspan'=> '1',
        'required' => 't', 
        'created_id' => $idUser]);

        $fieldUserBusinessName= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'business_name'],
        [
        'field' => 'business_name',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserBusinessName->id, 'language_id' => $idLanguage],
        ['description' => 'Ragione Sociale', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserBusinessName->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'pos_x'=> '10', 
        'pos_y'=> '10', 
        'colspan'=> '3',
        'required' => 't', 
        'created_id' => $idUser]);

       $fieldUserName= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'name'],
        [
        'field' => 'name',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserName->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserName->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'pos_x'=> '20', 
        'pos_y'=> '10', 
        'colspan'=> '3',
        'required' => 't', 
        'created_id' => $idUser]);


        $fieldUserSurname= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'surname'],
        [
        'field' => 'surname',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserSurname->id, 'language_id' => $idLanguage],
        ['description' => 'Cognome', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserSurname->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '50', 
        'pos_x'=> '30', 
        'pos_y'=> '10', 
        'colspan'=> '3',
        'required' => 't', 
        'created_id' => $idUser]);

         $fieldUserVatNumber= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'vat_number'],
        [
        'field' => 'vat_number',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserVatNumber->id, 'language_id' => $idLanguage],
        ['description' => 'Partita Iva', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserVatNumber->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> null,
        'pos_x'=> '10', 
        'pos_y'=> '40', 
        'colspan'=> '3',
        'required' => 't', 
        'created_id' => $idUser]);

         $fieldUserFiscalCode= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'fiscal_code'],
        [
        'field' => 'fiscal_code',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserFiscalCode->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Fiscale', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserFiscalCode->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> null,
        'pos_x'=> '20', 
        'pos_y'=> '40', 
        'colspan'=> '3',
        'required' => 't', 
        'created_id' => $idUser]);
        
        $fieldUserAddress= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'address'],
        [
        'field' => 'address',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserAddress->id, 'language_id' => $idLanguage],
        ['description' => 'Indirizzo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserAddress->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '80',
        'pos_x'=> '10', 
        'pos_y'=> '20', 
        'colspan'=> '4',
        'required' => 't', 
        'created_id' => $idUser]);
                
        $fieldUserPostalCode= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'postal_code'],
        [
        'field' => 'postal_code',
        'data_type' => '0',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserPostalCode->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Postale', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserPostalCode->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '90',
        'pos_x'=> '20', 
        'pos_y'=> '20', 
        'colspan'=> '2',
        'required' => 't', 
        'created_id' => $idUser]);

        $fieldUserTelephoneNumber= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'telephone_number'],
        [
        'field' => 'telephone_number',
        'data_type' => '0',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserTelephoneNumber->id, 'language_id' => $idLanguage],
        ['description' => 'Telefono', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserTelephoneNumber->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '8', 
        'table_order'=> '70', 
        'pos_x'=> '10', 
        'pos_y'=> '30', 
        'colspan'=> '3',
        'required' => 't', 
        'created_id' => $idUser]);

        $fieldUserFaxNumber= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'fax_number'],
        [
        'field' => 'fax_number',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserFaxNumber->id, 'language_id' => $idLanguage],
        ['description' => 'Fax', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserFaxNumber->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '8', 
        'table_order'=> null,
        'pos_x'=> '40', 
        'pos_y'=> '40', 
        'colspan'=> '2',
        'required' => 't', 
        'created_id' => $idUser]);

        $fieldUserWebSite= Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'website'],
        [
        'field' => 'website',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserWebSite->id, 'language_id' => $idLanguage],
        ['description' => 'Sito Web', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserWebSite->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> null,
        'pos_x'=> '30', 
        'pos_y'=> '40', 
        'colspan'=> '2',
        'required' => 't', 
        'created_id' => $idUser]);

        $fieldUserVatType = Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'vat_type_id'],
        [
        'field' => 'vat_type_id',
        'data_type' => '0',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserVatType->id, 'language_id' => $idLanguage],
        ['description' => 'Tipo Iva', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserVatType->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> null,
        'pos_x'=> null, 
        'pos_y'=> null, 
        'colspan'=> '4',
        'required' => 't', 
        'created_id' => $idUser]);

        $fieldUserCountry = Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'country'],
        [
        'field' => 'country',
        'service'=> 'cities',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserCountry->id, 'language_id' => $idLanguage],
        ['description' => 'Località', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserCountry->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'table_order'=> NULL,
        'pos_x'=> '30', 
        'pos_y'=> '20', 
        'colspan'=> '4',
        'required' => 't', 
        'created_id' => $idUser]);


        $fieldUserProvince = Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'province'],
        [
        'field' => 'province',
        'service'=> 'provinces',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserProvince->id, 'language_id' => $idLanguage],
        ['description' => 'Provincia', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserProvince->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'table_order'=> NULL,
        'pos_x'=> '40', 
        'pos_y'=> '20', 
        'colspan'=> '2',
        'required' => 't', 
        'created_id' => $idUser]);

        $fieldUserMobileNumber = Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'mobile_number'],
        [
        'field' => 'mobile_number',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserMobileNumber->id, 'language_id' => $idLanguage],
        ['description' => 'Cellulare', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserMobileNumber->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '8', 
        'table_order'=> null,
        'pos_x'=> '20', 
        'pos_y'=> '30', 
        'colspan'=> '3',
        'required' => 't', 
        'created_id' => $idUser]);


        $fieldUserPec = Field::updateOrCreate(['tab_id'   => $tabsTableUser->id, 
        'code' => 'pec'],
        [
        'field' => 'pec',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUserPec->id, 'language_id' => $idLanguage],
        ['description' => 'Pec', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUserPec->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0',
        'table_order'=> null, 
        'pos_x'=> '40', 
        'pos_y'=> '30', 
        'colspan'=> '3',
        'required' => 't', 
        'created_id' => $idUser]);


        $tabContentGroupsEnabled = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'UserSetting')->first()->id, 
            'code' => 'GroupsEnabled'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsContentGroupsEnabled =  Tab::where('functionality_id', Functionality::where('code', 'UserSetting')->first()->id)->where('code', 'GroupsEnabled')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabContentGroupsEnabled->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Gruppi abilitati',
        'created_id' => $idUser]);  
        

        $fieldTabContentGroupsEnabled = Field::updateOrCreate(['tab_id'   => $tabContentGroupsEnabled->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTabContentGroupsEnabled->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTabContentGroupsEnabled->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldGroupsEnabled = Field::updateOrCreate(['tab_id'   => $tabContentGroupsEnabled->id, 
        'code' => 'GroupsEnabled'],
        [
         'field' => 'description',   
          'data_type' => '1', 
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldGroupsEnabled->id, 'language_id' => $idLanguage],
        ['description' => 'Gruppi abilitati', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldGroupsEnabled->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '20', 
        'created_id' => $idUser]);



        $tabContentGroupsDisabled= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'UserSetting')->first()->id, 
            'code' => 'GroupsDisabled'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsContentGroupsDisabled =  Tab::where('functionality_id', Functionality::where('code', 'UserSetting')->first()->id)->where('code', 'GroupsDisabled')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabContentGroupsDisabled->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Gruppi disabilitati',
        'created_id' => $idUser]);  
        

        $fieldCheckBoxGroupsDisabled = Field::updateOrCreate(['tab_id'   => $tabContentGroupsDisabled->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCheckBoxGroupsDisabled->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCheckBoxGroupsDisabled->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);


 
      $fieldGroupsDisabled = Field::updateOrCreate(['tab_id'   => $tabContentGroupsDisabled->id, 
        'code' => 'GroupsEnabled'],
        [
         'field' => 'description',   
          'data_type' => '1', 
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldGroupsDisabled->id, 'language_id' => $idLanguage],
        ['description' => 'Gruppi disabilitati', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldGroupsDisabled->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '20', 
        'created_id' => $idUser]);
        // end Setting Configuration Seeder
        //endregion USER SETTING  
          

         $tabConfigurationTable= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'ConfigurationSetting')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsConfigurationTable =  Tab::where('functionality_id', Functionality::where('code', 'ConfigurationSetting')->first()->id)->where('code', 'Table')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabConfigurationTable->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Tabella',
        'created_id' => $idUser]);  
        

        $fieldsConfigurationCheckBox = Field::updateOrCreate(['tab_id'   => $tabConfigurationTable->id, 
        'code' => 'CheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldsConfigurationCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldsConfigurationCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        

        $fieldConfigurationFormatter = Field::updateOrCreate(['tab_id'   => $tabConfigurationTable->id, 
        'code' => 'ConfigurationFormatter'],
        [
        'formatter' => 'configurationFormatter',   
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldConfigurationFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldConfigurationFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

       
        $fieldConfigurationTableCode = Field::updateOrCreate(['tab_id'   => $tabConfigurationTable->id, 
        'code' => 'Code'],
        [
        'field' => 'code', 
        'data_type' => '1', 
        'on_change' => 'changeCode', 
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldConfigurationTableCode->id, 'language_id' => $idLanguage],
        ['description' => 'Codice', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldConfigurationTableCode->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'colspan'=> '4', 
        'created_id' => $idUser]); 

        $fieldConfigurationTablevalue = Field::updateOrCreate(['tab_id'   => $tabConfigurationTable->id, 
        'code' => 'Value'],
        [
        'field' => 'value', 
        'data_type' => '1', 
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldConfigurationTablevalue->id, 'language_id' => $idLanguage],
        ['description' => 'Valore', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldConfigurationTablevalue->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'colspan'=> '4', 
        'created_id' => $idUser]); 

        $fieldConfigurationTableDescription = Field::updateOrCreate(['tab_id'   => $tabConfigurationTable->id, 
        'code' => 'Description'],
        [
        'field' => 'description', 
        'data_type' => '1', 
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldConfigurationTableDescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldConfigurationTableDescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '50', 
        'colspan'=> '4', 
        'created_id' => $idUser]); 


        $tabConfigurationDetail= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'ConfigurationSetting')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsConfigurationDetail =  Tab::where('functionality_id', Functionality::where('code', 'ConfigurationSetting')->first()->id)->where('code', 'Detail')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabConfigurationDetail->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Dettaglio',
        'created_id' => $idUser]);  
        

        $fieldConfigurationDetaileCode = Field::updateOrCreate(['tab_id'   => $tabConfigurationDetail->id, 
        'code' => 'Code'],
        [
        'field' => 'code', 
        'data_type' => '1', 
        'on_change' => 'changeCode', 
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldConfigurationDetaileCode->id, 'language_id' => $idLanguage],
        ['description' => 'Codice', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldConfigurationDetaileCode->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '1', 
        'pos_y'=> '1', 
        'colspan'=> '4', 
        'created_id' => $idUser]); 

        $fieldConfigurationDetailvalue = Field::updateOrCreate(['tab_id'   => $tabConfigurationDetail->id, 
        'code' => 'Value'],
        [
        'field' => 'value', 
        'data_type' => '1', 
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldConfigurationDetailvalue->id, 'language_id' => $idLanguage],
        ['description' => 'Valore', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldConfigurationDetailvalue->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '1', 
        'pos_y'=> '2', 
        'colspan'=> '4', 
        'created_id' => $idUser]); 

        $fieldConfigurationDetailDescription = Field::updateOrCreate(['tab_id'   => $tabConfigurationDetail->id, 
        'code' => 'Description'],
        [
        'field' => 'description', 
        'data_type' => '1', 
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldConfigurationDetailDescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldConfigurationDetailDescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '1', 
        'pos_y'=> '3',  
        'colspan'=> '4', 
        'created_id' => $idUser]); 
        // end configuration setting seeder

        // CONTENT CONFIGURATION SEEDER
        $tabTableContentsConf= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Contents')->first()->id, 
            'code' => 'List'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTableContentsConf =  Tab::where('functionality_id', Functionality::where('code', 'Contents')->first()->id)->where('code', 'List')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableContentsConf->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Lista',
        'created_id' => $idUser]);  
        

        $fieldsTableContentsConfFormatter = Field::updateOrCreate(['tab_id'   => $tabTableContentsConf->id, 
        'code' => 'Formatter'],
        [
        'formatter' => 'contentFormatter',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldsTableContentsConfFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldsTableContentsConfFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '10', 
        'created_id' => $idUser]);


        $fieldCodeTableContentsConf = Field::updateOrCreate(['tab_id'   => $tabTableContentsConf->id, 
        'code' => 'CodeTable'],
        [
        'field' => 'code',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCodeTableContentsConf->id, 'language_id' => $idLanguage],
        ['description' => 'Codice', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCodeTableContentsConf->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '20', 
        'created_id' => $idUser]);


        $fieldTableDescriptionConf = Field::updateOrCreate(['tab_id'   => $tabTableContentsConf->id, 
        'code' => 'Description'],
        [
        'field' => 'description',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableDescriptionConf->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableDescriptionConf->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '30', 
        'created_id' => $idUser]);


        $fieldCodeDetailTableContentsConf = Field::updateOrCreate(['tab_id'   => $tabTableContentsConf->id, 
        'code' => 'CodeDetail'],
        [
        'field' => 'code',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCodeDetailTableContentsConf->id, 'language_id' => $idLanguage],
        ['description' => 'Codice', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCodeDetailTableContentsConf->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '12', 
        'pos_x'=> '10', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

       $fieldLanguageTableContentsConf = Field::updateOrCreate(['tab_id'   => $tabTableContentsConf->id, 
        'code' => 'Language'],
        [
        'field' => 'idLanguage',
        'service' => 'languages',
        'required' => 't',
        'data_type' => '0',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldLanguageTableContentsConf->id, 'language_id' => $idLanguage],
        ['description' => 'Lingua', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldLanguageTableContentsConf->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'pos_x'=> '10', 
        'pos_y'=> '10',
        'colspan'=> '4',
        'created_id' => $idUser]);


        $fieldContentTableContentsConf = Field::updateOrCreate(['tab_id'   => $tabTableContentsConf->id, 
        'code' => 'Description'],
        [
        'field' => 'description',
        'required' => 't',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldContentTableContentsConf->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldContentTableContentsConf->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '12', 
        'pos_x'=> '10', 
        'pos_y'=> '15',
        'colspan'=> '12',
        'created_id' => $idUser]);

        $fieldContentTableContentsConf = Field::updateOrCreate(['tab_id'   => $tabTableContentsConf->id, 
        'code' => 'Content'],
        [
        'field' => 'content',
        'required' => 't',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldContentTableContentsConf->id, 'language_id' => $idLanguage],
        ['description' => 'Contenuto', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldContentTableContentsConf->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '17', 
        'pos_x'=> '10', 
        'pos_y'=> '20',
        'colspan'=> '4',
        'created_id' => $idUser]);
        // END CONTENT CONFIGURATION SEEDER


        // TemplateEditorConfiguration Seeder
            
         $tabTableTemplateEditor= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'TemplateEditor')->first()->id, 
            'code' => 'tableTemplateEditor'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTableTemplateEditor =  Tab::where('functionality_id', Functionality::where('code', 'TemplateEditor')->first()->id)->where('code', 'tableTemplateEditor')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabsTableTemplateEditor->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Blocchi',
        'created_id' => $idUser]);   


        $fieldsTableTemplateEditorCheckBox = Field::updateOrCreate(['tab_id'   => $tabTableTemplateEditor->id, 
        'code' => 'TableCheckBox'],
        [
        'field' => 'TableCheckBox',
        'formatter' => '',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldsTableTemplateEditorCheckBox->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldsTableTemplateEditorCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);


        $fieldTemplateEditorFormatter = Field::updateOrCreate(['tab_id'   => $tabTableTemplateEditor->id, 
        'code' => 'TemplateEditorFormatter'],
        [
        'formatter' => 'templateEditorFormatter',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

        $fieldTemplateEditorDescription = Field::updateOrCreate(['tab_id'   => $tabTableTemplateEditor->id, 
        'code' => 'Description'],
        [
        'field' => 'description',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorDescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorDescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'created_id' => $idUser]);


        $fieldTemplateEditorImage = Field::updateOrCreate(['tab_id'   => $tabTableTemplateEditor->id, 
        'code' => 'Image'],
        [
        'field' => 'img',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorImage->id, 'language_id' => $idLanguage],
        ['description' => 'Immagine', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorImage->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '18', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

        $fieldTemplateEditoridTemplateEditorGroup = Field::updateOrCreate(['tab_id'   => $tabTableTemplateEditor->id, 
        'code' => 'Group'],
        [
        'field' => 'idTemplateEditorsGroup',
        'data_type' => '1',
        'service' => 'templateEditorGroup',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditoridTemplateEditorGroup->id, 'language_id' => $idLanguage],
        ['description' => 'Gruppo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditoridTemplateEditorGroup->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '50', 
        'created_id' => $idUser]);

        $fieldTemplateEditorOrder = Field::updateOrCreate(['tab_id'   => $tabTableTemplateEditor->id, 
        'code' => 'Order'],
        [
        'field' => 'order',
        'data_type' => '0',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorOrder->id, 'language_id' => $idLanguage],
        ['description' => 'Ordinamento', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorOrder->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '7', 
        'table_order'=> '60', 
        'created_id' => $idUser]);


        $tabdetailTemplateEditor= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'TemplateEditor')->first()->id, 
            'code' => 'detailTemplateEditor'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsdetailTemplateEditor =  Tab::where('functionality_id', Functionality::where('code', 'TemplateEditor')->first()->id)->where('code', 'detailTemplateEditor')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabdetailTemplateEditor->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Blocchi',
        'created_id' => $idUser]);   


        $fieldTemplateEditorDescriptionDetail = Field::updateOrCreate(['tab_id'   => $tabsdetailTemplateEditor->id, 
        'code' => 'Description'],
        [
        'field' => 'description',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorDescriptionDetail->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorDescriptionDetail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '10',
        'created_id' => $idUser]);


        $fieldTemplateEditorImageDetail = Field::updateOrCreate(['tab_id'   => $tabsdetailTemplateEditor->id, 
        'code' => 'Image'],
        [
        'field' => 'img',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorImageDetail->id, 'language_id' => $idLanguage],
        ['description' => 'Immagine', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorImageDetail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '18', 
        'pos_x'=> '20', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

        $fieldTemplateEditorGroupDetail = Field::updateOrCreate(['tab_id'   => $tabsdetailTemplateEditor->id, 
        'code' => 'Group'],
        [
        'field' => 'idTemplateEditorGroup',
        'data_type' => '0',
        'service' => 'templateEditorGroup',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupDetail->id, 'language_id' => $idLanguage],
        ['description' => 'Gruppo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupDetail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'pos_x'=> '30', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

        $fieldTemplateEditorOrderGroupDetail = Field::updateOrCreate(['tab_id'   => $tabsdetailTemplateEditor->id, 
        'code' => 'Order'],
        [
        'field' => 'order',
        'data_type' => '0',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorOrderGroupDetail->id, 'language_id' => $idLanguage],
        ['description' => 'Ordinamento', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorOrderGroupDetail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '7', 
        'pos_x'=> '40', 
        'pos_y'=> '10',
        'created_id' => $idUser]);


       /* $fieldTemplateEditorHtmlDetail = Field::updateOrCreate(['tab_id'   => $tabsdetailTemplateEditor->id, 
        'code' => 'Html'],
        [
        'field' => 'html',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorHtmlDetail->id, 'language_id' => $idLanguage],
        ['description' => 'Html', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorHtmlDetail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '17', 
        'pos_x'=> '10', 
        'pos_y'=> '20',
        'created_id' => $idUser]);*/

        //section template editor group 
        $tabTableTemplateEditorGroup= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'TemplateEditorGroup')->first()->id, 
            'code' => 'tableTemplateEditorGroup'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTableTemplateEditorGroup =  Tab::where('functionality_id', Functionality::where('code', 'TemplateEditorGroup')->first()->id)->where('code', 'tableTemplateEditorGroup')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableTemplateEditorGroup->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Blocchi',
        'created_id' => $idUser]);   


        $fieldTemplateEditorGroupCheckBox = Field::updateOrCreate(['tab_id'   => $tabTableTemplateEditorGroup->id, 
        'code' => 'TableCheckBox'],
        [
        'field' => 'TableCheckBox',
        'formatter' => '',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupCheckBox->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);


        $fieldTemplateEditorGroupFormatter = Field::updateOrCreate(['tab_id'   => $tabTableTemplateEditorGroup->id, 
        'code' => 'TemplateEditorGroupFormatter'],
        [
        'formatter' => 'templateEditorGroupFormatter',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

        $fieldTemplateEditorGroupCode = Field::updateOrCreate(['tab_id'   => $tabTableTemplateEditorGroup->id, 
        'code' => 'Code'],
        [
        'field' => 'code',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupCode->id, 'language_id' => $idLanguage],
        ['description' => 'Codice', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupCode->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

        $fieldTemplateEditorGroupDescription = Field::updateOrCreate(['tab_id'   => $tabTableTemplateEditorGroup->id, 
        'code' => 'Description'],
        [
        'field' => 'description',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupDescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupDescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

        $fieldTemplateEditorGroupOrder = Field::updateOrCreate(['tab_id'   => $tabTableTemplateEditorGroup->id, 
        'code' => 'Order'],
        [
        'field' => 'order',
        'data_type' => '0',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupOrder->id, 'language_id' => $idLanguage],
        ['description' => 'Ordinamento', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupOrder->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '7', 
        'table_order'=> '50', 
        'created_id' => $idUser]);

         $fieldTemplateEditorGroupType = Field::updateOrCreate(['tab_id'   => $tabTableTemplateEditorGroup->id, 
        'code' => 'Type'],
        [
        'field' => 'idTemplateEditorType',
        'service' => 'templateEditorType',

        'data_type' => '0',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupType->id, 'language_id' => $idLanguage],
        ['description' => 'Tipologia', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupType->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'table_order'=> '60', 
        'created_id' => $idUser]);

        $tabDetailTemplateEditorGroup= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'TemplateEditorGroup')->first()->id, 
            'code' => 'detailTemplateEditorGroup'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsDetailTemplateEditorGroup =  Tab::where('functionality_id', Functionality::where('code', 'TemplateEditorGroup')->first()->id)->where('code', 'detailTemplateEditorGroup')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailTemplateEditorGroup->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Blocchi',
        'created_id' => $idUser]);   

        $fieldTemplateEditorGroupCodeDetail = Field::updateOrCreate(['tab_id'   => $tabDetailTemplateEditorGroup->id, 
        'code' => 'Code'],
        [
        'field' => 'code',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupCodeDetail->id, 'language_id' => $idLanguage],
        ['description' => 'Codice', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupCodeDetail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'required'=> 't', 
        'pos_x'=> '10', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

       $fieldTemplateEditorGroupDescriptionDetail = Field::updateOrCreate(['tab_id'   => $tabDetailTemplateEditorGroup->id, 
        'code' => 'Description'],
        [
        'field' => 'description',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupDescriptionDetail->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupDescriptionDetail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'required'=> 't', 
        'pos_x'=> '20', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

        $fieldTemplateEditorGroupTypeDetail = Field::updateOrCreate(['tab_id'   => $tabDetailTemplateEditorGroup->id, 
        'code' => 'Type'],
        [
        'field' => 'idTemplateEditorType',
        'data_type' => '0',
        'service' => 'templateEditorType',
        
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupTypeDetail->id, 'language_id' => $idLanguage],
        ['description' => 'Tipologia', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupTypeDetail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'pos_x'=> '30', 
        'pos_y'=> '10',
        'required'=> 't', 
        'created_id' => $idUser]);

        $fieldTemplateEditorGroupOrderDetail = Field::updateOrCreate(['tab_id'   => $tabDetailTemplateEditorGroup->id, 
        'code' => 'Order'],
        [
        'field' => 'order',
        'data_type' => '0',        
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupOrderDetail->id, 'language_id' => $idLanguage],
        ['description' => 'Ordinamento', 
        'created_id' => $idUser]);
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTemplateEditorGroupOrderDetail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '7', 
        'pos_x'=> '40', 
        'pos_y'=> '10',
        'required'=> 't',
        'created_id' => $idUser]);
        // End TemplateEditorConfiguration Seeder

        // type optional seeder
        $tabtabletypeOption= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'TypeOptional')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabstabletypeOption =  Tab::where('functionality_id', Functionality::where('code', 'TypeOptional')->first()->id)->where('code', 'Table')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabtabletypeOption->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Table',
        'created_id' => $idUser]);   

        $fieldTypeOptionCheckBox = Field::updateOrCreate(['tab_id'   => $tabtabletypeOption->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTypeOptionCheckBox->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTypeOptionCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldTypeOptionalFormatter = Field::updateOrCreate(['tab_id'   => $tabtabletypeOption->id, 
        'code' => 'TypeOptionalFormatter'],
        [
        'formatter' => 'TypeOptionalFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTypeOptionalFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTypeOptionalFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

        $fieldTypeOptionalDescription = Field::updateOrCreate(['tab_id'   => $tabtabletypeOption->id, 
        'code' => 'Description'],
        [
        'data_type' => '1',  
        'field' => 'description',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTypeOptionalDescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTypeOptionalDescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

         
        $fieldTypeOptionalOrder = Field::updateOrCreate(['tab_id'   => $tabtabletypeOption->id, 
        'code' => 'Order'],
        [
        'data_type' => '1',  
        'field' => 'order',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTypeOptionalOrder->id, 'language_id' => $idLanguage],
        ['description' => 'Ordinamento', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTypeOptionalOrder->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'created_id' => $idUser]);                  



        $tabtabletypeOption= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'TypeOptional')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabstabletypeOption =  Tab::where('functionality_id', Functionality::where('code', 'TypeOptional')->first()->id)->where('code', 'Detail')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabtabletypeOption->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Dettaglio',
        'created_id' => $idUser]);   


        $fieldTypeOptionalDetailDescription = Field::updateOrCreate(['tab_id'   => $tabtabletypeOption->id, 
        'code' => 'Description'],
        [
        'data_type' => '1',  
        'field' => 'description',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTypeOptionalDetailDescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTypeOptionalDetailDescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);     
        

        $fieldTypeOptionalDetailOrder= Field::updateOrCreate(['tab_id'   => $tabtabletypeOption->id, 
        'code' => 'Order'],
        [
        'data_type' => '1',  
        'field' => 'order',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTypeOptionalDetailOrder->id, 'language_id' => $idLanguage],
        ['description' => 'Ordinamento', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTypeOptionalDetailOrder->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '20', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);   
        
        $fieldTypeOptionalDetailRequired= Field::updateOrCreate(['tab_id'   => $tabtabletypeOption->id, 
        'code' => 'Required'],
        [
        'data_type' => '1',  
        'field' => 'required',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTypeOptionalDetailRequired->id, 'language_id' => $idLanguage],
        ['description' => 'Obbligatorio', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTypeOptionalDetailRequired->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'pos_x'=> '30', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);   
/// end type optional seeder 


// optional seeder 
        $tabtableOption= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Optional')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabstableOption =  Tab::where('functionality_id', Functionality::where('code', 'Optional')->first()->id)->where('code', 'Table')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabtableOption->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Table',
        'created_id' => $idUser]);   

        $fieldOptionCheckBox = Field::updateOrCreate(['tab_id'   => $tabtableOption->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldOptionCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldOptionCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldOptionalFormatter = Field::updateOrCreate(['tab_id'   => $tabtableOption->id, 
        'code' => 'OptionalFormatter'],
        [
        'formatter' => 'OptionalFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldOptionalFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldOptionalFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

        $fieldOptionalDescription = Field::updateOrCreate(['tab_id'   => $tabtableOption->id, 
        'code' => 'Description'],
        [
        'data_type' => '1',  
        'field' => 'description',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldOptionalDescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldOptionalDescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'created_id' => $idUser]);


        $fieldOptionalTypeOptional = Field::updateOrCreate(['tab_id'   => $tabtableOption->id, 
        'code' => 'TypeOptional'],
        [
        'data_type' => '1',  
        'field' => 'typeOptional',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldOptionalTypeOptional->id, 'language_id' => $idLanguage],
        ['description' => 'Tipo di optional', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldOptionalTypeOptional->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'created_id' => $idUser]);     


        $fieldOptionalOrder = Field::updateOrCreate(['tab_id'   => $tabtableOption->id, 
        'code' => 'Order'],
        [
        'data_type' => '1',  
        'field' => 'order',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldOptionalOrder->id, 'language_id' => $idLanguage],
        ['description' => 'Ordinamento', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldOptionalOrder->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '50', 
        'created_id' => $idUser]);                  


        $tabDetailOption= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Optional')->first()->id, 
            'code' => 'detailNation'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsDetailOption =  Tab::where('functionality_id', Functionality::where('code', 'Optional')->first()->id)->where('code', 'detailNation')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailOption->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Dettaglio Optional',
        'created_id' => $idUser]);   


        $fieldOptionalDetailDescription = Field::updateOrCreate(['tab_id'   => $tabDetailOption->id, 
        'code' => 'Description'],
        [
        'data_type' => '1',  
        'field' => 'description',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldOptionalDetailDescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldOptionalDetailDescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);   
        
        $fieldOptionalDetaiLTypeOptionalId= Field::updateOrCreate(['tab_id'   => $tabDetailOption->id, 
        'code' => 'TypeOptionalId'],
        [
        'data_type' => '1',  
        'field' => 'typeOptionalId',
        'service' => 'typeOptional',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldOptionalDetaiLTypeOptionalId->id, 'language_id' => $idLanguage],
        ['description' => 'Tipo di optional', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldOptionalDetaiLTypeOptionalId->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'pos_x'=> '20', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);   
        
        $fieldOptionalDetailOrder= Field::updateOrCreate(['tab_id'   => $tabDetailOption->id, 
        'code' => 'Order'],
        [
        'data_type' => '1',  
        'field' => 'order',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldOptionalDetailOrder->id, 'language_id' => $idLanguage],
        ['description' => 'Ordinamento', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldOptionalDetailOrder->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '30', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);   
        // end optional seeder

        //new table item seeder 
        $tabTableItems= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Items')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTableItems =  Tab::where('functionality_id', Functionality::where('code', 'Items')->first()->id)->where('code', 'Table')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableItems->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Tabella',
        'created_id' => $idUser]);   


        $fieldTableItemsCheckBox = Field::updateOrCreate(['tab_id'   => $tabTableItems->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableItemsCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableItemsCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldItemsFormatter = Field::updateOrCreate(['tab_id'   => $tabTableItems->id, 
        'code' => 'ItemsFormatter'],
        [
        'formatter' => 'ItemsFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldItemsFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldItemsFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);


        $fieldtableitemsInternalCode = Field::updateOrCreate(['tab_id'   => $tabTableItems->id, 
        'code' => 'internal_code'],
        [
        'data_type' => '1',  
        'field' => 'internal_code',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableitemsInternalCode->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Articolo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableitemsInternalCode->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

          $fieldtableItemsdescriptionArticle= Field::updateOrCreate(['tab_id'   => $tabTableItems->id, 
        'code' => 'descriptionArticle'],
        [
        'data_type' => '1',  
        'field' => 'descriptionArticle',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableItemsdescriptionArticle->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione Articolo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableItemsdescriptionArticle->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

        $fieldTABLEitemsdescriptionArticle= Field::updateOrCreate(['tab_id'   => $tabTableItems->id, 
        'code' => 'Url'],
        [
        'data_type' => '1',  
        'field' => 'Url',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTABLEitemsdescriptionArticle->id, 'language_id' => $idLanguage],
        ['description' => 'Url', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTABLEitemsdescriptionArticle->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '50', 
        'created_id' => $idUser]);

        $fieldtableitemsDescriptionCategories= Field::updateOrCreate(['tab_id'   => $tabTableItems->id, 
        'code' => 'descriptionCategories'],
        [
        'data_type' => '1',  
        'field' => 'descriptionCategories',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableitemsDescriptionCategories->id, 'language_id' => $idLanguage],
        ['description' => 'Categorie', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableitemsDescriptionCategories->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '60', 
        'created_id' => $idUser]);

        $fieldtableitemsCheckOnlineFormatter= Field::updateOrCreate(['tab_id'   => $tabTableItems->id, 
        'code' => 'CheckOnlineFormatter'],
        [
        'data_type' => '1',  
        'formatter' => 'CheckOnlineFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableitemsCheckOnlineFormatter->id, 'language_id' => $idLanguage],
        ['description' => 'Online', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableitemsCheckOnlineFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '70', 
        'created_id' => $idUser]);
        //end new table item seeder 


        // REGION ZONE SEEDER
           $tabTableZone= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Zone')->first()->id, 
            'code' => 'TableZone'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTableZone =  Tab::where('functionality_id', Functionality::where('code', 'Zone')->first()->id)->where('code', 'TableZone')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableZone->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Tabella Zone',
        'created_id' => $idUser]);  
        
         $fieldTableZoneCheckBox = Field::updateOrCreate(['tab_id'   => $tabTableZone->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableZoneCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableZoneCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldTableZoneFormatter = Field::updateOrCreate(['tab_id'   => $tabTableZone->id, 
        'code' => 'ZoneFormatter'],
        [
        'formatter' => 'ZoneFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableZoneFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableZoneFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

        $fieldTableZoneIdCities= Field::updateOrCreate(['tab_id'   => $tabTableZone->id, 
        'code' => 'IdCities'],
        [
        'data_type' => '1',  
        'field' => 'IdCities',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableZoneIdCities->id, 'language_id' => $idLanguage],
        ['description' => 'Comuni', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableZoneIdCities->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '60', 
        'created_id' => $idUser]);
        
        $fieldZoneIdPostalCodes= Field::updateOrCreate(['tab_id'   => $tabTableZone->id, 
        'code' => 'IdPostalcodes'],
        [
        'data_type' => '1',  
        'field' => 'IdPostalcodes',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldZoneIdPostalCodes->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Postale', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldZoneIdPostalCodes->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '70', 
        'created_id' => $idUser]);

        $fieldZoneIdProvinces= Field::updateOrCreate(['tab_id'   => $tabTableZone->id, 
        'code' => 'IdProvinces'],
        [
        'data_type' => '1',  
        'field' => 'IdProvinces',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldZoneIdProvinces->id, 'language_id' => $idLanguage],
        ['description' => 'Provincia', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldZoneIdProvinces->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '50', 
        'created_id' => $idUser]);

        $fieldZoneIdRegions= Field::updateOrCreate(['tab_id'   => $tabTableZone->id, 
        'code' => 'IdRegions'],
        [
        'data_type' => '1',  
        'field' => 'IdRegions',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldZoneIdRegions->id, 'language_id' => $idLanguage],
        ['description' => 'Regione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldZoneIdRegions->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

        $fieldZoneIdNations= Field::updateOrCreate(['tab_id'   => $tabTableZone->id, 
        'code' => 'IdNations'],
        [
        'data_type' => '1',  
        'field' => 'IdNations',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldZoneIdNations->id, 'language_id' => $idLanguage],
        ['description' => 'Nazione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldZoneIdNations->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

       $tabDetailZone= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Zone')->first()->id, 
            'code' => 'DetailZone'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsDetailZone =  Tab::where('functionality_id', Functionality::where('code', 'Zone')->first()->id)->where('code', 'DetailZone')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailZone->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Dettaglio Zone',
        'created_id' => $idUser]);  

        $fieldDetailZoneIdCities= Field::updateOrCreate(['tab_id'   => $tabDetailZone->id, 
        'code' => 'id_cities'],
        [
        'service' => 'cities',    
        'data_type' => '1',  
        'field' => 'id_cities',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailZoneIdCities->id, 'language_id' => $idLanguage],
        ['description' => 'Comuni', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailZoneIdCities->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'pos_x'=> '20',
        'pos_y'=> '20',
        'created_id' => $idUser]);
        
        $fieldDetailZoneIdPostalCodes= Field::updateOrCreate(['tab_id'   => $tabDetailZone->id, 
        'code' => 'id_postalcodes'],
        [
         'service' => 'postalCode',     
        'data_type' => '1',  
        'field' => 'id_postalcodes',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailZoneIdPostalCodes->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Postale', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailZoneIdPostalCodes->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'pos_x'=> '10',
        'pos_y'=> '20', 
        'created_id' => $idUser]);

        $fieldDetailZoneIdProvinces= Field::updateOrCreate(['tab_id'   => $tabDetailZone->id, 
        'code' => 'id_provinces'],
        [
        'service' => 'provinces',  
        'data_type' => '1',  
        'field' => 'id_provinces',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailZoneIdProvinces->id, 'language_id' => $idLanguage],
        ['description' => 'Provincia', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailZoneIdProvinces->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'pos_x'=> '30',
        'pos_y'=> '10', 
        'created_id' => $idUser]);

        $fieldDetailZoneIdRegions= Field::updateOrCreate(['tab_id'   => $tabDetailZone->id, 
        'code' => 'id_regions'],
        [
        'service' => 'regions',     
        'data_type' => '1',  
        'field' => 'id_regions',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailZoneIdRegions->id, 'language_id' => $idLanguage],
        ['description' => 'Regione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailZoneIdRegions->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
         'pos_x'=> '20',
        'pos_y'=> '10',  
        'created_id' => $idUser]);

        $fieldDetailZoneIdNations= Field::updateOrCreate(['tab_id'   => $tabDetailZone->id, 
        'code' => 'id_nations'],
        [
        'service' => 'nations',     
        'data_type' => '1',  
        'field' => 'id_nations',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailZoneIdNations->id, 'language_id' => $idLanguage],
        ['description' => 'Nazione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailZoneIdNations->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'pos_x'=> '10',
        'pos_y'=> '10', 
        'created_id' => $idUser]);


        // END REGION ZONE SEEDER
        $tabRegionTable= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Regions')->first()->id, 
            'code' => 'TableRegions'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsRegionTable =  Tab::where('functionality_id', Functionality::where('code', 'Regions')->first()->id)->where('code', 'TableRegions')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabRegionTable->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Tabella Regioni',
        'created_id' => $idUser]);  
        
        $fieldTableRegionCheckBox = Field::updateOrCreate(['tab_id'   => $tabRegionTable->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableRegionCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableRegionCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldTableRegionFormatter = Field::updateOrCreate(['tab_id'   => $tabRegionTable->id, 
        'code' => 'RegionFormatter'],
        [
        'formatter' => 'RegionFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableRegionFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableRegionFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

        $fieldTableRegionDescription = Field::updateOrCreate(['tab_id'   => $tabRegionTable->id, 
        'code' => 'description'],
        [
        'data_type' => '1',  
        'field' => 'description',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableRegionDescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableRegionDescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

        $fieldTableRegionOrder = Field::updateOrCreate(['tab_id'   => $tabRegionTable->id, 
        'code' => 'order'],
        [
        'data_type' => '1',  
        'field' => 'order',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableRegionOrder->id, 'language_id' => $idLanguage],
        ['description' => 'Ordinamento', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableRegionOrder->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

        $tabDetailRegion= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Regions')->first()->id, 
            'code' => 'DetailRegions'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsDetailRegion =  Tab::where('functionality_id', Functionality::where('code', 'Regions')->first()->id)->where('code', 'DetailRegions')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailRegion->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Dettaglio Regioni',
        'created_id' => $idUser]); 

        $fieldDetailRegionDescription = Field::updateOrCreate(['tab_id'   => $tabDetailRegion->id, 
        'code' => 'description'],
        [
        'data_type' => '1',  
        'field' => 'description',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailRegionDescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailRegionDescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

        $fieldDetailRegionOrder = Field::updateOrCreate(['tab_id'   => $tabDetailRegion->id, 
        'code' => 'order'],
        [
        'data_type' => '1',  
        'field' => 'order',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailRegionOrder->id, 'language_id' => $idLanguage],
        ['description' => 'Ordinamento', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailRegionOrder->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '20', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);
         // END REGION REGION SEEDER


        // REGION PROVINCE SEEDER

           $tabProvinceTable= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Provinces')->first()->id, 
            'code' => 'TableProvinces'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsProvinceTable =  Tab::where('functionality_id', Functionality::where('code', 'Provinces')->first()->id)->where('code', 'TableProvinces')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabProvinceTable->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Tabella Province',
        'created_id' => $idUser]);  
        
        $fieldTableProvinceCheckBox = Field::updateOrCreate(['tab_id'   => $tabProvinceTable->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableProvinceCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableProvinceCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldTableProvinceFormatter = Field::updateOrCreate(['tab_id'   => $tabProvinceTable->id, 
        'code' => 'ProvinceFormatter'],
        [
        'formatter' => 'ProvinceFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableProvinceFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableProvinceFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);


        $fieldTableProvinceNation = Field::updateOrCreate(['tab_id'   => $tabProvinceTable->id, 
        'code' => 'nation_id'],
        [
        'field' => 'nation_id',
         'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableProvinceNation->id, 'language_id' => $idLanguage],
        ['description' => 'Nazione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableProvinceNation->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

        $fieldTableProvinceAbbreviation = Field::updateOrCreate(['tab_id'   => $tabProvinceTable->id, 
        'code' => 'abbreviation'],
        [
        'field' => 'abbreviation',
         'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableProvinceAbbreviation->id, 'language_id' => $idLanguage],
        ['description' => 'Abbreviazione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableProvinceAbbreviation->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

        $fieldTableDescription= Field::updateOrCreate(['tab_id'   => $tabProvinceTable->id, 
        'code' => 'description'],
        [
        'field' => 'description',
         'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableDescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableDescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '50', 
        'created_id' => $idUser]);

        $tabProvinceDetail= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Provinces')->first()->id, 
            'code' => 'DetailProvinces'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsProvinceDetail =  Tab::where('functionality_id', Functionality::where('code', 'Provinces')->first()->id)->where('code', 'DetailProvinces')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabProvinceDetail->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Dettaglio Province',
        'created_id' => $idUser]);  
        

        $fieldDetailProvinceNation = Field::updateOrCreate(['tab_id'   => $tabProvinceDetail->id, 
        'code' => 'nation_id'],
        [
        'field' => 'nation_id',
        'service' => 'nations',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailProvinceNation->id, 'language_id' => $idLanguage],
        ['description' => 'Seleziona Nazione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailProvinceNation->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'pos_x'=> '10', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

        $fieldDetailProvinceAbbreviation = Field::updateOrCreate(['tab_id'   => $tabProvinceDetail->id, 
        'code' => 'abbreviation'],
        [
        'field' => 'abbreviation',
         'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailProvinceAbbreviation->id, 'language_id' => $idLanguage],
        ['description' => 'Abbreviazione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailProvinceAbbreviation->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '20', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

        $fieldDetailProvinceDescription = Field::updateOrCreate(['tab_id'   => $tabProvinceDetail->id, 
        'code' => 'description'],
        [
        'field' => 'description',
         'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailProvinceDescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailProvinceDescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '30', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);
        // END REGION PROVINCE SEEDER


        // NATION SEEDER 


        $tabTableNation= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Nations')->first()->id, 
            'code' => 'Content'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTableNation =  Tab::where('functionality_id', Functionality::where('code', 'Nations')->first()->id)->where('code', 'Content')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableNation->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Contenuto',
        'created_id' => $idUser]); 
        
        
        $fieldTableNationCheckBox = Field::updateOrCreate(['tab_id'   => $tabTableNation->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableNationCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableNationCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldTableNationFormatter = Field::updateOrCreate(['tab_id'   => $tabTableNation->id, 
        'code' => 'NationFormatter'],
        [
        'formatter' => 'nationFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableNationFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableNationFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);


        $fieldTableNationCee = Field::updateOrCreate(['tab_id'   => $tabTableNation->id, 
        'code' => 'Cee'],
        [
        'field' => 'cee',
        'data_type' => '3',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableNationCee->id, 'language_id' => $idLanguage],
        ['description' => 'Cee', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableNationCee->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'table_order'=> '30', 
        'pos_x'=> '10', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);


        $fieldTableNationAbbreviation = Field::updateOrCreate(['tab_id'   => $tabTableNation->id, 
        'code' => 'Abbreviation'],
        [
        'field' => 'abbreviation',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableNationAbbreviation->id, 'language_id' => $idLanguage],
        ['description' => 'Abbreviazione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableNationAbbreviation->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'pos_x'=> '20', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);

        $fieldTableNationDescription = Field::updateOrCreate(['tab_id'   => $tabTableNation->id, 
        'code' => 'Description'],
        [
        'field' => 'description',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableNationDescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableNationDescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '50', 
        'pos_x'=> '30', 
        'pos_y'=> '10', 
        'created_id' => $idUser]); 
        // END NATION SEEDER 

        // CITY SEEDER 
         
        $tabTableCity= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Cities')->first()->id, 
            'code' => 'TableCities'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTableCity =  Tab::where('functionality_id', Functionality::where('code', 'Cities')->first()->id)->where('code', 'TableCities')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableCity->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Tabella Comuni',
        'created_id' => $idUser]); 
        
        $fieldTableCityCheckBox = Field::updateOrCreate(['tab_id'   => $tabsTableCity->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableCityCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableCityCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldTableCityFormatter = Field::updateOrCreate(['tab_id'   => $tabsTableCity->id, 
        'code' => 'CityFormatter'],
        [
        'formatter' => 'CityFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableCityFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableCityFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

        $fieldTableCityDescription = Field::updateOrCreate(['tab_id'   => $tabsTableCity->id, 
        'code' => 'description'],
        [
        'field' => 'description',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableCityDescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableCityDescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

        $fieldTableCityOrder= Field::updateOrCreate(['tab_id'   => $tabsTableCity->id, 
        'code' => 'order'],
        [
        'field' => 'order',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableCityOrder->id, 'language_id' => $idLanguage],
        ['description' => 'Ordine', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableCityOrder->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

        $fieldTableCityIstat= Field::updateOrCreate(['tab_id'   => $tabsTableCity->id, 
        'code' => 'istat'],
        [
        'field' => 'istat',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableCityIstat->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Istat', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableCityIstat->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '50', 
        'created_id' => $idUser]);

             $tabDetailCity= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Cities')->first()->id, 
            'code' => 'DetailCities'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsDetailCity =  Tab::where('functionality_id', Functionality::where('code', 'Cities')->first()->id)->where('code', 'DetailCities')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailCity->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Dettaglio Comuni',
        'created_id' => $idUser]); 
        
        $fieldDetailCityDescription = Field::updateOrCreate(['tab_id'   => $tabDetailCity->id, 
        'code' => 'description'],
        [
        'field' => 'description',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCityDescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCityDescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

        $fieldDetailCityOrder= Field::updateOrCreate(['tab_id'   => $tabDetailCity->id, 
        'code' => 'order'],
        [
        'field' => 'order',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCityOrder->id, 'language_id' => $idLanguage],
        ['description' => 'Ordine', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCityOrder->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '20', 
        'pos_y'=> '10',
        'created_id' => $idUser]);

        $fieldDetailCityIstat= Field::updateOrCreate(['tab_id'   => $tabDetailCity->id, 
        'code' => 'istat'],
        [
        'field' => 'istat',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCityIstat->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Istat', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCityIstat->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '30', 
        'pos_y'=> '10',
        'created_id' => $idUser]);
        // END CITY SEEDER 

        //PEOPLE SEEDER 
        $tabTablePeople= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'People')->first()->id, 
            'code' => 'TablePeople'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTablePeople =  Tab::where('functionality_id', Functionality::where('code', 'People')->first()->id)->where('code', 'TablePeople')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTablePeople->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'TablePeople',
        'created_id' => $idUser]); 


        $fieldTablePeopleCheckBox = Field::updateOrCreate(['tab_id'   => $tabTablePeople->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTablePeopleCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTablePeopleCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldTablePeopleFormatter = Field::updateOrCreate(['tab_id'   => $tabTablePeople->id, 
        'code' => 'PeopleFormatter'],
        [
        'formatter' => 'PeopleFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTablePeopleFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTablePeopleFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);


        $fieldTablePeopleName = Field::updateOrCreate(['tab_id'   => $tabTablePeople->id, 
        'code' => 'name'],
        [
        'data_type' => '1',    
        'field' => 'name',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTablePeopleName->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTablePeopleName->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'created_id' => $idUser]);


        $fieldTablePeopleSurname = Field::updateOrCreate(['tab_id'   => $tabTablePeople->id, 
        'code' => 'surname'],
        [
        'data_type' => '1',    
        'field' => 'surname',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTablePeopleSurname->id, 'language_id' => $idLanguage],
        ['description' => 'Cognome', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTablePeopleSurname->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'created_id' => $idUser]);


        $fieldTablePeopleEmail = Field::updateOrCreate(['tab_id'   => $tabTablePeople->id, 
        'code' => 'email'],
        [
        'data_type' => '1',    
        'field' => 'email',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTablePeopleEmail->id, 'language_id' => $idLanguage],
        ['description' => 'Email', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTablePeopleEmail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '50', 
        'created_id' => $idUser]);

        $fieldTablePeoplePhoneNumber = Field::updateOrCreate(['tab_id'   => $tabTablePeople->id, 
        'code' => 'phone_number'],
        [
        'data_type' => '1',    
        'field' => 'phone_number',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTablePeopleEmail->id, 'language_id' => $idLanguage],
        ['description' => 'Telefono', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTablePeopleEmail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '8', 
        'table_order'=> '60', 
        'created_id' => $idUser]);


        $tabDetailPeople= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'People')->first()->id, 
            'code' => 'DetailPeople'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsDetailPeople =  Tab::where('functionality_id', Functionality::where('code', 'People')->first()->id)->where('code', 'DetailPeople')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailPeople->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'DetailPeople',
        'created_id' => $idUser]); 

        $fieldDetailPeopleUser_Id = Field::updateOrCreate(['tab_id'   => $tabDetailPeople->id, 
        'code' => 'user_id'],
        [
        'service' => 'users',  
        'data_type' => '1',    
        'field' => 'user_id',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailPeopleUser_Id->id, 'language_id' => $idLanguage],
        ['description' => 'Utente', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailPeopleUser_Id->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'pos_x'=> '10', 
        'class'=>'userid',
        'pos_y'=> '10', 
        'created_id' => $idUser]);

        $fieldDetailPeopleName = Field::updateOrCreate(['tab_id'   => $tabDetailPeople->id, 
        'code' => 'name'],
        [
        'data_type' => '1',    
        'field' => 'name',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailPeopleName->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailPeopleName->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '20', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);

        $fieldDetailPeopleSurname = Field::updateOrCreate(['tab_id'   => $tabDetailPeople->id, 
        'code' => 'surname'],
        [
        'data_type' => '1',    
        'field' => 'surname',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailPeopleSurname->id, 'language_id' => $idLanguage],
        ['description' => 'Cognome', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailPeopleSurname->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '30', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);

        $fieldDetailPeopleEmail = Field::updateOrCreate(['tab_id'   => $tabDetailPeople->id, 
        'code' => 'email'],
        [
        'data_type' => '1',    
        'field' => 'email',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailPeopleEmail->id, 'language_id' => $idLanguage],
        ['description' => 'Email', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailPeopleEmail->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '20', 
        'pos_y'=> '20', 
        'created_id' => $idUser]);

        $fieldDetailPeoplePhoneNumber = Field::updateOrCreate(['tab_id'   => $tabDetailPeople->id, 
        'code' => 'phone_number'],
        [
        'data_type' => '1',    
        'field' => 'phone_number',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailPeoplePhoneNumber->id, 'language_id' => $idLanguage],
        ['description' => 'Telefono', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailPeoplePhoneNumber->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '8', 
        'pos_x'=> '30', 
        'pos_y'=> '20', 
        'created_id' => $idUser]);
        //END PEOPLE SEEDER 


          $tabTableId= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Message')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTableId =  Tab::where('functionality_id', Functionality::where('code', 'Message')->first()->id)->where('code', 'Table')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableId->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Table',
        'created_id' => $idUser]); 


        $fieldTableMessageCheckBox = Field::updateOrCreate(['tab_id'   => $tabTableId->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableMessageCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableMessageCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldTableMessageFormatter = Field::updateOrCreate(['tab_id'   => $tabTableId->id, 
        'code' => 'MessageFormatter'],
        [
        'formatter' => 'MessageFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableMessageFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableMessageFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);


        $fieldcode= Field::updateOrCreate(['tab_id'   => $tabTableId->id, 
        'code' => 'code'],
        [
        'data_type' => '1',    
        'field' => 'code',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldcode->id, 'language_id' => $idLanguage],
        ['description' => 'Codice', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldcode->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

        $fielddescription = Field::updateOrCreate(['tab_id'   => $tabTableId->id, 
        'code' => 'description'],
        [
        'data_type' => '1',    
        'field' => 'description',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fielddescription->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fielddescription->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'created_id' => $idUser]);


        $tabsDetailId= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Message')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabDetailId =  Tab::where('functionality_id', Functionality::where('code', 'Message')->first()->id)->where('code', 'Detail')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabsDetailId->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Detail',
        'created_id' => $idUser]); 

        $fieldcode = Field::updateOrCreate(['tab_id'   => $tabsDetailId->id, 
        'code' => 'code'],
        [
        'data_type' => '1',    
        'field' => 'code',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldcode->id, 'language_id' => $idLanguage],
        ['description' => 'Codice', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldcode->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);

        $fieldcontent = Field::updateOrCreate(['tab_id'   => $tabsDetailId->id, 
        'code' => 'content'],
        [
        'data_type' => '1',    
        'field' => 'content',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldcontent->id, 'language_id' => $idLanguage],
        ['description' => 'Testo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldcontent->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '17', 
        'pos_x'=> '10', 
        'pos_y'=> '30', 
        'created_id' => $idUser]);


        $fieldmessage_type_id = Field::updateOrCreate(['tab_id'   => $tabsDetailId->id, 
        'code' => 'message_type_id'],
        [
        'data_type' => '1', 
        'service' => 'messagetype',    
        'field' => 'message_type_id',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldmessage_type_id->id, 'language_id' => $idLanguage],
        ['description' => 'Testo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldmessage_type_id->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'pos_x'=> '20', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);

        $fielddescriptions = Field::updateOrCreate(['tab_id'   => $tabsDetailId->id, 
        'code' => 'description'], 
        [
        'data_type' => '1', 
        'field' => 'description',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fielddescriptions->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fielddescriptions->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '20', 
        'created_id' => $idUser]);
        //end message type seeder 

        // newsletter seeder 
     /*   $tabTableNewsletter= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Newsletter')->first()->id, 
            'code' => 'List'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTableNewsletter =  Tab::where('functionality_id', Functionality::where('code', 'Newsletter')->first()->id)->where('code', 'List')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableNewsletter->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Lista',
        'created_id' => $idUser]); 

        $fieldTableNewsletterCheckBox = Field::updateOrCreate(['tab_id'   => $tabTableNewsletter->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableNewsletterCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableNewsletterCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '100', 
        'created_id' => $idUser]);

        $fieldTableNewsletterFormatter = Field::updateOrCreate(['tab_id'   => $tabTableNewsletter->id, 
        'code' => 'NewsletterFormatter'],
        [
        'formatter' => 'newsletterFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableNewsletterFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableNewsletterFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '10', 
        'created_id' => $idUser]);


        $fieldDescriptionTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletter->id, 
        'code' => 'Description'],
        [
        'field' => 'description',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDescriptionTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDescriptionTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '20', 
        'created_id' => $idUser]);


        $fieldTitleTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletter->id, 
        'code' => 'Title'],
        [
        'field' => 'title',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Oggetto', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

         $fieldUrlTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletter->id, 
        'code' => 'Url'],
        [
        'field' => 'url',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUrlTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Url', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUrlTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

        $fieldDraftTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletter->id, 
        'code' => 'Draft'],
        [
        'field' => 'draft',
        'data_type' => '3',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDraftTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Bozza', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDraftTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '90', 
        'created_id' => $idUser]);

        $fieldStartSendDateTimeTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletter->id, 
        'code' => 'StartSendDateTime'],
        [
        'field' => 'startSendDateTime',
        'data_type' => '2',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldStartSendDateTimeTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Data e ora Inizio spedizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldStartSendDateTimeTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '60', 
        'created_id' => $idUser]);

        $fieldEndSendDateTimeTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletter->id, 
        'code' => 'EndSendDateTime'],
        [
        'field' => 'endSendDateTime',
        'data_type' => '2',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldEndSendDateTimeTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Data e ora Fine spedizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldEndSendDateTimeTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '70', 
        'created_id' => $idUser]);

        $fieldNrDestinatariTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletter->id, 
        'code' => 'NrDestinatari'],
        [
        'field' => 'NrDestinatari',
        'formatter' => 'newsletterNumberRecipients',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldNrDestinatariTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Nr. Destinatari', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldNrDestinatariTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '80', 
        'created_id' => $idUser]);


        $tabDetailNewsletter= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Newsletter')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsDetailNewsletter =  Tab::where('functionality_id', Functionality::where('code', 'Newsletter')->first()->id)->where('code', 'Detail')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailNewsletter->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Dettaglio',
        'created_id' => $idUser]); 


         $fieldDescriptionDetailNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabDetailNewsletter->id, 
        'code' => 'Description'],
        [
        'field' => 'description',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDescriptionDetailNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDescriptionDetailNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);

        $fieldTitleDetailNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabDetailNewsletter->id, 
        'code' => 'Title'],
        [
        'field' => 'title',
        'on_change' => 'changeTitle',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTitleDetailNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Oggetto', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTitleDetailNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '20', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);

        $fieldUrlDetailNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabDetailNewsletter->id, 
        'code' => 'Url'],
        [
        'field' => 'url',
        'on_change' => 'checkUrl',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUrlDetailNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Url', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUrlDetailNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '30', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);

        $fieldDraftDetailNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabDetailNewsletter->id, 
        'code' => 'Draft'],
        [
        'field' => 'draft',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDraftDetailNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Bozza', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDraftDetailNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'pos_x'=> '40', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);


        $fieldScheduleDateTimeDetailNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabDetailNewsletter->id, 
        'code' => 'ScheduleDateTime'],
        [
        'field' => 'scheduleDateTime',
        'data_type' => '2',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldScheduleDateTimeDetailNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Data e ora Schedulazione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldScheduleDateTimeDetailNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '4', 
        'pos_x'=> '10', 
        'pos_y'=> '20', 
        'created_id' => $idUser]);

        $fieldStartSendDateTimeDetailNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabDetailNewsletter->id, 
        'code' => 'StartSendDateTime'],
        [
        'field' => 'startSendDateTime',
        'data_type' => '2',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldStartSendDateTimeDetailNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Data e ora Inizio spedizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldStartSendDateTimeDetailNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'pos_x'=> '20', 
        'pos_y'=> '20', 
        'created_id' => $idUser]);

        $fieldEndSendDateTimeDetailNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabDetailNewsletter->id, 
        'code' => 'EndSendDateTime'],
        [
        'field' => 'endSendDateTime',
        'data_type' => '2',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldEndSendDateTimeDetailNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Data e ora Fine spedizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldEndSendDateTimeDetailNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'pos_x'=> '30', 
        'pos_y'=> '20', 
        'created_id' => $idUser]);

        $fieldHtmlDetailNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabDetailNewsletter->id, 
        'code' => 'Html'],
        [
        'field' => 'html',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldHtmlDetailNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Testo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldHtmlDetailNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '17', 
        'pos_x'=> '10', 
        'pos_y'=> '30', 
        'created_id' => $idUser]);*/
        //end newsletter seeder 

        //AddTabDatiSeeder

        $tabDetailItemsDate= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Items')->first()->id, 
            'code' => 'DetailDate'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsDetailItemsDate =  Tab::where('functionality_id', Functionality::where('code', 'Items')->first()->id)->where('code', 'DetailDate')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailItemsDate->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'DetailDate',
        'created_id' => $idUser]); 

        $fieldinternal_codeDetailDataItems = Field::updateOrCreate(['tab_id'   => $tabDetailItemsDate->id, 
        'code' => 'internal_code'],
        [
        'field' => 'internal_code',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldinternal_codeDetailDataItems->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Articolo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldinternal_codeDetailDataItems->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);


        $fielddescriptionDetailDataItems = Field::updateOrCreate(['tab_id'   => $tabDetailItemsDate->id, 
        'code' => 'description'],
        [
        'field' => 'description',
        'data_type' => '1',
        'on_change' => 'changeDescription',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fielddescriptionDetailDataItems->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fielddescriptionDetailDataItems->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '20', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);


        $fieldmeta_tag_linkDetailDataItems = Field::updateOrCreate(['tab_id'   => $tabDetailItemsDate->id, 
        'code' => 'meta_tag_link'],
        [
        'field' => 'meta_tag_link',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldmeta_tag_linkDetailDataItems->id, 'language_id' => $idLanguage],
        ['description' => 'Link', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldmeta_tag_linkDetailDataItems->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '30', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);


        $fieldmeta_tag_characteristicDetailDataItems = Field::updateOrCreate(['tab_id'   => $tabDetailItemsDate->id, 
        'code' => 'meta_tag_characteristic'],
        [
        'field' => 'meta_tag_characteristic',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldmeta_tag_characteristicDetailDataItems->id, 'language_id' => $idLanguage],
        ['description' => 'Caratteristica', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldmeta_tag_characteristicDetailDataItems->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '17', 
        'pos_x'=> '10', 
        'pos_y'=> '20', 
        'created_id' => $idUser]);

        $fieldenabled_fromDetailDataItems = Field::updateOrCreate(['tab_id'   => $tabDetailItemsDate->id, 
        'code' => 'enabled_from'],
        [
        'field' => 'enabled_from',
        'data_type' => '2',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldenabled_fromDetailDataItems->id, 'language_id' => $idLanguage],
        ['description' => 'Visibile dal', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldenabled_fromDetailDataItems->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '4', 
        'pos_x'=> '10', 
        'pos_y'=> '30', 
        'created_id' => $idUser]);

        $fieldenabled_toDetailDataItems = Field::updateOrCreate(['tab_id'   => $tabDetailItemsDate->id, 
        'code' => 'enabled_to'],
        [
        'field' => 'enabled_to',
        'data_type' => '2',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldenabled_toDetailDataItems->id, 'language_id' => $idLanguage],
        ['description' => 'Fino al', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldenabled_toDetailDataItems->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '4', 
        'pos_x'=> '20', 
        'pos_y'=> '30', 
        'created_id' => $idUser]);


        $fieldAnnuncementsDetailDataItems = Field::updateOrCreate(['tab_id'   => $tabDetailItemsDate->id, 
        'code' => 'announcements'],
        [
        'field' => 'announcements',
        'data_type' => '3',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldAnnuncementsDetailDataItems->id, 'language_id' => $idLanguage],
        ['description' => 'Novità', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldAnnuncementsDetailDataItems->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'pos_x'=> '30', 
        'pos_y'=> '30', 
        'colspan'=> '3', 
        'created_id' => $idUser]);

        $fieldAnnuncementsFromDetailDataItems = Field::updateOrCreate(['tab_id'   => $tabDetailItemsDate->id, 
        'code' => 'announcements_from'],
        [
        'field' => 'announcements_from',
        'data_type' => '2',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldAnnuncementsFromDetailDataItems->id, 'language_id' => $idLanguage],
        ['description' => 'Novità dal', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldAnnuncementsFromDetailDataItems->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '4', 
        'pos_x'=> '40', 
        'pos_y'=> '30', 
        'created_id' => $idUser]);

        $fieldAnnuncementsToDetailDataItems = Field::updateOrCreate(['tab_id'   => $tabDetailItemsDate->id, 
        'code' => 'announcements_to'],
        [
        'field' => 'announcements_to',
        'data_type' => '2',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldAnnuncementsToDetailDataItems->id, 'language_id' => $idLanguage],
        ['description' => 'Fino al', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldAnnuncementsToDetailDataItems->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '4', 
        'pos_x'=> '50', 
        'pos_y'=> '30', 
        'created_id' => $idUser]);

        $fieldBasePriceDetailDataItems = Field::updateOrCreate(['tab_id'   => $tabDetailItemsDate->id, 
        'code' => 'base_price'],
        [
        'field' => 'base_price',
        'data_type' => '0',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldBasePriceDetailDataItems->id, 'language_id' => $idLanguage],
        ['description' => 'Prezzo Base', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldBasePriceDetailDataItems->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'class'=> 'base_price', 
        'input_type'=> '7', 
        'pos_x'=> '60', 
        'pos_y'=> '30', 
        'colspan'=> '3', 
        'created_id' => $idUser]);

        $fieldmeta_tag_titleDetailDataItems = Field::updateOrCreate(['tab_id'   => $tabDetailItemsDate->id, 
        'code' => 'meta_tag_title'],
        [
        'field' => 'meta_tag_title',
        'on_change' => 'changeTitleItems',
        'on_keyup' => 'onKeyUpTitleItems',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldmeta_tag_titleDetailDataItems->id, 'language_id' => $idLanguage],
        ['description' => 'Titolo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldmeta_tag_titleDetailDataItems->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10', 
        'pos_y'=> '40', 
        'created_id' => $idUser]);


        $fieldmeta_tag_keywordDetailDataItems = Field::updateOrCreate(['tab_id'   => $tabDetailItemsDate->id, 
        'code' => 'meta_tag_keyword'],
        [
        'field' => 'meta_tag_keyword',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldmeta_tag_keywordDetailDataItems->id, 'language_id' => $idLanguage],
        ['description' => 'Meta Tag Keyword', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldmeta_tag_keywordDetailDataItems->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '20', 
        'pos_y'=> '40', 
        'created_id' => $idUser]);

        $fieldmeta_tag_descriptionDetailDataItems = Field::updateOrCreate(['tab_id'   => $tabDetailItemsDate->id, 
        'code' => 'meta_tag_description'],
        [
        'field' => 'meta_tag_description',
        'on_change' => 'changeDescriptionItems',
        'on_keyup' => 'onKeyUpDescriptionItems',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldmeta_tag_descriptionDetailDataItems->id, 'language_id' => $idLanguage],
        ['description' => 'Meta Tag Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldmeta_tag_descriptionDetailDataItems->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'pos_x'=> '10', 
        'pos_y'=> '50', 
        'created_id' => $idUser]);
        //End AddTabDatiSeeder 

        // Add Tab Detail Price 
       /* $tabDetailItemsAddDetailPrice= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Items')->first()->id, 
            'code' => 'DetailPrice'
        ],
        [
            'order' => 40,
            'created_id' => $idUser
        ]);
        $tabsDetailItemsDate =  Tab::where('functionality_id', Functionality::where('code', 'Items')->first()->id)->where('code', 'DetailPrice')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailItemsAddDetailPrice->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'DetailPrice',
        'created_id' => $idUser]); 

        $fieldpricelist_idDetailPriceListItems = Field::updateOrCreate(['tab_id'   => $tabDetailItemsAddDetailPrice->id, 
        'code' => 'pricelist_id'],
        [
        'field' => 'pricelist_id',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldpricelist_idDetailPriceListItems->id, 'language_id' => $idLanguage],
        ['description' => 'Listino', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldpricelist_idDetailPriceListItems->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '2', 
        'pos_x'=> '10', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);

        $fieldpricelistDetailPriceListItems = Field::updateOrCreate(['tab_id'   => $tabDetailItemsAddDetailPrice->id, 
        'code' => 'pricelist'],
        [
        'field' => 'pricelist',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldpricelistDetailPriceListItems->id, 'language_id' => $idLanguage],
        ['description' => 'Prezzo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldpricelistDetailPriceListItems->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '20', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);*/
        //End Add Tab Detail Price 


        // GROUP NEWSLETTER SEEDER 
        $tabtableGroupNewsletter= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'GroupNewsletter')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabstableGroupNewsletter =  Tab::where('functionality_id', Functionality::where('code', 'GroupNewsletter')->first()->id)->where('code', 'Table')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabtableGroupNewsletter->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Table',
        'created_id' => $idUser]); 

        $fieldtableGroupNewsletterCheckbox = Field::updateOrCreate(['tab_id'   => $tabtableGroupNewsletter->id, 
        'code' => 'TableCheckBox'
        ],
        [
        'data_type' => 1,
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableGroupNewsletterCheckbox->id, 
        'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableGroupNewsletterCheckbox->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 13 ,
        'table_order'=> 10 ,
        'created_id' => $idUser]); 

         $fieldFormatterGroupNewsletter = Field::updateOrCreate(['tab_id'   => $tabtableGroupNewsletter->id, 
         'code' => 'GroupNewsletterFormatter'
        ],
        [
        'formatter' => 'groupNewsletterFormatter',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldFormatterGroupNewsletter->id, 
        'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldFormatterGroupNewsletter->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 14 ,
        'table_order'=>  20 ,
        'created_id' => $idUser]); 

        $fieldDescriptionGroupNewsletter = Field::updateOrCreate(['tab_id'   => $tabtableGroupNewsletter->id, 
         'code' => 'Description'
        ],
        [
        'data_type' => '1',   
        'field' => 'description',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDescriptionGroupNewsletter->id, 
        'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDescriptionGroupNewsletter->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'table_order'=>  30 ,
        'created_id' => $idUser]); 

        $tabDetailGroupNewsletter= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'GroupNewsletter')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsDetailGroupNewsletter =  Tab::where('functionality_id', Functionality::where('code', 'GroupNewsletter')->first()->id)->where('code', 'Table')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailGroupNewsletter->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Detail',
        'created_id' => $idUser]); 

        $fieldDetailDescriptionGroupNewsletter = Field::updateOrCreate(['tab_id'   => $tabDetailGroupNewsletter->id, 
         'code' => 'Description'
        ],
        [
        'data_type' => '1',   
        'field' => 'description',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailDescriptionGroupNewsletter->id, 
        'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailDescriptionGroupNewsletter->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'colspan'=> 6 , 
        'pos_x'=>  10 ,
        'pos_y'=>  10 ,
        'created_id' => $idUser]); 

//
        $tabTableContactGroupNewsletter= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'GroupNewsletter')->first()->id, 
            'code' => 'TableContact'
        ],
        [
            'order' => 30,
            'created_id' => $idUser
        ]);
        $tabsTableContactGroupNewsletter =  Tab::where('functionality_id', Functionality::where('code', 'GroupNewsletter')->first()->id)->where('code', 'TableContact')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableContactGroupNewsletter->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'TableContact',
        'created_id' => $idUser]); 


        $fieldTableContactRagioneSocialeGroupNewsletter = Field::updateOrCreate(['tab_id'   => $tabTableContactGroupNewsletter->id, 
         'code' => 'business_name'
        ],
        [
        'data_type' => '1',   
        'field' => 'business_name',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableContactRagioneSocialeGroupNewsletter->id, 
        'language_id' => $idLanguage],
        ['description' => 'Ragione Sociale', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableContactRagioneSocialeGroupNewsletter->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'table_order'=>  10 ,
        'created_id' => $idUser]); 

        $fieldTableContactNameGroupNewsletter = Field::updateOrCreate(['tab_id'   => $tabTableContactGroupNewsletter->id, 
         'code' => 'name'
        ],
        [
        'data_type' => '1',   
        'field' => 'name',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableContactNameGroupNewsletter->id, 
        'language_id' => $idLanguage],
        ['description' => 'Nome', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableContactNameGroupNewsletter->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'table_order'=>  20 ,
        'created_id' => $idUser]); 

        $fieldTableContactSurnameGroupNewsletter = Field::updateOrCreate(['tab_id'   => $tabTableContactGroupNewsletter->id, 
         'code' => 'surname'
        ],
        [
        'data_type' => '1',   
        'field' => 'surname',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableContactSurnameGroupNewsletter->id, 
        'language_id' => $idLanguage],
        ['description' => 'Cognome', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableContactSurnameGroupNewsletter->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'table_order'=>  30 ,
        'created_id' => $idUser]); 

         $fieldTableContactTelephoneNumberGroupNewsletter = Field::updateOrCreate(['tab_id'   => $tabTableContactGroupNewsletter->id, 
         'code' => 'telephone_number'
        ],
        [
        'data_type' => '1',   
        'field' => 'telephone_number',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableContactTelephoneNumberGroupNewsletter->id, 
        'language_id' => $idLanguage],
        ['description' => 'Telefono', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableContactTelephoneNumberGroupNewsletter->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'table_order'=>  40 ,
        'created_id' => $idUser]); 

        $fieldgroupnewsletterCheckOnlineFormatter= Field::updateOrCreate(['tab_id'   => $tabTableContactGroupNewsletter->id, 
        'code' => 'CheckOnlineFormatter'],
        [
        'data_type' => '1',  
        'formatter' => 'CheckOnlineFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldgroupnewsletterCheckOnlineFormatter->id, 'language_id' => $idLanguage],
        ['description' => 'Nel gruppo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldgroupnewsletterCheckOnlineFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '60', 
        'created_id' => $idUser]);

         $tabTableUserGroupNewsletter= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'GroupNewsletter')->first()->id, 
            'code' => 'TableUser'
        ],
        [
            'order' => 40,
            'created_id' => $idUser
        ]);
        $tabsTableUserGroupNewsletter =  Tab::where('functionality_id', Functionality::where('code', 'GroupNewsletter')->first()->id)->where('code', 'TableUser')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableUserGroupNewsletter->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'TableUser',
        'created_id' => $idUser]); 


        $fieldTableUserRagioneSocialeGroupNewsletter = Field::updateOrCreate(['tab_id'   => $tabTableUserGroupNewsletter->id, 
         'code' => 'business_name'
        ],
        [
        'data_type' => '1',   
        'field' => 'business_name',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableUserRagioneSocialeGroupNewsletter->id, 
        'language_id' => $idLanguage],
        ['description' => 'Ragione Sociale', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableUserRagioneSocialeGroupNewsletter->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'table_order'=>  10 ,
        'created_id' => $idUser]); 

        $fieldTableUserNameGroupNewsletter = Field::updateOrCreate(['tab_id'   => $tabTableUserGroupNewsletter->id, 
         'code' => 'name'
        ],
        [
        'data_type' => '1',   
        'field' => 'name',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableUserNameGroupNewsletter->id, 
        'language_id' => $idLanguage],
        ['description' => 'Nome', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableUserNameGroupNewsletter->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'table_order'=>  20 ,
        'created_id' => $idUser]); 

        $fieldTableUserSurnameGroupNewsletter = Field::updateOrCreate(['tab_id'   => $tabTableUserGroupNewsletter->id, 
         'code' => 'surname'
        ],
        [
        'data_type' => '1',   
        'field' => 'surname',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableUserSurnameGroupNewsletter->id, 
        'language_id' => $idLanguage],
        ['description' => 'Cognome', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableUserSurnameGroupNewsletter->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'table_order'=>  30 ,
        'created_id' => $idUser]); 

         $fieldTableUserTelephoneNumberGroupNewsletter = Field::updateOrCreate(['tab_id'   => $tabTableUserGroupNewsletter->id, 
         'code' => 'telephone_number'
        ],
        [
        'data_type' => '1',   
        'field' => 'telephone_number',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableUserTelephoneNumberGroupNewsletter->id, 
        'language_id' => $idLanguage],
        ['description' => 'Telefono', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableUserTelephoneNumberGroupNewsletter->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'table_order'=>  40 ,
        'created_id' => $idUser]); 

        $fieldgroupnewsletterUserCheckOnlineFormatter= Field::updateOrCreate(['tab_id'   => $tabTableUserGroupNewsletter->id, 
        'code' => 'CheckOnlineUserFormatter'],
        [
        'data_type' => '1',  
        'formatter' => 'CheckOnlineUserFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldgroupnewsletterUserCheckOnlineFormatter->id, 'language_id' => $idLanguage],
        ['description' => 'Nel gruppo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldgroupnewsletterUserCheckOnlineFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '60', 
        'created_id' => $idUser]);
        //END GROUP NEWSLETTER SEEDER


        //AddCategoriesTranslationSeeder 
        $tabDetailTranslation= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Categories')->first()->id, 
            'code' => 'DetailTranslation'
        ],
        [
            'order' => 60,
            'created_id' => $idUser
        ]);
        $tabsDetailTranslation =  Tab::where('functionality_id', Functionality::where('code', 'Categories')->first()->id)->where('code', 'DetailTranslation')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailTranslation->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'DetailTranslation',
        'created_id' => $idUser]); 


        $fieldDetailTranslationLanguageId = Field::updateOrCreate(['tab_id'   => $tabDetailTranslation->id, 
         'code' => 'language_id'
        ],
        [
        'data_type' => '1',   
        'field' => 'language_id',
        'service' => 'languages',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailTranslationLanguageId->id, 
        'language_id' => $idLanguage],
        ['description' => 'Lingua', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailTranslationLanguageId->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 2 ,
        'pos_x'=>  10 ,
        'pos_y'=>  10 ,
        'created_id' => $idUser]); 

        $fieldDetailTranslationDescription = Field::updateOrCreate(['tab_id'   => $tabDetailTranslation->id, 
         'code' => 'description'
        ],
        [
        'data_type' => '1',   
        'field' => 'description',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailTranslationDescription->id, 
        'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailTranslationDescription->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=>  20 ,
        'pos_y'=>  20 ,
        'created_id' => $idUser]); 

        $fieldDetailTranslationmeta_tag_characteristic = Field::updateOrCreate(['tab_id'   => $tabDetailTranslation->id, 
         'code' => 'meta_tag_characteristic'
        ],
        [
        'data_type' => '1',   
        'field' => 'meta_tag_characteristic',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailTranslationmeta_tag_characteristic->id, 
        'language_id' => $idLanguage],
        ['description' => 'Caratteristica', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailTranslationmeta_tag_characteristic->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 17 ,
        'pos_x'=>  30 ,
        'pos_y'=>  30 ,
        'created_id' => $idUser]); 

       $fieldDetailTranslationlink = Field::updateOrCreate(['tab_id'   => $tabDetailTranslation->id, 
         'code' => 'link'
        ],
        [
        'data_type' => '1',   
        'field' => 'link',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailTranslationlink->id, 
        'language_id' => $idLanguage],
        ['description' => 'Link', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailTranslationlink->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=>  40 ,
        'pos_y'=>  40 ,
        'created_id' => $idUser]); 
        //END AddCategoriesTranslationSeeder 


    // AddTranslationSeeder 
        $tabDetailTranslation= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Items')->first()->id, 
            'code' => 'DetailTranslation'
        ],
        [
            'order' => 60,
            'created_id' => $idUser
        ]);
        $tabsDetailTranslation =  Tab::where('functionality_id', Functionality::where('code', 'Categories')->first()->id)->where('code', 'DetailTranslation')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailTranslation->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'DetailTranslation',
        'created_id' => $idUser]); 


        $fieldDetailTranslationLanguageId = Field::updateOrCreate(['tab_id'   => $tabDetailTranslation->id, 
         'code' => 'language_id'
        ],
        [
        'data_type' => '1',   
        'field' => 'language_id',
        'service' => 'languages',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailTranslationLanguageId->id, 
        'language_id' => $idLanguage],
        ['description' => 'Lingua', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailTranslationLanguageId->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 2 ,
        'pos_x'=>  10 ,
        'pos_y'=>  10 ,
        'created_id' => $idUser]); 

        $fieldDetailTranslationDescription = Field::updateOrCreate(['tab_id'   => $tabDetailTranslation->id, 
         'code' => 'description'
        ],
        [
        'data_type' => '1',   
        'field' => 'description',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailTranslationDescription->id, 
        'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailTranslationDescription->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=>  20 ,
        'pos_y'=>  20 ,
        'created_id' => $idUser]); 

        $fieldDetailTranslationmeta_tag_characteristic = Field::updateOrCreate(['tab_id'   => $tabDetailTranslation->id, 
         'code' => 'meta_tag_characteristic'
        ],
        [
        'data_type' => '1',   
        'field' => 'meta_tag_characteristic',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailTranslationmeta_tag_characteristic->id, 
        'language_id' => $idLanguage],
        ['description' => 'Caratteristica', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailTranslationmeta_tag_characteristic->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 17 ,
        'pos_x'=>  30 ,
        'pos_y'=>  30 ,
        'created_id' => $idUser]); 

       $fieldDetailTranslationlink = Field::updateOrCreate(['tab_id'   => $tabDetailTranslation->id, 
         'code' => 'link'
        ],
        [
        'data_type' => '1',   
        'field' => 'link',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailTranslationlink->id, 
        'language_id' => $idLanguage],
        ['description' => 'Link', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailTranslationlink->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=> 40 ,
        'pos_y'=> 40 ,
        'created_id' => $idUser]); 
    //end AddTranslationSeeder 

    //add tab attribute seeder 
    $tabDetailAttribute= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Items')->first()->id, 
            'code' => 'DetailAttribute'
        ],
        [
            'order' => 50,
            'created_id' => $idUser
        ]);
        $tabsDetailAttribute =  Tab::where('functionality_id', Functionality::where('code', 'Items')->first()->id)->where('code', 'DetailAttribute')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailAttribute->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'DetailAttribute',
        'created_id' => $idUser]); 


        $fieldDetailAttributewidth = Field::updateOrCreate(['tab_id'   => $tabDetailAttribute->id, 
         'code' => 'width'
        ],
        [
        'data_type' => '0',   
        'field' => 'width',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailAttributewidth->id, 
        'language_id' => $idLanguage],
        ['description' => 'Larghezza', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailAttributewidth->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 7 ,
        'pos_x'=> 10 ,
        'pos_y'=> 10 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 


      $fieldDetailAttributeheight = Field::updateOrCreate(['tab_id'   => $tabDetailAttribute->id, 
         'code' => 'height'
        ],
        [
        'data_type' => '0',   
        'field' => 'height',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailAttributeheight->id, 
        'language_id' => $idLanguage],
        ['description' => 'Altezza', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailAttributeheight->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 7 ,
        'pos_x'=> 20 ,
        'pos_y'=> 10 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 

        $fieldDetailAttributedepth = Field::updateOrCreate(['tab_id'   => $tabDetailAttribute->id, 
         'code' => 'depth'
        ],
        [
        'data_type' => '0',   
        'field' => 'depth',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailAttributedepth->id, 
        'language_id' => $idLanguage],
        ['description' => 'Profondità', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailAttributedepth->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 7 ,
        'pos_x'=> 30 ,
        'pos_y'=> 10 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 

        $fieldDetailAttributeweight = Field::updateOrCreate(['tab_id'   => $tabDetailAttribute->id, 
         'code' => 'weight'
        ],
        [
        'data_type' => '0',   
        'field' => 'weight',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailAttributeweight->id, 
        'language_id' => $idLanguage],
        ['description' => 'Peso', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailAttributeweight->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 7 ,
        'pos_x'=> 40 ,
        'pos_y'=> 10 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 

        $fieldDetailAttributedelivery_time_if_available = Field::updateOrCreate(['tab_id'   => $tabDetailAttribute->id, 
         'code' => 'delivery_time_if_available'
        ],
        [
        'data_type' => '0',   
        'field' => 'delivery_time_if_available',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailAttributedelivery_time_if_available->id, 
        'language_id' => $idLanguage],
        ['description' => 'Tempo di Consegna se Disponibile', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailAttributedelivery_time_if_available->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 7 ,
        'pos_x'=> 10 ,
        'pos_y'=> 20 ,
        'colspan'=> 6 ,
        'created_id' => $idUser]); 


        $fieldDetailAttributedelivery_time_if_out_stock = Field::updateOrCreate(['tab_id'   => $tabDetailAttribute->id, 
         'code' => 'delivery_time_if_out_stock'
        ],
        [
        'data_type' => '0',   
        'field' => 'delivery_time_if_out_stock',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailAttributedelivery_time_if_out_stock->id, 
        'language_id' => $idLanguage],
        ['description' => 'Tempo di Consegna se esaurito', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailAttributedelivery_time_if_out_stock->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 7 ,
        'pos_x'=> 20 ,
        'pos_y'=> 20 ,
        'colspan'=> 6 ,
        'created_id' => $idUser]); 


        $fieldDetailAttributeadditional_delivery_costs = Field::updateOrCreate(['tab_id'   => $tabDetailAttribute->id, 
         'code' => 'additional_delivery_costs'
        ],
        [
        'data_type' => '0',   
        'field' => 'additional_delivery_costs',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailAttributeadditional_delivery_costs->id, 
        'language_id' => $idLanguage],
        ['description' => 'Costi di Consegna Aggiuntivi', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailAttributeadditional_delivery_costs->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 7 ,
        'pos_x'=> 30 ,
        'pos_y'=> 30 ,
        'colspan'=> 6 ,
        'created_id' => $idUser]); 

     /*   $fieldDetailAttributeimg_attribute = Field::updateOrCreate(['tab_id'   => $tabDetailAttribute->id, 
         'code' => 'img_attribute'
        ],
        [
        'data_type' => '1',   
        'field' => 'img_attribute',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailAttributeimg_attribute->id, 
        'language_id' => $idLanguage],
        ['description' => 'Upload File', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailAttributeimg_attribute->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 18 ,
        'pos_x'=> 10 ,
        'pos_y'=> 40 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); */
    // end add tab attribute seeder 

    // add tab sale items seeder 
       $tabDetailSaleItems= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Items')->first()->id, 
            'code' => 'DetailSale'
        ],
        [
            'order' => 30,
            'created_id' => $idUser
        ]);
        $tabsDetailSaleItems =  Tab::where('functionality_id', Functionality::where('code', 'Items')->first()->id)->where('code', 'DetailSale')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailSaleItems->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'DetailSale',
        'created_id' => $idUser]);   
        
        $fieldavailableDetailSaleItems = Field::updateOrCreate(['tab_id'   => $tabDetailSaleItems->id, 
         'code' => 'available'
        ],
        [
        'data_type' => '1',   
        'field' => 'available',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldavailableDetailSaleItems->id, 
        'language_id' => $idLanguage],
        ['description' => 'Disponibilità', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldavailableDetailSaleItems->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=> 10 ,
        'pos_y'=> 10 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 


       $fieldDetailSaleItemssell_if_not_available = Field::updateOrCreate(['tab_id'   => $tabDetailSaleItems->id, 
         'code' => 'sell_if_not_available'
        ],
        [
        'data_type' => '3',   
        'field' => 'sell_if_not_available',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailSaleItemssell_if_not_available->id, 
        'language_id' => $idLanguage],
        ['description' => 'Vendi se non disponibile', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailSaleItemssell_if_not_available->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 5 ,
        'pos_x'=> 20 ,
        'pos_y'=> 10 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 

        $fieldDetailSaleItemsProducerId = Field::updateOrCreate(['tab_id'   => $tabDetailSaleItems->id, 
         'code' => 'producer_id'
        ],
        [
        'data_type' => '1',   
        'field' => 'producer_id',
        'service' => 'producer',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailSaleItemsProducerId->id, 
        'language_id' => $idLanguage],
        ['description' => 'Produttore', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailSaleItemsProducerId->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 2 ,
        'pos_x'=> 30 ,
        'pos_y'=> 10 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 



        $fieldDetailSaleItemsstock = Field::updateOrCreate(['tab_id'   => $tabDetailSaleItems->id, 
         'code' => 'stock'
        ],
        [
        'data_type' => '0',   
        'field' => 'stock',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailSaleItemsstock->id, 
        'language_id' => $idLanguage],
        ['description' => 'Giacenza', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailSaleItemsstock->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 7 ,
        'pos_x'=> 10 ,
        'pos_y'=> 20 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 


        $fieldDetailSaleItemscommited_customer = Field::updateOrCreate(['tab_id'   => $tabDetailSaleItems->id, 
         'code' => 'commited_customer'
        ],
        [
        'data_type' => '0',   
        'field' => 'commited_customer',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailSaleItemscommited_customer->id, 
        'language_id' => $idLanguage],
        ['description' => 'Impegnato da Cliente', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailSaleItemscommited_customer->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 7 ,
        'class'=> 'committed_customer' ,
        'pos_x'=> 20 ,
        'pos_y'=> 20 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 


        $fieldDetailSaleItemsordered_supplier= Field::updateOrCreate(['tab_id'   => $tabDetailSaleItems->id, 
         'code' => 'ordered_supplier'
        ],
        [
        'data_type' => '0',   
        'field' => 'ordered_supplier',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailSaleItemsordered_supplier->id, 
        'language_id' => $idLanguage],
        ['description' => 'Ordinato a Fornitore', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailSaleItemsordered_supplier->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 7 ,
        'pos_x'=> 30 ,
        'pos_y'=> 20 ,
        'class' => 'ordered_supplier',
        'colspan'=> 3 ,
        'created_id' => $idUser]); 

        $fieldDetailSaleItemsunit_of_measure_id= Field::updateOrCreate(['tab_id'   => $tabDetailSaleItems->id, 
         'code' => 'unit_of_measure_id'
        ],
        [
        'data_type' => '1',   
        'field' => 'unit_of_measure_id',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailSaleItemsunit_of_measure_id->id, 
        'language_id' => $idLanguage],
        ['description' => 'Unità di Misura', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailSaleItemsunit_of_measure_id->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 2 ,
        'pos_x'=> 10 ,
        'pos_y'=> 30 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 


        $fieldDetailSaleItemspieces_for_pack= Field::updateOrCreate(['tab_id'   => $tabDetailSaleItems->id, 
         'code' => 'pieces_for_pack'
        ],
        [
        'data_type' => '0',   
        'field' => 'pieces_for_pack',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailSaleItemspieces_for_pack->id, 
        'language_id' => $idLanguage],
        ['description' => 'Pezzi per Confezione', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailSaleItemspieces_for_pack->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 7 ,
        'pos_x'=> 20 ,
        'pos_y'=> 30 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 

        $fieldDetailSaleItemsunit_of_measure_id_packaging= Field::updateOrCreate(['tab_id'   => $tabDetailSaleItems->id, 
         'code' => 'unit_of_measure_id_packaging'
        ],
        [
        'data_type' => '1',   
        'field' => 'unit_of_measure_id_packaging',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailSaleItemsunit_of_measure_id_packaging->id, 
        'language_id' => $idLanguage],
        ['description' => 'Unità di Misura Confezione', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailSaleItemsunit_of_measure_id_packaging->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 2 ,
        'pos_x'=> 30 ,
        'pos_y'=> 30 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 

      $fieldDetailSaleItemsqtamin= Field::updateOrCreate(['tab_id'   => $tabDetailSaleItems->id, 
         'code' => 'qta_min'
        ],
        [
        'data_type' => '0',   
        'field' => 'qta_min',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailSaleItemsqtamin->id, 
        'language_id' => $idLanguage],
        ['description' => 'Quantità Minima', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailSaleItemsqtamin->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 7 ,
        'pos_x'=> 10 ,
        'pos_y'=> 40 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 

          $fieldDetailSaleItemsqtamax= Field::updateOrCreate(['tab_id'   => $tabDetailSaleItems->id, 
         'code' => 'qta_max'
        ],
        [
        'data_type' => '0',   
        'field' => 'qta_max',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailSaleItemsqtamax->id, 
        'language_id' => $idLanguage],
        ['description' => 'Quantità Massima', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailSaleItemsqtamax->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 7 ,
        'pos_x'=> 20 ,
        'pos_y'=> 40 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 

        $fieldDetailSaleItemsdigital= Field::updateOrCreate(['tab_id'   => $tabDetailSaleItems->id, 
         'code' => 'digital'
        ],
        [
        'data_type' => '3',   
        'field' => 'digital',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailSaleItemsdigital->id, 
        'language_id' => $idLanguage],
        ['description' => 'Digitale', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailSaleItemsdigital->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 5 ,
        'pos_x'=> 10 ,
        'pos_y'=> 50 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 


        $fieldDetailSaleItemsfieldDetailSaleItemsimg_digital= Field::updateOrCreate(['tab_id'   => $tabDetailSaleItems->id, 
         'code' => 'img_digital'
        ],
        [
        'data_type' => '1',   
        'field' => 'img_digital',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailSaleItemsfieldDetailSaleItemsimg_digital->id, 
        'language_id' => $idLanguage],
        ['description' => 'Upload File', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailSaleItemsfieldDetailSaleItemsimg_digital->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 18 ,
        'pos_x'=> 20 ,
        'pos_y'=> 50 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 



        $fieldDetailSaleItemsfieldDetailSaleItemslimited= Field::updateOrCreate(['tab_id'   => $tabDetailSaleItems->id, 
         'code' => 'limited'
        ],
        [
        'data_type' => '3',   
        'field' => 'limited',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailSaleItemsfieldDetailSaleItemslimited->id, 
        'language_id' => $idLanguage],
        ['description' => 'Limitato', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailSaleItemsfieldDetailSaleItemslimited->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 5 ,
        'pos_x'=> 30 ,
        'pos_y'=> 50 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 

        $fieldDetailSaleItemsfieldDetailSaleItemsday_of_validity= Field::updateOrCreate(['tab_id'   => $tabDetailSaleItems->id, 
         'code' => 'day_of_validity'
        ],
        [
        'data_type' => '1',   
        'field' => 'day_of_validity',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailSaleItemsfieldDetailSaleItemsday_of_validity->id, 
        'language_id' => $idLanguage],
        ['description' => 'Giorni di Validità', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailSaleItemsfieldDetailSaleItemsday_of_validity->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=> 40 ,
        'pos_y'=> 50 ,
        'colspan'=> 3 ,
        'created_id' => $idUser]); 
    // end add tab sale items seeder 


 
            // categories 
        $tabTableCategories= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Categories')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTableCategories =  Tab::where('functionality_id', Functionality::where('code', 'Categories')->first()->id)->where('code', 'Table')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableCategories->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Table',
        'created_id' => $idUser]); 

        $fieldTableCategoriesCHECKBOX = Field::updateOrCreate(['tab_id'   => $tabTableCategories->id, 
        'code' => 'TableCheckBox'
        ],
        [
        'data_type' => 1,
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableCategoriesCHECKBOX->id, 
        'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableCategoriesCHECKBOX->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 13 ,
        'table_order'=> 10 ,
        'created_id' => $idUser]); 

         $fieldTableCategoriesFormatter = Field::updateOrCreate(['tab_id'   => $tabTableCategories->id, 
         'code' => 'CategoriesDescriptionFormatter'
        ],
        [
        'formatter' => 'CategoriesDescriptionFormatter',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableCategoriesFormatter->id, 
        'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]); 
     
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableCategoriesFormatter->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 14 ,
        'table_order'=>  20 ,
        'created_id' => $idUser]); 

        $fieldTableCategoriesOrder = Field::updateOrCreate(['tab_id'   => $tabTableCategories->id, 
         'code' => 'OrderCategoriesFormatter'
        ],
        [
        'formatter' => 'OrderCategoriesFormatter',
        'created_id' => $idUser]);    
        
          $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableCategoriesOrder->id, 
        'language_id' => $idLanguage],
        ['description' => 'Ordinamento', 
        'created_id' => $idUser]); 
     
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableCategoriesOrder->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '14' ,
        'table_order'=>  65 ,
        'created_id' => $idUser]); 

        /*$fieldTableCategoriesdescription = Field::updateOrCreate(['tab_id'   => $tabTableCategories->id, 
         'code' => 'description'
        ],
        [
        'data_type' => '1', 
        'field' => 'description',
        'required' => true,    
        'created_id' => $idUser]);    
        
          $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableCategoriesdescription->id, 
        'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]); 
     
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableCategoriesdescription->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'table_order'=>  40 ,
        'created_id' => $idUser]); */


        $tabDetailCategories= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Categories')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsDetailCategories =  Tab::where('functionality_id', Functionality::where('code', 'Categories')->first()->id)->where('code', 'Detail')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailCategories->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Detail',
        'created_id' => $idUser]); 

        $fieldDetailCategoriesCode = Field::updateOrCreate(['tab_id'   => $tabDetailCategories->id, 
         'code' => 'code'
        ],
        [
        'data_type' => '1', 
        'field' => 'code',
        'required' => true,    
        'created_id' => $idUser]);    
        
          $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoriesCode->id, 
        'language_id' => $idLanguage],
        ['description' => 'Codice', 
        'created_id' => $idUser]); 
     
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoriesCode->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=> 10 ,
        'pos_y'=> 10 ,
        'created_id' => $idUser]);  
        
        $fieldDetailCategoriesDescription = Field::updateOrCreate(['tab_id'   => $tabDetailCategories->id, 
         'code' => 'description'
        ],
        [
        'data_type' => '1', 
        'field' => 'description',
        'on_change' => 'changeTitle',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoriesDescription->id, 
        'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]); 
     
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoriesDescription->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=> 20 ,
        'pos_y'=> 10 ,
        'created_id' => $idUser]);  

        $fieldDetailCategoriesCharacteristic = Field::updateOrCreate(['tab_id'   => $tabDetailCategories->id, 
         'code' => 'characteristic'
        ],
        [
        'data_type' => '1', 
        'field' => 'characteristic',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoriesCharacteristic->id, 
        'language_id' => $idLanguage],
        ['description' => 'Caratteristica', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoriesCharacteristic->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 17 ,
        'pos_x'=> 10 ,
        'pos_y'=> 20 ,
        'created_id' => $idUser]);  

        $fieldDetailCategoriesUrl = Field::updateOrCreate(['tab_id'   => $tabDetailCategories->id, 
         'code' => 'link'
        ],
        [
        'data_type' => '1', 
        'field' => 'link',
        'on_change' => 'checkUrl',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoriesUrl->id, 
        'language_id' => $idLanguage],
        ['description' => 'Link', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoriesUrl->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=> 30 ,
        'pos_y'=> 10 ,
        'created_id' => $idUser]);

        $fieldDetailmeta_tag_title = Field::updateOrCreate(['tab_id'   => $tabDetailCategories->id, 
         'code' => 'meta_tag_title'
        ],
        [
        'data_type' => '1', 
        'field' => 'meta_tag_title',
        'on_change' => 'changeMetaTagTitle',
        'on_keyup' => 'onKeyUpTitleItems',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailmeta_tag_title->id, 
        'language_id' => $idLanguage],
        ['description' => 'Meta Tag title', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailmeta_tag_title->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=> 10 ,
        'pos_y'=> 40 ,
        'created_id' => $idUser]);
        
        
        $fieldDetailmeta_tag_Description = Field::updateOrCreate(['tab_id'   => $tabDetailCategories->id, 
         'code' => 'meta_tag_description'
        ],
        [
        'data_type' => '1', 
        'field' => 'meta_tag_description',
        'on_change' => 'changeMetaTagDescription',
        'on_keyup' => 'onKeyUpDescriptionItems',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailmeta_tag_Description->id, 
        'language_id' => $idLanguage],
        ['description' => 'Meta Tag Descrizione', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailmeta_tag_Description->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '1' ,
        'pos_x'=> 10 ,
        'pos_y'=> 50 ,
        'created_id' => $idUser]);

        $fieldDetailmeta_tag_keyword = Field::updateOrCreate(['tab_id'   => $tabDetailCategories->id, 
         'code' => 'meta_tag_keyword'
        ],
        [
        'data_type' => '1', 
        'field' => 'meta_tag_keyword',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailmeta_tag_keyword->id, 
        'language_id' => $idLanguage],
        ['description' => 'Meta Tag Keyword', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailmeta_tag_keyword->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=> 20 ,
        'pos_y'=> 40 ,
        'created_id' => $idUser]);

        $fieldDetailVisibleFrom = Field::updateOrCreate(['tab_id'   => $tabDetailCategories->id, 
         'code' => 'visible_from'
        ],
        [
        'data_type' => '2', 
        'field' => 'visible_from',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailVisibleFrom->id, 
        'language_id' => $idLanguage],
        ['description' => 'Visibile dal', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailVisibleFrom->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 4 ,
        'pos_x'=> 10 ,
        'pos_y'=> 60 ,
        'created_id' => $idUser]);

         $fieldDetailvisible_to = Field::updateOrCreate(['tab_id'   => $tabDetailCategories->id, 
         'code' => 'visible_to'
        ],
        [
        'data_type' => '2', 
        'field' => 'visible_to',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailvisible_to->id, 
        'language_id' => $idLanguage],
        ['description' => 'Fino al', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailvisible_to->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 4 ,
        'pos_x'=> 20 ,
        'pos_y'=> 60 ,
        'created_id' => $idUser]);

          $tabDetailAttribute= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Categories')->first()->id, 
            'code' => 'DetailAttribute'
        ],
        [
            'order' => 30,
            'created_id' => $idUser
        ]);
        $tabsDetailAttribute =  Tab::where('functionality_id', Functionality::where('code', 'Categories')->first()->id)->where('code', 'DetailAttribute')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailAttribute->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'DetailAttribute',
        'created_id' => $idUser]); 


        $fieldDetailCategoryFatherId = Field::updateOrCreate(['tab_id'   => $tabDetailAttribute->id, 
         'code' => 'category_father_id'
        ],
        [
        'data_type' => '1', 
        'service' => 'category',
        'field' => 'category_father_id',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoryFatherId->id, 
        'language_id' => $idLanguage],
        ['description' => 'Categoria Padre', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoryFatherId->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 2 ,
        'pos_x'=> 10 ,
        'pos_y'=> 10 ,
        'created_id' => $idUser]);

        $fieldDetailCategoryTypeId = Field::updateOrCreate(['tab_id'   => $tabDetailAttribute->id, 
         'code' => 'category_type_id'
        ],
        [
        'data_type' => '1', 
        'service' => 'categoryType',
        'field' => 'category_type_id',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoryTypeId->id, 
        'language_id' => $idLanguage],
        ['description' => 'Tipo Categoria', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoryTypeId->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 2 ,
        'pos_x'=> 10 ,
        'pos_y'=> 20 ,
        'created_id' => $idUser]);

        $fieldDetailOrder = Field::updateOrCreate(['tab_id'   => $tabDetailAttribute->id, 
         'code' => 'order'
        ],
        [
        'data_type' => '1', 
        'field' => 'order',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailOrder->id, 
        'language_id' => $idLanguage],
        ['description' => 'Ordinamento', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailOrder->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=> 10 ,
        'pos_y'=> 30 ,
        'created_id' => $idUser]);

        $tabDetailImages= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Categories')->first()->id, 
            'code' => 'DetailImages'
        ],
        [
            'order' => 30,
            'created_id' => $idUser
        ]);
        $tabsDetailImages =  Tab::where('functionality_id', Functionality::where('code', 'Categories')->first()->id)->where('code', 'DetailImages')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailImages->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'DetailImages',
        'created_id' => $idUser]); 

        $fieldDetailOrder = Field::updateOrCreate(['tab_id'   => $tabDetailImages->id, 
         'code' => 'img'
        ],
        [
        'data_type' => '1', 
        'field' => 'img',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailOrder->id, 
        'language_id' => $idLanguage],
        ['description' => 'Immagine Principale', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailOrder->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 18 ,
        'pos_x'=> 10 ,
        'pos_y'=> 10 ,
        'created_id' => $idUser]);

         
        $tabDetailNewsletterCampaign= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Newsletter')->first()->id, 
            'code' => 'DetailHtml'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsDetailNewsletterCampaign =  Tab::where('functionality_id', Functionality::where('code', 'Newsletter')->first()->id)->where('code', 'DetailHtml')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailNewsletterCampaign->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'DetailHtml',
        'created_id' => $idUser]); 


        $fieldDetailHtmlNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabDetailNewsletterCampaign->id, 
        'code' => 'html'],
        [
        'field' => 'html',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailHtmlNewsletterCampaign->id, 'language_id' => $idLanguage],
        [
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailHtmlNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '17', 
        'pos_x'=> '10', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);   

         $tabTableNewsletters= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Newsletter')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTableNewsletters =  Tab::where('functionality_id', Functionality::where('code', 'Newsletter')->first()->id)->where('code', 'Table')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableNewsletters->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Tabella',
        'created_id' => $idUser]); 
        

        $fieldTableNewsletterCheckBox = Field::updateOrCreate(['tab_id'   => $tabTableNewsletters->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableNewsletterCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableNewsletterCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldTableNewsletterFormatter = Field::updateOrCreate(['tab_id'   => $tabTableNewsletters->id, 
        'code' => 'NewsletterFormatter'],
        [
        'formatter' => 'newsletterFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableNewsletterFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableNewsletterFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);


        $fieldtitleTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletters->id, 
        'code' => 'Title'],
        [
        'field' => 'title',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtitleTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Titolo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtitleTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '30', 
        'created_id' => $idUser]);


        $fieldschedule_date_timeTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletters->id, 
        'code' => 'schedule_date_time'],
        [
        'field' => 'schedule_date_time',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldschedule_date_timeTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Data', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldschedule_date_timeTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

         $fieldsenderTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletters->id, 
        'code' => 'sender'],
        [
        'field' => 'sender',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldsenderTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Mittente', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldsenderTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '50', 
        'created_id' => $idUser]);

        $fieldsender_emailTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletters->id, 
        'code' => 'sender_email'],
        [
        'field' => 'sender_email',
        'data_type' => '3',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldsender_emailTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Email Mittente', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldsender_emailTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '60', 
        'created_id' => $idUser]);

        $fieldobjectTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletters->id, 
        'code' => 'object'],
        [
        'field' => 'object',
        'data_type' => '2',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldobjectTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Oggetto', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldobjectTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '70', 
        'created_id' => $idUser]);

        $fieldpreview_objectTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletters->id, 
        'code' => 'preview_object'],
        [
        'field' => 'preview_object',
        'data_type' => '2',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldpreview_objectTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Anteprima', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldpreview_objectTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '80', 
        'created_id' => $idUser]);

        
        $fieldstart_send_date_timeTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletters->id, 
        'code' => 'start_send_date_time'],
        [
        'field' => 'start_send_date_time',
        'data_type' => '15',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldstart_send_date_timeTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Data Inizio spedizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldstart_send_date_timeTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '90', 
        'created_id' => $idUser]);

        $fieldend_send_date_timeTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletters->id, 
        'code' => 'end_send_date_time'],
        [
        'field' => 'end_send_date_time',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldend_send_date_timeTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Data Fine Spedizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldend_send_date_timeTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '100', 
        'created_id' => $idUser]);

        $fieldTableCheckNewsletterFormatter = Field::updateOrCreate(['tab_id'   => $tabTableNewsletters->id, 
        'code' => 'CheckNewsletterFormatter'],
        [
        'formatter' => 'CheckNewsletterFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableCheckNewsletterFormatter->id, 'language_id' => $idLanguage],
        ['description' => 'Bozza', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableCheckNewsletterFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '110', 
        'created_id' => $idUser]);

         $fieldUrlTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletters->id, 
        'code' => 'Url'],
        [
        'field' => 'url',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldUrlTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Url', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldUrlTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '120', 
        'created_id' => $idUser]);

        $fieldNrRecipientTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletters->id, 
        'code' => 'nr_recipients'],
        [
        'field' => 'nr_recipients',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldNrRecipientTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Nr Destinatari', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldNrRecipientTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '130', 
        'created_id' => $idUser]);

        $fieldsenderTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletters->id, 
        'code' => 'nr_sender'],
        [
        'field' => 'nr_sender',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldsenderTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Inviate', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldsenderTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '140', 
        'created_id' => $idUser]);

        $fieldopenTableNewsletterCampaign = Field::updateOrCreate(['tab_id'   => $tabTableNewsletters->id, 
        'code' => 'open'],
        [
        'field' => 'open',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldopenTableNewsletterCampaign->id, 'language_id' => $idLanguage],
        ['description' => 'Aperte', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldopenTableNewsletterCampaign->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '15', 
        'table_order'=> '150', 
        'created_id' => $idUser]);


        $fieldTableDeleteNewsletterFormatter = Field::updateOrCreate(['tab_id'   => $tabTableNewsletters->id, 
        'code' => 'deletenewsletterFormatter'],
        [
        'formatter' => 'deletenewsletterFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableDeleteNewsletterFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableDeleteNewsletterFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '160', 
        'created_id' => $idUser]);


        $tabstableQueryGroupNewsletter = Tab::updateOrCreate(
        [
        'functionality_id' => Functionality::where('code', 'QueryGroupNewsletter')->first()->id,
        'code' => 'Table'
        ],
        [
        'order' => 10,
        'created_id' => $idUser
        ]);

        $tabtableQueryGroupNewsletter = Tab::where('functionality_id', Functionality::where('code', 'QueryGroupNewsletter')->first()->id)->where('code', 'Table')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id' => $tabstableQueryGroupNewsletter->id],
        ['language_id'=> $idLanguage,
        'description'=> 'Table',
        'created_id' => $idUser]);

        $fieldTableCheckBoxQueryGroupNewsletter = Field::updateOrCreate(['tab_id' => $tabstableQueryGroupNewsletter->id,
        'code' => 'TableCheckBox'],
        [
        'field' => 'TableCheckBox',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldTableCheckBoxQueryGroupNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldTableCheckBoxQueryGroupNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '13',
        'table_order'=> '10',
        'created_id' => $idUser]);

        $fieldTableFormatterQueryGroupNewsletter= Field::updateOrCreate(['tab_id' => $tabstableQueryGroupNewsletter->id,
        'code' => 'QueryGroupNewsletterFormatter'],
        [
        'field' => 'QueryGroupNewsletterFormatter',
        'formatter' => 'QueryGroupNewsletterFormatter',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldTableFormatterQueryGroupNewsletter->id, 'language_id' => $idLanguage],
        ['description' => '', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldTableFormatterQueryGroupNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '14',
        'table_order'=> '20',
        'created_id' => $idUser]);

        $fieldNameQueryGroupNewsletter= Field::updateOrCreate(['tab_id' => $tabstableQueryGroupNewsletter->id,
        'code' => 'NameView'],
        [
        'data_type' => '1',
        'field' => 'nameView',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldNameQueryGroupNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldNameQueryGroupNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'table_order'=> '30',
        'created_id' => $idUser]);

        $fieldDescriptionQueryGroupNewsletter= Field::updateOrCreate(['tab_id' => $tabstableQueryGroupNewsletter->id,
        'code' => 'Description'],
        [
        'data_type' => '1',
        'field' => 'description',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDescriptionQueryGroupNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDescriptionQueryGroupNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'table_order'=> '40',
        'created_id' => $idUser]);


        $tabsDetailQueryGroupNewsletter= Tab::updateOrCreate(
        [
        'functionality_id' => Functionality::where('code', 'QueryGroupNewsletter')->first()->id,
        'code' => 'detailQueryGroupNewsl'
        ],
        [
        'order' => 20,
        'created_id' => $idUser
        ]);

        $tabDetailQueryGroupNewsletter = Tab::where('functionality_id', Functionality::where('code', 'QueryGroupNewsletter')->first()->id)->where('code', 'detailQueryGroupNewsl')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id' => $tabsDetailQueryGroupNewsletter->id],
        ['language_id'=> $idLanguage,
        'description'=> 'detailQueryGroupNewsl',
        'created_id' => $idUser]);

        $fieldDetailNameQueryGroupNewsletter= Field::updateOrCreate(['tab_id' => $tabsDetailQueryGroupNewsletter->id,
        'code' => 'NameView'],
        [
        'field' => 'nameView',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailNameQueryGroupNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailNameQueryGroupNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> '10',
        'pos_y'=> '10',
        'created_id' => $idUser]);

        $fieldDetailDescriptionQueryGroupNewsletter= Field::updateOrCreate(['tab_id' => $tabsDetailQueryGroupNewsletter->id,
        'code' => 'Description'],
        [
        'field' => 'description',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailDescriptionQueryGroupNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailDescriptionQueryGroupNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> '20',
        'pos_y'=> '10',
        'created_id' => $idUser]);


        $fieldDetailSelectQueryGroupNewsletter= Field::updateOrCreate(['tab_id' => $tabsDetailQueryGroupNewsletter->id,
        'code' => 'Select'],
        [
        'field' => 'select',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailSelectQueryGroupNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Select', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailSelectQueryGroupNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> '10',
        'pos_y'=> '20',
        'created_id' => $idUser]);

        $fieldDetailFromQueryGroupNewsletter= Field::updateOrCreate(['tab_id' => $tabsDetailQueryGroupNewsletter->id,
        'code' => 'From'],
        [
        'field' => 'from',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailFromQueryGroupNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'From', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailFromQueryGroupNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> '10',
        'pos_y'=> '30',
        'created_id' => $idUser]);

        $fieldDetailWhereQueryGroupNewsletter= Field::updateOrCreate(['tab_id' => $tabsDetailQueryGroupNewsletter->id,
        'code' => 'Where'],
        [
        'field' => 'where',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailWhereQueryGroupNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Where', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailWhereQueryGroupNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> '10',
        'pos_y'=> '40',
        'created_id' => $idUser]);

        $fieldDetailOrderByQueryGroupNewsletter= Field::updateOrCreate(['tab_id' => $tabsDetailQueryGroupNewsletter->id,
        'code' => 'OrderBy'],
        [
        'field' => 'orderBy',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailOrderByQueryGroupNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Order by', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailOrderByQueryGroupNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> '10',
        'pos_y'=> '50',
        'created_id' => $idUser]);

        $fieldDetailGroupByQueryGroupNewsletter= Field::updateOrCreate(['tab_id' => $tabsDetailQueryGroupNewsletter->id,
        'code' => 'GroupBy'],
        [
        'field' => 'groupBy',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailGroupByQueryGroupNewsletter->id, 'language_id' => $idLanguage],
        ['description' => 'Group by', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailGroupByQueryGroupNewsletter->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> '10',
        'pos_y'=> '60',
        'created_id' => $idUser]);

        //section Place    
        $tabTablePlaces= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Places')->first()->id, 
            'code' => 'TablePlaces'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTablePlaces =  Tab::where('functionality_id', Functionality::where('code', 'Places')->first()->id)->where('code', 'TablePlaces')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTablePlaces->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Tabella',
        'created_id' => $idUser]); 
        
        $fieldTablePlacesCheckBox = Field::updateOrCreate(['tab_id'   => $tabTablePlaces->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTablePlacesCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTablePlacesCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldTablePlacesFormatter = Field::updateOrCreate(['tab_id'   => $tabTablePlaces->id, 
        'code' => 'placesFormatter'],
        [
        'formatter' => 'placesFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTablePlacesFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTablePlacesFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

        $fieldNameTablePlaces = Field::updateOrCreate(['tab_id'   => $tabTablePlaces->id, 
        'code' => 'name'],
        [
        'field' => 'name',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldNameTablePlaces->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldNameTablePlaces->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

        $fieldAddressTablePlaces = Field::updateOrCreate(['tab_id'   => $tabTablePlaces->id, 
        'code' => 'address'],
        [
        'field' => 'address',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldAddressTablePlaces->id, 'language_id' => $idLanguage],
        ['description' => 'Indirizzo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldAddressTablePlaces->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

        $fieldCodePostalTablePlaces = Field::updateOrCreate(['tab_id'   => $tabTablePlaces->id, 
        'code' => 'postal_code'],
        [
        'field' => 'postal_code',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCodePostalTablePlaces->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Postale', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCodePostalTablePlaces->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '50', 
        'created_id' => $idUser]);

        $fieldplaceTablePlaces = Field::updateOrCreate(['tab_id'   => $tabTablePlaces->id, 
        'code' => 'place'],
        [
        'field' => 'place',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldplaceTablePlaces->id, 'language_id' => $idLanguage],
        ['description' => 'Località', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldplaceTablePlaces->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '60', 
        'created_id' => $idUser]);

        $fieldprovinceTablePlaces = Field::updateOrCreate(['tab_id'   => $tabTablePlaces->id, 
        'code' => 'Province'],
        [
        'field' => 'Province',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldprovinceTablePlaces->id, 'language_id' => $idLanguage],
        ['description' => 'Provincia', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldprovinceTablePlaces->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '70', 
        'created_id' => $idUser]);

        $fieldnationidTablePlaces = Field::updateOrCreate(['tab_id'   => $tabTablePlaces->id, 
        'code' => 'Nation'],
        [
        'field' => 'Nation',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldnationidTablePlaces->id, 'language_id' => $idLanguage],
        ['description' => 'Stato', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldnationidTablePlaces->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '80', 
        'created_id' => $idUser]);

        $tabDetailPlaces= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Places')->first()->id, 
            'code' => 'DetailPlaces'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsDetailPlaces =  Tab::where('functionality_id', Functionality::where('code', 'Places')->first()->id)->where('code', 'DetailPlaces')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailPlaces->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Dettaglio',
        'created_id' => $idUser]); 

        $fieldDetailNamePlaces= Field::updateOrCreate(['tab_id' => $tabDetailPlaces->id,
        'code' => 'name'],
        [
        'field' => 'name',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailNamePlaces->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailNamePlaces->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> '10',
        'pos_y'=> '10',
        'colspan'=> '4',
        'created_id' => $idUser]);

        $fieldDetailAddressPlaces= Field::updateOrCreate(['tab_id' => $tabDetailPlaces->id,
        'code' => 'address'],
        [
        'field' => 'address',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailAddressPlaces->id, 'language_id' => $idLanguage],
        ['description' => 'Indirizzo', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailAddressPlaces->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> '20',
        'pos_y'=> '20',
        'colspan'=> '6',
        'created_id' => $idUser]);

        $fieldDetailPostalCodePlaces= Field::firstOrCreate(['tab_id' => $tabDetailPlaces->id,
        'code' => 'postal_code'],
        [
        'field' => 'postal_code',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::firstOrCreate(['field_id' => $fieldDetailPostalCodePlaces->id, 'language_id' => $idLanguage],
        ['description' => 'Codice Postale', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::firstOrCreate(['field_id' => $fieldDetailPostalCodePlaces->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '8',
        'pos_x'=> '30',
        'pos_y'=> '30',
        'colspan'=> '2',
        'created_id' => $idUser]);

        $fieldDetailPlacePlaces= Field::firstOrCreate(['tab_id' => $tabDetailPlaces->id,
        'code' => 'place'],
        [
        'field' => 'place',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::firstOrCreate(['field_id' => $fieldDetailPlacePlaces->id, 'language_id' => $idLanguage],
        ['description' => 'Località', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::firstOrCreate(['field_id' => $fieldDetailPlacePlaces->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> '40',
        'pos_y'=> '30',
         'colspan'=> '4',
        'created_id' => $idUser]);

        $fieldDetailProvincePlaces= Field::firstOrCreate(['tab_id' => $tabDetailPlaces->id,
        'code' => 'province'],
        [
        'field' => 'province',
        'service' => 'provinces',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::firstOrCreate(['field_id' => $fieldDetailProvincePlaces->id, 'language_id' => $idLanguage],
        ['description' => 'Provincia', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::firstOrCreate(['field_id' => $fieldDetailProvincePlaces->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '2',
        'pos_x'=> '50',
        'pos_y'=> '30',
         'colspan'=> '1',
        'created_id' => $idUser]);

        $fieldDetailStatesPlaces= Field::firstOrCreate(['tab_id' => $tabDetailPlaces->id,
        'code' => 'nation_id'],
        [
        'field' => 'nation_id',
        'service' => 'nations',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::firstOrCreate(['field_id' => $fieldDetailStatesPlaces->id, 'language_id' => $idLanguage],
        ['description' => 'Stato', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::firstOrCreate(['field_id' => $fieldDetailStatesPlaces->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '2',
        'pos_x'=> '60',
        'pos_y'=> '30',
        'colspan'=> '2',
        'created_id' => $idUser]);

        $fieldDetailLatPlaces= Field::firstOrCreate(['tab_id' => $tabDetailPlaces->id,
        'code' => 'lat'],
        [
        'field' => 'lat',
        'data_type' => '0',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::firstOrCreate(['field_id' => $fieldDetailLatPlaces->id, 'language_id' => $idLanguage],
        ['description' => 'Latitudine', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::firstOrCreate(['field_id' => $fieldDetailLatPlaces->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '7',
        'pos_x'=> '10',
        'pos_y'=> '40',
        'colspan'=> '2',
        'created_id' => $idUser]);

        $fieldDetailLonPlaces= Field::firstOrCreate(['tab_id' => $tabDetailPlaces->id,
        'code' => 'lon'],
        [
        'field' => 'lon',
        'data_type' => '0',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::firstOrCreate(['field_id' => $fieldDetailLonPlaces->id, 'language_id' => $idLanguage],
        ['description' => 'Longitudine', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::firstOrCreate(['field_id' => $fieldDetailLonPlaces->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '7',
        'pos_x'=> '20',
        'pos_y'=> '40',
        'colspan'=> '2',
        'created_id' => $idUser]);
        //end Place

        //section Exceptions    
        $tabTableExceptions= Tab::updateOrCreate(
            [
                'functionality_id'   => Functionality::where('code', 'Exceptions')->first()->id, 
                'code' => 'TableExceptions'
            ],
            [
                'order' => 10,
                'created_id' => $idUser
            ]);
            $tabsTableExceptions =  Tab::where('functionality_id', Functionality::where('code', 'Exceptions')->first()->id)->where('code', 'TableExceptions')->first();
            $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableExceptions->id],
            ['language_id'=> $idLanguage, 
            'description'=> 'Tabella',
            'created_id' => $idUser]); 
            
            $fieldTableExceptionsCheckBox = Field::updateOrCreate(['tab_id'   => $tabTableExceptions->id, 
            'code' => 'TableCheckBox'],
            [
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableExceptionsCheckBox->id, 'language_id' => $idLanguage],
            ['description' => 'CheckBox', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableExceptionsCheckBox->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '13', 
            'table_order'=> '10', 
            'created_id' => $idUser]);
    
            $fieldTableExceptionsFormatter = Field::updateOrCreate(['tab_id'   => $tabTableExceptions->id, 
            'code' => 'ExceptionsFormatter'],
            [
            'formatter' => 'ExceptionsFormatter',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableExceptionsFormatter->id, 'language_id' => $idLanguage],
            ['description' => '', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableExceptionsFormatter->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '14', 
            'table_order'=> '20', 
            'created_id' => $idUser]);
    
            $fieldPlaceIdTableExceptions = Field::updateOrCreate(['tab_id'   => $tabTableExceptions->id, 
            'code' => 'PlaceId'],
            [
            'field' => 'PlaceId',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldPlaceIdTableExceptions->id, 'language_id' => $idLanguage],
            ['description' => 'Luogo', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldPlaceIdTableExceptions->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '30', 
            'created_id' => $idUser]);
    
            $fieldServiceIdTableExceptions = Field::updateOrCreate(['tab_id'   => $tabTableExceptions->id, 
            'code' => 'ServiceId'],
            [
            'field' => 'ServiceId',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldServiceIdTableExceptions->id, 'language_id' => $idLanguage],
            ['description' => 'Servizio', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldServiceIdTableExceptions->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '40', 
            'created_id' => $idUser]);
    
            $fieldCodeEmployee_id_TableExceptions = Field::updateOrCreate(['tab_id'   => $tabTableExceptions->id, 
            'code' => 'EmployeeId'],
            [
            'field' => 'EmployeeId',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCodeEmployee_id_TableExceptions->id, 'language_id' => $idLanguage],
            ['description' => 'Addetto', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCodeEmployee_id_TableExceptions->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '50', 
            'created_id' => $idUser]);
    
            $fieldtooltipTableExceptions = Field::updateOrCreate(['tab_id'   => $tabTableExceptions->id, 
            'code' => 'tooltip'],
            [
            'field' => 'tooltip',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtooltipTableExceptions->id, 'language_id' => $idLanguage],
            ['description' => 'Tooltip', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtooltipTableExceptions->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '60', 
            'created_id' => $idUser]);
    
            $fielddateStartTableExceptions = Field::updateOrCreate(['tab_id'   => $tabTableExceptions->id, 
            'code' => 'date_start'],
            [
            'field' => 'date_start',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fielddateStartTableExceptions->id, 'language_id' => $idLanguage],
            ['description' => 'Data/Ora di Inizio', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fielddateStartTableExceptions->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '70', 
            'created_id' => $idUser]);
    
            $fieldDateEndTableExceptions = Field::updateOrCreate(['tab_id'   => $tabTableExceptions->id, 
            'code' => 'date_end'],
            [
            'field' => 'date_end',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDateEndTableExceptions->id, 'language_id' => $idLanguage],
            ['description' => 'Data/Ora di Fine', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDateEndTableExceptions->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '80', 
            'created_id' => $idUser]);
    
            $tabDetailExceptions= Tab::updateOrCreate(
            [
                'functionality_id'   => Functionality::where('code', 'Exceptions')->first()->id, 
                'code' => 'DetailExceptions'
            ],
            [
                'order' => 20,
                'created_id' => $idUser
            ]);
            $tabsDetailExceptions =  Tab::where('functionality_id', Functionality::where('code', 'Exceptions')->first()->id)->where('code', 'DetailExceptions')->first();
            $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailExceptions->id],
            ['language_id'=> $idLanguage, 
            'description'=> 'Dettaglio',
            'created_id' => $idUser]); 
    
            $fieldDetailPlaceIdExceptions= Field::firstOrCreate(['tab_id' => $tabDetailExceptions->id,
            'code' => 'place_id'],
            [
            'field' => 'place_id',
            'service' => 'bookingplace',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::firstOrCreate(['field_id' => $fieldDetailPlaceIdExceptions->id, 'language_id' => $idLanguage],
            ['description' => 'Luogo', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::firstOrCreate(['field_id' => $fieldDetailPlaceIdExceptions->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '2',
            'pos_x'=> '10',
            'pos_y'=> '10',
            'colspan'=> '6',
            'created_id' => $idUser]);
    
            $fieldDetailServiceIdExceptions= Field::firstOrCreate(['tab_id' => $tabDetailExceptions->id,
            'code' => 'service_id'],
            [
            'field' => 'service_id',
            'service' => 'bookingservices',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::firstOrCreate(['field_id' => $fieldDetailServiceIdExceptions->id, 'language_id' => $idLanguage],
            ['description' => 'Servizio', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::firstOrCreate(['field_id' => $fieldDetailServiceIdExceptions->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '2',
            'pos_x'=> '10',
            'pos_y'=> '20',
            'colspan'=> '6',
            'created_id' => $idUser]);
    
            $fieldDetailEmployeeIdExceptions= Field::firstOrCreate(['tab_id' => $tabDetailExceptions->id,
            'code' => 'employee_id'],
            [
            'field' => 'employee_id',
            'service' => 'bookingemployee',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::firstOrCreate(['field_id' => $fieldDetailEmployeeIdExceptions->id, 'language_id' => $idLanguage],
            ['description' => 'Addetto', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::firstOrCreate(['field_id' => $fieldDetailEmployeeIdExceptions->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '2',
            'pos_x'=> '10',
            'pos_y'=> '30',
            'colspan'=> '6',
            'created_id' => $idUser]);
    
            $fieldDetailTooltipExceptions= Field::firstOrCreate(['tab_id' => $tabDetailExceptions->id,
            'code' => 'tooltip'],
            [
            'field' => 'tooltip',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailTooltipExceptions->id, 'language_id' => $idLanguage],
            ['description' => 'Tooltip', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailTooltipExceptions->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '1',
            'pos_x'=> '20',
            'pos_y'=> '10',
            'colspan'=> '6',
            'created_id' => $idUser]);
    
            $fieldDetailDateStartExceptionss= Field::firstOrCreate(['tab_id' => $tabDetailExceptions->id,
            'code' => 'date_start'],
            [
            'field' => 'date_start',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::firstOrCreate(['field_id' => $fieldDetailDateStartExceptionss->id, 'language_id' => $idLanguage],
            ['description' => 'Data Inizio', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::firstOrCreate(['field_id' => $fieldDetailDateStartExceptionss->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '4',
            'pos_x'=> '20',
            'pos_y'=> '30',
            'colspan'=> '3',
            'created_id' => $idUser]);
    
            $fieldDetailDateEndExceptions= Field::firstOrCreate(['tab_id' => $tabDetailExceptions->id,
            'code' => 'date_end'],
            [
            'field' => 'date_end',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::firstOrCreate(['field_id' => $fieldDetailDateEndExceptions->id, 'language_id' => $idLanguage],
            ['description' => 'Data Fine', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::firstOrCreate(['field_id' => $fieldDetailDateEndExceptions->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '4',
            'pos_x'=> '30',
            'pos_y'=> '30',
            'colspan'=> '3',
            'created_id' => $idUser]);
            //end Exceptions


        //section Connections    
        $tabTableConnections= Tab::updateOrCreate(
            [
                'functionality_id'   => Functionality::where('code', 'Connections')->first()->id, 
                'code' => 'TableConnections'
            ],
            [
                'order' => 10,
                'created_id' => $idUser
            ]);
            $tabsTableConnections =  Tab::where('functionality_id', Functionality::where('code', 'Connections')->first()->id)->where('code', 'TableConnections')->first();
            $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableConnections->id],
            ['language_id'=> $idLanguage, 
            'description'=> 'Tabella',
            'created_id' => $idUser]); 
            
            $fieldTableConnectionsCheckBox = Field::updateOrCreate(['tab_id'   => $tabTableConnections->id, 
            'code' => 'TableCheckBox'],
            [
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableConnectionsCheckBox->id, 'language_id' => $idLanguage],
            ['description' => 'CheckBox', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableConnectionsCheckBox->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '13', 
            'table_order'=> '10', 
            'created_id' => $idUser]);
    
            $fieldTableConnectionsFormatter = Field::updateOrCreate(['tab_id'   => $tabTableConnections->id, 
            'code' => 'ConnectionsFormatter'],
            [
            'formatter' => 'ConnectionsFormatter',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableConnectionsFormatter->id, 'language_id' => $idLanguage],
            ['description' => '', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableConnectionsFormatter->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '14', 
            'table_order'=> '20', 
            'created_id' => $idUser]);
    
            $fieldPlaceIdTableConnections = Field::updateOrCreate(['tab_id'   => $tabTableConnections->id, 
            'code' => 'PlaceId'],
            [
            'field' => 'PlaceId',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldPlaceIdTableConnections->id, 'language_id' => $idLanguage],
            ['description' => 'Luogo', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldPlaceIdTableConnections->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '30', 
            'created_id' => $idUser]);
    
            $fieldServiceIdTableConnections = Field::updateOrCreate(['tab_id'   => $tabTableConnections->id, 
            'code' => 'ServiceId'],
            [
            'field' => 'ServiceId',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldServiceIdTableConnections->id, 'language_id' => $idLanguage],
            ['description' => 'Servizio', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldServiceIdTableConnections->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '40', 
            'created_id' => $idUser]);
    
            $fieldCodeEmployee_id_TableConnections = Field::updateOrCreate(['tab_id'   => $tabTableConnections->id, 
            'code' => 'EmployeeId'],
            [
            'field' => 'EmployeeId',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCodeEmployee_id_TableConnections->id, 'language_id' => $idLanguage],
            ['description' => 'Addetto', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCodeEmployee_id_TableConnections->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '50', 
            'created_id' => $idUser]);
    
            $fieldNrPlaceTableConnections = Field::updateOrCreate(['tab_id'   => $tabTableConnections->id, 
            'code' => 'NrPlace'],
            [
            'field' => 'NrPlace',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldNrPlaceTableConnections->id, 'language_id' => $idLanguage],
            ['description' => 'Posti', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldNrPlaceTableConnections->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '60', 
            'created_id' => $idUser]);
    
            $fieldFixedDateTableConnections = Field::updateOrCreate(['tab_id'   => $tabTableConnections->id, 
            'code' => 'ShowAvailabilityCalendar'],
            [
            'field' => 'ShowAvailabilityCalendar',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldFixedDateTableConnections->id, 'language_id' => $idLanguage],
            ['description' => 'Disponibilità in calendario', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldFixedDateTableConnections->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '70', 
            'created_id' => $idUser]);
    
            $fieldFixedDateTableConnections = Field::updateOrCreate(['tab_id'   => $tabTableConnections->id, 
            'code' => 'FixedDate'],
            [
            'field' => 'FixedDate',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldFixedDateTableConnections->id, 'language_id' => $idLanguage],
            ['description' => 'Data Fissa', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldFixedDateTableConnections->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '80', 
            'created_id' => $idUser]);

            $fieldDateFromTableConnections = Field::updateOrCreate(['tab_id'   => $tabTableConnections->id, 
            'code' => 'DateFrom'],
            [
            'field' => 'DateFrom',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDateFromTableConnections->id, 'language_id' => $idLanguage],
            ['description' => 'Dal', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDateFromTableConnections->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '90', 
            'created_id' => $idUser]);

            $fieldDateToTableConnections = Field::updateOrCreate(['tab_id'   => $tabTableConnections->id, 
            'code' => 'DateFrom'],
            [
            'field' => 'DateFrom',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDateToTableConnections->id, 'language_id' => $idLanguage],
            ['description' => 'Dal', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDateToTableConnections->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '100', 
            'created_id' => $idUser]);

            $fieldDateToTableConnections = Field::updateOrCreate(['tab_id'   => $tabTableConnections->id, 
            'code' => 'StartTime'],
            [
            'field' => 'StartTime',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDateToTableConnections->id, 'language_id' => $idLanguage],
            ['description' => 'Ora Inizio', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDateToTableConnections->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '105', 
            'created_id' => $idUser]);

            $fieldDateToTableConnections = Field::updateOrCreate(['tab_id'   => $tabTableConnections->id, 
            'code' => 'EndTime'],
            [
            'field' => 'EndTime',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDateToTableConnections->id, 'language_id' => $idLanguage],
            ['description' => 'Ora Fine', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDateToTableConnections->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '110', 
            'created_id' => $idUser]);


    
            $tabDetailConnections= Tab::updateOrCreate(
            [
                'functionality_id'   => Functionality::where('code', 'Connections')->first()->id, 
                'code' => 'DetailConnections'
            ],
            [
                'order' => 20,
                'created_id' => $idUser
            ]);
            $tabsDetailConnections =  Tab::where('functionality_id', Functionality::where('code', 'Connections')->first()->id)->where('code', 'DetailConnections')->first();
            $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailConnections->id],
            ['language_id'=> $idLanguage, 
            'description'=> 'Dettaglio',
            'created_id' => $idUser]); 
    
            $fieldDetailPlaceIdConnections= Field::updateOrCreate(['tab_id' => $tabDetailConnections->id,
            'code' => 'place_id'],
            [
            'field' => 'place_id',
            'service' => 'bookingplace',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailPlaceIdConnections->id, 'language_id' => $idLanguage],
            ['description' => 'Luogo', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailPlaceIdConnections->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '2',
            'pos_x'=> '10',
            'pos_y'=> '10',
            'colspan'=> '12',
            'created_id' => $idUser]);
    
            $fieldDetailServiceIdConnections= Field::updateOrCreate(['tab_id' => $tabDetailConnections->id,
            'code' => 'service_id'],
            [
            'field' => 'service_id',
            'service' => 'bookingservices',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailServiceIdConnections->id, 'language_id' => $idLanguage],
            ['description' => 'Servizio', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailServiceIdConnections->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '2',
            'pos_x'=> '10',
            'pos_y'=> '20',
            'colspan'=> '12',
            'created_id' => $idUser]);
    
            $fieldDetailEmployeeIdConnections= Field::updateOrCreate(['tab_id' => $tabDetailConnections->id,
            'code' => 'employee_id'],
            [
            'field' => 'employee_id',
            'service' => 'bookingemployee',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailEmployeeIdConnections->id, 'language_id' => $idLanguage],
            ['description' => 'Addetto', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailEmployeeIdConnections->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '2',
            'pos_x'=> '10',
            'pos_y'=> '30',
            'colspan'=> '12',
            'created_id' => $idUser]);



            $tabDetailConnections2= Tab::updateOrCreate(
            [
                'functionality_id'   => Functionality::where('code', 'Connections')->first()->id, 
                'code' => 'DetailConnections2'
            ],
            [
                'order' => 30,
                'created_id' => $idUser
            ]);
            $tabsDetailConnections2 =  Tab::where('functionality_id', Functionality::where('code', 'Connections')->first()->id)->where('code', 'DetailConnections2')->first();
            $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailConnections2->id],
            ['language_id'=> $idLanguage, 
            'description'=> 'Dettaglio',
            'created_id' => $idUser]); 


            $fieldDetailnrplaceConnections= Field::updateOrCreate(['tab_id' => $tabDetailConnections2->id,
            'code' => 'nr_place'],
            [
            'field' => 'nr_place',
            'data_type' => '0',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailnrplaceConnections->id, 'language_id' => $idLanguage],
            ['description' => 'Posti', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailnrplaceConnections->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '7',
            'pos_x'=> '10',
            'pos_y'=> '40',
            'colspan'=> '3',
            'created_id' => $idUser]);

            $fieldDetailshowavailabilitycalendarConnections= Field::updateOrCreate(['tab_id' => $tabDetailConnections2->id,
            'code' => 'show_availability_calendar'],
            [
            'field' => 'show_availability_calendar',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailshowavailabilitycalendarConnections->id, 'language_id' => $idLanguage],
            ['description' => 'Mostra disponibilità', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailshowavailabilitycalendarConnections->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '5',
            'pos_x'=> '20',
            'pos_y'=> '40',
            'colspan'=> '2',
            'created_id' => $idUser]);


            
            $tabDetailConnections3= Tab::updateOrCreate(
                [
                    'functionality_id'   => Functionality::where('code', 'Connections')->first()->id, 
                    'code' => 'DetailConnections3'
                ],
                [
                    'order' => 40,
                    'created_id' => $idUser
                ]);
                $tabsDetailConnections3 =  Tab::where('functionality_id', Functionality::where('code', 'Connections')->first()->id)->where('code', 'DetailConnections3')->first();
                $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailConnections3->id],
                ['language_id'=> $idLanguage, 
                'description'=> 'Dettaglio',
                'created_id' => $idUser]); 

                $fieldDetailConnection3checkMultipleConnections= Field::updateOrCreate(['tab_id' => $tabDetailConnections3->id,
                'code' => 'check_multiple'],
                [
                'field' => 'check_multiple',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailConnection3checkMultipleConnections->id, 'language_id' => $idLanguage],
                ['description' => 'Multiplo', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailConnection3checkMultipleConnections->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '5',
                'pos_x'=> '10',
                'pos_y'=> '50',
                'colspan'=> '2',
                'created_id' => $idUser]);

                $fieldDetailConnectionsMinDayConnections= Field::updateOrCreate(['tab_id' => $tabDetailConnections3->id,
                'code' => 'min_day'],
                [
                'field' => 'min_day',
                'data_type' => '0',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailConnectionsMinDayConnections->id, 'language_id' => $idLanguage],
                ['description' => 'Nr.Giorni', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailConnectionsMinDayConnections->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '7',
                'pos_x'=> '20',
                'pos_y'=> '50',
                'colspan'=> '2',
                'created_id' => $idUser]);
            //end Connections


            //section Booking Employees    
        $tabTableEmployees= Tab::updateOrCreate(
            [
                'functionality_id'   => Functionality::where('code', 'Employees')->first()->id, 
                'code' => 'TableEmployees'
            ],
            [
                'order' => 10,
                'created_id' => $idUser
            ]);
            $tabsTableEmployees =  Tab::where('functionality_id', Functionality::where('code', 'Employees')->first()->id)->where('code', 'TableEmployees')->first();
            $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableEmployees->id],
            ['language_id'=> $idLanguage, 
            'description'=> 'Tabella',
            'created_id' => $idUser]); 
            
            $fieldTableEmployeesCheckBox = Field::updateOrCreate(['tab_id'   => $tabTableEmployees->id, 
            'code' => 'TableCheckBox'],
            [
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableEmployeesCheckBox->id, 'language_id' => $idLanguage],
            ['description' => 'CheckBox', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableEmployeesCheckBox->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '13', 
            'table_order'=> '10', 
            'created_id' => $idUser]);
    
            $fieldTableEmployeesFormatter = Field::updateOrCreate(['tab_id'   => $tabTableEmployees->id, 
            'code' => 'EmployeesFormatter'],
            [
            'formatter' => 'EmployeesFormatter',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableEmployeesFormatter->id, 'language_id' => $idLanguage],
            ['description' => '', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableEmployeesFormatter->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '14', 
            'table_order'=> '20', 
            'created_id' => $idUser]);
    
            $fieldNameTableEmployees = Field::updateOrCreate(['tab_id'   => $tabTableEmployees->id, 
            'code' => 'name'],
            [
            'field' => 'name',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldNameTableEmployees->id, 'language_id' => $idLanguage],
            ['description' => 'Nome', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldNameTableEmployees->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '30', 
            'created_id' => $idUser]);
    
            $fieldDescriptionTableEmployees = Field::updateOrCreate(['tab_id'   => $tabTableEmployees->id, 
            'code' => 'description'],
            [
            'field' => 'description',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDescriptionTableEmployees->id, 'language_id' => $idLanguage],
            ['description' => 'Descrizione', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDescriptionTableEmployees->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '40', 
            'created_id' => $idUser]);
    
            $fieldEmailTableEmployees = Field::updateOrCreate(['tab_id'   => $tabTableEmployees->id, 
            'code' => 'email'],
            [
            'field' => 'email',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldEmailTableEmployees->id, 'language_id' => $idLanguage],
            ['description' => 'Email', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldEmailTableEmployees->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '50', 
            'created_id' => $idUser]);
    
            $fieldtelephone_numberTableEmployees = Field::updateOrCreate(['tab_id'   => $tabTableEmployees->id, 
            'code' => 'telephone_number'],
            [
            'field' => 'telephone_number',
            'data_type' => '1',
            'created_id' => $idUser]); 
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtelephone_numberTableEmployees->id, 'language_id' => $idLanguage],
            ['description' => 'Telefono', 
            'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtelephone_numberTableEmployees->id, 'role_id' => $idRole],
            ['enabled' => 't', 
            'input_type'=> '1', 
            'table_order'=> '60', 
            'created_id' => $idUser]);

    
            $tabDetailEmployees= Tab::updateOrCreate(
            [
                'functionality_id'   => Functionality::where('code', 'Employees')->first()->id, 
                'code' => 'DetailEmployees'
            ],
            [
                'order' => 20,
                'created_id' => $idUser
            ]);
            $tabsDetailEmployees =  Tab::where('functionality_id', Functionality::where('code', 'Employees')->first()->id)->where('code', 'DetailEmployees')->first();
            $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailEmployees->id],
            ['language_id'=> $idLanguage, 
            'description'=> 'Dettaglio',
            'created_id' => $idUser]); 
    
            $fieldDetailNameEmployees= Field::updateOrCreate(['tab_id' => $tabDetailEmployees->id,
            'code' => 'name'],
            [
            'field' => 'name',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailNameEmployees->id, 'language_id' => $idLanguage],
            ['description' => 'Nome', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailNameEmployees->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '0',
            'pos_x'=> '10',
            'pos_y'=> '10',
            'colspan'=> '2',
            'created_id' => $idUser]);
    
            $fieldDetailDescriptionEmployees= Field::updateOrCreate(['tab_id' => $tabDetailEmployees->id,
            'code' => 'description'],
            [
            'field' => 'description',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailDescriptionEmployees->id, 'language_id' => $idLanguage],
            ['description' => 'Descrizione', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailDescriptionEmployees->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '0',
            'pos_x'=> '10',
            'pos_y'=> '20',
            'colspan'=> '8',
            'created_id' => $idUser]);
    
            $fieldDetailEmailEmployees= Field::updateOrCreate(['tab_id' => $tabDetailEmployees->id,
            'code' => 'email'],
            [
            'field' => 'email',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailEmailEmployees->id, 'language_id' => $idLanguage],
            ['description' => 'Email', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailEmailEmployees->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '0',
            'pos_x'=> '10',
            'pos_y'=> '30',
            'colspan'=> '4',
            'created_id' => $idUser]);
    
            $fieldDetailTelephoneNumberEmployees= Field::firstOrCreate(['tab_id' => $tabDetailEmployees->id,
            'code' => 'telephone_number'],
            [
            'field' => 'telephone_number',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailTelephoneNumberEmployees->id, 'language_id' => $idLanguage],
            ['description' => 'Telefono', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailTelephoneNumberEmployees->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '0',
            'pos_x'=> '10',
            'pos_y'=> '40',
            'colspan'=> '2',
            'created_id' => $idUser]);
    
            //end booking Employees



        // booking Services 
         $tabTableBookingServices= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'BookingServices')->first()->id, 
            'code' => 'TableBookingServices'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTableBookingServices =  Tab::where('functionality_id', Functionality::where('code', 'BookingServices')->first()->id)->where('code', 'TableBookingServices')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableBookingServices->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Tabella',
        'created_id' => $idUser]); 
        
        $fieldTableBookingServicesCheckBox = Field::updateOrCreate(['tab_id'   => $tabTableBookingServices->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableBookingServicesCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableBookingServicesCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldTableBookingServicesFormatter = Field::updateOrCreate(['tab_id'   => $tabTableBookingServices->id, 
        'code' => 'bookingservicesFormatter'],
        [
        'formatter' => 'bookingservicesFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableBookingServicesFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableBookingServicesFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

        $fieldtableNameBookingServices = Field::updateOrCreate(['tab_id'   => $tabTableBookingServices->id, 
        'code' => 'Name'],
        [
        'field' => 'name',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableNameBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableNameBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

        $fieldtableDurationBookingServices = Field::updateOrCreate(['tab_id'   => $tabTableBookingServices->id, 
        'code' => 'Duration'],
        [
        'field' => 'duration',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableDurationBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Durata', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableDurationBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '40', 
        'created_id' => $idUser]);
        
        $fieldtableSlotStepBookingServices = Field::updateOrCreate(['tab_id'   => $tabTableBookingServices->id, 
        'code' => 'Slot Step'],
        [
        'field' => 'slot_step',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableSlotStepBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Slot Step', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableSlotStepBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '50', 
        'created_id' => $idUser]);

        $fieldtableBlockFirstBookingServices = Field::updateOrCreate(['tab_id'   => $tabTableBookingServices->id, 
        'code' => 'Block First'],
        [
        'field' => 'block_first',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableBlockFirstBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Blocco Prima', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableBlockFirstBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '60', 
        'created_id' => $idUser]);

        $fieldtableBlockAfterBookingServices = Field::updateOrCreate(['tab_id'   => $tabTableBookingServices->id, 
        'code' => 'Block After'],
        [
        'field' => 'block_after',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableBlockAfterBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Blocco Dopo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableBlockAfterBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '70', 
        'created_id' => $idUser]);

        $fieldtablePriceBookingServices = Field::updateOrCreate(['tab_id'   => $tabTableBookingServices->id, 
        'code' => 'Price'],
        [
        'field' => 'price',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtablePriceBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Prezzo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtablePriceBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '80', 
        'created_id' => $idUser]);

        $fieldtableReducedPriceBookingServices = Field::updateOrCreate(['tab_id'   => $tabTableBookingServices->id, 
        'code' => 'Reduced Price'],
        [
        'field' => 'reduced_price',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableReducedPriceBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Ridotto', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableReducedPriceBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '90', 
        'created_id' => $idUser]);

        $fieldtableAdvanceBookingServices = Field::updateOrCreate(['tab_id'   => $tabTableBookingServices->id, 
        'code' => 'Advance'],
        [
        'field' => 'advance',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableAdvanceBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Anticipo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableAdvanceBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '90', 
        'created_id' => $idUser]);

        
        $tabDetailBookingServices= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'BookingServices')->first()->id, 
            'code' => 'DetailServices'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsDetailBookingServices =  Tab::where('functionality_id', Functionality::where('code', 'BookingServices')->first()->id)->where('code', 'DetailServices')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailBookingServices->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Dettaglio',
        'created_id' => $idUser]); 

        $fieldDetailNameBookingServices= Field::updateOrCreate(['tab_id' => $tabDetailBookingServices->id,
        'code' => 'name'],
        [
        'field' => 'name',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailNameBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Nome', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailNameBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> '10',
        'pos_y'=> '10',
        'colspan'=> '3',
        'created_id' => $idUser]);

        $fieldDetailDurationBookingServices= Field::updateOrCreate(['tab_id' => $tabDetailBookingServices->id,
        'code' => 'duration'],
        [
        'field' => 'duration',
        'data_type' => '0',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailDurationBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Durata', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailDurationBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '7',
        'pos_x'=> '10',
        'pos_y'=> '20',
        'colspan'=> '2',
        'created_id' => $idUser]);

        $fieldDetailDurationLabelBookingServices= Field::updateOrCreate(['tab_id' => $tabDetailBookingServices->id,
        'code' => 'durationLabel'],
        [
        'field' => 'durationLabel',
        'data_type' => '0',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailDurationLabelBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Minuti', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailDurationLabelBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '15',
        'pos_x'=> '20',
        'pos_y'=> '20',
        'colspan'=> '10',
        'class'=> 'durationLabel',
        'created_id' => $idUser]);


        $fieldDetailSlotStepBookingServices= Field::updateOrCreate(['tab_id' => $tabDetailBookingServices->id,
        'code' => 'slot_step'],
        [
        'field' => 'slot_step',
        'data_type' => '0',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailSlotStepBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Slot Step', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailSlotStepBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '7',
        'pos_x'=> '10',
        'pos_y'=> '30',
        'colspan'=> '2',
        'created_id' => $idUser]);

        $fieldDetailLabelSlotStepBookingServices= Field::updateOrCreate(['tab_id' => $tabDetailBookingServices->id,
        'code' => 'slot_steplabel'],
        [
        'field' => 'slot_steplabel',
        'data_type' => '0',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailLabelSlotStepBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Minuti (inserire 30 per accettare prenotazioni ogni 30 minuti, inserire 0 per limitarli ad ogni ora)', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailLabelSlotStepBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '15',
        'pos_x'=> '20',
        'pos_y'=> '30',
        'colspan'=> '10',
        'class'=> 'slot_steplabel',
        'created_id' => $idUser]);


        $fieldDetailBlockFirstBookingServices= Field::updateOrCreate(['tab_id' => $tabDetailBookingServices->id,
        'code' => 'block_first'],
        [
        'field' => 'block_first',
        'data_type' => '0',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailBlockFirstBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Blocco Prima', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailBlockFirstBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '7',
        'pos_x'=> '10',
        'pos_y'=> '40',
        'colspan'=> '2',
        'created_id' => $idUser]);

       $fieldDetailLabelBlockFirstBookingServices= Field::updateOrCreate(['tab_id' => $tabDetailBookingServices->id,
        'code' => 'block_firstlabel'],
        [
        'field' => 'block_firstlabel',
        'data_type' => '0',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailLabelBlockFirstBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Minuti (indicare i minuti antecedenti l\'appuntamento da considerare impegnati)', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailLabelBlockFirstBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '15',
        'pos_x'=> '20',
        'pos_y'=> '40',
        'colspan'=> '10',
        'class'=> 'block_firstlabel',
        'created_id' => $idUser]);


        $fieldDetailBlockAfterBookingServices= Field::updateOrCreate(['tab_id' => $tabDetailBookingServices->id,
        'code' => 'block_after'],
        [
        'field' => 'block_after',
        'data_type' => '0',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailBlockAfterBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Blocco Dopo', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailBlockAfterBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '7',
        'pos_x'=> '10',
        'pos_y'=> '50',
        'colspan'=> '2',
        'created_id' => $idUser]);


        $fieldDetailLabelBlockAfterBookingServices= Field::updateOrCreate(['tab_id' => $tabDetailBookingServices->id,
        'code' => 'block_afterlabel'],
        [
        'field' => 'block_afterlabel',
        'data_type' => '0',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailLabelBlockAfterBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Minuti (indicare i minuti successivi all\'appuntamento da considerare impegnati)', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailLabelBlockAfterBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '15',
        'pos_x'=> '20',
        'pos_y'=> '50',
        'colspan'=> '10',
        'class'=> 'block_afterlabel',
        'created_id' => $idUser]);


        $fieldDetailPriceBookingServices= Field::updateOrCreate(['tab_id' => $tabDetailBookingServices->id,
        'code' => 'price'],
        [
        'field' => 'price',
        'data_type' => '0',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailPriceBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Prezzo', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailPriceBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '7',
        'pos_x'=> '10',
        'pos_y'=> '60',
        'colspan'=> '1',
        'created_id' => $idUser]);

        $fieldDetailReducedPriceBookingServices= Field::updateOrCreate(['tab_id' => $tabDetailBookingServices->id,
        'code' => 'reduced_price'],
        [
        'field' => 'reduced_price',
        'data_type' => '0',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailReducedPriceBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Ridotto', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailReducedPriceBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '7',
        'pos_x'=> '20',
        'pos_y'=> '60',
        'colspan'=> '1',
        'created_id' => $idUser]);

         $fieldDetailAdvanceBookingServices= Field::updateOrCreate(['tab_id' => $tabDetailBookingServices->id,
        'code' => 'advance'],
        [
        'field' => 'advance',
        'data_type' => '0',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailAdvanceBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Anticipo', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailAdvanceBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '7',
        'pos_x'=> '30',
        'pos_y'=> '60',
        'colspan'=> '1',
        'created_id' => $idUser]);

        $fieldDetailActivePurchaseeBookingServices= Field::updateOrCreate(['tab_id' => $tabDetailBookingServices->id,
        'code' => 'active_purchase'],
        [
        'field' => 'active_purchase',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailActivePurchaseeBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Attiva Acquisto', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailActivePurchaseeBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '5',
        'pos_x'=> '10',
        'pos_y'=> '70',
        'colspan'=> '2',
        'created_id' => $idUser]);

        $fieldDetailSendConfirmationEndOnlyUponPaymentBookingServices= Field::updateOrCreate(['tab_id' => $tabDetailBookingServices->id,
        'code' => 'send_confirmation_end_only_upon_payment'],
        [
        'field' => 'send_confirmation_end_only_upon_payment',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailSendConfirmationEndOnlyUponPaymentBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Invia email di conferma solo su pagamento avvenuto', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailSendConfirmationEndOnlyUponPaymentBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '5',
        'pos_x'=> '20',
        'pos_y'=> '70',
        'colspan'=> '2',
        'created_id' => $idUser]);


        $fieldDetailtextgdprBookingServices= Field::updateOrCreate(['tab_id' => $tabDetailBookingServices->id,
        'code' => 'text_gdpr'],
        [
        'field' => 'text_gdpr',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailtextgdprBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Condizioni Aggiuntive', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailtextgdprBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '1',
        'pos_x'=> ' 10',
        'pos_y'=> '80',
        'colspan'=> '12',
        'created_id' => $idUser]);

        $fieldDetailacceptanceofmandatoryconditionsBookingServices= Field::updateOrCreate(['tab_id' => $tabDetailBookingServices->id,
        'code' => 'acceptance_of_mandatory_conditions'],
        [
        'field' => 'acceptance_of_mandatory_conditions',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailacceptanceofmandatoryconditionsBookingServices->id, 'language_id' => $idLanguage],
        ['description' => 'Accettazione condizioni obbligatorie', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailacceptanceofmandatoryconditionsBookingServices->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '5',
        'pos_x'=> ' 10',
        'pos_y'=> '90',
        'colspan'=> '2',
        'created_id' => $idUser]);

        //booking Settings 
          $tabTableBookingSettings= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'BookingSettings')->first()->id, 
            'code' => 'TableBookingSettings'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTableBookingSettings =  Tab::where('functionality_id', Functionality::where('code', 'BookingSettings')->first()->id)->where('code', 'TableBookingSettings')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableBookingSettings->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Tabella',
        'created_id' => $idUser]); 
        
        $fieldTableBookingSettingsCheckBox = Field::updateOrCreate(['tab_id'   => $tabTableBookingSettings->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableBookingSettingsCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableBookingSettingsCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldTableBookingSettingsFormatter = Field::updateOrCreate(['tab_id'   => $tabTableBookingSettings->id, 
        'code' => 'bookingsettingsFormatter'],
        [
        'formatter' => 'bookingsettingsFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableBookingSettingsFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableBookingSettingsFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

        $fieldnrappointments = Field::updateOrCreate(['tab_id'   => $tabTableBookingSettings->id, 
        'code' => 'nrappointments'],
        [
        'field' => 'nrappointments',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldnrappointments->id, 'language_id' => $idLanguage],
        ['description' => 'Numero massimo di appuntamenti', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldnrappointments->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

        $fieldemailreceivesnotifications = Field::updateOrCreate(['tab_id'   => $tabTableBookingSettings->id, 
        'code' => 'email_receives_notifications'],
        [
        'field' => 'email_receives_notifications',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldemailreceivesnotifications->id, 'language_id' => $idLanguage],
        ['description' => 'Email che riceve le notifiche', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldemailreceivesnotifications->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

        $fieldemailsendnotifications = Field::updateOrCreate(['tab_id'   => $tabTableBookingSettings->id, 
        'code' => 'email_send_notifications'],
        [
        'field' => 'email_send_notifications',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldemailsendnotifications->id, 'language_id' => $idLanguage],
        ['description' => 'Email che invia le notifiche', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldemailsendnotifications->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '50', 
        'created_id' => $idUser]);

        $fieldnotification_object_send_to_administrator = Field::updateOrCreate(['tab_id'   => $tabTableBookingSettings->id, 
        'code' => 'notification_object_send_to_administrator'],
        [
        'field' => 'notification_object_send_to_administrator',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldnotification_object_send_to_administrator->id, 'language_id' => $idLanguage],
        ['description' => 'Oggetto della notifica inviata all\'amministratore', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldnotification_object_send_to_administrator->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '60', 
        'created_id' => $idUser]);

         $fieldnotification_object_send_to_visitor = Field::updateOrCreate(['tab_id'   => $tabTableBookingSettings->id, 
        'code' => 'notification_object_send_to_visitor'],
        [
        'field' => 'notification_object_send_to_visitor',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldnotification_object_send_to_visitor->id, 'language_id' => $idLanguage],
        ['description' => 'Oggetto della notifica inviata al visitatore', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldnotification_object_send_to_visitor->id, 
        'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '70', 
        'created_id' => $idUser]);

        $fieldbooking_notification = Field::updateOrCreate(['tab_id'   => $tabTableBookingSettings->id, 
        'code' => 'DescriptionNotificationBooking'],
        [
        'field' => 'DescriptionNotificationBooking',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldbooking_notification->id, 'language_id' => $idLanguage],
        ['description' => 'Notifica Prenotazione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldbooking_notification->id, 
        'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '80', 
        'created_id' => $idUser]);

        $fieldcancellation_notification = Field::updateOrCreate(['tab_id'   => $tabTableBookingSettings->id, 
        'code' => 'CancellationNotification'],
        [
        'field' => 'CancellationNotification',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldcancellation_notification->id, 'language_id' => $idLanguage],
        ['description' => 'Notifica Annullamento', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldcancellation_notification->id, 
        'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '90', 
        'created_id' => $idUser]);

        $fieldconfirmation_notification = Field::updateOrCreate(['tab_id'   => $tabTableBookingSettings->id, 
        'code' => 'ConfirmationNotification'],
        [
        'field' => 'ConfirmationNotification',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldconfirmation_notification->id, 'language_id' => $idLanguage],
        ['description' => 'Notifica conferma', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldconfirmation_notification->id, 
        'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '100', 
        'created_id' => $idUser]);

        $fieldadmin_notification = Field::updateOrCreate(['tab_id'   => $tabTableBookingSettings->id, 
        'code' => 'AdminNotification'],
        [
        'field' => 'AdminNotification',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldadmin_notification->id, 'language_id' => $idLanguage],
        ['description' => 'Notifica Admin', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldadmin_notification->id, 
        'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '110', 
        'created_id' => $idUser]);

        $fieldtime_limit = Field::updateOrCreate(['tab_id'   => $tabTableBookingSettings->id, 
        'code' => 'time_limit'],
        [
        'field' => 'time_limit',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtime_limit->id, 'language_id' => $idLanguage],
        ['description' => 'Tempo Limite', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtime_limit->id, 
        'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '120', 
        'created_id' => $idUser]);


        $tabDetailBookingSettings= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'BookingSettings')->first()->id, 
            'code' => 'DetailBookingSettings'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsDetailBookingSettings =  Tab::where('functionality_id', Functionality::where('code', 'BookingSettings')->first()->id)->where('code', 'DetailBookingSettings')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailBookingSettings->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Dettaglio',
        'created_id' => $idUser]); 

        $fieldDetailnr_appointmentsBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailBookingSettings->id,
        'code' => 'nr_appointments'],
        [
        'field' => 'nr_appointments',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailnr_appointmentsBookingSettings->id, 'language_id' => $idLanguage],
        ['description' => 'Numero Massimo di Appuntamenti', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailnr_appointmentsBookingSettings->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> '10',
        'pos_y'=> '10',
        'colspan'=> '4',
        'created_id' => $idUser]);

        $fieldDetailsend_email_to_user_BookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailBookingSettings->id,
        'code' => 'send_email_to_user'],
        [
        'field' => 'send_email_to_user',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailsend_email_to_user_BookingSettings->id, 'language_id' => $idLanguage],
        ['description' => 'Invia Email all\'utente', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailsend_email_to_user_BookingSettings->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '5',
        'pos_x'=> '20',
        'pos_y'=> '10',
        'colspan'=> '4',
        'created_id' => $idUser]);

        $fieldDetailsend_email_to_employee_BookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailBookingSettings->id,
        'code' => 'send_email_to_employee'],
        [
        'field' => 'send_email_to_employee',
        'data_type' => '2',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailsend_email_to_employee_BookingSettings->id, 'language_id' => $idLanguage],
        ['description' => 'Invia Email all\'addetto', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailsend_email_to_employee_BookingSettings->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '5',
        'pos_x'=> '30',
        'pos_y'=> '10',
        'colspan'=> '4',
        'created_id' => $idUser]);

        $fieldDetailEmailReceivesNotificationsBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailBookingSettings->id,
        'code' => 'email_receives_notifications'],
        [
        'field' => 'email_receives_notifications',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailEmailReceivesNotificationsBookingSettings->id, 'language_id' => $idLanguage],
        ['description' => 'Email che riceve le notifiche', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailEmailReceivesNotificationsBookingSettings->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> ' 10',
        'pos_y'=> '20',
        'colspan'=> '6',
        'created_id' => $idUser]);

        $fieldDetailEmailsendnotificationsBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailBookingSettings->id,
        'code' => 'email_send_notifications'],
        [
        'field' => 'email_send_notifications',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailEmailsendnotificationsBookingSettings->id, 'language_id' => $idLanguage],
        ['description' => 'Email che invia le notifiche', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailEmailsendnotificationsBookingSettings->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> ' 10',
        'pos_y'=> '20',
        'colspan'=> '6',
        'created_id' => $idUser]);

        $fieldDetailNotificationObjectSendToAdministratorBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailBookingSettings->id,
        'code' => 'notification_object_send_to_administrator'],
        [
        'field' => 'notification_object_send_to_administrator',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailNotificationObjectSendToAdministratorBookingSettings->id, 'language_id' => $idLanguage],
        ['description' => 'Oggetto della notifica inviata all\'amministratore', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailNotificationObjectSendToAdministratorBookingSettings->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> ' 10',
        'pos_y'=> '30',
        'colspan'=> '6',
        'created_id' => $idUser]);

        $fieldDetailNotificationObjectSendToVisitorBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailBookingSettings->id,
        'code' => 'notification_object_send_to_visitor'],
        [
        'field' => 'notification_object_send_to_visitor',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailNotificationObjectSendToVisitorBookingSettings->id, 'language_id' => $idLanguage],
        ['description' => 'Oggetto della notifica inviata al visitatore', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailNotificationObjectSendToVisitorBookingSettings->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> ' 20',
        'pos_y'=> '30',
        'colspan'=> '6',
        'created_id' => $idUser]);

        $fieldDetailBookingNotificationIdBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailBookingSettings->id,
        'code' => 'booking_notification_id'],
        [
        'field' => 'booking_notification_id',
        'service' => 'messages',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailBookingNotificationIdBookingSettings->id, 'language_id' => $idLanguage],
        ['description' => 'Notifica Prenotazione', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailBookingNotificationIdBookingSettings->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '2',
        'pos_x'=> ' 10',
        'pos_y'=> '40',
        'colspan'=> '3',
        'created_id' => $idUser]);

         $fieldDetailCancellationNotificationIdBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailBookingSettings->id,
        'code' => 'cancellation_notification_id'],
        [
        'field' => 'cancellation_notification_id',
        'service' => 'messages',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCancellationNotificationIdBookingSettings->id, 'language_id' => $idLanguage],
        ['description' => 'Notifica Annullamento', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCancellationNotificationIdBookingSettings->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '2',
        'pos_x'=> ' 20',
        'pos_y'=> '40',
        'colspan'=> '3',
        'created_id' => $idUser]);

        $fieldDetailConfirmationNotificationIdBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailBookingSettings->id,
        'code' => 'confirmation_notification_id'],
        [
        'field' => 'confirmation_notification_id',
        'service' => 'messages',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailConfirmationNotificationIdBookingSettings->id, 'language_id' => $idLanguage],
        ['description' => 'Notifica Conferma', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailConfirmationNotificationIdBookingSettings->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '2',
        'pos_x'=> ' 30',
        'pos_y'=> '40',
        'colspan'=> '3',
        'created_id' => $idUser]);

        $fieldDetailAdminNotificationIdBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailBookingSettings->id,
        'code' => 'admin_notification_id'],
        [
        'field' => 'admin_notification_id',
        'service' => 'messages',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailAdminNotificationIdBookingSettings->id, 'language_id' => $idLanguage],
        ['description' => 'Notifica Conferma', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailAdminNotificationIdBookingSettings->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '2',
        'pos_x'=> ' 40',
        'pos_y'=> '40',
        'colspan'=> '3',
        'created_id' => $idUser]);

        $fieldDetailAllowCancellationBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailBookingSettings->id,
        'code' => 'allow_cancellation'],
        [
        'field' => 'allow_cancellation',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailAllowCancellationBookingSettings->id, 'language_id' => $idLanguage],
        ['description' => 'Consenti Annullamento', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailAllowCancellationBookingSettings->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '5',
        'pos_x'=> ' 10',
        'pos_y'=> '50',
        'colspan'=> '2',
        'created_id' => $idUser]);

        $fieldDetailTimeLimitBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailBookingSettings->id,
        'code' => 'time_limit'],
        [
        'field' => 'time_limit',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailTimeLimitBookingSettings->id, 'language_id' => $idLanguage],
        ['description' => 'Tempo Limite', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailTimeLimitBookingSettings->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '0',
        'pos_x'=> ' 20',
        'pos_y'=> '50',
        'colspan'=> '2',
        'created_id' => $idUser]);


        $fieldDetailLabelMinutesBeforeTheEventBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailBookingSettings->id,
        'code' => 'minutes_before_the_event'],
        [
        'field' => 'minutes_before_the_event',
        'data_type' => '0',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailLabelMinutesBeforeTheEventBookingSettings->id, 'language_id' => $idLanguage],
        ['description' => 'Minuti prima dell\'evento', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailLabelMinutesBeforeTheEventBookingSettings->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '15',
        'pos_x'=> '30',
        'pos_y'=> '50',
        'colspan'=> '8',
        'class'=> 'minutes_before_the_event',
        'created_id' => $idUser]);


        $fieldDetailtextgdprBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailBookingSettings->id,
        'code' => 'text_gdpr'],
        [
        'field' => 'text_gdpr',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailtextgdprBookingSettings->id, 'language_id' => $idLanguage],
        ['description' => 'Condizioni', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailtextgdprBookingSettings->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '1',
        'pos_x'=> ' 10',
        'pos_y'=> '60',
        'colspan'=> '12',
        'created_id' => $idUser]);

        $fieldDetailacceptanceofmandatoryconditionsBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailBookingSettings->id,
        'code' => 'acceptance_of_mandatory_conditions'],
        [
        'field' => 'acceptance_of_mandatory_conditions',
        'data_type' => '1',
        'created_id' => $idUser]);

        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailacceptanceofmandatoryconditionsBookingSettings->id, 'language_id' => $idLanguage],
        ['description' => 'Accettazione condizioni obbligatorie', 'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailacceptanceofmandatoryconditionsBookingSettings->id, 'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> '5',
        'pos_x'=> ' 10',
        'pos_y'=> '70',
        'colspan'=> '2',
        'created_id' => $idUser]);

        $tabDetailOptionCalendarWeekBookingSettings= Tab::updateOrCreate(
            [
                'functionality_id'   => Functionality::where('code', 'BookingSettings')->first()->id, 
                'code' => 'DetBookWeekOptionCalendar'
            ],
            [
                'order' => 40,
                'created_id' => $idUser
            ]);

            $tabsDetailOptionCalendarWeekBookingSettings =  Tab::where('functionality_id', Functionality::where('code', 'BookingSettings')->first()->id)->where('code', 'DetBookWeekOptionCalendar')->first();
            $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailOptionCalendarWeekBookingSettings->id],
            ['language_id'=> $idLanguage, 
            'description'=> 'Dettaglio',
            'created_id' => $idUser]); 

            $fieldDetailCheckUserWeekOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarWeekBookingSettings->id,
            'code' => 'check_user_week'],
            [
            'field' => 'check_user_week',
            'data_type' => '2',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckUserWeekOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
            ['description' =>  'Cliente', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckUserWeekOptionCalendarBookingSettings->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '5',
            'pos_x'=> ' 10',
            'pos_y'=> '10',
            'colspan'=> '2',
            'created_id' => $idUser]);


            $fieldDetailorder_user_weekOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarWeekBookingSettings->id,
            'code' => 'order_user_week'],
            [
            'field' => 'order_user_week',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_user_weekOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
            ['description' => 'Ord.to', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_user_weekOptionCalendarBookingSettings->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '0',
            'pos_x'=> ' 20',
            'pos_y'=> '10',
            'colspan'=> '3',
            'created_id' => $idUser]);

    
            $fieldDetailCheckServiceOptionWeekCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarWeekBookingSettings->id,
            'code' => 'check_service_week'],
            [
            'field' => 'check_service_week',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckServiceOptionWeekCalendarBookingSettings->id, 'language_id' => $idLanguage],
            ['description' => 'Servizio', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckServiceOptionWeekCalendarBookingSettings->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '5',
            'pos_x'=> ' 10',
            'pos_y'=> '20',
            'colspan'=> '2',
            'created_id' => $idUser]);

            $fieldDetailorder_service_weekOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarWeekBookingSettings->id,
            'code' => 'order_service_week'],
            [
            'field' => 'order_service_week',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_service_weekOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
            ['description' => 'Ord.to', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_service_weekOptionCalendarBookingSettings->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '0',
            'pos_x'=> ' 20',
            'pos_y'=> '20',
            'colspan'=> '3',
            'created_id' => $idUser]);
    
            $fieldDetailCheckEmployeeWeekOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarWeekBookingSettings->id,
            'code' => 'check_employee_week'],
            [
            'field' => 'check_employee_week',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckEmployeeWeekOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
            ['description' => 'Addetto', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckEmployeeWeekOptionCalendarBookingSettings->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '5',
            'pos_x'=> ' 10',
            'pos_y'=> '30',
            'colspan'=> '2',
            'created_id' => $idUser]);

            $fieldDetailorder_employee_weekOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarWeekBookingSettings->id,
            'code' => 'order_employee_week'],
            [
            'field' => 'order_employee_week',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_employee_weekOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
            ['description' => 'Ord.to', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_employee_weekOptionCalendarBookingSettings->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '0',
            'pos_x'=> ' 20',
            'pos_y'=> '30',
            'colspan'=> '3',
            'created_id' => $idUser]);
    
            $fieldDetailCheckPlaceOptionWeekCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarWeekBookingSettings->id,
            'code' => 'check_place_week'],
            [
            'field' => 'check_place_week',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckPlaceOptionWeekCalendarBookingSettings->id, 'language_id' => $idLanguage],
            ['description' => 'Luogo', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckPlaceOptionWeekCalendarBookingSettings->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '5',
            'pos_x'=> ' 10',
            'pos_y'=> '40',
            'colspan'=> '2',
            'created_id' => $idUser]);

            $fieldDetailorder_place_weekOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarWeekBookingSettings->id,
            'code' => 'order_place_week'],
            [
            'field' => 'order_place_week',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_place_weekOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
            ['description' => 'Ord.to', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_place_weekOptionCalendarBookingSettings->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '0',
            'pos_x'=> ' 20',
            'pos_y'=> '40',
            'colspan'=> '3',
            'created_id' => $idUser]);

    
            $fieldDetailCheckPriceOptionweekCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarWeekBookingSettings->id,
            'code' => 'check_price_week'],
            [
            'field' => 'check_price_week',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckPriceOptionweekCalendarBookingSettings->id, 'language_id' => $idLanguage],
            ['description' => 'Prezzo', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckPriceOptionweekCalendarBookingSettings->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '5',
            'pos_x'=> ' 10',
            'pos_y'=> '50',
            'colspan'=> '2',
            'created_id' => $idUser]);
    
            
            $fieldDetailorder_price_weekOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarWeekBookingSettings->id,
            'code' => 'order_price_week'],
            [
            'field' => 'order_price_week',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_price_weekOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
            ['description' => 'Ord.to', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_price_weekOptionCalendarBookingSettings->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '0',
            'pos_x'=> ' 20',
            'pos_y'=> '50',
            'colspan'=> '3',
            'created_id' => $idUser]);


            $fieldDetailCheckStatesOptionWeekCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarWeekBookingSettings->id,
            'code' => 'check_states_week'],
            [
            'field' => 'check_states_week',
            'data_type' => '1',
            'created_id' => $idUser]);


            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckStatesOptionWeekCalendarBookingSettings->id, 'language_id' => $idLanguage],
            ['description' => 'Stato', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckStatesOptionWeekCalendarBookingSettings->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '5',
            'pos_x'=> ' 10',
            'pos_y'=> '60',
            'colspan'=> '2',
            'created_id' => $idUser]);

            $fieldDetailorder_state_weekOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarWeekBookingSettings->id,
            'code' => 'order_states_week'],
            [
            'field' => 'order_states_week',
            'data_type' => '1',
            'created_id' => $idUser]);
    
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_state_weekOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
            ['description' => 'Ord.to', 'created_id' => $idUser]);
    
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_state_weekOptionCalendarBookingSettings->id, 'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> '0',
            'pos_x'=> ' 20',
            'pos_y'=> '60',
            'colspan'=> '3',
            'created_id' => $idUser]);



            $tabDetailOptionCalendarDayBookingSettings= Tab::updateOrCreate(
                [
                    'functionality_id'   => Functionality::where('code', 'BookingSettings')->first()->id, 
                    'code' => 'DetBookDayOptionCalendar'
                ],
                [
                    'order' => 50,
                    'created_id' => $idUser
                ]);
    
                $tabsDetailOptionCalendarDayBookingSettings =  Tab::where('functionality_id', Functionality::where('code', 'BookingSettings')->first()->id)->where('code', 'DetBookDayOptionCalendar')->first();
                $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailOptionCalendarDayBookingSettings->id],
                ['language_id'=> $idLanguage, 
                'description'=> 'Dettaglio',
                'created_id' => $idUser]); 
    
                $fieldDetailCheckUserDayOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarDayBookingSettings->id,
                'code' => 'check_user_day'],
                [
                'field' => 'check_user_day',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckUserDayOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Cliente', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckUserDayOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '5',
                'pos_x'=> ' 10',
                'pos_y'=> '10',
                'colspan'=> '2',
                'created_id' => $idUser]);
    
                $fieldDetailorder_user_dayOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarDayBookingSettings->id,
                'code' => 'order_user_day'],
                [
                'field' => 'order_user_day',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_user_dayOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Ord.to', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_user_dayOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '0',
                'pos_x'=> ' 20',
                'pos_y'=> '10',
                'colspan'=> '3',
                'created_id' => $idUser]);
    
        
                $fieldDetailCheckServiceOptiondayCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarDayBookingSettings->id,
                'code' => 'check_service_day'],
                [
                'field' => 'check_service_day',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckServiceOptiondayCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Servizio', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckServiceOptiondayCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '5',
                'pos_x'=> ' 10',
                'pos_y'=> '20',
                'colspan'=> '2',
                'created_id' => $idUser]);
    
                $fieldDetailorder_service_dayOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarDayBookingSettings->id,
                'code' => 'order_service_day'],
                [
                'field' => 'order_service_day',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_service_dayOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Ord.to', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_service_dayOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '0',
                'pos_x'=> ' 20',
                'pos_y'=> '20',
                'colspan'=> '3',
                'created_id' => $idUser]);
        
                $fieldDetailCheckEmployeedayOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarDayBookingSettings->id,
                'code' => 'check_employee_day'],
                [
                'field' => 'check_employee_day',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckEmployeedayOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Addetto', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckEmployeedayOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '5',
                'pos_x'=> ' 10',
                'pos_y'=> '30',
                'colspan'=> '2',
                'created_id' => $idUser]);
    
                $fieldDetailorder_employee_dayOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarDayBookingSettings->id,
                'code' => 'order_employee_day'],
                [
                'field' => 'order_employee_day',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_employee_dayOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Ord.to', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_employee_dayOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '0',
                'pos_x'=> ' 20',
                'pos_y'=> '30',
                'colspan'=> '3',
                'created_id' => $idUser]);
        
                $fieldDetailCheckPlaceOptiondayCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarDayBookingSettings->id,
                'code' => 'check_place_day'],
                [
                'field' => 'check_place_day',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckPlaceOptiondayCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Luogo', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckPlaceOptiondayCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '5',
                'pos_x'=> ' 10',
                'pos_y'=> '40',
                'colspan'=> '2',
                'created_id' => $idUser]);
    
                $fieldDetailorder_place_dayOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarDayBookingSettings->id,
                'code' => 'order_place_day'],
                [
                'field' => 'order_place_day',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_place_dayOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Ord.to', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_place_dayOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '0',
                'pos_x'=> ' 20',
                'pos_y'=> '40',
                'colspan'=> '3',
                'created_id' => $idUser]);
    
        
                $fieldDetailCheckPriceOptiondayCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarDayBookingSettings->id,
                'code' => 'check_price_day'],
                [
                'field' => 'check_price_day',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckPriceOptiondayCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Prezzo', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckPriceOptiondayCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '5',
                'pos_x'=> ' 10',
                'pos_y'=> '50',
                'colspan'=> '2',
                'created_id' => $idUser]);
        
                
                $fieldDetailorder_price_dayOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarDayBookingSettings->id,
                'code' => 'order_price_day'],
                [
                'field' => 'order_price_day',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_price_dayOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Ord.to', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_price_dayOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '0',
                'pos_x'=> ' 20',
                'pos_y'=> '50',
                'colspan'=> '3',
                'created_id' => $idUser]);
    
    
                $fieldDetailCheckStatesOptiondayCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarDayBookingSettings->id,
                'code' => 'check_states_day'],
                [
                'field' => 'check_states_day',
                'data_type' => '1',
                'created_id' => $idUser]);
    
    
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckStatesOptiondayCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Stato', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckStatesOptiondayCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '5',
                'pos_x'=> ' 10',
                'pos_y'=> '60',
                'colspan'=> '2',
                'created_id' => $idUser]);
    
                $fieldDetailorder_state_dayOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarDayBookingSettings->id,
                'code' => 'order_states_day'],
                [
                'field' => 'order_states_day',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_state_dayOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Ord.to', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_state_dayOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '0',
                'pos_x'=> ' 20',
                'pos_y'=> '60',
                'colspan'=> '3',
                'created_id' => $idUser]);

    //end booking settings

            // section booking settings calendar option 
            $tabDetailOptionCalendarBookingSettings= Tab::updateOrCreate(
                [
                    'functionality_id'   => Functionality::where('code', 'BookingSettings')->first()->id, 
                    'code' => 'DetBookSettOptionCalendar'
                ],
                [
                    'order' => 30,
                    'created_id' => $idUser
                ]);

                $tabsDetailOptionCalendarBookingSettings =  Tab::where('functionality_id', Functionality::where('code', 'BookingSettings')->first()->id)->where('code', 'DetBookSettOptionCalendar')->first();
                $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailOptionCalendarBookingSettings->id],
                ['language_id'=> $idLanguage, 
                'description'=> 'Dettaglio',
                'created_id' => $idUser]); 

                $fieldDetailCheckUserOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarBookingSettings->id,
                'code' => 'check_user'],
                [
                'field' => 'check_user',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckUserOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Cliente', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckUserOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '5',
                'pos_x'=> ' 10',
                'pos_y'=> '10',
                'colspan'=> '2',
                'created_id' => $idUser]);


                $fieldDetailorder_user_monthOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarBookingSettings->id,
                'code' => 'order_user_month'],
                [
                'field' => 'order_user_month',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_user_monthOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Ord.to', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_user_monthOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '0',
                'pos_x'=> ' 20',
                'pos_y'=> '10',
                'colspan'=> '3',
                'created_id' => $idUser]);

        
                $fieldDetailCheckServiceOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarBookingSettings->id,
                'code' => 'check_service'],
                [
                'field' => 'check_service',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckServiceOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Servizio', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckServiceOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '5',
                'pos_x'=> ' 10',
                'pos_y'=> '20',
                'colspan'=> '2',
                'created_id' => $idUser]);

                $fieldDetailorder_service_monthOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarBookingSettings->id,
                'code' => 'order_service_month'],
                [
                'field' => 'order_service_month',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_service_monthOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Ord.to', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_service_monthOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '0',
                'pos_x'=> ' 20',
                'pos_y'=> '20',
                'colspan'=> '3',
                'created_id' => $idUser]);
        
                $fieldDetailCheckEmployeeOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarBookingSettings->id,
                'code' => 'check_employee'],
                [
                'field' => 'check_employee',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckEmployeeOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Addetto', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckEmployeeOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '5',
                'pos_x'=> ' 10',
                'pos_y'=> '30',
                'colspan'=> '2',
                'created_id' => $idUser]);

                $fieldDetailorder_employee_monthOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarBookingSettings->id,
                'code' => 'order_employee_month'],
                [
                'field' => 'order_employee_month',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_employee_monthOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Ord.to', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_employee_monthOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '0',
                'pos_x'=> ' 20',
                'pos_y'=> '30',
                'colspan'=> '3',
                'created_id' => $idUser]);
        
                $fieldDetailCheckPlaceOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarBookingSettings->id,
                'code' => 'check_place'],
                [
                'field' => 'check_place',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckPlaceOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Luogo', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckPlaceOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '5',
                'pos_x'=> ' 10',
                'pos_y'=> '40',
                'colspan'=> '2',
                'created_id' => $idUser]);

                $fieldDetailorder_place_monthOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarBookingSettings->id,
                'code' => 'order_place_month'],
                [
                'field' => 'order_place_month',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_place_monthOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Ord.to', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_place_monthOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '0',
                'pos_x'=> ' 20',
                'pos_y'=> '40',
                'colspan'=> '3',
                'created_id' => $idUser]);

        
                $fieldDetailCheckPriceOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarBookingSettings->id,
                'code' => 'check_price'],
                [
                'field' => 'check_price',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckPriceOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Prezzo', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckPriceOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '5',
                'pos_x'=> ' 10',
                'pos_y'=> '50',
                'colspan'=> '2',
                'created_id' => $idUser]);
        
                $fieldDetailCheckStatesOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarBookingSettings->id,
                'code' => 'check_states'],
                [
                'field' => 'check_states',
                'data_type' => '1',
                'created_id' => $idUser]);

                $fieldDetailorder_price_monthOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarBookingSettings->id,
                'code' => 'order_price_month'],
                [
                'field' => 'order_price_month',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_price_monthOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Ord.to', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_price_monthOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '0',
                'pos_x'=> ' 20',
                'pos_y'=> '50',
                'colspan'=> '3',
                'created_id' => $idUser]);

                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailCheckStatesOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Stato', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailCheckStatesOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '5',
                'pos_x'=> ' 10',
                'pos_y'=> '60',
                'colspan'=> '2',
                'created_id' => $idUser]);

                $fieldDetailorder_state_monthOptionCalendarBookingSettings= Field::updateOrCreate(['tab_id' => $tabDetailOptionCalendarBookingSettings->id,
                'code' => 'order_states_month'],
                [
                'field' => 'order_states_month',
                'data_type' => '1',
                'created_id' => $idUser]);
        
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id' => $fieldDetailorder_state_monthOptionCalendarBookingSettings->id, 'language_id' => $idLanguage],
                ['description' => 'Ord.to', 'created_id' => $idUser]);
        
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id' => $fieldDetailorder_state_monthOptionCalendarBookingSettings->id, 'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> '0',
                'pos_x'=> ' 20',
                'pos_y'=> '60',
                'colspan'=> '3',
                'created_id' => $idUser]);
            // end section booking settings calendar option 


    /* CARRIAGE  */
         $tabTableCarriage= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Carriage')->first()->id, 
            'code' => 'TableCarriage'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTableCarriage =  Tab::where('functionality_id', Functionality::where('code', 'Carriage')->first()->id)->where('code', 'TableCarriage')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableCarriage->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Tabella',
        'created_id' => $idUser]); 
        
        $fieldTableCarriageCheckBox = Field::updateOrCreate(['tab_id'   => $tabTableCarriage->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableCarriageCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableCarriageCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldTableCarriageFormatter = Field::updateOrCreate(['tab_id'   => $tabTableCarriage->id, 
        'code' => 'CarriageFormatter'],
        [
        'formatter' => 'CarriageFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableCarriageFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableCarriageFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);

        $fieldtableCarriageCode = Field::updateOrCreate(['tab_id'   => $tabTableCarriage->id, 
        'code' => 'Code'],
        [
        'field' => 'Code',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableCarriageCode->id, 'language_id' => $idLanguage],
        ['description' => 'Codice', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableCarriageCode->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '30', 
        'created_id' => $idUser]);

        $fieldtableDescriptionCarriage = Field::updateOrCreate(['tab_id'   => $tabTableCarriage->id, 
        'code' => 'Description'],
        [
        'field' => 'Description',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableDescriptionCarriage->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableDescriptionCarriage->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '1', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

        $fieldtableOrderCarriage = Field::updateOrCreate(['tab_id'   => $tabTableCarriage->id, 
        'code' => 'Order'],
        [
        'field' => 'Order',
        'data_type' => '0',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableOrderCarriage->id, 'language_id' => $idLanguage],
        ['description' => 'Ordinamento', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableOrderCarriage->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '7', 
        'table_order'=> '50', 
        'created_id' => $idUser]);


        $fieldtableAddOnline = Field::updateOrCreate(['tab_id'   => $tabTableCarriage->id, 
        'code' => 'Online'],
        [
        'field' => 'Online',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableAddOnline->id, 'language_id' => $idLanguage],
        ['description' => 'Online', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableAddOnline->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'table_order'=> '60', 
        'created_id' => $idUser]);

          $tabDetailCarriage= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Carriage')->first()->id, 
            'code' => 'DetailCarriage'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsDetailCarriage =  Tab::where('functionality_id', Functionality::where('code', 'Carriage')->first()->id)->where('code', 'DetailCarriage')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailCarriage->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Dettaglio',
        'created_id' => $idUser]); 
        
        $fieldDetailCarriageCode = Field::updateOrCreate(['tab_id'   => $tabDetailCarriage->id, 
        'code' => 'code'],
        [
        'field' => 'code',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCarriageCode->id, 'language_id' => $idLanguage],
        ['description' => 'Codice', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCarriageCode->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10',
        'pos_y'=> '10',
        'created_id' => $idUser]);

        $fieldDetailDescriptionCarriage = Field::updateOrCreate(['tab_id'   => $tabDetailCarriage->id, 
        'code' => 'description'],
        [
        'field' => 'description',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailDescriptionCarriage->id, 'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailDescriptionCarriage->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'pos_x'=> '10',
        'pos_y'=> '20',
        'created_id' => $idUser]);

         $fieldDetailadd_transport_costOnline = Field::updateOrCreate(['tab_id'   => $tabDetailCarriage->id, 
        'code' => 'add_transport_cost'],
        [
        'field' => 'add_transport_cost',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailadd_transport_costOnline->id, 'language_id' => $idLanguage],
        ['description' => 'Aggiungi spese di trasporto', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailadd_transport_costOnline->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'pos_x'=> '10',
        'pos_y'=> '30', 
        'created_id' => $idUser]);

        $fieldDetailOrderCarriage = Field::updateOrCreate(['tab_id'   => $tabDetailCarriage->id, 
        'code' => 'order'],
        [
        'field' => 'order',
        'data_type' => '0',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailOrderCarriage->id, 'language_id' => $idLanguage],
        ['description' => 'Ordinamento', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailOrderCarriage->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '7', 
        'pos_x'=> '10',
        'pos_y'=> '40',
        'created_id' => $idUser]);

        $fieldDetailAddOnline = Field::updateOrCreate(['tab_id'   => $tabDetailCarriage->id, 
        'code' => 'online'],
        [
        'field' => 'online',
        'data_type' => '1',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailAddOnline->id, 'language_id' => $idLanguage],
        ['description' => 'Online', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailAddOnline->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5', 
        'pos_x'=> '10',
        'pos_y'=> '50', 
        'created_id' => $idUser]);


        /*POPULAR TABLE CARRIAGES AND CARRIAGES_LANGUAGES*/
        $CarriagePA = Carriage::updateOrCreate([
        'code' => 'PA'],['created_id' => $idUser]); 

        $CarriageLanguage = CarriageLanguage::updateOrCreate(['carriage_id'   => $CarriagePA->id, 
        'language_id' => $idLanguage],
        ['description' => 'Assegnato', 'created_id' => $idUser]); 

        $CarriagePF = Carriage::updateOrCreate([
        'code' => 'PF'],['created_id' => $idUser]); 

        $CarriageLanguage = CarriageLanguage::updateOrCreate(['carriage_id'   => $CarriagePF->id, 
        'language_id' => $idLanguage],
        ['description' => 'Franco', 'created_id' => $idUser]); 

        $CarriagePFA = Carriage::updateOrCreate([
        'code' => 'PFA'],['created_id' => $idUser]); 

        $CarriageLanguage = CarriageLanguage::updateOrCreate(['carriage_id'   => $CarriagePFA->id, 
        'language_id' => $idLanguage],
        ['description' => 'Franco con addebito', 'created_id' => $idUser]); 
        /*END POPULAR TABLE CARRIAGES AND CARRIAGES_LANGUAGES */

    /* SECTION TAB ADDRESS SETTING USERS */
     $tabtabletableAddress= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'UserSetting')->first()->id, 
            'code' => 'tableAddress'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabstabletableAddress =  Tab::where('functionality_id', Functionality::where('code', 'UserSetting')->first()->id)->where('code', 'tableAddress')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabtabletableAddress->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Indirizzi',
        'created_id' => $idUser]); 

        $fieldtableAddressCHECKBOX = Field::updateOrCreate(['tab_id'   => $tabtabletableAddress->id, 
        'code' => 'TableCheckBox'
        ],
        [
        'data_type' => 1,
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableAddressCHECKBOX->id, 
        'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableAddressCHECKBOX->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 13 ,
        'table_order'=> 10 ,
        'created_id' => $idUser]); 

         $fieldfieldtableAddressFormatter = Field::updateOrCreate(['tab_id'   => $tabtabletableAddress->id, 
         'code' => 'AddressFormatter'
        ],
        [
        'formatter' => 'AddressFormatter',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldfieldtableAddressFormatter->id, 
        'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]); 
     
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldfieldtableAddressFormatter->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 14 ,
        'table_order'=>  20 ,
        'created_id' => $idUser]); 

        $fieldcodeAddress = Field::updateOrCreate(['tab_id'   => $tabtabletableAddress->id, 
         'code' => 'code'
        ],
        [
        'field' => 'code',
        'data_type' => '1',
       

        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldcodeAddress->id, 
        'language_id' => $idLanguage],
        ['description' => 'Codice', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldcodeAddress->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'table_order'=>  30 ,
        'colspan'=>  4 ,
        'created_id' => $idUser]); 

        $fieldaddress= Field::updateOrCreate(['tab_id'   => $tabtabletableAddress->id, 
         'code' => 'address'
        ],
        [
        'field' => 'address',
        'data_type' => '1',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldaddress->id, 
        'language_id' => $idLanguage],
        ['description' => 'Indirizzo', 
        'created_id' => $idUser]); 
          
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldaddress->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'table_order'=>  30 ,
         'colspan'=>  4 ,
        'created_id' => $idUser]); 

        $fieldaddressDescription= Field::updateOrCreate(['tab_id'   => $tabtabletableAddress->id, 
         'code' => 'Description'
        ],
        [
        'field' => 'Description',
        'data_type' => '1',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldaddressDescription->id, 
        'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]); 
          
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldaddressDescription->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'table_order'=>  40 ,
         'colspan'=>  4 ,
        'created_id' => $idUser]); 

        $fieldtabledefaultaddress = Field::updateOrCreate(['tab_id'   => $tabtabletableAddress->id, 
        'code' => 'DefaultAddress'
        ],
        [
        'formatter' => 'DefaultAddressFormatter',
        'field' => 'DefaultAddress',
        'data_type' => 1,

        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtabledefaultaddress->id, 
        'language_id' => $idLanguage],
        ['description' => 'Default', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtabledefaultaddress->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 13 ,
        'table_order'=> 50 ,
        'created_id' => $idUser]); 

        $fieldtableonline = Field::updateOrCreate(['tab_id'   => $tabtabletableAddress->id, 
        'code' => 'Online'
        ],
        [
        'formatter' => 'OnlineFormatter',
        'field' => 'Online',
        'data_type' => 1,

        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableonline->id, 
        'language_id' => $idLanguage],
        ['description' => 'Online', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableonline->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 13 ,
        'table_order'=> 60 ,
        'created_id' => $idUser]); 



        /* DETAIL TAB ADDRESS SETTING USER */
        $tabdetailAddress= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'UserSetting')->first()->id, 
            'code' => 'detailAddress'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsdetailAddress =  Tab::where('functionality_id', Functionality::where('code', 'UserSetting')->first()->id)->where('code', 'detailAddress')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabdetailAddress->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Dettaglio',
        'created_id' => $idUser]); 


        $fieldcodedetailAddress = Field::updateOrCreate(['tab_id'   => $tabdetailAddress->id, 
         'code' => 'id_useraddress'
        ],
        [
        'field' => 'id_useraddress',
        'data_type' => '1',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldcodedetailAddress->id, 
        'language_id' => $idLanguage],
        ['description' => 'Codice', 
        'created_id' => $idUser]); 
     
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldcodedetailAddress->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=>  10 ,
        'pos_y'=>  10 ,
        'colspan'=>  4 ,
        'created_id' => $idUser]); 

        $fielddetailaddressDescription= Field::updateOrCreate(['tab_id'   => $tabdetailAddress->id, 
         'code' => 'description'
        ],
        [
        'field' => 'description',
        'data_type' => '1',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fielddetailaddressDescription->id, 
        'language_id' => $idLanguage],
        ['description' => 'Descrizione', 
        'created_id' => $idUser]); 
          
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fielddetailaddressDescription->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'input_type'=> 0 ,
        'pos_x'=>  20 ,
        'pos_y'=>  10 ,
        'colspan'=>  4 ,
        'created_id' => $idUser]); 

        $fielddetailonline = Field::updateOrCreate(['tab_id'   => $tabdetailAddress->id, 
        'code' => 'online'
        ],
        [
        'field' => 'online',
        'data_type' => 1,

        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fielddetailonline->id, 
        'language_id' => $idLanguage],
        ['description' => 'Online', 
        'created_id' => $idUser]); 
       
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fielddetailonline->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 5 ,
        'pos_x'=>  30 ,
        'pos_y'=>  10 ,
        'created_id' => $idUser]); 

        $fielddetailaddress= Field::updateOrCreate(['tab_id'   => $tabdetailAddress->id, 
         'code' => 'address'
        ],
        [
        'field' => 'address',
        'data_type' => '1',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fielddetailaddress->id, 
        'language_id' => $idLanguage],
        ['description' => 'Indirizzo', 
        'created_id' => $idUser]); 
          
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fielddetailaddress->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'pos_x'=>  10 ,
        'pos_y'=>  20 ,
        'colspan'=> 4 ,
        'created_id' => $idUser]); 


        $fielddetailaddressPostalCode= Field::updateOrCreate(['tab_id'   => $tabdetailAddress->id, 
         'code' => 'postal_code'
        ],
        [
        'field' => 'postal_code',
        'data_type' => '1',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fielddetailaddressPostalCode->id, 
        'language_id' => $idLanguage],
        ['description' => 'Codice Postale', 
        'created_id' => $idUser]); 
          
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fielddetailaddressPostalCode->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 0 ,
        'input_type'=> 0 ,
        'pos_x'=>  20 ,
        'pos_y'=>  20 ,
        'colspan'=>  2 ,
        'created_id' => $idUser]); 

    $fielddetailaddresslocality= Field::updateOrCreate(['tab_id'   => $tabdetailAddress->id, 
         'code' => 'locality'
        ],
        [
        'field' => 'locality',
        'service' => 'cities',
        'data_type' => '1',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fielddetailaddresslocality->id, 
        'language_id' => $idLanguage],
        ['description' => 'Località', 
        'created_id' => $idUser]); 
          
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fielddetailaddresslocality->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 2 ,
        'pos_x'=>  30 ,
        'pos_y'=>  20 ,
        'colspan'=>  4 ,
        'created_id' => $idUser]); 

    $fielddetailaddressprovince= Field::updateOrCreate(['tab_id'   => $tabdetailAddress->id, 
         'code' => 'province_id'
        ],
        [
        'field' => 'province_id',
        'service' => 'provinces',
        'data_type' => '1',
        'required' => true,    
        'created_id' => $idUser]);    
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fielddetailaddressprovince->id, 
        'language_id' => $idLanguage],
        ['description' => 'Provincia', 
        'created_id' => $idUser]); 
          
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fielddetailaddressprovince->id,
        'role_id' => $idRole],
        ['enabled' => 't',
        'input_type'=> 2 ,
        'pos_x'=>  40 ,
        'pos_y'=>  20 ,
        'colspan'=>  2 ,
        'created_id' => $idUser]);
         /* END SECTION TAB ADDRESS SETTING USERS*/

            /*SECTION REFERENT USER SETTING */
            $tabtableUsersReferent= Tab::updateOrCreate(
                [
                    'functionality_id'   => Functionality::where('code', 'UserSetting')->first()->id, 
                    'code' => 'UsersReferent'
                ],
                [
                    'order' => 10,
                    'created_id' => $idUser
                ]);
                $tabstableUsersReferent =  Tab::where('functionality_id', Functionality::where('code', 'UserSetting')->first()->id)->where('code', 'UsersReferent')->first();
                $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabtableUsersReferent->id],
                ['language_id'=> $idLanguage, 
                'description'=> 'Referenti',
                'created_id' => $idUser]); 
        
                 $fieldtableUsersReferentCHECKBOX = Field::updateOrCreate(['tab_id'   => $tabtableUsersReferent->id, 
                'code' => 'TableCheckBox'
                ],
                [
                'data_type' => 1,
                'required' => true,    
                'created_id' => $idUser]);    
                
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableUsersReferentCHECKBOX->id, 
                'language_id' => $idLanguage],
                ['description' => 'CheckBox', 
                'created_id' => $idUser]); 
               
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableUsersReferentCHECKBOX->id,
                'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> 13 ,
                'table_order'=> 10 ,
                'created_id' => $idUser]); 
        
                 $fieldfieldtableUsersReferentFormatter = Field::updateOrCreate(['tab_id'   => $tabtableUsersReferent->id, 
                 'code' => 'UserSettingReferentFormatter'
                ],
                [
                'formatter' => 'UserSettingReferentFormatter',
                'required' => true,    
                'created_id' => $idUser]);    
                
                  $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldfieldtableUsersReferentFormatter->id, 
                'language_id' => $idLanguage],
                ['description' => '', 
                'created_id' => $idUser]); 
             
               
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldfieldtableUsersReferentFormatter->id,
                'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> 14 ,
                'table_order'=>  20 ,
                'created_id' => $idUser]); 
        
                $fieldfieldtableUsersReferentName = Field::updateOrCreate(['tab_id'   => $tabtableUsersReferent->id, 
                 'code' => 'name'
                ],
                [
                'field' => 'names',
                'data_type' => '1',
                'on_change' => 'userHelpers.checkExistUsername',
                'required' => true,    
                'created_id' => $idUser]);    
                
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldfieldtableUsersReferentName->id, 
                'language_id' => $idLanguage],
                ['description' => 'Nome', 
                'created_id' => $idUser]); 
             
               
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldfieldtableUsersReferentName->id,
                'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> 0 ,
                'table_order'=>  30 ,
                 'colspan'=>  4 ,
                'created_id' => $idUser]); 
        
                 $fieldfieldtableUsersReferentsurname = Field::updateOrCreate(['tab_id'   => $tabtableUsersReferent->id, 
                 'code' => 'surname'
                ],
                [
                'field' => 'surnames',
                'data_type' => '1',
                'required' => true,    
                'created_id' => $idUser]);    
                
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldfieldtableUsersReferentsurname->id, 
                'language_id' => $idLanguage],
                ['description' => 'Cognome', 
                'created_id' => $idUser]); 
             
               
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldfieldtableUsersReferentsurname->id,
                'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> 0 ,
                'table_order'=>  40 ,
                'created_id' => $idUser]);

                $fieldfieldtableUsersReferentemail = Field::updateOrCreate(['tab_id'   => $tabtableUsersReferent->id, 
                'code' => 'email'
               ],
               [
               'field' => 'email',
               'data_type' => '1',
               'required' => true,    
               'created_id' => $idUser]);    
               
               $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldfieldtableUsersReferentemail->id, 
               'language_id' => $idLanguage],
               ['description' => 'Email', 
               'created_id' => $idUser]); 
            
              
               $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldfieldtableUsersReferentemail->id,
               'role_id' => $idRole],
               ['enabled' => 't',
               'input_type'=> 0 ,
               'table_order'=>  50 ,
               'created_id' => $idUser]);
                
                 $fieldfieldtableUsersReferentPhoneNumber = Field::updateOrCreate(['tab_id'   => $tabtableUsersReferent->id, 
                 'code' => 'phone_number'
                ],
                [
                'field' => 'phone_numbers',
                'data_type' => '1',
                'required' => true,    
                'created_id' => $idUser]);    
                
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldfieldtableUsersReferentPhoneNumber->id, 
                'language_id' => $idLanguage],
                ['description' => 'Telefono', 
                'created_id' => $idUser]); 
        
               
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldfieldtableUsersReferentPhoneNumber->id,
                'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> 8 ,
                'table_order'=>  60 ,
                'created_id' => $idUser]); 

                $fieldfieldtableUsersReferentUsername = Field::updateOrCreate(['tab_id'   => $tabtableUsersReferent->id, 
                'code' => 'username'
               ],
               [
               'field' => 'username',
               'data_type' => '1',
               'required' => true,    
               'created_id' => $idUser]);    
               
               $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldfieldtableUsersReferentUsername->id, 
               'language_id' => $idLanguage],
               ['description' => 'Username', 
               'created_id' => $idUser]); 
            
              
               $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldfieldtableUsersReferentUsername->id,
               'role_id' => $idRole],
               ['enabled' => 't',
               'input_type'=> 0 ,
               'table_order'=>  70 ,
               'created_id' => $idUser]);

               $fieldfieldtableDetailReferentOnlineFormatter = Field::updateOrCreate(['tab_id'   => $tabtableUsersReferent->id, 
               'code' => 'OnlineReferentFormatter'
              ],
              [
              'formatter' => 'OnlineReferentFormatter',
              'required' => true,    
              'created_id' => $idUser]);    
              
              $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldfieldtableDetailReferentOnlineFormatter->id, 
              'language_id' => $idLanguage],
              ['description' => 'Online', 
              'created_id' => $idUser]); 
        
              $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldfieldtableDetailReferentOnlineFormatter->id,
              'role_id' => $idRole],
              ['enabled' => 't',
              'input_type'=> 14 ,
              'table_order'=>  80 ,
              'created_id' => $idUser]); 
       
             
                $tabDetailReferent= Tab::updateOrCreate(
                [
                    'functionality_id'   => Functionality::where('code', 'UserSetting')->first()->id, 
                    'code' => 'DetailReferent'
                ],
                [
                    'order' => 15,
                    'created_id' => $idUser
                ]);
                $tabsDetailReferent =  Tab::where('functionality_id', Functionality::where('code', 'UserSetting')->first()->id)->where('code', 'DetailReferent')->first();
                $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailReferent->id],
                ['language_id'=> $idLanguage, 
                'description'=> 'DetailReferent',
                'created_id' => $idUser]); 
        
               $fieldDetailUsersReferentNome = Field::updateOrCreate(['tab_id'   => $tabDetailReferent->id, 
                 'code' => 'name'
                ],
                [
                'field' => 'name_referent',
                'data_type' => '1',
                'required' => true,    
                'created_id' => $idUser]);    
                
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailUsersReferentNome->id, 
                'language_id' => $idLanguage],
                ['description' => 'Nome', 
                'created_id' => $idUser]); 
             
               
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailUsersReferentNome->id,
                'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> 0 ,
                'pos_x'=>  10 ,
                'pos_y'=>  10 ,
                'created_id' => $idUser]); 
        
        
                $fieldDetailUsersReferentSurname = Field::updateOrCreate(['tab_id'   => $tabDetailReferent->id, 
                 'code' => 'surname'
                ],
                [
                'field' => 'surname_referent',
                'data_type' => '1',
                'required' => true,    
                'created_id' => $idUser]);    
                
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailUsersReferentSurname->id, 
                'language_id' => $idLanguage],
                ['description' => 'Cognome', 
                'created_id' => $idUser]); 
             
               
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailUsersReferentSurname->id,
                'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> 0 ,
                'pos_x'=>  20 ,
                'pos_y'=>  10 ,
                'created_id' => $idUser]); 
        
                $fieldDetailUsersReferentEmail = Field::updateOrCreate(['tab_id'   => $tabDetailReferent->id, 
                 'code' => 'email'
                ],
                [
                'field' => 'email_referent',
                'data_type' => '1',
                'required' => true,    
                'created_id' => $idUser]);    
                
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailUsersReferentEmail->id, 
                'language_id' => $idLanguage],
                ['description' => 'Email', 
                'created_id' => $idUser]); 
             
               
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailUsersReferentEmail->id,
                'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> 0 ,
                'pos_x'=>  30 ,
                'pos_y'=>  10 ,
                'created_id' => $idUser]); 
        
        
                $fieldDetailUsersReferentphonenumber = Field::updateOrCreate(['tab_id'   => $tabDetailReferent->id, 
                 'code' => 'phone_number'
                ],
                [
                'field' => 'phone_number_referent',
                'data_type' => '1',
                'required' => true,    
                'created_id' => $idUser]);    
                
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailUsersReferentphonenumber->id, 
                'language_id' => $idLanguage],
                ['description' => 'Telefono', 
                'created_id' => $idUser]); 
             
               
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailUsersReferentphonenumber->id,
                'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> 0 ,
                'pos_x'=>  40 ,
                'pos_y'=>  10 ,
                'created_id' => $idUser]); 

                $fielddetailreferentonline = Field::updateOrCreate(['tab_id'   => $tabDetailReferent->id, 
                'code' => 'online'
                ],
                [
                'field' => 'online',
                'data_type' => 1,
        
                'required' => true,    
                'created_id' => $idUser]);    
                
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fielddetailreferentonline->id, 
                'language_id' => $idLanguage],
                ['description' => 'Online', 
                'created_id' => $idUser]); 
               
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fielddetailreferentonline->id,
                'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> 5 ,
                'pos_x'=>  50 ,
                'pos_y'=>  10 ,
                'created_id' => $idUser]); 


                // TABLE USERS SALES
            $tabdetailSales= Tab::updateOrCreate(
            [
                'functionality_id'   => Functionality::where('code', 'UserSetting')->first()->id, 
                'code' => 'detailSalesUser'
            ],
            [
                'order' => 30,
                'created_id' => $idUser
            ]);
            $tabsdetailSales =  Tab::where('functionality_id', Functionality::where('code', 'UserSetting')->first()->id)->where('code', 'detailSalesUser')->first();
            $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabdetailSales->id],
            ['language_id'=> $idLanguage, 
            'description'=> 'Dettaglio',
            'created_id' => $idUser]); 

            $field_id_listdetailSales= Field::updateOrCreate(['tab_id'   => $tabdetailSales->id, 
            'code' => 'id_list'
            ],
            [
            'field' => 'id_list',
            'service' => 'list',
            'data_type' => '1',
            'required' => true,    
            'created_id' => $idUser]);    
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field_id_listdetailSales->id, 
            'language_id' => $idLanguage],
            ['description' => 'Listino', 
            'created_id' => $idUser]); 
            
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field_id_listdetailSales->id,
            'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> 2 ,
            'pos_x'=>  10 ,
            'pos_y'=>  10 ,
            'colspan'=> 2 ,
            'created_id' => $idUser]); 


            $field_carrier_id_Sales= Field::updateOrCreate(['tab_id'   => $tabdetailSales->id, 
            'code' => 'carrier_id'
            ],
            [
            'field' => 'carrier_id',
            'service' => 'carrierstype',
            'data_type' => '1',
            'required' => true,    
            'created_id' => $idUser]);    
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field_carrier_id_Sales->id, 
            'language_id' => $idLanguage],
            ['description' => 'Corriere', 
            'created_id' => $idUser]); 
            
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field_carrier_id_Sales->id,
            'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> 2 ,
            'pos_x'=>  20 ,
            'pos_y'=>  10 ,
            'colspan'=> 2 ,
            'created_id' => $idUser]); 

            $field_id_carriage_Sales= Field::updateOrCreate(['tab_id'   => $tabdetailSales->id, 
            'code' => 'carriage_id'
            ],
            [
            'field' => 'carriage_id',
            'service' => 'carriages',
            'data_type' => '1',
            'required' => true,    
            'created_id' => $idUser]);    
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field_id_carriage_Sales->id, 
            'language_id' => $idLanguage],
            ['description' => 'Porto', 
            'created_id' => $idUser]); 
            
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field_id_carriage_Sales->id,
            'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> 2 ,
            'pos_x'=>  30 ,
            'pos_y'=>  10 ,
            'colspan'=> 2 ,
            'created_id' => $idUser]); 


            $field_id_payments_Sales= Field::updateOrCreate(['tab_id'   => $tabdetailSales->id, 
            'code' => 'id_payment'
            ],
            [
            'field' => 'id_payment',
            'service' => 'payments',
            'data_type' => '1',
            'required' => true,    
            'created_id' => $idUser]);    
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field_id_payments_Sales->id, 
            'language_id' => $idLanguage],
            ['description' => 'Pagamento', 
            'created_id' => $idUser]); 
            
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field_id_payments_Sales->id,
            'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> 2 ,
            'pos_x'=>  40 ,
            'pos_y'=>  10 ,
            'colspan'=> 2 ,
            'created_id' => $idUser]); 


           $fieldpayment_fixedSales = Field::updateOrCreate(['tab_id'   => $tabdetailSales->id, 
            'code' => 'payment_fixed'
            ],
            [
            'field' => 'payment_fixed',
            'data_type' => 1,
    
            'required' => true,    
            'created_id' => $idUser]);    
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldpayment_fixedSales->id, 
            'language_id' => $idLanguage],
            ['description' => 'Fisso', 
            'created_id' => $idUser]); 
           
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldpayment_fixedSales->id,
            'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> 5 ,
            'pos_x'=>  50 ,
            'pos_y'=>  10 ,
            'colspan'=> 4 ,
            'created_id' => $idUser]); 

            $field_vat_type_id_Sales= Field::updateOrCreate(['tab_id'   => $tabdetailSales->id, 
             'code' => 'vat_type_id'
            ],
            [
            'field' => 'vat_type_id',
            'service' => 'vatType',
            'data_type' => '1',
            'required' => true,    
            'created_id' => $idUser]);    
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field_vat_type_id_Sales->id, 
            'language_id' => $idLanguage],
            ['description' => 'Aliquota IVA', 
            'created_id' => $idUser]); 
              
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field_vat_type_id_Sales->id,
            'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> 2 ,
            'pos_x'=>  10 ,
            'pos_y'=>  20 ,
            'colspan'=> 2 ,
            'created_id' => $idUser]); 

            $fieldVat_Exempt_fixedSales = Field::updateOrCreate(['tab_id'   => $tabdetailSales->id, 
            'code' => 'vat_exempt'
            ],
            [
            'field' => 'vat_exempt',
            'data_type' => 1,
    
            'required' => true,    
            'created_id' => $idUser]);    
            
            $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldVat_Exempt_fixedSales->id, 
            'language_id' => $idLanguage],
            ['description' => 'Esente', 
            'created_id' => $idUser]); 
           
            $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldVat_Exempt_fixedSales->id,
            'role_id' => $idRole],
            ['enabled' => 't',
            'input_type'=> 5 ,
            'pos_x'=>  20 ,
            'pos_y'=>  20 ,
            'colspan'=> 10 ,
            'created_id' => $idUser]); 
            // END TABLE USERS SALES 



            /*TABLE USERS SECTION CART RULES USERS */
            $tabtableCartRulesUsers= Tab::updateOrCreate(
                [
                    'functionality_id'   => Functionality::where('code', 'UserSetting')->first()->id, 
                    'code' => 'tableCartRulesUsers'
                ],
                [
                    'order' => 40,
                    'created_id' => $idUser
                ]);
                $tabstableCartRulesUsers =  Tab::where('functionality_id', Functionality::where('code', 'UserSetting')->first()->id)->where('code', 'tableCartRulesUsers')->first();
                $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabtableCartRulesUsers->id],
                ['language_id'=> $idLanguage, 
                'description'=> 'Regole del Carrello',
                'created_id' => $idUser]); 
        
                $fieldtableCartRulesUsersCHECKBOX = Field::updateOrCreate(['tab_id'   => $tabtableCartRulesUsers->id, 
                'code' => 'CheckBox'
                ],
                [
                'data_type' => 1,
                'required' => true,    
                'created_id' => $idUser]);    
                
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableCartRulesUsersCHECKBOX->id, 
                'language_id' => $idLanguage],
                ['description' => 'CheckBox', 
                'created_id' => $idUser]); 
               
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableCartRulesUsersCHECKBOX->id,
                'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> 13 ,
                'table_order'=> 10 ,
                'created_id' => $idUser]); 
        
                 $fieldtableCartRulesUsersFormatter = Field::updateOrCreate(['tab_id'   => $tabtableCartRulesUsers->id, 
                 'code' => 'CartRulesUsersFormatter'
                ],
                [
                'formatter' => 'CartRulesUsersFormatter',
                'required' => true,    
                'created_id' => $idUser]);    
                
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableCartRulesUsersFormatter->id, 
                'language_id' => $idLanguage],
                ['description' => '', 
                'created_id' => $idUser]); 
               
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableCartRulesUsersFormatter->id,
                'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> 14 ,
                'table_order'=>  20 ,
                'created_id' => $idUser]); 
        
                $fieldObjectCartRulesUsers = Field::updateOrCreate(['tab_id'   => $tabtableCartRulesUsers->id, 
                 'code' => 'object'
                ],
                [
                'field' => 'object',
                'data_type' => '1',
                'required' => true,    
                'created_id' => $idUser]);    
                
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldObjectCartRulesUsers->id, 
                'language_id' => $idLanguage],
                ['description' => 'Oggetto', 
                'created_id' => $idUser]); 
             
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldObjectCartRulesUsers->id,
                'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> 0 ,
                'table_order'=>  30 ,
                'created_id' => $idUser]); 
        
                $fieldConditionsCartRulesUsers = Field::updateOrCreate(['tab_id'   => $tabtableCartRulesUsers->id, 
                 'code' => 'description'
                ],
                [
                'field' => 'description',
                'data_type' => '1',
                'required' => true,    
                'created_id' => $idUser]);    
                
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldConditionsCartRulesUsers->id, 
                'language_id' => $idLanguage],
                ['description' => 'Condizione', 
                'created_id' => $idUser]); 
             
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldConditionsCartRulesUsers->id,
                'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> 0 ,
                'table_order'=>  40 ,
                'created_id' => $idUser]); 
        
                $fieldEnabled_FromCartRulesUsers = Field::updateOrCreate(['tab_id'   => $tabtableCartRulesUsers->id, 
                 'code' => 'enabled_from'
                ],
                [
                'field' => 'enabled_from',
                'data_type' => '1',
                'required' => true,    
                'created_id' => $idUser]);    
                
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldEnabled_FromCartRulesUsers->id, 
                'language_id' => $idLanguage],
                ['description' => 'Valido da', 
                'created_id' => $idUser]); 
             
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldEnabled_FromCartRulesUsers->id,
                'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> 0 ,
                'table_order'=>  50 ,
                'created_id' => $idUser]); 
        
                $fieldEnabled_toCartRulesUsers = Field::updateOrCreate(['tab_id'   => $tabtableCartRulesUsers->id, 
                 'code' => 'enabled_to'
                ],
                [
                'field' => 'enabled_to',
                'data_type' => '1',
                'required' => true,    
                'created_id' => $idUser]);    
                
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldEnabled_toCartRulesUsers->id, 
                'language_id' => $idLanguage],
                ['description' => 'Fino a', 
                'created_id' => $idUser]); 
             
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldEnabled_toCartRulesUsers->id,
                'role_id' => $idRole],
                ['enabled' => 't',
                'input_type'=> 0 ,
                'table_order'=>  60 ,
                'created_id' => $idUser]); 
            /*END TABLE USERS SECTION CART RULES USERS */      
            
            /* SECTION HISTORICAL USERS */
            $tabsTableUserHistorical = Tab::updateOrCreate(
                [
                    'functionality_id'   => Functionality::where('code', 'UserSetting')->first()->id, 
                    'code' => 'tableUserHistorical'
                ],
                [
                    'order' => 50,
                    'created_id' => $idUser
                ]);    
                $tabssTableUserHistorical = LanguageTab::updateOrCreate(['tab_id' => $tabsTableUserHistorical->id, 'language_id' => $idLanguage],['description' => 'tableUserHistorical','created_id' => $idUser]);     
        
                $field = Field::updateOrCreate(['tab_id'   => $tabsTableUserHistorical->id,'code' => 'UserHistoricalFormatter'],['created_id' => $idUser, 'formatter' => 'UserHistoricalFormatter']);                             
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '14','table_order'=>'10', 'created_id' => $idUser]); 
        
                $field = Field::updateOrCreate(['tab_id'   => $tabsTableUserHistorical->id, 'code' => 'code'],[ 'created_id' => $idUser, 'field'=> 'code','data_type' => '1']);                               
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole], ['enabled' => 't','input_type'=> '0','table_order'=>'20', 'created_id' => $idUser]); 
        
                $field = Field::updateOrCreate(['tab_id'   => $tabsTableUserHistorical->id, 'code' => 'date_order'],['created_id' => $idUser,'field'=> 'date_order', 'data_type' => '1']);                                       
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Data Ordine', 'created_id' => $idUser]); 
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'30', 'created_id' => $idUser]);   
                        
                $field = Field::updateOrCreate(['tab_id'   => $tabsTableUserHistorical->id, 'code' => 'note'],['field' => 'note','created_id' => $idUser, 'data_type' => '1']); 
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Note', 'created_id' => $idUser]); 
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'50', 'created_id' => $idUser]);    
        
                $field = Field::updateOrCreate(['tab_id'   => $tabsTableUserHistorical->id, 'code' => 'payment_type_id'],['field' => 'payment_type_id', 'created_id' => $idUser, 'data_type' => '1']); 
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Pagamento', 'created_id' => $idUser]); 
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'60', 'created_id' => $idUser]);    
        
                $field = Field::updateOrCreate(['tab_id'   => $tabsTableUserHistorical->id, 'code' => 'order_state_id'],['field' => 'order_state_id', 'created_id' => $idUser, 'data_type' => '1']); 
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Stato Ordine', 'created_id' => $idUser]); 
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'70', 'created_id' => $idUser]);    
        
                $field = Field::updateOrCreate(['tab_id'   => $tabsTableUserHistorical->id, 'code' => 'net_amount'],['field' => 'net_amount','created_id' => $idUser, 'data_type' => '1']); 
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Imponibile', 'created_id' => $idUser]); 
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'80', 'created_id' => $idUser]);    
        
                $field = Field::updateOrCreate(['tab_id'   => $tabsTableUserHistorical->id, 'code' => 'total_amount'],['field' => 'total_amount', 'created_id' => $idUser, 'data_type' => '1']); 
                $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Totale', 'created_id' => $idUser]); 
                $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'90', 'created_id' => $idUser]);    
            /*END SECTION HISTORICAL USERS */


            
        //SECTION CATEGORIES PAGES //
                  //new table item seeder 
                  $tabTableCategoriesPages= Tab::updateOrCreate(
                    [
                        'functionality_id'   => Functionality::where('code', 'CategoriesPages')->first()->id, 
                        'code' => 'Table'
                    ],
                    [
                        'order' => 10,
                        'created_id' => $idUser
                    ]);
                    $tabsTableCategoriesPages=  Tab::where('functionality_id', Functionality::where('code', 'CategoriesPages')->first()->id)->where('code', 'Table')->first();
                    $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableCategoriesPages->id],
                    ['language_id'=> $idLanguage, 
                    'description'=> 'Tabella',
                    'created_id' => $idUser]); 
            
                    $fieldCategoriesPagesCheckBox = Field::updateOrCreate(['tab_id'   => $tabTableCategoriesPages->id, 
                    'code' => 'TableCheckBox'],
                    [
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCategoriesPagesCheckBox->id, 'language_id' => $idLanguage],
                    ['description' => 'CheckBox', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCategoriesPagesCheckBox->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '13', 
                    'table_order'=> '10', 
                    'created_id' => $idUser]);
            
                    $fieldCategoriesPagesFormatter = Field::updateOrCreate(['tab_id'   => $tabTableCategoriesPages->id, 
                    'code' => 'CategoriesPagesFormatter'],
                    [
                    'formatter' => 'CategoriesPagesFormatter',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldCategoriesPagesFormatter->id, 'language_id' => $idLanguage],
                    ['description' => '', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldCategoriesPagesFormatter->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '14', 
                    'table_order'=> '20', 
                    'created_id' => $idUser]);
            
            
                    $fieldtableCategoriesPagesDescription = Field::updateOrCreate(['tab_id'   => $tabTableCategoriesPages->id, 
                    'code' => 'Description'],
                    [
                    'data_type' => '1',  
                    'field' => 'Description',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableCategoriesPagesDescription->id, 'language_id' => $idLanguage],
                    ['description' => 'Descrizione', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableCategoriesPagesDescription->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0', 
                    'table_order'=> '30', 
                    'created_id' => $idUser]);
            
                     $fieldtableCategoriesPagesLanguageId = Field::updateOrCreate(['tab_id'   => $tabTableCategoriesPages->id, 
                    'code' => 'LanguageId'],
                    [
                    'data_type' => '1',  
                    'field' => 'LanguageId',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableCategoriesPagesLanguageId->id, 'language_id' => $idLanguage],
                    ['description' => 'Lingua', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableCategoriesPagesLanguageId->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0', 
                    'table_order'=> '40', 
                    'created_id' => $idUser]);
            
                    $fieldtableCategoriesPageCategory_id = Field::updateOrCreate(['tab_id'   => $tabTableCategoriesPages->id, 
                    'code' => 'PageCategory_id'],
                    [
                    'data_type' => '1',  
                    'field' => 'PageCategory_id',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableCategoriesPageCategory_id->id, 'language_id' => $idLanguage],
                    ['description' => 'Categoria Padre', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableCategoriesPageCategory_id->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0', 
                    'table_order'=> '50', 
                    'created_id' => $idUser]);

                    $fieldtableLinkTable = Field::updateOrCreate(['tab_id'   => $tabTableCategoriesPages->id, 
                    'code' => 'LinkTable'],
                    [
                    'data_type' => '1',  
                    'field' => 'LinkTable',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableLinkTable->id, 'language_id' => $idLanguage],
                    ['description' => 'Url', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableLinkTable->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0', 
                    'table_order'=> '55', 
                    'created_id' => $idUser]);
            
                    $fieldtableCategoriesSeoTitle = Field::updateOrCreate(['tab_id'   => $tabTableCategoriesPages->id, 
                    'code' => 'SeoTitle'],
                    [
                    'data_type' => '1',  
                    'field' => 'SeoTitle',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableCategoriesSeoTitle->id, 'language_id' => $idLanguage],
                    ['description' => 'Seo Titolo', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableCategoriesSeoTitle->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0', 
                    'table_order'=> '60', 
                    'created_id' => $idUser]);
            
                    $fieldtableCategoriesSeoDescription = Field::updateOrCreate(['tab_id'   => $tabTableCategoriesPages->id, 
                    'code' => 'SeoDescription'],
                    [
                    'data_type' => '1',  
                    'field' => 'SeoDescription',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableCategoriesSeoDescription->id, 'language_id' => $idLanguage],
                    ['description' => 'Seo Descrizione', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableCategoriesSeoDescription->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0', 
                    'table_order'=> '70', 
                    'created_id' => $idUser]);
            
                    $fieldtableCategoriesSeoKeyword = Field::updateOrCreate(['tab_id'   => $tabTableCategoriesPages->id, 
                    'code' => 'SeoKeyword'],
                    [
                    'data_type' => '1',  
                    'field' => 'SeoKeyword',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableCategoriesSeoKeyword->id, 'language_id' => $idLanguage],
                    ['description' => 'Seo Keyword', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableCategoriesSeoKeyword->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0', 
                    'table_order'=> '80', 
                    'created_id' => $idUser]);
            
                    $fieldtableCategoriesShareDescription = Field::updateOrCreate(['tab_id'   => $tabTableCategoriesPages->id, 
                    'code' => 'ShareDescription'],
                    [
                    'data_type' => '1',  
                    'field' => 'ShareDescription',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableCategoriesShareDescription->id, 'language_id' => $idLanguage],
                    ['description' => 'Share Descrizione ', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableCategoriesShareDescription->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0', 
                    'table_order'=> '90', 
                    'created_id' => $idUser]);
            
                    $fieldtableCategoriesShareTitle = Field::updateOrCreate(['tab_id'   => $tabTableCategoriesPages->id, 
                    'code' => 'ShareTitle'],
                    [
                    'data_type' => '1',  
                    'field' => 'ShareTitle',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableCategoriesShareTitle->id, 'language_id' => $idLanguage],
                    ['description' => 'Share Titolo ', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableCategoriesShareTitle->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0', 
                    'table_order'=> '100', 
                    'created_id' => $idUser]);
            
                    $fieldtableCategoriesUrlcoverImage = Field::updateOrCreate(['tab_id'   => $tabTableCategoriesPages->id, 
                    'code' => 'UrlcoverImage'],
                    [
                    'data_type' => '1',  
                    'field' => 'UrlcoverImage',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableCategoriesUrlcoverImage->id, 'language_id' => $idLanguage],
                    ['description' => 'Url Cover Immagine ', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableCategoriesUrlcoverImage->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0', 
                    'table_order'=> '110', 
                    'created_id' => $idUser]);
            
                    $fieldtableCategoriesUrlOrder = Field::updateOrCreate(['tab_id'   => $tabTableCategoriesPages->id, 
                    'code' => 'Order'],
                    [
                    'data_type' => '1',  
                    'field' => 'Order',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldtableCategoriesUrlOrder->id, 'language_id' => $idLanguage],
                    ['description' => 'Ordinamento', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldtableCategoriesUrlOrder->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0', 
                    'table_order'=> '120', 
                    'created_id' => $idUser]);
            
            
                    $tabDetailCategoriesPages= Tab::updateOrCreate(
                    [
                        'functionality_id'   => Functionality::where('code', 'CategoriesPages')->first()->id, 
                        'code' => 'Detail'
                    ],
                    [
                        'order' => 20,
                        'created_id' => $idUser
                    ]);
                    $tabsDetailCategoriesPages =  Tab::where('functionality_id', Functionality::where('code', 'CategoriesPages')->first()->id)->where('code', 'Detail')->first();
                    $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailCategoriesPages->id],
                    ['language_id' => $idLanguage, 
                    'description' => 'Dettaglio',
                    'created_id' => $idUser]); 
            
            
                    $fieldDetailCategoriesPagesDescription = Field::updateOrCreate(['tab_id'   => $tabDetailCategoriesPages->id, 
                    'code' => 'description'],
                    [
                    'data_type' => '1',  
                    'field' => 'description',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoriesPagesDescription->id, 'language_id' => $idLanguage],
                    ['description' => 'Descrizione', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoriesPagesDescription->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0',  
                    'pos_x'=> '10', 
                    'pos_y'=> '10', 
                    'created_id' => $idUser]);
            
                     $fieldDetailCategoriesPagesLanguageId = Field::updateOrCreate(['tab_id'   => $tabDetailCategoriesPages->id, 
                    'code' => 'language_id'],
                    [
                    'data_type' => '1', 
                    'service' => 'languages',             
                    'field' => 'language_id',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoriesPagesLanguageId->id, 'language_id' => $idLanguage],
                    ['description' => 'Lingua', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoriesPagesLanguageId->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '2',  
                    'pos_x'=> '20', 
                    'pos_y'=> '10', 
                    'created_id' => $idUser]);
            
                     $fieldDetailCategoriesPageCategory_id = Field::updateOrCreate(['tab_id'   => $tabDetailCategoriesPages->id, 
                    'code' => 'page_category_id'],
                    [
                    'data_type' => '1',  
                    'service' => 'category_page', 
                    'field' => 'page_category_id',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoriesPageCategory_id->id, 'language_id' => $idLanguage],
                    ['description' => 'Categoria Padre', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoriesPageCategory_id->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '2',  
                    'pos_x'=> '30', 
                    'pos_y'=> '10', 
                    'created_id' => $idUser]);

                    $fieldDetailCategoriesSeoTitle = Field::updateOrCreate(['tab_id'   => $tabDetailCategoriesPages->id, 
                    'code' => 'url'],
                    [
                    'data_type' => '1',  
                    'field' => 'url',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoriesSeoTitle->id, 'language_id' => $idLanguage],
                    ['description' => 'Url', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoriesSeoTitle->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0',  
                    'pos_x'=> '10', 
                    'pos_y'=> '15', 
                    'created_id' => $idUser]);
            
                    $fieldDetailCategoriesSeoTitle = Field::updateOrCreate(['tab_id'   => $tabDetailCategoriesPages->id, 
                    'code' => 'seo_title'],
                    [
                    'data_type' => '1',  
                    'field' => 'seo_title',
                    'on_change'=> 'changeSeoTitle',
                    'on_keyup'=> 'onKeyUpTitleItems',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoriesSeoTitle->id, 'language_id' => $idLanguage],
                    ['description' => 'Seo Titolo', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoriesSeoTitle->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0',  
                    'pos_x'=> '10', 
                    'pos_y'=> '20', 
                    'created_id' => $idUser]);
            
                    $fieldDetailCategoriesSeoDescription = Field::updateOrCreate(['tab_id'   => $tabDetailCategoriesPages->id, 
                    'code' => 'seo_description'],
                    [
                    'data_type' => '1',  
                    'field' => 'seo_description',
                    'on_change'=> 'changeSeoDescription',
                    'on_keyup'=> 'onKeyUpDescriptionItems',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoriesSeoDescription->id, 'language_id' => $idLanguage],
                    ['description' => 'Seo Descrizione', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoriesSeoDescription->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0',  
                    'pos_x'=> '20', 
                    'pos_y'=> '20', 
                    'created_id' => $idUser]);
            
                    $fieldDetailCategoriesSeoKeyword = Field::updateOrCreate(['tab_id'   => $tabDetailCategoriesPages->id, 
                    'code' => 'seo_keyword'],
                    [
                    'data_type' => '1',  
                    'field' => 'seo_keyword',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoriesSeoKeyword->id, 'language_id' => $idLanguage],
                    ['description' => 'Seo Keyword', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoriesSeoKeyword->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0',  
                    'pos_x'=> '30', 
                    'pos_y'=> '20', 
                    'created_id' => $idUser]);
            
                    $fieldDetailCategoriesShareDescription = Field::updateOrCreate(['tab_id'   => $tabDetailCategoriesPages->id, 
                    'code' => 'share_description'],
                    [
                    'data_type' => '1',  
                    'field' => 'share_description',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoriesShareDescription->id, 'language_id' => $idLanguage],
                    ['description' => 'Share Descrizione ', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoriesShareDescription->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0',  
                    'pos_x'=> '10', 
                    'pos_y'=> '30', 
                    'created_id' => $idUser]);
            
                    $fieldDetailCategoriesShareTitle = Field::updateOrCreate(['tab_id'   => $tabDetailCategoriesPages->id, 
                    'code' => 'share_title'],
                    [
                    'data_type' => '1',  
                    'field' => 'share_title',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoriesShareTitle->id, 'language_id' => $idLanguage],
                    ['description' => 'Share Titolo', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoriesShareTitle->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0',  
                    'pos_x'=> '20', 
                    'pos_y'=> '30', 
                    'created_id' => $idUser]);
            
                    /*$fieldDetailCategoriesUrlcoverImage = Field::updateOrCreate(['tab_id'   => $tabDetailCategoriesPages->id, 
                    'code' => 'url_cover_image'],
                    [
                    'data_type' => '1',  
                    'field' => 'url_cover_image',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoriesUrlcoverImage->id, 'language_id' => $idLanguage],
                    ['description' => 'Url Cover Immagine ', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoriesUrlcoverImage->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                    'input_type'=> '0',  
                    'pos_x'=> '30', 
                    'pos_y'=> '30', 
                    'created_id' => $idUser]);*/
            
                    $fieldDetailCategoriesUrlOrder = Field::updateOrCreate(['tab_id'   => $tabDetailCategoriesPages->id, 
                    'code' => 'order'],
                    [
                    'data_type' => '1',  
                    'field' => 'order',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoriesUrlOrder->id, 'language_id' => $idLanguage],
                    ['description' => 'Ordinamento', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoriesUrlOrder->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                     'input_type'=> '0',  
                    'pos_x'=> '20', 
                    'pos_y'=> '15', 
                    'created_id' => $idUser]);

                    $fieldDetailCategoriesUrlOrder = Field::updateOrCreate(['tab_id'   => $tabDetailCategoriesPages->id, 
                    'code' => 'online'],
                    [
                    'data_type' => '3',  
                    'field' => 'online',
                    'created_id' => $idUser]); 
                    
                    $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailCategoriesUrlOrder->id, 'language_id' => $idLanguage],
                    ['description' => 'Online', 
                    'created_id' => $idUser]);
            
                    $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailCategoriesUrlOrder->id, 'role_id' => $idRole],
                    ['enabled' => 't', 
                     'input_type'=> '5',  
                    'pos_x'=> '40', 
                    'pos_y'=> '10', 
                    'created_id' => $idUser]);

        // END SECTION CATEGORIES PAGES //

        /* SECTION IMAGE RESIZE */
        /*$imageResize= ImageResize::firstOrCreate(['width'   => '60'],['container'   => 'default', 'online' => true]);
        $imageResize= ImageResize::firstOrCreate(['width'   => '360'],['container'   => 'default', 'online' => true]);
        $imageResize= ImageResize::firstOrCreate(['width'   => '800'],['container'   => 'default', 'online' => true]);
        $imageResize= ImageResize::firstOrCreate(['width'   => '1000'],['container'   => 'default', 'online' => true]);*/
        /* END SECTION IMAGE RESIZE */

        

        $FormBuilderFieldType= FormBuilderFieldType::firstOrCreate(['value'   => 'input'],['value'   => 'input', 'text' => 'input']);
        $FormBuilderFieldType= FormBuilderFieldType::firstOrCreate(['value'   => 'input-hidden'],['value'   => 'input-hidden', 'text' => 'input-hidden']);
        $FormBuilderFieldType= FormBuilderFieldType::firstOrCreate(['value'   => 'input-number'],['value'   => 'input-number', 'text' => 'input-number']);
        $FormBuilderFieldType= FormBuilderFieldType::firstOrCreate(['value'   => 'input-email'],['value'   => 'input-email', 'text' => 'input-email']);
        $FormBuilderFieldType= FormBuilderFieldType::firstOrCreate(['value'   => 'textarea'],['value'   => 'textarea', 'text' => 'textarea']);
        $FormBuilderFieldType= FormBuilderFieldType::firstOrCreate(['value'   => 'select'],['value'   => 'select', 'text' => 'select']);
        $FormBuilderFieldType= FormBuilderFieldType::firstOrCreate(['value'   => 'checkbox'],['value'   => 'checkbox', 'text' => 'checkbox']);
        $FormBuilderFieldType= FormBuilderFieldType::firstOrCreate(['value'   => 'radiobutton'],['value'   => 'radiobutton', 'text' => 'radiobutton']);


        /* redirect url */

        $tabTableRedirect= Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'RedirectUrlSetting')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        $tabsTableRedirect=  Tab::where('functionality_id', Functionality::where('code', 'RedirectUrlSetting')->first()->id)->where('code', 'Table')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabTableRedirect->id],
        ['language_id'=> $idLanguage, 
        'description'=> 'Tabella',
        'created_id' => $idUser]); 

        $fieldRedirectPagesCheckBox = Field::updateOrCreate(['tab_id'   => $tabTableRedirect->id, 
        'code' => 'TableCheckBox'],
        [
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldRedirectPagesCheckBox->id, 'language_id' => $idLanguage],
        ['description' => 'CheckBox', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldRedirectPagesCheckBox->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '13', 
        'table_order'=> '10', 
        'created_id' => $idUser]);

        $fieldRedirectFormatter = Field::updateOrCreate(['tab_id'   => $tabTableRedirect->id, 
        'code' => 'RedirectFormatter'],
        [
        'formatter' => 'RedirectFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldRedirectFormatter->id, 'language_id' => $idLanguage],
        ['description' => '', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldRedirectFormatter->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14', 
        'table_order'=> '20', 
        'created_id' => $idUser]);


        $fieldTableRedirectUrlOld = Field::updateOrCreate(['tab_id'   => $tabTableRedirect->id, 
        'code' => 'UrlOld'],
        [
        'data_type' => '1',  
        'field' => 'UrlOld',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableRedirectUrlOld->id, 'language_id' => $idLanguage],
        ['description' => 'Url Vecchio', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableRedirectUrlOld->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '30', 
        'created_id' => $idUser]);


        $fieldTableRedirectUrlNew= Field::updateOrCreate(['tab_id'   => $tabTableRedirect->id, 
        'code' => 'UrlNew'],
        [
        'data_type' => '1',  
        'field' => 'UrlNew',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableRedirectUrlNew->id, 'language_id' => $idLanguage],
        ['description' => 'Url Nuovo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableRedirectUrlNew->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0', 
        'table_order'=> '40', 
        'created_id' => $idUser]);

        $fieldTableActive = Field::updateOrCreate(['tab_id'   => $tabTableRedirect->id, 
        'code' => 'CheckOnlineRedirectFormatter'],
        [
        'formatter' => 'CheckOnlineRedirectFormatter',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldTableActive->id, 'language_id' => $idLanguage],
        ['description' => 'Attivo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldTableActive->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '14',  
        'table_order'=> '50', 
        'created_id' => $idUser]);

        $tabDetailRedirect = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'RedirectUrlSetting')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsDetailRedirect =  Tab::where('functionality_id', Functionality::where('code', 'RedirectUrlSetting')->first()->id)->where('code', 'Detail')->first();
        $LanguageTabs= LanguageTab::updateOrCreate(['tab_id'   => $tabDetailRedirect->id],
        ['language_id' => $idLanguage, 
        'description' => 'Dettaglio',
        'created_id' => $idUser]); 

        $fieldDetailRedirectUrl = Field::updateOrCreate(['tab_id'   => $tabDetailRedirect->id, 
        'code' => 'url_old'],
        [
        'data_type' => '1',  
        'field' => 'url_old',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailRedirectUrl->id, 'language_id' => $idLanguage],
        ['description' => 'Url Vecchio', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailRedirectUrl->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0',  
        'colspan'=>'5', 
        'pos_x'=> '10', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);


        $fieldDetailRedirectUrlNew= Field::updateOrCreate(['tab_id'   => $tabDetailRedirect->id, 
        'code' => 'url_new'],
        [
        'data_type' => '1',            
        'field' => 'url_new',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailRedirectUrlNew->id, 'language_id' => $idLanguage],
        ['description' => 'Url Nuovo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailRedirectUrlNew->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '0',  
        'colspan'=>'5', 
        'pos_x'=> '20', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);

        $fieldDetailRedirectActive= Field::updateOrCreate(['tab_id'   => $tabDetailRedirect->id, 
        'code' => 'active'],
        [
        'data_type' => '3',            
        'field' => 'active',
        'created_id' => $idUser]); 
        
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $fieldDetailRedirectActive->id, 'language_id' => $idLanguage],
        ['description' => 'Attivo', 
        'created_id' => $idUser]);

        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $fieldDetailRedirectActive->id, 'role_id' => $idRole],
        ['enabled' => 't', 
        'input_type'=> '5',  
        'colspan'=>'2', 
        'pos_x'=> '30', 
        'pos_y'=> '10', 
        'created_id' => $idUser]);
        /* end redirect url */

        



        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#menu#'],
            [
                'shortcode'=> '#menu#', 
                'description'=> 'Contenuto Header', 
                'where' => 'Contenuto Header',
                'how' => 'Scrivendo #menu# nella posizione desiderata.',
                'what' => 'Viene sostituito con l’elenco delle pagine inserite nel menu.',
                'order' => '10',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#burgermenu#'],
            [
                'shortcode'=> '#burgermenu#', 
                'description'=> 'Contenuto Header', 
                'where' => 'Contenuto Header',
                'how' => 'Scrivendo #burgermenu# nella posizione desiderata.',
                'what' => 'Viene sostituito con l’elenco delle pagine inserite nel menu con formattazione dedicata per smartphone.',
                'order' => '20',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#user#'],
            [
                'shortcode'=> '#user#', 
                'description'=> 'Contenuto Header', 
                'where' => 'Contenuto Header',
                'how' => 'Scrivendo #user# nella posizione desiderata.',
                'what' => 'Viene sostituito con il content UserShortcodeHeaderLoggedIn se l’utente e’ loggato al sito altrimenti con il content UserShortcodeHeaderLoggedOut se l’utente non e’ loggato al sito.',
                'order' => '30',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#salesman#'],
            [
                'shortcode'=> '#salesman#', 
                'description'=> 'Contenuto Header', 
                'where' => 'Contenuto Header',
                'how' => 'Scrivendo #salesman# nella posizione desiderata.',
                'what' => 'Viene sostituito con il content SalesmanShortcodeHeaderLoggedIn se l’agente e’ loggato al sito altrimenti con il content SalesmanShortcodeHeaderLoggedOut se l’agente non e’ loggato al sito.',
                'order' => '40',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#totItemCart#'],
            [
                'shortcode'=> '#totItemCart#', 
                'description'=> 'Contenuto Header', 
                'where' => 'Contenuto Header',
                'how' => 'Scrivendo #totItemCart# nella posizione desiderata.',
                'what' => 'Viene sostituito con il numero di articoli presenti nel carrello.',
                'order' => '50',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#blog#'],
            [
                'shortcode'=> '#blog#', 
                'description'=> '#blog#', 
                'where' => 'Tutte le pagine', 
                'how' => 'Scrivendo #blog# nella posizione desiderata.',
                'what' => 'Se inserito in home page viene sostituito con n° articoli di blog (il numero e’ personalizzabili nelle Configurazioni –> “HomeBlogList”)<br>
                Se inserito in qualsiasi altra pagina viene sostituito con n° articoli di blog utilizzando il paginatore (il numero e’ personalizzabili nelle Configurazioni –> "BlogList") <br>
                L’html estratto e’ generato dal rispettivo contenuto BlogListArticle<br>
                All’interno del contenuto e’ possibile utilizzare:<br>
                - Lo shortcode #publish# il quale estrae la data di pubblicazione.',
                'order' => '60',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#news#'],
            [
                'shortcode'=> '#news#', 
                'description'=> '#news#', 
                'where' => 'Tutte le pagine', 
                'how' => 'Scrivendo #news# nella posizione desiderata.',
                'what' => 'Se inserito in home page viene sostituito con n° articoli di news (il numero e’ personalizzabili nelle Configurazioni –> “HomeBlogList”)<br>
                Se inserito in qualsiasi altra pagina viene sostituito con n° articoli di news utilizzando il paginatore (il numero e’ personalizzabili nelle Configurazioni –> “NewsList”)<br>
                L’html estratto e’ generato dal rispettivo contenuto NewsListArticle<br>
                All’interno del contenuto e’ possibile utilizzare:<br>
                - Lo shortcode #publish# il quale estrae la data di pubblicazione.',
                'order' => '70',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#breadcrumbs#'],
            [
                'shortcode'=> '#breadcrumbs#', 
                'description'=> '#breadcrumbs#', 
                'where' => 'Tutte le pagine', 
                'how' => 'Scrivendo #breadcrumbs# nella posizione desiderata.',
                'what' => 'Viene sostituito con l’elenco delle pagine padre di dove si trova l’url navigato all’interno della mappa del sito.<br>
                L’html estratto e’ generato in modo dinamico e non associato a nessun Contenuto in quanto e’ una semplice lista html',
                'order' => '80',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#categories#'],
            [
                'shortcode'=> '#categories#', 
                'description'=> '#categories#', 
                'where' => 'Tutte le pagine, preferibilmente all’interno del contenuto PageCategories (questo contenuto e’ l’html del dettaglio di categorie)', 
                'how' => 'Scrivendo #categories# nella posizione desiderata.',
                'what' => 'Viene sostituito con l’elenco delle categorie / sottocategorie a seconda di quale livello si sta navigando<br>
                L’html estratto e’ generato dal rispettivo contenuto Categories.',
                'order' => '90',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#itemlist#'],
            [
                'shortcode'=> '#itemlist#', 
                'description'=> '#itemlist#', 
                'where' => 'All’interno del contenuto PageCategories (questo contenuto e’ l’html del dettaglio di categorie)', 
                'how' => 'Scrivendo #itemlist# nella posizione desiderata.',
                'what' => 'Viene sostituito con l’elenco degli articoli appartenenti alla categoria navigata<br>
                L’html estratto e’ generato dal rispettivo contenuto Items<br>
                All’interno del contenuto e’ possibile utilizzare:<br>
                - Lo shortcode #producer# il quale estrae la ragione sociale del produttore legato all’articolo.<br>
                - Lo shortcode #classFavoritesItem# il quale inserisce la classe fa-heart se l’articolo e’ tra i preferiti o inserisce la classe fa-heart-o se l’articolo non e’ tra i preferiti.<br>s
                - Lo shortcode #userInfo# il quale estrae la ragione sociale dell’utente appartenente di quell’articolo ',
                'order' => '100',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#favouriteuseritems#'],
            [
                'shortcode'=> '#favouriteuseritems#', 
                'description'=> '#favouriteuseritems#', 
                'where' => 'All’interno di qualsiasi pagina protetta da Login', 
                'how' => 'Scrivendo #favouriteuseritems# nella posizione desiderata.',
                'what' => 'Viene sostituito con l’elenco degli articoli messi tra i preferiti dell’utente loggato<br>
                L’html estratto e’ generato dal rispettivo contenuto Items<br>
                All’interno del contenuto e’ possibile utilizzare:<br>
                - Lo shortcode #classFavoritesItem# il quale inserisce la classe fa-heart se l’articolo e’ tra i preferiti o inserisce la classe fa-heart-o se l’articolo non e’ tra i preferiti.',
                'order' => '110',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#categoriesfilter#'],
            [
                'shortcode'=> '#categoriesfilter#', 
                'description'=> '#categoriesfilter#', 
                'where' => 'All’interno del contenuto PageCategories (questo contenuto e’ l’html del dettaglio di categorie)', 
                'how' => 'Scrivendo #categoriesfilter# nella posizione desiderata.',
                'what' => 'Viene sostituito con l’elenco dei filtri (select) a seconda dei dati presenti all’interno delle tabelle relative alle specifiche tecniche legate agli articoli di questa categoria.<br>
                Le opzioni di ogni filtro sono estratte dinamicamente a seconda di quali articoli sono rimasti anche dopo essere filtrati (rendendo cosi i filtri “filtrabili”).',
                'order' => '120',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#cart_summary#'],
            [
                'shortcode'=> '#cart_summary#', 
                'description'=> '#cart_summary#', 
                'where' => 'Qualsiasi pagina, preferibilmente all’interno della pagina di Carrello', 
                'how' => 'Scrivendo #cart_summary# nella posizione desiderata.',
                'what' => 'Viene usato per riepilogare i totali di carrello.<br>
                L’html estratto e’ generato dal rispettivo contenuto Cart_summary.',
                'order' => '130',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#cart_detail_single_row#'],
            [
                'shortcode'=> '#cart_detail_single_row#', 
                'description'=> '#cart_detail_single_row#', 
                'where' => 'Qualsiasi pagina, preferibilmente all’interno della pagina di Carrello', 
                'how' => 'Scrivendo #cart_detail_single_row# nella posizione desiderata.',
                'what' => 'Viene usato per riepilogare ed estrarre le righe del carrello.
                L’html estratto e’ generato dal rispettivo contenuto Cart_Detail_Single_Row.<br>
                All’interno del contenuto e’ possibile utilizzare:<br>
                - Lo shortcode #cart_detail_optional# il quale estrae gli optional legati ad ogni articolo del carrello.<br>
                - Lo shortcode #support_id_description# il quale estrae il supporto desiderato per ogni articolo.<br>
                - Lo shortcode #img# per estrarre l’immagine dell’articolo.',
                'order' => '140',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#cart_user_address#'],
            [
                'shortcode'=> '#cart_user_address#', 
                'description'=> '#cart_user_address#', 
                'where' => 'Qualsiasi pagina, preferibilmente all’interno della pagina di Carrello', 
                'how' => 'Scrivendo #cart_user_address# nella posizione desiderata.',
                'what' => 'Viene usato per riepilogare ed estrarre le sedi dell’utente.<br>
                L’html estratto e’ generato dal rispettivo contenuto CartUsersAddresses.<br>
                All’interno del contenuto e’ possibile utilizzare:<br>
                - Lo shortcode #description_province# il quale estrae la descrizione della provincia.<br>
                - Lo shortcode #description_nation# il quale estrae la descrizione della nazione.',
                'order' => '150',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#group_technicals_specifications#'],
            [
                'shortcode'=> '#group_technicals_specifications#', 
                'description'=> '#group_technicals_specifications#', 
                'where' => 'All’interno del contenuto PageItems', 
                'how' => 'Scrivendo #group_technicals_specifications# nella posizione desiderata.',
                'what' => 'Viene usato per riepilogare i gruppi di specifiche tecniche dell’articolo navigato.<br>
                L’html estratto e’ generato dal rispettivo contenuto GroupTechnicalsSpecifications.<br>
                All’interno del contenuto e’ possibile utilizzare:<br>
                - Lo shortcode #description# il quale estrae la descrizione del gruppo di specifiche tecniche<br>
                - Lo shortcode #item_group_technicals_pecifications# il quale viene sostituito con il rispettivo contenuto ItemGroupTechnicalsSpecifications, All’interno di questo contenuto e’ possibile utilizzare i seguenti shortcode:<br>
                      - Lo shortcode #description# che estrae la descrizione della specifica tecnica.<br>
                      - Lo shortcode #value# che estrae il valore della specifica tecnica.',
                'order' => '160',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#item_attachment#'],
            [
                'shortcode'=> '#item_attachment#', 
                'description'=> '#item_attachment#', 
                'where' => 'All’interno del contenuto PageItems', 
                'how' => 'Scrivendo #item_attachment# nella posizione desiderata.',
                'what' => 'Viene usato per riepilogare gli allegati di ogni articolo/categoria, ovvero documenti che devono essere visualizzati o scaricati dall’utente.<br>
                L’html estratto e’ generato dal rispettivo contenuto ItemAttachmentType.<br>
                All’interno del contenuto e’ possibile utilizzare:<br>
                - Lo shortcode #description# il quale estrae la descrizione del gruppo di allegati.<br>
                - Lo shortcode #item_attachment_file# il quale viene sostituito con con il rispettivo contenuto ItemAttachmentFile. All’interno di questo contenuto e’ possibile utilizzare i seguenti shortcode:<br>
                      - Lo shortcode #description# che estrae la descrizione del file/allegato.<br>
                      - Lo shortcode #url# che estrae il link del file/allegato.<br>
                Durante l’estrazione dei file viene effettuato il controllo sull’esistenza fisica del file sul server se il link punta al server interno di selena altrimenti stampa il link così come scritto',
                'order' => '170',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#optional#'],
            [
                'shortcode'=> '#optional#', 
                'description'=> '#optional#', 
                'where' => 'All’interno del contenuto PageItems', 
                'how' => 'Scrivendo #optional# nella posizione desiderata.',
                'what' => 'Viene usato per estrarre gli optional/varianti legati all’articolo che viene navigato.',
                'order' => '180',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '#supports#'],
            [
                'shortcode'=> '#supports#', 
                'description'=> '#supports#', 
                'where' => 'All’interno del contenuto PageItems', 
                'how' => 'Scrivendo #supports# nella posizione desiderata.',
                'what' => 'Viene usato per estrarre i supporti/&materiali legati all’articolo che viene navigato.',
                'order' => '190',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '[article type=blog items=5 order=random category=canon|sony|nikon article]'],
            [
                'shortcode'=> '[article type=blog items=5 order=random category=canon|sony|nikon article]', 
                'description'=> '[article type=blog items=5 order=random category=canon|sony|nikon article]', 
                'where' => 'All’interno di ogni pagina', 
                'how' => 'Scrivendo la dicitura con inizio “[article” e fine “article]” e inserendo i parametri desiderati nella posizione html desiderata.',
                'what' => 'Estrae automaticamente articoli di tipo blog o news.<br>
                Lo shortcode puo’ essere personalizzato con i seguenti parametri:<br>
                - type: e’ l’unico parametro obbligatorio, puo’ essere compilato con blog o news o page.<br>
                - items: e’ il numero di articoli che si desidera estrarre, se viene omesso di default prende 3.<br>
                - order: e’ il tipo di ordinamento (campo data di pubblicazione). Se viene omesso prende gli articoli in ordine decrescente. Puo’ essere desc, asc, random.<br>
                - category: e il filtro per nome categoria. Se viene omesso prende tutte le categorie. Per estrarre piu’ categorie si possono dividere le descrizioni con il pipe (|)',
                'order' => '200',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '[gallery type=popular user_id=1 camera_id=1 lens_id=1 image_category_id=1 image_id_toexclude=1 limit=10 gallery]'],
            [
                'shortcode'=> '[gallery type=popular user_id=1 camera_id=1 lens_id=1 image_category_id=1 image_id_toexclude=1 limit=10 gallery]', 
                'description'=> '[gallery type=popular user_id=1 camera_id=1 lens_id=1 image_category_id=1 image_id_toexclude=1 limit=10 gallery]', 
                'where' => 'All’interno di ogni pagina', 
                'how' => 'Scrivendo la dicitura con inizio “[gallery” e fine “gallery]” e inserendo i parametri desiderati nella posizione html desiderata.',
                'what' => 'Lo shortcode estrae automaticamente una galleria di foto nella community con stile masonry.<br>
                Lo shortcode puo’ essere personalizzato con i seguenti parametri:<br>
                - type: e’ l’unico campo obbligatorio e puo’ essere compilato con:<br>
                      - popular, sono le foto piu’ popolari delle ultime 24h ordinate da un rating calcolato in base ai voti degli utenti, all’aggiunta ai preferiti e commenti degli utenti.<br>
                      - fresh, sono le ultime foto caricate ordinate in base alla data di upload decrescente<br>
                      - editorpick, sono le foto selezionate da noi ordinate in modo decrescente<br>
                      - favorite, sono le foto preferite dall’utente, questo campo necessita del parametro user_id<br>
                      - last_comment, sono gli ultimi commenti in ordine cronologico, questo campo necessita del parametro limit<br>
                - user_id: Se settato crea una gallery con le sole foto dell’utente specificato.<br>
                - camera_id: Se settato crea una gallery con le sole foto di quella fotocamera.<br>
                - lens_id: Se settato crea una gallery con le sole foto di quell’obiettivo.<br>
                - image_category_id: Se settato crea una gallery con le sole foto di quella categoria<br>
                - image_id_to_exclude: Se settato crea una gallery escludendo una determinata foto<br>
                - limit: Se settato crea una gallery con quel numero limite di foto, Di default le estrae tutte.',
                'order' => '210',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '[item type=random item_category_id=1 item_producer_id=1 limit=10 item]'],
            [
                'shortcode'=> '[item type=random item_category_id=1 item_producer_id=1 limit=10 item]', 
                'description'=> '[item type=random item_category_id=1 item_producer_id=1 limit=10 item]', 
                'where' => 'All’interno di ogni pagina', 
                'how' => 'Scrivendo la dicitura con inizio “[item” e fine “item]” e inserendo i parametri desiderati nella posizione html desiderata.',
                'what' => 'Estrae automaticamente l’elenco articoli utilizzando il content Items.<br>
                Lo shortcode puo’ essere personalizzato con i seguenti parametri:<br>
                - type: e’ l’unico campo obbligatorio e puo’ avere i seguenti valori:<br>
                      - promotion: Estrae l’elenco degli articoli in promozione, ovvero quelli contrassegnati con il campo Promozione nella tabella items<br>
                      - new: Estrae l’elenco degli articoli in promozione, ovvero quelli contrassegnati con il campo Novita’ nella tabella items<br>
                      - bestseller: Estrae l’elenco degli articoli piu’ venduti, se non sono presenti ordini vengono estratti in modo random.<br>
                      - random: Estrae l’elenco degli articoli in modo casuale.<br>
                - item_category_id: Se settato estrae l’elenco articoli appartenente a quella determinata categoria.<br>
                - item_producer_id: Se settato estrae l’elenco articoli appartenente a quel determinato produttore.<br>
                - limit: Se settato estrae l’elenco articoli con quel numero limite.',
                'order' => '220',
            ]
        );


        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '[form id=1 form]'],
            [
                'shortcode'=> '[form id=1 form]', 
                'description'=> '[form id=1 form]', 
                'where' => 'All’interno di ogni pagina', 
                'how' => 'Scrivendo la dicitura con inizio “[form” e fine “form]” e inserendo i parametri desiderati nella posizione html desiderata.',
                'what' => 'Estrae automaticamente un form di “richiesta contatto”.<br>
                Lo shortcode puo’ essere personalizzato con i seguenti parametri:<br>
                - id: Popolato con l’id del form a cui sono legati tutti i campi da estrarre.<br>
                Lo shortcode utilizza il contenuto FormBuilderFieldType, all’interno sono presenti i seguenti shortcode:<br>
                - #description#, viene sostituito con la descrizione della tipologia di campi.<br>
                - #form#, viene sostituito con l’elenco dei campi (input, select, checkbox ecc..) inseriti all’interno della sezione amministrativa.',
                'order' => '230',
            ]
        );

        $FaqAdmin= FaqAdmin::firstOrCreate(
            ['shortcode'   => '[booking id=1 language=1 booking]'],
            [
                'shortcode'=> '[booking id=1 language=1 booking]', 
                'description'=> '[booking id=1 language=1 booking]', 
                'where' => 'All’interno di ogni pagina', 
                'how' => 'Scrivendo la dicitura con inizio “[booking” e fine “booking]” e inserendo i parametri desiderati nella posizione html desiderata.',
                'what' => 'Estrae automaticamente la sezione per prenotare quanto impostato all’interno della sezione amministrativa.<br>
                Lo shortcode puo’ essere personalizzato con i seguenti parametri (esempio: Fisicaexperience):<br>
                - id: Popolato con l’id del booking a cui sono legati tutti i campi da estrarre.<br>
                - language: Id della lingua con cui deve essere visualizzato il calendario.<br>
                Lo shortcode utilizza il contenuto BookingConnectionsDate, all’interno sono presenti i seguenti shortcode:<br>
                - #nr_appointments#: sostituito con il numero di appuntamenti massimo possibile.<br>
                - #active_purchase#: sostituito con un booleano se l’acquisto e’ attivo oppure no.<br>
                - #replaceClassDispBtnSelect#: sostituito con la dicitura “block” o “none” se il calendario prevede i posti ridotti.',
                'order' => '240',
            ]
        );

    }
}


 
    
        
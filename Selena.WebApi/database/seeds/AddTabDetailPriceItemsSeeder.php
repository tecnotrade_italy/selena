<?php

use App\Dictionary;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class AddTabDetailPriceItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $IdFunctionality = Functionality::where('code', 'Items')->first()->id;

        #region TAB
        $tabContactDetail = new Tab();
        $tabContactDetail->code = 'DetailPrice';
        $tabContactDetail->functionality_id = $IdFunctionality;
        $tabContactDetail->order = 40;
        $tabContactDetail->created_id = $idUser;
        $tabContactDetail->save();

        $languageTabContactDetail = new LanguageTab();
        $languageTabContactDetail->language_id = $idLanguage;
        $languageTabContactDetail->tab_id = $tabContactDetail->id;
        $languageTabContactDetail->description = 'DetailPrice';
        $languageTabContactDetail->created_id = $idUser;
        $languageTabContactDetail->save();
        #endregion TAB

        #region Listino
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'pricelist_id';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'pricelist_id';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Listino';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 10;
        $fieldUserRoleContactDetailCompany->pos_y = 10;
        $fieldUserRoleContactDetailCompany->input_type = 2;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion Listino

        #region Listino
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'pricelist';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'pricelist';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Prezzo';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 20;
        $fieldUserRoleContactDetailCompany->pos_y = 10;
        $fieldUserRoleContactDetailCompany->input_type = 0;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion Listino
    }
}

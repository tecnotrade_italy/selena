<?php

use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class MdMicrodetectorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*$this->call([
            UserSeeder::class,
            RoleSeeder::class,
            PermissionSeeder::class,
            LanguageSeeder::class,
            DictionarySeeder::class,
            DocumentTypeSeeder::class,
            DocumentRowTypeSeeder::class,
            SettingSeeder::class,
            SettingConfigurationSeeder::class
        ]);*/

        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        #region OC LIST

        $functionalityPickingOCList = new Functionality();
        $functionalityPickingOCList->code = 'picking_shipping';
        $functionalityPickingOCList->created_id = $idUser;
        $functionalityPickingOCList->save();

        $iconFileImageO = new Icon();
        $iconFileImageO->description = 'File image O';
        $iconFileImageO->css = 'fa fa-file-image-o';
        $iconFileImageO->created_id = $idUser;
        $iconFileImageO->save();

        $menuAdminPickingOCList = new MenuAdmin();                                                  
        $menuAdminPickingOCList->code = 'picking_shipping';
        $menuAdminPickingOCList->functionality_id = $functionalityPickingOCList->id;
        $menuAdminPickingOCList->icon_id = $iconFileImageO->id;
        $menuAdminPickingOCList->url = '/admin';
        $menuAdminPickingOCList->created_id = $idUser;
        $menuAdminPickingOCList->save();

        $languageMenuAdminPickingOCList = new LanguageMenuAdmin();
        $languageMenuAdminPickingOCList->language_id = $idLanguage;
        $languageMenuAdminPickingOCList->menu_admin_id = $menuAdminPickingOCList->id;
        $languageMenuAdminPickingOCList->description = 'Elenco consegne';
        $languageMenuAdminPickingOCList->created_id = $idUser;
        $languageMenuAdminPickingOCList->save();

        $menuAdminUserRolePickingOCList = new MenuAdminUserRole();
        $menuAdminUserRolePickingOCList->menu_admin_id = $menuAdminPickingOCList->id;
        $menuAdminUserRolePickingOCList->role_id = $idRole;
        $menuAdminUserRolePickingOCList->enabled = true;
        $menuAdminUserRolePickingOCList->order = 10;
        $menuAdminUserRolePickingOCList->created_id = $idUser;
        $menuAdminUserRolePickingOCList->save();

        $tabContentPickingOCList = new Tab();
        $tabContentPickingOCList->code = 'tab_shipping';
        $tabContentPickingOCList->functionality_id = $functionalityPickingOCList->id;
        $tabContentPickingOCList->order = 10;
        $tabContentPickingOCList->created_id = $idUser;
        $tabContentPickingOCList->save();

        $languageTabContentPickingOCList = new LanguageTab();
        $languageTabContentPickingOCList->language_id = $idLanguage;
        $languageTabContentPickingOCList->tab_id = $tabContentPickingOCList->id;
        $languageTabContentPickingOCList->description = 'Lista consegne';
        $languageTabContentPickingOCList->created_id = $idUser;
        $languageTabContentPickingOCList->save();


        #region ORDER NUMBER

        $fieldOrderNumber = new Field();
        $fieldOrderNumber->code = 'order_number';
        $fieldOrderNumber->tab_id = $tabContentPickingOCList->id;
        $fieldOrderNumber->field = 'orderNumber';
        $fieldOrderNumber->data_type = 1;
        $fieldOrderNumber->created_id = $idUser;
        $fieldOrderNumber->save();

        $fieldLanguageOrderNumber = new FieldLanguage();
        $fieldLanguageOrderNumber->field_id = $fieldOrderNumber->id;
        $fieldLanguageOrderNumber->language_id = $idLanguage;
        $fieldLanguageOrderNumber->description = 'Codice consegna';
        $fieldLanguageOrderNumber->created_id = $idUser;
        $fieldLanguageOrderNumber->save();

        $fieldUserRoleOrderNumber = new FieldUserRole();
        $fieldUserRoleOrderNumber->field_id = $fieldOrderNumber->id;
        $fieldUserRoleOrderNumber->role_id = $idRole;
        $fieldUserRoleOrderNumber->enabled = true;
        $fieldUserRoleOrderNumber->input_type = 15;
        $fieldUserRoleOrderNumber->table_order = 10;
        $fieldUserRoleOrderNumber->created_id = $idUser;
        $fieldUserRoleOrderNumber->save();

        #endregion ORDER NUMBER


        #region CUSTOMER DESCRIPTION

        $fieldCustomerDescription = new Field();
        $fieldCustomerDescription->code = 'customer_description';
        $fieldCustomerDescription->tab_id = $tabContentPickingOCList->id;
        $fieldCustomerDescription->field = 'customerDescription';
        $fieldCustomerDescription->data_type = 1;
        $fieldCustomerDescription->created_id = $idUser;
        $fieldCustomerDescription->save();

        $fieldLanguageCustomerDescription = new FieldLanguage();
        $fieldLanguageCustomerDescription->field_id = $fieldCustomerDescription->id;
        $fieldLanguageCustomerDescription->language_id = $idLanguage;
        $fieldLanguageCustomerDescription->description = 'Cliente';
        $fieldLanguageCustomerDescription->created_id = $idUser;
        $fieldLanguageCustomerDescription->save();

        $fieldUserRoleCustomerDescription = new FieldUserRole();
        $fieldUserRoleCustomerDescription->field_id = $fieldCustomerDescription->id;
        $fieldUserRoleCustomerDescription->role_id = $idRole;
        $fieldUserRoleCustomerDescription->enabled = true;
        $fieldUserRoleCustomerDescription->input_type = 15;
        $fieldUserRoleCustomerDescription->table_order = 20;
        $fieldUserRoleCustomerDescription->created_id = $idUser;
        $fieldUserRoleCustomerDescription->save();

        #endregion CUSTOMER DESCRIPTION

        #region SHIPPING DATE

        $fieldShippingDate = new Field();
        $fieldShippingDate->code = 'shipping_date';
        $fieldShippingDate->tab_id = $tabContentPickingOCList->id;
        $fieldShippingDate->field = 'shippingDate';
        $fieldShippingDate->data_type = 1;
        $fieldShippingDate->created_id = $idUser;
        $fieldShippingDate->save();

        $fieldLanguageShippingDate = new FieldLanguage();
        $fieldLanguageShippingDate->field_id = $fieldShippingDate->id;
        $fieldLanguageShippingDate->language_id = $idLanguage;
        $fieldLanguageShippingDate->description = 'Data Spedizione';
        $fieldLanguageShippingDate->created_id = $idUser;
        $fieldLanguageShippingDate->save();

        $fieldUserRoleShippingDate = new FieldUserRole();
        $fieldUserRoleShippingDate->field_id = $fieldShippingDate->id;
        $fieldUserRoleShippingDate->role_id = $idRole;
        $fieldUserRoleShippingDate->enabled = true;
        $fieldUserRoleShippingDate->input_type = 15;
        $fieldUserRoleShippingDate->table_order = 20;
        $fieldUserRoleShippingDate->created_id = $idUser;
        $fieldUserRoleShippingDate->save();

        #endregion SHIPPING DATE

        

        

        $tabContentPickingOCList = new Tab();
        $tabContentPickingOCList->code = 'shipping_product_list';
        $tabContentPickingOCList->functionality_id = $functionalityPickingOCList->id;
        $tabContentPickingOCList->order = 10;
        $tabContentPickingOCList->created_id = $idUser;
        $tabContentPickingOCList->save();

        $languageTabContentPickingOCList = new LanguageTab();
        $languageTabContentPickingOCList->language_id = $idLanguage;
        $languageTabContentPickingOCList->tab_id = $tabContentPickingOCList->id;
        $languageTabContentPickingOCList->description = 'Lista Articoli';
        $languageTabContentPickingOCList->created_id = $idUser;
        $languageTabContentPickingOCList->save();

        #region ITEM DESCRIPTION

        $fieldItemDescription = new Field();
        $fieldItemDescription->code = 'item_code';
        $fieldItemDescription->tab_id = $tabContentPickingOCList->id;
        $fieldItemDescription->field = 'itemCode';
        $fieldItemDescription->formatter = 'articleStateFormatter';
        $fieldItemDescription->data_type = 1;
        $fieldItemDescription->created_id = $idUser;
        $fieldItemDescription->save();

        $fieldLanguageItemDescription = new FieldLanguage();
        $fieldLanguageItemDescription->field_id = $fieldItemDescription->id;
        $fieldLanguageItemDescription->language_id = $idLanguage;
        $fieldLanguageItemDescription->description = 'Articolo';
        $fieldLanguageItemDescription->created_id = $idUser;
        $fieldLanguageItemDescription->save();

        $fieldUserRoleItemDescription = new FieldUserRole();
        $fieldUserRoleItemDescription->field_id = $fieldItemDescription->id;
        $fieldUserRoleItemDescription->role_id = $idRole;
        $fieldUserRoleItemDescription->enabled = true;
        $fieldUserRoleItemDescription->input_type = 15;
        $fieldUserRoleItemDescription->table_order = 10;
        $fieldUserRoleItemDescription->created_id = $idUser;
        $fieldUserRoleItemDescription->save();

        #endregion ITEM DESCRIPTION

        #region QUANTITY

        $fieldQuantity = new Field();
        $fieldQuantity->code = 'quantity';
        $fieldQuantity->tab_id = $tabContentPickingOCList->id;
        $fieldQuantity->field = 'quantity';
        $fieldQuantity->formatter = 'quantityStateFormatter';
        $fieldQuantity->data_type = 1;
        $fieldQuantity->created_id = $idUser;
        $fieldQuantity->save();

        $fieldLanguageQuantity = new FieldLanguage();
        $fieldLanguageQuantity->field_id = $fieldQuantity->id;
        $fieldLanguageQuantity->language_id = $idLanguage;
        $fieldLanguageQuantity->description = 'Qtà';
        $fieldLanguageQuantity->created_id = $idUser;
        $fieldLanguageQuantity->save();

        $fieldUserRoleQuantity = new FieldUserRole();
        $fieldUserRoleQuantity->field_id = $fieldQuantity->id;
        $fieldUserRoleQuantity->role_id = $idRole;
        $fieldUserRoleQuantity->enabled = true;
        $fieldUserRoleQuantity->input_type = 15;
        $fieldUserRoleQuantity->table_order = 20;
        $fieldUserRoleQuantity->created_id = $idUser;
        $fieldUserRoleQuantity->save();

        #endregion QUANTITY

        #region CELLA

        $fieldCell = new Field();
        $fieldCell->code = 'cell';
        $fieldCell->tab_id = $tabContentPickingOCList->id;
        $fieldCell->field = 'cell';
        $fieldCell->formatter = 'cellStateFormatter';
        $fieldCell->data_type = 1;
        $fieldCell->created_id = $idUser;
        $fieldCell->save();

        $fieldLanguageCell = new FieldLanguage();
        $fieldLanguageCell->field_id = $fieldCell->id;
        $fieldLanguageCell->language_id = $idLanguage;
        $fieldLanguageCell->description = 'Cella';
        $fieldLanguageCell->created_id = $idUser;
        $fieldLanguageCell->save();

        $fieldUserRoleCell = new FieldUserRole();
        $fieldUserRoleCell->field_id = $fieldCell->id;
        $fieldUserRoleCell->role_id = $idRole;
        $fieldUserRoleCell->enabled = true;
        $fieldUserRoleCell->input_type = 15;
        $fieldUserRoleCell->table_order = 20;
        $fieldUserRoleCell->created_id = $idUser;
        $fieldUserRoleCell->save();

        #endregion CELLA

        #endregion OC LIST

        #region PICKING READ BARCODE

        $functionalityPickingReadBarcode = new Functionality();
        $functionalityPickingReadBarcode->code = 'shipping_read_barcode';
        $functionalityPickingReadBarcode->created_id = $idUser;
        $functionalityPickingReadBarcode->save();

        $iconFileBarcode = new Icon();
        $iconFileBarcode->description = 'Barcode';
        $iconFileBarcode->css = 'fa fa-barcode';
        $iconFileBarcode->created_id = $idUser;
        $iconFileBarcode->save();

        $menuAdminPickingReadBarcode = new MenuAdmin();
        $menuAdminPickingReadBarcode->code = 'shipping_read_barcode';
        $menuAdminPickingReadBarcode->functionality_id = $functionalityPickingReadBarcode->id;
        $menuAdminPickingReadBarcode->icon_id = $iconFileBarcode->id;
        $menuAdminPickingReadBarcode->url = '/admin/picking/readingshipping';
        $menuAdminPickingReadBarcode->created_id = $idUser;
        $menuAdminPickingReadBarcode->save();

        $languageMenuAdminPickingReadBarcode = new LanguageMenuAdmin();
        $languageMenuAdminPickingReadBarcode->language_id = $idLanguage;
        $languageMenuAdminPickingReadBarcode->menu_admin_id = $menuAdminPickingReadBarcode->id;
        $languageMenuAdminPickingReadBarcode->description = 'Avvia lettura consegna';
        $languageMenuAdminPickingReadBarcode->created_id = $idUser;
        $languageMenuAdminPickingReadBarcode->save();

        $menuAdminUserRolePickingReadBarcode = new MenuAdminUserRole();
        $menuAdminUserRolePickingReadBarcode->menu_admin_id = $menuAdminPickingReadBarcode->id;
        $menuAdminUserRolePickingReadBarcode->role_id = $idRole;
        $menuAdminUserRolePickingReadBarcode->enabled = true;
        $menuAdminUserRolePickingReadBarcode->order = 20;
        $menuAdminUserRolePickingReadBarcode->created_id = $idUser;
        $menuAdminUserRolePickingReadBarcode->save();

        #endregion PICKING READ BARCORDE
    }
}

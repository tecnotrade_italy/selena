<?php

use Illuminate\Database\Seeder;
use App\AdvertisingArea;
use App\User;

class AdvertisingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idUser = User::where('name', '=', 'admin')->first()->id;

        /*********************************************************************/
        /*   AREA HEADER
        /*********************************************************************/
        $area = AdvertisingArea::updateOrCreate(['code'   => 'header'],
        [ 
            'description' => 'Area Header',
            'width_pixels'     => 0,
            'height_pixels'     => 0,
            'active'     => 1,
            'created_id' => $idUser
        ]);

        /*********************************************************************/
        /*   AREA FOOTER
        /*********************************************************************/
        $area = AdvertisingArea::updateOrCreate(['code'   => 'footer'],
        [ 
            'description' => 'Area Footer',
            'width_pixels'     => 0,
            'height_pixels'     => 0,
            'active'     => 1,
            'created_id' => $idUser
        ]);

        /*********************************************************************/
        /*   AREA CONTENT TOP
        /*********************************************************************/
        $area = AdvertisingArea::updateOrCreate(['code'   => 'content_top'],
        [ 
            'description' => 'Area Content Top',
            'width_pixels'     => 0,
            'height_pixels'     => 0,
            'active'     => 1,
            'created_id' => $idUser
        ]);

        /*********************************************************************/
        /*   AREA CONTENT BOTTOM
        /*********************************************************************/
        $area = AdvertisingArea::updateOrCreate(['code'   => 'content_bottom'],
        [ 
            'description' => 'Area Content Bottom',
            'width_pixels'     => 0,
            'height_pixels'     => 0,
            'active'     => 1,
            'created_id' => $idUser
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class BormacSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            RoleSeeder::class,
            PermissionSeeder::class,
            LanguageSeeder::class,
            DictionarySeeder::class,
            SettingSeeder::class,
            ItemTechnicalSpecificationSeeder::class,
            SettingConfigurationSeeder::class
        ]);
    }
}

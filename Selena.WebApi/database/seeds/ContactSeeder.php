<?php

use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $idMenuAdminCRM = MenuAdmin::where('code', 'CRM')->first()->id;

        #region CONTACT

        #region ICON

        $iconIdCard = new Icon();
        $iconIdCard->description = 'IdCard';
        $iconIdCard->css = 'fa fa-id-card';
        $iconIdCard->created_id = $idUser;
        $iconIdCard->save();

        #endregion ICON

        #region FUNCTIONALITY

        $functionalityContact = new Functionality();
        $functionalityContact->code = 'Contact';
        $functionalityContact->created_id = $idUser;
        $functionalityContact->save();

        #endregion FUNCTIONALITY

        #region MENU

        $menuAdminContact = new MenuAdmin();
        $menuAdminContact->code = 'Contact';
        $menuAdminContact->functionality_id = $functionalityContact->id;
        $menuAdminContact->icon_id = $iconIdCard->id;
        $menuAdminContact->url = '/admin/crm/contact';
        $menuAdminContact->created_id = $idUser;
        $menuAdminContact->save();

        $languageMenuAdminContact = new LanguageMenuAdmin();
        $languageMenuAdminContact->language_id = $idLanguage;
        $languageMenuAdminContact->menu_admin_id = $menuAdminContact->id;
        $languageMenuAdminContact->description = 'Contatti';
        $languageMenuAdminContact->created_id = $idUser;
        $languageMenuAdminContact->save();

        $menuAdminUserRoleContact = new MenuAdminUserRole();
        $menuAdminUserRoleContact->menu_admin_id = $menuAdminContact->id;
        $menuAdminUserRoleContact->menu_admin_father_id = $idMenuAdminCRM;
        $menuAdminUserRoleContact->role_id = $idRole;
        $menuAdminUserRoleContact->enabled = true;
        $menuAdminUserRoleContact->order = 30;
        $menuAdminUserRoleContact->created_id = $idUser;
        $menuAdminUserRoleContact->save();

        #endregion MENU

        #region TABLE

        #region TAB

        $tabContactTable = new Tab();
        $tabContactTable->code = 'Table';
        $tabContactTable->functionality_id = $functionalityContact->id;
        $tabContactTable->order = 10;
        $tabContactTable->created_id = $idUser;
        $tabContactTable->save();

        $languageTabContactTable = new LanguageTab();
        $languageTabContactTable->language_id = $idLanguage;
        $languageTabContactTable->tab_id = $tabContactTable->id;
        $languageTabContactTable->description = 'Table';
        $languageTabContactTable->created_id = $idUser;
        $languageTabContactTable->save();

        #endregion TAB

        #region CHECKBOX

        $fieldContactCheckBox = new Field();
        $fieldContactCheckBox->tab_id = $tabContactTable->id;
        $fieldContactCheckBox->code = 'TableCheckBox';
        $fieldContactCheckBox->created_id = $idUser;
        $fieldContactCheckBox->save();

        $fieldLanguageContactCheckBox = new FieldLanguage();
        $fieldLanguageContactCheckBox->field_id = $fieldContactCheckBox->id;
        $fieldLanguageContactCheckBox->language_id = $idLanguage;
        $fieldLanguageContactCheckBox->description = 'CheckBox';
        $fieldLanguageContactCheckBox->created_id = $idUser;
        $fieldLanguageContactCheckBox->save();

        $fieldUserRoleContactCheckBox = new FieldUserRole();
        $fieldUserRoleContactCheckBox->field_id = $fieldContactCheckBox->id;
        $fieldUserRoleContactCheckBox->role_id = $idRole;
        $fieldUserRoleContactCheckBox->enabled = true;
        $fieldUserRoleContactCheckBox->table_order = 10;
        $fieldUserRoleContactCheckBox->input_type = 13;
        $fieldUserRoleContactCheckBox->created_id = $idUser;
        $fieldUserRoleContactCheckBox->save();

        #endregion CHECKBOX

        #region FORMATTER

        $fieldContactFormatter = new Field();
        $fieldContactFormatter->code = 'ContactFormatter';
        $fieldContactFormatter->formatter = 'ContactFormatter';
        $fieldContactFormatter->tab_id = $tabContactTable->id;
        $fieldContactFormatter->created_id = $idUser;
        $fieldContactFormatter->save();

        $fieldLanguageContactFormatter = new FieldLanguage();
        $fieldLanguageContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldLanguageContactFormatter->language_id = $idLanguage;
        $fieldLanguageContactFormatter->description = '';
        $fieldLanguageContactFormatter->created_id = $idUser;
        $fieldLanguageContactFormatter->save();

        $fieldUserRoleContactFormatter = new FieldUserRole();
        $fieldUserRoleContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldUserRoleContactFormatter->role_id = $idRole;
        $fieldUserRoleContactFormatter->enabled = true;
        $fieldUserRoleContactFormatter->table_order = 20;
        $fieldUserRoleContactFormatter->input_type = 14;
        $fieldUserRoleContactFormatter->created_id = $idUser;
        $fieldUserRoleContactFormatter->save();

        #endregion FORMATTER

        #region NAME

        $fieldContactTableCompany = new Field();
        $fieldContactTableCompany->code = 'Name';
        $fieldContactTableCompany->tab_id = $tabContactTable->id;
        $fieldContactTableCompany->field = 'name';
        $fieldContactTableCompany->data_type = 1;
        $fieldContactTableCompany->created_id = $idUser;
        $fieldContactTableCompany->save();

        $fieldLanguageContactTableCompany = new FieldLanguage();
        $fieldLanguageContactTableCompany->field_id = $fieldContactTableCompany->id;
        $fieldLanguageContactTableCompany->language_id = $idLanguage;
        $fieldLanguageContactTableCompany->description = 'Nome';
        $fieldLanguageContactTableCompany->created_id = $idUser;
        $fieldLanguageContactTableCompany->save();

        $fieldUserRoleContactTableCompany = new FieldUserRole();
        $fieldUserRoleContactTableCompany->field_id = $fieldContactTableCompany->id;
        $fieldUserRoleContactTableCompany->role_id = $idRole;
        $fieldUserRoleContactTableCompany->enabled = true;
        $fieldUserRoleContactTableCompany->table_order = 30;
        $fieldUserRoleContactTableCompany->input_type = 0;
        $fieldUserRoleContactTableCompany->created_id = $idUser;
        $fieldUserRoleContactTableCompany->save();

        #endregion NAME

        #region SURNAME

        $fieldContactTableBusinessName = new Field();
        $fieldContactTableBusinessName->code = 'Surname';
        $fieldContactTableBusinessName->tab_id = $tabContactTable->id;
        $fieldContactTableBusinessName->field = 'surname';
        $fieldContactTableBusinessName->data_type = 1;
        $fieldContactTableBusinessName->created_id = $idUser;
        $fieldContactTableBusinessName->save();

        $fieldLanguageContactTableBusinessName = new FieldLanguage();
        $fieldLanguageContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldLanguageContactTableBusinessName->language_id = $idLanguage;
        $fieldLanguageContactTableBusinessName->description = 'Cognome';
        $fieldLanguageContactTableBusinessName->created_id = $idUser;
        $fieldLanguageContactTableBusinessName->save();

        $fieldUserRoleContactTableBusinessName = new FieldUserRole();
        $fieldUserRoleContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldUserRoleContactTableBusinessName->role_id = $idRole;
        $fieldUserRoleContactTableBusinessName->enabled = true;
        $fieldUserRoleContactTableBusinessName->table_order = 40;
        $fieldUserRoleContactTableBusinessName->input_type = 0;
        $fieldUserRoleContactTableBusinessName->created_id = $idUser;
        $fieldUserRoleContactTableBusinessName->save();

        #endregion SURNAME

        #region BUSINESS NAME

        $fieldContactTableName = new Field();
        $fieldContactTableName->code = 'BusinessName';
        $fieldContactTableName->tab_id = $tabContactTable->id;
        $fieldContactTableName->field = 'businessName';
        $fieldContactTableName->data_type = 1;
        $fieldContactTableName->created_id = $idUser;
        $fieldContactTableName->save();

        $fieldLanguageContactTableName = new FieldLanguage();
        $fieldLanguageContactTableName->field_id = $fieldContactTableName->id;
        $fieldLanguageContactTableName->language_id = $idLanguage;
        $fieldLanguageContactTableName->description = 'Ragione Sociale';
        $fieldLanguageContactTableName->created_id = $idUser;
        $fieldLanguageContactTableName->save();

        $fieldUserRoleContactTableName = new FieldUserRole();
        $fieldUserRoleContactTableName->field_id = $fieldContactTableName->id;
        $fieldUserRoleContactTableName->role_id = $idRole;
        $fieldUserRoleContactTableName->enabled = true;
        $fieldUserRoleContactTableName->table_order = 50;
        $fieldUserRoleContactTableName->input_type = 0;
        $fieldUserRoleContactTableName->created_id = $idUser;
        $fieldUserRoleContactTableName->save();

        #endregion BUSINESS NAME

        #region EMAIL

        $fieldContactTableSurname = new Field();
        $fieldContactTableSurname->code = 'Email';
        $fieldContactTableSurname->tab_id = $tabContactTable->id;
        $fieldContactTableSurname->field = 'email';
        $fieldContactTableSurname->data_type = 1;
        $fieldContactTableSurname->created_id = $idUser;
        $fieldContactTableSurname->save();

        $fieldLanguageContactTableSurname = new FieldLanguage();
        $fieldLanguageContactTableSurname->field_id = $fieldContactTableSurname->id;
        $fieldLanguageContactTableSurname->language_id = $idLanguage;
        $fieldLanguageContactTableSurname->description = 'Email';
        $fieldLanguageContactTableSurname->created_id = $idUser;
        $fieldLanguageContactTableSurname->save();

        $fieldUserRoleContactTableSurname = new FieldUserRole();
        $fieldUserRoleContactTableSurname->field_id = $fieldContactTableSurname->id;
        $fieldUserRoleContactTableSurname->role_id = $idRole;
        $fieldUserRoleContactTableSurname->enabled = true;
        $fieldUserRoleContactTableSurname->table_order = 60;
        $fieldUserRoleContactTableSurname->input_type = 0;
        $fieldUserRoleContactTableSurname->created_id = $idUser;
        $fieldUserRoleContactTableSurname->save();

        #endregion EMAIL

        #region PHONE NUMBER

        $fieldContactTableVatNumber = new Field();
        $fieldContactTableVatNumber->code = 'PhoneNumber';
        $fieldContactTableVatNumber->tab_id = $tabContactTable->id;
        $fieldContactTableVatNumber->field = 'phoneNumber';
        $fieldContactTableVatNumber->data_type = 1;
        $fieldContactTableVatNumber->created_id = $idUser;
        $fieldContactTableVatNumber->save();

        $fieldLanguageContactTableVatNumber = new FieldLanguage();
        $fieldLanguageContactTableVatNumber->field_id = $fieldContactTableVatNumber->id;
        $fieldLanguageContactTableVatNumber->language_id = $idLanguage;
        $fieldLanguageContactTableVatNumber->description = 'Telefono';
        $fieldLanguageContactTableVatNumber->created_id = $idUser;
        $fieldLanguageContactTableVatNumber->save();

        $fieldUserRoleContactTableVatNumber = new FieldUserRole();
        $fieldUserRoleContactTableVatNumber->field_id = $fieldContactTableVatNumber->id;
        $fieldUserRoleContactTableVatNumber->role_id = $idRole;
        $fieldUserRoleContactTableVatNumber->enabled = true;
        $fieldUserRoleContactTableVatNumber->table_order = 70;
        $fieldUserRoleContactTableVatNumber->input_type = 0;
        $fieldUserRoleContactTableVatNumber->created_id = $idUser;
        $fieldUserRoleContactTableVatNumber->save();

        #endregion PHONE NUMBER

        #region SEND NEWSLETTER

        $fieldContactTableSendNewsletter = new Field();
        $fieldContactTableSendNewsletter->code = 'SendNewsletter';
        $fieldContactTableSendNewsletter->tab_id = $tabContactTable->id;
        $fieldContactTableSendNewsletter->field = 'sendNewsletter';
        $fieldContactTableSendNewsletter->data_type = 3;
        $fieldContactTableSendNewsletter->created_id = $idUser;
        $fieldContactTableSendNewsletter->save();

        $fieldLanguageContactTableSendNewsletter = new FieldLanguage();
        $fieldLanguageContactTableSendNewsletter->field_id = $fieldContactTableSendNewsletter->id;
        $fieldLanguageContactTableSendNewsletter->language_id = $idLanguage;
        $fieldLanguageContactTableSendNewsletter->description = 'Invia newsletter';
        $fieldLanguageContactTableSendNewsletter->created_id = $idUser;
        $fieldLanguageContactTableSendNewsletter->save();

        $fieldUserRoleContactTableSendNewsletter = new FieldUserRole();
        $fieldUserRoleContactTableSendNewsletter->field_id = $fieldContactTableSendNewsletter->id;
        $fieldUserRoleContactTableSendNewsletter->role_id = $idRole;
        $fieldUserRoleContactTableSendNewsletter->enabled = true;
        $fieldUserRoleContactTableSendNewsletter->table_order = 80;
        $fieldUserRoleContactTableSendNewsletter->input_type = 15;
        $fieldUserRoleContactTableSendNewsletter->created_id = $idUser;
        $fieldUserRoleContactTableSendNewsletter->save();

        #endregion SEND NEWSLETTER

        #region ERROR NEWSLETTER

        $fieldContactTableErrorNewsletter = new Field();
        $fieldContactTableErrorNewsletter->code = 'ErrorNewsletter';
        $fieldContactTableErrorNewsletter->tab_id = $tabContactTable->id;
        $fieldContactTableErrorNewsletter->field = 'errorNewsletter';
        $fieldContactTableErrorNewsletter->data_type = 3;
        $fieldContactTableErrorNewsletter->created_id = $idUser;
        $fieldContactTableErrorNewsletter->save();

        $fieldLanguageContactTableErrorNewsletterr = new FieldLanguage();
        $fieldLanguageContactTableErrorNewsletterr->field_id = $fieldContactTableErrorNewsletter->id;
        $fieldLanguageContactTableErrorNewsletterr->language_id = $idLanguage;
        $fieldLanguageContactTableErrorNewsletterr->description = 'Errore newsletter';
        $fieldLanguageContactTableErrorNewsletterr->created_id = $idUser;
        $fieldLanguageContactTableErrorNewsletterr->save();

        $fieldUserRoleContactTableErrorNewsletter = new FieldUserRole();
        $fieldUserRoleContactTableErrorNewsletter->field_id = $fieldContactTableErrorNewsletter->id;
        $fieldUserRoleContactTableErrorNewsletter->role_id = $idRole;
        $fieldUserRoleContactTableErrorNewsletter->enabled = true;
        $fieldUserRoleContactTableErrorNewsletter->table_order = 90;
        $fieldUserRoleContactTableErrorNewsletter->input_type = 15;
        $fieldUserRoleContactTableErrorNewsletter->created_id = $idUser;
        $fieldUserRoleContactTableErrorNewsletter->save();

        #endregion ERROR NEWSLETTER

        #endregion TABLE




        #region DETAIL

        #region TAB

        $tabContactDetail = new Tab();
        $tabContactDetail->code = 'Detail';
        $tabContactDetail->functionality_id = $functionalityContact->id;
        $tabContactDetail->order = 20;
        $tabContactDetail->created_id = $idUser;
        $tabContactDetail->save();

        $languageTabContactDetail = new LanguageTab();
        $languageTabContactDetail->language_id = $idLanguage;
        $languageTabContactDetail->tab_id = $tabContactDetail->id;
        $languageTabContactDetail->description = 'Detail';
        $languageTabContactDetail->created_id = $idUser;
        $languageTabContactDetail->save();

        #endregion TAB

        #region NAME

        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'Name';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'name';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Nome';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 10;
        $fieldUserRoleContactDetailCompany->pos_y = 10;
        $fieldUserRoleContactDetailCompany->input_type = 0;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();

        #endregion NAME

        #region SURNAME

        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'Surname';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'surname';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Cognome';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 20;
        $fieldUserRoleSurnameContactDetail->pos_y = 10;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();

        #endregion SURNAME

        #region BUSINNES NAME

        $fieldContactDetailBusinessName = new Field();
        $fieldContactDetailBusinessName->code = 'BusinessName';
        $fieldContactDetailBusinessName->tab_id = $tabContactDetail->id;
        $fieldContactDetailBusinessName->field = 'businessName';
        $fieldContactDetailBusinessName->data_type = 1;
        $fieldContactDetailBusinessName->created_id = $idUser;
        $fieldContactDetailBusinessName->save();

        $fieldLanguageContactDetailBusinessName = new FieldLanguage();
        $fieldLanguageContactDetailBusinessName->field_id = $fieldContactDetailBusinessName->id;
        $fieldLanguageContactDetailBusinessName->language_id = $idLanguage;
        $fieldLanguageContactDetailBusinessName->description = 'Ragione Sociale';
        $fieldLanguageContactDetailBusinessName->created_id = $idUser;
        $fieldLanguageContactDetailBusinessName->save();

        $fieldUserRoleContactDetailBusinessName = new FieldUserRole();
        $fieldUserRoleContactDetailBusinessName->field_id = $fieldContactDetailBusinessName->id;
        $fieldUserRoleContactDetailBusinessName->role_id = $idRole;
        $fieldUserRoleContactDetailBusinessName->enabled = true;
        $fieldUserRoleContactDetailBusinessName->pos_x = 30;
        $fieldUserRoleContactDetailBusinessName->pos_y = 10;
        $fieldUserRoleContactDetailBusinessName->input_type = 0;
        $fieldUserRoleContactDetailBusinessName->created_id = $idUser;
        $fieldUserRoleContactDetailBusinessName->save();

        #endregion BUSINNES NAME

        #region EMAIL

        $fieldContactDetailName = new Field();
        $fieldContactDetailName->code = 'Email';
        $fieldContactDetailName->tab_id = $tabContactDetail->id;
        $fieldContactDetailName->field = 'email';
        $fieldContactDetailName->data_type = 1;
        $fieldContactDetailName->created_id = $idUser;
        $fieldContactDetailName->save();

        $fieldLanguageContactDetailName = new FieldLanguage();
        $fieldLanguageContactDetailName->field_id = $fieldContactDetailName->id;
        $fieldLanguageContactDetailName->language_id = $idLanguage;
        $fieldLanguageContactDetailName->description = 'Email';
        $fieldLanguageContactDetailName->created_id = $idUser;
        $fieldLanguageContactDetailName->save();

        $fieldUserRoleContactDetailName = new FieldUserRole();
        $fieldUserRoleContactDetailName->field_id = $fieldContactDetailName->id;
        $fieldUserRoleContactDetailName->role_id = $idRole;
        $fieldUserRoleContactDetailName->enabled = true;
        $fieldUserRoleContactDetailName->pos_x = 40;
        $fieldUserRoleContactDetailName->pos_y = 10;
        $fieldUserRoleContactDetailName->input_type = 0;
        $fieldUserRoleContactDetailName->created_id = $idUser;
        $fieldUserRoleContactDetailName->save();

        #endregion EMAIL

        #region PHONE NUMBER

        $fieldContactDetailSurname = new Field();
        $fieldContactDetailSurname->code = 'PhoneNumber';
        $fieldContactDetailSurname->tab_id = $tabContactDetail->id;
        $fieldContactDetailSurname->field = 'phoneNumber';
        $fieldContactDetailSurname->data_type = 1;
        $fieldContactDetailSurname->created_id = $idUser;
        $fieldContactDetailSurname->save();

        $fieldLanguageContactDetailSurname = new FieldLanguage();
        $fieldLanguageContactDetailSurname->field_id = $fieldContactDetailSurname->id;
        $fieldLanguageContactDetailSurname->language_id = $idLanguage;
        $fieldLanguageContactDetailSurname->description = 'Telefono';
        $fieldLanguageContactDetailSurname->created_id = $idUser;
        $fieldLanguageContactDetailSurname->save();

        $fieldUserRoleContactDetailSurname = new FieldUserRole();
        $fieldUserRoleContactDetailSurname->field_id = $fieldContactDetailSurname->id;
        $fieldUserRoleContactDetailSurname->role_id = $idRole;
        $fieldUserRoleContactDetailSurname->enabled = true;
        $fieldUserRoleContactDetailSurname->pos_x = 10;
        $fieldUserRoleContactDetailSurname->pos_y = 20;
        $fieldUserRoleContactDetailSurname->input_type = 0;
        $fieldUserRoleContactDetailSurname->created_id = $idUser;
        $fieldUserRoleContactDetailSurname->save();

        #endregion PHONE NUMBER

        #region SEND NEWSLETTER

        $fieldContactDetailSendNewsletter = new Field();
        $fieldContactDetailSendNewsletter->code = 'SendNewsletter';
        $fieldContactDetailSendNewsletter->tab_id = $tabContactDetail->id;
        $fieldContactDetailSendNewsletter->field = 'sendNewsletter';
        $fieldContactDetailSendNewsletter->data_type = 3;
        $fieldContactDetailSendNewsletter->created_id = $idUser;
        $fieldContactDetailSendNewsletter->save();

        $fieldLanguageContactDetailSendNewsletter = new FieldLanguage();
        $fieldLanguageContactDetailSendNewsletter->field_id = $fieldContactDetailSendNewsletter->id;
        $fieldLanguageContactDetailSendNewsletter->language_id = $idLanguage;
        $fieldLanguageContactDetailSendNewsletter->description = 'Invia newsletter';
        $fieldLanguageContactDetailSendNewsletter->created_id = $idUser;
        $fieldLanguageContactDetailSendNewsletter->save();

        $fieldUserRoleContactDetailSendNewsletter = new FieldUserRole();
        $fieldUserRoleContactDetailSendNewsletter->field_id = $fieldContactDetailSendNewsletter->id;
        $fieldUserRoleContactDetailSendNewsletter->role_id = $idRole;
        $fieldUserRoleContactDetailSendNewsletter->enabled = true;
        $fieldUserRoleContactDetailSendNewsletter->pos_x = 20;
        $fieldUserRoleContactDetailSendNewsletter->pos_y = 20;
        $fieldUserRoleContactDetailSendNewsletter->input_type = 5;
        $fieldUserRoleContactDetailSendNewsletter->created_id = $idUser;
        $fieldUserRoleContactDetailSendNewsletter->save();

        #endregion SEND NEWSLETTER

        #region ERROR NEWSLETTER

        $fieldContactDetailErrorNewsletter = new Field();
        $fieldContactDetailErrorNewsletter->code = 'ErrorNewsletter';
        $fieldContactDetailErrorNewsletter->tab_id = $tabContactDetail->id;
        $fieldContactDetailErrorNewsletter->field = 'errorNewsletter';
        $fieldContactDetailErrorNewsletter->data_type = 3;
        $fieldContactDetailErrorNewsletter->created_id = $idUser;
        $fieldContactDetailErrorNewsletter->save();

        $fieldLanguageContactDetailErrorNewsletterr = new FieldLanguage();
        $fieldLanguageContactDetailErrorNewsletterr->field_id = $fieldContactDetailErrorNewsletter->id;
        $fieldLanguageContactDetailErrorNewsletterr->language_id = $idLanguage;
        $fieldLanguageContactDetailErrorNewsletterr->description = 'Errore newsletter';
        $fieldLanguageContactDetailErrorNewsletterr->created_id = $idUser;
        $fieldLanguageContactDetailErrorNewsletterr->save();

        $fieldUserRoleContactDetailErrorNewsletter = new FieldUserRole();
        $fieldUserRoleContactDetailErrorNewsletter->field_id = $fieldContactDetailErrorNewsletter->id;
        $fieldUserRoleContactDetailErrorNewsletter->role_id = $idRole;
        $fieldUserRoleContactDetailErrorNewsletter->enabled = true;
        $fieldUserRoleContactDetailErrorNewsletter->pos_x = 30;
        $fieldUserRoleContactDetailErrorNewsletter->pos_y = 20;
        $fieldUserRoleContactDetailErrorNewsletter->input_type = 5;
        $fieldUserRoleContactDetailErrorNewsletter->created_id = $idUser;
        $fieldUserRoleContactDetailErrorNewsletter->save();

        #endregion ERROR NEWSLETTER

        #endregion DETAIL

        #endregion CONTACT
    }
}

<?php

use App\Dictionary;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class ProducerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        //$idMenuAdminCRM = MenuAdmin::where('code', 'CRM')->first()->id;

 		#region add newProducer 
        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'newTitleProducer';
        $dictionary->description = 'Inserisci Produttori';
        $dictionary->created_id = $idUser;
        $dictionary->save();
        #endregion add newProducer

        $iconAddressCard = new Icon();
        $iconAddressCard->description = 'AddressCard';
        $iconAddressCard->css = 'fa fa-address-card';
        $iconAddressCard->created_id = $idUser;
        $iconAddressCard->save();


        #region ICON
        $iconIdCard = new Icon();
        $iconIdCard->description = 'Wrench';
        $iconIdCard->css = 'fa fa-wrench';
        $iconIdCard->created_id = $idUser;
        $iconIdCard->save(); 

        $iconAddressCard = new Icon();
        $iconAddressCard->description = 'Book';
        $iconAddressCard->css = 'fa fa-book';
        $iconAddressCard->created_id = $idUser;
        $iconAddressCard->save();

        #endregion ICON

 		#region FUNCTIONALITY
        $functionalityContact = new Functionality();
        $functionalityContact->code = 'Producers';
        $functionalityContact->created_id = $idUser;
        $functionalityContact->save();
        #endregion FUNCTIONALITY

        #region MENU[]
     /*   $menuAdminCRM = new MenuAdmin();
        $menuAdminCRM->code = 'Anagrafiche';
        $menuAdminCRM->icon_id = $iconAddressCard->id;
        $menuAdminCRM->created_id = $idUser;
        $menuAdminCRM->save();
 
        $languageMenuAdminCRM = new LanguageMenuAdmin();
        $languageMenuAdminCRM->language_id = $idLanguage;
        $languageMenuAdminCRM->menu_admin_id = $menuAdminCRM->id;
        $languageMenuAdminCRM->description = 'Anagrafiche';
        $languageMenuAdminCRM->created_id = $idUser;
        $languageMenuAdminCRM->save();

        $menuAdminUserRoleCRM = new MenuAdminUserRole();
        $menuAdminUserRoleCRM->menu_admin_id = $menuAdminCRM->id;
        $menuAdminUserRoleCRM->role_id = $idRole;
        $menuAdminUserRoleCRM->enabled = true;
        $menuAdminUserRoleCRM->order = 160;
        $menuAdminUserRoleCRM->created_id = $idUser;
        $menuAdminUserRoleCRM->save();*/


      /*  $menuAdminContact = new MenuAdmin();
        $menuAdminContact->code = 'Produttori';
        $menuAdminContact->functionality_id = $functionalityContact->id;
        $menuAdminContact->icon_id = $iconIdCard->id;
        $menuAdminContact->url = '/admin/registries/Producer';
        $menuAdminContact->created_id = $idUser;
        $menuAdminContact->save();

        $languageMenuAdminContact = new LanguageMenuAdmin();
        $languageMenuAdminContact->language_id = $idLanguage;
        $languageMenuAdminContact->menu_admin_id = $menuAdminContact->id;
        $languageMenuAdminContact->description = 'Produttori';
        $languageMenuAdminContact->created_id = $idUser;
        $languageMenuAdminContact->save();

        $menuAdminUserRoleContact = new MenuAdminUserRole();
        $menuAdminUserRoleContact->menu_admin_id = $menuAdminContact->id;
        $menuAdminUserRoleContact->menu_admin_father_id = $menuAdminCRM->id;
        $menuAdminUserRoleContact->role_id = $idRole;
        $menuAdminUserRoleContact->enabled = true;
        $menuAdminUserRoleContact->order = 10;
        $menuAdminUserRoleContact->created_id = $idUser;
        $menuAdminUserRoleContact->save();*/
        #endregion MENU
        
		#region TAB
        $tabContactTable = new Tab();
        $tabContactTable->code = 'Table';
        $tabContactTable->functionality_id = $functionalityContact->id;
        $tabContactTable->order = 10;
        $tabContactTable->created_id = $idUser;
        $tabContactTable->save();

        $languageTabContactTable = new LanguageTab();
        $languageTabContactTable->language_id = $idLanguage;
        $languageTabContactTable->tab_id = $tabContactTable->id;
        $languageTabContactTable->description = 'Table';
        $languageTabContactTable->created_id = $idUser;
        $languageTabContactTable->save();
        #endregion TAB

        #region CHECKBOX
        $fieldContactCheckBox = new Field();
        $fieldContactCheckBox->tab_id = $tabContactTable->id;
        $fieldContactCheckBox->code = 'TableCheckBox';
        $fieldContactCheckBox->created_id = $idUser;
        $fieldContactCheckBox->save();

        $fieldLanguageContactCheckBox = new FieldLanguage();
        $fieldLanguageContactCheckBox->field_id = $fieldContactCheckBox->id;
        $fieldLanguageContactCheckBox->language_id = $idLanguage;
        $fieldLanguageContactCheckBox->description = 'CheckBox';
        $fieldLanguageContactCheckBox->created_id = $idUser;
        $fieldLanguageContactCheckBox->save();

        $fieldUserRoleContactCheckBox = new FieldUserRole();
        $fieldUserRoleContactCheckBox->field_id = $fieldContactCheckBox->id;
        $fieldUserRoleContactCheckBox->role_id = $idRole;
        $fieldUserRoleContactCheckBox->enabled = true;
        $fieldUserRoleContactCheckBox->table_order = 10;
        $fieldUserRoleContactCheckBox->input_type = 13;
        $fieldUserRoleContactCheckBox->created_id = $idUser;
        $fieldUserRoleContactCheckBox->save();
        #endregion CHECKBOX

         #region FORMATTER
        $fieldContactFormatter = new Field();
        $fieldContactFormatter->code = 'ProducersFormatter';
        $fieldContactFormatter->formatter = 'ProducersFormatter';
        $fieldContactFormatter->tab_id = $tabContactTable->id;
        $fieldContactFormatter->created_id = $idUser;
        $fieldContactFormatter->save();

        $fieldLanguageContactFormatter = new FieldLanguage();
        $fieldLanguageContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldLanguageContactFormatter->language_id = $idLanguage;
        $fieldLanguageContactFormatter->description = '';
        $fieldLanguageContactFormatter->created_id = $idUser;
        $fieldLanguageContactFormatter->save();

        $fieldUserRoleContactFormatter = new FieldUserRole();
        $fieldUserRoleContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldUserRoleContactFormatter->role_id = $idRole;
        $fieldUserRoleContactFormatter->enabled = true;
        $fieldUserRoleContactFormatter->table_order = 20;
        $fieldUserRoleContactFormatter->input_type = 14;
        $fieldUserRoleContactFormatter->created_id = $idUser;
        $fieldUserRoleContactFormatter->save();
        #endregion FORMATTER

        #region code
        $fieldContactTableCompany = new Field();
        $fieldContactTableCompany->code = 'Code';
        $fieldContactTableCompany->tab_id = $tabContactTable->id;
        $fieldContactTableCompany->field = 'Code';
        $fieldContactTableCompany->data_type = 1;
        $fieldContactTableCompany->created_id = $idUser;
        $fieldContactTableCompany->save();

        $fieldLanguageContactTableCompany = new FieldLanguage();
        $fieldLanguageContactTableCompany->field_id = $fieldContactTableCompany->id;
        $fieldLanguageContactTableCompany->language_id = $idLanguage;
        $fieldLanguageContactTableCompany->description = 'Codice';
        $fieldLanguageContactTableCompany->created_id = $idUser;
        $fieldLanguageContactTableCompany->save();

        $fieldUserRoleContactTableCompany = new FieldUserRole();
        $fieldUserRoleContactTableCompany->field_id = $fieldContactTableCompany->id;
        $fieldUserRoleContactTableCompany->role_id = $idRole;
        $fieldUserRoleContactTableCompany->enabled = true;
        $fieldUserRoleContactTableCompany->table_order = 30;
        $fieldUserRoleContactTableCompany->input_type = 0;
        $fieldUserRoleContactTableCompany->created_id = $idUser;
        $fieldUserRoleContactTableCompany->save();     
        #endregion code

        #region business_name
        $fieldContactTableBusinessName = new Field();
        $fieldContactTableBusinessName->code = 'BusinessName';
        $fieldContactTableBusinessName->tab_id = $tabContactTable->id;
        $fieldContactTableBusinessName->field = 'BusinessName';
        $fieldContactTableBusinessName->data_type = 1;
        $fieldContactTableBusinessName->created_id = $idUser;
        $fieldContactTableBusinessName->save();

        $fieldLanguageContactTableBusinessName = new FieldLanguage();
        $fieldLanguageContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldLanguageContactTableBusinessName->language_id = $idLanguage;
        $fieldLanguageContactTableBusinessName->description = 'Ragione Sociale';
        $fieldLanguageContactTableBusinessName->created_id = $idUser;
        $fieldLanguageContactTableBusinessName->save();

        $fieldUserRoleContactTableBusinessName = new FieldUserRole();
        $fieldUserRoleContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldUserRoleContactTableBusinessName->role_id = $idRole;
        $fieldUserRoleContactTableBusinessName->enabled = true;
        $fieldUserRoleContactTableBusinessName->table_order = 40;
        $fieldUserRoleContactTableBusinessName->input_type = 0;
        $fieldUserRoleContactTableBusinessName->created_id = $idUser;
        $fieldUserRoleContactTableBusinessName->save();
        #endregion business_name

 		#region address
        $fieldContactTableBusinessName = new Field();
        $fieldContactTableBusinessName->code = 'Address';
        $fieldContactTableBusinessName->tab_id = $tabContactTable->id;
        $fieldContactTableBusinessName->field = 'Address';
        $fieldContactTableBusinessName->data_type = 1;
        $fieldContactTableBusinessName->created_id = $idUser;
        $fieldContactTableBusinessName->save();

        $fieldLanguageContactTableBusinessName = new FieldLanguage();
        $fieldLanguageContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldLanguageContactTableBusinessName->language_id = $idLanguage;
        $fieldLanguageContactTableBusinessName->description = 'Indirizzo';
        $fieldLanguageContactTableBusinessName->created_id = $idUser;
        $fieldLanguageContactTableBusinessName->save();

        $fieldUserRoleContactTableBusinessName = new FieldUserRole();
        $fieldUserRoleContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldUserRoleContactTableBusinessName->role_id = $idRole;
        $fieldUserRoleContactTableBusinessName->enabled = true;
        $fieldUserRoleContactTableBusinessName->table_order = 50;
        $fieldUserRoleContactTableBusinessName->input_type = 0;
        $fieldUserRoleContactTableBusinessName->created_id = $idUser;
        $fieldUserRoleContactTableBusinessName->save();
        #endregion address

		#region resort
        $fieldContactTableBusinessName = new Field();
        $fieldContactTableBusinessName->code = 'Resort';
        $fieldContactTableBusinessName->tab_id = $tabContactTable->id;
        $fieldContactTableBusinessName->field = 'Resort';
        $fieldContactTableBusinessName->data_type = 1;
        $fieldContactTableBusinessName->created_id = $idUser;
        $fieldContactTableBusinessName->save();

        $fieldLanguageContactTableBusinessName = new FieldLanguage();
        $fieldLanguageContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldLanguageContactTableBusinessName->language_id = $idLanguage;
        $fieldLanguageContactTableBusinessName->description = 'Località';
        $fieldLanguageContactTableBusinessName->created_id = $idUser;
        $fieldLanguageContactTableBusinessName->save();

        $fieldUserRoleContactTableBusinessName = new FieldUserRole();
        $fieldUserRoleContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldUserRoleContactTableBusinessName->role_id = $idRole;
        $fieldUserRoleContactTableBusinessName->enabled = true;
        $fieldUserRoleContactTableBusinessName->table_order = 60;
        $fieldUserRoleContactTableBusinessName->input_type = 0;
        $fieldUserRoleContactTableBusinessName->created_id = $idUser;
        $fieldUserRoleContactTableBusinessName->save();
        #endregion resort

		#region province
        $fieldContactTableBusinessName = new Field();
        $fieldContactTableBusinessName->code = 'Province';
        $fieldContactTableBusinessName->tab_id = $tabContactTable->id;
        $fieldContactTableBusinessName->field = 'Province';
        $fieldContactTableBusinessName->data_type = 1;
        $fieldContactTableBusinessName->created_id = $idUser;
        $fieldContactTableBusinessName->save();

        $fieldLanguageContactTableBusinessName = new FieldLanguage();
        $fieldLanguageContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldLanguageContactTableBusinessName->language_id = $idLanguage;
        $fieldLanguageContactTableBusinessName->description = 'Provincia';
        $fieldLanguageContactTableBusinessName->created_id = $idUser;
        $fieldLanguageContactTableBusinessName->save();

        $fieldUserRoleContactTableBusinessName = new FieldUserRole();
        $fieldUserRoleContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldUserRoleContactTableBusinessName->role_id = $idRole;
        $fieldUserRoleContactTableBusinessName->enabled = true;
        $fieldUserRoleContactTableBusinessName->table_order = 70;
        $fieldUserRoleContactTableBusinessName->input_type = 0;
        $fieldUserRoleContactTableBusinessName->created_id = $idUser;
        $fieldUserRoleContactTableBusinessName->save();
        #endregion province

		#region postal_code
        $fieldContactTableBusinessName = new Field();
        $fieldContactTableBusinessName->code = 'PostalCode';
        $fieldContactTableBusinessName->tab_id = $tabContactTable->id;
        $fieldContactTableBusinessName->field = 'PostalCode';
        $fieldContactTableBusinessName->data_type = 1;
        $fieldContactTableBusinessName->created_id = $idUser;
        $fieldContactTableBusinessName->save();

        $fieldLanguageContactTableBusinessName = new FieldLanguage();
        $fieldLanguageContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldLanguageContactTableBusinessName->language_id = $idLanguage;
        $fieldLanguageContactTableBusinessName->description = 'Codice Postale';
        $fieldLanguageContactTableBusinessName->created_id = $idUser;
        $fieldLanguageContactTableBusinessName->save();

        $fieldUserRoleContactTableBusinessName = new FieldUserRole();
        $fieldUserRoleContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldUserRoleContactTableBusinessName->role_id = $idRole;
        $fieldUserRoleContactTableBusinessName->enabled = true;
        $fieldUserRoleContactTableBusinessName->table_order = 80;
        $fieldUserRoleContactTableBusinessName->input_type = 0;
        $fieldUserRoleContactTableBusinessName->created_id = $idUser;
        $fieldUserRoleContactTableBusinessName->save();
        #endregion postal_code

		#region email
        $fieldContactTableBusinessName = new Field();
        $fieldContactTableBusinessName->code = 'Email';
        $fieldContactTableBusinessName->tab_id = $tabContactTable->id;
        $fieldContactTableBusinessName->field = 'Email';
        $fieldContactTableBusinessName->data_type = 1;
        $fieldContactTableBusinessName->created_id = $idUser;
        $fieldContactTableBusinessName->save();

        $fieldLanguageContactTableBusinessName = new FieldLanguage();
        $fieldLanguageContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldLanguageContactTableBusinessName->language_id = $idLanguage;
        $fieldLanguageContactTableBusinessName->description = 'Email';
        $fieldLanguageContactTableBusinessName->created_id = $idUser;
        $fieldLanguageContactTableBusinessName->save();

        $fieldUserRoleContactTableBusinessName = new FieldUserRole();
        $fieldUserRoleContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldUserRoleContactTableBusinessName->role_id = $idRole;
        $fieldUserRoleContactTableBusinessName->enabled = true;
        $fieldUserRoleContactTableBusinessName->table_order = 90;
        $fieldUserRoleContactTableBusinessName->input_type = 0;
        $fieldUserRoleContactTableBusinessName->created_id = $idUser;
        $fieldUserRoleContactTableBusinessName->save();
        #endregion email

        #endregion TAB

        #region DETAIL
        #region TAB
        $tabContactDetail = new Tab();
        $tabContactDetail->code = 'DetailProducers';
        $tabContactDetail->functionality_id = $functionalityContact->id;
        $tabContactDetail->order = 20;
        $tabContactDetail->created_id = $idUser;
        $tabContactDetail->save();

        $languageTabContactDetail = new LanguageTab();
        $languageTabContactDetail->language_id = $idLanguage;
        $languageTabContactDetail->tab_id = $tabContactDetail->id;
        $languageTabContactDetail->description = 'DetailProducers';
        $languageTabContactDetail->created_id = $idUser;
        $languageTabContactDetail->save();
        #endregion TAB

  		#region code
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'code';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'code';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Codice';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 10;
        $fieldUserRoleContactDetailCompany->pos_y = 10;
        $fieldUserRoleContactDetailCompany->input_type = 0;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion code


		#region business_name
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'business_name';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'business_name';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Ragione Sociale';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 20;
        $fieldUserRoleContactDetailCompany->pos_y = 10;
        $fieldUserRoleContactDetailCompany->input_type = 0;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion business_name


		#region address 
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'address';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'address';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Indirizzo';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 30;
        $fieldUserRoleContactDetailCompany->pos_y = 10;
        $fieldUserRoleContactDetailCompany->input_type = 0;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion address

		#region resort 
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'resort';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'resort';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Località';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 40;
        $fieldUserRoleContactDetailCompany->pos_y = 20;
        $fieldUserRoleContactDetailCompany->input_type = 0;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion resort

		#region province 
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'province';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'province';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Provincia';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 50;
        $fieldUserRoleContactDetailCompany->pos_y = 20;
        $fieldUserRoleContactDetailCompany->input_type = 0;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion resort

         #region postal_code_id
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'postal_code_id';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'postal_code_id';
        $fieldSurnameContactDetail->service = 'postalCode';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Codice Postale';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 60;
        $fieldUserRoleSurnameContactDetail->pos_y = 20;
        $fieldUserRoleSurnameContactDetail->input_type = 2;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion postal_code_id

  #region TELEPHONE NUMBER

        $fieldCustomerDetailTelephoneNumber = new Field();
        $fieldCustomerDetailTelephoneNumber->code = 'telephone_number';
        $fieldCustomerDetailTelephoneNumber->tab_id = $tabContactDetail->id;
        $fieldCustomerDetailTelephoneNumber->field = 'telephone_number';
        $fieldCustomerDetailTelephoneNumber->data_type = 1;
        $fieldCustomerDetailTelephoneNumber->created_id = $idUser;
        $fieldCustomerDetailTelephoneNumber->save();

        $fieldLanguageCustomerDetailTelephoneNumber = new FieldLanguage();
        $fieldLanguageCustomerDetailTelephoneNumber->field_id = $fieldCustomerDetailTelephoneNumber->id;
        $fieldLanguageCustomerDetailTelephoneNumber->language_id = $idLanguage;
        $fieldLanguageCustomerDetailTelephoneNumber->description = 'Telefono';
        $fieldLanguageCustomerDetailTelephoneNumber->created_id = $idUser;
        $fieldLanguageCustomerDetailTelephoneNumber->save();

        $fieldUserRoleCustomerDetailTelephoneNumber = new FieldUserRole();
        $fieldUserRoleCustomerDetailTelephoneNumber->field_id = $fieldCustomerDetailTelephoneNumber->id;
        $fieldUserRoleCustomerDetailTelephoneNumber->role_id = $idRole;
        $fieldUserRoleCustomerDetailTelephoneNumber->enabled = true;
        $fieldUserRoleCustomerDetailTelephoneNumber->pos_x = 70;
        $fieldUserRoleCustomerDetailTelephoneNumber->pos_y = 30;
        $fieldUserRoleCustomerDetailTelephoneNumber->input_type = 8;
        $fieldUserRoleCustomerDetailTelephoneNumber->created_id = $idUser;
        $fieldUserRoleCustomerDetailTelephoneNumber->save();

        #endregion TELEPHONE NUMBER

 		#region EMAIL
        $fieldCustomerDetailEmail = new Field();
        $fieldCustomerDetailEmail->code = 'email';
        $fieldCustomerDetailEmail->tab_id = $tabContactDetail->id;
        $fieldCustomerDetailEmail->field = 'email';
        $fieldCustomerDetailEmail->data_type = 1;
        $fieldCustomerDetailEmail->created_id = $idUser;
        $fieldCustomerDetailEmail->save();

        $fieldLanguageCustomerDetailEmail = new FieldLanguage();
        $fieldLanguageCustomerDetailEmail->field_id = $fieldCustomerDetailEmail->id;
        $fieldLanguageCustomerDetailEmail->language_id = $idLanguage;
        $fieldLanguageCustomerDetailEmail->description = 'Email';
        $fieldLanguageCustomerDetailEmail->created_id = $idUser;
        $fieldLanguageCustomerDetailEmail->save();

        $fieldUserRoleCustomerDetailEmail = new FieldUserRole();
        $fieldUserRoleCustomerDetailEmail->field_id = $fieldCustomerDetailEmail->id;
        $fieldUserRoleCustomerDetailEmail->role_id = $idRole;
        $fieldUserRoleCustomerDetailEmail->enabled = true;
        $fieldUserRoleCustomerDetailEmail->pos_x = 80;
        $fieldUserRoleCustomerDetailEmail->pos_y = 30;
        $fieldUserRoleCustomerDetailEmail->input_type = 9;
        $fieldUserRoleCustomerDetailEmail->created_id = $idUser;
        $fieldUserRoleCustomerDetailEmail->save();
        #endregion EMAIL

         #region online
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'online';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'online';
        $fieldSurnameContactDetail->data_type = 3;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Online';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 90;
        $fieldUserRoleSurnameContactDetail->pos_y = 30;
        $fieldUserRoleSurnameContactDetail->input_type = 5;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion online

    }
}

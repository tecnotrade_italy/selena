<?php

use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class SixtenSeederNew extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*$this->call([
            UserSeeder::class,
            RoleSeeder::class,
            PermissionSeeder::class,
            LanguageSeeder::class,
            DictionarySeeder::class,
            DocumentTypeSeeder::class,
            DocumentRowTypeSeeder::class,
            // SixtenSeeder::class
            // PickingSeeder
        ]);*/

        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'prando')->first()->id;

        #region OC LIST

        $functionalityPickingOCList = new Functionality();
        $functionalityPickingOCList->code = 'delete_picking_oc_list';
        $functionalityPickingOCList->created_id = $idUser;
        $functionalityPickingOCList->save();

        $iconFileImageO = new Icon();
        $iconFileImageO->description = 'List';
        $iconFileImageO->css = 'fa fa-list';
        $iconFileImageO->created_id = $idUser;
        $iconFileImageO->save();

        $menuAdminPickingOCList = new MenuAdmin();                                                  
        $menuAdminPickingOCList->code = 'delete_picking_oc_list';
        $menuAdminPickingOCList->functionality_id = $functionalityPickingOCList->id;
        $menuAdminPickingOCList->icon_id = $iconFileImageO->id;
        $menuAdminPickingOCList->url = '/admin/picking/PickedList';
        $menuAdminPickingOCList->created_id = $idUser;
        $menuAdminPickingOCList->save();

        $languageMenuAdminPickingOCList = new LanguageMenuAdmin();
        $languageMenuAdminPickingOCList->language_id = $idLanguage;
        $languageMenuAdminPickingOCList->menu_admin_id = $menuAdminPickingOCList->id;
        $languageMenuAdminPickingOCList->description = 'Lista Letture OC';
        $languageMenuAdminPickingOCList->created_id = $idUser;
        $languageMenuAdminPickingOCList->save();

        $menuAdminUserRolePickingOCList = new MenuAdminUserRole();
        $menuAdminUserRolePickingOCList->menu_admin_id = $menuAdminPickingOCList->id;
        $menuAdminUserRolePickingOCList->role_id = $idRole;
        $menuAdminUserRolePickingOCList->enabled = true;
        $menuAdminUserRolePickingOCList->order = 10;
        $menuAdminUserRolePickingOCList->created_id = $idUser;
        $menuAdminUserRolePickingOCList->save();

        $tabContentPickingOCList = new Tab();
        $tabContentPickingOCList->code = 'picked_oc_list';
        $tabContentPickingOCList->functionality_id = $functionalityPickingOCList->id;
        $tabContentPickingOCList->order = 10;
        $tabContentPickingOCList->created_id = $idUser;
        $tabContentPickingOCList->save();

        $languageTabContentPickingOCList = new LanguageTab();
        $languageTabContentPickingOCList->language_id = $idLanguage;
        $languageTabContentPickingOCList->tab_id = $tabContentPickingOCList->id;
        $languageTabContentPickingOCList->description = 'Lista Letture OC';
        $languageTabContentPickingOCList->created_id = $idUser;
        $languageTabContentPickingOCList->save();

        #region CUSTOMER DESCRIPTION

        $fieldCustomerDescription = new Field();
        $fieldCustomerDescription->code = 'customer_description';
        $fieldCustomerDescription->tab_id = $tabContentPickingOCList->id;
        $fieldCustomerDescription->field = 'customerDescription';
        $fieldCustomerDescription->data_type = 1;
        $fieldCustomerDescription->created_id = $idUser;
        $fieldCustomerDescription->save();

        $fieldLanguageCustomerDescription = new FieldLanguage();
        $fieldLanguageCustomerDescription->field_id = $fieldCustomerDescription->id;
        $fieldLanguageCustomerDescription->language_id = $idLanguage;
        $fieldLanguageCustomerDescription->description = 'Cliente';
        $fieldLanguageCustomerDescription->created_id = $idUser;
        $fieldLanguageCustomerDescription->save();

        $fieldUserRoleCustomerDescription = new FieldUserRole();
        $fieldUserRoleCustomerDescription->field_id = $fieldCustomerDescription->id;
        $fieldUserRoleCustomerDescription->role_id = $idRole;
        $fieldUserRoleCustomerDescription->enabled = true;
        $fieldUserRoleCustomerDescription->input_type = 15;
        $fieldUserRoleCustomerDescription->table_order = 10;
        $fieldUserRoleCustomerDescription->created_id = $idUser;
        $fieldUserRoleCustomerDescription->save();

        #endregion CUSTOMER DESCRIPTION

        #region ORDER NUMBER

        $fieldOrderNumber = new Field();
        $fieldOrderNumber->code = 'order_number';
        $fieldOrderNumber->tab_id = $tabContentPickingOCList->id;
        $fieldOrderNumber->field = 'orderNumber';
        $fieldOrderNumber->data_type = 1;
        $fieldOrderNumber->created_id = $idUser;
        $fieldOrderNumber->save();

        $fieldLanguageOrderNumber = new FieldLanguage();
        $fieldLanguageOrderNumber->field_id = $fieldOrderNumber->id;
        $fieldLanguageOrderNumber->language_id = $idLanguage;
        $fieldLanguageOrderNumber->description = 'Nr. OC';
        $fieldLanguageOrderNumber->created_id = $idUser;
        $fieldLanguageOrderNumber->save();

        $fieldUserRoleOrderNumber = new FieldUserRole();
        $fieldUserRoleOrderNumber->field_id = $fieldOrderNumber->id;
        $fieldUserRoleOrderNumber->role_id = $idRole;
        $fieldUserRoleOrderNumber->enabled = true;
        $fieldUserRoleOrderNumber->input_type = 15;
        $fieldUserRoleOrderNumber->table_order = 20;
        $fieldUserRoleOrderNumber->created_id = $idUser;
        $fieldUserRoleOrderNumber->save();

        #endregion ORDER NUMBER

        #region SHIPMENT MONTH

        $fieldShipmentMonth = new Field();
        $fieldShipmentMonth->code = 'shipment_month';
        $fieldShipmentMonth->tab_id = $tabContentPickingOCList->id;
        $fieldShipmentMonth->field = 'shipmentMonth';
        $fieldShipmentMonth->data_type = 1;
        $fieldShipmentMonth->created_id = $idUser;
        $fieldShipmentMonth->save();

        $fieldLanguageShipmentMonth = new FieldLanguage();
        $fieldLanguageShipmentMonth->field_id = $fieldShipmentMonth->id;
        $fieldLanguageShipmentMonth->language_id = $idLanguage;
        $fieldLanguageShipmentMonth->description = 'Mese';
        $fieldLanguageShipmentMonth->created_id = $idUser;
        $fieldLanguageShipmentMonth->save();

        $fieldUserRoleShipmentMonth = new FieldUserRole();
        $fieldUserRoleShipmentMonth->field_id = $fieldShipmentMonth->id;
        $fieldUserRoleShipmentMonth->role_id = $idRole;
        $fieldUserRoleShipmentMonth->enabled = true;
        $fieldUserRoleShipmentMonth->input_type = 15;
        $fieldUserRoleShipmentMonth->table_order = 30;
        $fieldUserRoleShipmentMonth->created_id = $idUser;
        $fieldUserRoleShipmentMonth->save();

        #endregion SHIPMENT MOUNTH

        $tabContentPickingOCList = new Tab();
        $tabContentPickingOCList->code = 'product_picked_list';
        $tabContentPickingOCList->functionality_id = $functionalityPickingOCList->id;
        $tabContentPickingOCList->order = 10;
        $tabContentPickingOCList->created_id = $idUser;
        $tabContentPickingOCList->save();

        $languageTabContentPickingOCList = new LanguageTab();
        $languageTabContentPickingOCList->language_id = $idLanguage;
        $languageTabContentPickingOCList->tab_id = $tabContentPickingOCList->id;
        $languageTabContentPickingOCList->description = 'Lista Lettura prodotti';
        $languageTabContentPickingOCList->created_id = $idUser;
        $languageTabContentPickingOCList->save();
        

        #region FORMATTER

        $fieldContactFormatter = new Field();
        $fieldContactFormatter->code = 'PickedListFormatter';
        $fieldContactFormatter->formatter = 'PickedListFormatter';
        $fieldContactFormatter->tab_id = $tabContentPickingOCList->id;
        $fieldContactFormatter->created_id = $idUser;
        $fieldContactFormatter->save();

        $fieldLanguageContactFormatter = new FieldLanguage();
        $fieldLanguageContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldLanguageContactFormatter->language_id = $idLanguage;
        $fieldLanguageContactFormatter->description = '';
        $fieldLanguageContactFormatter->created_id = $idUser;
        $fieldLanguageContactFormatter->save();

        $fieldUserRoleContactFormatter = new FieldUserRole();
        $fieldUserRoleContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldUserRoleContactFormatter->role_id = $idRole;
        $fieldUserRoleContactFormatter->enabled = true;
        $fieldUserRoleContactFormatter->table_order = 10;
        $fieldUserRoleContactFormatter->input_type = 14;
        $fieldUserRoleContactFormatter->created_id = $idUser;
        $fieldUserRoleContactFormatter->save();

        #endregion FORMATTER

        #region ITEM DESCRIPTION

        $fieldItemDescription = new Field();
        $fieldItemDescription->code = 'item_description';
        $fieldItemDescription->tab_id = $tabContentPickingOCList->id;
        $fieldItemDescription->field = 'itemDescription';
        $fieldItemDescription->data_type = 1;
        $fieldItemDescription->created_id = $idUser;
        $fieldItemDescription->save();

        $fieldLanguageItemDescription = new FieldLanguage();
        $fieldLanguageItemDescription->field_id = $fieldItemDescription->id;
        $fieldLanguageItemDescription->language_id = $idLanguage;
        $fieldLanguageItemDescription->description = 'Articolo';
        $fieldLanguageItemDescription->created_id = $idUser;
        $fieldLanguageItemDescription->save();

        $fieldUserRoleItemDescription = new FieldUserRole();
        $fieldUserRoleItemDescription->field_id = $fieldItemDescription->id;
        $fieldUserRoleItemDescription->role_id = $idRole;
        $fieldUserRoleItemDescription->enabled = true;
        $fieldUserRoleItemDescription->input_type = 15;
        $fieldUserRoleItemDescription->table_order = 20;
        $fieldUserRoleItemDescription->created_id = $idUser;
        $fieldUserRoleItemDescription->save();

        #endregion ITEM DESCRIPTION

        #region QUANTITY

        $fieldQuantity = new Field();
        $fieldQuantity->code = 'quantity';
        $fieldQuantity->tab_id = $tabContentPickingOCList->id;
        $fieldQuantity->field = 'quantity';
        $fieldQuantity->data_type = 1;
        $fieldQuantity->created_id = $idUser;
        $fieldQuantity->save();

        $fieldLanguageQuantity = new FieldLanguage();
        $fieldLanguageQuantity->field_id = $fieldQuantity->id;
        $fieldLanguageQuantity->language_id = $idLanguage;
        $fieldLanguageQuantity->description = 'Quantity';
        $fieldLanguageQuantity->created_id = $idUser;
        $fieldLanguageQuantity->save();

        $fieldUserRoleQuantity = new FieldUserRole();
        $fieldUserRoleQuantity->field_id = $fieldQuantity->id;
        $fieldUserRoleQuantity->role_id = $idRole;
        $fieldUserRoleQuantity->enabled = true;
        $fieldUserRoleQuantity->input_type = 15;
        $fieldUserRoleQuantity->table_order = 30;
        $fieldUserRoleQuantity->created_id = $idUser;
        $fieldUserRoleQuantity->save();

        #endregion QUANTITY

       #region QUANTITY PICKED

       $fieldQuantity = new Field();
       $fieldQuantity->code = 'pickedQuantity';
       $fieldQuantity->tab_id = $tabContentPickingOCList->id;
       $fieldQuantity->field = 'picked_quantity';
       $fieldQuantity->data_type = 1;
       $fieldQuantity->created_id = $idUser;
       $fieldQuantity->save();

       $fieldLanguageQuantity = new FieldLanguage();
       $fieldLanguageQuantity->field_id = $fieldQuantity->id;
       $fieldLanguageQuantity->language_id = $idLanguage;
       $fieldLanguageQuantity->description = 'Picked Quantity';
       $fieldLanguageQuantity->created_id = $idUser;
       $fieldLanguageQuantity->save();

       $fieldUserRoleQuantity = new FieldUserRole();
       $fieldUserRoleQuantity->field_id = $fieldQuantity->id;
       $fieldUserRoleQuantity->role_id = $idRole;
       $fieldUserRoleQuantity->enabled = true;
       $fieldUserRoleQuantity->input_type = 15;
       $fieldUserRoleQuantity->table_order = 40;
       $fieldUserRoleQuantity->created_id = $idUser;
       $fieldUserRoleQuantity->save();

       #endregion QUANTITY PICKED

        #endregion OC LIST

    }
}

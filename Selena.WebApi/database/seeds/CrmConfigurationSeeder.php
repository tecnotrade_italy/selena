<?php

use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class CrmConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        #region CRM

        #region ICON

        $iconAddressCard = new Icon();
        $iconAddressCard->description = 'AddressCard';
        $iconAddressCard->css = 'fa fa-address-card';
        $iconAddressCard->created_id = $idUser;
        $iconAddressCard->save();


        #endregion ICON

        #region MENU

        $menuAdminCRM = new MenuAdmin();
        $menuAdminCRM->code = 'CRM';
        $menuAdminCRM->icon_id = $iconAddressCard->id;
        $menuAdminCRM->created_id = $idUser;
        $menuAdminCRM->save();

        $languageMenuAdminCRM = new LanguageMenuAdmin();
        $languageMenuAdminCRM->language_id = $idLanguage;
        $languageMenuAdminCRM->menu_admin_id = $menuAdminCRM->id;
        $languageMenuAdminCRM->description = 'CRM';
        $languageMenuAdminCRM->created_id = $idUser;
        $languageMenuAdminCRM->save();

        $menuAdminUserRoleCRM = new MenuAdminUserRole();
        $menuAdminUserRoleCRM->menu_admin_id = $menuAdminCRM->id;
        $menuAdminUserRoleCRM->role_id = $idRole;
        $menuAdminUserRoleCRM->enabled = true;
        $menuAdminUserRoleCRM->order = 50;
        $menuAdminUserRoleCRM->created_id = $idUser;
        $menuAdminUserRoleCRM->save();


        #endregion MENU

        #endregion CRM

        #region REQUEST CONTACT



        #region FUNCTIONALITY

        $functionalityRequestContacts = new Functionality();
        $functionalityRequestContacts->code = 'RequestContacts';
        $functionalityRequestContacts->created_id = $idUser;
        $functionalityRequestContacts->save();

        #endregion FUNCTIONALITY

        #region ICON

        $iconTicket = new Icon();
        $iconTicket->description = 'Ticket';
        $iconTicket->css = 'fa fa-ticket';
        $iconTicket->created_id = $idUser;
        $iconTicket->save();

        #endregion ICON

        #region MENU

        $menuAdminRequestContacts = new MenuAdmin();
        $menuAdminRequestContacts->code = 'RequestContacts';
        $menuAdminRequestContacts->functionality_id = $functionalityRequestContacts->id;
        $menuAdminRequestContacts->icon_id = $iconTicket->id;
        $menuAdminRequestContacts->url = '/admin/crm/requestcontact';
        $menuAdminRequestContacts->created_id = $idUser;
        $menuAdminRequestContacts->save();

        $languageMenuAdminRequestContacts = new LanguageMenuAdmin();
        $languageMenuAdminRequestContacts->language_id = $idLanguage;
        $languageMenuAdminRequestContacts->menu_admin_id = $menuAdminRequestContacts->id;
        $languageMenuAdminRequestContacts->description = 'Richiesta contatti';
        $languageMenuAdminRequestContacts->created_id = $idUser;
        $languageMenuAdminRequestContacts->save();

        $menuAdminUserRoleRequestContacts = new MenuAdminUserRole();
        $menuAdminUserRoleRequestContacts->menu_admin_id = $menuAdminRequestContacts->id;
        $menuAdminUserRoleRequestContacts->menu_admin_father_id = $menuAdminCRM->id;
        $menuAdminUserRoleRequestContacts->role_id = $idRole;
        $menuAdminUserRoleRequestContacts->enabled = true;
        $menuAdminUserRoleRequestContacts->order = 10;
        $menuAdminUserRoleRequestContacts->created_id = $idUser;
        $menuAdminUserRoleRequestContacts->save();

        #endregion MENU

        #region TAB

        $tabContentRequestContacts = new Tab();
        $tabContentRequestContacts->code = 'Content';
        $tabContentRequestContacts->functionality_id = $functionalityRequestContacts->id;
        $tabContentRequestContacts->order = 10;
        $tabContentRequestContacts->created_id = $idUser;
        $tabContentRequestContacts->save();

        $languageTabContentRequestContacts = new LanguageTab();
        $languageTabContentRequestContacts->language_id = $idLanguage;
        $languageTabContentRequestContacts->tab_id = $tabContentRequestContacts->id;
        $languageTabContentRequestContacts->description = 'Contenuto';
        $languageTabContentRequestContacts->created_id = $idUser;
        $languageTabContentRequestContacts->save();

        #endregion TAB

        #region TABLE

        #region NAME

        $fieldRequestContactsName = new Field();
        $fieldRequestContactsName->code = 'Name';
        $fieldRequestContactsName->tab_id = $tabContentRequestContacts->id;
        $fieldRequestContactsName->field = 'name';
        $fieldRequestContactsName->data_type = 1;
        $fieldRequestContactsName->created_id = $idUser;
        $fieldRequestContactsName->save();

        $fieldLanguageRequestContactsName = new FieldLanguage();
        $fieldLanguageRequestContactsName->field_id = $fieldRequestContactsName->id;
        $fieldLanguageRequestContactsName->language_id = $idLanguage;
        $fieldLanguageRequestContactsName->description = 'Nome';
        $fieldLanguageRequestContactsName->created_id = $idUser;
        $fieldLanguageRequestContactsName->save();

        $fieldUserRoleRequestContactsName = new FieldUserRole();
        $fieldUserRoleRequestContactsName->field_id = $fieldRequestContactsName->id;
        $fieldUserRoleRequestContactsName->role_id = $idRole;
        $fieldUserRoleRequestContactsName->enabled = true;
        $fieldUserRoleRequestContactsName->input_type = 15;
        $fieldUserRoleRequestContactsName->table_order = 10;
        $fieldUserRoleRequestContactsName->created_id = $idUser;
        $fieldUserRoleRequestContactsName->save();

        #endregion NAME

        #region SURNAME

        $fieldRequestContactsSurname = new Field();
        $fieldRequestContactsSurname->code = 'Surname';
        $fieldRequestContactsSurname->tab_id = $tabContentRequestContacts->id;
        $fieldRequestContactsSurname->field = 'surname';
        $fieldRequestContactsSurname->data_type = 1;
        $fieldRequestContactsSurname->created_id = $idUser;
        $fieldRequestContactsSurname->save();

        $fieldLanguageRequestContactsSurname = new FieldLanguage();
        $fieldLanguageRequestContactsSurname->field_id = $fieldRequestContactsSurname->id;
        $fieldLanguageRequestContactsSurname->language_id = $idLanguage;
        $fieldLanguageRequestContactsSurname->description = 'Cognome';
        $fieldLanguageRequestContactsSurname->created_id = $idUser;
        $fieldLanguageRequestContactsSurname->save();

        $fieldUserRoleRequestContactsSurname = new FieldUserRole();
        $fieldUserRoleRequestContactsSurname->field_id = $fieldRequestContactsSurname->id;
        $fieldUserRoleRequestContactsSurname->role_id = $idRole;
        $fieldUserRoleRequestContactsSurname->enabled = true;
        $fieldUserRoleRequestContactsSurname->input_type = 15;
        $fieldUserRoleRequestContactsSurname->table_order = 20;
        $fieldUserRoleRequestContactsSurname->created_id = $idUser;
        $fieldUserRoleRequestContactsSurname->save();

        #endregion SURNAME

        #region EMAIL

        $fieldRequestContactsEmail = new Field();
        $fieldRequestContactsEmail->code = 'Email';
        $fieldRequestContactsEmail->tab_id = $tabContentRequestContacts->id;
        $fieldRequestContactsEmail->field = 'email';
        $fieldRequestContactsEmail->data_type = 1;
        $fieldRequestContactsEmail->created_id = $idUser;
        $fieldRequestContactsEmail->save();

        $fieldLanguageRequestContactsEmail = new FieldLanguage();
        $fieldLanguageRequestContactsEmail->field_id = $fieldRequestContactsEmail->id;
        $fieldLanguageRequestContactsEmail->language_id = $idLanguage;
        $fieldLanguageRequestContactsEmail->description = 'Email';
        $fieldLanguageRequestContactsEmail->created_id = $idUser;
        $fieldLanguageRequestContactsEmail->save();

        $fieldUserRoleRequestContactsEmail = new FieldUserRole();
        $fieldUserRoleRequestContactsEmail->field_id = $fieldRequestContactsEmail->id;
        $fieldUserRoleRequestContactsEmail->role_id = $idRole;
        $fieldUserRoleRequestContactsEmail->enabled = true;
        $fieldUserRoleRequestContactsEmail->input_type = 15;
        $fieldUserRoleRequestContactsEmail->table_order = 30;
        $fieldUserRoleRequestContactsEmail->created_id = $idUser;
        $fieldUserRoleRequestContactsEmail->save();

        #endregion EMAIL

        #region EMAILTOSEND

        $fieldRequestContactsEmailToSend = new Field();
        $fieldRequestContactsEmailToSend->code = 'EmailToSend';
        $fieldRequestContactsEmailToSend->tab_id = $tabContentRequestContacts->id;
        $fieldRequestContactsEmailToSend->field = 'emailToSend';
        $fieldRequestContactsEmailToSend->data_type = 1;
        $fieldRequestContactsEmailToSend->required = false;
        $fieldRequestContactsEmailToSend->created_id = $idUser;
        $fieldRequestContactsEmailToSend->save();

        $fieldLanguageRequestContactsEmailToSend = new FieldLanguage();
        $fieldLanguageRequestContactsEmailToSend->field_id = $fieldRequestContactsEmailToSend->id;
        $fieldLanguageRequestContactsEmailToSend->language_id = $idLanguage;
        $fieldLanguageRequestContactsEmailToSend->description = 'Email di destinazione';
        $fieldLanguageRequestContactsEmailToSend->created_id = $idUser;
        $fieldLanguageRequestContactsEmailToSend->save();

        $fieldUserRoleRequestContactsEmailToSend = new FieldUserRole();
        $fieldUserRoleRequestContactsEmailToSend->field_id = $fieldRequestContactsEmailToSend->id;
        $fieldUserRoleRequestContactsEmailToSend->role_id = $idRole;
        $fieldUserRoleRequestContactsEmailToSend->enabled = true;
        $fieldUserRoleRequestContactsEmailToSend->required = false;
        $fieldUserRoleRequestContactsEmailToSend->input_type = 15;
        $fieldUserRoleRequestContactsEmailToSend->table_order = 60;
        $fieldUserRoleRequestContactsEmailToSend->created_id = $idUser;
        $fieldUserRoleRequestContactsEmailToSend->save();

        #endregion EMAILTOSEND

        #region TELEFONO

        $fieldRequestContactsPhone = new Field();
        $fieldRequestContactsPhone->code = 'PhoneNumber';
        $fieldRequestContactsPhone->tab_id = $tabContentRequestContacts->id;
        $fieldRequestContactsPhone->field = 'phoneNumber';
        $fieldRequestContactsPhone->data_type = 1;
        $fieldRequestContactsPhone->created_id = $idUser;
        $fieldRequestContactsPhone->save();

        $fieldLanguageRequestContactsPhone = new FieldLanguage();
        $fieldLanguageRequestContactsPhone->field_id = $fieldRequestContactsPhone->id;
        $fieldLanguageRequestContactsPhone->language_id = $idLanguage;
        $fieldLanguageRequestContactsPhone->description = 'Telefono';
        $fieldLanguageRequestContactsPhone->created_id = $idUser;
        $fieldLanguageRequestContactsPhone->save();

        $fieldUserRoleRequestContactsPhone = new FieldUserRole();
        $fieldUserRoleRequestContactsPhone->field_id = $fieldRequestContactsPhone->id;
        $fieldUserRoleRequestContactsPhone->role_id = $idRole;
        $fieldUserRoleRequestContactsPhone->enabled = true;
        $fieldUserRoleRequestContactsPhone->input_type = 15;
        $fieldUserRoleRequestContactsPhone->table_order = 35;
        $fieldUserRoleRequestContactsPhone->created_id = $idUser;
        $fieldUserRoleRequestContactsPhone->save();

        #endregion TELEFONO

        #region DATE

        $fieldRequestContactsDate = new Field();
        $fieldRequestContactsDate->code = 'Date';
        $fieldRequestContactsDate->tab_id = $tabContentRequestContacts->id;
        $fieldRequestContactsDate->field = 'date';
        $fieldRequestContactsDate->data_type = 1;
        $fieldRequestContactsDate->created_id = $idUser;
        $fieldRequestContactsDate->save();

        $fieldLanguageRequestContactsDate = new FieldLanguage();
        $fieldLanguageRequestContactsDate->field_id = $fieldRequestContactsDate->id;
        $fieldLanguageRequestContactsDate->language_id = $idLanguage;
        $fieldLanguageRequestContactsDate->description = 'Data';
        $fieldLanguageRequestContactsDate->created_id = $idUser;
        $fieldLanguageRequestContactsDate->save();

        $fieldUserRoleRequestContactsDate = new FieldUserRole();
        $fieldUserRoleRequestContactsDate->field_id = $fieldRequestContactsDate->id;
        $fieldUserRoleRequestContactsDate->role_id = $idRole;
        $fieldUserRoleRequestContactsDate->enabled = true;
        $fieldUserRoleRequestContactsDate->input_type = 15;
        $fieldUserRoleRequestContactsDate->table_order = 40;
        $fieldUserRoleRequestContactsDate->created_id = $idUser;
        $fieldUserRoleRequestContactsDate->save();

        #endregion DATE

        #region DESCRIPTION

        $fieldRequestContactsDescription = new Field();
        $fieldRequestContactsDescription->code = 'Description';
        $fieldRequestContactsDescription->tab_id = $tabContentRequestContacts->id;
        $fieldRequestContactsDescription->field = 'description';
        $fieldRequestContactsDescription->data_type = 1;
        $fieldRequestContactsDescription->created_id = $idUser;
        $fieldRequestContactsDescription->save();

        $fieldLanguageRequestContactsDescription = new FieldLanguage();
        $fieldLanguageRequestContactsDescription->field_id = $fieldRequestContactsDescription->id;
        $fieldLanguageRequestContactsDescription->language_id = $idLanguage;
        $fieldLanguageRequestContactsDescription->description = 'Descrizione';
        $fieldLanguageRequestContactsDescription->created_id = $idUser;
        $fieldLanguageRequestContactsDescription->save();

        $fieldUserRoleRequestContactsDescription = new FieldUserRole();
        $fieldUserRoleRequestContactsDescription->field_id = $fieldRequestContactsDescription->id;
        $fieldUserRoleRequestContactsDescription->role_id = $idRole;
        $fieldUserRoleRequestContactsDescription->enabled = true;
        $fieldUserRoleRequestContactsDescription->input_type = 15;
        $fieldUserRoleRequestContactsDescription->table_order = 50;
        $fieldUserRoleRequestContactsDescription->created_id = $idUser;
        $fieldUserRoleRequestContactsDescription->save();

        #endregion DESCRIPTION

        #endregion TABLE

        #endregion REQUEST CONTACT

        #region NATIONS

        #region FUNCTIONALITY

        $functionalityNations = new Functionality();
        $functionalityNations->code = 'Nations';
        $functionalityNations->created_id = $idUser;
        $functionalityNations->save();

        #endregion FUNCTIONALITY        

        #region ICON

        $iconFlag = new Icon();
        $iconFlag->description = 'Flag';
        $iconFlag->css = 'fa fa-flag';
        $iconFlag->created_id = $idUser;
        $iconFlag->save();

        #endregion ICON

        #region MENU

        $menuAdminNations = new MenuAdmin();
        $menuAdminNations->code = 'Nations';
        $menuAdminNations->functionality_id = $functionalityNations->id;
        $menuAdminNations->icon_id = $iconFlag->id;
        $menuAdminNations->url = '/admin/crm/nations';
        $menuAdminNations->created_id = $idUser;
        $menuAdminNations->save();

        $languageMenuAdminNations = new LanguageMenuAdmin();
        $languageMenuAdminNations->language_id = $idLanguage;
        $languageMenuAdminNations->menu_admin_id = $menuAdminNations->id;
        $languageMenuAdminNations->description = 'Nazioni';
        $languageMenuAdminNations->created_id = $idUser;
        $languageMenuAdminNations->save();

        $menuAdminUserRoleNations = new MenuAdminUserRole();
        $menuAdminUserRoleNations->menu_admin_id = $menuAdminNations->id;
        $menuAdminUserRoleNations->menu_admin_father_id = $menuAdminCRM->id;
        $menuAdminUserRoleNations->role_id = $idRole;
        $menuAdminUserRoleNations->enabled = true;
        $menuAdminUserRoleNations->order = 10;
        $menuAdminUserRoleNations->created_id = $idUser;
        $menuAdminUserRoleNations->save();

        #endregion MENU

        #region TAB

        $tabContentNations = new Tab();
        $tabContentNations->code = 'Content';
        $tabContentNations->functionality_id = $functionalityNations->id;
        $tabContentNations->order = 10;
        $tabContentNations->created_id = $idUser;
        $tabContentNations->save();

        $languageTabContentNations = new LanguageTab();
        $languageTabContentNations->language_id = $idLanguage;
        $languageTabContentNations->tab_id = $tabContentNations->id;
        $languageTabContentNations->description = 'Contenuto';
        $languageTabContentNations->created_id = $idUser;
        $languageTabContentNations->save();

        #endregion TAB

        #region TABLE

        #region CHECKBOX

        $fieldContentNationCheckBox = new Field();
        $fieldContentNationCheckBox->tab_id = $tabContentNations->id;
        $fieldContentNationCheckBox->code = 'TableCheckBox';
        $fieldContentNationCheckBox->created_id = $idUser;
        $fieldContentNationCheckBox->save();

        $fieldLanguageContentNationCheckBox = new FieldLanguage();
        $fieldLanguageContentNationCheckBox->field_id = $fieldContentNationCheckBox->id;
        $fieldLanguageContentNationCheckBox->language_id = $idLanguage;
        $fieldLanguageContentNationCheckBox->description = 'CheckBox';
        $fieldLanguageContentNationCheckBox->created_id = $idUser;
        $fieldLanguageContentNationCheckBox->save();

        $fieldUserRoleContentNationCheckBox = new FieldUserRole();
        $fieldUserRoleContentNationCheckBox->field_id = $fieldContentNationCheckBox->id;
        $fieldUserRoleContentNationCheckBox->role_id = $idRole;
        $fieldUserRoleContentNationCheckBox->enabled = true;
        $fieldUserRoleContentNationCheckBox->table_order = 10;
        $fieldUserRoleContentNationCheckBox->input_type = 13;
        $fieldUserRoleContentNationCheckBox->created_id = $idUser;
        $fieldUserRoleContentNationCheckBox->save();

        #endregion CHECKBOX

        #region FORMATTER

        $fieldNationFormatter = new Field();
        $fieldNationFormatter->code = 'NationFormatter';
        $fieldNationFormatter->formatter = 'nationFormatter';
        $fieldNationFormatter->tab_id = $tabContentNations->id;
        $fieldNationFormatter->created_id = $idUser;
        $fieldNationFormatter->save();

        $fieldLanguageNationFormatter = new FieldLanguage();
        $fieldLanguageNationFormatter->field_id = $fieldNationFormatter->id;
        $fieldLanguageNationFormatter->language_id = $idLanguage;
        $fieldLanguageNationFormatter->description = '';
        $fieldLanguageNationFormatter->created_id = $idUser;
        $fieldLanguageNationFormatter->save();

        $fieldUserRoleNationFormatter = new FieldUserRole();
        $fieldUserRoleNationFormatter->field_id = $fieldNationFormatter->id;
        $fieldUserRoleNationFormatter->role_id = $idRole;
        $fieldUserRoleNationFormatter->enabled = true;
        $fieldUserRoleNationFormatter->table_order = 20;
        $fieldUserRoleNationFormatter->input_type = 14;
        $fieldUserRoleNationFormatter->created_id = $idUser;
        $fieldUserRoleNationFormatter->save();

        #endregion FORMATTER

        #region CEE

        $fieldNationsCee = new Field();
        $fieldNationsCee->code = 'Cee';
        $fieldNationsCee->tab_id = $tabContentNations->id;
        $fieldNationsCee->field = 'cee';
        $fieldNationsCee->data_type = 3;
        $fieldNationsCee->created_id = $idUser;
        $fieldNationsCee->save();

        $fieldLanguageNationsCee = new FieldLanguage();
        $fieldLanguageNationsCee->field_id = $fieldNationsCee->id;
        $fieldLanguageNationsCee->language_id = $idLanguage;
        $fieldLanguageNationsCee->description = 'Cee';
        $fieldLanguageNationsCee->created_id = $idUser;
        $fieldLanguageNationsCee->save();

        $fieldUserRoleNationsCee = new FieldUserRole();
        $fieldUserRoleNationsCee->field_id = $fieldNationsCee->id;
        $fieldUserRoleNationsCee->role_id = $idRole;
        $fieldUserRoleNationsCee->enabled = true;
        $fieldUserRoleNationsCee->input_type = 5;
        $fieldUserRoleNationsCee->pos_x = 10;
        $fieldUserRoleNationsCee->pos_y = 10;
        $fieldUserRoleNationsCee->table_order = 10;
        $fieldUserRoleNationsCee->created_id = $idUser;
        $fieldUserRoleNationsCee->save();

        #endregion CEE

        #region ABBREVIATION

        $fieldNationsAbbrevation = new Field();
        $fieldNationsAbbrevation->code = 'Abbreviation';
        $fieldNationsAbbrevation->tab_id = $tabContentNations->id;
        $fieldNationsAbbrevation->field = 'abbreviation';
        $fieldNationsAbbrevation->data_type = 1;
        $fieldNationsAbbrevation->created_id = $idUser;
        $fieldNationsAbbrevation->save();

        $fieldLanguageNationsAbbrevation = new FieldLanguage();
        $fieldLanguageNationsAbbrevation->field_id = $fieldNationsAbbrevation->id;
        $fieldLanguageNationsAbbrevation->language_id = $idLanguage;
        $fieldLanguageNationsAbbrevation->description = 'Abbreviazione';
        $fieldLanguageNationsAbbrevation->created_id = $idUser;
        $fieldLanguageNationsAbbrevation->save();

        $fieldUserRoleNationsAbbrevations = new FieldUserRole();
        $fieldUserRoleNationsAbbrevations->field_id = $fieldLanguageNationsAbbrevation->id;
        $fieldUserRoleNationsAbbrevations->role_id = $idRole;
        $fieldUserRoleNationsAbbrevations->enabled = true;
        $fieldUserRoleNationsAbbrevations->input_type = 0;
        $fieldUserRoleNationsAbbrevations->pos_x = 20;
        $fieldUserRoleNationsAbbrevations->pos_y = 10;
        $fieldUserRoleNationsAbbrevations->table_order = 20;
        $fieldUserRoleNationsAbbrevations->created_id = $idUser;
        $fieldUserRoleNationsAbbrevations->save();

        #endregion ABBREVATION

        #region DESCRIPTION

        $fieldNationsDescription = new Field();
        $fieldNationsDescription->code = 'Description';
        $fieldNationsDescription->tab_id = $tabContentNations->id;
        $fieldNationsDescription->field = 'description';
        $fieldNationsDescription->data_type = 1;
        $fieldNationsDescription->created_id = $idUser;
        $fieldNationsDescription->save();

        $fieldLanguageNationsDescription = new FieldLanguage();
        $fieldLanguageNationsDescription->field_id = $fieldNationsDescription->id;
        $fieldLanguageNationsDescription->language_id = $idLanguage;
        $fieldLanguageNationsDescription->description = 'Descrizione';
        $fieldLanguageNationsDescription->created_id = $idUser;
        $fieldLanguageNationsDescription->save();

        $fieldUserRoleNationsDescription = new FieldUserRole();
        $fieldUserRoleNationsDescription->field_id = $fieldNationsDescription->id;
        $fieldUserRoleNationsDescription->role_id = $idRole;
        $fieldUserRoleNationsDescription->enabled = true;
        $fieldUserRoleNationsDescription->input_type = 0;
        $fieldUserRoleNationsDescription->pos_x = 30;
        $fieldUserRoleNationsDescription->pos_y = 10;
        $fieldUserRoleNationsDescription->table_order = 20;
        $fieldUserRoleNationsDescription->created_id = $idUser;
        $fieldUserRoleNationsDescription->save();

        #endregion DESCRIPTION

        #endregion TABLE

        #endregion NATIONS
    }
}

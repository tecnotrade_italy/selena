<?php

use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class OptionalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $idMenuAdminCRM = MenuAdmin::where('code', 'Setting')->first()->id;
        $idFunctionality = Functionality::where('code', 'Optional')->first()->id;
        
        #region OPTIONAL

        #region TABLE

        #region TAB

        $tabContactTable = new Tab();
        $tabContactTable->code = 'Table';
        $tabContactTable->functionality_id = $idFunctionality;
        $tabContactTable->order = 10;
        $tabContactTable->created_id = $idUser;
        $tabContactTable->save();

        $languageTabContactTable = new LanguageTab();
        $languageTabContactTable->language_id = $idLanguage;
        $languageTabContactTable->tab_id = $tabContactTable->id;
        $languageTabContactTable->description = 'Table';
        $languageTabContactTable->created_id = $idUser;
        $languageTabContactTable->save();

        #endregion TAB

        #region CHECKBOX

        $fieldContactCheckBox = new Field();
        $fieldContactCheckBox->tab_id = $tabContactTable->id;
        $fieldContactCheckBox->code = 'TableCheckBox';
        $fieldContactCheckBox->created_id = $idUser;
        $fieldContactCheckBox->save();

        $fieldLanguageContactCheckBox = new FieldLanguage();
        $fieldLanguageContactCheckBox->field_id = $fieldContactCheckBox->id;
        $fieldLanguageContactCheckBox->language_id = $idLanguage;
        $fieldLanguageContactCheckBox->description = 'CheckBox';
        $fieldLanguageContactCheckBox->created_id = $idUser;
        $fieldLanguageContactCheckBox->save();

        $fieldUserRoleContactCheckBox = new FieldUserRole();
        $fieldUserRoleContactCheckBox->field_id = $fieldContactCheckBox->id;
        $fieldUserRoleContactCheckBox->role_id = $idRole;
        $fieldUserRoleContactCheckBox->enabled = true;
        $fieldUserRoleContactCheckBox->table_order = 10;
        $fieldUserRoleContactCheckBox->input_type = 13;
        $fieldUserRoleContactCheckBox->created_id = $idUser;
        $fieldUserRoleContactCheckBox->save();

        #endregion CHECKBOX

        #region FORMATTER

        $fieldContactFormatter = new Field();
        $fieldContactFormatter->code = 'OptionalFormatter';
        $fieldContactFormatter->formatter = 'OptionalFormatter';
        $fieldContactFormatter->tab_id = $tabContactTable->id;
        $fieldContactFormatter->created_id = $idUser;
        $fieldContactFormatter->save();

        $fieldLanguageContactFormatter = new FieldLanguage();
        $fieldLanguageContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldLanguageContactFormatter->language_id = $idLanguage;
        $fieldLanguageContactFormatter->description = '';
        $fieldLanguageContactFormatter->created_id = $idUser;
        $fieldLanguageContactFormatter->save();

        $fieldUserRoleContactFormatter = new FieldUserRole();
        $fieldUserRoleContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldUserRoleContactFormatter->role_id = $idRole;
        $fieldUserRoleContactFormatter->enabled = true;
        $fieldUserRoleContactFormatter->table_order = 20;
        $fieldUserRoleContactFormatter->input_type = 14;
        $fieldUserRoleContactFormatter->created_id = $idUser;
        $fieldUserRoleContactFormatter->save();

        #endregion FORMATTER

        #region DESCRITPION

        $fieldContactTableCompany = new Field();
        $fieldContactTableCompany->code = 'Description';
        $fieldContactTableCompany->tab_id = $tabContactTable->id;
        $fieldContactTableCompany->field = 'description';
        $fieldContactTableCompany->data_type = 1;
        $fieldContactTableCompany->created_id = $idUser;
        $fieldContactTableCompany->save();

        $fieldLanguageContactTableCompany = new FieldLanguage();
        $fieldLanguageContactTableCompany->field_id = $fieldContactTableCompany->id;
        $fieldLanguageContactTableCompany->language_id = $idLanguage;
        $fieldLanguageContactTableCompany->description = 'Descrizione';
        $fieldLanguageContactTableCompany->created_id = $idUser;
        $fieldLanguageContactTableCompany->save();

        $fieldUserRoleContactTableCompany = new FieldUserRole();
        $fieldUserRoleContactTableCompany->field_id = $fieldContactTableCompany->id;
        $fieldUserRoleContactTableCompany->role_id = $idRole;
        $fieldUserRoleContactTableCompany->enabled = true;
        $fieldUserRoleContactTableCompany->table_order = 30;
        $fieldUserRoleContactTableCompany->input_type = 0;
        $fieldUserRoleContactTableCompany->created_id = $idUser;
        $fieldUserRoleContactTableCompany->save();

        #endregion DESCRITPION

        #region TYPE OPTIONAL

        $fieldContactTableCompany = new Field();
        $fieldContactTableCompany->code = 'TypeOptional';
        $fieldContactTableCompany->tab_id = $tabContactTable->id;
        $fieldContactTableCompany->field = 'typeOptional';
        $fieldContactTableCompany->data_type = 1;
        $fieldContactTableCompany->created_id = $idUser;
        $fieldContactTableCompany->save();

        $fieldLanguageContactTableCompany = new FieldLanguage();
        $fieldLanguageContactTableCompany->field_id = $fieldContactTableCompany->id;
        $fieldLanguageContactTableCompany->language_id = $idLanguage;
        $fieldLanguageContactTableCompany->description = 'Tipo di optional';
        $fieldLanguageContactTableCompany->created_id = $idUser;
        $fieldLanguageContactTableCompany->save();

        $fieldUserRoleContactTableCompany = new FieldUserRole();
        $fieldUserRoleContactTableCompany->field_id = $fieldContactTableCompany->id;
        $fieldUserRoleContactTableCompany->role_id = $idRole;
        $fieldUserRoleContactTableCompany->enabled = true;
        $fieldUserRoleContactTableCompany->table_order = 40;
        $fieldUserRoleContactTableCompany->input_type = 0;
        $fieldUserRoleContactTableCompany->created_id = $idUser;
        $fieldUserRoleContactTableCompany->save();

        #endregion TYPE OPTIONAL

        #region ORDER

        $fieldContactTableBusinessName = new Field();
        $fieldContactTableBusinessName->code = 'Order';
        $fieldContactTableBusinessName->tab_id = $tabContactTable->id;
        $fieldContactTableBusinessName->field = 'order';
        $fieldContactTableBusinessName->data_type = 1;
        $fieldContactTableBusinessName->created_id = $idUser;
        $fieldContactTableBusinessName->save();

        $fieldLanguageContactTableBusinessName = new FieldLanguage();
        $fieldLanguageContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldLanguageContactTableBusinessName->language_id = $idLanguage;
        $fieldLanguageContactTableBusinessName->description = 'Ordinamento';
        $fieldLanguageContactTableBusinessName->created_id = $idUser;
        $fieldLanguageContactTableBusinessName->save();

        $fieldUserRoleContactTableBusinessName = new FieldUserRole();
        $fieldUserRoleContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldUserRoleContactTableBusinessName->role_id = $idRole;
        $fieldUserRoleContactTableBusinessName->enabled = true;
        $fieldUserRoleContactTableBusinessName->table_order = 50;
        $fieldUserRoleContactTableBusinessName->input_type = 0;
        $fieldUserRoleContactTableBusinessName->created_id = $idUser;
        $fieldUserRoleContactTableBusinessName->save();

        #endregion ORDER

        #endregion TABLE


        #region DETAIL

        #region TAB

        $tabContactDetail = new Tab();
        $tabContactDetail->code = 'Detail';
        $tabContactDetail->functionality_id = $idFunctionality;
        $tabContactDetail->order = 20;
        $tabContactDetail->created_id = $idUser;
        $tabContactDetail->save();

        $languageTabContactDetail = new LanguageTab();
        $languageTabContactDetail->language_id = $idLanguage;
        $languageTabContactDetail->tab_id = $tabContactDetail->id;
        $languageTabContactDetail->description = 'Detail';
        $languageTabContactDetail->created_id = $idUser;
        $languageTabContactDetail->save();

        #endregion TAB

        #region DESCRIPTION

        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'Description';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'description';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Descrizione';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->required = true;
        $fieldUserRoleContactDetailCompany->pos_x = 10;
        $fieldUserRoleContactDetailCompany->pos_y = 10;
        $fieldUserRoleContactDetailCompany->input_type = 0;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();

        #endregion DESCRIPTION

        #region TYPE OPTIONAL

        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'TypeOptionalId';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'typeOptionalId';
        $fieldContactDetailCompany->service = 'typeOptional';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Tipo di optional';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->required = true;
        $fieldUserRoleContactDetailCompany->pos_x = 20;
        $fieldUserRoleContactDetailCompany->pos_y = 10;
        $fieldUserRoleContactDetailCompany->input_type = 2;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();

        #endregion TYPE OPTIONAL

        #region ORDER

        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'Order';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'order';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Ordinamento';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 30;
        $fieldUserRoleSurnameContactDetail->pos_y = 10;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();

        #endregion ORDER

        #endregion DETAIL

        #endregion OPTIONAL
    }
}

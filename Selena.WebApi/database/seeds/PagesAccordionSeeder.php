<?php

use App\Enums\PageTypesEnum;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Icon;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Tab;
use Illuminate\Database\Seeder;
use App\Language;
use App\Role;
use App\User;
use App\Functionality;

class PagesAccordionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $idFunctionality = Functionality::where('code', '=', 'Pages')->first()->id;

        #region PAGES

        /*$functionality = new Functionality();
        $functionality->code = 'Pages';
        $functionality->created_id = $idUser;
        $functionality->save();*/

        $icon = new Icon();
        $icon->description = 'List';
        $icon->css = 'fa fa-list-alt';
        $icon->created_id = $idUser;
        $icon->save();

        #region tab
        $tabContent = new Tab();
        $tabContent->code = 'Content';
        $tabContent->functionality_id = $idFunctionality;
        $tabContent->order = 10;
        $tabContent->created_id = $idUser;
        $tabContent->save();

        $languageTabContent = new LanguageTab();
        $languageTabContent->language_id = $idLanguage;
        $languageTabContent->tab_id = $tabContent->id;
        $languageTabContent->description = 'Contenuto';
        $languageTabContent->created_id = $idUser;
        $languageTabContent->save();

        $fieldContent = new Field();
        $fieldContent->code = 'Content';
        $fieldContent->tab_id = $tabContent->id;
        $fieldContent->field = 'languagePageSetting.content';
        $fieldContent->data_type = 1;
        $fieldContent->required = true;
        $fieldContent->created_id = $idUser;
        $fieldContent->save();

        $fieldLanguageContent = new FieldLanguage();
        $fieldLanguageContent->field_id = $fieldContent->id;
        $fieldLanguageContent->language_id = $idLanguage;
        $fieldLanguageContent->description = 'Contenuto';
        $fieldLanguageContent->created_id = $idUser;
        $fieldLanguageContent->save();

        $fieldUserRoleContent = new FieldUserRole();
        $fieldUserRoleContent->field_id = $fieldContent->id;
        $fieldUserRoleContent->role_id = $idRole;
        $fieldUserRoleContent->enabled = true;
        $fieldUserRoleContent->input_type = 1;
        $fieldUserRoleContent->pos_x = 10;
        $fieldUserRoleContent->pos_y = 10;
        $fieldUserRoleContent->colspan = 12;
        $fieldUserRoleContent->created_id = $idUser;
        $fieldUserRoleContent->save();
        #endregion Content

        $tabAttribute = new Tab();
        $tabAttribute->code = 'TabAttribute';
        $tabAttribute->functionality_id = $idFunctionality;
        $tabAttribute->order = 20;
        $tabAttribute->created_id = $idUser;
        $tabAttribute->save();

        $languageTabAttribute = new LanguageTab();
        $languageTabAttribute->language_id = $idLanguage;
        $languageTabAttribute->tab_id = $tabAttribute->id;
        $languageTabAttribute->description = 'TabAttributi';
        $languageTabAttribute->created_id = $idUser;
        $languageTabAttribute->save();
        #end region TABS

        #region Title
        $fieldTitlePage = new Field();
        $fieldTitlePage->code = 'TitlePage';
        $fieldTitlePage->tab_id = $tabAttribute->id;
        $fieldTitlePage->field = 'languagePageSetting.title';
        $fieldTitlePage->data_type = 1;
        $fieldTitlePage->required = true;
        $fieldTitlePage->max_length = 255;
        $fieldTitlePage->on_change = 'changeTitle';
        $fieldTitlePage->created_id = $idUser;
        $fieldTitlePage->save();

        $fieldLanguageTitlePage = new FieldLanguage();
        $fieldLanguageTitlePage->field_id = $fieldTitlePage->id;
        $fieldLanguageTitlePage->language_id = $idLanguage;
        $fieldLanguageTitlePage->description = 'Titolo';
        $fieldLanguageTitlePage->created_id = $idUser;
        $fieldLanguageTitlePage->save();

        $fieldUserRoleTitlePage = new FieldUserRole();
        $fieldUserRoleTitlePage->field_id = $fieldTitlePage->id;
        $fieldUserRoleTitlePage->role_id = $idRole;
        $fieldUserRoleTitlePage->enabled = true;
        $fieldUserRoleTitlePage->input_type = 0;
        $fieldUserRoleTitlePage->pos_x = 10;
        $fieldUserRoleTitlePage->pos_y = 10;
        $fieldUserRoleTitlePage->required = true;
        $fieldUserRoleTitlePage->colspan = 12;
        $fieldUserRoleTitlePage->created_id = $idUser;
        $fieldUserRoleTitlePage->save();
        #endregion Title

        #region SeoDescription
        $fieldSeoDescription = new Field();
        $fieldSeoDescription->code = 'SeoDescription';
        $fieldSeoDescription->tab_id = $tabAttribute->id;
        $fieldSeoDescription->field = 'languagePageSetting.seoDescription';
        $fieldSeoDescription->data_type = 1;
        $fieldSeoDescription->max_length = 230;
        $fieldSeoDescription->created_id = $idUser;
        $fieldSeoDescription->save();

        $fieldLanguageSeoDescription = new FieldLanguage();
        $fieldLanguageSeoDescription->field_id = $fieldSeoDescription->id;
        $fieldLanguageSeoDescription->language_id = $idLanguage;
        $fieldLanguageSeoDescription->description = 'Descrizione SEO';
        $fieldLanguageSeoDescription->created_id = $idUser;
        $fieldLanguageSeoDescription->save();

        $fieldUserRoleSeoDescription = new FieldUserRole();
        $fieldUserRoleSeoDescription->field_id = $fieldSeoDescription->id;
        $fieldUserRoleSeoDescription->role_id = $idRole;
        $fieldUserRoleSeoDescription->enabled = true;
        $fieldUserRoleSeoDescription->input_type = 0;
        $fieldUserRoleSeoDescription->pos_x = 10;
        $fieldUserRoleSeoDescription->pos_y = 140;
        $fieldUserRoleSeoDescription->colspan = 12;
        $fieldUserRoleSeoDescription->created_id = $idUser;
        $fieldUserRoleSeoDescription->save();
        #endregion SeoDescription

        #region Url
        $fieldUrl = new Field();
        $fieldUrl->code = 'Url';
        $fieldUrl->tab_id = $tabAttribute->id;
        $fieldUrl->field = 'languagePageSetting.url';
        $fieldUrl->data_type = 1;
        $fieldUrl->max_length = 255;
        $fieldUrl->on_change = 'checkUrl';
        $fieldUrl->created_id = $idUser;
        $fieldUrl->save();

        $fieldLanguageUrl = new FieldLanguage();
        $fieldLanguageUrl->field_id = $fieldUrl->id;
        $fieldLanguageUrl->language_id = $idLanguage;
        $fieldLanguageUrl->description = 'Url';
        $fieldLanguageUrl->created_id = $idUser;
        $fieldLanguageUrl->save();

        $fieldUserRoleUrl = new FieldUserRole();
        $fieldUserRoleUrl->field_id = $fieldUrl->id;
        $fieldUserRoleUrl->role_id = $idRole;
        $fieldUserRoleUrl->enabled = true;
        $fieldUserRoleUrl->input_type = 0;
        $fieldUserRoleUrl->pos_x = 10;
        $fieldUserRoleUrl->pos_y = 30;
        $fieldUserRoleUrl->colspan = 12;
        $fieldUserRoleUrl->created_id = $idUser;
        $fieldUserRoleUrl->save();
        #endregion Url

        #region UrlCanonical
        $fieldUrl = new Field();
        $fieldUrl->code = 'UrlCanonical';
        $fieldUrl->tab_id = $tabAttribute->id;
        $fieldUrl->field = 'languagePageSetting.urlCanonical';
        $fieldUrl->formatter = 'urlFormatter';
        $fieldUrl->data_type = 1;
        $fieldUrl->max_length = 255;
        $fieldUrl->on_change = 'checkUrl';
        $fieldUrl->created_id = $idUser;
        $fieldUrl->save();

        $fieldLanguageUrl = new FieldLanguage();
        $fieldLanguageUrl->field_id = $fieldUrl->id;
        $fieldLanguageUrl->language_id = $idLanguage;
        $fieldLanguageUrl->description = 'UrlCanonical';
        $fieldLanguageUrl->created_id = $idUser;
        $fieldLanguageUrl->save();

        $fieldUserRoleUrl = new FieldUserRole();
        $fieldUserRoleUrl->field_id = $fieldUrl->id;
        $fieldUserRoleUrl->role_id = $idRole;
        $fieldUserRoleUrl->enabled = true;
        $fieldUserRoleUrl->input_type = 0;
        $fieldUserRoleUrl->pos_x = 10;
        $fieldUserRoleUrl->pos_y = 40;
        $fieldUserRoleUrl->table_order = 40;
        $fieldUserRoleUrl->colspan = 12;
        $fieldUserRoleUrl->created_id = $idUser;
        $fieldUserRoleUrl->save();
        #endregion UrlCanonical

        #region SeoKeyword
        $fieldSeoKeyword = new Field();
        $fieldSeoKeyword->code = 'SeoKeyword';
        $fieldSeoKeyword->tab_id = $tabAttribute->id;
        $fieldSeoKeyword->field = 'languagePageSetting.seoKeyword';
        $fieldSeoKeyword->data_type = 1;
        $fieldSeoKeyword->created_id = $idUser;
        $fieldSeoKeyword->save();

        $fieldLanguageSeoKeyword = new FieldLanguage();
        $fieldLanguageSeoKeyword->field_id = $fieldSeoKeyword->id;
        $fieldLanguageSeoKeyword->language_id = $idLanguage;
        $fieldLanguageSeoKeyword->description = 'Parole chiave SEO';
        $fieldLanguageSeoKeyword->created_id = $idUser;
        $fieldLanguageSeoKeyword->save();

        $fieldUserRoleSeoKeyword = new FieldUserRole();
        $fieldUserRoleSeoKeyword->field_id = $fieldSeoKeyword->id;
        $fieldUserRoleSeoKeyword->role_id = $idRole;
        $fieldUserRoleSeoKeyword->enabled = true;
        $fieldUserRoleSeoKeyword->input_type = 1;
        $fieldUserRoleSeoKeyword->pos_x = 10;
        $fieldUserRoleSeoKeyword->pos_y = 150;
        $fieldUserRoleSeoKeyword->colspan = 12;
        $fieldUserRoleSeoKeyword->created_id = $idUser;
        $fieldUserRoleSeoKeyword->save();
        #endregion SeoKeyword

        #region TABSPubblicazione
        $tabPublication = new Tab();
        $tabPublication->code = 'TabPublication';
        $tabPublication->functionality_id = $idFunctionality;
        $tabPublication->order = 20;
        $tabPublication->created_id = $idUser;
        $tabPublication->save();

        $languageTabPublication = new LanguageTab();
        $languageTabPublication->language_id = $idLanguage;
        $languageTabPublication->tab_id = $tabPublication->id;
        $languageTabPublication->description = 'TabPubblicazione';
        $languageTabPublication->created_id = $idUser;
        $languageTabPublication->save();
        #end region TABSPubblicazione


        #region VisibleFrom
        $fieldVisibleFrom = new Field();
        $fieldVisibleFrom->code = 'VisibleFrom';
        $fieldVisibleFrom->tab_id = $tabPublication->id;
        $fieldVisibleFrom->field = 'visibleFrom';
        $fieldVisibleFrom->data_type = 2;
        $fieldVisibleFrom->created_id = $idUser;
        $fieldVisibleFrom->save();

        $fieldLanguageVisibleFrom = new FieldLanguage();
        $fieldLanguageVisibleFrom->field_id = $fieldVisibleFrom->id;
        $fieldLanguageVisibleFrom->language_id = $idLanguage;
        $fieldLanguageVisibleFrom->description = 'Visibile dal';
        $fieldLanguageVisibleFrom->created_id = $idUser;
        $fieldLanguageVisibleFrom->save();

        $fieldUserRoleVisibleFrom = new FieldUserRole();
        $fieldUserRoleVisibleFrom->field_id = $fieldVisibleFrom->id;
        $fieldUserRoleVisibleFrom->role_id = $idRole;
        $fieldUserRoleVisibleFrom->enabled = true;
        $fieldUserRoleVisibleFrom->input_type = 4;
        $fieldUserRoleVisibleFrom->pos_x = 10;
        $fieldUserRoleVisibleFrom->pos_y = 40;
        $fieldUserRoleVisibleFrom->colspan = 12;
        $fieldUserRoleVisibleFrom->created_id = $idUser;
        $fieldUserRoleVisibleFrom->save();

        #endregion VisibleFrom

        #region VisibleEnd
        $fieldVisibleEnd = new Field();
        $fieldVisibleEnd->code = 'VisibleEnd';
        $fieldVisibleEnd->tab_id = $tabPublication->id;
        $fieldVisibleEnd->field = 'visibleEnd';
        $fieldVisibleEnd->data_type = 2;
        $fieldVisibleEnd->created_id = $idUser;
        $fieldVisibleEnd->save();

        $fieldLanguageVisibleEnd = new FieldLanguage();
        $fieldLanguageVisibleEnd->field_id = $fieldVisibleEnd->id;
        $fieldLanguageVisibleEnd->language_id = $idLanguage;
        $fieldLanguageVisibleEnd->description = 'Fino al';
        $fieldLanguageVisibleEnd->created_id = $idUser;
        $fieldLanguageVisibleEnd->save();

        $fieldUserRoleVisibleEnd = new FieldUserRole();
        $fieldUserRoleVisibleEnd->field_id = $fieldVisibleEnd->id;
        $fieldUserRoleVisibleEnd->role_id = $idRole;
        $fieldUserRoleVisibleEnd->enabled = true;
        $fieldUserRoleVisibleEnd->input_type = 3;
        $fieldUserRoleVisibleEnd->pos_x = 10;
        $fieldUserRoleVisibleEnd->pos_y = 50;
        $fieldUserRoleVisibleEnd->colspan = 12;
        $fieldUserRoleVisibleEnd->created_id = $idUser;
        $fieldUserRoleVisibleEnd->save();
        #endregion VisibleEnd


        #region HomePage
        $fieldHomePage = new Field();
        $fieldHomePage->code = 'HomePage';
        $fieldHomePage->tab_id = $tabPublication->id;
        $fieldHomePage->field = 'homePage';
        $fieldHomePage->data_type = 3;
        $fieldHomePage->default_value = false;
        $fieldHomePage->created_id = $idUser;
        $fieldHomePage->save();

        $fieldLanguageHomePage = new FieldLanguage();
        $fieldLanguageHomePage->field_id = $fieldHomePage->id;
        $fieldLanguageHomePage->language_id = $idLanguage;
        $fieldLanguageHomePage->description = 'Imposta come home page';
        $fieldLanguageHomePage->created_id = $idUser;
        $fieldLanguageHomePage->save();

        $fieldUserRoleHomePage = new FieldUserRole();
        $fieldUserRoleHomePage->field_id = $fieldHomePage->id;
        $fieldUserRoleHomePage->role_id = $idRole;
        $fieldUserRoleHomePage->enabled = true;
        $fieldUserRoleHomePage->input_type = 5;
        $fieldUserRoleHomePage->pos_x = 10;
        $fieldUserRoleHomePage->pos_y = 70;
        $fieldUserRoleHomePage->colspan = 12;
        $fieldUserRoleHomePage->created_id = $idUser;
        $fieldUserRoleHomePage->save();
        #endregion HomePage

        #region HideHeader
        $fieldHideHeader = new Field();
        $fieldHideHeader->code = 'HideHeader';
        $fieldHideHeader->tab_id = $tabPublication->id;
        $fieldHideHeader->field = 'languagePageSetting.hideHeader';
        $fieldHideHeader->data_type = 3;
        $fieldHideHeader->default_value = false;
        $fieldHideHeader->created_id = $idUser;
        $fieldHideHeader->save();

        $fieldLanguageHideHeader = new FieldLanguage();
        $fieldLanguageHideHeader->field_id = $fieldHideHeader->id;
        $fieldLanguageHideHeader->language_id = $idLanguage;
        $fieldLanguageHideHeader->description = 'Nascondi header';
        $fieldLanguageHideHeader->created_id = $idUser;
        $fieldLanguageHideHeader->save();

        $fieldUserRoleHideHeader = new FieldUserRole();
        $fieldUserRoleHideHeader->field_id = $fieldHideHeader->id;
        $fieldUserRoleHideHeader->role_id = $idRole;
        $fieldUserRoleHideHeader->enabled = true;
        $fieldUserRoleHideHeader->input_type = 5;
        $fieldUserRoleHideHeader->pos_x = 10;
        $fieldUserRoleHideHeader->pos_y = 80;
        $fieldUserRoleHideHeader->colspan = 12;
        $fieldUserRoleHideHeader->created_id = $idUser;
        $fieldUserRoleHideHeader->save();
        #endregion HideHeader

        #region HideFooter
        $fieldHideFooter = new Field();
        $fieldHideFooter->code = 'HideFooter';
        $fieldHideFooter->tab_id = $tabPublication->id;
        $fieldHideFooter->field = 'languagePageSetting.hideFooter';
        $fieldHideFooter->data_type = 3;
        $fieldHideFooter->default_value = false;
        $fieldHideFooter->created_id = $idUser;
        $fieldHideFooter->save();

        $fieldLanguageHideFooter = new FieldLanguage();
        $fieldLanguageHideFooter->field_id = $fieldHideFooter->id;
        $fieldLanguageHideFooter->language_id = $idLanguage;
        $fieldLanguageHideFooter->description = 'Nascondi footer';
        $fieldLanguageHideFooter->created_id = $idUser;
        $fieldLanguageHideFooter->save();

        $fieldUserRoleHideFooter = new FieldUserRole();
        $fieldUserRoleHideFooter->field_id = $fieldHideFooter->id;
        $fieldUserRoleHideFooter->role_id = $idRole;
        $fieldUserRoleHideFooter->enabled = true;
        $fieldUserRoleHideFooter->input_type = 5;
        $fieldUserRoleHideFooter->pos_x = 10;
        $fieldUserRoleHideFooter->pos_y = 90;
        $fieldUserRoleHideFooter->colspan = 12;
        $fieldUserRoleHideFooter->created_id = $idUser;
        $fieldUserRoleHideFooter->save();
        #endregion HideFooter

        #region HideBreadcrumb
        $fieldHideBreadcrumbs = new Field();
        $fieldHideBreadcrumbs->code = 'HideBreadcrumb';
        $fieldHideBreadcrumbs->tab_id = $tabPublication->id;
        $fieldHideBreadcrumbs->field = 'languagePageSetting.hideBreadcrumb';
        $fieldHideBreadcrumbs->data_type = 3;
        $fieldHideBreadcrumbs->default_value = false;
        $fieldHideBreadcrumbs->created_id = $idUser;
        $fieldHideBreadcrumbs->save();

        $fieldLanguageHideBreadcrumbs = new FieldLanguage();
        $fieldLanguageHideBreadcrumbs->field_id = $fieldHideBreadcrumbs->id;
        $fieldLanguageHideBreadcrumbs->language_id = $idLanguage;
        $fieldLanguageHideBreadcrumbs->description = 'Nascondi breadcrumbs';
        $fieldLanguageHideBreadcrumbs->created_id = $idUser;
        $fieldLanguageHideBreadcrumbs->save();

        $fieldUserRoleHideBreadcrumbs = new FieldUserRole();
        $fieldUserRoleHideBreadcrumbs->field_id = $fieldHideBreadcrumbs->id;
        $fieldUserRoleHideBreadcrumbs->role_id = $idRole;
        $fieldUserRoleHideBreadcrumbs->enabled = true;
        $fieldUserRoleHideBreadcrumbs->input_type = 5;
        $fieldUserRoleHideBreadcrumbs->pos_x = 10;
        $fieldUserRoleHideBreadcrumbs->pos_y = 100;
        $fieldUserRoleHideBreadcrumbs->colspan = 12;
        $fieldUserRoleHideBreadcrumbs->created_id = $idUser;
        $fieldUserRoleHideBreadcrumbs->save();
        #endregion HideBreadcrumb

        #region HideMenu
        $fieldHideMenu = new Field();
        $fieldHideMenu->code = 'HideMenu';
        $fieldHideMenu->tab_id = $tabPublication->id;
        $fieldHideMenu->field = 'hideMenu';
        $fieldHideMenu->data_type = 3;
        $fieldHideMenu->default_value = false;
        $fieldHideMenu->created_id = $idUser;
        $fieldHideMenu->save();

        $fieldLanguageHideMenu = new FieldLanguage();
        $fieldLanguageHideMenu->field_id = $fieldHideMenu->id;
        $fieldLanguageHideMenu->language_id = $idLanguage;
        $fieldLanguageHideMenu->description = 'Nascondi menu';
        $fieldLanguageHideMenu->created_id = $idUser;
        $fieldLanguageHideMenu->save();

        $fieldUserRoleHideMenu = new FieldUserRole();
        $fieldUserRoleHideMenu->field_id = $fieldHideMenu->id;
        $fieldUserRoleHideMenu->role_id = $idRole;
        $fieldUserRoleHideMenu->enabled = true;
        $fieldUserRoleHideMenu->input_type = 5;
        $fieldUserRoleHideMenu->pos_x = 10;
        $fieldUserRoleHideMenu->pos_y = 110;
        $fieldUserRoleHideMenu->colspan = 12;
        $fieldUserRoleHideMenu->created_id = $idUser;
        $fieldUserRoleHideMenu->save();
        #endregion HideMenu

        #region HideMobileMenu
        $fieldHideMobileMenu = new Field();
        $fieldHideMobileMenu->code = 'HideMobileMenu';
        $fieldHideMobileMenu->tab_id = $tabPublication->id;
        $fieldHideMobileMenu->field = 'hideMenuMobile';
        $fieldHideMobileMenu->data_type = 3;
        $fieldHideMobileMenu->default_value = false;
        $fieldHideMobileMenu->created_id = $idUser;
        $fieldHideMobileMenu->save();

        $fieldLanguageHideMobileMenu = new FieldLanguage();
        $fieldLanguageHideMobileMenu->field_id = $fieldHideMobileMenu->id;
        $fieldLanguageHideMobileMenu->language_id = $idLanguage;
        $fieldLanguageHideMobileMenu->description = 'Nascondi menu mobile';
        $fieldLanguageHideMobileMenu->created_id = $idUser;
        $fieldLanguageHideMobileMenu->save();

        $fieldUserRoleHideMobileMenu = new FieldUserRole();
        $fieldUserRoleHideMobileMenu->field_id = $fieldHideMobileMenu->id;
        $fieldUserRoleHideMobileMenu->role_id = $idRole;
        $fieldUserRoleHideMobileMenu->enabled = true;
        $fieldUserRoleHideMobileMenu->input_type = 5;
        $fieldUserRoleHideMobileMenu->pos_x = 10;
        $fieldUserRoleHideMobileMenu->pos_y = 120;
        $fieldUserRoleHideMobileMenu->colspan = 12;
        $fieldUserRoleHideMobileMenu->created_id = $idUser;
        $fieldUserRoleHideMobileMenu->save();
        #endregion HideMobileMenu

        #region HideSearchEngines
        $fieldHideFromSearchEngines = new Field();
        $fieldHideFromSearchEngines->code = 'HideFromSearchEngines';
        $fieldHideFromSearchEngines->tab_id = $tabPublication->id;
        $fieldHideFromSearchEngines->field = 'hideSearchEngine';
        $fieldHideFromSearchEngines->data_type = 3;
        $fieldHideFromSearchEngines->default_value = false;
        $fieldHideFromSearchEngines->created_id = $idUser;
        $fieldHideFromSearchEngines->save();

        $fieldLanguageHideFromSearchEngines = new FieldLanguage();
        $fieldLanguageHideFromSearchEngines->field_id = $fieldHideFromSearchEngines->id;
        $fieldLanguageHideFromSearchEngines->language_id = $idLanguage;
        $fieldLanguageHideFromSearchEngines->description = 'Nascondi dai motori di ricerca';
        $fieldLanguageHideFromSearchEngines->created_id = $idUser;
        $fieldLanguageHideFromSearchEngines->save();

        $fieldUserRoleHideFromSearchEngines = new FieldUserRole();
        $fieldUserRoleHideFromSearchEngines->field_id = $fieldHideFromSearchEngines->id;
        $fieldUserRoleHideFromSearchEngines->role_id = $idRole;
        $fieldUserRoleHideFromSearchEngines->enabled = true;
        $fieldUserRoleHideFromSearchEngines->input_type = 5;
        $fieldUserRoleHideFromSearchEngines->pos_x = 10;
        $fieldUserRoleHideFromSearchEngines->pos_y = 160;
        $fieldUserRoleHideFromSearchEngines->colspan = 12;
        $fieldUserRoleHideFromSearchEngines->created_id = $idUser;
        $fieldUserRoleHideFromSearchEngines->save();
        #endregion HideSearchEngines

        $tabSocialShare = new Tab();
        $tabSocialShare->code = 'TabSocialShare';
        $tabSocialShare->functionality_id = $idFunctionality;
        $tabSocialShare->order = 20;
        $tabSocialShare->created_id = $idUser;
        $tabSocialShare->save();

        $languageTabSocialShare = new LanguageTab();
        $languageTabSocialShare->language_id = $idLanguage;
        $languageTabSocialShare->tab_id = $tabSocialShare->id;
        $languageTabSocialShare->description = 'TabSocialShare';
        $languageTabSocialShare->created_id = $idUser;
        $languageTabSocialShare->save();
        #end region TABSocialShare


        #region ShareTitle
        $fieldShareTitle = new Field();
        $fieldShareTitle->code = 'ShareTitle';
        $fieldShareTitle->tab_id = $tabSocialShare->id;
        $fieldShareTitle->field = 'languagePageSetting.shareTitle';
        $fieldShareTitle->data_type = 1;
        $fieldShareTitle->max_length = 60;
        $fieldShareTitle->created_id = $idUser;
        $fieldShareTitle->save();

        $fieldLanguageShareTitle = new FieldLanguage();
        $fieldLanguageShareTitle->field_id = $fieldShareTitle->id;
        $fieldLanguageShareTitle->language_id = $idLanguage;
        $fieldLanguageShareTitle->description = 'Titolo condivisione';
        $fieldLanguageShareTitle->created_id = $idUser;
        $fieldLanguageShareTitle->save();

        $fieldUserRoleShareTitle = new FieldUserRole();
        $fieldUserRoleShareTitle->field_id = $fieldShareTitle->id;
        $fieldUserRoleShareTitle->role_id = $idRole;
        $fieldUserRoleShareTitle->enabled = true;
        $fieldUserRoleShareTitle->input_type = 0;
        $fieldUserRoleShareTitle->pos_x = 10;
        $fieldUserRoleShareTitle->pos_y = 170;
        $fieldUserRoleShareTitle->colspan = 12;
        $fieldUserRoleShareTitle->created_id = $idUser;
        $fieldUserRoleShareTitle->save();
        #endregion ShareTitle

        #region ShareDescription
        $fieldShareDescription = new Field();
        $fieldShareDescription->code = 'ShareDescription';
        $fieldShareDescription->tab_id = $tabSocialShare->id;
        $fieldShareDescription->field = 'languagePageSetting.shareDescription';
        $fieldShareDescription->data_type = 1;
        $fieldShareDescription->max_length = 230;
        $fieldShareDescription->created_id = $idUser;
        $fieldShareDescription->save();

        $fieldLanguageShareDescription = new FieldLanguage();
        $fieldLanguageShareDescription->field_id = $fieldShareDescription->id;
        $fieldLanguageShareDescription->language_id = $idLanguage;
        $fieldLanguageShareDescription->description = 'Descrizione condivisione';
        $fieldLanguageShareDescription->created_id = $idUser;
        $fieldLanguageShareDescription->save();

        $fieldUserRoleShareDescription = new FieldUserRole();
        $fieldUserRoleShareDescription->field_id = $fieldShareDescription->id;
        $fieldUserRoleShareDescription->role_id = $idRole;
        $fieldUserRoleShareDescription->enabled = true;
        $fieldUserRoleShareDescription->input_type = 0;
        $fieldUserRoleShareDescription->pos_x = 10;
        $fieldUserRoleShareDescription->pos_y = 180;
        $fieldUserRoleShareDescription->colspan = 12;
        $fieldUserRoleShareDescription->created_id = $idUser;
        $fieldUserRoleShareDescription->save();
        #endregion ShareDescription

        #region UrlCoverImage
        $fieldCoverImage = new Field();
        $fieldCoverImage->code = 'CoverImage';
        $fieldCoverImage->tab_id = $tabSocialShare->id;
        $fieldCoverImage->field = 'languagePageSetting.urlCoverImage';
        $fieldCoverImage->data_type = 1;
        $fieldCoverImage->max_length = 255;
        $fieldCoverImage->created_id = $idUser;
        $fieldCoverImage->save();

        $fieldLanguageCoverImage = new FieldLanguage();
        $fieldLanguageCoverImage->field_id = $fieldCoverImage->id;
        $fieldLanguageCoverImage->language_id = $idLanguage;
        $fieldLanguageCoverImage->description = 'Url immagine condivisione';
        $fieldLanguageCoverImage->created_id = $idUser;
        $fieldLanguageCoverImage->save();

        $fieldUserRoleCoverImage = new FieldUserRole();
        $fieldUserRoleCoverImage->field_id = $fieldCoverImage->id;
        $fieldUserRoleCoverImage->role_id = $idRole;
        $fieldUserRoleCoverImage->enabled = true;
        $fieldUserRoleCoverImage->input_type = 0;
        $fieldUserRoleCoverImage->pos_x = 10;
        $fieldUserRoleCoverImage->pos_y = 150;
        $fieldUserRoleCoverImage->colspan = 12;
        $fieldUserRoleCoverImage->created_id = $idUser;
        $fieldUserRoleCoverImage->save();
        #endregion UrlCoverImage

        $tabSettings = new Tab();
        $tabSettings->code = 'Settings';
        $tabSettings->functionality_id = $idFunctionality;
        $tabSettings->order = 20;
        $tabSettings->created_id = $idUser;
        $tabSettings->save();

        $languageTabSettings = new LanguageTab();
        $languageTabSettings->language_id = $idLanguage;
        $languageTabSettings->tab_id = $tabSettings->id;
        $languageTabSettings->description = 'Impostazioni';
        $languageTabSettings->created_id = $idUser;
        $languageTabSettings->save();

        #region IdLanguage
        $fieldIdLanguage = new Field();
        $fieldIdLanguage->code = 'IdLanguage';
        $fieldIdLanguage->tab_id = $tabSettings->id;
        $fieldIdLanguage->field = 'languagePageSetting.idLanguage';
        $fieldIdLanguage->data_type = 0;
        $fieldIdLanguage->created_id = $idUser;
        $fieldIdLanguage->save();

        $fieldLanguageIdLanguage = new FieldLanguage();
        $fieldLanguageIdLanguage->field_id = $fieldIdLanguage->id;
        $fieldLanguageIdLanguage->language_id = $idLanguage;
        $fieldLanguageIdLanguage->description = 'Lingua';
        $fieldLanguageIdLanguage->created_id = $idUser;
        $fieldLanguageIdLanguage->save();

        $fieldUserRoleIdLanguage = new FieldUserRole();
        $fieldUserRoleIdLanguage->field_id = $fieldIdLanguage->id;
        $fieldUserRoleIdLanguage->role_id = $idRole;
        $fieldUserRoleIdLanguage->enabled = true;
        $fieldUserRoleIdLanguage->input_type = 19;
        $fieldUserRoleIdLanguage->pos_x = 10;
        $fieldUserRoleIdLanguage->pos_y = 10;
        $fieldUserRoleIdLanguage->created_id = $idUser;
        $fieldUserRoleIdLanguage->save();
        #endregion IdLanguage

        #region CommandNews
        $fieldCommandNews = new Field();
        $fieldCommandNews->code = 'CommandTable';
        $fieldCommandNews->tab_id = $tabSettings->id;
        $fieldCommandNews->formatter = 'tableFormatter';
        $fieldCommandNews->created_id = $idUser;
        $fieldCommandNews->save();

        $fieldLanguageCommandNews = new FieldLanguage();
        $fieldLanguageCommandNews->field_id = $fieldCommandNews->id;
        $fieldLanguageCommandNews->language_id = $idLanguage;
        $fieldLanguageCommandNews->description = 'Lingua';
        $fieldLanguageCommandNews->created_id = $idUser;
        $fieldLanguageCommandNews->save();

        $fieldUserRoleCommandNews = new FieldUserRole();
        $fieldUserRoleCommandNews->field_id = $fieldCommandNews->id;
        $fieldUserRoleCommandNews->role_id = $idRole;
        $fieldUserRoleCommandNews->enabled = true;
        $fieldUserRoleCommandNews->table_order = 10;
        $fieldUserRoleCommandNews->input_type = 14;
        $fieldUserRoleCommandNews->created_id = $idUser;
        $fieldUserRoleCommandNews->save();
        #endregion CommandNews

        $tabFunctionality = new Tab();
        $tabFunctionality->code = 'TabFunctionality';
        $tabFunctionality->functionality_id = $idFunctionality;
        $tabFunctionality->order = 20;
        $tabFunctionality->created_id = $idUser;
        $tabFunctionality->save();

        $languageTabFunctionality = new LanguageTab();
        $languageTabFunctionality->language_id = $idLanguage;
        $languageTabFunctionality->tab_id = $tabFunctionality->id;
        $languageTabFunctionality->description = 'TabFunctionality';
        $languageTabFunctionality->created_id = $idUser;
        $languageTabFunctionality->save();
        #end region TabFunctionality

        #region Enable JS
        $fieldEnableJS = new Field();
        $fieldEnableJS->code = 'EnableJS';
        $fieldEnableJS->tab_id = $tabFunctionality->id;
        $fieldEnableJS->field = 'enableJS';
        $fieldEnableJS->data_type = 3;
        $fieldEnableJS->default_value = false;
        $fieldEnableJS->created_id = $idUser;
        $fieldEnableJS->save();

        $fieldLanguageEnableJS = new FieldLanguage();
        $fieldLanguageEnableJS->field_id = $fieldEnableJS->id;
        $fieldLanguageEnableJS->language_id = $idLanguage;
        $fieldLanguageEnableJS->description = 'Abilita Javascript';
        $fieldLanguageEnableJS->created_id = $idUser;
        $fieldLanguageEnableJS->save();

        $fieldUserRoleEnableJS = new FieldUserRole();
        $fieldUserRoleEnableJS->field_id = $fieldEnableJS->id;
        $fieldUserRoleEnableJS->role_id = $idRole;
        $fieldUserRoleEnableJS->enabled = true;
        $fieldUserRoleEnableJS->input_type = 5;
        $fieldUserRoleEnableJS->pos_x = 10;
        $fieldUserRoleEnableJS->pos_y = 1000;
        $fieldUserRoleEnableJS->created_id = $idUser;
        $fieldUserRoleEnableJS->save();
        #endregion Enable JS

        #region PROTECTED
        $fieldProtected = new Field();
        $fieldProtected->code = 'Protected';
        $fieldProtected->tab_id = $tabFunctionality->id;
        $fieldProtected->field = 'protected';
        $fieldProtected->data_type = 3;
        $fieldProtected->default_value = false;
        $fieldProtected->created_id = $idUser;
        $fieldProtected->save();

        $fieldLanguageProtected = new FieldLanguage();
        $fieldLanguageProtected->field_id = $fieldProtected->id;
        $fieldLanguageProtected->language_id = $idLanguage;
        $fieldLanguageProtected->description = 'Richiedi accesso';
        $fieldLanguageProtected->created_id = $idUser;
        $fieldLanguageProtected->save();

        $fieldUserRoleProtected = new FieldUserRole();
        $fieldUserRoleProtected->field_id = $fieldProtected->id;
        $fieldUserRoleProtected->role_id = $idRole;
        $fieldUserRoleProtected->enabled = true;
        $fieldUserRoleProtected->input_type = 5;
        $fieldUserRoleProtected->pos_x = 10;
        $fieldUserRoleProtected->pos_y = 1010;
        $fieldUserRoleProtected->created_id = $idUser;
        $fieldUserRoleProtected->save();
        #endregion PROTECTED

        #region field hidden
        $fieldPageType = new Field();
        $fieldPageType->code = 'PageType';
        $fieldPageType->tab_id = $tabAttribute->id;
        $fieldPageType->field = 'pageType';
        $fieldPageType->default_value = PageTypesEnum::Page;
        $fieldPageType->data_type = 0;
        $fieldPageType->created_id = $idUser;
        $fieldPageType->save();

        $fieldLanguagePageType = new FieldLanguage();
        $fieldLanguagePageType->field_id = $fieldPageType->id;
        $fieldLanguagePageType->language_id = $idLanguage;
        $fieldLanguagePageType->description = 'Tipo pagina';
        $fieldLanguagePageType->created_id = $idUser;
        $fieldLanguagePageType->save();

        $fieldUserRolePageType = new FieldUserRole();
        $fieldUserRolePageType->field_id = $fieldPageType->id;
        $fieldUserRolePageType->role_id = $idRole;
        $fieldUserRolePageType->enabled = true;
        $fieldUserRolePageType->input_type = 12;
        $fieldUserRolePageType->pos_x = 10;
        $fieldUserRolePageType->pos_y = 10;
        $fieldUserRolePageType->created_id = $idUser;
        $fieldUserRolePageType->save();
        #endregion field hidden
        #endregion PAGES
    }
}

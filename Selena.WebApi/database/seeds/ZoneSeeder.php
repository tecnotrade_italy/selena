<?php
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class ZoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $functionalityZone = Functionality::where('code', 'Zone')->first();
               
        //------------ Tab Tabella ---------------------------------//
        #region TAB
        $tabZoneTable = new Tab();
        $tabZoneTable->code = 'TableZone';
        $tabZoneTable->functionality_id = $functionalityZone->id;
        $tabZoneTable->order = 10;
        $tabZoneTable->created_id = $idUser;
        $tabZoneTable->save();

        $languageTabZoneTable = new LanguageTab();
        $languageTabZoneTable->language_id = $idLanguage;
        $languageTabZoneTable->tab_id = $tabZoneTable->id;
        $languageTabZoneTable->description = 'Tabella Zone';
        $languageTabZoneTable->created_id = $idUser;
        $languageTabZoneTable->save();
        #END region TAB

        #region CHECKBOX
        $fieldZoneCheckBox = new Field();
        $fieldZoneCheckBox->tab_id = $tabZoneTable->id;
        $fieldZoneCheckBox->code = 'TableCheckBox';
        $fieldZoneCheckBox->created_id = $idUser;
        $fieldZoneCheckBox->save();

        $fieldLanguageZoneCheckBox = new FieldLanguage();
        $fieldLanguageZoneCheckBox->field_id = $fieldZoneCheckBox->id;
        $fieldLanguageZoneCheckBox->language_id = $idLanguage;
        $fieldLanguageZoneCheckBox->description = 'CheckBox';
        $fieldLanguageZoneCheckBox->created_id = $idUser;
        $fieldLanguageZoneCheckBox->save();

        $fieldUserRoleZoneCheckBox = new FieldUserRole();
        $fieldUserRoleZoneCheckBox->field_id = $fieldZoneCheckBox->id;
        $fieldUserRoleZoneCheckBox->role_id = $idRole;
        $fieldUserRoleZoneCheckBox->enabled = true;
        $fieldUserRoleZoneCheckBox->table_order = 10;
        $fieldUserRoleZoneCheckBox->input_type = 13;
        $fieldUserRoleZoneCheckBox->created_id = $idUser;
        $fieldUserRoleZoneCheckBox->save();
        #endregion CHECKBOX
  
        #region FORMATTER
        $fieldZoneFormatter = new Field();
        $fieldZoneFormatter->code = 'ZoneFormatter';
        $fieldZoneFormatter->formatter = 'ZoneFormatter';
        $fieldZoneFormatter->tab_id = $tabZoneTable->id;
        $fieldZoneFormatter->created_id = $idUser;
        $fieldZoneFormatter->save();

        $fieldLanguageZoneFormatter = new FieldLanguage();
        $fieldLanguageZoneFormatter->field_id = $fieldZoneFormatter->id;
        $fieldLanguageZoneFormatter->language_id = $idLanguage;
        $fieldLanguageZoneFormatter->description = '';
        $fieldLanguageZoneFormatter->created_id = $idUser;
        $fieldLanguageZoneFormatter->save();

        $fieldUserRoleZoneFormatter = new FieldUserRole();
        $fieldUserRoleZoneFormatter->field_id = $fieldZoneFormatter->id;
        $fieldUserRoleZoneFormatter->role_id = $idRole;
        $fieldUserRoleZoneFormatter->enabled = true;
        $fieldUserRoleZoneFormatter->table_order = 20;
        $fieldUserRoleZoneFormatter->input_type = 14;
        $fieldUserRoleZoneFormatter->created_id = $idUser;
        $fieldUserRoleZoneFormatter->save();
        #endregion FORMATTER

        #region id_cities
        $fieldZoneIdCities = new Field();
        $fieldZoneIdCities->code = 'IdCities';
        $fieldZoneIdCities->tab_id = $tabZoneTable->id;
        $fieldZoneIdCities->field = 'IdCities';
        $fieldZoneIdCities->data_type = 1;
        $fieldZoneIdCities->created_id = $idUser;
        $fieldZoneIdCities->save();

        $fieldLanguageIdCities = new FieldLanguage();
        $fieldLanguageIdCities->field_id = $fieldZoneIdCities->id;
        $fieldLanguageIdCities->language_id = $idLanguage;
        $fieldLanguageIdCities->description = 'Comuni';
        $fieldLanguageIdCities->created_id = $idUser;
        $fieldLanguageIdCities->save();

        $fieldUserRoleIdCities = new FieldUserRole();
        $fieldUserRoleIdCities->field_id = $fieldZoneIdCities->id;
        $fieldUserRoleIdCities->role_id = $idRole;
        $fieldUserRoleIdCities->enabled = true;
        $fieldUserRoleIdCities->table_order = 60;
        $fieldUserRoleIdCities->input_type = 0;
        $fieldUserRoleIdCities->created_id = $idUser;
        $fieldUserRoleIdCities->save();
        #endregion id_cities

         #region id_postalcodes
        $fieldZoneIdPostalCodes = new Field();
        $fieldZoneIdPostalCodes->code = 'IdPostalcodes';
        $fieldZoneIdPostalCodes->tab_id = $tabZoneTable->id;
        $fieldZoneIdPostalCodes->field = 'IdPostalcodes';
        $fieldZoneIdPostalCodes->data_type = 1;
        $fieldZoneIdPostalCodes->created_id = $idUser;
        $fieldZoneIdPostalCodes->save();

        $fieldLanguageIdPostalCodes = new FieldLanguage();
        $fieldLanguageIdPostalCodes->field_id = $fieldZoneIdPostalCodes->id;
        $fieldLanguageIdPostalCodes->language_id = $idLanguage;
        $fieldLanguageIdPostalCodes->description = 'Codice Postale';
        $fieldLanguageIdPostalCodes->created_id = $idUser;
        $fieldLanguageIdPostalCodes->save();

        $fieldUserRoleIdPostalCodes = new FieldUserRole();
        $fieldUserRoleIdPostalCodes->field_id = $fieldZoneIdPostalCodes->id;
        $fieldUserRoleIdPostalCodes->role_id = $idRole;
        $fieldUserRoleIdPostalCodes->enabled = true;
        $fieldUserRoleIdPostalCodes->table_order = 70;
        $fieldUserRoleIdPostalCodes->input_type = 0;
        $fieldUserRoleIdPostalCodes->created_id = $idUser;
        $fieldUserRoleIdPostalCodes->save();
        #endregion id_postalcodes

        #region id_provinces
        $fieldZoneIdProvinces = new Field();
        $fieldZoneIdProvinces->code = 'IdProvinces';
        $fieldZoneIdProvinces->tab_id = $tabZoneTable->id;
        $fieldZoneIdProvinces->field = 'IdProvinces';
        $fieldZoneIdProvinces->data_type = 1;
        $fieldZoneIdProvinces->created_id = $idUser;
        $fieldZoneIdProvinces->save();

        $fieldLanguageIdProvinces = new FieldLanguage();
        $fieldLanguageIdProvinces->field_id = $fieldZoneIdProvinces->id;
        $fieldLanguageIdProvinces->language_id = $idLanguage;
        $fieldLanguageIdProvinces->description = 'Provincia';
        $fieldLanguageIdProvinces->created_id = $idUser;
        $fieldLanguageIdProvinces->save();

        $fieldUserRoleIdProvinces = new FieldUserRole();
        $fieldUserRoleIdProvinces->field_id = $fieldZoneIdProvinces->id;
        $fieldUserRoleIdProvinces->role_id = $idRole;
        $fieldUserRoleIdProvinces->enabled = true;
        $fieldUserRoleIdProvinces->table_order = 50;
        $fieldUserRoleIdProvinces->input_type = 0;
        $fieldUserRoleIdProvinces->created_id = $idUser;
        $fieldUserRoleIdProvinces->save();
        #endregion id_provinces   

        #region id_regions
        $fieldZoneIdRegions = new Field();
        $fieldZoneIdRegions->code = 'IdRegions';
        $fieldZoneIdRegions->tab_id = $tabZoneTable->id;
        $fieldZoneIdRegions->field = 'IdRegions';
        $fieldZoneIdRegions->data_type = 1;
        $fieldZoneIdRegions->created_id = $idUser;
        $fieldZoneIdRegions->save();

        $fieldLanguageIdRegions = new FieldLanguage();
        $fieldLanguageIdRegions->field_id = $fieldZoneIdRegions->id;
        $fieldLanguageIdRegions->language_id = $idLanguage;
        $fieldLanguageIdRegions->description = 'Regione';
        $fieldLanguageIdRegions->created_id = $idUser;
        $fieldLanguageIdRegions->save();

        $fieldUserRoleIdRegions = new FieldUserRole();
        $fieldUserRoleIdRegions->field_id = $fieldZoneIdRegions->id;
        $fieldUserRoleIdRegions->role_id = $idRole;
        $fieldUserRoleIdRegions->enabled = true;
        $fieldUserRoleIdRegions->table_order = 40;
        $fieldUserRoleIdRegions->input_type = 0;
        $fieldUserRoleIdRegions->created_id = $idUser;
        $fieldUserRoleIdRegions->save();
        #endregion id_regions   

          #region id_nations
        $fieldZoneIdNations = new Field();
        $fieldZoneIdNations->code = 'IdNations';
        $fieldZoneIdNations->tab_id = $tabZoneTable->id;
        $fieldZoneIdNations->field = 'IdNations';
        $fieldZoneIdNations->data_type = 1;
        $fieldZoneIdNations->created_id = $idUser;
        $fieldZoneIdNations->save();

        $fieldLanguageIdNations = new FieldLanguage();
        $fieldLanguageIdNations->field_id = $fieldZoneIdNations->id;
        $fieldLanguageIdNations->language_id = $idLanguage;
        $fieldLanguageIdNations->description = 'Nazione';
        $fieldLanguageIdNations->created_id = $idUser;
        $fieldLanguageIdNations->save();

        $fieldUserRoleIdNations = new FieldUserRole();
        $fieldUserRoleIdNations->field_id = $fieldZoneIdNations->id;
        $fieldUserRoleIdNations->role_id = $idRole;
        $fieldUserRoleIdNations->enabled = true;
        $fieldUserRoleIdNations->table_order = 30;
        $fieldUserRoleIdNations->input_type = 0;
        $fieldUserRoleIdNations->created_id = $idUser;
        $fieldUserRoleIdNations->save();
        #endregion id_nations   



        //------------ Tab Dettaglio ---------------------------------//
        #region TAB
        $detailZoneTable = new Tab();
        $detailZoneTable->code = 'DetailZone';
        $detailZoneTable->functionality_id = $functionalityZone->id;
        $detailZoneTable->order = 10;
        $detailZoneTable->created_id = $idUser;
        $detailZoneTable->save();

        $languageTabZoneTable = new LanguageTab();
        $languageTabZoneTable->language_id = $idLanguage;
        $languageTabZoneTable->tab_id = $detailZoneTable->id;
        $languageTabZoneTable->description = 'Dettaglio Zone';
        $languageTabZoneTable->created_id = $idUser;
        $languageTabZoneTable->save();
        #END region TAB

        #region id_nations
        $fieldZoneIdNations = new Field();
        $fieldZoneIdNations->code = 'id_nations';
        $fieldZoneIdNations->tab_id = $detailZoneTable->id;
        $fieldZoneIdNations->field = 'id_nations';
        $fieldZoneIdNations->service = 'nations';
        $fieldZoneIdNations->data_type = 1;
        $fieldZoneIdNations->created_id = $idUser;
        $fieldZoneIdNations->save();

        $fieldLanguageIdNations = new FieldLanguage();
        $fieldLanguageIdNations->field_id = $fieldZoneIdNations->id;
        $fieldLanguageIdNations->language_id = $idLanguage;
        $fieldLanguageIdNations->description = 'Nazione';
        $fieldLanguageIdNations->created_id = $idUser;
        $fieldLanguageIdNations->save();

        $fieldUserRoleIdNations = new FieldUserRole();
        $fieldUserRoleIdNations->field_id = $fieldZoneIdNations->id;
        $fieldUserRoleIdNations->role_id = $idRole;
        $fieldUserRoleIdNations->enabled = true;
        $fieldUserRoleIdNations->pos_x = 10;
        $fieldUserRoleIdNations->pos_y = 10;
        $fieldUserRoleIdNations->input_type = 2;
        $fieldUserRoleIdNations->created_id = $idUser;
        $fieldUserRoleIdNations->save();
        #endregion id_nations

        #region id_regions
        $fieldZoneIdRegions = new Field();
        $fieldZoneIdRegions->code = 'id_regions';
        $fieldZoneIdRegions->tab_id = $detailZoneTable->id;
        $fieldZoneIdRegions->field = 'id_regions';
        $fieldZoneIdRegions->service = 'regions';
        $fieldZoneIdRegions->data_type = 1;
        $fieldZoneIdRegions->created_id = $idUser;
        $fieldZoneIdRegions->save();

        $fieldLanguageIdRegions = new FieldLanguage();
        $fieldLanguageIdRegions->field_id = $fieldZoneIdRegions->id;
        $fieldLanguageIdRegions->language_id = $idLanguage;
        $fieldLanguageIdRegions->description = 'Regione';
        $fieldLanguageIdRegions->created_id = $idUser;
        $fieldLanguageIdRegions->save();

        $fieldUserRoleIdRegions = new FieldUserRole();
        $fieldUserRoleIdRegions->field_id = $fieldZoneIdRegions->id;
        $fieldUserRoleIdRegions->role_id = $idRole;
        $fieldUserRoleIdRegions->enabled = true;
        $fieldUserRoleIdRegions->pos_x = 20;
        $fieldUserRoleIdRegions->pos_y = 10;
        $fieldUserRoleIdRegions->input_type = 2;
        $fieldUserRoleIdRegions->created_id = $idUser;
        $fieldUserRoleIdRegions->save();
        #endregion id_regions

        #region id_provinces
        $fieldZoneIdProvinces = new Field();
        $fieldZoneIdProvinces->code = 'id_provinces';
        $fieldZoneIdProvinces->tab_id = $detailZoneTable->id;
        $fieldZoneIdProvinces->field = 'id_provinces';
        $fieldZoneIdProvinces->service = 'provinces';
        $fieldZoneIdProvinces->data_type = 1;
        $fieldZoneIdProvinces->created_id = $idUser;
        $fieldZoneIdProvinces->save();

        $fieldLanguageIdProvinces = new FieldLanguage();
        $fieldLanguageIdProvinces->field_id = $fieldZoneIdProvinces->id;
        $fieldLanguageIdProvinces->language_id = $idLanguage;
        $fieldLanguageIdProvinces->description = 'Provincia';
        $fieldLanguageIdProvinces->created_id = $idUser;
        $fieldLanguageIdProvinces->save();

        $fieldUserRoleIdProvinces = new FieldUserRole();
        $fieldUserRoleIdProvinces->field_id = $fieldZoneIdProvinces->id;
        $fieldUserRoleIdProvinces->role_id = $idRole;
        $fieldUserRoleIdProvinces->enabled = true;
        $fieldUserRoleIdProvinces->pos_x = 30;
        $fieldUserRoleIdProvinces->pos_y = 10;
        $fieldUserRoleIdProvinces->input_type = 2;
        $fieldUserRoleIdProvinces->created_id = $idUser;
        $fieldUserRoleIdProvinces->save();
        #endregion id_provinces

        #region id_postalcodes
        $fieldZoneIdPostalCodes = new Field();
        $fieldZoneIdPostalCodes->code = 'id_postalcodes';
        $fieldZoneIdPostalCodes->tab_id = $detailZoneTable->id;
        $fieldZoneIdPostalCodes->field = 'id_postalcodes';
        $fieldZoneIdPostalCodes->service = 'postalCode';
        $fieldZoneIdPostalCodes->data_type = 1;
        $fieldZoneIdPostalCodes->created_id = $idUser;
        $fieldZoneIdPostalCodes->save();

        $fieldLanguageIdPostalCodes = new FieldLanguage();
        $fieldLanguageIdPostalCodes->field_id = $fieldZoneIdPostalCodes->id;
        $fieldLanguageIdPostalCodes->language_id = $idLanguage;
        $fieldLanguageIdPostalCodes->description = 'Codice Postale';
        $fieldLanguageIdPostalCodes->created_id = $idUser;
        $fieldLanguageIdPostalCodes->save();

        $fieldUserRoleIdPostalCodes = new FieldUserRole();
        $fieldUserRoleIdPostalCodes->field_id = $fieldZoneIdPostalCodes->id;
        $fieldUserRoleIdPostalCodes->role_id = $idRole;
        $fieldUserRoleIdPostalCodes->enabled = true;
        $fieldUserRoleIdPostalCodes->pos_x = 10;
        $fieldUserRoleIdPostalCodes->pos_y = 20;
        $fieldUserRoleIdPostalCodes->input_type = 2;
        $fieldUserRoleIdPostalCodes->created_id = $idUser;
        $fieldUserRoleIdPostalCodes->save();
        #endregion id_postalcodes

        #region id_cities
        $fieldZoneIdCities = new Field();
        $fieldZoneIdCities->code = 'id_cities';
        $fieldZoneIdCities->tab_id = $detailZoneTable->id;
        $fieldZoneIdCities->field = 'id_cities';
        $fieldZoneIdPostalCodes->service = 'cities';
        $fieldZoneIdCities->data_type = 1;
        $fieldZoneIdCities->created_id = $idUser;
        $fieldZoneIdCities->save();

        $fieldLanguageIdCities = new FieldLanguage();
        $fieldLanguageIdCities->field_id = $fieldZoneIdCities->id;
        $fieldLanguageIdCities->language_id = $idLanguage;
        $fieldLanguageIdCities->description = 'Comune';
        $fieldLanguageIdCities->created_id = $idUser;
        $fieldLanguageIdCities->save();

        $fieldUserRoleIdCities = new FieldUserRole();
        $fieldUserRoleIdCities->field_id = $fieldZoneIdCities->id;
        $fieldUserRoleIdCities->role_id = $idRole;
        $fieldUserRoleIdCities->enabled = true;
        $fieldUserRoleIdCities->pos_x = 20;
        $fieldUserRoleIdCities->pos_y = 20;
        $fieldUserRoleIdCities->input_type = 2;
        $fieldUserRoleIdCities->created_id = $idUser;
        $fieldUserRoleIdCities->save();
        #endregion id_cities
   

    }
}

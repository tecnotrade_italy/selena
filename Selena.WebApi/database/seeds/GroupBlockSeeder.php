<?php

use App\Language;
use App\Role;
use Illuminate\Database\Seeder;
use App\User;
use App\TemplateEditorGroup;

class GroupBlockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idUser = User::first()->id;
        $idLanguage = Language::first()->id;
        $idRole = Role::where('name', 'admin')->first()->id;

        #region GRUPPO BASE

        /**
         * 
         * 
         */
        $groupBlock = new TemplateEditorGroup();
        $groupBlock->description = 'Paper plane';
        $groupBlock->created_id = $idUser;
        $groupBlock->save();

        #endregion GRUPPO BASE

    }
}

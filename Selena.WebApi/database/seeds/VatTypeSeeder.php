<?php

use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class VatTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $idMenuAdminCRM = MenuAdmin::where('code', 'CRM')->first()->id;

        #region VATTYPE

        #region ICON

        $iconIdCard = new Icon();
        $iconIdCard->description = 'Bookmark';
        $iconIdCard->css = 'fa fa-bookmark-o';
        $iconIdCard->created_id = $idUser;
        $iconIdCard->save();

        #endregion ICON

        #region FUNCTIONALITY

        $functionalityCustomer = new Functionality();
        $functionalityCustomer->code = 'VatType';
        $functionalityCustomer->created_id = $idUser;
        $functionalityCustomer->save();

        #endregion FUNCTIONALITY

        #region MENU

        $menuAdminCustomer = new MenuAdmin();
        $menuAdminCustomer->code = 'VatType';
        $menuAdminCustomer->functionality_id = $functionalityCustomer->id;
        $menuAdminCustomer->icon_id = $iconIdCard->id;
        $menuAdminCustomer->url = '/admin/crm/vattype';
        $menuAdminCustomer->created_id = $idUser;
        $menuAdminCustomer->save();

        $languageMenuAdminCustomer = new LanguageMenuAdmin();
        $languageMenuAdminCustomer->language_id = $idLanguage;
        $languageMenuAdminCustomer->menu_admin_id = $menuAdminCustomer->id;
        $languageMenuAdminCustomer->description = 'Iva';
        $languageMenuAdminCustomer->created_id = $idUser;
        $languageMenuAdminCustomer->save();

        $menuAdminUserRoleCustomer = new MenuAdminUserRole();
        $menuAdminUserRoleCustomer->menu_admin_id = $menuAdminCustomer->id;
        $menuAdminUserRoleCustomer->menu_admin_father_id = $idMenuAdminCRM;
        $menuAdminUserRoleCustomer->role_id = $idRole;
        $menuAdminUserRoleCustomer->enabled = true;
        $menuAdminUserRoleCustomer->order = 30;
        $menuAdminUserRoleCustomer->created_id = $idUser;
        $menuAdminUserRoleCustomer->save();

        #endregion MENU

        #region TABLE

        #region TAB

        $tabCustomerTable = new Tab();
        $tabCustomerTable->code = 'Table';
        $tabCustomerTable->functionality_id = $functionalityCustomer->id;
        $tabCustomerTable->order = 10;
        $tabCustomerTable->created_id = $idUser;
        $tabCustomerTable->save();

        $languageTabCustomerTable = new LanguageTab();
        $languageTabCustomerTable->language_id = $idLanguage;
        $languageTabCustomerTable->tab_id = $tabCustomerTable->id;
        $languageTabCustomerTable->description = 'Table';
        $languageTabCustomerTable->created_id = $idUser;
        $languageTabCustomerTable->save();

        #endregion TAB

        #region CHECKBOX

        $fieldCustomerCheckBox = new Field();
        $fieldCustomerCheckBox->tab_id = $tabCustomerTable->id;
        $fieldCustomerCheckBox->code = 'TableCheckBox';
        $fieldCustomerCheckBox->created_id = $idUser;
        $fieldCustomerCheckBox->save();

        $fieldLanguageCustomerCheckBox = new FieldLanguage();
        $fieldLanguageCustomerCheckBox->field_id = $fieldCustomerCheckBox->id;
        $fieldLanguageCustomerCheckBox->language_id = $idLanguage;
        $fieldLanguageCustomerCheckBox->description = 'CheckBox';
        $fieldLanguageCustomerCheckBox->created_id = $idUser;
        $fieldLanguageCustomerCheckBox->save();

        $fieldUserRoleCustomerCheckBox = new FieldUserRole();
        $fieldUserRoleCustomerCheckBox->field_id = $fieldCustomerCheckBox->id;
        $fieldUserRoleCustomerCheckBox->role_id = $idRole;
        $fieldUserRoleCustomerCheckBox->enabled = true;
        $fieldUserRoleCustomerCheckBox->table_order = 10;
        $fieldUserRoleCustomerCheckBox->input_type = 13;
        $fieldUserRoleCustomerCheckBox->created_id = $idUser;
        $fieldUserRoleCustomerCheckBox->save();

        #endregion CHECKBOX

        #region FORMATTER

        $fieldCustomerFormatter = new Field();
        $fieldCustomerFormatter->code = 'VatTypeFormatter';
        $fieldCustomerFormatter->formatter = 'vatTypeFormatter';
        $fieldCustomerFormatter->tab_id = $tabCustomerTable->id;
        $fieldCustomerFormatter->created_id = $idUser;
        $fieldCustomerFormatter->save();

        $fieldLanguageCustomerFormatter = new FieldLanguage();
        $fieldLanguageCustomerFormatter->field_id = $fieldCustomerFormatter->id;
        $fieldLanguageCustomerFormatter->language_id = $idLanguage;
        $fieldLanguageCustomerFormatter->description = '';
        $fieldLanguageCustomerFormatter->created_id = $idUser;
        $fieldLanguageCustomerFormatter->save();

        $fieldUserRoleCustomerFormatter = new FieldUserRole();
        $fieldUserRoleCustomerFormatter->field_id = $fieldCustomerFormatter->id;
        $fieldUserRoleCustomerFormatter->role_id = $idRole;
        $fieldUserRoleCustomerFormatter->enabled = true;
        $fieldUserRoleCustomerFormatter->table_order = 20;
        $fieldUserRoleCustomerFormatter->input_type = 14;
        $fieldUserRoleCustomerFormatter->created_id = $idUser;
        $fieldUserRoleCustomerFormatter->save();

        #endregion FORMATTER

        #region RATE

        $fieldCustomerTableCompany = new Field();
        $fieldCustomerTableCompany->code = 'Rate';
        $fieldCustomerTableCompany->tab_id = $tabCustomerTable->id;
        $fieldCustomerTableCompany->field = 'rate';
        $fieldCustomerTableCompany->data_type = 1;
        $fieldCustomerTableCompany->created_id = $idUser;
        $fieldCustomerTableCompany->save();

        $fieldLanguageCustomerTableCompany = new FieldLanguage();
        $fieldLanguageCustomerTableCompany->field_id = $fieldCustomerTableCompany->id;
        $fieldLanguageCustomerTableCompany->language_id = $idLanguage;
        $fieldLanguageCustomerTableCompany->description = 'Iva';
        $fieldLanguageCustomerTableCompany->created_id = $idUser;
        $fieldLanguageCustomerTableCompany->save();

        $fieldUserRoleCustomerTableCompany = new FieldUserRole();
        $fieldUserRoleCustomerTableCompany->field_id = $fieldCustomerTableCompany->id;
        $fieldUserRoleCustomerTableCompany->role_id = $idRole;
        $fieldUserRoleCustomerTableCompany->enabled = true;
        $fieldUserRoleCustomerTableCompany->table_order = 30;
        $fieldUserRoleCustomerTableCompany->input_type = 0;
        $fieldUserRoleCustomerTableCompany->created_id = $idUser;
        $fieldUserRoleCustomerTableCompany->save();

        #endregion RATE
 
        #endregion TABLE

        #region DETAIL

        #region TAB

        $tabCustomerDetail = new Tab();
        $tabCustomerDetail->code = 'Detail';
        $tabCustomerDetail->functionality_id = $functionalityCustomer->id;
        $tabCustomerDetail->order = 20;
        $tabCustomerDetail->created_id = $idUser;
        $tabCustomerDetail->save();

        $languageTabCustomerDetail = new LanguageTab();
        $languageTabCustomerDetail->language_id = $idLanguage;
        $languageTabCustomerDetail->tab_id = $tabCustomerDetail->id;
        $languageTabCustomerDetail->description = 'Detail';
        $languageTabCustomerDetail->created_id = $idUser;
        $languageTabCustomerDetail->save();

        #endregion TAB

        #region RATE

        $fieldCustomerDetailCompany = new Field();
        $fieldCustomerDetailCompany->code = 'Rate';
        $fieldCustomerDetailCompany->tab_id = $tabCustomerDetail->id;
        $fieldCustomerDetailCompany->field = 'rate';
        $fieldCustomerDetailCompany->data_type = 1;
        $fieldCustomerDetailCompany->created_id = $idUser;
        $fieldCustomerDetailCompany->save();

        $fieldLanguageCustomerDetailCompany = new FieldLanguage();
        $fieldLanguageCustomerDetailCompany->field_id = $fieldCustomerDetailCompany->id;
        $fieldLanguageCustomerDetailCompany->language_id = $idLanguage;
        $fieldLanguageCustomerDetailCompany->description = 'Iva';
        $fieldLanguageCustomerDetailCompany->created_id = $idUser;
        $fieldLanguageCustomerDetailCompany->save();

        $fieldUserRoleCustomerDetailCompany = new FieldUserRole();
        $fieldUserRoleCustomerDetailCompany->field_id = $fieldCustomerDetailCompany->id;
        $fieldUserRoleCustomerDetailCompany->role_id = $idRole;
        $fieldUserRoleCustomerDetailCompany->enabled = true;
        $fieldUserRoleCustomerDetailCompany->pos_x = 10;
        $fieldUserRoleCustomerDetailCompany->pos_y = 10;
        $fieldUserRoleCustomerDetailCompany->input_type = 0;
        $fieldUserRoleCustomerDetailCompany->created_id = $idUser;
        $fieldUserRoleCustomerDetailCompany->save();

        #endregion RATE

        #endregion DETAIL

        #endregion VATTYPE
    }
}

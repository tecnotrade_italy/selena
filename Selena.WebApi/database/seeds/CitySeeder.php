<?php
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $functionalityCity = Functionality::where('code', 'Cities')->first();
               
        //------------ Tab Tabella ---------------------------------
        #region TAB
        $tabCityTable = new Tab();
        $tabCityTable->code = 'TableCities';
        $tabCityTable->functionality_id = $functionalityCity->id;
        $tabCityTable->order = 10;
        $tabCityTable->created_id = $idUser;
        $tabCityTable->save();

        $languageTabCityTable = new LanguageTab();
        $languageTabCityTable->language_id = $idLanguage;
        $languageTabCityTable->tab_id = $tabCityTable->id;
        $languageTabCityTable->description = 'Tabella Comuni';
        $languageTabCityTable->created_id = $idUser;
        $languageTabCityTable->save();
        #END region TAB

        #region CHECKBOX
        $fieldCityCheckBox = new Field();
        $fieldCityCheckBox->tab_id = $tabCityTable->id;
        $fieldCityCheckBox->code = 'TableCheckBox';
        $fieldCityCheckBox->created_id = $idUser;
        $fieldCityCheckBox->save();

        $fieldLanguageCityCheckBox = new FieldLanguage();
        $fieldLanguageCityCheckBox->field_id = $fieldCityCheckBox->id;
        $fieldLanguageCityCheckBox->language_id = $idLanguage;
        $fieldLanguageCityCheckBox->description = 'CheckBox';
        $fieldLanguageCityCheckBox->created_id = $idUser;
        $fieldLanguageCityCheckBox->save();

        $fieldUserRoleCityCheckBox = new FieldUserRole();
        $fieldUserRoleCityCheckBox->field_id = $fieldCityCheckBox->id;
        $fieldUserRoleCityCheckBox->role_id = $idRole;
        $fieldUserRoleCityCheckBox->enabled = true;
        $fieldUserRoleCityCheckBox->table_order = 10;
        $fieldUserRoleCityCheckBox->input_type = 13;
        $fieldUserRoleCityCheckBox->created_id = $idUser;
        $fieldUserRoleCityCheckBox->save();
        #endregion CHECKBOX
  
        #region FORMATTER
        $fieldCityFormatter = new Field();
        $fieldCityFormatter->code = 'CityFormatter';
        $fieldCityFormatter->formatter = 'CityFormatter';
        $fieldCityFormatter->tab_id = $tabCityTable->id;
        $fieldCityFormatter->created_id = $idUser;
        $fieldCityFormatter->save();

        $fieldLanguageCityFormatter = new FieldLanguage();
        $fieldLanguageCityFormatter->field_id = $fieldCityFormatter->id;
        $fieldLanguageCityFormatter->language_id = $idLanguage;
        $fieldLanguageCityFormatter->description = '';
        $fieldLanguageCityFormatter->created_id = $idUser;
        $fieldLanguageCityFormatter->save();

        $fieldUserRoleCityFormatter = new FieldUserRole();
        $fieldUserRoleCityFormatter->field_id = $fieldCityFormatter->id;
        $fieldUserRoleCityFormatter->role_id = $idRole;
        $fieldUserRoleCityFormatter->enabled = true;
        $fieldUserRoleCityFormatter->table_order = 20;
        $fieldUserRoleCityFormatter->input_type = 14;
        $fieldUserRoleCityFormatter->created_id = $idUser;
        $fieldUserRoleCityFormatter->save();
        #endregion FORMATTER

        #region description
        $fieldCityDescription = new Field();
        $fieldCityDescription->code = 'description';
        $fieldCityDescription->tab_id = $tabCityTable->id;
        $fieldCityDescription->field = 'description';
        $fieldCityDescription->data_type = 1;
        $fieldCityDescription->created_id = $idUser;
        $fieldCityDescription->save();

        $fieldLanguageCityDescription = new FieldLanguage();
        $fieldLanguageCityDescription->field_id = $fieldCityDescription->id;
        $fieldLanguageCityDescription->language_id = $idLanguage;
        $fieldLanguageCityDescription->description = 'Descrizione';
        $fieldLanguageCityDescription->created_id = $idUser;
        $fieldLanguageCityDescription->save();

        $fieldUserRoleCityDescription = new FieldUserRole();
        $fieldUserRoleCityDescription->field_id = $fieldCityDescription->id;
        $fieldUserRoleCityDescription->role_id = $idRole;
        $fieldUserRoleCityDescription->enabled = true;
        $fieldUserRoleCityDescription->table_order = 30;
        $fieldUserRoleCityDescription->input_type = 0;
        $fieldUserRoleCityDescription->created_id = $idUser;
        $fieldUserRoleCityDescription->save();
        #endregion description

        #region order
        $fieldCityOrder = new Field();
        $fieldCityOrder->code = 'order';
        $fieldCityOrder->tab_id = $tabCityTable->id;
        $fieldCityOrder->field = 'order';
        $fieldCityOrder->data_type = 1;
        $fieldCityOrder->created_id = $idUser;
        $fieldCityOrder->save();

        $fieldLanguageCityOrder = new FieldLanguage();
        $fieldLanguageCityOrder->field_id = $fieldCityOrder->id;
        $fieldLanguageCityOrder->language_id = $idLanguage;
        $fieldLanguageCityOrder->description = 'Ordine';
        $fieldLanguageCityOrder->created_id = $idUser;
        $fieldLanguageCityOrder->save();

        $fieldUserRoleCityOrder = new FieldUserRole();
        $fieldUserRoleCityOrder->field_id = $fieldCityOrder->id;
        $fieldUserRoleCityOrder->role_id = $idRole;
        $fieldUserRoleCityOrder->enabled = true;
        $fieldUserRoleCityOrder->table_order = 40;
        $fieldUserRoleCityOrder->input_type = 0;
        $fieldUserRoleCityOrder->created_id = $idUser;
        $fieldUserRoleCityOrder->save();
        #endregion order

        #region istat
        $fieldCityIstat = new Field();
        $fieldCityIstat->code = 'istat';
        $fieldCityIstat->tab_id = $tabCityTable->id;
        $fieldCityIstat->field = 'istat';
        $fieldCityIstat->data_type = 1;
        $fieldCityIstat->created_id = $idUser;
        $fieldCityIstat->save();

        $fieldLanguageCityIstat = new FieldLanguage();
        $fieldLanguageCityIstat->field_id = $fieldCityIstat->id;
        $fieldLanguageCityIstat->language_id = $idLanguage;
        $fieldLanguageCityIstat->description = 'Codice Istat';
        $fieldLanguageCityIstat->created_id = $idUser;
        $fieldLanguageCityIstat->save();

        $fieldUserRoleCityIstat = new FieldUserRole();
        $fieldUserRoleCityIstat->field_id = $fieldCityIstat->id;
        $fieldUserRoleCityIstat->role_id = $idRole;
        $fieldUserRoleCityIstat->enabled = true;
        $fieldUserRoleCityIstat->table_order = 50;
        $fieldUserRoleCityIstat->input_type = 0;
        $fieldUserRoleCityIstat->created_id = $idUser;
        $fieldUserRoleCityIstat->save();
        #endregion istat
        

        //------------ Tab Dettaglio ---------------------------------
        #region TAB
        $detailCityTable = new Tab();
        $detailCityTable->code = 'DetailCities';
        $detailCityTable->functionality_id = $functionalityCity->id;
        $detailCityTable->order = 10;
        $detailCityTable->created_id = $idUser;
        $detailCityTable->save();

        $languageTabCityTable = new LanguageTab();
        $languageTabCityTable->language_id = $idLanguage;
        $languageTabCityTable->tab_id = $detailCityTable->id;
        $languageTabCityTable->description = 'Dettaglio Comuni';
        $languageTabCityTable->created_id = $idUser;
        $languageTabCityTable->save();
        #END region TAB

        #region description
        $fieldCityDescription = new Field();
        $fieldCityDescription->code = 'description';
        $fieldCityDescription->tab_id = $detailCityTable->id;
        $fieldCityDescription->field = 'description';
        $fieldCityDescription->data_type = 1;
        $fieldCityDescription->created_id = $idUser;
        $fieldCityDescription->save();

        $fieldLanguageCityDescription = new FieldLanguage();
        $fieldLanguageCityDescription->field_id = $fieldCityDescription->id;
        $fieldLanguageCityDescription->language_id = $idLanguage;
        $fieldLanguageCityDescription->description = 'Descrizione';
        $fieldLanguageCityDescription->created_id = $idUser;
        $fieldLanguageCityDescription->save();

        $fieldUserRoleCityDescription = new FieldUserRole();
        $fieldUserRoleCityDescription->field_id = $fieldCityDescription->id;
        $fieldUserRoleCityDescription->role_id = $idRole;
        $fieldUserRoleCityDescription->enabled = true;
        $fieldUserRoleCityDescription->pos_x = 10;
        $fieldUserRoleCityDescription->pos_y = 10;
        $fieldUserRoleCityDescription->input_type = 0;
        $fieldUserRoleCityDescription->created_id = $idUser;
        $fieldUserRoleCityDescription->save();
        #endregion description

        #region order
        $fieldCityOrder = new Field();
        $fieldCityOrder->code = 'order';
        $fieldCityOrder->tab_id = $detailCityTable->id;
        $fieldCityOrder->field = 'order';
        $fieldCityOrder->data_type = 1;
        $fieldCityOrder->created_id = $idUser;
        $fieldCityOrder->save();

        $fieldLanguageCityOrder = new FieldLanguage();
        $fieldLanguageCityOrder->field_id = $fieldCityOrder->id;
        $fieldLanguageCityOrder->language_id = $idLanguage;
        $fieldLanguageCityOrder->description = 'Ordinamento';
        $fieldLanguageCityOrder->created_id = $idUser;
        $fieldLanguageCityOrder->save();

        $fieldUserRoleCityOrder = new FieldUserRole();
        $fieldUserRoleCityOrder->field_id = $fieldCityOrder->id;
        $fieldUserRoleCityOrder->role_id = $idRole;
        $fieldUserRoleCityOrder->enabled = true;
        $fieldUserRoleCityOrder->pos_x = 20;
        $fieldUserRoleCityOrder->pos_y = 10;
        $fieldUserRoleCityOrder->input_type = 0;
        $fieldUserRoleCityOrder->created_id = $idUser;
        $fieldUserRoleCityOrder->save();
        #endregion order

        #region istat
        $fieldCityIstat = new Field();
        $fieldCityIstat->code = 'istat';
        $fieldCityIstat->tab_id = $detailCityTable->id;
        $fieldCityIstat->field = 'istat';
        $fieldCityIstat->data_type = 1;
        $fieldCityIstat->created_id = $idUser;
        $fieldCityIstat->save();

        $fieldLanguageCityIstat = new FieldLanguage();
        $fieldLanguageCityIstat->field_id = $fieldCityIstat->id;
        $fieldLanguageCityIstat->language_id = $idLanguage;
        $fieldLanguageCityIstat->description = 'Codice Istat';
        $fieldLanguageCityIstat->created_id = $idUser;
        $fieldLanguageCityIstat->save();

        $fieldUserRoleCityIstat = new FieldUserRole();
        $fieldUserRoleCityIstat->field_id = $fieldCityIstat->id;
        $fieldUserRoleCityIstat->role_id = $idRole;
        $fieldUserRoleCityIstat->enabled = true;
        $fieldUserRoleCityIstat->pos_x = 30;
        $fieldUserRoleCityIstat->pos_y = 10;
        $fieldUserRoleCityIstat->input_type = 0;
        $fieldUserRoleCityIstat->created_id = $idUser;
        $fieldUserRoleCityIstat->save();
        #endregion istat
        

    }
}

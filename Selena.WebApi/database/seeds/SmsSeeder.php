<?php

use Illuminate\Database\Seeder;
use App\Dictionary;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;


class SmsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $functionalityUserSetting = Functionality::where('code', 'SendSms')->first()->id;
        
        #region TAB USER
        $tabContentUsers = new Tab();
        $tabContentUsers->code = 'TableSendSms';
        $tabContentUsers->functionality_id = $functionalityUserSetting;
        $tabContentUsers->order = 10;
        $tabContentUsers->created_id = $idUser;
        $tabContentUsers->save();

        $languageTabContentUsers = new LanguageTab();
        $languageTabContentUsers->language_id = $idLanguage;
        $languageTabContentUsers->tab_id = $tabContentUsers->id;
        $languageTabContentUsers->description = 'Sms';
        $languageTabContentUsers->created_id = $idUser;
        $languageTabContentUsers->save();

        #endregion TAB USER

        #region CHECKBOX

        $fieldCheckBoxUsername = new Field();
        $fieldCheckBoxUsername->tab_id = $tabContentUsers->id;
        $fieldCheckBoxUsername->code = 'TableCheckBox';
        $fieldCheckBoxUsername->created_id = $idUser;
        $fieldCheckBoxUsername->save();

        $fieldLanguageCheckBoxUsername = new FieldLanguage();
        $fieldLanguageCheckBoxUsername->field_id = $fieldCheckBoxUsername->id;
        $fieldLanguageCheckBoxUsername->language_id = $idLanguage;
        $fieldLanguageCheckBoxUsername->description = 'CheckBox';
        $fieldLanguageCheckBoxUsername->created_id = $idUser;
        $fieldLanguageCheckBoxUsername->save();

        $fieldUserRoleCheckBoxUsername = new FieldUserRole();
        $fieldUserRoleCheckBoxUsername->field_id = $fieldCheckBoxUsername->id;
        $fieldUserRoleCheckBoxUsername->role_id = $idRole;
        $fieldUserRoleCheckBoxUsername->enabled = true;
        $fieldUserRoleCheckBoxUsername->table_order = 10;
        $fieldUserRoleCheckBoxUsername->input_type = 13;
        $fieldUserRoleCheckBoxUsername->created_id = $idUser;
        $fieldUserRoleCheckBoxUsername->save();

        #endregion CHECKBOX

        #region FORMATTER

        $fieldUserSettingFormatter = new Field();
        $fieldUserSettingFormatter->code = 'SendSmsFormatter';
        $fieldUserSettingFormatter->formatter = 'SendSmsFormatter';
        $fieldUserSettingFormatter->tab_id = $tabContentUsers->id;
        $fieldUserSettingFormatter->created_id = $idUser;
        $fieldUserSettingFormatter->save();

        $fieldLanguageUserSettingFormatter = new FieldLanguage();
        $fieldLanguageUserSettingFormatter->field_id = $fieldUserSettingFormatter->id;
        $fieldLanguageUserSettingFormatter->language_id = $idLanguage;
        $fieldLanguageUserSettingFormatter->description = '';
        $fieldLanguageUserSettingFormatter->created_id = $idUser;
        $fieldLanguageUserSettingFormatter->save();

        $fieldUserRoleUserSettingFormatter = new FieldUserRole();
        $fieldUserRoleUserSettingFormatter->field_id = $fieldUserSettingFormatter->id;
        $fieldUserRoleUserSettingFormatter->role_id = $idRole;
        $fieldUserRoleUserSettingFormatter->enabled = true;
        $fieldUserRoleUserSettingFormatter->table_order = 20;
        $fieldUserRoleUserSettingFormatter->input_type = 14;
        $fieldUserRoleUserSettingFormatter->created_id = $idUser;
        $fieldUserRoleUserSettingFormatter->save();

        #endregion FORMATTER

        #region TITLE
        $fieldUsername = new Field();
        $fieldUsername->code = 'title';
        $fieldUsername->field = 'title';
        $fieldUsername->data_type = 1;
        $fieldUsername->tab_id = $tabContentUsers->id;
        $fieldUsername->created_id = $idUser;
        $fieldUsername->save();

        $fieldLanguageUsername = new FieldLanguage();
        $fieldLanguageUsername->field_id = $fieldUsername->id;
        $fieldLanguageUsername->language_id = $idLanguage;
        $fieldLanguageUsername->description = 'Titolo';
        $fieldLanguageUsername->created_id = $idUser;
        $fieldLanguageUsername->save();

        $fieldUserRoleUsername = new FieldUserRole();
        $fieldUserRoleUsername->field_id = $fieldUsername->id;
        $fieldUserRoleUsername->role_id = $idRole;
        $fieldUserRoleUsername->enabled = true;
        $fieldUserRoleUsername->table_order = 30;
        $fieldUserRoleUsername->input_type = 0;
        $fieldUserRoleUsername->colspan = 4;
        $fieldUserRoleUsername->required = true;
        $fieldUserRoleUsername->created_id = $idUser;
        $fieldUserRoleUsername->save();


        #region text_message
        $fieldEmail = new Field();
        $fieldEmail->code = 'text_message';
        $fieldEmail->field = 'text_message';
        $fieldEmail->data_type = 1;
        $fieldEmail->tab_id = $tabContentUsers->id;
        $fieldEmail->created_id = $idUser;
        $fieldEmail->save();

        $fieldLanguageEmail = new FieldLanguage();
        $fieldLanguageEmail->field_id = $fieldEmail->id;
        $fieldLanguageEmail->language_id = $idLanguage;
        $fieldLanguageEmail->description = 'Testo del Messaggio';
        $fieldLanguageEmail->created_id = $idUser;
        $fieldLanguageEmail->save();

        $fieldUserRoleEmail = new FieldUserRole();
        $fieldUserRoleEmail->field_id = $fieldEmail->id;
        $fieldUserRoleEmail->role_id = $idRole;
        $fieldUserRoleEmail->enabled = true;
        $fieldUserRoleEmail->table_order = 40;
        $fieldUserRoleEmail->input_type = 9;
        $fieldUserRoleEmail->colspan = 4;
        $fieldUserRoleEmail->required = true;
        $fieldUserRoleEmail->created_id = $idUser;
        $fieldUserRoleEmail->save();
        

       #region telephone_number
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'telephone_number';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'telephone_number';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Numeri a cui inviare';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 50;
        $fieldUserRoleSurnameContactDetail->input_type = 8;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion telephone_number


        #region result_sms
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'result_sms';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'result_sms';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Esito Invio Sms';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 60;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion result_sms

       
        #region date_send_sms
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'date_send_sms';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'date_send_sms';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Data Invio Sms';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 70;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion result_sms


        #region Detail Send SMS
        $DetailContentUsers = new Tab();
        $DetailContentUsers->code = 'DetailSendSms';
        $DetailContentUsers->functionality_id = $functionalityUserSetting;
        $DetailContentUsers->order = 10;
        $DetailContentUsers->created_id = $idUser;
        $DetailContentUsers->save();

        $languageDetailContentUsers = new LanguageTab();
        $languageDetailContentUsers->language_id = $idLanguage;
        $languageDetailContentUsers->tab_id = $DetailContentUsers->id;
        $languageDetailContentUsers->description = 'Dettaglio Sms';
        $languageDetailContentUsers->created_id = $idUser;
        $languageDetailContentUsers->save();


        $fieldUsername = new Field();
        $fieldUsername->code = 'title';
        $fieldUsername->field = 'title';
        $fieldUsername->data_type = 1;
        $fieldUsername->tab_id = $DetailContentUsers->id;
        $fieldUsername->created_id = $idUser;
        $fieldUsername->save();

        $fieldLanguageUsername = new FieldLanguage();
        $fieldLanguageUsername->field_id = $fieldUsername->id;
        $fieldLanguageUsername->language_id = $idLanguage;
        $fieldLanguageUsername->description = 'Titolo';
        $fieldLanguageUsername->created_id = $idUser;
        $fieldLanguageUsername->save();

        $fieldUserRoleUsername = new FieldUserRole();
        $fieldUserRoleUsername->field_id = $fieldUsername->id;
        $fieldUserRoleUsername->role_id = $idRole;
        $fieldUserRoleUsername->enabled = true;
        $fieldUserRoleUsername->table_order = 30;
        $fieldUserRoleUsername->input_type = 0;
        $fieldUserRoleUsername->pos_x = 10;
        $fieldUserRoleUsername->pos_y = 10;
        $fieldUserRoleUsername->colspan = 4;
        $fieldUserRoleUsername->required = true;
        $fieldUserRoleUsername->created_id = $idUser;
        $fieldUserRoleUsername->save();


        #region text_message
       /* $fieldEmail = new Field();
        $fieldEmail->code = 'text_message';
        $fieldEmail->field = 'text_message';
        $fieldEmail->data_type = 1;
        $fieldEmail->tab_id = $DetailContentUsers->id;
        $fieldEmail->created_id = $idUser;
        $fieldEmail->save();

        $fieldLanguageEmail = new FieldLanguage();
        $fieldLanguageEmail->field_id = $fieldEmail->id;
        $fieldLanguageEmail->language_id = $idLanguage;
        $fieldLanguageEmail->description = 'Testo del Messaggio';
        $fieldLanguageEmail->created_id = $idUser;
        $fieldLanguageEmail->save();

        $fieldUserRoleEmail = new FieldUserRole();
        $fieldUserRoleEmail->field_id = $fieldEmail->id;
        $fieldUserRoleEmail->role_id = $idRole;
        $fieldUserRoleEmail->enabled = true;
        $fieldUserRoleEmail->table_order = 40;
        $fieldUserRoleEmail->pos_x = 20;
        $fieldUserRoleEmail->pos_y = 10;
        $fieldUserRoleEmail->input_type = 9;
        $fieldUserRoleEmail->colspan = 4;
        $fieldUserRoleEmail->required = true;
        $fieldUserRoleEmail->created_id = $idUser;
        $fieldUserRoleEmail->save();
        

       #region telephone_number
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'telephone_number';
        $fieldSurnameContactDetail->tab_id = $DetailContentUsers->id;
        $fieldSurnameContactDetail->field = 'telephone_number';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Telefono';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 50;
        $fieldUserRoleSurnameContactDetail->input_type = 8;
         $fieldUserRoleSurnameContactDetail->pos_x = 30;
        $fieldUserRoleSurnameContactDetail->pos_y = 10;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion telephone_number


        #region result_sms
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'result_sms';
        $fieldSurnameContactDetail->tab_id = $DetailContentUsers->id;
        $fieldSurnameContactDetail->field = 'result_sms';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Esito Invio Sms';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 60;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->pos_x = 40;
        $fieldUserRoleSurnameContactDetail->pos_y = 10;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion result_sms

       
        #region date_send_sms
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'date_send_sms';
        $fieldSurnameContactDetail->tab_id = $DetailContentUsers->id;
        $fieldSurnameContactDetail->field = 'date_send_sms';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Data Invio Sms';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 70;
        $fieldUserRoleSurnameContactDetail->pos_x = 20;
        $fieldUserRoleSurnameContactDetail->pos_y = 20;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->colspan = 4;

        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();*/
        #endregion result_sms
       

    }
}

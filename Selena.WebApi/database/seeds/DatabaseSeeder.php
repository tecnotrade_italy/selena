<?php

use Illuminate\Database\Seeder;
use App\Helpers\LogHelper;
use App\VatType;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            RoleSeeder::class,
            PermissionSeeder::class,
            LanguageSeeder::class,
            ContentSeeder::class,
            DictionarySeeder::class,
            DocumentTypeSeeder::class,
            DocumentRowTypeSeeder::class,
            CrmConfigurationSeeder::class,
            ContactSeeder::class,
            CustomerConfigurationSeeder::class,

            MessagesTypesSeeder::class
            //MessagesSeeder::class,
            
            //GroupNewsletterSeeder::class
            /*SettingSeeder::class,
            
            SettingConfigurationSeeder::class,
            ContentConfigurationSeeder::class,
            TemplateEditorSeeder::class,
            TemplateEditorConfigurationSeeder::class,
            VatTypeSeeder::class,
            
            CategoriesSeeder::class,
            UnitMeasureSeeder::class,
            CategoryTypeSeeder::class,
            
            SelenaSqlViewsSeeder::class,
            ProducerSeeder::class,
            PeopleSeeder::class,
            RolesSeeder::class,
            
            MenuAdminSelena::class,
            DefaultDataSeeder::class,
            
            TypeOptionalSeeder::class,
            OptionalSeeder::class,
            
            ItemTableSeeder::class,
            AddTabDatiSeeder::class,
            AddTabSaleItemsSeeder::class,
            AddTabDetailPriceItemsSeeder::class,
            AddTabAttributeSeeder::class,
            AddTranslationSeeder::class,
            
            AddCategoriesTranslationSeeder::class,
            AdvertisingSeeder::class,
            AmazonTemplateSeeder::class,
            
            ZoneSeeder::class, 
            RegionSeeder::class,
            ProvinceSeeder::class,
            NationSeeder::class,
            CitySeeder::class,
            ListConfigurationSeeder::class,
            CitiesDatesSeeder::class*/
        ]);
    }
}

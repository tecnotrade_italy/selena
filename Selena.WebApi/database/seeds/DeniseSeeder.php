<?php

use App\Dictionary;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class DeniseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
  public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $functionalityUserSetting = Functionality::where('code', '=', 'UserSetting')->first()->id;
       // $idTab = Tab::where('code', '=', 'DetailReferent')->first()->id;

        #region TAB USER

        $tabContentUsers = new Tab();
        $tabContentUsers->code = 'UsersReferent';
        $tabContentUsers->functionality_id = $functionalityUserSetting;
        $tabContentUsers->order = 10;
        $tabContentUsers->created_id = $idUser;
        $tabContentUsers->save();

        $languageTabContentUsers = new LanguageTab();
        $languageTabContentUsers->language_id = $idLanguage;
        $languageTabContentUsers->tab_id = $tabContentUsers->id;
        $languageTabContentUsers->description = 'Referenti';
        $languageTabContentUsers->created_id = $idUser;
        $languageTabContentUsers->save();

        #endregion TAB USER

        #region CHECKBOX
        $fieldCheckBoxUsername = new Field();
        $fieldCheckBoxUsername->tab_id = $tabContentUsers->id;
        $fieldCheckBoxUsername->code = 'TableCheckBox';
        $fieldCheckBoxUsername->created_id = $idUser;
        $fieldCheckBoxUsername->save();

        $fieldLanguageCheckBoxUsername = new FieldLanguage();
        $fieldLanguageCheckBoxUsername->field_id = $fieldCheckBoxUsername->id;
        $fieldLanguageCheckBoxUsername->language_id = $idLanguage;
        $fieldLanguageCheckBoxUsername->description = 'CheckBox';
        $fieldLanguageCheckBoxUsername->created_id = $idUser;
        $fieldLanguageCheckBoxUsername->save();

        $fieldUserRoleCheckBoxUsername = new FieldUserRole();
        $fieldUserRoleCheckBoxUsername->field_id = $fieldCheckBoxUsername->id;
        $fieldUserRoleCheckBoxUsername->role_id = $idRole;
        $fieldUserRoleCheckBoxUsername->enabled = true;
        $fieldUserRoleCheckBoxUsername->table_order = 10;
        $fieldUserRoleCheckBoxUsername->input_type = 13;
        $fieldUserRoleCheckBoxUsername->created_id = $idUser;
        $fieldUserRoleCheckBoxUsername->save();
        #endregion CHECKBOX

        #region FORMATTER
        $fieldUserSettingFormatter = new Field();
        $fieldUserSettingFormatter->code = 'UserSettingReferentFormatter';
        $fieldUserSettingFormatter->formatter = 'UserSettingReferentFormatter';
        $fieldUserSettingFormatter->tab_id = $tabContentUsers->id;
        $fieldUserSettingFormatter->created_id = $idUser;
        $fieldUserSettingFormatter->save();

        $fieldLanguageUserSettingFormatter = new FieldLanguage();
        $fieldLanguageUserSettingFormatter->field_id = $fieldUserSettingFormatter->id;
        $fieldLanguageUserSettingFormatter->language_id = $idLanguage;
        $fieldLanguageUserSettingFormatter->description = '';
        $fieldLanguageUserSettingFormatter->created_id = $idUser;
        $fieldLanguageUserSettingFormatter->save();

        $fieldUserRoleUserSettingFormatter = new FieldUserRole();
        $fieldUserRoleUserSettingFormatter->field_id = $fieldUserSettingFormatter->id;
        $fieldUserRoleUserSettingFormatter->role_id = $idRole;
        $fieldUserRoleUserSettingFormatter->enabled = true;
        $fieldUserRoleUserSettingFormatter->table_order = 20;
        $fieldUserRoleUserSettingFormatter->input_type = 14;
        $fieldUserRoleUserSettingFormatter->created_id = $idUser;
        $fieldUserRoleUserSettingFormatter->save();
        #endregion FORMATTER

        #region name
        $fieldUsername = new Field();
        $fieldUsername->code = 'name';
        $fieldUsername->field = 'names';
        $fieldUsername->data_type = 1;
        $fieldUsername->tab_id =$tabContentUsers->id;
        $fieldUsername->created_id = $idUser;
        $fieldUsername->on_change = 'userHelpers.checkExistUsername';
        $fieldUsername->save();

        $fieldLanguageUsername = new FieldLanguage();
        $fieldLanguageUsername->field_id = $fieldUsername->id;
        $fieldLanguageUsername->language_id = $idLanguage;
        $fieldLanguageUsername->description = 'Username';
        $fieldLanguageUsername->created_id = $idUser;
        $fieldLanguageUsername->save();

        $fieldUserRoleUsername = new FieldUserRole();
        $fieldUserRoleUsername->field_id = $fieldUsername->id;
        $fieldUserRoleUsername->role_id = $idRole;
        $fieldUserRoleUsername->enabled = true;
        $fieldUserRoleUsername->table_order = 30;
        $fieldUserRoleUsername->input_type = 0;
        $fieldUserRoleUsername->colspan = 4;
        $fieldUserRoleUsername->required = true;
        $fieldUserRoleUsername->created_id = $idUser;
        $fieldUserRoleUsername->save();
        #endregion name
        #region surname
        $fieldUsername = new Field();
        $fieldUsername->code = 'surname';
        $fieldUsername->field = 'surnames';
        $fieldUsername->data_type = 1;
        $fieldUsername->tab_id = $tabContentUsers->id;
        $fieldUsername->created_id = $idUser;
        $fieldUsername->on_change = 'userHelpers.checkExistUsername';
        $fieldUsername->save();

        $fieldLanguageUsername = new FieldLanguage();
        $fieldLanguageUsername->field_id = $fieldUsername->id;
        $fieldLanguageUsername->language_id = $idLanguage;
        $fieldLanguageUsername->description = 'Cognome';
        $fieldLanguageUsername->created_id = $idUser;
        $fieldLanguageUsername->save();

        $fieldUserRoleUsername = new FieldUserRole();
        $fieldUserRoleUsername->field_id = $fieldUsername->id;
        $fieldUserRoleUsername->role_id = $idRole;
        $fieldUserRoleUsername->enabled = true;
        $fieldUserRoleUsername->table_order = 30;
        $fieldUserRoleUsername->input_type = 0;
        $fieldUserRoleUsername->colspan = 4;
        $fieldUserRoleUsername->required = true;
        $fieldUserRoleUsername->created_id = $idUser;
        $fieldUserRoleUsername->save();
        #endregion surname

        #region phone_number
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'phone_number';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'phone_numbers';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Telefono';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 190;
        $fieldUserRoleSurnameContactDetail->input_type = 8;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion phone_number
    

        #region DETAIL
        #region TAB
        $tabContactDetail = new Tab();
        $tabContactDetail->code = 'DetailReferent';
        $tabContactDetail->functionality_id = $functionalityUserSetting;
        $tabContactDetail->order = 20;
        $tabContactDetail->created_id = $idUser;
        $tabContactDetail->save();

        $languageTabContactDetail = new LanguageTab();
        $languageTabContactDetail->language_id = $idLanguage;
        $languageTabContactDetail->tab_id = $tabContactDetail->id;
        $languageTabContactDetail->description = 'DetailReferent';
        $languageTabContactDetail->created_id = $idUser;
        $languageTabContactDetail->save();
        #endregion TAB

        #region name
        $fieldContactDetailName = new Field();
        $fieldContactDetailName->code = 'name';
        $fieldContactDetailName->tab_id = $tabContactDetail->id;
        $fieldContactDetailName->field = 'name_referent';
        $fieldContactDetailName->data_type = 1;
        $fieldContactDetailName->created_id = $idUser;
        $fieldContactDetailName->save();

        $fieldLanguageContactDetailName = new FieldLanguage();
        $fieldLanguageContactDetailName->field_id = $fieldContactDetailName->id;
        $fieldLanguageContactDetailName->language_id = $idLanguage;
        $fieldLanguageContactDetailName->description = 'Nome';
        $fieldLanguageContactDetailName->created_id = $idUser;
        $fieldLanguageContactDetailName->save();

        $fieldUserRoleContactDetailName = new FieldUserRole();
        $fieldUserRoleContactDetailName->field_id = $fieldContactDetailName->id;
        $fieldUserRoleContactDetailName->role_id = $idRole;
        $fieldUserRoleContactDetailName->enabled = true;
        $fieldUserRoleContactDetailName->pos_x = 10;
        $fieldUserRoleContactDetailName->pos_y = 10;
        $fieldUserRoleContactDetailName->input_type = 1;
        $fieldUserRoleContactDetailName->created_id = $idUser;
        $fieldUserRoleContactDetailName->save();
        #endregion name

        #region surname
        $fieldContactDetailName = new Field();
        $fieldContactDetailName->code = 'surname';
        $fieldContactDetailName->tab_id = $tabContactDetail->id;
        $fieldContactDetailName->field = 'surname_referent';
        $fieldContactDetailName->data_type = 1;
        $fieldContactDetailName->created_id = $idUser;
        $fieldContactDetailName->save();

        $fieldLanguageContactDetailName = new FieldLanguage();
        $fieldLanguageContactDetailName->field_id = $fieldContactDetailName->id;
        $fieldLanguageContactDetailName->language_id = $idLanguage;
        $fieldLanguageContactDetailName->description = 'Cognome';
        $fieldLanguageContactDetailName->created_id = $idUser;
        $fieldLanguageContactDetailName->save();

        $fieldUserRoleContactDetailName = new FieldUserRole();
        $fieldUserRoleContactDetailName->field_id = $fieldContactDetailName->id;
        $fieldUserRoleContactDetailName->role_id = $idRole;
        $fieldUserRoleContactDetailName->enabled = true;
        $fieldUserRoleContactDetailName->pos_x = 20;
        $fieldUserRoleContactDetailName->pos_y = 10;
        $fieldUserRoleContactDetailName->input_type = 1;
        $fieldUserRoleContactDetailName->created_id = $idUser;
        $fieldUserRoleContactDetailName->save();
        #endregion surname

        #region email
        $fieldContactDetailName = new Field();
        $fieldContactDetailName->code = 'email';
        $fieldContactDetailName->tab_id = $tabContactDetail->id;
        $fieldContactDetailName->field = 'email_referent';
        $fieldContactDetailName->data_type = 1;
        $fieldContactDetailName->created_id = $idUser;
        $fieldContactDetailName->save();

        $fieldLanguageContactDetailName = new FieldLanguage();
        $fieldLanguageContactDetailName->field_id = $fieldContactDetailName->id;
        $fieldLanguageContactDetailName->language_id = $idLanguage;
        $fieldLanguageContactDetailName->description = 'Email';
        $fieldLanguageContactDetailName->created_id = $idUser;
        $fieldLanguageContactDetailName->save();

        $fieldUserRoleContactDetailName = new FieldUserRole();
        $fieldUserRoleContactDetailName->field_id = $fieldContactDetailName->id;
        $fieldUserRoleContactDetailName->role_id = $idRole;
        $fieldUserRoleContactDetailName->enabled = true;
        $fieldUserRoleContactDetailName->pos_x = 30;
        $fieldUserRoleContactDetailName->pos_y = 10;
        $fieldUserRoleContactDetailName->input_type = 1;
        $fieldUserRoleContactDetailName->created_id = $idUser;
        $fieldUserRoleContactDetailName->save();
        #endregion surname

        #region phone_number
        $fieldContactDetailName = new Field();
        $fieldContactDetailName->code = 'phone_number';
        $fieldContactDetailName->tab_id = $tabContactDetail->id;
        $fieldContactDetailName->field = 'phone_number_referent';
        $fieldContactDetailName->data_type = 1;
        $fieldContactDetailName->created_id = $idUser;
        $fieldContactDetailName->save();

        $fieldLanguageContactDetailName = new FieldLanguage();
        $fieldLanguageContactDetailName->field_id = $fieldContactDetailName->id;
        $fieldLanguageContactDetailName->language_id = $idLanguage;
        $fieldLanguageContactDetailName->description = 'Telefono';
        $fieldLanguageContactDetailName->created_id = $idUser;
        $fieldLanguageContactDetailName->save();

        $fieldUserRoleContactDetailName = new FieldUserRole();
        $fieldUserRoleContactDetailName->field_id = $fieldContactDetailName->id;
        $fieldUserRoleContactDetailName->role_id = $idRole;
        $fieldUserRoleContactDetailName->enabled = true;
        $fieldUserRoleContactDetailName->pos_x = 40;
        $fieldUserRoleContactDetailName->pos_y = 10;
        $fieldUserRoleContactDetailName->input_type = 8;
        $fieldUserRoleContactDetailName->created_id = $idUser;
        $fieldUserRoleContactDetailName->save();
        #endregion phone_number
        #endregion TABLE
        #endregion Categories
      
    }
}

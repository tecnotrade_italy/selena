<?php

use App\Language;
use App\User;
use Illuminate\Database\Seeder;
use App\Dictionary;

class DictionarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'UnableToConnectToServer';
        $dictionary->description = 'Impossibile collegarsi al server';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Username';
        $dictionary->description = 'Utente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Login';
        $dictionary->description = 'Login';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Error';
        $dictionary->description = 'Errore';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'RememberMe';
        $dictionary->description = 'Resta connesso';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'SignIn';
        $dictionary->description = 'Collegati';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NoServerConnection';
        $dictionary->description = 'Impossibile collegarsi al server';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ThisFieldIsRequired';
        $dictionary->description = 'Campo obbligatorio';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Warning';
        $dictionary->description = 'Attenzione';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'WrongUsernameOrPassword';
        $dictionary->description = 'Utente o Password errata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Account';
        $dictionary->description = 'Profilo';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Logout';
        $dictionary->description = 'Disconnetti';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Menu';
        $dictionary->description = 'Menu';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Page';
        $dictionary->description = 'Pagine';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'MenuMobile';
        $dictionary->description = 'Menu Mobile';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NotificationUnread';
        $dictionary->description = 'Hai {0} notifiche non lette';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewPage';
        $dictionary->description = 'Nuova Pagina';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewLink';
        $dictionary->description = 'Nuovo Link';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewFolder';
        $dictionary->description = 'Nuova Cartella';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Online';
        $dictionary->description = 'Online';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Preview';
        $dictionary->description = 'Anteprima';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Content';
        $dictionary->description = 'Contenuto';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'PageSetting';
        $dictionary->description = 'Impostazioni pagina';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Cancel';
        $dictionary->description = 'Annulla';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Save';
        $dictionary->description = 'Salva';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'SaveAndPublish';
        $dictionary->description = 'Salva e Pubblica';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'CancelNotification';
        $dictionary->description = 'Annullamento eseguito con successo';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'InsertCompleted';
        $dictionary->description = 'Inserimento avvenuto con successo';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'UpdateCompleted';
        $dictionary->description = 'Aggiornamento avvenuto con successo';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'AuthorNotExist';
        $dictionary->description = 'L\'autore non esiste';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'IconNotExist';
        $dictionary->description = 'L\'icona non esiste';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'FunctionNotValued';
        $dictionary->description = 'Funzione non valorizzata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'FunctionNotExist';
        $dictionary->description = 'La funzione non esiste';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'PageCategoryNotExist';
        $dictionary->description = 'Il tipo di categoria pagina non esiste';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'PageShareTypeNotExist';
        $dictionary->description = 'Il tipo di condivisione pagina non esiste';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'LanguageNotValued';
        $dictionary->description = 'La lingua non è valorizzata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'LanguageNotExist';
        $dictionary->description = 'La lingua non esiste';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Language';
        $dictionary->description = 'Lingua';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Select2Empty';
        $dictionary->description = '-';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'RequiredFieldsNotCompleted';
        $dictionary->description = 'Ci sono campi obbligatori non valorizzati o errati';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'PageNotValued';
        $dictionary->description = 'Pagina non valorizzata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'PageNotExist';
        $dictionary->description = 'La pagina non esiste';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'UrlAlreadyExist';
        $dictionary->description = 'Url già presente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'UrlNotFound';
        $dictionary->description = 'Url non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'TitleNotFound';
        $dictionary->description = 'Titolo non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'IdNotValued';
        $dictionary->description = 'Id non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'MenuNotExist';
        $dictionary->description = 'Menu non esistente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'MoreLanguageMenuForSameLanguageMenuFound';
        $dictionary->description = 'Troppi record trovati su LanguageMenu';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'MoreLanguagePageForSameLanguagePageFound';
        $dictionary->description = 'Troppi record trovati su LanguagePage';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'MoreMenuForPageFound';
        $dictionary->description = 'Troppi record trovati su Menu per la Pagina';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Password';
        $dictionary->description = 'Password';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Admin';
        $dictionary->description = 'Admin';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Pages';
        $dictionary->description = 'Pagine';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'CodeNotFound';
        $dictionary->description = 'Codice non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'CodeNotEncoded';
        $dictionary->description = 'Codice non codificato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ValueNotFound';
        $dictionary->description = 'Valora non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'CodeAlreadyExist';
        $dictionary->description = 'Codice già esistente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'SettingNotFound';
        $dictionary->description = 'Setting non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'PleaseEnterXorMoreCharacters';
        $dictionary->description = 'Per favore inserisci {0} o più caratteri';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NoResultsFound';
        $dictionary->description = 'Nessun risultato trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Searching';
        $dictionary->description = 'Caricamento';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ErrorLoading';
        $dictionary->description = 'Errore durante il caricamento';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ModalAskCopyTitle';
        $dictionary->description = 'Vuoi copiare dalla lingua di default:';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ModalAskCopyContent';
        $dictionary->description = 'Il contenuto:';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ModalAskCopySetting';
        $dictionary->description = 'Le impostazioni:';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'UserNotEnabled';
        $dictionary->description = 'Utente non abilitato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'TechincalSpecificationNotFound';
        $dictionary->description = 'Specifica tecnica non trovata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'GroupTechincalSpecificationNotFound';
        $dictionary->description = 'Gruppo specifiche tecniche non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'GroupDisplayTechincalSpecificationNotFound';
        $dictionary->description = 'Gruppo di visualizzazione specifiche tecniche non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'GroupTechincalSpecificationTechincalSpecificationNotFound';
        $dictionary->description = 'Associazione gruppo alle specifiche tecniche non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ItemNotFound';
        $dictionary->description = 'Articolo non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ItemTechnicalSpecificationNotFound';
        $dictionary->description = 'Associazione articolo - specifica tecnica non trovata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ItemGroupTechnicalSpecificationNotFound';
        $dictionary->description = 'Associazione articolo - gruppo specifica tecnica non trovata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'TechnicalSpecification';
        $dictionary->description = 'Specifiche tecniche';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Add';
        $dictionary->description = 'Aggiungi';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewTechnicalSpecification';
        $dictionary->description = 'Nuova specifica tecnica';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'TechnicalSpecificationAlreadyExists';
        $dictionary->description = 'Specifica tecnica già presente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ConfirmSaveRowSelected';
        $dictionary->description = 'Confermi il salvataggio delle righe selezionate?';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'UpdateCompleted';
        $dictionary->description = 'Aggiornamento avvenuto con successo';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'InsertCompleted';
        $dictionary->description = 'Inserimento avvenuto con successo';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ConfirmDeleteRowSelected';
        $dictionary->description = 'Confermi la cancellazione delle righe selezionate?';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'TechnicalSpecificationNotEmpty';
        $dictionary->description = 'La specifica tecnica deve essere valorizzata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Delete';
        $dictionary->description = 'Elimina';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DeleteCompleted';
        $dictionary->description = 'Eliminazione avvenuta con successo';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NoRowSelected';
        $dictionary->description = 'Non ci sono righe selezionate';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'GroupTechnicalSpecification';
        $dictionary->description = 'Gruppi specifiche tecniche';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'GroupTechnicalSpecificationAlreadyExists';
        $dictionary->description = 'Gruppo specifiche tecniche già esistente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'GroupTechnicalSpecificationNotEmpty';
        $dictionary->description = 'Il gruppo specifiche tecniche deve essere valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Groups';
        $dictionary->description = 'Gruppi';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Remove';
        $dictionary->description = 'Rimuovi';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'MoreRowSelected';
        $dictionary->description = 'Troppe righe selezionate';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Included';
        $dictionary->description = 'Inclusi';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Excluded';
        $dictionary->description = 'Esclusi';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'GroupTechnicalSpecificationNotFound';
        $dictionary->description = 'Gruppo specifiche tecniche non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewGroupTechnicalSpecification';
        $dictionary->description = 'Nuovo gruppo specifiche tecniche';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewGroupDisplayTechnicalSpecification';
        $dictionary->description = 'Nuova aggregazione specifiche tecniche';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'GroupsDisplay';
        $dictionary->description = 'Aggregazioni';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'GroupDisplay';
        $dictionary->description = 'Aggregazione';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'GroupDisplayTechnicalSpecification';
        $dictionary->description = 'Aggregazioni specifiche tecniche';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'OrderNotEmpty';
        $dictionary->description = 'L\'ordinamento non può essere vuoto';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'PleaseEnterAValidNumber';
        $dictionary->description = 'Sono consentiti solo numeri';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'GroupDisplayTechnicalSpecificationAlreadyExists';
        $dictionary->description = 'Aggregazione specifiche techniche già presente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'GroupDisplayTechincalSpecificationTechincalSpecificationNotFound';
        $dictionary->description = 'Associazione aggregazione alle specifiche tecniche non trovata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'OrderOnlyNumeric';
        $dictionary->description = 'L\'ordine può contenere solo numeri';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DefaultLanguageRequired';
        $dictionary->description = 'La lingua di default deve essere valorizzata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'TableShowing';
        $dictionary->description = 'Da';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'TableTo';
        $dictionary->description = 'a';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'TableOf';
        $dictionary->description = 'di';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'TableRows';
        $dictionary->description = 'righe';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'TableRowsForPage';
        $dictionary->description = 'righe per pagina';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Items';
        $dictionary->description = 'Articoli';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Sorting';
        $dictionary->description = 'Ordinamento';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Group';
        $dictionary->description = 'Gruppo';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Item';
        $dictionary->description = 'Articolo';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Items';
        $dictionary->description = 'Articoli';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'History';
        $dictionary->description = 'Storico';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Last';
        $dictionary->description = 'Ultima';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Version';
        $dictionary->description = 'Versione';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'GroupDisplayTechnicalSpecificationNotEmpty';
        $dictionary->description = 'Aggregazione specifiche techniche deve essere valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Search';
        $dictionary->description = 'Cerca';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'OpenDetail';
        $dictionary->description = 'Visualizza il dettaglio';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Label';
        $dictionary->description = 'Etichette';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Duplicate';
        $dictionary->description = 'Duplica';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Description';
        $dictionary->description = 'Descrizione';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Code';
        $dictionary->description = 'Codice';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'InfoTechnicalSpecification';
        $dictionary->description = 'Per segnare una specifica come inutilizzata, utilizzare il carattere #';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Users';
        $dictionary->description = 'Utenti';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'User';
        $dictionary->description = 'Utente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'EmailNotValid';
        $dictionary->description = 'Email non valida';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'UsernameNotValued';
        $dictionary->description = 'L\'username deve essere valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'PasswordNotValued';
        $dictionary->description = 'La password deve essere valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'UsernameAlreadyExist';
        $dictionary->description = 'Username già esistente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'EmailAlreadyExist';
        $dictionary->description = 'Email già esistente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'UserNotFound';
        $dictionary->description = 'Utente non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewUser';
        $dictionary->description = 'Nuovo utente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Picking';
        $dictionary->description = 'Prelievo';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'OCList';
        $dictionary->description = 'Lista OC';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Customer';
        $dictionary->description = 'Cliente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Order';
        $dictionary->description = 'Ordine';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'StartReading';
        $dictionary->description = 'Avvia lettura';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'EscapeFrom';
        $dictionary->description = 'Da evadere';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ToEscape';
        $dictionary->description = 'Da evadere';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Reading';
        $dictionary->description = 'Lettura';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'OrderQuantity';
        $dictionary->description = 'Quantità ordinata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'OrderPicking';
        $dictionary->description = 'Quantità prelevata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Package';
        $dictionary->description = 'Collo';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Back';
        $dictionary->description = 'Indietro';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Close';
        $dictionary->description = 'Chiudi';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ConfirmClose';
        $dictionary->description = 'Confermi la chiusura?';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ConfirmSave';
        $dictionary->description = 'Confermi il salvataggio?';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ConfirmCancel';
        $dictionary->description = 'Confermi l\'annullamento?';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Read';
        $dictionary->description = 'Leggi';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'EnableScanner';
        $dictionary->description = 'Attiva scanner';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DisableScanner';
        $dictionary->description = 'Disattiva scanner';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'AddPackage';
        $dictionary->description = 'Aggiungi collo';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ConfirmAddPackage';
        $dictionary->description = 'Confermi l\'aggiunta del collo?';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DocumentRowNotExist';
        $dictionary->description = 'La riga del documento non esiste';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Shipments';
        $dictionary->description = 'Spedizioni';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Sent';
        $dictionary->description = 'Invio';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'January';
        $dictionary->description = 'Gennaio';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'February';
        $dictionary->description = 'Febbraio';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'March';
        $dictionary->description = 'Marzo';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'April';
        $dictionary->description = 'Aprile';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'May';
        $dictionary->description = 'Maggio';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'June';
        $dictionary->description = 'Giugno';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'July';
        $dictionary->description = 'Luglio';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'August';
        $dictionary->description = 'Agosto';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'September';
        $dictionary->description = 'Settembre';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'October';
        $dictionary->description = 'Ottobre';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'November';
        $dictionary->description = 'Novembre';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'December';
        $dictionary->description = 'Dicembre';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Month';
        $dictionary->description = 'Mese';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'PackagingTypeSixten';
        $dictionary->description = 'Aspetto dei beni';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Weight';
        $dictionary->description = 'Peso';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Carriage';
        $dictionary->description = 'Porto';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Carrier';
        $dictionary->description = 'Vettore';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'WarningCarrierChangeSixten';
        $dictionary->description = 'Attenzione il vettore non è quello del cliente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'WarningCarriageChangeSixten';
        $dictionary->description = 'Attenzione il porto non è quello del cliente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ShipmentCodeSixten';
        $dictionary->description = 'Codici di spedizione';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DocumentHeadNotExist';
        $dictionary->description = 'Documento non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'MoreDocumentFound';
        $dictionary->description = 'Trovati più documenti';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'News';
        $dictionary->description = 'News';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Blog';
        $dictionary->description = 'Blog';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ItemAlreadyPicked';
        $dictionary->description = 'Articolo già prelevato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'QuantityPickedMoreThatQuantityOrder';
        $dictionary->description = 'La quantità prelevata è maggiore della quantità ordinata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'UserNotEmpty';
        $dictionary->description = 'Utente non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'RoleNotEmpty';
        $dictionary->description = 'Gruppo non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'RoleNotFound';
        $dictionary->description = 'Gruppo non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewNews';
        $dictionary->description = 'Nuova News';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewBlog';
        $dictionary->description = 'Nuovo articolo di Blog';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'LastSync';
        $dictionary->description = 'Ultima sincronizzazione';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ItemsNotConfigured';
        $dictionary->description = 'Articoli non configurati';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Configurations';
        $dictionary->description = 'Configurazioni';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewConfiguration';
        $dictionary->description = 'Nuova configurazione';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ContentNotDecode';
        $dictionary->description = 'Contenuto non codificato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ContentNotFound';
        $dictionary->description = 'Contenuto non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ContentMismatch';
        $dictionary->description = 'Contenuto già codificato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ContentAlreadyExists';
        $dictionary->description = 'Contenuto già esistente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Contents';
        $dictionary->description = 'Contenuti';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'FileTypeNotAllowed';
        $dictionary->description = 'Tipo file non accettato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NameFileAlreadyExists';
        $dictionary->description = 'Nome file già presente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'CantDeletedImage';
        $dictionary->description = 'Impossibile cancellare l\'immagine';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DescriptionNotFound';
        $dictionary->description = 'Descrizione non trovata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'TemplateEditorTypeNotFound';
        $dictionary->description = 'Tipo blocco non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'TemplateEditorGroupNotFound';
        $dictionary->description = 'Gruppo blocco non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'TemplateEditorNotFound';
        $dictionary->description = 'Blocco non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ImageNotFound';
        $dictionary->description = 'Immagine non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'HtmlNotFound';
        $dictionary->description = 'Html non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'TemplateEditor';
        $dictionary->description = 'Gestione blocchi';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewTemplateEditor';
        $dictionary->description = 'Nuovo blocco';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewTemplateEditorGroup';
        $dictionary->description = 'Nuovo gruppo dei blocchi';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'TemplateEditorGroup';
        $dictionary->description = 'Gruppi blocchi';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Setting';
        $dictionary->description = 'Impostazioni';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ManageStyle';
        $dictionary->description = 'Gestione css';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Newsletter';
        $dictionary->description = 'Newsletter';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'campaigns';
        $dictionary->description = 'Campagne';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'campaign';
        $dictionary->description = 'Campagna';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'CloneCompleted';
        $dictionary->description = 'Copia avvenuta con successo';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NameNotValued';
        $dictionary->description = 'Nome non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'SurnameNotValued';
        $dictionary->description = 'Cognome non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'EmailNotValued';
        $dictionary->description = 'Email non valorizzata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'RequestNotValued';
        $dictionary->description = 'Richiesta non valorizzata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ContactRequestCompleted';
        $dictionary->description = 'Richiesta di contatto inserita correttamente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'CRM';
        $dictionary->description = 'CRM';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'RequestContact';
        $dictionary->description = 'Richieste contatto';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ContactNotFound';
        $dictionary->description = 'Contatto non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'GroupNewsletterNotFound';
        $dictionary->description = 'Gruppo newsletter non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DescriptionNotValued';
        $dictionary->description = 'Descrizione non valorizzata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ContactGroupNewsletterNotFound';
        $dictionary->description = 'Contatti gruppo newsletter non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ContactNotValued';
        $dictionary->description = 'Contatto non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ContactRequestNotFound';
        $dictionary->description = 'Contatto richista non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'CompanyNotValued';
        $dictionary->description = 'Ditta non trovata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewCustomer';
        $dictionary->description = 'Nuovo cliente';
        $dictionary->created_id = $idUser;
        $dictionary->save();


        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Nation';
        $dictionary->description = 'Nazioni';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'VatType';
        $dictionary->description = 'Iva';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewsletterNotFound';
        $dictionary->description = 'Newsletter non trovata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewsletterNotValued';
        $dictionary->description = 'Newsletter non valorizzata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'CustomerNotFound';
        $dictionary->description = 'Cliente non valorizzata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'CustomerNotValued';
        $dictionary->description = 'Cliente non valorizzata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'GroupNewsletterNotValued';
        $dictionary->description = 'Gruppo newsletter non valorizzata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewsletterRecipientNotFound';
        $dictionary->description = 'Contatto newsletter non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'PostalCode';
        $dictionary->description = 'Codice Postale';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Customers';
        $dictionary->description = 'Clienti';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Contacts';
        $dictionary->description = 'Contatti';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Feedback';
        $dictionary->description = 'Resoconto';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Campaign';
        $dictionary->description = 'Campagna';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'NewCampaign';
        $dictionary->description = 'Nuova campagna';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ViewNotSet';
        $dictionary->description = 'View Non settata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'GoogleAnalyticsViewNotFound';
        $dictionary->description = 'Google Analytics View non valorizzata';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Complete';
        $dictionary->description = 'Completa';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'BackGroundVisitorsViewsEmpty';
        $dictionary->description = 'Colore di sfondo di Google Analytics pagine visitatori non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'BorderVisitorsViewsEmpty';
        $dictionary->description = 'Linea di Google Analytics visitatori non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'BackGroundPagesViewsEmpty';
        $dictionary->description = 'Colore di sfondo di Google Analytics pagine visitate non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'BorderPagesViewsEmpty';
        $dictionary->description = 'Linea di Google Analytics pagine visitate non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'BackGroundDeviceSessionEmpty';
        $dictionary->description = 'Colore di sfondo di Google Analytics sessioni devi non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'BackGroundSourceSessionEmpty';
        $dictionary->description = 'Colore di sfondo di Google Analytics sorgente sessioni non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DashboardSession';
        $dictionary->description = 'Sessioni';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DashboardPagesViews';
        $dictionary->description = 'Pagine viste';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DashboardPagesPerSession';
        $dictionary->description = 'Pagine per sessione';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DashboardAvgSession';
        $dictionary->description = 'Sessione media';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DashboardBounceRate';
        $dictionary->description = 'Frequenza di rimbalzo';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DashboardRealTimeUser';
        $dictionary->description = 'Utenti attivi in tempo reale';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DashboardViews';
        $dictionary->description = 'Visite';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DashboardSource';
        $dictionary->description = 'Sorgente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DashboardVisitors';
        $dictionary->description = 'Visitatori';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'DashboardTecnology';
        $dictionary->description = 'Tecnologia';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'UnsubscribeContactCompleted';
        $dictionary->description = 'Disiscrizione avvenuta correttamente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ContactAlreadyExists';
        $dictionary->description = 'Contatto già esistente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'SubscribeContactCompleted';
        $dictionary->description = 'Iscrizione avvenuta correttamente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

         $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'VatNumberAlreadyExist';
        $dictionary->description = 'Partita Iva già esistente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'FiscalCodeAlreadyExist';
        $dictionary->description = 'Codice Fiscale già esistente';
        $dictionary->created_id = $idUser;
        $dictionary->save();

















    }
}

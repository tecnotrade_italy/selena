<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\MenuAdmin;
use App\LanguageMenuAdmin;
use App\MenuAdminUserRole;
use App\Language;
use App\Icon;
use App\Functionality;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Message;
use App\MessageLanguage;
use App\User;
use App\Helpers\LogHelper;
use App\SelenaSqlViews;
use App\Content;
use App\ContentLanguage;
use App\Region;
use App\Setting;

class DefaultDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        /*****************************************************************************/
        /* SQL VIEWS */
        /*****************************************************************************/
        $sqlView = SelenaSqlViews::firstOrCreate(['name_view'   => 'carts_details'],
        [ 
            'description' => 'Righe dettaglio carrello',
            'select' => '*',
            'from' => 'carts_details',
            'created_id' => $idUser
        ]);

        $sqlView = SelenaSqlViews::firstOrCreate(['name_view'   => 'salesman'],
        [ 
            'description' => 'Agenti',
            'select' => '*',
            'from' => 'salesman',
            'created_id' => $idUser
        ]);

        $sqlView = SelenaSqlViews::firstOrCreate(['name_view'   => 'carts'],
        [ 
            'description' => 'Sommario carrello',
            'select' => '*',
            'from' => 'carts',
            'created_id' => $idUser
        ]);
        
        $sqlView = SelenaSqlViews::firstOrCreate(['name_view'   => 'sales_orders_details'],
        [ 
            'description' => 'Righe dettaglio ordine vendita',
            'select' => '*',
            'from' => 'sales_orders_details',
            'created_id' => $idUser
        ]);

        $sqlView = SelenaSqlViews::firstOrCreate(['name_view'   => 'sales_orders'],
        [ 
            'description' => 'Ordine vendita',
            'select' => '*',
            'from' => 'sales_orders',
            'created_id' => $idUser
        ]);

        $sqlView = SelenaSqlViews::firstOrCreate(['name_view'   => 'View_UsersDatas'],
        [ 
            'description' => 'View per dati utenti',
            'select' => '*',
            'from' => 'users_datas',
            'created_id' => $idUser
        ]);

        $sqlView = SelenaSqlViews::firstOrCreate(['name_view'   => 'View_UsersAddresses'],
        [ 
            'description' => 'View tabella indirizzi di spedizione',
            'select' => '*',
            'from' => 'users_addresses',
            'created_id' => $idUser
        ]);

        $sqlView = SelenaSqlViews::firstOrCreate(['name_view'   => 'View_BlogNews'],
        [ 
            'description' => 'View per estrazione articoli di blog e news',
            'select' => 'pages.online, pages.visible_from, pages.page_type, languages_pages.title as titolo, languages_pages."content", languages_pages.url, languages_pages.seo_title as titoloseo, languages_pages.seo_description as descrizioneseo,languages_pages.share_title as titoloshare, languages_pages.share_description as descrizioneshare,languages_pages.url_cover_image,users.name as Autore, languages_page_categories.description as categoria',
            'from' => 'pages INNER JOIN languages_pages on pages.id = languages_pages.page_id left outer join users on pages.author_id = users.id left outer join languages_page_categories on languages_page_categories.page_category_id = pages.page_category_id ',
            'created_id' => $idUser
        ]);

        $sqlView = SelenaSqlViews::firstOrCreate(['name_view'   => 'View_Articoli'],
        [ 
            'description' => 'Query per estrazione articoli',
            'select' => '*',
            'from' => 'items',
            'where' => 'online = t',
            'created_id' => $idUser
        ]);
          $sqlView = SelenaSqlViews::firstOrCreate(['name_view'   => 'View_Categorie'],
        [ 
            'description' => 'Query per estrazione categorie',
            'select' => '*',
            'from' => 'categories
            INNER JOIN categories_languages ON categories.id = categories_languages.category_id',
            'created_id' => $idUser
        ]);
          	
             $sqlView = SelenaSqlViews::firstOrCreate(['name_view'   => 'sales_order_tokenlink_user'],
        [ 
            'description' => 'tabella ordini lista con token utente per acquisto corso fotografia',
            'select' => '*',
            'from' => 'sales_order_tokenlink_user',
            'created_id' => $idUser
        ]);
          $sqlView = SelenaSqlViews::firstOrCreate(['name_view'   => 'Contatti'],
        [ 
            'description' => 'Contatti',
            'select' => '*',
            'from' => 'contacts',
            'created_id' => $idUser
        ]);
        $sqlView = SelenaSqlViews::firstOrCreate(['name_view'   => 'community_images'],
        [ 
            'description' => 'Immagini della community',
            'select' => '*',
            'from' => 'community_images',
            'created_id' => $idUser
        ]);
        /*****************************************************************************/
        /* MESSAGGI */
        /*****************************************************************************/
        $message = Message::updateOrCreate(['code'   => 'ORDER_CONFIRMATION_MAIL_TEXT'],
        [ 
            'message_type_id' => 3,
            'created_id' => $idUser
        ]);

        $messageLanguage = MessageLanguage::firstOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div style="width:100%;background:#ededed;margin:0 !important;padding:20px 20px 60px 20px;">
            <div style="text-align:center;"><img src="https://selena.tecnotrade.com/api/storage/images/logo.png" style="width: 350px;" class="fr-fic fr-dii"></div>
            <table align="center" style="border-collapse:collapse; background:#fff;border-radius:.4rem; margin:1rem auto; font-family: Open Sans, Arial, sans-serif !important; font-size:16px;" width="640">
                <tbody>
                    <tr>
                        <td>
                            <div style="background-size: cover; background: url(https://selena.tecnotrade.com/api/storage/images/pattern_lightblue.png) #0e2a39; border-radius:.4rem .4rem 0 0; max-height: 75px;height: 75px; width: 640px; position: relative;"><img src="https://selena.tecnotrade.com/api/storage/images/ico_pacco.png" style="width:45px; margin-top: 15px; margin-left: 50px; display: inline-block;" class="fr-fic fr-dii">
        
                                <p style="color: #fff;font-size: 23px; position: absolute; left: 120px; display: inline-block;">Riepilogo Ordine&nbsp;</p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="padding:40px;color:#5a5a5a;">
                                <p>Grazie, l&#39;ordine &egrave; stato registrato.
                                    <br>Di seguito trovi tutte le informazioni relative al tuo ordine.</p>
                                <p style="border-bottom: 1px solid #f1f3f5;">Data Ordine<span style="float: right;">#date_order_formatter#</span></p>
                                <p style="border-bottom: 1px solid #f1f3f5;">Numero Ordine<span style="float: right;">#id#</span></p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br>
            <br>
            <table align="center" style="border-collapse:collapse; background:#fff;border-radius:.4rem; margin-top:1rem; font-family: Open Sans, Arial, sans-serif !important; font-size:16px;" width="640">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>
                            <div style="background-size: cover; background: url(https://selena.tecnotrade.com/api/storage/images/pattern_lightyellow.png) #846321; border-radius:.4rem .4rem 0 0; max-height: 75px;height: 75px; width: 640px; position: relative;"><img src="https://selena.tecnotrade.com/api/storage/images/ico_cartellino.png" style="width:45px; margin-top: 15px; margin-left: 50px; display: inline-block;" class="fr-fic fr-dii">
        
                                <p style="color: #fff;font-size: 23px; position: absolute; left: 120px; display: inline-block;">Dettagli Ordine<span style="margin-left:180px;">Nr. #id#</span></p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="padding:40px 40px 0px 40px;color:#5a5a5a;">#sales_order_detail_single_row#</div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style="padding:40px;color:#5a5a5a;">
                                <p style="margin:0;">Subtotale:<span style="float:right;">&euro; #net_amount#</span></p>
                                <p style="margin:0;">Imposta:<span style="float:right;">&euro; #vat_amount#</span></p>
                                <p style="font-size:18px; font-weight:bold; margin-top:10px;">Totale:<span style="float:right;">&euro; #total_amount#</span></p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <table align="center" style="border-collapse:collapse; background:#f1f3f5;border-radius:.4rem; margin-top:1rem; font-family: Open Sans, Arial, sans-serif !important; font-size:16px;" width="640">
                <tbody>
                    <tr>
                        <td style="text-align:center;">
                            <a href="https://selena.tecnotrade.com/"><img src="https://selena.tecnotrade.com/api/storage/images/logo.png" style="width: 250px; text-align:center;" class="fr-fic fr-dib"></a>
                            <br>
                           <!-- <p style="margin:0;font-family: Open Sans, Arial, sans-serif !important; font-size:13px; color:#5a5a5a;text-align:center;"><em>&copy; COPYRIGHT 2020 - Selena</em></p>
                            <p style="margin:0;font-family: Open Sans, Arial, sans-serif !important; font-size:13px; color:#5a5a5a;text-align:center;"><em>Via Giulio Cesare Croce, 18, 40017 San Giovanni in Persiceto BO</em></p>
                            <p style="margin:0;font-family: Open Sans, Arial, sans-serif !important; font-size:13px; color:#5a5a5a; text-align:center;"><em>TUTTI I DIRITTI SONO RISERVATI</em></p> -->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>',
            'created_id' => $idUser
        ]);

        $message = Message::updateOrCreate(['code'   => 'ORDER_CONFIRMATION_MAIL_SUBJECT'],
        [ 
            'message_type_id' => 1,
            'created_id' => $idUser
        ]);

        $messageLanguage = MessageLanguage::firstOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => 'Oggetto mail di conferma ordine',
            'created_id' => $idUser
        ]);



        $message = Message::updateOrCreate(['code'   => 'REGISTRATION_CONFIRM_MAIL_TEXT'],
        [ 
            'message_type_id' => 3,
            'created_id' => $idUser
        ]);

        $messageLanguage = MessageLanguage::firstOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<p>Ciao #name#,</p><p>Grazie per esserti registrato al nostro sito, per completare la registrazione &egrave; necessario cliccare sul link seguente: <a href="#url#/confirmregistration?i=#id#">Clicca Qui</a></p>',
            'created_id' => $idUser
        ]);

        $message = Message::updateOrCreate(['code'   => 'REGISTRATION_CONFIRM_MAIL_OBJECT'],
        [ 
            'message_type_id' => 3,
            'created_id' => $idUser
        ]);

        $messageLanguage = MessageLanguage::firstOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => 'Conferma Registrazione',
            'created_id' => $idUser
        ]);



        $message = Message::updateOrCreate(['code'   => 'USER_RESCUE_PASSWORD_TEXT'],
        [ 
            'message_type_id' => 3,
            'created_id' => $idUser
        ]);

        $messageLanguage = MessageLanguage::firstOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div style="width:950px; margin:0 auto;"><table cellpadding="5" cellspacing="5" style="margin:0 0 1rem 0;" width="100%"><tbody><tr><td height="1" width="1"><br></td><td height="1"><br></td><td height="1" width="1"><br></td></tr><tr><td width="1"><br></td><td><table cellpadding="5" cellspacing="5" style="margin:0 0 1rem 0;" width="100%"><tbody><tr align="center"><td colspan="2"><h1>Ciao #name#,<br>Cliccando sul link sotto potrai resettare la tua password<br><a href="#url#/resetpassword?i=#id#">cambia password</a></h1></td></tr></tbody></table></td><td width="1"><br></td></tr><tr><td height="1" width="1"><br></td><td height="1"><br></td><td height="1" width="1"><br></td></tr></tbody></table></div>',
            'created_id' => $idUser
        ]);

        $message = Message::updateOrCreate(['code'   => 'USER_RESCUE_PASSWORD_SUBJECT'],
        [ 
            'message_type_id' => 1,
            'created_id' => $idUser
        ]);

        $messageLanguage = MessageLanguage::firstOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => 'Recupera Password',
            'created_id' => $idUser
        ]);

        $message = Message::updateOrCreate(['code'   => 'EMPTY_CART'],
        [ 
            'message_type_id' => 1,
            'created_id' => $idUser
        ]);

        $messageLanguage = MessageLanguage::firstOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="container"><div class="row"><div class="col-12 text-center" style="padding-top:3rem;padding-bottom:3rem;"><h1>Carrello vuoto</h1><br><a href="/">&nbsp;vai allo shop</a></div></div></div>',
            'created_id' => $idUser
        ]);
        

         $message = Message::updateOrCreate(['code'   => 'ORDER_CONFIRM_SUMMARY_MAIL_TEXT'],
        [ 
            'message_type_id' => 1,
            'created_id' => $idUser
        ]);

        $messageLanguage = MessageLanguage::firstOrCreate(
        [
            'message_id'   => $message->id, 
            'language_id' => $idLanguage
        ],
        [
           'content'     => '<div style="width:100%;background:#ededed;margin:0 !important;padding:20px 20px 60px 20px;">
           <div style="text-align:center;"><img src="https://selena.tecnotrade.com/api/storage/images/logo.png" style="width: 350px;" class="fr-fic fr-dii"></div>
           <table align="center" style="border-collapse:collapse; background:#fff;border-radius:.4rem; margin:1rem auto; font-family: Arial, sans-serif !important; font-size:16px;" width="640">
               <tbody>
                   <tr>
                       <td>
                           <div style="background-size: cover; background: url(https://selena.tecnotrade.com/api/storage/images/pattern_lightblue.png) #0e2a39; border-radius:.4rem .4rem 0 0; max-height: 75px;height: 75px; width: 640px; position: relative;"><img src="https://selena.tecnotrade.com/api/storage/images/ico_pacco.png" style="width:45px; margin-top: 15px; margin-left: 50px; display: inline-block;" class="fr-fic fr-dii">
                               <p style="color: #fff;font-size: 23px; position: absolute; left: 120px; display: inline-block;">Riepilogo Ordine <span style="margin-left:180px;">Nr. #id#</span></p>
                           </div>
                       </td>
                   </tr>
                   <tr>
                       <td>
                           <div style="padding:40px;color:#5a5a5a;">
                               <p>Grazie, l&#39;ordine &egrave; stato registrato.
                                   <br>Di seguito trovi tutte le informazioni relative al tuo ordine.</p>
       
                               <p style="border-bottom: 1px solid #f1f3f5;">Data Ordine<span style="float: right;">#SalesOrder_date_order#</span></p>
       
                               <p style="border-bottom: 1px solid #f1f3f5;">Numero Ordine<span style="float: right;">#id#</span></p>
                           </div>
                       </td>
                   </tr>
               </tbody>
           </table>
       
           <table align="center" style="border-collapse:collapse; background:#fff;border-radius:.4rem; margin-top:1rem; font-family:  Arial, sans-serif !important; font-size:16px;" width="640">
               <tbody>
                   <tr>
                       <td>
                           <div style="background-size: cover; background: url(https://selena.tecnotrade.com/api/storage/images/pattern_lightgreen.png) #363d1f; border-radius:.4rem .4rem 0 0; max-height: 75px;height: 75px; width: 640px; position: relative;"><img src="https://selena.tecnotrade.com/api/storage/images/ico_spedito.png" style="width:45px; margin-top: 15px; margin-left: 50px; display: inline-block;" class="fr-fic fr-dii">
       
                               <p style="color: #fff;font-size: 23px; position: absolute; left: 120px; display: inline-block;">Spedizione</p>
                           </div>
                       </td>
                   </tr>
                   <tr>
                       <td>
                           <div style="padding:40px;color:#5a5a5a;">
       
                               <p>Spedizione affidata a: <span style="color:#383838;">#SalesOrder_carrier_id#</span><span style="margin-left:15px;">Tracking number: #SalesOrder_tracking_shipment#</span></p>
                               <div style="padding:10px 20px; background:#f1f3f5; color:#5a5a5a;">
       
                                   <p>Indirizzo di consegna:</p>
       
                                   <p>#UserAddressGood_name# #UserAddressGood_surname#</p>
       
                                   <p style="margin:0;">#UserAddressGoodAddress#</p>
       
                                   <p style="margin:0;">#UserAddressGoodPostal_code# #UserAddressGoodLocality# (#UserAddressGoodProvince_id#)
                                       <br>Tel: #UserAddressGoodPhone_number#</p>
                               </div></div>
                       </td>
                   </tr>
               </tbody>
           </table>
       
           <table align="center" style="border-collapse:collapse; background:#fff;border-radius:.4rem; margin-top:1rem; font-family: Arial, sans-serif !important; font-size:16px;" width="640">
               <tbody>
                   <tr>
                       <td>
                           <div style="background-size: cover; background: url(https://selena.tecnotrade.com/api/storage/images/pattern_lightyellow.png) #846321; border-radius:.4rem .4rem 0 0; max-height: 75px;height: 75px; width: 640px; position: relative;"><img src="https://selena.tecnotrade.com/api/storage/images/ico_cartellino.png" style="width:45px; margin-top: 15px; margin-left: 50px; display: inline-block;" class="fr-fic fr-dii">
       
                               <p style="color: #fff;font-size: 23px; position: absolute; left: 120px; display: inline-block;">Dettagli Ordine<span style="margin-left:180px;">Nr. #id#</span></p>
                           </div>
                       </td>
                   </tr>
                   <tr>
                       <td>
                           <div style="padding:40px 40px 0px 40px;color:#5a5a5a;">#DetailMailTableOrderSales#</div>
                       </td>
                   </tr>
                   <tr>
                       <td>
                           <div style="padding:40px;color:#5a5a5a;">
       
                               <p style="margin:0;">Subtotale:<span style="float:right;">&euro; #SalesOrder_net_amount#</span></p>
       
                               <p style="margin:0;">Spese di Spedizione:<span style="float:right;">&euro; #SalesOrder_shipment_amount#</span></p>
       
                               <p style="margin:0;">Spese di Pagamento:<span style="float:right;">&euro; #SalesOrder_payment_cost#</span></p>
       
                               <p style="margin:0;">Sconto<span style="float:right;">&euro; #SalesOrder_vat_amount#</span></p>
       
                               <p style="margin:0;">Imposta:<span style="float:right;">&euro; #SalesOrder_discount_value#</span></p>
       
                               <p style="font-size:18px; font-weight:bold; margin-top:10px;">Totale:<span style="float:right;">&euro; #SalesOrder_total_amount#</span></p>
                           </div>
                       </td>
                   </tr>
               </tbody>
           </table>
       
           <table align="center" style="border-collapse:collapse; background:#f1f3f5;border-radius:.4rem; margin-top:1rem; font-family: Arial, sans-serif !important; font-size:16px;" width="640">
               <tbody>
                   <tr>
                       <td style="text-align:center;">
                           <a href="https://selena.tecnotrade.com/"><img src="https://selena.tecnotrade.com/api/storage/images/logo.png" style="width: 250px; text-align:center;" class="fr-fic fr-dib"></a>
                           <br>
       
                          <!-- <p style="margin:0;font-family: Arial, sans-serif !important; font-size:13px; color:#5a5a5a;text-align:center;"><em>&copy; COPYRIGHT 2020 - Selena</em></p>
       
                           <p style="margin:0;font-family: Arial, sans-serif !important; font-size:13px; color:#5a5a5a;text-align:center;"><em>Via Giulio Cesare Croce, 18, 40017 San Giovanni in Persiceto</em></p>
       
                           <p style="margin:0;font-family: Arial, sans-serif !important; font-size:13px; color:#5a5a5a; text-align:center;"><em>TUTTI I DIRITTI SONO RISERVATI</em></p> -->
                       </td>
                   </tr>
               </tbody>
           </table>
       </div>
       ',
            'created_id' => $idUser 
        ]);



        
    $message = Message::updateOrCreate(['code'   => 'ORDER_CONFIRM_TRACKING_SHIPMENT_MAIL_OBJECT'],
            [ 
                'message_type_id' => 1,
                'created_id' => $idUser
            ]);

            $messageLanguage = MessageLanguage::firstOrCreate(
            [
                'message_id'   => $message->id, 
                'language_id' => $idLanguage
            ],
            [
                'content'     => '<p>I dettagli del tuo ordine</p>',
                'created_id' => $idUser
            ]);

            $message = Message::updateOrCreate(['code'   => 'ORDER_CONFIRM_TRACKING_SHIPMENT_MAIL_TEXT'],
            [ 
                'message_type_id' => 1,
                'created_id' => $idUser
            ]);

            $messageLanguage = MessageLanguage::firstOrCreate(
            [
                'message_id'   => $message->id, 
                'language_id' => $idLanguage
            ],
            [
                'content'     => '<div style="width:100%;background:#ededed;margin:0 !important;padding:20px 20px 60px 20px;m text-align:center;"><img src="https://selena.tecnotrade.com/api/storage/images/logo.png" style="width:350px;" class="fr-fic fr-dib">
                <table align="center" style="border-collapse:collapse; background:#fff;border-radius:.4rem; margin-top:1rem; font-family: Open Sans, Arial, sans-serif !important; font-size:16px;" width="640">
                    <tbody>
                        <tr>
                            <td>
                                <div style="background: url(https://selena.tecnotrade.com/api/storage/images/pattern_lightgreen.png) #363d1f; background-size:cover; border-radius:.4rem .4rem 0 0; max-height: 75px;height: 75px; width: 640px; position: relative;"><img src="https://selena.tecnotrade.com/api/storage/images/ico_spedito.png" style="width:45px; margin-top: 15px; margin-left: 50px; display: inline-block;" class="fr-fic fr-dii">
                                    <p style="color: #fff;font-size: 23px; position: absolute; left: 120px; display: inline-block;">Spedizione Ordine <span style="margin-left:180px;">Nr. #id#</span></p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="padding:40px;color:#5a5a5a;">
                                    <div style="padding:10px 20px; background:#f1f3f5; color:#5a5a5a;">
            
                                        <p>Spedizione affidata a: <span style="color:#383838;">#SalesOrder_carrier_id#</span></p>
            
                                        <p>Tracking number: <span style="color:#383838;">#SalesOrder_tracking_shipment#</span></p>
            
                                        <p style="margin:0;">#tracking_url##SalesOrder_tracking_shipment#</p>
            
                                        <p>Indirizzo di consegna:</p>
            
                                        <p>#UserAddressGood_name# #UserAddressGood_surname#</p>
            
                                        <p style="margin:0;">#UserAddressGoodAddress#</p>
            
                                        <p>#UserAddressGoodBusiness_name#</p>
            
                                        <p style="margin:0;">#UserAddressGoodPostal_code# #UserAddressGoodLocality# (#UserAddressGoodProvince_id#)
                                            <br>Tel: #UserAddressGoodPhone_number#</p>
                                    </div></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            
                <table align="center" style="border-collapse:collapse; background:#f1f3f5;border-radius:.4rem; margin-top:1rem; font-family: Open Sans, Arial, sans-serif !important; font-size:16px;" width="640">
                    <tbody>
                        <tr>
                            <td style="text-align:center;">
                                <a href="https://selena.tecnotrade.com/"><img src="https://selena.tecnotrade.com/api/storage/images/logo.png" style="width: 250px; text-align:center;" class="fr-fic fr-dib"></a>
                                <br>
            
                                <p style="margin:0;font-family: Open Sans, Arial, sans-serif !important; font-size:13px; color:#5a5a5a;text-align:center;"><em>&copy; COPYRIGHT 2020 - Selena divisione commerciale</em></p>
            
                                <p style="margin:0;font-family: Open Sans, Arial, sans-serif !important; font-size:13px; color:#5a5a5a;text-align:center;"><em>Selena<br>Via Giulio Cesare Croce,18<br>40017 San Giovanni in persiceto (BO)</em></p>
            
                                <p style="margin:0;font-family: Open Sans, Arial, sans-serif !important; font-size:13px; color:#5a5a5a; text-align:center;"><em>TUTTI I DIRITTI SONO RISERVATI</em></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>',
                'created_id' => $idUser
            ]);



                 $message = Message::updateOrCreate(['code'   => 'ORDER_CONFIRM_MAIL_SUMMARY_OBJECT'],
            [ 
                'message_type_id' => 1,
                'created_id' => $idUser
            ]);

            $messageLanguage = MessageLanguage::firstOrCreate(
            [
                'message_id'   => $message->id, 
                'language_id' => $idLanguage
            ],
            [
                'content'     => '<p>Il tuo ordine</p>',
                'created_id' => $idUser
            ]);

            $message = Message::updateOrCreate(['code'   => 'ORDER_CONFIRM_MESSAGE_MAIL_TEXT'],
            [ 
                'message_type_id' => 1,
                'created_id' => $idUser
            ]);

            $messageLanguage = MessageLanguage::firstOrCreate(
            [
                'message_id'   => $message->id, 
                'language_id' => $idLanguage
            ],
            [
                'content' => '<div style="width:100%;background:#ededed;margin:0 !important;padding:20px 20px 60px 20px; text-align: center;"><img src="https://selena.tecnotrade.com/api/storage/images/logo.png" style="width:350px;" class="fr-fic fr-dib">
                <table align="center" style="border-collapse:collapse; background:#fff;border-radius:.4rem; margin-top:1rem; font-family:Arial, sans-serif !important; font-size:16px;" width="640">
                    <tbody>
                        <tr>
                            <td>
                                <div style="background: #778899; border-radius:.4rem .4rem 0 0; max-height: 75px;height: 75px; width: 640px; position: relative;"><img src="https://selena.tecnotrade.com/api/storage/images/logo.png" style="width:45px; margin-top: 15px; margin-left: 50px; display: inline-block;" class="fr-fic fr-dii">
            
                                    <p style="color: #fff;font-size: 23px; position: absolute; left: 120px; display: inline-block;"><span>Nuovo Messaggio da #id_sender# <br>#created_at#</span></p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="padding:40px;color:#5a5a5a;">
            
                                    <p>#id_sender# ti ha scritto:</p>
                                    <p>&nbsp;#message#</p>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>',
                //'content'     => '<div style="width:100%;background:#ededed;margin:0 !important;padding:20px 20px 60px 20px; text-align:center;"><div style="text-align:center;"><img src="https://cn.tecnotrade.com/api/storage/images/logo_white_colorato.png" style="width: 350px;" class="fr-fic fr-dii"></div><table align="center" style="border-collapse:collapse; background:#fff;border-radius:.4rem; margin:1rem auto; font-family: Open Sans, Arial, sans-serif !important; font-size:16px;" width="640"><tbody><tr><td><div style="background-size: cover; background: url(https://cn.tecnotrade.com/api/storage/images/pattern_lightblue.png) #0e2a39; border-radius:.4rem .4rem 0 0; max-height: 75px;height: 75px; width: 640px; position: relative;"><img src="https://cn.tecnotrade.com/api/storage/images/ico_pacco.png" style="width:45px; margin-top: 15px; margin-left: 50px; display: inline-block;" class="fr-fic fr-dii"><p style="color: #fff;font-size: 23px; position: absolute; left: 120px; display: inline-block;">Riepilogo Ordine&nbsp;</p></div></td></tr><tr><td><div style="padding:40px;color:#5a5a5a;"><p>Grazie, l&#39;ordine &egrave; stato registrato.<br>Di seguito trovi tutte le informazioni relative al tuo ordine.</p><p style="border-bottom: 1px solid #f1f3f5;">Data Ordine<span style="float: right;">#date_order_formatter#</span></p><p style="border-bottom: 1px solid #f1f3f5;">Numero Ordine<span style="float: right;">#id#</span></p></div></td></tr></tbody></table><br><br><br><br><br><br><br><br><br><br><br><br><br><table align="center" style="border-collapse:collapse; background:#fff;border-radius:.4rem; margin-top:1rem; font-family: Open Sans, Arial, sans-serif !important; font-size:16px;" width="640"><thead></thead><tbody><tr><td><div style="background-size: cover; background: url(https://cn.tecnotrade.com/api/storage/images/pattern_lightyellow.png) #846321; border-radius:.4rem .4rem 0 0; max-height: 75px;height: 75px; width: 640px; position: relative;"><img src="https://cn.tecnotrade.com/api/storage/images/ico_cartellino.png" style="width:45px; margin-top: 15px; margin-left: 50px; display: inline-block;" class="fr-fic fr-dii"><p style="color: #fff;font-size: 23px; position: absolute; left: 120px; display: inline-block;">Dettagli Ordine<span style="margin-left:180px;">Nr. #id#</span></p></div></td></tr><tr><td><div style="padding:40px 40px 0px 40px;color:#5a5a5a;">#sales_order_detail_single_row#</div></td></tr><tr><td><div style="padding:40px;color:#5a5a5a;"><p style="margin:0;">Subtotale:<span style="float:right;">&euro; #net_amount#</span></p><p style="margin:0;">Imposta:<span style="float:right;">&euro; #vat_amount#</span></p><p style="font-size:18px; font-weight:bold; margin-top:10px;">Totale:<span style="float:right;">&euro; #total_amount#</span></p></div></td></tr></tbody></table><table align="center" style="border-collapse:collapse; background:#f1f3f5;border-radius:.4rem; margin-top:1rem; font-family: Open Sans, Arial, sans-serif !important; font-size:16px;" width="640"><tbody><tr><td style="text-align:center;"><a href="https://cn.tecnotrade.com/"><img src="https://cn.tecnotrade.com/api/storage/images/logo_white_colorato.png" style="width: 250px; text-align:center;" class="fr-fic fr-dib"></a><br><p style="margin:0;font-family: Open Sans, Arial, sans-serif !important; font-size:13px; color:#5a5a5a;text-align:center;"><em>&copy; COPYRIGHT 2020 - Camera Nation</em></p><p style="margin:0;font-family: Open Sans, Arial, sans-serif !important; font-size:13px; color:#5a5a5a;text-align:center;"><em>Via Giulio Cesare Croce 18 40017 S.Giovanni in Persiceto (BO)&nbsp;</em></p><p style="margin:0;font-family: Open Sans, Arial, sans-serif !important; font-size:13px; color:#5a5a5a; text-align:center;"><em>TUTTI I DIRITTI SONO RISERVATI</em></p></td></tr></tbody></table></div>',
                'created_id' => $idUser
            ]);

            

            $message = Message::updateOrCreate(['code'   => 'ORDER_CONFIRM_MESSAGE_MAIL_OBJECT'],
            [ 
                'message_type_id' => 1,
                'created_id' => $idUser
            ]);

            $messageLanguage = MessageLanguage::firstOrCreate(
            [
                'message_id'   => $message->id, 
                'language_id' => $idLanguage
            ],
            [
                'content'     => '<p>Nuovo Messaggio da #id_sender#</p>',
                'created_id' => $idUser
            ]);



            $message = Message::updateOrCreate(['code'   => 'SUBSCRIBE_CONFIRM_NEWSLETTER_MAIL_TEXT'],
            [ 
                'message_type_id' => 1,
                'created_id' => $idUser
            ]);

            $messageLanguage = MessageLanguage::firstOrCreate(
            [
                'message_id'   => $message->id, 
                'language_id' => $idLanguage
            ],
            [
                'content'     => '<div style="width:100%;background:#fff;margin:0 !important; text-align:center;"><div style="text-align:center;"><img src="" style="width: 350px;" class="fr-fic fr-dii"></div><div style="text-align: center;"><span style="font-size: 18px; font-family: Georgia, serif;">Grazie per esserti registrato alla nostra newsletter!</span></div></div>',
                'created_id' => $idUser
            ]);

            $message = Message::updateOrCreate(['code'   => 'SUBSCRIBE_CONFIRM_NEWSLETTER_MAIL_OBJECT'],
            [ 
                'message_type_id' => 1,
                'created_id' => $idUser
            ]);

            $messageLanguage = MessageLanguage::firstOrCreate(
            [
                'message_id'   => $message->id, 
                'language_id' => $idLanguage
            ],
            [
                'content'     => '<p>Conferma registrazione newsletter!</p>',
                'created_id' => $idUser
            ]);


            $message = Message::updateOrCreate(['code'   => 'CONFIRM_REQUEST_FORM_BUILDER_TEXT'],
            [ 
                'message_type_id' => 1,
                'created_id' => $idUser
            ]);

            $messageLanguage = MessageLanguage::firstOrCreate(
            [
                'message_id'   => $message->id, 
                'language_id' => $idLanguage
            ],
            [
                'content'     => '<div style="width:100%;background:#ededed;margin:0 !important;padding:20px 20px 60px 20px; text-align: center;"><img src="https://selena.tecnotrade.com/api/storage/images/logo.png" style="width:350px;" class="fr-fic fr-dib"><table align="center" style="border-collapse:collapse; background:#fff;border-radius:.4rem; margin-top:1rem; font-family: \'Open Sans\', Arial, sans-serif !important; font-size:16px;" width="640"><tbody><tr><td><div style="background: #778899; border-radius:.4rem .4rem 0 0; max-height: 75px;height: 75px; width: 640px; position: relative;"><img src="https://selena.tecnotrade.com/api/storage/images/logo.png" style="width:45px; margin-top: 15px; margin-left: 50px; display: inline-block;" class="fr-fic fr-dii"></div></td></tr><tr><td><div style="padding:40px;color:#5a5a5a;">#form_builder_field#</div></td></tr></tbody></table></div>',
                'created_id' => $idUser
            ]);


            

            /* disinscrizione alla newsletter*/
                $message = Message::updateOrCreate(['code'   => 'UNSUBSCRIBERNEWSLETTER_TEXT'],
            [ 
                'message_type_id' => 1,
                'created_id' => $idUser
            ]);

            $messageLanguage = MessageLanguage::firstOrCreate(
            [
                'message_id'   => $message->id, 
                'language_id' => $idLanguage
            ],
            [
                'content'     => '<p><span style="color: rgb(65, 65, 65); font-family: sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;">La tua richiesta di disiscrizione &egrave; stata registrata correttamente: da questo momento in poi, non riceverai pi&ugrave; la nostra newsletter. Nel caso in cui dovessi ricevere ancora email, &egrave; perch&eacute; sono state pianificate prima della ricezione della tua richiesta di disiscrizione</span></p>',
                'created_id' => $idUser
            ]);
             /*  end disinscrizione alla newsletter*/

        /*****************************************************************************/
        /* CONTENTS */
        /*****************************************************************************/
        
        //sidebar
        /*$content = Content::updateOrCreate(['code'   => 'Sidebar'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => 'Sidebar',
            'created_id' => $idUser
        ]);*/

        //articoli preferiti utente
        /*$content = Content::updateOrCreate(['code'   => 'FavoritesUserItems'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => 'FavoritesUserItems',
            'created_id' => $idUser
        ]);*/

        //singola riga dettaglio carrello
        /*$content = Content::updateOrCreate(['code'   => 'Cart_Detail_Single_Row'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => 'Riga dettaglio carrello',
            'created_id' => $idUser
        ]);*/

        //sommario del carrello
        /*$content = Content::updateOrCreate(['code'   => 'Cart_Summary'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => 'Sommario carrello',
            'created_id' => $idUser
        ]);*/

        //singola riga dettaglio ordine vendita
        /*$content = Content::updateOrCreate(['code'   => 'Sales_Order_Detail_Single_Row'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => 'Riga dettaglio ordine vendita',
            'created_id' => $idUser
        ]);*/

        //preview indirizzo di spedizione in carrello
        /*$content = Content::updateOrCreate(['code'   => 'CartUsersAddresses'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row"><div class="col-12"><label>#name# #surname#</label></div><div class="col-12">#address#, #locality#, #postal_code#</div></div>',
            'created_id' => $idUser
        ]);*/

        //lista indirizzi di spedizione per utente
        /*$content = Content::updateOrCreate(['code'   => 'ListUsersAddresses'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row box-address"><div class="col-10"><h5>#name# #surname#</h5><label>#address# - #locality# - #postal_code#</label><br><label>#phone_number#</label></div><div class="col-2"><i class="fa fa-trash fa-2x" data-id-address="#id#"></i> <i class="fa fa-check fa-2x" data-id-address="#id#"></i></div></div>',
            'created_id' => $idUser
        ]);*/

        //html visualizzato nell'header quando l'utente NON È loggato
        $content = Content::updateOrCreate(['code'   => 'UserShortcodeHeaderLoggedOut'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row div-user"><div class="col-12 div-user-login"><a href="/login"><i class="fa fa-user"></i></a></div></div>',
            'created_id' => $idUser
        ]);

        //html visualizzato nell'header quando l'utente È loggato
        $content = Content::updateOrCreate(['code'   => 'UserShortcodeHeaderLoggedIn'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row div-user"><div class="col-6 div-user-welcome"><span>#name#</span></div><div class="col-6 div-user-logout"><i class="fa fa-sign-out"></i></div></div>',
            'created_id' => $idUser
        ]);

        
        //html visualizzato nell'header quando l'utente NON È loggato
        $content = Content::updateOrCreate(['code'   => 'SalesmanShortcodeHeaderLoggedOut'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row div-user"><div class="col-12 div-user-login"><a href="/login-agente"><i class="fa fa-user"></i></a></div></div>',
            'created_id' => $idUser
        ]);

        //html visualizzato nell'header quando l'utente È loggato
        $content = Content::updateOrCreate(['code'   => 'SalesmanShortcodeHeaderLoggedIn'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row div-user"><div class="col-6 div-user-welcome"><span>#name#</span></div><div class="col-6 div-user-logout"><i class="fa fa-sign-out"></i></div></div>',
            'created_id' => $idUser
        ]);



        //html per la lista articoli blog
        $content = Content::updateOrCreate(['code'   => 'BlogListArticle'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="col-12 col-md-4"><img src="#url_cover_image#" style="width: 100%;" class="fr-fic fr-dii"><div class="row"><div class="col-12"><h3 style="font-weight:bold;">#title#</h3><p>#seo_description#</p><div class="text-right"><a href="#url#">Scopri di pi&ugrave;</a></div></div></div></div>',
            'created_id' => $idUser
        ]);

        //html per la lista articoli news
        $content = Content::updateOrCreate(['code'   => 'NewsListArticle'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="col-12 col-md-4"><img src="#url_cover_image#" style="width: 100%;" class="fr-fic fr-dii"><div class="row"><div class="col-12"><h3 style="font-weight:bold;">#title#</h3><p>#seo_description#</p><div class="text-right"><a href="#url#">Scopri di pi&ugrave;</a></div></div></div></div>',
            'created_id' => $idUser
        ]);


        //gestione content sezione dedicata al dettaglio e elenco ordini
         $content = Content::updateOrCreate(['code'   => 'ListSalesOrder'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row box-address"><div class="col-12"><div class="row"><div class="col-2"><label><strong>Codice Articolo</strong></label></div><div class="col-3"><label><strong>Totale</strong></label></div><div class="col-3"><label><strong>Data Ordine</strong></label></div><div class="col-2"><label><strong>Stato Pagamento</strong></label></div></div></div><div class="col-12"><div class="row"><div class="col-2"><label>#id#</label></div><div class="col-3"><label>&euro; #total_amount#</label></div><div class="col-3"><label>#date_order#</label></div><div class="col-2"><label>#order-state#</label></div><div class="col-2"><i class="fa fa-pencil fa-2x list-sale-sorder" data-id-list-sales-order="#id#"></i></div></div></div></div>',
            'created_id' => $idUser
        ]);

         //gestione content sezione dedicata al dettaglio e elenco ordini
         $content = Content::updateOrCreate(['code'   => 'ListQuotes'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row box-address"><div class="col-12"><div class="row"><div class="col-2"><label><strong>Codice Articolo</strong></label></div><div class="col-3"><label><strong>Totale</strong></label></div><div class="col-3"><label><strong>Data Ordine</strong></label></div><div class="col-2"><label><strong>Stato Pagamento</strong></label></div></div></div><div class="col-12"><div class="row"><div class="col-2"><label>#id#</label></div><div class="col-3"><label>&euro; #total_amount#</label></div><div class="col-3"><label>#date_order#</label></div><div class="col-2"><label>#order-state#</label></div><div class="col-2"><i class="fa fa-pencil fa-2x list-sale-sorder" data-id-list-sales-order="#id#"></i></div></div></div></div>',
            'created_id' => $idUser
        ]);

       $content = Content::updateOrCreate(['code'   => 'DetailSalesOrders'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row"><div class="col-12">#order_user_data#</div></div><hr><div class="row"><div class="col-6">#order-goods-shipment-data#</div><div class="col-6">#order-data-shipping-documents#</div></div><hr><div class="row"><div class="col-12">#summary-description-document#</div></div><hr><div class="row"><div class="col-12">#detail-row-order#</div></div><hr><div class="row"><div class="col-6"><br></div><div class="col-6">#order-amounts-summary#</div></div>',
            'created_id' => $idUser
        ]); 

         $content = Content::updateOrCreate(['code'   => 'DetailSalesOrderUserData'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row" style="background-color:#f2f3f5;"><div class="col-sm-12"><h4><em><strong>Dati Anagrafici</strong></em></h4></div></div><div class="row" style="background-color:#f2f3f5;"><div class="col-sm-6 col-md-4 col-lg-4"><strong><em><label><strong>Ragione Sociale</strong></label>&nbsp;</em></strong><div><em><span style="font-size: 16px;"><p>#business_name#</p></span></em></div></div><div class="col-sm-6 col-md-4 col-lg-4"><strong><em><label><strong>Nome e Cognome</strong></label>&nbsp;</em></strong><div><p><em><span style="font-size: 16px;">#name# #surname#&nbsp;</span></em></p></div></div></div><div class="row" style="background-color:#f2f3f5;"><div class="col-sm-6 col-md-4 col-lg-4"><strong><em><label><strong>Indirizzo</strong></label>&nbsp;</em></strong><div><p><em><span style="font-size: 16px;">#address#</span></em></p></div></div><div class="col-sm-6 col-md-4 col-lg-4"><strong><em><label><strong>Email</strong></label>&nbsp;</em></strong><div><p><em><span style="font-size: 16px;">#email#</span></em></p></div></div><div class="col-sm-6 col-md-4 col-lg-4"><strong><em><label><strong>Modalit&agrave; di Pagamento</strong></label>&nbsp;</em></strong><div><p><em><span style="font-size: 16px;">Paypal</span></em></p></div></div></div><div class="row" style="background-color:#f2f3f5;"><div class="col-sm-6 col-md-4 col-lg-4"><strong><em><label><strong>Localit&agrave;</strong></label>&nbsp;</em></strong><div><em><span style="font-size: 16px;"><p>#postal_code# #country# (#provinces#)</p></span></em></div></div><div class="col-sm-6 col-md-4 col-lg-4"><strong><em><label><strong>Telefono</strong></label>&nbsp;</em></strong><div><p><em><span style="font-size: 16px;">#telephone_number#</span></em></p></div></div></div><div class="row" style="background-color:#f2f3f5;"><div class="col-sm-6 col-md-4 col-lg-4"><strong><em><label><strong>Partita IVA</strong></label>&nbsp;</em></strong><div><p><em><span style="font-size: 16px;">#vat_number#</span></em></p></div></div><div class="col-sm-6 col-md-4 col-lg-4"><strong><em><label><strong>Fax</strong></label>&nbsp;</em></strong><div><p><em><span style="font-size: 16px;">#fax_number#</span></em></p></div></div></div><div class="row" style="background-color:#f2f3f5;"><div class="col-sm-6 col-md-4 col-lg-4"><strong><em><label><strong>Codice Fiscale</strong></label>&nbsp;</em></strong><div><p><em><span style="font-size: 16px;">#fiscal_code#</span></em></p></div></div></div>',
            'created_id' => $idUser
        ]); 

           $content = Content::updateOrCreate(['code'   => 'DetailShipmentData'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row"><div class="col-12 col-md-12 col-lg-12"><h4><em><strong>Dati Spedizione Merci</strong></em></h4></div></div><div class="row"><div class="col-12 col-md-4 col-lg-4"><em><label><strong>Destinazione</strong></label></em></div><div><p><em>&nbsp;<span style="font-size: 16px;">#business_name#</span></em></p></div></div><div class="row" style="background-color:#f2f3f5;"><div class="col-12 col-md-4 col-lg-4"><em><label><strong>Indirizzo</strong></label></em></div><div><p><em><span style="font-size: 16px;">#address#</span></em></p></div></div><div class="row"><div class="col-12 col-md-4 col-lg-4"><em><label><strong>Localit&agrave;</strong></label></em></div><div><p><em><span style="font-size: 16px;">#postal_code# #locality# (#province#)</span></em></p></div></div><div class="row" style="background-color:#f2f3f5;"><div class="col-12 col-md-4 col-lg-4"><em><label><strong>Telefono</strong></label></em></div><div><p><em><span style="font-size: 16px;">#phone_number#</span></em></p></div></div>',
            'created_id' => $idUser
        ]); 

            $content = Content::updateOrCreate(['code'   => 'DetailGoodShipmentData'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row"><div class="col-12 col-md-12 col-lg-12"><h4><em><strong>Dati Spedizione Merci</strong></em></h4></div></div><div class="row"><div class="col-12 col-md-4 col-lg-4"><em><label><strong>Destinazione</strong></label></em></div><div><p><em>&nbsp;<span style="font-size: 16px;">#business_name#</span></em></p></div></div><div class="row" style="background-color:#f2f3f5;"><div class="col-12 col-md-4 col-lg-4"><em><label><strong>Indirizzo</strong></label></em></div><div><p><em><span style="font-size: 16px;">#address#</span></em></p></div></div><div class="row"><div class="col-12 col-md-4 col-lg-4"><em><label><strong>Localit&agrave;</strong></label></em></div><div><p><em><span style="font-size: 16px;">#postal_code# #locality# (#province#)</span></em></p></div></div><div class="row" style="background-color:#f2f3f5;"><div class="col-12 col-md-4 col-lg-4"><em><label><strong>Telefono</strong></label></em></div><div><p><em><span style="font-size: 16px;">#phone_number#</span></em></p></div></div>',
            'created_id' => $idUser
        ]); 

            $content = Content::updateOrCreate(['code'   => 'OrderDataShippingDocuments'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row"><div class="col-12"><h4><em><strong>Dati Spedizione Documenti</strong></em></h4></div></div><div class="row"><div class="col-4"><strong><em><label>Destinazione</label></em></strong></div><div><p><em><span style="font-size: 16px;">#business_name#</span></em></p></div></div><div class="row" style="background-color:#f2f3f5;"><div class="col-4"><strong><em><label>Indirizzo</label></em></strong></div><div><p><em><span style="font-size: 16px;">#address#</span></em></p></div></div><div class="row"><div class="col-4"><strong><em><label><strong>Localit&agrave;</strong></label>&nbsp;</em></strong></div><div><p><em><span style="font-size: 16px;">#postal_code# #locality# (#province#)</span></em></p></div></div><div class="row" style="background-color:#f2f3f5;"><div class="col-4"><strong><em><label><strong>Telefono</strong></label>&nbsp;</em></strong></div><div><p><em><span style="font-size: 16px;">#phone_number#</span></em></p></div></div>',
            'created_id' => $idUser
        ]); 

            $content = Content::updateOrCreate(['code'   => 'SummaryDescriptionDocument'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row" style="background-color:#f2f3f5;"><div class="col-12"><h4><em><strong>Descrizione Documento</strong></em></h4></div></div><div class="row"><div class="col-sm-3"><strong><em><label><strong>Numero Ordine:</strong></label>&nbsp;</em></strong><p><em><span style="font-size: 16px;">#id#</span></em></p></div><div class="col-sm-5"><strong><em><label><strong>Data Ordine:</strong></label>&nbsp;</em></strong><p><em><span style="font-size: 16px;">#date_order_formatter#</span></em></p></div><div class="col-sm-4"><strong><em><label><strong>Stato:</strong></label>&nbsp;</em></strong><p><em><span style="font-size: 16px;">In attesa di essere processato</span></em></p><!--#sales_order_state_description# --></div></div><div class="row"><div class="col-sm-8" style="background-color:#f2f3f5;"><strong><em><label><strong>Vettore:</strong></label>&nbsp;</em></strong><p><em><span style="font-size: 16px;">#carrier_description#</span></em></p></div><div class="col-sm-4" style="background-color:#f2f3f5;"><strong><em><label><strong>Porto:</strong></label>&nbsp;</em></strong><p><em><span style="font-size: 16px;">#carriage_description#</span></em></p></div></div><div class="row"><div class="col-sm-8"><strong><em><label><strong>Pagamento:</strong></label>&nbsp;</em></strong><p><em><span style="font-size: 16px;">#payment_description#</span></em></p></div><div class="col-sm-4"><strong><em><label><strong>Note:</strong></label>&nbsp;</em></strong><p><em><span style="font-size: 16px;">#note#</span></em></p></div></div>',
            'created_id' => $idUser
        ]); 

            $content = Content::updateOrCreate(['code'   => 'DetailRowOrder'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row no-gutters"><div class="col-12 col-md-6 col-lg-6"><div class="row"><div class="col-12 col-md-3 col-lg-3"><label><strong>Codice</strong></label><div><p><span style="font-size: 16px;"><em>#items_description#</em></span></p></div></div><div class="col-12 col-md-9 col-lg-9"><label><strong>Descrizione</strong></label><div><p><span style="font-size: 16px;"><em>#description#</em></span></p></div></div></div></div><div class="col-12 col-md-6 col-lg-6"><div class="row"><div class="col-3 col-md-3 col-lg-3"><label><strong>Q.TA&#39;</strong></label><div><p><span style="font-size: 16px;"><em>#quantity#</em></span></p></div></div><div class="col-3 col-md-3 col-lg-3"><label><strong>Netto</strong></label><div><p><span style="font-size: 16px;"><em>&euro; #net_amount#</em></span></p></div></div><div class="col-3 col-md-3 col-lg-3"><label><strong>Tot.Iva</strong></label><div><p><span style="font-size: 16px;"><em>&euro; #vat_amount#</em></span></p></div></div><!-- <div><strong><span style="font-size: 16px;"><em>#vat_type_description#%</em></span></strong></div></div>----><div class="col-3 col-md-3 col-lg-3"><label><strong>Totale</strong></label><div><p><span style="font-size: 16px;"><em>&euro; #total_amount#</em></span></p></div></div></div></div></div><hr>',
            'created_id' => $idUser
        ]); 

        $content = Content::updateOrCreate(['code'   => 'SummaryAmountOrder'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row" style="background-color:#f2f3f5;"><div class="col-12"><h4><strong><em>Riepilogo</em></strong></h4></div></div><div class="row"><div class="col-8"><strong><em><label><strong>Imponibile Merce</strong></label>&nbsp;</em></strong></div><div><strong><em><span style="font-size: 16px;">&euro;#net_amount#</span></em></strong></div></div><div class="row" style="background-color:#f2f3f5;"><div class="col-8"><strong><em><label><strong>Spese Aggiuntive di Spedizione</strong></label>&nbsp;</em></strong></div><div><strong><em><span style="font-size: 16px;">&euro;#shipment_amount#</span></em></strong></div></div><div class="row"><div class="col-8"><strong><em><label><strong>Spese Aggiuntive al Pagamento</strong></label>&nbsp;</em></strong></div><div><strong><em><span style="font-size: 16px;">&euro;#payment_cost#</span></em></strong></div></div><div class="row" style="background-color:#f2f3f5;"><div class="col-8"><strong><em><label><strong>Totale Imposte</strong></label>&nbsp;</em></strong></div><div><strong><em><span style="font-size: 16px;">&euro;#vat_amount#</span></em></strong></div></div><hr><div class="row" style="background-color:#f2f3f5;"><div class="col-8"><strong><em><label><strong>Totale da Pagare</strong></label>&nbsp;</em></strong></div><div><strong><em><span style="font-size: 16px;">&euro;#total_amount#</span></em></strong></div></div>',
            'created_id' => $idUser
        ]); 



        $content = Content::updateOrCreate(['code'   => 'DetailMailTableOrderSales'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
           'content'     => '<table cellpadding="5" open="" style="border-collapse:collapse; background:#fff;font-family: ;"><tbody><tr><td style="width:100px;white-space: nowrap;font-size:14px;"><p>Qt</p><p>#quantity#</p></td><td style="width:440px;font-size:14px;"><p>Descrizione</p><p>#items_description# - #description_language_item#</p></td><td style="width:100px;white-space: nowrap;font-size:14px; text-align: right;"><p>Prezzo</p><p>&euro; #unit_price#</p></td></tr></tbody></table>',
           'created_id' => $idUser
        ]); 


          //header articolo blog e news
          $content = Content::updateOrCreate(['code'   => 'HeaderArticle'],
          [ 
              'created_id' => $idUser
          ]);
  
          $contentLanguage = ContentLanguage::firstOrCreate(
          [
              'content_id'   => $content->id, 
              'language_id' => $idLanguage
          ],
          [
              'content'     => 'HeaderArticle',
              'created_id' => $idUser
          ]);   
          
          
          //pagina prezzi articolo
          $content = Content::updateOrCreate(['code'   => 'PagePrices'],
          [ 
              'created_id' => $idUser
          ]);
  
          $contentLanguage = ContentLanguage::firstOrCreate(
          [
              'content_id'   => $content->id, 
              'language_id' => $idLanguage
          ],
          [
              'content'     => 'PagePrices',
              'created_id' => $idUser
          ]); 

          //pagina prezzi articolo
          $content = Content::updateOrCreate(['code'   => 'Price_Detail_Single_Row'],
          [ 
              'created_id' => $idUser
          ]);
  
          $contentLanguage = ContentLanguage::firstOrCreate(
          [
              'content_id'   => $content->id, 
              'language_id' => $idLanguage
          ],
          [
              'content'     => 'Price_Detail_Single_Row',
              'created_id' => $idUser
          ]); 

        //pagina galleria fotografie prodotto
        $content = Content::updateOrCreate(['code'   => 'ItemGalleryPage'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => 'ItemGalleryPage',
            'description' => 'Pagina delle foto fatte con quel prodotto',
            'created_id' => $idUser
        ]); 
        
        //pagina dettaglio immagine community
        $content = Content::updateOrCreate(['code'   => 'Community_Image_Detail'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => 'Community_Image_Detail',
            'created_id' => $idUser
        ]); 

        //pagina commento all'immagine della community
        $content = Content::updateOrCreate(['code'   => 'Community_Image_Comment'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => 'Community_Image_Comment',
            'created_id' => $idUser
        ]); 

        $content = Content::updateOrCreate(['code'   => 'Community_Image_Comment_Homepage'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => 'Community_Image_Comment_Homepage',
            'created_id' => $idUser
        ]); 

        //pagina profilo utente
        $content = Content::updateOrCreate(['code'   => 'User_Profile_Page'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => 'User_Profile_Page',
            'created_id' => $idUser
        ]); 


        //From Builder Field
        $content = Content::updateOrCreate(['code'   => 'FormBuilderField'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<table style="width:640px"><tr><td>#description#</td><td>#value#</td></tr></table>',
            'description' => 'Contenuto utilizzato nell\'invio dinamico del form builder, questo shortcode è contenuto all\'interno del messaggio CONFIRM_REQUEST_FORM_BUILDER_TEXT',
            'created_id' => $idUser
        ]); 

        //From Builder Field
        $content = Content::updateOrCreate(['code'   => 'FormBuilderFieldType'],
        [ 
            'created_id' => $idUser
        ]);

        $contentLanguage = ContentLanguage::firstOrCreate(
        [
            'content_id'   => $content->id, 
            'language_id' => $idLanguage
        ],
        [
            'content'     => '<div class="row"><div class="col-12">#description#</div><div class="col-12">#form#</div></div>',
            'description' => 'Contenuto utilizzato per la visualizzazione dello shortcode [form form]',
            'created_id' => $idUser
        ]); 

        /*****************************************************************************/
        /* SETTINGS */
        /*****************************************************************************/
        $setting = Setting::firstOrCreate(
        [
            'code'   => 'Sales_Order_Confirmation_Page'
        ],
        [
            'description'     => 'Pagina di ringraziamento al termine dell\'ordine di acquisto',
            'value' => 'orderconfirmationpage',
            'created_id' => $idUser
        ]);

        $setting = Setting::firstOrCreate(
        [
            'code'   => 'PriceIncludingVat'
        ],
        [
            'description'     => 'Prezzi IVA compresa',
            'value' => 'True',
            'created_id' => $idUser
        ]);

        $setting = Setting::firstOrCreate(
        [
            'code'   => 'DefaultShippingPrice'
        ],
        [
            'description'     => 'Prezzo di default per le spese di trasporto',
            'value' => '9,99',
            'created_id' => $idUser
        ]);

        $setting = Setting::firstOrCreate(
        [
            'code'   => 'SidebarVisible'
        ],
        [
            'description'     => 'Visualizzazione della sidebar all\'interno di articoli blog o news',
            'value' => 'False',
            'created_id' => $idUser
        ]);

        $setting = Setting::firstOrCreate(
        [
            'code'   => 'SidebarPosition'
        ],
        [
            'description'     => 'Posizione di visualizzazione della sidebar (Left | Right)',
            'value' => 'Right',
            'created_id' => $idUser
        ]);

        $setting = Setting::firstOrCreate(
        [
            'code'   => 'SidebarWidth'
        ],
        [
            'description'     => 'Larghezza della sidebar (numero di colonne)',
            'value' => '4',
            'created_id' => $idUser
        ]);

        $setting = Setting::firstOrCreate(
        [
            'code'   => 'SidebarWidthMedium'
        ],
        [
            'description'     => 'Larghezza della sidebar per dispositivi Tablet (numero di colonne)',
            'value' => '4',
            'created_id' => $idUser
        ]);

        $setting = Setting::firstOrCreate(
        [
            'code'   => 'SidebarWidthSmall'
        ],
        [
            'description'     => 'Larghezza della sidebar per dispositivi Smartphone (numero di colonne)',
            'value' => '12',
            'created_id' => $idUser
        ]);

        $setting = Setting::firstOrCreate(
        [
            'code'   => 'UsernameProviderMessaggi'
        ],
        [
            'description'     => 'Username per la gestione invio SMS',
            'value' => 'tecnotrade',
            'created_id' => $idUser
        ]);

        $setting = Setting::firstOrCreate(
        [
            'code'   => 'PasswordProviderMessaggi'
        ],
        [
            'description'     => 'Password per la gestione invio SMS',
            'value' => 'tecnotrade',
            'created_id' => $idUser
        ]);

        $setting = Setting::firstOrCreate(
        [
            'code'   => 'SenderProviderMessaggi'
        ],
        [
            'description'     => 'Mittente per la gestione invio SMS',
            'value' => 'Tecnotrade',
            'created_id' => $idUser
        ]);

        $setting = Setting::firstOrCreate(
        [
            'code'   => 'TokenAPITinyurl'
        ],
        [
            'description'     => 'Chiave di accesso per API TinyUrl',
            'value' => 'tRw1K9s4u4fdkbMuBmMZDGhKV3Gt7EmvydDJluNhi3f9TtL0LQf9qIrHSoXB',
            'created_id' => $idUser
        ]);

        

        /*****************************************************************************/
        /* REGION */
        /*****************************************************************************/
        $region = Region::firstOrCreate(
        [
            'description'   => 'Abruzzo'
        ],
        [
            'order' => '10',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Basilicata'
        ],
        [
            'order' => '20',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Calabria'
        ],
        [
            'order' => '30',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Campania'
        ],
        [
            'order' => '40',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Emilia Romagna'
        ],
        [
            'order' => '50',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Friuli Venezia Giulia'
        ],
        [
            'order' => '60',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Lazio'
        ],
        [
            'order' => '70',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Liguria'
        ],
        [
            'order' => '80',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Lombardia'
        ],
        [
            'order' => '90',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Marche'
        ],
        [
            'order' => '100',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Molise'
        ],
        [
            'order' => '110',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Piemonte'
        ],
        [
            'order' => '120',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Puglia'
        ],
        [
            'order' => '130',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Sardegna'
        ],
        [
            'order' => '140',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Sicilia'
        ],
        [
            'order' => '150',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Toscana'
        ],
        [
            'order' => '160',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Trentino-Alto Adige'
        ],
        [
            'order' => '170',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Umbria'
        ],
        [
            'order' => '180',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Valle d\'Aosta'
        ],
        [
            'order' => '190',
            'created_id' => $idUser
        ]);

        $region = Region::firstOrCreate(
        [
            'description'   => 'Veneto'
        ],
        [
            'order' => '200',
            'created_id' => $idUser
        ]);

    }
}

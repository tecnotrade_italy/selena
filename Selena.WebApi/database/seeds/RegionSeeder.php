<?php
use App\Dictionary;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $functionalityRegion = Functionality::where('code', 'Regions')->first();

        //------------ Tab Tabella ---------------------------------
        #region TAB
        $tabRegionTable = new Tab();
        $tabRegionTable->code = 'TableRegions';
        $tabRegionTable->functionality_id = $functionalityRegion->id;
        $tabRegionTable->order = 10;
        $tabRegionTable->created_id = $idUser;
        $tabRegionTable->save();

        $languageTabRegionTable = new LanguageTab();
        $languageTabRegionTable->language_id = $idLanguage;
        $languageTabRegionTable->tab_id = $tabRegionTable->id;
        $languageTabRegionTable->description = 'Tabella Regioni';
        $languageTabRegionTable->created_id = $idUser;
        $languageTabRegionTable->save();
        #END region TAB

        #region CHECKBOX
        $fieldRegionCheckBox = new Field();
        $fieldRegionCheckBox->tab_id = $tabRegionTable->id;
        $fieldRegionCheckBox->code = 'TableCheckBox';
        $fieldRegionCheckBox->created_id = $idUser;
        $fieldRegionCheckBox->save();

        $fieldLanguageContactCheckBox = new FieldLanguage();
        $fieldLanguageContactCheckBox->field_id = $fieldRegionCheckBox->id;
        $fieldLanguageContactCheckBox->language_id = $idLanguage;
        $fieldLanguageContactCheckBox->description = 'CheckBox';
        $fieldLanguageContactCheckBox->created_id = $idUser;
        $fieldLanguageContactCheckBox->save();

        $fieldUserRoleContactCheckBox = new FieldUserRole();
        $fieldUserRoleContactCheckBox->field_id = $fieldRegionCheckBox->id;
        $fieldUserRoleContactCheckBox->role_id = $idRole;
        $fieldUserRoleContactCheckBox->enabled = true;
        $fieldUserRoleContactCheckBox->table_order = 10;
        $fieldUserRoleContactCheckBox->input_type = 13;
        $fieldUserRoleContactCheckBox->created_id = $idUser;
        $fieldUserRoleContactCheckBox->save();
        #endregion CHECKBOX
  
        #region FORMATTER
        $fieldRegionFormatter = new Field();
        $fieldRegionFormatter->code = 'RegionFormatter';
        $fieldRegionFormatter->formatter = 'RegionFormatter';
        $fieldRegionFormatter->tab_id = $tabRegionTable->id;
        $fieldRegionFormatter->created_id = $idUser;
        $fieldRegionFormatter->save();

        $fieldLanguageRegionFormatter = new FieldLanguage();
        $fieldLanguageRegionFormatter->field_id = $fieldRegionFormatter->id;
        $fieldLanguageRegionFormatter->language_id = $idLanguage;
        $fieldLanguageRegionFormatter->description = '';
        $fieldLanguageRegionFormatter->created_id = $idUser;
        $fieldLanguageRegionFormatter->save();

        $fieldUserRoleRegionFormatter = new FieldUserRole();
        $fieldUserRoleRegionFormatter->field_id = $fieldRegionFormatter->id;
        $fieldUserRoleRegionFormatter->role_id = $idRole;
        $fieldUserRoleRegionFormatter->enabled = true;
        $fieldUserRoleRegionFormatter->table_order = 20;
        $fieldUserRoleRegionFormatter->input_type = 14;
        $fieldUserRoleRegionFormatter->created_id = $idUser;
        $fieldUserRoleRegionFormatter->save();
        #endregion FORMATTER

        #region description
        $fieldRegionDescription = new Field();
        $fieldRegionDescription->code = 'description';
        $fieldRegionDescription->tab_id = $tabRegionTable->id;
        $fieldRegionDescription->field = 'description';
        $fieldRegionDescription->data_type = 1;
        $fieldRegionDescription->created_id = $idUser;
        $fieldRegionDescription->save();

        $fieldLanguageRegionDescription = new FieldLanguage();
        $fieldLanguageRegionDescription->field_id = $fieldRegionDescription->id;
        $fieldLanguageRegionDescription->language_id = $idLanguage;
        $fieldLanguageRegionDescription->description = 'Descrizione';
        $fieldLanguageRegionDescription->created_id = $idUser;
        $fieldLanguageRegionDescription->save();

        $fieldUserRoleRegionDescription = new FieldUserRole();
        $fieldUserRoleRegionDescription->field_id = $fieldRegionDescription->id;
        $fieldUserRoleRegionDescription->role_id = $idRole;
        $fieldUserRoleRegionDescription->enabled = true;
        $fieldUserRoleRegionDescription->table_order = 30;
        $fieldUserRoleRegionDescription->input_type = 0;
        $fieldUserRoleRegionDescription->created_id = $idUser;
        $fieldUserRoleRegionDescription->save();
        #endregion description

        #region order
        $fieldRegionOrder = new Field();
        $fieldRegionOrder->code = 'order';
        $fieldRegionOrder->tab_id = $tabRegionTable->id;
        $fieldRegionOrder->field = 'order';
        $fieldRegionOrder->data_type = 1;
        $fieldRegionOrder->created_id = $idUser;
        $fieldRegionOrder->save();

        $fieldLanguageRegionOrder = new FieldLanguage();
        $fieldLanguageRegionOrder->field_id = $fieldRegionOrder->id;
        $fieldLanguageRegionOrder->language_id = $idLanguage;
        $fieldLanguageRegionOrder->description = 'Ordine';
        $fieldLanguageRegionOrder->created_id = $idUser;
        $fieldLanguageRegionOrder->save();

        $fieldUserRoleRegionOrder = new FieldUserRole();
        $fieldUserRoleRegionOrder->field_id = $fieldRegionOrder->id;
        $fieldUserRoleRegionOrder->role_id = $idRole;
        $fieldUserRoleRegionOrder->enabled = true;
        $fieldUserRoleRegionOrder->table_order = 40;
        $fieldUserRoleRegionOrder->input_type = 0;
        $fieldUserRoleRegionOrder->created_id = $idUser;
        $fieldUserRoleRegionOrder->save();
        #endregion order

        //------------ Tab Dettaglio ---------------------------------
        #region TAB
        $detailRegionTable = new Tab();
        $detailRegionTable->code = 'DetailRegions';
        $detailRegionTable->functionality_id = $functionalityRegion->id;
        $detailRegionTable->order = 10;
        $detailRegionTable->created_id = $idUser;
        $detailRegionTable->save();

        $languageTabRegionTable = new LanguageTab();
        $languageTabRegionTable->language_id = $idLanguage;
        $languageTabRegionTable->tab_id = $detailRegionTable->id;
        $languageTabRegionTable->description = 'Dettaglio Regioni';
        $languageTabRegionTable->created_id = $idUser;
        $languageTabRegionTable->save();
        #END region TAB

        #region description
        $fieldRegionDescription = new Field();
        $fieldRegionDescription->code = 'description';
        $fieldRegionDescription->tab_id = $detailRegionTable->id;
        $fieldRegionDescription->field = 'description';
        $fieldRegionDescription->data_type = 1;
        $fieldRegionDescription->created_id = $idUser;
        $fieldRegionDescription->save();

        $fieldLanguageRegionDescription = new FieldLanguage();
        $fieldLanguageRegionDescription->field_id = $fieldRegionDescription->id;
        $fieldLanguageRegionDescription->language_id = $idLanguage;
        $fieldLanguageRegionDescription->description = 'Descrizione';
        $fieldLanguageRegionDescription->created_id = $idUser;
        $fieldLanguageRegionDescription->save();

        $fieldUserRoleRegionDescription = new FieldUserRole();
        $fieldUserRoleRegionDescription->field_id = $fieldRegionDescription->id;
        $fieldUserRoleRegionDescription->role_id = $idRole;
        $fieldUserRoleRegionDescription->enabled = true;
        $fieldUserRoleRegionDescription->pos_x = 10;
        $fieldUserRoleRegionDescription->pos_y = 10;
        $fieldUserRoleRegionDescription->input_type = 0;
        $fieldUserRoleRegionDescription->created_id = $idUser;
        $fieldUserRoleRegionDescription->save();
        #endregion description

        #region order
        $fieldRegionOrder = new Field();
        $fieldRegionOrder->code = 'order';
        $fieldRegionOrder->tab_id = $detailRegionTable->id;
        $fieldRegionOrder->field = 'order';
        $fieldRegionOrder->data_type = 1;
        $fieldRegionOrder->created_id = $idUser;
        $fieldRegionOrder->save();

        $fieldLanguageRegionOrder = new FieldLanguage();
        $fieldLanguageRegionOrder->field_id = $fieldRegionOrder->id;
        $fieldLanguageRegionOrder->language_id = $idLanguage;
        $fieldLanguageRegionOrder->description = 'Ordinamento';
        $fieldLanguageRegionOrder->created_id = $idUser;
        $fieldLanguageRegionOrder->save();

        $fieldUserRoleRegionOrder = new FieldUserRole();
        $fieldUserRoleRegionOrder->field_id = $fieldRegionOrder->id;
        $fieldUserRoleRegionOrder->role_id = $idRole;
        $fieldUserRoleRegionOrder->enabled = true;
        $fieldUserRoleRegionOrder->pos_x = 20;
        $fieldUserRoleRegionOrder->pos_y = 10;
        $fieldUserRoleRegionOrder->input_type = 0;
        $fieldUserRoleRegionOrder->created_id = $idUser;
        $fieldUserRoleRegionOrder->save();
        #endregion order 

    }
}

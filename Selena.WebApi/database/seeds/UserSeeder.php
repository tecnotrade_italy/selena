<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'admin';
        $user->email = 'support@tecnotrade.com';
        $user->password = bcrypt('admin');
        $user->online = true;
        $user->confirmed = true;
        $user->save();

        $userSyncro = new User();
        $userSyncro->name = 'syncro';
        $userSyncro->email = 'supporto@tecnotrade.com';
        $userSyncro->password = bcrypt('#5yNcR0$upP0rT0!');
        $userSyncro->online = true;
        $userSyncro->confirmed = true;
        $userSyncro->save();

        $userSyncro = new User();
        $userSyncro->name = 'newsletter';
        $userSyncro->email = 'newsletter@tecnotrade.com';
        $userSyncro->password = bcrypt('$N3w5l3tt3r$upP0rT0)');
        $userSyncro->online = true;
        $userSyncro->confirmed = true;
        $userSyncro->save();
    }
}

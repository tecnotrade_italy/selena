<?php

use App\Dictionary;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Icon;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Tab;
use Illuminate\Database\Seeder;
use App\Language;
use App\Role;
use App\User;
use App\Functionality;

class NegotiationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'ContactNegotiationCompleted';
        $dictionary->description = 'Messaggio inviato correttamente!';
        $dictionary->created_id = $idUser;
        $dictionary->save();
        
        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'UserShopNotValued';
        $dictionary->description = 'Utente Negozio non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'UserNegotiatiorNotValued';
        $dictionary->description = 'Utente non valorizzato';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'MessageNotValued';
        $dictionary->description = 'Messaggio non trovato';
        $dictionary->created_id = $idUser;
        $dictionary->save();
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Dictionary;
use App\Enums\SettingEnum;
use App\Enums\PageTypesEnum;
use App\Language;
use App\User;
use App\Tab;
use App\Role;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Setting;
use Illuminate\Support\Carbon;

class TempSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idUser = User::first()->id;
        $idLanguage = Language::first()->id;
        $idRole = Role::where('name', 'admin')->first()->id;

    }
}

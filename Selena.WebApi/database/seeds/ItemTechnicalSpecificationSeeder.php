<?php

use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use Illuminate\Database\Seeder;
use App\Language;
use App\Role;
use App\User;
use App\Functionality;
use App\Icon;
use App\MenuAdmin;
use App\LanguageMenuAdmin;
use App\MenuAdminUserRole;
use App\Tab;
use App\LanguageTab;

class ItemTechnicalSpecificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        #region SETTING TECHINICAL SPECIFICATION

        $functionalitySTS = new Functionality();
        $functionalitySTS->code = 'SettingTechinicalSpecification';
        $functionalitySTS->created_id = $idUser;
        $functionalitySTS->save();

        $iconCog = new Icon();
        $iconCog->description = 'Cog';
        $iconCog->css = 'fa fa-cog';
        $iconCog->created_id = $idUser;
        $iconCog->save();

        $menuAdminSTS = new MenuAdmin();
        $menuAdminSTS->code = 'SettingTechinicalSpecification';
        $menuAdminSTS->functionality_id = $functionalitySTS->id;
        $menuAdminSTS->icon_id = $iconCog->id;
        $menuAdminSTS->created_id = $idUser;
        $menuAdminSTS->save();

        $languageMenuAdminSTS = new LanguageMenuAdmin();
        $languageMenuAdminSTS->language_id = $idLanguage;
        $languageMenuAdminSTS->menu_admin_id = $menuAdminSTS->id;
        $languageMenuAdminSTS->description = 'Configurazioni';
        $languageMenuAdminSTS->created_id = $idUser;
        $languageMenuAdminSTS->save();

        $menuAdminUserRoleSTS = new MenuAdminUserRole();
        $menuAdminUserRoleSTS->menu_admin_id = $menuAdminSTS->id;
        $menuAdminUserRoleSTS->role_id = $idRole;
        $menuAdminUserRoleSTS->enabled = true;
        $menuAdminUserRoleSTS->order = 100;
        $menuAdminUserRoleSTS->created_id = $idUser;
        $menuAdminUserRoleSTS->save();

        #endregion SETTING TECHINICAL SPECIFICATION

        #region MANAGEMENT TECHINICAL SPECIFICATION

        $functionalityMTS = new Functionality();
        $functionalityMTS->code = 'ManagementTechinicalSpecification';
        $functionalityMTS->created_id = $idUser;
        $functionalityMTS->save();

        $iconBars = new Icon();
        $iconBars->description = 'Bars';
        $iconBars->css = 'fa fa-bars';
        $iconBars->created_id = $idUser;
        $iconBars->save();

        $menuAdminMTS = new MenuAdmin();
        $menuAdminMTS->code = 'ManagementTechinicalSpecification';
        $menuAdminMTS->functionality_id = $functionalityMTS->id;
        $menuAdminMTS->url = '/admin/managementTechinicalSpecification';
        $menuAdminMTS->icon_id = $iconBars->id;
        $menuAdminMTS->created_id = $idUser;
        $menuAdminMTS->save();

        $languageMenuAdminMTS = new LanguageMenuAdmin();
        $languageMenuAdminMTS->language_id = $idLanguage;
        $languageMenuAdminMTS->menu_admin_id = $menuAdminMTS->id;
        $languageMenuAdminMTS->description = 'Etichette';
        $languageMenuAdminMTS->created_id = $idUser;
        $languageMenuAdminMTS->save();

        $menuAdminUserRoleMTS = new MenuAdminUserRole();
        $menuAdminUserRoleMTS->menu_admin_id = $menuAdminMTS->id;
        $menuAdminUserRoleMTS->menu_admin_father_id = $menuAdminSTS->id;
        $menuAdminUserRoleMTS->role_id = $idRole;
        $menuAdminUserRoleMTS->enabled = true;
        $menuAdminUserRoleMTS->order = 10;
        $menuAdminUserRoleMTS->created_id = $idUser;
        $menuAdminUserRoleMTS->save();

        $tabContentMTS = new Tab();
        $tabContentMTS->code = 'Setting';
        $tabContentMTS->functionality_id = $functionalityMTS->id;
        $tabContentMTS->order = 10;
        $tabContentMTS->created_id = $idUser;
        $tabContentMTS->save();

        $languageTabContentMTS = new LanguageTab();
        $languageTabContentMTS->language_id = $idLanguage;
        $languageTabContentMTS->tab_id = $tabContentMTS->id;
        $languageTabContentMTS->description = 'Impostazioni';
        $languageTabContentMTS->created_id = $idUser;
        $languageTabContentMTS->save();

        #endregion MANAGEMENT TECHINICAL SPECIFICATION

        #region GROUP TECHINICAL SPECIFICATION

        $functionalityGTS = new Functionality();
        $functionalityGTS->code = 'GroupTechinicalSpecification';
        $functionalityGTS->created_id = $idUser;
        $functionalityGTS->save();

        $iconObjectGroup = new Icon();
        $iconObjectGroup->description = 'Object group';
        $iconObjectGroup->css = 'fa fa-object-group';
        $iconObjectGroup->created_id = $idUser;
        $iconObjectGroup->save();

        $menuAdminGTS = new MenuAdmin();
        $menuAdminGTS->code = 'GroupTechinicalSpecification';
        $menuAdminGTS->functionality_id = $functionalityGTS->id;
        $menuAdminGTS->url = '/admin/groupsTechinicalSpecification';
        $menuAdminGTS->icon_id = $iconObjectGroup->id;
        $menuAdminGTS->created_id = $idUser;
        $menuAdminGTS->save();

        $languageMenuAdminGTS = new LanguageMenuAdmin();
        $languageMenuAdminGTS->language_id = $idLanguage;
        $languageMenuAdminGTS->menu_admin_id = $menuAdminGTS->id;
        $languageMenuAdminGTS->description = 'Gruppi';
        $languageMenuAdminGTS->created_id = $idUser;
        $languageMenuAdminGTS->save();

        $menuAdminUserRoleGTS = new MenuAdminUserRole();
        $menuAdminUserRoleGTS->menu_admin_id = $menuAdminGTS->id;
        $menuAdminUserRoleGTS->menu_admin_father_id = $menuAdminSTS->id;
        $menuAdminUserRoleGTS->role_id = $idRole;
        $menuAdminUserRoleGTS->enabled = true;
        $menuAdminUserRoleGTS->order = 20;
        $menuAdminUserRoleGTS->created_id = $idUser;
        $menuAdminUserRoleGTS->save();

        $tabContentGTS = new Tab();
        $tabContentGTS->code = 'Setting';
        $tabContentGTS->functionality_id = $functionalityGTS->id;
        $tabContentGTS->order = 10;
        $tabContentGTS->created_id = $idUser;
        $tabContentGTS->save();

        $languageTabContentGTS = new LanguageTab();
        $languageTabContentGTS->language_id = $idLanguage;
        $languageTabContentGTS->tab_id = $tabContentGTS->id;
        $languageTabContentGTS->description = 'Impostazioni';
        $languageTabContentGTS->created_id = $idUser;
        $languageTabContentGTS->save();

        #endregion GROUP TECHINICAL SPECIFICATION

        #region GROUP DISPLAY TECHINICAL SPECIFICATION

        $functionalityGDTS = new Functionality();
        $functionalityGDTS->code = 'GroupDisplayTechinicalSpecification';
        $functionalityGDTS->created_id = $idUser;
        $functionalityGDTS->save();

        $iconWindowRestore = new Icon();
        $iconWindowRestore->description = 'Window restore';
        $iconWindowRestore->css = 'fa fa-window-restore';
        $iconWindowRestore->created_id = $idUser;
        $iconWindowRestore->save();

        $menuAdminGDTS = new MenuAdmin();
        $menuAdminGDTS->code = 'GroupDisplayTechinicalSpecification';
        $menuAdminGDTS->functionality_id = $functionalityGDTS->id;
        $menuAdminGDTS->url = '/admin/groupsDisplayTechinicalSpecification';
        $menuAdminGDTS->icon_id = $iconWindowRestore->id;
        $menuAdminGDTS->created_id = $idUser;
        $menuAdminGDTS->save();

        $languageMenuAdminGDTS = new LanguageMenuAdmin();
        $languageMenuAdminGDTS->language_id = $idLanguage;
        $languageMenuAdminGDTS->menu_admin_id = $menuAdminGDTS->id;
        $languageMenuAdminGDTS->description = 'Aggregazioni';
        $languageMenuAdminGDTS->created_id = $idUser;
        $languageMenuAdminGDTS->save();

        $menuAdminUserRoleGDTS = new MenuAdminUserRole();
        $menuAdminUserRoleGDTS->menu_admin_id = $menuAdminGDTS->id;
        $menuAdminUserRoleGDTS->menu_admin_father_id = $menuAdminSTS->id;
        $menuAdminUserRoleGDTS->role_id = $idRole;
        $menuAdminUserRoleGDTS->enabled = true;
        $menuAdminUserRoleGDTS->order = 30;
        $menuAdminUserRoleGDTS->created_id = $idUser;
        $menuAdminUserRoleGDTS->save();

        $tabContentGDTS = new Tab();
        $tabContentGDTS->code = 'Setting';
        $tabContentGDTS->functionality_id = $functionalityGDTS->id;
        $tabContentGDTS->order = 10;
        $tabContentGDTS->created_id = $idUser;
        $tabContentGDTS->save();

        $languageTabContentGDTS = new LanguageTab();
        $languageTabContentGDTS->language_id = $idLanguage;
        $languageTabContentGDTS->tab_id = $tabContentGDTS->id;
        $languageTabContentGDTS->description = 'Impostazioni';
        $languageTabContentGDTS->created_id = $idUser;
        $languageTabContentGDTS->save();

        #endregion GROUP TECHINICAL SPECIFICATION

        #region TECHINICAL SPECIFICATION

        $functionalityTS = new Functionality();
        $functionalityTS->code = 'TechinicalSpecification';
        $functionalityTS->created_id = $idUser;
        $functionalityTS->save();

        $iconServer = new Icon();
        $iconServer->description = 'Bars';
        $iconServer->css = 'fa fa-server';
        $iconServer->created_id = $idUser;
        $iconServer->save();

        $menuAdminTS = new MenuAdmin();
        $menuAdminTS->code = 'TechinicalSpecification';
        $menuAdminTS->functionality_id = $functionalityTS->id;
        $menuAdminTS->url = '/admin/techinicalSpecification';
        $menuAdminTS->icon_id = $iconServer->id;
        $menuAdminTS->created_id = $idUser;
        $menuAdminTS->save();

        $languageMenuAdminTS = new LanguageMenuAdmin();
        $languageMenuAdminTS->language_id = $idLanguage;
        $languageMenuAdminTS->menu_admin_id = $menuAdminTS->id;
        $languageMenuAdminTS->description = 'Specifiche tecniche';
        $languageMenuAdminTS->created_id = $idUser;
        $languageMenuAdminTS->save();

        $menuAdminUserRoleTS = new MenuAdminUserRole();
        $menuAdminUserRoleTS->menu_admin_id = $menuAdminTS->id;
        $menuAdminUserRoleTS->role_id = $idRole;
        $menuAdminUserRoleTS->enabled = true;
        $menuAdminUserRoleTS->order = 50;
        $menuAdminUserRoleTS->created_id = $idUser;
        $menuAdminUserRoleTS->save();

        $tabContentTS = new Tab();
        $tabContentTS->code = 'Items';
        $tabContentTS->functionality_id = $functionalityTS->id;
        $tabContentTS->order = 10;
        $tabContentTS->created_id = $idUser;
        $tabContentTS->save();

        $languageTabContentTS = new LanguageTab();
        $languageTabContentTS->language_id = $idLanguage;
        $languageTabContentTS->tab_id = $tabContentTS->id;
        $languageTabContentTS->description = 'Articoli';
        $languageTabContentTS->created_id = $idUser;
        $languageTabContentTS->save();

        #region CHECK BOX

        $fieldFormatter = new Field();
        $fieldFormatter->code = 'Formatter';
        $fieldFormatter->formatter = 'technicalSpecificationFormatter';
        $fieldFormatter->tab_id = $tabContentTS->id;
        $fieldFormatter->created_id = $idUser;
        $fieldFormatter->save();

        $fieldLanguageFormatter = new FieldLanguage();
        $fieldLanguageFormatter->field_id = $fieldFormatter->id;
        $fieldLanguageFormatter->language_id = $idLanguage;
        $fieldLanguageFormatter->description = '';
        $fieldLanguageFormatter->created_id = $idUser;
        $fieldLanguageFormatter->save();

        $fieldUserRoleFormatter = new FieldUserRole();
        $fieldUserRoleFormatter->field_id = $fieldFormatter->id;
        $fieldUserRoleFormatter->role_id = $idRole;
        $fieldUserRoleFormatter->enabled = true;
        $fieldUserRoleFormatter->table_order = 10;
        $fieldUserRoleFormatter->input_type = 14;
        $fieldUserRoleFormatter->created_id = $idUser;
        $fieldUserRoleFormatter->save();

        #endregion CHECK BOX

        #region INTERNAL CODE

        $fieldInternalCode = new Field();
        $fieldInternalCode->code = 'InternalCode';
        $fieldInternalCode->field = 'internalCodeItem';
        $fieldInternalCode->data_type = 1;
        $fieldInternalCode->tab_id = $tabContentTS->id;
        $fieldInternalCode->created_id = $idUser;
        $fieldInternalCode->save();

        $fieldLanguageInternalCode = new FieldLanguage();
        $fieldLanguageInternalCode->field_id = $fieldInternalCode->id;
        $fieldLanguageInternalCode->language_id = $idLanguage;
        $fieldLanguageInternalCode->description = 'Codice';
        $fieldLanguageInternalCode->created_id = $idUser;
        $fieldLanguageInternalCode->save();

        $fieldUserRoleInternalCode = new FieldUserRole();
        $fieldUserRoleInternalCode->field_id = $fieldInternalCode->id;
        $fieldUserRoleInternalCode->role_id = $idRole;
        $fieldUserRoleInternalCode->enabled = true;
        $fieldUserRoleInternalCode->table_order = 20;
        $fieldUserRoleInternalCode->input_type = 15;
        $fieldUserRoleInternalCode->created_id = $idUser;
        $fieldUserRoleInternalCode->save();

        #endregion INTERNAL CODE

        #region DESCRIPTION

        $fieldDescription = new Field();
        $fieldDescription->code = 'Description';
        $fieldDescription->field = 'descriptionItem';
        $fieldDescription->data_type = 1;
        $fieldDescription->tab_id = $tabContentTS->id;
        $fieldDescription->created_id = $idUser;
        $fieldDescription->save();

        $fieldLanguageDescription = new FieldLanguage();
        $fieldLanguageDescription->field_id = $fieldDescription->id;
        $fieldLanguageDescription->language_id = $idLanguage;
        $fieldLanguageDescription->description = 'Descrizione';
        $fieldLanguageDescription->created_id = $idUser;
        $fieldLanguageDescription->save();

        $fieldUserRoleDescription = new FieldUserRole();
        $fieldUserRoleDescription->field_id = $fieldDescription->id;
        $fieldUserRoleDescription->role_id = $idRole;
        $fieldUserRoleDescription->enabled = true;
        $fieldUserRoleDescription->table_order = 30;
        $fieldUserRoleDescription->input_type = 15;
        $fieldUserRoleDescription->created_id = $idUser;
        $fieldUserRoleDescription->save();

        #endregion DESCRIPTION

        #region GROUP DESCRIPTION

        $fieldInternalGroupDescription = new Field();
        $fieldInternalGroupDescription->code = 'GroupDescription';
        $fieldInternalGroupDescription->field = 'descriptionGroupTechnicalSpecification';
        $fieldInternalGroupDescription->data_type = 1;
        $fieldInternalGroupDescription->tab_id = $tabContentTS->id;
        $fieldInternalGroupDescription->created_id = $idUser;
        $fieldInternalGroupDescription->save();

        $fieldLanguageGroupDescription = new FieldLanguage();
        $fieldLanguageGroupDescription->field_id = $fieldInternalGroupDescription->id;
        $fieldLanguageGroupDescription->language_id = $idLanguage;
        $fieldLanguageGroupDescription->description = 'Gruppo';
        $fieldLanguageGroupDescription->created_id = $idUser;
        $fieldLanguageGroupDescription->save();

        $fieldUserRoleGroupDescription = new FieldUserRole();
        $fieldUserRoleGroupDescription->field_id = $fieldInternalGroupDescription->id;
        $fieldUserRoleGroupDescription->role_id = $idRole;
        $fieldUserRoleGroupDescription->enabled = true;
        $fieldUserRoleGroupDescription->table_order = 40;
        $fieldUserRoleGroupDescription->input_type = 15;
        $fieldUserRoleGroupDescription->created_id = $idUser;
        $fieldUserRoleGroupDescription->save();

        #endregion GROUP DESCRIPTION

        #region HIDE MANAGEMENT

        $fieldCanManage = new Field();
        $fieldCanManage->code = 'canManage';
        $fieldCanManage->tab_id = $tabContentTS->id;
        $fieldCanManage->default_value = true;
        $fieldCanManage->created_id = $idUser;
        $fieldCanManage->save();

        $fieldLanguageCanManage = new FieldLanguage();
        $fieldLanguageCanManage->field_id = $fieldCanManage->id;
        $fieldLanguageCanManage->language_id = $idLanguage;
        $fieldLanguageCanManage->description = 'canManage';
        $fieldLanguageCanManage->created_id = $idUser;
        $fieldLanguageCanManage->save();

        $fieldUserRoleCanManage = new FieldUserRole();
        $fieldUserRoleCanManage->field_id = $fieldCanManage->id;
        $fieldUserRoleCanManage->role_id = $idRole;
        $fieldUserRoleCanManage->enabled = true;
        $fieldUserRoleCanManage->pos_x = 1;
        $fieldUserRoleCanManage->pos_y = 1;
        $fieldUserRoleCanManage->input_type = 12;
        $fieldUserRoleCanManage->created_id = $idUser;
        $fieldUserRoleCanManage->save();

        #endregion HIDE MANAGEMENT

        #endregion TECHINICAL SPECIFICATION

        #region MANAGEMENT

        $roleManagement = new Role();
        $roleManagement->name = 'management';
        $roleManagement->display_name = 'Gestione';
        $roleManagement->description = 'Gestione';
        $roleManagement->created_id = $idUser;
        $roleManagement->save();

        #region TECHINICAL SPECIFICATION

        $menuAdminUserRoleTSManagement = new MenuAdminUserRole();
        $menuAdminUserRoleTSManagement->menu_admin_id = $menuAdminTS->id;
        $menuAdminUserRoleTSManagement->role_id = $roleManagement->id;
        $menuAdminUserRoleTSManagement->enabled = true;
        $menuAdminUserRoleTSManagement->order = 50;
        $menuAdminUserRoleTSManagement->created_id = $idUser;
        $menuAdminUserRoleTSManagement->save();

        #region CHECK BOX

        $fieldUserRoleFormatterManagement = new FieldUserRole();
        $fieldUserRoleFormatterManagement->field_id = $fieldFormatter->id;
        $fieldUserRoleFormatterManagement->role_id = $roleManagement->id;
        $fieldUserRoleFormatterManagement->enabled = true;
        $fieldUserRoleFormatterManagement->table_order = 10;
        $fieldUserRoleFormatterManagement->input_type = 15;
        $fieldUserRoleFormatterManagement->created_id = $idUser;
        $fieldUserRoleFormatterManagement->save();

        #endregion CHECK BOX

        #region INTERNAL CODE

        $fieldUserRoleInternalCodeManagement = new FieldUserRole();
        $fieldUserRoleInternalCodeManagement->field_id = $fieldInternalCode->id;
        $fieldUserRoleInternalCodeManagement->role_id = $roleManagement->id;
        $fieldUserRoleInternalCodeManagement->enabled = true;
        $fieldUserRoleInternalCodeManagement->table_order = 20;
        $fieldUserRoleInternalCodeManagement->input_type = 15;
        $fieldUserRoleInternalCodeManagement->created_id = $idUser;
        $fieldUserRoleInternalCodeManagement->save();

        #endregion INTERNAL CODE

        #region DESCRIPTION

        $fieldUserRoleDescriptionManagement = new FieldUserRole();
        $fieldUserRoleDescriptionManagement->field_id = $fieldDescription->id;
        $fieldUserRoleDescriptionManagement->role_id = $roleManagement->id;
        $fieldUserRoleDescriptionManagement->enabled = true;
        $fieldUserRoleDescriptionManagement->table_order = 30;
        $fieldUserRoleDescriptionManagement->input_type = 15;
        $fieldUserRoleDescriptionManagement->created_id = $idUser;
        $fieldUserRoleDescriptionManagement->save();

        #endregion DESCRIPTION

        #region GROUP DESCRIPTION

        $fieldUserRoleGroupDescriptionManagement = new FieldUserRole();
        $fieldUserRoleGroupDescriptionManagement->field_id = $fieldInternalGroupDescription->id;
        $fieldUserRoleGroupDescriptionManagement->role_id = $roleManagement->id;
        $fieldUserRoleGroupDescriptionManagement->enabled = true;
        $fieldUserRoleGroupDescriptionManagement->table_order = 40;
        $fieldUserRoleGroupDescriptionManagement->input_type = 15;
        $fieldUserRoleGroupDescriptionManagement->created_id = $idUser;
        $fieldUserRoleGroupDescriptionManagement->save();

        #endregion GROUP DESCRIPTION

        #region HIDE MANAGEMENT

        $fieldUserRoleCanManage = new FieldUserRole();
        $fieldUserRoleCanManage->field_id = $fieldCanManage->id;
        $fieldUserRoleCanManage->role_id = $roleManagement->id;
        $fieldUserRoleCanManage->enabled = true;
        $fieldUserRoleCanManage->pos_x = 1;
        $fieldUserRoleCanManage->pos_y = 1;
        $fieldUserRoleCanManage->input_type = 12;
        $fieldUserRoleCanManage->created_id = $idUser;
        $fieldUserRoleCanManage->save();

        #endregion HIDE MANAGEMENT

        #endregion TECHINICAL SPECIFICATION

        #region MANAGEMENT TECHINICAL SPECIFICATION

        $menuAdminUserRoleMTSManagement = new MenuAdminUserRole();
        $menuAdminUserRoleMTSManagement->menu_admin_id = $menuAdminMTS->id;
        $menuAdminUserRoleMTSManagement->menu_admin_father_id = $menuAdminSTS->id;
        $menuAdminUserRoleMTSManagement->role_id = $roleManagement->id;
        $menuAdminUserRoleMTSManagement->enabled = true;
        $menuAdminUserRoleMTSManagement->order = 50;
        $menuAdminUserRoleMTSManagement->created_id = $idUser;
        $menuAdminUserRoleMTSManagement->save();

        #endregion MANAGEMENT TECHINICAL SPECIFICATION

        #region GROUP TECHINICAL SPECIFICATION

        $menuAdminUserRoleGTSManagement = new MenuAdminUserRole();
        $menuAdminUserRoleGTSManagement->menu_admin_id = $menuAdminGTS->id;
        $menuAdminUserRoleGTSManagement->menu_admin_father_id = $menuAdminSTS->id;
        $menuAdminUserRoleGTSManagement->role_id = $roleManagement->id;
        $menuAdminUserRoleGTSManagement->enabled = true;
        $menuAdminUserRoleGTSManagement->order = 50;
        $menuAdminUserRoleGTSManagement->created_id = $idUser;
        $menuAdminUserRoleGTSManagement->save();

        #endregion GROUP TECHINICAL SPECIFICATION

        #region GROUP DISPLAY TECHINICAL SPECIFICATION

        $menuAdminUserRoleGDTSManagement = new MenuAdminUserRole();
        $menuAdminUserRoleGDTSManagement->menu_admin_id = $menuAdminGDTS->id;
        $menuAdminUserRoleGDTSManagement->menu_admin_father_id = $menuAdminSTS->id;
        $menuAdminUserRoleGDTSManagement->role_id = $roleManagement->id;
        $menuAdminUserRoleGDTSManagement->enabled = true;
        $menuAdminUserRoleGDTSManagement->order = 50;
        $menuAdminUserRoleGDTSManagement->created_id = $idUser;
        $menuAdminUserRoleGDTSManagement->save();

        #endregion GROUP DISPLAY TECHINICAL SPECIFICATION

        #endregion MANAGEMENT

        #region READ ONLY

        $roleReadOnly = new Role();
        $roleReadOnly->name = 'readOnly';
        $roleReadOnly->display_name = 'Lettura';
        $roleReadOnly->description = 'Sola lettura';
        $roleReadOnly->created_id = $idUser;
        $roleReadOnly->save();

        #region TECHINICAL SPECIFICATION

        $menuAdminUserRoleTSReadOnly = new MenuAdminUserRole();
        $menuAdminUserRoleTSReadOnly->menu_admin_id = $menuAdminTS->id;
        $menuAdminUserRoleTSReadOnly->role_id = $roleReadOnly->id;
        $menuAdminUserRoleTSReadOnly->enabled = true;
        $menuAdminUserRoleTSReadOnly->order = 50;
        $menuAdminUserRoleTSReadOnly->created_id = $idUser;
        $menuAdminUserRoleTSReadOnly->save();

        #region CHECK BOX

        $fieldUserRoleFormatterReadOnly = new FieldUserRole();
        $fieldUserRoleFormatterReadOnly->field_id = $fieldFormatter->id;
        $fieldUserRoleFormatterReadOnly->role_id = $roleReadOnly->id;
        $fieldUserRoleFormatterReadOnly->enabled = true;
        $fieldUserRoleFormatterReadOnly->table_order = 10;
        $fieldUserRoleFormatterReadOnly->input_type = 15;
        $fieldUserRoleFormatterReadOnly->created_id = $idUser;
        $fieldUserRoleFormatterReadOnly->save();

        #endregion CHECK BOX

        #region INTERNAL CODE

        $fieldUserRoleInternalCodeReadOnly = new FieldUserRole();
        $fieldUserRoleInternalCodeReadOnly->field_id = $fieldInternalCode->id;
        $fieldUserRoleInternalCodeReadOnly->role_id = $roleReadOnly->id;
        $fieldUserRoleInternalCodeReadOnly->enabled = true;
        $fieldUserRoleInternalCodeReadOnly->table_order = 20;
        $fieldUserRoleInternalCodeReadOnly->input_type = 15;
        $fieldUserRoleInternalCodeReadOnly->created_id = $idUser;
        $fieldUserRoleInternalCodeReadOnly->save();

        #endregion INTERNAL CODE

        #region DESCRIPTION

        $fieldUserRoleDescriptionReadOnly = new FieldUserRole();
        $fieldUserRoleDescriptionReadOnly->field_id = $fieldDescription->id;
        $fieldUserRoleDescriptionReadOnly->role_id = $roleReadOnly->id;
        $fieldUserRoleDescriptionReadOnly->enabled = true;
        $fieldUserRoleDescriptionReadOnly->table_order = 30;
        $fieldUserRoleDescriptionReadOnly->input_type = 15;
        $fieldUserRoleDescriptionReadOnly->created_id = $idUser;
        $fieldUserRoleDescriptionReadOnly->save();

        #endregion DESCRIPTION

        #region GROUP DESCRIPTION

        $fieldUserRoleGroupDescriptionReadOnly = new FieldUserRole();
        $fieldUserRoleGroupDescriptionReadOnly->field_id = $fieldInternalGroupDescription->id;
        $fieldUserRoleGroupDescriptionReadOnly->role_id = $roleReadOnly->id;
        $fieldUserRoleGroupDescriptionReadOnly->enabled = true;
        $fieldUserRoleGroupDescriptionReadOnly->table_order = 40;
        $fieldUserRoleGroupDescriptionReadOnly->input_type = 15;
        $fieldUserRoleGroupDescriptionReadOnly->created_id = $idUser;
        $fieldUserRoleGroupDescriptionReadOnly->save();

        #endregion GROUP DESCRIPTION

        #region HIDE MANAGEMENT

        $fieldUserRoleCanManage = new FieldUserRole();
        $fieldUserRoleCanManage->field_id = $fieldCanManage->id;
        $fieldUserRoleCanManage->role_id = $roleReadOnly->id;
        $fieldUserRoleCanManage->enabled = true;
        $fieldUserRoleCanManage->pos_x = 1;
        $fieldUserRoleCanManage->pos_y = 1;
        $fieldUserRoleCanManage->input_type = 12;
        $fieldUserRoleCanManage->default_value = false;
        $fieldUserRoleCanManage->created_id = $idUser;
        $fieldUserRoleCanManage->save();

        #endregion HIDE MANAGEMENT

        #endregion TECHINICAL SPECIFICATION

        #endregion READ ONLY
    }
}

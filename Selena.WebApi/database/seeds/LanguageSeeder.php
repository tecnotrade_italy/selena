<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Language;
use App\User;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idUser = User::first()->id;

        $language = new Language();
        $language->description = 'Italiano';
        $language->default = true;
        $language->created_id = $idUser;
        $language->date_format = 'd/m/Y';
        $language->date_time_format = 'd/m/Y H:i';
        $language->date_format_client = 'DD/MM/YYYY';
        $language->date_time_format_client = 'DD/MM/YYYY HH:mm';
        $language->save();

        $user = User::first();
        $user->language_id = $language->id;
        $user->save();

        $user = User::where('name', 'syncro')->first();
        $user->language_id = $language->id;
        $user->save();

        $user = User::where('name', 'newsletter')->first();
        $user->language_id = $language->id;
        $user->save();

        $language = new Language();
        $language->description = 'English';
        $language->default = false;
        $language->created_id = $idUser;
        $language->date_format = 'Y/m/d';
        $language->date_time_format = 'Y/m/d H:i';
        $language->date_format_client = 'YYYY/MM/DD';
        $language->date_time_format_client = 'YYYY/MM/DD HH:mm';
        $language->save();
    }
}

<?php

use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Icon;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Tab;
use Illuminate\Database\Seeder;
use App\Language;
use App\Role;
use App\User;
use App\Functionality;

class NewsletterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        #region NEWSLETTER

        $functionalityNewsletter = new Functionality();
        $functionalityNewsletter->code = 'Newsletter';
        $functionalityNewsletter->created_id = $idUser;
        $functionalityNewsletter->save();

        $iconEmail = new Icon();
        $iconEmail->description = 'Email';
        $iconEmail->css = 'fa fa-envelope-o';
        $iconEmail->created_id = $idUser;
        $iconEmail->save();

        $iconListOl = new Icon();
        $iconListOl->description = 'List ol';
        $iconListOl->css = 'fa fa-list-ol';
        $iconListOl->created_id = $idUser;
        $iconListOl->save();

        #region MENU

        $menuAdminNewsletter = new MenuAdmin();
        $menuAdminNewsletter->code = 'Newsletter';
        $menuAdminNewsletter->icon_id = $iconEmail->id;
        $menuAdminNewsletter->created_id = $idUser;
        $menuAdminNewsletter->save();

        $languageMenuAdminNewsletter = new LanguageMenuAdmin();
        $languageMenuAdminNewsletter->language_id = $idLanguage;
        $languageMenuAdminNewsletter->menu_admin_id = $menuAdminNewsletter->id;
        $languageMenuAdminNewsletter->description = 'Newsletter';
        $languageMenuAdminNewsletter->created_id = $idUser;
        $languageMenuAdminNewsletter->save();

        $menuAdminUserRoleNewsletter = new MenuAdminUserRole();
        $menuAdminUserRoleNewsletter->menu_admin_id = $menuAdminNewsletter->id;
        $menuAdminUserRoleNewsletter->role_id = $idRole;
        $menuAdminUserRoleNewsletter->enabled = true;
        $menuAdminUserRoleNewsletter->order = 100;
        $menuAdminUserRoleNewsletter->created_id = $idUser;
        $menuAdminUserRoleNewsletter->save();

        $menuAdminNewsletterCampaign = new MenuAdmin();
        $menuAdminNewsletterCampaign->code = 'NewsletterCampaign';
        $menuAdminNewsletterCampaign->functionality_id = $functionalityNewsletter->id;
        $menuAdminNewsletterCampaign->icon_id = $iconListOl->id;
        $menuAdminNewsletterCampaign->url = '/admin/newsletter/campaign';
        $menuAdminNewsletterCampaign->created_id = $idUser;
        $menuAdminNewsletterCampaign->save();

        $languageMenuAdminNewsletterCampaign = new LanguageMenuAdmin();
        $languageMenuAdminNewsletterCampaign->language_id = $idLanguage;
        $languageMenuAdminNewsletterCampaign->menu_admin_id = $menuAdminNewsletterCampaign->id;
        $languageMenuAdminNewsletterCampaign->description = 'Campagne';
        $languageMenuAdminNewsletterCampaign->created_id = $idUser;
        $languageMenuAdminNewsletterCampaign->save();

        $menuAdminUserRoleNewsletterCampaign = new MenuAdminUserRole();
        $menuAdminUserRoleNewsletterCampaign->menu_admin_id = $menuAdminNewsletterCampaign->id;
        $menuAdminUserRoleNewsletterCampaign->menu_admin_father_id = $menuAdminNewsletter->id;
        $menuAdminUserRoleNewsletterCampaign->role_id = $idRole;
        $menuAdminUserRoleNewsletterCampaign->enabled = true;
        $menuAdminUserRoleNewsletterCampaign->order = 10;
        $menuAdminUserRoleNewsletterCampaign->created_id = $idUser;
        $menuAdminUserRoleNewsletterCampaign->save();

        #endregion MENU

        #region TABLE

        #region TAB

        $tabListNewsletterCampaign = new Tab();
        $tabListNewsletterCampaign->code = 'List';
        $tabListNewsletterCampaign->functionality_id = $functionalityNewsletter->id;
        $tabListNewsletterCampaign->order = 10;
        $tabListNewsletterCampaign->created_id = $idUser;
        $tabListNewsletterCampaign->save();

        $languageTabListNewsletterCampaign = new LanguageTab();
        $languageTabListNewsletterCampaign->language_id = $idLanguage;
        $languageTabListNewsletterCampaign->tab_id = $tabListNewsletterCampaign->id;
        $languageTabListNewsletterCampaign->description = 'Lista';
        $languageTabListNewsletterCampaign->created_id = $idUser;
        $languageTabListNewsletterCampaign->save();

        #endregion TAB

        #region CHECKBOX

        $fieldCheckBoxTableNewsletterCampaign = new Field();
        $fieldCheckBoxTableNewsletterCampaign->tab_id = $tabListNewsletterCampaign->id;
        $fieldCheckBoxTableNewsletterCampaign->code = 'TableCheckBox';
        $fieldCheckBoxTableNewsletterCampaign->created_id = $idUser;
        $fieldCheckBoxTableNewsletterCampaign->save();

        $fieldLanguageCheckBoxTableNewsletterCampaign = new FieldLanguage();
        $fieldLanguageCheckBoxTableNewsletterCampaign->field_id = $fieldCheckBoxTableNewsletterCampaign->id;
        $fieldLanguageCheckBoxTableNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageCheckBoxTableNewsletterCampaign->description = 'CheckBox';
        $fieldLanguageCheckBoxTableNewsletterCampaign->created_id = $idUser;
        $fieldLanguageCheckBoxTableNewsletterCampaign->save();

        $fieldUserRoleCheckBoxTableNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleCheckBoxTableNewsletterCampaign->field_id = $fieldCheckBoxTableNewsletterCampaign->id;
        $fieldUserRoleCheckBoxTableNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleCheckBoxTableNewsletterCampaign->enabled = true;
        $fieldUserRoleCheckBoxTableNewsletterCampaign->table_order = 100;
        $fieldUserRoleCheckBoxTableNewsletterCampaign->input_type = 13;
        $fieldUserRoleCheckBoxTableNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleCheckBoxTableNewsletterCampaign->save();

        #endregion CHECKBOX

        #region FORMATTER

        $fieldFormatterTableNewsletterCampaign = new Field();
        $fieldFormatterTableNewsletterCampaign->code = 'NewsletterFormatter';
        $fieldFormatterTableNewsletterCampaign->formatter = 'newsletterFormatter';
        $fieldFormatterTableNewsletterCampaign->tab_id = $tabListNewsletterCampaign->id;
        $fieldFormatterTableNewsletterCampaign->created_id = $idUser;
        $fieldFormatterTableNewsletterCampaign->save();

        $fieldLanguageFormatterTableNewsletterCampaign = new FieldLanguage();
        $fieldLanguageFormatterTableNewsletterCampaign->field_id = $fieldFormatterTableNewsletterCampaign->id;
        $fieldLanguageFormatterTableNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageFormatterTableNewsletterCampaign->description = '';
        $fieldLanguageFormatterTableNewsletterCampaign->created_id = $idUser;
        $fieldLanguageFormatterTableNewsletterCampaign->save();

        $fieldUserRoleFormatterTableNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleFormatterTableNewsletterCampaign->field_id = $fieldFormatterTableNewsletterCampaign->id;
        $fieldUserRoleFormatterTableNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleFormatterTableNewsletterCampaign->enabled = true;
        $fieldUserRoleFormatterTableNewsletterCampaign->table_order = 10;
        $fieldUserRoleFormatterTableNewsletterCampaign->input_type = 14;
        $fieldUserRoleFormatterTableNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleFormatterTableNewsletterCampaign->save();

        #endregion FORMATTER

        #region DESCRIPTION

        $fieldDescriptionTableNewsletterCampaign = new Field();
        $fieldDescriptionTableNewsletterCampaign->code = 'Description';
        $fieldDescriptionTableNewsletterCampaign->tab_id = $tabListNewsletterCampaign->id;
        $fieldDescriptionTableNewsletterCampaign->field = 'description';
        $fieldDescriptionTableNewsletterCampaign->data_type = 1;
        $fieldDescriptionTableNewsletterCampaign->required = true;
        $fieldDescriptionTableNewsletterCampaign->created_id = $idUser;
        $fieldDescriptionTableNewsletterCampaign->save();

        $fieldLanguageDescriptionTableNewsletterCampaign = new FieldLanguage();
        $fieldLanguageDescriptionTableNewsletterCampaign->field_id = $fieldDescriptionTableNewsletterCampaign->id;
        $fieldLanguageDescriptionTableNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageDescriptionTableNewsletterCampaign->description = 'Nome';
        $fieldLanguageDescriptionTableNewsletterCampaign->created_id = $idUser;
        $fieldLanguageDescriptionTableNewsletterCampaign->save();

        $fieldUserRoleDescriptionTableNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleDescriptionTableNewsletterCampaign->field_id = $fieldDescriptionTableNewsletterCampaign->id;
        $fieldUserRoleDescriptionTableNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleDescriptionTableNewsletterCampaign->enabled = true;
        $fieldUserRoleDescriptionTableNewsletterCampaign->input_type = 1;
        $fieldUserRoleDescriptionTableNewsletterCampaign->table_order = 20;
        $fieldUserRoleDescriptionTableNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleDescriptionTableNewsletterCampaign->save();

        #endregion DESCRIPTION

        #region TITLE

        $fieldTitleTableNewsletterCampaign = new Field();
        $fieldTitleTableNewsletterCampaign->code = 'Title';
        $fieldTitleTableNewsletterCampaign->tab_id = $tabListNewsletterCampaign->id;
        $fieldTitleTableNewsletterCampaign->field = 'title';
        $fieldTitleTableNewsletterCampaign->data_type = 1;
        $fieldTitleTableNewsletterCampaign->required = true;
        $fieldTitleTableNewsletterCampaign->created_id = $idUser;
        $fieldTitleTableNewsletterCampaign->save();

        $fieldLanguageTitleTableNewsletterCampaign = new FieldLanguage();
        $fieldLanguageTitleTableNewsletterCampaign->field_id = $fieldTitleTableNewsletterCampaign->id;
        $fieldLanguageTitleTableNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageTitleTableNewsletterCampaign->description = 'Oggetto';
        $fieldLanguageTitleTableNewsletterCampaign->created_id = $idUser;
        $fieldLanguageTitleTableNewsletterCampaign->save();

        $fieldUserRoleTitleTableNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleTitleTableNewsletterCampaign->field_id = $fieldTitleTableNewsletterCampaign->id;
        $fieldUserRoleTitleTableNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleTitleTableNewsletterCampaign->enabled = true;
        $fieldUserRoleTitleTableNewsletterCampaign->input_type = 1;
        $fieldUserRoleTitleTableNewsletterCampaign->table_order = 30;
        $fieldUserRoleTitleTableNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleTitleTableNewsletterCampaign->save();

        #endregion TITLE

        #region URL

        $fieldUrlTableNewsletterCampaign = new Field();
        $fieldUrlTableNewsletterCampaign->code = 'Url';
        $fieldUrlTableNewsletterCampaign->tab_id = $tabListNewsletterCampaign->id;
        $fieldUrlTableNewsletterCampaign->field = 'url';
        $fieldUrlTableNewsletterCampaign->data_type = 1;
        $fieldUrlTableNewsletterCampaign->required = true;
        $fieldUrlTableNewsletterCampaign->on_change = "adminNewsletterCampaign.__vue__.checkUrl";
        $fieldUrlTableNewsletterCampaign->created_id = $idUser;
        $fieldUrlTableNewsletterCampaign->save();

        $fieldLanguageUrlTableNewsletterCampaign = new FieldLanguage();
        $fieldLanguageUrlTableNewsletterCampaign->field_id = $fieldUrlTableNewsletterCampaign->id;
        $fieldLanguageUrlTableNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageUrlTableNewsletterCampaign->description = 'Url';
        $fieldLanguageUrlTableNewsletterCampaign->created_id = $idUser;
        $fieldLanguageUrlTableNewsletterCampaign->save();

        $fieldUserRoleUrlTableNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleUrlTableNewsletterCampaign->field_id = $fieldUrlTableNewsletterCampaign->id;
        $fieldUserRoleUrlTableNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleUrlTableNewsletterCampaign->enabled = true;
        $fieldUserRoleUrlTableNewsletterCampaign->input_type = 1;
        $fieldUserRoleUrlTableNewsletterCampaign->table_order = 40;
        $fieldUserRoleUrlTableNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleUrlTableNewsletterCampaign->save();

        #endregion URL

        #region DRAFT

        $fieldDraftTableNewsletterCampaign = new Field();
        $fieldDraftTableNewsletterCampaign->code = 'Draft';
        $fieldDraftTableNewsletterCampaign->tab_id = $tabListNewsletterCampaign->id;
        $fieldDraftTableNewsletterCampaign->field = 'draft';
        $fieldDraftTableNewsletterCampaign->data_type = 3;
        $fieldDraftTableNewsletterCampaign->created_id = $idUser;
        $fieldDraftTableNewsletterCampaign->save();

        $fieldLanguageDraftTableNewsletterCampaign = new FieldLanguage();
        $fieldLanguageDraftTableNewsletterCampaign->field_id = $fieldDraftTableNewsletterCampaign->id;
        $fieldLanguageDraftTableNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageDraftTableNewsletterCampaign->description = 'Bozza';
        $fieldLanguageDraftTableNewsletterCampaign->created_id = $idUser;
        $fieldLanguageDraftTableNewsletterCampaign->save();

        $fieldUserRoleDraftTableNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleDraftTableNewsletterCampaign->field_id = $fieldDraftTableNewsletterCampaign->id;
        $fieldUserRoleDraftTableNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleDraftTableNewsletterCampaign->enabled = true;
        $fieldUserRoleDraftTableNewsletterCampaign->input_type = 15;
        $fieldUserRoleDraftTableNewsletterCampaign->table_order = 90;
        $fieldUserRoleDraftTableNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleDraftTableNewsletterCampaign->save();

        #endregion DRAFT

        #region SCHEDULE DATE TIME

        $fieldScheduleDateTimeTableNewsletterCampaign = new Field();
        $fieldScheduleDateTimeTableNewsletterCampaign->code = 'ScheduleDateTime';
        $fieldScheduleDateTimeTableNewsletterCampaign->tab_id = $tabListNewsletterCampaign->id;
        $fieldScheduleDateTimeTableNewsletterCampaign->field = 'scheduleDateTime';
        $fieldScheduleDateTimeTableNewsletterCampaign->data_type = 2;
        $fieldScheduleDateTimeTableNewsletterCampaign->created_id = $idUser;
        $fieldScheduleDateTimeTableNewsletterCampaign->save();

        $fieldLanguageScheduleDateTimeTableNewsletterCampaign = new FieldLanguage();
        $fieldLanguageScheduleDateTimeTableNewsletterCampaign->field_id = $fieldScheduleDateTimeTableNewsletterCampaign->id;
        $fieldLanguageScheduleDateTimeTableNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageScheduleDateTimeTableNewsletterCampaign->description = 'Data e ora Schedulazione';
        $fieldLanguageScheduleDateTimeTableNewsletterCampaign->created_id = $idUser;
        $fieldLanguageScheduleDateTimeTableNewsletterCampaign->save();

        $fieldUserRoleScheduleDateTimeTableNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleScheduleDateTimeTableNewsletterCampaign->field_id = $fieldScheduleDateTimeTableNewsletterCampaign->id;
        $fieldUserRoleScheduleDateTimeTableNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleScheduleDateTimeTableNewsletterCampaign->enabled = true;
        $fieldUserRoleScheduleDateTimeTableNewsletterCampaign->input_type = 15;
        $fieldUserRoleScheduleDateTimeTableNewsletterCampaign->table_order = 50;
        $fieldUserRoleScheduleDateTimeTableNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleScheduleDateTimeTableNewsletterCampaign->save();

        #endregion SCHEDULE DATE TIME

        #region START SEND DATE TIME

        $fieldStartSendDateTimeTableNewsletterCampaign = new Field();
        $fieldStartSendDateTimeTableNewsletterCampaign->code = 'StartSendDateTime';
        $fieldStartSendDateTimeTableNewsletterCampaign->tab_id = $tabListNewsletterCampaign->id;
        $fieldStartSendDateTimeTableNewsletterCampaign->field = 'startSendDateTime';
        $fieldStartSendDateTimeTableNewsletterCampaign->data_type = 2;
        $fieldStartSendDateTimeTableNewsletterCampaign->created_id = $idUser;
        $fieldStartSendDateTimeTableNewsletterCampaign->save();

        $fieldLanguageStartSendDateTimeTableNewsletterCampaign = new FieldLanguage();
        $fieldLanguageStartSendDateTimeTableNewsletterCampaign->field_id = $fieldStartSendDateTimeTableNewsletterCampaign->id;
        $fieldLanguageStartSendDateTimeTableNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageStartSendDateTimeTableNewsletterCampaign->description = 'Data e ora Inizio spedizione';
        $fieldLanguageStartSendDateTimeTableNewsletterCampaign->created_id = $idUser;
        $fieldLanguageStartSendDateTimeTableNewsletterCampaign->save();

        $fieldUserRoleStartSendDateTimeTableNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleStartSendDateTimeTableNewsletterCampaign->field_id = $fieldStartSendDateTimeTableNewsletterCampaign->id;
        $fieldUserRoleStartSendDateTimeTableNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleStartSendDateTimeTableNewsletterCampaign->enabled = true;
        $fieldUserRoleStartSendDateTimeTableNewsletterCampaign->input_type = 15;
        $fieldUserRoleStartSendDateTimeTableNewsletterCampaign->table_order = 60;
        $fieldUserRoleStartSendDateTimeTableNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleStartSendDateTimeTableNewsletterCampaign->save();

        #endregion START SEND DATE TIME

        #region END SEND DATE TIME

        $fieldEndSendDateTimeTableNewsletterCampaign = new Field();
        $fieldEndSendDateTimeTableNewsletterCampaign->code = 'EndSendDateTime';
        $fieldEndSendDateTimeTableNewsletterCampaign->tab_id = $tabListNewsletterCampaign->id;
        $fieldEndSendDateTimeTableNewsletterCampaign->field = 'endSendDateTime';
        $fieldEndSendDateTimeTableNewsletterCampaign->data_type = 2;
        $fieldEndSendDateTimeTableNewsletterCampaign->created_id = $idUser;
        $fieldEndSendDateTimeTableNewsletterCampaign->save();

        $fieldLanguageEndSendDateTimeTableNewsletterCampaign = new FieldLanguage();
        $fieldLanguageEndSendDateTimeTableNewsletterCampaign->field_id = $fieldEndSendDateTimeTableNewsletterCampaign->id;
        $fieldLanguageEndSendDateTimeTableNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageEndSendDateTimeTableNewsletterCampaign->description = 'Data e ora Fine spedizione';
        $fieldLanguageEndSendDateTimeTableNewsletterCampaign->created_id = $idUser;
        $fieldLanguageEndSendDateTimeTableNewsletterCampaign->save();

        $fieldUserRoleEndSendDateTimeTableNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleEndSendDateTimeTableNewsletterCampaign->field_id = $fieldEndSendDateTimeTableNewsletterCampaign->id;
        $fieldUserRoleEndSendDateTimeTableNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleEndSendDateTimeTableNewsletterCampaign->enabled = true;
        $fieldUserRoleEndSendDateTimeTableNewsletterCampaign->input_type = 15;
        $fieldUserRoleEndSendDateTimeTableNewsletterCampaign->table_order = 70;
        $fieldUserRoleEndSendDateTimeTableNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleEndSendDateTimeTableNewsletterCampaign->save();

        #endregion END SEND DATE TIME

        #region NUMERO DESTINATARI

        $fieldUrlTableNewsletterCampaign = new Field();
        $fieldUrlTableNewsletterCampaign->code = 'NrDestinatari';
        $fieldUrlTableNewsletterCampaign->tab_id = $tabListNewsletterCampaign->id;
        $fieldUrlTableNewsletterCampaign->field = 'NrDestinatari';
        $fieldUrlTableNewsletterCampaign->data_type = 1;
        $fieldUrlTableNewsletterCampaign->required = true;
        $fieldUrlTableNewsletterCampaign->formatter = 'newsletterNumberRecipients';
        $fieldUrlTableNewsletterCampaign->created_id = $idUser;
        $fieldUrlTableNewsletterCampaign->save();

        $fieldLanguageUrlTableNewsletterCampaign = new FieldLanguage();
        $fieldLanguageUrlTableNewsletterCampaign->field_id = $fieldUrlTableNewsletterCampaign->id;
        $fieldLanguageUrlTableNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageUrlTableNewsletterCampaign->description = 'Nr. Destinatari';
        $fieldLanguageUrlTableNewsletterCampaign->created_id = $idUser;
        $fieldLanguageUrlTableNewsletterCampaign->save();

        $fieldUserRoleUrlTableNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleUrlTableNewsletterCampaign->field_id = $fieldUrlTableNewsletterCampaign->id;
        $fieldUserRoleUrlTableNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleUrlTableNewsletterCampaign->enabled = true;
        $fieldUserRoleUrlTableNewsletterCampaign->input_type = 15;
        $fieldUserRoleUrlTableNewsletterCampaign->table_order = 80;
        $fieldUserRoleUrlTableNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleUrlTableNewsletterCampaign->save();

        #endregion NUMERO DESTINATARI

        #endregion TABLE

        #region DETAIL

        #region TAB

        $tabDetailNewsletterCampaign = new Tab();
        $tabDetailNewsletterCampaign->code = 'Detail';
        $tabDetailNewsletterCampaign->functionality_id = $functionalityNewsletter->id;
        $tabDetailNewsletterCampaign->order = 20;
        $tabDetailNewsletterCampaign->created_id = $idUser;
        $tabDetailNewsletterCampaign->save();

        $languageTabDetailNewsletterCampaign = new LanguageTab();
        $languageTabDetailNewsletterCampaign->language_id = $idLanguage;
        $languageTabDetailNewsletterCampaign->tab_id = $tabDetailNewsletterCampaign->id;
        $languageTabDetailNewsletterCampaign->description = 'Dettaglio';
        $languageTabDetailNewsletterCampaign->created_id = $idUser;
        $languageTabDetailNewsletterCampaign->save();

        #endregion TAB

        #region DESCRIPTION

        $fieldDescriptionDetailNewsletterCampaign = new Field();
        $fieldDescriptionDetailNewsletterCampaign->code = 'Description';
        $fieldDescriptionDetailNewsletterCampaign->tab_id = $tabDetailNewsletterCampaign->id;
        $fieldDescriptionDetailNewsletterCampaign->field = 'description';
        $fieldDescriptionDetailNewsletterCampaign->data_type = 1;
        $fieldDescriptionDetailNewsletterCampaign->required = true;
        $fieldDescriptionDetailNewsletterCampaign->created_id = $idUser;
        $fieldDescriptionDetailNewsletterCampaign->save();

        $fieldLanguageDescriptionDetailNewsletterCampaign = new FieldLanguage();
        $fieldLanguageDescriptionDetailNewsletterCampaign->field_id = $fieldDescriptionDetailNewsletterCampaign->id;
        $fieldLanguageDescriptionDetailNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageDescriptionDetailNewsletterCampaign->description = 'Nome';
        $fieldLanguageDescriptionDetailNewsletterCampaign->created_id = $idUser;
        $fieldLanguageDescriptionDetailNewsletterCampaign->save();

        $fieldUserRoleDescriptionDetailNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleDescriptionDetailNewsletterCampaign->field_id = $fieldDescriptionDetailNewsletterCampaign->id;
        $fieldUserRoleDescriptionDetailNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleDescriptionDetailNewsletterCampaign->enabled = true;
        $fieldUserRoleDescriptionDetailNewsletterCampaign->input_type = 0;
        $fieldUserRoleDescriptionDetailNewsletterCampaign->pos_y = 10;
        $fieldUserRoleDescriptionDetailNewsletterCampaign->pos_x = 10;
        $fieldUserRoleDescriptionDetailNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleDescriptionDetailNewsletterCampaign->save();

        #endregion DESCRIPTION

        #region TITLE

        $fieldTitleDetailNewsletterCampaign = new Field();
        $fieldTitleDetailNewsletterCampaign->code = 'Title';
        $fieldTitleDetailNewsletterCampaign->tab_id = $tabDetailNewsletterCampaign->id;
        $fieldTitleDetailNewsletterCampaign->field = 'title';
        $fieldTitleDetailNewsletterCampaign->data_type = 1;
        $fieldTitleDetailNewsletterCampaign->on_change = 'changeTitle';
        $fieldTitleDetailNewsletterCampaign->required = true;
        $fieldTitleDetailNewsletterCampaign->created_id = $idUser;
        $fieldTitleDetailNewsletterCampaign->save();

        $fieldLanguageTitleDetailNewsletterCampaign = new FieldLanguage();
        $fieldLanguageTitleDetailNewsletterCampaign->field_id = $fieldTitleDetailNewsletterCampaign->id;
        $fieldLanguageTitleDetailNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageTitleDetailNewsletterCampaign->description = 'Oggetto';
        $fieldLanguageTitleDetailNewsletterCampaign->created_id = $idUser;
        $fieldLanguageTitleDetailNewsletterCampaign->save();

        $fieldUserRoleTitleDetailNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleTitleDetailNewsletterCampaign->field_id = $fieldTitleDetailNewsletterCampaign->id;
        $fieldUserRoleTitleDetailNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleTitleDetailNewsletterCampaign->enabled = true;
        $fieldUserRoleTitleDetailNewsletterCampaign->input_type = 0;
        $fieldUserRoleTitleDetailNewsletterCampaign->pos_y = 10;
        $fieldUserRoleTitleDetailNewsletterCampaign->pos_x = 20;
        $fieldUserRoleTitleDetailNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleTitleDetailNewsletterCampaign->save();

        #endregion TITLE

        #region URL

        $fieldUrlDetailNewsletterCampaign = new Field();
        $fieldUrlDetailNewsletterCampaign->code = 'Url';
        $fieldUrlDetailNewsletterCampaign->tab_id = $tabDetailNewsletterCampaign->id;
        $fieldUrlDetailNewsletterCampaign->field = 'url';
        $fieldUrlDetailNewsletterCampaign->data_type = 1;
        $fieldUrlDetailNewsletterCampaign->on_change = 'checkUrl';
        $fieldUrlDetailNewsletterCampaign->required = true;
        $fieldUrlDetailNewsletterCampaign->created_id = $idUser;
        $fieldUrlDetailNewsletterCampaign->save();

        $fieldLanguageUrlDetailNewsletterCampaign = new FieldLanguage();
        $fieldLanguageUrlDetailNewsletterCampaign->field_id = $fieldUrlDetailNewsletterCampaign->id;
        $fieldLanguageUrlDetailNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageUrlDetailNewsletterCampaign->description = 'Url';
        $fieldLanguageUrlDetailNewsletterCampaign->created_id = $idUser;
        $fieldLanguageUrlDetailNewsletterCampaign->save();

        $fieldUserRoleUrlDetailNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleUrlDetailNewsletterCampaign->field_id = $fieldUrlDetailNewsletterCampaign->id;
        $fieldUserRoleUrlDetailNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleUrlDetailNewsletterCampaign->enabled = true;
        $fieldUserRoleUrlDetailNewsletterCampaign->input_type = 0;
        $fieldUserRoleUrlDetailNewsletterCampaign->pos_y = 10;
        $fieldUserRoleUrlDetailNewsletterCampaign->pos_x = 30;
        $fieldUserRoleUrlDetailNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleUrlDetailNewsletterCampaign->save();

        #endregion URL

        #region DRAFT

        $fieldDraftDetailNewsletterCampaign = new Field();
        $fieldDraftDetailNewsletterCampaign->code = 'Draft';
        $fieldDraftDetailNewsletterCampaign->tab_id = $tabDetailNewsletterCampaign->id;
        $fieldDraftDetailNewsletterCampaign->field = 'draft';
        $fieldDraftDetailNewsletterCampaign->data_type = 3;
        $fieldDraftDetailNewsletterCampaign->created_id = $idUser;
        $fieldDraftDetailNewsletterCampaign->save();

        $fieldLanguageDraftDetailNewsletterCampaign = new FieldLanguage();
        $fieldLanguageDraftDetailNewsletterCampaign->field_id = $fieldDraftDetailNewsletterCampaign->id;
        $fieldLanguageDraftDetailNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageDraftDetailNewsletterCampaign->description = 'Bozza';
        $fieldLanguageDraftDetailNewsletterCampaign->created_id = $idUser;
        $fieldLanguageDraftDetailNewsletterCampaign->save();

        $fieldUserRoleDraftDetailNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleDraftDetailNewsletterCampaign->field_id = $fieldDraftDetailNewsletterCampaign->id;
        $fieldUserRoleDraftDetailNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleDraftDetailNewsletterCampaign->enabled = true;
        $fieldUserRoleDraftDetailNewsletterCampaign->input_type = 5;
        $fieldUserRoleDraftDetailNewsletterCampaign->pos_y = 10;
        $fieldUserRoleDraftDetailNewsletterCampaign->pos_x = 40;
        $fieldUserRoleDraftDetailNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleDraftDetailNewsletterCampaign->save();

        #endregion DRAFT

        #region SCHEDULE DATE TIME

        $fieldScheduleDateTimeDetailNewsletterCampaign = new Field();
        $fieldScheduleDateTimeDetailNewsletterCampaign->code = 'ScheduleDateTime';
        $fieldScheduleDateTimeDetailNewsletterCampaign->tab_id = $tabDetailNewsletterCampaign->id;
        $fieldScheduleDateTimeDetailNewsletterCampaign->field = 'scheduleDateTime';
        $fieldScheduleDateTimeDetailNewsletterCampaign->data_type = 2;
        $fieldScheduleDateTimeDetailNewsletterCampaign->created_id = $idUser;
        $fieldScheduleDateTimeDetailNewsletterCampaign->save();

        $fieldLanguageScheduleDateTimeDetailNewsletterCampaign = new FieldLanguage();
        $fieldLanguageScheduleDateTimeDetailNewsletterCampaign->field_id = $fieldScheduleDateTimeDetailNewsletterCampaign->id;
        $fieldLanguageScheduleDateTimeDetailNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageScheduleDateTimeDetailNewsletterCampaign->description = 'Data e ora Schedulazione';
        $fieldLanguageScheduleDateTimeDetailNewsletterCampaign->created_id = $idUser;
        $fieldLanguageScheduleDateTimeDetailNewsletterCampaign->save();

        $fieldUserRoleScheduleDateTimeDetailNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleScheduleDateTimeDetailNewsletterCampaign->field_id = $fieldScheduleDateTimeDetailNewsletterCampaign->id;
        $fieldUserRoleScheduleDateTimeDetailNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleScheduleDateTimeDetailNewsletterCampaign->enabled = true;
        $fieldUserRoleScheduleDateTimeDetailNewsletterCampaign->input_type = 4;
        $fieldUserRoleScheduleDateTimeDetailNewsletterCampaign->pos_y = 20;
        $fieldUserRoleScheduleDateTimeDetailNewsletterCampaign->pos_x = 10;
        $fieldUserRoleScheduleDateTimeDetailNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleScheduleDateTimeDetailNewsletterCampaign->save();

        #endregion SCHEDULE DATE TIME

        #region START SEND DATE TIME

        $fieldStartSendDateTimeDetailNewsletterCampaign = new Field();
        $fieldStartSendDateTimeDetailNewsletterCampaign->code = 'StartSendDateTime';
        $fieldStartSendDateTimeDetailNewsletterCampaign->tab_id = $tabDetailNewsletterCampaign->id;
        $fieldStartSendDateTimeDetailNewsletterCampaign->field = 'startSendDateTime';
        $fieldStartSendDateTimeDetailNewsletterCampaign->data_type = 2;
        $fieldStartSendDateTimeDetailNewsletterCampaign->created_id = $idUser;
        $fieldStartSendDateTimeDetailNewsletterCampaign->save();

        $fieldLanguageStartSendDateTimeDetailNewsletterCampaign = new FieldLanguage();
        $fieldLanguageStartSendDateTimeDetailNewsletterCampaign->field_id = $fieldStartSendDateTimeDetailNewsletterCampaign->id;
        $fieldLanguageStartSendDateTimeDetailNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageStartSendDateTimeDetailNewsletterCampaign->description = 'Data e ora Inizio spedizione';
        $fieldLanguageStartSendDateTimeDetailNewsletterCampaign->created_id = $idUser;
        $fieldLanguageStartSendDateTimeDetailNewsletterCampaign->save();

        $fieldUserRoleStartSendDateTimeDetailNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleStartSendDateTimeDetailNewsletterCampaign->field_id = $fieldStartSendDateTimeDetailNewsletterCampaign->id;
        $fieldUserRoleStartSendDateTimeDetailNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleStartSendDateTimeDetailNewsletterCampaign->enabled = true;
        $fieldUserRoleStartSendDateTimeDetailNewsletterCampaign->input_type = 15;
        $fieldUserRoleStartSendDateTimeDetailNewsletterCampaign->pos_y = 20;
        $fieldUserRoleStartSendDateTimeDetailNewsletterCampaign->pos_x = 20;
        $fieldUserRoleStartSendDateTimeDetailNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleStartSendDateTimeDetailNewsletterCampaign->save();

        #endregion START SEND DATE TIME

        #region END SEND DATE TIME

        $fieldEndSendDateTimeDetailNewsletterCampaign = new Field();
        $fieldEndSendDateTimeDetailNewsletterCampaign->code = 'EndSendDateTime';
        $fieldEndSendDateTimeDetailNewsletterCampaign->tab_id = $tabDetailNewsletterCampaign->id;
        $fieldEndSendDateTimeDetailNewsletterCampaign->field = 'endSendDateTime';
        $fieldEndSendDateTimeDetailNewsletterCampaign->data_type = 2;
        $fieldEndSendDateTimeDetailNewsletterCampaign->created_id = $idUser;
        $fieldEndSendDateTimeDetailNewsletterCampaign->save();

        $fieldLanguageEndSendDateTimeDetailNewsletterCampaign = new FieldLanguage();
        $fieldLanguageEndSendDateTimeDetailNewsletterCampaign->field_id = $fieldEndSendDateTimeDetailNewsletterCampaign->id;
        $fieldLanguageEndSendDateTimeDetailNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageEndSendDateTimeDetailNewsletterCampaign->description = 'Data e ora Fine spedizione';
        $fieldLanguageEndSendDateTimeDetailNewsletterCampaign->created_id = $idUser;
        $fieldLanguageEndSendDateTimeDetailNewsletterCampaign->save();

        $fieldUserRoleEndSendDateTimeDetailNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleEndSendDateTimeDetailNewsletterCampaign->field_id = $fieldEndSendDateTimeDetailNewsletterCampaign->id;
        $fieldUserRoleEndSendDateTimeDetailNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleEndSendDateTimeDetailNewsletterCampaign->enabled = true;
        $fieldUserRoleEndSendDateTimeDetailNewsletterCampaign->input_type = 15;
        $fieldUserRoleEndSendDateTimeDetailNewsletterCampaign->pos_y = 20;
        $fieldUserRoleEndSendDateTimeDetailNewsletterCampaign->pos_x = 30;
        $fieldUserRoleEndSendDateTimeDetailNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleEndSendDateTimeDetailNewsletterCampaign->save();

        #endregion END SEND DATE TIME

        #region HTML

        $fieldHtmlDetailNewsletterCampaign = new Field();
        $fieldHtmlDetailNewsletterCampaign->code = 'Html';
        $fieldHtmlDetailNewsletterCampaign->tab_id = $tabDetailNewsletterCampaign->id;
        $fieldHtmlDetailNewsletterCampaign->field = 'html';
        $fieldHtmlDetailNewsletterCampaign->data_type = 1;
        $fieldHtmlDetailNewsletterCampaign->required = true;
        $fieldHtmlDetailNewsletterCampaign->created_id = $idUser;
        $fieldHtmlDetailNewsletterCampaign->save();

        $fieldLanguageHtmlDetailNewsletterCampaign = new FieldLanguage();
        $fieldLanguageHtmlDetailNewsletterCampaign->field_id = $fieldHtmlDetailNewsletterCampaign->id;
        $fieldLanguageHtmlDetailNewsletterCampaign->language_id = $idLanguage;
        $fieldLanguageHtmlDetailNewsletterCampaign->description = 'Testo';
        $fieldLanguageHtmlDetailNewsletterCampaign->created_id = $idUser;
        $fieldLanguageHtmlDetailNewsletterCampaign->save();

        $fieldUserRoleHtmlDetailNewsletterCampaign = new FieldUserRole();
        $fieldUserRoleHtmlDetailNewsletterCampaign->field_id = $fieldHtmlDetailNewsletterCampaign->id;
        $fieldUserRoleHtmlDetailNewsletterCampaign->role_id = $idRole;
        $fieldUserRoleHtmlDetailNewsletterCampaign->enabled = true;
        $fieldUserRoleHtmlDetailNewsletterCampaign->input_type = 17;
        $fieldUserRoleHtmlDetailNewsletterCampaign->pos_y = 30;
        $fieldUserRoleHtmlDetailNewsletterCampaign->pos_x = 10;
        $fieldUserRoleHtmlDetailNewsletterCampaign->created_id = $idUser;
        $fieldUserRoleHtmlDetailNewsletterCampaign->save();

        #endregion EDITOR

        #endregion DETAIL

        #endregion NEWSLETTER
    }
}

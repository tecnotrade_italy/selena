<?php

use App\Dictionary;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $idMenuAdminCRM = MenuAdmin::where('code', 'Anagrafiche')->first()->id;

        #region Roles

        #region add DICTIONARY         
        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'Roles';
        $dictionary->description = 'Ruoli';
        $dictionary->created_id = $idUser;
        $dictionary->save();
        #endregion add DICTIONARY


        #region add newTitleRolesnewTitleRoles 

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'newTitleRoles';
        $dictionary->description = 'Inserisci Ruoli';
        $dictionary->created_id = $idUser;
        $dictionary->save();
        #endregion add newTitleRoles

        #region ICON
        $iconIdCard = new Icon();
        $iconIdCard->description = 'UserCircle';
        $iconIdCard->css = 'fa fa-user-circle';
        $iconIdCard->created_id = $idUser;
        $iconIdCard->save();
        #endregion ICON

        #region FUNCTIONALITY
        $functionalityContact = new Functionality();
        $functionalityContact->code = 'Roles';
        $functionalityContact->created_id = $idUser;
        $functionalityContact->save();
        #endregion FUNCTIONALITY

        #region MENU
        $menuAdminContact = new MenuAdmin();
        $menuAdminContact->code = 'Roles';
        $menuAdminContact->functionality_id = $functionalityContact->id;
        $menuAdminContact->icon_id = $iconIdCard->id;
        $menuAdminContact->url = '/admin/registries/Roles';
        $menuAdminContact->created_id = $idUser;
        $menuAdminContact->save();

        $languageMenuAdminContact = new LanguageMenuAdmin();
        $languageMenuAdminContact->language_id = $idLanguage;
        $languageMenuAdminContact->menu_admin_id = $menuAdminContact->id;
        $languageMenuAdminContact->description = 'Ruoli';
        $languageMenuAdminContact->created_id = $idUser;
        $languageMenuAdminContact->save();

        $menuAdminUserRoleContact = new MenuAdminUserRole();
        $menuAdminUserRoleContact->menu_admin_id = $menuAdminContact->id;
        $menuAdminUserRoleContact->menu_admin_father_id = $idMenuAdminCRM;
        $menuAdminUserRoleContact->role_id = $idRole;
        $menuAdminUserRoleContact->enabled = true;
        $menuAdminUserRoleContact->order = 30;
        $menuAdminUserRoleContact->created_id = $idUser;
        $menuAdminUserRoleContact->save();
        #endregion MENU

  		#region TABLE
        #region TAB
        $tabContactTable = new Tab();
        $tabContactTable->code = 'TableRoles';
        $tabContactTable->functionality_id = $functionalityContact->id;
        $tabContactTable->order = 10;
        $tabContactTable->created_id = $idUser;
        $tabContactTable->save();

        $languageTabContactTable = new LanguageTab();
        $languageTabContactTable->language_id = $idLanguage;
        $languageTabContactTable->tab_id = $tabContactTable->id;
        $languageTabContactTable->description = 'TableRoles';
        $languageTabContactTable->created_id = $idUser;
        $languageTabContactTable->save();
        #endregion TAB

        #region CHECKBOX
        $fieldContactCheckBox = new Field();
        $fieldContactCheckBox->tab_id = $tabContactTable->id;
        $fieldContactCheckBox->code = 'TableCheckBox';
        $fieldContactCheckBox->created_id = $idUser;
        $fieldContactCheckBox->save();

        $fieldLanguageContactCheckBox = new FieldLanguage();
        $fieldLanguageContactCheckBox->field_id = $fieldContactCheckBox->id;
        $fieldLanguageContactCheckBox->language_id = $idLanguage;
        $fieldLanguageContactCheckBox->description = 'CheckBox';
        $fieldLanguageContactCheckBox->created_id = $idUser;
        $fieldLanguageContactCheckBox->save();

        $fieldUserRoleContactCheckBox = new FieldUserRole();
        $fieldUserRoleContactCheckBox->field_id = $fieldContactCheckBox->id;
        $fieldUserRoleContactCheckBox->role_id = $idRole;
        $fieldUserRoleContactCheckBox->enabled = true;
        $fieldUserRoleContactCheckBox->table_order = 10;
        $fieldUserRoleContactCheckBox->input_type = 13;
        $fieldUserRoleContactCheckBox->created_id = $idUser;
        $fieldUserRoleContactCheckBox->save();
        #endregion CHECKBOX

        #region FORMATTER
        $fieldContactFormatter = new Field();
        $fieldContactFormatter->code = 'RolesFormatter';
        $fieldContactFormatter->formatter = 'RolesFormatter';
        $fieldContactFormatter->tab_id = $tabContactTable->id;
        $fieldContactFormatter->created_id = $idUser;
        $fieldContactFormatter->save();

        $fieldLanguageContactFormatter = new FieldLanguage();
        $fieldLanguageContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldLanguageContactFormatter->language_id = $idLanguage;
        $fieldLanguageContactFormatter->description = '';
        $fieldLanguageContactFormatter->created_id = $idUser;
        $fieldLanguageContactFormatter->save();

        $fieldUserRoleContactFormatter = new FieldUserRole();
        $fieldUserRoleContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldUserRoleContactFormatter->role_id = $idRole;
        $fieldUserRoleContactFormatter->enabled = true;
        $fieldUserRoleContactFormatter->table_order = 20;
        $fieldUserRoleContactFormatter->input_type = 14;
        $fieldUserRoleContactFormatter->created_id = $idUser;
        $fieldUserRoleContactFormatter->save();
        #endregion FORMATTER

 #region name

        $fieldContactTableCompany = new Field();
        $fieldContactTableCompany->code = 'name';
        $fieldContactTableCompany->tab_id = $tabContactTable->id;
        $fieldContactTableCompany->field = 'name';
        $fieldContactTableCompany->data_type = 1;
        $fieldContactTableCompany->created_id = $idUser;
        $fieldContactTableCompany->save();

        $fieldLanguageContactTableCompany = new FieldLanguage();
        $fieldLanguageContactTableCompany->field_id = $fieldContactTableCompany->id;
        $fieldLanguageContactTableCompany->language_id = $idLanguage;
        $fieldLanguageContactTableCompany->description = 'Nome';
        $fieldLanguageContactTableCompany->created_id = $idUser;
        $fieldLanguageContactTableCompany->save();

        $fieldUserRoleContactTableCompany = new FieldUserRole();
        $fieldUserRoleContactTableCompany->field_id = $fieldContactTableCompany->id;
        $fieldUserRoleContactTableCompany->role_id = $idRole;
        $fieldUserRoleContactTableCompany->enabled = true;
        $fieldUserRoleContactTableCompany->table_order = 30;
        $fieldUserRoleContactTableCompany->input_type = 0;
        $fieldUserRoleContactTableCompany->created_id = $idUser;
        $fieldUserRoleContactTableCompany->save();
        
        #endregion name

        #region display_name

        $fieldContactTableBusinessName = new Field();
        $fieldContactTableBusinessName->code = 'display_name';
        $fieldContactTableBusinessName->tab_id = $tabContactTable->id;
        $fieldContactTableBusinessName->field = 'display_name';
        $fieldContactTableBusinessName->data_type = 1;
        $fieldContactTableBusinessName->created_id = $idUser;
        $fieldContactTableBusinessName->save();

        $fieldLanguageContactTableBusinessName = new FieldLanguage();
        $fieldLanguageContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldLanguageContactTableBusinessName->language_id = $idLanguage;
        $fieldLanguageContactTableBusinessName->description = 'Display Nome';
        $fieldLanguageContactTableBusinessName->created_id = $idUser;
        $fieldLanguageContactTableBusinessName->save();

        $fieldUserRoleContactTableBusinessName = new FieldUserRole();
        $fieldUserRoleContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldUserRoleContactTableBusinessName->role_id = $idRole;
        $fieldUserRoleContactTableBusinessName->enabled = true;
        $fieldUserRoleContactTableBusinessName->table_order = 40;
        $fieldUserRoleContactTableBusinessName->input_type = 0;
        $fieldUserRoleContactTableBusinessName->created_id = $idUser;
        $fieldUserRoleContactTableBusinessName->save();
        #endregion display_name
        #endregion TABLE

        #region DETAIL
        #region TAB

        $tabContactDetail = new Tab();
        $tabContactDetail->code = 'DetailRoles';
        $tabContactDetail->functionality_id = $functionalityContact->id;
        $tabContactDetail->order = 20;
        $tabContactDetail->created_id = $idUser;
        $tabContactDetail->save();

        $languageTabContactDetail = new LanguageTab();
        $languageTabContactDetail->language_id = $idLanguage;
        $languageTabContactDetail->tab_id = $tabContactDetail->id;
        $languageTabContactDetail->description = 'DetailRoles';
        $languageTabContactDetail->created_id = $idUser;
        $languageTabContactDetail->save();

        #endregion TAB

        #region name
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'name';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'name';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Nome';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 10;
        $fieldUserRoleContactDetailCompany->pos_y = 10;
        $fieldUserRoleContactDetailCompany->input_type = 0;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();    
        #endregion name

        #region display_name

        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'display_name';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'display_name';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Display Nome';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 20;
        $fieldUserRoleSurnameContactDetail->pos_y = 10;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();

        #endregion display_name

        #region description

        $fieldHideHeader = new Field();
        $fieldHideHeader->code = 'description';
        $fieldHideHeader->tab_id = $tabContactDetail->id;
        $fieldHideHeader->field = 'description';
        $fieldHideHeader->data_type = 1;
        $fieldHideHeader->default_value = false;
        $fieldHideHeader->created_id = $idUser;
        $fieldHideHeader->save();

        $fieldLanguageHideHeader = new FieldLanguage();
        $fieldLanguageHideHeader->field_id = $fieldHideHeader->id;
        $fieldLanguageHideHeader->language_id = $idLanguage;
        $fieldLanguageHideHeader->description = 'Descrizione';
        $fieldLanguageHideHeader->created_id = $idUser;
        $fieldLanguageHideHeader->save();

        $fieldUserRoleHideHeader = new FieldUserRole();
        $fieldUserRoleHideHeader->field_id = $fieldHideHeader->id;
        $fieldUserRoleHideHeader->role_id = $idRole;
        $fieldUserRoleHideHeader->enabled = true;
        $fieldUserRoleHideHeader->input_type = 0;
        $fieldUserRoleHideHeader->pos_x = 30;
        $fieldUserRoleHideHeader->pos_y = 10;
        $fieldUserRoleHideHeader->created_id = $idUser;
        $fieldUserRoleHideHeader->save();

        #endregion description




    }
}

<?php

use App\Dictionary;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class DocumentStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
       

        #region FUNCTIONALITY
        $functionalityContact = new Functionality();
        $functionalityContact->code = 'DocumentStatuses';
        $functionalityContact->created_id = $idUser;
        $functionalityContact->save();
        #endregion FUNCTIONALITY

        #region ICON
        $iconIdCard = new Icon();
        $iconIdCard->description = 'Clipboard';
        $iconIdCard->css = 'fa fa-clipboard';
        $iconIdCard->created_id = $idUser;
        $iconIdCard->save(); 

        $iconAddressCard = new Icon();
        $iconAddressCard->description = 'Clipboard';
        $iconAddressCard->css = 'fa fa-clipboard';
        $iconAddressCard->created_id = $idUser;
        $iconAddressCard->save();

      
   
        
        #region TAB
        $tabContactTable = new Tab();
        $tabContactTable->code = 'TableDocumentsStatuses';
        $tabContactTable->functionality_id = $functionalityContact->id;
        $tabContactTable->order = 10;
        $tabContactTable->created_id = $idUser;
        $tabContactTable->save();

        $languageTabContactTable = new LanguageTab();
        $languageTabContactTable->language_id = $idLanguage;
        $languageTabContactTable->tab_id = $tabContactTable->id;
        $languageTabContactTable->description = 'TableDocumentsStatuses';
        $languageTabContactTable->created_id = $idUser;
        $languageTabContactTable->save();
        #END region TAB

          #region CHECKBOX
        $fieldContactCheckBox = new Field();
        $fieldContactCheckBox->tab_id = $tabContactTable->id;
        $fieldContactCheckBox->code = 'TableCheckBox';
        $fieldContactCheckBox->created_id = $idUser;
        $fieldContactCheckBox->save();

        $fieldLanguageContactCheckBox = new FieldLanguage();
        $fieldLanguageContactCheckBox->field_id = $fieldContactCheckBox->id;
        $fieldLanguageContactCheckBox->language_id = $idLanguage;
        $fieldLanguageContactCheckBox->description = 'CheckBox';
        $fieldLanguageContactCheckBox->created_id = $idUser;
        $fieldLanguageContactCheckBox->save();

        $fieldUserRoleContactCheckBox = new FieldUserRole();
        $fieldUserRoleContactCheckBox->field_id = $fieldContactCheckBox->id;
        $fieldUserRoleContactCheckBox->role_id = $idRole;
        $fieldUserRoleContactCheckBox->enabled = true;
        $fieldUserRoleContactCheckBox->table_order = 10;
        $fieldUserRoleContactCheckBox->input_type = 13;
        $fieldUserRoleContactCheckBox->created_id = $idUser;
        $fieldUserRoleContactCheckBox->save();
        #endregion CHECKBOX

        #region FORMATTER
        $fieldContactFormatter = new Field();
        $fieldContactFormatter->code = 'StatesOrderDocumentsFormatter';
        $fieldContactFormatter->formatter = 'StatesOrderDocumentsFormatter';
        $fieldContactFormatter->tab_id = $tabContactTable->id;
        $fieldContactFormatter->created_id = $idUser;
        $fieldContactFormatter->save();

        $fieldLanguageContactFormatter = new FieldLanguage();
        $fieldLanguageContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldLanguageContactFormatter->language_id = $idLanguage;
        $fieldLanguageContactFormatter->description = '';
        $fieldLanguageContactFormatter->created_id = $idUser;
        $fieldLanguageContactFormatter->save();

        $fieldUserRoleContactFormatter = new FieldUserRole();
        $fieldUserRoleContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldUserRoleContactFormatter->role_id = $idRole;
        $fieldUserRoleContactFormatter->enabled = true;
        $fieldUserRoleContactFormatter->table_order = 20;
        $fieldUserRoleContactFormatter->input_type = 14;
        $fieldUserRoleContactFormatter->created_id = $idUser;
        $fieldUserRoleContactFormatter->save();
        #endregion FORMATTER

        #region code
        $fieldContactTableBusinessName = new Field();
        $fieldContactTableBusinessName->code = 'code';
        $fieldContactTableBusinessName->tab_id = $tabContactTable->id;
        $fieldContactTableBusinessName->field = 'code';
        $fieldContactTableBusinessName->data_type = 1;
        $fieldContactTableBusinessName->created_id = $idUser;
        $fieldContactTableBusinessName->save();

        $fieldLanguageContactTableBusinessName = new FieldLanguage();
        $fieldLanguageContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldLanguageContactTableBusinessName->language_id = $idLanguage;
        $fieldLanguageContactTableBusinessName->description = 'Codice';
        $fieldLanguageContactTableBusinessName->created_id = $idUser;
        $fieldLanguageContactTableBusinessName->save();

        $fieldUserRoleContactTableBusinessName = new FieldUserRole();
        $fieldUserRoleContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldUserRoleContactTableBusinessName->role_id = $idRole;
        $fieldUserRoleContactTableBusinessName->enabled = true;
        $fieldUserRoleContactTableBusinessName->table_order = 30;
        $fieldUserRoleContactTableBusinessName->input_type = 0;
        $fieldUserRoleContactTableBusinessName->created_id = $idUser;
        $fieldUserRoleContactTableBusinessName->save();
        #endregion code

        #region description
        $fieldContactTableBusinessName = new Field();
        $fieldContactTableBusinessName->code = 'description';
        $fieldContactTableBusinessName->tab_id = $tabContactTable->id;
        $fieldContactTableBusinessName->field = 'description';
        $fieldContactTableBusinessName->data_type = 1;
        $fieldContactTableBusinessName->created_id = $idUser;
        $fieldContactTableBusinessName->save();

        $fieldLanguageContactTableBusinessName = new FieldLanguage();
        $fieldLanguageContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldLanguageContactTableBusinessName->language_id = $idLanguage;
        $fieldLanguageContactTableBusinessName->description = 'Descrizione';
        $fieldLanguageContactTableBusinessName->created_id = $idUser;
        $fieldLanguageContactTableBusinessName->save();

        $fieldUserRoleContactTableBusinessName = new FieldUserRole();
        $fieldUserRoleContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldUserRoleContactTableBusinessName->role_id = $idRole;
        $fieldUserRoleContactTableBusinessName->enabled = true;
        $fieldUserRoleContactTableBusinessName->table_order = 40;
        $fieldUserRoleContactTableBusinessName->input_type = 0;
        $fieldUserRoleContactTableBusinessName->created_id = $idUser;
        $fieldUserRoleContactTableBusinessName->save();
        #endregion description

        #region FORMATTER HTML VIEW
        $fieldContactFormatter = new Field();
        $fieldContactFormatter->code = 'HtmlViewFormatter';
        $fieldContactFormatter->formatter = 'HtmlViewFormatter';
        $fieldContactFormatter->tab_id = $tabContactTable->id;
        $fieldContactFormatter->created_id = $idUser;
        $fieldContactFormatter->save();

        $fieldLanguageContactFormatter = new FieldLanguage();
        $fieldLanguageContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldLanguageContactFormatter->language_id = $idLanguage;
        $fieldLanguageContactFormatter->description = '';
        $fieldLanguageContactFormatter->created_id = $idUser;
        $fieldLanguageContactFormatter->save();

        $fieldUserRoleContactFormatter = new FieldUserRole();
        $fieldUserRoleContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldUserRoleContactFormatter->role_id = $idRole;
        $fieldUserRoleContactFormatter->enabled = true;
        $fieldUserRoleContactFormatter->table_order = 50;
        $fieldUserRoleContactFormatter->input_type = 14;
        $fieldUserRoleContactFormatter->created_id = $idUser;
        $fieldUserRoleContactFormatter->save();
        #endregion HTML VIEW

        #region DETAIL
        #region TAB
        $tabContactDetail = new Tab();
        $tabContactDetail->code = 'DetailDocumentsStatuses';
        $tabContactDetail->functionality_id = $functionalityContact->id;
        $tabContactDetail->order = 20;
        $tabContactDetail->created_id = $idUser;
        $tabContactDetail->save();

        $languageTabContactDetail = new LanguageTab();
        $languageTabContactDetail->language_id = $idLanguage;
        $languageTabContactDetail->tab_id = $tabContactDetail->id;
        $languageTabContactDetail->description = 'DetailDocumentsStatuses';
        $languageTabContactDetail->created_id = $idUser;
        $languageTabContactDetail->save();
        #endregion TAB

        #region code
        $fieldContactTableBusinessName = new Field();
        $fieldContactTableBusinessName->code = 'code';
        $fieldContactTableBusinessName->tab_id = $tabContactDetail->id;
        $fieldContactTableBusinessName->field = 'code';
        $fieldContactTableBusinessName->data_type = 1;
        $fieldContactTableBusinessName->created_id = $idUser;
        $fieldContactTableBusinessName->save();

        $fieldLanguageContactTableBusinessName = new FieldLanguage();
        $fieldLanguageContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldLanguageContactTableBusinessName->language_id = $idLanguage;
        $fieldLanguageContactTableBusinessName->description = 'Codice';
        $fieldLanguageContactTableBusinessName->created_id = $idUser;
        $fieldLanguageContactTableBusinessName->save();

        $fieldUserRoleContactTableBusinessName = new FieldUserRole();
        $fieldUserRoleContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldUserRoleContactTableBusinessName->role_id = $idRole;
        $fieldUserRoleContactTableBusinessName->enabled = true;
        $fieldUserRoleContactTableBusinessName->pos_x = 10;
        $fieldUserRoleContactTableBusinessName->pos_y = 10;
        $fieldUserRoleContactTableBusinessName->input_type = 0;
        $fieldUserRoleContactTableBusinessName->created_id = $idUser;
        $fieldUserRoleContactTableBusinessName->save();
        #endregion code

  		  #region description
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'description';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'description';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Descrizione';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 10;
        $fieldUserRoleContactDetailCompany->pos_y = 20;
        $fieldUserRoleContactDetailCompany->input_type = 0;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion description

          #region CHARACTERISTIC

        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'html';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'html';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Html Visualizzato';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 10;
        $fieldUserRoleSurnameContactDetail->pos_y = 30;
        $fieldUserRoleSurnameContactDetail->input_type = 17;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        
        #endregion CHARACTERISTIC
    }
}

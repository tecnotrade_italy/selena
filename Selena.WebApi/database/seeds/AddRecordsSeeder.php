<?php

use App\Dictionary;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class AddRecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $idMenuAdminCRM = MenuAdmin::where('code', 'CRM')->first()->id;


        #region FUNCTIONALITY
        $functionalityContact = new Functionality();
        $functionalityContact->code = 'Items';
        $functionalityContact->created_id = $idUser;
        $functionalityContact->save();
        #endregion FUNCTIONALITY


        #region DETAIL
       
        $tabContactDetail = new Tab();
        $tabContactDetail->code = 'Detail';
        $tabContactDetail->functionality_id = $functionalityContact->id;
        $tabContactDetail->order = 20;
        $tabContactDetail->created_id = $idUser;
        $tabContactDetail->save();

        $languageTabContactDetail = new LanguageTab();
        $languageTabContactDetail->language_id = $idLanguage;
        $languageTabContactDetail->tab_id = $tabContactDetail->id;
        $languageTabContactDetail->description = 'Detail';
        $languageTabContactDetail->created_id = $idUser;
        $languageTabContactDetail->save();
     

        #region online
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'online';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'online';
        $fieldContactDetailCompany->data_type = 3;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Online';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 10;
        $fieldUserRoleContactDetailCompany->pos_y = 10;
        $fieldUserRoleContactDetailCompany->input_type = 5;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion online
        #endregion DETAIL
         
        #region DETAILATTRIBUTE
        $tabContactDetail = new Tab();
        $tabContactDetail->code = 'DetailAttribute';
        $tabContactDetail->functionality_id = $functionalityContact->id;
        $tabContactDetail->order = 20;
        $tabContactDetail->created_id = $idUser;
        $tabContactDetail->save();

        $languageTabContactDetail = new LanguageTab();
        $languageTabContactDetail->language_id = $idLanguage;
        $languageTabContactDetail->tab_id = $tabContactDetail->id;
        $languageTabContactDetail->description = 'DetailAttribute';
        $languageTabContactDetail->created_id = $idUser;
        $languageTabContactDetail->save();
     

        #region Producer
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'Producer';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'Producer';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Produttore';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 10;
        $fieldUserRoleContactDetailCompany->pos_y = 10;
        $fieldUserRoleContactDetailCompany->input_type = 2;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion Producer
        #endregion DETAILATTRIBUTE


         #region DETAILCommerciale
        $tabContactDetail = new Tab();
        $tabContactDetail->code = 'DetailCommercial';
        $tabContactDetail->functionality_id = $functionalityContact->id;
        $tabContactDetail->order = 20;
        $tabContactDetail->created_id = $idUser;
        $tabContactDetail->save();

        $languageTabContactDetail = new LanguageTab();
        $languageTabContactDetail->language_id = $idLanguage;
        $languageTabContactDetail->tab_id = $tabContactDetail->id;
        $languageTabContactDetail->description = 'DetailCommercial';
        $languageTabContactDetail->created_id = $idUser;
        $languageTabContactDetail->save();
     

        #region QTA_MINIMA
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'qta_min';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'qta_min';
        $fieldContactDetailCompany->data_type = 0;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Quantità Minima';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 10;
        $fieldUserRoleContactDetailCompany->pos_y = 10;
        $fieldUserRoleContactDetailCompany->input_type = 7;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion QTA_MINIMA



        #region shipment_request
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'shipment_request';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'shipment_request';
        $fieldContactDetailCompany->data_type = 3;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Richiesta di Spedizione';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 10;
        $fieldUserRoleContactDetailCompany->pos_y = 20;
        $fieldUserRoleContactDetailCompany->input_type = 5;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion shipment_request

        #region availability
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'availability';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'availability';
        $fieldContactDetailCompany->data_type = 0;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Disponibilità';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 10;
        $fieldUserRoleContactDetailCompany->pos_y = 30;
        $fieldUserRoleContactDetailCompany->input_type = 7;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion availability


        #region committed_customer
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'committed_customer';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'committed_customer';
        $fieldContactDetailCompany->data_type = 0;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Impegnato da Cliente';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 10;
        $fieldUserRoleContactDetailCompany->pos_y = 40;
        $fieldUserRoleContactDetailCompany->input_type = 7;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion committed_customer

        #region ordered_supplier
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'ordered_supplier';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'ordered_supplier';
        $fieldContactDetailCompany->data_type = 0;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Ordinato a Fornitore';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 10;
        $fieldUserRoleContactDetailCompany->pos_y = 50;
        $fieldUserRoleContactDetailCompany->input_type = 7;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion ordered_supplier

        #region STOCK
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'stock';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'stock';
        $fieldContactDetailCompany->data_type = 0;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Giacenza';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 10;
        $fieldUserRoleContactDetailCompany->pos_y = 50;
        $fieldUserRoleContactDetailCompany->input_type = 7;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion STOCK
        #endregion DETAILCommerciale
      
    }
}

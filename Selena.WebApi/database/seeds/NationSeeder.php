<?php
use App\Dictionary;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class NationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $functionalityNations = Functionality::where('code', 'Nations')->first();

        //------------ Tab Tabella/Dettaglio ---------------------------------
        #region TAB
        $tabContentNations = new Tab();
        $tabContentNations->code = 'Content';
        $tabContentNations->functionality_id = $functionalityNations->id;
        $tabContentNations->order = 10;
        $tabContentNations->created_id = $idUser;
        $tabContentNations->save();

        $languageTabContentNations = new LanguageTab();
        $languageTabContentNations->language_id = $idLanguage;
        $languageTabContentNations->tab_id = $tabContentNations->id;
        $languageTabContentNations->description = 'Contenuto';
        $languageTabContentNations->created_id = $idUser;
        $languageTabContentNations->save();
        #endregion TAB

        #region CHECKBOX

        $fieldContentNationCheckBox = new Field();
        $fieldContentNationCheckBox->tab_id = $tabContentNations->id;
        $fieldContentNationCheckBox->code = 'TableCheckBox';
        $fieldContentNationCheckBox->created_id = $idUser;
        $fieldContentNationCheckBox->save();

        $fieldLanguageContentNationCheckBox = new FieldLanguage();
        $fieldLanguageContentNationCheckBox->field_id = $fieldContentNationCheckBox->id;
        $fieldLanguageContentNationCheckBox->language_id = $idLanguage;
        $fieldLanguageContentNationCheckBox->description = 'CheckBox';
        $fieldLanguageContentNationCheckBox->created_id = $idUser;
        $fieldLanguageContentNationCheckBox->save();

        $fieldUserRoleContentNationCheckBox = new FieldUserRole();
        $fieldUserRoleContentNationCheckBox->field_id = $fieldContentNationCheckBox->id;
        $fieldUserRoleContentNationCheckBox->role_id = $idRole;
        $fieldUserRoleContentNationCheckBox->enabled = true;
        $fieldUserRoleContentNationCheckBox->table_order = 10;
        $fieldUserRoleContentNationCheckBox->input_type = 13;
        $fieldUserRoleContentNationCheckBox->created_id = $idUser;
        $fieldUserRoleContentNationCheckBox->save();

        #endregion CHECKBOX

        #region FORMATTER

        $fieldNationFormatter = new Field();
        $fieldNationFormatter->code = 'NationFormatter';
        $fieldNationFormatter->formatter = 'nationFormatter';
        $fieldNationFormatter->tab_id = $tabContentNations->id;
        $fieldNationFormatter->created_id = $idUser;
        $fieldNationFormatter->save();

        $fieldLanguageNationFormatter = new FieldLanguage();
        $fieldLanguageNationFormatter->field_id = $fieldNationFormatter->id;
        $fieldLanguageNationFormatter->language_id = $idLanguage;
        $fieldLanguageNationFormatter->description = '';
        $fieldLanguageNationFormatter->created_id = $idUser;
        $fieldLanguageNationFormatter->save();

        $fieldUserRoleNationFormatter = new FieldUserRole();
        $fieldUserRoleNationFormatter->field_id = $fieldNationFormatter->id;
        $fieldUserRoleNationFormatter->role_id = $idRole;
        $fieldUserRoleNationFormatter->enabled = true;
        $fieldUserRoleNationFormatter->table_order = 20;
        $fieldUserRoleNationFormatter->input_type = 14;
        $fieldUserRoleNationFormatter->created_id = $idUser;
        $fieldUserRoleNationFormatter->save();

        #endregion FORMATTER

        #region CEE

        $fieldNationsCee = new Field();
        $fieldNationsCee->code = 'Cee';
        $fieldNationsCee->tab_id = $tabContentNations->id;
        $fieldNationsCee->field = 'cee';
        $fieldNationsCee->data_type = 3;
        $fieldNationsCee->created_id = $idUser;
        $fieldNationsCee->save();

        $fieldLanguageNationsCee = new FieldLanguage();
        $fieldLanguageNationsCee->field_id = $fieldNationsCee->id;
        $fieldLanguageNationsCee->language_id = $idLanguage;
        $fieldLanguageNationsCee->description = 'Cee';
        $fieldLanguageNationsCee->created_id = $idUser;
        $fieldLanguageNationsCee->save();

        $fieldUserRoleNationsCee = new FieldUserRole();
        $fieldUserRoleNationsCee->field_id = $fieldNationsCee->id;
        $fieldUserRoleNationsCee->role_id = $idRole;
        $fieldUserRoleNationsCee->enabled = true;
        $fieldUserRoleNationsCee->input_type = 5;
        $fieldUserRoleNationsCee->pos_x = 10;
        $fieldUserRoleNationsCee->pos_y = 10;
        $fieldUserRoleNationsCee->table_order = 10;
        $fieldUserRoleNationsCee->created_id = $idUser;
        $fieldUserRoleNationsCee->save();

        #endregion CEE

        #region ABBREVIATION

        $fieldNationsAbbrevation = new Field();
        $fieldNationsAbbrevation->code = 'Abbreviation';
        $fieldNationsAbbrevation->tab_id = $tabContentNations->id;
        $fieldNationsAbbrevation->field = 'abbreviation';
        $fieldNationsAbbrevation->data_type = 1;
        $fieldNationsAbbrevation->created_id = $idUser;
        $fieldNationsAbbrevation->save();

        $fieldLanguageNationsAbbrevation = new FieldLanguage();
        $fieldLanguageNationsAbbrevation->field_id = $fieldNationsAbbrevation->id;
        $fieldLanguageNationsAbbrevation->language_id = $idLanguage;
        $fieldLanguageNationsAbbrevation->description = 'Abbreviazione';
        $fieldLanguageNationsAbbrevation->created_id = $idUser;
        $fieldLanguageNationsAbbrevation->save();

        $fieldUserRoleNationsAbbrevations = new FieldUserRole();
        $fieldUserRoleNationsAbbrevations->field_id = $fieldLanguageNationsAbbrevation->id;
        $fieldUserRoleNationsAbbrevations->role_id = $idRole;
        $fieldUserRoleNationsAbbrevations->enabled = true;
        $fieldUserRoleNationsAbbrevations->input_type = 0;
        $fieldUserRoleNationsAbbrevations->pos_x = 20;
        $fieldUserRoleNationsAbbrevations->pos_y = 10;
        $fieldUserRoleNationsAbbrevations->table_order = 20;
        $fieldUserRoleNationsAbbrevations->created_id = $idUser;
        $fieldUserRoleNationsAbbrevations->save();

        #endregion ABBREVATION

        #region DESCRIPTION

        $fieldNationsDescription = new Field();
        $fieldNationsDescription->code = 'Description';
        $fieldNationsDescription->tab_id = $tabContentNations->id;
        $fieldNationsDescription->field = 'description';
        $fieldNationsDescription->data_type = 1;
        $fieldNationsDescription->created_id = $idUser;
        $fieldNationsDescription->save();

        $fieldLanguageNationsDescription = new FieldLanguage();
        $fieldLanguageNationsDescription->field_id = $fieldNationsDescription->id;
        $fieldLanguageNationsDescription->language_id = $idLanguage;
        $fieldLanguageNationsDescription->description = 'Descrizione';
        $fieldLanguageNationsDescription->created_id = $idUser;
        $fieldLanguageNationsDescription->save();

        $fieldUserRoleNationsDescription = new FieldUserRole();
        $fieldUserRoleNationsDescription->field_id = $fieldNationsDescription->id;
        $fieldUserRoleNationsDescription->role_id = $idRole;
        $fieldUserRoleNationsDescription->enabled = true;
        $fieldUserRoleNationsDescription->input_type = 0;
        $fieldUserRoleNationsDescription->pos_x = 30;
        $fieldUserRoleNationsDescription->pos_y = 10;
        $fieldUserRoleNationsDescription->table_order = 20;
        $fieldUserRoleNationsDescription->created_id = $idUser;
        $fieldUserRoleNationsDescription->save();

        #endregion DESCRIPTION

        
    }
}

<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\MenuAdmin;
use App\LanguageMenuAdmin;
use App\Language;
use App\Icon;
use App\Functionality;
use App\User;
use App\Helpers\LogHelper;
use App\CommunityActionType;
use App\CommunityActionTypeLanguage;

class CommunityActionsTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        $actionType = CommunityActionType::updateOrCreate(['code'   => 'STAR1'],
        [ 
            'rating_value' => -5,
            'created_id' => $idUser
        ]);

        $actionTypeLanguage = CommunityActionTypeLanguage::firstOrCreate(
        [
            'action_type_id'   => $actionType->id, 
            'language_id' => $idLanguage
        ],
        [
            'description' => 'Voto immagine - 1 stella',
            'created_id' => $idUser
        ]);

        $actionType = CommunityActionType::updateOrCreate(['code'   => 'STAR2'],
        [ 
            'rating_value' => -2,
            'created_id' => $idUser
        ]);

        $actionTypeLanguage = CommunityActionTypeLanguage::firstOrCreate(
        [
            'action_type_id'   => $actionType->id, 
            'language_id' => $idLanguage
        ],
        [
            'description' => 'Voto immagine - 2 stelle',
            'created_id' => $idUser
        ]);

        $actionType = CommunityActionType::updateOrCreate(['code'   => 'STAR3'],
        [ 
            'rating_value' => 1,
            'created_id' => $idUser
        ]);

        $actionTypeLanguage = CommunityActionTypeLanguage::firstOrCreate(
        [
            'action_type_id'   => $actionType->id, 
            'language_id' => $idLanguage
        ],
        [
            'description' => 'Voto immagine - 3 stelle',
            'created_id' => $idUser
        ]);

        $actionType = CommunityActionType::updateOrCreate(['code'   => 'STAR4'],
        [ 
            'rating_value' => 4,
            'created_id' => $idUser
        ]);

        $actionTypeLanguage = CommunityActionTypeLanguage::firstOrCreate(
        [
            'action_type_id'   => $actionType->id, 
            'language_id' => $idLanguage
        ],
        [
            'description' => 'Voto immagine - 4 stelle',
            'created_id' => $idUser
        ]);

        $actionType = CommunityActionType::updateOrCreate(['code'   => 'STAR5'],
        [ 
            'rating_value' => 7,
            'created_id' => $idUser
        ]);

        $actionTypeLanguage = CommunityActionTypeLanguage::firstOrCreate(
        [
            'action_type_id'   => $actionType->id, 
            'language_id' => $idLanguage
        ],
        [
            'description' => 'Voto immagine - 5 stelle',
            'created_id' => $idUser
        ]);

        $actionType = CommunityActionType::updateOrCreate(['code'   => 'REPORTIMAGE'],
        [ 
            'rating_value' => -20,
            'created_id' => $idUser
        ]);

        $actionTypeLanguage = CommunityActionTypeLanguage::firstOrCreate(
        [
            'action_type_id'   => $actionType->id, 
            'language_id' => $idLanguage
        ],
        [
            'description' => 'Segnalazione immagine',
            'created_id' => $idUser
        ]);
        
        $actionType = CommunityActionType::updateOrCreate(['code'   => 'ADDIMAGETOFAVORITE'],
        [ 
            'rating_value' => 20,
            'created_id' => $idUser
        ]);

        $actionTypeLanguage = CommunityActionTypeLanguage::firstOrCreate(
        [
            'action_type_id'   => $actionType->id, 
            'language_id' => $idLanguage
        ],
        [
            'description' => 'Aggiunta immagine alle preferite',
            'created_id' => $idUser
        ]);

        $actionType = CommunityActionType::updateOrCreate(['code'   => 'REMOVEIMAGEFROMFAVORITE'],
        [ 
            'rating_value' => -20,
            'created_id' => $idUser
        ]);

        $actionTypeLanguage = CommunityActionTypeLanguage::firstOrCreate(
        [
            'action_type_id'   => $actionType->id, 
            'language_id' => $idLanguage
        ],
        [
            'description' => 'Rimozione immagine dalle preferite',
            'created_id' => $idUser
        ]);

        $actionType = CommunityActionType::updateOrCreate(['code'   => 'ADDCOMMENT'],
        [ 
            'rating_value' => 3,
            'created_id' => $idUser
        ]);

        $actionTypeLanguage = CommunityActionTypeLanguage::firstOrCreate(
        [
            'action_type_id'   => $actionType->id, 
            'language_id' => $idLanguage
        ],
        [
            'description' => 'Aggiunta di un commento',
            'created_id' => $idUser
        ]);

        $actionType = CommunityActionType::updateOrCreate(['code'   => 'REPORTCOMMENT'],
        [ 
            'rating_value' => -10,
            'created_id' => $idUser
        ]);

        $actionTypeLanguage = CommunityActionTypeLanguage::firstOrCreate(
        [
            'action_type_id'   => $actionType->id, 
            'language_id' => $idLanguage
        ],
        [
            'description' => 'Segnalazione commento immagine',
            'created_id' => $idUser
        ]);

        $actionType = CommunityActionType::updateOrCreate(['code'   => 'ADDTOEDITORPICK'],
        [ 
            'rating_value' => 30,
            'created_id' => $idUser
        ]);

        $actionTypeLanguage = CommunityActionTypeLanguage::firstOrCreate(
        [
            'action_type_id'   => $actionType->id, 
            'language_id' => $idLanguage
        ],
        [
            'description' => 'Immagine selezionata da noi',
            'created_id' => $idUser
        ]);


    }
}

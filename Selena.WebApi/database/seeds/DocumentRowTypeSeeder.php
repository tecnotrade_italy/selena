<?php

use App\DocumentRowType;
use App\DocumentRowTypeLanguage;
use App\Enums\DocumentRowTypeEnum;
use App\Language;
use App\User;
use Illuminate\Database\Seeder;

class DocumentRowTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        $documentRowTypeItem = new DocumentRowType();
        $documentRowTypeItem->code = DocumentRowTypeEnum::Item;
        $documentRowTypeItem->created_id = $idUser;
        $documentRowTypeItem->save();

        $documentRowTypeLanguageItem = new DocumentRowTypeLanguage();
        $documentRowTypeLanguageItem->document_row_type_id = $documentRowTypeItem->id;
        $documentRowTypeLanguageItem->language_id = $idLanguage;
        $documentRowTypeLanguageItem->description = 'Articolo';
        $documentRowTypeLanguageItem->created_id = $idUser;
        $documentRowTypeLanguageItem->save();
    }
}

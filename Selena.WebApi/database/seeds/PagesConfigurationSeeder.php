<?php

use App\Enums\PageTypesEnum;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Icon;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Tab;
use Illuminate\Database\Seeder;
use App\Language;
use App\Role;
use App\User;
use App\Functionality;

class PagesConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        #region PAGES

        $functionality = new Functionality();
        $functionality->code = 'Pages';
        $functionality->created_id = $idUser;
        $functionality->save();

        $icon = new Icon();
        $icon->description = 'List';
        $icon->css = 'fa fa-list-alt';
        $icon->created_id = $idUser;
        $icon->save();

        $menuAdmin = new MenuAdmin();
        $menuAdmin->code = 'Pages';
        $menuAdmin->functionality_id = $functionality->id;
        $menuAdmin->icon_id = $icon->id;
        $menuAdmin->url = '/admin/pages';
        $menuAdmin->created_id = $idUser;
        $menuAdmin->save();

        $languageMenuAdmin = new LanguageMenuAdmin();
        $languageMenuAdmin->language_id = $idLanguage;
        $languageMenuAdmin->menu_admin_id = $menuAdmin->id;
        $languageMenuAdmin->description = 'Pagine';
        $languageMenuAdmin->created_id = $idUser;
        $languageMenuAdmin->save();

        $menuAdminUserRole = new MenuAdminUserRole();
        $menuAdminUserRole->menu_admin_id = $menuAdmin->id;
        $menuAdminUserRole->role_id = $idRole;
        $menuAdminUserRole->enabled = true;
        $menuAdminUserRole->order = 10;
        $menuAdminUserRole->created_id = $idUser;
        $menuAdminUserRole->save();

        #region tab
        $tabContent = new Tab();
        $tabContent->code = 'Content';
        $tabContent->functionality_id = $functionality->id;
        $tabContent->order = 10;
        $tabContent->created_id = $idUser;
        $tabContent->save();

        $languageTabContent = new LanguageTab();
        $languageTabContent->language_id = $idLanguage;
        $languageTabContent->tab_id = $tabContent->id;
        $languageTabContent->description = 'Contenuto';
        $languageTabContent->created_id = $idUser;
        $languageTabContent->save();

        $tabSettings = new Tab();
        $tabSettings->code = 'Settings';
        $tabSettings->functionality_id = $functionality->id;
        $tabSettings->order = 20;
        $tabSettings->created_id = $idUser;
        $tabSettings->save();

        $languageTabSettings = new LanguageTab();
        $languageTabSettings->language_id = $idLanguage;
        $languageTabSettings->tab_id = $tabSettings->id;
        $languageTabSettings->description = 'Impostazioni';
        $languageTabSettings->created_id = $idUser;
        $languageTabSettings->save();

        #region Content

        $fieldContent = new Field();
        $fieldContent->code = 'Content';
        $fieldContent->tab_id = $tabContent->id;
        $fieldContent->field = 'languagePageSetting.content';
        $fieldContent->data_type = 1;
        $fieldContent->required = true;
        $fieldContent->created_id = $idUser;
        $fieldContent->save();

        $fieldLanguageContent = new FieldLanguage();
        $fieldLanguageContent->field_id = $fieldContent->id;
        $fieldLanguageContent->language_id = $idLanguage;
        $fieldLanguageContent->description = 'Contenuto';
        $fieldLanguageContent->created_id = $idUser;
        $fieldLanguageContent->save();

        $fieldUserRoleContent = new FieldUserRole();
        $fieldUserRoleContent->field_id = $fieldContent->id;
        $fieldUserRoleContent->role_id = $idRole;
        $fieldUserRoleContent->enabled = true;
        $fieldUserRoleContent->input_type = 1;
        $fieldUserRoleContent->pos_x = 10;
        $fieldUserRoleContent->pos_y = 10;
        $fieldUserRoleContent->colspan = 12;
        $fieldUserRoleContent->created_id = $idUser;
        $fieldUserRoleContent->save();

        #endregion Content

        #region Title
        $fieldTitlePage = new Field();
        $fieldTitlePage->code = 'TitlePage';
        $fieldTitlePage->tab_id = $tabSettings->id;
        $fieldTitlePage->field = 'languagePageSetting.title';
        $fieldTitlePage->data_type = 1;
        $fieldTitlePage->required = true;
        $fieldTitlePage->max_length = 255;
        $fieldTitlePage->on_change = 'changeTitle';
        $fieldTitlePage->created_id = $idUser;
        $fieldTitlePage->save();

        $fieldLanguageTitlePage = new FieldLanguage();
        $fieldLanguageTitlePage->field_id = $fieldTitlePage->id;
        $fieldLanguageTitlePage->language_id = $idLanguage;
        $fieldLanguageTitlePage->description = 'Titolo';
        $fieldLanguageTitlePage->created_id = $idUser;
        $fieldLanguageTitlePage->save();

        $fieldUserRoleTitlePage = new FieldUserRole();
        $fieldUserRoleTitlePage->field_id = $fieldTitlePage->id;
        $fieldUserRoleTitlePage->role_id = $idRole;
        $fieldUserRoleTitlePage->enabled = true;
        $fieldUserRoleTitlePage->input_type = 0;
        $fieldUserRoleTitlePage->pos_x = 10;
        $fieldUserRoleTitlePage->pos_y = 10;
        $fieldUserRoleTitlePage->required = true;
        $fieldUserRoleTitlePage->colspan = 12;
        $fieldUserRoleTitlePage->created_id = $idUser;
        $fieldUserRoleTitlePage->save();

        #endregion Title

        #region Author

        $fieldAuthor = new Field();
        $fieldAuthor->code = 'Author';
        $fieldAuthor->tab_id = $tabSettings->id;
        $fieldAuthor->field = 'idAuthor';
        $fieldAuthor->data_type = 0;
        $fieldAuthor->service = 'users';
        $fieldAuthor->created_id = $idUser;
        $fieldAuthor->save();

        $fieldLanguageAuthor = new FieldLanguage();
        $fieldLanguageAuthor->field_id = $fieldAuthor->id;
        $fieldLanguageAuthor->language_id = $idLanguage;
        $fieldLanguageAuthor->description = 'Autore';
        $fieldLanguageAuthor->created_id = $idUser;
        $fieldLanguageAuthor->save();

        $fieldUserRoleAuthor = new FieldUserRole();
        $fieldUserRoleAuthor->field_id = $fieldAuthor->id;
        $fieldUserRoleAuthor->role_id = $idRole;
        $fieldUserRoleAuthor->enabled = true;
        $fieldUserRoleAuthor->input_type = 2;
        $fieldUserRoleAuthor->pos_x = 10;
        $fieldUserRoleAuthor->pos_y = 20;
        $fieldUserRoleAuthor->colspan = 12;
        $fieldUserRoleAuthor->created_id = $idUser;
        $fieldUserRoleAuthor->save();

        #endregion Author

        #region Url
        $fieldUrl = new Field();
        $fieldUrl->code = 'Url';
        $fieldUrl->tab_id = $tabSettings->id;
        $fieldUrl->field = 'languagePageSetting.url';
        $fieldUrl->data_type = 1;
        $fieldUrl->max_length = 255;
        $fieldUrl->on_change = 'checkUrl';
        $fieldUrl->created_id = $idUser;
        $fieldUrl->save();

        $fieldLanguageUrl = new FieldLanguage();
        $fieldLanguageUrl->field_id = $fieldUrl->id;
        $fieldLanguageUrl->language_id = $idLanguage;
        $fieldLanguageUrl->description = 'Url';
        $fieldLanguageUrl->created_id = $idUser;
        $fieldLanguageUrl->save();

        $fieldUserRoleUrl = new FieldUserRole();
        $fieldUserRoleUrl->field_id = $fieldUrl->id;
        $fieldUserRoleUrl->role_id = $idRole;
        $fieldUserRoleUrl->enabled = true;
        $fieldUserRoleUrl->input_type = 0;
        $fieldUserRoleUrl->pos_x = 10;
        $fieldUserRoleUrl->pos_y = 30;
        $fieldUserRoleUrl->colspan = 12;
        $fieldUserRoleUrl->created_id = $idUser;
        $fieldUserRoleUrl->save();

        #endregion Url

        #region VisibleFrom
        $fieldVisibleFrom = new Field();
        $fieldVisibleFrom->code = 'VisibleFrom';
        $fieldVisibleFrom->tab_id = $tabSettings->id;
        $fieldVisibleFrom->field = 'visibleFrom';
        $fieldVisibleFrom->data_type = 2;
        $fieldVisibleFrom->created_id = $idUser;
        $fieldVisibleFrom->save();

        $fieldLanguageVisibleFrom = new FieldLanguage();
        $fieldLanguageVisibleFrom->field_id = $fieldVisibleFrom->id;
        $fieldLanguageVisibleFrom->language_id = $idLanguage;
        $fieldLanguageVisibleFrom->description = 'Visibile dal';
        $fieldLanguageVisibleFrom->created_id = $idUser;
        $fieldLanguageVisibleFrom->save();

        $fieldUserRoleVisibleFrom = new FieldUserRole();
        $fieldUserRoleVisibleFrom->field_id = $fieldVisibleFrom->id;
        $fieldUserRoleVisibleFrom->role_id = $idRole;
        $fieldUserRoleVisibleFrom->enabled = true;
        $fieldUserRoleVisibleFrom->input_type = 4;
        $fieldUserRoleVisibleFrom->pos_x = 10;
        $fieldUserRoleVisibleFrom->pos_y = 40;
        $fieldUserRoleVisibleFrom->colspan = 12;
        $fieldUserRoleVisibleFrom->created_id = $idUser;
        $fieldUserRoleVisibleFrom->save();

        #endregion VisibleFrom

        #region VisibleEnd

        $fieldVisibleEnd = new Field();
        $fieldVisibleEnd->code = 'VisibleEnd';
        $fieldVisibleEnd->tab_id = $tabSettings->id;
        $fieldVisibleEnd->field = 'visibleEnd';
        $fieldVisibleEnd->data_type = 2;
        $fieldVisibleEnd->created_id = $idUser;
        $fieldVisibleEnd->save();

        $fieldLanguageVisibleEnd = new FieldLanguage();
        $fieldLanguageVisibleEnd->field_id = $fieldVisibleEnd->id;
        $fieldLanguageVisibleEnd->language_id = $idLanguage;
        $fieldLanguageVisibleEnd->description = 'Fino al';
        $fieldLanguageVisibleEnd->created_id = $idUser;
        $fieldLanguageVisibleEnd->save();

        $fieldUserRoleVisibleEnd = new FieldUserRole();
        $fieldUserRoleVisibleEnd->field_id = $fieldVisibleEnd->id;
        $fieldUserRoleVisibleEnd->role_id = $idRole;
        $fieldUserRoleVisibleEnd->enabled = true;
        $fieldUserRoleVisibleEnd->input_type = 3;
        $fieldUserRoleVisibleEnd->pos_x = 10;
        $fieldUserRoleVisibleEnd->pos_y = 50;
        $fieldUserRoleVisibleEnd->colspan = 12;
        $fieldUserRoleVisibleEnd->created_id = $idUser;
        $fieldUserRoleVisibleEnd->save();

        #endregion VisibleEnd

        #region Icon

        $fieldIcon = new Field();
        $fieldIcon->code = 'Icon';
        $fieldIcon->tab_id = $tabSettings->id;
        $fieldIcon->field = 'idIcon';
        $fieldIcon->data_type = 0;
        $fieldIcon->service = 'icons';
        $fieldIcon->created_id = $idUser;
        $fieldIcon->save();

        $fieldLanguageIcon = new FieldLanguage();
        $fieldLanguageIcon->field_id = $fieldIcon->id;
        $fieldLanguageIcon->language_id = $idLanguage;
        $fieldLanguageIcon->description = 'Icona';
        $fieldLanguageIcon->created_id = $idUser;
        $fieldLanguageIcon->save();

        $fieldUserRoleIcon = new FieldUserRole();
        $fieldUserRoleIcon->field_id = $fieldIcon->id;
        $fieldUserRoleIcon->role_id = $idRole;
        $fieldUserRoleIcon->enabled = true;
        $fieldUserRoleIcon->input_type = 2;
        $fieldUserRoleIcon->pos_x = 10;
        $fieldUserRoleIcon->pos_y = 60;
        $fieldUserRoleIcon->colspan = 12;
        $fieldUserRoleIcon->created_id = $idUser;
        $fieldUserRoleIcon->save();

        #endregion Icon

        #region HomePage
        $fieldHomePage = new Field();
        $fieldHomePage->code = 'HomePage';
        $fieldHomePage->tab_id = $tabSettings->id;
        $fieldHomePage->field = 'homePage';
        $fieldHomePage->data_type = 3;
        $fieldHomePage->default_value = false;
        $fieldHomePage->created_id = $idUser;
        $fieldHomePage->save();

        $fieldLanguageHomePage = new FieldLanguage();
        $fieldLanguageHomePage->field_id = $fieldHomePage->id;
        $fieldLanguageHomePage->language_id = $idLanguage;
        $fieldLanguageHomePage->description = 'Imposta come home page';
        $fieldLanguageHomePage->created_id = $idUser;
        $fieldLanguageHomePage->save();

        $fieldUserRoleHomePage = new FieldUserRole();
        $fieldUserRoleHomePage->field_id = $fieldHomePage->id;
        $fieldUserRoleHomePage->role_id = $idRole;
        $fieldUserRoleHomePage->enabled = true;
        $fieldUserRoleHomePage->input_type = 5;
        $fieldUserRoleHomePage->pos_x = 10;
        $fieldUserRoleHomePage->pos_y = 70;
        $fieldUserRoleHomePage->colspan = 12;
        $fieldUserRoleHomePage->created_id = $idUser;
        $fieldUserRoleHomePage->save();
        #endregion HomePage

        #region HideHeader
        $fieldHideHeader = new Field();
        $fieldHideHeader->code = 'HideHeader';
        $fieldHideHeader->tab_id = $tabSettings->id;
        $fieldHideHeader->field = 'languagePageSetting.hideHeader';
        $fieldHideHeader->data_type = 3;
        $fieldHideHeader->default_value = false;
        $fieldHideHeader->created_id = $idUser;
        $fieldHideHeader->save();

        $fieldLanguageHideHeader = new FieldLanguage();
        $fieldLanguageHideHeader->field_id = $fieldHideHeader->id;
        $fieldLanguageHideHeader->language_id = $idLanguage;
        $fieldLanguageHideHeader->description = 'Nascondi header';
        $fieldLanguageHideHeader->created_id = $idUser;
        $fieldLanguageHideHeader->save();

        $fieldUserRoleHideHeader = new FieldUserRole();
        $fieldUserRoleHideHeader->field_id = $fieldHideHeader->id;
        $fieldUserRoleHideHeader->role_id = $idRole;
        $fieldUserRoleHideHeader->enabled = true;
        $fieldUserRoleHideHeader->input_type = 5;
        $fieldUserRoleHideHeader->pos_x = 10;
        $fieldUserRoleHideHeader->pos_y = 80;
        $fieldUserRoleHideHeader->colspan = 12;
        $fieldUserRoleHideHeader->created_id = $idUser;
        $fieldUserRoleHideHeader->save();
        #endregion HideHeader

        #region HideFooter
        $fieldHideFooter = new Field();
        $fieldHideFooter->code = 'HideFooter';
        $fieldHideFooter->tab_id = $tabSettings->id;
        $fieldHideFooter->field = 'languagePageSetting.hideFooter';
        $fieldHideFooter->data_type = 3;
        $fieldHideFooter->default_value = false;
        $fieldHideFooter->created_id = $idUser;
        $fieldHideFooter->save();

        $fieldLanguageHideFooter = new FieldLanguage();
        $fieldLanguageHideFooter->field_id = $fieldHideFooter->id;
        $fieldLanguageHideFooter->language_id = $idLanguage;
        $fieldLanguageHideFooter->description = 'Nascondi footer';
        $fieldLanguageHideFooter->created_id = $idUser;
        $fieldLanguageHideFooter->save();

        $fieldUserRoleHideFooter = new FieldUserRole();
        $fieldUserRoleHideFooter->field_id = $fieldHideFooter->id;
        $fieldUserRoleHideFooter->role_id = $idRole;
        $fieldUserRoleHideFooter->enabled = true;
        $fieldUserRoleHideFooter->input_type = 5;
        $fieldUserRoleHideFooter->pos_x = 10;
        $fieldUserRoleHideFooter->pos_y = 90;
        $fieldUserRoleHideFooter->colspan = 12;
        $fieldUserRoleHideFooter->created_id = $idUser;
        $fieldUserRoleHideFooter->save();
        #endregion HideFooter

        #region HideBreadcrumb
        $fieldHideBreadcrumbs = new Field();
        $fieldHideBreadcrumbs->code = 'HideBreadcrumb';
        $fieldHideBreadcrumbs->tab_id = $tabSettings->id;
        $fieldHideBreadcrumbs->field = 'languagePageSetting.hideBreadcrumb';
        $fieldHideBreadcrumbs->data_type = 3;
        $fieldHideBreadcrumbs->default_value = false;
        $fieldHideBreadcrumbs->created_id = $idUser;
        $fieldHideBreadcrumbs->save();

        $fieldLanguageHideBreadcrumbs = new FieldLanguage();
        $fieldLanguageHideBreadcrumbs->field_id = $fieldHideBreadcrumbs->id;
        $fieldLanguageHideBreadcrumbs->language_id = $idLanguage;
        $fieldLanguageHideBreadcrumbs->description = 'Nascondi breadcrumbs';
        $fieldLanguageHideBreadcrumbs->created_id = $idUser;
        $fieldLanguageHideBreadcrumbs->save();

        $fieldUserRoleHideBreadcrumbs = new FieldUserRole();
        $fieldUserRoleHideBreadcrumbs->field_id = $fieldHideBreadcrumbs->id;
        $fieldUserRoleHideBreadcrumbs->role_id = $idRole;
        $fieldUserRoleHideBreadcrumbs->enabled = true;
        $fieldUserRoleHideBreadcrumbs->input_type = 5;
        $fieldUserRoleHideBreadcrumbs->pos_x = 10;
        $fieldUserRoleHideBreadcrumbs->pos_y = 100;
        $fieldUserRoleHideBreadcrumbs->colspan = 12;
        $fieldUserRoleHideBreadcrumbs->created_id = $idUser;
        $fieldUserRoleHideBreadcrumbs->save();
        #endregion HideBreadcrumb

        #region HideMenu
        $fieldHideMenu = new Field();
        $fieldHideMenu->code = 'HideMenu';
        $fieldHideMenu->tab_id = $tabSettings->id;
        $fieldHideMenu->field = 'hideMenu';
        $fieldHideMenu->data_type = 3;
        $fieldHideMenu->default_value = false;
        $fieldHideMenu->created_id = $idUser;
        $fieldHideMenu->save();

        $fieldLanguageHideMenu = new FieldLanguage();
        $fieldLanguageHideMenu->field_id = $fieldHideMenu->id;
        $fieldLanguageHideMenu->language_id = $idLanguage;
        $fieldLanguageHideMenu->description = 'Nascondi menu';
        $fieldLanguageHideMenu->created_id = $idUser;
        $fieldLanguageHideMenu->save();

        $fieldUserRoleHideMenu = new FieldUserRole();
        $fieldUserRoleHideMenu->field_id = $fieldHideMenu->id;
        $fieldUserRoleHideMenu->role_id = $idRole;
        $fieldUserRoleHideMenu->enabled = true;
        $fieldUserRoleHideMenu->input_type = 5;
        $fieldUserRoleHideMenu->pos_x = 10;
        $fieldUserRoleHideMenu->pos_y = 110;
        $fieldUserRoleHideMenu->colspan = 12;
        $fieldUserRoleHideMenu->created_id = $idUser;
        $fieldUserRoleHideMenu->save();
        #endregion HideMenu

        #region HideMobileMenu
        $fieldHideMobileMenu = new Field();
        $fieldHideMobileMenu->code = 'HideMobileMenu';
        $fieldHideMobileMenu->tab_id = $tabSettings->id;
        $fieldHideMobileMenu->field = 'hideMenuMobile';
        $fieldHideMobileMenu->data_type = 3;
        $fieldHideMobileMenu->default_value = false;
        $fieldHideMobileMenu->created_id = $idUser;
        $fieldHideMobileMenu->save();

        $fieldLanguageHideMobileMenu = new FieldLanguage();
        $fieldLanguageHideMobileMenu->field_id = $fieldHideMobileMenu->id;
        $fieldLanguageHideMobileMenu->language_id = $idLanguage;
        $fieldLanguageHideMobileMenu->description = 'Nascondi menu mobile';
        $fieldLanguageHideMobileMenu->created_id = $idUser;
        $fieldLanguageHideMobileMenu->save();

        $fieldUserRoleHideMobileMenu = new FieldUserRole();
        $fieldUserRoleHideMobileMenu->field_id = $fieldHideMobileMenu->id;
        $fieldUserRoleHideMobileMenu->role_id = $idRole;
        $fieldUserRoleHideMobileMenu->enabled = true;
        $fieldUserRoleHideMobileMenu->input_type = 5;
        $fieldUserRoleHideMobileMenu->pos_x = 10;
        $fieldUserRoleHideMobileMenu->pos_y = 120;
        $fieldUserRoleHideMobileMenu->colspan = 12;
        $fieldUserRoleHideMobileMenu->created_id = $idUser;
        $fieldUserRoleHideMobileMenu->save();
        #endregion HideMobileMenu

        #region SeoTitle

        $fieldSeoTitle = new Field();
        $fieldSeoTitle->code = 'SeoTitle';
        $fieldSeoTitle->tab_id = $tabSettings->id;
        $fieldSeoTitle->field = 'languagePageSetting.seoTitle';
        $fieldSeoTitle->data_type = 1;
        $fieldSeoTitle->max_length = 60;
        $fieldSeoTitle->created_id = $idUser;
        $fieldSeoTitle->save();

        $fieldLanguageSeoTitle = new FieldLanguage();
        $fieldLanguageSeoTitle->field_id = $fieldSeoTitle->id;
        $fieldLanguageSeoTitle->language_id = $idLanguage;
        $fieldLanguageSeoTitle->description = 'Titolo SEO';
        $fieldLanguageSeoTitle->created_id = $idUser;
        $fieldLanguageSeoTitle->save();

        $fieldUserRoleSeoTitle = new FieldUserRole();
        $fieldUserRoleSeoTitle->field_id = $fieldSeoTitle->id;
        $fieldUserRoleSeoTitle->role_id = $idRole;
        $fieldUserRoleSeoTitle->enabled = true;
        $fieldUserRoleSeoTitle->input_type = 0;
        $fieldUserRoleSeoTitle->pos_x = 10;
        $fieldUserRoleSeoTitle->pos_y = 130;
        $fieldUserRoleSeoTitle->colspan = 12;
        $fieldUserRoleSeoTitle->created_id = $idUser;
        $fieldUserRoleSeoTitle->save();

        #endregion SeoTitle

        #region SeoDescription
        $fieldSeoDescription = new Field();
        $fieldSeoDescription->code = 'SeoDescription';
        $fieldSeoDescription->tab_id = $tabSettings->id;
        $fieldSeoDescription->field = 'languagePageSetting.seoDescription';
        $fieldSeoDescription->data_type = 1;
        $fieldSeoDescription->max_length = 230;
        $fieldSeoDescription->created_id = $idUser;
        $fieldSeoDescription->save();

        $fieldLanguageSeoDescription = new FieldLanguage();
        $fieldLanguageSeoDescription->field_id = $fieldSeoDescription->id;
        $fieldLanguageSeoDescription->language_id = $idLanguage;
        $fieldLanguageSeoDescription->description = 'Descrizione SEO';
        $fieldLanguageSeoDescription->created_id = $idUser;
        $fieldLanguageSeoDescription->save();

        $fieldUserRoleSeoDescription = new FieldUserRole();
        $fieldUserRoleSeoDescription->field_id = $fieldSeoDescription->id;
        $fieldUserRoleSeoDescription->role_id = $idRole;
        $fieldUserRoleSeoDescription->enabled = true;
        $fieldUserRoleSeoDescription->input_type = 0;
        $fieldUserRoleSeoDescription->pos_x = 10;
        $fieldUserRoleSeoDescription->pos_y = 140;
        $fieldUserRoleSeoDescription->colspan = 12;
        $fieldUserRoleSeoDescription->created_id = $idUser;
        $fieldUserRoleSeoDescription->save();

        #endregion SeoDescription

        #region SeoKeyword

        $fieldSeoKeyword = new Field();
        $fieldSeoKeyword->code = 'SeoKeyword';
        $fieldSeoKeyword->tab_id = $tabSettings->id;
        $fieldSeoKeyword->field = 'languagePageSetting.seoKeyword';
        $fieldSeoKeyword->data_type = 1;
        $fieldSeoKeyword->created_id = $idUser;
        $fieldSeoKeyword->save();

        $fieldLanguageSeoKeyword = new FieldLanguage();
        $fieldLanguageSeoKeyword->field_id = $fieldSeoKeyword->id;
        $fieldLanguageSeoKeyword->language_id = $idLanguage;
        $fieldLanguageSeoKeyword->description = 'Parole chiave SEO';
        $fieldLanguageSeoKeyword->created_id = $idUser;
        $fieldLanguageSeoKeyword->save();

        $fieldUserRoleSeoKeyword = new FieldUserRole();
        $fieldUserRoleSeoKeyword->field_id = $fieldSeoKeyword->id;
        $fieldUserRoleSeoKeyword->role_id = $idRole;
        $fieldUserRoleSeoKeyword->enabled = true;
        $fieldUserRoleSeoKeyword->input_type = 1;
        $fieldUserRoleSeoKeyword->pos_x = 10;
        $fieldUserRoleSeoKeyword->pos_y = 150;
        $fieldUserRoleSeoKeyword->colspan = 12;
        $fieldUserRoleSeoKeyword->created_id = $idUser;
        $fieldUserRoleSeoKeyword->save();

        #endregion SeoKeyword

        #region HideSearchEngines
        $fieldHideFromSearchEngines = new Field();
        $fieldHideFromSearchEngines->code = 'HideFromSearchEngines';
        $fieldHideFromSearchEngines->tab_id = $tabSettings->id;
        $fieldHideFromSearchEngines->field = 'hideSearchEngine';
        $fieldHideFromSearchEngines->data_type = 3;
        $fieldHideFromSearchEngines->default_value = false;
        $fieldHideFromSearchEngines->created_id = $idUser;
        $fieldHideFromSearchEngines->save();

        $fieldLanguageHideFromSearchEngines = new FieldLanguage();
        $fieldLanguageHideFromSearchEngines->field_id = $fieldHideFromSearchEngines->id;
        $fieldLanguageHideFromSearchEngines->language_id = $idLanguage;
        $fieldLanguageHideFromSearchEngines->description = 'Nascondi dai motori di ricerca';
        $fieldLanguageHideFromSearchEngines->created_id = $idUser;
        $fieldLanguageHideFromSearchEngines->save();

        $fieldUserRoleHideFromSearchEngines = new FieldUserRole();
        $fieldUserRoleHideFromSearchEngines->field_id = $fieldHideFromSearchEngines->id;
        $fieldUserRoleHideFromSearchEngines->role_id = $idRole;
        $fieldUserRoleHideFromSearchEngines->enabled = true;
        $fieldUserRoleHideFromSearchEngines->input_type = 5;
        $fieldUserRoleHideFromSearchEngines->pos_x = 10;
        $fieldUserRoleHideFromSearchEngines->pos_y = 160;
        $fieldUserRoleHideFromSearchEngines->colspan = 12;
        $fieldUserRoleHideFromSearchEngines->created_id = $idUser;
        $fieldUserRoleHideFromSearchEngines->save();
        #endregion HideSearchEngines

        #region ShareTitle
        $fieldShareTitle = new Field();
        $fieldShareTitle->code = 'ShareTitle';
        $fieldShareTitle->tab_id = $tabSettings->id;
        $fieldShareTitle->field = 'languagePageSetting.shareTitle';
        $fieldShareTitle->data_type = 1;
        $fieldShareTitle->max_length = 60;
        $fieldShareTitle->created_id = $idUser;
        $fieldShareTitle->save();

        $fieldLanguageShareTitle = new FieldLanguage();
        $fieldLanguageShareTitle->field_id = $fieldShareTitle->id;
        $fieldLanguageShareTitle->language_id = $idLanguage;
        $fieldLanguageShareTitle->description = 'Titolo condivisione';
        $fieldLanguageShareTitle->created_id = $idUser;
        $fieldLanguageShareTitle->save();

        $fieldUserRoleShareTitle = new FieldUserRole();
        $fieldUserRoleShareTitle->field_id = $fieldShareTitle->id;
        $fieldUserRoleShareTitle->role_id = $idRole;
        $fieldUserRoleShareTitle->enabled = true;
        $fieldUserRoleShareTitle->input_type = 0;
        $fieldUserRoleShareTitle->pos_x = 10;
        $fieldUserRoleShareTitle->pos_y = 170;
        $fieldUserRoleShareTitle->colspan = 12;
        $fieldUserRoleShareTitle->created_id = $idUser;
        $fieldUserRoleShareTitle->save();
        #endregion ShareTitle

        #region ShareDescription
        $fieldShareDescription = new Field();
        $fieldShareDescription->code = 'ShareDescription';
        $fieldShareDescription->tab_id = $tabSettings->id;
        $fieldShareDescription->field = 'languagePageSetting.shareDescription';
        $fieldShareDescription->data_type = 1;
        $fieldShareDescription->max_length = 230;
        $fieldShareDescription->created_id = $idUser;
        $fieldShareDescription->save();

        $fieldLanguageShareDescription = new FieldLanguage();
        $fieldLanguageShareDescription->field_id = $fieldShareDescription->id;
        $fieldLanguageShareDescription->language_id = $idLanguage;
        $fieldLanguageShareDescription->description = 'Descrizione condivisione';
        $fieldLanguageShareDescription->created_id = $idUser;
        $fieldLanguageShareDescription->save();

        $fieldUserRoleShareDescription = new FieldUserRole();
        $fieldUserRoleShareDescription->field_id = $fieldShareDescription->id;
        $fieldUserRoleShareDescription->role_id = $idRole;
        $fieldUserRoleShareDescription->enabled = true;
        $fieldUserRoleShareDescription->input_type = 0;
        $fieldUserRoleShareDescription->pos_x = 10;
        $fieldUserRoleShareDescription->pos_y = 180;
        $fieldUserRoleShareDescription->colspan = 12;
        $fieldUserRoleShareDescription->created_id = $idUser;
        $fieldUserRoleShareDescription->save();
        #endregion ShareDescription

        #region PageShareType
        $fieldPageShareType = new Field();
        $fieldPageShareType->code = 'PageShareType';
        $fieldPageShareType->tab_id = $tabSettings->id;
        $fieldPageShareType->field = 'idPageShareType';
        $fieldPageShareType->data_type = 0;
        $fieldPageShareType->service = 'pageShareType';
        $fieldPageShareType->created_id = $idUser;
        $fieldPageShareType->save();

        $fieldLanguagePageShareType = new FieldLanguage();
        $fieldLanguagePageShareType->field_id = $fieldPageShareType->id;
        $fieldLanguagePageShareType->language_id = $idLanguage;
        $fieldLanguagePageShareType->description = 'Tipo condivisione';
        $fieldLanguagePageShareType->created_id = $idUser;
        $fieldLanguagePageShareType->save();

        $fieldUserRolePageShareType = new FieldUserRole();
        $fieldUserRolePageShareType->field_id = $fieldPageShareType->id;
        $fieldUserRolePageShareType->role_id = $idRole;
        $fieldUserRolePageShareType->enabled = true;
        $fieldUserRolePageShareType->input_type = 2;
        $fieldUserRolePageShareType->pos_x = 10;
        $fieldUserRolePageShareType->pos_y = 190;
        $fieldUserRolePageShareType->colspan = 12;
        $fieldUserRolePageShareType->created_id = $idUser;
        $fieldUserRolePageShareType->save();
        #endregion PageShareType

        #region PageCategory

        $fieldPageCategory = new Field();
        $fieldPageCategory->code = 'PageCategory';
        $fieldPageCategory->tab_id = $tabSettings->id;
        $fieldPageCategory->field = 'idPageCategory';
        $fieldPageCategory->data_type = 0;
        $fieldPageCategory->service = 'pageCategory';
        $fieldPageCategory->created_id = $idUser;
        $fieldPageCategory->save();

        $fieldLanguagePageCategory = new FieldLanguage();
        $fieldLanguagePageCategory->field_id = $fieldPageCategory->id;
        $fieldLanguagePageCategory->language_id = $idLanguage;
        $fieldLanguagePageCategory->description = 'Categoria';
        $fieldLanguagePageCategory->created_id = $idUser;
        $fieldLanguagePageCategory->save();

        $fieldUserRolePageCategory = new FieldUserRole();
        $fieldUserRolePageCategory->field_id = $fieldPageCategory->id;
        $fieldUserRolePageCategory->role_id = $idRole;
        $fieldUserRolePageCategory->enabled = true;
        $fieldUserRolePageCategory->input_type = 2;
        $fieldUserRolePageCategory->pos_x = 10;
        $fieldUserRolePageCategory->pos_y = 200;
        $fieldUserRolePageCategory->colspan = 12;
        $fieldUserRolePageCategory->created_id = $idUser;
        $fieldUserRolePageCategory->save();

        #endregion PageCategory

        #region FixedPost

        $fieldFixedPost = new Field();
        $fieldFixedPost->code = 'Fixed';
        $fieldFixedPost->tab_id = $tabSettings->id;
        $fieldFixedPost->field = 'fixed';
        $fieldFixedPost->data_type = 3;
        $fieldFixedPost->default_value = false;
        $fieldFixedPost->created_id = $idUser;
        $fieldFixedPost->save();

        $fieldLanguageFixedPost = new FieldLanguage();
        $fieldLanguageFixedPost->field_id = $fieldFixedPost->id;
        $fieldLanguageFixedPost->language_id = $idLanguage;
        $fieldLanguageFixedPost->description = 'Fissa il post nella parte superiore della pagina';
        $fieldLanguageFixedPost->created_id = $idUser;
        $fieldLanguageFixedPost->save();

        $fieldUserRoleFixedPost = new FieldUserRole();
        $fieldUserRoleFixedPost->field_id = $fieldFixedPost->id;
        $fieldUserRoleFixedPost->role_id = $idRole;
        $fieldUserRoleFixedPost->enabled = true;
        $fieldUserRoleFixedPost->input_type = 5;
        $fieldUserRoleFixedPost->pos_x = 10;
        $fieldUserRoleFixedPost->pos_y = 210;
        $fieldUserRoleFixedPost->colspan = 12;
        $fieldUserRoleFixedPost->created_id = $idUser;
        $fieldUserRoleFixedPost->save();

        #endregion FixedPost

        #region FixedPostEndDate

        $fieldFixedPostEndDate = new Field();
        $fieldFixedPostEndDate->code = 'FixedEnd';
        $fieldFixedPostEndDate->tab_id = $tabSettings->id;
        $fieldFixedPostEndDate->field = 'fixedEnd';
        $fieldFixedPostEndDate->data_type = 0;
        $fieldFixedPostEndDate->service = 'users';
        $fieldFixedPostEndDate->created_id = $idUser;
        $fieldFixedPostEndDate->save();

        $fieldLanguageFixedPostEndDate = new FieldLanguage();
        $fieldLanguageFixedPostEndDate->field_id = $fieldFixedPostEndDate->id;
        $fieldLanguageFixedPostEndDate->language_id = $idLanguage;
        $fieldLanguageFixedPostEndDate->description = 'Data di fine post in alto';
        $fieldLanguageFixedPostEndDate->created_id = $idUser;
        $fieldLanguageFixedPostEndDate->save();

        $fieldUserRoleFixedPostEndDate = new FieldUserRole();
        $fieldUserRoleFixedPostEndDate->field_id = $fieldFixedPostEndDate->id;
        $fieldUserRoleFixedPostEndDate->role_id = $idRole;
        $fieldUserRoleFixedPostEndDate->enabled = true;
        $fieldUserRoleFixedPostEndDate->input_type = 3;
        $fieldUserRoleFixedPostEndDate->pos_x = 10;
        $fieldUserRoleFixedPostEndDate->pos_y = 220;
        $fieldUserRoleFixedPostEndDate->colspan = 12;
        $fieldUserRoleFixedPostEndDate->created_id = $idUser;
        $fieldUserRoleFixedPostEndDate->save();

        #endregion FixedPostEndDate

        #region UrlCoverImage

        $fieldCoverImage = new Field();
        $fieldCoverImage->code = 'CoverImage';
        $fieldCoverImage->tab_id = $tabSettings->id;
        $fieldCoverImage->field = 'languagePageSetting.urlCoverImage';
        $fieldCoverImage->data_type = 1;
        $fieldCoverImage->max_length = 255;
        $fieldCoverImage->created_id = $idUser;
        $fieldCoverImage->save();

        $fieldLanguageCoverImage = new FieldLanguage();
        $fieldLanguageCoverImage->field_id = $fieldCoverImage->id;
        $fieldLanguageCoverImage->language_id = $idLanguage;
        $fieldLanguageCoverImage->description = 'Url immagine condivisione';
        $fieldLanguageCoverImage->created_id = $idUser;
        $fieldLanguageCoverImage->save();

        $fieldUserRoleCoverImage = new FieldUserRole();
        $fieldUserRoleCoverImage->field_id = $fieldCoverImage->id;
        $fieldUserRoleCoverImage->role_id = $idRole;
        $fieldUserRoleCoverImage->enabled = true;
        $fieldUserRoleCoverImage->input_type = 0;
        $fieldUserRoleCoverImage->pos_x = 10;
        $fieldUserRoleCoverImage->pos_y = 230;
        $fieldUserRoleCoverImage->colspan = 12;
        $fieldUserRoleCoverImage->created_id = $idUser;
        $fieldUserRoleCoverImage->save();

        #endregion UrlCoverImage

        #region PublishDate

        $field = new Field();
        $field->code = 'Publish';
        $field->tab_id = $tabSettings->id;
        $field->field = 'publish';
        $field->data_type = 2;
        $field->created_id = $idUser;
        $field->save();

        $fieldLanguage = new FieldLanguage();
        $fieldLanguage->field_id = $field->id;
        $fieldLanguage->language_id = $idLanguage;
        $fieldLanguage->description = 'Data di pubblicazione';
        $fieldLanguage->created_id = $idUser;
        $fieldLanguage->save();

        $fieldUserRole = new FieldUserRole();
        $fieldUserRole->field_id = $field->id;
        $fieldUserRole->role_id = $idRole;
        $fieldUserRole->enabled = true;
        $fieldUserRole->input_type = 3;
        $fieldUserRole->pos_x = 10;
        $fieldUserRole->pos_y = 240;
        $fieldUserRole->colspan = 12;
        $fieldUserRole->created_id = $idUser;
        $fieldUserRole->save();

        #endregion PublishDate

        #region IdLanguage
        $fieldIdLanguage = new Field();
        $fieldIdLanguage->code = 'IdLanguage';
        $fieldIdLanguage->tab_id = $tabSettings->id;
        $fieldIdLanguage->field = 'languagePageSetting.idLanguage';
        $fieldIdLanguage->data_type = 0;
        $fieldIdLanguage->created_id = $idUser;
        $fieldIdLanguage->save();

        $fieldLanguageIdLanguage = new FieldLanguage();
        $fieldLanguageIdLanguage->field_id = $fieldIdLanguage->id;
        $fieldLanguageIdLanguage->language_id = $idLanguage;
        $fieldLanguageIdLanguage->description = 'Lingua';
        $fieldLanguageIdLanguage->created_id = $idUser;
        $fieldLanguageIdLanguage->save();

        $fieldUserRoleIdLanguage = new FieldUserRole();
        $fieldUserRoleIdLanguage->field_id = $fieldIdLanguage->id;
        $fieldUserRoleIdLanguage->role_id = $idRole;
        $fieldUserRoleIdLanguage->enabled = true;
        $fieldUserRoleIdLanguage->input_type = 19;
        $fieldUserRoleIdLanguage->pos_x = 10;
        $fieldUserRoleIdLanguage->pos_y = 10;
        $fieldUserRoleIdLanguage->created_id = $idUser;
        $fieldUserRoleIdLanguage->save();

        #endregion IdLanguage

        #region field hidden
        $fieldPageType = new Field();
        $fieldPageType->code = 'PageType';
        $fieldPageType->tab_id = $tabSettings->id;
        $fieldPageType->field = 'pageType';
        $fieldPageType->default_value = PageTypesEnum::Page;
        $fieldPageType->data_type = 0;
        $fieldPageType->created_id = $idUser;
        $fieldPageType->save();

        $fieldLanguagePageType = new FieldLanguage();
        $fieldLanguagePageType->field_id = $fieldPageType->id;
        $fieldLanguagePageType->language_id = $idLanguage;
        $fieldLanguagePageType->description = 'Tipo pagina';
        $fieldLanguagePageType->created_id = $idUser;
        $fieldLanguagePageType->save();

        $fieldUserRolePageType = new FieldUserRole();
        $fieldUserRolePageType->field_id = $fieldPageType->id;
        $fieldUserRolePageType->role_id = $idRole;
        $fieldUserRolePageType->enabled = true;
        $fieldUserRolePageType->input_type = 12;
        $fieldUserRolePageType->pos_x = 10;
        $fieldUserRolePageType->pos_y = 10;
        $fieldUserRolePageType->created_id = $idUser;
        $fieldUserRolePageType->save();
        #endregion field hidden

        #region Enable JS
        $fieldEnableJS = new Field();
        $fieldEnableJS->code = 'EnableJS';
        $fieldEnableJS->tab_id = $tabSettings->id;
        $fieldEnableJS->field = 'enableJS';
        $fieldEnableJS->data_type = 3;
        $fieldEnableJS->default_value = false;
        $fieldEnableJS->created_id = $idUser;
        $fieldEnableJS->save();

        $fieldLanguageEnableJS = new FieldLanguage();
        $fieldLanguageEnableJS->field_id = $fieldEnableJS->id;
        $fieldLanguageEnableJS->language_id = $idLanguage;
        $fieldLanguageEnableJS->description = 'Abilita Javascript';
        $fieldLanguageEnableJS->created_id = $idUser;
        $fieldLanguageEnableJS->save();

        $fieldUserRoleEnableJS = new FieldUserRole();
        $fieldUserRoleEnableJS->field_id = $fieldEnableJS->id;
        $fieldUserRoleEnableJS->role_id = $idRole;
        $fieldUserRoleEnableJS->enabled = true;
        $fieldUserRoleEnableJS->input_type = 5;
        $fieldUserRoleEnableJS->pos_x = 10;
        $fieldUserRoleEnableJS->pos_y = 1000;
        $fieldUserRoleEnableJS->created_id = $idUser;
        $fieldUserRoleEnableJS->save();
        #endregion Enable JS

        #region PROTECTED
        $fieldProtected = new Field();
        $fieldProtected->code = 'Protected';
        $fieldProtected->tab_id = $tabSettings->id;
        $fieldProtected->field = 'protected';
        $fieldProtected->data_type = 3;
        $fieldProtected->default_value = false;
        $fieldProtected->created_id = $idUser;
        $fieldProtected->save();

        $fieldLanguageProtected = new FieldLanguage();
        $fieldLanguageProtected->field_id = $fieldProtected->id;
        $fieldLanguageProtected->language_id = $idLanguage;
        $fieldLanguageProtected->description = 'Richiedi accesso';
        $fieldLanguageProtected->created_id = $idUser;
        $fieldLanguageProtected->save();

        $fieldUserRoleProtected = new FieldUserRole();
        $fieldUserRoleProtected->field_id = $fieldProtected->id;
        $fieldUserRoleProtected->role_id = $idRole;
        $fieldUserRoleProtected->enabled = true;
        $fieldUserRoleProtected->input_type = 5;
        $fieldUserRoleProtected->pos_x = 10;
        $fieldUserRoleProtected->pos_y = 1010;
        $fieldUserRoleProtected->created_id = $idUser;
        $fieldUserRoleProtected->save();
        #endregion PROTECTED

        #region JS

        $fieldJS = new Field();
        $fieldJS->code = 'JS';
        $fieldJS->tab_id = $tabContent->id;
        $fieldJS->field = 'js';
        $fieldJS->data_type = 1;
        $fieldJS->created_id = $idUser;
        $fieldJS->save();

        $fieldLanguageJS = new FieldLanguage();
        $fieldLanguageJS->field_id = $fieldJS->id;
        $fieldLanguageJS->language_id = $idLanguage;
        $fieldLanguageJS->description = 'Javascript';
        $fieldLanguageJS->created_id = $idUser;
        $fieldLanguageJS->save();

        $fieldUserRoleJS = new FieldUserRole();
        $fieldUserRoleJS->field_id = $fieldJS->id;
        $fieldUserRoleJS->role_id = $idRole;
        $fieldUserRoleJS->enabled = true;
        $fieldUserRoleJS->input_type = 20;
        $fieldUserRoleJS->pos_x = 10;
        $fieldUserRoleJS->pos_y = 20;
        $fieldUserRoleJS->created_id = $idUser;
        $fieldUserRoleJS->save();
        #endregion JS



        #endregion PAGES
    }
}

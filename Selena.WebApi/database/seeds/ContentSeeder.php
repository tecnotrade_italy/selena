<?php

use App\Content;
use App\ContentLanguage;
use App\Enums\ContentEnum;
use App\Language;
use App\User;
use Illuminate\Database\Seeder;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        #region HEADER

        $contentHeader = new Content();
        $contentHeader->code = ContentEnum::Header;
        $contentHeader->created_id = $idUser;
        $contentHeader->save();

        $contentLanguageHeader = new ContentLanguage();
        $contentLanguageHeader->content_id = $contentHeader->id;
        $contentLanguageHeader->language_id = $idLanguage;
        $contentLanguageHeader->content = 'Header';
        $contentLanguageHeader->created_id = $idUser;
        $contentLanguageHeader->save();

        #endregion HEADER

        #region FOOTER

        $contentFooter = new Content();
        $contentFooter->code = ContentEnum::Footer;
        $contentFooter->created_id = $idUser;
        $contentFooter->save();

        $contentLanguageFooter = new ContentLanguage();
        $contentLanguageFooter->content_id = $contentFooter->id;
        $contentLanguageFooter->language_id = $idLanguage;
        $contentLanguageFooter->content = 'Footer';
        $contentLanguageFooter->created_id = $idUser;
        $contentLanguageFooter->save();

        #endregion FOOTER

        #region CATEGORIES

        $contentFooter = new Content();
        $contentFooter->code = ContentEnum::Categories;
        $contentFooter->created_id = $idUser;
        $contentFooter->save();

        $contentLanguageFooter = new ContentLanguage();
        $contentLanguageFooter->content_id = $contentFooter->id;
        $contentLanguageFooter->language_id = $idLanguage;
        $contentLanguageFooter->content = 'Categories';
        $contentLanguageFooter->created_id = $idUser;
        $contentLanguageFooter->save();

        #endregion CATEGORIES

        #region ITEMS

        $contentFooter = new Content();
        $contentFooter->code = ContentEnum::Items;
        $contentFooter->created_id = $idUser;
        $contentFooter->save();

        $contentLanguageFooter = new ContentLanguage();
        $contentLanguageFooter->content_id = $contentFooter->id;
        $contentLanguageFooter->language_id = $idLanguage;
        $contentLanguageFooter->content = 'Items';
        $contentLanguageFooter->created_id = $idUser;
        $contentLanguageFooter->save();

        #endregion ITEMS

        #region PAGE CATEGORIES

        $contentFooter = new Content();
        $contentFooter->code = ContentEnum::PageCategories;
        $contentFooter->created_id = $idUser;
        $contentFooter->save();

        $contentLanguageFooter = new ContentLanguage();
        $contentLanguageFooter->content_id = $contentFooter->id;
        $contentLanguageFooter->language_id = $idLanguage;
        $contentLanguageFooter->content = 'PageCategories';
        $contentLanguageFooter->created_id = $idUser;
        $contentLanguageFooter->save();

        #endregion PAGE CATEGORIES

        #region PAGE ITEMS

        $contentFooter = new Content();
        $contentFooter->code = ContentEnum::PageItems;
        $contentFooter->created_id = $idUser;
        $contentFooter->save();

        $contentLanguageFooter = new ContentLanguage();
        $contentLanguageFooter->content_id = $contentFooter->id;
        $contentLanguageFooter->language_id = $idLanguage;
        $contentLanguageFooter->content = 'PageItems';
        $contentLanguageFooter->created_id = $idUser;
        $contentLanguageFooter->save();

        #endregion PAGE ITEMS
    }
}

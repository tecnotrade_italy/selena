<?php

use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Icon;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Tab;
use Illuminate\Database\Seeder;
use App\Language;
use App\Role;
use App\User;
use App\Functionality;

class PageAccSeeder extends Seeder
{

    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        #region PAGES
        $functionality = new Functionality();
        $functionality->code = 'Pages';
        $functionality->created_id = $idUser;
        $functionality->save();

        $tabSettings = new Tab();
        $tabSettings->code = 'Settings';
        $tabSettings->functionality_id = $functionality->id;
        $tabSettings->order = 20;
        $tabSettings->created_id = $idUser;
        $tabSettings->save();

        $languageTabSettings = new LanguageTab();
        $languageTabSettings->language_id = $idLanguage;
        $languageTabSettings->tab_id = $tabSettings->id;
        $languageTabSettings->description = 'Impostazioni';
        $languageTabSettings->created_id = $idUser;
        $languageTabSettings->save();


        #region IdLanguage
        $fieldIdLanguage = new Field();
        $fieldIdLanguage->code = 'IdLanguage';
        $fieldIdLanguage->tab_id = $tabSettings->id;
        $fieldIdLanguage->field = 'languagePageSetting.idLanguage';
        $fieldIdLanguage->data_type = 0;
        $fieldIdLanguage->created_id = $idUser;
        $fieldIdLanguage->save();

        $fieldLanguageIdLanguage = new FieldLanguage();
        $fieldLanguageIdLanguage->field_id = $fieldIdLanguage->id;
        $fieldLanguageIdLanguage->language_id = $idLanguage;
        $fieldLanguageIdLanguage->description = 'Lingua';
        $fieldLanguageIdLanguage->created_id = $idUser;
        $fieldLanguageIdLanguage->save();

        $fieldUserRoleIdLanguage = new FieldUserRole();
        $fieldUserRoleIdLanguage->field_id = $fieldIdLanguage->id;
        $fieldUserRoleIdLanguage->role_id = $idRole;
        $fieldUserRoleIdLanguage->enabled = true;
        $fieldUserRoleIdLanguage->input_type = 19;
        $fieldUserRoleIdLanguage->pos_x = 10;
        $fieldUserRoleIdLanguage->pos_y = 160;
        $fieldUserRoleIdLanguage->created_id = $idUser;
        $fieldUserRoleIdLanguage->save();
        #endregion IdLanguage

        #region CommandNews
        $fieldCommandNews = new Field();
        $fieldCommandNews->code = 'CommandTable';
        $fieldCommandNews->tab_id = $tabSettings->id;
        $fieldCommandNews->formatter = 'tableFormatter';
        $fieldCommandNews->created_id = $idUser;
        $fieldCommandNews->save();

        $fieldLanguageCommandNews = new FieldLanguage();
        $fieldLanguageCommandNews->field_id = $fieldCommandNews->id;
        $fieldLanguageCommandNews->language_id = $idLanguage;
        $fieldLanguageCommandNews->description = 'Lingua';
        $fieldLanguageCommandNews->created_id = $idUser;
        $fieldLanguageCommandNews->save();

        $fieldUserRoleCommandNews = new FieldUserRole();
        $fieldUserRoleCommandNews->field_id = $fieldCommandNews->id;
        $fieldUserRoleCommandNews->role_id = $idRole;
        $fieldUserRoleCommandNews->enabled = true;
        $fieldUserRoleCommandNews->table_order = 10;
        $fieldUserRoleCommandNews->input_type = 14;
        $fieldUserRoleCommandNews->created_id = $idUser;
        $fieldUserRoleCommandNews->save();
        #endregion CommandNews


    }
}

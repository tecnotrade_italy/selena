<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\MenuAdmin;
use App\LanguageMenuAdmin;
use App\MenuAdminUserRole;
use App\Language;
use App\Icon;
use App\Functionality;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Role;
use App\User;
use App\Helpers\LogHelper;
use App\Tab;
use App\LanguageTab;
use App\Setting;
use Illuminate\Support\Facades\DB;
use App\TemplateEditorType;
use App\TemplateEditorGroup;
use App\TemplateEditor;
use App\Enums\SettingEnum;


class MenuAdminSelena extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
     
        /*****************************************************************************/
        /* ICONE */
        /*****************************************************************************/
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-users'],[ 'description' => 'user']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-user-o'],[ 'description' => 'user']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-scissors'],[ 'description' => 'Scissors']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-eye'],[ 'description' => 'Eye']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-address-card'],[ 'description' => 'AddressCard']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-bookmark'],[ 'description' => 'Bookmark']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-eur'],[ 'description' => 'Euro']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-ticket'],[ 'description' => 'Ticket']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-flag'],[ 'description' => 'Flag']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-shopping-cart'],[ 'description' => 'Cart']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-chevron-down'],[ 'description' => 'ChevronDowns']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-cogs'],[ 'description' => 'Cogs']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-table'],[ 'description' => 'Table']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-cog'],[ 'description' => 'Settings']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-suitcase'],[ 'description' => 'Suitcase']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-bullhorn'],[ 'description' => 'Bullhorn']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-jsfiddle'],[ 'description' => 'JsFiddle']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-id-card'],[ 'description' => 'IdCard']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-book'],[ 'description' => 'Book']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-css3'],[ 'description' => 'Css3']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-file-o'],[ 'description' => 'File']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-newspaper-o'],[ 'description' => 'NewsPaper']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-file-text-o'],[ 'description' => 'File Text']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-list-alt'],[ 'description' => 'ListAlt']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-bookmark-o'],[ 'description' => 'Bookmark']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-truck'],[ 'description' => 'Truck']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-gift'],[ 'description' => 'Gift']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-user-circle'],[ 'description' => 'UserCicle']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-flag-checkered'],[ 'description' => 'Flag']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-envelope-o'],[ 'description' => 'EMail']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-thumb-tack'],[ 'description' => 'Thumb Tack']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-handshake-o'],[ 'description' => 'Handshake']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-server'],[ 'description' => 'Server']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-list-ol'],[ 'description' => 'List Ol']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-square'],[ 'description' => 'Square']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-map-signs'],[ 'description' => 'Signs']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-th-list'],[ 'description' => 'List']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-list-list'],[ 'description' => 'List']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-object-group'],[ 'description' => 'Object Group']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-map-o'],[ 'description' => 'Map']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-cart-plus'],[ 'description' => 'Cart Plus']);
        $icona = Icon::updateOrCreate(['css'   => 'fa'],[ 'description' => 'Empty']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-commenting-o'],[ 'description' => 'Comment']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-wrench'],[ 'description' => 'Wrench']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-tasks'],[ 'description' => 'Tasks']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-television'],[ 'description' => 'Television']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-th'],[ 'description' => 'Many Square']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-pencil'],[ 'description' => 'Pencil']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-credit-card'],[ 'description' => 'Credit Card']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-delicious'],[ 'description' => 'Delicious']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-paper-plane-o'],[ 'description' => 'Paper Plane']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-area-chart'],[ 'description' => 'AreaChart']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-clipboard'],[ 'description' => 'Clipboard']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-comment-o'],[ 'description' => 'Comment']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-maps-o'],[ 'description' => 'Maps']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-tags'],[ 'description' => 'Tags']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-university'],[ 'description' => 'University']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-map-pin'],[ 'description' => 'Zone']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-cube'],[ 'description' => 'Cube']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-address-book'],[ 'description' => 'Booking']); 
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-calendar'],[ 'description' => 'Calendar']); 
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-calendar-check-o'],[ 'description' => 'Appuntamenti']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-building-o'],[ 'description' => 'Luoghi']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-hand-paper-o'],[ 'description' => 'Servizi']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-link'],[ 'description' => 'Connessioni']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-percent'],[ 'description' => 'Percentuale']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-balance-scale'],[ 'description' => 'Bilancia']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-random'],[ 'description' => 'Random']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-exclamation-circle'],[ 'description' => 'Exclamation']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-paperclip'],[ 'description' => 'Attachment']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-home'],[ 'description' => 'Home']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-arrows-alt'],[ 'description' => 'ResizeImage']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-briefcase'],[ 'description' => 'Briefcase']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-keyboard-o'],[ 'description' => 'Keyboard']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-arrow-circle-o-down'],[ 'description' => 'Keyboard']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-sitemap'],[ 'description' => 'Sitemap']);
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-list'],[ 'description' => 'GeneratorPriceList']); 
        $icona = Icon::updateOrCreate(['css'   => 'fa fa-clock-o'],[ 'description' => 'Clock']);
        
        
     
        /*****************************************************************************/
        /* FUNZIONALITA' */
        /*****************************************************************************/        
        $funzionalita = Functionality::updateOrCreate(['code'   => 'RequestContacts'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'News&Blog'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Pages'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Newsletter'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Setting'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'UserSetting'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'ConfigurationSetting'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'ManageStyle'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'JS'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Contents'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'TemplateEditor'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'TemplateEditorGroup'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Customer'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'VatType'],[ 'created_id' => $idUser]);   
        $funzionalita = Functionality::updateOrCreate(['code'   => 'GroupNewsletter'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Contact'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'SelenaViews'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'UnitMeasure'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Category'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Producers'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'People'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Roles'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Categories'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Items'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Message'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Voucher'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'VoucherType'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'CartRules'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'TypeOptional'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Optional'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Carriers'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'CommunityImages'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'CommunityComments'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'CommunityReports'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'CommunityCommentsReports'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'CommunityContests'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'CommunityImagesCategories'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'CommunityImagesReportsTypes'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'AdvertisingArea'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'AdvertisingBanner'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'AmazonTemplate'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'DocumentStatuses'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'SalesOrder'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'SalesCarts'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Languages'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Cities'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'PostalCode'],[ 'created_id' => $idUser]); 
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Provinces'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Regions'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Nations'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Message'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'SendSms'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'HistoricalSms'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Blog'],[ 'created_id' => $idUser]);  
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Zone'],[ 'created_id' => $idUser]);  
        $funzionalita = Functionality::updateOrCreate(['code'   => 'GroupSms'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Supports'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'QueryGroupNewsletter'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Automation'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Places'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'BookingServices'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'BookingSettings'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Employees'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Exceptions'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Connections'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'News'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Carriage'],[ 'created_id' => $idUser]);  
        $funzionalita = Functionality::updateOrCreate(['code'   => 'CartRulesVoucher'],[ 'created_id' => $idUser]); 
        $funzionalita = Functionality::updateOrCreate(['code'   => 'ItemAttachmentType'],[ 'created_id' => $idUser]);  
        $funzionalita = Functionality::updateOrCreate(['code'   => 'ItemAttachmentFile'],[ 'created_id' => $idUser]);  
        $funzionalita = Functionality::updateOrCreate(['code'   => 'ImageResize'],[ 'created_id' => $idUser]);  
        $funzionalita = Functionality::updateOrCreate(['code'   => 'CategoriesPages'],[ 'created_id' => $idUser]); 
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Salesman'],[ 'created_id' => $idUser]); 
        $funzionalita = Functionality::updateOrCreate(['code'   => 'RobotsSetting'],[ 'created_id' => $idUser]); 
        $funzionalita = Functionality::updateOrCreate(['code'   => 'FormBuilder'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'FormBuilderType'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'RedirectUrlSetting'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Redirect'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Faq'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'MappaCatalogo'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'PriceListGenerator'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'Scheduler'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'RequestContactFormBuilder'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'TipologiaKeywors'],[ 'created_id' => $idUser]);
        $funzionalita = Functionality::updateOrCreate(['code'   => 'ListKeywors'],[ 'created_id' => $idUser]);
        
    /************************************************************************ *****/
        /* SETTINGS CONFIGURAZIONI    -  */
    /*****************************************************************************/
     $ConfigurationSettings= Setting::firstorCreate(['code'   => 'EmailOrder'],['value'   => '-', 'description' => 'Email di destinazione ordini azienda']);
     $ConfigurationSettings= Setting::firstorCreate(['code'   => 'EmailNewUser'],['value'   => '-', 'description' => 'Email di comunicazione se nuovi utenti si registrano al sito']);
     $ConfigurationSettings= Setting::firstorCreate(['code'   => 'ProtectFile'],['value'   => 'False', 'description' => 'Parametro di controllo per visualizzazione file in pagina dettaglio articolo']);
     $ConfigurationSettings= Setting::firstorCreate(['code'   => 'GoogleAnalyticsTrackId'],['value'   => '-', 'description' => 'Codice di tracciamento Google Analytics']);
     $ConfigurationSettings= Setting::firstorCreate(['code'   => 'GoogleAnalyticsViewId'],['value'   => '-', 'description' => 'Identificativo della vista Google associata all\'account Analytics']);
     $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::CopyContentDefaultLanguage],['value'   => 'ASK', 
       'created_id' => $idUser]);
       $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::CopyContentDefaultLanguage],['value'   => 'ASK', 
       'created_id' => $idUser]);
        $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::CopySettingDefaultLanguage],['value'   => 'ASK', 
       'created_id' => $idUser]);
        $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::LastDateSync],['value'   => Carbon::createFromDate(1900, 1, 1), 
       'description' => 'Ultima data di sincronizzazione', 'created_id' => $idUser]);
        $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::MenuRender],['value'   => 'Normal', 
       'description' => 'Tipo di visualizzazione menu', 'created_id' => $idUser]);
        $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::HomeNewsList],['value'   => '3', 
       'description' => 'Numero di articoli news in home', 'created_id' => $idUser]);
        
        $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::HomeBlogList],['value'   => '3', 
       'description' => 'Numero di articoli blog in home', 'created_id' => $idUser]);

        $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::BlogList],['value'   => '10', 
       'description' => 'Numero di articoli blog per pagina', 'created_id' => $idUser]);

        $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::NewsList],['value'   => '10', 
       'description' => 'Numero di articoli news per pagina', 'created_id' => $idUser]);
        $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::BlogRender],['value'   => '', 
       'description' => 'Tipo di visualizzazione articoli blog', 'created_id' => $idUser]);
        $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::NewsRender],['value'   => 'Box', 
       'description' => 'Tipo di visualizzazione articoli news', 'created_id' => $idUser]);

        $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::ContactsEmail],['value'   => 'supporto@tecnotrade.com', 
       'description' => 'Email di destinazione richieste contatti', 'created_id' => $idUser]);
        $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::BackgroundColorChartVisitorsViews],['value'   => '#3e95cd', 
       'description' => 'Colore di background grafico Visitatori', 'created_id' => $idUser]);
        $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::BorderColorChartVisitorsViews],['value'   => 'rgba(62,149,205,0.4)', 
       'description' => 'Colore di background grafico Pagine viste', 'created_id' => $idUser]);
        $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::BorderColorChartPagesViews],['value'   => 'rgb(255, 99, 132)', 
       'description' => 'Colore di bordo grafico Pagine viste', 'created_id' => $idUser]);
        $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::BackgroundColorChartDeviceSession],['value'   => '#0080ff, #de2727, #2eb85c', 'value'   => 'rgb(255, 99, 132)',
       'description' => 'Colore di background grafico Sessioni per dispositivo', 'created_id' => $idUser]);


       $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::BackgroundColorChartPagesViews],['value'   => '#0080ff, #de2727, #2eb85c', 'value'   => 'rgb(255, 99, 132)',
       'description' => 'Colore di background grafico Pagine viste', 'created_id' => $idUser]);

       

        $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::BackgroundColorChartSourceSession],['value'   => '#0080ff, #de2727, #2eb85c', 'value'   => '#rgb(255, 99, 132)',
       'description' => 'Colore di background grafico Sessioni per dispositivo"', 'created_id' => $idUser]);
        $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::BackgroundColorChartReturningVisitors],['value'   => '#2819ae, #b2b8c1, #248f48, #d69405, #de2727, #0080ff, #cfd4d8, #4d5666', 
       'description' => 'Colore di background grafico tipologia utenti', 'created_id' => $idUser]);

       $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::FacebookTrackId],['value'   => '', 
       'description' => 'Id Facebook', 'created_id' => $idUser]);
       $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::NumberOfItemsPerPage],['value'   => '20', 
       'description' => 'Numero di articoli per pagina', 'created_id' => $idUser]);


       $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::MailHost],['value'   => 'ssl0.ovh.net', 
       'description' => 'Mail Host', 'created_id' => $idUser]);
       $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::MailPort],['value'   => '465', 
       'description' => 'Mail Port', 'created_id' => $idUser]);
       $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::MailUsername],['value'   => 'selena@tecnotrade.com', 
       'description' => 'Mail Username', 'created_id' => $idUser]);
       $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::MailPassword],['value'   => 'wngF9kHTDaqF85vs!', 
       'description' => 'Mail Password', 'created_id' => $idUser]);
       $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::MailEncryption],['value'   => 'ssl', 
       'description' => 'Mail Encryption', 'created_id' => $idUser]);
       $ConfigurationSettings= Setting::firstorCreate(['code'   => SettingEnum::MailToErrorNewsletter],['value'   => 'support@tecnotrade.com', 
       'description' => 'Mail To Error Newsletter ', 'created_id' => $idUser]);
       

        /************************************************************************ *****/
        /* END SETTINGS CONFIGURAZIONI    -  */
        /*****************************************************************************/


        /************************************************************************ *****/
        /* DATI TEMPLATE EDITOR TYPES    -  */
        /*****************************************************************************/
        /*$template_editor_types = TemplateEditorType::updateorCreate(['code'   => 'Header'],[ 'description' => 'Header']);
        $template_editor_types = TemplateEditorType::updateorCreate(['code'   => 'Footer'],[ 'description' => 'Footer']);
        $template_editor_types = TemplateEditorType::updateorCreate(['code'   => 'Pages'],[ 'description' => 'Pagine']);
        $template_editor_types = TemplateEditorType::updateorCreate(['code'   => 'Email'],[ 'description' => 'Email']);
        $template_editor_types = TemplateEditorType::updateorCreate(['code'   => 'Blog'],[ 'description' => 'Blog']);*/
        /************************************************************************ *****/
        /* END DATI TEMPLATE EDITOR TYPES   -  */
        /*****************************************************************************/

         /************************************************************************ *****/
        /* DATI TEMPLATE EDITOR GROUP    -  */
        /*****************************************************************************/
        /*$template_editor_type_id= TemplateEditorType::where('code', '=', 'Pages')->first();
        $template_editor_type_id_footer= TemplateEditorType::where('code', '=', 'Footer')->first();
        $template_editor_type_id_header= TemplateEditorType::where('code', '=', 'Header')->first();
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Base'],[ 'description' => 'Base', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Contatti'],[ 'description' => 'Contatti', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Mappa'],[ 'description' => 'Mappa', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Processo'],[ 'description' => 'Processo', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Prezzo'],[ 'description' => 'Prezzo', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Pulsante'],[ 'description' => 'Pulsante', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Teams'],[ 'description' => 'Teams', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Immagini'],[ 'description' => 'Immagini', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Video'],[ 'description' => 'Video', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Newsletter'],[ 'description' => 'Newsletter', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Testimonial'],[ 'description' => 'Testimonial', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Slider'],[ 'description' => 'Slider', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Footer'],[ 'description' => 'Footer', 'template_editor_type_id' =>  $template_editor_type_id_footer->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Header'],[ 'description' => 'Header', 'template_editor_type_id' =>  $template_editor_type_id_header->id]);
        /************************************************************************ *****/
        /* END DATI TEMPLATE EDITOR GROUP    -  */
        /*****************************************************************************/


         /************************************************************************ *****/
        /* DATI TEMPLATE EDITOR  -  */
        /*****************************************************************************/
      /*  $template_editor_group= TemplateEditorGroup::where('code', '=', 'Immagini')->first();
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Immagine (fit)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/immagine-01.png',
        'html' => '<div class="section-pages"><div class="container"><img src="/api/storage/images/sea.jpg" style="width: 100%;" class="fr-fic fr-dii"></div></div><div><br></div>',
        'template_editor_group_id' =>  $template_editor_group->id,
        'order' => 20]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => '3 immagini (fit)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/immagine-04.png',
        'html' => '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-4">
        <img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></div>',
        'template_editor_group_id' =>  $template_editor_group->id,
        'order' => 20]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Immagine (100%)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/immagine-02.png',
        'html' => '<div class="section-pages"><div class="container-fluid"><img src="/api/storage/images/sea.jpg" style="width: 100%;" class="fr-fic fr-dii"></div></div><div><br></div>',
        'template_editor_group_id' =>  $template_editor_group->id,
        'order' => 20]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => '3 immagini con titolo e testo'],[ 
        'img' => '/api/storage/images/TemplateEditor/Previews/portfolio-3-blocchi.png',
        'html' => '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 text-center"><h1 style="color:#222;font-size:25px;">LOREM IPSUM</h1></div></div><div class="row"><div class="col-12 text-center"><hr></div></div><div class="row"><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div></div></div></div>',
        'template_editor_group_id' =>  $template_editor_group->id,
        'order' => 20]);

         $template_editor= TemplateEditor::updateOrCreate(['description'    => '2 immagini (fit)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/immagine-03.png',
        'html' => '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 col-md-6"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-6"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></div>',
        'template_editor_group_id' =>  $template_editor_group->id,
        'order' => 20]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => '4 blocchi con titolo e testo'],[ 
        'img' => '/api/storage/images/TemplateEditor/Previews/portfolio-4-blocchi.png',
        'html' => '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 text-center"><h1 style="color:#222;font-size:25px;">LOREM IPSUM</h1></div></div><div class="row"><div class="col-12 text-center"><hr></div></div><div class="row"><div class="col-12 col-md-3"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-3"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-3"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-3"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div></div></div></div>',
        'template_editor_group_id' =>  $template_editor_group->id,
        'order' => 20]);
      
          $template_editor= TemplateEditor::updateOrCreate(['description'    => '3 immagini senza margini (100%)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/immagine-44.png',
        'html' => '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12 col-md-4"><img src="/api/storage/images/rose.jpg" class="fr-fic fr-dii"></div><div class="col-12 col-md-4"><img src="/api/storage/images/grass.jpg" class="fr-fic fr-dii"></div><div class="col-12 col-md-4"><img src="https://selena.tecnotrade.com/api/storage/images/flower.jpg" class="fr-fic fr-dii"></div></div></div></div><div><br></div>',
        'template_editor_group_id' =>  $template_editor_group->id,
        'order' => 20]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => '4 immagini senza margini (100%)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/immagine-16.png',
        'html' => '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12 col-md-6 col-lg-3"><img src="/api/storage/images/flowers.jpg" class="fr-fic fr-dii"></div><div class="col-12 col-md-6 col-lg-3"><img src="/api/storage/images/rocks.jpg" class="fr-fic fr-dii"></div><div class="col-12 col-md-6 col-lg-3"><img src="/api/storage/images/skate.jpg" class="fr-fic fr-dii"></div><div class="col-12 col-md-6 col-lg-3"><img src="/api/storage/images/reading.jpg" class="fr-fic fr-dii"></div></div></div><div><br></div></div><div><br></div>',
        'template_editor_group_id' =>  $template_editor_group->id,
        'order' => 20]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => '2 immagini senza margini (100%)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/immagine-32.png',
        'html' => '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12 col-md-6"><img src="/api/storage/images/rose.jpg" class="fr-fic fr-dii"></div><div class="col-12 col-md-6"><img src="/api/storage/images/grass.jpg" class="fr-fic fr-dii"></div></div></div></div><div><br></div>',
        'template_editor_group_id' =>  $template_editor_group->id,
        'order' => 20]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => '4 immagini (fit)'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/immagine-05.png',
        'html' => '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 col-md-6 col-lg-3"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-6 col-lg-3"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-6 col-lg-3"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-6 col-lg-3"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></div><div><br></div>',
        'template_editor_group_id' =>  $template_editor_group->id,
        'order' => 20]);

         $template_editor= TemplateEditor::updateOrCreate(['description'    => '2 immagini con titolo e testo'],[ 
        'img' => '2 immagini con titolo e testo',
        'html' => '2 immagini con titolo e testo',
        'template_editor_group_id' =>  $template_editor_group->id,
        'order' => 20]);

        $template_editor_groupContact= TemplateEditorGroup::where('code', '=', 'Contatti')->first();
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 1'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-01.png',
        'html' => '<section><div class="container-fluid p-0 pb-md-5"><iframe class="map" src="https://maps.google.com/maps?hl=en&q=piazza maggiore+(Bologna)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" width="100%" height="300px"></iframe></div><div class="container"><div class="row mt-5"><div class="col-12 col-md-6 col-lg-5"><h2>Contattaci</h2><p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove.</p><p class="lead">It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p><p class="h3 mt-5"><strong>Email:</strong> <a href="#">hello@website.com</a></p><p class="lead"><strong>Phone:</strong> <a href="#">+39 123 123 1232</a></p></div><div class="col-12 col-md-6 ml-auto pt-5 pt-md-0"><div class="row"><div class="col"><input type="text" class="form-control" placeholder="Nome"></div><div class="col"><input type="text" class="form-control" placeholder="Cognome"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Inserisci la tua mail"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Oggetto"></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="3" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col"><button class="btn btn-primary" type="submit">Invia</button></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_groupContact->id,
        'order' => 70]);

       
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Form contatti'],[ 
        'img' => '/api/storage/images/TemplateEditor/Previews/FormContatti.JPG',
        'html' =>'<div class="section-pages"><div class="container"><form id="contactRequest"><div class="row"><div class="col-4"><label for="name"><strong>Nome</strong></label></div><div class="col-4"><label for="surname"><strong>Cognome</strong></label></div><div class="col-4"><label for="email"><strong>Email</strong></label></div></div><div class="row"><div class="col-4"><input id="name" name="name" type="text" required="required" value=""></div><div class="col-4"><input id="surname" name="surname" type="text" required="required" value=""></div><div class="col-4"><input id="email" name="email" type="email" required="required" value=""></div></div><div class="row"><div class="col-12"><label for="description"><strong>Richiesta</strong></label></div></div><div class="row"><div class="col-12"><textarea id="description" name="description" required="required" value="" style="width: 100%;" rows="5"></textarea></div></div><div class="row"><div class="col-12 text-right"><button class="btn btn-primary" id="btnSend" type="button">Invia richiesta</button></div></div></form></div></div>',
        'template_editor_group_id' =>  $template_editor_groupContact->id,
        'order' => 70]);

       
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 9'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-09.png',
        'html' =>'<section style="padding: 7.5rem 0;background-color:#F4F7FE;"><div class="container"><div class="row text-center justify-content-center"><div class="col-12 col-md-8 col-lg-7"><p class="h2">info@azienda.com</p><p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><p><br></p><div class="row"><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-facebook fa-stack" style="color:#222;font-size:40px;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-youtube-play fa-stack" style="color:#222;font-size:40px;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-instagram fa-stack" style="color:#222;font-size:40px;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-linkedin fa-stack" style="color:#222;font-size:40px;"></i></a></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_groupContact->id,
        'order' => 70]);

       
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 2 (rosso)'],[ 
        'img' => 'Modulo 2 (rosso)',
        'html' =>'<section style="background-image: url(/api/storage/images/hero/red.svg);background-size: cover;padding: 7rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-12 col-md-8 col-lg-7"><h1>Contattaci</h1><h2>Vorremmo avere tue notizie!</h2></div></div><div class="row pt-4"><div class="col-12"><div class="row"><div class="col-12 col-md"><input type="text" class="form-control" placeholder="Name"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Email"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Telefono (opzionale)"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Oggetto"></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="3" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col text-center"><button class="btn btn-dark" type="submit">Invia</button></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_groupContact->id,
        'order' => 70]);

        
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 2 (blu)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-02.png',
        'html' =>'<section class="fdb-block bg-dark" style="background-image: url(/api/storage/images/hero/blue.svg);background-size: cover;padding: 7rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-12 col-md-8 col-lg-7"><h1>Contattaci</h1><h2>Vorremmo avere tue notizie!</h2></div></div><div class="row pt-4"><div class="col-12"><div class="row"><div class="col-12 col-md"><input type="text" class="form-control" placeholder="Name"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Email"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Telefono (opzionale)"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Oggetto"></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="3" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col text-center"><button class="btn btn-dark" type="submit">Invia</button></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_groupContact->id,
        'order' => 70]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 4'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-04.png',
        'html' =>'<section style="background-image: url(/api/storage/images/shapes/9.svg);background-size: contain; background-position: center; background-repeat: no-repeat; padding: 3rem 0;"><div class="container py-5 my-5"><div class="row py-5"><div class="col py-5"><div class="fdb-box fdb-touch" style="background: #FFFFFF; padding: 3.75rem 2.5rem; overflow: hidden; color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-top: solid 0.3125rem #329ef7;"><div class="row text-center justify-content-center"><div class="col-12 col-md-9 col-lg-7"><h1>Contattaci</h1><p class="lead">Lorem Ipsum dolor sit amet, Lorem Ipsum dolor sit amet, Lorem Ipsum dolor sit amet, Lorem Ipsum dolor sit amet, .</p></div></div><div class="row justify-content-center pt-4"><div class="col-12 col-md-8"><div class="row"><div class="col-12 col-md"><input type="text" class="form-control" placeholder="Nome"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Email"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Oggetto"></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="3" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col text-center"><button class="btn btn-primary" type="submit">Invia</button></div></div></div></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_groupContact->id,
        'order' => 70]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 8'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-08.png',
        'html' =>'<section><div style="background-color: #F4F7FE;"><div class="container"><div class="row-100"><br></div><div class="row text-left"><div class="col-8"><h1>Contattaci</h1><p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate</p></div></div><div class="row-100"><br></div></div></div><div class="container bg-r"><div class="row-100"><br></div><div class="row"><div class="col-12 col-md-6 col-lg-5"><h2>Contatti e recapiti</h2><p class="text-large">Supporto, vendite e servizi sono disponibili ai seguenti recapiti</p><p class="h3 mt-4 mt-lg-5"><strong>Supporto</strong></p><p>+800 800 800</p><p><a href="#">Contatta il Supporto</a></p><p>Il nostro supporto tecnico &egrave; disponibile per telefono o email dalle 9.00 alle 17.00, dal Luned&igrave; al Venerd&igrave;.</p><p class="h3 mt-4 mt-lg-5"><strong>Vendite</strong></p><p>+800 800 800</p><p><a href="#">Contata il reparto commerciale</a></p><p>Il nostro supporto commerciale &egrave; disponibile per telefono o email dalle 9.00 alle 17.00, dal Luned&igrave; al Venerd&igrave;.</p><p class="h3 mt-4 mt-lg-5"><strong>Domande Generiche</strong></p><p><a href="#">hello@website.com</a></p></div><div class="col-12 col-md-6 ml-auto"><h2>Fornisci maggiori informazioni</h2><div class="row"><div class="col"><input type="text" class="form-control" placeholder="Nome"></div><div class="col"><input type="text" class="form-control" placeholder="Cognome"></div></div><div class="row mt-4"><div class="col"><input type="text" class="form-control" placeholder="Azienda"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Email"></div></div><div class="row mt-4"><div class="col"><input type="text" class="form-control" placeholder="Telefono"></div><div class="col"><input type="text" class="form-control" placeholder="Località"></div></div><div class="row mt-4"><div class="col"><select class="form-control" required="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<option value="">Seleziona il dipartimento</option> <option value="1">Supporto Tecnico</option> <option value="2">Vendite</option> <option value="3">Amministrazione</option>&nbsp;</select></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="5" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col"><button class="btn btn-primary" type="submit">Invia</button></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_groupContact->id,
        'order' => 70]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 3', 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-03.png',
        'html' =>'<section style="padding:4rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-12 col-md-8 col-lg-7"><h1>Contattaci</h1><p class="lead">Lorem Ipsum decided to leave for the far World of Grammar.</p></div></div><div class="row pt-4"><div class="col-12 col-md-6"><iframe class="map" src="https://maps.google.com/maps?hl=en&q=piazza maggiore+(Bologna)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" width="100%" height="350px"></iframe></div><div class="col-12 col-md-6 pt-5 pt-md-0"><div class="row"><div class="col"><input type="email" class="form-control" placeholder="Inserisci la tua mail"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Oggetto"></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="3" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col"><button class="btn btn-primary" type="submit">Invia</button></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_groupContact->id,
        'order' => 70]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 7'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-07.png',
        'html' =>'<section style="background-image: url(/api/storage/images/shapes/6.svg);background-size: contain; background-position: center; background-repeat: no-repeat;"><div class="container py-5"><div class="row text-center justify-content-center"><div class="col-12 col-md-8 col-lg-7"><h1>Contattaci</h1><p">Lasciaci i tuoi dati e sar&agrave; nostra premura contattarti al pi&ugrave; presto!</p></div></div><div class="row-50"><br></div><div class="row justify-content-center"><div class="col-12 col-md-8 col-lg-7"><div class="row"><div class="col"><label>Il tuo indirizzo email</label> &nbsp;&nbsp;&nbsp;<input type="text" class="form-control"></div></div><div class="row mt-4"><div class="col"><label>Oggetto</label> &nbsp;&nbsp;&nbsp;<input type="email" class="form-control"></div></div><div class="row mt-4"><div class="col"><label>Come possiamo aiutarti?</label> <textarea class="form-control" name="message" rows="3"></textarea></div></div><div class="row mt-4"><div class="col text-right"><button class="btn btn-dark" type="submit">Invia</button></div></div></div></div><div class="row-100"><br></div></div><div class="container-fluid p-0"><iframe class="map" src="https://maps.google.com/maps?hl=en&q=piazza maggiore+(Bologna)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" width="100%" height="300" allowfullscreen=""></iframe></div></section>',
        'template_editor_group_id' =>  $template_editor_groupContact->id,
        'order' => 70]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 6'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-06.png',
        'html' =>'<section><div class="container-fluid p-0 pb-5"><iframe class="map" src="https://maps.google.com/maps?hl=en&q=piazza maggiore+(Bologna)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" width="100%" height="300" allowfullscreen=""></iframe></div><div class="container"><div class="row pt-5"><div class="col-12"><div class="row"><div class="col-12 col-md"><label>Nome</label> &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" class="form-control"></div><div class="col-12 col-md mt-4 mt-md-0"><label>Cognome</label> &nbsp;&nbsp;&nbsp;&nbsp;<input type="text" class="form-control"></div></div><div class="row mt-4"><div class="col"><label>Il tuo indirizzo email</label> &nbsp;&nbsp;&nbsp;&nbsp;<input type="email" class="form-control"></div></div><div class="row mt-4"><div class="col"><label>Come possiamo aiutarti?</label> <textarea class="form-control" name="message" rows="3"></textarea></div></div><div class="row mt-4 text-center"><div class="col"><button class="btn btn-primary" type="submit">Invia</button></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_groupContact->id,
        'order' => 70]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 5'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-05.png',
        'html' =>'<section><div class="container-fluid p-0 pb-3"><iframe class="map" src="https://maps.google.com/maps?hl=en&q=piazza maggiore+(Bologna)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" width="100%" height="300" allowfullscreen=""></iframe></div><div class="container"><div class="row text-center justify-content-center pt-5"><div class="col-12 col-md-7"><h1>Contattaci</h1></div></div><div class="row justify-content-center pt-4"><div class="col-12 col-md-7"><div class="row"><div class="col"><input type="text" class="form-control" placeholder="Email"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Oggetto"></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="3" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col text-center"><button class="btn btn-primary" type="submit">Invia</button></div></div></div></div><div class="row-100"><br></div></div><div class="bg-dark"><div class="container"><div class="row-50"><br></div><div class="row justify-content-center text-center"><div class="col-12 col-md mr-auto ml-auto"><img alt="image" height="40" class="mb-2 fr-fic fr-dii" src="/api/storage/images/icons/phone.svg"><p class="lead">+39 112 123 752</p></div><div class="col-12 col-md pt-4 pt-md-0 mr-auto ml-auto"><img alt="image" height="40" class="mb-2 fr-fic fr-dii" src="/api/storage/images/icons/navigation.svg"><p class="lead">Piazza Maggiore 1<br>Bologna, BO 40100 Italy</p></div><div class="col-12 col-md pt-4 pt-md-0 mr-auto ml-auto"><img alt="image" height="40" class="mb-2 fr-fic fr-dii" src="/api/storage/images/icons/mail.svg"><p class="lead">supporto@azienda.com</p></div></div><div class="row-50"><br></div></div></div><div class="container"><div class="row-70"><br></div><div class="row text-center"><div class="col"><p class="h2"><a class="mx-2" href="#"></a> <a class="mx-2" href="#"></a> <a class="mx-2" href="#"></a> <a class="mx-2" href="#"></a> <a class="mx-2" href="#"></a></p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_groupContact->id,
        'order' => 70]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 2 (vola)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-02-viola.png',
        'html' =>'<section style="background-image: url(/api/storage/images/hero/purple.svg);background-size: cover;padding: 7rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-12 col-md-8 col-lg-7"><h1>Contattaci</h1><h2>Vorremmo avere tue notizie!</h2></div></div><div class="row pt-4"><div class="col-12"><div class="row"><div class="col-12 col-md"><input type="text" class="form-control" placeholder="Name"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Email"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Telefono (opzionale)"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Oggetto"></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="3" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col text-center"><button class="btn btn-dark" type="submit">Invia</button></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_groupContact->id,
        'order' => 70]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 2 (giallo)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/contatto-02-yellow.png',
        'html' =>'<section style="background-image: url(/api/storage/images/hero/yellow.svg);background-size: cover;padding: 7rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-12 col-md-8 col-lg-7"><h1 style="color:#333;">Contattaci</h1><h2 style="color:#333;">Vorremmo avere tue notizie!</h2></div></div><div class="row pt-4"><div class="col-12"><div class="row"><div class="col-12 col-md"><input type="text" class="form-control" placeholder="Name"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Email"></div><div class="col-12 col-md mt-4 mt-md-0"><input type="text" class="form-control" placeholder="Telefono (opzionale)"></div></div><div class="row mt-4"><div class="col"><input type="email" class="form-control" placeholder="Oggetto"></div></div><div class="row mt-4"><div class="col"><textarea class="form-control" name="message" rows="3" placeholder="Come possiamo aiutarti?"></textarea></div></div><div class="row mt-4"><div class="col text-center"><button class="btn btn-dark" type="submit">Invia</button></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_groupContact->id,
        'order' => 70]);

        $template_editor_process = TemplateEditorGroup::where('code', '=', 'Processo')->first();
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Processo con foto'],[ 
        'img' => '/api/storage/images/TemplateEditor/Previews/timeline-con-foto.png',
        'html' =>'<section><div class="container"><div class="row" style="margin-bottom:25px;"><div class="col-12 col-md-6"><img src="/api/storage/images/process-1.png" style="width: 100%;" class="fr-fic fr-dib"></div><div class="col-12 col-md-6"><h6 style="font-size:22px;margin-bottom:30px;">STEP ONE</h6><h2 style="font-size:50px;font-weight:bold;margin-bottom:15px;">Discovery</h2><p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s.</p></div></div><div class="row"><div class="col-12 col-md-6"><h6 style="font-size:22px;margin-bottom:30px;">STEP TWO</h6><h2 style="font-size:50px;font-weight:bold;margin-bottom:15px;">Design and Development</h2><p>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s.</p></div><div class="col-12 col-md-6"><img src="/api/storage/images/process-2.png" style="width: 100%;" class="fr-fic fr-dib"></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_process->id,
        'order' => 110]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Processi con step nero'],[ 
        'img' => '/api/storage/images/TemplateEditor/Previews/process-line-step-black.png',
        'html' =>'<div class="section-pages"><div class="container"><div class="row" style="margin-bottom:40px;"><div class="col-12 text-center"><h1>THE PROCESS</h1></div></div><div class="row" style="margin-bottom:25px;"><div class="col-12 col-md-4 text-center"><i class="fa fa-lightbulb-o fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;color:#fff;background:#000;"></i><h3>Step 1</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div><div class="col-12 col-md-4 text-center"><i class="fa fa-pencil-square-o fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;color:#fff;background:#000;"></i><h3>Step 2</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div><div class="col-12 col-md-4 text-center"><i class="fa fa-code fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;color:#fff;background:#000;"></i><h3>Step 3</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div></div><div class="row" style="margin-bottom:25px;"><div class="col-12 col-md-4 text-center"><i class="fa fa-rocket fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;color:#fff;background:#000;"></i><h3>Step 4</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div><div class="col-12 col-md-4 text-center"><i class="fa fa-desktop fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;color:#fff;background:#000;"></i><h3>Step 5</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div><div class="col-12 col-md-4 text-center"><i class="fa fa-money fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;color:#fff;background:#000;"></i><h3>Step 6</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div></div></div></div>',
        'template_editor_group_id' =>  $template_editor_process->id,
        'order' => 110]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Processi con step'],[
        'img' => '/api/storage/images/TemplateEditor/Previews/process-line-step.png',
        'html' =>'<section><div class="container"><div class="row"><div class="col-12 text-center"><h1>THE PROCESS</h1></div></div><div class="row" style="margin-bottom:25px;"><div class="col-12 col-md-4 text-center"><i class="fa fa-lightbulb-o fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;"></i><h3>Step 1</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div><div class="col-12 col-md-4 text-center"><i class="fa fa-pencil-square-o fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;"></i><h3>Step 2</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div><div class="col-12 col-md-4 text-center"><i class="fa fa-code fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;"></i><h3>Step 3</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div></div><div class="row" style="margin-bottom:25px;"><div class="col-12 col-md-4 text-center"><i class="fa fa-rocket fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;"></i><h3>Step 4</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div><div class="col-12 col-md-4 text-center"><i class="fa fa-desktop fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;"></i><h3>Step 5</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div><div class="col-12 col-md-4 text-center"><i class="fa fa-money fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;"></i><h3>Step 6</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_process->id,
        'order' => 110]);

        $template_editor_price= TemplateEditorGroup::where('code', '=', 'Prezzo')->first();
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Prezzi con background tondo'],[ 
        'img' => 'Prezzi con background tondo',
        'html' =>'',
        'template_editor_group_id' =>  $template_editor_price->id,
        'order' => 120]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Prezzi con background'],[
        'img' => '/api/storage/images/TemplateEditor/Previews/pricing.png',
        'html' =>'<div class="section-pages"><div class="container"><div class="row" style="margin-bottom:40px;"><div class="col-12 text-center"><h1 style="margin-bottom:15px;">SUBSCRIPTION PLANS</h1><h5 style="font-size:17px;">Choose the right plan that works for you.</h5></div></div><div class="row" style="margin-bottom:25px;"><div class="col-12 col-md-4 text-center"><div style="border:1px solid #ededed;padding-bottom:70px;padding-top:35px;box-shadow:rgba(0, 0, 0, 0.2) 0px 0px 10px -2px;"><h2 style="font-size:5.5rem;color:#ddd;margin-bottom:5px;">01</h2><h6 style="font-size:1.5rem;color:#000;margin-bottom:15px;">BASIC / FREE</h6><p style="font-size:18px;">Lorem Ipsum is<br>dummy text of the<br>printing industry.<br><br></p><div class="text-center"><a href="#" style="padding:10px 20px;border:2px solid #000;color:#000;">SELECT PLAN</a></div></div></div><div class="col-12 col-md-4 text-center"><div style="border:1px solid #ededed;padding-bottom:70px;padding-top:35px;box-shadow:rgba(0, 0, 0, 0.2) 0px 0px 10px -2px;background:#27ae60;"><h2 style="font-size:5.5rem;color:#fff;margin-bottom:5px;">02</h2><h6 style="font-size:1.5rem;color:#fff;margin-bottom:15px;">DELUX / &euro; 77</h6><p style="font-size:18px;color:#fff;">Lorem Ipsum is<br>dummy text of the<br>printing industry.<br><br></p><div class="text-center"><a href="#" style="padding:10px 20px;border:2px solid #fff;color:#fff;">SELECT PLAN</a></div></div></div><div class="col-12 col-md-4 text-center"><div style="border:1px solid #ededed;padding-bottom:70px;padding-top:35px;box-shadow:rgba(0, 0, 0, 0.2) 0px 0px 10px -2px;background:#f39c12;"><h2 style="font-size:5.5rem;color:#fff;margin-bottom:5px;">03</h2><h6 style="font-size:1.5rem;color:#fff;margin-bottom:15px;">PREMIUM / &euro; 189</h6><p style="font-size:18px;color:#fff;">Lorem Ipsum is<br>dummy text of the<br>printing industry.<br><br></p><div class="text-center"><a href="#" style="padding:10px 20px;border:2px solid #fff;color:#fff;">SELECT PLAN</a></div></div></div></div></div></div>',
        'template_editor_group_id' =>  $template_editor_price->id,
        'order' => 120]); 

        $template_Editor_video= TemplateEditorGroup::where('code', '=', 'Video')->first();
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'YouTube video (100%)'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/video-01.png',
        'html' =>'<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12 videoWrapper"><iframe width="100%" height="315" src="https://www.youtube.com/embed/mkggXE5e2yk" frameborder="0" allowfullscreen=""></iframe></div></div></div></div>',
        'template_editor_group_id' =>  $template_Editor_video->id,
        'order' => 50]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'YouTube video (fit)'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/video-00.png',
        'html' =>'<div class="section-pages"><div class="container"><div class="row"><div class="col-12 videoWrapper"><iframe width="100%" height="315" src="https://www.youtube.com/embed/mkggXE5e2yk" frameborder="0" allowfullscreen=""></iframe></div></div></div></div>',
        'template_editor_group_id' =>  $template_Editor_video->id,
        'order' => 50]); 


        $template_editor_mappa= TemplateEditorGroup::where('code', '=', 'Mappa')->first();
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Mappa (fit)'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/mappa-01.png',
        'html' =>'<section><div class="container p-0 pb-md-5"><iframe class="map" src="https://maps.google.com/maps?hl=en&q=piazza maggiore+(Bologna)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" width="100%" height="500px"></iframe></div></section>',
        'template_editor_group_id' =>  $template_editor_mappa->id,
        'order' => 80]);   

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Mappa (100%)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/mappa-02.png',
        'html' =>'<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12"><div style="height:240px;"><br></div></div></div></div></div><div><br></div>',
        'template_editor_group_id' =>  $template_editor_mappa->id,
        'order' => 80]);   

        $template_editor_header= TemplateEditorGroup::where('code', '=', 'Header')->first();
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Header 1'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/header-01.jpg',
        'html' =>'<section data-id-template-selena="1"><div class="section-pages"><div class="container"><div class="row" style="padding: 1rem 0;"><div class="col-4 offset-md-0 offset-4 col-md-1 nopadding text-center text-md-left"><img src="/api/storage/images/logo.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-3 offset-md-4" style="margin-top: 20px;"><div class="row"><div class="col-3" style="text-align: right; padding-right: 0px;"><i class="fa fa-phone" style="background: #e5e5e5; color: #253a8e; width: 50px; height: 50px;line-height: 50px; font-size: 26px; border-radius: 50%;text-align: center;"></i></div><div class="col-9" style="margin-top: 3px;"><a href="tel:+39051123456">+39 051 123456</a><br><a href="mailto:info@azienda.com">info@azienda.com</a></div></div></div><div class="col-12 col-md-4" style="margin-top: 20px;"><div class="row"><div class="col-3" style="text-align: right; padding-right: 0px;"><i class="fa fa-map-marker" style="background: #e5e5e5; color: #253a8e; width: 50px; height: 50px;line-height: 50px; font-size: 26px; border-radius: 50%;text-align: center;"></i></div><div class="col-9" style="margin-top: 3px;"><a href="/contatti">Viale della Libert&agrave;, 1<br>40010 BOLOGNA (BO)</a></div></div></div></div></div></div></section><div class="container"><div class="row menu">#menu#</div></div>',
        'template_editor_group_id' =>  $template_editor_header->id,
        'order' => 100]);   

        $template_editor_footer= TemplateEditorGroup::where('code', '=', 'Footer')->first();
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Footer 1'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/footer-01.jpg',
        'html' =>'<section data-id-template-selena="20"><div class="section-pages"><div class="container"><div class="row"><div class="col-4 offset-4 col-md-1 offset-md-0"><img src="/api/storage/images/logo-white.png" style="width:100%; margin-bottom:3rem;" class="fr-fic fr-dib"></div><div class="col-12 col-md-3 offset-md-1"><h1>CONTATTI</h1><p>VIALE LIBERT, 1<br>40010 BOLOGNA (BO)<br>Tel. <a href="tel:+39051123456">+39 051 123456</a><br>E-mail <a href="mailto:info@azienda.com">info@azienda.com</a></p></div><div class="col-12 col-md-3"><h1>LEGAL</h1><p>&copy; AZIENDA SRL<br>P.Iva e C.F.: 02345678901<br>C.S. 10.000,00 &euro; i.v.<br>Numero REA - BO - 123456<br><a href="/informativa">Informativa sito</a></p></div><div class="col-12 col-md-3"><h1>SOCIAL</h1><p><a href="#" target="_blank"><i class="fa fa-linkedin-square fa-2x fr-deletable"></i> Linkedin</a><br>Visita il nostro profilo su Linkedin e rimani aggiornato sugli ultimi progetti</p></div></div><div class="row" style="margin-top:4rem;"><div class="col-12 text-center"><a href="https://tecnotrade.com" style="font-size: 12px;" target="_blank">&nbsp;<span style="color:#3EA662;">Made</span> in <span style="color:#F64846;">Italy</span> with <i class="fa fa-heart fr-deletable" style="color:#F64846;"></i> by Tecnotrade</a></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_footer->id,
        'order' => 120]);   

        $template_editor_slider= TemplateEditorGroup::where('code', '=', 'Slider')->first();
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Slider Immagini (full)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/slider-01.jpg',
        'html' =>'<section><div class="container-fluid"><div id="bxslider"><div><img src="/api/storage/images/slide1.jpg" style="width: 100%;" class="fr-fic fr-dii"></div><div><img src="/api/storage/images/slide2.jpg" style="width: 100%;" class="fr-fic fr-dii"></div><div><img src="/api/storage/images/slide3.jpg" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_slider->id,
        'order' => 130]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Slider Immagini con testo (full)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/slider-02.jpg',
        'html' =>'<section><div class="container-fluid"><div id="bxslider"><div><img src="/api/storage/images/slide1.jpg" style="width: 100%;" class="fr-fic fr-dii"><div class="section-banner" style="position:absolute;width:100%;top:30%;"><div class="row"><div class="col-sm-12 text-center"><h1 class="fadeInUp animated" data-caption-delay="100" style="color:#fff;">Benvenuto nel nostro sito</h1><br><a class="button fadeInUp button-slider animated" data-caption-delay="100" href="/">Scopri di pi&ugrave;</a></div></div></div></div><div><img src="/api/storage/images/slide2.jpg" style="width: 100%;" class="fr-fic fr-dii"><div class="section-banner" style="position:absolute;width:100%;top:30%;"><div class="row"><div class="col-sm-12 text-center"><h1 class="fadeInUp animated" data-caption-delay="100" style="color:#fff;">Acquista online</h1><br><a class="button fadeInUp button-slider animated" data-caption-delay="100" href="/">Scopri come</a></div></div></div></div><div><img src="/api/storage/images/slide3.jpg" style="width: 100%;" class="fr-fic fr-dii"><div class="section-banner" style="position:absolute;width:100%;top:30%;"><div class="row"><div class="col-sm-12 text-center"><h1 class="fadeInUp animated" data-caption-delay="100" style="color:#fff;">Accedi all&#39;area riservata</h1><br><a class="button button-slider " href="/green">Registrati ora</a></div></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_slider->id,
        'order' => 130]); 

        $template_editor_testimonial= TemplateEditorGroup::where('code', '=', 'Testimonial')->first();
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 3'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/testimonial-03.png',
        'html' =>'<section style="padding: 7.5rem 0;background-image: url(/api/storage/images/hero/red.svg);background-size: cover; background-position: center; background-repeat: no-repeat;"><div class="container" style="background: #FFFFFF; padding: 3.75rem 2.5rem;border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><div class="row align-items-center justify-content-center"><div class="col-12 col-md-10 col-lg-8"><p class="lead">&quot;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&quot;</p><p class="lead"><strong>Mario Rossi</strong> <em class="ml-4">CEO e Titolare</em></p></div><div class="col-8 col-sm-6 col-md-2 col-lg-3 col-xl-2 mt-4 mt-md-0 ml-auto mr-auto mr-md-0"><img class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/1.jpg"></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_testimonial->id,
        'order' => 90]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 2'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/testimonial-02.png',
        'html' =>'<section style="padding:3rem 0;"><div class="container"><div class="row align-items-center justify-content-center"><div class="col-10 col-sm-6 col-md-4 col-lg-3 col-xl-2 m-auto"><img alt="image" class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/3.jpg"></div><div class="col-12 col-md-8 ml-auto mr-auto mt-4 mt-md-0"><p class="lead">&quot;Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.&quot;</p><p class="h3 mt-4 mt-lg-5"><strong>Claudia Bianchi</strong></p><p><em>Direttore Marketing</em></p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_testimonial->id,
        'order' => 90]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 6'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/testimonial-06.png',
        'html' =>'<section><div class="container"><div class="row text-center justify-content-center"><div class="col-md-10 col-lg-8 col-xl-7"><h1>Dicono di noi</h1><p class="lead">A small river named Duden flows by their place and supplies it with the necessary regelialia. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p></div></div><div class="row mt-5 justify-content-center"><div class="col-md-10 col-lg-3 ml-auto mr-auto text-center"><p class="h3 mb-4 mb-lg-5">&quot;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.&quot;</p><p><img alt="image" height="50" class="rounded-circle fr-fic fr-dii" src="/api/storage/images/people/6.jpg"></p><p class="lead"><strong>Claudia Bianchi</strong></p><p>Co-founder, Company</p></div><div class="col-md-10 col-lg-3 pt-5 pt-lg-0 ml-auto mr-auto text-center"><p class="h3 mb-4 mb-lg-5">&quot;Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&quot;</p><p><img alt="image" height="50" class="rounded-circle fr-fic fr-dii" src="/api/storage/images/people/3.jpg"></p><p class="lead"><strong>Teresa Rossi</strong></p><p>Co-founder, Company</p></div><div class="col-md-10 col-lg-3 pt-5 pt-lg-0 ml-auto mr-auto text-center"><p class="h3 mb-4 mb-lg-5">&quot;A small river named Duden flows by their place and supplies it with the necessary regelialia.&quot;</p><p><img alt="image" height="50" class="rounded-circle fr-fic fr-dii" src="/api/storage/images/people/9.jpg"></p><p class="lead"><strong>Mario Verdi</strong></p><p>Co-founder, Company</p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_testimonial->id,
        'order' => 90]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 5'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/testimonial-05.png',
        'html' =>'<section style="background-image: url(/api/storage/images/shapes/9.svg);padding: 7.5rem 0; position: relative; background-size: cover; background-position: center; background-repeat: no-repeat; background-color: #FFFFFF;"><div class="container"><div class="row text-center justify-content-center"><div class="col-md-10 col-lg-8 col-xl-7"><h1>Dicono di noi</h1><p class="lead">A small river named Duden flows by their place and supplies it with the necessary regelialia. Separated they live in Bookmarksgrove right at the coast of the Semantics.</p></div></div><div class="row mt-5 align-items-center justify-content-center"><div class="col-md-8 col-lg-4"><div style="background: #FFFFFF; padding: 3.75rem 2.5rem; overflow: hidden; color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><div class="row no-gutters align-items-center"><div class="col-3"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/1.jpg"></div><div class="col-8 ml-auto"><p><strong>Mario Rossi</strong><br><em>Co-founder at Company</em></p></div></div><div class="row mt-4"><div class="col-12"><p class="lead">&quot;Even the all-powerful Pointing has no control about the blind texts it is an small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.&quot;</p></div></div></div></div><div class="col-md-8 col-lg-4 mt-4 mt-lg-0"><div style="background: #FFFFFF; padding: 3.75rem 2.5rem; overflow: hidden; color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><div class="row no-gutters align-items-center"><div class="col-3"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/3.jpg"></div><div class="col-8 ml-auto"><p><strong>Teresa Bianchi</strong><br><em>Co-founder at Company</em></p></div></div><div class="row mt-4"><div class="col-12"><p class="lead">&quot;Far far away, behind the word mountains, far from the countries Vokalia. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&quot;</p></div></div></div></div><div class="col-md-8 col-lg-4 mt-4 mt-lg-0"><div style="background: #FFFFFF; padding: 3.75rem 2.5rem; overflow: hidden; color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><div class="row no-gutters align-items-center"><div class="col-3"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/6.jpg"></div><div class="col-8 ml-auto"><p><strong>Lucia Verdi</strong><br><em>Co-founder at Company</em></p></div></div><div class="row mt-4"><div class="col-12"><p class="lead">&quot;Separated they live in Bookmarksgrove right at the coast of the Semantics, the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.&quot;</p></div></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_testimonial->id,
        'order' => 90]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 4'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/testimonial-04.png',
        'html' =>'<section style="padding: 3rem 0;"><div class="container"><div class="row align-items-center justify-content-center"><div class="col-md-6"><div style="border-top: solid 0.3125rem #329ef7;padding: 3.75rem 2.5rem;color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><p class="h3 mb-4">&quot;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics.&quot;</p><p><img alt="image" height="50" class="rounded-circle fr-fic fr-dii" src="/api/storage/images/people/3.jpg"> <strong class="ml-3">Claudia Bianchi</strong></p></div></div><div class="col-md-6 mt-4 mt-md-0"><div style="border-top: solid 0.3125rem #329ef7;padding: 3.75rem 2.5rem;color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><p class="h3 mb-4">&quot;A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.&quot;</p><p><img alt="image" height="50" class="rounded-circle fr-fic fr-dii" src="/api/storage/images/people/9.jpg"> <strong class="ml-3">Mario Rossi</strong></p></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_testimonial->id,
        'order' => 90]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 1'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/testimonial-01.png',
        'html' =>'<section style="padding:3rem 0;"><div class="container"><div class="row align-items-center justify-content-center"><div class="col-12 col-md-10 col-lg-8"><p class="lead">&quot;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&quot;</p><p class="lead"><strong>Mario Rossi</strong> <em class="ml-4">CEO e Titolare</em></p></div><div class="col-8 col-sm-6 col-md-2 col-lg-3 col-xl-2 mt-4 mt-md-0 ml-auto mr-auto mr-md-0"><img class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/1.jpg"></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_testimonial->id,
        'order' => 90]); 

        $template_editor_newsletter= TemplateEditorGroup::where('code', '=', 'Newsletter')->first();
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 4'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/newsletter-04.png',
        'html' =>'<section style="padding: 7.5rem 0;"><div class="container"><div class="row"><div class="col-12 col-md-6 m-md-auto ml-lg-0 col-lg-5"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/draws/group-chat.svg"></div><div class="col-12 col-md-10 col-lg-6 mt-4 mt-lg-0 ml-auto mr-auto ml-lg-auto text-left"><div class="row"><div class="col"><h1>Iscriviti</h1><p class="lead">Ricevi le nostre comunicazioni periodiche e rimani aggiornato su tutte le nostre novit&agrave;, offerte e promozioni!</p></div></div><div class="row mt-4"><div class="col"><div class="input-group"><input type="text" class="form-control" placeholder="Inserisci il tuo indirizzo email"><div class="input-group-append"><button class="btn btn-primary" style="padding: .475rem 3rem .575rem; margin-left: -1px;margin-top: 1px;" type="button">Invia</button></div></div></div></div><div class="row"><div class="col"><p><em>*Il tuo indirizzo &egrave; al sicuro, non lo condivideremo con nessuno.</em></p></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_newsletter->id,
        'order' => 30]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 1'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/newsletter-01.png',
        'html' =>'<section class="fdb-block"><div class="container" style="padding: 7.5rem 0;background-position: right;background-repeat: no-repeat; background-size: contain;"><div class="row justify-content-center"><div class="col-12 col-md-8 col-lg-6 text-center"><h1>Iscriviti</h1><div class="input-group mt-4 mb-4"><input type="text" class="form-control" placeholder="Inserisci il tuo indirizzo email"><div class="input-group-append"><button class="btn btn-primary" type="button">Invia</button></div></div><p class="h4">Seguici su <a href="#">Facebook</a> e su <a href="#">Instagram</a>.</p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_newsletter->id,
        'order' => 30]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 3'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/newsletter-03.png',
        'html' =>'<section style="background-color:#F4F7FE;padding: 7.5rem 0;"><div class="container"><div class="row justify-content-center"><div class="col-12  col-md-10 col-lg-8 col-xl-6 text-center"><img alt="image" height="40" src="/api/storage/images/icons/layers.svg" style="margin-bottom:20px;" class="fr-fic fr-dii"><h1>Iscriviti alla newsletter!</h1><p class="lead">Ricevi le nostre comunicazioni periodiche e rimani aggiornato su tutte le nostre novit&agrave;, offerte e promozioni!</p><div class="input-group mt-4 mb-4"><input type="text" class="form-control" placeholder="Inserisci il tuo indirizzo email"><div class="input-group-append"><button class="btn btn-primary" style="border: 0.125rem solid transparent; padding: 0.45rem 2.625rem; margin-left: -1px; margin-top: 0px;" type="button">Invia</button></div></div><p class="h5"><em>*Il tuo indirizzo &egrave; al sicuro, non lo condivideremo con nessuno.</em></p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_newsletter->id,
        'order' => 30]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 2'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/newsletter-02.png',
        'html' =>'<section class="fdb-block" style="background-image: url(/api/storage/images/hero/blue.svg);padding: 7.5rem 0;background-position: right;background-repeat: no-repeat; background-size: cover;"><div class="container"><div class="fdb-box"><div class="row justify-content-center align-items-center" style="background: #FFFFFF; padding: 3.75rem 2.5rem; overflow: hidden; color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><div class="col-12 col-lg-6"><h2>Unisciti a noi!</h2><p class="lead">Iscriviti alla nostra newsletter e rimani aggiornato su tutte le nostre novit&agrave;, offerte e promozioni!</p></div><div class="col-12 col-lg-5 text-center"><div class="input-group mt-4"><input type="text" class="form-control" placeholder="Inserisci il tuo indirizzo email"><div class="input-group-append"><button class="btn btn-primary" style="margin-left: -1px; padding: 0.475rem 1.625rem 0.575rem; margin-top: 0px;" type="button">Invia</button></div></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_newsletter->id,
        'order' => 30]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 5'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/newsletter-05.png',
        'html' =>'<section style="background-image: url(/api/storage/images/shapes/1.svg);background-size: cover; background-position: center; background-repeat: no-repeat; padding: 7.5rem 0;"><div class="container"><div class="row justify-content-end"><div class="col-12 col-md-9 col-lg-8"><div style="background: #FFFFFF; padding: 3.75rem 2.5rem;overflow: hidden;color: #444444;border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-top: solid 0.3125rem #329ef7;"><div class="row justify-content-center"><div class="col-12 col-lg-10"><h1>Iscriviti alla newsletter</h1><p>Ricevi le nostre comunicazioni periodiche e rimani aggiornato su tutte le nostre novit&agrave;, offerte e promozioni!</p></div></div><div class="row justify-content-center mt-4"><div class="col-12 col-lg-10"><div class="input-group"><input type="text" class="form-control" placeholder="Inserisci il tuo indirizzo email"><div class="input-group-append"><button class="btn btn-primary" style="padding: .475rem 3rem .575rem; margin-left: -1px;margin-top: 1px;" type="button">Invia</button></div></div></div></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_newsletter->id,
        'order' => 30]); 

        $template_editor_teams= TemplateEditorGroup::where('code', '=', 'Teams')->first();
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 1'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/teams-01.png',
        'html' =>'<section style="padding: 7.5rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-8"><h1>Team</h1></div></div><div class="row text-center mt-5"><div class="col-4"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/9.jpg"><h3 style="margin-top:1.5rem;"><strong>Mario Rossi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">Separated they live in Bookmarksgrove right at the coast of the Semantics.</p></div><div class="col-4"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/1.jpg"><h3 style="margin-top:1.5rem;"><strong>Carlo Bianchi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">One morning, when Gregor Samsa woke from troubled dreams.</p></div><div class="col-4"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/3.jpg"><h3 style="margin-top:1.5rem;"><strong>Lucia Verdi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">A small river named Duden flows by their place and supplies it.</p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_teams->id,
        'order' => 60]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 7'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/teams-07.png',
        'html' =>'<section style="padding: 7.5rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-8"><h1>Il Nostro Team</h1><p class="lead">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p></div></div><div style="height:6rem;"><br></div><div class="row justify-content-center text-left"><div class="col-sm-6"><div class="row align-items-center"><div class="col-4"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/6.jpg"></div><div class="col-8"><h3><strong>Claudia Bianchi</strong></h3><p class="lead">Ruolo</p><p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p></div></div></div><div class="col-sm-6"><div class="row align-items-center"><div class="col-4"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/3.jpg"></div><div class="col-8"><h3><strong>Lucia Rossi</strong></h3><p class="lead">Ruolo</p><p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p></div></div></div></div><div class="row-70"><br></div><div class="row justify-content-center text-left"><div class="col-sm-6"><div class="row align-items-center"><div class="col-4"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/1.jpg"></div><div class="col-8"><h3><strong>Mario Verdi</strong></h3><p class="lead">Ruolo</p><p>One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p></div></div></div><div class="col-sm-6"><div class="row align-items-center"><div class="col-4"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/9.jpg"></div><div class="col-8"><h3><strong>Fabio Pollici</strong></h3><p class="lead">Ruolo</p><p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life.</p></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_teams->id,
        'order' => 60]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 4'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/teams-04.png',
        'html' =>'<section style="padding: 7.5rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-8"><h1>Team</h1></div></div><div class="row text-center mt-5"><div class="col-lg-3 col-sm-12 col-md-6"><div style="box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-radius: 0.25rem;"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/people/9.jpg"><div style="padding: 1rem!important;"><h3 style="margin-top:1.5rem;"><strong>Mario Rossi</strong></h3><p style="color: #8892a0;">Ruolo</p></div></div></div><div class="col-lg-3 col-sm-12 col-md-6"><div style="box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-radius: 0.25rem;"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/people/1.jpg"><div style="padding: 1rem!important;"><h3 style="margin-top:1.5rem;"><strong>Carlo Bianchi</strong></h3><p style="color: #8892a0;">Ruolo</p></div></div></div><div class="col-lg-3 col-sm-12 col-md-6"><div style="box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-radius: 0.25rem;"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/people/3.jpg"><div style="padding: 1rem!important;"><h3 style="margin-top:1.5rem;"><strong>Lucia Verdi</strong></h3><p style="color: #8892a0;">Ruolo</p></div></div></div><div class="col-lg-3 col-sm-12 col-md-6"><div style="box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-radius: 0.25rem;"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/people/6.jpg"><div style="padding: 1rem!important;"><h3 style="margin-top:1.5rem;"><strong>Claudia Aranci</strong></h3><p style="color: #8892a0;">Ruolo</p></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_teams->id,
        'order' => 60]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 2'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/teams-02.png',
        'html' =>'<section style="padding: 7.5rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-8"><h1>Team</h1></div></div><div class="row text-center mt-5"><div class="col-lg-3 col-sm-12 col-md-6"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/9.jpg"><h3 style="margin-top:1.5rem;"><strong>Mario Rossi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">Separated they live in Bookmarksgrove right at the coast of the Semantics.</p></div><div class="col-lg-3 col-sm-12 col-md-6"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/1.jpg"><h3 style="margin-top:1.5rem;"><strong>Carlo Bianchi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">One morning, when Gregor Samsa woke from troubled dreams.</p></div><div class="col-lg-3 col-sm-12 col-md-6"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/3.jpg"><h3 style="margin-top:1.5rem;"><strong>Lucia Verdi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">A small river named Duden flows by their place and supplies it.</p></div><div class="col-lg-3 col-sm-12 col-md-6"><img alt="image" class="img-fluid rounded fr-fic fr-dii" src="/api/storage/images/people/6.jpg"><h3 style="margin-top:1.5rem;"><strong>Claudia Aranci</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">A small river named Duden flows by their place and supplies it.</p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_teams->id,
        'order' => 60]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 5'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/teams-05.png',
        'html' =>'<section style="padding: 7.5rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-8"><h1>Team</h1></div></div><div class="row text-center mt-5"><div class="col-sm-3 m-sm-auto"><img alt="image" class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/9.jpg"><h3 style="margin-top:1.5rem;"><strong>Mario Rossi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">Separated they live in Bookmarksgrove right at the coast of the Semantics.</p></div><div class="col-sm-3 m-sm-auto"><img alt="image" class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/1.jpg"><h3 style="margin-top:1.5rem;"><strong>Carlo Bianchi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">One morning, when Gregor Samsa woke from troubled dreams.</p></div><div class="col-sm-3 m-sm-auto"><img alt="image" class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/3.jpg"><h3 style="margin-top:1.5rem;"><strong>Lucia Verdi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">A small river named Duden flows by their place and supplies it.</p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_teams->id,
        'order' => 60]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 3'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/teams-03.png',
        'html' =>'<section style="padding: 7.5rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-8"><h1>Team</h1></div></div><div class="row text-center mt-5"><div class="col-lg-4 col-sm-12"><div style="box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-radius: 0.25rem;"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/people/1.jpg"><div style="padding: 1rem!important;"><h3 style="margin-top:1.5rem;"><strong>Carlo Bianchi</strong></h3><p style="color: #8892a0;">Ruolo</p></div></div></div><div class="col-lg-4 col-sm-12"><div style="box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-radius: 0.25rem;"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/people/3.jpg"><div style="padding: 1rem!important;"><h3 style="margin-top:1.5rem;"><strong>Lucia Verdi</strong></h3><p style="color: #8892a0;">Ruolo</p></div></div></div><div class="col-lg-4 col-sm-12"><div style="box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;border-radius: 0.25rem;"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/people/6.jpg"><div style="padding: 1rem!important;"><h3 style="margin-top:1.5rem;"><strong>Claudia Aranci</strong></h3><p style="color: #8892a0;">Ruolo</p></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_teams->id,
        'order' => 60]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 6'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/teams-06.png',
        'html' =>'<section style="padding: 7.5rem 0;"><div class="container"><div class="row text-center justify-content-center"><div class="col-8"><h1>Team</h1></div></div><div class="row text-center mt-5"><div class="col-sm-2 m-sm-auto"><img alt="image" class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/9.jpg"><h3 style="margin-top:1.5rem;"><strong>Mario</strong><br><strong>Rossi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">Separated they live in Bookmarksgrove right at the coast of the Semantics.</p></div><div class="col-sm-2 m-sm-auto"><img alt="image" class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/1.jpg"><h3 style="margin-top:1.5rem;"><strong>Carlo</strong><br><strong>Bianchi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">One morning, when Gregor Samsa woke from troubled dreams.</p></div><div class="col-sm-2 m-sm-auto"><img alt="image" class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/3.jpg"><h3 style="margin-top:1.5rem;"><strong>Lucia</strong><br><strong>Verdi</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">A small river named Duden flows by their place and supplies it.</p></div><div class="col-sm-2 m-sm-auto"><img alt="image" class="img-fluid rounded-circle fr-fic fr-dii" src="/api/storage/images/people/6.jpg"><h3 style="margin-top:1.5rem;"><strong>Claudia</strong><br><strong>Gialli</strong></h3><p style="color: #8892a0;">Ruolo</p><p style="color: #8892a0;">A small river named Duden flows by their place and supplies it.</p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_teams->id,
        'order' => 60]); 

        $template_editor_pulsante= TemplateEditorGroup::where('code', '=', 'Pulsante')->first();
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Pulsante 9 (viola)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-09.png',
        'html' =>'<section style="background-image: url(/api/storage/images/hero/purple.svg);min-height: calc(100% - 2 * 7.5rem);padding: 7.5rem 0;background-position: center; background-repeat: no-repeat; background-size: cover;"><div class="container justify-content-center align-items-center d-flex"><div class="row justify-content-center text-center"><div class="col-12 col-md-8"><img alt="image" class="fr-fic fr-dii" src="/api/storage/images/icons/coffee.svg" style="width: 3.75rem;"><h1 style="color:#fff;">Prendiamoci un caff&egrave;</h1><p style="color:#fff;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p class="mt-5"><a class="btn btn-dark" href="#">Clicca qui per...</a></p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_pulsante->id,
        'order' => 40]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Pulsante 5'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-05.png',
        'html' =>'<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0; background-size: cover; background-position: center; background-repeat: no-repeat;"><div class="container py-5 my-5" style="background-image: url(/api/storage/images/shapes/2.svg);background-size: contain; background-position: center; background-repeat: no-repeat;"><div class="row justify-content-center py-5"><div class="col-12 col-md-10 col-lg-8 text-center"><div style="background: #FFFFFF; padding: 3.75rem 2.5rem; overflow: hidden; color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><h1>Call to Action</h1><p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;</p><p class="mt-4"><a class="btn btn-primary" href="#">Download</a></p></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_pulsante->id,
        'order' => 40]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Pulsante 6'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-06.png',
        'html' =>'<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 5rem 0;"><div class="container py-5 my-5 bg-r" style="background-image: url(/api/storage/images/shapes/4.svg);background-size: contain; background-position: center; background-repeat: no-repeat;"><div class="row justify-content-end py-5"><div class="col-12 col-md-8 col-lg-6 mr-5 text-center"><div style="background: #FFFFFF; padding: 3.75rem 2.5rem; overflow: hidden; color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><h1>Call to Action</h1><p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><p class="mt-4"><a class="btn btn-secondary" href="#">Download</a></p></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_pulsante->id,
        'order' => 40]); 
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Pulsante 7'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-07.png',
        'html' =>'<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0;"><div class="container"><div class="row align-items-center justify-content-center"><div class="col-auto"><h2>Clicca qui per scaricare la nostra presentazione</h2></div><div class="col-auto mt-4 mt-sm-0"><a class="btn btn-primary" href="#">Download</a></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_pulsante->id,
        'order' => 40]); 
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Pulsante 8'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-08.png',
        'html' =>'<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0;"><div class="container"><div class="row align-items-center justify-content-center"><div class="col-auto text-center"><a class="btn btn-outline-secondary" href="#">Download</a></div><div class="col-auto mt-4 mt-sm-0"><h2>Clicca qui per scaricare la nostra presentazione</h2></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_pulsante->id,
        'order' => 40]); 
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Pulsante 9 (blu)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-09-blue.png',
        'html' =>'<section style="background-image: url(/api/storage/images/hero/blue.svg);min-height: calc(100% - 2 * 7.5rem);padding: 7.5rem 0;background-position: center; background-repeat: no-repeat; background-size: cover;"><div class="container justify-content-center align-items-center d-flex"><div class="row justify-content-center text-center"><div class="col-12 col-md-8"><img alt="image" class="fr-fic fr-dii" src="/api/storage/images/icons/coffee.svg" style="width: 3.75rem;"><h1 style="color:#fff;">Prendiamoci un caff&egrave;</h1><p style="color:#fff;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p class="mt-5"><a class="btn btn-dark" href="#">Clicca qui per...</a></p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_pulsante->id,
        'order' => 40]); 
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Pulsante 9 (rosso)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-09-red.png',
        'html' =>'<section style="background-image: url(/api/storage/images/hero/red.svg);min-height: calc(100% - 2 * 7.5rem);padding: 7.5rem 0;background-position: center; background-repeat: no-repeat; background-size: cover;"><div class="container justify-content-center align-items-center d-flex"><div class="row justify-content-center text-center"><div class="col-12 col-md-8"><img alt="image" class="fr-fic fr-dii" src="/api/storage/images/icons/coffee.svg" style="width: 3.75rem;"><h1 style="color:#fff;">Prendiamoci un caff&egrave;</h1><p style="color:#fff;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p class="mt-5"><a class="btn btn-dark" href="#">Clicca qui per...</a></p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_pulsante->id,
        'order' => 40]); 
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Pulsante 9 (giallo)'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-09-yellow.png',
        'html' =>'<section style="background-image: url(/api/storage/images/hero/yellow.svg);min-height: calc(100% - 2 * 7.5rem);padding: 7.5rem 0;background-position: center; background-repeat: no-repeat; background-size: cover;"><div class="container justify-content-center align-items-center d-flex"><div class="row justify-content-center text-center"><div class="col-12 col-md-8"><img alt="image" class="fr-fic fr-dii" src="/api/storage/images/icons/coffee.svg" style="width: 3.75rem;"><h1>Prendiamoci un caff&egrave;</h1><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p class="mt-5"><a class="btn btn-dark" href="#">Clicca qui per...</a></p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_pulsante->id,
        'order' => 40]); 
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Pulsante 10'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-10.png',
        'html' =>'<section class="fdb-block py-0"><div class="container bg-r py-5 my-5" style="background-image: url(/api/storage/images/shapes/1.svg);padding: 7.5rem 0;background-position: right;background-repeat: no-repeat; background-size: contain;"><div class="row py-5"><div class="col-12 col-sm-10 col-md-8 col-lg-6 text-left"><h1>Call to Action</h1><p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p><p class="mt-4"><a class="btn btn-primary" href="#">Download</a></p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_pulsante->id,
        'order' => 40]); 
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Pulsante 1'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-01.png',
        'html' =>'<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0;"><div class="container"><div class="row justify-content-center"><div class="col-12 col-md-8 text-center"><p class="lead">&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.&quot;</p><p class="mt-5 mt-sm-4"><a class="btn btn-primary" href="#">Download</a></p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_pulsante->id,
        'order' => 40]); 
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Pulsante 2'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-02.png',
        'html' =>'<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0;"><div class="container"><div class="row justify-content-center"><div class="col-12 col-md-6 text-center"><h1>Call to Action</h1><p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><p class="mt-5 mt-sm-4"><a class="btn btn-secondary" href="#">Download</a></p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_pulsante->id,
        'order' => 40]); 
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Pulsante 3'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-03.png',
        'html' =>'<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0;"><div class="container"><div class="row justify-content-center"><div class="col-12 col-sm-10 col-md-8 col-lg-4 text-center"><p class="lead">Seguici la nostra attivit&agrave; sui social:</p><p class="h2"><a class="mx-2" href="#"><i class="fa fa-facebook fa-stack" style="color:#329ef7;font-size:30px;"></i></a> <a class="mx-2" href="#"><i class="fa fa-instagram fa-stack" style="color:#329ef7;font-size:30px;"></i></a> <a class="mx-2" href="#"><i class="fa fa-linkedin fa-stack" style="color:#329ef7;font-size:30px;"></i></a> <a class="mx-2" href="#"><i class="fa fa-youtube fa-stack" style="color:#329ef7;font-size:30px;"></i></a></p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_pulsante->id,
        'order' => 40]); 
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Pulsante 4'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-04.png',
        'html' =>'<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0;"><div class="container"><div class="row text-center pb-0 pb-lg-4"><div class="col-12"><h1>Call to action</h1></div></div><div class="row text-center pt-4 pt-md-5"><div class="col-12 col-sm-10 col-md-6 col-lg-4 m-sm-auto"><img alt="image" class="fdb-icon fr-fic fr-dii" src="/api/storage/images/icons/gift.svg"><h3>Prima Azione</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><p class="mt-3"><a class="btn btn-light sl-1" href="#">Button</a></p></div><div class="col-12 col-sm-10 col-md-6 col-lg-4 ml-sm-auto mr-sm-auto mt-5 mt-md-0"><img alt="image" class="fdb-icon fr-fic fr-dii" src="/api/storage/images/icons/cloud.svg"><h3>Seconda Azione</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><p class="mt-3"><a class="btn btn-light sl-1" href="#">Button</a></p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_pulsante->id,
        'order' => 40]); 

        $template_editor_base= TemplateEditorGroup::where('code', '=', 'Base')->first();
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Spaziatore (240 px)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-122.png',
        'html' =>'<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12"><div style="height:240px;"><br></div></div></div></div></div><div><br></div>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 10 (100%)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-10.png',
        'html' =>'<section><div class="container-fluid"><div class="row align-items-center"><div class="col-12 col-md-6 mb-4 mb-md-0"><img alt="image" class="img-fluid fr-fic fr-dii" src="https://selena.tecnotrade.com/api/storage/images/rose.jpg"></div><div class="col-11 col-md-6 col-lg-5 ml-md-auto offset-sm-1 text-left"><h1>Titolo</h1><p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 6'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-06.png',
        'html' =>'<section style="padding:3rem 0;"><div class="container"><div class="row"><div class="col text-left"><h2>Far far away...</h2><p>Far far away, behind the word mountains, far from the countries <a href="#">Vokalia</a> and <a href="#">Consonantia</a>, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p><p>On her way she met a copy. The copy warned the Little Blind Text, that where it came from it would have been rewritten a thousand times and everything that was left from its origin would be the word &quot;and&quot; and the <a href="#">Little Blind Text</a> should turn around and return to its own, safe country. But nothing the copy said could convince her and so it didn&rsquo;t take long until a few insidious Copy Writers ambushed her, made her drunk with Longe and Parole and dragged her into their agency, where they abused her for their.</p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 7'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-07.png',
        'html' =>'<section style="padding:3rem 0;"><div class="container"><div class="row"><div class="col text-center"><h1>Una azienda incredibile</h1><div class="row text-left pt-4"><div class="col-12 col-md-6"><p style="margin-right:30px;">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place far far away.</p></div><div class="col-12 col-md-6"><p>Separated they live in Bookmarksgrove right at the coast of the Semantics, far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast.</p></div></div></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 1'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-01.png',
        'html' =>'<section style="padding:3rem 0;"><div class="container"><div class="row justify-content-center"><div class="col col-md-8 text-center"><h1>Una azienda incredibile</h1></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Spaziatore (120 px)'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-121.png',
        'html' =>'<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12"><div style="height:120px;"><br></div></div></div></div></div><div><br></div>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 8 (fit)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-08.png',
        'html' =>'<section style="padding:3rem 0;"><div class="container"><div class="row align-items-center"><div class="col-12 col-md-6 col-lg-5"><h1>Titolo</h1><p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p></div><div class="col-12 col-md-6 ml-md-auto mt-4 mt-md-0"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/shapes/2.svg"></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Icone social'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-15.png',
        'html' =>'<section><div class="container"><div class="row"><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-facebook fa-stack" style="color:#222;font-size:40px;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-youtube-play fa-stack" style="color:#222;font-size:40px;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-instagram fa-stack" style="color:#222;font-size:40px;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-linkedin fa-stack" style="color:#222;font-size:40px;"></i></a></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 2 (70%)'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-02-2.png',
        'html' =>'<section style="padding:3rem 0;"><div class="container"><div class="row justify-content-center"><div class="col col-md-8 text-center"><p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 
       
       $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Linea orizzontale (fit)'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-13.png',
        'html' =>'<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12"><hr style="background: none; background-color: transparent; border: none; border-top: rgba(0, 0, 0, 0.18) 1px solid; margin: 30px 0 25px; padding: 5px;"><br></div></div></div></div><div><br></div>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 


         $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 2 (100%)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-02-1.png',
        'html' =>'<section style="padding:3rem 0;"><div class="container"><div class="row justify-content-center"><div class="col col-md-12 text-center"><p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean1</p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 

         $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 3'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-03.png',
        'html' =>'<section style="padding:3rem 0;"><div class="container"><div class="row justify-content-center"><div class="col col-md-8 text-center"><h1>Una azienda incredibile</h1><p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far <a href="#">World of Grammar</a>.</p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 

     
        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 9 (fit)'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-09.png',
        'html' =>'<section style="padding:3rem 0;"><div class="container"><div class="row align-items-center"><div class="col-12 col-md-6 mb-4 mb-md-0"><img alt="image" class="img-fluid fr-fic fr-dii" src="/api/storage/images/shapes/2.svg"></div><div class="col-12 col-md-6 col-lg-5 ml-md-auto text-left"><h1>Titolo</h1><p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 4'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-04.png',
        'html' =>'<section style="padding:3rem 0;"><div class="container"><div class="row"><div class="col col-sm-10 col-md-8 text-left"><p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 


         $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Spaziatore (60 px)'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-12.png',
        'html' =>'<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12"><div style="height:60px;"><br></div></div></div></div></div><div><br></div>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 

         $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Icone social colorate'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-16.png',
        'html' =>'<section><div class="container"><div class="row"><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-facebook fa-stack" style="color:#fff;font-size:40px;background:#1877f2;border-radius:50%;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-youtube-play fa-stack" style="color:#fff;font-size:40px;background:#ff0000;border-radius:50%;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-instagram fa-stack" style="color:#fff;font-size:40px;background:#c32aa3;border-radius:50%;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-linkedin fa-stack" style="color:#fff;font-size:40px;background:#007bb5;border-radius:50%;"></i></a></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 

         $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 11 (100%)'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-11.png',
        'html' =>'<section><div class="container-fluid"><div class="row align-items-center"><div class="col-11 col-md-6 col-lg-6 ml-md-auto offset-sm-1 text-left"><h1>Titolo</h1><p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p></div><div class="col-12 col-md-6 mb-4 mb-md-0"><img alt="image" class="img-fluid fr-fic fr-dii" src="https://selena.tecnotrade.com/api/storage/images/rose.jpg"></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); 


         $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Modulo 5'],[
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/base-05.png',
        'html' =>'<section style="padding:3rem 0;"><div class="container"><div class="row justify-content-end"><div class="col col-sm-10 col-md-8 text-left"><p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far <a href="#">World of Grammar</a>.</p></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_base->id,
        'order' => 10]); */
        /************************************************************************ *****/
        /* END DATI TEMPLATE EDITOR  -  */
        /*****************************************************************************/

        /************************************************************************ *****/
        /* MENU CONTENUTI - INVIO SMS */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'ContentsSms'],
        [ 
            'icon_id' => Icon::where('css', 'fa fa-comment-o')->first()->id,
            'url'     => '',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'SMS',
            'created_id' => $idUser
        ]);

        $menu = MenuAdmin::updateOrCreate(['code'   => 'Marketing']);
        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 25,
            'created_id' => $idUser
        ]);


        /********************/
        /* VOCE MENU CAMPAGNE SMS */
        /********************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'SendSms'],
        [ 
            'functionality_id' => Functionality::where('code', 'SendSms')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-comment-o')->first()->id,
            'url'     => '/admin/sms/sendSms',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Campagne',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'ContentsSms')->first()->id,
            'created_id' => $idUser
        ]);

        /********************/
        /* VOCE MENU GRUPPI SMS */
        /********************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'GroupSms'],
        [ 
            'functionality_id' => Functionality::where('code', 'GroupSms')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-comment-o')->first()->id,
            'url'     => '/admin/sms/GroupSms',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Gruppi Sms',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 'f',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'ContentsSms')->first()->id,
            'created_id' => $idUser
        ]);


        /********************/
        /* TAB TABLE */
        /********************/
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'GroupSms')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);

        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'TableCheckBox'],['created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'CheckBox', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '13', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'GroupSmsFormatter'],['created_id' => $idUser, 'formatter' => 'GroupSmsFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'20', 'created_id' => $idUser]); 
        

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 

        
        /********************/
        /* TAB DETTAGLIO */
        /********************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'GroupSms')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);
        
        //description
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'colspan' => '12', 'created_id' => $idUser]); 
        
        /*****************************************************************************/
        /*
        /*
        /* MENU CONTENUTI */
        /*
        /*
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'WebContents'],
        [ 
            'icon_id' => Icon::where('css', 'fa fa-pencil')->first()->id,
            'url'     => '',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Contenuti',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CONTENUTI - PAGINE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Pages'],
        [ 
            'functionality_id' => Functionality::where('code', 'Pages')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-list-alt')->first()->id,
            'url'     => '/admin/pages',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Pagine',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'WebContents')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CONTENUTI - BLOG */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Blog'],
        [ 
            'functionality_id' => Functionality::where('code', 'Blog')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-newspaper-o')->first()->id,
            'url'     => '/admin/blog',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Blog',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'WebContents')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CONTENUTI - MESSAGGI*/
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Message'],
        [ 
            'functionality_id' => Functionality::where('code', 'Message')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-commenting-o')->first()->id,
            'url'     => '/admin/setting/messages',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Messaggi',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 30,
            'menu_admin_father_id' => MenuAdmin::where('code', 'WebContents')->first()->id,
            'created_id' => $idUser
        ]);

        //nella tabella devo sostituire il content con la descrizione
        $tabsTableId = Tab::where('code', 'Table')->where('functionality_id', Functionality::where('code', 'Message')->first()->id);
        //DB::update('UPDATE fields SET code = \'description\', field = \'description\' where tab_id = :tab_id and code = \'content\'', ['tab_id' => $tabsTableId]);

        /*$field = Field::where('tab_id', $tabsTableId)->where('code', 'description')->first();
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'40', 'created_id' => $idUser]); 

        //tabella dettaglio
        $tabsDetailId = Tab::where('code', 'Detail')->where('functionality_id', Functionality::where('code', 'Message')->first()->id)->first()->id;
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetailId, 'code' => 'code'],['created_id' => $idUser, 'field'=> 'code', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsDetailId, 'code' => 'content'],['created_id' => $idUser, 'field'=> 'content', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Testo', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '17', 'pos_x'=>'10', 'pos_y'=>'30', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsDetailId, 'code' => 'message_type_id'],['created_id' => $idUser, 'field'=> 'message_type_id', 'service' => 'messagetype', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Tipo Messaggio', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '2', 'pos_x'=>'20', 'pos_y'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsDetailId, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'20', 'created_id' => $idUser]); 

*/

        /************************************************************************ *****/
        /* MENU CONTENUTI - NEWS */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'News'],
        [ 
            //'functionality_id' => Functionality::where('code', 'Message')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-television')->first()->id,
            'url'     => '/admin/news',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'News',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 40,
            'menu_admin_father_id' => MenuAdmin::where('code', 'WebContents')->first()->id,
            'created_id' => $idUser
        ]);

    
        $menu = MenuAdmin::updateOrCreate(['code'   => 'ContentsSettings'],
        [ 
            'icon_id' => Icon::where('css', 'fa fa-cog')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Impostazioni',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 60,
            'menu_admin_father_id' => MenuAdmin::where('code', 'WebContents')->first()->id,
            'created_id' => $idUser
        ]);


        $menu = MenuAdmin::updateOrCreate(['code'   => 'CategoriesPages'],
        [ 
            'functionality_id' => Functionality::where('code', 'CategoriesPages')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-th')->first()->id,
            'url'     => '/admin/crm/categoriespages',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Categorie Post',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'  => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'ContentsSettings')->first()->id,
            'created_id' => $idUser
        ]);

        /************************************************************************ *****/
        /* MENU CONTENUTI - IMPOSTAZIONI - TIPOLOGIA KEYWORDS */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'TypologyKeywords'],
        [ 
            'functionality_id' => Functionality::where('code', 'TipologiaKeywors')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-list')->first()->id,
            'url'     => '/admin/crm/typologykeywords',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Tipologia Keywords',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'  => 't',
            'order' => 15,
            'menu_admin_father_id' => MenuAdmin::where('code', 'ContentsSettings')->first()->id,
            'created_id' => $idUser
        ]);

        /******************/
        /* TAB TABLE */
        /******************/

        $tabsTable = Tab::updateOrCreate(['functionality_id'   => Functionality::where('code', 'TipologiaKeywors')->first()->id, 'code' => 'Table'],['order' => 10, 'created_id' => $idUser]);
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'TipologiaKeyworsFormatter'],['created_id' => $idUser, 'formatter' => 'TipologiaKeyworsFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'20', 'created_id' => $idUser]); 

        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(['functionality_id'   => Functionality::where('code', 'TipologiaKeywors')->first()->id, 'code' => 'Detail'],['order' => 20,'created_id' => $idUser]); 
        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);
        
        //description
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'colspan' => '12', 'created_id' => $idUser]); 
    
        //html
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'html'],['field'=>'html', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Template html keywords', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '17', 'pos_x'=>'10', 'pos_y'=>'20', 'colspan' => '12', 'created_id' => $idUser]); 
    
        /************************************************************************ *****/
        /* MENU CONTENUTI - IMPOSTAZIONI - LISTA KEYWORDS */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'ListKeywords'],
        [ 
            'functionality_id' => Functionality::where('code', 'ListKeywors')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-list')->first()->id,
            'url'     => '/admin/crm/listkeywords',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Lista Keywords',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'  => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'ContentsSettings')->first()->id,
            'created_id' => $idUser
        ]);

        /******************/
        /* TAB TABLE */
        /******************/

        $tabsTable = Tab::updateOrCreate(['functionality_id'   => Functionality::where('code', 'ListKeywors')->first()->id, 'code' => 'Table'],['order' => 10, 'created_id' => $idUser]);
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'ListKeyworsFormatter'],['created_id' => $idUser, 'formatter' => 'ListKeyworsFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'url'],['created_id' => $idUser, 'field'=> 'url', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Url', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 

        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(['functionality_id'   => Functionality::where('code', 'ListKeywors')->first()->id, 'code' => 'Detail'],['order' => 20,'created_id' => $idUser]); 
        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);
        
        //description
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'colspan' => '6', 'created_id' => $idUser]); 
    
        //tipologia
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'typology_keywords_id'],['field'=>'typology_keywords_id', 'service' => 'typologyKeywords', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Tipologia keywords', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '2', 'pos_x'=>'20', 'pos_y'=>'10', 'colspan' => '6', 'created_id' => $idUser]);

        //keyword
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'keywords'],['field'=>'keywords', 'data_type'=> '1', 'on_change' => 'generateUrl', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Seo keywords', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'20', 'colspan' => '6', 'created_id' => $idUser]);  
        
        //titolo
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'title'],['field'=>'title', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Seo Title', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'20', 'colspan' => '6', 'created_id' => $idUser]);


        //url
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'url'],['field'=>'url', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Url', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'30', 'colspan' => '10', 'created_id' => $idUser]);

        
         //online
         $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'online'],['field'=>'online', 'data_type'=> '3', 'created_id' => $idUser]); 
         $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Online', 'created_id' => $idUser]); 
         $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'30', 'pos_y'=>'30', 'colspan' => '2', 'created_id' => $idUser]);


        /*****************************************************************************/
        /*
        /*
        /* MENU CATALOGO */
        /*
        /*
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Catalogo'],
        [ 
            //'functionality_id' => XXX,
            'icon_id' => Icon::where('css', 'fa fa-book')->first()->id,
            'url'     => '',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Catalogo',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CATALOGO - PRODOTTI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Items'],
        [ 
            'functionality_id' => Functionality::where('code', 'Items')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-square')->first()->id,
            'url'     => '/admin/crm/Items',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Prodotti',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Catalogo')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CATALOGO - CATEGORIE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Categorie'],
        [ 
            'functionality_id' => Functionality::where('code', 'Categories')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-th')->first()->id,
            'url'     => '/admin/crm/Categories',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Categorie',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Catalogo')->first()->id,
            'created_id' => $idUser
        ]);

        //field categoria padre in tabella elenco cateogrie
        $idFuncionality = Functionality::where('code', 'Categories')->first()->id;
        $tabsTableId = Tab::where('code', 'Table')->where('functionality_id', $idFuncionality)->get()->first();
        
        /*$field = Field::updateOrCreate(['tab_id'   => $tabsTableId->id, 'code' => 'categoryFather'],['field'=>'categoryFather', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Appartiene a', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'50', 'created_id' => $idUser]); 
*/
        //field online checbox in tabella elenco cateogrie
        $field = Field::updateOrCreate(['tab_id'   => $tabsTableId->id, 'code' => 'CategoriesFatherFormatter'],['created_id' => $idUser, 'formatter' => 'CategoriesFatherFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Categoria', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'30', 'created_id' => $idUser]); 

        //field url in tabella elenco cateogrie
        $field = Field::updateOrCreate(['tab_id'   => $tabsTableId->id, 'code' => 'urlTable'],['field'=>'urlTable', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Url', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '15', 'table_order'=>'60', 'created_id' => $idUser]); 

        //field url in tabella elenco cateogrie
        $field = Field::updateOrCreate(['tab_id'   => $tabsTableId->id, 'code' => 'nuberOfItem'],['field'=>'nuberOfItem', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Prodotti', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '15', 'table_order'=>'69', 'created_id' => $idUser]); 
        
        //field online checbox in tabella elenco cateogrie
        $field = Field::updateOrCreate(['tab_id'   => $tabsTableId->id, 'code' => 'OnlineCategoriesFormatter'],['created_id' => $idUser, 'formatter' => 'OnlineCategoriesFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Online', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'70', 'created_id' => $idUser]); 



         /*****************************************************************************/
        /* MENU CATALOGO - CATEGORIE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'MappaCatalogo'],
        [ 
            'functionality_id' => Functionality::where('code', 'MappaCatalogo')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-sitemap')->first()->id,
            'url'     => '/admin/crm/mappacatalogo',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Albero',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 25,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Catalogo')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CATALOGO - BRAND */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Produttori'],
        [ 
            'functionality_id' => Functionality::where('code', 'Producers')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-wrench')->first()->id,
            'url'     => '/admin/registries/Producer',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Brand',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 30,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Catalogo')->first()->id,
            'created_id' => $idUser
        ]);


        /*****************************************************************************/
        /* MENU CATALOGO - LISTINI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'PriceLists'],
        [ 
            //'functionality_id' => Functionality::where('code', 'Producers')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-eur')->first()->id,
            'url'     => '/admin/crm/list',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Listini',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 40,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Catalogo')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CATALOGO - ALLEGATI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Attachment'],
        [ 
            //'functionality_id' => Functionality::where('code', 'Producers')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-paperclip')->first()->id,
            'url'     => '/admin/crm/attachment',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Allegati',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 50,
            'menu_admin_father_id' => MenuAdmin::where('code', 'WebContents')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CATALOGO - ALLEGATI - ATICOLI ALLEGATI TIPOLOGIE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'ItemAttachmentType'],
        [ 
            'functionality_id' => Functionality::where('code', 'ItemAttachmentType')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-paperclip')->first()->id,
            'url'     => '/admin/crm/itemattachmenttype',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Tipologie',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'  => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Attachment')->first()->id,
            'created_id' => $idUser
        ]);

        /******************/
        /* TAB TABLE */
        /******************/

        $tabsTable = Tab::updateOrCreate(['functionality_id'   => Functionality::where('code', 'ItemAttachmentType')->first()->id, 'code' => 'Table'],['order' => 10, 'created_id' => $idUser]);
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'ItemAttachmentTypeFormatter'],['created_id' => $idUser, 'formatter' => 'ItemAttachmentTypeFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'10', 'created_id' => $idUser]); 
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'code'],['created_id' => $idUser, 'field'=> 'code', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'20', 'created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'item_attachment_father_type_id_description'],['created_id' => $idUser, 'field'=> 'item_attachment_father_type_id_description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Appartiene a', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'40', 'created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'order'],['created_id' => $idUser, 'field'=> 'order', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ordinamento', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'50', 'created_id' => $idUser]);
        
        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(['functionality_id'   => Functionality::where('code', 'ItemAttachmentType')->first()->id, 'code' => 'Detail'],['order' => 20,'created_id' => $idUser]); 
        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);
        
        //codice
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'code'],['field'=>'code', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'colspan' => '2', 'created_id' => $idUser]); 
    
        //descrizione
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'10', 'colspan' => '4', 'created_id' => $idUser]); 
    
        //appartiene a
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'item_attachment_father_type_id'],['field'=>'item_attachment_father_type_id', 'service' => 'itemAttachmentFatherType', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Appartiene a', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '2', 'pos_x'=>'30', 'pos_y'=>'10', 'colspan' => '4', 'created_id' => $idUser]); 

        //ordinamento
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'order'],['field'=>'order', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ordinamento', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'40', 'pos_y'=>'10', 'colspan' => '2', 'created_id' => $idUser]); 
        

        /*****************************************************************************/
        /* MENU CATALOGO - ALLEGATI - ARTICOLI ALLEGATI TIPOLOGIE FILE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'ItemAttachmentFile'],
        [ 
            'functionality_id' => Functionality::where('code', 'ItemAttachmentFile')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-paperclip')->first()->id,
            'url'     => '/admin/crm/itemattachmentfile',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Documenti',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'  => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Attachment')->first()->id,
            'created_id' => $idUser
        ]);

        /******************/
        /* TAB TABLE */
        /******************/

        $tabsTable = Tab::updateOrCreate(['functionality_id'   => Functionality::where('code', 'ItemAttachmentFile')->first()->id, 'code' => 'Table'],['order' => 10, 'created_id' => $idUser]);
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'ItemAttachmentFileFormatter'],['created_id' => $idUser, 'formatter' => 'ItemAttachmentFileFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'10', 'created_id' => $idUser]); 
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'attachment'],['created_id' => $idUser, 'field'=> 'attachment', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Allegato', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'20', 'created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'items'],['created_id' => $idUser, 'field'=> 'items', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Articoli', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'40', 'created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'categories'],['created_id' => $idUser, 'field'=> 'categories', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Categorie', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'50', 'created_id' => $idUser]);
        

        /*****************************************************************************/
        /* MENU CATALOGO - SCHEDE TECNICHE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'DataSheet'],
        [ 
            //'functionality_id' => Functionality::where('code', 'Producers')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-table')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Schede Tecniche',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 60,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Catalogo')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CATALOGO - SCHEDE TECNICHE - OPTIONAL */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'ItemsAttributes'],
        [ 
            'functionality_id' => Functionality::where('code', 'Optional')->first()->id,
            'icon_id' => Icon::where('css', 'fa')->first()->id,
            'url'     => '/admin/crm/optional',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Optional',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'DataSheet')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CATALOGO - SCHEDE TECNICHE - TIPI OPTIONAL */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'ItemsTypeAttributes'],
        [ 
            'functionality_id' => Functionality::where('code', 'TypeOptional')->first()->id,
            'icon_id' => Icon::where('css', 'fa')->first()->id,
            'url'     => '/admin/crm/typeoptional',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Tipi di Optional',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'DataSheet')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CATALOGO - SCHEDE TECNICHE - GRUPPI DI SPECIFICHE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'DataSheetGroups'],
        [ 
            //'functionality_id' => Functionality::where('code', 'Producers')->first()->id,
            'icon_id' => Icon::where('css', 'fa')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Gruppi di specifiche',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'DataSheet')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CATALOGO - REVIEWS */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Reviews'],
        [ 
            //'functionality_id' => Functionality::where('code', 'Producers')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-commenting-o')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Reviews',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 70,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Catalogo')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CATALOGO - IMPOSTAZIONI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'CatalogSettings'],
        [ 
            //'functionality_id' => Functionality::where('code', 'Producers')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-cog')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Impostazioni',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 80,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Catalogo')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CATALOGO - IMPOSTAZIONI - ALIQUOTE IVA*/
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'VatType'],
        [ 
            'functionality_id' => Functionality::where('code', 'VatType')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-percent')->first()->id,
            'url'     => '/admin/crm/vattype',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Aliquote Iva',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'CatalogSettings')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CATALOGO - IMPOSTAZIONI - UNITA' DI MISURA*/
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'UnitMeasure'],
        [ 
            'functionality_id' => Functionality::where('code', 'UnitMeasure')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-balance-scale')->first()->id,
            'url'     => '/admin/crm/UnitMeasure',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Unità di misura',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'CatalogSettings')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CATALOGO - IMPOSTAZIONI - TIPOLOGIE CATEGORIE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'CategoryType'],
        [ 
            'functionality_id' => Functionality::where('code', 'Category')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-bookmark-o')->first()->id,
            'url'     => '/admin/crm/CategoryType',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Tipologie categorie',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 30,
            'menu_admin_father_id' => MenuAdmin::where('code', 'CatalogSettings')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CATALOGO - IMPOSTAZIONI - TIPOLOGIE PRODOTTI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'ItemsTypes'],
        [ 
            //'functionality_id' => Functionality::where('code', 'Category')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-th-list')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Tipologie prodotti',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 40,
            'menu_admin_father_id' => MenuAdmin::where('code', 'CatalogSettings')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU CATALOGO - IMPOSTAZIONI - SUPPORTI*/
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Supports'],
        [ 
            'functionality_id' => Functionality::where('code', 'Supports')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-cube')->first()->id,
            'url'     => '/admin/crm/supports',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Supporti',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 50,
            'menu_admin_father_id' => MenuAdmin::where('code', 'CatalogSettings')->first()->id,
            'created_id' => $idUser
        ]);

        
        /******************/
        /* TAB TABLE */
        /******************/

        $tabsTable = Tab::updateOrCreate(['functionality_id'   => Functionality::where('code', 'Supports')->first()->id, 'code' => 'Table'],['order' => 10, 'created_id' => $idUser]);
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'TableCheckBox'],['created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'CheckBox', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '13', 'table_order'=>'10', 'created_id' => $idUser]); 
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'SupportsFormatter'],['created_id' => $idUser, 'formatter' => 'SupportsFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 
        
        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(['functionality_id'   => Functionality::where('code', 'Supports')->first()->id, 'code' => 'Detail'],['order' => 20,'created_id' => $idUser]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);
        
        //descrizione supporto
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'colspan' => '12', 'created_id' => $idUser]); 
    
        //width supporto
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'width'],['field'=>'width', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Larghezza', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'20', 'colspan' => '3', 'created_id' => $idUser]); 
        
        //height supporto
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'height'],['field'=>'height', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Altezza', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'20', 'colspan' => '3', 'created_id' => $idUser]); 
        
        //depth supporto
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'depth'],['field'=>'depth', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Profondità', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'30', 'pos_y'=>'20', 'colspan' => '3', 'created_id' => $idUser]); 
        
        //um dimension supporto
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'um_dimension_id'],['field'=>'um_dimension_id', 'service' => 'unitMeasure', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Unità di misura Dimensioni', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '2', 'pos_x'=>'40', 'pos_y'=>'20', 'colspan' => '3', 'created_id' => $idUser]); 
        
        //area supporto
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'area'],['field'=>'area', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Area', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'30', 'colspan' => '4', 'created_id' => $idUser]); 
        
        //um dimension area
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'um_area_id'],['field'=>'um_area_id', 'service' => 'unitMeasure', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Unità di misura Area', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '2', 'pos_x'=>'20', 'pos_y'=>'30', 'colspan' => '4', 'created_id' => $idUser]); 
        
        //margins supporto
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'margins'],['field'=>'margins', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Margini', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'30', 'pos_y'=>'30', 'colspan' => '4', 'created_id' => $idUser]); 
        
        

 /*****************************************************************************/
        /* MENU CATALOGO - IMPOSTAZIONI - GENERATORE LISTINO*/
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'PriceListGenerator'],
        [
            'functionality_id' => Functionality::where('code', 'PriceListGenerator')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-list')->first()->id,
            'url'     => '/admin/crm/pricelistgenerator',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Generatore di Listino',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 60,
            'menu_admin_father_id' => MenuAdmin::where('code', 'CatalogSettings')->first()->id,
            'created_id' => $idUser
        ]);


        /******************/
        /* TAB TABLE PRICE LIST GENERATOR */
        /******************/

        $tabsTable = Tab::updateOrCreate(['functionality_id'   => Functionality::where('code', 'PriceListGenerator')->first()->id, 'code' => 'Table'],['order' => 10, 'created_id' => $idUser]);
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'TableCheckBox'],['created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'CheckBox', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '13', 'table_order'=>'10', 'created_id' => $idUser]); 
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'PriceListGeneratorFormatter'],['created_id' => $idUser, 'formatter' => 'PriceListGeneratorFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Regola', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'last_run'],['created_id' => $idUser, 'field'=> 'last_run', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ultima Esecuzione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'40', 'created_id' => $idUser]); 
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'state'],['created_id' => $idUser, 'field'=> 'state', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Stato', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'50', 'created_id' => $idUser]); 
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'StatePriceListGeneratorFormatter'],['created_id' => $idUser, 'formatter' => 'StatePriceListGeneratorFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'60', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'next_run'],['created_id' => $idUser, 'field'=> 'next_run', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Prossima Esecuzione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'70', 'created_id' => $idUser]); 
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'CheckActivePriceListGeneratorFormatter'],['created_id' => $idUser, 'formatter' => 'CheckActivePriceListGeneratorFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Attiva', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'80', 'created_id' => $idUser]); 

        /******************/
        /* TAB DETTAGLIO PRICE LIST GENERATOR */
        /******************/
        $tabsDetail = Tab::updateOrCreate(['functionality_id'   => Functionality::where('code', 'PriceListGenerator')->first()->id, 'code' => 'Detail'],['order' => 20,'created_id' => $idUser]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);
        
        //descrizione supporto
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'colspan' => '12', 'created_id' => $idUser]); 
    
        /*****************************************************************************/
        /*
        /*
        /* MENU ANAGRAFICHE */
        /*
        /*
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Anagrafiche'],
        [ 
            //'functionality_id' => XXX,
            'icon_id' => Icon::where('css', 'fa fa-address-card')->first()->id,
            'url'     => '',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Anagrafiche',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 30,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU ANAGRAFICHE - UTENTI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'UserSetting'],
        [ 
            'functionality_id' => Functionality::where('code', 'UserSetting')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-user-o')->first()->id,
            'url'     => '/admin/setting/user',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Utenti',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Anagrafiche')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU ANAGRAFICHE - CLIENTI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Customer'],
        [ 
            'functionality_id' => Functionality::where('code', 'Customer')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-id-card')->first()->id,
            'url'     => '/admin/crm/customer',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Clienti',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 'f',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Anagrafiche')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU ANAGRAFICHE - PERSONE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'People'],
        [ 
            'functionality_id' => Functionality::where('code', 'People')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-users')->first()->id,
            'url'     => '/admin/registries/People',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Persone',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 30,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Anagrafiche')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU ANAGRAFICHE - CONTATTI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Contact'],
        [ 
            'functionality_id' => Functionality::where('code', 'Contact')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-id-card')->first()->id,
            'url'     => '/admin/crm/contact',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Contatti',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 40,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Anagrafiche')->first()->id,
            'created_id' => $idUser
        ]);

        $tabsDetailDocumentsStatuses = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Contact')->first()->id, 
            'code' => 'DetailDocumentsStatuses'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        
        $tab =  Tab::where('functionality_id', Functionality::where('code', 'Contact')->first()->id)->where('code', 'Detail')->first();
        $field = Field::updateOrCreate(['tab_id'   => $tab->id, 'code' => 'group_sms_id'],['field'=>'group_sms_id', 'service' => 'groupSms', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Gruppo SMS', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '2', 'pos_x'=>'15', 'pos_y'=>'20', 'created_id' => $idUser]);

        /*****************************************************************************/
        /* MENU ANAGRAFICHE - AGENTI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Salesmen'],
        [ 
            'functionality_id' => Functionality::where('code', 'Salesman')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-suitcase')->first()->id,
            'url'     => '/admin/crm/salesman',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Agenti',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 50,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Anagrafiche')->first()->id,
            'created_id' => $idUser
        ]);

        
        /******************/
        /* TAB TABLE SALESMAN */
        /******************/

        $tabsTable = Tab::updateOrCreate(['functionality_id'   => Functionality::where('code', 'Salesman')->first()->id, 'code' => 'Table'],['order' => 10, 'created_id' => $idUser]);
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'TableCheckBox'],['created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'CheckBox', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '13', 'table_order'=>'10', 'created_id' => $idUser]); 
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'SalesmanFormatter'],['created_id' => $idUser, 'formatter' => 'SalesmanFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'code'],['created_id' => $idUser, 'field'=> 'code', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'business_name'],['created_id' => $idUser, 'field'=> 'business_name', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ragione Sociale', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'40', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'name'],['created_id' => $idUser, 'field'=> 'name', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Nome', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'50', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'surname'],['created_id' => $idUser, 'field'=> 'surname', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Cognome', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'60', 'created_id' => $idUser]); 
        
        /******************/
        /* TAB DETTAGLIO SALESMAN*/
        /******************/
        $tabsDetail = Tab::updateOrCreate(['functionality_id'   => Functionality::where('code', 'Salesman')->first()->id, 'code' => 'Detail'],['order' => 20,'created_id' => $idUser]); 
        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);
        
        //codice agente
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'code'],['field'=>'code', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'colspan' => '2', 'created_id' => $idUser]); 
    
        //business_name agente
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'business_name'],['field'=>'business_name', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ragione Sociale', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'10', 'colspan' => '4', 'created_id' => $idUser]); 
        
        //name agente
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'name'],['field'=>'name', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Nome', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'30', 'pos_y'=>'10', 'colspan' => '3', 'created_id' => $idUser]); 
        
        //surname agente
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'surname'],['field'=>'surname', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Cognome', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'40', 'pos_y'=>'10', 'colspan' => '3', 'created_id' => $idUser]); 
        
        //email agente
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'email'],['field'=>'email', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Email', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'20', 'colspan' => '3', 'created_id' => $idUser]);

        //phone number agente
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'phone_number'],['field'=>'phone_number', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Telefono', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'20', 'colspan' => '3', 'created_id' => $idUser]);

        //vat number agente
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'vat_number'],['field'=>'vat_number', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Partita IVA', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'30', 'pos_y'=>'20', 'colspan' => '3', 'created_id' => $idUser]);
        
        //fiscal code agente
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'fiscal_code'],['field'=>'fiscal_code', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice Fiscale', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'40', 'pos_y'=>'20', 'colspan' => '3', 'created_id' => $idUser]);

        //address agente
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'address'],['field'=>'address', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Indirizzo', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'30', 'colspan' => '4', 'created_id' => $idUser]);

        //city agente
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'city'],['field'=>'city', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Località', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'30', 'colspan' => '4', 'created_id' => $idUser]);
        
        //province agente
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'province'],['field'=>'province', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Provincia', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'30', 'pos_y'=>'30', 'colspan' => '2', 'created_id' => $idUser]);
        
        //CAP agente
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'postal_code'],['field'=>'postal_code', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'CAP', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'40', 'pos_y'=>'30', 'colspan' => '2', 'created_id' => $idUser]);

        //username agente
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'username'],['field'=>'username', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Username', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'40', 'colspan' => '6', 'created_id' => $idUser]);
        
        //username agente
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'password'],['field'=>'password', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Password', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'40', 'colspan' => '6', 'created_id' => $idUser]);
        
        //can_create_new_customer
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'can_create_new_customer'],['field'=>'can_create_new_customer', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Può creare un nuovo cliente', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'10', 'pos_y'=>'50', 'colspan' => '3', 'created_id' => $idUser]); 
        
        //can_edit_price
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'can_edit_price'],['field'=>'can_edit_price', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Può modificare il prezzo', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'20', 'pos_y'=>'50', 'colspan' => '3', 'created_id' => $idUser]); 
        
        //can_edit_discount
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'can_edit_discount'],['field'=>'can_edit_discount', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Può modificare lo sconto', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'30', 'pos_y'=>'50', 'colspan' => '3', 'created_id' => $idUser]);

        //can_edit_order
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'can_edit_order'],['field'=>'can_edit_order', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Può modificare l\'ordine', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'40', 'pos_y'=>'50', 'colspan' => '3', 'created_id' => $idUser]); 

        //can_active_customer
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'can_active_customer'],['field'=>'can_active_customer', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Può attivare i clienti', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'10', 'pos_y'=>'60', 'colspan' => '4', 'created_id' => $idUser]); 

        //view_all_customer
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'view_all_customer'],['field'=>'view_all_customer', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Associa a tutti i clienti', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'20', 'pos_y'=>'60', 'colspan' => '4', 'created_id' => $idUser]);

        //online
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'online'],['field'=>'online', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Online', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'30', 'pos_y'=>'60', 'colspan' => '4', 'created_id' => $idUser]);

        //view_availability
        /*$field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'view_availability'],['field'=>'view_availability', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Visualizza disponibilità', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'60', 'pos_y'=>'20', 'colspan' => '3', 'created_id' => $idUser]);*/

        /*******/
        /* GROUP NEWSLETTER */
        /*******/

        $menu = MenuAdmin::updateOrCreate(['code'   => 'GroupNewsletter'],
        [ 
            'functionality_id' => Functionality::where('code', 'GroupNewsletter')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-users')->first()->id,
            'url'     => '/admin/newsletter/groups',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Gruppi',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 60,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Anagrafiche')->first()->id,
            'created_id' => $idUser
        ]);

        $menu = MenuAdmin::updateOrCreate(['code'   => 'QueryGroupNewsletter'],
        [ 
            'functionality_id' => Functionality::where('code', 'QueryGroupNewsletter')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-object-group')->first()->id,
            'url'     => '/admin/newsletter/automaticgroups',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Gruppi Automatici',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 70,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Anagrafiche')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU ANAGRAFICHE - IMPOSTAZIONI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'RegistrySetting'],
        [ 
            //'functionality_id' => Functionality::where('code', 'Roles')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-cog')->first()->id,
            'url'     => '',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Impostazioni',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 80,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Anagrafiche')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU ANAGRAFICHE - IMPOSTAZIONI - GRUPPI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'UserGroups'],
        [ 
            //'functionality_id' => Functionality::where('code', 'Roles')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-users')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Gruppi',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 'f',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'RegistrySetting')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU ANAGRAFICHE - IMPOSTAZIONI - RUOLI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Roles'],
        [ 
            'functionality_id' => Functionality::where('code', 'Roles')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-user-circle')->first()->id,
            'url'     => '/admin/registries/Roles',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Ruoli',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 40,
            'menu_admin_father_id' => MenuAdmin::where('code', 'RegistrySetting')->first()->id,
            'created_id' => $idUser
        ]);

       /*****************************************************************************/
        /*
        /*
        /* MENU VENDITE */
        /*
        /*
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Sales'],
        [ 
            //'functionality_id' => XXX,
            'icon_id' => Icon::where('css', 'fa fa-credit-card')->first()->id,
            'url'     => '',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Vendite',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 40,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU VENDITE - ORDINI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Orders'],
        [ 
            'functionality_id' => Functionality::where('code', 'SalesOrder')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-file-text-o')->first()->id,
            'url'     => '/admin/sales/salesorders',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Ordini',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Sales')->first()->id,
            'created_id' => $idUser
        ]);


        // ------- CREATE FIELD AND TABS Sales Orders  -- //
        $tabsTableSalesOrder = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'SalesOrder')->first()->id, 
            'code' => 'TableSalesOrder'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);    
        $tabsTableSalessOrder = LanguageTab::updateOrCreate(['tab_id' => $tabsTableSalesOrder->id, 'language_id' => $idLanguage],['description' => 'TableSalesOrder','created_id' => $idUser]);     

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id,'code' => 'SalesOrderFormatter'],['created_id' => $idUser, 'formatter' => 'SalesOrderFormatter']);                             
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '14','table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id, 'code' => 'code'],[ 'created_id' => $idUser, 'field'=> 'code','data_type' => '1']);                               
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole], ['enabled' => 't','input_type'=> '0','table_order'=>'20', 'created_id' => $idUser]); 

         $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id, 'code' => 'date_order'],['created_id' => $idUser,'field'=> 'date_order', 'data_type' => '1']);                                       
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Data Ordine', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'30', 'created_id' => $idUser]);   

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id, 'code' => 'user_id'],['field' => 'user_id', 'created_id' => $idUser, 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Cliente', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'40', 'created_id' => $idUser]);    

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id, 'code' => 'note'],['field' => 'note','created_id' => $idUser, 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Note', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'50', 'created_id' => $idUser]);    

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id, 'code' => 'payment_type_id'],['field' => 'payment_type_id', 'created_id' => $idUser, 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Pagamento', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'60', 'created_id' => $idUser]);    

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id, 'code' => 'order_state_id'],['field' => 'order_state_id', 'created_id' => $idUser, 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Stato Ordine', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'70', 'created_id' => $idUser]);    

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id, 'code' => 'ChangeStatesOrderFormatter'],['field' => 'ChangeStatesOrderFormatter', 'created_id' => $idUser, 'formatter' => 'ChangeStatesOrderFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '14','table_order'=>'80', 'created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id, 'code' => 'net_amount'],['field' => 'net_amount','created_id' => $idUser, 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Imponibile', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'90', 'created_id' => $idUser]);    

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id, 'code' => 'total_amount'],['field' => 'total_amount', 'created_id' => $idUser, 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Totale', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'100', 'created_id' => $idUser]);    
     
        $tabsDetailSalesOrder = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'SalesOrder')->first()->id, 
            'code' => 'DetailSalesOrder'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);    
        $tabsDetailSalessOrder = LanguageTab::updateOrCreate(['tab_id' => $tabsDetailSalesOrder->id,'language_id' => $idLanguage],['description' => 'DetailSalesOrder','created_id' => $idUser]);  
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetailSalesOrder->id, 'code' => 'total_amount'],['created_id' => $idUser,'field'=> 'total_amount', 'data_type' => '1']);                                      
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '0','pos_x'=> '10','pos_y'=> '10','created_id' => $idUser]);    
        // -- END CREATE FIELD AND TABS Sales Orders -- //

        /*****************************************************************************/
        /* MENU VENDITE - PREVENTIVI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Offers'],
        [ 
            //'functionality_id' => Functionality::where('code', 'Roles')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-file-o')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Preventivi',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Sales')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU VENDITE - CARRELLI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Carts'],
        [ 
            'functionality_id' => Functionality::where('code', 'SalesCarts')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-shopping-cart')->first()->id,
            'url'     => '/admin/sales/salescarts',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Carrelli',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 30,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Sales')->first()->id,
            'created_id' => $idUser
        ]);

        $tabsTableSalesOrder = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'SalesCarts')->first()->id, 
            'code' => 'TableSalesCarts'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);    
        $tabsTableSalessOrder = LanguageTab::updateOrCreate(['tab_id' => $tabsTableSalesOrder->id, 'language_id' => $idLanguage],['description' => 'TableSalesCarts','created_id' => $idUser]);     

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id,'code' => 'SalesCartsFormatter'],['created_id' => $idUser]);                             
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '0','table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id, 'code' => 'code'],['created_id' => $idUser, 'field'=> 'code','data_type' => '1']);                               
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole], ['enabled' => 't','input_type'=> '15','table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id, 'code' => 'date_order'],['created_id' => $idUser,'field'=> 'dateOrder', 'data_type' => '1']);                                       
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Data Ordine', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'30', 'created_id' => $idUser]);      

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id, 'code' => 'userId'],['created_id' => $idUser, 'field'=> 'userId','data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Cliente', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'50', 'created_id' => $idUser]);    

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id, 'code' => 'orderState'],['created_id' => $idUser, 'field'=> 'orderState','data_type' => '1']);
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Stato Carrello', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'80', 'created_id' => $idUser]);    

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id, 'code' => 'netAmount'],['created_id' => $idUser, 'field'=> 'netAmount','data_type' => '1']);
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Imponibile', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'90', 'created_id' => $idUser]);    

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableSalesOrder->id, 'code' => 'totalAmount'],['created_id' => $idUser, 'field'=> 'totalAmount','data_type' => '1']);
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Totale', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '15','table_order'=>'100', 'created_id' => $idUser]);

        /*****************************************************************************/
        /* MENU VENDITE - REGOLE DEL CARRELLO */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'CartRules'],
        [ 
            'functionality_id' => Functionality::where('code', 'CartRules')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-cart-plus')->first()->id,
            'url'     => '/admin/sales/cartrules',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Regole del carrello',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 40,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Sales')->first()->id,
            'created_id' => $idUser
        ]);


        /*****************************************************************************/
        /* MENU SALES - IMPOSTAZIONI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'SettingSales'],
        [ 
            'icon_id' => Icon::where('css', 'fa fa-cog')->first()->id,
            'url'     => '',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Impostazioni',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 60,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Sales')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU SALES - IMPOSTAZIONI - STATI DOCUMENTO */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'DocumentStates'],
        [ 
            'functionality_id' => Functionality::where('code', 'DocumentStatuses')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-clipboard')->first()->id,
            'url'     => '/admin/sales/statesdocuments',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Stati Documenti',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'SettingSales')->first()->id,
            'created_id' => $idUser
        ]);

        // ------- CREATE FIELD AND TABS DOCUMENTS STATUSES -- //
         $tabsTableDocumentsStatuses = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'DocumentStatuses')->first()->id, 
            'code' => 'TableDocumentsStatuses'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);    
        $tabsTableDocumentssStatuses = LanguageTab::updateOrCreate(['tab_id' => $tabsTableDocumentsStatuses->id, 'language_id' => $idLanguage],['description' => 'TableDocumentsStatuses','created_id' => $idUser]);  

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableDocumentsStatuses->id, 'code' => 'TableCheckBox'],['created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage], ['description' => 'CheckBox', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '13','table_order'=>'10',  'created_id' => $idUser]);    

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableDocumentsStatuses->id,'code' => 'StatesOrderDocumentsFormatter'],['created_id' => $idUser, 'formatter' => 'StatesOrderDocumentsFormatter']);                             
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '14','table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableDocumentsStatuses->id, 'code' => 'code'],['created_id' => $idUser, 'field'=> 'code','data_type' => '1']);                               
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole], ['enabled' => 't','input_type'=> '0','table_order'=>'30', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableDocumentsStatuses->id, 'code' => 'description'],['created_id' => $idUser,'field'=> 'description', 'data_type' => '1']);                                       
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '0','table_order'=>'40', 'created_id' => $idUser]);   

        $field = Field::updateOrCreate(['tab_id'   => $tabsTableDocumentsStatuses->id, 'code' => 'HtmlViewFormatter'],['created_id' => $idUser, 'formatter' => 'HtmlViewFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '14','table_order'=>'50', 'created_id' => $idUser]);    


        $tabsDetailDocumentsStatuses = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'DocumentStatuses')->first()->id, 
            'code' => 'DetailDocumentsStatuses'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsDetailsDocumentsStatuses = LanguageTab::updateOrCreate(['tab_id' => $tabsDetailDocumentsStatuses->id,'language_id' => $idLanguage],['description' => 'DetailDocumentsStatuses','created_id' => $idUser]);  
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetailDocumentsStatuses->id, 'code' => 'code'],['created_id' => $idUser,'field'=> 'code', 'data_type' => '1']);                                      
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '0','pos_x'=> '10','pos_y'=> '10','created_id' => $idUser]);    
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetailDocumentsStatuses->id, 'code' => 'description'],['created_id' => $idUser,'field'=> 'description','data_type' => '1']);                                    
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '0','pos_x'=> '10','pos_y'=> '20','created_id' => $idUser]);      
       
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetailDocumentsStatuses->id, 'code' => 'html'],['created_id' => $idUser,'field'=> 'html','data_type' => '1']);                                    
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Html Visualizzato', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id,'role_id' => $idRole],['enabled' => 't','input_type'=> '17','pos_x'=> '10','pos_y'=> '30','created_id' => $idUser]);    
        // -- END CREATE FIELD AND TABS DOCUMENTS STATUSES -- //   

        // -------------------------------- // 
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'CartRules')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);

        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'TableCheckBox'],['created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'CheckBox', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '13', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'CartRulesFormatter'],['created_id' => $idUser, 'formatter' => 'CartRulesFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'name'],['created_id' => $idUser, 'field'=> 'name', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Nome Regola', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'40', 'created_id' => $idUser]); 

        /********************/
        /* TAB INFORMAZIONI */
        /********************/
        $tabInformazioni = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'CartRules')->first()->id, 
            'code' => 'Informazioni'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabInformazioni->id,'language_id' => $idLanguage],['description' => 'Informazioni','created_id' => $idUser]);

        //nome
        $field = Field::updateOrCreate(['tab_id'   => $tabInformazioni->id, 'code' => 'name'],['field'=>'name', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Nome', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 
    
        //descrizione
        $field = Field::updateOrCreate(['tab_id'   => $tabInformazioni->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'10', 'created_id' => $idUser]); 
 
        //codice voucher
        $field = Field::updateOrCreate(['tab_id'   => $tabInformazioni->id, 'code' => 'voucher_code'],['field'=>'voucher_code', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice Voucher', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'20', 'created_id' => $idUser]); 

        //tipo voucher
        $field = Field::updateOrCreate(['tab_id'   => $tabInformazioni->id, 'code' => 'voucher_type_id'],['field'=>'voucher_type_id', 'service' => 'vouchertype', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Tipo voucher', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '2', 'pos_x'=>'20', 'pos_y'=>'20', 'created_id' => $idUser]); 
        
        //disponibilità
        $field = Field::updateOrCreate(['tab_id'   => $tabInformazioni->id, 'code' => 'availability'],['field'=>'availability', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Disponibilità', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'10', 'pos_y'=>'30', 'created_id' => $idUser]); 

        //disponibilità residua
        $field = Field::updateOrCreate(['tab_id'   => $tabInformazioni->id, 'code' => 'availability_per_user'],['field'=>'availability_per_user', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Disponibilità per utente', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'20', 'pos_y'=>'30', 'created_id' => $idUser]); 
        
        //disponibilità per utente
        $field = Field::updateOrCreate(['tab_id'   => $tabInformazioni->id, 'code' => 'availability_residual'],['field'=>'availability_residual', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Disponibilità residua', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'30', 'pos_y'=>'30', 'created_id' => $idUser]); 
  
        //valido dal
        $field = Field::updateOrCreate(['tab_id'   => $tabInformazioni->id, 'code' => 'enabled_from'],['field'=>'enabled_from', 'data_type'=> '2', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Valido dal', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'pos_x'=>'10', 'pos_y'=>'40', 'created_id' => $idUser]); 

        //valido al
        $field = Field::updateOrCreate(['tab_id'   => $tabInformazioni->id, 'code' => 'enabled_to'],['field'=>'enabled_to', 'data_type'=> '2', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Valido al', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'pos_x'=>'20', 'pos_y'=>'40', 'created_id' => $idUser]); 

        //priorità
        $field = Field::updateOrCreate(['tab_id'   => $tabInformazioni->id, 'code' => 'priority'],['field'=>'priority', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Priorità', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'10', 'pos_y'=>'50', 'created_id' => $idUser]); 
 
        //online si/no
        $field = Field::updateOrCreate(['tab_id'   => $tabInformazioni->id, 'code' => 'Online'],['field'=>'online', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Online', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'20', 'pos_y'=>'50', 'created_id' => $idUser]); 
    
        //cumulativo
        $field = Field::updateOrCreate(['tab_id'   => $tabInformazioni->id, 'code' => 'exclude_other_rules'],['field'=>'exclude_other_rules', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Escludi altre regole', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'30', 'pos_y'=>'50', 'created_id' => $idUser]); 
 
        /********************/
        /* TAB CONDIZIONI */
        /********************/
        $tabCondizioni = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'CartRules')->first()->id, 
            'code' => 'Condizioni'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabCondizioni->id,'language_id' => $idLanguage],['description' => 'Condizioni','created_id' => $idUser]);
    
        //Applica a tutti i clienti o seleziona
        $field = Field::updateOrCreate(['tab_id'   => $tabCondizioni->id, 'code' => 'apply_to_all_customers'],['field'=>'apply_to_all_customers', 'data_type'=> '0', 'created_id' => $idUser, 'arrayRadioButton'=> '[{"id":1, "text":"Tutti","name":"applicaatuttiiclienti", "value" : "1"}, {"id":0, "text":"Seleziona","name":"applicaatuttiiclienti", "value" : "0"}]']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Clienti', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '6', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 

        //Applica a tutti i gruppi o seleziona
        $field = Field::updateOrCreate(['tab_id'   => $tabCondizioni->id, 'code' => 'apply_to_all_groups'],['field'=>'apply_to_all_groups', 'data_type'=> '0', 'created_id' => $idUser, 'arrayRadioButton'=> '[{"id":1, "text":"Tutti","name":"applicaatuttiigruppi", "value" : "1"}, {"id":0, "text":"Seleziona","name":"applicaatuttiigruppi", "value" : "0"}]']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Gruppi', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '6', 'pos_x'=>'10', 'pos_y'=>'20', 'created_id' => $idUser]); 

        //Applica a tutti i vettori o seleziona
        $field = Field::updateOrCreate(['tab_id'   => $tabCondizioni->id, 'code' => 'apply_to_all_carriers'],['field'=>'apply_to_all_carriers', 'data_type'=> '0', 'created_id' => $idUser, 'arrayRadioButton'=> '[{"id":1, "text":"Tutti","name":"applicaatuttiivettori", "value" : "1"}, {"id":0, "text":"Seleziona","name":"applicaatuttiivettori", "value" : "0"}]']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Vettori', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '6', 'pos_x'=>'10', 'pos_y'=>'30', 'created_id' => $idUser]); 

        //Applica a tutti i prodotti o seleziona
        $field = Field::updateOrCreate(['tab_id'   => $tabCondizioni->id, 'code' => 'apply_to_all_items'],['field'=>'apply_to_all_items', 'data_type'=> '0', 'created_id' => $idUser, 'arrayRadioButton'=> '[{"id":1, "text":"Tutti","name":"applicaatuttiiprodotti", "value" : "1"}, {"id":0, "text":"Seleziona","name":"applicaatuttiiprodotti", "value" : "0"}]']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Prodotti', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '6', 'pos_x'=>'10', 'pos_y'=>'40', 'created_id' => $idUser]); 

        //Applica a tutte le categorie prodotto o seleziona
        $field = Field::updateOrCreate(['tab_id'   => $tabCondizioni->id, 'code' => 'apply_to_all_categories_items'],['field'=>'apply_to_all_categories_items', 'data_type'=> '0', 'created_id' => $idUser, 'arrayRadioButton'=> '[{"id":1, "text":"Tutte","name":"applicaatuttecategorie", "value" : "1"}, {"id":0, "text":"Seleziona","name":"applicaatuttecategorie", "value" : "0"}]']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Categorie', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '6', 'pos_x'=>'10', 'pos_y'=>'50', 'created_id' => $idUser]); 

        //Applica a tutti i produttori
        $field = Field::updateOrCreate(['tab_id'   => $tabCondizioni->id, 'code' => 'apply_to_all_producers'],['field'=>'apply_to_all_producers', 'data_type'=> '0', 'created_id' => $idUser, 'arrayRadioButton'=> '[{"id":1, "text":"Tutti","name":"applicaatuttiiproduttori", "value" : "1"}, {"id":0, "text":"Seleziona","name":"applicaatuttiiproduttori", "value" : "0"}]']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Produttori', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '6', 'pos_x'=>'10', 'pos_y'=>'55', 'created_id' => $idUser]);//Applica a tutti i produttori
        
        //Applica a tutti i fornitori
        $field = Field::updateOrCreate(['tab_id'   => $tabCondizioni->id, 'code' => 'apply_to_all_suppliers'],['field'=>'apply_to_all_suppliers', 'data_type'=> '0', 'created_id' => $idUser, 'arrayRadioButton'=> '[{"id":1, "text":"Tutti","name":"applicaatuttiifornitori", "value" : "1"}, {"id":0, "text":"Seleziona","name":"applicaatuttiifornitori", "value" : "0"}]']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Fornitori', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '6', 'pos_x'=>'10', 'pos_y'=>'58', 'created_id' => $idUser]);  

        //Quantità minima
        $field = Field::updateOrCreate(['tab_id'   => $tabCondizioni->id, 'code' => 'minimum_quantity'],['field'=>'minimum_quantity', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Quantità minima', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'10', 'pos_y'=>'60', 'created_id' => $idUser]); 

        //Quantità massima
        $field = Field::updateOrCreate(['tab_id'   => $tabCondizioni->id, 'code' => 'maximum_quantity'],['field'=>'maximum_quantity', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Quantità massima', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'20', 'pos_y'=>'60', 'created_id' => $idUser]); 

        //Importo minimo ordine
        $field = Field::updateOrCreate(['tab_id'   => $tabCondizioni->id, 'code' => 'minimum_order_amount'],['field'=>'minimum_order_amount', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Importo minimo ordine', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'30', 'pos_y'=>'60', 'created_id' => $idUser]); 

        //tasse escluse
        $field = Field::updateOrCreate(['tab_id'   => $tabCondizioni->id, 'code' => 'taxes_excluded'],['field'=>'taxes_excluded', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Tasse escluse', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'10', 'pos_y'=>'70', 'created_id' => $idUser]); 

        //spedizione esclusa
        $field = Field::updateOrCreate(['tab_id'   => $tabCondizioni->id, 'code' => 'shipping_excluded'],['field'=>'shipping_excluded', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Spedizione esclusa', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'20', 'pos_y'=>'70', 'created_id' => $idUser]); 

        /********************/
        /* TAB AZIONI */
        /********************/
        $tabAzioni = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'CartRules')->first()->id, 
            'code' => 'Azioni'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabAzioni->id,'language_id' => $idLanguage],['description' => 'Azioni','created_id' => $idUser]);
    
        //Tipo di sconto - Radio button
        $field = Field::updateOrCreate(['tab_id'   => $tabAzioni->id, 'code' => 'discount_type'],['field'=>'discount_type', 'data_type'=> '0', 'created_id' => $idUser, 'arrayRadioButton'=> '[{"id":1, "text":"%","name":"applicaSconto", "value" : "1"}, {"id":2, "text":"Importo","name":"applicaSconto", "value" : "2"}, {"id":0, "text":"No","name":"applicaSconto", "value" : "0"}]']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Applica sconto', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '6', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 

        //Valore sconto
        $field = Field::updateOrCreate(['tab_id'   => $tabAzioni->id, 'code' => 'discount_value'],['field'=>'discount_value', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Valore sconto', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'20', 'pos_y'=>'10', 'created_id' => $idUser]); 
  
        //spese spedizioni gratis
        $field = Field::updateOrCreate(['tab_id'   => $tabAzioni->id, 'code' => 'free_shipping'],['field'=>'free_shipping', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Spedizione gratuita', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'30', 'pos_y'=>'10', 'created_id' => $idUser]); 
  
        //Applica lo sconto a - Radio button
        $field = Field::updateOrCreate(['tab_id'   => $tabAzioni->id, 'code' => 'apply_discount_to'],['field'=>'apply_discount_to', 'data_type'=> '0', 'created_id' => $idUser, 'arrayRadioButton'=> '[{"id":0, "text":"Ordine","name":"applicaScontoA", "value" : "0"}, {"id":1, "text":"Prodotti delle condizioni","name":"applicaScontoA", "value" : "1"}, {"id":2, "text":"Prodotto meno caro","name":"applicaScontoA", "value" : "2"}, {"id":3, "text":"Prodotti specifici","name":"applicaScontoA", "value" : "3"}]']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Applica lo sconto a', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '6', 'pos_x'=>'10', 'pos_y'=>'20', 'created_id' => $idUser]); 

        //spese spedizioni gratis
        $field = Field::updateOrCreate(['tab_id'   => $tabAzioni->id, 'code' => 'add_to_cart_automatically'],['field'=>'add_to_cart_automatically', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Aggiungi al carrello in automatico', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'10', 'pos_y'=>'30', 'created_id' => $idUser]); 
  
        /*****************************************************************************/
        /* MENU VENDITE - SPESE DI SPEDIZIONE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'ShippingCosts'],
        [ 
            'functionality_id' => Functionality::where('code', 'Carriers')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-truck')->first()->id,
            'url'     => '/admin/crm/Carriers',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Spese di spedizione',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 50,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Sales')->first()->id,
            'created_id' => $idUser
        ]);

             $menu = MenuAdmin::updateOrCreate(['code'   => 'Carriage'],
        [ 
            'functionality_id' => Functionality::where('code', 'Carriage')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-random')->first()->id,
            'url'     => '/admin/crm/Carriage',
            'created_id' => $idUser
        ]);

           $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Porti',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 60,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Sales')->first()->id,
            'created_id' => $idUser
        ]);

        /********************/
        /* TAB TABLE */
        /********************/
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Carriers')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);

        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'TableCheckBox'],['created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'CheckBox', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '13', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'CarriersFormatter'],['created_id' => $idUser, 'formatter' => 'CarriersFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'20', 'created_id' => $idUser]); 
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'name'],['created_id' => $idUser, 'field'=> 'name', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Nome', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'40', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'order'],['created_id' => $idUser, 'field'=> 'order', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ordinamento', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'50', 'created_id' => $idUser]); 
        
        /********************/
        /* TAB DETTAGLIO */
        /********************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Carriers')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);
        
        //nome carriers
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'name'],['field'=>'name', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Nome', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'colspan' => '4', 'created_id' => $idUser]); 
 
        //description carriers
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'20', 'colspan' => '10', 'created_id' => $idUser]); 
        
        //logo carriers
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'img'],['field'=>'img', 'data_type'=> '1', 'created_id' => $idUser]);
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Logo', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '18', 'pos_x'=>'10', 'pos_y'=>'30', 'colspan' => '6', 'created_id' => $idUser]); 
        
        //iva carriers
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'vat_type_id'],['field'=>'vat_type_id', 'data_type'=> '1', 'created_id' => $idUser, 'service' => 'vatType']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice IVA', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '2', 'pos_x'=>'10', 'pos_y'=>'40', 'colspan' => '3', 'created_id' => $idUser]); 
        
        //tracking url carriers
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'tracking_url'],['field'=>'tracking_url', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'URL Tracking', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'50', 'colspan' => '8', 'created_id' => $idUser]); 
        
        //assicurazione carriers
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'insurance'],['field'=>'insurance', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Assicurazione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'10', 'pos_y'=>'60', 'colspan' => '2', 'created_id' => $idUser]); 
        
        //ordinamento carriers
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'order'],['field'=>'order', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ordinamento', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'10', 'pos_y'=>'70', 'colspan' => '2', 'created_id' => $idUser]); 
        

        /*****************************************************************************/
        /* MENU VENDITE - VOUCHER */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Vouchers'],
        [ 
            'functionality_id' => Functionality::where('code', 'CartRulesVoucher')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-gift')->first()->id,
            'url'     => '/admin/sales/voucher',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Voucher',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 60,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Sales')->first()->id,
            'created_id' => $idUser
        ]);

        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'CartRulesVoucher')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'TableCheckBox'],['created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'CheckBox', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '13', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'VoucherFormatter'],['created_id' => $idUser, 'formatter' => 'VoucherFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'voucher_code'],['created_id' => $idUser, 'field'=> 'voucher_code', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]);
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'40', 'created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'enabled_from'],['created_id' => $idUser, 'field'=> 'enabled_from', 'data_type' => '2']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Data Inizio Validità', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'table_order'=>'50', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'enabled_to'],['created_id' => $idUser, 'field'=> 'enabled_to', 'data_type' => '2']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Data Fine Validità', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'table_order'=>'60', 'created_id' => $idUser]); 

        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'CartRulesVoucher')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);

        //codice voucher
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'voucher_code'],['field'=>'voucher_code', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 

        //descrizione voucher
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'10', 'created_id' => $idUser]);

        //codice online si/no
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'online'],['field'=>'online', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Online', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'30', 'pos_y'=>'10', 'created_id' => $idUser]);
 
        //tipo voucher
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'voucher_type_id'],['field'=>'voucher_type_id', 'service' => 'vouchertype', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Tipo voucher', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '2', 'pos_x'=>'10', 'pos_y'=>'20', 'created_id' => $idUser]); 
 
        //valido dal
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'enabled_from'],['field'=>'enabled_from', 'data_type'=> '2', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Valido dal', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'pos_x'=>'20', 'pos_y'=>'20', 'created_id' => $idUser]); 
 
        //valido al
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'enabled_to'],['field'=>'enabled_to', 'data_type'=> '2', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Valido al', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'pos_x'=>'30', 'pos_y'=>'20', 'created_id' => $idUser]); 

        //prezzo minimo validità
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'minimum_order_amount'],['field'=>'minimum_order_amount', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Prezzo minimo', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'10', 'pos_y'=>'30', 'created_id' => $idUser]); 

        //sconto valore
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'discount_value'],['field'=>'discount_value', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Sconto in valore', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'20', 'pos_y'=>'30', 'created_id' => $idUser]); 

        //disponibilità
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'availability'],['field'=>'availability', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Disponibilità', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'30', 'pos_y'=>'30', 'created_id' => $idUser]); 

        //disponibilità residua
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'availability_residual'],['field'=>'availability_residual', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Disponibilità residua', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'40', 'pos_y'=>'30', 'created_id' => $idUser]); 

            
        /*****************************************************************************/
        /* MENU VENDITE - REPORT */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'SalesReports'],
        [ 
            //'functionality_id' => Functionality::where('code', 'Roles')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-area-chart')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Report',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 900,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Sales')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /*
        /*
        /* MENU MARKETING */
        /*
        /*
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Marketing'],
        [ 
            //'functionality_id' => XXX,
            'icon_id' => Icon::where('css', 'fa fa-bullhorn')->first()->id,
            'url'     => '',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Marketing',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 50,
            'created_id' => $idUser
        ]);
        /*****************************************************************************/
        /* MENU MARKETING - RICHIESTE DA FORM */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'RequestContacts'],
        [ 
            'functionality_id' => Functionality::where('code', 'RequestContacts')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-ticket')->first()->id,
            'url'     => '/admin/crm/requestcontact',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Richieste da form',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Marketing')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU MARKETING - RICHIESTE DA FORM BUILDER */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'RequestContactFormBuilder'],
        [ 
            'functionality_id' => Functionality::where('code', 'RequestContactFormBuilder')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-ticket')->first()->id,
            'url'     => '/admin/crm/requestcontactformbuilder',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Richieste da form builder',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 15,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Marketing')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* TAB TABLE */
        /*****************************************************************************/
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'RequestContactFormBuilder')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
            
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'RequestContactFormBuilderFormatter'],['created_id' => $idUser, 'formatter' => 'RequestContactFormBuilderFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'20', 'created_id' => $idUser]);
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'date'],['created_id' => $idUser, 'field'=> 'date', 'data_type' => '2']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Data', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'table_order'=>'30', 'created_id' => $idUser]); 
        

        /*****************************************************************************/
        /* MENU MARKETING - NEWSLETTER */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Newsletter'],
        [ 
            //'functionality_id' => Functionality::where('code', 'RequestContacts')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-envelope-o')->first()->id,
            'url'     => '',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Newsletter',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Marketing')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU MARKETING - NEWSLETTER - CAMPAGNE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'NewsletterCampaign'],
        [ 
            'functionality_id' => Functionality::where('code', 'Newsletter')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-list-ol')->first()->id,
            'url'     => '/admin/newsletter/campaign',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Campagne',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Newsletter')->first()->id,
            'created_id' => $idUser
        ]);



        //
        /* $menu = MenuAdmin::updateOrCreate(['code'   => 'NewsletterCampaign1'],
        [ 
            'functionality_id' => Functionality::where('code', 'Newsletter1')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-list-ol')->first()->id,
            'url'     => '/admin/newsletter/campaign1',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Campagne',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Newsletter1')->first()->id,
            'created_id' => $idUser
        ]);*/
        //

       /*$menu = MenuAdmin::updateOrCreate(['code'   => 'QueryGroupNewsletter'],
        [ 
            'functionality_id' => Functionality::where('code', 'QueryGroupNewsletter')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-object-group')->first()->id,
            'url'     => '/admin/newsletter/querygroupnewsletter',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Query Gruppi Newsletter',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Newsletter1')->first()->id,
            'created_id' => $idUser
        ]);*/



        /*****************************************************************************/
        /* MENU MARKETING - NEWSLETTER - GRUPPI NEWSLETTER */
        /*****************************************************************************/
     

        /*****************************************************************************/
        /* MENU MARKETING - AUTOMATION */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Automation'],
        [ 
            'functionality_id' => Functionality::where('code', 'Automation')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-cogs')->first()->id,
            'url'     => '/admin/crm/automation',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Automation',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 30,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Marketing')->first()->id,
            'created_id' => $idUser
        ]);

        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Automation')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'TableCheckBox'],['created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'CheckBox', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '13', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'AutomationFormatter'],['created_id' => $idUser, 'formatter' => 'AutomationFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'from'],['created_id' => $idUser, 'field'=> 'from', 'data_type' => '2']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Data Inizio', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'table_order'=>'40', 'created_id' => $idUser]); 
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'to'],['created_id' => $idUser, 'field'=> 'to', 'data_type' => '2']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Data Fine', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'table_order'=>'50', 'created_id' => $idUser]); 
    
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'AutomationOnlineFormatter'],['created_id' => $idUser, 'formatter' => 'AutomationOnlineFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Online', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'60', 'created_id' => $idUser]); 

        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Automation')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]);
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);

        //description
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 

        //data inizio
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'from'],['field'=>'from', 'data_type'=> '2', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Data inizio', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'pos_x'=>'10', 'pos_y'=>'20', 'created_id' => $idUser]); 

        //data fine
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'to'],['field'=>'to', 'data_type'=> '2', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Data fine', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'pos_x'=>'20', 'pos_y'=>'20', 'created_id' => $idUser]); 
        
        //online
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'online'],['field'=>'online', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Online', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'30', 'pos_y'=>'20', 'created_id' => $idUser]); 


        /*****************************************************************************/
        /* MENU MARKETING - ADVERTISING */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Advertising'],
        [ 
            'icon_id' => Icon::where('css', 'fa fa-eur')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Advertising',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 40,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Marketing')->first()->id,
            'created_id' => $idUser
        ]);



        /******************/
        /* TAB TABLE */
        /******************/
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'AdvertisingArea')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'AdvertisingAreaFormatter'],['created_id' => $idUser, 'formatter' => 'AdvertisingAreaFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'code'],['created_id' => $idUser, 'field'=> 'code', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 

        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'AdvertisingArea')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);

        //codice
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'code'],['field'=>'code', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 

        //active
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'active'],['field'=>'active', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Attiva', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'20', 'pos_y'=>'10', 'created_id' => $idUser]); 
    
        //descrizione
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'20', 'created_id' => $idUser]); 
    
        //Larghezza banner
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'width_pixels'],['field'=>'width_pixels', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Larghezza banner', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'10', 'pos_y'=>'30', 'created_id' => $idUser]); 
    
        //Altezza banner
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'height_pixels'],['field'=>'height_pixels', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Altezza banner', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'20', 'pos_y'=>'30', 'created_id' => $idUser]); 
   

         /*****************************************************************************/
         /* MENU MARKETING - ADVERTISING - BANNER */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'AdvertisingBanner'],
        [ 
            'functionality_id' => Functionality::where('code', 'AdvertisingBanner')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-list-alt')->first()->id,
            'url'     => '/admin/advertising/advertisingBanner',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Banner',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Advertising')->first()->id,
            'created_id' => $idUser
        ]);

        /******************/
        /* TAB TABLE */
        /******************/
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'AdvertisingBanner')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'AdvertisingBannerFormatter'],['created_id' => $idUser, 'formatter' => 'AdvertisingBannerFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'advertisingArea'],['created_id' => $idUser, 'field'=> 'advertising_area', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Area', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 
    
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'start_date'],['created_id' => $idUser, 'field'=> 'start_date', 'data_type' => '2']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Data Inizio', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'table_order'=>'40', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'stop_date'],['created_id' => $idUser, 'field'=> 'stop_date', 'data_type' => '2']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Data Fine', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'table_order'=>'50', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'weight'],['created_id' => $idUser, 'field'=> 'weight', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Peso', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'60', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'active'],['created_id' => $idUser, 'field'=> 'active', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Attivo', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'70', 'created_id' => $idUser]); 

        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'AdvertisingBanner')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);

        //area
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'advertising_area_id'],['field'=>'advertising_area_id', 'service' => 'advertisingarea', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Area', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '2', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 
        
        //descrizione
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'10', 'created_id' => $idUser]); 
            
        //valido dal
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'start_date'],['field'=>'start_date', 'data_type'=> '2', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Valido dal', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'pos_x'=>'10', 'pos_y'=>'20', 'created_id' => $idUser]); 

        //valido al
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'stop_date'],['field'=>'stop_date', 'data_type'=> '2', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Valido al', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'pos_x'=>'20', 'pos_y'=>'20', 'created_id' => $idUser]); 
 
        //image_file
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'image_file'],['field'=>'image_file', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Immagine', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '18', 'pos_x'=>'10', 'pos_y'=>'30', 'created_id' => $idUser]); 

        //descrizione
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'image_alt'],['field'=>'image_alt', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Testo alternativo IMG', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'30', 'created_id' => $idUser]); 

        //descrizione
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'link_url'],['field'=>'link_url', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'URL Img.', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'40', 'created_id' => $idUser]); 
        
        //target_blank
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'target_blank'],['field'=>'target_blank', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Apri in una nuova finestra', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'20', 'pos_y'=>'40', 'created_id' => $idUser]); 
 
        //Script JS
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'script_Js'],['field'=>'script_Js', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Script JS o codice HTML', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'50', 'created_id' => $idUser]); 

        //Numero massimo impressions
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'maximum_impressions'],['field'=>'maximum_impressions', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Impressions Massime', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'10', 'pos_y'=>'60', 'created_id' => $idUser]); 

        //Impressions progressive
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'progressive_impressions'],['field'=>'progressive_impressions', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Impressions Progressive', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'20', 'pos_y'=>'60', 'created_id' => $idUser]); 

        //Peso
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'weight'],['field'=>'weight', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Peso', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'30', 'pos_y'=>'60', 'created_id' => $idUser]); 

        //attivo
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'active'],['field'=>'active', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Attivo', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'10', 'pos_y'=>'70', 'created_id' => $idUser]); 
    
        //attiva statistiche
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'active_reports'],['field'=>'active_reports', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Attiva statistiche', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'20', 'pos_y'=>'70', 'created_id' => $idUser]); 

        /*****************************************************************************/
         /* MENU MARKETING - ADVERTISING - AMAZON TEMPLATE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'AmazonTemplate'],
        [ 
            'functionality_id' => Functionality::where('code', 'AmazonTemplate')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-delicious')->first()->id,
            'url'     => '/admin/advertising/amazonTemplate',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Teamplates Amazon',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 30,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Advertising')->first()->id,
            'created_id' => $idUser
        ]);

        /******************/
        /* TAB TABLE */
        /******************/
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'AmazonTemplate')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'AmazonTemplateFormatter'],['created_id' => $idUser, 'formatter' => 'AmazonTemplateFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'code'],['created_id' => $idUser, 'field'=> 'code', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'shortcode_example'],['created_id' => $idUser, 'field'=> 'shortcode_example', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Shortcode esempio', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'40', 'created_id' => $idUser]); 

        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'AmazonTemplate')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);

        //codice
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'code'],['field'=>'code', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Codice', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 

        //shortcode esempio
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'shortcode_example'],['field'=>'shortcode_example', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Shortcode esempio', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'10', 'created_id' => $idUser]); 

        //descrizione
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'20', 'created_id' => $idUser]); 

        //content
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'content'],['field'=>'content', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'HTML', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '17', 'pos_x'=>'10', 'pos_y'=>'30', 'created_id' => $idUser]); 
        
        /*****************************************************************************/
        /* MENU MARKETING - AFFILIAZIONI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Affiliates'],
        [ 
            //'functionality_id' => Functionality::where('code', 'RequestContacts')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-handshake-o')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Affiliazioni',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 50,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Marketing')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU MARKETING - REPORT */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'MarketingReports'],
        [ 
            //'functionality_id' => Functionality::where('code', 'RequestContacts')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-area-chart')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Report',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 900,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Marketing')->first()->id,
            'created_id' => $idUser
        ]);



      /*****************************************************************************/
        /*
        /*
        /* MENU BOOKING */
        /*
        /*
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Booking'],
        [ 
            'icon_id' => Icon::where('css', 'fa fa-calendar')->first()->id,
            'url'     => '',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Booking',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 70,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU BOOKING - APPUNTAMENTI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Appointments'],
        [ 
            'icon_id' => Icon::where('css', 'fa fa-calendar-check-o')->first()->id,
            'url'     =>  '/admin/booking/Appointments',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Agenda',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Booking')->first()->id,
            'created_id' => $idUser
        ]);
        

        /*****************************************************************************/
        /* MENU BOOKING - LUOGHI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Place'],
        [ 
           'functionality_id' => Functionality::where('code', 'Places')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-building-o')->first()->id,
            'url'     =>  '/admin/booking/place',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Luoghi',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Booking')->first()->id,
            'created_id' => $idUser
        ]);
        
        
        /*****************************************************************************/
        /* MENU BOOKING - SERVIZI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Services'],
        [ 
            'functionality_id' => Functionality::where('code', 'BookingServices')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-hand-paper-o')->first()->id,
            'url'     =>  '/admin/booking/services',
            'created_id' => $idUser 
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Servizi',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 30,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Booking')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU BOOKING - ADDETTI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Employee'],
        [ 
            'functionality_id' => Functionality::where('code', 'Employees')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-users')->first()->id,
            'url'     =>  '/admin/booking/employee',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Addetti',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 40,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Booking')->first()->id,
            'created_id' => $idUser
        ]);

         /*****************************************************************************/
        /* MENU BOOKING - CONNESSIONI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Connections'],
        [ 
            'functionality_id' => Functionality::where('code', 'Connections')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-link')->first()->id,
            'url'     =>  '/admin/booking/connections',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Connessioni',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 50,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Booking')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU BOOKING - ECCEZIONI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Exceptions'],
        [ 
            'functionality_id' => Functionality::where('code', 'Exceptions')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-exclamation-circle')->first()->id,
            'url'     =>  '/admin/booking/exceptions',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Eccezioni',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 60,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Booking')->first()->id,
            'created_id' => $idUser
        ]);

             
        /*****************************************************************************/
        /* MENU BOOKING - IMPOSTAZIONI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Settings'],
        [ 
            'functionality_id' => Functionality::where('code', 'BookingSettings')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-cog')->first()->id,
            'url'     =>  '/admin/booking/bookingsettings',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Impostazioni',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 80,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Booking')->first()->id,
            'created_id' => $idUser
        ]);


        /*****************************************************************************/
        /*
        /*
        /* MENU DESIGN */
        /*
        /*
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Design'],
        [ 
            //'functionality_id' => XXX,
            'icon_id' => Icon::where('css', 'fa fa-delicious')->first()->id,
            'url'     => '',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Design',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 80,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU DESIGN - TEMA */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Theme'],
        [ 
            //'functionality_id' => Functionality::where('code', 'RequestContacts')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-object-group')->first()->id,
            'url'     => '',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Tema',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Design')->first()->id,
            'created_id' => $idUser
        ]);
        
        /*****************************************************************************/
        /* MENU DESIGN - TEMA - COMPONENTI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Contents'],
        [ 
            'functionality_id' => Functionality::where('code', 'Contents')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-server')->first()->id,
            'url'     => '/admin/contents',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Componenti',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Theme')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU DESIGN - TEMA - CSS */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'ManageStyle'],
        [ 
            'functionality_id' => Functionality::where('code', 'ManageStyle')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-css3')->first()->id,
            'url'     => '/admin/setting/manageStyle',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'CSS',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Theme')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU DESIGN - TEMA - JS */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'JS'],
        [ 
            'functionality_id' => Functionality::where('code', 'JS')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-jsfiddle')->first()->id,
            'url'     => '/admin/setting/js',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'JS',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Theme')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU DESIGN - BLOCCHI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Template'],
        [ 
            //'functionality_id' => Functionality::where('code', 'RequestContacts')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-list-alt')->first()->id,
            'url'     => '',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Blocchi',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Design')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU DESIGN - BLOCCHI - GESTIONE BLOCCHI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'TemplateEditor'],
        [ 
            'functionality_id' => Functionality::where('code', 'TemplateEditor')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-tasks')->first()->id,
            'url'     => '/admin/setting/templateEditor',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Gestione blocchi',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Template')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU DESIGN - BLOCCHI - GRUPPI BLOCCHI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'TemplateEditorGroup'],
        [ 
            'functionality_id' => Functionality::where('code', 'TemplateEditorGroup')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-server')->first()->id,
            'url'     => '/admin/setting/templateEditorGroup',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Gruppi blocchi',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Template')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /*
        /*
        /* MENU IMPOSTAZIONI */
        /*
        /*
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Setting'],
        [ 
            //'functionality_id' => XXX,
            'icon_id' => Icon::where('css', 'fa fa-cog')->first()->id,
            'url'     => '',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Impostazioni',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 80,
            'created_id' => $idUser
        ]);
        
        /*****************************************************************************/
        /* MENU IMPOSTAZIONI - LINGUE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Languages'],
        [ 
            'functionality_id' => Functionality::where('code', '=', 'Languages')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-eye')->first()->id,
            'url'     => '/admin/setting/languages',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Lingue',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 5,
            'menu_admin_father_id' => MenuAdmin::where('code', '=', 'Setting')->first()->id,
            'created_id' => $idUser
        ]);

        /******************/
        /* TAB TABLE */
        /******************/
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Languages')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'LanguagesFormatter'],['created_id' => $idUser, 'formatter' => 'LanguagesFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Lingua', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '15', 'table_order'=>'20', 'created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'LanguagesDeafultFormatter'],['created_id' => $idUser, 'formatter' => 'LanguagesDeafultFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'30', 'created_id' => $idUser]);
 

        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Languages')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);

        //description
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Lingua', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 

        //default
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'default'],['field'=>'default', 'data_type'=> '3', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Lingua Predefinita', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'20', 'pos_y'=>'10', 'created_id' => $idUser]); 

        //date_format
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'date_format'],['field'=>'date_format', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Formato Data', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'20', 'created_id' => $idUser]); 

        //date_time_format
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'date_time_format'],['field'=>'date_time_format', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Formato Data e ora', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'20', 'created_id' => $idUser]);
        
        //date_format_client
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'date_format_client'],['field'=>'date_format_client', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Formato Data web', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'30', 'created_id' => $idUser]);
        
        //date_time_format_client
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'date_time_format_client'],['field'=>'date_time_format_client', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Formato Data e ora Web', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'30', 'created_id' => $idUser]);


        /*****************************************************************************/
        /* MENU IMPOSTAZIONI - RIDIMENSIONAMENTO IMMAGINI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'ImageResize'],
        [ 
            'functionality_id' => Functionality::where('code', '=', 'ImageResize')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-arrows-alt')->first()->id,
            'url'     => '/admin/setting/imageresize',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Grandezza Immagini',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 7,
            'menu_admin_father_id' => MenuAdmin::where('code', '=', 'Setting')->first()->id,
            'created_id' => $idUser
        ]);

        /******************/
        /* TAB TABLE */
        /******************/
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'ImageResize')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'ImageResizeFormatter'],['created_id' => $idUser, 'formatter' => 'ImageResizeFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'container'],['created_id' => $idUser, 'field'=> 'container', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Contenitore', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '15', 'table_order'=>'20', 'created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'width'],['created_id' => $idUser, 'field'=> 'width', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Width', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '15', 'table_order'=>'30', 'created_id' => $idUser]);
        

        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'ImageResize')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);

        //container
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'container'],['field'=>'container', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Contenitore', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 

        //width
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'width'],['field'=>'width', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Width', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'10', 'created_id' => $idUser]);

        //online
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'online'],['field'=>'online', 'data_type'=> '3', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Online', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'30', 'pos_y'=>'10', 'created_id' => $idUser]); 

        /*****************************************************************************/
        /* MENU IMPOSTAZIONI - VISTE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'SelenaViews'],
        [ 
            'functionality_id' => Functionality::where('code', 'SelenaViews')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-eye')->first()->id,
            'url'     => '/admin/setting/selenaviews',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Viste',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Setting')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU IMPOSTAZIONI - CONFIGURAZIONI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'ConfigurationSetting'],
        [ 
            'functionality_id' => Functionality::where('code', 'ConfigurationSetting')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-cog')->first()->id,
            'url'     => '/admin/setting/configuration',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Configurazioni',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Setting')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU IMPOSTAZIONI - TABELLE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'SettingTables'],
        [ 
            //'functionality_id' => Functionality::where('code', 'ConfigurationSetting')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-table')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Tabelle',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 90,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Setting')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU IMPOSTAZIONI - TABELLE - LINGUE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Languages'],
        [ 
            //'functionality_id' => Functionality::where('code', 'ConfigurationSetting')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-flag-checkered')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Lingue',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'SettingTables')->first()->id,
            'created_id' => $idUser
        ]);

  
        /*****************************************************************************/
        /* MENU IMPOSTAZIONI - ZONE/AREE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Zones'],
        [ 
            'icon_id' => Icon::where('css', 'fa fa-map-o')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Zone/Aree',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 30,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Setting')->first()->id,
            'created_id' => $idUser
        ]);

        /**********/
        /* Zone */
        /**********/
        $menu = MenuAdmin::updateOrCreate(['code' => 'Zone'],
        [ 
            'functionality_id' => Functionality::where('code', 'Zone')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-map-pin')->first()->id,
            'url'     => '/admin/crm/zone',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Zone',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 60,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Zones')->first()->id,
            'created_id' => $idUser
        ]);




        /**********/
        /* COMUNI */
        /**********/
        $menu = MenuAdmin::updateOrCreate(['code' => 'Cities'],
        [ 
            'functionality_id' => Functionality::where('code', 'Cities')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-university')->first()->id,
            'url'     => '/admin/crm/cities',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Città',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Zones')->first()->id,
            'created_id' => $idUser
        ]);

        /**********/
        /*   CAP  */
        /**********/
        $menu = MenuAdmin::updateOrCreate(['code'  => 'PostalCode'],
        [ 
            'functionality_id' => Functionality::where('code', 'PostalCode')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-thumb-tack')->first()->id,
            'url'     => '/admin/crm/postalcode',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'CAP',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Zones')->first()->id,
            'created_id' => $idUser
        ]);

        /************/
        /* PROVINCE */
        /************/
        $menu = MenuAdmin::updateOrCreate(['code'  => 'Provinces'],
        [ 
            'functionality_id' => Functionality::where('code', 'Provinces')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-map-signs')->first()->id,
            'url'     => '/admin/crm/provinces',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Province',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 30,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Zones')->first()->id,
            'created_id' => $idUser
        ]);

        /***********/
        /* REGIONI */
        /***********/
        $menu = MenuAdmin::updateOrCreate(['code'  => 'Regions'],
        [ 
            'functionality_id' => Functionality::where('code', 'Regions')->first()->id,
             'icon_id' => Icon::where('css', 'fa fa-tags')->first()->id,
            'url'     => '/admin/crm/regions',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Regioni',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 40,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Zones')->first()->id,
            'created_id' => $idUser
        ]);

        /***********/
        /* NAZIONI */
        /***********/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Nations'],
        [ 
            'functionality_id' => Functionality::where('code', 'Nations')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-flag')->first()->id,
            'url'     => '/admin/crm/nations',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Nazioni',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 50,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Zones')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU IMPOSTAZIONI - TABELLE - GRUPPI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'UsersGroups'],
        [ 
            //'functionality_id' => Functionality::where('code', 'Nations')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-users')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Gruppi',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 'f',
            'order' => 40,
            'menu_admin_father_id' => MenuAdmin::where('code', 'SettingTables')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU IMPOSTAZIONI - TABELLE - TIPI VOUCHER */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'VoucherType'],
        [ 
            'functionality_id' => Functionality::where('code', 'VoucherType')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-gift')->first()->id,
            'url'     => '/admin/sales/voucherType',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Tipi Voucher',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 50,
            'menu_admin_father_id' => MenuAdmin::where('code', 'SettingTables')->first()->id,
            'created_id' => $idUser
        ]);

        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'VoucherType')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'TableCheckBox'],['created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'CheckBox', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '13', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'VoucherTypeFormatter'],['created_id' => $idUser, 'formatter' => 'VoucherTypeFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 

        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'VoucherType')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);

        //codice voucher
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 
    
        
        /*****************************************************************************/
        /*
        /*
        /* MENU IMPOSTAZIONI - ROBOTS SETTING */
        /*
        /*
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'RobotsSetting'],
        [ 
            'functionality_id' => Functionality::where('code', 'RobotsSetting')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-cog')->first()->id,
            'url'     => '/admin/setting/robotssetting',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Robots',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 100,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Setting')->first()->id,
            'created_id' => $idUser
        ]);
        

        /******************/
        /* TAB TABLE */
        /******************/
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Faq')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'FaqFormatter'],['created_id' => $idUser, 'formatter' => 'FaqFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'10', 'created_id' => $idUser]); 
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'shortcode'],['created_id' => $idUser, 'field'=> 'shortcode', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Shortcode', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 
    
        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Faq')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);

        //shortcode
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'shortcode'],['field'=>'shortcode', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Shortcode', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 
    
        //description
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'10', 'created_id' => $idUser]); 
    
        //order
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'order'],['field'=>'order', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ordinamento', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'30', 'pos_y'=>'10', 'created_id' => $idUser]); 

        //where
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'where'],['field'=>'where', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Dove è usato', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '1', 'pos_x'=>'10', 'pos_y'=>'20', 'created_id' => $idUser]); 
    
        //how
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'how'],['field'=>'how', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Come è usato', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '1', 'pos_x'=>'10', 'pos_y'=>'30', 'created_id' => $idUser]); 
    
        //what
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'what'],['field'=>'what', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Cosa fa', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '1', 'pos_x'=>'10', 'pos_y'=>'40', 'created_id' => $idUser]); 
    


        /*****************************************************************************/
        /*
        /*
        /* MENU IMPOSTAZIONI - REDIRECT URL */
        /*
        /*
        /*****************************************************************************/ 
        $menu = MenuAdmin::updateOrCreate(['code'   => 'RedirectUrlSetting'],
        [ 
            'functionality_id' => Functionality::where('code', 'RedirectUrlSetting')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-arrow-circle-o-down')->first()->id,
            'url'     => '/admin/setting/redirecturl',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Redirect',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 110,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Setting')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /*
        /*
        /* MENU IMPOSTAZIONI - FAQ */
        /*
        /*
        /*****************************************************************************/ 
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Faq'],
        [ 
            'functionality_id' => Functionality::where('code', 'Faq')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-book')->first()->id,
            'url'     => '/admin/setting/faq',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Faq',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled' => 't',
            'order' => 120,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Setting')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /*
        /*
        /* MENU IMPOSTAZIONI - SCHEDULATORE */
        /*
        /*
        /*****************************************************************************/ 
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Scheduler'],
        [ 
            'functionality_id' => Functionality::where('code', 'Scheduler')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-clock-o')->first()->id,
            'url'     => '/admin/setting/scheduler',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Schedulatore',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled' => 't',
            'order' => 115,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Setting')->first()->id,
            'created_id' => $idUser
        ]);

        /******************/
        /* TAB TABLE */
        /******************/
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Scheduler')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'SchedulerFormatter'],['created_id' => $idUser, 'formatter' => 'SchedulerFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'10', 'created_id' => $idUser]); 
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'name'],['created_id' => $idUser, 'field'=> 'name', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Nome', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'SchedulerFormatterNotification'],['created_id' => $idUser, 'formatter' => 'SchedulerFormatterNotification']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Avviso errore', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'40', 'created_id' => $idUser]); 
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'SchedulerFormatterActive'],['created_id' => $idUser, 'formatter' => 'SchedulerFormatterActive']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Attivo', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'50', 'created_id' => $idUser]); 
        
        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'Scheduler')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);

        //handler
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'handler'],['field'=>'handler', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Handler', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 
    
        //name
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'name'],['field'=>'name', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Nome', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'10', 'created_id' => $idUser]); 
    
        //description
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'30', 'pos_y'=>'10', 'created_id' => $idUser]); 

        //action
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'action'],['field'=>'action', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Azione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '1', 'pos_x'=>'10', 'pos_y'=>'20', 'created_id' => $idUser]); 
    
        //arguments
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'arguments'],['field'=>'arguments', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Opzioni di lancio azione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '1', 'pos_x'=>'10', 'pos_y'=>'30', 'created_id' => $idUser]); 
    
        //schedule
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'schedule'],['field'=>'schedule', 'service' => 'typeScheduler', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Tipo di schedulazione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '2', 'pos_x'=>'10', 'pos_y'=>'40', 'created_id' => $idUser]); 
    
        //frequency
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'frequency'],['field'=>'frequency', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Frequenza di schedulazione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'40', 'created_id' => $idUser]); 
        
        //repeat_after_hour
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'repeat_after_hour'],['field'=>'repeat_after_hour', 'service' => 'repeatHourScheduler', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ripeti ogni N° ore', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '2', 'pos_x'=>'30', 'pos_y'=>'40', 'created_id' => $idUser]); 

        //repeat_after_min
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'repeat_after_min'],['field'=>'repeat_after_min', 'service' => 'repeatMinutesScheduler', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ripeti ogni N° minuti', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '2', 'pos_x'=>'40', 'pos_y'=>'40', 'created_id' => $idUser]); 

        //repeat_times
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'repeat_times'],['field'=>'repeat_times', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ripeti N° volte', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'50', 'pos_y'=>'40', 'created_id' => $idUser]); 


        //start_time_action
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'start_time_action'],['field'=>'start_time_action', 'data_type'=> '2', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Data / ora di avvio', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'pos_x'=>'10', 'pos_y'=>'50', 'created_id' => $idUser]); 
    
        //next_run
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'next_run'],['field'=>'next_run', 'data_type'=> '2', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Prossima esecuzione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'pos_x'=>'20', 'pos_y'=>'50', 'created_id' => $idUser]); 
        
        //last_run
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'last_run'],['field'=>'last_run', 'data_type'=> '2', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ultima esecuzione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '4', 'pos_x'=>'30', 'pos_y'=>'50', 'created_id' => $idUser]); 

        //status
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'status'],['field'=>'status', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ultimo stato', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'60', 'created_id' => $idUser]); 

        //notification
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'notification'],['field'=>'notification', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Avviso errore', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'20', 'pos_y'=>'60', 'created_id' => $idUser]); 

        //active
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'active'],['field'=>'active', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Attivo', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'30', 'pos_y'=>'60', 'created_id' => $idUser]); 



        /*****************************************************************************/
        /*
        /*
        /* MENU IMPOSTAZIONI - FROM BUILDER SETTING */
        /*
        /*
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'FormBuilder'],
        [ 
            'functionality_id' => Functionality::where('code', 'FormBuilder')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-keyboard-o')->first()->id,
            'url'     => '/admin/setting/formbuilder',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Form Builder',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 40,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Theme')->first()->id,
            'created_id' => $idUser
        ]);

        /******************/
        /* TAB TABLE */
        /******************/
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'FormBuilder')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'FormBuilderFormatter'],['created_id' => $idUser, 'formatter' => 'FormBuilderFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Nome form', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '15', 'table_order'=>'20', 'created_id' => $idUser]);
 
    
        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'FormBuilder')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);
        


        //container
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Nome form', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]);
        
        //oggetto email
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'object'],['field'=>'object', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Oggetto email', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'20', 'created_id' => $idUser]); 

        //email
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'email_destination'],['field'=>'email_destination', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Email di destinazione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'20', 'created_id' => $idUser]);

        //feedback_message
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'feedback_message'],['field'=>'feedback_message', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Messaggio di feedback', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'30', 'created_id' => $idUser]);
        
        //message company
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'message_company'],['field'=>'message_company', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Messaggio di feedback in email all\'azienda', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'25', 'created_id' => $idUser]);

        //message customer
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'message_customer'],['field'=>'message_customer', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Messaggio di feedback in email al cliente', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'25', 'created_id' => $idUser]);


        //feedback_redirect
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'feedback_redirect'],['field'=>'feedback_redirect', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Feddback redirect', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'20', 'pos_y'=>'30', 'created_id' => $idUser]);

        //feedback_url_redirect
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'feedback_url_redirect'],['field'=>'feedback_url_redirect', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Feedback url di redirect', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'30', 'pos_y'=>'30', 'created_id' => $idUser]);

        /******************/
        /* TAB TABLE FIELD FORM*/
        /******************/
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'FormBuilder')->first()->id, 
            'code' => 'TableFieldFormBuilder'
        ],
        [
            'order' => 30,
            'created_id' => $idUser
        ]);
        
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'FormBuilderFieldFormatter'],['created_id' => $idUser, 'formatter' => 'FormBuilderFieldFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'typeDescription'],['created_id' => $idUser, 'field'=> 'typeDescription', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Tipologia campo', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '15', 'table_order'=>'20', 'created_id' => $idUser]);
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'label'],['created_id' => $idUser, 'field'=> 'label', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Label', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '15', 'table_order'=>'30', 'created_id' => $idUser]);
          
        /******************/
        /* TAB DETTAGLIO FIELD FORM*/
        /******************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'FormBuilder')->first()->id, 
            'code' => 'DetailFieldFormBuilder'
        ],
        [
            'order' => 40,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);

        //Tipologia campo
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'form_builder_type_id'],['field'=>'form_builder_type_id', 'service' => 'formBuilderFieldCategoryType', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Gruppo di visualizzazione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '2', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 

        //Tipologia di dato
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'type'],['field'=>'type', 'service' => 'formBuilderFieldType', 'on_change' => 'changeFieldType', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Tipologia di dato', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '2', 'pos_x'=>'20', 'pos_y'=>'10', 'created_id' => $idUser]); 

        //Label
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'label'],['field'=>'label', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Etichetta', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'30', 'pos_y'=>'10', 'created_id' => $idUser]); 

        //Descrizione 
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione di default', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'40', 'pos_y'=>'10', 'created_id' => $idUser]); 

        //nome
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'name'],['field'=>'name', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Nome del campo', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'50', 'pos_y'=>'10', 'created_id' => $idUser]);
    
        //value 
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'value'],['field'=>'value', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Valori predefiniti', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'20', 'created_id' => $idUser]); 

        //placeholder 
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'placeholder'],['field'=>'placeholder', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Placeholder', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'15', 'pos_y'=>'20', 'created_id' => $idUser]);

        //width 
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'width'],['field'=>'width', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Numero di colonne (1 - 12)', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'20', 'created_id' => $idUser]); 


        //order 
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'order'],['field'=>'order', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ordinamento', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'30', 'pos_y'=>'20', 'created_id' => $idUser]); 

        //css class 
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'css'],['field'=>'css', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Classe css', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'40', 'pos_y'=>'20', 'created_id' => $idUser]);

        //obbligatorio
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'required'],['field'=>'required', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Obbligatorio', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'50', 'pos_y'=>'20', 'created_id' => $idUser]); 


        /*****************************************************************************/
        /*
        /*
        /* MENU IMPOSTAZIONI - FROM BUILDER TYPE */
        /*
        /*
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'FormBuilderType'],
        [ 
            'functionality_id' => Functionality::where('code', 'FormBuilderType')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-keyboard-o')->first()->id,
            'url'     => '/admin/setting/formbuildertype',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Gruppi campi Form Builder',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 50,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Theme')->first()->id,
            'created_id' => $idUser
        ]);

        /******************/
        /* TAB TABLE */
        /******************/
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'FormBuilderType')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);
        
        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'FormBuilderTypeFormatter'],['created_id' => $idUser, 'formatter' => 'FormBuilderTypeFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Nome form', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '15', 'table_order'=>'20', 'created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'order'],['created_id' => $idUser, 'field'=> 'order', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ordinamento', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '15', 'table_order'=>'30', 'created_id' => $idUser]);
 
    
        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'FormBuilderType')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);
        


        //descrizione
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione gruppo', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]);
        
        //ordinamento
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'order'],['field'=>'order', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ordinamento', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'10', 'created_id' => $idUser]); 


        /*****************************************************************************/
        /*
        /*
        /* MENU COMMUNITY */
        /*
        /*
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'Community'],
        [ 
            'icon_id' => Icon::where('css', 'fa fa-users')->first()->id,
            'url'     => '',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Community',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 60,
            'created_id' => $idUser
        ]);


        /*****************************************************************************/
        /* MENU COMMUNITY - IMMAGINI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'CommunityImages'],
        [ 
            'functionality_id' => Functionality::where('code', 'CommunityImages')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-eye')->first()->id,
            'url'     => '/admin/community/images',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Immagini',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Community')->first()->id,
            'created_id' => $idUser
        ]);

        /******************/
        /* TAB TABLE */
        /******************/
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'CommunityImages')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);

        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'CommunityImagesFormatter'],['created_id' => $idUser, 'formatter' => 'CommunityImagesFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'image_title'],['created_id' => $idUser, 'field'=> 'image_title', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Titolo', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'20', 'created_id' => $idUser]); 
    
        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'ImageCategoryId'],['created_id' => $idUser, 'field'=> 'ImageCategoryId', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Categoria', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'UploadDateTime'],['created_id' => $idUser, 'field'=> 'UploadDateTime', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Data', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'40', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'CheckOnlineCommunityImagesFormatter'],['created_id' => $idUser, 'formatter' => 'CheckOnlineCommunityImagesFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'table_order'=>'50', 'created_id' => $idUser]); 
   
        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'CommunityImages')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'image_title'],['field'=>'image_title', 'data_type'=> '1', 'on_change'=> 'changeTitle', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Titolo Immagine', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'colspan'=> '5', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 
        
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'link'],['field'=>'link', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Link', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'colspan'=> '5', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'10',
        'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'online'],['field'=>'online', 'data_type'=> '3', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Online', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'colspan'=> '2', 'input_type'=> '5', 'pos_x'=>'30', 'pos_y'=>'10',
        'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'image_description'],['field'=>'image_description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione Immagine', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '1', 'pos_x'=>'10', 'pos_y'=>'20',
        'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'image_category_id'],['field'=>'image_category_id', 'data_type'=> '1', 'service'=>'communityImageCategory', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Categoria Immagine', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'colspan'=> '2', 'input_type'=> '2', 'pos_x'=>'10', 'pos_y'=>'30',
        'created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'keywords'],['field'=>'keywords', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Keywords', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'colspan'=> '6', 'input_type'=> '0', 'pos_x'=>'20', 'pos_y'=>'30',
        'created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'users_id'],['field'=>'users_id', 'service'=>'users', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Utente', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'colspan'=> '2', 'input_type'=> '2', 'pos_x'=>'30', 'pos_y'=>'30',
        'created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'upload_date_time'],['field'=>'upload_date_time', 'data_type'=> '2', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Data', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't',  'colspan'=> '2', 'input_type'=> '4', 'pos_x'=>'40', 'pos_y'=>'30',
        'created_id' => $idUser]);



        /*****************************************************************************/
        /* MENU COMMUNITY - SEGNALAZIONI ALLE IMMAGINI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'CommunityReports'],
        [ 
            'functionality_id' => Functionality::where('code', 'CommunityReports')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-bullhorn')->first()->id,
            'url'     => '/admin/community/reports',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Immagini segnalate',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Community')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU COMMUNITY - COMMENTI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'CommunityComments'],
        [ 
            'functionality_id' => Functionality::where('code', 'CommunityComments')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-commenting-o')->first()->id,
            'url'     => '/admin/community/comments',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Commenti',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 30,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Community')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU COMMUNITY - SEGNALAZIONI AI COMMENTI*/
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'CommunityCommentsReports'],
        [ 
            'functionality_id' => Functionality::where('code', 'CommunityCommentsReports')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-bullhorn')->first()->id,
            'url'     => '/admin/community/commentsReports',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Commenti segnalati',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 40,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Community')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU COMMUNITY - CONTESTS */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'CommunityContests'],
        [ 
            'functionality_id' => Functionality::where('code', 'CommunityContests')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-flag-checkered')->first()->id,
            'url'     => '/admin/community/contests',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Concorsi',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 50,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Community')->first()->id,
            'created_id' => $idUser
        ]);
    
       /*****************************************************************************/
        /* MENU COMMUNITY - TABELLE */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'CommunityTables'],
        [ 
            'icon_id' => Icon::where('css', 'fa fa-table')->first()->id,
            'url'     => '#',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Tabelle',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 60,
            'menu_admin_father_id' => MenuAdmin::where('code', 'Community')->first()->id,
            'created_id' => $idUser
        ]);

        /*****************************************************************************/
        /* MENU COMMUNITY - TABELLE - CATEGORIE IMMAGINI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'CommunityImagesCategories'],
        [ 
            'functionality_id' => Functionality::where('code', 'CommunityImagesCategories')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-th')->first()->id,
            'url'     => '/admin/community/imageCategory',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Categorie Immagini',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 10,
            'menu_admin_father_id' => MenuAdmin::where('code', 'CommunityTables')->first()->id,
            'created_id' => $idUser
        ]);

        /******************/
        /* TAB TABLE */
        /******************/
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'CommunityImagesCategories')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);

        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'TableCheckBox'],['created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'CheckBox', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '13', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'CommunityImagesCategoriesFormatter'],['created_id' => $idUser, 'formatter' => 'CommunityImagesCategoriesFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 

        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'CommunityImagesCategories')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);

        //descrizione
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 
    
        //Ordine
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'order'],['field'=>'order', 'data_type'=> '0', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Ordinamento', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '7', 'pos_x'=>'10', 'pos_y'=>'20', 'created_id' => $idUser]); 
    
        //online si/no
        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'Online'],['field'=>'online', 'data_type'=> '3', 'default_value'=>0, 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Online', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '5', 'pos_x'=>'20', 'pos_y'=>'20', 'created_id' => $idUser]); 
    

        /*****************************************************************************/
        /* MENU COMMUNITY - TABELLE - TIPI SEGNALAZIONI IMMAGINI */
        /*****************************************************************************/
        $menu = MenuAdmin::updateOrCreate(['code'   => 'CommunityImagesReportsTypes'],
        [ 
            'functionality_id' => Functionality::where('code', 'CommunityImagesReportsTypes')->first()->id,
            'icon_id' => Icon::where('css', 'fa fa-th')->first()->id,
            'url'     => '/admin/community/imageReportType',
            'created_id' => $idUser
        ]);

        $menuLanguage = LanguageMenuAdmin::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'language_id' => $idLanguage
        ],
        [
            'description'     => 'Tipologie segnalazioni',
            'created_id' => $idUser
        ]);

        $menuAdminUserRole = MenuAdminUserRole::updateOrCreate(
        [
            'menu_admin_id'   => $menu->id, 
            'role_id' => $idRole
        ],
        [
            'enabled'     => 't',
            'order' => 20,
            'menu_admin_father_id' => MenuAdmin::where('code', 'CommunityTables')->first()->id,
            'created_id' => $idUser
        ]);

        /******************/
        /* TAB TABLE */
        /******************/
        $tabsTable = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'CommunityImagesReportsTypes')->first()->id, 
            'code' => 'Table'
        ],
        [
            'order' => 10,
            'created_id' => $idUser
        ]);

        $tabsLanguageTable = LanguageTab::updateOrCreate(['tab_id'   => $tabsTable->id,'language_id' => $idLanguage],['description' => 'Table','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'TableCheckBox'],['created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'CheckBox', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '13', 'table_order'=>'10', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'CommunityImagesReportsTypesFormatter'],['created_id' => $idUser, 'formatter' => 'CommunityImagesReportsTypesFormatter']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => '', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '14', 'table_order'=>'20', 'created_id' => $idUser]); 

        $field = Field::updateOrCreate(['tab_id'   => $tabsTable->id, 'code' => 'description'],['created_id' => $idUser, 'field'=> 'description', 'data_type' => '1']); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'table_order'=>'30', 'created_id' => $idUser]); 

        /******************/
        /* TAB DETTAGLIO */
        /******************/
        $tabsDetail = Tab::updateOrCreate(
        [
            'functionality_id'   => Functionality::where('code', 'CommunityImagesReportsTypes')->first()->id, 
            'code' => 'Detail'
        ],
        [
            'order' => 20,
            'created_id' => $idUser
        ]); 

        $tabsLanguageDetail = LanguageTab::updateOrCreate(['tab_id'   => $tabsDetail->id,'language_id' => $idLanguage],['description' => 'Detail','created_id' => $idUser]);

        $field = Field::updateOrCreate(['tab_id'   => $tabsDetail->id, 'code' => 'description'],['field'=>'description', 'data_type'=> '1', 'created_id' => $idUser]); 
        $fieldLanguage = FieldLanguage::updateOrCreate(['field_id'   => $field->id, 'language_id' => $idLanguage],['description' => 'Descrizione', 'created_id' => $idUser]); 
        $fieldUserRole = FieldUserRole::updateOrCreate(['field_id'   => $field->id, 'role_id' => $idRole],['enabled' => 't', 'input_type'=> '0', 'pos_x'=>'10', 'pos_y'=>'10', 'created_id' => $idUser]); 
    

     /*   $idFunctionality = Functionality::where('code', 'Newsletter')->first();
        $IdTab = Tab::where('functionality_id', $idFunctionality->id)->get()->first();   
        $LanguagesTabs= LanguageTab::where('tab_id',$IdTab->id)->get()->first(); 
        $fieldId= Field::where('tab_id',$IdTab->id)->get()->first(); 

          $FieldUserRole= FieldUserRole::where('field_id', $fieldId->id)->delete();
        $fieldLanguages= FieldLanguage::where('field_id', $fieldId->id)->delete();
        $fieldId= Field::where('tab_id',$IdTab->id)->delete();

        $LanguagesTabs= LanguageTab::where('tab_id',$IdTab->id)->delete();
        $IdTabs = Tab::where('functionality_id', $idFunctionality->id)->delete();*/
        
 
        /* CANCELLAZIONE MENU' NON PIU' UTILIZZATI */
        $idMenu = MenuAdmin::where('code', 'CRM')->first();
        if ($idMenu != null) {
            LanguageMenuAdmin::where('menu_admin_id', $idMenu->id)->delete();
            MenuAdminUserRole::where('menu_admin_id', $idMenu->id)->delete();
            MenuAdmin::where('id', $idMenu->id)->delete();
        }

        $idMenu = MenuAdmin::where('code', 'News&Blog')->first();
        if ($idMenu != null) {
            LanguageMenuAdmin::where('menu_admin_id', $idMenu->id)->delete();
            MenuAdminUserRole::where('menu_admin_id', $idMenu->id)->delete();
            MenuAdmin::where('id', $idMenu->id)->delete();
        }

        $idMenu = MenuAdmin::where('code', 'Voucher')->first();
        if ($idMenu != null) {
            LanguageMenuAdmin::where('menu_admin_id', $idMenu->id)->delete();
            MenuAdminUserRole::where('menu_admin_id', $idMenu->id)->delete();
            MenuAdmin::where('id', $idMenu->id)->delete();
        }

    }
}

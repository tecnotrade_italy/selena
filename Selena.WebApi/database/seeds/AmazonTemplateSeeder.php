<?php

use Illuminate\Database\Seeder;
use App\AmazonTemplate;
use App\User;

class AmazonTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idUser = User::where('name', '=', 'admin')->first()->id;

        /*********************************************************************/
        /*   TEMPLATE BOX CLASSICO O ORIZZONTALE
        /*********************************************************************/
        $template = AmazonTemplate::updateOrCreate(['code'   => 'horizontal'],
        [ 
            'description' => 'Template orizzontale',
            'shortcode_example'     => '[amazon box="CODICEART"]',
            'created_id' => $idUser
        ]);

        /*********************************************************************/
        /*   TEMPLATE VERTICALE
        /*********************************************************************/
        $template = AmazonTemplate::updateOrCreate(['code'   => 'vertical'],
        [ 
            'description' => 'Template verticale',
            'shortcode_example'     => '[amazon box="CODICEART1,CODICEART2,CODICEART3" template="vertical"]',
            'created_id' => $idUser
        ]);
  
        /*********************************************************************/
        /*   TEMPLATE LISTA
        /*********************************************************************/
        $template = AmazonTemplate::updateOrCreate(['code'   => 'list'],
        [ 
            'description' => 'Template lista',
            'shortcode_example'     => '[amazon box="CODICEART1,CODICEART2,CODICEART3" template="list"]',
            'created_id' => $idUser
        ]);

        /*********************************************************************/
        /*   TEMPLATE BESTSELLER
        /*********************************************************************/
        $template = AmazonTemplate::updateOrCreate(['code'   => 'bestseller'],
        [ 
            'description' => 'Template bestseller',
            'shortcode_example'     => '[amazon bestseller="STRINGARICERCA" items="3" template="table"]',
            'created_id' => $idUser
        ]);

    }

}

<?php

use App\Dictionary;
use App\Enums\SettingEnum;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tabs;
use App\Setting;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class FilippoSeeder extends Seeder
{

    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*$this->call([
            ContactSeeder::class
        ]);*/
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $idRole = Role::where('name', 'admin')->first()->id;
        $idFunctionality = Functionality::where('code', 'Categories')->first()->id;

        $tabContactDetail = Tab::where('code', 'Detail')->where('functionality_id', $idFunctionality)->first()->id;

       #region VISIBLE FROM

       $fieldSurnameContactDetail = new Field();
       $fieldSurnameContactDetail->code = 'visible_from';
       $fieldSurnameContactDetail->tab_id = $tabContactDetail;
       $fieldSurnameContactDetail->field = 'visible_from';
       $fieldSurnameContactDetail->data_type = 2;
       $fieldSurnameContactDetail->created_id = $idUser;
       $fieldSurnameContactDetail->save();

       $fieldLanguageSurnameContactDetail = new FieldLanguage();
       $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
       $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
       $fieldLanguageSurnameContactDetail->description = 'Visibile dal';
       $fieldLanguageSurnameContactDetail->created_id = $idUser;
       $fieldLanguageSurnameContactDetail->save();

       $fieldUserRoleSurnameContactDetail = new FieldUserRole();
       $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
       $fieldUserRoleSurnameContactDetail->role_id = $idRole;
       $fieldUserRoleSurnameContactDetail->enabled = true;
       $fieldUserRoleSurnameContactDetail->pos_x = 10;
       $fieldUserRoleSurnameContactDetail->pos_y = 60;
       $fieldUserRoleSurnameContactDetail->input_type = 4;
       $fieldUserRoleSurnameContactDetail->created_id = $idUser;
       $fieldUserRoleSurnameContactDetail->save();
       
       #endregion VISIBILE FROM

       #region VISIBLE TO

       $fieldSurnameContactDetail = new Field();
       $fieldSurnameContactDetail->code = 'visible_to';
       $fieldSurnameContactDetail->tab_id = $tabContactDetail;
       $fieldSurnameContactDetail->field = 'visible_to';
       $fieldSurnameContactDetail->data_type = 2;
       $fieldSurnameContactDetail->created_id = $idUser;
       $fieldSurnameContactDetail->save();

       $fieldLanguageSurnameContactDetail = new FieldLanguage();
       $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
       $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
       $fieldLanguageSurnameContactDetail->description = 'Fino al';
       $fieldLanguageSurnameContactDetail->created_id = $idUser;
       $fieldLanguageSurnameContactDetail->save();

       $fieldUserRoleSurnameContactDetail = new FieldUserRole();
       $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
       $fieldUserRoleSurnameContactDetail->role_id = $idRole;
       $fieldUserRoleSurnameContactDetail->enabled = true;
       $fieldUserRoleSurnameContactDetail->pos_x = 20;
       $fieldUserRoleSurnameContactDetail->pos_y = 60;
       $fieldUserRoleSurnameContactDetail->input_type = 4;
       $fieldUserRoleSurnameContactDetail->created_id = $idUser;
       $fieldUserRoleSurnameContactDetail->save();
       
       #endregion VISIBILE TO

    }
}

<?php

use Illuminate\Database\Seeder;
use App\Dictionary;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;


class FaultModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $idMenuAdminCRM = MenuAdmin::where('code', 'CRM')->first()->id;

        #region GrInterventions

        #region add newTitleInterventions 

        $dictionary = new Dictionary();
        $dictionary->language_id = $idLanguage;
        $dictionary->code = 'newTitleFaultModule';
        $dictionary->description = 'Inserisci Modulo Guasti';
        $dictionary->created_id = $idUser;
        $dictionary->save();

        #endregion add newTitleInterventions


        #region VATTYPE

        #region ICON

        $iconIdCard = new Icon();
        $iconIdCard->description = 'Bookmark';
        $iconIdCard->css = 'fa fa-bookmark-o';
        $iconIdCard->created_id = $idUser;
        $iconIdCard->save();

        #endregion ICON

        #region FUNCTIONALITY

        $functionalityContact = new Functionality();
        $functionalityContact->code = 'FaultModule';
        $functionalityContact->created_id = $idUser;
        $functionalityContact->save();

        #endregion FUNCTIONALITY

        #region MENU
        $menuAdminContact = new MenuAdmin();
        $menuAdminContact->code = 'FaultModule';
        $menuAdminContact->functionality_id = $functionalityContact->id;
        $menuAdminContact->icon_id = $iconIdCard->id;
        $menuAdminContact->url = '/admin/crm/FaultModule';
        $menuAdminContact->created_id = $idUser;
        $menuAdminContact->save();

        $languageMenuAdminContact = new LanguageMenuAdmin();
        $languageMenuAdminContact->language_id = $idLanguage;
        $languageMenuAdminContact->menu_admin_id = $menuAdminContact->id;
        $languageMenuAdminContact->description = 'FaultModule';
        $languageMenuAdminContact->created_id = $idUser;
        $languageMenuAdminContact->save();

        $menuAdminUserRoleContact = new MenuAdminUserRole();
        $menuAdminUserRoleContact->menu_admin_id = $menuAdminContact->id;
        $menuAdminUserRoleContact->menu_admin_father_id = $idMenuAdminCRM;
        $menuAdminUserRoleContact->role_id = $idRole;
        $menuAdminUserRoleContact->enabled = true;
        $menuAdminUserRoleContact->order = 30;
        $menuAdminUserRoleContact->created_id = $idUser;
        $menuAdminUserRoleContact->save();
        #endregion MENU



        $tabContactTable = new Tab();
        $tabContactTable->code = 'Table';
        $tabContactTable->functionality_id = $functionalityContact->id;
        $tabContactTable->order = 10;
        $tabContactTable->created_id = $idUser;
        $tabContactTable->save();

        $languageTabContactTable = new LanguageTab();
        $languageTabContactTable->language_id = $idLanguage;
        $languageTabContactTable->tab_id = $tabContactTable->id;
        $languageTabContactTable->description = 'Table';
        $languageTabContactTable->created_id = $idUser;
        $languageTabContactTable->save();

        #endregion TAB

        #region CHECKBOX

        $fieldContactCheckBox = new Field();
        $fieldContactCheckBox->tab_id = $tabContactTable->id;
        $fieldContactCheckBox->code = 'TableCheckBox';
        $fieldContactCheckBox->created_id = $idUser;
        $fieldContactCheckBox->save();

        $fieldLanguageContactCheckBox = new FieldLanguage();
        $fieldLanguageContactCheckBox->field_id = $fieldContactCheckBox->id;
        $fieldLanguageContactCheckBox->language_id = $idLanguage;
        $fieldLanguageContactCheckBox->description = 'CheckBox';
        $fieldLanguageContactCheckBox->created_id = $idUser;
        $fieldLanguageContactCheckBox->save();

        $fieldUserRoleContactCheckBox = new FieldUserRole();
        $fieldUserRoleContactCheckBox->field_id = $fieldContactCheckBox->id;
        $fieldUserRoleContactCheckBox->role_id = $idRole;
        $fieldUserRoleContactCheckBox->enabled = true;
        $fieldUserRoleContactCheckBox->table_order = 10;
        $fieldUserRoleContactCheckBox->input_type = 13;
        $fieldUserRoleContactCheckBox->created_id = $idUser;
        $fieldUserRoleContactCheckBox->save();

        #endregion CHECKBOX

        #region FORMATTER

        $fieldContactFormatter = new Field();
        $fieldContactFormatter->code = 'InterventionsFormatter';
        $fieldContactFormatter->formatter = 'InterventionsFormatter';
        $fieldContactFormatter->tab_id = $tabContactTable->id;
        $fieldContactFormatter->created_id = $idUser;
        $fieldContactFormatter->save();

        $fieldLanguageContactFormatter = new FieldLanguage();
        $fieldLanguageContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldLanguageContactFormatter->language_id = $idLanguage;
        $fieldLanguageContactFormatter->description = '';
        $fieldLanguageContactFormatter->created_id = $idUser;
        $fieldLanguageContactFormatter->save();

        $fieldUserRoleContactFormatter = new FieldUserRole();
        $fieldUserRoleContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldUserRoleContactFormatter->role_id = $idRole;
        $fieldUserRoleContactFormatter->enabled = true;
        $fieldUserRoleContactFormatter->table_order = 20;
        $fieldUserRoleContactFormatter->input_type = 14;
        $fieldUserRoleContactFormatter->created_id = $idUser;
        $fieldUserRoleContactFormatter->save();

        #endregion FORMATTER

        #region id

        $fieldContactTableCompany = new Field();
        $fieldContactTableCompany->code = 'id';
        $fieldContactTableCompany->tab_id = $tabContactTable->id;
        $fieldContactTableCompany->field = 'id';
        $fieldContactTableCompany->data_type = 1;
        $fieldContactTableCompany->created_id = $idUser;
        $fieldContactTableCompany->save();

        $fieldLanguageContactTableCompany = new FieldLanguage();
        $fieldLanguageContactTableCompany->field_id = $fieldContactTableCompany->id;
        $fieldLanguageContactTableCompany->language_id = $idLanguage;
        $fieldLanguageContactTableCompany->description = 'ID Progressivo';
        $fieldLanguageContactTableCompany->created_id = $idUser;
        $fieldLanguageContactTableCompany->save();

        $fieldUserRoleContactTableCompany = new FieldUserRole();
        $fieldUserRoleContactTableCompany->field_id = $fieldContactTableCompany->id;
        $fieldUserRoleContactTableCompany->role_id = $idRole;
        $fieldUserRoleContactTableCompany->enabled = true;
        $fieldUserRoleContactTableCompany->table_order = 30;
        $fieldUserRoleContactTableCompany->input_type = 0;
        $fieldUserRoleContactTableCompany->created_id = $idUser;
        $fieldUserRoleContactTableCompany->save();
        
        #endregion id

        #region Cliente

        $fieldContactTableBusinessName = new Field();
        $fieldContactTableBusinessName->code = 'customer_id';
        $fieldContactTableBusinessName->tab_id = $tabContactTable->id;
        $fieldContactTableBusinessName->field = 'customer_id';
        $fieldContactTableBusinessName->data_type = 1;
        $fieldContactTableBusinessName->created_id = $idUser;
        $fieldContactTableBusinessName->save();

        $fieldLanguageContactTableBusinessName = new FieldLanguage();
        $fieldLanguageContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldLanguageContactTableBusinessName->language_id = $idLanguage;
        $fieldLanguageContactTableBusinessName->description = 'Cliente';
        $fieldLanguageContactTableBusinessName->created_id = $idUser;
        $fieldLanguageContactTableBusinessName->save();

        $fieldUserRoleContactTableBusinessName = new FieldUserRole();
        $fieldUserRoleContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldUserRoleContactTableBusinessName->role_id = $idRole;
        $fieldUserRoleContactTableBusinessName->enabled = true;
        $fieldUserRoleContactTableBusinessName->table_order = 40;
        $fieldUserRoleContactTableBusinessName->input_type = 0;
        $fieldUserRoleContactTableBusinessName->created_id = $idUser;
        $fieldUserRoleContactTableBusinessName->save();


        #region DETAIL

        #region TAB
        $tabContactDetail = new Tab();
        $tabContactDetail->code = 'Detail';
        $tabContactDetail->functionality_id = $functionalityContact->id;
        $tabContactDetail->order = 20;
        $tabContactDetail->created_id = $idUser;
        $tabContactDetail->save();

        $languageTabContactDetail = new LanguageTab();
        $languageTabContactDetail->language_id = $idLanguage;
        $languageTabContactDetail->tab_id = $tabContactDetail->id;
        $languageTabContactDetail->description = 'Detail';
        $languageTabContactDetail->created_id = $idUser;
        $languageTabContactDetail->save();
        #endregion TAB

        #region Vs Referente
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'referent';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'referent';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Vs Referente';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 10;
        $fieldUserRoleContactDetailCompany->pos_y = 10;
        $fieldUserRoleContactDetailCompany->input_type = 0;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion Vs Referente

        #region numero ddt
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'nr_ddt';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'nr_ddt';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Numero DDT';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 20;
        $fieldUserRoleSurnameContactDetail->pos_y = 20;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion numero ddt

        #region  tipo impianto
        $fieldContactDetailBusinessName = new Field();
        $fieldContactDetailBusinessName->code = 'plant_type';
        $fieldContactDetailBusinessName->tab_id = $tabContactDetail->id;
        $fieldContactDetailBusinessName->field = 'plant_type';
        $fieldContactDetailBusinessName->data_type = 1;
        $fieldContactDetailBusinessName->created_id = $idUser;
        $fieldContactDetailBusinessName->save();

        $fieldLanguageContactDetailBusinessName = new FieldLanguage();
        $fieldLanguageContactDetailBusinessName->field_id = $fieldContactDetailBusinessName->id;
        $fieldLanguageContactDetailBusinessName->language_id = $idLanguage;
        $fieldLanguageContactDetailBusinessName->description = 'Tipo Impianto';
        $fieldLanguageContactDetailBusinessName->created_id = $idUser;
        $fieldLanguageContactDetailBusinessName->save();

        $fieldUserRoleContactDetailBusinessName = new FieldUserRole();
        $fieldUserRoleContactDetailBusinessName->field_id = $fieldContactDetailBusinessName->id;
        $fieldUserRoleContactDetailBusinessName->role_id = $idRole;
        $fieldUserRoleContactDetailBusinessName->enabled = true;
        $fieldUserRoleContactDetailBusinessName->pos_x = 30;
        $fieldUserRoleContactDetailBusinessName->pos_y = 20;
        $fieldUserRoleContactDetailBusinessName->input_type = 0;
        $fieldUserRoleContactDetailBusinessName->created_id = $idUser;
        $fieldUserRoleContactDetailBusinessName->save();
        #endregion tipo impianto

        #region tipo carrello
        $fieldContactDetailName = new Field();
        $fieldContactDetailName->code = 'trolley_type';
        $fieldContactDetailName->tab_id = $tabContactDetail->id;
        $fieldContactDetailName->field = 'trolley_type';
        $fieldContactDetailName->data_type = 1;
        $fieldContactDetailName->created_id = $idUser;
        $fieldContactDetailName->save();

        $fieldLanguageContactDetailName = new FieldLanguage();
        $fieldLanguageContactDetailName->field_id = $fieldContactDetailName->id;
        $fieldLanguageContactDetailName->language_id = $idLanguage;
        $fieldLanguageContactDetailName->description = 'Tipo Carrello';
        $fieldLanguageContactDetailName->created_id = $idUser;
        $fieldLanguageContactDetailName->save();

        $fieldUserRoleContactDetailName = new FieldUserRole();
        $fieldUserRoleContactDetailName->field_id = $fieldContactDetailName->id;
        $fieldUserRoleContactDetailName->role_id = $idRole;
        $fieldUserRoleContactDetailName->enabled = true;
        $fieldUserRoleContactDetailName->pos_x = 40;
        $fieldUserRoleContactDetailName->pos_y = 50;
        $fieldUserRoleContactDetailName->input_type = 0;
        $fieldUserRoleContactDetailName->created_id = $idUser;
        $fieldUserRoleContactDetailName->save();
        #endregion tipo carrello

        #region serie
        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'series';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'series';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Serie';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 50;
        $fieldUserRoleContactDetailCompany->pos_y = 50;
        $fieldUserRoleContactDetailCompany->input_type = 0;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();
        #endregion serie


        #region difetto lamentato
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'defect';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'defect';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Difetto Lamentato';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 60;
        $fieldUserRoleSurnameContactDetail->pos_y = 70;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion difetto lamentato 


        #region voltaggio
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'voltage';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'voltage';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Voltaggio';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 70;
        $fieldUserRoleSurnameContactDetail->pos_y = 70;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion voltaggio


         #region note
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'exit_notes';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'exit_notes';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Note';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 80;
        $fieldUserRoleSurnameContactDetail->pos_y = 70;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion note

 #region customer id 
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'customer_id';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'customer_id';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Committente';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 20;
        $fieldUserRoleSurnameContactDetail->pos_y = 20;
        $fieldUserRoleSurnameContactDetail->input_type = 2;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion customer id 

        #region DATA DDT
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'date_ddt';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'date_ddt';
        $fieldSurnameContactDetail->data_type = 2;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Data DDT';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 70;
        $fieldUserRoleSurnameContactDetail->pos_y = 70;
        $fieldUserRoleSurnameContactDetail->input_type = 4;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion DATA DDT 



         #endregion DETAIL

        #endregion GrInterventions

      
    }
}



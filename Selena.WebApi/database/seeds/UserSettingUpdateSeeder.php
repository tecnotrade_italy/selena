<?php

use Illuminate\Database\Seeder;
use App\Dictionary;
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;


class UserSettingUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        //$idMenuAdminCRM = MenuAdmin::where('code', 'CRM')->first()->id;
        $functionalityUserSetting = Functionality::where('code', 'UserSetting')->first()->id;
        
        #region TAB USER

        $tabContentUsers = new Tab();
        $tabContentUsers->code = 'Users';
        $tabContentUsers->functionality_id = $functionalityUserSetting;
        $tabContentUsers->order = 10;
        $tabContentUsers->created_id = $idUser;
        $tabContentUsers->save();

        $languageTabContentUsers = new LanguageTab();
        $languageTabContentUsers->language_id = $idLanguage;
        $languageTabContentUsers->tab_id = $tabContentUsers->id;
        $languageTabContentUsers->description = 'Utenti';
        $languageTabContentUsers->created_id = $idUser;
        $languageTabContentUsers->save();

        #endregion TAB USER

        #region CHECKBOX

        $fieldCheckBoxUsername = new Field();
        $fieldCheckBoxUsername->tab_id = $tabContentUsers->id;
        $fieldCheckBoxUsername->code = 'TableCheckBox';
        $fieldCheckBoxUsername->created_id = $idUser;
        $fieldCheckBoxUsername->save();

        $fieldLanguageCheckBoxUsername = new FieldLanguage();
        $fieldLanguageCheckBoxUsername->field_id = $fieldCheckBoxUsername->id;
        $fieldLanguageCheckBoxUsername->language_id = $idLanguage;
        $fieldLanguageCheckBoxUsername->description = 'CheckBox';
        $fieldLanguageCheckBoxUsername->created_id = $idUser;
        $fieldLanguageCheckBoxUsername->save();

        $fieldUserRoleCheckBoxUsername = new FieldUserRole();
        $fieldUserRoleCheckBoxUsername->field_id = $fieldCheckBoxUsername->id;
        $fieldUserRoleCheckBoxUsername->role_id = $idRole;
        $fieldUserRoleCheckBoxUsername->enabled = true;
        $fieldUserRoleCheckBoxUsername->table_order = 10;
        $fieldUserRoleCheckBoxUsername->input_type = 13;
        $fieldUserRoleCheckBoxUsername->created_id = $idUser;
        $fieldUserRoleCheckBoxUsername->save();

        #endregion CHECKBOX

        #region FORMATTER

        $fieldUserSettingFormatter = new Field();
        $fieldUserSettingFormatter->code = 'UserSettingFormatter';
        $fieldUserSettingFormatter->formatter = 'userFormatter';
        $fieldUserSettingFormatter->tab_id = $tabContentUsers->id;
        $fieldUserSettingFormatter->created_id = $idUser;
        $fieldUserSettingFormatter->save();

        $fieldLanguageUserSettingFormatter = new FieldLanguage();
        $fieldLanguageUserSettingFormatter->field_id = $fieldUserSettingFormatter->id;
        $fieldLanguageUserSettingFormatter->language_id = $idLanguage;
        $fieldLanguageUserSettingFormatter->description = '';
        $fieldLanguageUserSettingFormatter->created_id = $idUser;
        $fieldLanguageUserSettingFormatter->save();

        $fieldUserRoleUserSettingFormatter = new FieldUserRole();
        $fieldUserRoleUserSettingFormatter->field_id = $fieldUserSettingFormatter->id;
        $fieldUserRoleUserSettingFormatter->role_id = $idRole;
        $fieldUserRoleUserSettingFormatter->enabled = true;
        $fieldUserRoleUserSettingFormatter->table_order = 20;
        $fieldUserRoleUserSettingFormatter->input_type = 14;
        $fieldUserRoleUserSettingFormatter->created_id = $idUser;
        $fieldUserRoleUserSettingFormatter->save();

        #endregion FORMATTER

        #region USERNAME

        $fieldUsername = new Field();
        $fieldUsername->code = 'Username';
        $fieldUsername->field = 'username';
        $fieldUsername->data_type = 1;
        $fieldUsername->tab_id = $tabContentUsers->id;
        $fieldUsername->created_id = $idUser;
        $fieldUsername->on_change = 'userHelpers.checkExistUsername';
        $fieldUsername->save();

        $fieldLanguageUsername = new FieldLanguage();
        $fieldLanguageUsername->field_id = $fieldUsername->id;
        $fieldLanguageUsername->language_id = $idLanguage;
        $fieldLanguageUsername->description = 'Username';
        $fieldLanguageUsername->created_id = $idUser;
        $fieldLanguageUsername->save();

        $fieldUserRoleUsername = new FieldUserRole();
        $fieldUserRoleUsername->field_id = $fieldUsername->id;
        $fieldUserRoleUsername->role_id = $idRole;
        $fieldUserRoleUsername->enabled = true;
        $fieldUserRoleUsername->table_order = 30;
        $fieldUserRoleUsername->input_type = 0;
        $fieldUserRoleUsername->pos_x = 10;
        $fieldUserRoleUsername->pos_y = 10;
        $fieldUserRoleUsername->colspan = 4;
        $fieldUserRoleUsername->required = true;
        $fieldUserRoleUsername->created_id = $idUser;
        $fieldUserRoleUsername->save();

        #endregion USERNAME

        #region EMAIL

        $fieldEmail = new Field();
        $fieldEmail->code = 'Email';
        $fieldEmail->field = 'email';
        $fieldEmail->data_type = 1;
        $fieldEmail->tab_id = $tabContentUsers->id;
        $fieldEmail->created_id = $idUser;
        $fieldUsername->on_change = 'userHelpers.checkExistEmail';
        $fieldEmail->save();

        $fieldLanguageEmail = new FieldLanguage();
        $fieldLanguageEmail->field_id = $fieldEmail->id;
        $fieldLanguageEmail->language_id = $idLanguage;
        $fieldLanguageEmail->description = 'Email';
        $fieldLanguageEmail->created_id = $idUser;
        $fieldLanguageEmail->save();

        $fieldUserRoleEmail = new FieldUserRole();
        $fieldUserRoleEmail->field_id = $fieldEmail->id;
        $fieldUserRoleEmail->role_id = $idRole;
        $fieldUserRoleEmail->enabled = true;
        $fieldUserRoleEmail->table_order = 40;
        $fieldUserRoleEmail->input_type = 9;
        $fieldUserRoleEmail->pos_x = 20;
        $fieldUserRoleEmail->pos_y = 10;
        $fieldUserRoleEmail->colspan = 4;
        $fieldUserRoleEmail->required = true;
        $fieldUserRoleEmail->created_id = $idUser;
        $fieldUserRoleEmail->save();

        #endregion EMAIL

        #region PASSWORD

        $fieldPassword = new Field();
        $fieldPassword->code = 'Password';
        $fieldPassword->field = 'password';
        $fieldPassword->data_type = 1;
        $fieldPassword->tab_id = $tabContentUsers->id;
        $fieldPassword->created_id = $idUser;
        $fieldPassword->save();

        $fieldLanguagePassword = new FieldLanguage();
        $fieldLanguagePassword->field_id = $fieldPassword->id;
        $fieldLanguagePassword->language_id = $idLanguage;
        $fieldLanguagePassword->description = 'Password';
        $fieldLanguagePassword->created_id = $idUser;
        $fieldLanguagePassword->save();

        $fieldUserRolePassword = new FieldUserRole();
        $fieldUserRolePassword->field_id = $fieldPassword->id;
        $fieldUserRolePassword->role_id = $idRole;
        $fieldUserRolePassword->enabled = true;
        $fieldUserRolePassword->table_order = 50;
        $fieldUserRolePassword->input_type = 16;
        $fieldUserRolePassword->pos_x = 30;
        $fieldUserRolePassword->pos_y = 10;
        $fieldUserRolePassword->colspan = 4;
        $fieldUserRolePassword->created_id = $idUser;
        $fieldUserRolePassword->save();

        #endregion PASSWORD

        #region LANGUAGE
        $fieldLanguage = new Field();
        $fieldLanguage->code = 'Language';
        $fieldLanguage->field = 'idLanguage';
        $fieldLanguage->data_type = 1;
        $fieldLanguage->tab_id = $tabContentUsers->id;
        $fieldLanguage->service = 'languages';
        $fieldLanguage->created_id = $idUser;
        $fieldLanguage->save();

        $fieldLanguageLanguage = new FieldLanguage();
        $fieldLanguageLanguage->field_id = $fieldLanguage->id;
        $fieldLanguageLanguage->language_id = $idLanguage;
        $fieldLanguageLanguage->description = 'Lingua';
        $fieldLanguageLanguage->created_id = $idUser;
        $fieldLanguageLanguage->save();

        $fieldUserRoleLanguage = new FieldUserRole();
        $fieldUserRoleLanguage->field_id = $fieldLanguage->id;
        $fieldUserRoleLanguage->role_id = $idRole;
        $fieldUserRoleLanguage->enabled = true;
        $fieldUserRoleLanguage->table_order = 60;
        $fieldUserRoleLanguage->input_type = 2;
        $fieldUserRoleLanguage->pos_x = 10;
        $fieldUserRoleLanguage->pos_y = 40;
        $fieldUserRoleLanguage->colspan = 4;
        $fieldUserRoleLanguage->required = true;
        $fieldUserRoleLanguage->created_id = $idUser;
        $fieldUserRoleLanguage->save();
        #endregion LANGUAGE


        #region company
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'company';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'company';
        $fieldSurnameContactDetail->data_type = 3;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Ditta';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 70;
        $fieldUserRoleSurnameContactDetail->input_type = 5;
        $fieldUserRoleSurnameContactDetail->pos_x =50;
        $fieldUserRoleSurnameContactDetail->pos_y = 10;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion company

        #region business_name
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'business_name';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'business_name';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Ragione Sociale';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 80;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->pos_x =60;
        $fieldUserRoleSurnameContactDetail->pos_y = 10;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion business_name

        #region name
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'name';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'name';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Nome';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 90;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->pos_x =10;
        $fieldUserRoleSurnameContactDetail->pos_y = 20;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion name

        #region surname
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'surname';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'surname';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Cognome';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 100;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->pos_x =20;
        $fieldUserRoleSurnameContactDetail->pos_y = 20;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion surname

        #region vat_number
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'vat_number';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'vat_number';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Partita Iva';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 110;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->pos_x =30;
        $fieldUserRoleSurnameContactDetail->pos_y = 20;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion vat_number

        #region fiscal_code
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'fiscal_code';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'fiscal_code';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Codice Fiscale';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 120;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->pos_x =10;
        $fieldUserRoleSurnameContactDetail->pos_y = 30;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion fiscal_code


        #region address
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'address';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'address';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Indirizzo';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 130;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->pos_x =20;
        $fieldUserRoleSurnameContactDetail->pos_y = 30;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion address


        #region postal_code
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'postal_code';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->service = 'postalCode';
        $fieldSurnameContactDetail->field = 'postal_code';
        $fieldSurnameContactDetail->data_type = 0;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Codice Postale';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 140;
        $fieldUserRoleSurnameContactDetail->input_type = 2;
        $fieldUserRoleSurnameContactDetail->pos_x =30;
        $fieldUserRoleSurnameContactDetail->pos_y = 30;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion postal_code


        #region telephone_number
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'telephone_number';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'telephone_number';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Telefono';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 190;
        $fieldUserRoleSurnameContactDetail->input_type = 8;
        $fieldUserRoleSurnameContactDetail->pos_x =20;
        $fieldUserRoleSurnameContactDetail->pos_y = 40;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion telephone_number

        #region fax_number
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'fax_number';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'fax_number';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Fax';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 200;
        $fieldUserRoleSurnameContactDetail->input_type = 8;
        $fieldUserRoleSurnameContactDetail->pos_x =30;
        $fieldUserRoleSurnameContactDetail->pos_y = 40;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion telephone_number

        #region website
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'website';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'website';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Sito Web';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 170;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->pos_x =60;
        $fieldUserRoleSurnameContactDetail->pos_y = 30;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion telephone_number

         #region vat_type_id
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'vat_type_id';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->service = 'vatType';
        $fieldSurnameContactDetail->field = 'vat_type_id';
        $fieldSurnameContactDetail->data_type = 0;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Tipo Iva';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 180;
        $fieldUserRoleSurnameContactDetail->input_type = 2;
        $fieldUserRoleSurnameContactDetail->pos_x =10;
        $fieldUserRoleSurnameContactDetail->pos_y = 40;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion vat_type_id

  
        #region country
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'country';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'country';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Località';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 150;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->pos_x =40;
        $fieldUserRoleSurnameContactDetail->pos_y = 30;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion country

        #region province
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'province';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'province';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Provincia';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 160;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->pos_x =50;
        $fieldUserRoleSurnameContactDetail->pos_y = 30;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion province

       
        #region mobile_number
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'mobile_number';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'mobile_number';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Cellulare';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 210;
        $fieldUserRoleSurnameContactDetail->input_type = 8;
        $fieldUserRoleSurnameContactDetail->pos_x =40;
        $fieldUserRoleSurnameContactDetail->pos_y = 40;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion mobile_number

        #region pec
        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'pec';
        $fieldSurnameContactDetail->tab_id = $tabContentUsers->id;
        $fieldSurnameContactDetail->field = 'pec';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Pec';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->table_order = 220;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->pos_x =50;
        $fieldUserRoleSurnameContactDetail->pos_y = 40;
        $fieldUserRoleSurnameContactDetail->colspan = 4;
        $fieldUserRoleSurnameContactDetail->required = true;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();
        #endregion pec

        #endregion TABLE
        #endregion USER


        #region TAB GROUPS ENABLED

        $tabContentGroupsEnabled = new Tab();
        $tabContentGroupsEnabled->code = 'GroupsEnabled';
        $tabContentGroupsEnabled->functionality_id = $functionalityUserSetting;
        $tabContentGroupsEnabled->order = 20;
        $tabContentGroupsEnabled->created_id = $idUser;
        $tabContentGroupsEnabled->save();

        $languagetabContentGroupsEnabled = new LanguageTab();
        $languagetabContentGroupsEnabled->language_id = $idLanguage;
        $languagetabContentGroupsEnabled->tab_id = $tabContentGroupsEnabled->id;
        $languagetabContentGroupsEnabled->description = 'Gruppi abilitati';
        $languagetabContentGroupsEnabled->created_id = $idUser;
        $languagetabContentGroupsEnabled->save();

        #endregion TAB GROUPS ENABLED

        #region CHECKBOX

        $fieldCheckBoxGroupsEnabled = new Field();
        $fieldCheckBoxGroupsEnabled->tab_id = $tabContentGroupsEnabled->id;
        $fieldCheckBoxGroupsEnabled->code = 'TableCheckBox';
        $fieldCheckBoxGroupsEnabled->created_id = $idUser;
        $fieldCheckBoxGroupsEnabled->save();

        $fieldLanguageCheckBoxGroupsEnabled = new FieldLanguage();
        $fieldLanguageCheckBoxGroupsEnabled->field_id = $fieldCheckBoxGroupsEnabled->id;
        $fieldLanguageCheckBoxGroupsEnabled->language_id = $idLanguage;
        $fieldLanguageCheckBoxGroupsEnabled->description = 'CheckBox';
        $fieldLanguageCheckBoxGroupsEnabled->created_id = $idUser;
        $fieldLanguageCheckBoxGroupsEnabled->save();

        $fieldUserRoleCheckBoxGroupsEnabled = new FieldUserRole();
        $fieldUserRoleCheckBoxGroupsEnabled->field_id = $fieldCheckBoxGroupsEnabled->id;
        $fieldUserRoleCheckBoxGroupsEnabled->role_id = $idRole;
        $fieldUserRoleCheckBoxGroupsEnabled->enabled = true;
        $fieldUserRoleCheckBoxGroupsEnabled->table_order = 10;
        $fieldUserRoleCheckBoxGroupsEnabled->input_type = 13;
        $fieldUserRoleCheckBoxGroupsEnabled->created_id = $idUser;
        $fieldUserRoleCheckBoxGroupsEnabled->save();

        #endregion CHECKBOX

        #region GROUPS ENABLED

        $fieldGroupsEnabled = new Field();
        $fieldGroupsEnabled->code = 'GroupsEnabled';
        $fieldGroupsEnabled->field = 'description';
        $fieldGroupsEnabled->data_type = 1;
        $fieldGroupsEnabled->tab_id = $tabContentGroupsEnabled->id;
        $fieldGroupsEnabled->created_id = $idUser;
        $fieldGroupsEnabled->save();

        $fieldLanguageGroupsEnabled = new FieldLanguage();
        $fieldLanguageGroupsEnabled->field_id = $fieldGroupsEnabled->id;
        $fieldLanguageGroupsEnabled->language_id = $idLanguage;
        $fieldLanguageGroupsEnabled->description = 'Gruppi abilitati';
        $fieldLanguageGroupsEnabled->created_id = $idUser;
        $fieldLanguageGroupsEnabled->save();

        $fieldUserRoleGroupsEnabled = new FieldUserRole();
        $fieldUserRoleGroupsEnabled->field_id = $fieldGroupsEnabled->id;
        $fieldUserRoleGroupsEnabled->role_id = $idRole;
        $fieldUserRoleGroupsEnabled->enabled = true;
        $fieldUserRoleGroupsEnabled->table_order = 20;
        $fieldUserRoleGroupsEnabled->input_type = 15;
        $fieldUserRoleGroupsEnabled->created_id = $idUser;
        $fieldUserRoleGroupsEnabled->save();

        #endregion GROUPS ENABLED

        #region TAB GROUPS DISABLED

        $tabContentGroupsDisabled = new Tab();
        $tabContentGroupsDisabled->code = 'GroupsDisabled';
        $tabContentGroupsDisabled->functionality_id = $functionalityUserSetting;
        $tabContentGroupsDisabled->order = 20;
        $tabContentGroupsDisabled->created_id = $idUser;
        $tabContentGroupsDisabled->save();

        $languagetabContentGroupsDisabled = new LanguageTab();
        $languagetabContentGroupsDisabled->language_id = $idLanguage;
        $languagetabContentGroupsDisabled->tab_id = $tabContentGroupsDisabled->id;
        $languagetabContentGroupsDisabled->description = 'Gruppi disabilitati';
        $languagetabContentGroupsDisabled->created_id = $idUser;
        $languagetabContentGroupsDisabled->save();

        #endregion TAB GROUPS DISABLED

        #region CHECKBOX

        $fieldCheckBoxGroupsDisabled = new Field();
        $fieldCheckBoxGroupsDisabled->tab_id = $tabContentGroupsDisabled->id;
        $fieldCheckBoxGroupsDisabled->code = 'TableCheckBox';
        $fieldCheckBoxGroupsDisabled->created_id = $idUser;
        $fieldCheckBoxGroupsDisabled->save();

        $fieldLanguageCheckBoxGroupsDisabled = new FieldLanguage();
        $fieldLanguageCheckBoxGroupsDisabled->field_id = $fieldCheckBoxGroupsDisabled->id;
        $fieldLanguageCheckBoxGroupsDisabled->language_id = $idLanguage;
        $fieldLanguageCheckBoxGroupsDisabled->description = 'CheckBox';
        $fieldLanguageCheckBoxGroupsDisabled->created_id = $idUser;
        $fieldLanguageCheckBoxGroupsDisabled->save();

        $fieldUserRoleCheckBoxGroupsDisabled = new FieldUserRole();
        $fieldUserRoleCheckBoxGroupsDisabled->field_id = $fieldCheckBoxGroupsDisabled->id;
        $fieldUserRoleCheckBoxGroupsDisabled->role_id = $idRole;
        $fieldUserRoleCheckBoxGroupsDisabled->enabled = true;
        $fieldUserRoleCheckBoxGroupsDisabled->table_order = 10;
        $fieldUserRoleCheckBoxGroupsDisabled->input_type = 13;
        $fieldUserRoleCheckBoxGroupsDisabled->created_id = $idUser;
        $fieldUserRoleCheckBoxGroupsDisabled->save();

        #endregion CHECKBOX

        #region GROUPS DISABLED

        $fieldGroupsDisabled = new Field();
        $fieldGroupsDisabled->code = 'GroupsEnabled';
        $fieldGroupsDisabled->field = 'description';
        $fieldGroupsDisabled->data_type = 1;
        $fieldGroupsDisabled->tab_id = $tabContentGroupsDisabled->id;
        $fieldGroupsDisabled->created_id = $idUser;
        $fieldGroupsDisabled->save();

        $fieldLanguageGroupsDisabled = new FieldLanguage();
        $fieldLanguageGroupsDisabled->field_id = $fieldGroupsDisabled->id;
        $fieldLanguageGroupsDisabled->language_id = $idLanguage;
        $fieldLanguageGroupsDisabled->description = 'Gruppi disabilitati';
        $fieldLanguageGroupsDisabled->created_id = $idUser;
        $fieldLanguageGroupsDisabled->save();

        $fieldUserRoleGroupsDisabled = new FieldUserRole();
        $fieldUserRoleGroupsDisabled->field_id = $fieldGroupsDisabled->id;
        $fieldUserRoleGroupsDisabled->role_id = $idRole;
        $fieldUserRoleGroupsDisabled->enabled = true;
        $fieldUserRoleGroupsDisabled->table_order = 20;
        $fieldUserRoleGroupsDisabled->input_type = 15;
        $fieldUserRoleGroupsDisabled->created_id = $idUser;
        $fieldUserRoleGroupsDisabled->save();
        #endregion GROUPS DISABLED

    }
}

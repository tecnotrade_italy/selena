<?php

use App\Role as Role;
use App\User as User;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Role();
        $admin->name = 'admin';
        $admin->display_name = 'User Administrator'; // optional
        $admin->description = 'User is allowed to manage and edit other users'; // optional
        $admin->created_id = User::where('name', '=', 'admin')->first()->id;
        $admin->save();

        $customer = new Role();
        $customer->name = 'customer';
        $customer->display_name = 'User Customer'; // optional
        $customer->description = 'User is allowed to access'; // optional
        $customer->created_id = User::where('name', '=', 'admin')->first()->id;
        $customer->save();

        $user = User::where('name', '=', 'admin')->firstOrFail();
        $user->attachRole($admin);
    }
}

<?php

use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Icon;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Tab;
use Illuminate\Database\Seeder;
use App\Language;
use App\Role;
use App\User;
use App\Functionality;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        #region PAGES
        $functionality = new Functionality();
        $functionality->code = 'Blog';
        $functionality->created_id = $idUser;
        $functionality->save();

        $icon = new Icon();
        $icon->description = 'Paper plane';
        $icon->css = 'fa fa-paper-plane-o';
        $icon->created_id = $idUser;
        $icon->save();

        $menuAdmin = new MenuAdmin();
        $menuAdmin->code = 'Blog';
        $menuAdmin->functionality_id = $functionality->id;
        $menuAdmin->icon_id = $icon->id;
        $menuAdmin->url = '/admin/blog';
        $menuAdmin->created_id = $idUser;
        $menuAdmin->save();

        $languageMenuAdmin = new LanguageMenuAdmin();
        $languageMenuAdmin->language_id = $idLanguage;
        $languageMenuAdmin->menu_admin_id = $menuAdmin->id;
        $languageMenuAdmin->description = 'Blog';
        $languageMenuAdmin->created_id = $idUser;
        $languageMenuAdmin->save();

        $menuAdminUserRole = new MenuAdminUserRole();
        $menuAdminUserRole->menu_admin_id = $menuAdmin->id;
        $menuAdminUserRole->role_id = $idRole;
        $menuAdminUserRole->enabled = true;
        $menuAdminUserRole->order = 20;
        $menuAdminUserRole->created_id = $idUser;
        $menuAdminUserRole->save();

        # region TABS
        $tabContent = new Tab();
        $tabContent->code = 'Content';
        $tabContent->functionality_id = $functionality->id;
        $tabContent->order = 10;
        $tabContent->created_id = $idUser;
        $tabContent->save();

        $languageTabContent = new LanguageTab();
        $languageTabContent->language_id = $idLanguage;
        $languageTabContent->tab_id = $tabContent->id;
        $languageTabContent->description = 'Contenuto';
        $languageTabContent->created_id = $idUser;
        $languageTabContent->save();

        $tabSettings = new Tab();
        $tabSettings->code = 'Settings';
        $tabSettings->functionality_id = $functionality->id;
        $tabSettings->order = 20;
        $tabSettings->created_id = $idUser;
        $tabSettings->save();

        $languageTabSettings = new LanguageTab();
        $languageTabSettings->language_id = $idLanguage;
        $languageTabSettings->tab_id = $tabSettings->id;
        $languageTabSettings->description = 'Impostazioni';
        $languageTabSettings->created_id = $idUser;
        $languageTabSettings->save();
        #end region TABS

        #region Content
        $fieldContent = new Field();
        $fieldContent->code = 'Content';
        $fieldContent->tab_id = $tabContent->id;
        $fieldContent->field = 'languagePageSetting.content';
        $fieldContent->data_type = 1;
        $fieldContent->required = true;
        $fieldContent->created_id = $idUser;
        $fieldContent->save();

        $fieldLanguageContent = new FieldLanguage();
        $fieldLanguageContent->field_id = $fieldContent->id;
        $fieldLanguageContent->language_id = $idLanguage;
        $fieldLanguageContent->description = 'Contenuto';
        $fieldLanguageContent->created_id = $idUser;
        $fieldLanguageContent->save();

        $fieldUserRoleContent = new FieldUserRole();
        $fieldUserRoleContent->field_id = $fieldContent->id;
        $fieldUserRoleContent->role_id = $idRole;
        $fieldUserRoleContent->enabled = true;
        $fieldUserRoleContent->input_type = 19;
        $fieldUserRoleContent->pos_x = 10;
        $fieldUserRoleContent->pos_y = 10;
        $fieldUserRoleContent->colspan = 12;
        $fieldUserRoleContent->created_id = $idUser;
        $fieldUserRoleContent->save();
        #endregion Content

        #region Title
        $fieldTitlePage = new Field();
        $fieldTitlePage->code = 'TitlePage';
        $fieldTitlePage->tab_id = $tabSettings->id;
        $fieldTitlePage->field = 'languagePageSetting.title';
        $fieldTitlePage->formatter = 'titleFormatter';
        $fieldTitlePage->data_type = 1;
        $fieldTitlePage->required = true;
        $fieldTitlePage->max_length = 255;
        $fieldTitlePage->on_change = 'changeTitle';
        $fieldTitlePage->created_id = $idUser;
        $fieldTitlePage->save();

        $fieldLanguageTitlePage = new FieldLanguage();
        $fieldLanguageTitlePage->field_id = $fieldTitlePage->id;
        $fieldLanguageTitlePage->language_id = $idLanguage;
        $fieldLanguageTitlePage->description = 'Titolo';
        $fieldLanguageTitlePage->created_id = $idUser;
        $fieldLanguageTitlePage->save();

        $fieldUserRoleTitlePage = new FieldUserRole();
        $fieldUserRoleTitlePage->field_id = $fieldTitlePage->id;
        $fieldUserRoleTitlePage->role_id = $idRole;
        $fieldUserRoleTitlePage->enabled = true;
        $fieldUserRoleTitlePage->input_type = 0;
        $fieldUserRoleTitlePage->pos_x = 10;
        $fieldUserRoleTitlePage->pos_y = 10;
        $fieldUserRoleTitlePage->table_order = 20;
        $fieldUserRoleTitlePage->required = true;
        $fieldUserRoleTitlePage->colspan = 12;
        $fieldUserRoleTitlePage->created_id = $idUser;
        $fieldUserRoleTitlePage->save();
        #endregion Title

        #region SeoDescription
        $fieldSeoDescription = new Field();
        $fieldSeoDescription->code = 'SeoDescription';
        $fieldSeoDescription->tab_id = $tabSettings->id;
        $fieldSeoDescription->field = 'languagePageSetting.seoDescription';
        $fieldSeoDescription->data_type = 1;
        $fieldSeoDescription->max_length = 230;
        $fieldSeoDescription->created_id = $idUser;
        $fieldSeoDescription->save();

        $fieldLanguageSeoDescription = new FieldLanguage();
        $fieldLanguageSeoDescription->field_id = $fieldSeoDescription->id;
        $fieldLanguageSeoDescription->language_id = $idLanguage;
        $fieldLanguageSeoDescription->description = 'Descrizione SEO';
        $fieldLanguageSeoDescription->created_id = $idUser;
        $fieldLanguageSeoDescription->save();

        $fieldUserRoleSeoDescription = new FieldUserRole();
        $fieldUserRoleSeoDescription->field_id = $fieldSeoDescription->id;
        $fieldUserRoleSeoDescription->role_id = $idRole;
        $fieldUserRoleSeoDescription->enabled = true;
        $fieldUserRoleSeoDescription->input_type = 0;
        $fieldUserRoleSeoDescription->pos_x = 10;
        $fieldUserRoleSeoDescription->pos_y = 20;
        $fieldUserRoleSeoDescription->colspan = 12;
        $fieldUserRoleSeoDescription->created_id = $idUser;
        $fieldUserRoleSeoDescription->save();
        #endregion SeoDescription

          #region Url
          $fieldUrl = new Field();
          $fieldUrl->code = 'Url';
          $fieldUrl->tab_id = $tabSettings->id;
          $fieldUrl->field = 'languagePageSetting.url';
          $fieldUrl->formatter = 'urlFormatter';
          $fieldUrl->data_type = 1;
          $fieldUrl->max_length = 255;
          $fieldUrl->on_change = 'checkUrl';
          $fieldUrl->created_id = $idUser;
          $fieldUrl->save();
  
          $fieldLanguageUrl = new FieldLanguage();
          $fieldLanguageUrl->field_id = $fieldUrl->id;
          $fieldLanguageUrl->language_id = $idLanguage;
          $fieldLanguageUrl->description = 'Url';
          $fieldLanguageUrl->created_id = $idUser;
          $fieldLanguageUrl->save();
  
          $fieldUserRoleUrl = new FieldUserRole();
          $fieldUserRoleUrl->field_id = $fieldUrl->id;
          $fieldUserRoleUrl->role_id = $idRole;
          $fieldUserRoleUrl->enabled = true;
          $fieldUserRoleUrl->input_type = 0;
          $fieldUserRoleUrl->pos_x = 10;
          $fieldUserRoleUrl->pos_y = 30;
          $fieldUserRoleUrl->table_order = 30;
          $fieldUserRoleUrl->colspan = 12;
          $fieldUserRoleUrl->created_id = $idUser;
          $fieldUserRoleUrl->save();
          #endregion Url

  #region UrlCanonical
  $fieldUrl = new Field();
  $fieldUrl->code = 'UrlCanonical';
  $fieldUrl->tab_id = $tabSettings->id;
  $fieldUrl->field = 'languagePageSetting.url';
  $fieldUrl->formatter = 'urlFormatter';
  $fieldUrl->data_type = 1;
  $fieldUrl->max_length = 255;
  $fieldUrl->on_change = 'checkUrl';
  $fieldUrl->created_id = $idUser;
  $fieldUrl->save();

  $fieldLanguageUrl = new FieldLanguage();
  $fieldLanguageUrl->field_id = $fieldUrl->id;
  $fieldLanguageUrl->language_id = $idLanguage;
  $fieldLanguageUrl->description = 'Url';
  $fieldLanguageUrl->created_id = $idUser;
  $fieldLanguageUrl->save();

  $fieldUserRoleUrl = new FieldUserRole();
  $fieldUserRoleUrl->field_id = $fieldUrl->id;
  $fieldUserRoleUrl->role_id = $idRole;
  $fieldUserRoleUrl->enabled = true;
  $fieldUserRoleUrl->input_type = 0;
  $fieldUserRoleUrl->pos_x = 10;
  $fieldUserRoleUrl->pos_y = 40;
  $fieldUserRoleUrl->table_order = 40;
  $fieldUserRoleUrl->colspan = 12;
  $fieldUserRoleUrl->created_id = $idUser;
  $fieldUserRoleUrl->save();
  #endregion UrlCanonical

    #region SeoKeyword
    $fieldSeoKeyword = new Field();
    $fieldSeoKeyword->code = 'SeoKeyword';
    $fieldSeoKeyword->tab_id = $tabSettings->id;
    $fieldSeoKeyword->field = 'languagePageSetting.seoKeyword';
    $fieldSeoKeyword->data_type = 1;
    $fieldSeoKeyword->created_id = $idUser;
    $fieldSeoKeyword->save();

    $fieldLanguageSeoKeyword = new FieldLanguage();
    $fieldLanguageSeoKeyword->field_id = $fieldSeoKeyword->id;
    $fieldLanguageSeoKeyword->language_id = $idLanguage;
    $fieldLanguageSeoKeyword->description = 'Parole chiave SEO';
    $fieldLanguageSeoKeyword->created_id = $idUser;
    $fieldLanguageSeoKeyword->save();

    $fieldUserRoleSeoKeyword = new FieldUserRole();
    $fieldUserRoleSeoKeyword->field_id = $fieldSeoKeyword->id;
    $fieldUserRoleSeoKeyword->role_id = $idRole;
    $fieldUserRoleSeoKeyword->enabled = true;
    $fieldUserRoleSeoKeyword->input_type = 1;
    $fieldUserRoleSeoKeyword->pos_x = 10;
    $fieldUserRoleSeoKeyword->pos_y = 50;
    $fieldUserRoleSeoKeyword->colspan = 12;
    $fieldUserRoleSeoKeyword->created_id = $idUser;
    $fieldUserRoleSeoKeyword->save();
    #endregion SeoKeyword


       #region Author
       $fieldAuthor = new Field();
       $fieldAuthor->code = 'Author';
       $fieldAuthor->tab_id = $tabSettings->id;
       $fieldAuthor->field = 'idAuthor';
       $fieldAuthor->data_type = 0;
       $fieldAuthor->service = 'users';
       $fieldAuthor->created_id = $idUser;
       $fieldAuthor->save();

       $fieldLanguageAuthor = new FieldLanguage();
       $fieldLanguageAuthor->field_id = $fieldAuthor->id;
       $fieldLanguageAuthor->language_id = $idLanguage;
       $fieldLanguageAuthor->description = 'Autore';
       $fieldLanguageAuthor->created_id = $idUser;
       $fieldLanguageAuthor->save();

       $fieldUserRoleAuthor = new FieldUserRole();
       $fieldUserRoleAuthor->field_id = $fieldAuthor->id;
       $fieldUserRoleAuthor->role_id = $idRole;
       $fieldUserRoleAuthor->enabled = true;
       $fieldUserRoleAuthor->input_type = 2;
       $fieldUserRoleAuthor->pos_x = 10;
       $fieldUserRoleAuthor->pos_y = 60;
       $fieldUserRoleAuthor->colspan = 12;
       $fieldUserRoleAuthor->created_id = $idUser;
       $fieldUserRoleAuthor->save();
       #endregion Author

  #region PageCategory
  $fieldPageCategory = new Field();
  $fieldPageCategory->code = 'PageCategory';
  $fieldPageCategory->tab_id = $tabSettings->id;
  $fieldPageCategory->field = 'idPageCategory';
  $fieldPageCategory->data_type = 0;
  $fieldPageCategory->service = 'pageCategory';
  $fieldPageCategory->created_id = $idUser;
  $fieldPageCategory->save();

  $fieldLanguagePageCategory = new FieldLanguage();
  $fieldLanguagePageCategory->field_id = $fieldPageCategory->id;
  $fieldLanguagePageCategory->language_id = $idLanguage;
  $fieldLanguagePageCategory->description = 'Categoria';
  $fieldLanguagePageCategory->created_id = $idUser;
  $fieldLanguagePageCategory->save();

  $fieldUserRolePageCategory = new FieldUserRole();
  $fieldUserRolePageCategory->field_id = $fieldPageCategory->id;
  $fieldUserRolePageCategory->role_id = $idRole;
  $fieldUserRolePageCategory->enabled = true;
  $fieldUserRolePageCategory->input_type = 2;
  $fieldUserRolePageCategory->pos_x = 10;
  $fieldUserRolePageCategory->pos_y = 70;
  $fieldUserRolePageCategory->colspan = 12;
  $fieldUserRolePageCategory->created_id = $idUser;
  $fieldUserRolePageCategory->save();
  #endregion PageCategory


        #region PublishDate
        $field = new Field();
        $field->code = 'Publish';
        $field->tab_id = $tabSettings->id;
        $field->field = 'publish';
        $field->data_type = 2;
        $field->created_id = $idUser;
        $field->save();

        $fieldLanguage = new FieldLanguage();
        $fieldLanguage->field_id = $field->id;
        $fieldLanguage->language_id = $idLanguage;
        $fieldLanguage->description = 'Data di pubblicazione';
        $fieldLanguage->created_id = $idUser;
        $fieldLanguage->save();

        $fieldUserRole = new FieldUserRole();
        $fieldUserRole->field_id = $field->id;
        $fieldUserRole->role_id = $idRole;
        $fieldUserRole->enabled = true;
        $fieldUserRole->input_type = 3;
        $fieldUserRole->pos_x = 10;
        $fieldUserRole->pos_y = 80;
        $fieldUserRole->colspan = 12;
        $fieldUserRole->created_id = $idUser;
        $fieldUserRole->save();
        #endregion PublishDate



  #region VisibleFrom
  $fieldVisibleFrom = new Field();
  $fieldVisibleFrom->code = 'VisibleFrom';
  $fieldVisibleFrom->tab_id = $tabSettings->id;
  $fieldVisibleFrom->field = 'visibleFrom';
  $fieldVisibleFrom->data_type = 2;
  $fieldVisibleFrom->created_id = $idUser;
  $fieldVisibleFrom->save();

  $fieldLanguageVisibleFrom = new FieldLanguage();
  $fieldLanguageVisibleFrom->field_id = $fieldVisibleFrom->id;
  $fieldLanguageVisibleFrom->language_id = $idLanguage;
  $fieldLanguageVisibleFrom->description = 'Visibile dal';
  $fieldLanguageVisibleFrom->created_id = $idUser;
  $fieldLanguageVisibleFrom->save();

  $fieldUserRoleVisibleFrom = new FieldUserRole();
  $fieldUserRoleVisibleFrom->field_id = $fieldVisibleFrom->id;
  $fieldUserRoleVisibleFrom->role_id = $idRole;
  $fieldUserRoleVisibleFrom->enabled = true;
  $fieldUserRoleVisibleFrom->input_type = 4;
  $fieldUserRoleVisibleFrom->pos_x = 10;
  $fieldUserRoleVisibleFrom->pos_y = 90;
  $fieldUserRoleVisibleFrom->colspan = 12;
  $fieldUserRoleVisibleFrom->created_id = $idUser;
  $fieldUserRoleVisibleFrom->save();
  #endregion VisibleFrom

  #region VisibleEnd
  $fieldVisibleEnd = new Field();
  $fieldVisibleEnd->code = 'VisibleEnd';
  $fieldVisibleEnd->tab_id = $tabSettings->id;
  $fieldVisibleEnd->field = 'visibleEnd';
  $fieldVisibleEnd->data_type = 2;
  $fieldVisibleEnd->created_id = $idUser;
  $fieldVisibleEnd->save();

  $fieldLanguageVisibleEnd = new FieldLanguage();
  $fieldLanguageVisibleEnd->field_id = $fieldVisibleEnd->id;
  $fieldLanguageVisibleEnd->language_id = $idLanguage;
  $fieldLanguageVisibleEnd->description = 'Fino al';
  $fieldLanguageVisibleEnd->created_id = $idUser;
  $fieldLanguageVisibleEnd->save();

  $fieldUserRoleVisibleEnd = new FieldUserRole();
  $fieldUserRoleVisibleEnd->field_id = $fieldVisibleEnd->id;
  $fieldUserRoleVisibleEnd->role_id = $idRole;
  $fieldUserRoleVisibleEnd->enabled = true;
  $fieldUserRoleVisibleEnd->input_type = 3;
  $fieldUserRoleVisibleEnd->pos_x = 10;
  $fieldUserRoleVisibleEnd->pos_y = 100;
  $fieldUserRoleVisibleEnd->colspan = 12;
  $fieldUserRoleVisibleEnd->created_id = $idUser;
  $fieldUserRoleVisibleEnd->save();
  #endregion VisibleEnd

     #region FixedPost
     $fieldFixedPost = new Field();
     $fieldFixedPost->code = 'Fixed';
     $fieldFixedPost->tab_id = $tabSettings->id;
     $fieldFixedPost->field = 'fixed';
     $fieldFixedPost->data_type = 3;
     $fieldFixedPost->default_value = false;
     $fieldFixedPost->created_id = $idUser;
     $fieldFixedPost->save();

     $fieldLanguageFixedPost = new FieldLanguage();
     $fieldLanguageFixedPost->field_id = $fieldFixedPost->id;
     $fieldLanguageFixedPost->language_id = $idLanguage;
     $fieldLanguageFixedPost->description = 'Fissa il post nella parte superiore della pagina';
     $fieldLanguageFixedPost->created_id = $idUser;
     $fieldLanguageFixedPost->save();

     $fieldUserRoleFixedPost = new FieldUserRole();
     $fieldUserRoleFixedPost->field_id = $fieldFixedPost->id;
     $fieldUserRoleFixedPost->role_id = $idRole;
     $fieldUserRoleFixedPost->enabled = true;
     $fieldUserRoleFixedPost->input_type = 5;
     $fieldUserRoleFixedPost->pos_x = 10;
     $fieldUserRoleFixedPost->pos_y = 110;
     $fieldUserRoleFixedPost->colspan = 12;
     $fieldUserRoleFixedPost->created_id = $idUser;
     $fieldUserRoleFixedPost->save();
     #endregion FixedPost

     #region FixedPostEndDate
     $fieldFixedPostEndDate = new Field();
     $fieldFixedPostEndDate->code = 'FixedEnd';
     $fieldFixedPostEndDate->tab_id = $tabSettings->id;
     $fieldFixedPostEndDate->field = 'fixedEnd';
     $fieldFixedPostEndDate->data_type = 0;
     $fieldFixedPostEndDate->service = 'users';
     $fieldFixedPostEndDate->created_id = $idUser;
     $fieldFixedPostEndDate->save();

     $fieldLanguageFixedPostEndDate = new FieldLanguage();
     $fieldLanguageFixedPostEndDate->field_id = $fieldFixedPostEndDate->id;
     $fieldLanguageFixedPostEndDate->language_id = $idLanguage;
     $fieldLanguageFixedPostEndDate->description = 'Data di fine post in alto';
     $fieldLanguageFixedPostEndDate->created_id = $idUser;
     $fieldLanguageFixedPostEndDate->save();

     $fieldUserRoleFixedPostEndDate = new FieldUserRole();
     $fieldUserRoleFixedPostEndDate->field_id = $fieldFixedPostEndDate->id;
     $fieldUserRoleFixedPostEndDate->role_id = $idRole;
     $fieldUserRoleFixedPostEndDate->enabled = true;
     $fieldUserRoleFixedPostEndDate->input_type = 3;
     $fieldUserRoleFixedPostEndDate->pos_x = 10;
     $fieldUserRoleFixedPostEndDate->pos_y = 120;
     $fieldUserRoleFixedPostEndDate->colspan = 12;
     $fieldUserRoleFixedPostEndDate->created_id = $idUser;
     $fieldUserRoleFixedPostEndDate->save();
     #endregion FixedPostEndDate

    #region ShareTitle
    $fieldShareTitle = new Field();
    $fieldShareTitle->code = 'ShareTitle';
    $fieldShareTitle->tab_id = $tabSettings->id;
    $fieldShareTitle->field = 'languagePageSetting.shareTitle';
    $fieldShareTitle->data_type = 1;
    $fieldShareTitle->max_length = 60;
    $fieldShareTitle->created_id = $idUser;
    $fieldShareTitle->save();

    $fieldLanguageShareTitle = new FieldLanguage();
    $fieldLanguageShareTitle->field_id = $fieldShareTitle->id;
    $fieldLanguageShareTitle->language_id = $idLanguage;
    $fieldLanguageShareTitle->description = 'Titolo condivisione';
    $fieldLanguageShareTitle->created_id = $idUser;
    $fieldLanguageShareTitle->save();

    $fieldUserRoleShareTitle = new FieldUserRole();
    $fieldUserRoleShareTitle->field_id = $fieldShareTitle->id;
    $fieldUserRoleShareTitle->role_id = $idRole;
    $fieldUserRoleShareTitle->enabled = true;
    $fieldUserRoleShareTitle->input_type = 0;
    $fieldUserRoleShareTitle->pos_x = 10;
    $fieldUserRoleShareTitle->pos_y = 130;
    $fieldUserRoleShareTitle->colspan = 12;
    $fieldUserRoleShareTitle->created_id = $idUser;
    $fieldUserRoleShareTitle->save();
    #endregion ShareTitle

    #region ShareDescription
    $fieldShareDescription = new Field();
    $fieldShareDescription->code = 'ShareDescription';
    $fieldShareDescription->tab_id = $tabSettings->id;
    $fieldShareDescription->field = 'languagePageSetting.shareDescription';
    $fieldShareDescription->data_type = 1;
    $fieldShareDescription->max_length = 230;
    $fieldShareDescription->created_id = $idUser;
    $fieldShareDescription->save();

    $fieldLanguageShareDescription = new FieldLanguage();
    $fieldLanguageShareDescription->field_id = $fieldShareDescription->id;
    $fieldLanguageShareDescription->language_id = $idLanguage;
    $fieldLanguageShareDescription->description = 'Descrizione condivisione';
    $fieldLanguageShareDescription->created_id = $idUser;
    $fieldLanguageShareDescription->save();

    $fieldUserRoleShareDescription = new FieldUserRole();
    $fieldUserRoleShareDescription->field_id = $fieldShareDescription->id;
    $fieldUserRoleShareDescription->role_id = $idRole;
    $fieldUserRoleShareDescription->enabled = true;
    $fieldUserRoleShareDescription->input_type = 0;
    $fieldUserRoleShareDescription->pos_x = 10;
    $fieldUserRoleShareDescription->pos_y = 140;
    $fieldUserRoleShareDescription->colspan = 12;
    $fieldUserRoleShareDescription->created_id = $idUser;
    $fieldUserRoleShareDescription->save();
    #endregion ShareDescription

   #region UrlCoverImage
   $fieldCoverImage = new Field();
   $fieldCoverImage->code = 'CoverImage';
   $fieldCoverImage->tab_id = $tabSettings->id;
   $fieldCoverImage->field = 'languagePageSetting.urlCoverImage';
   $fieldCoverImage->data_type = 1;
   $fieldCoverImage->max_length = 255;
   $fieldCoverImage->created_id = $idUser;
   $fieldCoverImage->save();

   $fieldLanguageCoverImage = new FieldLanguage();
   $fieldLanguageCoverImage->field_id = $fieldCoverImage->id;
   $fieldLanguageCoverImage->language_id = $idLanguage;
   $fieldLanguageCoverImage->description = 'Url immagine condivisione';
   $fieldLanguageCoverImage->created_id = $idUser;
   $fieldLanguageCoverImage->save();

   $fieldUserRoleCoverImage = new FieldUserRole();
   $fieldUserRoleCoverImage->field_id = $fieldCoverImage->id;
   $fieldUserRoleCoverImage->role_id = $idRole;
   $fieldUserRoleCoverImage->enabled = true;
   $fieldUserRoleCoverImage->input_type = 0;
   $fieldUserRoleCoverImage->pos_x = 10;
   $fieldUserRoleCoverImage->pos_y = 150;
   $fieldUserRoleCoverImage->colspan = 12;
   $fieldUserRoleCoverImage->created_id = $idUser;
   $fieldUserRoleCoverImage->save();
   #endregion UrlCoverImage

   #region IdLanguage

   $fieldIdLanguage = new Field();
   $fieldIdLanguage->code = 'IdLanguage';
   $fieldIdLanguage->tab_id = $tabSettings->id;
   $fieldIdLanguage->field = 'languagePageSetting.idLanguage';
   $fieldIdLanguage->data_type = 0;
   $fieldIdLanguage->created_id = $idUser;
   $fieldIdLanguage->save();

   $fieldLanguageIdLanguage = new FieldLanguage();
   $fieldLanguageIdLanguage->field_id = $fieldIdLanguage->id;
   $fieldLanguageIdLanguage->language_id = $idLanguage;
   $fieldLanguageIdLanguage->description = 'Lingua';
   $fieldLanguageIdLanguage->created_id = $idUser;
   $fieldLanguageIdLanguage->save();

   $fieldUserRoleIdLanguage = new FieldUserRole();
   $fieldUserRoleIdLanguage->field_id = $fieldIdLanguage->id;
   $fieldUserRoleIdLanguage->role_id = $idRole;
   $fieldUserRoleIdLanguage->enabled = true;
   $fieldUserRoleIdLanguage->input_type = 19;
   $fieldUserRoleIdLanguage->pos_x = 10;
   $fieldUserRoleIdLanguage->pos_y = 160;
   $fieldUserRoleIdLanguage->created_id = $idUser;
   $fieldUserRoleIdLanguage->save();

   #endregion IdLanguage


   #region CommandNews

   $fieldCommandNews = new Field();
   $fieldCommandNews->code = 'CommandTable';
   $fieldCommandNews->tab_id = $tabSettings->id;
   $fieldCommandNews->formatter = 'tableFormatter';
   $fieldCommandNews->created_id = $idUser;
   $fieldCommandNews->save();

   $fieldLanguageCommandNews = new FieldLanguage();
   $fieldLanguageCommandNews->field_id = $fieldCommandNews->id;
   $fieldLanguageCommandNews->language_id = $idLanguage;
   $fieldLanguageCommandNews->description = 'Lingua';
   $fieldLanguageCommandNews->created_id = $idUser;
   $fieldLanguageCommandNews->save();

   $fieldUserRoleCommandNews = new FieldUserRole();
   $fieldUserRoleCommandNews->field_id = $fieldCommandNews->id;
   $fieldUserRoleCommandNews->role_id = $idRole;
   $fieldUserRoleCommandNews->enabled = true;
   $fieldUserRoleCommandNews->table_order = 10;
   $fieldUserRoleCommandNews->input_type = 14;
   $fieldUserRoleCommandNews->created_id = $idUser;
   $fieldUserRoleCommandNews->save();

   #endregion CommandNews


    }
}

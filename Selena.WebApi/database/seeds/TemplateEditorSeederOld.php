<?php

use App\Language;
use App\TemplateEditor;
use App\TemplateEditorGroup;
use App\TemplateEditorType;
use App\User;
use Illuminate\Database\Seeder;

class TemplateEditorSeederOld extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idUser = User::first()->id;

        #region TEMPLATE EDITOR TYPE

        $templateEditoTypeHeader = new TemplateEditorType();
        $templateEditoTypeHeader->code = 'Header';
        $templateEditoTypeHeader->description = 'Header';
        $templateEditoTypeHeader->created_id = $idUser;
        $templateEditoTypeHeader->save();

        $templateEditoTypeFooter = new TemplateEditorType();
        $templateEditoTypeFooter->code = 'Footer';
        $templateEditoTypeFooter->description = 'Footer';
        $templateEditoTypeFooter->created_id = $idUser;
        $templateEditoTypeFooter->save();

        $templateEditoTypePages = new TemplateEditorType();
        $templateEditoTypePages->code = 'Pages';
        $templateEditoTypePages->description = 'Pagine';
        $templateEditoTypePages->created_id = $idUser;
        $templateEditoTypePages->save();

        $templateEditoTypeEmail = new TemplateEditorType();
        $templateEditoTypeEmail->code = 'Email';
        $templateEditoTypeEmail->description = 'Email';
        $templateEditoTypeEmail->created_id = $idUser;
        $templateEditoTypeEmail->save();

        $templateEditoTypeBlog = new TemplateEditorType();
        $templateEditoTypeBlog->code = 'Blog';
        $templateEditoTypeBlog->description = 'Blog';
        $templateEditoTypeBlog->created_id = $idUser;
        $templateEditoTypeBlog->save();

        #endregion TEMPLATE EDITOR TYPE

        #region TEMPLATE EDITOR GROUP

        $templateEditorGroupHome = new TemplateEditorGroup();
        $templateEditorGroupHome->code = 'Home';
        $templateEditorGroupHome->description = 'Home';
        $templateEditorGroupHome->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupHome->created_id = $idUser;
        $templateEditorGroupHome->save();

        $templateEditorGroupTitolo = new TemplateEditorGroup();
        $templateEditorGroupTitolo->code = 'Titolo';
        $templateEditorGroupTitolo->description = 'Titolo';
        $templateEditorGroupTitolo->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupTitolo->created_id = $idUser;
        $templateEditorGroupTitolo->save();

        $templateEditorGroupFoto = new TemplateEditorGroup();
        $templateEditorGroupFoto->code = 'Foto';
        $templateEditorGroupFoto->description = 'Foto';
        $templateEditorGroupFoto->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupFoto->created_id = $idUser;
        $templateEditorGroupFoto->save();

        $templateEditorGroupContatti = new TemplateEditorGroup();
        $templateEditorGroupContatti->code = 'Contatti';
        $templateEditorGroupContatti->description = 'Contatti';
        $templateEditorGroupContatti->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupContatti->created_id = $idUser;
        $templateEditorGroupContatti->save();

        $templateEditorGroupProdotto = new TemplateEditorGroup();
        $templateEditorGroupProdotto->code = 'Prodotto';
        $templateEditorGroupProdotto->description = 'Prodotti';
        $templateEditorGroupProdotto->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupProdotto->created_id = $idUser;
        $templateEditorGroupProdotto->save();

        $templateEditorGroupCaratteristiche = new TemplateEditorGroup();
        $templateEditorGroupCaratteristiche->code = 'Caratteristiche';
        $templateEditorGroupCaratteristiche->description = 'Caratteristiche';
        $templateEditorGroupCaratteristiche->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupCaratteristiche->created_id = $idUser;
        $templateEditorGroupCaratteristiche->save();

        $templateEditorGroupProcesso = new TemplateEditorGroup();
        $templateEditorGroupProcesso->code = 'Processo';
        $templateEditorGroupProcesso->description = 'Processi';
        $templateEditorGroupProcesso->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupProcesso->created_id = $idUser;
        $templateEditorGroupProcesso->save();

        $templateEditorGroupPrezzo = new TemplateEditorGroup();
        $templateEditorGroupPrezzo->code = 'Prezzo';
        $templateEditorGroupPrezzo->description = 'Prezzi';
        $templateEditorGroupPrezzo->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupPrezzo->created_id = $idUser;
        $templateEditorGroupPrezzo->save();

        $templateEditorGroupSkill = new TemplateEditorGroup();
        $templateEditorGroupSkill->code = 'Skill';
        $templateEditorGroupSkill->description = 'Skill';
        $templateEditorGroupSkill->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupSkill->created_id = $idUser;
        $templateEditorGroupSkill->save();

        $templateEditorGroupRiconoscimento = new TemplateEditorGroup();
        $templateEditorGroupRiconoscimento->code = 'Riconoscimento';
        $templateEditorGroupRiconoscimento->description = 'Riconoscimenti';
        $templateEditorGroupRiconoscimento->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupRiconoscimento->created_id = $idUser;
        $templateEditorGroupRiconoscimento->save();

        $templateEditorGroupCitazione = new TemplateEditorGroup();
        $templateEditorGroupCitazione->code = 'Citazione';
        $templateEditorGroupCitazione->description = 'Citazioni';
        $templateEditorGroupCitazione->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupCitazione->created_id = $idUser;
        $templateEditorGroupCitazione->save();

        $templateEditorGroupAiuto = new TemplateEditorGroup();
        $templateEditorGroupAiuto->code = 'Aiuto';
        $templateEditorGroupAiuto->description = 'Help / FAQ';
        $templateEditorGroupAiuto->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupAiuto->created_id = $idUser;
        $templateEditorGroupAiuto->save();

        $templateEditorGroupVideo = new TemplateEditorGroup();
        $templateEditorGroupVideo->code = 'Video';
        $templateEditorGroupVideo->description = 'Video';
        $templateEditorGroupVideo->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupVideo->created_id = $idUser;
        $templateEditorGroupVideo->save();

        $templateEditorGroupMappa = new TemplateEditorGroup();
        $templateEditorGroupMappa->code = 'Mappa';
        $templateEditorGroupMappa->description = 'Mappe';
        $templateEditorGroupMappa->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupMappa->created_id = $idUser;
        $templateEditorGroupMappa->save();

        $templateEditorGroupArticolo = new TemplateEditorGroup();
        $templateEditorGroupArticolo->code = 'Articolo';
        $templateEditorGroupArticolo->description = 'Articolo';
        $templateEditorGroupArticolo->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupArticolo->created_id = $idUser;
        $templateEditorGroupArticolo->save();

        $templateEditorGroupBase = new TemplateEditorGroup();
        $templateEditorGroupBase->code = 'Base';
        $templateEditorGroupBase->description = 'Base';
        $templateEditorGroupBase->template_editor_type_id = $templateEditoTypePages->id;
        $templateEditorGroupBase->created_id = $idUser;
        $templateEditorGroupBase->save();

        #endregion TEMPLATE EDITOR GROUP

        #region TEMPLATE EDITOR

        $templateEditorImmagine_destra_e_testo = new TemplateEditor();
        $templateEditorImmagine_destra_e_testo->description = 'Immagine destra e testo';
        $templateEditorImmagine_destra_e_testo->img = '/api/storage/images/TemplateEditor/Previews/testosx_imgdx.gif';
        $templateEditorImmagine_destra_e_testo->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 col-md-6 text-left"><p style="margin-bottom:15px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempus arcu eros, vitae tincidunt lacus sagittis congue. Mauris ut dui pretium, viverra mauris a, posuere nulla. Cras molestie justo vitae lectus tincidunt semper. Curabitur volutpat placerat lectus vel bibendum. Mauris ut eros nisi.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempus arcu eros, vitae tincidunt lacus sagittis congue. Mauris ut dui pretium, viverra mauris a, posuere nulla. Cras molestie justo vitae lectus tincidunt semper. Curabitur volutpat placerat lectus vel bibendum. Mauris ut eros nisi.</p><p style="margin-bottom:15px;"><br></p></div><div class="col-12 col-md-6 text-center"><img src="/api/storage/images/img-right.jpg" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></div>';
        $templateEditorImmagine_destra_e_testo->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorImmagine_destra_e_testo->created_id = $idUser;
        $templateEditorImmagine_destra_e_testo->save();

        $templateEditorTitolo_e_testo = new TemplateEditor();
        $templateEditorTitolo_e_testo->description = 'Titolo e testo';
        $templateEditorTitolo_e_testo->img = '/api/storage/images/TemplateEditor/Previews/Titolo-e-testo.png';
        $templateEditorTitolo_e_testo->html = '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12"><h1>Titolo</h1><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut neque eros. Cras id varius tellus, eget feugiat erat. Aliquam viverra ligula id bibendum vulputate. Sed at vulputate erat, suscipit luctus diam. Integer scelerisque elementum mauris et maximus. Mauris in lectus eu diam imperdiet pretium vitae ut sapien. Praesent varius, magna scelerisque lacinia dignissim, dolor ipsum sodales tortor, ut scelerisque risus dui vitae risus. Donec libero leo, facilisis ac suscipit vitae, condimentum ac justo.</p></div></div></div></div>';
        $templateEditorTitolo_e_testo->template_editor_group_id = $templateEditorGroupTitolo->id;
        $templateEditorTitolo_e_testo->created_id = $idUser;
        $templateEditorTitolo_e_testo->save();

        $templateEditorMappa_full_page = new TemplateEditor();
        $templateEditorMappa_full_page->description = 'Mappa full page';
        $templateEditorMappa_full_page->img = '/api/storage/images/TemplateEditor/Previews/mappa-full-page.png';
        $templateEditorMappa_full_page->html = '<div class="section-pages"><div class="container-fluid"><iframe width="100%" height="400" frameborder="0" scrolling="no" class="mg1" src="https://maps.google.com/maps?q=Melbourne,+Victoria,+Australia&hl=en&sll=-7.981898,112.626504&sspn=0.009084,0.016512&oq=melbourne&hnear=Melbourne+Victoria,+Australia&t=m&z=10&output=embed"></iframe></div></div>';
        $templateEditorMappa_full_page->template_editor_group_id = $templateEditorGroupMappa->id;
        $templateEditorMappa_full_page->created_id = $idUser;
        $templateEditorMappa_full_page->save();

        $templateEditorBanner_100 = new TemplateEditor();
        $templateEditorBanner_100->description = 'Banner 100%';
        $templateEditorBanner_100->img = '/api/storage/images/TemplateEditor/Previews/1080x500.jpg';
        $templateEditorBanner_100->html = '<div class="section-pages"><div class="container-fluid"><img src="/api/storage/images/1080x500.jpg" style="width: 100%;" class="fr-fic fr-dii"></div></div>';
        $templateEditorBanner_100->template_editor_group_id = $templateEditorGroupHome->id;
        $templateEditorBanner_100->created_id = $idUser;
        $templateEditorBanner_100->save();

        $templateEditorMappa_container = new TemplateEditor();
        $templateEditorMappa_container->description = 'Mappa container';
        $templateEditorMappa_container->img = '/api/storage/images/TemplateEditor/Previews/mappa-container.png';
        $templateEditorMappa_container->html = '<div class="section-pages"><div class="container"><iframe width="100%" height="400" frameborder="0" scrolling="no" class="mg1" src="https://maps.google.com/maps?q=Melbourne,+Victoria,+Australia&hl=en&sll=-7.981898,112.626504&sspn=0.009084,0.016512&oq=melbourne&hnear=Melbourne+Victoria,+Australia&t=m&z=10&output=embed"></iframe></div></div>';
        $templateEditorMappa_container->template_editor_group_id = $templateEditorGroupHome->id;
        $templateEditorMappa_container->created_id = $idUser;
        $templateEditorMappa_container->save();

        $templateEditorReport_e_titolo = new TemplateEditor();
        $templateEditorReport_e_titolo->description = 'Report e titolo';
        $templateEditorReport_e_titolo->img = '/api/storage/images/TemplateEditor/Previews/report-titolo.png';
        $templateEditorReport_e_titolo->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12"><p><em>This is a special report</em></p><h1 style="font-size:30px;text-transform:uppercase;font-weight:bold;">LOREM IPSUM IS SIMPLY DUMMY TEXT.<br>OF THE PRINTING INDUSTRY.</h1></div></div></div></div>';
        $templateEditorReport_e_titolo->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorReport_e_titolo->created_id = $idUser;
        $templateEditorReport_e_titolo->save();

        $templateEditorLista_con_testo_a_due_blocchi = new TemplateEditor();
        $templateEditorLista_con_testo_a_due_blocchi->description = 'Lista con testo a due blocchi';
        $templateEditorLista_con_testo_a_due_blocchi->img = '/api/storage/images/TemplateEditor/Previews/lista-due-blocchi.png';
        $templateEditorLista_con_testo_a_due_blocchi->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 col-md-6"><h3 style="color:#222;font-weight:bold;"><i class="fa fa-check"></i> List Item</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec egestas tincidunt magna, quis accumsan ipsum lobortis vehicula. Vivamus tempor erat nisl, blandit scelerisque arcu dictum in. Praesent tempus volutpat massa, sit amet finibus lectus tincidunt a. Nulla vel massa justo. Morbi in lacus luctus libero ullamcorper pharetra.</p></div><div class="col-12 col-md-6"><h3 style="color:#222;font-weight:bold;"><i class="fa fa-check"></i> List Item</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec egestas tincidunt magna, quis accumsan ipsum lobortis vehicula. Vivamus tempor erat nisl, blandit scelerisque arcu dictum in. Praesent tempus volutpat massa, sit amet finibus lectus tincidunt a. Nulla vel massa justo. Morbi in lacus luctus libero ullamcorper pharetra.</p></div></div></div></div>';
        $templateEditorLista_con_testo_a_due_blocchi->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorLista_con_testo_a_due_blocchi->created_id = $idUser;
        $templateEditorLista_con_testo_a_due_blocchi->save();

        $templateEditorTitolo = new TemplateEditor();
        $templateEditorTitolo->description = 'Titolo';
        $templateEditorTitolo->img = '/api/storage/images/TemplateEditor/Previews/titolo.png';
        $templateEditorTitolo->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12"><h1 style="font-weight:bold;font-size:30px;color:#222;">LOREM IPSUM IS SIMPLY DUMMY TEXT<br>OF THE PRINTING INDUSTRY.</h1></div></div></div></div>';
        $templateEditorTitolo->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorTitolo->created_id = $idUser;
        $templateEditorTitolo->save();

        $templateEditorParagrafo = new TemplateEditor();
        $templateEditorParagrafo->description = 'Paragrafo';
        $templateEditorParagrafo->img = '/api/storage/images/TemplateEditor/Previews/paragrafo.png';
        $templateEditorParagrafo->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec egestas tincidunt magna, quis accumsan ipsum lobortis vehicula. Vivamus tempor erat nisl, blandit scelerisque arcu dictum in. Praesent tempus volutpat massa, sit amet finibus lectus tincidunt a. Nulla vel massa justo. Morbi in lacus luctus libero ullamcorper pharetra.<br>Nulla vel massa justo. Morbi in lacus luctus libero ullamcorper pharetra.</p></div></div></div></div>';
        $templateEditorParagrafo->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorParagrafo->created_id = $idUser;
        $templateEditorParagrafo->save();

        $templateEditorIcone_social = new TemplateEditor();
        $templateEditorIcone_social->description = 'Icone social';
        $templateEditorIcone_social->img = '/api/storage/images/TemplateEditor/Previews/social-black.png';
        $templateEditorIcone_social->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-facebook fa-stack" style="color:#222;font-size:40px;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-youtube-play fa-stack" style="color:#222;font-size:40px;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-instagram fa-stack" style="color:#222;font-size:40px;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-linkedin fa-stack" style="color:#222;font-size:40px;"></i></a></div></div></div></div>';
        $templateEditorIcone_social->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorIcone_social->created_id = $idUser;
        $templateEditorIcone_social->save();

        $templateEditorIcone_social_colorate = new TemplateEditor();
        $templateEditorIcone_social_colorate->description = 'Icone social colorate';
        $templateEditorIcone_social_colorate->img = '/api/storage/images/TemplateEditor/Previews/social-colorati.png';
        $templateEditorIcone_social_colorate->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-facebook fa-stack" style="color:#fff;font-size:40px;background:#1877f2;border-radius:50%;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-youtube-play fa-stack" style="color:#fff;font-size:40px;background:#ff0000;border-radius:50%;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-instagram fa-stack" style="color:#fff;font-size:40px;background:#c32aa3;border-radius:50%;"></i></a></div><div class="col-12 col-md-3 text-center"><a href="#"><i class="fa fa-linkedin fa-stack" style="color:#fff;font-size:40px;background:#007bb5;border-radius:50%;"></i></a></div></div></div></div>';
        $templateEditorIcone_social_colorate->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorIcone_social_colorate->created_id = $idUser;
        $templateEditorIcone_social_colorate->save();

        $templateEditorTitolo_testo_e_bottone = new TemplateEditor();
        $templateEditorTitolo_testo_e_bottone->description = 'Titolo - testo e bottone';
        $templateEditorTitolo_testo_e_bottone->img = '/api/storage/images/TemplateEditor/Previews/titolo-test-e-bottone.png';
        $templateEditorTitolo_testo_e_bottone->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 text-center"><h1>Lorem Ipsum</h1><p style="margin-bottom:15px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempus arcu eros, vitae tincidunt lacus sagittis congue. Mauris ut dui pretium, viverra mauris a, posuere nulla. Cras molestie justo vitae lectus tincidunt semper. Curabitur volutpat placerat lectus vel bibendum. Mauris ut eros nisi.</p><p style="margin-bottom:15px;"><br></p></div></div><div class="row"><div class="col-12 text-center"><a class="button" href="/" style="padding:10px;border:1px solid #000;color:#000;margin-top:15px;">Lorem ipsum</a></div></div></div></div>';
        $templateEditorTitolo_testo_e_bottone->template_editor_group_id = $templateEditorGroupTitolo->id;
        $templateEditorTitolo_testo_e_bottone->created_id = $idUser;
        $templateEditorTitolo_testo_e_bottone->save();

        $templateEditorBlocco_2_immagini = new TemplateEditor();
        $templateEditorBlocco_2_immagini->description = 'Blocco 2 immagini';
        $templateEditorBlocco_2_immagini->img = '/api/storage/images/TemplateEditor/Previews/2-blocchi-immagini.png';
        $templateEditorBlocco_2_immagini->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 col-md-6"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-6"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></div>';
        $templateEditorBlocco_2_immagini->template_editor_group_id = $templateEditorGroupFoto->id;
        $templateEditorBlocco_2_immagini->created_id = $idUser;
        $templateEditorBlocco_2_immagini->save();

        $templateEditorBlocco_3_immagini = new TemplateEditor();
        $templateEditorBlocco_3_immagini->description = 'Blocco 3 immagini';
        $templateEditorBlocco_3_immagini->img = '/api/storage/images/TemplateEditor/Previews/3-blocchi-immagini.png';
        $templateEditorBlocco_3_immagini->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></div>';
        $templateEditorBlocco_3_immagini->template_editor_group_id = $templateEditorGroupFoto->id;
        $templateEditorBlocco_3_immagini->created_id = $idUser;
        $templateEditorBlocco_3_immagini->save();

        $templateEditorYouTube_video_container = new TemplateEditor();
        $templateEditorYouTube_video_container->description = 'YouTube video container';
        $templateEditorYouTube_video_container->img = '/api/storage/images/TemplateEditor/Previews/youtube-video-container.png';
        $templateEditorYouTube_video_container->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12"><iframe width="100%" height="400" src="//www.youtube.com/embed/P5yHEKqx86U?rel=0" frameborder="0" allowfullscreen=""></iframe></div></div></div></div>';
        $templateEditorYouTube_video_container->template_editor_group_id = $templateEditorGroupVideo->id;
        $templateEditorYouTube_video_container->created_id = $idUser;
        $templateEditorYouTube_video_container->save();

        $templateEditorPortfolio_3_blocchi = new TemplateEditor();
        $templateEditorPortfolio_3_blocchi->description = 'Portfolio 3 blocchi';
        $templateEditorPortfolio_3_blocchi->img = '/api/storage/images/TemplateEditor/Previews/portfolio-3-blocchi.png';
        $templateEditorPortfolio_3_blocchi->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 text-center"><h1 style="color:#222;font-size:25px;">LOREM IPSUM</h1></div></div><div class="row"><div class="col-12 text-center"><hr></div></div><div class="row"><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-4"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div></div></div></div>';
        $templateEditorPortfolio_3_blocchi->template_editor_group_id = $templateEditorGroupFoto->id;
        $templateEditorPortfolio_3_blocchi->created_id = $idUser;
        $templateEditorPortfolio_3_blocchi->save();

        $templateEditorPortfolio_2_blocchi = new TemplateEditor();
        $templateEditorPortfolio_2_blocchi->description = 'Portfolio 2 blocchi';
        $templateEditorPortfolio_2_blocchi->img = '/api/storage/images/TemplateEditor/Previews/portfolio-2-blocchi.png';
        $templateEditorPortfolio_2_blocchi->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 text-center"><h1 style="color:#222;font-size:25px;">LOREM IPSUM</h1></div></div><div class="row"><div class="col-12 text-center"><hr></div></div><div class="row"><div class="col-12 col-md-6"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-6"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div></div></div></div>';
        $templateEditorPortfolio_2_blocchi->template_editor_group_id = $templateEditorGroupFoto->id;
        $templateEditorPortfolio_2_blocchi->created_id = $idUser;
        $templateEditorPortfolio_2_blocchi->save();

        $templateEditorPortfolio_4_blocchi = new TemplateEditor();
        $templateEditorPortfolio_4_blocchi->description = 'Portfolio 4 blocchi';
        $templateEditorPortfolio_4_blocchi->img = '/api/storage/images/TemplateEditor/Previews/portfolio-4-blocchi.png';
        $templateEditorPortfolio_4_blocchi->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 text-center"><h1 style="color:#222;font-size:25px;">LOREM IPSUM</h1></div></div><div class="row"><div class="col-12 text-center"><hr></div></div><div class="row"><div class="col-12 col-md-3"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-3"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-3"><img src="/api/storage/images/blocco-img-1.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div><div class="col-12 col-md-3"><img src="/api/storage/images/blocco-img-2.png" style="width: 100%;" class="fr-fic fr-dii"><br><br><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris interdum mauris erat, eget elementum nisi convallis vitae. Phasellus a placerat magna. Phasellus ligula mauris, gravida eu ultrices id, rutrum non augue. Ut dolor leo, ornare sit amet justo vel, pulvinar laoreet lacus.</p><div class="text-right"><a href="#" style="text-align:right;">View more</a></div></div></div></div></div>';
        $templateEditorPortfolio_4_blocchi->template_editor_group_id = $templateEditorGroupFoto->id;
        $templateEditorPortfolio_4_blocchi->created_id = $idUser;
        $templateEditorPortfolio_4_blocchi->save();

        $templateEditorTeam = new TemplateEditor();
        $templateEditorTeam->description = 'Team';
        $templateEditorTeam->img = '/api/storage/images/TemplateEditor/Previews/team.png';
        $templateEditorTeam->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 text-center"><h1 style="color:#222;font-size:40px;margin-bottom:35px;">LOREM IPSUM</h1></div></div><div class="row"><div class="col-12 col-md-4 text-center"><img src="/api/storage/images/team-1.jpg" style="border-radius:50%;" class="fr-fic fr-dii"><br><br><h3>Lorem ipsum</h3><p><em>Lorem ipsum</em></p></div><div class="col-12 col-md-4 text-center"><img src="/api/storage/images/team-2.jpg" style="border-radius:50%;" class="fr-fic fr-dii"><br><br><h3>Lorem ipsum</h3><p><em>Lorem ipsum</em></p></div><div class="col-12 col-md-4 text-center"><img src="/api/storage/images/team-3.jpg" style="border-radius:50%;" class="fr-fic fr-dii"><br><br><h3>Lorem ipsum</h3><p><em>Lorem ipsum</em></p></div></div></div></div>';
        $templateEditorTeam->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorTeam->created_id = $idUser;
        $templateEditorTeam->save();

        $templateEditorTeam_con_icone_social = new TemplateEditor();
        $templateEditorTeam_con_icone_social->description = 'Team con icone social';
        $templateEditorTeam_con_icone_social->img = '/api/storage/images/TemplateEditor/Previews/team-con-social.png';
        $templateEditorTeam_con_icone_social->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 text-center"><h1 style="color:#222;font-size:40px;margin-bottom:35px;">LOREM IPSUM</h1></div></div><div class="row"><div class="col-12 col-md-4 text-center"><img src="/api/storage/images/team-1.jpg" style="border-radius:50%;" class="fr-fic fr-dii"><br><br><h3>Lorem ipsum</h3><div class="row"><div class="col-4 text-right"><a href="#" style="color:#222;font-size:17px;"><i class="fa fa-linkedin"></i></a></div><div class="col-4 text-center"><a href="#" style="color:#222;font-size:17px;"><i class="fa fa-instagram"></i></a></div><div class="col-4 text-left"><a href="#" style="color:#222;font-size:17px;"><i class="fa fa-facebook"></i></a></div></div></div><div class="col-12 col-md-4 text-center"><img src="/api/storage/images/team-2.jpg" style="border-radius:50%;" class="fr-fic fr-dii"><br><br><h3>Lorem ipsum</h3><div class="row"><div class="col-4 text-right"><a href="#" style="color:#222;font-size:17px;"><i class="fa fa-linkedin"></i></a></div><div class="col-4 text-center"><a href="#" style="color:#222;font-size:17px;"><i class="fa fa-instagram"></i></a></div><div class="col-4 text-left"><a href="#" style="color:#222;font-size:17px;"><i class="fa fa-facebook"></i></a></div></div></div><div class="col-12 col-md-4 text-center"><img src="/api/storage/images/team-3.jpg" style="border-radius:50%;" class="fr-fic fr-dii"><br><br><h3>Lorem ipsum</h3><div class="row"><div class="col-4 text-right"><a href="#" style="color:#222;font-size:17px;"><i class="fa fa-linkedin"></i></a></div><div class="col-4 text-center"><a href="#" style="color:#222;font-size:17px;"><i class="fa fa-instagram"></i></a></div><div class="col-4 text-left"><a href="#" style="color:#222;font-size:17px;"><i class="fa fa-facebook"></i></a></div></div></div></div></div></div>';
        $templateEditorTeam_con_icone_social->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorTeam_con_icone_social->created_id = $idUser;
        $templateEditorTeam_con_icone_social->save();

        $templateEditorMappa_e_test = new TemplateEditor();
        $templateEditorMappa_e_test->description = 'Mappa e test';
        $templateEditorMappa_e_test->img = '/api/storage/images/TemplateEditor/Previews/contatti-mappa-testo.png';
        $templateEditorMappa_e_test->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 col-md-6"><iframe width="100%" height="200" frameborder="0" scrolling="no" class="mg1" src="https://maps.google.com/maps?q=Melbourne,+Victoria,+Australia&hl=en&sll=-7.981898,112.626504&sspn=0.009084,0.016512&oq=melbourne&hnear=Melbourne+Victoria,+Australia&t=m&z=10&output=embed"></iframe></div><div class="col-12 col-md-6"><h3 style="color:#222;font-weight:bold;">FIND US HERE</h3><p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s.</p><p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s.</p><a href="#" style="font-size:17px;color:#222;"><i class="fa fa-linkedin"></i></a> <a href="#" style="font-size:17px;color:#222;"><i class="fa fa-facebook"></i></a> <a href="#" style="font-size:17px;color:#222;"><i class="fa fa-envelope-o"></i></a></div></div></div></div>';
        $templateEditorMappa_e_test->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorMappa_e_test->created_id = $idUser;
        $templateEditorMappa_e_test->save();

        $templateEditorFrase_contatti = new TemplateEditor();
        $templateEditorFrase_contatti->description = 'Frase contatti';
        $templateEditorFrase_contatti->img = '/api/storage/images/TemplateEditor/Previews/frase-contatti.png';
        $templateEditorFrase_contatti->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 text-center"><h2 style="margin-bottom:30px;font-weight:light;">Have questions? Give us a call <a href="tel:0%20123%20456%2078%2090">0 123 456 78 90</a></h2></div></div><div class="row"><div class="col-12 text-center"><a href="#"><i class="fa fa-twitter" style="color:#222;font-size:20px;margin-right:25px;"></i></a> <a href="#"><i class="fa fa-facebook" style="color:#222;font-size:20px;margin-right:25px;"></i></a> <a href="#"><i class="fa fa-envelope-o" style="color:#222;font-size:20px;margin-right:25px;"></i></a></div></div></div></div>';
        $templateEditorFrase_contatti->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorFrase_contatti->created_id = $idUser;
        $templateEditorFrase_contatti->save();

        $templateEditorProcesso_con_foto = new TemplateEditor();
        $templateEditorProcesso_con_foto->description = 'Processo con foto';
        $templateEditorProcesso_con_foto->img = '/api/storage/images/TemplateEditor/Previews/timeline-con-foto.png';
        $templateEditorProcesso_con_foto->html = '<div class="section-pages"><div class="container"><div class="row" style="margin-bottom:25px;"><div class="col-12 col-md-6"><img src="/api/storage/images/process-1.png" style="width: 100%;" class="fr-fic fr-dib"></div><div class="col-12 col-md-6"><h6 style="font-size:22px;margin-bottom:30px;">STEP ONE</h6><h2 style="font-size:50px;font-weight:bold;margin-bottom:15px;">Discovery</h2><p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s.</p></div></div><div class="row"><div class="col-12 col-md-6"><h6 style="font-size:22px;margin-bottom:30px;">STEP TWO</h6><h2 style="font-size:50px;font-weight:bold;margin-bottom:15px;">Design and Development</h2><p>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s.</p></div><div class="col-12 col-md-6"><img src="/api/storage/images/process-2.png" style="width: 100%;" class="fr-fic fr-dib"></div></div></div></div>';
        $templateEditorProcesso_con_foto->template_editor_group_id = $templateEditorGroupProcesso->id;
        $templateEditorProcesso_con_foto->created_id = $idUser;
        $templateEditorProcesso_con_foto->save();

        $templateEditorProcessi_con_step_nero = new TemplateEditor();
        $templateEditorProcessi_con_step_nero->description = 'Processi con step nero';
        $templateEditorProcessi_con_step_nero->img = '/api/storage/images/TemplateEditor/Previews/process-line-step-black.png';
        $templateEditorProcessi_con_step_nero->html = '<div class="section-pages"><div class="container"><div class="row" style="margin-bottom:40px;"><div class="col-12 text-center"><h1>THE PROCESS</h1></div></div><div class="row" style="margin-bottom:25px;"><div class="col-12 col-md-4 text-center"><i class="fa fa-lightbulb-o fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;color:#fff;background:#000;"></i><h3>Step 1</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div><div class="col-12 col-md-4 text-center"><i class="fa fa-pencil-square-o fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;color:#fff;background:#000;"></i><h3>Step 2</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div><div class="col-12 col-md-4 text-center"><i class="fa fa-code fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;color:#fff;background:#000;"></i><h3>Step 3</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div></div><div class="row" style="margin-bottom:25px;"><div class="col-12 col-md-4 text-center"><i class="fa fa-rocket fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;color:#fff;background:#000;"></i><h3>Step 4</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div><div class="col-12 col-md-4 text-center"><i class="fa fa-desktop fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;color:#fff;background:#000;"></i><h3>Step 5</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div><div class="col-12 col-md-4 text-center"><i class="fa fa-money fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;color:#fff;background:#000;"></i><h3>Step 6</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div></div></div></div>';
        $templateEditorProcessi_con_step_nero->template_editor_group_id = $templateEditorGroupProcesso->id;
        $templateEditorProcessi_con_step_nero->created_id = $idUser;
        $templateEditorProcessi_con_step_nero->save();

        $templateEditorProcessi_con_step = new TemplateEditor();
        $templateEditorProcessi_con_step->description = 'Processi con step';
        $templateEditorProcessi_con_step->img = '/api/storage/images/TemplateEditor/Previews/process-line-step.png';
        $templateEditorProcessi_con_step->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 text-center"><h1>THE PROCESS</h1></div></div><div class="row" style="margin-bottom:25px;"><div class="col-12 col-md-4 text-center"><i class="fa fa-lightbulb-o fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;"></i><h3>Step 1</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div><div class="col-12 col-md-4 text-center"><i class="fa fa-pencil-square-o fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;"></i><h3>Step 2</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div><div class="col-12 col-md-4 text-center"><i class="fa fa-code fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;"></i><h3>Step 3</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div></div><div class="row" style="margin-bottom:25px;"><div class="col-12 col-md-4 text-center"><i class="fa fa-rocket fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;"></i><h3>Step 4</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div><div class="col-12 col-md-4 text-center"><i class="fa fa-desktop fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;"></i><h3>Step 5</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div><div class="col-12 col-md-4 text-center"><i class="fa fa-money fa-stack" style="font-size:40px;border-radius:50%;border:2px solid #cecece;margin-bottom: 20px;"></i><h3>Step 6</h3><p>Lorem Ipsum is<br>simply dummy text.</p></div></div></div></div>';
        $templateEditorProcessi_con_step->template_editor_group_id = $templateEditorGroupProcesso->id;
        $templateEditorProcessi_con_step->created_id = $idUser;
        $templateEditorProcessi_con_step->save();

        $templateEditorPrezzi_con_background = new TemplateEditor();
        $templateEditorPrezzi_con_background->description = 'Prezzi con background';
        $templateEditorPrezzi_con_background->img = '/api/storage/images/TemplateEditor/Previews/pricing.png';
        $templateEditorPrezzi_con_background->html = '<div class="section-pages"><div class="container"><div class="row" style="margin-bottom:40px;"><div class="col-12 text-center"><h1 style="margin-bottom:15px;">SUBSCRIPTION PLANS</h1><h5 style="font-size:17px;">Choose the right plan that works for you.</h5></div></div><div class="row" style="margin-bottom:25px;"><div class="col-12 col-md-4 text-center"><div style="border:1px solid #ededed;padding-bottom:70px;padding-top:35px;box-shadow:rgba(0, 0, 0, 0.2) 0px 0px 10px -2px;"><h2 style="font-size:5.5rem;color:#ddd;margin-bottom:5px;">01</h2><h6 style="font-size:1.5rem;color:#000;margin-bottom:15px;">BASIC / FREE</h6><p style="font-size:18px;">Lorem Ipsum is<br>dummy text of the<br>printing industry.<br><br></p><div class="text-center"><a href="#" style="padding:10px 20px;border:2px solid #000;color:#000;">SELECT PLAN</a></div></div></div><div class="col-12 col-md-4 text-center"><div style="border:1px solid #ededed;padding-bottom:70px;padding-top:35px;box-shadow:rgba(0, 0, 0, 0.2) 0px 0px 10px -2px;background:#27ae60;"><h2 style="font-size:5.5rem;color:#fff;margin-bottom:5px;">02</h2><h6 style="font-size:1.5rem;color:#fff;margin-bottom:15px;">DELUX / &euro; 77</h6><p style="font-size:18px;color:#fff;">Lorem Ipsum is<br>dummy text of the<br>printing industry.<br><br></p><div class="text-center"><a href="#" style="padding:10px 20px;border:2px solid #fff;color:#fff;">SELECT PLAN</a></div></div></div><div class="col-12 col-md-4 text-center"><div style="border:1px solid #ededed;padding-bottom:70px;padding-top:35px;box-shadow:rgba(0, 0, 0, 0.2) 0px 0px 10px -2px;background:#f39c12;"><h2 style="font-size:5.5rem;color:#fff;margin-bottom:5px;">03</h2><h6 style="font-size:1.5rem;color:#fff;margin-bottom:15px;">PREMIUM / &euro; 189</h6><p style="font-size:18px;color:#fff;">Lorem Ipsum is<br>dummy text of the<br>printing industry.<br><br></p><div class="text-center"><a href="#" style="padding:10px 20px;border:2px solid #fff;color:#fff;">SELECT PLAN</a></div></div></div></div></div></div>';
        $templateEditorPrezzi_con_background->template_editor_group_id = $templateEditorGroupPrezzo->id;
        $templateEditorPrezzi_con_background->created_id = $idUser;
        $templateEditorPrezzi_con_background->save();

        $templateEditorPrezzi_con_background_tondo = new TemplateEditor();
        $templateEditorPrezzi_con_background_tondo->description = 'Prezzi con background tondo';
        $templateEditorPrezzi_con_background_tondo->img = '/api/storage/images/TemplateEditor/Previews/pricing-radius.png';
        $templateEditorPrezzi_con_background_tondo->html = '<div class="section-pages"><div class="container"><div class="row" style="margin-bottom:40px;"><div class="col-12 text-center"><h1 style="margin-bottom:15px;">SUBSCRIPTION PLANS</h1><h5 style="font-size:17px;">Choose the right plan that works for you.</h5></div></div><div class="row" style="margin-bottom:25px;"><div class="col-12 col-md-4 text-center"><div style="border:1px solid #ededed;padding-bottom:70px;padding-top:35px;box-shadow:rgba(0, 0, 0, 0.2) 0px 0px 10px -2px;"><h2 class="fa-stack" style="font-size:4rem;color:#fff;margin-bottom:25px;background:#2980b9;border-radius:50%;">&euro;25</h2><p style="font-size:18px;color:#000;">Lorem Ipsum is<br>dummy text of the<br>printing industry.<br><br></p><div class="text-center"><a href="#" style="padding:10px 20px;border:2px solid #000;color:#000;">SELECT PLAN</a></div></div></div><div class="col-12 col-md-4 text-center"><div style="border:1px solid #ededed;padding-bottom:70px;padding-top:35px;box-shadow:rgba(0, 0, 0, 0.2) 0px 0px 10px -2px;"><h2 class="fa-stack" style="font-size:4rem;color:#fff;margin-bottom:25px;background:#27ae60;border-radius:50%;">&euro;55</h2><p style="font-size:18px;color:#000;">Lorem Ipsum is<br>dummy text of the<br>printing industry.<br><br></p><div class="text-center"><a href="#" style="padding:10px 20px;border:2px solid #000;color:#000;">SELECT PLAN</a></div></div></div><div class="col-12 col-md-4 text-center"><div style="border:1px solid #ededed;padding-bottom:70px;padding-top:35px;box-shadow:rgba(0, 0, 0, 0.2) 0px 0px 10px -2px;"><h2 class="fa-stack" style="font-size:4rem;color:#fff;margin-bottom:25px;background:#8e44ad;border-radius:50%;">&euro;95</h2><p style="font-size:18px;color:#000;">Lorem Ipsum is<br>dummy text of the<br>printing industry.<br><br></p><div class="text-center"><a href="#" style="padding:10px 20px;border:2px solid #000;color:#000;">SELECT PLAN</a></div></div></div></div></div></div>';
        $templateEditorPrezzi_con_background_tondo->template_editor_group_id = $templateEditorGroupPrezzo->id;
        $templateEditorPrezzi_con_background_tondo->created_id = $idUser;
        $templateEditorPrezzi_con_background_tondo->save();

        $templateEditorContatti_3_blocchi = new TemplateEditor();
        $templateEditorContatti_3_blocchi->description = 'Contatti 3 blocchi';
        $templateEditorContatti_3_blocchi->img = '/api/storage/images/TemplateEditor/Previews/contatti-3-blocchi.png';
        $templateEditorContatti_3_blocchi->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 col-md-4 text-center"><h2><i class="fa fa-clock-o"></i><br>ORARI DI APERTURA</h2><br><p>Luned&igrave; &ndash; Venerd&igrave;: 9:00 &ndash; 16:30<br>Sabato: 8:00 &nbsp;&ndash; 20:00</p></div><div class="col-12 col-md-4 text-center"><h2><i class="fa fa-map-marker"></i><br>DOVE SIAMO</h2><br><p>Via Roma, 123<br>40100 Bologna<br>Phone: (+39) 051 987654</p></div><div class="col-12 col-md-4 text-center"><h2><i class="fa fa-home"></i><br>LEGAL</h2><br><p>P.Iva: 12345678910<br><a href="#">Privacy Policy</a><br><a href="#">Cookie Policy</a></p></div></div></div></div>';
        $templateEditorContatti_3_blocchi->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorContatti_3_blocchi->created_id = $idUser;
        $templateEditorContatti_3_blocchi->save();

        $templateEditorYouTube_video_100 = new TemplateEditor();
        $templateEditorYouTube_video_100->description = 'YouTube video 100%';
        $templateEditorYouTube_video_100->img = '/api/storage/images/TemplateEditor/Previews/youtube-video-full-page.png';
        $templateEditorYouTube_video_100->html = '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12"><iframe width="100%" height="500" src="//www.youtube.com/embed/P5yHEKqx86U?rel=0" frameborder="0" allowfullscreen=""></iframe></div></div></div></div>';
        $templateEditorYouTube_video_100->template_editor_group_id = $templateEditorGroupVideo->id;
        $templateEditorYouTube_video_100->created_id = $idUser;
        $templateEditorYouTube_video_100->save();

        $templateEditorSpaziatore = new TemplateEditor();
        $templateEditorSpaziatore->description = 'Spaziatore';
        $templateEditorSpaziatore->img = '/api/storage/images/TemplateEditor/Previews/spaziatore.gif';
        $templateEditorSpaziatore->html = '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12"><div style="height:80px;"><br></div></div></div></div></div>';
        $templateEditorSpaziatore->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorSpaziatore->created_id = $idUser;
        $templateEditorSpaziatore->save();

        $templateEditorLinea_orizzontale = new TemplateEditor();
        $templateEditorLinea_orizzontale->description = 'Linea orizzontale';
        $templateEditorLinea_orizzontale->img = '/api/storage/images/TemplateEditor/Previews/linea-orizzontale.gif';
        $templateEditorLinea_orizzontale->html = '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12"><hr style="background: none; background-color: transparent; border: none; border-top: rgba(0, 0, 0, 0.18) 1px solid; margin: 30px 0 25px; padding: 5px;"><br></div></div></div></div>';
        $templateEditorLinea_orizzontale->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorLinea_orizzontale->created_id = $idUser;
        $templateEditorLinea_orizzontale->save();

        $templateEditorImmagine_a_sinistra_e_testo = new TemplateEditor();
        $templateEditorImmagine_a_sinistra_e_testo->description = 'Immagine a sinistra e testo';
        $templateEditorImmagine_a_sinistra_e_testo->img = '/api/storage/images/TemplateEditor/Previews/imgsx-textdx.gif';
        $templateEditorImmagine_a_sinistra_e_testo->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 col-md-6 text-center"><img src="/api/storage/images/img-left.jpg" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-6 text-left"><p style="margin-bottom:15px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempus arcu eros, vitae tincidunt lacus sagittis congue. Mauris ut dui pretium, viverra mauris a, posuere nulla. Cras molestie justo vitae lectus tincidunt semper. Curabitur volutpat placerat lectus vel bibendum. Mauris ut eros nisi.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempus arcu eros, vitae tincidunt lacus sagittis congue. Mauris ut dui pretium, viverra mauris a, posuere nulla. Cras molestie justo vitae lectus tincidunt semper. Curabitur volutpat placerat lectus vel bibendum. Mauris ut eros nisi.</p></div></div></div></div>';
        $templateEditorImmagine_a_sinistra_e_testo->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorImmagine_a_sinistra_e_testo->created_id = $idUser;
        $templateEditorImmagine_a_sinistra_e_testo->save();

        $templateEditorDue_colonne = new TemplateEditor();
        $templateEditorDue_colonne->description = 'Due colonne';
        $templateEditorDue_colonne->img = '/api/storage/images/TemplateEditor/Previews/duecolonne.gif';
        $templateEditorDue_colonne->html = '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 col-md-6 text-left"><p style="margin-bottom:15px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempus arcu eros, vitae tincidunt lacus sagittis congue. Mauris ut dui pretium, viverra mauris a, posuere nulla. Cras molestie justo vitae lectus tincidunt semper. Curabitur volutpat placerat lectus vel bibendum. Mauris ut eros nisi.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempus arcu eros, vitae tincidunt lacus sagittis congue. Mauris ut dui pretium, viverra mauris a, posuere nulla. Cras molestie justo vitae lectus tincidunt semper. Curabitur volutpat placerat lectus vel bibendum. Mauris ut eros nisi.</p></div><div class="col-12 col-md-6 text-left"><p style="margin-bottom:15px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempus arcu eros, vitae tincidunt lacus sagittis congue. Mauris ut dui pretium, viverra mauris a, posuere nulla. Cras molestie justo vitae lectus tincidunt semper. Curabitur volutpat placerat lectus vel bibendum. Mauris ut eros nisi.<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempus arcu eros, vitae tincidunt lacus sagittis congue. Mauris ut dui pretium, viverra mauris a, posuere nulla. Cras molestie justo vitae lectus tincidunt semper. Curabitur volutpat placerat lectus vel bibendum. Mauris ut eros nisi.</p></div></div></div></div>';
        $templateEditorDue_colonne->template_editor_group_id = $templateEditorGroupBase->id;
        $templateEditorDue_colonne->created_id = $idUser;
        $templateEditorDue_colonne->save();

        $templateEditorForm_contatti = new TemplateEditor();
        $templateEditorForm_contatti->description = 'Form contatti';
        $templateEditorForm_contatti->img = '/api/storage/images/TemplateEditor/Previews/FormContatti.JPG';
        $templateEditorForm_contatti->html = '<div class="section-pages"><div class="container"><form id="contactRequest"><div class="row"><div class="col-4"><label for="name"><strong>Nome</strong></label></div><div class="col-4"><label for="surname"><strong>Cognome</strong></label></div><div class="col-4"><label for="email"><strong>Email</strong></label></div></div><div class="row"><div class="col-4"><input id="name" name="name" type="text" required="required" value=""></div><div class="col-4"><input id="surname" name="surname" type="text" required="required" value=""></div><div class="col-4"><input id="email" name="email" type="email" required="required" value=""></div></div><div class="row"><div class="col-12"><label for="description"><strong>Richiesta</strong></label></div></div><div class="row"><div class="col-12"><textarea id="description" name="description" required="required" value="" style="width: 100%;" rows="5"></textarea></div></div><div class="row"><div class="col-12 text-right"><button class="btn btn-primary" id="btnSend" type="button">Invia richiesta</button></div></div></form></div></div>';
        $templateEditorForm_contatti->template_editor_group_id = $templateEditorGroupContatti->id;
        $templateEditorForm_contatti->created_id = $idUser;
        $templateEditorForm_contatti->save();


        #endregion TEMPLATE EDITOR
    }
}

<?php

use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class SelenaSqlViewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $idMenuAdminCRM = MenuAdmin::where('code', 'Setting')->first()->id;

        #region SELENA SQL VIEWS

        #region ICON

        $iconIdCard = new Icon();
        $iconIdCard->description = 'IdCard';
        $iconIdCard->css = 'fa fa-cog';
        $iconIdCard->created_id = $idUser;
        $iconIdCard->save();

        #endregion ICON

        #region FUNCTIONALITY

        $functionalityContact = new Functionality();
        $functionalityContact->code = 'SelenaViews';
        $functionalityContact->created_id = $idUser;
        $functionalityContact->save();

        #endregion FUNCTIONALITY

        #region MENU

        $menuAdminContact = new MenuAdmin();
        $menuAdminContact->code = 'SelenaViews';
        $menuAdminContact->functionality_id = $functionalityContact->id;
        $menuAdminContact->icon_id = $iconIdCard->id;
        $menuAdminContact->url = '/admin/setting/selenaviews';
        $menuAdminContact->created_id = $idUser;
        $menuAdminContact->save();

        $languageMenuAdminContact = new LanguageMenuAdmin();
        $languageMenuAdminContact->language_id = $idLanguage;
        $languageMenuAdminContact->menu_admin_id = $menuAdminContact->id;
        $languageMenuAdminContact->description = 'Selena Views';
        $languageMenuAdminContact->created_id = $idUser;
        $languageMenuAdminContact->save();

        $menuAdminUserRoleContact = new MenuAdminUserRole();
        $menuAdminUserRoleContact->menu_admin_id = $menuAdminContact->id;
        $menuAdminUserRoleContact->menu_admin_father_id = $idMenuAdminCRM;
        $menuAdminUserRoleContact->role_id = $idRole;
        $menuAdminUserRoleContact->enabled = true;
        $menuAdminUserRoleContact->order = 30;
        $menuAdminUserRoleContact->created_id = $idUser;
        $menuAdminUserRoleContact->save();

        #endregion MENU

        #region TABLE

        #region TAB

        $tabContactTable = new Tab();
        $tabContactTable->code = 'Table';
        $tabContactTable->functionality_id = $functionalityContact->id;
        $tabContactTable->order = 10;
        $tabContactTable->created_id = $idUser;
        $tabContactTable->save();

        $languageTabContactTable = new LanguageTab();
        $languageTabContactTable->language_id = $idLanguage;
        $languageTabContactTable->tab_id = $tabContactTable->id;
        $languageTabContactTable->description = 'Table';
        $languageTabContactTable->created_id = $idUser;
        $languageTabContactTable->save();

        #endregion TAB

        #region CHECKBOX

        $fieldContactCheckBox = new Field();
        $fieldContactCheckBox->tab_id = $tabContactTable->id;
        $fieldContactCheckBox->code = 'TableCheckBox';
        $fieldContactCheckBox->created_id = $idUser;
        $fieldContactCheckBox->save();

        $fieldLanguageContactCheckBox = new FieldLanguage();
        $fieldLanguageContactCheckBox->field_id = $fieldContactCheckBox->id;
        $fieldLanguageContactCheckBox->language_id = $idLanguage;
        $fieldLanguageContactCheckBox->description = 'CheckBox';
        $fieldLanguageContactCheckBox->created_id = $idUser;
        $fieldLanguageContactCheckBox->save();

        $fieldUserRoleContactCheckBox = new FieldUserRole();
        $fieldUserRoleContactCheckBox->field_id = $fieldContactCheckBox->id;
        $fieldUserRoleContactCheckBox->role_id = $idRole;
        $fieldUserRoleContactCheckBox->enabled = true;
        $fieldUserRoleContactCheckBox->table_order = 10;
        $fieldUserRoleContactCheckBox->input_type = 13;
        $fieldUserRoleContactCheckBox->created_id = $idUser;
        $fieldUserRoleContactCheckBox->save();

        #endregion CHECKBOX

        #region FORMATTER

        $fieldContactFormatter = new Field();
        $fieldContactFormatter->code = 'SelenaViewsFormatter';
        $fieldContactFormatter->formatter = 'SelenaViewsFormatter';
        $fieldContactFormatter->tab_id = $tabContactTable->id;
        $fieldContactFormatter->created_id = $idUser;
        $fieldContactFormatter->save();

        $fieldLanguageContactFormatter = new FieldLanguage();
        $fieldLanguageContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldLanguageContactFormatter->language_id = $idLanguage;
        $fieldLanguageContactFormatter->description = '';
        $fieldLanguageContactFormatter->created_id = $idUser;
        $fieldLanguageContactFormatter->save();

        $fieldUserRoleContactFormatter = new FieldUserRole();
        $fieldUserRoleContactFormatter->field_id = $fieldContactFormatter->id;
        $fieldUserRoleContactFormatter->role_id = $idRole;
        $fieldUserRoleContactFormatter->enabled = true;
        $fieldUserRoleContactFormatter->table_order = 20;
        $fieldUserRoleContactFormatter->input_type = 14;
        $fieldUserRoleContactFormatter->created_id = $idUser;
        $fieldUserRoleContactFormatter->save();

        #endregion FORMATTER

        #region NAMEVIEW

        $fieldContactTableCompany = new Field();
        $fieldContactTableCompany->code = 'NameView';
        $fieldContactTableCompany->tab_id = $tabContactTable->id;
        $fieldContactTableCompany->field = 'nameView';
        $fieldContactTableCompany->data_type = 1;
        $fieldContactTableCompany->created_id = $idUser;
        $fieldContactTableCompany->save();

        $fieldLanguageContactTableCompany = new FieldLanguage();
        $fieldLanguageContactTableCompany->field_id = $fieldContactTableCompany->id;
        $fieldLanguageContactTableCompany->language_id = $idLanguage;
        $fieldLanguageContactTableCompany->description = 'Nome View';
        $fieldLanguageContactTableCompany->created_id = $idUser;
        $fieldLanguageContactTableCompany->save();

        $fieldUserRoleContactTableCompany = new FieldUserRole();
        $fieldUserRoleContactTableCompany->field_id = $fieldContactTableCompany->id;
        $fieldUserRoleContactTableCompany->role_id = $idRole;
        $fieldUserRoleContactTableCompany->enabled = true;
        $fieldUserRoleContactTableCompany->table_order = 30;
        $fieldUserRoleContactTableCompany->input_type = 0;
        $fieldUserRoleContactTableCompany->created_id = $idUser;
        $fieldUserRoleContactTableCompany->save();

        #endregion NAMEVIEW

        #region DESCRIPTION

        $fieldContactTableBusinessName = new Field();
        $fieldContactTableBusinessName->code = 'Description';
        $fieldContactTableBusinessName->tab_id = $tabContactTable->id;
        $fieldContactTableBusinessName->field = 'description';
        $fieldContactTableBusinessName->data_type = 1;
        $fieldContactTableBusinessName->created_id = $idUser;
        $fieldContactTableBusinessName->save();

        $fieldLanguageContactTableBusinessName = new FieldLanguage();
        $fieldLanguageContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldLanguageContactTableBusinessName->language_id = $idLanguage;
        $fieldLanguageContactTableBusinessName->description = 'Decrizione view';
        $fieldLanguageContactTableBusinessName->created_id = $idUser;
        $fieldLanguageContactTableBusinessName->save();

        $fieldUserRoleContactTableBusinessName = new FieldUserRole();
        $fieldUserRoleContactTableBusinessName->field_id = $fieldContactTableBusinessName->id;
        $fieldUserRoleContactTableBusinessName->role_id = $idRole;
        $fieldUserRoleContactTableBusinessName->enabled = true;
        $fieldUserRoleContactTableBusinessName->table_order = 40;
        $fieldUserRoleContactTableBusinessName->input_type = 0;
        $fieldUserRoleContactTableBusinessName->created_id = $idUser;
        $fieldUserRoleContactTableBusinessName->save();

        #endregion DESCRIPTION

        #endregion TABLE




        #region DETAIL

        #region TAB

        $tabContactDetail = new Tab();
        $tabContactDetail->code = 'Detail';
        $tabContactDetail->functionality_id = $functionalityContact->id;
        $tabContactDetail->order = 20;
        $tabContactDetail->created_id = $idUser;
        $tabContactDetail->save();

        $languageTabContactDetail = new LanguageTab();
        $languageTabContactDetail->language_id = $idLanguage;
        $languageTabContactDetail->tab_id = $tabContactDetail->id;
        $languageTabContactDetail->description = 'Detail';
        $languageTabContactDetail->created_id = $idUser;
        $languageTabContactDetail->save();

        #endregion TAB

        #region NAMEVIEW

        $fieldContactDetailCompany = new Field();
        $fieldContactDetailCompany->code = 'NameView';
        $fieldContactDetailCompany->tab_id = $tabContactDetail->id;
        $fieldContactDetailCompany->field = 'nameView';
        $fieldContactDetailCompany->data_type = 1;
        $fieldContactDetailCompany->created_id = $idUser;
        $fieldContactDetailCompany->save();

        $fieldLanguageContactDetailCompany = new FieldLanguage();
        $fieldLanguageContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldLanguageContactDetailCompany->language_id = $idLanguage;
        $fieldLanguageContactDetailCompany->description = 'Nome View';
        $fieldLanguageContactDetailCompany->created_id = $idUser;
        $fieldLanguageContactDetailCompany->save();

        $fieldUserRoleContactDetailCompany = new FieldUserRole();
        $fieldUserRoleContactDetailCompany->field_id = $fieldContactDetailCompany->id;
        $fieldUserRoleContactDetailCompany->role_id = $idRole;
        $fieldUserRoleContactDetailCompany->enabled = true;
        $fieldUserRoleContactDetailCompany->pos_x = 10;
        $fieldUserRoleContactDetailCompany->pos_y = 10;
        $fieldUserRoleContactDetailCompany->input_type = 0;
        $fieldUserRoleContactDetailCompany->created_id = $idUser;
        $fieldUserRoleContactDetailCompany->save();

        #endregion NAMEVIEW

        #region DESCRIPTION

        $fieldSurnameContactDetail = new Field();
        $fieldSurnameContactDetail->code = 'Description';
        $fieldSurnameContactDetail->tab_id = $tabContactDetail->id;
        $fieldSurnameContactDetail->field = 'description';
        $fieldSurnameContactDetail->data_type = 1;
        $fieldSurnameContactDetail->created_id = $idUser;
        $fieldSurnameContactDetail->save();

        $fieldLanguageSurnameContactDetail = new FieldLanguage();
        $fieldLanguageSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldLanguageSurnameContactDetail->language_id = $idLanguage;
        $fieldLanguageSurnameContactDetail->description = 'Descrizione View';
        $fieldLanguageSurnameContactDetail->created_id = $idUser;
        $fieldLanguageSurnameContactDetail->save();

        $fieldUserRoleSurnameContactDetail = new FieldUserRole();
        $fieldUserRoleSurnameContactDetail->field_id = $fieldSurnameContactDetail->id;
        $fieldUserRoleSurnameContactDetail->role_id = $idRole;
        $fieldUserRoleSurnameContactDetail->enabled = true;
        $fieldUserRoleSurnameContactDetail->pos_x = 20;
        $fieldUserRoleSurnameContactDetail->pos_y = 10;
        $fieldUserRoleSurnameContactDetail->input_type = 0;
        $fieldUserRoleSurnameContactDetail->created_id = $idUser;
        $fieldUserRoleSurnameContactDetail->save();

        #endregion DESCRIPTION

        #region SELECT

        $fieldContactDetailBusinessName = new Field();
        $fieldContactDetailBusinessName->code = 'Select';
        $fieldContactDetailBusinessName->tab_id = $tabContactDetail->id;
        $fieldContactDetailBusinessName->field = 'select';
        $fieldContactDetailBusinessName->data_type = 1;
        $fieldContactDetailBusinessName->created_id = $idUser;
        $fieldContactDetailBusinessName->save();

        $fieldLanguageContactDetailBusinessName = new FieldLanguage();
        $fieldLanguageContactDetailBusinessName->field_id = $fieldContactDetailBusinessName->id;
        $fieldLanguageContactDetailBusinessName->language_id = $idLanguage;
        $fieldLanguageContactDetailBusinessName->description = 'Select';
        $fieldLanguageContactDetailBusinessName->created_id = $idUser;
        $fieldLanguageContactDetailBusinessName->save();

        $fieldUserRoleContactDetailBusinessName = new FieldUserRole();
        $fieldUserRoleContactDetailBusinessName->field_id = $fieldContactDetailBusinessName->id;
        $fieldUserRoleContactDetailBusinessName->role_id = $idRole;
        $fieldUserRoleContactDetailBusinessName->enabled = true;
        $fieldUserRoleContactDetailBusinessName->pos_x = 10;
        $fieldUserRoleContactDetailBusinessName->pos_y = 20;
        $fieldUserRoleContactDetailBusinessName->input_type = 1;
        $fieldUserRoleContactDetailBusinessName->created_id = $idUser;
        $fieldUserRoleContactDetailBusinessName->save();

        #endregion SELECT

        #region FROM

        $fieldContactDetailName = new Field();
        $fieldContactDetailName->code = 'From';
        $fieldContactDetailName->tab_id = $tabContactDetail->id;
        $fieldContactDetailName->field = 'from';
        $fieldContactDetailName->data_type = 1;
        $fieldContactDetailName->created_id = $idUser;
        $fieldContactDetailName->save();

        $fieldLanguageContactDetailName = new FieldLanguage();
        $fieldLanguageContactDetailName->field_id = $fieldContactDetailName->id;
        $fieldLanguageContactDetailName->language_id = $idLanguage;
        $fieldLanguageContactDetailName->description = 'From';
        $fieldLanguageContactDetailName->created_id = $idUser;
        $fieldLanguageContactDetailName->save();

        $fieldUserRoleContactDetailName = new FieldUserRole();
        $fieldUserRoleContactDetailName->field_id = $fieldContactDetailName->id;
        $fieldUserRoleContactDetailName->role_id = $idRole;
        $fieldUserRoleContactDetailName->enabled = true;
        $fieldUserRoleContactDetailName->pos_x = 10;
        $fieldUserRoleContactDetailName->pos_y = 30;
        $fieldUserRoleContactDetailName->input_type = 1;
        $fieldUserRoleContactDetailName->created_id = $idUser;
        $fieldUserRoleContactDetailName->save();

        #endregion FROM

        #region WHERE

        $fieldContactDetailSurname = new Field();
        $fieldContactDetailSurname->code = 'Where';
        $fieldContactDetailSurname->tab_id = $tabContactDetail->id;
        $fieldContactDetailSurname->field = 'where';
        $fieldContactDetailSurname->data_type = 1;
        $fieldContactDetailSurname->created_id = $idUser;
        $fieldContactDetailSurname->save();

        $fieldLanguageContactDetailSurname = new FieldLanguage();
        $fieldLanguageContactDetailSurname->field_id = $fieldContactDetailSurname->id;
        $fieldLanguageContactDetailSurname->language_id = $idLanguage;
        $fieldLanguageContactDetailSurname->description = 'Where';
        $fieldLanguageContactDetailSurname->created_id = $idUser;
        $fieldLanguageContactDetailSurname->save();

        $fieldUserRoleContactDetailSurname = new FieldUserRole();
        $fieldUserRoleContactDetailSurname->field_id = $fieldContactDetailSurname->id;
        $fieldUserRoleContactDetailSurname->role_id = $idRole;
        $fieldUserRoleContactDetailSurname->enabled = true;
        $fieldUserRoleContactDetailSurname->pos_x = 10;
        $fieldUserRoleContactDetailSurname->pos_y = 40;
        $fieldUserRoleContactDetailSurname->input_type = 1;
        $fieldUserRoleContactDetailSurname->created_id = $idUser;
        $fieldUserRoleContactDetailSurname->save();

        #endregion WHERE

        #region ORDER BY

        $fieldContactDetailSendNewsletter = new Field();
        $fieldContactDetailSendNewsletter->code = 'OrderBy';
        $fieldContactDetailSendNewsletter->tab_id = $tabContactDetail->id;
        $fieldContactDetailSendNewsletter->field = 'orderBy';
        $fieldContactDetailSendNewsletter->data_type = 1;
        $fieldContactDetailSendNewsletter->created_id = $idUser;
        $fieldContactDetailSendNewsletter->save();

        $fieldLanguageContactDetailSendNewsletter = new FieldLanguage();
        $fieldLanguageContactDetailSendNewsletter->field_id = $fieldContactDetailSendNewsletter->id;
        $fieldLanguageContactDetailSendNewsletter->language_id = $idLanguage;
        $fieldLanguageContactDetailSendNewsletter->description = 'Order by';
        $fieldLanguageContactDetailSendNewsletter->created_id = $idUser;
        $fieldLanguageContactDetailSendNewsletter->save();

        $fieldUserRoleContactDetailSendNewsletter = new FieldUserRole();
        $fieldUserRoleContactDetailSendNewsletter->field_id = $fieldContactDetailSendNewsletter->id;
        $fieldUserRoleContactDetailSendNewsletter->role_id = $idRole;
        $fieldUserRoleContactDetailSendNewsletter->enabled = true;
        $fieldUserRoleContactDetailSendNewsletter->pos_x = 10;
        $fieldUserRoleContactDetailSendNewsletter->pos_y = 50;
        $fieldUserRoleContactDetailSendNewsletter->input_type = 1;
        $fieldUserRoleContactDetailSendNewsletter->created_id = $idUser;
        $fieldUserRoleContactDetailSendNewsletter->save();

        #endregion ORDER BY

        #region GROUP BY

        $fieldContactDetailErrorNewsletter = new Field();
        $fieldContactDetailErrorNewsletter->code = 'GroupBy';
        $fieldContactDetailErrorNewsletter->tab_id = $tabContactDetail->id;
        $fieldContactDetailErrorNewsletter->field = 'groupBy';
        $fieldContactDetailErrorNewsletter->data_type = 1;
        $fieldContactDetailErrorNewsletter->created_id = $idUser;
        $fieldContactDetailErrorNewsletter->save();

        $fieldLanguageContactDetailErrorNewsletterr = new FieldLanguage();
        $fieldLanguageContactDetailErrorNewsletterr->field_id = $fieldContactDetailErrorNewsletter->id;
        $fieldLanguageContactDetailErrorNewsletterr->language_id = $idLanguage;
        $fieldLanguageContactDetailErrorNewsletterr->description = 'Group by';
        $fieldLanguageContactDetailErrorNewsletterr->created_id = $idUser;
        $fieldLanguageContactDetailErrorNewsletterr->save();

        $fieldUserRoleContactDetailErrorNewsletter = new FieldUserRole();
        $fieldUserRoleContactDetailErrorNewsletter->field_id = $fieldContactDetailErrorNewsletter->id;
        $fieldUserRoleContactDetailErrorNewsletter->role_id = $idRole;
        $fieldUserRoleContactDetailErrorNewsletter->enabled = true;
        $fieldUserRoleContactDetailErrorNewsletter->pos_x = 10;
        $fieldUserRoleContactDetailErrorNewsletter->pos_y = 60;
        $fieldUserRoleContactDetailErrorNewsletter->input_type = 1;
        $fieldUserRoleContactDetailErrorNewsletter->created_id = $idUser;
        $fieldUserRoleContactDetailErrorNewsletter->save();

        #endregion GROUP BY

        #endregion DETAIL

        #endregion SELENA SQL VIEWS
    }
}

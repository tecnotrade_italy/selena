<?php

use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class ListConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $idMenuAdminCRM = MenuAdmin::where('code', 'PriceLists')->first()->id;

        $functionalityList = new Functionality();
        $functionalityList->code = 'List';
        $functionalityList->created_id = $idUser;
        $functionalityList->save();
      
        #region TAB
        $tabListTable = new Tab();
        $tabListTable->code = 'Table';
        $tabListTable->functionality_id = $functionalityList->id;
        $tabListTable->order = 10;
        $tabListTable->created_id = $idUser;
        $tabListTable->save();

        $languageTabListTable = new LanguageTab();
        $languageTabListTable->language_id = $idLanguage;
        $languageTabListTable->tab_id = $tabListTable->id;
        $languageTabListTable->description = 'Table';
        $languageTabListTable->created_id = $idUser;
        $languageTabListTable->save();
        #endregion TAB

        #region CHECKBOX
        $fieldListCheckBox = new Field();
        $fieldListCheckBox->tab_id = $tabListTable->id;
        $fieldListCheckBox->code = 'TableCheckBox';
        $fieldListCheckBox->created_id = $idUser;
        $fieldListCheckBox->save();

        $fieldLanguageListCheckBox = new FieldLanguage();
        $fieldLanguageListCheckBox->field_id = $fieldListCheckBox->id;
        $fieldLanguageListCheckBox->language_id = $idLanguage;
        $fieldLanguageListCheckBox->description = 'CheckBox';
        $fieldLanguageListCheckBox->created_id = $idUser;
        $fieldLanguageListCheckBox->save();

        $fieldUserRoleListCheckBox = new FieldUserRole();
        $fieldUserRoleListCheckBox->field_id = $fieldListCheckBox->id;
        $fieldUserRoleListCheckBox->role_id = $idRole;
        $fieldUserRoleListCheckBox->enabled = true;
        $fieldUserRoleListCheckBox->table_order = 10;
        $fieldUserRoleListCheckBox->input_type = 13;
        $fieldUserRoleListCheckBox->created_id = $idUser;
        $fieldUserRoleListCheckBox->save();

        #endregion CHECKBOX

        #region FORMATTER
        $fieldListFormatter = new Field();
        $fieldListFormatter->code = 'ListFormatter';
        $fieldListFormatter->formatter = 'listFormatter';
        $fieldListFormatter->tab_id = $tabListTable->id;
        $fieldListFormatter->created_id = $idUser;
        $fieldListFormatter->save();

        $fieldLanguageListFormatter = new FieldLanguage();
        $fieldLanguageListFormatter->field_id = $fieldListFormatter->id;
        $fieldLanguageListFormatter->language_id = $idLanguage;
        $fieldLanguageListFormatter->description = '';
        $fieldLanguageListFormatter->created_id = $idUser;
        $fieldLanguageListFormatter->save();

        $fieldUserRoleListFormatter = new FieldUserRole();
        $fieldUserRoleListFormatter->field_id = $fieldListFormatter->id;
        $fieldUserRoleListFormatter->role_id = $idRole;
        $fieldUserRoleListFormatter->enabled = true;
        $fieldUserRoleListFormatter->table_order = 20;
        $fieldUserRoleListFormatter->input_type = 14;
        $fieldUserRoleListFormatter->created_id = $idUser;
        $fieldUserRoleListFormatter->save();
        #endregion FORMATTER  

        #region code_list
        $fieldListTableCodeList = new Field();
        $fieldListTableCodeList->code = 'code_list';
        $fieldListTableCodeList->tab_id = $tabListTable->id;
        $fieldListTableCodeList->field = 'code_list';
        $fieldListTableCodeList->data_type = 1;
        $fieldListTableCodeList->created_id = $idUser;
        $fieldListTableCodeList->save();

        $fieldLanguageListTableCodeList = new FieldLanguage();
        $fieldLanguageListTableCodeList->field_id = $fieldListTableCodeList->id;
        $fieldLanguageListTableCodeList->language_id = $idLanguage;
        $fieldLanguageListTableCodeList->description = 'Codice Lista';
        $fieldLanguageListTableCodeList->created_id = $idUser;
        $fieldLanguageListTableCodeList->save();

        $fieldUserRoleListTableCodeList = new FieldUserRole();
        $fieldUserRoleListTableCodeList->field_id = $fieldListTableCodeList->id;
        $fieldUserRoleListTableCodeList->role_id = $idRole;
        $fieldUserRoleListTableCodeList->enabled = true;
        $fieldUserRoleListTableCodeList->table_order = 30;
        $fieldUserRoleListTableCodeList->input_type = 0;
        $fieldUserRoleListTableCodeList->created_id = $idUser;
        $fieldUserRoleListTableCodeList->save();
        #endregion code_list

        #region description
        $fieldListTableCodeList = new Field();
        $fieldListTableCodeList->code = 'description';
        $fieldListTableCodeList->tab_id = $tabListTable->id;
        $fieldListTableCodeList->field = 'description';
        $fieldListTableCodeList->data_type = 1;
        $fieldListTableCodeList->created_id = $idUser;
        $fieldListTableCodeList->save();

        $fieldLanguageListTableCodeList = new FieldLanguage();
        $fieldLanguageListTableCodeList->field_id = $fieldListTableCodeList->id;
        $fieldLanguageListTableCodeList->language_id = $idLanguage;
        $fieldLanguageListTableCodeList->description = 'Descrizione';
        $fieldLanguageListTableCodeList->created_id = $idUser;
        $fieldLanguageListTableCodeList->save();

        $fieldUserRoleListTableCodeList = new FieldUserRole();
        $fieldUserRoleListTableCodeList->field_id = $fieldListTableCodeList->id;
        $fieldUserRoleListTableCodeList->role_id = $idRole;
        $fieldUserRoleListTableCodeList->enabled = true;
        $fieldUserRoleListTableCodeList->table_order = 40;
        $fieldUserRoleListTableCodeList->input_type = 0;
        $fieldUserRoleListTableCodeList->created_id = $idUser;
        $fieldUserRoleListTableCodeList->save();
        #endregion description

        #region beginning_validity
        $fieldListTableCodeList = new Field();
        $fieldListTableCodeList->code = 'beginning_validity';
        $fieldListTableCodeList->tab_id = $tabListTable->id;
        $fieldListTableCodeList->field = 'beginning_validity';
        $fieldListTableCodeList->data_type = 2;
        $fieldListTableCodeList->created_id = $idUser;
        $fieldListTableCodeList->save();

        $fieldLanguageListTableCodeList = new FieldLanguage();
        $fieldLanguageListTableCodeList->field_id = $fieldListTableCodeList->id;
        $fieldLanguageListTableCodeList->language_id = $idLanguage;
        $fieldLanguageListTableCodeList->description = 'Inizio Validità';
        $fieldLanguageListTableCodeList->created_id = $idUser;
        $fieldLanguageListTableCodeList->save();

        $fieldUserRoleListTableCodeList = new FieldUserRole();
        $fieldUserRoleListTableCodeList->field_id = $fieldListTableCodeList->id;
        $fieldUserRoleListTableCodeList->role_id = $idRole;
        $fieldUserRoleListTableCodeList->enabled = true;
        $fieldUserRoleListTableCodeList->table_order = 50;
        $fieldUserRoleListTableCodeList->input_type = 4;
        $fieldUserRoleListTableCodeList->created_id = $idUser;
        $fieldUserRoleListTableCodeList->save();
        #endregion beginning_validity

        #region end_validity
        $fieldListTableCodeList = new Field();
        $fieldListTableCodeList->code = 'end_validity';
        $fieldListTableCodeList->tab_id = $tabListTable->id;
        $fieldListTableCodeList->field = 'end_validity';
        $fieldListTableCodeList->data_type = 2;
        $fieldListTableCodeList->created_id = $idUser;
        $fieldListTableCodeList->save();

        $fieldLanguageListTableCodeList = new FieldLanguage();
        $fieldLanguageListTableCodeList->field_id = $fieldListTableCodeList->id;
        $fieldLanguageListTableCodeList->language_id = $idLanguage;
        $fieldLanguageListTableCodeList->description = 'Fine Validità';
        $fieldLanguageListTableCodeList->created_id = $idUser;
        $fieldLanguageListTableCodeList->save();

        $fieldUserRoleListTableCodeList = new FieldUserRole();
        $fieldUserRoleListTableCodeList->field_id = $fieldListTableCodeList->id;
        $fieldUserRoleListTableCodeList->role_id = $idRole;
        $fieldUserRoleListTableCodeList->enabled = true;
        $fieldUserRoleListTableCodeList->table_order = 60;
        $fieldUserRoleListTableCodeList->input_type = 4;
        $fieldUserRoleListTableCodeList->created_id = $idUser;
        $fieldUserRoleListTableCodeList->save();
        #endregion end_validity

        #region value_list
        $fieldListTableCodeList = new Field();
        $fieldListTableCodeList->code = 'value_list';
        $fieldListTableCodeList->tab_id = $tabListTable->id;
        $fieldListTableCodeList->field = 'value_list';
        $fieldListTableCodeList->data_type = 1;
        $fieldListTableCodeList->created_id = $idUser;
        $fieldListTableCodeList->save();

        $fieldLanguageListTableCodeList = new FieldLanguage();
        $fieldLanguageListTableCodeList->field_id = $fieldListTableCodeList->id;
        $fieldLanguageListTableCodeList->language_id = $idLanguage;
        $fieldLanguageListTableCodeList->description = 'Valuta Listino';
        $fieldLanguageListTableCodeList->created_id = $idUser;
        $fieldLanguageListTableCodeList->save();

        $fieldUserRoleListTableCodeList = new FieldUserRole();
        $fieldUserRoleListTableCodeList->field_id = $fieldListTableCodeList->id;
        $fieldUserRoleListTableCodeList->role_id = $idRole;
        $fieldUserRoleListTableCodeList->enabled = true;
        $fieldUserRoleListTableCodeList->table_order = 70;
        $fieldUserRoleListTableCodeList->input_type = 0;
        $fieldUserRoleListTableCodeList->created_id = $idUser;
        $fieldUserRoleListTableCodeList->save();
        #endregion value_list

        #region TAB
        $tabDetailTable = new Tab();
        $tabDetailTable->code = 'Detail';
        $tabDetailTable->functionality_id = $functionalityList->id;
        $tabDetailTable->order = 10;
        $tabDetailTable->created_id = $idUser;
        $tabDetailTable->save();

        $languageDetailListTable = new LanguageTab();
        $languageDetailListTable->language_id = $idLanguage;
        $languageDetailListTable->tab_id = $tabDetailTable->id;
        $languageDetailListTable->description = 'Detail';
        $languageDetailListTable->created_id = $idUser;
        $languageDetailListTable->save();
        #endregion TAB


        #region Code List
        $fieldListCodeList = new Field();
        $fieldListCodeList->code = 'code_list';
        $fieldListCodeList->tab_id = $tabDetailTable->id;
        $fieldListCodeList->field = 'code_list';
        $fieldListCodeList->data_type = 1;
        $fieldListCodeList->created_id = $idUser;
        $fieldListCodeList->save();

        $fieldLanguageCodeListDetail = new FieldLanguage();
        $fieldLanguageCodeListDetail->field_id = $fieldListCodeList->id;
        $fieldLanguageCodeListDetail->language_id = $idLanguage;
        $fieldLanguageCodeListDetail->description = 'Codice Listino';
        $fieldLanguageCodeListDetail->created_id = $idUser;
        $fieldLanguageCodeListDetail->save();

        $fieldUserRoleCodeListDetail = new FieldUserRole();
        $fieldUserRoleCodeListDetail->field_id = $fieldListCodeList->id;
        $fieldUserRoleCodeListDetail->role_id = $idRole;
        $fieldUserRoleCodeListDetail->enabled = true;
        $fieldUserRoleCodeListDetail->pos_x = 10;
        $fieldUserRoleCodeListDetail->pos_y = 10;
        $fieldUserRoleCodeListDetail->input_type = 0;
        $fieldUserRoleCodeListDetail->created_id = $idUser;
        $fieldUserRoleCodeListDetail->save();
        #endregion Code List

        #region Descrizione
        $fieldListCodeList = new Field();
        $fieldListCodeList->code = 'description';
        $fieldListCodeList->tab_id = $tabDetailTable->id;
        $fieldListCodeList->field = 'description';
        $fieldListCodeList->data_type = 1;
        $fieldListCodeList->created_id = $idUser;
        $fieldListCodeList->save();

        $fieldLanguageCodeListDetail = new FieldLanguage();
        $fieldLanguageCodeListDetail->field_id = $fieldListCodeList->id;
        $fieldLanguageCodeListDetail->language_id = $idLanguage;
        $fieldLanguageCodeListDetail->description = 'Descrizione';
        $fieldLanguageCodeListDetail->created_id = $idUser;
        $fieldLanguageCodeListDetail->save();

        $fieldUserRoleCodeListDetail = new FieldUserRole();
        $fieldUserRoleCodeListDetail->field_id = $fieldListCodeList->id;
        $fieldUserRoleCodeListDetail->role_id = $idRole;
        $fieldUserRoleCodeListDetail->enabled = true;
        $fieldUserRoleCodeListDetail->pos_x = 20;
        $fieldUserRoleCodeListDetail->pos_y = 10;
        $fieldUserRoleCodeListDetail->input_type = 0;
        $fieldUserRoleCodeListDetail->created_id = $idUser;
        $fieldUserRoleCodeListDetail->save();
        #endregion Descrizione

        #region Inizio Validità
        $fieldListCodeList = new Field();
        $fieldListCodeList->code = 'beginning_validity';
        $fieldListCodeList->tab_id = $tabDetailTable->id;
        $fieldListCodeList->field = 'beginning_validity';
        $fieldListCodeList->data_type = 2;
        $fieldListCodeList->created_id = $idUser;
        $fieldListCodeList->save();

        $fieldLanguageCodeListDetail = new FieldLanguage();
        $fieldLanguageCodeListDetail->field_id = $fieldListCodeList->id;
        $fieldLanguageCodeListDetail->language_id = $idLanguage;
        $fieldLanguageCodeListDetail->description = 'Inizio Validità';
        $fieldLanguageCodeListDetail->created_id = $idUser;
        $fieldLanguageCodeListDetail->save();

        $fieldUserRoleCodeListDetail = new FieldUserRole();
        $fieldUserRoleCodeListDetail->field_id = $fieldListCodeList->id;
        $fieldUserRoleCodeListDetail->role_id = $idRole;
        $fieldUserRoleCodeListDetail->enabled = true;
        $fieldUserRoleCodeListDetail->pos_x = 30;
        $fieldUserRoleCodeListDetail->pos_y = 10;
        $fieldUserRoleCodeListDetail->input_type = 4;
        $fieldUserRoleCodeListDetail->created_id = $idUser;
        $fieldUserRoleCodeListDetail->save();
        #endregion Inizio Validità

        #region Fine Validità
        $fieldListCodeList = new Field();
        $fieldListCodeList->code = 'end_validity';
        $fieldListCodeList->tab_id = $tabDetailTable->id;
        $fieldListCodeList->field = 'end_validity';
        $fieldListCodeList->data_type = 2;
        $fieldListCodeList->created_id = $idUser;
        $fieldListCodeList->save();

        $fieldLanguageCodeListDetail = new FieldLanguage();
        $fieldLanguageCodeListDetail->field_id = $fieldListCodeList->id;
        $fieldLanguageCodeListDetail->language_id = $idLanguage;
        $fieldLanguageCodeListDetail->description = 'Fine Validità';
        $fieldLanguageCodeListDetail->created_id = $idUser;
        $fieldLanguageCodeListDetail->save();

        $fieldUserRoleCodeListDetail = new FieldUserRole();
        $fieldUserRoleCodeListDetail->field_id = $fieldListCodeList->id;
        $fieldUserRoleCodeListDetail->role_id = $idRole;
        $fieldUserRoleCodeListDetail->enabled = true;
        $fieldUserRoleCodeListDetail->pos_x = 40;
        $fieldUserRoleCodeListDetail->pos_y = 10;
        $fieldUserRoleCodeListDetail->input_type = 4;
        $fieldUserRoleCodeListDetail->created_id = $idUser;
        $fieldUserRoleCodeListDetail->save();
        #endregion Fine Validità

        #region Valore Listino
        $fieldListCodeList = new Field();
        $fieldListCodeList->code = 'value_list';
        $fieldListCodeList->tab_id = $tabDetailTable->id;
        $fieldListCodeList->field = 'value_list';
        $fieldListCodeList->data_type = 1;
        $fieldListCodeList->created_id = $idUser;
        $fieldListCodeList->save();

        $fieldLanguageCodeListDetail = new FieldLanguage();
        $fieldLanguageCodeListDetail->field_id = $fieldListCodeList->id;
        $fieldLanguageCodeListDetail->language_id = $idLanguage;
        $fieldLanguageCodeListDetail->description = 'Valore Listino';
        $fieldLanguageCodeListDetail->created_id = $idUser;
        $fieldLanguageCodeListDetail->save();

        $fieldUserRoleCodeListDetail = new FieldUserRole();
        $fieldUserRoleCodeListDetail->field_id = $fieldListCodeList->id;
        $fieldUserRoleCodeListDetail->role_id = $idRole;
        $fieldUserRoleCodeListDetail->enabled = true;
        $fieldUserRoleCodeListDetail->pos_x = 50;
        $fieldUserRoleCodeListDetail->pos_y = 10;
        $fieldUserRoleCodeListDetail->input_type = 0;
        $fieldUserRoleCodeListDetail->created_id = $idUser;
        $fieldUserRoleCodeListDetail->save();
        #endregion Valore Listino

    }
}

<?php

use App\Role as Role;
use App\TemplateEditor;
use App\TemplateEditorGroup;
use App\TemplateEditorType;
use App\User as User;
use Illuminate\Database\Seeder;

class NewContentTemplateEditorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $idUser = User::where('name', '=', 'admin')->first()->id;
        
        $template_editor_groups = TemplateEditorType::updateorCreate(['code'   => 'Header'],[ 'description' => 'Header', 'created_id' =>  $idUser]);
        $template_editor_groups = TemplateEditorType::updateorCreate(['code'   => 'Footer'],[ 'description' => 'Footer', 'created_id' =>  $idUser]);
        $template_editor_groups = TemplateEditorType::updateorCreate(['code'   => 'Pages'],[ 'description' => 'Pages', 'created_id' =>  $idUser]);

        $template_editor_type_id= TemplateEditorType::where('code', '=', 'Pages')->first();
        $template_editor_type_id_footer= TemplateEditorType::where('code', '=', 'Footer')->first();
        $template_editor_type_id_header= TemplateEditorType::where('code', '=', 'Header')->first();

        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Base'],[ 'description' => 'Base', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Contatti'],[ 'description' => 'Contatti', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Mappa'],[ 'description' => 'Mappa', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Pulsante'],[ 'description' => 'Pulsante', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Immagini'],[ 'description' => 'Immagini', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Video'],[ 'description' => 'Video', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Newsletter'],[ 'description' => 'Newsletter', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Footer'],[ 'description' => 'Footer', 'template_editor_type_id' =>  $template_editor_type_id_footer->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Header'],[ 'description' => 'Header', 'template_editor_type_id' =>  $template_editor_type_id_header->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Divisori'],[ 'description' => 'Divisori', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Servizi'],[ 'description' => 'Servizi', 'template_editor_type_id' =>  $template_editor_type_id->id]);
        $template_editor_groups = TemplateEditorGroup::updateorCreate(['code'   => 'Email'],[ 'description' => 'Email', 'template_editor_type_id' =>  $template_editor_type_id->id]);

        $template_editor_group= TemplateEditorGroup::where('code', '=', 'Immagini')->first();

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Immagine (100%)'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Immagine_100.png',
        'html' => '<section><div class="section-pages"><div class="container"><div class="row justify-content-center"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Immagine_fullscreen.webp" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></section>',
        'template_editor_group_id' =>  $template_editor_group->id,
        'order' => 20]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Slider (100%)'],[ 
            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Slider_100.jpg',
            'html' => '<section><div class="container"><div id="bxslider"><div><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/slide1.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/slide2.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/slide3.webp" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></section>',
            'template_editor_group_id' =>  $template_editor_group->id,
            'order' => 140]);
       
            $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Immagine (fullscreen)'],[ 
                'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Immagine_fullscreen.png',
                'html' => '<section><div class="section-pages"><div class="container-fluid"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Immagine_fullscreen.webp" style="width: 100%;" class="fr-fic fr-dii"></div></div></section>',
                'template_editor_group_id' =>  $template_editor_group->id,
                'order' => 10]);

                $template_editor= TemplateEditor::updateOrCreate(['description'    => '2 immagini'],[ 
                    'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/2_immagini.png',
                    'html' => '<section><div class="section-pages"><div class="container"><div class="row justify-content-center"><div class="col-sm-12 col-md-6 mb-4 mb-md-0"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/fireworks-1.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-sm-12 col-md-6 mb-4 mb-md-0"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/fireworks-2.webp" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></div></section>',
                    'template_editor_group_id' =>  $template_editor_group->id,
                    'order' => 40]);

                    $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Immagine (70%)'],[ 
                        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Immagine_70.png',
                        'html' => '<section><div class="section-pages"><div class="container"><div class="row justify-content-center"><div class="col-sm-12 col-md-8"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Immagine_fullscreen.webp" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></div></section>',
                        'template_editor_group_id' =>  $template_editor_group->id,
                        'order' => 30]);

                        $template_editor= TemplateEditor::updateOrCreate(['description'    => '2 immagini (senza margine)'],[ 
                            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/2_immagini_nomargine.png',
                            'html' => '<section><div class="section-pages"><div class="container"><div class="row justify-content-center"><div class="col-12 col-md-6"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/fireworks-1.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-6"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/fireworks-2.webp" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></div></section>',
                            'template_editor_group_id' =>  $template_editor_group->id,
                            'order' => 70]);
                
                            $template_editor= TemplateEditor::updateOrCreate(['description'    => '4 immagini'],[ 
                                'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/4_immagini.png',
                                'html' => '<section><div class="section-pages"><div class="container"><div class="row justify-content-center"><div class="col-sm-12 col-md-6 col-lg-3 mb-4 mb-lg-0"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/people/4.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-sm-12 col-md-6 col-lg-3 mb-4 mb-lg-0"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/people/6.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-sm-12 col-md-6 col-lg-3 mb-4 mb-lg-0"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/people/3.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-sm-12 col-md-6 col-lg-3 mb-4 mb-lg-0"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/people/9.webp" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></div></section>',
                                'template_editor_group_id' =>  $template_editor_group->id,
                                'order' => 60]);

                                $template_editor= TemplateEditor::updateOrCreate(['description'    => '3 immagini (senza margine)'],[ 
                                    'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/3_immagini_nomargine.png',
                                    'html' => '<section><div class="section-pages"><div class="container"><div class="row justify-content-center"><div class="col-12 col-md-4"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/map-1.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-4"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/map-2.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-4"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/map-3.webp" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></div></section>',
                                    'template_editor_group_id' =>  $template_editor_group->id,
                                    'order' => 80]);

                                    $template_editor= TemplateEditor::updateOrCreate(['description'    => '3 immagini'],[ 
                                        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/3_immagini.png',
                                        'html' => '<section><div class="section-pages"><div class="container"><div class="row justify-content-center"><div class="col-sm-12 col-md-4 mb-4 mb-md-0"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/map-1.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-sm-12 col-md-4 mb-4 mb-md-0"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/map-2.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-sm-12 col-md-4 mb-4 mb-md-0"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/map-3.webp" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></div></section>',
                                        'template_editor_group_id' =>  $template_editor_group->id,
                                        'order' => 50]);

                                    $template_editor= TemplateEditor::updateOrCreate(['description'    => '4 immagini (senza margine)'],[ 
                                        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/4_immagini_nomargine.png',
                                        'html' => '<section><div class="section-pages"><div class="container"><div class="row justify-content-center"><div class="col-12 col-md-3"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/people/4.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-3"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/people/6.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-3"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/people/3.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-3"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/people/9.webp" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></div></section>',
                                        'template_editor_group_id' =>  $template_editor_group->id,
                                        'order' => 90]);
    
                                        $template_editor= TemplateEditor::updateOrCreate(['description'    => '2 immagini (fullscreen)'],[ 
                                            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/2_immagini_fullscreen.png',
                                            'html' => '<section><div class="container-fluid"><div class="row justify-content-center"><div class="col-12 col-md-6"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/fireworks-1.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-6"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/fireworks-2.webp" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></section>',
                                            'template_editor_group_id' =>  $template_editor_group->id,
                                            'order' => 100]);

                                            $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Slider (fullscreen)'],[ 
                                                'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Slider_fullscreen.png',
                                                'html' => '<section><div class="container-fluid"><div id="bxslider"><div><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/slide1.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/slide2.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/slide3.webp" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></section>',
                                                'template_editor_group_id' =>  $template_editor_group->id,
                                                'order' => 130]);

                                                $template_editor= TemplateEditor::updateOrCreate(['description'    => '3 immagini (fullscreen)'],[ 
                                                    'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/3_immagini_fullscreen.png',
                                                    'html' => '<section><div class="container-fluid"><div class="row justify-content-center"><div class="col-12 col-md-4"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/map-1.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-4"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/map-2.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-4"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/map-3.webp" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></section>',
                                                    'template_editor_group_id' =>  $template_editor_group->id,
                                                    'order' => 110]);

                                                    $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Ticker (fullscreen)'],[ 
                                                        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/4_immagini_fullscreen.png',
                                                        'html' => '<section><div class="container-fluid"><div id="ticker"><ul><li><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/ticker1.webp" class="fr-fic fr-dii"></li><li><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/ticker2.webp" class="fr-fic fr-dii"></li><li><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/ticker3.webp" class="fr-fic fr-dii"></li><li><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/ticker4.webp" class="fr-fic fr-dii"></li><li><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/ticker5.webp" class="fr-fic fr-dii"></li><li><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/ticker6.webp" class="fr-fic fr-dii"></li><li><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/ticker7.webp" class="fr-fic fr-dii"></li><li><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/ticker8.webp" class="fr-fic fr-dii"></li><li><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/ticker9.webp" class="fr-fic fr-dii"></li><li><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/ticker10.webp" class="fr-fic fr-dii"></li></ul></div></div></section>',
                                                        'template_editor_group_id' =>  $template_editor_group->id,
                                                        'order' => 150]);

                                                        $template_editor= TemplateEditor::updateOrCreate(['description'    => '4 immagini (fullscreen)'],[ 
                                                            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/ticker.png',
                                                            'html' => '<section><div class="container-fluid"><div class="row justify-content-center"><div class="col-12 col-md-3"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/people/4.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-3"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/people/6.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-3"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/people/3.webp" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-3"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/people/9.webp" style="width: 100%;" class="fr-fic fr-dii"></div></div></div></section>',
                                                            'template_editor_group_id' =>  $template_editor_group->id,
                                                            'order' => 120]);

        
        $template_editor_groupContact = TemplateEditorGroup::where('code', '=', 'Contatti')->first();

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Form contatti'],[ 
            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/FormContatti.JPG',
            'html' => '<div class="section-pages"><div class="container"><form id="contactRequest"><div class="row"><div class="col-4"><label for="name"><strong>Nome</strong></label></div><div class="col-4"><label for="surname"><strong>Cognome</strong></label></div><div class="col-4"><label for="email"><strong>Email</strong></label></div></div><div class="row"><div class="col-4"><input id="name" name="name" type="text" required="required" value=""></div><div class="col-4"><input id="surname" name="surname" type="text" required="required" value=""></div><div class="col-4"><input id="email" name="email" type="email" required="required" value=""></div></div><div class="row"><div class="col-12"><label for="description"><strong>Richiesta</strong></label></div></div><div class="row"><div class="col-12"><textarea id="description" name="description" required="required" value="" style="width: 100%;" rows="5"></textarea></div></div><div class="row"><div class="col-12 text-right"><button class="btn btn-primary" id="btnSend" type="button">Invia richiesta</button></div></div></form></div></div>',
            'template_editor_group_id' =>  $template_editor_groupContact->id,
            'order' => 70]);

            $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Form contatto small'],[ 
            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Contatti_1.png',
            'html' => '<section>
            <div class="container">
            <div class="row justify-content-center">
            <div class="col-sm-12 col-md-5" style="box-shadow:#a7a7a7 0 0px 10px;padding: 1rem 0;border-radius: 0.5rem;background: #fff;">
            <div style="width:95%;margin-left:3%;">
            <p style="font-size: 1.6rem; font-weight: bold;">Contatti</p>
            <p style="line-height: 1.1rem;">Come possiamo aiutarti?
            <br>Digita la tua richiesta nell&#39;apposito spazio, cercheremo di risponderti nel pi&ugrave; breve tempo possibile!</p>[form id=1 form]</div></div></div></div>
            </section>',
            'template_editor_group_id' =>  $template_editor_groupContact->id,
            'order' => 10]);

            $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Form contatto medio con recapiti'],[ 
            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Contatti_2.png',
            'html' => '<section>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-8" style="box-shadow:#a7a7a7 0 0px 10px;border-radius: 0.2rem;">
                            <div class="row justify-content-center">
                                <div class="col-sm-12 col-md-6" style="background: url(https://selena.tecnotrade.com/api/storage/images/TemplateEditor/shapes/6.svg) no-repeat center center;background-size: 90%; padding:2rem;">
                                    <p style="margin-bottom:0rem; font-size: 1.3rem; font-weight: bold;">Entriamo in contatto</p>
                                    <p style="line-height: 1.1rem;margin-bottom: 2rem;">Come possiamo aiutarti?</p>
                                    <div class="row justify-content-center">
                                        <div class="col-sm-1"><i class="fa fa-map-marker" style="color: #777;font-size: 1.5em;"></i></div>
                                        <div class="col-sm-10">
                                            <p style="line-height: 1.3rem;">Via Milano 456
                                                <br>Bologna</p>
                                        </div></div>
                                    <div class="row justify-content-center">
                                        <div class="col-sm-1"><i class="fa fa-phone" style="color: #777;font-size: 1.5em;"></i></div>
                                        <div class="col-sm-10">
                                            <p style="line-height: 1.3rem;"><a href="tel:+39 051 0019916">+39 051 0019916</a>
                                                <br>
                                                <br>
                                            </p>
                                        </div></div>
                                    <div class="row justify-content-center">
                                        <div class="col-sm-1"><i class="fa fa-envelope-o" style="color: #777;font-size: 1.5em;"></i></div>
                                        <div class="col-sm-10">
                                            <p style="line-height: 1.3rem;"><a href="mailto:info@tecnotrade.com">info@tecnotrade.com</a>
                                                <br><a href="mailto:support@tecnotrade.com">support@tecnotrade.com</a></p>
                                        </div></div></div>
                                <div class="col-sm-12 col-md-6" style="background:url(https://selena.tecnotrade.com/api/storage/images/TemplateEditor/hero/blue.svg); padding:2rem;">
                                    <div style="width:95%;margin-left:3%;">[form id=1 form]</div></div></div></div></div></div>
            </section>',
            'template_editor_group_id' =>  $template_editor_groupContact->id,
            'order' => 20]);

            $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Form contatto grande con recapiti'],[ 
            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Contatti_3.png',
            'html' => '<section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-12" style="box-shadow:#a7a7a7 0 0px 10px;padding: 2rem 0;border-radius: 0.5rem;background: #fff;">
                    <div class="row justify-content-center">
                        <div class="col-sm-12 col-md-3 text-center" style="border-right: 1px solid #ccc;border-image:linear-gradient(transparent 0%, #ccc 50% 90%, transparent 100%) 0 1 0 0 / 2px;"><i class="fa fa-map-marker" style="color: #d7d7d7;font-size: 2.5em;padding: 1rem 0 0.2rem 0;"></i>

                            <p style="font-size: 1.1rem; font-weight: bold; margin-bottom:0;">Indirizzo</p>

                            <p style="line-height: 1.1rem;">Via Milano 456
                                <br>Bologna</p><i class="fa fa-phone" style="color: #d7d7d7;font-size: 2.5em;padding: 1rem 0 0.2rem 0;"></i>

                            <p style="font-size: 1.1rem; font-weight: bold; margin-bottom:0;">Telefono</p>

                            <p style="line-height: 1.1rem;"><a href="tel:+39 051 0019916">+39 051 0019916</a></p><i class="fa fa-envelope-o" style="color: #d7d7d7;font-size: 2.5em;padding: 1rem 0 0.2rem 0;"></i>

                            <p style="font-size: 1.1rem; font-weight: bold; margin-bottom:0;">Email</p>

                            <p style="line-height: 1.1rem;"><a href="mailto:info@tecnotrade.com">info@tecnotrade.com</a>
                                <br><a href="mailto:support@tecnotrade.com">support@tecnotrade.com</a></p>
                        </div>
                        <div class="col-sm-12 col-md-9">
                            <div style="width:95%;margin-left:3%;">

                                <p style="padding:1rem 0 .3rem; font-size: 1.6rem; font-weight: bold;">Inviaci un messaggio</p>

                                <p style="line-height: 1.1rem;">Come possiamo aiutarti?
                                    <br>Digita la tua richiesta nell&#39;apposito spazio, cercheremo di risponderti nel pi&ugrave; breve tempo possibile!</p>[form id=1 form]</div></div></div></div></div></div>
        </section>',
            'template_editor_group_id' =>  $template_editor_groupContact->id,
            'order' => 30]);






                 
        $template_editor_groupVideo = TemplateEditorGroup::where('code', '=', 'Video')->first();

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'YouTube video (100%)'],[ 
            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/video-01.png',
            'html' => '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12 videoWrapper"><iframe width="100%" height="315" src="https://www.youtube.com/embed/mkggXE5e2yk" frameborder="0" allowfullscreen=""></iframe></div></div></div></div>',
            'template_editor_group_id' =>  $template_editor_groupVideo->id,
            'order' => 50]);

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'YouTube video (fit)'],[ 
            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/video-00.png',
            'html' => '<div class="section-pages"><div class="container"><div class="row"><div class="col-12 videoWrapper"><iframe width="100%" height="315" src="https://www.youtube.com/embed/mkggXE5e2yk" frameborder="0" allowfullscreen=""></iframe></div></div></div></div>',
            'template_editor_group_id' =>  $template_editor_groupVideo->id,
            'order' => 60]);


        $template_editor_groupMappa = TemplateEditorGroup::where('code', '=', 'Mappa')->first();

        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Mappa (fit)'],[ 
            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/mappa-01.png',
            'html' => '<section><div class="container p-0 pb-md-5"><iframe class="map" src="https://maps.google.com/maps?hl=en&q=piazza maggiore+(Bologna)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" width="100%" height="500px"></iframe></div></section>',
            'template_editor_group_id' =>  $template_editor_groupMappa->id,
            'order' => 80]);

            $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Mappa (100%)'],[ 
                'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/mappa-02.png',
                'html' => '<div class="section-pages"><div class="container-fluid"><div class="row"><div class="col-12"><div style="height:240px;"><br></div></div></div></div></div><div><br></div>',
                'template_editor_group_id' =>  $template_editor_groupMappa->id,
                'order' => 90]);
    

            $template_editor_groupBase = TemplateEditorGroup::where('code', '=', 'Base')->first();
            
                $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Titolo + Paragrafo'],[ 
                    'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Titolo_Paragrafo.png',
                    'html' => '<section><div class="container"><div class="row justify-content-center"><div class="col-12"><h1>Titolo</h1><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div></div></div></section>',
                    'template_editor_group_id' =>  $template_editor_groupBase->id,
                    'order' => 60]);

                $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Titolo + Sottotitolo'],[ 
                        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Titolo_Sottotitolo.png',
                        'html' => '<section><div class="container"><div class="row justify-content-center"><div class="col-12 text-center"><h1>Titolo</h1><h2>Sottotitolo</h2></div></div></div></section>',
                        'template_editor_group_id' =>  $template_editor_groupBase->id,
                        'order' => 30]);

                $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Paragrafo (70%)'],[ 
                            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Paragrafo70.png',
                            'html' => '<section><div class="container"><div class="row justify-content-center"><div class="col-sm-12 col-md-8"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div></div></div></section>',
                            'template_editor_group_id' =>  $template_editor_groupBase->id,
                            'order' => 50]);

                            $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Sottotitolo + Paragrafo'],[ 
                                'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Sottotitolo_Paragrafo.png',
                                'html' => '<section><div class="container"><div class="row justify-content-center"><div class="col-12"><h2>Sottotitolo</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div></div></div></section>',
                                'template_editor_group_id' =>  $template_editor_groupBase->id,
                                'order' => 70]);

                                $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Sottotitolo'],[ 
                                    'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Sottotitolo.png',
                                    'html' => '<section><div class="container"><div class="row justify-content-center"><div class="col-12 text-center"><h2>Sottotitolo</h2></div></div></div></section>',
                                    'template_editor_group_id' =>  $template_editor_groupBase->id,
                                    'order' => 20]);

                                $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Paragrafo 2 colonne (40% / 60%)'],[ 
                                    'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Paragrafo_2_colonne_40_60.png',
                                    'html' => '<section><div class="container"><div class="row justify-content-center"><div class="col-sm-12 col-md-4"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</p></div><div class="col-sm-12 col-md-8"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div></div></div></section>',
                                    'template_editor_group_id' =>  $template_editor_groupBase->id,
                                    'order' => 90]);

                                    $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Titolo'],[ 
                                        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/titolo.png',
                                        'html' => '<section><div class="container"><div class="row justify-content-center"><div class="col-12 text-center"><h1>Titolo</h1></div></div></div></section>',
                                        'template_editor_group_id' =>  $template_editor_groupBase->id,
                                        'order' => 10]);

                                        $template_editor= TemplateEditor::updateOrCreate(['description'    => 'Titolo + Sottotitolo - Copy'],[ 
                                            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Titolo_Sottotitolo.png',
                                            'html' => '<section><div class="container"><div class="row justify-content-center"><div class="col-12 text-center"><h1>Titolo</h1><h2>Sottotitolo</h2></div></div></div></section>',
                                            'template_editor_group_id' =>  $template_editor_groupBase->id,
                                            'order' => 30]);

                                            $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Paragrafo 3 colonne (33% / 33% / 33%)'],[ 
                                                'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Paragrafo_3_colonne.png',
                                                'html' => '<section><div class="container"><div class="row justify-content-center"><div class="col-sm-12 col-md-4"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</p></div><div class="col-sm-12 col-md-4"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</p></div><div class="col-sm-12 col-md-4"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</p></div></div></div></section>',
                                                'template_editor_group_id' =>  $template_editor_groupBase->id,
                                                'order' => 100]);

                                            $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Paragrafo (100%)'],[ 
                                                    'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Paragrafo100.png',
                                                    'html' => '<section><div class="container"><div class="row justify-content-center"><div class="col-12"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div></div></div></section>',
                                                    'template_editor_group_id' =>  $template_editor_groupBase->id,
                                                    'order' => 40]);

                                                    $template_editor_groupPulsante = TemplateEditorGroup::where('code', '=', 'Pulsante')->first();

                                                    $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Pulsante 7'],[ 
                                                        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-07.png',
                                                        'html' => '<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0;"><div class="container"><div class="row align-items-center justify-content-center"><div class="col-auto"><h2>Clicca qui per scaricare la nostra presentazione</h2></div><div class="col-auto mt-4 mt-sm-0"><a class="btn btn-primary" href="#">Download</a></div></div></div></section>',
                                                        'template_editor_group_id' =>  $template_editor_groupPulsante->id,
                                                        'order' => 40]);

                                                        $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Pulsante 5'],[ 
                                                            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-05.png',
                                                            'html' => '<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0; background-size: cover; background-position: center; background-repeat: no-repeat;"><div class="container py-5 my-5" style="background-image: url(/api/storage/images/shapes/2.svg);background-size: contain; background-position: center; background-repeat: no-repeat;"><div class="row justify-content-center py-5"><div class="col-12 col-md-10 col-lg-8 text-center"><div style="background: #FFFFFF; padding: 3.75rem 2.5rem; overflow: hidden; color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><h1>Call to Action</h1><p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;</p><p class="mt-4"><a class="btn btn-primary" href="#">Download</a></p></div></div></div></div></section>',
                                                            'template_editor_group_id' =>  $template_editor_groupPulsante->id,
                                                            'order' => 40]);

                                                            $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Pulsante 6'],[ 
                                                                'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-06.png',
                                                                'html' => '<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 5rem 0;"><div class="container py-5 my-5 bg-r" style="background-image: url(/api/storage/images/shapes/4.svg);background-size: contain; background-position: center; background-repeat: no-repeat;"><div class="row justify-content-end py-5"><div class="col-12 col-md-8 col-lg-6 mr-5 text-center"><div style="background: #FFFFFF; padding: 3.75rem 2.5rem; overflow: hidden; color: #444444; border-radius: 0.25rem; box-shadow: 0 0.3125rem 0.875rem 0 rgba(129, 129, 129, 0.2) !important;"><h1>Call to Action</h1><p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><p class="mt-4"><a class="btn btn-secondary" href="#">Download</a></p></div></div></div></div></section>',
                                                                'template_editor_group_id' =>  $template_editor_groupPulsante->id,
                                                                'order' => 40]);

                                                                $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Pulsante 8'],[ 
                                                                    'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/pulsante-08.png',
                                                                    'html' => '<section style="-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; text-rendering: optimizelegibility; padding: 7.5rem 0;"><div class="container"><div class="row align-items-center justify-content-center"><div class="col-auto text-center"><a class="btn btn-outline-secondary" href="#">Download</a></div><div class="col-auto mt-4 mt-sm-0"><h2>Clicca qui per scaricare la nostra presentazione</h2></div></div></div></section>',
                                                                    'template_editor_group_id' =>  $template_editor_groupPulsante->id,
                                                                    'order' => 40]);

         $template_editor_groupServizi = TemplateEditorGroup::where('code', '=', 'Servizi')->first();

         $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Paragrafo e Immagine (75% / 25%)'],[ 
            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Testo_immagine_75_25.png',
            'html' => '<section><div class="container"><div class="row align-items-center"><div class="col-12 col-md-9 col-lg-8 align-self-center"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div><div class="col-12 col-md-3 ml-md-auto mt-4 mt-md-0"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/map-1.webp" class="fr-fic fr-dii"></div></div></div></section>',
            'template_editor_group_id' =>  $template_editor_groupServizi->id,
            'order' => 40]); 
         
            $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Paragrafo e Immagine (50% / 50%)'],[ 
                'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Testo_immagine_50_50.png',
                'html' => '<section><div class="container"><div class="row align-items-center"><div class="col-12 col-md-6 col-lg-5 align-self-center"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div><div class="col-12 col-md-6 ml-md-auto mt-4 mt-md-0"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/map-1.webp" class="fr-fic fr-dii"></div></div></div></section>',
                'template_editor_group_id' =>  $template_editor_groupServizi->id,
                'order' => 20]); 

                $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Immagine e paragrafo (25% / 75%)'],[ 
                    'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Immagine_testo_25_75.png',
                    'html' => '<section><div class="container"><div class="row align-items-center"><div class="col-12 col-md-3 mb-4 mb-md-0 order-sm-last order-lg-first"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/map-1.webp" class="fr-fic fr-dii"></div><div class="col-12 col-md-9 col-lg-8 ml-md-auto text-left order-sm-first order-lg-last"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div></div></div></section>',
                    'template_editor_group_id' =>  $template_editor_groupServizi->id,
                    'order' => 30]); 

                    $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Titolo + immagine + paragrafo (2 colonne)'],[ 
                        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Titolo_Immagine_Paragrafo_2.png',
                        'html' => '<section><div class="container"><div class="row align-items-center"><div class="col-sm-12 col-md-6"><h2>Titolo</h2><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Servizi1.webp" class="mb-4 fr-fic fr-dii"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div><div class="col-sm-12 col-md-6"><h2>Titolo</h2><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Servizi2.webp" class="mb-4 fr-fic fr-dii"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div></div></div></section>',
                        'template_editor_group_id' =>  $template_editor_groupServizi->id,
                        'order' => 70]); 
    
                        $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Titolo + immagine + paragrafo (3 colonne)'],[ 
                            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Titolo_Immagine_Paragrafo_3.png',
                            'html' => '<section><div class="container"><div class="row align-items-center"><div class="col-sm-12 col-md-4"><h2>Titolo</h2><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Servizi1.webp" class="mb-4 fr-fic fr-dii"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div><div class="col-sm-12 col-md-4"><h2>Titolo</h2><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Servizi2.webp" class="mb-4 fr-fic fr-dii"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div><div class="col-sm-12 col-md-4"><h2>Titolo</h2><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Servizi3.webp" class="mb-4 fr-fic fr-dii"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div></div></div></section>',
                            'template_editor_group_id' =>  $template_editor_groupServizi->id,
                            'order' => 80]); 

                            $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Titolo + immagine + paragrafo (4 colonne)'],[ 
                                'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Titolo_Immagine_Paragrafo_4.png',
                                'html' => '<section><div class="container"><div class="row align-items-center"><div class="col-sm-12 col-md-6 col-lg-3"><h2>Titolo</h2><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Servizi1.webp" class="mb-4 fr-fic fr-dii"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div><div class="col-sm-12 col-md-6 col-lg-3"><h2>Titolo</h2><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Servizi2.webp" class="mb-4 fr-fic fr-dii"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div><div class="col-sm-12 col-md-6 col-lg-3"><h2>Titolo</h2><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Servizi3.webp" class="mb-4 fr-fic fr-dii"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div><div class="col-sm-12 col-md-6 col-lg-3"><h2>Titolo</h2><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Servizi4.webp" class="mb-4 fr-fic fr-dii"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div></div></div></section>',
                                'template_editor_group_id' =>  $template_editor_groupServizi->id,
                                'order' => 90]); 

                                $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Immagine e paragrafo (50% / 50%)'],[ 
                                    'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Immagine_testo_50_50.png',
                                    'html' => '<section><div class="container"><div class="row align-items-center"><div class="col-12 col-md-6 mb-4 mb-md-0 order-sm-last order-lg-first"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/map-1.webp" class="fr-fic fr-dii"></div><div class="col-12 col-md-6 col-lg-5 ml-md-auto text-left order-sm-first order-lg-last"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p></div></div></div></section>',
                                    'template_editor_group_id' =>  $template_editor_groupServizi->id,
                                    'order' => 10]); 

                                    $template_editor = TemplateEditor::updateOrCreate(['description'    => '2 Immagini e paragrafo'],[ 
                                        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/2_Immagine_testo.png',
                                        'html' => '<section><div class="container"><div class="row align-items-center"><div class="col-sm-12 col-md-6 col-lg-3 mb-4 mb-md-0 order-md-last order-lg-first"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/fireworks-1.webp" class="fr-fic fr-dii"></div><div class="col-sm-12 col-md-6 col-lg-3 mb-4 mb-md-0 order-md-last order-lg-first"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/fireworks-2.webp" class="fr-fic fr-dii"></div><div class="col-sm-12 col-lg-6 ml-md-auto text-left order-md-first order-lg-last"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.&nbsp;</p></div></div></div></section>',
                                        'template_editor_group_id' =>  $template_editor_groupServizi->id,
                                        'order' => 50]); 

                                        $template_editor = TemplateEditor::updateOrCreate(['description'    => '2 Paragrafi con titolo e immagine'],[ 
                                            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/2_paragrafi_immagine.png',
                                            'html' => '<section><div class="container"><div class="row align-items-center"><div class="col-sm-12 col-md-6 col-lg-4"><h2>Titolo</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div><div class="col-sm-12 col-md-6 col-lg-4 pt-4 pt-md-0"><h2>Titolo</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></div><div class="col-sm-12 col-md-8 m-auto m-lg-0 col-lg-4 pt-5 pt-lg-0"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Servizi3.webp" class="fr-fic fr-dii"></div></div></div></section>',
                                            'template_editor_group_id' =>  $template_editor_groupServizi->id,
                                            'order' => 60]); 
            
                                         $template_editor_groupNewsletter = TemplateEditorGroup::where('code', '=', 'Newsletter')->first();            

                                         $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Iscrizione con titolo'],[ 
                                            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/iscrizione_newsletter.png',
                                            'html' => '<section><form id="contactSubscribe"><div class="container"><div class="row justify-content-center"><div class="col-12 col-md-8 col-lg-6 text-center"><h2>Iscriviti alla Newsletter</h2><div class="input-group mt-4 mb-1"><input type="text" class="form-control" id="emailnewsletter" name="emailnewsletter" placeholder="Inserisci la tua email"><div class="input-group-append"><button class="btn btn-primary" id="btnSendNewsletter" type="button">Conferma la tua iscrizione</button></div></div><div class="text-left" style="color:#6d6d6d;"><p style="font-size:12px; margin-bottom:0;">Presto il consenso al trattamento dei miei dati ai fini dell&#39;invio di comunicazioni commerciali</p><div class="form-check form-check-inline" style="font-size:12px;"><input class="form-check-input" type="radio" name="processing_of_personal_data" id="processing_of_personal_data1"> <label class="form-check-label" for="processing_of_personal_data1" style="margin-right: 1rem;">Accetto</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="form-check-input" type="radio" name="processing_of_personal_data" id="processing_of_personal_data2"> <label class="form-check-label" for="processing_of_personal_data2">Non accetto</label></div></div></div></div></div></form></section>',
                                            'template_editor_group_id' =>  $template_editor_groupNewsletter->id,
                                            'order' => 10]); 

                                            $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Iscrizione con titolo in box'],[ 
                                                'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/iscrizione_box.png',
                                                'html' => '<section><form id="contactSubscribe"><div class="container"><div class="row justify-content-center"><div class="col-sm-12 col-md-8 col-lg-6 text-center" style="background:#363945;padding:3rem 2rem 1rem;border-radius:1.5rem;box-shadow:#666 0 5px 10px;"><div style="position: absolute; top: -11%;left: 46%;background: #1C8EB7;padding: 2%; border-radius: 50%;"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/icons/mail.svg" style="width:30px;" class="fr-fic fr-dii"></div><h2 style="color:#fff;">Iscriviti alla Newsletter</h2><p style="color:#ddd;">Rimani aggiornato sulle nostre novit&agrave; e sulle nostre soluzioni</p><div class="input-group mt-4 mb-4"><input type="text" class="form-control" id="emailnewsletter" name="emailnewsletter" placeholder="Inserisci la tua email"><div class="input-group-append"><button class="btn btn-primary" id="btnSendNewsletter" type="button">Conferma la tua iscrizione</button></div></div></div></div></div></form></section>',
                                                'template_editor_group_id' =>  $template_editor_groupNewsletter->id,
                                                'order' => 30]); 

                                                $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Iscrizione con immagine titolo e paragrafo'],[ 
                                                    'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/iscrizione_immagine_titolo_paragrafo.png',
                                                    'html' => '<section>
	<form id="contactSubscribe">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-12 col-md-6 m-md-auto ml-lg-0 col-lg-5"><img alt="image" class="img-fluid fr-fic fr-dii" src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/draws/group-chat.svg"></div>
				<div class="col-12 col-md-10 col-lg-6 mt-4 mt-lg-0 ml-auto mr-auto ml-lg-auto text-left">
					<h2>Iscriviti alla Newsletter</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
					<div class="input-group mt-4 mb-4">
						<input type="text" class="form-control" id="emailnewsletter" name="emailnewsletter" placeholder="Inserisci la tua email">
						<div class="input-group-append">
							<button class="btn btn-primary" id="btnSendNewsletter" type="button">Conferma la tua iscrizione</button>
						</div></div></div></div></div>
	</form>
</section>
',
                                                    'template_editor_group_id' =>  $template_editor_groupNewsletter->id,
                                                    'order' => 40]); 

                                                    $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Iscrizione con titolo in linea'],[ 
                                                        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/titolo_linea.png',
                                                        'html' => '<section><form id="contactSubscribe"><div class="container"><div class="row text-left"><div class="col-12 col-md-7 align-self-center"><p style="margin-bottom:0;">ISCRIVITI ALLA NOSTRA</p><h2>NEWSLETTER</h2></div><div class="col-12 col-md-5 align-self-center"><div class="input-group"><input type="text" class="form-control" id="emailnewsletter" name="emailnewsletter" placeholder="Inserisci il tuo indirizzo email" style="padding: 30px 0.75rem!important;"><div class="input-group-append"><button class="btn btn-primary" id="btnSendNewsletter" style="border: 0.125rem solid #141414; padding: 0.45rem 2.625rem; margin-left: -1px; margin-top: 0px;background-color: #141414;margin-bottom: 0px;" type="button">ISCRIVITI</button></div></div></div></div></div></form></section>',
                                                        'template_editor_group_id' =>  $template_editor_groupNewsletter->id,
                                                        'order' => 20]); 

                                                        $template_editor_groupHeader = TemplateEditorGroup::where('code', '=', 'Header')->first(); 
                                            
                                                        $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Header 1'],[ 
                                                            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/header-01.jpg',
                                                            'html' => '<section data-id-template-selena="1"><div class="section-pages"><div class="container"><div class="row" style="padding: 1rem 0;"><div class="col-4 offset-md-0 offset-4 col-md-1 nopadding text-center text-md-left"><img src="/api/storage/images/logo.png" style="width: 100%;" class="fr-fic fr-dii"></div><div class="col-12 col-md-3 offset-md-4" style="margin-top: 20px;"><div class="row"><div class="col-3" style="text-align: right; padding-right: 0px;"><i class="fa fa-phone" style="background: #e5e5e5; color: #253a8e; width: 50px; height: 50px;line-height: 50px; font-size: 26px; border-radius: 50%;text-align: center;"></i></div><div class="col-9" style="margin-top: 3px;"><a href="tel:+39051123456">+39 051 123456</a><br><a href="mailto:info@azienda.com">info@azienda.com</a></div></div></div><div class="col-12 col-md-4" style="margin-top: 20px;"><div class="row"><div class="col-3" style="text-align: right; padding-right: 0px;"><i class="fa fa-map-marker" style="background: #e5e5e5; color: #253a8e; width: 50px; height: 50px;line-height: 50px; font-size: 26px; border-radius: 50%;text-align: center;"></i></div><div class="col-9" style="margin-top: 3px;"><a href="/contatti">Viale della Libert&agrave;, 1<br>40010 BOLOGNA (BO)</a></div></div></div></div></div></div></section><div class="container"><div class="row menu">#menu#</div></div>',
                                                            'template_editor_group_id' =>  $template_editor_groupHeader->id,
                                                            'order' => 100]); 

                                                            $template_editor_groupFooter = TemplateEditorGroup::where('code', '=', 'Footer')->first(); 
                                                            $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Footer 1'],[ 
                                                                'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/footer-01.jpg',
                                                                'html' => '<section data-id-template-selena="20"><div class="section-pages"><div class="container"><div class="row"><div class="col-4 offset-4 col-md-1 offset-md-0"><img src="/api/storage/images/logo-white.png" style="width:100%; margin-bottom:3rem;" class="fr-fic fr-dib"></div><div class="col-12 col-md-3 offset-md-1"><h1>CONTATTI</h1><p>VIALE LIBERT, 1<br>40010 BOLOGNA (BO)<br>Tel. <a href="tel:+39051123456">+39 051 123456</a><br>E-mail <a href="mailto:info@azienda.com">info@azienda.com</a></p></div><div class="col-12 col-md-3"><h1>LEGAL</h1><p>&copy; AZIENDA SRL<br>P.Iva e C.F.: 02345678901<br>C.S. 10.000,00 &euro; i.v.<br>Numero REA - BO - 123456<br><a href="/informativa">Informativa sito</a></p></div><div class="col-12 col-md-3"><h1>SOCIAL</h1><p><a href="#" target="_blank"><i class="fa fa-linkedin-square fa-2x fr-deletable"></i> Linkedin</a><br>Visita il nostro profilo su Linkedin e rimani aggiornato sugli ultimi progetti</p></div></div><div class="row" style="margin-top:4rem;"><div class="col-12 text-center"><a href="https://tecnotrade.com" style="font-size: 12px;" target="_blank">&nbsp;<span style="color:#3EA662;">Made</span> in <span style="color:#F64846;">Italy</span> with <i class="fa fa-heart fr-deletable" style="color:#F64846;"></i> by Tecnotrade</a></div></div></div></div></section>',
                                                                'template_editor_group_id' =>  $template_editor_groupFooter->id,
                                                                'order' => 120]); 


                                                            $template_editor_groupDivisori = TemplateEditorGroup::where('code', '=', 'Divisori')->first();

                                                            $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Spaziatore 240 px'],[ 
                                                                'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Spaziatore_240.png',
                                                                'html' => '<section><div style="height:240px;"><br></div></section>',
                                                                'template_editor_group_id' =>  $template_editor_groupDivisori->id,
                                                                'order' => 50]); 
                                
                                                                $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Spaziatore 120 px'],[ 
                                                                    'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Spaziatore_120.png',
                                                                    'html' => '<section><div style="height:120px;"><br></div></section>',
                                                                    'template_editor_group_id' =>  $template_editor_groupDivisori->id,
                                                                    'order' => 40]); 


                                                                    $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Linea orizzontale fullscreen'],[ 
                                                                        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/linea_fullscreen.png',
                                                                        'html' => '<section><div class="container-fluid"><div class="justify-content-center"><div style="height:1px;background-color:#333;"><br></div></div></div></section>',
                                                                        'template_editor_group_id' =>  $template_editor_groupDivisori->id,
                                                                        'order' => 110]); 

                                                                        $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Linea orizzontale 70%'],[ 
                                                                            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/linea_70.png',
                                                                            'html' => '<section><div class="container-fluid"><div class="row justify-content-center"><div class="col-8"><div style="height:1px;background-color:#333;"><br></div></div></div></div></section>',
                                                                            'template_editor_group_id' =>  $template_editor_groupDivisori->id,
                                                                            'order' => 90]); 

                                                                            $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Linea orizzontale 30%'],[ 
                                                                                'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/linea_30.png',
                                                                                'html' => '<section><div class="container-fluid"><div class="row justify-content-center"><div class="col-4"><div style="height:1px;background-color:#333;"><br></div></div></div></div></section>',
                                                                                'template_editor_group_id' =>  $template_editor_groupDivisori->id,
                                                                                'order' => 80]); 

                                                                                $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Spaziatore 480 px'],[ 
                                                                                    'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Spaziatore_480.png',
                                                                                    'html' => '<section><div style="height:480px;"><br></div></section>',
                                                                                    'template_editor_group_id' =>  $template_editor_groupDivisori->id,
                                                                                    'order' => 60]); 

                                                                                    $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Spaziatore 30 px'],[ 
                                                                                        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Spaziatore_30.png',
                                                                                        'html' => '<section><div style="height:30px;"><br></div></section>',
                                                                                        'template_editor_group_id' =>  $template_editor_groupDivisori->id,
                                                                                        'order' => 20]); 

                                                                                        $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Linea orizzontale 10%'],[ 
                                                                                            'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/linea_10.png',
                                                                                            'html' => '<section><div class="container-fluid"><div class="row justify-content-center"><div class="col-1"><div style="height:1px;background-color:#333;"><br></div></div></div></div></section>',
                                                                                            'template_editor_group_id' =>  $template_editor_groupDivisori->id,
                                                                                            'order' => 70]); 

                                                                                            $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Spaziatore 60 px'],[ 
                                                                                                'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Spaziatore_60.png',
                                                                                                'html' => '<section><div style="height:60px;"><br></div></section>',
                                                                                                'template_editor_group_id' =>  $template_editor_groupDivisori->id,
                                                                                                'order' => 30]); 

                                                                                                $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Linea orizzontale 100%'],[ 
                                                                                                    'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/linea_100.png',
                                                                                                    'html' => '<section><div class="container-fluid"><div class="row justify-content-center"><div class="col-12"><div style="height:1px;background-color:#333;"><br></div></div></div></div></section>',
                                                                                                    'template_editor_group_id' =>  $template_editor_groupDivisori->id,
                                                                                                    'order' => 100]); 

                                                                                                    $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Spaziatore 10 px'],[ 
                                                                                                        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Spaziatore_10.png',
                                                                                                        'html' => '<section><div style="height:10px;"><br></div></section>',
                                                                                                        'template_editor_group_id' =>  $template_editor_groupDivisori->id,
                                                                                                        'order' => 10]); 
                                                
                                            

                            
     $template_editor_groupEmail = TemplateEditorGroup::where('code', '=', 'Email')->first();

        $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Header con logo'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Email_header_1.png',
        'html' => '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%; max-width: 600px;">
        <tbody><tr><td align="center"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/email/your-logo.jpg" alt="logo" title="logo" class="fr-fic fr-dii"></td>
            </tr>
        </tbody>
     </table>',
        'template_editor_group_id' =>  $template_editor_groupEmail->id,
        'order' => 10]); 

        $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Header con logo e menu'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Email_header_2.jpg',
        'html' => '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%; max-width: 600px;">
	<tbody>
		<tr>
			<td width="40%"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/email/your-logo.jpg" alt="logo" title="logo" class="fr-fic fr-dii"></td>
			<td align="right" valign="bottom" width="60%">

				<p style="display:inline-block; margin-right:20px;"><a href="">LINK 1</a></p>

				<p style="display:inline-block; margin-right:20px;"><a href="">LINK 2</a></p>

				<p style="display:inline-block; margin-right:20px;"><a href="">LINK 3</a></p>

				<p style="display:inline-block; margin-right:20px;"><a href="">LINK 4</a></p>
			</td>
		</tr>
	</tbody>
</table>
',
        'template_editor_group_id' =>  $template_editor_groupEmail->id,
        'order' => 20]); 
                                                                                                                                                  

     $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Titolo'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Email_titolo.jpg',
        'html' => '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%; max-width: 600px;">
	<tbody>
		<tr>
			<td width="100%">
				<h1>Titolo principale</h1>
			</td>
		</tr>
	</tbody>
</table>',
        'template_editor_group_id' =>  $template_editor_groupEmail->id,
        'order' => 40]); 
  
           
        $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Menu'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Email_menu.jpg',
        'html' => '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%; max-width: 600px;">
	<tbody>
		<tr>
			<td align="center" width="100%">
				<p style="display:inline-block; margin-right:20px;"><a href="">LINK 1</a></p>
				<p style="display:inline-block; margin-right:20px;"><a href="">LINK 2</a></p>
				<p style="display:inline-block; margin-right:20px;"><a href="">LINK 3</a></p>
				<p style="display:inline-block; margin-right:20px;"><a href="">LINK 4</a></p>
				<p style="display:inline-block; margin-right:20px;"><a href="">LINK 5</a></p>
				<p style="display:inline-block; margin-right:20px;"><a href="">LINK 6</a></p>
			</td>
		</tr>
	</tbody>
</table>',
        'template_editor_group_id' =>  $template_editor_groupEmail->id,
        'order' => 30]); 
                          
        $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Testo'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Email_Paragrafo.png',
        'html' => '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%; max-width: 600px;">
	<tbody>
		<tr>
			<td width="100%">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</td>
		</tr>
	</tbody>
</table>',
        'template_editor_group_id' =>  $template_editor_groupEmail->id,
        'order' => 60]); 
                           
        $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Titolo con button'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Email_titolo_button.jpg',
        'html' => '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%; max-width: 600px;">
        <tbody>
            <tr>
                <td width="70%">

                    <h1>Titolo principale</h1>
                </td>
                <td align="right" width="30%"><a href="" style="background:#d7d7d7;border-radius:20px;padding:.5rem 1rem;">SCOPRI</a></td>
            </tr>
        </tbody>
    </table>',
        'template_editor_group_id' =>  $template_editor_groupEmail->id,
        'order' => 50]); 

           $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Immagine'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Email_Immagine.png',
        'html' => '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%; max-width: 600px;">
            <tbody>
                <tr>
                    <td align="center"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Immagine_fullscreen.webp" alt="logo" title="logo" class="fr-fic fr-dii"></td>
                </tr>
            </tbody>
        </table>',
        'template_editor_group_id' =>  $template_editor_groupEmail->id,
        'order' => 70]); 


        $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Immagine e testo'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Email_Immagine_testo.jpg',
        'html' => '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%; max-width: 600px;">
        <tbody>
            <tr>
                <td valign="top" width="40%"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Immagine_fullscreen.webp" alt="logo" title="logo" class="fr-fic fr-dii"></td>
                <td valign="top" width="60%">

                    <p style="margin-left:20px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                </td>
            </tr>
        </tbody>
    </table>',
        'template_editor_group_id' =>  $template_editor_groupEmail->id,
        'order' => 80]); 

         $template_editor = TemplateEditor::updateOrCreate(['description'    => 'Footer'],[ 
        'img' => 'https://selena.tecnotrade.com/api/storage/images/TemplateEditor/Previews/Email_Immagine_testo.jpg',
        'html' => '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%; max-width: 600px;">
            <tbody>
                <tr>
                    <td align="center"><img src="https://selena.tecnotrade.com/api/storage/images/TemplateEditor/email/your-logo.jpg" width="25%" alt="logo" title="logo" class="fr-fic fr-dii">
                        <br>
                        <p style="color:#ddd; padding:2rem 0; font-size:.8rem;">Copyright &copy; Company Name
                            <br>Tutti i diritti riservati - All rights reserved
                            <br>
                            <br><a href="mailto:info@company.it" style="color:#666;">info@company.it</a>
                            <br>
                            <br><a href="https://company.it/unsubscribe" rel="noopener noreferrer" style="color:#666;" target="_blank">Unsubscribe | Cancellami</a></p>
                    </td>
                </tr>
            </tbody>
        </table>',
        'template_editor_group_id' =>  $template_editor_groupEmail->id,
        'order' => 90]);                                      
                                            
                        
     
    }
}

<?php

use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class ContentConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        #region CONTENTS

        $functionalityContents = new Functionality();
        $functionalityContents->code = 'Contents';
        $functionalityContents->created_id = $idUser;
        $functionalityContents->save();

        $iconServer = new Icon();
        $iconServer->description = 'Server';
        $iconServer->css = 'fa fa-server';
        $iconServer->created_id = $idUser;
        $iconServer->save();

        #region MENU

        $menuAdminContents = new MenuAdmin();
        $menuAdminContents->code = 'Contents';
        $menuAdminContents->functionality_id = $functionalityContents->id;
        $menuAdminContents->icon_id = $iconServer->id;
        $menuAdminContents->url = '/admin/contents';
        $menuAdminContents->created_id = $idUser;
        $menuAdminContents->save();

        $languageMenuAdminContents = new LanguageMenuAdmin();
        $languageMenuAdminContents->language_id = $idLanguage;
        $languageMenuAdminContents->menu_admin_id = $menuAdminContents->id;
        $languageMenuAdminContents->description = 'Personalizza';
        $languageMenuAdminContents->created_id = $idUser;
        $languageMenuAdminContents->save();

        $menuAdminUserRoleContents = new MenuAdminUserRole();
        $menuAdminUserRoleContents->menu_admin_id = $menuAdminContents->id;
        $menuAdminUserRoleContents->menu_admin_father_id = MenuAdmin::where('code', 'Theme')->first()->id;
        $menuAdminUserRoleContents->role_id = $idRole;
        $menuAdminUserRoleContents->enabled = true;
        $menuAdminUserRoleContents->order = 30;
        $menuAdminUserRoleContents->created_id = $idUser;
        $menuAdminUserRoleContents->save();

        #endregion MENU

        #region TAB

        $tabList = new Tab();
        $tabList->code = 'List';
        $tabList->functionality_id = $functionalityContents->id;
        $tabList->order = 10;
        $tabList->created_id = $idUser;
        $tabList->save();

        $languageTabList = new LanguageTab();
        $languageTabList->language_id = $idLanguage;
        $languageTabList->tab_id = $tabList->id;
        $languageTabList->description = 'Lista';
        $languageTabList->created_id = $idUser;
        $languageTabList->save();

        #endregion TAB

        #region TABLE

        #region FORMATTER

        $fieldFormatter = new Field();
        $fieldFormatter->code = 'Formatter';
        $fieldFormatter->tab_id = $tabList->id;
        $fieldFormatter->formatter = 'contentFormatter';
        $fieldFormatter->data_type = 1;
        $fieldFormatter->created_id = $idUser;
        $fieldFormatter->save();

        $fieldLanguageFormatter = new FieldLanguage();
        $fieldLanguageFormatter->field_id = $fieldFormatter->id;
        $fieldLanguageFormatter->language_id = $idLanguage;
        $fieldLanguageFormatter->description = '';
        $fieldLanguageFormatter->created_id = $idUser;
        $fieldLanguageFormatter->save();

        $fieldUserRoleFormatter = new FieldUserRole();
        $fieldUserRoleFormatter->field_id = $fieldFormatter->id;
        $fieldUserRoleFormatter->role_id = $idRole;
        $fieldUserRoleFormatter->enabled = true;
        $fieldUserRoleFormatter->input_type = 14;
        $fieldUserRoleFormatter->table_order = 10;
        $fieldUserRoleFormatter->created_id = $idUser;
        $fieldUserRoleFormatter->save();

        #endregion FORMATTER

        #region CODE

        $fieldCodeTable = new Field();
        $fieldCodeTable->code = 'CodeTable';
        $fieldCodeTable->tab_id = $tabList->id;
        $fieldCodeTable->field = 'code';
        $fieldCodeTable->data_type = 1;
        $fieldCodeTable->created_id = $idUser;
        $fieldCodeTable->save();

        $fieldLanguageCodeTable = new FieldLanguage();
        $fieldLanguageCodeTable->field_id = $fieldCodeTable->id;
        $fieldLanguageCodeTable->language_id = $idLanguage;
        $fieldLanguageCodeTable->description = 'Codice';
        $fieldLanguageCodeTable->created_id = $idUser;
        $fieldLanguageCodeTable->save();

        $fieldUserRoleCodeTable = new FieldUserRole();
        $fieldUserRoleCodeTable->field_id = $fieldCodeTable->id;
        $fieldUserRoleCodeTable->role_id = $idRole;
        $fieldUserRoleCodeTable->enabled = true;
        $fieldUserRoleCodeTable->input_type = 15;
        $fieldUserRoleCodeTable->table_order = 20;
        $fieldUserRoleCodeTable->created_id = $idUser;
        $fieldUserRoleCodeTable->save();

        #endregion CODE

        #endregion TABLE

        #region DETAIL

        #region CODE

        $fieldCodeDetail = new Field();
        $fieldCodeDetail->code = 'CodeDetail';
        $fieldCodeDetail->tab_id = $tabList->id;
        $fieldCodeDetail->field = 'code';
        $fieldCodeDetail->data_type = 1;
        $fieldCodeDetail->required = true;
        $fieldCodeDetail->created_id = $idUser;
        $fieldCodeDetail->save();

        $fieldLanguageCodeDetail = new FieldLanguage();
        $fieldLanguageCodeDetail->field_id = $fieldCodeDetail->id;
        $fieldLanguageCodeDetail->language_id = $idLanguage;
        $fieldLanguageCodeDetail->description = 'Codice';
        $fieldLanguageCodeDetail->created_id = $idUser;
        $fieldLanguageCodeDetail->save();

        $fieldUserRoleCodeDetail = new FieldUserRole();
        $fieldUserRoleCodeDetail->field_id = $fieldCodeDetail->id;
        $fieldUserRoleCodeDetail->role_id = $idRole;
        $fieldUserRoleCodeDetail->enabled = true;
        $fieldUserRoleCodeDetail->input_type = 12;
        $fieldUserRoleCodeDetail->pos_x = 10;
        $fieldUserRoleCodeDetail->pos_y = 10;
        $fieldUserRoleCodeDetail->created_id = $idUser;
        $fieldUserRoleCodeDetail->save();

        #endregion CODE

        #region LANGUAGE

        $fieldLanguage = new Field();
        $fieldLanguage->code = 'Language';
        $fieldLanguage->tab_id = $tabList->id;
        $fieldLanguage->field = 'idLanguage';
        $fieldLanguage->data_type = 0;
        $fieldLanguage->service = 'languages';
        $fieldLanguage->required = true;
        $fieldLanguage->created_id = $idUser;
        $fieldLanguage->save();

        $fieldLanguageLanguage = new FieldLanguage();
        $fieldLanguageLanguage->field_id = $fieldLanguage->id;
        $fieldLanguageLanguage->language_id = $idLanguage;
        $fieldLanguageLanguage->description = 'Lingua';
        $fieldLanguageLanguage->created_id = $idUser;
        $fieldLanguageLanguage->save();

        $fieldUserRoleLanguage = new FieldUserRole();
        $fieldUserRoleLanguage->field_id = $fieldLanguage->id;
        $fieldUserRoleLanguage->role_id = $idRole;
        $fieldUserRoleLanguage->enabled = true;
        $fieldUserRoleLanguage->input_type = 2;
        $fieldUserRoleLanguage->pos_x = 10;
        $fieldUserRoleLanguage->pos_y = 10;
        $fieldUserRoleLanguage->colspan = 4;
        $fieldUserRoleLanguage->created_id = $idUser;
        $fieldUserRoleLanguage->save();

        #endregion LANGUAGE

        #region CONTENT

        $fieldContent = new Field();
        $fieldContent->code = 'Content';
        $fieldContent->tab_id = $tabList->id;
        $fieldContent->field = 'content';
        $fieldContent->data_type = 1;
        $fieldContent->required = true;
        $fieldContent->created_id = $idUser;
        $fieldContent->save();

        $fieldLanguageContent = new FieldLanguage();
        $fieldLanguageContent->field_id = $fieldContent->id;
        $fieldLanguageContent->language_id = $idLanguage;
        $fieldLanguageContent->description = 'Contenuto';
        $fieldLanguageContent->created_id = $idUser;
        $fieldLanguageContent->save();

        $fieldUserRoleContent = new FieldUserRole();
        $fieldUserRoleContent->field_id = $fieldContent->id;
        $fieldUserRoleContent->role_id = $idRole;
        $fieldUserRoleContent->enabled = true;
        $fieldUserRoleContent->input_type = 17;
        $fieldUserRoleContent->pos_x = 10;
        $fieldUserRoleContent->pos_y = 20;
        $fieldUserRoleContent->colspan = 4;
        $fieldUserRoleContent->created_id = $idUser;
        $fieldUserRoleContent->save();

        #endregion CONTENT

        #endregion DETAIL

        #endregion CONTENTS
    }
}

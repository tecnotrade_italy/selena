<?php

use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class PickingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        #region OC LIST

        $functionalityPickingOCList = new Functionality();
        $functionalityPickingOCList->code = 'picking_oc_list';
        $functionalityPickingOCList->created_id = $idUser;
        $functionalityPickingOCList->save();

        $iconFileImageO = new Icon();
        $iconFileImageO->description = 'File image O';
        $iconFileImageO->css = 'fa fa-file-image-o';
        $iconFileImageO->created_id = $idUser;
        $iconFileImageO->save();

        $menuAdminPickingOCList = new MenuAdmin();
        $menuAdminPickingOCList->code = 'picking_oc_list';
        $menuAdminPickingOCList->functionality_id = $functionalityPickingOCList->id;
        $menuAdminPickingOCList->icon_id = $iconFileImageO->id;
        $menuAdminPickingOCList->url = '/admin/picking/OCList';
        $menuAdminPickingOCList->created_id = $idUser;
        $menuAdminPickingOCList->save();

        $languageMenuAdminPickingOCList = new LanguageMenuAdmin();
        $languageMenuAdminPickingOCList->language_id = $idLanguage;
        $languageMenuAdminPickingOCList->menu_admin_id = $menuAdminPickingOCList->id;
        $languageMenuAdminPickingOCList->description = 'Lista OC';
        $languageMenuAdminPickingOCList->created_id = $idUser;
        $languageMenuAdminPickingOCList->save();

        $menuAdminUserRolePickingOCList = new MenuAdminUserRole();
        $menuAdminUserRolePickingOCList->menu_admin_id = $menuAdminPickingOCList->id;
        $menuAdminUserRolePickingOCList->role_id = $idRole;
        $menuAdminUserRolePickingOCList->enabled = true;
        $menuAdminUserRolePickingOCList->order = 10;
        $menuAdminUserRolePickingOCList->created_id = $idUser;
        $menuAdminUserRolePickingOCList->save();

        $tabContentPickingOCList = new Tab();
        $tabContentPickingOCList->code = 'oc_list';
        $tabContentPickingOCList->functionality_id = $functionalityPickingOCList->id;
        $tabContentPickingOCList->order = 10;
        $tabContentPickingOCList->created_id = $idUser;
        $tabContentPickingOCList->save();

        $languageTabContentPickingOCList = new LanguageTab();
        $languageTabContentPickingOCList->language_id = $idLanguage;
        $languageTabContentPickingOCList->tab_id = $tabContentPickingOCList->id;
        $languageTabContentPickingOCList->description = 'Lista OC';
        $languageTabContentPickingOCList->created_id = $idUser;
        $languageTabContentPickingOCList->save();

        #region CUSTOMER DESCRIPTION

        $fieldCustomerDescription = new Field();
        $fieldCustomerDescription->code = 'customer_description';
        $fieldCustomerDescription->tab_id = $tabContentPickingOCList->id;
        $fieldCustomerDescription->field = 'customerDescription';
        $fieldCustomerDescription->data_type = 1;
        $fieldCustomerDescription->created_id = $idUser;
        $fieldCustomerDescription->save();

        $fieldLanguageCustomerDescription = new FieldLanguage();
        $fieldLanguageCustomerDescription->field_id = $fieldCustomerDescription->id;
        $fieldLanguageCustomerDescription->language_id = $idLanguage;
        $fieldLanguageCustomerDescription->description = 'Cliente';
        $fieldLanguageCustomerDescription->created_id = $idUser;
        $fieldLanguageCustomerDescription->save();

        $fieldUserRoleCustomerDescription = new FieldUserRole();
        $fieldUserRoleCustomerDescription->field_id = $fieldCustomerDescription->id;
        $fieldUserRoleCustomerDescription->role_id = $idRole;
        $fieldUserRoleCustomerDescription->enabled = true;
        $fieldUserRoleCustomerDescription->input_type = 15;
        $fieldUserRoleCustomerDescription->table_order = 10;
        $fieldUserRoleCustomerDescription->created_id = $idUser;
        $fieldUserRoleCustomerDescription->save();

        #endregion CUSTOMER DESCRIPTION

        #region ORDER NUMBER

        $fieldOrderNumber = new Field();
        $fieldOrderNumber->code = 'order_number';
        $fieldOrderNumber->tab_id = $tabContentPickingOCList->id;
        $fieldOrderNumber->field = 'orderNumber';
        $fieldOrderNumber->data_type = 1;
        $fieldOrderNumber->created_id = $idUser;
        $fieldOrderNumber->save();

        $fieldLanguageOrderNumber = new FieldLanguage();
        $fieldLanguageOrderNumber->field_id = $fieldOrderNumber->id;
        $fieldLanguageOrderNumber->language_id = $idLanguage;
        $fieldLanguageOrderNumber->description = 'Nr. OC';
        $fieldLanguageOrderNumber->created_id = $idUser;
        $fieldLanguageOrderNumber->save();

        $fieldUserRoleOrderNumber = new FieldUserRole();
        $fieldUserRoleOrderNumber->field_id = $fieldOrderNumber->id;
        $fieldUserRoleOrderNumber->role_id = $idRole;
        $fieldUserRoleOrderNumber->enabled = true;
        $fieldUserRoleOrderNumber->input_type = 15;
        $fieldUserRoleOrderNumber->table_order = 20;
        $fieldUserRoleOrderNumber->created_id = $idUser;
        $fieldUserRoleOrderNumber->save();

        #endregion ORDER NUMBER

        #region SHIPMENT MONTH

        $fieldShipmentMonth = new Field();
        $fieldShipmentMonth->code = 'shipment_month';
        $fieldShipmentMonth->tab_id = $tabContentPickingOCList->id;
        $fieldShipmentMonth->field = 'shipmentMonth';
        $fieldShipmentMonth->data_type = 1;
        $fieldShipmentMonth->created_id = $idUser;
        $fieldShipmentMonth->save();

        $fieldLanguageShipmentMonth = new FieldLanguage();
        $fieldLanguageShipmentMonth->field_id = $fieldShipmentMonth->id;
        $fieldLanguageShipmentMonth->language_id = $idLanguage;
        $fieldLanguageShipmentMonth->description = 'Mese';
        $fieldLanguageShipmentMonth->created_id = $idUser;
        $fieldLanguageShipmentMonth->save();

        $fieldUserRoleShipmentMonth = new FieldUserRole();
        $fieldUserRoleShipmentMonth->field_id = $fieldShipmentMonth->id;
        $fieldUserRoleShipmentMonth->role_id = $idRole;
        $fieldUserRoleShipmentMonth->enabled = true;
        $fieldUserRoleShipmentMonth->input_type = 15;
        $fieldUserRoleShipmentMonth->table_order = 30;
        $fieldUserRoleShipmentMonth->created_id = $idUser;
        $fieldUserRoleShipmentMonth->save();

        #endregion SHIPMENT MOUNTH

        $tabContentPickingOCList = new Tab();
        $tabContentPickingOCList->code = 'product_list';
        $tabContentPickingOCList->functionality_id = $functionalityPickingOCList->id;
        $tabContentPickingOCList->order = 10;
        $tabContentPickingOCList->created_id = $idUser;
        $tabContentPickingOCList->save();

        $languageTabContentPickingOCList = new LanguageTab();
        $languageTabContentPickingOCList->language_id = $idLanguage;
        $languageTabContentPickingOCList->tab_id = $tabContentPickingOCList->id;
        $languageTabContentPickingOCList->description = 'Lista prodotti';
        $languageTabContentPickingOCList->created_id = $idUser;
        $languageTabContentPickingOCList->save();

        #region ITEM DESCRIPTION

        $fieldItemDescription = new Field();
        $fieldItemDescription->code = 'item_description';
        $fieldItemDescription->tab_id = $tabContentPickingOCList->id;
        $fieldItemDescription->field = 'itemDescription';
        $fieldItemDescription->data_type = 1;
        $fieldItemDescription->created_id = $idUser;
        $fieldItemDescription->save();

        $fieldLanguageItemDescription = new FieldLanguage();
        $fieldLanguageItemDescription->field_id = $fieldItemDescription->id;
        $fieldLanguageItemDescription->language_id = $idLanguage;
        $fieldLanguageItemDescription->description = 'Articolo';
        $fieldLanguageItemDescription->created_id = $idUser;
        $fieldLanguageItemDescription->save();

        $fieldUserRoleItemDescription = new FieldUserRole();
        $fieldUserRoleItemDescription->field_id = $fieldItemDescription->id;
        $fieldUserRoleItemDescription->role_id = $idRole;
        $fieldUserRoleItemDescription->enabled = true;
        $fieldUserRoleItemDescription->input_type = 15;
        $fieldUserRoleItemDescription->table_order = 10;
        $fieldUserRoleItemDescription->created_id = $idUser;
        $fieldUserRoleItemDescription->save();

        #endregion ITEM DESCRIPTION

        #region QUANTITY

        $fieldQuantity = new Field();
        $fieldQuantity->code = 'quantity';
        $fieldQuantity->tab_id = $tabContentPickingOCList->id;
        $fieldQuantity->field = 'quantity';
        $fieldQuantity->data_type = 1;
        $fieldQuantity->created_id = $idUser;
        $fieldQuantity->save();

        $fieldLanguageQuantity = new FieldLanguage();
        $fieldLanguageQuantity->field_id = $fieldQuantity->id;
        $fieldLanguageQuantity->language_id = $idLanguage;
        $fieldLanguageQuantity->description = 'Quantity';
        $fieldLanguageQuantity->created_id = $idUser;
        $fieldLanguageQuantity->save();

        $fieldUserRoleQuantity = new FieldUserRole();
        $fieldUserRoleQuantity->field_id = $fieldQuantity->id;
        $fieldUserRoleQuantity->role_id = $idRole;
        $fieldUserRoleQuantity->enabled = true;
        $fieldUserRoleQuantity->input_type = 15;
        $fieldUserRoleQuantity->table_order = 20;
        $fieldUserRoleQuantity->created_id = $idUser;
        $fieldUserRoleQuantity->save();

        #endregion QUANTITY

        #region PICKING STATE

        $fieldPickingState = new Field();
        $fieldPickingState->code = 'picking_state';
        $fieldPickingState->tab_id = $tabContentPickingOCList->id;
        $fieldPickingState->field = 'pickingState';
        $fieldPickingState->formatter = 'pickingStateFormatter';
        $fieldPickingState->data_type = 1;
        $fieldPickingState->created_id = $idUser;
        $fieldPickingState->save();

        $fieldLanguagePickingState = new FieldLanguage();
        $fieldLanguagePickingState->field_id = $fieldPickingState->id;
        $fieldLanguagePickingState->language_id = $idLanguage;
        $fieldLanguagePickingState->description = 'Stato';
        $fieldLanguagePickingState->created_id = $idUser;
        $fieldLanguagePickingState->save();

        $fieldUserRolePickingState = new FieldUserRole();
        $fieldUserRolePickingState->field_id = $fieldPickingState->id;
        $fieldUserRolePickingState->role_id = $idRole;
        $fieldUserRolePickingState->enabled = true;
        $fieldUserRolePickingState->input_type = 14;
        $fieldUserRolePickingState->table_order = 30;
        $fieldUserRolePickingState->created_id = $idUser;
        $fieldUserRolePickingState->save();

        #endregion PICKING STATE

        #endregion OC LIST

        #region PICKING READ BARCODE

        $functionalityPickingReadBarcode = new Functionality();
        $functionalityPickingReadBarcode->code = 'picking_read_barcode';
        $functionalityPickingReadBarcode->created_id = $idUser;
        $functionalityPickingReadBarcode->save();

        $iconFileBarcode = new Icon();
        $iconFileBarcode->description = 'Barcode';
        $iconFileBarcode->css = 'fa fa-barcode';
        $iconFileBarcode->created_id = $idUser;
        $iconFileBarcode->save();

        $menuAdminPickingReadBarcode = new MenuAdmin();
        $menuAdminPickingReadBarcode->code = 'picking_read_barcode';
        $menuAdminPickingReadBarcode->functionality_id = $functionalityPickingReadBarcode->id;
        $menuAdminPickingReadBarcode->icon_id = $iconFileBarcode->id;
        $menuAdminPickingReadBarcode->url = '/admin/picking/reading';
        $menuAdminPickingReadBarcode->created_id = $idUser;
        $menuAdminPickingReadBarcode->save();

        $languageMenuAdminPickingReadBarcode = new LanguageMenuAdmin();
        $languageMenuAdminPickingReadBarcode->language_id = $idLanguage;
        $languageMenuAdminPickingReadBarcode->menu_admin_id = $menuAdminPickingReadBarcode->id;
        $languageMenuAdminPickingReadBarcode->description = 'Avvia lettura picking';
        $languageMenuAdminPickingReadBarcode->created_id = $idUser;
        $languageMenuAdminPickingReadBarcode->save();

        $menuAdminUserRolePickingReadBarcode = new MenuAdminUserRole();
        $menuAdminUserRolePickingReadBarcode->menu_admin_id = $menuAdminPickingReadBarcode->id;
        $menuAdminUserRolePickingReadBarcode->role_id = $idRole;
        $menuAdminUserRolePickingReadBarcode->enabled = true;
        $menuAdminUserRolePickingReadBarcode->order = 20;
        $menuAdminUserRolePickingReadBarcode->created_id = $idUser;
        $menuAdminUserRolePickingReadBarcode->save();

        #endregion PICKING READ BARCORDE

        #region SHIPMENT SENT SIXTEN

        $functionalityShipmentSentSixten = new Functionality();
        $functionalityShipmentSentSixten->code = 'shipment_sent_sixten';
        $functionalityShipmentSentSixten->created_id = $idUser;
        $functionalityShipmentSentSixten->save();

        $iconFilePaperPlaneO = new Icon();
        $iconFilePaperPlaneO->description = 'Paper plane O';
        $iconFilePaperPlaneO->css = 'fa fa-paper-plane-o';
        $iconFilePaperPlaneO->created_id = $idUser;
        $iconFilePaperPlaneO->save();

        $menuAdminShipmentSentSixten = new MenuAdmin();
        $menuAdminShipmentSentSixten->code = 'shipment_sent_sixten';
        $menuAdminShipmentSentSixten->functionality_id = $functionalityShipmentSentSixten->id;
        $menuAdminShipmentSentSixten->icon_id = $iconFilePaperPlaneO->id;
        $menuAdminShipmentSentSixten->url = '/admin/shipment/sentSixten';
        $menuAdminShipmentSentSixten->created_id = $idUser;
        $menuAdminShipmentSentSixten->save();

        $languageMenuAdminShipmentSentSixten = new LanguageMenuAdmin();
        $languageMenuAdminShipmentSentSixten->language_id = $idLanguage;
        $languageMenuAdminShipmentSentSixten->menu_admin_id = $menuAdminShipmentSentSixten->id;
        $languageMenuAdminShipmentSentSixten->description = 'Spedisci';
        $languageMenuAdminShipmentSentSixten->created_id = $idUser;
        $languageMenuAdminShipmentSentSixten->save();

        $menuAdminUserRoleShipmentSentSixten = new MenuAdminUserRole();
        $menuAdminUserRoleShipmentSentSixten->menu_admin_id = $menuAdminShipmentSentSixten->id;
        $menuAdminUserRoleShipmentSentSixten->role_id = $idRole;
        $menuAdminUserRoleShipmentSentSixten->enabled = true;
        $menuAdminUserRoleShipmentSentSixten->order = 30;
        $menuAdminUserRoleShipmentSentSixten->created_id = $idUser;
        $menuAdminUserRoleShipmentSentSixten->save();

        #endregion SHIPMENT SENT SIXTEN

    }
}

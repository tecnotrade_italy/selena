<?php

use App\Enums\SettingEnum;
use Illuminate\Database\Seeder;
use App\Setting;
use App\User;
use Carbon\Carbon;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idUser = User::first()->id;

        $setting = new Setting();
        $setting->code = SettingEnum::CopyContentDefaultLanguage;
        $setting->value = "ASK";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::CopySettingDefaultLanguage;
        $setting->value = "ASK";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::LastDateSync;
        $setting->value = Carbon::createFromDate(1900, 1, 1);
        $setting->description = "Ultima data di sincronizzazione";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::MenuRender;
        $setting->value = "Normal";
        $setting->description = "Tipo di visualizzazione menu";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::HomeNewsList;
        $setting->value = "3";
        $setting->description = "Numero di articoli news in home";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::HomeBlogList;
        $setting->value = "3";
        $setting->description = "Numero di articoli blog in home";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::BlogList;
        $setting->value = "10";
        $setting->description = "Numero di articoli blog per pagina";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::NewsList;
        $setting->value = "10";
        $setting->description = "Numero di articoli news per pagina";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::BlogRender;
        $setting->value = "Box";
        $setting->description = "Tipo di visualizzazione articoli blog";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::NewsRender;
        $setting->value = "Box";
        $setting->description = "Tipo di visualizzazione articoli news";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::ContactsEmail;
        $setting->value = "supporto@tecnotrade.com";
        $setting->description = "Email di destinazione richieste contatti";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::BackgroundColorChartVisitorsViews;
        $setting->value = "#3e95cd";
        $setting->description = "Colore di background grafico Visitatori";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::BorderColorChartVisitorsViews;
        $setting->value = "rgba(62,149,205,0.4)";
        $setting->description = "Colore di bordo grafico Visitatori";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::BackgroundColorChartPagesViews;
        $setting->value = "rgb(255, 99, 132)";
        $setting->description = "Colore di background grafico Pagine viste";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::BorderColorChartPagesViews;
        $setting->value = "rgb(255, 99, 132)";
        $setting->description = "Colore di bordo grafico Pagine viste";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::BackgroundColorChartDeviceSession;
        $setting->value = "#0080ff, #de2727, #2eb85c";
        $setting->value = "rgb(255, 99, 132)";
        $setting->description = "Colore di background grafico Sessioni per dispositivo";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::BackgroundColorChartSourceSession;
        $setting->value = "#2819ae, #b2b8c1, #248f48, #d69405, #de2727, #0080ff, #cfd4d8, #4d5666";
        $setting->description = "Colore di background grafico sorgente";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::BackgroundColorChartReturningVisitors;
        $setting->value = "#2819ae, #b2b8c1, #248f48, #d69405, #de2727, #0080ff, #cfd4d8, #4d5666";
        $setting->description = "Colore di background grafico tipologia utenti";
        $setting->created_id = $idUser;
        $setting->save();

        $setting = new Setting();
        $setting->code = SettingEnum::FacebookTrackId;
        $setting->description = "Id Facebook";
        $setting->value = "";
        $setting->created_id = $idUser;
        $setting->save();
        
        $setting = new Setting();
        $setting->code = SettingEnum::NumberOfItemsPerPage;
        $setting->description = "Numero di articoli per pagina";
        $setting->value = "20";
        $setting->created_id = $idUser;
        $setting->save();


    }
}

<?php
use App\Field;
use App\FieldLanguage;
use App\FieldUserRole;
use App\Functionality;
use App\Icon;
use App\Language;
use App\LanguageMenuAdmin;
use App\LanguageTab;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\Role;
use App\Tab;
use App\User;
use Illuminate\Database\Seeder;

class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idRole = Role::where('name', '=', 'admin')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;
        $functionalityProvince = Functionality::where('code', 'Provinces')->first();

        //------------ Tab Tabella ---------------------------------
        #region TAB
        $tabProvinceTable = new Tab();
        $tabProvinceTable->code = 'TableProvinces';
        $tabProvinceTable->functionality_id = $functionalityProvince->id;
        $tabProvinceTable->order = 10;
        $tabProvinceTable->created_id = $idUser;
        $tabProvinceTable->save();

        $languageTabProvinceTable = new LanguageTab();
        $languageTabProvinceTable->language_id = $idLanguage;
        $languageTabProvinceTable->tab_id = $tabProvinceTable->id;
        $languageTabProvinceTable->description = 'Tabella Province';
        $languageTabProvinceTable->created_id = $idUser;
        $languageTabProvinceTable->save();
        #END region TAB

        #region CHECKBOX
        $fieldProvinceCheckBox = new Field();
        $fieldProvinceCheckBox->tab_id = $tabProvinceTable->id;
        $fieldProvinceCheckBox->code = 'TableCheckBox';
        $fieldProvinceCheckBox->created_id = $idUser;
        $fieldProvinceCheckBox->save();

        $fieldLanguageProvinceCheckBox = new FieldLanguage();
        $fieldLanguageProvinceCheckBox->field_id = $fieldProvinceCheckBox->id;
        $fieldLanguageProvinceCheckBox->language_id = $idLanguage;
        $fieldLanguageProvinceCheckBox->description = 'CheckBox';
        $fieldLanguageProvinceCheckBox->created_id = $idUser;
        $fieldLanguageProvinceCheckBox->save();

        $fieldUserRoleProvinceCheckBox = new FieldUserRole();
        $fieldUserRoleProvinceCheckBox->field_id = $fieldProvinceCheckBox->id;
        $fieldUserRoleProvinceCheckBox->role_id = $idRole;
        $fieldUserRoleProvinceCheckBox->enabled = true;
        $fieldUserRoleProvinceCheckBox->table_order = 10;
        $fieldUserRoleProvinceCheckBox->input_type = 13;
        $fieldUserRoleProvinceCheckBox->created_id = $idUser;
        $fieldUserRoleProvinceCheckBox->save();
        #endregion CHECKBOX
  
        #region FORMATTER
        $fieldProvinceFormatter = new Field();
        $fieldProvinceFormatter->code = 'ProvinceFormatter';
        $fieldProvinceFormatter->formatter = 'ProvinceFormatter';
        $fieldProvinceFormatter->tab_id = $tabProvinceTable->id;
        $fieldProvinceFormatter->created_id = $idUser;
        $fieldProvinceFormatter->save();

        $fieldLanguageProvinceFormatter = new FieldLanguage();
        $fieldLanguageProvinceFormatter->field_id = $fieldProvinceFormatter->id;
        $fieldLanguageProvinceFormatter->language_id = $idLanguage;
        $fieldLanguageProvinceFormatter->description = '';
        $fieldLanguageProvinceFormatter->created_id = $idUser;
        $fieldLanguageProvinceFormatter->save();

        $fieldUserRoleProvinceFormatter = new FieldUserRole();
        $fieldUserRoleProvinceFormatter->field_id = $fieldProvinceFormatter->id;
        $fieldUserRoleProvinceFormatter->role_id = $idRole;
        $fieldUserRoleProvinceFormatter->enabled = true;
        $fieldUserRoleProvinceFormatter->table_order = 20;
        $fieldUserRoleProvinceFormatter->input_type = 14;
        $fieldUserRoleProvinceFormatter->created_id = $idUser;
        $fieldUserRoleProvinceFormatter->save();
        #endregion FORMATTER

        #region nation
        $fieldProvinceNation = new Field();
        $fieldProvinceNation->code = 'nation_id';
        $fieldProvinceNation->tab_id = $tabProvinceTable->id;
        $fieldProvinceNation->field = 'nation_id';
        $fieldProvinceNation->data_type = 1;
        $fieldProvinceNation->created_id = $idUser;
        $fieldProvinceNation->save();

        $fieldLanguageProvinceeNation = new FieldLanguage();
        $fieldLanguageProvinceeNation->field_id = $fieldProvinceNation->id;
        $fieldLanguageProvinceeNation->language_id = $idLanguage;
        $fieldLanguageProvinceeNation->description = 'Nazione';
        $fieldLanguageProvinceeNation->created_id = $idUser;
        $fieldLanguageProvinceeNation->save();

        $fieldUserRoleProvinceNation = new FieldUserRole();
        $fieldUserRoleProvinceNation->field_id = $fieldProvinceNation->id;
        $fieldUserRoleProvinceNation->role_id = $idRole;
        $fieldUserRoleProvinceNation->enabled = true;
        $fieldUserRoleProvinceNation->table_order = 30;
        $fieldUserRoleProvinceNation->input_type = 0;
        $fieldUserRoleProvinceNation->created_id = $idUser;
        $fieldUserRoleProvinceNation->save();
        #endregion nation

        #region abbreviation
        $fieldProvinceAbbreviation = new Field();
        $fieldProvinceAbbreviation->code = 'abbreviation';
        $fieldProvinceAbbreviation->tab_id = $tabProvinceTable->id;
        $fieldProvinceAbbreviation->field = 'abbreviation';
        $fieldProvinceAbbreviation->data_type = 1;
        $fieldProvinceAbbreviation->created_id = $idUser;
        $fieldProvinceAbbreviation->save();

        $fieldLanguageProvinceAbbreviation = new FieldLanguage();
        $fieldLanguageProvinceAbbreviation->field_id = $fieldProvinceAbbreviation->id;
        $fieldLanguageProvinceAbbreviation->language_id = $idLanguage;
        $fieldLanguageProvinceAbbreviation->description = 'Abbreviazione';
        $fieldLanguageProvinceAbbreviation->created_id = $idUser;
        $fieldLanguageProvinceAbbreviation->save();

        $fieldUserRoleProvinceAbbreviation = new FieldUserRole();
        $fieldUserRoleProvinceAbbreviation->field_id = $fieldProvinceAbbreviation->id;
        $fieldUserRoleProvinceAbbreviation->role_id = $idRole;
        $fieldUserRoleProvinceAbbreviation->enabled = true;
        $fieldUserRoleProvinceAbbreviation->table_order = 40;
        $fieldUserRoleProvinceAbbreviation->input_type = 0;
        $fieldUserRoleProvinceAbbreviation->created_id = $idUser;
        $fieldUserRoleProvinceAbbreviation->save();
        #endregion abbreviation

        #region description
        $fieldProvinceDescription = new Field();
        $fieldProvinceDescription->code = 'description';
        $fieldProvinceDescription->tab_id = $tabProvinceTable->id;
        $fieldProvinceDescription->field = 'description';
        $fieldProvinceDescription->data_type = 1;
        $fieldProvinceDescription->created_id = $idUser;
        $fieldProvinceDescription->save();

        $fieldLanguageProvinceDescription = new FieldLanguage();
        $fieldLanguageProvinceDescription->field_id = $fieldProvinceDescription->id;
        $fieldLanguageProvinceDescription->language_id = $idLanguage;
        $fieldLanguageProvinceDescription->description = 'Descrizione';
        $fieldLanguageProvinceDescription->created_id = $idUser;
        $fieldLanguageProvinceDescription->save();

        $fieldUserRoleProvinceDescription = new FieldUserRole();
        $fieldUserRoleProvinceDescription->field_id = $fieldProvinceDescription->id;
        $fieldUserRoleProvinceDescription->role_id = $idRole;
        $fieldUserRoleProvinceDescription->enabled = true;
        $fieldUserRoleProvinceDescription->table_order = 50;
        $fieldUserRoleProvinceDescription->input_type = 0;
        $fieldUserRoleProvinceDescription->created_id = $idUser;
        $fieldUserRoleProvinceDescription->save();
        #endregion description

        //------------ Tab Dettaglio ---------------------------------
        #region TAB
        $detailProvinceTable = new Tab();
        $detailProvinceTable->code = 'DetailProvinces';
        $detailProvinceTable->functionality_id = $functionalityProvince->id;
        $detailProvinceTable->order = 10;
        $detailProvinceTable->created_id = $idUser;
        $detailProvinceTable->save();

        $languageTabProvinceTable = new LanguageTab();
        $languageTabProvinceTable->language_id = $idLanguage;
        $languageTabProvinceTable->tab_id = $detailProvinceTable->id;
        $languageTabProvinceTable->description = 'Dettaglio Province';
        $languageTabProvinceTable->created_id = $idUser;
        $languageTabProvinceTable->save();
        #END region TAB

        #region nation
        $fieldProvinceNation = new Field();
        $fieldProvinceNation->code = 'nation_id';
        $fieldProvinceNation->tab_id = $detailProvinceTable->id;
        $fieldProvinceNation->field = 'nation_id';
        $fieldProvinceNation->service = 'nations';
        $fieldProvinceNation->data_type = 1;
        $fieldProvinceNation->created_id = $idUser;
        $fieldProvinceNation->save();

        $fieldLanguageProvinceeNation = new FieldLanguage();
        $fieldLanguageProvinceeNation->field_id = $fieldProvinceNation->id;
        $fieldLanguageProvinceeNation->language_id = $idLanguage;
        $fieldLanguageProvinceeNation->description = 'Seleziona Nazione';
        $fieldLanguageProvinceeNation->created_id = $idUser;
        $fieldLanguageProvinceeNation->save();

        $fieldUserRoleProvinceNation = new FieldUserRole();
        $fieldUserRoleProvinceNation->field_id = $fieldProvinceNation->id;
        $fieldUserRoleProvinceNation->role_id = $idRole;
        $fieldUserRoleProvinceNation->enabled = true;
        $fieldUserRoleProvinceNation->pos_x = 10;
        $fieldUserRoleProvinceNation->pos_y = 10;
        $fieldUserRoleProvinceNation->input_type = 2;
        $fieldUserRoleProvinceNation->created_id = $idUser;
        $fieldUserRoleProvinceNation->save();
        #endregion nation

        #region abbreviation
        $fieldProvinceAbbreviation = new Field();
        $fieldProvinceAbbreviation->code = 'abbreviation';
        $fieldProvinceAbbreviation->tab_id = $detailProvinceTable->id;
        $fieldProvinceAbbreviation->field = 'abbreviation';
        $fieldProvinceAbbreviation->data_type = 1;
        $fieldProvinceAbbreviation->created_id = $idUser;
        $fieldProvinceAbbreviation->save();

        $fieldLanguageProvinceAbbreviation = new FieldLanguage();
        $fieldLanguageProvinceAbbreviation->field_id = $fieldProvinceAbbreviation->id;
        $fieldLanguageProvinceAbbreviation->language_id = $idLanguage;
        $fieldLanguageProvinceAbbreviation->description = 'Abbreviazione';
        $fieldLanguageProvinceAbbreviation->created_id = $idUser;
        $fieldLanguageProvinceAbbreviation->save();

        $fieldUserRoleProvinceAbbreviation = new FieldUserRole();
        $fieldUserRoleProvinceAbbreviation->field_id = $fieldProvinceAbbreviation->id;
        $fieldUserRoleProvinceAbbreviation->role_id = $idRole;
        $fieldUserRoleProvinceAbbreviation->enabled = true;
        $fieldUserRoleProvinceAbbreviation->pos_x = 20;
        $fieldUserRoleProvinceAbbreviation->pos_y = 10;
        $fieldUserRoleProvinceAbbreviation->input_type = 0;
        $fieldUserRoleProvinceAbbreviation->created_id = $idUser;
        $fieldUserRoleProvinceAbbreviation->save();
        #endregion abbreviation

        #region description
        $fieldProvinceDescription = new Field();
        $fieldProvinceDescription->code = 'description';
        $fieldProvinceDescription->tab_id = $detailProvinceTable->id;
        $fieldProvinceDescription->field = 'description';
        $fieldProvinceDescription->data_type = 1;
        $fieldProvinceDescription->created_id = $idUser;
        $fieldProvinceDescription->save();

        $fieldLanguageProvinceDescription = new FieldLanguage();
        $fieldLanguageProvinceDescription->field_id = $fieldProvinceDescription->id;
        $fieldLanguageProvinceDescription->language_id = $idLanguage;
        $fieldLanguageProvinceDescription->description = 'Descrizione';
        $fieldLanguageProvinceDescription->created_id = $idUser;
        $fieldLanguageProvinceDescription->save();

        $fieldUserRoleProvinceDescription = new FieldUserRole();
        $fieldUserRoleProvinceDescription->field_id = $fieldProvinceDescription->id;
        $fieldUserRoleProvinceDescription->role_id = $idRole;
        $fieldUserRoleProvinceDescription->enabled = true;
        $fieldUserRoleProvinceDescription->pos_x = 30;
        $fieldUserRoleProvinceDescription->pos_y = 10;
        $fieldUserRoleProvinceDescription->input_type = 0;
        $fieldUserRoleProvinceDescription->created_id = $idUser;
        $fieldUserRoleProvinceDescription->save();
        #endregion description 

    }
}

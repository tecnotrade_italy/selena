<?php

use App\DocumentType;
use App\DocumentTypeLanguage;
use App\Enums\DocumentTypeEnum;
use App\Helpers\LogHelper;
use App\Language;
use App\User;
use BenSampo\Enum\Enum;
use Illuminate\Database\Seeder;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idLanguage = Language::where('description', '=', 'Italiano')->first()->id;
        $idUser = User::where('name', '=', 'admin')->first()->id;

        $documentTypeOffer = new DocumentType();
        $documentTypeOffer->code = DocumentTypeEnum::Offer;
        $documentTypeOffer->created_id = $idUser;
        $documentTypeOffer->save();

        $documentTypeLanguageOffer = new DocumentTypeLanguage();
        $documentTypeLanguageOffer->document_type_id = $documentTypeOffer->id;
        $documentTypeLanguageOffer->language_id = $idLanguage;
        $documentTypeLanguageOffer->description = 'Offerte';
        $documentTypeLanguageOffer->created_id = $idUser;
        $documentTypeLanguageOffer->save();

        $documentTypeOrder = new DocumentType();
        $documentTypeOrder->code = DocumentTypeEnum::Order;
        $documentTypeOrder->created_id = $idUser;
        $documentTypeOrder->save();

        $documentTypeLanguageOrder = new DocumentTypeLanguage();
        $documentTypeLanguageOrder->document_type_id = $documentTypeOrder->id;
        $documentTypeLanguageOrder->language_id = $idLanguage;
        $documentTypeLanguageOrder->description = 'Ordine';
        $documentTypeLanguageOrder->created_id = $idUser;
        $documentTypeLanguageOrder->save();

        $documentTypeBeforeDDT = new DocumentType();
        $documentTypeBeforeDDT->code = DocumentTypeEnum::BeforeDDT;
        $documentTypeBeforeDDT->created_id = $idUser;
        $documentTypeBeforeDDT->save();

        $documentTypeLanguageBeforeDDT = new DocumentTypeLanguage();
        $documentTypeLanguageBeforeDDT->document_type_id = $documentTypeBeforeDDT->id;
        $documentTypeLanguageBeforeDDT->language_id = $idLanguage;
        $documentTypeLanguageBeforeDDT->description = 'Pre Bolla';
        $documentTypeLanguageBeforeDDT->created_id = $idUser;
        $documentTypeLanguageBeforeDDT->save();

        $documentTypeDDT = new DocumentType();
        $documentTypeDDT->code = DocumentTypeEnum::DDT;
        $documentTypeDDT->created_id = $idUser;
        $documentTypeDDT->save();

        $documentTypeLanguageDDT = new DocumentTypeLanguage();
        $documentTypeLanguageDDT->document_type_id = $documentTypeDDT->id;
        $documentTypeLanguageDDT->language_id = $idLanguage;
        $documentTypeLanguageDDT->description = 'Bolla';
        $documentTypeLanguageDDT->created_id = $idUser;
        $documentTypeLanguageDDT->save();
    }
}

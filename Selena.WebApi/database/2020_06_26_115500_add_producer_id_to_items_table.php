<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProducerIdToItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->unsignedBigInteger('producer_id')->nullable();
            $table->foreign('producer_id')->references('id')->on('producer');
        });

      /*  Schema::connection(env('DB_CONNECTION_TEST', 'data_test'))->table('items', function (Blueprint $table) {
            $table->unsignedBigInteger('producer_id')->nullable();
            $table->foreign('producer_id')->references('id')->on('producer');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('producer_id');
        });

      /*  Schema::connection(env('DB_CONNECTION_TEST', 'data_test'))->table('items', function (Blueprint $table) {
            $table->dropColumn('producer_id');
        });*/
    }
}
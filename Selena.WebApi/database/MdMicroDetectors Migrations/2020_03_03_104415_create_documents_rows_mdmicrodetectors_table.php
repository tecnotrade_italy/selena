<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsRowsMdmicrodetectorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_rows_mdmicrodetectors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('document_head_id', 255);
            $table->string('codice_articolo', 255);
            $table->string('codice_articolo_cliente', 255)->nullable();
            $table->string('barcode_spedizione', 255)->nullable();
            $table->integer('quantity')->nullable();
            $table->boolean('check_spedizione')->nullable();
            $table->string('cell', 255)->nullable();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents_rows_mdmicrodetectors');
      
    }
}

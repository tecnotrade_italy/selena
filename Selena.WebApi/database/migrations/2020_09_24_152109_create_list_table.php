<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code_list', 255)->nullable();
            $table->string('description', 255)->nullable();
            $table->string('beginning_validity', 255)->nullable();
            $table->string('end_validity', 255)->nullable();
            $table->string('value_list', 255)->nullable();
            $table->timestamps();
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
        });

    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list');
       
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderGroupDisplayTechnicalSpecificationTechnicalSpecifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groups_display_technical_specification_tech_spec', function (Blueprint $table) {
            $table->integer('order');
        });

    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups_display_technical_specification_tech_spec', function (Blueprint $table) {
            $table->dropColumn('order');
        });

       
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages_pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('language_id');
            $table->unsignedBigInteger('page_id');
            $table->string('title', 255);
            $table->text('content')->nullable();
            $table->string('url', 255)->nullable();
            $table->boolean('hide_header')->nullable();
            $table->boolean('hide_footer')->nullable();
            $table->boolean('hide_breadcrumb')->nullable();
            $table->string('seo_title', 60)->nullable();
            $table->string('seo_description', 230)->nullable();
            $table->text('seo_keyword')->nullable();
            $table->string('share_title', 60)->nullable();
            $table->string('share_description', 230)->nullable();
            $table->string('url_cover_image', 255)->nullable();
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('page_id')->references('id')->on('pages');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages_pages');
    
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveKeyCustomerGroupNewsletterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers_groups_newsletters', function($table)
        {
            $table->dropForeign('customers_groups_newsletters_customer_id_foreign');
        });
    }

    public function down()
    {
       //
    }
}

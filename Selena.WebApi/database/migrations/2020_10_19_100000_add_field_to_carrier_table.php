<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToCarrierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carriers', function (Blueprint $table) {
            $table->string('name', 255)->nullable();
            $table->string('description', 255)->nullable();
            $table->decimal('insurance', 255)->nullable();
            $table->string('img', 255)->nullable();
            $table->string('tracking_url', 255)->nullable();
            $table->unsignedBigInteger('order')->nullable();
            $table->unsignedBigInteger('vat_type_id')->nullable();
            
            $table->foreign('vat_type_id')->references('id')->on('vat_types');
           
        });
     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carriers', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('description');
            $table->dropColumn('insurance');
            $table->dropColumn('img');
            $table->dropColumn('tracking_url');
            $table->dropColumn('order');
            $table->dropColumn('vat_type_id');
        });

     
    }
}

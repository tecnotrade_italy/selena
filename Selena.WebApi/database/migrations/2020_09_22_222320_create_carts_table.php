<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->text('session_token');
            $table->unsignedBigInteger('currency_id')->nullable();
            $table->decimal('net_amount', 10, 4)->nullable();
            $table->decimal('vat_amount', 10, 4)->nullable();
            $table->decimal('shipment_amount', 10, 4)->nullable();
            $table->decimal('payment_cost', 10, 4)->nullable();  
            $table->decimal('total_amount', 10, 4)->nullable();   
            $table->decimal('service_amount', 10, 4)->nullable();          
            $table->decimal('discount_percentage', 10, 2)->nullable();
            $table->decimal('discount_value', 10, 4)->nullable();
            $table->unsignedBigInteger('voucher_id')->nullable();
            $table->text('note')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('currency_id')->references('id')->on('currencies');
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}

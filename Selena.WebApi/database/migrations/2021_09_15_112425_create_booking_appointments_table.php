<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_appointments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('connections_id')->nullable();
            $table->foreign('connections_id')->references('id')->on('booking_connections');
            $table->bigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->bigInteger('states_id')->nullable();
            $table->foreign('states_id')->references('id')->on('booking_states');
            $table->bigInteger('price')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->date('fixed_date')->nullable();
            $table->date('date_from')->nullable();
            $table->date('date_to')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_appointments');
    }
}

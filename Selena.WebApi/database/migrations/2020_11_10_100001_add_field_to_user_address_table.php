<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToUserAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_address', function (Blueprint $table) {
            $table->string('business_name', 255)->nullable();
            $table->string('name', 255)->nullable();
            $table->string('surname', 255)->nullable();
            $table->string('fiscal_code', 255)->nullable();
            $table->string('vat_number', 255)->nullable();
            $table->string('phone_number', 255)->nullable();
            $table->unsignedBigInteger('province_id')->nullable();
            $table->unsignedBigInteger('region_id')->nullable();
            $table->unsignedBigInteger('nation_id')->nullable();
            $table->foreign('province_id')->references('id')->on('province');
            $table->foreign('region_id')->references('id')->on('region');
            $table->foreign('nation_id')->references('id')->on('nations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_address', function (Blueprint $table) {
            $table->dropColumn('business_name');
            $table->dropColumn('name');
            $table->dropColumn('surname');
            $table->dropColumn('fiscal_code');
            $table->dropColumn('vat_number');
            $table->dropColumn('phone_number');
            $table->dropColumn('province_id');
            $table->dropColumn('region_id');
            $table->dropColumn('nation_id');
        });
    }
}

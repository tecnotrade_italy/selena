<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordsToBookingSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_settings', function (Blueprint $table) {
            $table->boolean('check_user')->default(false);
            $table->boolean('check_service')->default(false);
            $table->boolean('check_employee')->default(false);
            $table->boolean('check_place')->default(false);
            $table->boolean('check_price')->default(false);
            $table->boolean('check_states')->default(false);

            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_settings', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarriageLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carriages_languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('carriage_id');
            $table->unsignedBigInteger('language_id');
            $table->string('description', 255);

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('carriage_id')->references('id')->on('carriages');
            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carriages_languages');
       
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('internal_code', 50)->nullable();
            $table->unsignedBigInteger('document_row_type_id')->nullable();
            $table->unsignedBigInteger('unit_of_measure_id')->nullable();
            $table->decimal('price', 10, 4)->nullable();
            $table->unsignedBigInteger('vat_type_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->decimal('height', 10, 4)->nullable();
            $table->decimal('width', 10, 4)->nullable();
            $table->decimal('depth', 10, 4)->nullable();
            $table->decimal('volume', 10, 4)->nullable();
            $table->decimal('weight', 10, 4)->nullable();
            $table->dateTime('enabled_from')->nullable();
            $table->dateTime('enabled_to')->nullable();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('document_row_type_id')->references('id')->on('documents_rows_types');
            $table->foreign('unit_of_measure_id')->references('id')->on('units_of_measures');
            $table->foreign('vat_type_id')->references('id')->on('vat_types');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
        
    }
}

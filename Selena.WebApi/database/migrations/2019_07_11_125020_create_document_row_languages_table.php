<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentRowLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_rows_languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('document_row_id');
            $table->unsignedBigInteger('language_id');
            $table->string('description', 25);

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('document_row_id')->references('id')->on('documents_rows');
            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents_rows_languages');
       
    }
}

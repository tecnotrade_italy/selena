<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsphotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itemsphotos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('img', 255)->nullable();
            $table->unsignedBigInteger('item_id')->nullable();
            $table->timestamps();

            $table->foreign('item_id')->references('id')->on('items');
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable(); 
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itemsphotos');

    }
}

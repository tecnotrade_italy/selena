<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordsOnSalesOrdersDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sales_orders_details', function (Blueprint $table) {
        $table->bigInteger('width')->nullable();
        $table->bigInteger('depth')->nullable();
        $table->bigInteger('height')->nullable();
        $table->bigInteger('support_id')->nullable();
        $table->foreign('support_id')->references('id')->on('supports');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sales_orders_details', function (Blueprint $table) {
            //
        });
    }
}

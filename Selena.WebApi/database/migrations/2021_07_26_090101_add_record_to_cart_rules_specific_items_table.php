<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordToCartRulesSpecificItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cart_rules_specific_items', function (Blueprint $table) {
            $table->decimal('discount_percentage', 10, 2)->nullable();
            $table->decimal('discount_value', 10, 4)->nullable();
            $table->unsignedBigInteger('quantity');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cart_rules_specific_items', function (Blueprint $table) {
            //
        });
    }
}

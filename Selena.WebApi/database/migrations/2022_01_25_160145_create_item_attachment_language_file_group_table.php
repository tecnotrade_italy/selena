<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemAttachmentLanguageFileGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_attachment_language_file_group_newsletters_table', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('item_attachment_file_id')->nullable();
            $table->unsignedBigInteger('group_newsletter_id')->nullable();
            
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('item_attachment_file_id')->references('id')->on('item_attachment_file');
            $table->foreign('group_newsletter_id')->references('id')->on('groups_newsletters');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_attachment_language_file_group_newsletters_table');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListGeneratorDestinationGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_generator_destination_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_list_generator');
            $table->unsignedBigInteger('group_id');

            $table->foreign('id_list_generator')->references('id')->on('list_generator');
            $table->foreign('group_id')->references('id')->on('groups_newsletters');
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_generator_starting_groups');
    }
}

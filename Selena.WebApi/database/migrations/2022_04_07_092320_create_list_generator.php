<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListGenerator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_generator', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('starting_price_type')->nullable();
            $table->unsignedBigInteger('id_starting_price_list');
            $table->bigInteger('type_of_price_change')->nullable();
            $table->bigInteger('price_change')->nullable();
            $table->bigInteger('destination_type')->nullable();
            $table->unsignedBigInteger('id_destination_price_list');
            $table->boolean('populate_only_if_empty')->default(false)->nullable();
            $table->boolean('round_to_decimals')->default(false)->nullable();
            $table->boolean('save_rule')->default(false)->nullable();
            $table->string('description_save_rule', 500)->nullable();
            $table->boolean('schedule')->default(false)->nullable();
            $table->string('days_of_the_week', 500)->nullable();
            $table->string('days_of_the_month', 500)->nullable();
            $table->string('month', 500)->nullable();
            $table->bigInteger('repeat_after_hour')->nullable();
            $table->bigInteger('repeat_after_min')->nullable();
            $table->bigInteger('repeat_times')->nullable();

            $table->foreign('id_starting_price_list')->references('id')->on('list');
            $table->foreign('id_destination_price_list')->references('id')->on('list');
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_generator');
        
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOptionalTypologyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('optional_typology', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('type_optional_id');
            $table->unsignedBigInteger('optional_id');
            $table->unsignedBigInteger('order')->nullable();
                        
            $table->timestamps();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();

            $table->foreign('type_optional_id')->references('id')->on('type_optional');
            $table->foreign('optional_id')->references('id')->on('optional');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('optional_typology');
      
    }
}

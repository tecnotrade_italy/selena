<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesOrderTokenlinkUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_order_tokenlink_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_user')->nullable();
            $table->bigInteger('sales_order_id')->nullable();
            $table->bigInteger('sales_order_detail_id')->nullable();
            $table->string('token_link', 255)->nullable();    
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('sales_order_id')->references('id')->on('sales_orders');
            $table->foreign('sales_order_detail_id')->references('id')->on('sales_orders_details');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_order_tokenlink_user');
    }
}

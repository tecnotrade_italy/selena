<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordsOnListGenerator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('list_generator', function (Blueprint $table) {
            $table->string('description_execute_time', 1000)->nullable();
            $table->string('type_schedule', 1000)->nullable();
            $table->boolean('execute')->default(false)->nullable();
     
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('list_generator', function (Blueprint $table) {
        
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts_rules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('description', 255)->nullable();
            $table->string('voucher_code', 100)->nullable();
            $table->bigInteger('voucher_type_id')->nullable();
            $table->bigInteger('availability')->nullable();
            $table->bigInteger('availability_residual')->nullable();
            $table->bigInteger('availability_per_user')->nullable();
            $table->bigInteger('priority')->nullable();
            $table->boolean('exclude_other_rules')->nullable();
            $table->boolean('online')->nullable();
            $table->dateTime('enabled_from')->nullable();
            $table->dateTime('enabled_to')->nullable();
            $table->bigInteger('apply_to_all_customers')->nullable();
            $table->bigInteger('apply_to_all_groups')->nullable();
            $table->bigInteger('apply_to_all_carriers')->nullable();
            $table->bigInteger('apply_to_all_items')->nullable();
            $table->bigInteger('apply_to_all_categories_items')->nullable();
            $table->unsignedBigInteger('minimum_quantity')->nullable();
            $table->unsignedBigInteger('maximum_quantity')->nullable();
            $table->decimal('minimum_order_amount', 10, 4)->nullable();    
            $table->boolean('taxes_excluded')->nullable();
            $table->boolean('shipping_excluded')->nullable();
            $table->boolean('free_shipping')->nullable();
            $table->bigInteger('discount_type')->nullable();
            $table->decimal('discount_value', 10, 2)->nullable();
            $table->bigInteger('apply_discount_to')->nullable();
            $table->boolean('add_to_cart_automatically')->nullable();    
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('voucher_type_id')->references('id')->on('vouchers_types');

        });

        Schema::create('carts_rules_customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cart_rule_id')->nullable();
            $table->bigInteger('customer_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('cart_rule_id')->references('id')->on('carts_rules');
            $table->foreign('customer_id')->references('id')->on('customers');
        });

        Schema::create('carts_rules_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cart_rule_id')->nullable();
            $table->bigInteger('group_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('cart_rule_id')->references('id')->on('carts_rules');
            //$table->foreign('customer_id')->references('id')->on('customers');
        });

        Schema::create('carts_rules_carriers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cart_rule_id')->nullable();
            $table->bigInteger('carrier_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('cart_rule_id')->references('id')->on('carts_rules');
            $table->foreign('carrier_id')->references('id')->on('carriers');
        });

        Schema::create('carts_rules_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cart_rule_id')->nullable();
            $table->bigInteger('item_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('cart_rule_id')->references('id')->on('carts_rules');
            $table->foreign('item_id')->references('id')->on('items');
        });

        Schema::create('carts_rules_categories_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cart_rule_id')->nullable();
            $table->bigInteger('category_item_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('cart_rule_id')->references('id')->on('carts_rules');
            $table->foreign('category_item_id')->references('id')->on('categories_items');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts_rules_customers');
        Schema::dropIfExists('carts_rules_groups');
        Schema::dropIfExists('carts_rules_carriers');
        Schema::dropIfExists('carts_rules_items');
        Schema::dropIfExists('carts_rules_categories_items');
        Schema::dropIfExists('carts_rules');
    }
}

<?php

use App\RoleUser;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddIdRoleUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('role_user', function (Blueprint $table) {
            $table->dropPrimary('role_user_pkey');
        });

        Schema::table('role_user', function (Blueprint $table) {
            $table->bigIncrements('id');
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('role_user', function (Blueprint $table) {
            $table->dropColumn('id');
        });

        Schema::table('role_user', function (Blueprint $table) {
            $table->primary(['user_id', 'role_id', 'user_type'], 'role_user_pkey');
        });

    }
}

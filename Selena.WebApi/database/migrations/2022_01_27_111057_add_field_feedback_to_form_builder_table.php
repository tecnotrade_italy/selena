<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldFeedbackToFormBuilderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('form_builder', function (Blueprint $table) {
            $table->string('feedback_message', 500)->nullable();
            $table->boolean('feedback_redirect')->default(false);
            $table->string('feedback_url_redirect', 500)->nullable();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('form_builder', function (Blueprint $table) {
            //
        });
    }
}

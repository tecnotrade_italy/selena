<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorrelationPeopleuser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('correlation_peopleuser', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_people')->nullable();
            $table->bigInteger('id_user')->nullable();
             $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('id_people')->references('id')->on('people');
            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('correlation_peopleuser');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertising extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertising_areas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 100);
            $table->string('description', 255);
            $table->unsignedBigInteger('width_pixels')->nullable();
            $table->unsignedBigInteger('height_pixels')->nullable();
            $table->boolean('active')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');

        });

        Schema::create('advertising_banners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('advertising_area_id');
            $table->string('description', 255);
            $table->dateTime('start_date');
            $table->dateTime('stop_date')->nullable();
            $table->string('image_file', 255)->nullable();
            $table->string('image_alt', 255)->nullable();
            $table->string('link_url', 255)->nullable();
            $table->boolean('target_blank')->nullable();
            $table->boolean('active')->nullable();
            $table->text('script_Js')->nullable();
            $table->unsignedBigInteger('maximum_impressions')->nullable();
            $table->unsignedBigInteger('progressive_impressions')->nullable();
            $table->unsignedBigInteger('weight');
            $table->unsignedBigInteger('user_id')->nullable();   //inteso come cliente che paga la pubblicità
            $table->boolean('active_reports')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();
            
            $table->foreign('advertising_area_id')->references('id')->on('advertising_areas');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');

        });

        Schema::create('advertising_banners_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('advertising_banner_id');
            $table->dateTime('impression_date');
            $table->text('page');
            $table->string('source_ip', 255)->nullable();

            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();
            
            $table->foreign('advertising_banner_id')->references('id')->on('advertising_banners');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertising');
    }
}

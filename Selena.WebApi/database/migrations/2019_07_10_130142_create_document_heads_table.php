<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentHeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_heads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->unsignedBigInteger('document_type_id');
            $table->string('reference_code', 50)->nullable();
            $table->string('description', 255)->nullable();
            $table->dateTime('date')->nullable();
            $table->dateTime('shipping_date')->nullable();
            $table->dateTime('expected_shipping_date')->nullable();
            $table->unsignedBigInteger('currency_id')->nullable();
            $table->unsignedBigInteger('carriage_id')->nullable();
            $table->unsignedBigInteger('payment_type_id')->nullable();
            $table->unsignedBigInteger('carrier_id')->nullable();
            $table->string('shipping_address', 255)->nullable();
            $table->unsignedBigInteger('shipping_district_id')->nullable();
            $table->unsignedBigInteger('shipping_province_id')->nullable();
            $table->unsignedBigInteger('shipping_nation_id')->nullable();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('carriage_id')->references('id')->on('carriages');
            $table->foreign('carrier_id')->references('id')->on('carriers');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('document_type_id')->references('id')->on('documents_types');
            $table->foreign('payment_type_id')->references('id')->on('payments_types');
            $table->foreign('shipping_district_id')->references('id')->on('districts');
            $table->foreign('shipping_province_id')->references('id')->on('provinces');
            $table->foreign('shipping_nation_id')->references('id')->on('nations');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents_heads');
       
    }
}

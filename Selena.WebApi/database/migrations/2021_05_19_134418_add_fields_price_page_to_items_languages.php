<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsPricePageToItemsLanguages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items_languages', function (Blueprint $table) {
            $table->string('link_price', 255)->nullable();  
            $table->string('meta_tag_title_price', 255)->nullable();  
            $table->string('meta_tag_description_price', 255)->nullable();  
            $table->string('meta_tag_keyword_price', 255)->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items_languages', function (Blueprint $table) {
            //
        });
    }
}

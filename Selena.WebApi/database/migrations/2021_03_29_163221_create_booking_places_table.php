<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingPlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_places', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255)->nullable();  
            $table->string('address', 255)->nullable(); 
            $table->string('postal_code', 255)->nullable();   
            $table->string('place', 255)->nullable();  
            $table->string('province', 255)->nullable(); 
            $table->bigInteger('nation_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('nation_id')->references('id')->on('nations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_places');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToUsersAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_addresses', function (Blueprint $table) {
            $table->string('business_name', 255)->nullable();
            $table->string('name', 255)->nullable();
            $table->string('surname', 255)->nullable();
            $table->string('fiscal_code', 255)->nullable();
            $table->string('postal_code', 255)->nullable();
            $table->string('vat_number', 255)->nullable();
            $table->unsignedBigInteger('region_id')->nullable();
            $table->foreign('region_id')->references('id')->on('region');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_addresses', function (Blueprint $table) {
            $table->dropColumn('business_name');
            $table->dropColumn('name');
            $table->dropColumn('surname');
            $table->dropColumn('fiscal_code');
            $table->dropColumn('postal_code');
            $table->dropColumn('vat_number');
            $table->dropColumn('phone_number');
            $table->dropColumn('region_id');
        });

    }
}

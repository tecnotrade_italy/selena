<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordsOnLanguagesPageCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('languages_page_categories', function (Blueprint $table) {
            $table->string('seo_title', 255)->nullable();
            $table->string('seo_description', 255)->nullable();
            $table->text('seo_keyword')->nullable();
            $table->string('share_title', 255)->nullable();
            $table->string('share_description', 255)->nullable();
            $table->string('url_cover_image', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('languages_page_categories', function (Blueprint $table) {
            //
        });
    }
}

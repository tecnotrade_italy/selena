<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProducerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 255)->nullable();
            $table->string('business_name', 255)->nullable();
            $table->string('address', 255)->nullable();
            $table->string('resort', 255)->nullable();
            $table->string('province', 255)->nullable();
            $table->unsignedBigInteger('postal_code_id')->nullable();
            $table->string('telephone_number', 50)->nullable();
            $table->string('email', 255)->nullable();
            $table->unsignedBigInteger('language_id')->nullable();
             $table->boolean('online')->nullable();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('postal_code_id')->references('id')->on('postals_codes');
            $table->foreign('language_id')->references('id')->on('languages');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producer');
       
    }
}

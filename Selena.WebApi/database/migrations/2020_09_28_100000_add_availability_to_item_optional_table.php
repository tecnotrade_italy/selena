<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAvailabilityToItemOptionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_optional', function (Blueprint $table) {
            $table->unsignedBigInteger('availability')->nullable();
           
        });
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_optional', function (Blueprint $table) {
            $table->unsignedBigInteger('availability')->nullable();
        });

      
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 50);
            $table->unsignedBigInteger('tab_id');
            $table->string('field', 100)->nullable();
            $table->integer('data_type')->nullable();
            $table->boolean('required')->nullable();
            $table->string('default_value', 255)->nullable();
            $table->string('service', 255)->nullable();
            $table->integer('max_length')->nullable();
            $table->string('on_change', 255)->nullable();
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('tab_id')->references('id')->on('tabs');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fields');
   
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_connections', function (Blueprint $table) {
            $table->bigIncrements('id');
       
            $table->bigInteger('place_id')->nullable();
            $table->bigInteger('service_id')->nullable();
            $table->bigInteger('employee_id')->nullable();
            $table->foreign('place_id')->references('id')->on('booking_places');
            $table->foreign('service_id')->references('id')->on('booking_services');
            $table->foreign('employee_id')->references('id')->on('booking_employees');

            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->string('nr_place');
            $table->boolean('show_availability_calendar')->nullable();
            $table->bigInteger('type_of_date');
            $table->boolean('MONDAY')->default(false);
            $table->boolean('TUESDAY')->default(false);
            $table->boolean('WEDNESDAY')->default(false);
            $table->boolean('THURSDAY')->default(false);
            $table->boolean('FRIDAY')->default(false);
            $table->boolean('SATURDAY')->default(false);
            $table->boolean('SUNDAY')->default(false);
            $table->boolean('exclude_holidays')->default(false);
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->date('fixed_date')->nullable();
            $table->date('date_from')->nullable();
            $table->date('date_to')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_connections');
    }
}

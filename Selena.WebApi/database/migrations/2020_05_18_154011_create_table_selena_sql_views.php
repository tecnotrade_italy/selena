<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableSelenaSqlViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('selena_sql_views', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name_view', 255);
            $table->string('description', 255);
            $table->text('select')->nullable();
            $table->text('from')->nullable();
            $table->text('where')->nullable();
            $table->text('order_by')->nullable();
            $table->text('group_by')->nullable();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('selena_sql_views');
      
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFielsToCategoriesLanguages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories_languages', function (Blueprint $table) {
            $table->text('meta_tag_characteristic')->nullable();
            $table->string('meta_tag_link', 255)->nullable();
            $table->string('meta_tag_title', 255)->nullable();
            $table->string('meta_tag_description',255)->nullable();
            $table->string('keyword',255)->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories_languages', function (Blueprint $table) {
            $table->dropColumn('meta_tag_characteristic');
            $table->dropColumn('meta_tag_link');
            $table->dropColumn('meta_tag_title');
            $table->dropColumn('meta_tag_description');
            $table->dropColumn('keyword');
        });
    }
}

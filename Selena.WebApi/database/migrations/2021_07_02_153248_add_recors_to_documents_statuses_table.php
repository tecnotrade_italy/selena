<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecorsToDocumentsStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_statuses', function (Blueprint $table) {
            $table->string('html', 500)->nullable();  
            $table->string('delivered_document', 500)->nullable();  
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_statuses', function (Blueprint $table) {
            //
        });
    }
}

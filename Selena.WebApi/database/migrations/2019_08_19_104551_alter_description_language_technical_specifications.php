<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDescriptionLanguageTechnicalSpecifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('languages_technicals_specifications', function (Blueprint $table) {
            $table->string('description', 255)->change();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('languages_technicals_specifications', function (Blueprint $table) {
            $table->string('description', 25)->change();
        });

       
    }
}

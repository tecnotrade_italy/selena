<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordsOnBookingConnections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('booking_connections', function (Blueprint $table) {
            $table->time('monday_start_time')->nullable();
            $table->time('monday_end_time')->nullable();
            $table->time('tuesday_start_time')->nullable();
            $table->time('tuesday_end_time')->nullable();
            $table->time('wednesday_start_time')->nullable();
            $table->time('wednesday_end_time')->nullable();
            $table->time('thursday_start_time')->nullable();
            $table->time('thursday_end_time')->nullable();
            $table->time('friday_start_time')->nullable();
            $table->time('friday_end_time')->nullable(); 
            $table->time('saturday_start_time')->nullable();
            $table->time('saturday_end_time')->nullable();
            $table->time('sunday_start_time')->nullable();
            $table->time('sunday_end_time')->nullable();


            
            /*
            $table->time('monday_start_time_pm')->nullable();
            $table->time('monday_end_time_pm')->nullable();
            $table->time('tuesday_start_time_pm')->nullable();
            $table->time('tuesday_end_time_pm')->nullable();
            $table->time('wednesday_start_time_pm')->nullable();
            $table->time('wednesday_end_time_pm')->nullable();
            $table->time('thursday_start_time_pm')->nullable();
            $table->time('thursday_end_time_pm')->nullable();
            $table->time('friday_start_time_pm')->nullable();
            $table->time('friday_end_time_pm')->nullable();
            $table->time('saturday_start_time_pm')->nullable();
            $table->time('saturday_end_time_pm')->nullable();
            $table->time('sunday_start_time_pm')->nullable();
            $table->time('sunday_end_time_pm')->nullable();
            */

            /*$table->boolean('monday_exclude_holidays_agg')->nullable();
            $table->boolean('tuesday_exclude_holidays_agg')->nullable();
            $table->boolean('wednesday_exclude_holidays_agg')->nullable();
            $table->boolean('thursday_exclude_holidays_agg')->nullable();
            $table->boolean('friday_exclude_holidays_agg')->nullable();
            $table->boolean('saturday_exclude_holidays_agg')->nullable();
            $table->boolean('sunday_exclude_holidays_agg')->nullable();

            $table->string('monday_nr_place')->nullable();
            $table->string('tuesday_nr_place')->nullable();
            $table->string('wednesday_nr_place')->nullable();
            $table->string('thursday_nr_place')->nullable();
            $table->string('friday_nr_place')->nullable();
            $table->string('saturday_nr_place')->nullable();
            $table->string('sunday_nr_place')->nullable();*/

    });
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('booking_connections', function (Blueprint $table) {
            //
        });

    }
}

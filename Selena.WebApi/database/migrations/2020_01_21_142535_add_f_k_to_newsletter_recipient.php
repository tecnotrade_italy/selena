<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFKToNewsletterRecipient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('newsletters_recipients', function (Blueprint $table) {
            $table->unsignedBigInteger('contact_id')->nullable();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->unsignedBigInteger('group_newsletter_id')->nullable();

            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('group_newsletter_id')->references('id')->on('groups_newsletters');
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}

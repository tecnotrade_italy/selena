<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordsOnCarriagesLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carriages_languages', function (Blueprint $table) {
            $table->string('order', 255)->nullable(); 
            $table->boolean('add_transport_cost')->default(false);
            $table->boolean('online')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carriages_languages', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCampiOnBookingConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_connections', function (Blueprint $table) {
            
            $table->boolean('limit_reservations_monday')->nullable();
            $table->boolean('limit_reservations_tuesday')->nullable();
            $table->boolean('limit_reservations_wednesday')->nullable();
            $table->boolean('limit_reservations_thursday')->nullable();
            $table->boolean('limit_reservations_friday')->nullable();
            $table->boolean('limit_reservations_saturday')->nullable();
            $table->boolean('limit_reservations_sunday')->nullable();

            $table->string('hour_monday')->nullable();
            $table->string('hour_tuesday')->nullable();
            $table->string('hour_wednesday')->nullable();
            $table->string('hour_thursday')->nullable(); 
            $table->string('hour_friday')->nullable();
            $table->string('hour_saturday')->nullable();
            $table->string('hour_sunday')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_connections', function (Blueprint $table) {
            //
        });
    }
}

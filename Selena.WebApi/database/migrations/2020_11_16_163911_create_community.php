<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommunity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        /**********************************************************************************************/
        // TABELLA CON LE GALLERIE (NON OBBLIGATORIE) DELL'UTENTE
        /**********************************************************************************************/
        Schema::create('community_images_galleries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('name', 255);
            $table->string('description', 255)->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /**********************************************************************************************/
        // TABELLA CON LE CATEGORIE DELLE IMMAGINI
        /**********************************************************************************************/
        Schema::create('community_images_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order')->nullable();
            $table->boolean('online')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /**********************************************************************************************/
        // TABELLA CON LE CATEGORIE DELLE IMMAGINI IN LINGUA
        /**********************************************************************************************/
        Schema::create('community_images_categories_languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('image_category_id');
            $table->unsignedBigInteger('language_id');
            $table->string('description', 255);
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();

            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('image_category_id')->references('id')->on('community_images_categories');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /**********************************************************************************************/
        // TABELLA CON LE IMMAGINI
        /**********************************************************************************************/
        Schema::create('community_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('image_title', 255);
            $table->unsignedBigInteger('image_category_id');
            $table->unsignedBigInteger('gallery_id')->nullable();
            $table->text('image_description')->nullable();
            $table->decimal('location_latitude', 9, 6)->nullable();  
            $table->decimal('location_longitude', 9, 6)->nullable();  
            $table->text('keywords')->nullable();
            $table->boolean('susceptible_content')->nullable();
            $table->dateTime('upload_datetime');
            $table->unsignedBigInteger('nr_like')->nullable();
            $table->unsignedBigInteger('nr_favorites')->nullable();
            $table->unsignedBigInteger('nr_views')->nullable();
            $table->unsignedBigInteger('rating')->nullable();
            $table->string('EXIF_camera', 255)->nullable();
            $table->unsignedBigInteger('EXIF_camera_id')->nullable();
            $table->string('EXIF_lens', 255)->nullable();
            $table->unsignedBigInteger('EXIF_lens_id')->nullable();
            $table->string('EXIF_iso', 255)->nullable();
            $table->string('EXIF_shutter_speed', 255)->nullable();
            $table->string('EXIF_aperture', 255)->nullable();
            $table->string('EXIF_focal_lenght', 255)->nullable();
            $table->boolean('editor_pick')->nullable();
            $table->dateTime('editor_pick_date')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->boolean('online');
            
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('image_category_id')->references('id')->on('community_images_categories');
            $table->foreign('gallery_id')->references('id')->on('community_images_galleries');
            $table->foreign('EXIF_camera_id')->references('id')->on('items');
            $table->foreign('EXIF_lens_id')->references('id')->on('items');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /**********************************************************************************************/
        // TABELLA CON I COMMENTI ALLE IMMAGINI
        /**********************************************************************************************/
        Schema::create('community_images_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('image_id');
            $table->dateTime('comment_datetime');
            $table->text('comment');
            $table->unsignedBigInteger('comment_id_response')->nullable();
            $table->boolean('invisible')->nullable();
            $table->text('comment_admin_moderation')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('image_id')->references('id')->on('community_images');
            $table->foreign('comment_id_response')->references('id')->on('community_images_comments');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /**********************************************************************************************/
        // TABELLA CON LE SEGNALAZIONI DEGLI UTENTI AI COMMENTI
        /**********************************************************************************************/
        Schema::create('community_images_comments_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('comment_id');
            $table->text('report_message')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('comment_id')->references('id')->on('community_images_comments');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /**********************************************************************************************/
        // TABELLA CON I CONTEST FOTOGRAFICI
        /**********************************************************************************************/
        Schema::create('community_contests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('start_datetime');
            $table->dateTime('stop_datetime');
            $table->unsignedBigInteger('image_id_winner');
            $table->unsignedBigInteger('image_id_winner2');
            $table->unsignedBigInteger('image_id_winner3');
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();

            $table->foreign('image_id_winner')->references('id')->on('community_images');
            $table->foreign('image_id_winner2')->references('id')->on('community_images');
            $table->foreign('image_id_winner3')->references('id')->on('community_images');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /**********************************************************************************************/
        // TABELLA CON I CONTEST FOTOGRAFICI IN LINGUA
        /**********************************************************************************************/
        Schema::create('community_contests_languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('contest_id');
            $table->unsignedBigInteger('language_id');
            $table->string('title', 255);
            $table->text('description')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();

            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('contest_id')->references('id')->on('community_contests');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /**********************************************************************************************/
        // TABELLA CON FOTO PARTECIPANTI AI CONTEST
        /**********************************************************************************************/
        Schema::create('community_contests_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('contest_id');
            $table->unsignedBigInteger('image_id');
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();

            $table->foreign('contest_id')->references('id')->on('community_contests');
            $table->foreign('image_id')->references('id')->on('community_images');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /**********************************************************************************************/
        // TABELLA CON I TIPI DI AZIONE FATTIBILI SULLE IMMAGINI (LIKE, FAVORITE, ECC.)
        /**********************************************************************************************/
        Schema::create('community_actions_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /**********************************************************************************************/
        // TABELLA CON I TIPI DI AZIONE FATTIBILI SULLE IMMAGINI (LIKE, FAVORITE, ECC.) IN LINGUA
        /**********************************************************************************************/
        Schema::create('community_actions_types_languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('action_type_id');
            $table->unsignedBigInteger('language_id');
            $table->string('description', 255);
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();

            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('action_type_id')->references('id')->on('community_actions_types');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /**********************************************************************************************/
        // TABELLA CON LE AZIONI FATTE DAGLI UTENTI SULLE IMMAGINI
        /**********************************************************************************************/
        Schema::create('community_actions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('action_type_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('image_id');
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();

            $table->foreign('action_type_id')->references('id')->on('community_actions_types');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('image_id')->references('id')->on('community_images');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /**********************************************************************************************/
        // TABELLA CON I TIPI DI SEGNALAZIONE AD UNA IMMAGINE (OFFENSIVA, VIOLAZIONE COPYRIGHT, ECC.)
        /**********************************************************************************************/       
        Schema::create('community_images_reports_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /**********************************************************************************************/
        // TABELLA CON I TIPI DI SEGNALAZIONE AD UNA IMMAGINE (OFFENSIVA, VIOLAZIONE COPYRIGHT, ECC.) IN LINGUA
        /**********************************************************************************************/
        Schema::create('community_images_reports_types_languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('image_report_type_id');
            $table->unsignedBigInteger('language_id');
            $table->string('description', 255);
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();

            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('image_report_type_id')->references('id')->on('community_images_reports_types');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /**********************************************************************************************/
        // TABELLA CON SEGNALAZIONI ALLE IMMAGINI
        /**********************************************************************************************/
        Schema::create('community_images_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('image_report_type_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('image_id');
            $table->text('report_message')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();

            $table->foreign('image_report_type_id')->references('id')->on('community_images_reports_types');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('image_id')->references('id')->on('community_images');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /**********************************************************************************************/
        // TABELLA CON GALLERIE DELLE IMMAGINI FAVORITE
        /**********************************************************************************************/
        Schema::create('community_favorite_images_galleries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('name', 255);
            $table->string('description', 255)->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /**********************************************************************************************/
        // TABELLA CON FOTO FAVORITE
        /**********************************************************************************************/
        Schema::create('community_favorite_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('image_id');
            $table->unsignedBigInteger('favorite_gallery_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('image_id')->references('id')->on('community_images');
            $table->foreign('favorite_gallery_id')->references('id')->on('community_favorite_images_galleries');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      
        Schema::dropIfExists('community_images_reports_types_languages');
        Schema::dropIfExists('community_images_reports_types');
        Schema::dropIfExists('community_images_reports');

        Schema::dropIfExists('community_actions_types_languages');
        Schema::dropIfExists('community_actions_types');
        Schema::dropIfExists('community_actions');

        Schema::dropIfExists('community_images_comments_reports');
        Schema::dropIfExists('community_images_comments');

        Schema::dropIfExists('community_contests_languages');
        Schema::dropIfExists('community_contests_images');
        Schema::dropIfExists('community_contests');

        Schema::dropIfExists('community_favorite_images_galleries');
        Schema::dropIfExists('community_favorite_images');

        Schema::dropIfExists('community_images_categories_languages');
        Schema::dropIfExists('community_images_categories');
        Schema::dropIfExists('community_images_galleries');
        Schema::dropIfExists('community_images');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingConnectionAppointmentsCartDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_connection_appointments_cart_detail', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('connections_id')->nullable();
            $table->foreign('connections_id')->references('id')->on('booking_connections');

            $table->bigInteger('cart_id')->nullable();
            $table->foreign('cart_id')->references('id')->on('carts');
            
            $table->time('start_time')->nullable();
            $table->bigInteger('available')->nullable();
            $table->bigInteger('price')->nullable();
            $table->bigInteger('total_price')->nullable();
            $table->bigInteger('nr_place')->nullable();
            $table->bigInteger('reduced_price')->nullable();
            $table->bigInteger('nr_place_disponibili')->nullable();
            
    
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_connection_appointments_cart_detail');
    }
}

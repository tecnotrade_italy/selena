<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyPickedQuantiDocumentRow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_rows', function (Blueprint $table) {
            $table->integer('picked_quantity')->nullable()->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_rows', function (Blueprint $table) {
            $table->integer('picked_quantity')->change();
        });

    
    }
}

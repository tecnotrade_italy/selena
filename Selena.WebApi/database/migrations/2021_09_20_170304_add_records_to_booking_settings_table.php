<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordsToBookingSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_settings', function (Blueprint $table) {
            $table->boolean('check_user_week')->default(false);
            $table->boolean('check_service_week')->default(false);
            $table->boolean('check_employee_week')->default(false);
            $table->boolean('check_place_week')->default(false);
            $table->boolean('check_price_week')->default(false);
            $table->boolean('check_states_week')->default(false);

            $table->boolean('check_user_day')->default(false);
            $table->boolean('check_service_day')->default(false);
            $table->boolean('check_employee_day')->default(false);
            $table->boolean('check_place_day')->default(false);
            $table->boolean('check_price_day')->default(false);
            $table->boolean('check_states_day')->default(false);

            $table->string('order_user_week')->nullable();
            $table->string('order_service_week')->nullable();
            $table->string('order_employee_week')->nullable();
            $table->string('order_place_week')->nullable();
            $table->string('order_price_week')->nullable();
            $table->string('order_states_week')->nullable();

            $table->string('order_user_day')->nullable();
            $table->string('order_service_day')->nullable();
            $table->string('order_employee_day')->nullable();
            $table->string('order_place_day')->nullable();
            $table->string('order_price_day')->nullable();
            $table->string('order_states_day')->nullable();

            $table->string('order_user_month')->nullable();
            $table->string('order_service_month')->nullable();
            $table->string('order_employee_month')->nullable();
            $table->string('order_place_month')->nullable();
            $table->string('order_price_month')->nullable();
            $table->string('order_states_month')->nullable();

            





            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_settings', function (Blueprint $table) {
            //
        });
    }
}

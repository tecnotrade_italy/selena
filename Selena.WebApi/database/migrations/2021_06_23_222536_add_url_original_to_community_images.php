<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUrlOriginalToCommunityImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('community_images', function (Blueprint $table) {
            //
            $table->string('img_original', 500)->nullable();  
            $table->string('img_detail', 500)->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('community_images', function (Blueprint $table) {
            //
        });
    }
}

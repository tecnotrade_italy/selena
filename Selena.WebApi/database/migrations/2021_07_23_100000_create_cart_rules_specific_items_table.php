<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartRulesSpecificItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_rules_specific_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('carts_rules_id');
            $table->bigInteger('item_id');
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();
            $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('carts_rules_id')->references('id')->on('carts_rules');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_rules_specific_items');
    }
}

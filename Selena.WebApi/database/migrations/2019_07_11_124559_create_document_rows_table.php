<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_rows', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('document_head_id');
            $table->unsignedBigInteger('document_row_type_id');
            $table->unsignedBigInteger('item_id')->nullable();
            $table->unsignedBigInteger('unit_of_measure_id')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('quantity_evaded')->nullable();
            $table->decimal('unit_price', 10, 4)->nullable();
            $table->decimal('discount', 10, 4)->nullable();
            $table->unsignedBigInteger('vat_type_id')->nullable();
            $table->decimal('total_price', 10, 4)->nullable();
            $table->unsignedBigInteger('sale_type_id')->nullable();
            $table->unsignedBigInteger('document_status_id')->nullable();
            $table->dateTime('expected_evaded_date')->nullable();
            $table->dateTime('evaded_date')->nullable();
            $table->integer('order')->nullable();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('document_head_id')->references('id')->on('documents_heads');
            $table->foreign('document_status_id')->references('id')->on('documents_statuses');
            $table->foreign('document_row_type_id')->references('id')->on('documents_rows_types');
            $table->foreign('sale_type_id')->references('id')->on('sales_types');
            $table->foreign('unit_of_measure_id')->references('id')->on('units_of_measures');
            $table->foreign('vat_type_id')->references('id')->on('vat_types');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents_rows');
    }
}

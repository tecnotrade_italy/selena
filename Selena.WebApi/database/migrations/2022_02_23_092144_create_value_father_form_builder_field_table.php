<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValueFatherFormBuilderFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('value_father_form_builder_field', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('value_form_builder_field_id');
            $table->string('value', 255)->nullable();
            $table->string('img', 255)->nullable();
            $table->bigInteger('order')->nullable();
            

            $table->foreign('value_form_builder_field_id')->references('id')->on('value_form_builder_field');
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('value_father_form_builder_field');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupDisplayTechnicalSpecificationTechnicalSpecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups_display_technical_specification_tech_spec', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('group_display_technical_specification_id');
            $table->unsignedBigInteger('technical_specification_id');

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('group_display_technical_specification_id')->references('id')->on('groups_display_technicals_specifications');
            $table->foreign('technical_specification_id')->references('id')->on('technicals_specifications');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups_display_technical_specification_tech_spec');
       
    }
}

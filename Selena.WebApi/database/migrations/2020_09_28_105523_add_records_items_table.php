<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordsItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->string('qta_max', 255)->nullable();
            $table->string('pieces_for_pack', 255)->nullable();
            $table->string('day_of_validity', 255)->nullable();
            $table->string('img_digital', 255)->nullable();
            $table->boolean('digital')->nullable();
            $table->boolean('limited')->nullable();
            $table->boolean('sell_if_not_available')->nullable();
            $table->unsignedBigInteger('unit_of_measure_id_packaging')->nullable();
            $table->foreign('unit_of_measure_id_packaging')->references('id')->on('units_of_measures');
        });

    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}

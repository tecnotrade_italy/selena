<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConvertRateOnVatType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vat_types', function (Blueprint $table) {
            $table->string('rate', 10)->nullable()->change();
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vat_types', function (Blueprint $table) {
            $table->float('rate', 4, 2)->change();
        });

      
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCarriageAndCarrierToCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->unsignedBigInteger('carriage_id')->nullable();
            $table->unsignedBigInteger('carrier_id')->nullable();

            $table->foreign('carriage_id')->references('id')->on('carriages');
            $table->foreign('carrier_id')->references('id')->on('carriers');
        });

     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('carriage_id');
            $table->dropColumn('carrier_id');
        });

      
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmazonItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazon_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('asin', 50);
            $table->string('title', 500)->nullable();
            $table->string('url', 500)->nullable();
            $table->text('informations')->nullable();
            $table->string('imageSmall', 500)->nullable();
            $table->string('imageMedium', 500)->nullable();
            $table->string('imageLarge', 500)->nullable();
            $table->string('buyingPrice', 15)->nullable();
            $table->string('normalPrice', 15)->nullable();
            $table->string('discountPercentage', 10)->nullable();
            $table->string('discountValue', 50)->nullable();
            $table->string('prime', 50)->nullable();          
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amazon_items');
    }
}

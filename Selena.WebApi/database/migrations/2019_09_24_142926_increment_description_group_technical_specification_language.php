<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IncrementDescriptionGroupTechnicalSpecificationLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groups_technicals_specifications_languages', function (Blueprint $table) {
            $table->string('description', 50)->change();
        });

      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups_technicals_specifications_languages', function (Blueprint $table) {
            $table->string('description', 50)->change();
        });

      
    }
}

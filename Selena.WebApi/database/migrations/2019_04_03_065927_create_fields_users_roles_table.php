<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsUsersRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields_users_roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('field_id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('role_id')->nullable();
            $table->boolean('enabled')->nullable();
            $table->string('description', 50)->nullable();
            $table->integer('input_type');
            $table->integer('pos_x')->nullable();
            $table->integer('pos_y')->nullable();
            $table->integer('table_order')->nullable();
            $table->boolean('required')->nullable();
            $table->string('class', 255)->nullable();
            $table->string('attribute', 255)->nullable();
            $table->string('default_value', 255)->nullable();
            $table->integer('colspan')->nullable();
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('field_id')->references('id')->on('fields');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fields_users_roles');
       
    }
}

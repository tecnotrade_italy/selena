<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('author_id')->nullable();
            $table->boolean('online')->nullable();
            $table->dateTime('publish')->nullable();
            $table->dateTime('visible_from')->nullable();
            $table->dateTime('visible_end')->nullable();
            $table->unsignedBigInteger('icon_id')->nullable();
            $table->boolean('homepage')->nullable();
            $table->boolean('hide_search_engine')->nullable();
            $table->unsignedBigInteger('page_share_type_id')->nullable();
            $table->unsignedBigInteger('page_category_id')->nullable();
            $table->boolean('fixed')->nullable();
            $table->dateTime('fixed_end')->nullable();
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('author_id')->references('id')->on('users');
            $table->foreign('icon_id')->references('id')->on('icons');
            $table->foreign('page_share_type_id')->references('id')->on('page_share_types');
            $table->foreign('page_category_id')->references('id')->on('page_categories');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
      
    }
}

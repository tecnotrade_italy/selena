<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('quotes_id');
            $table->unsignedBigInteger('nr_row');
            $table->unsignedBigInteger('item_id');
            $table->string('description', 250)->nullable();
            $table->unsignedBigInteger('unit_of_measure_id')->nullable();            
            $table->decimal('unit_price', 10, 4)->nullable();
            $table->unsignedBigInteger('quantity');  
            $table->decimal('net_amount', 10, 4)->nullable();
            $table->decimal('vat_amount', 10, 4)->nullable();
            $table->decimal('discount_percentage', 10, 2)->nullable();
            $table->decimal('discount_value', 10, 4)->nullable();                                  
            $table->unsignedBigInteger('vat_type_id')->nullable();
            $table->decimal('total_amount', 10, 4)->nullable();            
            $table->text('note')->nullable();

            $table->bigInteger('width')->nullable();
            $table->bigInteger('depth')->nullable();
            $table->bigInteger('height')->nullable();
            $table->bigInteger('support_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('quotes_id')->references('id')->on('quotes');
            $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('unit_of_measure_id')->references('id')->on('units_of_measures');
            $table->foreign('vat_type_id')->references('id')->on('vat_types');
            $table->foreign('support_id')->references('id')->on('supports');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes_details');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesOrdersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /************************************************
        //Aggiungo il flag AFFILIATO alla tabella degli utenti
        ************************************************/
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('affiliate')->nullable();
        });

        /************************************************
            Tabella indirizzi utente
        ************************************************/
        Schema::create('users_addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('address', 255);
            $table->string('locality', 255);
            $table->bigInteger('postalcode_id')->nullable();
            $table->bigInteger('province_id')->nullable();
            $table->bigInteger('nation_id')->nullable();
            $table->string('phone_number', 255);
            $table->string('note', 255);
            $table->boolean('default_address')->nullable();

            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('postalcode_id')->references('id')->on('postals_codes');
            $table->foreign('province_id')->references('id')->on('provinces');
            $table->foreign('nation_id')->references('id')->on('nations');
            $table->timestamps();
        });

        /************************************************
        //Aggiungo l'id della destinazione nel carrello
        ************************************************/
        Schema::table('carts', function (Blueprint $table) {
            $table->unsignedBigInteger('user_address_goods_id')->nullable();
            $table->unsignedBigInteger('user_address_documents_id')->nullable();
            $table->foreign('user_address_goods_id')->references('id')->on('users_addresses');
            $table->foreign('user_address_documents_id')->references('id')->on('users_addresses');
            $table->dropColumn('voucher_id');
        });

       /************************************************
            Tabella stati ordini di vendita (in lingua)
        ************************************************/
       /*  Schema::create('sales_orders_states', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->timestamps();
        });

        Schema::create('sales_orders_states_languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('language_id');
            $table->unsignedBigInteger('sales_order_state_id');
            $table->string('description', 100);
            $table->timestamps();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();

            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('sales_order_state_id')->references('id')->on('sales_orders_states');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });/*

        /************************************************
            Tabella porti (in lingua)
        ************************************************/
        Schema::create('carriage_paid_to', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->timestamps();
        });

        Schema::create('carriage_paid_to_languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('language_id');
            $table->unsignedBigInteger('carriage_paid_to_id');
            $table->string('description', 100);
            $table->timestamps();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();

            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('carriage_paid_to_id')->references('id')->on('carriage_paid_to');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /************************************************
            Tabella tipi righe ordini vendita
        ************************************************/
        Schema::create('sales_orders_rows_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->timestamps();
        });

        Schema::create('sales_orders_rows_types_languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('language_id');
            $table->unsignedBigInteger('sales_order_row_type_id');
            $table->string('description', 100);
            $table->timestamps();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();

            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('sales_order_row_type_id')->references('id')->on('sales_orders_rows_types');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /************************************************
            Tabella ordini vendita
        ************************************************/
        Schema::create('sales_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_number', 100)->nullable();
            $table->dateTime('date_order');
            $table->unsignedBigInteger('order_state_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('currency_id')->nullable();
            $table->unsignedBigInteger('payment_id')->nullable();
            $table->decimal('net_amount', 10, 4)->nullable();
            $table->decimal('vat_amount', 10, 4)->nullable();
            $table->decimal('shipment_amount', 10, 4)->nullable();
            $table->decimal('payment_cost', 10, 4)->nullable();  
            $table->decimal('service_amount', 10, 4)->nullable();          
            $table->decimal('discount_percentage', 10, 2)->nullable();
            $table->decimal('discount_value', 10, 4)->nullable();
            $table->decimal('total_amount', 10, 4)->nullable(); 
            $table->text('note')->nullable();
            $table->unsignedBigInteger('affiliate_id')->nullable();
            $table->unsignedBigInteger('carrier_id')->nullable();
            $table->unsignedBigInteger('user_address_goods_id')->nullable();
            $table->unsignedBigInteger('user_address_documents_id')->nullable();
            $table->unsignedBigInteger('carriage_paid_to_id')->nullable();
            $table->dateTime('fulfillment_date')->nullable();
            $table->boolean('to_be_updated')->nullable();
            $table->boolean('updated')->nullable();

            $table->timestamps();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('affiliate_id')->references('id')->on('users');
            $table->foreign('user_address_goods_id')->references('id')->on('users_addresses');
            $table->foreign('user_address_documents_id')->references('id')->on('users_addresses');
            $table->foreign('carriage_paid_to_id')->references('id')->on('carriage_paid_to');
            $table->foreign('order_state_id')->references('id')->on('documents_statuses');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('payment_id')->references('id')->on('payments');
            $table->foreign('carrier_id')->references('id')->on('carriers');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

        /************************************************
            Tabella righe ordini vendita
        ************************************************/

        Schema::create('sales_orders_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id');
            $table->unsignedBigInteger('row_number');
            $table->unsignedBigInteger('sales_order_row_type_id')->nullable();
            $table->unsignedBigInteger('order_detail_state_id')->nullable();
            $table->unsignedBigInteger('item_id');
            $table->string('description', 255)->nullable();
            $table->unsignedBigInteger('unit_of_measure_id')->nullable();
            $table->decimal('unit_price', 10, 4)->nullable();   
            $table->unsignedBigInteger('quantity');  
            $table->decimal('net_amount', 10, 4)->nullable();
            $table->decimal('vat_amount', 10, 4)->nullable();
            $table->decimal('discount_percentage', 10, 2)->nullable();
            $table->decimal('discount_value', 10, 4)->nullable();                                  
            $table->unsignedBigInteger('vat_type_id')->nullable();
            $table->decimal('total_amount', 10, 4)->nullable();            
            $table->text('note')->nullable();
            $table->dateTime('fulfillment_date')->nullable();
            $table->unsignedBigInteger('shipped_quantity')->nullable();

            $table->timestamps();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();

            $table->foreign('order_id')->references('id')->on('sales_orders');
            $table->foreign('sales_order_row_type_id')->references('id')->on('sales_orders_rows_types');
            $table->foreign('order_detail_state_id')->references('id')->on('documents_statuses');
            $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('unit_of_measure_id')->references('id')->on('units_of_measures');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });


        Schema::create('carts_rules_applied', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('cart_rule_id');
            $table->bigInteger('sales_order_id')->nullable();
            $table->bigInteger('cart_id_temporary')->nullable();

            $table->string('name', 100);
            $table->string('description', 255)->nullable();
            $table->string('voucher_code', 100)->nullable();
            $table->bigInteger('voucher_type_id')->nullable();
            $table->bigInteger('availability')->nullable();
            $table->bigInteger('availability_residual')->nullable();
            $table->bigInteger('availability_per_user')->nullable();
            $table->bigInteger('priority')->nullable();
            $table->boolean('exclude_other_rules')->nullable();
            $table->boolean('online')->nullable();
            $table->dateTime('enabled_from')->nullable();
            $table->dateTime('enabled_to')->nullable();
            $table->bigInteger('apply_to_all_customers')->nullable();
            $table->bigInteger('apply_to_all_groups')->nullable();
            $table->bigInteger('apply_to_all_carriers')->nullable();
            $table->bigInteger('apply_to_all_items')->nullable();
            $table->bigInteger('apply_to_all_categories_items')->nullable();
            $table->unsignedBigInteger('minimum_quantity')->nullable();
            $table->unsignedBigInteger('maximum_quantity')->nullable();
            $table->decimal('minimum_order_amount', 10, 4)->nullable();    
            $table->boolean('taxes_excluded')->nullable();
            $table->boolean('shipping_excluded')->nullable();
            $table->boolean('free_shipping')->nullable();
            $table->bigInteger('discount_type')->nullable();
            $table->decimal('discount_value', 10, 2)->nullable();
            $table->bigInteger('apply_discount_to')->nullable();
            $table->boolean('add_to_cart_automatically')->nullable();    
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();
            
            $table->foreign('cart_id_temporary')->references('id')->on('carts');
            $table->foreign('cart_rule_id')->references('id')->on('carts_rules');
            $table->foreign('sales_order_id')->references('id')->on('sales_orders');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('voucher_type_id')->references('id')->on('vouchers_types');

        });

        Schema::create('carts_rules_customers_applied', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cart_rule_id')->nullable();
            $table->bigInteger('customer_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('cart_rule_id')->references('id')->on('carts_rules_applied');
            $table->foreign('customer_id')->references('id')->on('customers');
        });

        Schema::create('carts_rules_groups_applied', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cart_rule_id')->nullable();
            $table->bigInteger('group_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('cart_rule_id')->references('id')->on('carts_rules_applied');
        });

        Schema::create('carts_rules_carriers_applied', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cart_rule_id')->nullable();
            $table->bigInteger('carrier_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('cart_rule_id')->references('id')->on('carts_rules_applied');
            $table->foreign('carrier_id')->references('id')->on('carriers');
        });

        Schema::create('carts_rules_items_applied', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cart_rule_id')->nullable();
            $table->bigInteger('item_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('cart_rule_id')->references('id')->on('carts_rules_applied');
            $table->foreign('item_id')->references('id')->on('items');
        });

        Schema::create('carts_rules_categories_items_applied', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cart_rule_id')->nullable();
            $table->bigInteger('category_item_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('cart_rule_id')->references('id')->on('carts_rules_applied');
            $table->foreign('category_item_id')->references('id')->on('categories_items');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_orders_details');
        Schema::dropIfExists('sales_orders');
        Schema::dropIfExists('sales_orders_states_languages');
        Schema::dropIfExists('sales_orders_states');
        Schema::dropIfExists('carriage_paid_to_languages');
        Schema::dropIfExists('carriage_paid_to');
        Schema::dropIfExists('sales_orders_rows_types_languages');
        Schema::dropIfExists('sales_orders_rows_types'); 
        Schema::dropIfExists('users_addresses');
        Schema::dropIfExists('carts_rules_customers_applied');
        Schema::dropIfExists('carts_rules_groups_applied');
        Schema::dropIfExists('carts_rules_carriers_applied');
        Schema::dropIfExists('carts_rules_items_applied');
        Schema::dropIfExists('carts_rules_categories_items_applied');
        Schema::dropIfExists('carts_rules_applied');

    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecipientSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipient_sms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_sms')->nullable();
            $table->string('telephone_number', 255)->nullable();
            $table->dateTime('date')->nullable();
            $table->string('result_sms', 255)->nullable();
            $table->dateTime('date_send_sms')->nullable();  
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('id_sms')->references('id')->on('sms');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipient_sms');
    }
}

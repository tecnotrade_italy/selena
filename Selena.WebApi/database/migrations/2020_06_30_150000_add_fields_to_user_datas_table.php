<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUserDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_datas', function (Blueprint $table) {
            $table->text('description')->nullable();
            $table->text('accepted_payments')->nullable();
            $table->text('terms_of_sale')->nullable();
            $table->string('business_name', 255)->nullable();  
            $table->string('name', 255)->nullable();  
            $table->string('surname', 255)->nullable();  
            $table->string('vat_number', 255)->nullable();  
            $table->string('fiscal_code', 255)->nullable(); 
            $table->string('address', 255)->nullable(); 
            $table->string('postal_code', 255)->nullable(); 
            $table->string('telephone_number', 255)->nullable();
            $table->string('fax_number', 255)->nullable();
            $table->string('website', 255)->nullable();
            $table->bigInteger('language_id')->nullable();
            $table->bigInteger('vat_type_id')->nullable();
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_datas', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('accepted_payments');
            $table->dropColumn('terms_of_sale');
            $table->dropColumn('business_name');  
            $table->dropColumn('name');  
            $table->dropColumn('surname');  
            $table->dropColumn('vat_number');  
            $table->dropColumn('fiscal_code'); 
            $table->dropColumn('address'); 
            $table->dropColumn('postal_code'); 
            $table->dropColumn('telephone_number');
            $table->dropColumn('fax_number');
            $table->dropColumn('website');
            $table->dropColumn('language_id');
            $table->dropColumn('vat_type_id');
        });

       
    }
}
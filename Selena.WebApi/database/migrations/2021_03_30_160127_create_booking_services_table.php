<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255)->nullable(); 
            $table->bigInteger('duration')->nullable();
            $table->bigInteger('slot_step')->nullable();
            $table->bigInteger('block_first')->nullable();
            $table->bigInteger('block_after')->nullable();
            $table->bigInteger('price')->nullable();
            $table->bigInteger('reduced_price')->nullable();
            $table->bigInteger('advance')->nullable();
            $table->boolean('activate_purchase')->nullable('false');
            $table->boolean('send_confirmation_end_only_upon_payment')->nullable('false');
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_services');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingExceptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_exceptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tooltip');
            $table->timestamp('date_start');
            $table->timestamp('date_end');
            $table->bigInteger('place_id')->nullable();
            $table->bigInteger('service_id')->nullable();
            $table->bigInteger('employee_id')->nullable();
            $table->foreign('place_id')->references('id')->on('booking_places');
            $table->foreign('service_id')->references('id')->on('booking_services');
            $table->foreign('employee_id')->references('id')->on('booking_employees');
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_exceptions');
    }
}

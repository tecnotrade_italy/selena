<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordsToUsersDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_datas', function (Blueprint $table) {
            $table->boolean('company')->default(false);
            $table->boolean('vat_exempt')->default(false);
            $table->boolean('payment_fixed')->default(false);
            $table->bigInteger('id_list')->nullable();
            $table->foreign('id_list')->references('id')->on('list');
            $table->bigInteger('id_carriage')->nullable();
            $table->foreign('id_carriage')->references('id')->on('carriages');
            $table->bigInteger('id_payment')->nullable();
            $table->foreign('id_payment')->references('id')->on('payments_types');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_datas', function (Blueprint $table) {
            //
        });
    }
}

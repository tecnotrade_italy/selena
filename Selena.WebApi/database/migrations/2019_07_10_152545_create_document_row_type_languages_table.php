<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentRowTypeLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_rows_types_languages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('document_row_type_id');
            $table->unsignedBigInteger('language_id');
            $table->string('description', 255);

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('document_row_type_id')->references('id')->on('documents_rows_types');
            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents_rows_types_languages');
      
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('nr_appointments')->nullable();
            $table->boolean('send_email_to_user')->nullable('false');
            $table->boolean('send_email_to_employee')->nullable('false');       
            $table->boolean('allow_cancellation')->nullable('false');       
            $table->boolean('acceptance_of_mandatory_conditions')->nullable('false');   
            $table->string('text_gdpr', 255)->nullable();  
            $table->string('email_receives_notifications', 255)->nullable();  
            $table->string('email_send_notifications', 255)->nullable();  
            $table->string('notification_object_send_to_administrator', 255)->nullable();  
            $table->string('notification_object_send_to_visitor', 255)->nullable(); 

            $table->bigInteger('booking_notification_id')->nullable();
            $table->bigInteger('cancellation_notification_id')->nullable();
            $table->bigInteger('confirmation_notification_id')->nullable();
            $table->bigInteger('admin_notification_id')->nullable();

            $table->bigInteger('time_limit')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->timestamps();
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('booking_notification_id')->references('id')->on('messages');
            $table->foreign('cancellation_notification_id')->references('id')->on('messages');
            $table->foreign('confirmation_notification_id')->references('id')->on('messages');
            $table->foreign('admin_notification_id')->references('id')->on('messages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_settings');
    }
}

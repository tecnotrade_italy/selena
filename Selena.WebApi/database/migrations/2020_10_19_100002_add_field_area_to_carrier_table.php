<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldAreaToCarrierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carriers', function (Blueprint $table) {
            $table->boolean('nation_id')->default(1);
            $table->boolean('region_id')->default(0);
            $table->boolean('province_id')->default(0);
            $table->boolean('custom_id')->default(0);
            $table->string('custom_field', 255)->nullable();
        });
     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carriers', function (Blueprint $table) {
            $table->dropColumn('nation_id');
            $table->dropColumn('region_id');
            $table->dropColumn('province_id');
            $table->dropColumn('custom_id');
            $table->dropColumn('custom_field');
        });

      
    }
}

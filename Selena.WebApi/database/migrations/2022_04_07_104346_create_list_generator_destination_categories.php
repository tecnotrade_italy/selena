<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListGeneratorDestinationCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_generator_destination_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_list_generator');
            $table->unsignedBigInteger('categories_id');

            $table->foreign('id_list_generator')->references('id')->on('list_generator');
            $table->foreign('categories_id')->references('id')->on('categories');
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_generator_starting_categories');
    }
}

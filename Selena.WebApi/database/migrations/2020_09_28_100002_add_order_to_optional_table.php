<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrderToOptionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('optional', function (Blueprint $table) {
            $table->dropColumn('value_optional');
            $table->unsignedBigInteger('order')->nullable();
           
        });
     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('optional', function (Blueprint $table) {
            $table->dropColumn('value_optional');
            $table->unsignedBigInteger('order')->nullable();
        });

      
    }
}

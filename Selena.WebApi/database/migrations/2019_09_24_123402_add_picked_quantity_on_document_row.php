<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPickedQuantityOnDocumentRow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_rows', function (Blueprint $table) {
            $table->integer('picked_quantity')->null();
        });

      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_rows', function (Blueprint $table) {
            $table->dropColumn('picked_quantity');
        });

       
    }
}

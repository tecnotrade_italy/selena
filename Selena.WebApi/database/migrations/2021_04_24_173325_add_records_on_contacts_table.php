<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordsOnContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts_requests', function (Blueprint $table) {
            $table->boolean('checkprivacy_cookie_policy')->nullable();
            $table->boolean('processing_of_personal_data')->nullable(); 
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts_requests', function (Blueprint $table) {
            //
        });
    }
}

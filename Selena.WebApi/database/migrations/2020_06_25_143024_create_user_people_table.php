<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_people', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('people_id')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->timestamps();
            $table->foreign('people_id')->references('id')->on('people');
            $table->foreign('user_id')->references('id')->on('users');         
        });

      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_people');
          
    }
}

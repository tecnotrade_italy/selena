<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLanguageItemTechnicalSpecification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items_technicals_specifications', function (Blueprint $table) {
            $table->unsignedBigInteger('language_id');

            $table->foreign('language_id')->references('id')->on('languages');
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items_technicals_specifications', function (Blueprint $table) {
            $table->dropColumn('language_id');
        });

    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemAttachmentFileCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_attachment_language_file_category_table', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('item_attachment_file_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('item_attachment_file_id')->references('id')->on('item_attachment_file');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_attachment_language_file_category_table');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStarRatingToCommunityImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('community_images', function (Blueprint $table) {
          $table->dropColumn('nr_favorites');
          $table->dropColumn('nr_like');
        });

        Schema::table('community_images', function (Blueprint $table) { 
            $table->decimal('star_rating', 10, 4)->nullable(); 
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('community_images', function (Blueprint $table) {
            //
        });
    }
}

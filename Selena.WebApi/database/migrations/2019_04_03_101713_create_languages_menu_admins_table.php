<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesMenuAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages_menu_admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('language_id');
            $table->unsignedBigInteger('menu_admin_id');
            $table->string('description', 25);
            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('menu_admin_id')->references('id')->on('menu_admins');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages_menu_admins');
       
    }
}

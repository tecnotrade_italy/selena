<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFormBuilderIdToFormBuilderFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('form_builder_field', function (Blueprint $table) {
            $table->unsignedBigInteger('form_builder_id');
            $table->foreign('form_builder_id')->references('id')->on('form_builder');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('form_builder_field', function (Blueprint $table) {
            //
        });
    }
}

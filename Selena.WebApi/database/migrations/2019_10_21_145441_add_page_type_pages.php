<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPageTypePages extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
      Schema::table('pages', function (Blueprint $table) {
          $table->String('page_type',25)->nullable();
      });

    
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::table('pages', function (Blueprint $table) {
          $table->dropColumn('page_type');
      });
  }
}

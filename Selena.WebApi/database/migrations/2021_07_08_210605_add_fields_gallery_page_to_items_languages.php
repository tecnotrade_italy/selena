<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsGalleryPageToItemsLanguages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items_languages', function (Blueprint $table) {
            $table->string('link_gallery', 255)->nullable();  
            $table->string('meta_tag_title_gallery', 255)->nullable();  
            $table->string('meta_tag_description_gallery', 255)->nullable();  
            $table->string('meta_tag_keyword_gallery', 255)->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items_languages', function (Blueprint $table) {
            //
        });
    }
}

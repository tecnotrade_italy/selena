<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyErpIdDocumentHead extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_heads', function (Blueprint $table) {
            $table->string('erp_id', 25)->nullable()->change();
        });

    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_heads', function (Blueprint $table) {
            $table->string('erp_id', 25)->change();
        });

       
    }
}

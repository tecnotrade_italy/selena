<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('company');
            $table->string('business_name', 255)->nullable();
            $table->string('name', 255)->nullable();
            $table->string('surname', 255)->nullable();
            $table->string('vat_number', 50)->nullable();
            $table->string('fiscal_code', 50)->nullable();
            $table->string('address', 255)->nullable();
            $table->unsignedBigInteger('postal_code_id')->nullable();
            $table->string('telephone_number', 50)->nullable();
            $table->string('fax_number', 50)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('website', 255)->nullable();
            $table->unsignedBigInteger('language_id')->nullable();
            $table->unsignedBigInteger('vat_type_id')->nullable();
            $table->dateTime('enable_from')->nullable();
            $table->dateTime('enable_to')->nullable();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('postal_code_id')->references('id')->on('postals_codes');
            $table->foreign('vat_type_id')->references('id')->on('vat_types');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
       
    }
}

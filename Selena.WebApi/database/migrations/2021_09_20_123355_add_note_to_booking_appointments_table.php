<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNoteToBookingAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_appointments', function (Blueprint $table) {
            $table->string('note')->nullable();
            $table->bigInteger('reduced_price')->nullable();
            $table->bigInteger('total_price')->nullable();
            $table->bigInteger('nr_place')->nullable();
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_appointments', function (Blueprint $table) {
            //
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTechnicalSpecificationRevisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_technicals_specifications_revisions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('item_technical_specification_id');
            $table->unsignedBigInteger('language_id');
            $table->string('group_display_description', 255)->nullable();
            $table->integer('group_display_order')->nullable();
            $table->string('field_description', 255)->nullable();
            $table->integer('field_posx')->nullable();
            $table->integer('field_posy')->nullable();
            $table->integer('field_input_type')->nullable();

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('item_technical_specification_id')->references('id')->on('items_technicals_specifications');
            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_technicals_specifications_revisions');
        
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordsUsersDatas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_datas', function (Blueprint $table) {
            $table->bigInteger('carrier_id')->nullable();
            $table->bigInteger('id_payments')->nullable();
            $table->string('id_sdi', 255)->nullable();
            $table->boolean('create_quote')->nullable();
            $table->boolean('privates')->nullable();

            $table->foreign('carrier_id')->references('id')->on('carriers');
 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_datas', function (Blueprint $table) {
            //
        });
    }
}

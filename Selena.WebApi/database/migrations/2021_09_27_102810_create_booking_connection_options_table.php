<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingConnectionOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_connection_options', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('connections_id')->nullable();
            $table->foreign('connections_id')->references('id')->on('booking_connections');
            $table->bigInteger('cost')->nullable();
            $table->string('options')->nullable();
            $table->boolean('check_each')->default(false);
            $table->bigInteger('optional_id')->nullable();
            $table->foreign('optional_id')->references('id')->on('optional');
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_connection_options');
    }
}

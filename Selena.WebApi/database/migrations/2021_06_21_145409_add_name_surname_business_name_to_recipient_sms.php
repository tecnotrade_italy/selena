<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNameSurnameBusinessNameToRecipientSms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recipient_sms', function (Blueprint $table) {
            $table->string('name', 500)->nullable();  
            $table->string('surname', 500)->nullable();  
            $table->string('business_name', 500)->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recipient_sms', function (Blueprint $table) {
            //
        });
    }
}

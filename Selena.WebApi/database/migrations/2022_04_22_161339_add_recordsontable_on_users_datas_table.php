<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordsontableOnUsersDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_datas', function (Blueprint $table) { 
            $table->bigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');


            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_datas', function (Blueprint $table) {
            //
        });
    }
}

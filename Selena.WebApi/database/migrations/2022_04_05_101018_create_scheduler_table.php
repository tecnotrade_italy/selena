<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchedulerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduler', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type', 500)->nullable();
            $table->unsignedBigInteger('id_task_type')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->string('name', 500)->nullable();
            $table->string('description', 500)->nullable();
            $table->text('action')->nullable();
            $table->text('arguments')->nullable();

            $table->dateTime('start_time_action')->nullable();
            $table->string('schedule', 20)->nullable();
            $table->string('frequency', 50)->nullable();
            $table->unsignedBigInteger('repeat_after_hour')->nullable();
            $table->unsignedBigInteger('repeat_after_min')->nullable();
            $table->unsignedBigInteger('repeat_times')->nullable();
            $table->dateTime('next_run')->nullable();
            $table->dateTime('last_run')->nullable();
            $table->string('status', 500)->nullable();

            $table->boolean('notification')->default(0);
            $table->boolean('active')->default(0);

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduler');
    }
}

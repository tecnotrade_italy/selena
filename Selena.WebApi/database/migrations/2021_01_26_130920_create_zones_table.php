<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->bigInteger('id_cities')->nullable();
            $table->bigInteger('id_postalcodes')->nullable();
            $table->bigInteger('id_provinces')->nullable();
            $table->bigInteger('id_regions')->nullable();
            $table->bigInteger('id_nations')->nullable();
            
            $table->timestamps();
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('id_cities')->references('id')->on('cities');
            $table->foreign('id_postalcodes')->references('id')->on('postals_codes');
            $table->foreign('id_provinces')->references('id')->on('provinces');
            $table->foreign('id_regions')->references('id')->on('region');
            $table->foreign('id_nations')->references('id')->on('nations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zones');
    }
}

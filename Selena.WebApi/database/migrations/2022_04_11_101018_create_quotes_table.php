<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->text('session_token');
            $table->string('description', 500)->nullable();
            $table->unsignedBigInteger('currency_id')->nullable();
            $table->decimal('net_amount', 10, 4)->nullable();
            $table->decimal('vat_amount', 10, 4)->nullable();
            $table->decimal('shipment_amount', 10, 4)->nullable();
            $table->decimal('payment_cost', 10, 4)->nullable();  
            $table->decimal('total_amount', 10, 4)->nullable();   
            $table->decimal('service_amount', 10, 4)->nullable();          
            $table->decimal('discount_percentage', 10, 2)->nullable();
            $table->decimal('discount_value', 10, 4)->nullable();
            $table->text('note')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();

            $table->unsignedBigInteger('user_address_goods_id')->nullable();
            $table->unsignedBigInteger('user_address_documents_id')->nullable();
            $table->unsignedBigInteger('carrier_id')->nullable();
            $table->bigInteger('payment_type_id')->nullable();

            $table->string('carrier_description', 255)->nullable(); 
            $table->bigInteger('carriage_id')->nullable();

            $table->bigInteger('salesman_id')->nullable();

            

            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->foreign('carriage_id')->references('id')->on('carriages');
            $table->foreign('payment_type_id')->references('id')->on('payments_types');
            $table->foreign('carrier_id')->references('id')->on('carriers');
            $table->foreign('user_address_goods_id')->references('id')->on('users_addresses');
            $table->foreign('user_address_documents_id')->references('id')->on('users_addresses');
    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotes');
    }
}

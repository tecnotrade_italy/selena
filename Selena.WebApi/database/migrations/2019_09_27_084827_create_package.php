<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description', 25);

            $table->unsignedBigInteger('created_id');
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });

      

        Schema::table('documents_rows', function (Blueprint $table) {
            $table->dropColumn('package');
            $table->unsignedBigInteger('package_id')->nullable();

            $table->foreign('package_id')->references('id')->on('packages');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_rows', function (Blueprint $table) {
            $table->dropColumn('package_id');
        });

      
        Schema::dropIfExists('packages');
    
    }
}

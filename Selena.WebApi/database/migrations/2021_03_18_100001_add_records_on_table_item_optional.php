<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordsOnTableItemOptional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_optional', function (Blueprint $table) {
            $table->bigInteger('um_support_variation_id')->nullable();
            $table->string('support_prameter', 255)->nullable();
            $table->decimal('support_variation_value', 10, 4)->nullable();

            $table->foreign('um_support_variation_id')->references('id')->on('units_of_measures');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_optional', function (Blueprint $table) {
            //
        });
    }
}

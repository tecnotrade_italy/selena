<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->string('business_name', 50)->nullable()->change();
            $table->string('phone_number', 50)->nullable()->change();
            $table->boolean('send_newsletter')->nullable()->change();
            $table->boolean('error_newsletter')->nullable()->change();
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function (Blueprint $table) {
            $table->string('business_name', 50)->change();
            $table->string('phone_number', 50)->change();
            $table->boolean('send_newsletter')->change();
            $table->boolean('error_newsletter')->change();
        });

        
    }
}

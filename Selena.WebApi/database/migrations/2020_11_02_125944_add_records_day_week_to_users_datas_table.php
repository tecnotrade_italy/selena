<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecordsDayWeekToUsersDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_datas', function (Blueprint $table) {
             $table->string('monday', 255)->nullable();
              $table->string('tuesday', 255)->nullable();
               $table->string('wednesday', 255)->nullable();
                $table->string('thursday', 255)->nullable();
                 $table->string('friday', 255)->nullable();
                  $table->string('saturday', 255)->nullable();
                   $table->string('sunday', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_datas', function (Blueprint $table) {
            //
        });
    }
}

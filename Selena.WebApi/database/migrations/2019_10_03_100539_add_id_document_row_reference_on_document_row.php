<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdDocumentRowReferenceOnDocumentRow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documents_rows', function (Blueprint $table) {
            $table->unsignedBigInteger('document_row_reference')->nullable();

            $table->foreign('document_row_reference')->references('id')->on('documents_rows');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documents_rows', function (Blueprint $table) {
            $table->dropColumn('document_row_reference');
        });

     
    }
}

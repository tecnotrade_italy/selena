<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('date', 255)->nullable();
            $table->string('adult_nr', 255)->nullable();  
            $table->string('kids_nr', 255)->nullable();  
            $table->string('babies_nr', 255)->nullable();  
            $table->string('total_amount', 255)->nullable(); 
            $table->string('name', 255)->nullable();
            $table->string('surname', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('telephone', 255)->nullable();
            $table->string('notes', 2000)->nullable();
            $table->string('status', 255)->nullable();
            $table->string('course_id', 255)->nullable();
            $table->boolean('payed')->nullable('false');
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            $table->timestamps();
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_tour');
    }
}

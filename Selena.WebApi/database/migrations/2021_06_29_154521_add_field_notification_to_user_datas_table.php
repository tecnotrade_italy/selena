<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldNotificationToUserDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_datas', function (Blueprint $table) {
            $table->boolean('community_send_notification_comments')->default(false);
            $table->boolean('community_send_notification_favorites')->default(false);
            $table->boolean('community_send_notification_star_rates')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_datas', function (Blueprint $table) {
            //
        });
    }
}

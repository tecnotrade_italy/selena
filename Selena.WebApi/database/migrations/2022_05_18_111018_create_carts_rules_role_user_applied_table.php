<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsRulesRoleUserAppliedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts_rules_role_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cart_rule_id')->nullable();
            $table->bigInteger('role_user_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('cart_rule_id')->references('id')->on('carts_rules');
        });
        Schema::create('carts_rules_role_user_applied', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('cart_rule_id')->nullable();
            $table->bigInteger('role_user_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();

            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('cart_rule_id')->references('id')->on('carts_rules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts_rules_role_user');
        Schema::dropIfExists('carts_rules_role_user_applied');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesOrdersDetailOptionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_orders_detail_optional', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sales_order_detail_id')->nullable();
            $table->bigInteger('optional_id')->nullable();
            $table->bigInteger('created_id')->nullable();
            $table->bigInteger('updated_id')->nullable();
            
            $table->timestamps();
            $table->foreign('created_id')->references('id')->on('users');
            $table->foreign('updated_id')->references('id')->on('users');
            $table->foreign('sales_order_detail_id')->references('id')->on('sales_orders_details');
            $table->foreign('optional_id')->references('id')->on('optional');
        });
     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_orders_detail_optional');
    }
}

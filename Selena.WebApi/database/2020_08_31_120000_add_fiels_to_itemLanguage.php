<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFielsToItemLanguage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items_languages', function (Blueprint $table) {
            $table->string('link', 255)->nullable();
            $table->string('meta_tag_title', 255)->nullable();
            $table->string('meta_tag_description',255)->nullable();
            $table->string('meta_tag_keyword',255)->nullable();
        });

       /* Schema::connection(env('DB_CONNECTION_TEST', 'data_test'))->table('items_languages', function (Blueprint $table) {
            $table->string('link', 255)->nullable();
            $table->string('meta_tag_title', 255)->nullable();
            $table->string('meta_tag_description',255)->nullable();
            $table->string('meta_tag_keyword',255)->nullable();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items_languages', function (Blueprint $table) {
            $table->dropColumn('province');
            $table->dropColumn('link');
            $table->dropColumn('meta_tag_title');
            $table->dropColumn('meta_tag_description');
            $table->dropColumn('meta_tag_keyword');
        });

       /* Schema::connection(env('DB_CONNECTION_TEST', 'data_test'))->table('items_languages', function (Blueprint $table) {
            $table->dropColumn('province');
            $table->dropColumn('link');
            $table->dropColumn('meta_tag_title');
            $table->dropColumn('meta_tag_description');
            $table->dropColumn('meta_tag_keyword');
        });*/
    }
}

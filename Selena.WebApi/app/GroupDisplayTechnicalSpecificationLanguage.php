<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupDisplayTechnicalSpecificationLanguage extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'groups_display_technicals_specifications_languages';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}

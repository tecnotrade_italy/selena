<?php

namespace App;

use App\Helpers\HttpHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RelatedItems extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'related_items';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}

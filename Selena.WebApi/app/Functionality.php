<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Functionality extends Model
{
    use ObservantTrait;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}

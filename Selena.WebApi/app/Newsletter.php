<?php

namespace App;

use App\Helpers\HttpHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    use ObservantTrait;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at'
    ];

    #region DATE FORMAT FIELD

    /**
     * Get schedule_date_time with format
     */
  /*  public function getScheduleDateTimeAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }*/

    /**
     * Set schedule_date_time from client format date to server format
     */
   public function setScheduleDateTimeAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['schedule_date_time'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }
    }

    /**
     * Get start_send_date_time with format
     */
    public function getStartSendDateTimeAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set start_send_date_time from client format date to server format
     */
    public function setStartSendDateTimeAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['start_send_date_time'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }
    }

    /**
     * Get end_send_date_time with format
     */
    public function getEndSendDateTimeAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set end_send_date_time from client format date to server format
     */
    public function setEndSendDateTimeAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['end_send_date_time'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }
    }

    #endregion DATE FORMAT FIELD
}

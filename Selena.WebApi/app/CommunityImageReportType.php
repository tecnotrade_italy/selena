<?php

namespace App;

use App\Helpers\LogHelper;
use Illuminate\Database\Eloquent\Model;

class CommunityImageReportType extends Model
{
    use ObservantTrait;

    protected $table = 'community_images_reports_types';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}

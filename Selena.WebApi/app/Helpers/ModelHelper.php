<?php

namespace App\Helpers;

use App\Helpers\LogHelper;

class ModelHelper
{
    /**
     * Convert model to array
     *
     * @param \App $model
     * @return array
     */
    public static function toArray($model)
    {
        $array = array();

        foreach ($model as $key => $value) {
            $array[$key] = $value;
        }
        return $array;
    }

    public static function checkConfiguration($resultModel, $dto, $configs, $keySubModel, $resultSubModel, $dtoOnDB = null)
    {
        if (is_null($configs)) {
            return $resultModel;
        } else {
            if ($dtoOnDB != null) {
                $resultModel = $dtoOnDB;
            }

            foreach ($configs as $config) {
                $field = $config->field;

                if (!stripos($field, '.')) {
                    $resultModel->$field = $dto[$field];
                }
            }

            foreach ($dto->$keySubModel as $subModel) {
                $tmpSubModel = new $resultSubModel;

                foreach ($configs as $config) {
                    $field = $config->field;
                    if (stripos($config->field, '.')) {
                        $fieldKey = explode('.', $config->field)[1];
                        if (array_key_exists($fieldKey, $subModel)) {
                            $tmpSubModel->$fieldKey = $subModel[$fieldKey];
                        }
                    }
                }
                $resultModel->$keySubModel->push($tmpSubModel);
            }

            return $resultModel;
        }
    }
}

<?php

namespace App\Helpers;

class ArrayHelper
{
    /**
     * Convert from array to collection
     * 
     * @param array $array
     * @return collection
     */
    public static function toCollection($array)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = static::toCollection($value);
                $array[$key] = $value;
            }
        }

        return collect($array);
    }
}
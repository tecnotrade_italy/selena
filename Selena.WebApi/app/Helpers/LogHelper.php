<?php

namespace App\Helpers;

use App\Mail\LogMail;
use Illuminate\Support\Facades\Log as Log;
use Illuminate\Support\Facades\Mail;

class LogHelper
{
    /*
     *   Log alert
     */
    public static function alert($message)
    {
        Log::alert($message);

        switch (config('app.log_mail_level')) {
            case 'debug':
            case 'info':
            case 'notice':
            case 'warning':
            case 'error':
            case 'critical':
            case 'alert':
                //Mail::to(config('app.log_mail_to'))->send(new LogMail('Alert', str_replace('\'', ' ', $message)));
                break;
        }
    }

    /*
     *   Log critical
     */
    public static function critical($message)
    {
        Log::critical($message);

        switch (config('app.log_mail_level')) {
            case 'debug':
            case 'info':
            case 'notice':
            case 'warning':
            case 'error':
            case 'critical':
                //Mail::to(config('app.log_mail_to'))->send(new LogMail('Critical', str_replace('\'', ' ', $message)));
                break;
        }
    }

    /*
     *   Log debug
     */
    public static function debug($message)
    {
        Log::debug($message);

        if (config('app.log_mail_level') == 'debug') {
            //Mail::to(config('app.log_mail_to'))->send(new LogMail('Debug', str_replace('\'', ' ', $message)));
        }
    }

    /*
     *   Log emergency
     */
    public static function emergency($message)
    {
        Log::emergency($message);

        switch (config('app.log_mail_level')) {
            case 'debug':
            case 'info':
            case 'notice':
            case 'warning':
            case 'error':
            case 'critical':
            case 'alert':
            case 'emergency':
                //Mail::to(config('app.log_mail_to'))->send(new LogMail('Emergency', str_replace('\'', ' ', $message)));
                break;
        }
    }

    /*
     *   Log error
     */
    public static function error($message)
    {
        Log::error($message);

        switch (config('app.log_mail_level')) {
            case 'debug':
            case 'info':
            case 'notice':
            case 'warning':
            case 'error':
                //Mail::to(config('app.log_mail_to'))->send(new LogMail('Error', str_replace('\'', ' ', $message)));
                break;
        }
    }

    /*
     *   Log info
     */
    public static function info($message)
    {
        Log::info($message);

        switch (config('app.log_mail_level')) {
            case 'debug':
            case 'info':
                //Mail::to(config('app.log_mail_to'))->send(new LogMail('Info', str_replace('\'', ' ', $message)));
                break;
        }
    }

    /*
     *   Log notice
     */
    public static function notice($message)
    {
        Log::notice($message);

        switch (config('app.log_mail_level')) {
            case 'debug':
            case 'info':
            case 'notice':
                //Mail::to(config('app.log_mail_to'))->send(new LogMail('Notice', str_replace('\'', ' ', $message)));
                break;
        }
    }

    /*
     *   Log warning
     */
    public static function warning($message)
    {
        Log::warning($message);

        switch (config('app.log_mail_level')) {
            case 'debug':
            case 'info':
            case 'notice':
            case 'warning':
                //Mail::to(config('app.log_mail_to'))->send(new LogMail('Warning', str_replace('\'', ' ', $message)));
                break;
        }
    }
}

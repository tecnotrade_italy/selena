<?php

namespace App\Helpers;

use App\Mail\LogMail;
use Illuminate\Support\Facades\Mail;

class LogSyncroHelper
{
    /**
     * Start log syncro
     * 
     * @param string date
     * @param string syncroTitle
     */
    public static function startLog($date, string $syncroTitle)
    {
        $fileDir = storage_path() . '/logs/syncro_' . $syncroTitle . '_' . $date . '.log';
        error_log("###   Start syncro " . $syncroTitle . "   ###\n", 3, $fileDir);
    }

    /**
     * Log per sycnro
     * 
     * @param string date
     * @param string syncroTitle
     * @param object message    
     * @param object row
     * @param exception exception
     */
    public static function log($date, string $syncroTitle, $message, $row = null, $exception = null)
    {
        $fileDir = storage_path() . '/logs/syncro_' . $syncroTitle . '_' . $date . '.log';

        if (is_null($exception)) {
            if (is_null($row)) {
                error_log("\n" . $message, 3, $fileDir);
            } else {
                error_log("\n" . $message . "\nof:\n" . var_export($row, true), 3, $fileDir);
            }
        } else {
            if (is_null($row)) {
                error_log("\n" . var_export($message, true) . "\nExpection:\n" .  $exception, 3, $fileDir);
            } else {
                error_log("\n" . $message . "\nof:\n" . var_export($row, true) . "\nExpection:\n" .  $exception, 3, $fileDir);
            }
        }
    }
    /**
     * End log syncro
     * 
     * @param string date
     * @param string syncroTitle
     */
    public static function endLog($date, string $syncroTitle)
    {
        $fileDir = storage_path() . '/logs/syncro_' . $syncroTitle . '_' . $date . '.log';
        error_log("\n\n###   End syncro " . $syncroTitle . '   ###', 3, $fileDir);
    }

    /**
     * Send email syncro
     * 
     * @param string date
     * @param string syncroTitle
     */
    public static function send($date, string $syncroTitle)
    {
        $fileDir = storage_path() . '\logs\syncro_' . $syncroTitle . '_' . $date . '.log';
        if (file_exists($fileDir)) {
            //$myfile = fopen($fileDir, "r") or die("Unable to open file!");
            Mail::to(config('app.log_mail_to'))->send(new LogMail('Error syncro', $fileDir));
            //fclose($myfile);
        }
    }
}

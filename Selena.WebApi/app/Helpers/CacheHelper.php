<?php

namespace App\Helpers;

use Cache;
use Carbon\Carbon;
use App\BusinessLogic\LanguageBL;
use App\BusinessLogic\PageBL;

class CacheHelper
{
    #region PRIVATE

    private $cache_long_days = 1;
    private $cache_language_url = 'language_url_{0}';

    #endregion PRIVATE

    public function __construct()
    { }

    private function setCache($name, $values, $expired_at = 0)
    {
        $expired_at = (!empty($expired_at)) ? $expired_at : now()->addDays($this->cache_long_days);
        cache([$name => $values], $expired_at);
    }

    #region LANGUAGE BY URL

    /**
     * Set language by url
     */
    public function setLanguageByUrl()
    {
        $url = request()->url();
        $idLanguage = PageBL::getIdLanguageUrl($url);
        $language = LanguageBL::get($idLanguage);
        if (is_null($language)) {
            $language = LanguageBL::getDefault();
        }

        $this->setCache(str_replace('{0}', $url, $this->cache_language_url), $language);
    }

    /**
     * Get language by url
     */
    public function getLanguageByUrl()
    {
        if (!Cache::has(str_replace('{0}', request()->url(), $this->cache_language_url))) {
            static::setLanguageByUrl();
        }

        return cache(str_replace('{0}', request()->url(), $this->cache_language_url));
    }

    #endregion LANGUAGE BY URL
}

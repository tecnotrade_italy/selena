<?php

namespace App\Helpers;

use App\Language;
use App\User;
use Illuminate\Support\Facades\Auth;

class HttpHelper
{
    /**
     * Get id user autenticate
     *
     * @return int idUser
     */
    public static function getUserId()
    {
        if (Auth::check()) {
            return Auth::user()->id;
        } else {
            return null;
        }
    }

    /**
     * Get id language by user autenticate or url
     *
     * @return int idLanguage
     */
    public static function getLanguageId()
    {
        if (Auth::check()) {
            return Auth::user()->language->id;
        } else {
            $cacheHelper = new CacheHelper();
            return $cacheHelper->getLanguageByUrl()->id;
        }
    }

    /**
     * Get date format language by user autenticate or url
     *
     * @return string dateFormat
     */
    public static function getLanguageDateFormat()
    {
        if (Auth::check()) {
            return Auth::user()->language->date_format;
        } else {
            $cacheHelper = new CacheHelper();
            return $cacheHelper->getLanguageByUrl()->date_format;
        }
    }

    /**
     * Get date time format language by user autenticate or url
     *
     * @return string dateTimeFormat
     */
    public static function getLanguageDateTimeFormat()
    {
        if (Auth::check()) {
            return Auth::user()->language->date_time_format;
        } else {
            $cacheHelper = new CacheHelper();
            return $cacheHelper->getLanguageByUrl()->date_time_format;
        }
    }

    /**
     * Get date format client language by user autenticate or url
     *
     * @return string dateFormatClient
     */
    public static function getLanguageDateFormatClient()
    {
        if (Auth::check()) {
            return Auth::user()->language->date_format_client;
        } else {
            $cacheHelper = new CacheHelper();
            return $cacheHelper->getLanguageByUrl()->date_format_client;
        }
    }

    /**
     * Get date time format client language by user autenticate or url
     *
     * @return string dateTimeFormatClient
     */
    public static function getLanguageDateTimeFormatClient()
    {
        if (Auth::check()) {
            return Auth::user()->language->date_time_format_client;
        } else {
            $cacheHelper = new CacheHelper();
            return $cacheHelper->getLanguageByUrl()->date_time_format_client;
        }
    }
}

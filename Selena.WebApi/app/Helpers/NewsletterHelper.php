<?php

namespace App\Helpers;

use Soundasleep\Html2Text;
use App\Mail\ConfirmTestMailSender;
use Illuminate\Support\Facades\Mail;
use App\BusinessLogic\SettingBL;
use App\Enums\SettingEnum;

class NewsletterHelper
{
    /**
     * Send
     * 
     * @param string host
     * @param int port
     * @param string encryption
     * @param string username
     * @param string $password
     * @param string $fromName
     * @param string $fromAddress
     * @param string $toName
     * @param string $toAddress
     * @param string $subject
     * @param string $html
     */
    public static function send(
        string $sender_email,
        string $email,
        string $title,
        string $html,
        string $object,
        string $sender,
        string $preview_object,
        string $cssEmail,
        int $idNewsletterRecipient
    ) {
                            
            $emailFrom = $sender_email;
            $html = $html;
            $title = $title;
            $sender = $sender;
            $sender_email = $sender_email;
            $object = $object;
            $preview_object = $preview_object;
            $cssEmail = $cssEmail;
            $email=$email;
            $url = config('app.url');
            $htmlToSend = '<img width="1" height="1" src="' . $url . '/v1/reportnewsletter/' . $idNewsletterRecipient . '/OPEN">' . $html;
           
            
            $MailHost = SettingBL::getByCode(SettingEnum::MailHost); //smtp-out.kpnqwest.it
            $MailPort = SettingBL::getByCode(SettingEnum::MailPort); //25
            $MailUsername = SettingBL::getByCode(SettingEnum::MailUsername); //selena@tecnotrade.com
            $MailPassword = SettingBL::getByCode(SettingEnum::MailPassword); //7CU^$#ptBUc4.2}C
            $MailEncryption = SettingBL::getByCode(SettingEnum::MailEncryption); //tls

            /*$backup = Mail::getSwiftMailer();
            $transport = \Swift_SmtpTransport::newInstance('mtp-out.kpnqwest.it', 25, 'tls');
            $transport->setUsername('selena@tecnotrade.com');
            $transport->setPassword('7CU^$#ptBUc4.2}C');
            $smtpNewsletter = new \Swift_Mailer($transport);
            Mail::setSwiftMailer($smtpNewsletter);*/

            \Config::set('mail.encryption', $MailEncryption);
            \Config::set('mail.host', $MailHost);
            \Config::set('mail.port', $MailPort);
            \Config::set('mail.username', $MailUsername);
            \Config::set('mail.password', $MailPassword);

            #region SEND EMAIL 
            Mail::to($email)->send(new ConfirmTestMailSender($emailFrom, $htmlToSend, $title, $sender, $sender_email, $object, $preview_object, $cssEmail));

    }
}

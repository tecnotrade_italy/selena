<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DBHelper
{
    #region PRIVATE

    /**
     * Return ID transaction
     *
     * @return int
     */
    public static function getIdTransaction()
    {
        return DB::select('SELECT THREAD_ID as id FROM performance_schema.threads WHERE PROCESSLIST_ID = CONNECTION_ID()')[0]->id;
    }

    #endregion PRIVATE

    #region CONVERT

    /**
     * Convert model to logModel
     *
     * @param \App\ $model
     * @param \App\ModelsLogs\ $modelLog
     * @return \App\ModelsLogs\
     */
    public static function convertToLogModelToInsert($model, $modelLog)
    {
        foreach ($model->toArray() as $key => $value) {
            if ($key == 'id') {
                $modelLog->log_id = $value;
            } else {
                $modelLog->$key = $value;
            }
        }

        if (Auth::check()) {
            $modelLog->log_user_id = Auth::user()->id;
        } else {
            $modelLog->log_user_id = $model->created_id;
        }

        $modelLog->log_action = 'INSERT';
        $modelLog->log_created_at = Carbon::now();
        $modelLog->log_transaction_id = static::getIdTransaction();
        return $modelLog;
    }

    /**
     * Convert model to logModel
     *
     * @param \App\ $model
     * @param \App\ModelsLogs\ $modelLog
     * @return \App\ModelsLogs\
     */
    public static function convertToLogModelToUpdate($model, $modelLog)
    {
        foreach ($model->toArray() as $key => $value) {
            if ($key == 'id') {
                $modelLog->log_id = $value;
            } else {
                if ($model->isDirty($key)) {
                    $modelLog->$key = $value;
                }
            }
        }

        if (Auth::check()) {
            $modelLog->log_user_id = Auth::user()->id;
        } else {
            $modelLog->log_user_id = $model->updated_id;
        }

        $modelLog->log_action = 'UPDATE';
        $modelLog->log_created_at = Carbon::now();
        $modelLog->log_transaction_id = static::getIdTransaction();
        return $modelLog;
    }

    /**
     * Convert model to logModel
     *
     * @param \App\ $model
     * @param \App\ModelsLogs\ $modelLog
     * @return \App\ModelsLogs\
     */
    public static function convertToLogModelToDelete($model, $modelLog)
    {
        $modelLog->log_id = $model->id;
        $modelLog->log_user_id = Auth::user()->id;
        $modelLog->log_action = 'DELETE';
        $modelLog->log_created_at = Carbon::now();
        $modelLog->log_transaction_id = static::getIdTransaction();
        return $modelLog;
    }

    #endregion CONVERT

    /**
     * Update field and save
     *
     * @param Model modelToSave
     * @param Model modelOnDB
     */
    public static function updateAndSave($modelToSave, $modelOnDB)
    {
        foreach ($modelToSave->toArray() as $key => $value) {
            if ($key != 'id') {
                $modelOnDB->$key = $value;
            }
        }

        $modelOnDB->save();
    }

    /**
     * Update field and save
     *
     * @param Model modelToSave
     * @param Model modelOnDB
     */
    public static function updateAndSaveBlogNews($modelToSave, $modelOnDB)
    {

        foreach ($modelToSave->toArray() as $key => $value) {
            if ($key != 'id') {
                $modelOnDB->$key = $value;
            }
        }

        $modelOnDB->save();
    }
}

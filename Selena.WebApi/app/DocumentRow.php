<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentRow extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'documents_rows';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];

    #region DATE FORMAT FIELD

    /**
     * Get expected_evaded_date with format
     */
    public function getExpectedEvadedDateAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set expected_evaded_date from client format date to server format
     */
    public function setExpectedEvadedDateAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['expected_evaded_date'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }
    }

    /**
     * Get evaded_date with format
     */
    public function getEvadedDateAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set evaded_date from client format date to server format
     */
    public function setEvadedDateAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['evaded_date'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }
    }

    #endregion DATE FORMAT FIELD
}

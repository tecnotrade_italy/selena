<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriesFilterLanguages extends Model
{
    use ObservantTrait;
    /**
     * Table
     *
     * @var table
     */
    protected $table = 'categories_filter_languages';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}

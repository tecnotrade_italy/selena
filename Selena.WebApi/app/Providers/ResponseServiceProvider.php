<?php

namespace App\Providers;

use App\Http\Resources\DataResource;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;
use App\Helpers\ArrayHelper;
use function GuzzleHttp\json_encode;
use App\Helpers\LogHelper;

class ResponseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $headers = [
            'Content-Type' => config('project.api.response.content_type') . ';charset=' . config('project.api.response.charset'),
        ];

        Response::macro('error', function ($message, $code = 404) use ($headers) {
            return response()->json([
                'code' => $code,
                'success' => false,
                'status' => 'error',
                'message' => $message,
            ], $code, $headers, JSON_UNESCAPED_UNICODE);
        });

        Response::macro('link', function ($url) use ($headers) {
            return response()->json(['link' => $url], 200, $headers, JSON_UNESCAPED_UNICODE);
        });

        Response::macro('rulesFailed', function ($errors, $message = 'Correggere i dati inseriti') use ($headers) {
            return response()->json([
                'code' => 422,
                'success' => false,
                'status' => 'error',
                'message' => $message,
                'fields' => $errors,
            ], 422, $headers, JSON_UNESCAPED_UNICODE);
        });

        Response::macro('model', function ($model, $message = '', $additional = []) use ($headers) {
            $additional['code'] = 200;

            return (new DataResource($model))->additional($additional);
        });

        Response::macro('success', function ($data = [], $code = 200) use ($headers) {
            if (is_string($data)) {
                $data = ['message' => $data, 'data' => []];
            } else {
                $data = compact('data');
            }
            $response = array_merge(['code' => 200, 'status' => 'success', 'success' => true], $data);
            return response()->json($response, $code, $headers, JSON_UNESCAPED_UNICODE);
        });

        Response::macro('getSelect', function ($models = []) {
            $data = $models->map(function ($model) {

                $css = '';
                try {
                    if (!is_null($model->css)) {
                        $css = $model->css;
                    }
                } catch (\Throwable $ex) { }

                return [
                    'id' => $model->id,
                    'text' => $model->description,
                    'html' => $css
                ];
            });
            return $this->success($data);
        });

        Response::macro('dictionary', function ($models = []) {
            $data = $models->map(function ($model) {
                return [
                    $model->code => $model->description
                ];
            });
            return $this->success($data);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

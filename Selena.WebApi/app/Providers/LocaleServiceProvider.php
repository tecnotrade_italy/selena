<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class LocaleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Route::group(['prefix' => config('project.prefix.api').'/'.config('project.api.version')], function() {
			if (request()->is('*/alexa')) {
				$this->forceLocale('it');
			} else {
				$this->forceLocale();
			}
        });

        Route::group(['prefix' => config('project.prefix.manager')], function() {
			$this->forceLocale();
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function forceLocale($locale = '')
	{
		if (empty($locale)) {
			$locale = request()->server('HTTP_ACCEPT_LANGUAGE') or config('app.fallback_locale');
			if (strlen($locale) > 2) $locale = substr($locale, 0, 2);
		}
		$locale = strtolower($locale);
		app()->setLocale($locale);
	}
}

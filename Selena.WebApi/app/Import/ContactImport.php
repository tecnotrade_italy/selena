<?php
  
namespace App\Import;
  
use App\Contact;
use Maatwebsite\Excel\Concerns\ToModel;
  
class ContactImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Contact([
            'name' => $row[0],
            'surname' => $row[1],
            'businessName' => $row[2],
            'email' => $row[3],
            'phoneNumber' => $row[4],
            'sendNewsletter' => $row[5]
        ]);
    }
}
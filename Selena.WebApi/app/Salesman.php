<?php

namespace App;

use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Salesman extends Model
{
    use ObservantTrait;

    protected $table = 'salesman';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];

}
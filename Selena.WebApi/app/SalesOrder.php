<?php

namespace App;

use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class SalesOrder extends Model
{
    use ObservantTrait;

    protected $table = 'sales_orders';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];

      /**
     * Get enabled_from with format
     */
    public function getDateOrderAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set enabled_from from client format date to server format
     */
 /*   public function setDateOrderAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['date_order'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }else{
            $this->attributes['date_order'] = null;
        }
    }*/
}
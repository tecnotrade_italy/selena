<?php

namespace App;

use App\Helpers\HttpHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Negotiation extends Model
{
    use ObservantTrait;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];

    /**
     * Get publish with format
     */
    public function getCreatedDataAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }
    
}

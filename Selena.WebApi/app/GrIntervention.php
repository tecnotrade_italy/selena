<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;

class GrIntervention extends Model
{
    use ObservantTrait;



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at'
    ];

  /**
     * Get subscription_date with format
     */
    public function getDateDdtAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * 
     */
    public function setDateDdtAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['date_ddt'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d');
        }else{
            $this->attributes['date_ddt'] = null;
        }
    }
     /***/
    public function getDateDdtGrAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /***/
    public function setDateDdtGrAttribute($value)
    {
       if (!is_null($value)) {
            $this->attributes['date_ddt_gr'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d');
        }else{
            $this->attributes['date_ddt_gr'] = null;
        }
    }




}
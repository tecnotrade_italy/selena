<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\HttpHelper;

class Category extends Model
{
    use ObservantTrait;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];

    #region DATE FORMAT FIELD

    /**
     * Get visible_from with format
     */
    public function getVisibleFromAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set visible_from from client format date to server format
     */
    public function setVisibleFromAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['visible_from'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }else{
            $this->attributes['visible_from'] = null;
        }
    }

    /**
     * Get visible_to with format
     */
    public function getVisibleToAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set visible_to from client format date to server format
     */
    public function setVisibleToAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['visible_to'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }else{
            $this->attributes['visible_to'] = null;
        }
    }

    #endregion DATE FORMAT FIELD
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class DataResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (is_array($this->resource)) {
			return $this->resource;
        } else {
            return parent::toArray($request);
        }
    }

    public function with($request)
    {
        return [ 'status' => 'success', 'success' => true ];
	}
	
	public function withResponse($request, $response)
    {
		$response->headers->set('Content-Type', config('project.api.response.content_type').';charset='.config('project.api.response.charset'));
    }
}

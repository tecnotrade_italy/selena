<?php

namespace App\Http\Controllers;

use App\User;
use App\Salesman;
use App\Cart;
use App\BusinessLogic\CartBL;
use App\BusinessLogic\LogsBL;
use App\BusinessLogic\MessageBL;
use App\BusinessLogic\UserBL;
use App\Helpers\HttpHelper;
use App\RoleUser;
use App\UsersDatas;
use App\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Helpers\LogHelper;
use App\Message;
use App\MessageLanguage;

class AuthController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
        ]);

        $user = new User([
            'name' => $request->name,
            'email' => $request->email,           
            'password' => bcrypt($request->password),

        ]);

        $user->save();

        return response()->json(['message' => 'Successfully created user!'], 201);
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            // 'email' => 'required|string|email',
            'name' => 'required|string',
            'password' => 'required|string',
            // 'remember_me' => 'boolean',

        ]);

        $credentials = request(['name', 'password']);
        $userDescription = '';

        if (!Auth::attempt($credentials)) {
            return response()->json(['message' => 'Credenziali Errate', 'code' => '401']);
        }else{
            $user = $request->user();
           
            if ($user->online == 'true' && $user->confirmed == 'true'){
                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;
                $roleUser = RoleUser::where('user_id', $user->id)->get()->first();
                if(isset($roleUser)){
                    $roleDescription = Role::where('id', $roleUser->role_id)->first()->name;
                }else{
                    $roleDescription = "";
                }
                $usersDatas = UsersDatas::where('user_id', $user->id)->get()->first();
                if(isset($usersDatas)){
                    $userDescription = $usersDatas->business_name;
                }
                
                if ($request->remember_me) {
                    $token->expires_at = Carbon::now()->addWeeks(1);
                }
                $token->save();

                //Funzione di tracciamento login
                LogsBL::insert($user->id, $request->ip(), 'LOGIN', 'Login Effettuato');
            } else { 
               
                if ($user->online == false && $user->confirmed == true){
                    $Message = Message::where('code', 'message_user_not_enabled')->first();  
                    $MessageLanguages = MessageLanguage::where('message_id', $Message->id)->first();  
                    return response()->json(['message' => $MessageLanguages->content, 'code' => '404']);      
                }else{  
                    if ($user->online == true && $user->confirmed == false){
                         $Message = Message::where('code', 'message_unconfirmed_account')->first();  
                         $MessageLanguages = MessageLanguage::where('message_id', $Message->id)->first();
                        return response()->json(['message' => $MessageLanguages->content, 'code' => '404']);      
                    }else{
                        if ($user->online == false && $user->confirmed == false) {
                             $Message = Message::where('code', 'message_unauthorized_user')->first();  
                             $MessageLanguages = MessageLanguage::where('message_id', $Message->id)->first();
                            return response()->json(['message' => $MessageLanguages->content, 'code' => '404']); 
                        }
                    }  
                }
            }
        } 
         
        //mi memorizzo l'eventuale carrello dell'utente in memoria
        //$cartUser = CartBL::getByUserId($user->id);

        $cartUser = Cart::where('user_id', $user->id)->get()->first();
        $cartid = '';
        if(isset($cartUser)){
            $cartUser->salesman_id = null;
            $cartUser->save();
            $cartid = $cartUser->id;
        }else{
            $cartUserAnonymus = Cart::where('session_token', $request->session_token)->get()->first();
            if(isset($cartUserAnonymus)){
                $cartUserAnonymus->user_id = $user->id;
                $cartUserAnonymus->salesman_id = null;
                $cartUserAnonymus->session_token = 'Bearer ' . $tokenResult->accessToken;
                $cartUserAnonymus->save();
                $cartid = $cartUserAnonymus->id;
            }else{
                $cartNewUser = new Cart();
                $cartNewUser->user_id = $user->id;

                $usersDatas = UsersDatas::where('user_id', $user->id)->get()->first();
                if(isset($usersDatas)){
                    $cartNewUser->payment_type_id = $usersDatas->id_payments;
                }

                $cartNewUser->salesman_id = null;
                $cartNewUser->session_token = 'Bearer ' . $tokenResult->accessToken;
                $cartNewUser->save();
                $cartid = $cartNewUser->id;
            }            
        }

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
            'username' => $user->name,
            'role' => $roleDescription,
            'idLanguage' => $user->language_id,
            'id' => $user->id,
            'cart_id' => $cartid,
            'user_description' => $userDescription,
            'code' => '200'
        ]);
    }

    /**
     * Login salesman
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     */
    public function loginSalesman(Request $request){

        $request->validate([
            // 'email' => 'required|string|email',
            'name' => 'required|string',
            'password' => 'required|string',
            // 'remember_me' => 'boolean',
        ]);

        $salesman = Salesman::where('username', $request->name)->where('password', $request->password)->where('online', true)->get()->first();
        if(isset($salesman)){
            return response()->json([
                'id' => $salesman->id,
                'code' => '200'
            ]);
        }else{
            return response()->json(['message' => 'Credenziali Errate', 'code' => '401']);
        }
    }
    

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function insertLoginForFacebook(Request $request)
    {
        return response()->success(UserBL::insertLoginForFacebook($request, HttpHelper::getUserId()));   
    }

    public function insertLoginForGoogle(Request $request)
    {
        return response()->success(UserBL::insertLoginForGoogle($request, HttpHelper::getUserId()));   
    }


}

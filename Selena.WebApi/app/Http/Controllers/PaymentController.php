<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Omnipay\Omnipay;
use App\Payment;
use App\PaymentType;
use App\Cart;
use App\User;
use App\BusinessLogic\MessageBL;
use App\BusinessLogic\ContentBL;
use App\SalesOrder;
use App\BusinessLogic\CartBL;
use App\BusinessLogic\LogsBL;
use App\BusinessLogic\SettingBL;
use App\BusinessLogic\SelenaViewsBL;
use App\Content;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use App\Mail\DynamicMail;
use App\Enums\SettingEnum;
use App\OptionalLanguages;
use App\SalesOrderDetailOptional;
use App\OptionalTypology;
use App\ContentLanguage;
use App\Helpers\HttpHelper;
use App\Item;
use App\LanguageProvince;
use App\SalesOrderDetail;
use App\SalesOrderTokenLinkUser;
use App\TypeOptionalLanguages;
use App\Setting;
use App\Supports;
use App\UsersDatas;
use Carbon\Carbon;

class PaymentController extends Controller
{
 
    public $gateway;
 
    public function __construct()
    {
        $this->gateway = Omnipay::create('PayPal_Rest');
        //$this->gateway->setClientId(env('PAYPAL_CLIENT_ID'));
        //$this->gateway->setSecret(env('PAYPAL_CLIENT_SECRET'));
        $setTestMode = SettingBL::getByCode('TestModePayPal');
        $setClientId = SettingBL::getByCode('ClientIdPayPal');
        
        if($setClientId == '' || strtolower($setTestMode) == 'true'){
            $setClientId = 'AaHYlKDmrvm9OYdgi5798WWsiS-bDQuRP10NdcIAgnLFBK8fOllaq8USc_MofmoFqicz7Q7NYIZfSjXA';
        }

        $setSecret = SettingBL::getByCode('SecretPayPal');
        if($setSecret == '' || strtolower($setTestMode) == 'true'){
            $setSecret = 'EG6xEdNRTKprjckIzBELKxqhzuB9-tGvgNIw3MZ8xiQYWCd3WY3_yuQ7qfO4KpECAvd0V5PAnrgo-LBg';
        }

        
        if($setTestMode == ''){
            $setTestMode = true;
        }else{
            if(strtolower($setTestMode) == 'false' ){
                $setTestMode = false;
            }else{
                $setTestMode = true;
            }
        }

        $this->gateway->setClientId($setClientId);
        $this->gateway->setSecret($setSecret);
        $this->gateway->setTestMode($setTestMode); //set it to 'false' when go live
    }
 
    public function index()
    {
        return view('payment');
    }
 
    public function charge(Request $request)
    {
        $idCart = $request->cart_id;
        $cartOnDb = Cart::find($idCart);
        
        $onlinePayment = true;

        if(!is_null($cartOnDb->payment_type_id)){
            $paymentType = PaymentType::where('id', $cartOnDb->payment_type_id)->get()->first();
            if(isset($paymentType)){
                if(!$paymentType->online_type_of_payment){
                    $onlinePayment = false;
                }
            }
        }        

        if(!$onlinePayment){
            try {
                DB::beginTransaction();

                LogsBL::insert($cartOnDb->user_id, '', 'CHECKOUTORDER', 'Ordine confermato, pagamento online');

                $idSalesOrder = CartBL::transformCartIntoSalesOrder($idCart, 0);
    
                //mando la mail e reindirizzo l'utente alla pagina di riepilogo
                static::sendEmailOrderConfirmation($idSalesOrder,1);

                DB::commit();
            } catch (\Exception $ex) {
                DB::rollback();
                throw $ex;
            }

            $pageLanding = Setting::where('code', SettingEnum::Sales_Order_Confirmation_Page)->first()->value;

            $protocol = "";
            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
                $protocol = "https://";
            } else {
                $protocol = "http://";
            }

            return Redirect::to($protocol. $_SERVER["HTTP_HOST"].'/'.$pageLanding);

        }else{
            LogsBL::insert($cartOnDb->user_id, '', 'CHECKOUTORDER', 'Ordine confermato, pagamento offline');

            $returnUrl = '';
            $cancelUrl = '';

            
            $protocol = "";
            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
                $protocol = "https://";
            } else {
                $protocol = "http://";
            }
            //attivare in produzione
            $returnUrl = $protocol. $_SERVER["HTTP_HOST"] . '/api/paymentsuccess?id='.$idCart;
            $cancelUrl = $protocol. $_SERVER["HTTP_HOST"] . '/api/paymenterror';

            //attivare se in localhost
            //$returnUrl = 'http://localhost/api/paymentsuccess?id='.$idCart;
            //$cancelUrl = 'http://localhost/api/paymenterror';


            //$returnUrl = 'http://localhost/api/paymentsuccess?id='.$idCart;
            //$cancelUrl = 'http://localhost/api/paymenterror';

            try {
                $response = $this->gateway->purchase(array(
                    'amount' =>  round($cartOnDb->total_amount, 2),
                    'currency' => 'EUR',
                    'returnUrl' => $returnUrl,
                    'cancelUrl' => $cancelUrl,
                ))->send();


                if ($response->isRedirect()) {
                    $response->redirect(); // this will automatically forward the customer
                } else {
                    // not successful
                    return $response->getMessage();
                }
            } catch(Exception $e) {
                return $e->getMessage();
            }
        }        
    }
 
    public function payment_success(Request $request)
    {
        // Once the transaction has been approved, we need to complete it.
        if ($request->input('paymentId') && $request->input('PayerID'))
        {            
            try {
                DB::beginTransaction();

                $transaction = $this->gateway->completePurchase(array(
                    'payer_id'             => $request->input('PayerID'),
                    'transactionReference' => $request->input('paymentId'),
                ));
                $response = $transaction->send();
             
                if ($response->isSuccessful())
                {
                    // The customer has successfully paid.
                    $arr_body = $response->getData();
             
                    // Insert transaction data into the database
                    $isPaymentExist = Payment::where('payment_id', $arr_body['id'])->first();
             
                    if(!$isPaymentExist)
                    {
                        $payment = new Payment;
                        $payment->payment_id = $arr_body['id'];
                        $payment->payer_id = $arr_body['payer']['payer_info']['payer_id'];
                        $payment->payer_email = $arr_body['payer']['payer_info']['email'];
                        $payment->amount = $arr_body['transactions'][0]['amount']['total'];
                        $payment->currency = 'EUR';
                        $payment->payment_status = $arr_body['state'];
                        $payment->save();

                        $idSalesOrder = CartBL::transformCartIntoSalesOrder($request->id, $payment->id);
    
                        //mando la mail e reindirizzo l'utente alla pagina di riepilogo
                        static::sendEmailOrderConfirmation($idSalesOrder,1);

                    }
                    DB::commit();
                    
                    $pageLanding = Setting::where('code', SettingEnum::Sales_Order_Confirmation_Page)->first()->value;

                    $protocol = "";
                    if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
                        $protocol = "https://";
                    } else {
                        $protocol = "http://";
                    }

                    return Redirect::to($protocol. $_SERVER["HTTP_HOST"].'/'.$pageLanding);
                    //attivare se in localhost
                    //return Redirect::to('http://localhost:8081/'. $pageLanding);
                    
                    //return "Payment is successful. Your transaction id is: ". $arr_body['id'];
                } else {
                    DB::commit();
                    return $response->getMessage();
                }

            } catch (\Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        } else {
            return 'Transaction is declined';
        }
    }

    public function paymentbookingoffline(int $cartId, int $idLanguage)
    {
        try {
            DB::beginTransaction();

            $idSalesOrder = CartBL::transformCartIntoSalesOrder($cartId, 0);
    
            //mando la mail e reindirizzo l'utente alla pagina di riepilogo
            static::sendEmailOrderConfirmation($idSalesOrder, $idLanguage);

            DB::commit();
                    
            $pageLanding = Setting::where('code', SettingEnum::Sales_Order_Confirmation_Page)->first()->value;

            $protocol = "";
            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
                $protocol = "https://";
            } else {
                $protocol = "http://";
            }

            return Redirect::to($protocol. $_SERVER["HTTP_HOST"].'/'.$pageLanding);

        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
 
    private static function sendEmailOrderConfirmation(int $idSalesOrder, int $idLanguage)
    {
        //mi memorizzo l'html con i tag inclusi
        $testoMailConTag = MessageBL::getByCode('ORDER_CONFIRMATION_MAIL_TEXT', $idLanguage);
        $testoMail = '';

        //mi memorizzo l'oggetto della mail con i tag inclusi
        $oggettoMailConTag = MessageBL::getByCode('ORDER_CONFIRMATION_MAIL_SUBJECT', $idLanguage);
        $oggettoMail = '';

        //mi memorizzo la testata ordine
        $salesOrder = SalesOrder::where('id', $idSalesOrder)->first();

        //mi memorizzo l'html della singola riga ordine di vendita
        $contentSingleRowSalesOrder = ContentBL::getByCode('Sales_Order_Detail_Single_Row', $idLanguage);
        $contentSingleRowSalesOrderTokenUrl = ContentBL::getByCode('SalesOrderDetailTokenUrl');
        //mi memorizzo la mail dell'utente che ha fatto l'ordine
        $mailUser = User::where('id', $salesOrder->user_id)->first()->email;

 

        /*****************************************************************************************/
        //verifico se ho dei tag nell'oggetto e li vado a sostituire con la view della testata ordine
        /*****************************************************************************************/
        $tags = SelenaViewsBL::getTagByType('sales_orders');      
        $tagsDetail = SelenaViewsBL::getTagByType('sales_orders_details');    
        $tagsDetailLinkUser = SelenaViewsBL::getTagByType('sales_order_tokenlink_user'); 
        $oggettoMail = $oggettoMailConTag;
        $testoMail = $testoMailConTag;

        foreach (DB::select(
            'SELECT *
            FROM sales_orders 
            WHERE id = :_id'
            , ['_id' => $idSalesOrder] 
        ) as $salesOrdersummary){            
            foreach ($tags as $fieldsDb){
                if(isset($fieldsDb->tag)){   
                    //qua controllo l'oggetto della mail                                                              
                    if (strpos($oggettoMailConTag, $fieldsDb->tag) !== false) {
                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                        if(is_numeric($salesOrdersummary->{$cleanTag})){
                            if ($cleanTag=="id")
                                $tag = (int)$salesOrdersummary->{$cleanTag};
                            else
                                $tag = number_format($salesOrdersummary->{$cleanTag}, 2); 
                        }else{
                            $tag = $salesOrdersummary->{$cleanTag};
                        }
                        $oggettoMail = str_replace($fieldsDb->tag , $tag,  $oggettoMail);                                                                               
                    }
                    //qua controllo il testo della mail
                    if (strpos($testoMailConTag, $fieldsDb->tag) !== false) {
                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                        
                        if(is_numeric($salesOrdersummary->{$cleanTag})){
                            if ($cleanTag=="id")
                                $tag = (int)$salesOrdersummary->{$cleanTag};
                            else
                                $tag = number_format($salesOrdersummary->{$cleanTag}, 2); 
                        }else{
                            $tag = $salesOrdersummary->{$cleanTag};
                        }
                        
                        $testoMail = str_replace($fieldsDb->tag , $tag,  $testoMail);                                                                               
                    }
                }
            }
              if (strpos($testoMail, '#date_order_formatter#') !== false) {
                   $dateorderformatter = SalesOrder::where('id', $salesOrder->id)->first()->date_order;   
                   $dateorderformatters = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $dateorderformatter)->format('d/m/Y H:i:s');
                    //Carbon::parse($dateorderformatter)->format('d/m/Y H:i:s');         
                    if (isset($dateorderformatters)){     
                    $testoMail = str_replace('#date_order_formatter#',$dateorderformatters,  $testoMail); 
                    }else{
                    $testoMail = str_replace('#date_order_formatter#', '',  $testoMail);           
                    }
                }

                /**/ 
        //prelevo i dati anagrafici dell utente
        $user = User::where('id', $salesOrder->user_id)->first();
        $usersdatas = UsersDatas::where('user_id', $salesOrder->user_id)->first();

        if (isset($usersdatas->name)) {  
            $name = $usersdatas->name;
        }else{
            $name = '';  
        }
        if (isset($usersdatas->surname)) {  
            $surname = $usersdatas->surname;
        }else{
            $surname = '';  
        }
        if (isset($usersdatas->business_name)) {  
            $business_name = $usersdatas->business_name;
        }else{
            $business_name = '';  
        }
         if (isset($usersdatas->address)) {  
            $address = $usersdatas->address;
        }else{
            $address = '';  
        }
        if (isset($usersdatas->postal_code)) {  
            $postal_code = $usersdatas->postal_code;
        }else{
            $postal_code = '';  
        }
        if (isset($usersdatas->country)) {  
            $country = $usersdatas->country;
        }else{
            $country = '';  
        }
        if (isset($usersdatas->province_id)) {  
            $province = LanguageProvince::where('province_id', $usersdatas->province_id)->first()->description;                 
        }else{
            $province = '';  
        }
        if (isset($usersdatas->telephone_number)) {  
            $telephone_number = $usersdatas->telephone_number;
        }else{
            $telephone_number = '';  
        }
        if (isset($user->email)) {  
            $email = $user->email;
        }else{
            $email = '';  
        }
        if (isset($usersdatas->vat_number)) {  
            $vat_number = $usersdatas->vat_number;
        }else{
            $vat_number = '';  
        }
        if (isset($usersdatas->fiscal_code)) {  
            $fiscal_code = $usersdatas->fiscal_code;
        }else{
            $fiscal_code = '';  
        }
                    $testoMail =str_replace ('#name#', $name, $testoMail);
                    $testoMail =str_replace ('#surname#', $surname, $testoMail);
                    $testoMail =str_replace ('#business_name#', $business_name, $testoMail);
                    $testoMail =str_replace ('#address#', $address, $testoMail);
                    $testoMail =str_replace ('#postal_code#', $postal_code, $testoMail);
                    $testoMail =str_replace ('#country#', $country, $testoMail);
                    $testoMail =str_replace ('#province#', $province, $testoMail);
                    $testoMail =str_replace ('#telephone_number#', $telephone_number, $testoMail);
                    $testoMail =str_replace ('#fiscal_code#', $fiscal_code, $testoMail);
                    $testoMail =str_replace ('#vat_number#', $vat_number, $testoMail);
                    $testoMail =str_replace ('#email#', $email, $testoMail);   
               /* */



        };

        /*****************************************************************************************/
        //ora verifico se ho il tag #sales_order_detail_single_row# e lo elaboro prendendo tutte le righe
        /*****************************************************************************************/
        if (strpos($testoMail, '#sales_order_detail_single_row#') !== false) {   
            $finalHtml =  '';
            foreach (DB::select(
                'SELECT *
                FROM sales_orders_details
                WHERE order_id = :_id'
                , ['_id' => $idSalesOrder]              
            ) as $salesOrderDetailsummary){  
                $singleRow = $contentSingleRowSalesOrder->content;  
                  foreach ($tagsDetail as $fieldsDb){  
                        if(isset($fieldsDb->tag)){                                                                 
                            if (strpos($singleRow, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);

                                if(is_numeric($salesOrderDetailsummary->{$cleanTag}) && strpos($salesOrderDetailsummary->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($salesOrderDetailsummary->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $salesOrderDetailsummary->{$cleanTag};
                                }
                                $singleRow = str_replace($fieldsDb->tag , $tag,  $singleRow);                                                                               
                            }
                        }
                    }          
                    if (strpos($singleRow, '#htmlSalesOrdertokenLinkUser#') !== false) {
                      $salesOrderTokenLinkUser = SalesOrderTokenLinkUser::where('sales_order_detail_id', $salesOrderDetailsummary->id)->get()->first(); 
                        if (isset($salesOrderTokenLinkUser)){
                        $UrlHtml = '<a href="https://cameranation.it/download?User='.$salesOrderTokenLinkUser->id_user.'&Token='.$salesOrderTokenLinkUser->token_link.'&Items='.$salesOrderDetailsummary->item_id.'&Order='.$salesOrderTokenLinkUser->sales_order_id.'">Clicca qui</a>';
                        $singleRow = str_replace('#htmlSalesOrdertokenLinkUser#', $UrlHtml, $singleRow); 
                        }else{
                            $singleRow = str_replace('#htmlSalesOrdertokenLinkUser#', '',  $singleRow);           
                        }
                    }

                    if (strpos($singleRow, '#code_items#') !== false) {
                        $DescriptionItems = Item::where('id', $salesOrderDetailsummary->item_id)->first();
                        if (isset($DescriptionItems)){
                        $singleRow = str_replace('#code_items#', $DescriptionItems->internal_code, $singleRow); 
                        }else{
                            $singleRow = str_replace('#code_items#', '',  $singleRow);           
                        }
                    }

                  if (strpos($singleRow, '#support_id_description#') !== false) {
                        $DescriptionSupport = Supports::where('id', $salesOrderDetailsummary->support_id)->first();
                        if (isset($DescriptionSupport)){
                        $singleRow = str_replace('#support_id_description#', $DescriptionSupport->description, $singleRow); 
                        }else{
                            $singleRow = str_replace('#support_id_description#', '',  $singleRow);           
                        }
                    }
                    if (strpos($singleRow, '#sales_detail_optional#') !== false) {
                        $singleRow = static::_salesDetailOptional($salesOrderDetailsummary->id, 'SalesDetailOptional', '#sales_detail_optional#', $singleRow);  
                    }

                $finalHtml = $finalHtml . $singleRow;
            };

            //sostituisco anche l'eventuale dettaglio dell'ordine
            $testoMail = str_replace('#sales_order_detail_single_row#' , $finalHtml,  $testoMail);   
        }

        Mail::to($mailUser)->send(new DynamicMail($oggettoMail, $testoMail));

        $mailAzi = SettingBL::getByCode('EmailOrder');

        if($mailAzi != null && $mailAzi!= ''){
            Mail::to($mailAzi)->send(new DynamicMail($oggettoMail, $testoMail));
        }    
    }

    public function payment_error()
    {
        return 'User is canceled the payment.';
    }

    
      private static function _salesDetailOptional(int $SalesDetailId,string $codeContent, string $elementReplaced, string $contentPassed)
    {
        $SalesDetailOptionals = SalesOrderDetailOptional::where('sales_order_detail_id', $SalesDetailId);
        if(isset($SalesDetailOptionals)){
            $idContent = Content::where('code', $codeContent)->first();
            if(isset($idContent)){
                $htmlContent = ContentLanguage::where('content_id', $idContent->id)->first()->content;
                if(isset($htmlContent)){
                    $finalHtml = '';
                    $newContent  = '';
                    foreach (DB::select(
                        'SELECT *
                        FROM sales_orders_detail_optional
                        WHERE sales_orders_detail_optional.sales_order_detail_id = :SalesDetailId',
                        ['SalesDetailId' => $SalesDetailId]
                    ) as $SalesDetailOptional){
                        $newContent = $htmlContent;
                        
                        if (strpos($newContent, '#description_id_optionalPrint#') !== false) {
                            $Description_optionalPrint = OptionalLanguages::where('optional_id', $SalesDetailOptional->optional_id)->get()->first();
                            if (isset($Description_optionalPrint)){
                            $newContent = str_replace('#description_id_optionalPrint#', $Description_optionalPrint->description,  $newContent); 
                            }else{
                                $newContent = str_replace('#description_id_optionalPrint#', '',  $newContent);           
                            }
                        }

                    if (strpos($newContent, '#title_tipology_optional#') !== false) {
                            $Title_Tipology_Optional = OptionalTypology::where('optional_id', $SalesDetailOptional->optional_id)->get()->first();
                            $Title_Tipology_OptionalDescription = TypeOptionalLanguages::where('type_optional_id', $Title_Tipology_Optional->type_optional_id)->get()->first();
                            if (isset($Title_Tipology_OptionalDescription)){
                            $newContent = str_replace('#title_tipology_optional#', $Title_Tipology_OptionalDescription->description,  $newContent); 
                            }else{
                                $newContent = str_replace('#title_tipology_optional#', '',  $newContent);           
                            }
                        }
                         
                      $finalHtml = $finalHtml . $newContent;
                    }

                }
                return str_replace($elementReplaced , $finalHtml,  $contentPassed); 
            }else{
                return "";
            }  
        }else{
            return "";
        }
    }


 
}
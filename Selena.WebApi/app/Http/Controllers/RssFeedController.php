<?php

namespace App\Http\Controllers;

use App\Language;
use App\LanguagePage;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoBlogNewsData;
use App\User;
use App\Helpers\LogHelper;
use Illuminate\Routing\Controller;
use App\BusinessLogic\LanguagePageBL;
use App\BusinessLogic\LanguageBL;
use App\BusinessLogic\LinkPagesCategoryBL;
use App\BusinessLogic\PageBL;
use App\BusinessLogic\UserBL;
use App\DtoModel\DtoPageRender;
use App\Helpers\ArrayHelper;
use App\Helpers\ModelHelper;

use Illuminate\Http\Request;

class RssFeedController extends Controller
{

    /**
     * Return dtoPage to render
     *
     * @param pageRow
     * @param string category
     * @param int page
     * @return DtoPageRender
     */
    private static function _convertToDtoUser($pageRow, string $category = null, int $page = 1, $idUser , $idCart, $idLanguage, $url)
    {

        $dtoPageRender = new DtoPageRender;

        if (count($pageRow) > 0) {
            $pageArray = ArrayHelper::toCollection($pageRow)[0];
            
            $dtoPageRender->content = $pageArray->content;

            if (isset($pageArray->hide_breadcrumb)) {
                $dtoPageRender->hideBreadcrumb = $pageArray->hide_breadcrumb;
            } else {
                $dtoPageRender->hideBreadcrumb = true;
            }

            if (isset($pageArray->hide_footer)) {
                $dtoPageRender->hideFooter = $pageArray->hide_footer;
            } else {
                $dtoPageRender->hideFooter = true;
            }

            if (isset($pageArray->hide_header)) {
                $dtoPageRender->hideHeader = $pageArray->hide_header;
            } else {
                $dtoPageRender->hideHeader = true;
            }

            if (isset($pageArray->share_title)) {
                $dtoPageRender->shareTitle = $pageArray->share_title;
            }

            if (isset($pageArray->share_description)) {
                $dtoPageRender->shareDescription = $pageArray->share_description;
            }

            if (isset($pageArray->url_cover_image)) {
                $dtoPageRender->urlCoverImage = $pageArray->url_cover_image;
            }

            if (isset($pageArray->visible_from)) {
                $dtoPageRender->visibleFrom = $pageArray->visible_from;
            }

            if (isset($pageArray->visible_end)) {
                $dtoPageRender->visibleEnd = $pageArray->visible_end;
            }

            if (isset($pageArray->hide_search_engine)) {
                $dtoPageRender->hideSearchEngine = $pageArray->hide_search_engine;
            } else {
                $dtoPageRender->hideSearchEngine = true;
            }

            if (isset($pageArray->seo_description)) {
                $dtoPageRender->seoDescription = $pageArray->seo_description;
            }

            if (isset($pageArray->seo_keyword)) {
                $dtoPageRender->seoKeyword = $pageArray->seo_keyword;
            }

            if (isset($pageArray->seo_title)) {
                $dtoPageRender->seoTitle = $pageArray->seo_title;
            }

            if (isset($pageArray->title)) {
                $dtoPageRender->title = $pageArray->title;
            }

            if (isset($pageArray->js)) {
                $dtoPageRender->js = $pageArray->js;
            }

            if (isset($pageArray->enable_js)) {
                $dtoPageRender->enableJS = $pageArray->enable_js;
            } else {
                $dtoPageRender->enableJS = false;
            }

            if (isset($pageArray->protected)) {
                $dtoPageRender->protected = $pageArray->protected;
            } else {
                $dtoPageRender->protected = false;
            }
            if (isset($pageArray->rss)) {
                $dtoPageRender->rss = $pageArray->rss;
            } else {
                $dtoPageRender->rss = false;
            }
        }

        return $dtoPageRender;
    }


    public function feed(Request $request){
        /****************************************************************************/
        //caso 1 - mi servono i feed di tutto il sito
        /****************************************************************************/
        if ($request->url=='/feed' or $request->url=='/rss'){
            $posts = RssFeedController::getFeedRSS(-1,-1);
        
        /****************************************************************************/
        //caso 2 - feed di una sola categoria    
        /****************************************************************************/
        }elseif (strpos($request->url, '/category/')!==false or strpos($request->url, '/categoria/')!==false){
            if (str_ends_with($request->url,'/feed')) 
                $idPageCategory = LinkPagesCategoryBL::getPageCategoryIDByUrl(substr($request->url, 0, -5));
            if (str_ends_with($request->url,'/rss')) 
                $idPageCategory = LinkPagesCategoryBL::getPageCategoryIDByUrl(substr($request->url, 0, -4));
              
            $posts = RssFeedController::getFeedRSS($idPageCategory,-1);
        /****************************************************************************/
        //caso 3 - feed specifico di 1 articolo    
        /****************************************************************************/
        }else{
            if (str_ends_with($request->url,'/feed')) 
                $idPage = PageBL::getPageIDByUrl(substr($request->url, 0, -5));
            if (str_ends_with($request->url,'/rss')) 
                $idPage = PageBL::getPageIDByUrl(substr($request->url, 0, -4));
            $posts = RssFeedController::getFeedRSS(-1,$idPage);
        }
        //in futuro - caso 4 - feed autore

        $html = '<?xml version="1.0" encoding="UTF-8"?>';
        $html = $html .'<rss version="2.0" ';
        //$html = $html .'xmlns:content="http://purl.org/rss/1.0/modules/content/" ';
        //$html = $html .'xmlns:media="http://search.yahoo.com/mrss/">';
        $html = $html .'xmlns:content="http://purl.org/rss/1.0/modules/content/" ';
        $html = $html .'xmlns:wfw="http://wellformedweb.org/CommentAPI/" ';
        $html = $html .'xmlns:dc="http://purl.org/dc/elements/1.1/" ';
        $html = $html .'xmlns:atom="http://www.w3.org/2005/Atom" ';
        $html = $html .'xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"  ';
        $html = $html .'xmlns:slash="http://purl.org/rss/1.0/modules/slash/">';
        $html = $html .'<channel>';
        //<lastBuildDate>Fri, 23 Jan 2015 23:26:19 +0000</lastBuildDate>
        $html = $html .'<title>Camera Nation</title>';
        $html = $html .'<link>https://cameranation.it</link>';
        $html = $html .'<description>Il portale della fotografia nato per condividere la passione per questa meravigliosa arte. </description>';
      
        foreach ($posts as $post) {
            $html = $html.'<item>';
            $html = $html.'<title><![CDATA['.$post->title.']]></title>';
            $html = $html.'<link>https://cameranation.it'.$post->url.'</link>';
            $html = $html.'<description><![CDATA['.$post->text.']]></description>';
            $html = $html.'<author><![CDATA[info@cameranation.it (Camera Nation)]]></author>';
            $html = $html.'<guid>https://cameranation.it'.$post->url.'</guid>';
            $html = $html.'<pubDate>'.$post->dateString.'</pubDate>';
            $html = $html.'<enclosure url="'.$post->urlCoverImage.'" length="1000" type="image/jpeg"></enclosure>';
            $html = $html.'<content:encoded><![CDATA['.$post->content.']]></content:encoded>';
            $html = $html.'</item>';
        }

        $html = $html .'</channel>';
        $html = $html .'</rss>';

        $pageToRender = collect();
        $dtoPageRender = new DtoPageRender();
        $dtoPageRender->content = $html;
        $dtoPageRender->hide_footer = true;
        $dtoPageRender->hide_header = true;
        $dtoPageRender->hideSearchEngine = true;
        $dtoPageRender->title = '';
        $dtoPageRender->share_title = '';
        $dtoPageRender->share_description = '';
        $dtoPageRender->seo_title = '';
        $dtoPageRender->seo_description = '';
        $dtoPageRender->seo_keyword = '';
        $dtoPageRender->url_cover_image = '';
        $dtoPageRender->availability = '';
        $dtoPageRender->rss = true;
        $pageToRender->push($dtoPageRender);
     
        return response()->success(static::_convertToDtoUser($pageToRender, substr(strrchr($request->url, "/"), 1), 1, $request->idUser , $request->idCart, $request->idLanguage, $request->url)->toArray());
        
    }

    public function feedRss(Request $requestUrl){

        $request = $requestUrl->query();
        $idPageCategory = -1;
        $idPage = -1;
        if ($request['url']=='/feed' or $request['url']=='/rss'){
            $posts = RssFeedController::getFeedRSS(-1,-1);
        /****************************************************************************/
        //caso 2 - feed di una sola categoria    
        /****************************************************************************/
        }elseif (strpos($request['url'], '/category/')!==false or strpos($request['url'], '/categoria/')!==false){
            if (str_ends_with($request['url'],'/feed')) 
                $idPageCategory = LinkPagesCategoryBL::getPageCategoryIDByUrl(substr($request['url'], 0, -5));
            if (str_ends_with($request['url'],'/rss')) 
                $idPageCategory = LinkPagesCategoryBL::getPageCategoryIDByUrl(substr($request['url'], 0, -4));
            
            $posts = RssFeedController::getFeedRSS($idPageCategory,-1);
        /****************************************************************************/
        //caso 3 - feed specifico di 1 articolo    
        /****************************************************************************/
        }else{
            if (str_ends_with($request['url'],'/feed')) 
                $idPage = PageBL::getPageIDByUrl(substr($request['url'], 0, -5));
            if (str_ends_with($request['url'],'/rss')) 
                $idPage = PageBL::getPageIDByUrl(substr($request['url'], 0, -4));
            $posts = RssFeedController::getFeedRSS(-1,$idPage);
        }
        
        //$postsNew = LanguagePage::get();

        return response()->view('rss.feed', compact('posts'))->header('Content-Type', 'application/xml');
        
        
    }

    public static function getFeedRSS(int $idCategory , int $idPage )
    {
        $result = collect();

        $idLanguage = LanguageBL::getDefault()->id; 
        if ($idCategory >= 0){
           $query = " SELECT * 
            from pages_pages_category 
            inner join pages on pages.id = pages_pages_category.page_id 
            INNER JOIN languages_pages ON pages.id = languages_pages.page_id
            where pages.online = true
           AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() )
           AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() )
           and publish is not NULL
           AND pages.page_type = 'Blog'
           AND languages_pages.language_id = " . $idLanguage;
           $query = $query . " AND pages_pages_category.page_category_id = " .$idCategory;
           
        }elseif ($idPage >= 0){
            $query = "  SELECT *
            FROM pages
            INNER JOIN languages_pages ON pages.id = languages_pages.page_id
            WHERE pages.online = true
            AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() )
            AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() )
            AND languages_pages.content IS NOT NULL
            and publish is not NULL
            AND pages.page_type = 'Blog'
            AND languages_pages.language_id = " . $idLanguage;
            $query = $query . " AND pages.id = " .$idPage;
        }else{
            $query = "  SELECT *
            FROM pages
            INNER JOIN languages_pages ON pages.id = languages_pages.page_id
            WHERE pages.online = true
            AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() )
            AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() )
            AND languages_pages.content IS NOT NULL
            and publish is not NULL
            AND pages.page_type = 'Blog'
            AND languages_pages.language_id = " . $idLanguage;
        }
        
        $query = $query . " ORDER BY pages.publish DESC LIMIT 50";
        foreach (DB::select($query) as $blogNews) {
            $blogNewsData = new DtoBlogNewsData();
            if (is_null($blogNews->seo_title)) {
                $blogNewsData->title = $blogNews->title;
            } else {
                $blogNewsData->title = $blogNews->seo_title;
            }
            $blogNewsData->urlCoverImage = $blogNews->url_cover_image;
            $blogNewsData->url = $blogNews->url;
            $blogNewsData->content = $blogNews->content;
            $blogNewsData->text = $blogNews->seo_description;            
            $blogNewsData->dateString = date("D, d M Y H:i:s +0000", strtotime($blogNews->publish));
            $blogNewsData->dateStringLastBuildUpdate = date("D, d M Y H:i:s +0000", strtotime(date("Y-m-d H:i:s")));
            $blogNewsData->date = $blogNews->publish;

            if (!is_null($blogNews->author_id)) {
                $blogNewsData->author = UserBL::getById($blogNews->author_id)->name;
            }
            $result->push($blogNewsData);
        }

        return $result;
    }



}
<?php

namespace App\Http\Controllers\Web;

use App\BusinessLogic\ContentBL;
use App\BusinessLogic\ContentLanguageBL;
use App\BusinessLogic\LanguageBL;
use App\BusinessLogic\SettingBL;
use App\BusinessLogic\PageBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

use App\Content;



class ContentController
{
    public function getContentHome(string $slug = '')
    {

    }

    
    public function getContent(string $slug = '')
    {
        $userId = 1; 
        $cartId = '';
        $url = '/' . $slug;
        $salesmanId = '';

        if($_SERVER['QUERY_STRING'] != ''){
            parse_str($_SERVER['QUERY_STRING'], $paramUrl);
        }else{
            $paramUrl = '';
        }
        $titolo = '';
        $analytics = '';
        $appIdFacebook = '';
        $appIdGoogle = '';
        $stringAnalitycs = '';
        
        if (empty($_COOKIE['sIdLanguage'])) {
            $idLanguage = LanguageBL::getDefault()->id;
        }else{
            $idLanguage =  $_COOKIE['sIdLanguage'];
        }

        if (empty($_COOKIE['sUserId'])) {
            $userId = null;
        }else{
            $userId =  $_COOKIE['sUserId'];
        }

        if (empty($_COOKIE['sCartId'])) {
            $cartId = null;
        }else{
            $cartId =  $_COOKIE['sCartId'];
        }

        if (empty($_COOKIE['sSalesmanId'])) {
            $salesmanId = null;
        }else{
            $salesmanId =  $_COOKIE['sSalesmanId'];
        }

        
        
        // HTML
        $dtoHtml = PageBL::getRenderUser(
            $url, 
            $paramUrl,
            $userId,
            $cartId, 
            $idLanguage, 
            $salesmanId);
       
        // elementi seo
        $titolo = $dtoHtml->title;
        $shareTitle = $dtoHtml->shareTitle;
        $shareDescription = $dtoHtml->shareDescription;
        $shareKeywords = $dtoHtml->seoKeyword;
        $shareUrlCoverImage = $dtoHtml->urlCoverImage;

        $seoTitle = $dtoHtml->seoTitle;
        $seoDescription = $dtoHtml->seoDescription;
        $seoKeyword = $dtoHtml->seoKeyword;
        $hideSearchEngine = $dtoHtml->hideSearchEngine;

        //elementi pagina
        $body = $dtoHtml->content;
        $hideHeader = $dtoHtml->hideHeader;
        $hideFooter = $dtoHtml->hideFooter;
        $redirect = $dtoHtml->redirect;
        $protected = $dtoHtml->protected;


        $enableJS = $dtoHtml->enableJS;
        $js = '';
        if($enableJS){
            $js = $dtoHtml->js;
        }
        
        if($hideSearchEngine){
            $hideSearchEngine = 'noindex, nofollow';
        }else{
            $hideSearchEngine = 'index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1';
        }
        // HEADER
        if($hideHeader){
            $header = '';
        }else{
            $content =  Content::where('code', 'Header')->first();
            $header = ContentLanguageBL::getByCodeUserAndLanguage(
                $content->id, 
                $idLanguage, 
                $userId, 
                $cartId, 
                $url, 
                $salesmanId);
        }

        // FOOTER
        if($hideHeader){
            $footer = '';
        }else{
            $content =  Content::where('code', 'Footer')->first();
            $footer = ContentLanguageBL::getByCodeUserAndLanguage(
                $content->id, 
                $idLanguage, 
                $userId, 
                $cartId, 
                $url, 
                $salesmanId);
        }

        $analytics = SettingBL::getByCode('GoogleAnalyticsTrackId');
        $appIdFacebook = SettingBL::getByCode('AppIdFacebook');
        $appIdGoogle = SettingBL::getByCode('AppIdGoogle');

        $stringAnalitycs = "window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', '" . $analytics . "');";
            
        return view('web.test', compact(
            'header', 
            'footer', 
            'body', 
            'titolo', 
            'seoTitle', 
            'seoDescription', 
            'seoKeyword', 
            'shareTitle', 
            'shareDescription', 
            'shareKeywords', 
            'shareUrlCoverImage', 
            'hideSearchEngine', 
            'redirect', 
            'protected', 
            'js',
            'analytics',
            'stringAnalitycs',
            'appIdFacebook',
            'appIdGoogle'
        ));
    }

   
}

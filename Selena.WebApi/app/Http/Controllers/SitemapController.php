<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BusinessLogic\PageBL;
use App\BusinessLogic\ItemBL;
use App\BusinessLogic\CategoriesBL;
use App\BusinessLogic\LinkPagesCategoryBL;
use App\BusinessLogic\UserBL;
use App\BusinessLogic\CompareItemBL;
use App\BusinessLogic\CommunityImageBL;
use App\BusinessLogic\ListKeywordsBL;
use App\Page;
use App\CompareItem;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;

class SitemapController extends Controller
{

    public function index()
    {

      $news = PageBL::getUrl(1); 
      $blog = PageBL::getUrl(1);
      $pages = PageBL::getUrl(1);
      $items = ItemBL::getUrlSitemap(1);
      $pricesItems = ItemBL::getUrlSitemapPrices(1);
      $photoItems = ItemBL::getUrlSitemapPhoto(1);
      $categories = CategoriesBL::getUrlSitemap(1);
      $linkcategories = LinkPagesCategoryBL::getUrlSitemap(1);
      $users = UserBL::getUrlSitemap(1);

      $totItems = DB::select('SELECT count(item_compare.id) as cont from item_compare');
      $maxPage = 1;
      if($totItems[0]->cont >= 50000){
        $maxPage = ceil($totItems[0]->cont / 50000);
      }else{
        $maxPage = 1;
      }

      $totItemsCommunityImage = DB::select('SELECT count(community_images.id) AS cont FROM community_images');
      $maxPageCommunityImage = 1;
      if($totItemsCommunityImage[0]->cont >= 50000){
        $maxPageCommunityImage = ceil($totItemsCommunityImage[0]->cont / 50000);
      }else{
        $maxPageCommunityImage = 1;
      }
      

        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        /* controllo recordset */
        $showNews = false;
        $newsCheck = PageBL::getPages("News",1);
        if(count($newsCheck) > 0){
            $showNews = true;
        }

        $showBlog = false;
        $blogCheck = PageBL::getPages("Blog",1);
        if(count($blogCheck) > 0){
            $showBlog = true;
        }

        $showPages = false;
        $pagesCheck = PageBL::getPages("Page",1);
        if(count($pagesCheck) > 0){
            $showPages = true;
        }
    
        $showItems = false;
        $itemsCheck = ItemBL::getPages("Page",1);
        if(count($itemsCheck) > 0){
            $showItems = true;
        }

        $showPricesItems = false;
        $pricesItemsCheck = ItemBL::getPagesPrices(1);
        if(count($pricesItemsCheck) > 0){
            $showPricesItems = true;
        }

        $showPhotoItems = false;
        $photoItemsCheck = ItemBL::getPagesPhoto(1);
        if(count($photoItemsCheck) > 0){
            $showPhotoItems = true;
        }

        $showCategories = false;
        $categoriesCheck = CategoriesBL::getPages("Page",1);
        if(count($categoriesCheck) > 0){
            $showCategories = true;
        }
        
        $showLinkcategories= false;
        $linkcategoriesCheck = LinkPagesCategoryBL::getPages("Page",1);
        if(count($linkcategoriesCheck) > 0){
            $showLinkcategories = true;
        }

        $showUser= false;
        $userCheck = UserBL::getPages("Page",1);
        if(count($userCheck) > 0){
            $showUser = true;
        }

       /* $showMaxPage= false;
        $maxPageCheck =CompareItemBL::getPages("Page", 1, $page);
        if(count($maxPageCheck) > 0){
            $showMaxPage = true;
        }*/

        $showKeywords= false;
        $keywordsCheck = ListKeywordsBL::getPages("Page",1);
        if(count($keywordsCheck) > 0){
            $showKeywords = true;
        }
        
    
      return response()->view('sitemap.index', [
          'news' => $news,
          'showNews' => $showNews,

          'blog' => $blog,
          'showBlog' => $showBlog,

          'pages' => $pages,
          'showPages' => $showPages,

          'items' => $items,
          'showItems' => $showItems,

          'pricesItems' >= $pricesItems,
          'showPricesItems' => $showPricesItems,

          'photoItems' >= $photoItems,
          'showPhotoItems' => $showPhotoItems,

          'categories' => $categories,
          'showCategories' => $showCategories,

          'linkcategories' => $linkcategories,
          'showLinkcategories' => $showLinkcategories,

          'users' => $users,
          'showUser' => $showUser,

          'showKeywords' => $showKeywords,

          'pageCompareItem' => $maxPage,
          //'showMaxPage' => $showMaxPage,


          'communityImage' => $maxPageCommunityImage,


          'host' => $protocol . $_SERVER["HTTP_HOST"] . '/api/storage'
      ])->header('Content-Type', 'text/xml');

    }

    public function news()
    {
        $news = PageBL::getPages("News",1);

        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return response()->view('sitemap.news', [
            'news' => $news,
            'host' => $protocol . $_SERVER["HTTP_HOST"]
        ])->header('Content-Type', 'text/xml');
        
    }

    public function blog()
    {
        $blog = PageBL::getPages("Blog",1);

        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return response()->view('sitemap.blog', [
            'blog' => $blog,
            'host' => $protocol . $_SERVER["HTTP_HOST"]
        ])->header('Content-Type', 'text/xml');
    }

    public function pages()
    {
        $pages = PageBL::getPages("Page",1);

        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return response()->view('sitemap.pages', [
            'pages' => $pages,
            'host' => $protocol . $_SERVER["HTTP_HOST"]
        ])->header('Content-Type', 'text/xml');
    }

    public function items()
    {
        $items = ItemBL::getPages("Page",1);

        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return response()->view('sitemap.items', [
            'items' => $items,
            'host' => $protocol . $_SERVER["HTTP_HOST"]
        ])->header('Content-Type', 'text/xml');
    }

    public function pricesItems()
    {
        $pricesItems = ItemBL::getPagesPrices(1);

        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return response()->view('sitemap.pricesItems', [
            'pricesItems' => $pricesItems,
            'host' => $protocol . $_SERVER["HTTP_HOST"]
        ])->header('Content-Type', 'text/xml');
    }
    public function photoItems()
    {
        $photoItems = ItemBL::getPagesPhoto(1);

        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return response()->view('sitemap.photoItems', [
            'photoItems' => $photoItems,
            'host' => $protocol . $_SERVER["HTTP_HOST"]
        ])->header('Content-Type', 'text/xml');
    }

    public function categories()
    {
        $categories = CategoriesBL::getPages("Page",1);

        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return response()->view('sitemap.categories', [
            'categories' => $categories,
            'host' => $protocol . $_SERVER["HTTP_HOST"]
        ])->header('Content-Type', 'text/xml');
    }

    public function linkcategories()
    {
        $linkcategories = LinkPagesCategoryBL::getPages("Page",1);

        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return response()->view('sitemap.linkcategories', [
            'linkcategories' => $linkcategories,
            'host' => $protocol . $_SERVER["HTTP_HOST"]
        ])->header('Content-Type', 'text/xml');
    }

    public function users()
    {
        $users = UserBL::getPages("Page",1);

        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return response()->view('sitemap.users', [
            'users' => $users,
            'host' => $protocol . $_SERVER["HTTP_HOST"]
        ])->header('Content-Type', 'text/xml');
    }

    public function keywords()
    {
        $keywords = ListKeywordsBL::getPages("Page",1);

        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return response()->view('sitemap.keywords', [
            'keywords' => $keywords,
            'host' => $protocol . $_SERVER["HTTP_HOST"]
        ])->header('Content-Type', 'text/xml');
    }

    public function compareItem(int $page)
    {
        //funzione per restituzione articoli per pagina
        $compareItem = CompareItemBL::getPages("Page", 1, $page);

        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return response()->view('sitemap.compareitem', [
            'compareItem' => $compareItem,
            'host' => $protocol . $_SERVER["HTTP_HOST"]
        ])->header('Content-Type', 'text/xml');
    }

    public function communityImage(int $page)
    {
        //funzione per restituzione articoli per pagina
        $communityImage = CommunityImageBL::getPages("Page", 1, $page);

        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return response()->view('sitemap.communityimage', [
            'communityImage' => $communityImage,
            'host' => $protocol . $_SERVER["HTTP_HOST"]
        ])->header('Content-Type', 'text/xml');
    }
}

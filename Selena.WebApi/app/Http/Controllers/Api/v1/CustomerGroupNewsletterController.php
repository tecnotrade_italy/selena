<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ContactGroupNewsletterBL;
use App\BusinessLogic\CustomerGroupNewsletterBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class CustomerGroupNewsletterController extends Controller
{
    #region GET

    /**
     * Get included by contact
     * 
     * @param int idContact
     *
     * @return Response
     */
    public function getIncludedByCustomer(int $idContact)
    {
        return response()->success(CustomerGroupNewsletterBL::getIncludedByCustomer($idContact));
    }

    /**
     * Get excluded by contact
     *
     * @param int idContact
     * 
     * @return Response
     */
    public function getExcludedByCustomer(int $idContact)
    {
        return response()->success(CustomerGroupNewsletterBL::getExcludedByCustomer($idContact));
    }


    /**
     * Get included by contact
     * 
     * @param int idContact
     *
     * @return Response
     */
    public function getIncludedByCustomerGroup(int $idContact)
    {
        return response()->success(CustomerGroupNewsletterBL::getIncludedByCustomerGroup($idContact));
    }

    /**
     * Get excluded by contact
     *
     * @param int idContact
     * 
     * @return Response
     */
    public function getExcludedByCustomerGroup(int $idContact)
    {
        return response()->success(CustomerGroupNewsletterBL::getExcludedByCustomerGroup($idContact));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @param Request dtoContact
     * 
     * @return Response
     */
    public function insert(Request $dtoContact)
    {
        return response()->success(CustomerGroupNewsletterBL::insert($dtoContact, HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoContact
     * 
     * @return Response
     */
    public function update(Request $dtoContact)
    {
        return response()->success(CustomerGroupNewsletterBL::update($dtoContact, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return Response
     */
    public function delete(int $id)
    {
        return response()->success(CustomerGroupNewsletterBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

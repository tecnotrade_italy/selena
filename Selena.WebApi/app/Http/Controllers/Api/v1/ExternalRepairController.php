<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ExternalRepairBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;

class ExternalRepairController
{
    #region GET

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllexternalRepair(int $idLanguage)
    {
        return response()->success(ExternalRepairBL::getAllexternalRepair($idLanguage));
    }

    #endregion GET

       /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllexternalRepairSearch(Request $request)
    {
        return response()->success(ExternalRepairBL::getAllexternalRepairSearch($request));
    }


  /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getexternalRepairSearchViewClose(Request $request)
    {
        return response()->success(ExternalRepairBL::getexternalRepairSearchViewClose($request));
    }

  /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getexternalRepairSearchViewOpen(Request $request)
    {
        return response()->success(ExternalRepairBL::getexternalRepairSearchViewOpen($request));
    }



    /**
     * Get by getByRepair
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByRepair(Int $id)
    {
        return response()->success(ExternalRepairBL::getByRepair($id));
    }


   /**
     * Get by getbyFaultModule
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getbyFaultModule(Int $id)
    {
        return response()->success(ExternalRepairBL::getbyFaultModule($id));
    }

 /**
     * updateExternalRepair
     *
     * @param Request DtoExternalRepair
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateRepairExternal(Request $DtoExternalRepair)
    {
        return response()->success(ExternalRepairBL::updateRepairExternal($DtoExternalRepair, HttpHelper::getLanguageId()));
    }

 #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(ExternalRepairBL::delete($id, HttpHelper::getLanguageId()));
    }
    #endregion DELETE


}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\CarriageBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class CarriageController
{
    #region GET

    /**
     * Get by customer
     *
     * @param int idCustomer
     * @param int idFunctionality
     * @return \Illuminate\Http\Response
     */
    public function getByCustomer(int $idCustomer, int $idFunctionality)
    {
        return response()->success(CarriageBL::getByCustomer($idCustomer, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(CarriageBL::getSelect($search));
        //return response()->getSelect(CarriageBL::getSelect($search, $request->idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion GET

/**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(CarriageBL::getById($id));
    }


    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(CarriageBL::getAll($idLanguage));
    }
    #endregion GET
    
    #region INSERT

    /**
     * Insert
     *
     * @param Request DtoCarriage
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoCarriage)
    {
        return response()->success(CarriageBL::insert($DtoCarriage, HttpHelper::getUserId()));
       
    }
    #endregion INSERT

    #region UPDATE
    /**
     * Update
     *
     * @param Request DtoCarriage
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoCarriage)
    {
        return response()->success(CarriageBL::update($DtoCarriage));
    }
    #endregion UPDATE

     #region UPDATE
    /**
     * Update
     *
     * @param Request DtoCarriage
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateCheck(Request $DtoCarriage)
    {
        return response()->success(CarriageBL::updateCheck($DtoCarriage));
    }
    #endregion UPDATE

     /**
     * Get all
     * 
     * @param int idLanguage
     * @param int cartId
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllCarriageByCartId(int $idLanguage, int $cartId)
    {
        return response()->success(CarriageBL::getAllCarriageByCartId($idLanguage, $cartId));
    }
    
    #region DELETE
    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(CarriageBL::delete($id));
    }
}

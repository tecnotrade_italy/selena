<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\DocumentStatusesBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\SalesOrderStateLanguage;
use App\SalesOrderState;
use App\Helpers\LogHelper;


class DocumentStatusesController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(DocumentStatusesBL::getById($id));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(DocumentStatusesBL::getSelect($search));
    }

        /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(DocumentStatusesBL::getAll($idLanguage));
    }

    #endregion GET
    
    public function insert(Request $request)
    {
        return response()->success(DocumentStatusesBL::insert($request, HttpHelper::getLanguageId()));
    }
    
    public function update(Request $request)
    {
        return response()->success(DocumentStatusesBL::update($request, HttpHelper::getLanguageId()));
    }

    public function delete(int $id)
    {
        return response()->success(DocumentStatusesBL::delete($id, HttpHelper::getLanguageId()));
    }
   
}

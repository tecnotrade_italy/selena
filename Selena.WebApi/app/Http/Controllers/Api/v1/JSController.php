<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\JSBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class JSController
{
    #region GET

    /**
     * Get
     *
     * @param int idFunctionality
     * @return \Illuminate\Http\Response
     */
    public function get(int $idFunctionality)
    {
        return response()->success(JSBL::get($idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion GET

    #region POST

    /**
     * Update
     *
     * @param Request request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return response()->success(JSBL::update($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Update head
     *
     * @param Request request
     * @return \Illuminate\Http\Response
     */
    public function updateHead(Request $request)
    {
        return response()->success(JSBL::updateHead($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion POST
}

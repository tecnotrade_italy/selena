<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\PeopleBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class PeopleController
{
    #region GET

    /**
     * Get all People
     *
     * @param int $People
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        return response()->success(PeopleBL::getAllDto(HttpHelper::getLanguageId()));
    }

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(PeopleBL::getById($id));
    }

      /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
        public function getByIdReferent(Int $id)
    {
       return response()->success(PeopleBL::getByIdReferent($id)); 
    }
    
    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(PeopleBL::getSelect($search));
    }

    /**
     * Get getSelectExternalReferent
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelectExternalReferent(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(PeopleBL::getSelectExternalReferent($search));
    }

    


 /**
     * Get getPeopleUser
     *
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getPeopleUser(int $id)
    {
        return response()->success(PeopleBL::getPeopleUser($id));
    }

    
     /**
     * Get getPeopleUserWeb
     *
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getPeopleUserWeb(int $id)
    {
        return response()->success(PeopleBL::getPeopleUserWeb($id));
    }

 /**
     * Get all data
     *
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllReferent(int $id, int $idFunctionality)
    {
        return response()->success(PeopleBL::getAllReferent($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get People from user securpoint 
     *
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllUteByUserSecurPoint(int $id)
    {
        return response()->success(PeopleBL::getAllUteByUserSecurPoint($id));
    }


    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(PeopleBL::getAll($idLanguage));
    }

    #endregion GET

    #region INSERT
    
    /**
     * Insert
     *
     * @param Request DtoPeople
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoPeople)
    {
        return response()->success(PeopleBL::insert($DtoPeople, HttpHelper::getLanguageId()));
       
    }
/**
     * Insert
     *
     * @param Request DtoPeople
     * 
     * @return \Illuminate\Http\Response
     */
      public function insertNoAuth(Request $DtoPeople)
      {
       return response()->success(PeopleBL::insertNoAuth($DtoPeople, HttpHelper::getLanguageId()));
      }  
      
      /**
     * Insert
     *
     * @param Request DtoPeople
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertUteByUserSecurPoint(Request $DtoPeople)
    {
     return response()->success(PeopleBL::insertUteByUserSecurPoint($DtoPeople, HttpHelper::getLanguageId()));
    } 

      
/**
     * insertReferentWeb
     *
     * @param Request DtoPeople
     * 
     * @return \Illuminate\Http\Response
     */
      public function insertReferentWeb(Request $DtoPeople)
      {
       return response()->success(PeopleBL::insertReferentWeb($DtoPeople, HttpHelper::getLanguageId()));
      }  
 
    /**
     * Clone
     *
     * @param int $id
     * @param int $idFunctionality
     * 
     */
    
    public function clone(int $id, int $idFunctionality){
        return response()->success(PeopleBL::clone($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request DtoPeople
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateReferent(Request $DtoPeople)
    {
        return response()->success(PeopleBL::updateReferent($DtoPeople, HttpHelper::getLanguageId()));
 
    }
        
    #endregion UPDATE


   /**
     * updateReferentWeb
     *
     * @param Request DtoPeople
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateReferentWeb(Request $DtoPeople)
    {
        return response()->success(PeopleBL::updateReferentWeb($DtoPeople, HttpHelper::getLanguageId()));
     
    }
        
    #endregion updateReferentWeb


    /**
     * Update
     *
     * @param Request DtoPeople
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoPeople)
    {
        return response()->success(PeopleBL::update($DtoPeople, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(PeopleBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

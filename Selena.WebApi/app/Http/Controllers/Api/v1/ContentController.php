<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ContentBL;
use App\BusinessLogic\ContentLanguageBL;
use App\BusinessLogic\LanguageBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class ContentController
{
    #region GET

    /**
     * Get all
     * 
     * @param Int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idFunctionality)
    {
        return response()->success(ContentBL::getAll($idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get by language
     * 
     * @param Int idContent
     * @param Int idLanguage
     * @param Int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByLanguage(int $idContent, int $idLanguage, int $idFunctionality)
    {
        return response()->success(ContentLanguageBL::getByCodeAndLanguage($idContent, $idLanguage, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get by code
     * 
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByCodeRender(Request $request)
    {
        return response()->success(ContentBL::getByCodeRender($request));
    }

    /**
     * Get by code
     * 
     * @param String code
     * @param Int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByCode(string $code, int $idLanguage = null)
    {
        return response()->success(ContentBL::getByCode($code, $idLanguage));
    }

    /**
     * Get by code user
     * 
     * @param Request request
     * @param Int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByCodeUser(Request $request)
    {
        return response()->success(ContentBL::getByCodeUser($request));
    }

    /**
     * get Tag By Type
     * 
     * @param String code
     * 
     * @return \Illuminate\Http\Response
     */
    public function getTagByType(string $code)
    {
        return response()->success(ContentBL::getTagByType($code));
    }

    /**
     * get breadcrumbs
     * 
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getBreadcrumbs(Request $request)
    {
        return response()->success(ContentBL::getBreadcrumbs($request));
    }

    #endregion GET

    #region PUT

    /**
     * Insert
     * 
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(ContentBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion PUT

    #region POST

    /**
     * Update
     * 
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return response()->success(ContentBL::update($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion POST

    #region DELETE

    /**
     * Delete
     * 
     * @param Int id
     * @param Int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id, int $idFunctionality)
    {
        return response()->success(ContentBL::delete($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Delete language
     * 
     * @param Int idContent
     * @param Int idLanguage
     * @param Int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteLanguage(int $idContent, int $idLanguage, int $idFunctionality)
    {
        return response()->success(ContentLanguageBL::deleteLanguage($idContent, $idLanguage, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

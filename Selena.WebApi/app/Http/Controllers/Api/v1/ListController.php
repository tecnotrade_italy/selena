<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoList;
use App\BusinessLogic\ListBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\PriceList;


class ListController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(ListBL::getById($id));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(ListBL::getSelect($search));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(ListBL::getAll($idLanguage));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @param Request DtoUnitMeasure
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoList)
    {
        return response()->success(ListBL::insert($DtoList, HttpHelper::getUserId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request DtoUnitMeasure
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoList)
    {
        return response()->success(ListBL::update($DtoList));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(ListBL::delete($id));
    }

    #endregion DELETE
}

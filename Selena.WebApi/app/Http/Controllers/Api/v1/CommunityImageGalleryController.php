<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoCommunityImageGallery;
use App\BusinessLogic\CommunityImageGalleryBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\CommunityImageGallery;
use App\Helpers\LogHelper;


class CommunityImageGalleryController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(CommunityImageGalleryBL::getById($id));
    }


        /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(CommunityImageGalleryBL::getAll());
    }


    #endregion GET
    
    public function insert(Request $dtoCommunityImageGallery)
    {
        return response()->success(CommunityImageGalleryBL::insert($dtoCommunityImageGallery,HttpHelper::getUserId()));
    }
    
    public function update(Request $dtoCommunityImageGallery)
    {
        return response()->success(CommunityImageGalleryBL::update($dtoCommunityImageGallery));
    }

    public function delete(int $id)
    {
        return response()->success(CommunityImageGalleryBL::delete($id));
    }
   
}

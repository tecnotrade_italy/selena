<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\TypeOptionalBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class TypeOptionalController
{
    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(TypeOptionalBL::getAll($idLanguage));
    }

    /**
     * Get All Type And Optional
     * 
     * @param int idLanguage
     * @param int idItem
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllTypeAndOptional(int $idLanguage, int $idItem)
    {
        return response()->success(TypeOptionalBL::getAllTypeAndOptional($idLanguage, $idItem));
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(TypeOptionalBL::getById($id));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(TypeOptionalBL::getSelect($search));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(TypeOptionalBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Insert item optional
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertItemTypeOptional(Request $request)
    {
        return response()->success(TypeOptionalBL::insertItemTypeOptional($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoSelenaViews
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoSelenaViews)
    {
        return response()->success(TypeOptionalBL::update($dtoSelenaViews, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(TypeOptionalBL::delete($id, HttpHelper::getLanguageId()));
    }

    /**
     * Delete item optional
     *
     * @param int idTypeOptional
     * @param int idItem
     * 
     * @return \Illuminate\Http\Response
     */
    public function delItemTypeOptional(int $idTypeOptional, int $idItem)
    {
        return response()->success(TypeOptionalBL::delItemTypeOptional($idTypeOptional, $idItem, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\SearchDataSheetBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class SearchDataSheetController
{
    #region POST

    /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function searchDataSheet(Request $request)
    {
        return response()->success(SearchDataSheetBL::searchDataSheet($request));
    }

      /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function searchPaginatorDataSheet(Request $request)
    {
        return response()->success(SearchDataSheetBL::searchPaginatorDataSheet($request));
    }
    #endregion POST

    




}

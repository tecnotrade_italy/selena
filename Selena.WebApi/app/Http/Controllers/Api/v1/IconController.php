<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\IconBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;

class IconController
{
    #region GET

    /**
     * Get all icon
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(IconBL::getSelect($search));
    }

    /**
     * Get icon by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(IconBL::getById($id));
    }



    #endregion GET
}

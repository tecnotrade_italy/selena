<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\AppointmentsBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class AppointmentsController
{
    #region GET
    /**
     * Get all Employees
     *
     * @param int $Employees
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        return response()->success(AppointmentsBL::getAllDto(HttpHelper::getLanguageId()));
    }

    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(AppointmentsBL::getSelect($search));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(AppointmentsBL::getAll($idLanguage));
    }
    #endregion GET

  
    public function getAllAppointmentsSearch(Request $DtoAppointments)
        {
            return response()->success(AppointmentsBL::getAllAppointmentsSearch($DtoAppointments, HttpHelper::getLanguageId()));
        }

                /**
     * getAllResultCalendar
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllResultCalendar(int $idLanguage)
    {
        return response()->success(AppointmentsBL::getAllResultCalendar($idLanguage));
    }
    #endregion GET

    public function getByIdEvent(int $id)
    {
        return response()->success(AppointmentsBL::getByIdEvent($id));
    }
    #endregion GET

    

            
   

}

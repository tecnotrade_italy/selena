<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoCommunityImageComment;
use App\BusinessLogic\CommunityImageCommentBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\CommunityImageComment;
use App\Helpers\LogHelper;


class CommunityImageCommentController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(CommunityImageCommentBL::getById($id));
    }


        /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(CommunityImageCommentBL::getAll());
    }


    #endregion GET
    
    public function insert(Request $dtoCommunityImageComment)
    {
        return response()->success(CommunityImageCommentBL::insert($dtoCommunityImageComment));
    }
    
    public function update(Request $dtoCommunityImageComment)
    {
        return response()->success(CommunityImageCommentBL::update($dtoCommunityImageComment));
    }

    public function delete(int $id)
    {
        return response()->success(CommunityImageCommentBL::delete($id));
    }
   
}

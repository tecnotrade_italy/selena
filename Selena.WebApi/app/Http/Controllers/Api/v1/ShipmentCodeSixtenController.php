<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ShipmentCodeSixtenBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class ShipmentCodeSixtenController
{
    #region GET

    /**
     * Get select
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(ShipmentCodeSixtenBL::getSelect($search, $request->idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get by customer sixten
     * 
     * @param int idCustomer
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByCustomerSixten(int $idCustomer, int $idFunctionality)
    {
        return response()->success(ShipmentCodeSixtenBL::getByCustomerSixten($idCustomer, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion GET
}

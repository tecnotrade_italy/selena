<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\EmployeesBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class EmployeesController
{
    #region GET
    /**
     * Get all Employees
     *
     * @param int $Employees
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        return response()->success(EmployeesBL::getAllDto(HttpHelper::getLanguageId()));
    }

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(EmployeesBL::getById($id));
    }
 
    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(EmployeesBL::getSelect($search));
    }


    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(EmployeesBL::getAll($idLanguage));
    }
    #endregion GET

    #region INSERT 
    /**
     * Insert
     *
     * @param Request DtoEmployees
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoEmployees)
    {
        return response()->success(EmployeesBL::insert($DtoEmployees, HttpHelper::getLanguageId()));
       
    }
    #endregion INSERT

    /**
     * Clone
     *
     * @param int $id
     * @param int $idFunctionality
     * 
     */
    
    public function clone(int $id, int $idFunctionality){
        return response()->success(EmployeesBL::clone($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #REGION UPDATE
    /**
     * Update
     *
     * @param Request DtoEmployees
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoEmployees)
    {
        return response()->success(EmployeesBL::update($DtoEmployees, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE

    #region DELETE
    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(EmployeesBL::delete($id, HttpHelper::getLanguageId()));
    }
    #endregion DELETE
}

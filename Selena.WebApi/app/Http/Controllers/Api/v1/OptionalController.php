<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\OptionalBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class OptionalController
{
    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(OptionalBL::getAll($idLanguage));
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(OptionalBL::getById($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(OptionalBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Insert item optional
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertItemOptional(Request $request)
    {
        return response()->success(OptionalBL::insertItemOptional($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoSelenaViews
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoSelenaViews)
    {
        return response()->success(OptionalBL::update($dtoSelenaViews, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(OptionalBL::delete($id, HttpHelper::getLanguageId()));
    }

    /**
     * Delete item optional
     *
     * @param int idOptional
     * @param int idItem
     * 
     * @return \Illuminate\Http\Response
     */
    public function delItemOptional(int $idOptional, int $idItem)
    {
        return response()->success(OptionalBL::delItemOptional($idOptional, $idItem, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

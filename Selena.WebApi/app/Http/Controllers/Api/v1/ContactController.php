<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ContactBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class ContactController
{
    #region GET
    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(ContactBL::getSelect($search));
    }

    /**
     * Get all request
     * 
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllRequest(int $idFunctionality)
    {
        return response()->success(ContactBL::getAllRequest($idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(ContactBL::getAll());
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(ContactBL::getById($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertRequest(Request $request)
    {
        return response()->success(ContactBL::insertRequest($request));
    }
    

    /**
     * import Data
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function importData(Request $request)
    {   
        return response()->success(ContactBL::importData($request));
    }

    /**
     * Insert
     *
     * @param Request dtoContact
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $dtoContact)
    {
        return response()->success(ContactBL::insert($dtoContact, HttpHelper::getLanguageId()));
    }

    /**
     * Subscribe
     *
     * @param Request dtoContact
     * 
     * @return \Illuminate\Http\Response
     */
    public function subscribe(Request $dtoContact)
    {
        return response()->success(ContactBL::subscribe($dtoContact, HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoContact
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoContact)
    {
        return response()->success(ContactBL::update($dtoContact, HttpHelper::getLanguageId()));
    }

    /**
     * Unsubscribe
     *
     * @param Request dtoContact
     * 
     * @return \Illuminate\Http\Response
     */
    public function unsubscribe(Request $dtoContact)
    {
        return response()->success(ContactBL::unsubscribe($dtoContact, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE


    /**
     * Unsubscribe
     *
     * @param Request dtoContact
     * 
     * @return \Illuminate\Http\Response
     */
    public function unsubscribeNewsletter(Request $dtoContact)
    {
        return response()->success(ContactBL::unsubscribeNewsletter($dtoContact, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(ContactBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

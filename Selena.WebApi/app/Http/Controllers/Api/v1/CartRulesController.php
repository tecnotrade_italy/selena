<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\CartRulesBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;

class CartRulesController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(CartRulesBL::getById($id));
    }

 /**
     * Get UserSelectedAndNot
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getUserSelectedAndNot(Int $id)
    {
        return response()->success(CartRulesBL::getUserSelectedAndNot($id));
    }

 /**
     * Get GroupSelectedAndNot
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getGroupSelectedAndNot(Int $id)
    {
        return response()->success(CartRulesBL::getGroupSelectedAndNot($id));
    }

     /**
     * Get GroupSelectedAndNot
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getCarriersSelectedAndNot(Int $id)
    {
        return response()->success(CartRulesBL::getCarriersSelectedAndNot($id));
    }

    
       /**
     * Get getItemsSelectedAndNot
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getItemsSelectedAndNot(Int $id)
    {
        return response()->success(CartRulesBL::getItemsSelectedAndNot($id));
    }

    
     /**
     * Get getCategoriesSelectedAndNot
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getCategoriesSelectedAndNot(Int $id)
    {
        return response()->success(CartRulesBL::getCategoriesSelectedAndNot($id));
    }

    /**
     * Get getProducersSelectedAndNot
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getProducersSelectedAndNot(Int $id)
    {
        return response()->success(CartRulesBL::getProducersSelectedAndNot($id));
    }

    /**
     * Get getSuppliersSelectedAndNot
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getSuppliersSelectedAndNot(Int $id)
    {
        return response()->success(CartRulesBL::getSuppliersSelectedAndNot($id));
    }

     /**
     * Get getSpecificProductsSelectedAndNot
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getSpecificProductsSelectedAndNot(Int $id)
    {
        return response()->success(CartRulesBL::getSpecificProductsSelectedAndNot($id));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(CartRulesBL::getAll());
    }

    #endregion GET


    #region INSERT
    /**
     * Insert
     *
     * @param Request dtoVatType
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $dtoCartRules)
    {
        return response()->success(CartRulesBL::insert($dtoCartRules, HttpHelper::getLanguageId()));
    }

    #endregion INSERT

/**
     * insertUserCartRoleUser
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertUserCartRoleUser(Request $request)
    {
        return response()->success(CartRulesBL::insertUserCartRoleUser($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


/**
     * insertCategoriesCartRoleCategories
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertCategoriesCartRoleCategories(Request $request)
    {
        return response()->success(CartRulesBL::insertCategoriesCartRoleCategories($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * insertProducersCartRoleProducers
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertProducersCartRoleProducers(Request $request)
    {
        return response()->success(CartRulesBL::insertProducersCartRoleProducers($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * insertSuppliersCartRoleSuppliers
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertSuppliersCartRoleSuppliers(Request $request)
    {
        return response()->success(CartRulesBL::insertSuppliersCartRoleSuppliers($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    
    

    /**
     * insertSpecificItemsCartRole
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertSpecificItemsCartRole(Request $request)
    {
        return response()->success(CartRulesBL::insertSpecificItemsCartRole($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    /**
     * updateSpecificProductsValue
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateSpecificProductsValue(Request $request)
    {
        return response()->success(CartRulesBL::updateSpecificProductsValue($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }
    
/**
     * insertItemCartRole
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertItemCartRole(Request $request)
    {
        return response()->success(CartRulesBL::insertItemCartRole($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }
/**
     * insertCarriersCartRole
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertCarriersCartRole(Request $request)
    {
        return response()->success(CartRulesBL::insertCarriersCartRole($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


/**
     * insertCartRoleGroup
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertCartRoleGroup(Request $request)
    {
        return response()->success(CartRulesBL::insertCartRoleGroup($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * getUserCartRolesSearch
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getUserCartRolesSearch(Request $request)
    {
        return response()->success(CartRulesBL::getUserCartRolesSearch($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

/**
     * getGroupCartRolesSearch
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getGroupCartRolesSearch(Request $request)
    {
        return response()->success(CartRulesBL::getGroupCartRolesSearch($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

/**
     * getGroupSearchNameCartRuleGroup
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getGroupSearchNameCartRuleGroup(Request $request)
    {
        return response()->success(CartRulesBL::getGroupSearchNameCartRuleGroup($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

/**
     * getUserSearchBusinessNameCartRuleUser
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getUserSearchBusinessNameCartRuleUser(Request $request)
    {
        return response()->success(CartRulesBL::getUserSearchBusinessNameCartRuleUser($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }
    
    /**
     * getCarriersCartRolesSearch
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getCarriersCartRolesSearch(Request $request)
    {
        return response()->success(CartRulesBL::getCarriersCartRolesSearch($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    /**
     * getCarrierSearchNameCartRuleCarriers
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getCarrierSearchNameCartRuleCarriers(Request $request)
    {
        return response()->success(CartRulesBL::getCarrierSearchNameCartRuleCarriers($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

/**
     * getItemsCartRolesSearch
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getItemsCartRolesSearch(Request $request)
    {
        return response()->success(CartRulesBL::getItemsCartRolesSearch($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }
    
/**
     * getItemsSearchDescriptionCartRuleItems
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getItemsSearchDescriptionCartRuleItems(Request $request)
    {
        return response()->success(CartRulesBL::getItemsSearchDescriptionCartRuleItems($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * getItemsSearchDescriptionCartRuleItems
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getCategoriesCartRolesSearch(Request $request)
    {
        return response()->success(CartRulesBL::getCategoriesCartRolesSearch($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * getProducersCartRolesSearch
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getProducersCartRolesSearch(Request $request)
    {
        return response()->success(CartRulesBL::getProducersCartRolesSearch($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    /**
     * getSuppliersCartRolesSearch
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getSuppliersCartRolesSearch(Request $request)
    {
        return response()->success(CartRulesBL::getSuppliersCartRolesSearch($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }
    

    /**
     * getSpecificProductCartRolesSearch
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getSpecificProductCartRolesSearch(Request $request)
    {
        return response()->success(CartRulesBL::getSpecificProductCartRolesSearch($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


/**
     * getSearchDescriptionCartRuleCategories
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getSearchDescriptionCartRuleCategories(Request $request)
    {
        return response()->success(CartRulesBL::getSearchDescriptionCartRuleCategories($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * getSearchDescriptionCartRuleProducers
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getSearchDescriptionCartRuleProducers(Request $request)
    {
        return response()->success(CartRulesBL::getSearchDescriptionCartRuleProducers($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * getSearchDescriptionCartRuleSuppliers
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getSearchDescriptionCartRuleSuppliers(Request $request)
    {
        return response()->success(CartRulesBL::getSearchDescriptionCartRuleSuppliers($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }
    
    /**
     * getSearchSpecificProductsSelectedCartRule
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getSearchSpecificProductsSelectedCartRule(Request $request)
    {
        return response()->success(CartRulesBL::getSearchSpecificProductsSelectedCartRule($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    #region UPDATE
    /**
     * Update
     *
     * @param Request dtoCartRuless
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoCartRules)
    {
        return response()->success(CartRulesBL::update($dtoCartRules, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(CartRulesBL::delete($id, HttpHelper::getLanguageId()));
    }
    #endregion DELETE

  /**
     * deleteUserCartRoleUser
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteUserCartRoleUser(int $id)
    {
        return response()->success(CartRulesBL::deleteUserCartRoleUser($id, HttpHelper::getLanguageId()));
    }
    #endregion deleteUserCartRoleUser
    

    /**
     * deleteCategoriesCartRoleCategories
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteCategoriesCartRoleCategories(int $id)
    {
        return response()->success(CartRulesBL::deleteCategoriesCartRoleCategories($id, HttpHelper::getLanguageId()));
    }
    #endregion deleteUserCartRoleUser

    /**
     * deleteProducersCartRoleProducers
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteProducersCartRoleProducers(int $id)
    {
        return response()->success(CartRulesBL::deleteProducersCartRoleProducers($id, HttpHelper::getLanguageId()));
    }
    #endregion deleteProducersCartRoleProducers

    

    /**
     * deleteSuppliersCartRoleSuppliers
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteSuppliersCartRoleSuppliers(int $id)
    {
        return response()->success(CartRulesBL::deleteSuppliersCartRoleSuppliers($id, HttpHelper::getLanguageId()));
    }
    #endregion deleteSuppliersCartRoleSuppliers

    /**
     * deleteSpecificProductsCartRole
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteSpecificProductsCartRole(int $id)
    {
        return response()->success(CartRulesBL::deleteSpecificProductsCartRole($id, HttpHelper::getLanguageId()));
    }
    #endregion deleteUserCartRoleUser

/**
     * deleteCartRoleCarriers
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteCartRoleCarriers(int $id)
    {
        return response()->success(CartRulesBL::deleteCartRoleCarriers($id, HttpHelper::getLanguageId()));
    }
    #endregion deleteCartRoleCarriers


/**
     * deleteCartRoleItem
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteCartRoleItem(int $id)
    {
        return response()->success(CartRulesBL::deleteCartRoleItem($id, HttpHelper::getLanguageId()));
    }
    #endregion deleteCartRoleItem


  /**
     * deleteCartRoleGroup
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteCartRoleGroup(int $id)
    {
        return response()->success(CartRulesBL::deleteCartRoleGroup($id, HttpHelper::getLanguageId()));
    }
    #endregion deleteCartRoleGroup


}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\BookingStatesBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class BookingStatesController
{
  

    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(BookingStatesBL::getSelect($search));
    }

}

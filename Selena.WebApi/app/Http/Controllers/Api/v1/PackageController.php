<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\PackageBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class PackageController
{
    #region GET

    /**
     * Get select by erp
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function selectByErp(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(PackageBL::getSelectByErp($request->idErp, $search, $request->idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get max by erp
     * 
     * @param string idErp
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getMaxByErp(string $idErp, int $idFunctionality)
    {
        return response()->success(PackageBL::getMaxByErp($idErp, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion GET

    #region INSERT

    /**
     * Create byErp
     * 
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function createByErp(Request $request)
    {
        return response()->success(PackageBL::createByErp($request->idErp, $request->description, $request->idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion INSERT
}

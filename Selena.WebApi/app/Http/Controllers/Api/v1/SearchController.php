<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\SearchBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class SearchController
{
    #region POST

    /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        return response()->success(SearchBL::search($request));
    }

     /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function searchImplemented(Request $request)
    {
        return response()->success(SearchBL::searchImplemented($request));
    }

     /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function searchGeneral(Request $request)
    {
        return response()->success(SearchBL::searchGeneral($request));
    }

    

    #endregion POST
}

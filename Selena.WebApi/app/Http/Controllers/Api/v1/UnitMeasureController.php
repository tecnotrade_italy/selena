<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoUnitMeasure;
use App\LanguageUnitMeasure;
use App\BusinessLogic\UnitMeasureBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\UnitMeasure;


class UnitMeasureController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(UnitMeasureBL::getById($id));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(UnitMeasureBL::getSelect($search));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(UnitMeasureBL::getAll($idLanguage));
    }

    #endregion GET
    
    #region INSERT

    /**
     * Insert
     *
     * @param Request DtoUnitMeasure
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoUnitMeasure)
    {
        return response()->success(UnitMeasureBL::insert($DtoUnitMeasure, HttpHelper::getUserId()));
       
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request DtoUnitMeasure
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoUnitMeasure)
    {
        return response()->success(UnitMeasureBL::update($DtoUnitMeasure));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(UnitMeasureBL::delete($id));
    }

    #endregion DELETE
}

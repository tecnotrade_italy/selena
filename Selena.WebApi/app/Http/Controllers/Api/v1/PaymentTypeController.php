<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\PaymentTypeBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\Helpers\LogHelper;

class PaymentTypeController
{
    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * @param int cartId
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllByCartId(int $idLanguage, int $cartId)
    {
        return response()->success(PaymentTypeBL::getAllByCartId($idLanguage, $cartId));
    }

    #endregion GET

    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(PaymentTypeBL::getSelect($search));
    }

    #region INSERT


    #endregion INSERT

    #region UPDATE

    #endregion UPDATE

    #region DELETE

    #endregion DELETE
}

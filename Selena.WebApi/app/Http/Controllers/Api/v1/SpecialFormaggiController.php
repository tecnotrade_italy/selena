<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\SpecialFormaggiBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class SpecialFormaggiController
{
    #region GET

    /**
     * Get items from external api
     * 
     * 
     * @return \Illuminate\Http\Response
     */
    public function importItems()
    {
        return response()->success(SpecialFormaggiBL::importItems());
    }

     /**
     * Get users from external api
     * 
     * 
     * @return \Illuminate\Http\Response
     */
    public function importUsers()
    {
        return response()->success(SpecialFormaggiBL::importUsers());
    }

    /**
     * Get Group Agenti from external api
     * 
     * 
     * @return \Illuminate\Http\Response
     */
    public function importGroupAgenti()
    {
        return response()->success(SpecialFormaggiBL::importGroupAgenti());
    }

    /**
     * Get Canali Vendita from external api
     * 
     * 
     * @return \Illuminate\Http\Response
     */
    public function importCanaliVendita()
    {
        return response()->success(SpecialFormaggiBL::importCanaliVendita());
    }

    /**
     * Get Categorie Cliente from external api
     * 
     * 
     * @return \Illuminate\Http\Response
     */
    public function importCategorieCliente()
    {
        return response()->success(SpecialFormaggiBL::importCategorieCliente());
    }

    /**
     * Get Zone Commerciali from external api
     * 
     * 
     * @return \Illuminate\Http\Response
     */
    public function importZoneCommerciali()
    {
        return response()->success(SpecialFormaggiBL::importZoneCommerciali());
    }

    #endregion GET
}

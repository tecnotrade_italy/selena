<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\DocumentRowBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class DocumentRowController
{
    #region GET

    /**
     * Get sixten by id document head
     *
     * @param int idHead
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getSixtenByHead(int $idHead, int $idFunctionality)
    {
        return response()->success(DocumentRowBL::getSixtenByHead($idHead, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get mdmicrodetectors by id document head
     *
     * @param string idHead
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getMdMicrodetectorsByHead(string $idHead)
    {
        return response()->success(DocumentRowBL::getMdMicrodetectorsByHead($idHead, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get by idErp
     * 
     * @param string idErp
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByErp(string $idErp, int $idFunctionality)
    {
        return response()->success(DocumentRowBL::getByErp($idErp, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get number package for DDT
     * 
     * @param int idCustomer
     * @param int idMonth
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getNumberPackageDDT(int $idCustomer, int $idMonth, int $idFunctionality)
    {
        return response()->success(DocumentRowBL::getNumberPackageDDT($idCustomer, $idMonth, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert beforeDDT
     * 
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function insertBeforeDDT(Request $request)
    {
        return response()->success(DocumentRowBL::insertBeforeDDT($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion INSERT


    #region UPDATE

    
    /**
     * Check article from shipping MdMicrodetectors
     *
     * @param Request dtoData
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateMdMicrodetectorsShippingRow(Request $dtoData)
    {
        return response()->success(DocumentRowBL::updateMdMicrodetectorsShippingRow($dtoData, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Reset all article from shipping MdMicrodetectors
     *
     * @param Request dtoData
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateMdMicrodetectorsResetShippingRow(Request $dtoData)
    {
        return response()->success(DocumentRowBL::updateMdMicrodetectorsResetShippingRow($dtoData, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion UPDATE
}

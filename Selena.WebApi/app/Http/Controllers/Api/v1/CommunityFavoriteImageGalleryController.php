<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoCommunityFavoriteImageGallery;
use App\BusinessLogic\CommunityFavoriteImageGalleryBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\CommunityFavoriteImageGallery;
use App\Helpers\LogHelper;


class CommunityFavoriteImageGalleryController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(CommunityFavoriteImageGalleryBL::getById($id));
    }


        /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(CommunityFavoriteImageGalleryBL::getAll());
    }


    #endregion GET
    
    public function insert(Request $dtoCommunityFavoriteImageGallery)
    {
        return response()->success(CommunityFavoriteImageGalleryBL::insert($dtoCommunityFavoriteImageGallery,HttpHelper::getUserId()));
    }
    
    public function update(Request $dtoCommunityFavoriteImageGallery)
    {
        return response()->success(CommunityFavoriteImageGalleryBL::update($dtoCommunityFavoriteImageGallery));
    }

    public function delete(int $id)
    {
        return response()->success(CommunityFavoriteImageGalleryBL::delete($id));
    }
   
}

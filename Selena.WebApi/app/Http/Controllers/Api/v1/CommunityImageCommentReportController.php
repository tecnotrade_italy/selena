<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoCommunityImageCommentReport;
use App\BusinessLogic\CommunityImageCommentReportBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\CommunityImageCommentReport;
use App\Helpers\LogHelper;


class CommunityImageCommentReportController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(CommunityImageCommentReportBL::getById($id));
    }


        /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(CommunityImageCommentReportBL::getAll());
    }


    #endregion GET
    
    public function insert(Request $dtoCommunityImageCommentReport)
    {
        return response()->success(CommunityImageCommentReportBL::insert($dtoCommunityImageCommentReport,HttpHelper::getUserId()));
    }
    
    public function update(Request $dtoCommunityImageCommentReport)
    {
        return response()->success(CommunityImageCommentReportBL::update($dtoCommunityImageCommentReport));
    }

    public function delete(int $id)
    {
        return response()->success(CommunityImageCommentReportBL::delete($id));
    }
   
}

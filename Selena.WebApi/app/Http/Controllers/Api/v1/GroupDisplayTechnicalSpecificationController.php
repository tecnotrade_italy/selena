<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\GroupDisplayTechnicalSpecificationBL;
use App\BusinessLogic\GroupDisplayTechnicalSpecificationLanguageBL;
use App\BusinessLogic\GroupDisplayTechnicalSpecificationTechnicalSpecificationBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;

class GroupDisplayTechnicalSpecificationController
{
    #region GET

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getDtoTable()
    {
        return response()->success(GroupDisplayTechnicalSpecificationBL::getDtoTable());
    }

    /**
     * Get all technical specification included and excluded
     * 
     * @param int $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getTechnicalSpecification(int $id)
    {
        return response()->success(GroupDisplayTechnicalSpecificationBL::getTechnicalSpecification($id, HttpHelper::getLanguageId()));
    }

    /**
     * Check exists
     *
     * @param string description
     * @param int idLanguage
     * @param int idGroupTechnicalSpecification
     * 
     * @return \Illuminate\Http\Response
     */
    public function checkExists(string $description, int $idLanguage, int $id = null)
    {
        return response()->success(GroupDisplayTechnicalSpecificationLanguageBL::checkExists($description, $idLanguage, $id));
    }

    #endregion GET

    #region PUT

    /**
     * Insert
     *
     * @param Request
     *
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(GroupDisplayTechnicalSpecificationBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Add technical specification
     * 
     * @param int $id
     * @param int $idTechnicalSpecification
     * @param int $idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function addTechnicalSpecification(int $id, int $idTechnicalSpecification, int $idFunctionality)
    {
        return response()->success(GroupDisplayTechnicalSpecificationTechnicalSpecificationBL::insert($id, $idTechnicalSpecification, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion PUT

    #region POST

    /**
     * Update
     *
     * @param Request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return response()->success(GroupDisplayTechnicalSpecificationBL::update($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Update technical specification
     * 
     * @param int $id
     * @param int $order
     * @param int $idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateTechnicalSpecification(int $id, int $order, int $idFunctionality)
    {
        return response()->success(GroupDisplayTechnicalSpecificationTechnicalSpecificationBL::update($id, $order, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion POST

    #region DELETE

    /**
     * delete
     *
     * @param int id
     * @param int idFunctionality
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id, int $idFunctionality)
    {
        return response()->success(GroupDisplayTechnicalSpecificationBL::delete($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * delete
     *
     * @param int $id
     * @param int $idFunctionality
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteGroupDisplayTechnicalSpecificationTechnicalSpecification(int $id, int $idFunctionality)
    {
        return response()->success(GroupDisplayTechnicalSpecificationTechnicalSpecificationBL::delete($id, $idFunctionality,  HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\QuotesViewBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;

class QuotesViewController
{
    #region GET

 /**
     * Get by getByViewQuotes
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByViewQuotes(Int $id, Int $idUser)
    {
        return response()->success(QuotesViewBL::getByViewQuotes($id, $idUser));
    }

    /**
     * Get by getViewPdfQuotes
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getViewPdfQuotes(Int $id)
    
    {
        return response()->success(QuotesViewBL::getViewPdfQuotes($id));
    }
    
    /**
     * Get by getViewPdfQuotes
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function SearchQuotes(Request $request)
    {
        return response()->success(QuotesViewBL::SearchQuotes($request));
    }

 /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllViewQuote(int $idLanguage)
    {
        return response()->success(QuotesViewBL::getAllViewQuote($idLanguage));
    }

    #endregion GET

   
    /**
     * updateInterventionUpdateViewQuotes
     *
     * @param Request DtoInterventions
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateInterventionUpdateViewQuotes(Request $DtoQuotes)
    {
        return response()->success(QuotesViewBL::updateInterventionUpdateViewQuotes($DtoQuotes, HttpHelper::getLanguageId()));
    }

    #endregion updateInterventionUpdateViewQuotes

    /**
     * updateandSendEmailQuotes
     *
     * @param Request DtoInterventions
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateandSendEmailQuotes(Request $DtoQuotes)
    {
        return response()->success(QuotesViewBL::updateandSendEmailQuotes($DtoQuotes, HttpHelper::getLanguageId()));
    }

    #endregion updateandSendEmailQuotes
    
    public function insertMessageQuotes(Request $request)
    {
        return response()->success(QuotesViewBL::insertMessageQuotes($request, HttpHelper::getUserId()));
    }

    public function insertReferent(Request $request)
    {
        return response()->success(QuotesViewBL::insertReferent($request, HttpHelper::getUserId()));
    }


    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(QuotesViewBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoInterventions;
use App\CategoryLanguage;
use App\BusinessLogic\FaultModulesBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\GrIntervention;
use App\Helpers\LogHelper;

class FaultModulesController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(FaultModulesBL::getById($id));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(FaultModulesBL::getSelect($search));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(FaultModulesBL::getAll($idLanguage));
    }

 
    
    #region INSERT

    /**
     * Insert
     *
     * @param Request DtoCategories
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoInterventions)
    {
        return response()->success(FaultModulesBL::insert($DtoInterventions, HttpHelper::getUserId()));
       
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request DtoCategories
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoInterventions)
    {
        return response()->success(FaultModulesBL::update($DtoInterventions));
    }

    #endregion UPDATE

/**
     * Clone
     *
     * @param int $id
     * @param int $idFunctionality
     * 
     */
    
    public function clone(int $id, int $idFunctionality){
        return response()->success(FaultModulesBL::clone($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }
   
    #endregion Clone

    #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(FaultModulesBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

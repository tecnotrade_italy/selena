<?php

namespace App\Http\Controllers\Api\v1;
use App\BusinessLogic\InterventionCustomerBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;

class InterventionCustomerController
{
    #region GET

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllInterventionCustomer(int $idUser)
    {
        return response()->success(InterventionCustomerBL::getAllInterventionCustomer($idUser));
    }
    #endregion GET


 /**
     * Get by getByViewQuotesCustomer
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByViewQuotesCustomer(Int $id)
    {
        return response()->success(InterventionCustomerBL::getByViewQuotesCustomer($id));
    }

    
 /**
     * Get by getByViewModuleCustomer
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByViewModuleCustomer(Int $id)
    {
        return response()->success(InterventionCustomerBL::getByViewModuleCustomer($id));
    }


 /**
     * Get by getByViewModuleValidationGuarantee
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByViewModuleValidationGuarantee(Int $id)
    {
        return response()->success(InterventionCustomerBL::getByViewModuleValidationGuarantee($id));
    }

     /**
     * updateInterventionUpdateViewCustomerQuotes
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateInterventionUpdateViewCustomerQuotes(Request $DtoQuotes)
    {
        return response()->success(InterventionCustomerBL::updateInterventionUpdateViewCustomerQuotes($DtoQuotes, HttpHelper::getUserId()));
    }

        /**
     * updateModuleValidationGuarantee
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateModuleValidationGuarantee(Request $DtoModuleValidationGuarantee)
    {
        return response()->success(InterventionCustomerBL::updateModuleValidationGuarantee($DtoModuleValidationGuarantee, HttpHelper::getUserId()));
    }

    

      /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllInterventionCustomerSearch(Request $request)
    {
        return response()->success(InterventionCustomerBL::getAllInterventionCustomerSearch($request));
    }

    
          /**
     * updateSendFaultModule
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateSendFaultModule(Request $request)
    {
        return response()->success(InterventionCustomerBL::updateSendFaultModule($request, HttpHelper::getUserId()));
    }

    public function insertMessageCustomerQuotes(Request $request)
    {
        return response()->success(InterventionCustomerBL::insertMessageCustomerQuotes($request, HttpHelper::getUserId()));
    }
    

    #endregion UPDATE

}

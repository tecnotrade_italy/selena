<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ConnectionsBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class ConnectionsController
{
    #region GET
    /**
     * Get all Connections
     *
     * @param int $Connections
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        return response()->success(ConnectionsBL::getAllDto(HttpHelper::getLanguageId()));
    }

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(ConnectionsBL::getById($id));
    }
 
    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(ConnectionsBL::getSelect($search));
    }


    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(ConnectionsBL::getAll($idLanguage));
    }
    #endregion GET

    #region INSERT 
    /**
     * Insert
     *
     * @param Request DtoConnections
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoConnections)
    {
        return response()->success(ConnectionsBL::insert($DtoConnections, HttpHelper::getLanguageId()));
       
    }
    #endregion INSERT

       #region INSERT 
    /**
     * Insert
     *
     * @param Request DtoConnections
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertPrenotazioneForCliBooking(Request $request)
    {
        return response()->success(ConnectionsBL::insertPrenotazioneForCliBooking($request, HttpHelper::getLanguageId()));
       
    }
    #endregion INSERT

    

    /**
     * Clone
     *
     * @param int $id
     * @param int $idFunctionality
     * 
     */
    public function clone(int $id, int $idFunctionality){
        return response()->success(ConnectionsBL::clone($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #REGION UPDATE
    /**
     * Update
     *
     * @param Request DtoConnections
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoConnections)
    {
        return response()->success(ConnectionsBL::update($DtoConnections, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE

    public function getByIdConnections(Request $request)
    {
        return response()->success(ConnectionsBL::getByIdConnections($request, HttpHelper::getLanguageId()));
    }

    public function getByEventResultForParameter(Request $request)
    {
        return response()->success(ConnectionsBL::getByEventResultForParameter($request, HttpHelper::getLanguageId()));
    }


    #region DELETE
    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(ConnectionsBL::delete($id, HttpHelper::getLanguageId()));
    }
    #endregion DELETE
}

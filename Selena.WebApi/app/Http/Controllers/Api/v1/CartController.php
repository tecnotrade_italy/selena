<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\CartBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class CartController
{
    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(CartBL::getAll());
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(CartBL::getById($id));
    }

    public function getTotalAmount(int $id)
    {
        return response()->success(CartBL::getTotalAmount($id));
    }

    public function getNrProductRows(int $id)
    {
        return response()->success(CartBL::getNrProductRows($id));
    } 

    public function getNrRows(int $id)
    {
        return response()->success(CartBL::getNrRows($id));
    } 
    
    public function getNoteByCartId(int $id)
    {
        return response()->success(CartBL::getNoteByCartId($id));
    } 
    
    

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(CartBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoNation
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoCart)
    {
        return response()->success(CartBL::update($dtoCart, HttpHelper::getLanguageId()));
    }

    /**
     * Update
     *
     * @param Request dtoNation
     * 
     * @return \Illuminate\Http\Response
     */
    public function updatePaymentType(Request $dtoCart)
    {
        return response()->success(CartBL::updatePaymentType($dtoCart));
    }

    /**
     * update Note
     *
     * @param Request dtoNation
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateNote(Request $dtoCart)
    {
        return response()->success(CartBL::updateNote($dtoCart));
    }

    

      /**
     * updateCarriage
     *
     * @param Request dtoNation
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateCarriage(Request $dtoCart)
    {
       
        return response()->success(CartBL::updateCarriage($dtoCart));
    }

     public function updateCarrier(Request $dtoCart)
    {
        return response()->success(CartBL::updateCarrier($dtoCart));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(CartBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE

    public function applyRulesAndRefreshTotalAmount(int $id)
    {
        return response()->success(CartBL::applyRulesAndRefreshTotalAmount($id));
    }
}

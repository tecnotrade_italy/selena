<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\QueryGroupNewsletterBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class QueryGroupNewsletterController
{
    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(QueryGroupNewsletterBL::getAll());
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(QueryGroupNewsletterBL::getById($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(QueryGroupNewsletterBL::insert($request));
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoSelenaViews
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return response()->success(QueryGroupNewsletterBL::update($request, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(QueryGroupNewsletterBL::getSelect($search));
    }

      /**
     * 
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function executequeryresult(Request $request)
    {
        return response()->success(QueryGroupNewsletterBL::executequeryresult($request));
    }

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(QueryGroupNewsletterBL::delete($id, HttpHelper::getLanguageId()));
    }


    /**
     * DeleteRow
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteRow(int $id)
    {
        return response()->success(QueryGroupNewsletterBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

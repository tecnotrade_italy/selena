<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\LanguagePageCategoryBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;

class LanguagePageCategoryController
{
    #region GET

    /**
     * Get all page categories
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(LanguagePageCategoryBL::getSelect($search));
    }



    /**
     * Get page category by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(LanguagePageCategoryBL::getById($id));
    }

    /**
     * Get All By Page Id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getAllByPageId(Int $id)
    {
        return response()->success(LanguagePageCategoryBL::getAllByPageId($id));
    }

       /**
     * Get All List Navigation
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllListCategories(int $idLanguage, int $idPages)
    {
        return response()->success(LanguagePageCategoryBL::getAllListCategories($idLanguage, $idPages));
    }

    #endregion GET
}

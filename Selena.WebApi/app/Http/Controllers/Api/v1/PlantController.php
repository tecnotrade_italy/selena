<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\PlantBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class PlantController
{
  
 /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(PlantBL::getSelect($search));
    }


}
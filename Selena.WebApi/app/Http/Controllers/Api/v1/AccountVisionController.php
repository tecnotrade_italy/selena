<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\AccountVisionBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;

class AccountVisionController
{
    #region GET

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllAccountVision(int $idLanguage)
    {
        return response()->success(AccountVisionBL::getAllAccountVision($idLanguage));
    }

    #endregion GET

    /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function SearchAccountVision(Request $request)
    {
        return response()->success(AccountVisionBL::SearchAccountVision($request));
    }


    /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function SearchAccountVisionOpen(Request $request)
    {
        return response()->success(AccountVisionBL::SearchAccountVisionOpen($request));
    }

    /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function SearchAccountVisionClose(Request $request)
    {
        return response()->success(AccountVisionBL::SearchAccountVisionClose($request));
    }
    

    /**
     * Get by getById
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(AccountVisionBL::getById($id));
    }

 /**
     * update
     *
     * @param Request DtoAccountVision
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoAccountVision)
    {
        return response()->success(AccountVisionBL::update($DtoAccountVision, HttpHelper::getLanguageId()));
    }


  /**
     * Get Last id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getLastId()
    {
        return response()->success(AccountVisionBL::getLastId());
    }

    public function getLastIdContoVisione()
    {
        return response()->success(AccountVisionBL::getLastIdContoVisione());
    }

    
/**
     * Insert
     *
     * @param Request DtoAccountVision
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(AccountVisionBL::insert($request, HttpHelper::getUserId()));
    }
    #endregion insert


 #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(AccountVisionBL::delete($id, HttpHelper::getLanguageId()));
    }
    #endregion DELETE


}

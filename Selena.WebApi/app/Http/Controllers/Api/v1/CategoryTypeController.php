<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoCategoryType;
use App\CategoryTypeLanguage;
use App\BusinessLogic\CategoryTypeBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\CategoryType;
use App\Helpers\LogHelper;


class CategoryTypeController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(CategoryTypeBL::getById($id));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(CategoryTypeBL::getSelect($search));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(CategoryTypeBL::getAll($idLanguage));
    }

    #endregion GET
    
    #region INSERT

    /**
     * Insert
     *
     * @param Request DtoCategoryType
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoCategoryType)
    {
        return response()->success(CategoryTypeBL::insert($DtoCategoryType, HttpHelper::getUserId()));
       
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request DtoCategoryType
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoCategoryType)
    {
        return response()->success(CategoryTypeBL::update($DtoCategoryType));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(CategoryTypeBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

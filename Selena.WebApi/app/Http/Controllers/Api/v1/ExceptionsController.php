<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ExceptionsBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class ExceptionsController
{
    #region GET
    /**
     * Get all Exceptions
     *
     * @param int $Exceptions
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        return response()->success(ExceptionsBL::getAllDto(HttpHelper::getLanguageId()));
    }

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(ExceptionsBL::getById($id));
    }
 
    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(ExceptionsBL::getSelect($search));
    }


    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(ExceptionsBL::getAll($idLanguage));
    }
    #endregion GET

    #region INSERT 
    /**
     * Insert
     *
     * @param Request DtoExceptions
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoExceptions)
    {
        return response()->success(ExceptionsBL::insert($DtoExceptions, HttpHelper::getLanguageId()));
       
    }
    #endregion INSERT

    /**
     * Clone
     *
     * @param int $id
     * @param int $idFunctionality
     * 
     */
    
    public function clone(int $id, int $idFunctionality){
        return response()->success(ExceptionsBL::clone($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #REGION UPDATE
    /**
     * Update
     *
     * @param Request DtoExceptions
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoExceptions)
    {
        return response()->success(ExceptionsBL::update($DtoExceptions, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE

    #region DELETE
    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(ExceptionsBL::delete($id, HttpHelper::getLanguageId()));
    }
    #endregion DELETE
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\PlacesBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class PlacesController
{
    #region GET
    /**
     * Get all Places
     *
     * @param int $Places
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        return response()->success(PlacesBL::getAllDto(HttpHelper::getLanguageId()));
    }

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(PlacesBL::getById($id));
    }
 
    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(PlacesBL::getSelect($search));
    }


    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(PlacesBL::getAll($idLanguage));
    }
    #endregion GET

    #region INSERT 
    /**
     * Insert
     *
     * @param Request DtoPlaces
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoPlaces)
    {
        return response()->success(PlacesBL::insert($DtoPlaces, HttpHelper::getLanguageId()));
       
    }
    #endregion INSERT

    /**
     * Clone
     *
     * @param int $id
     * @param int $idFunctionality
     * 
     */
    
    public function clone(int $id, int $idFunctionality){
        return response()->success(PlacesBL::clone($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


 
    #REGION UPDATE
    /**
     * Update
     *
     * @param Request DtoPlaces
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoPlaces)
    {
        return response()->success(PlacesBL::update($DtoPlaces, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE


    #region DELETE
    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(PlacesBL::delete($id, HttpHelper::getLanguageId()));
    }
    #endregion DELETE
}

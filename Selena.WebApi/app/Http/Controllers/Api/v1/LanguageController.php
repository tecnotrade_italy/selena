<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\LanguageBL;
use App\Http\Controllers\Controller;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;

class LanguageController extends Controller
{
    #region GET

    /**
     * Return a default language
     *
     * @return \Illuminate\Http\Response
     */
    public function getDefault()
    {
        return response()->success(LanguageBL::getDefault());
    }

    /**
     * Return a language by id
     *
     * @param int id
     * @return \Illuminate\Http\Response
     */
    public function get($id)
    {
        return response()->success(LanguageBL::get($id));
    }

    /**
     * Return a language for select
     *
     * @param Request  request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);;
        return response()->getSelect(LanguageBL::getSelect($search));
    }


    /**
     * Return a language for select
     *
     * @param Request  request
     * @return \Illuminate\Http\Response
     */
    public function selectlanguagestranslate(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(LanguageBL::selectlanguagestranslate($search));
    }

    /**
     * Return a language for select
     *
     * @param Request  request
     * @return \Illuminate\Http\Response
     */
    public function selectlanguagescategoriestranslate(Request $request)
    {
        $search = trim($request->search);
        $idCat = trim($request->idCategory);
        return response()->getSelect(LanguageBL::selectlanguagescategoriestranslate($search, $idCat));
    }

    /**
     * Return a language for select
     *
     * @param Request  request
     * @return \Illuminate\Http\Response
     */
    public function selectlanguagesitemstranslate(Request $request)
    {
        $search = trim($request->search);
        $idItem = trim($request->idItem);
        return response()->getSelect(LanguageBL::selectlanguagesitemstranslate($search, $idItem));
    }

    /**
     * Return all language
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(LanguageBL::getAll());
    }

    /**
     * Return configuration
     *
     * @return \Illuminate\Http\Response
     */
    public function getConfiguration()
    {
        return response()->success(LanguageBL::getConfiguration());
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(LanguageBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * get Url Translate
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getUrlTranslate(Request $request)
    {
        return response()->success(LanguageBL::getUrlTranslate($request));
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return response()->success(LanguageBL::update($request, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE
}

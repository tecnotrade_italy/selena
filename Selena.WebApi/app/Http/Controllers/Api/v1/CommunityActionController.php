<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoCommunityAction;
use App\BusinessLogic\CommunityActionBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\CommunityAction;
use App\Helpers\LogHelper;


class CommunityActionController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(CommunityActionBL::getById($id));
    }


        /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(CommunityActionBL::getAll());
    }


    #endregion GET
    
    public function insert(Request $dtoCommunityAction)
    {
        return response()->success(CommunityActionBL::insert($dtoCommunityAction,HttpHelper::getUserId()));
    }
    
    public function update(Request $dtoCommunityAction)
    {
        return response()->success(CommunityActionBL::update($dtoCommunityAction));
    }

    public function delete(int $id)
    {
        return response()->success(CommunityActionBL::delete($id));
    }
   
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ProvinceBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\Helpers\LogHelper;

class ProvinceController
{
    #region GET

    /**
     * Get all icon
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(ProvinceBL::getSelect($search));
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(ProvinceBL::getAll($idLanguage));
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(ProvinceBL::getById($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(ProvinceBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoProvince
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoProvince)
    {
        return response()->success(ProvinceBL::update($dtoProvince, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(ProvinceBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

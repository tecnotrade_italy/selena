<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\TechnicalViewBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;

class TechnicalViewController
{
    #region GET

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllProcessingTechnical(int $idUser)
    {
        return response()->success(TechnicalViewBL::getAllProcessingTechnical($idUser));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllQuotesTechnical(int $idUser)
    {
        return response()->success(TechnicalViewBL::getAllQuotesTechnical($idUser));
    }

}

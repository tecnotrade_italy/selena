<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\TipologyBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class TipologyController
{
  
 /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(TipologyBL::getSelect($search));
    }


}
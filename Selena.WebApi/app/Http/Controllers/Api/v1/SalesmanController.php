<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\SalesmanBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class SalesmanController
{
    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(SalesmanBL::getAll($idLanguage));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(SalesmanBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * getAllUsersBySalesman
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function activeUserById(Request $request)
    {
        return response()->success(SalesmanBL::activeUserById($request));
    }

    /**
     * getAllUsersBySalesman
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllUsersBySalesman(Request $request)
    {
        return response()->success(SalesmanBL::getAllUsersBySalesman($request));
    }

    /**
     * insert Salesman Users
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertSalesmanUsers(Request $request)
    {
        return response()->success(SalesmanBL::insertSalesmanUsers($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * remove Salesman Users
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function removeSalesmanUsers(Request $request)
    {
        return response()->success(SalesmanBL::removeSalesmanUsers($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }
    

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoSelenaViews
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoSelenaViews)
    {
        return response()->success(SalesmanBL::update($dtoSelenaViews, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(SalesmanBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\CommunityImageCategoryBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class CommunityImageCategoryController
{
    #region GET

    /**
     * Get all CommunityImageCategory
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(CommunityImageCategoryBL::getSelect($search));
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(CommunityImageCategoryBL::getAll($idLanguage));
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(CommunityImageCategoryBL::getById($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(CommunityImageCategoryBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoCommunityImageCategory
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoCommunityImageCategory)
    {
        return response()->success(CommunityImageCategoryBL::update($dtoCommunityImageCategory, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(CommunityImageCategoryBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoStates;
use App\BusinessLogic\StatesBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\GrStates;


class StatesController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(StatesBL::getById($id));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(StatesBL::getSelect($search));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(StatesBL::getAll($idLanguage));
    }

    #endregion GET
    
    #region INSERT

    /**
     * Insert
     *
     * @param Request DtoStates
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoStates)
    {
        return response()->success(StatesBL::insert($DtoStates, HttpHelper::getUserId()));
       
    }

    #endregion INSERT


 #region INSERTNOAUTH

    /**
     * Insert
     *
     * @param Request DtoStates
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertNoAuth(Request $DtoStates)
    {
        return response()->success(StatesBL::insertNoAuth($DtoStates, HttpHelper::getUserId()));
       
    }

    #endregion INSERTNOAUTH






    #region UPDATE

    /**
     * Update
     *
     * @param Request DtoStates
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoStates)
    {
        return response()->success(StatesBL::update($DtoStates, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

#region UPDATENOAUTH

    /**
     * Update
     *
     * @param Request DtoStates
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateNoAuth(Request $DtoStates)
    {
        return response()->success(StatesBL::updateNoAuth($DtoStates, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE


    #region DELETE

    /**
     * Delete
     *
     * @param int id
     
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(StatesBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

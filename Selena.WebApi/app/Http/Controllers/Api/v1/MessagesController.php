<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\MessageBL;
use App\BusinessLogic\MessageLanguageBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class MessagesController
{
    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(MessageBL::getAll($idLanguage));
    }

     /**
     * Get Select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(MessageBL::getSelect($search));
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(MessageBL::getById($id));
    }

        /**
     * Get by code
     *
     * @param String code
     * @param Int idLanguage
     *
     * @return String
     */

    public function getByCode(string $code)
    {
        return response()->success(MessageBL::getByCode($code));
    }


    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $dtoMessages)
    {
        return response()->success(MessageBL::insert($dtoMessages, HttpHelper::getUserId()));
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoMessages
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoMessages)
    {
        return response()->success(MessageBL::update($dtoMessages, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(MessageBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

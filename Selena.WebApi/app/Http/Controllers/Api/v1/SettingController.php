<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\BusinessLogic\SettingBL;
use Symfony\Component\HttpFoundation\Request;
use App\Helpers\HttpHelper;

class SettingController extends Controller
{
    #region GET

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(SettingBL::getAll());
    }

    /**
     * Get codes
     *
     * @return \Illuminate\Http\Response
     */
    public function getCodes()
    {
        return response()->success(SettingBL::getCodes());
    }

    /**
     * Get last sync
     *
     * @return \Illuminate\Http\Response
     */
    public function getLastSync()
    {
        return response()->success(SettingBL::getLastSync());
    }


    /**
     * Get Render Menu
     *
     * @return \Illuminate\Http\Response
     */
    public function getRenderMenu()
    {
        return response()->success(SettingBL::getRenderMenu());
    }


    /**
     * Get Render blog
     *
     * @return \Illuminate\Http\Response
     */
    public function getRenderBlog()
    {
        return response()->success(SettingBL::getRenderBlog());
    }


    /**
     * Get Render News
     *
     * @return \Illuminate\Http\Response
     */
    public function getRenderNews()
    {
        return response()->success(SettingBL::getRenderNews());
    }


    /**
     * Get by code
     *
     * @param string code
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByCode(string $code)
    {
        return response()->success(SettingBL::getByCode($code));
    }

    #region GET

    #region PUT

    /**
     * Insert
     *
     * @param Request
     *
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(SettingBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion PUT

    #region POST

    /**
     * Update
     *
     * @param Request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return response()->success(SettingBL::update($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    public function updateImageNameFavicon(Request $request)
    {
        return response()->success(SettingBL::updateImageNameFavicon($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion POST

    #region DELETE

    /**
     * Delete
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(SettingBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

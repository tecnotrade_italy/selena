<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\NewsletterBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class NewsletterController
{
    #region GET

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(NewsletterBL::getAll());
    }

     /**
     * getAllCampaign
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllCampaign()
    {
        return response()->success(NewsletterBL::getAllCampaign());
    }


    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(NewsletterBL::getById($id));
    }

     /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllviewlistmailerror(int $id)
    {
        return response()->success(NewsletterBL::getAllviewlistmailerror($id));
    }

         /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllviewlistmailopen(int $id)
    {
        return response()->success(NewsletterBL::getAllviewlistmailopen($id));
    }

 
    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByListDateForId(int $id)
    {
        return response()->success(NewsletterBL::getByListDateForId($id));
    }
 

    #endregion GET

   /**
     * Get getemail
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getemail(int $id)
    {
        return response()->success(NewsletterBL::getemail($id));
    }
     /**
     * Get getListEmailByGroup
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getListEmailByGroup(int $id)
    {
        return response()->success(NewsletterBL::getListEmailByGroup($id));
    }
    

       /**
     * Get getemailusers
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getemailusers(int $id)
    {
        return response()->success(NewsletterBL::getemailusers($id));
    }
    /**
     * Get getListEmailQueryGroupNewsletter
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getListEmailQueryGroupNewsletter(int $id)
    {
        return response()->success(NewsletterBL::getListEmailQueryGroupNewsletter($id));
    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     *
     * @param Request dtoNewsletter
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $dtoNewsletter)
    {
        return response()->success(NewsletterBL::insert($dtoNewsletter, HttpHelper::getLanguageId()));
    }
    #endregion INSERT

  /**
     * Insert
     *
     * @param Request dtoNewsletter
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertCampaign(Request $request)
    {
        return response()->success(NewsletterBL::insertCampaign($request, HttpHelper::getLanguageId()));
    }
    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoNewsletter
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoNewsletter)
    {
        return response()->success(NewsletterBL::update($dtoNewsletter, HttpHelper::getLanguageId()));
    }

        /**
     * confirmSendMailTestSender
     *
     * @param Request dtoNewsletter
     * 
     * @return \Illuminate\Http\Response
     */
    public function confirmSendMailTestSender(Request $request)
    {
        return response()->success(NewsletterBL::confirmSendMailTestSender($request, HttpHelper::getLanguageId()));
    }

       /**
     * updateCampaign
     *
     * @param Request dtoNewsletter
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateCampaign(Request $dtoNewsletter)
    {
        return response()->success(NewsletterBL::updateCampaign($dtoNewsletter, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE

         /**
     * unsubscribefornewsletter
     *
     * @param Request dtoNewsletter
     * 
     * @return \Illuminate\Http\Response
     */
    public function unsubscribefornewsletter(Request $dtoNewsletter)
    {
        return response()->success(NewsletterBL::unsubscribefornewsletter($dtoNewsletter, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE

     /* unsubscribeallfornewsletter
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function unsubscribeallfornewsletter(Request $request)
    {
        return response()->success(NewsletterBL::unsubscribeallfornewsletter($request, HttpHelper::getLanguageId()));
    }
    #endregion unsubscribeallfornewsletter

     public function updateemailnewsletter(Request $request)
    {
        return response()->success(NewsletterBL::updateemailnewsletter($request, HttpHelper::getLanguageId()));
    }


          /**
     * updateOnlineNewsletterCheck
     *
     * @param Request dtoNewsletter
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateOnlineNewsletterCheck(Request $request)
    {
        return response()->success(NewsletterBL::updateOnlineNewsletterCheck($request, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE


          /**
     * clone
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function clone(Request $request)
    {
        return response()->success(NewsletterBL::clone($request, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE

    

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(NewsletterBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

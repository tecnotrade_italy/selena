<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\GroupDisplayTechnicalSpecificationBL;
use App\BusinessLogic\ItemBL;
use App\BusinessLogic\ItemTechnicalSpecificationBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class ItemTechnicalSpecificationController
{
    #region GET

    /**
     * Get by item
     *
     * @param int idItem
     * @param int idFunctionality
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getConfiguration(int $idItem, int $idFunctionality, int $idLanguage = null)
    {
        if (is_null($idLanguage)) {
            $idLanguage = HttpHelper::getLanguageId();
        }

        return response()->success(GroupDisplayTechnicalSpecificationBL::getConfiguration($idItem, $idFunctionality, HttpHelper::getUserId(), $idLanguage));
    }

    /**
     * Get by item
     *
     * @param int idItem
     * @param int version
     * @param int idFunctionality
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getVersionConfiguration(int $idItem, int $version, int $idFunctionality, int $idLanguage = null)
    {
        if (is_null($idLanguage)) {
            $idLanguage = HttpHelper::getLanguageId();
        }

        return response()->success(GroupDisplayTechnicalSpecificationBL::getVersionConfiguration($idItem, $version, $idFunctionality, HttpHelper::getUserId(), $idLanguage));
    }

    /**
     * Get by item
     *
     * @param int $idItem
     * @param int idFunctionality
     * @param int idLanguage
     * @param int version
     * 
     * @return \Illuminate\Http\Response
     */
    public function get(int $idItem, int $idFunctionality, int $idLanguage = null, int $version = null)
    {
        if (is_null($idLanguage)) {
            $idLanguage = HttpHelper::getLanguageId();
        }

        return response()->success(ItemTechnicalSpecificationBL::get($idItem, $idFunctionality, HttpHelper::getUserId(), $idLanguage, $version));
    }

    /**
     * Get version by item
     *
     * @param int idItem
     * @param int idFunctionality
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getVersion(int $idItem, int $idFunctionality, int $idLanguage = null)
    {
        if (is_null($idLanguage)) {
            $idLanguage = HttpHelper::getLanguageId();
        }
        return response()->getSelect(ItemTechnicalSpecificationBL::getVersion($idItem, $idFunctionality, HttpHelper::getUserId(), $idLanguage));
    }

    /**
     * Get completed stat by language
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getCompletedStatByLanguage(int $idLanguage)
    {
        return response()->success(ItemTechnicalSpecificationBL::getCompletedStatByLanguage($idLanguage));
    }

    #endregion GET

    #region PUT

    /**
     * Insert
     * 
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(ItemTechnicalSpecificationBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion PUT

    #region POST

    /**
     * Insert
     * 
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function complete(Request $request)
    {
        return response()->success(ItemBL::complete($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion POST
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\CategoriesPagesBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class CategoriesPagesController
{
    #region GET

    /**
     * Get all nation
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(CategoriesPagesBL::getSelect($search));
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(CategoriesPagesBL::getAll($idLanguage));
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(CategoriesPagesBL::getById($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(CategoriesPagesBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoNation
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoNation)
    {
        return response()->success(CategoriesPagesBL::update($dtoNation, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE
    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(CategoriesPagesBL::delete($id, HttpHelper::getLanguageId()));
    }
    #endregion DELETE
}

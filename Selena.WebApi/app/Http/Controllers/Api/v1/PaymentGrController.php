<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\PaymentGrBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class PaymentGrController
{
  
 /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function selectpaymentGr(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(PaymentGrBL::selectpaymentGr($search));
    }


}
<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\RoleUserBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class RoleUserController
{
    #region PUT

    /**
     * Insert
     *
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(RoleUserBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion PUT

    #region DELETE

    /**
     * Delete
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id, int $idFunctionality)
    {
        return response()->success(RoleUserBL::delete($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

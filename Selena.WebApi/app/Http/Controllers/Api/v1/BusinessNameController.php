<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\BusinessNameBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class BusinessNameController
{
  

 /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(BusinessNameBL::getSelect($search));
    }


      /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllSupplierRegistrySearch(Request $request)
    {
        return response()->success(BusinessNameBL::getAllSupplierRegistrySearch($request));
    }

 /**
     * Get by getById
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(BusinessNameBL::getById($id));
    }
 /**
     * Get by getByIdUp
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByIdUp(Int $id)
    {
        return response()->success(BusinessNameBL::getByIdUp($id));
    }
    
    /**
     * updatedetailsupplierregistry
     *
     * @param Request DtoExternalRepair
     * 
     * @return \Illuminate\Http\Response
     */
    public function updatedetailsupplierregistry(Request $DtoBusinessName)
    {
        return response()->success(BusinessNameBL::updatedetailsupplierregistry($DtoBusinessName, HttpHelper::getLanguageId()));
    }

        /**
     * insertsupplierregistry
     *
     * @param Request DtoExternalRepair
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertsupplierregistry(Request $DtoBusinessName)
    {
        return response()->success(BusinessNameBL::insertsupplierregistry($DtoBusinessName, HttpHelper::getLanguageId()));
    }

       /**
     * Delete
     *
     * @param int id
     
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(BusinessNameBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE


}
<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\SalesOrderBL;
use App\BusinessLogic\UserAddressBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Symfony\Component\HttpFoundation\Request;

class SalesOrderController
{
    

    #region GET
    /**
     * Get address by user id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
     public function getAllBySales($id, int $idLanguage)
    {
    
        return response()->success(SalesOrderBL::getAllBySales($id, $idLanguage));
    }

     /**
     * get All orders By Salesman Id
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getAllBySalesmanId(Request $request)
    {
        return response()->success(SalesOrderBL::getAllBySalesmanId($request));
    }
    #endregion GET

 public function getByDetailSaleOrder(Int $id, int $idLanguage)
    {
        return response()->success(SalesOrderBL::getByDetailSaleOrder($id, $idLanguage));
    }

    public function DetailPopupSelect(Int $id)
    {
        return response()->success(SalesOrderBL::getDetailPopupSelect($id));
    }


  /** 
     * Get getAllSalesOrder
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllSalesOrder()
    {
        return response()->success(SalesOrderBL::getAllSalesOrder()); 
    }
    #endregion GET

  /** 
     * Get getBySalesOrder
     *
     * @return \Illuminate\Http\Response
     */
    public function getBySalesOrderAdmin(Int $id)
    {
        return response()->success(SalesOrderBL::getBySalesOrder($id)); 
    }
    #endregion GET

    
   /**
     * updateSalesOrderDetail
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateSalesOrderDetail(Request $request)
    {
        return response()->success(SalesOrderBL::updateSalesOrderDetail($request, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

 /**
     * updateTrackingShipment
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateTrackingShipment(Request $request)
    {
        return response()->success(SalesOrderBL::updateTrackingShipment($request, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE

    /**
     * updateNote
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateNote(Request $request)
    {
        return response()->success(SalesOrderBL::updateNote($request, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE

    public function saveChangeStatus(Request $request)
    {
        return response()->success(SalesOrderBL::saveChangeStatus($request, HttpHelper::getLanguageId()));
    }


 /**
     * SendMailCustomer
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function SendMailCustomer(Request $request)
    {
        return response()->success(SalesOrderBL::SendMailCustomer($request, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE

     /**
     * SendShipment
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function SendShipment(Request $request)
    {
        return response()->success(SalesOrderBL::SendShipment($request, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE

       /**
     * insertAndSendMessages
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertAndSendMessages(Request $request)
    {
        return response()->success(SalesOrderBL::insertAndSendMessages($request, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE

      public function insertFileOrderSales(Request $request)
    {
        return response()->success(SalesOrderBL::insertFileOrderSales($request, HttpHelper::getLanguageId()));
    }

    /**
     * sendEmailDetailSummaryOrder
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function sendEmailDetailSummaryOrder(Request $request)
    {
        return response()->success(SalesOrderBL::sendEmailDetailSummaryOrder($request, HttpHelper::getLanguageId()));
    }
    

     /**
     * insertMessages
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertMessages(Request $request)
    {
        return response()->success(SalesOrderBL::insertMessages($request, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE



        /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function deleteOrderDetail(int $id)
    {
        return response()->success(SalesOrderBL::deleteOrderDetail($id, HttpHelper::getLanguageId()));
    }

        /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(SalesOrderBL::delete($id, HttpHelper::getLanguageId()));
    }


}

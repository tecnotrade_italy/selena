<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\BusinessLogic\GroupTechnicalSpecificationBL;
use App\BusinessLogic\GroupTechnicalSpecificationLanguageBL;
use App\BusinessLogic\GroupTechnicalSpecificationTechnicalSpecificationBL;
use App\BusinessLogic\ItemGroupTechnicalSpecificationBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;

class GroupTechnicalSpecificationController extends Controller
{
    #region GET

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getDtoTable()
    {
        return response()->success(GroupTechnicalSpecificationBL::getDtoTable());
    }


    /**
     * Get graphic all group technical specification
     *
     * @return \Illuminate\Http\Response
     */
    public function getGraphic()
    {
        return response()->success(GroupTechnicalSpecificationBL::getGraphic());
    }

    /**
     * Get all items included and excluded
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getItems(int $id)
    {
        return response()->success(GroupTechnicalSpecificationBL::getItems($id, HttpHelper::getLanguageId()));
    }

    /**
     * Get all technical specification included and excluded
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getTechnicalSpecification(int $id)
    {
        return response()->success(GroupTechnicalSpecificationBL::getTechnicalSpecification($id, HttpHelper::getLanguageId()));
    }

    /**
     * Check exists
     *
     * @param string description
     * @param int idLanguage
     * @param int idGroupTechnicalSpecification
     *
     * @return \Illuminate\Http\Response
     */
    public function checkExists(string $description, int $idLanguage, int $id = null)
    {
        return response()->success(GroupTechnicalSpecificationLanguageBL::checkExists($description, $idLanguage, $id));
    }

    #endregion GET

    #region PUT

    /**
     * Insert
     *
     * @param Request
     *
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(GroupTechnicalSpecificationBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Add item
     *
     * @param int $id
     * @param int $idItem
     * @param int $idFunctionality
     *
     * @return \Illuminate\Http\Response
     */
    public function addItem(int $id, int $idItem, int $idFunctionality)
    {
        return response()->success(ItemGroupTechnicalSpecificationBL::insert($id, $idItem, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Add technical specification
     *
     * @param int $id
     * @param int $idTechnicalSpecification
     * @param int $idFunctionality
     *
     * @return \Illuminate\Http\Response
     */
    public function addTechnicalSpecification(int $id, int $idTechnicalSpecification, int $idFunctionality)
    {
        return response()->success(GroupTechnicalSpecificationTechnicalSpecificationBL::insert($id, $idTechnicalSpecification, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion PUT

    #region POST

    /**
     * Update
     *
     * @param Request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return response()->success(GroupTechnicalSpecificationBL::update($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion POST

    #region DELETE

    /**
     * delete
     *
     * @param int $id
     * @param int $idFunctionality
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id, int $idFunctionality)
    {
        return response()->success(GroupTechnicalSpecificationBL::delete($id, $idFunctionality,  HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * delete
     *
     * @param int $id
     * @param int $idFunctionality
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteItemGroupTechnicalSpecification(int $id, int $idFunctionality)
    {
        return response()->success(ItemGroupTechnicalSpecificationBL::delete($id, $idFunctionality,  HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * delete
     *
     * @param int $id
     * @param int $idFunctionality
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteGroupTechnicalSpecificationTechnicalSpecification(int $id, int $idFunctionality)
    {
        return response()->success(GroupTechnicalSpecificationTechnicalSpecificationBL::delete($id, $idFunctionality,  HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

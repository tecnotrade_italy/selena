<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\FormBuilderBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class FormBuilderController
{
    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(FormBuilderBL::getAll($idLanguage));
    }

    /**
     * get All Request by form
     * 
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllRequest()
    {
        return response()->success(FormBuilderBL::getAllRequest());
    }

     /**
     * Get all by id
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllById(int $idLanguage, int $id)
    {
        return response()->success(FormBuilderBL::getAllById($idLanguage, $id));
    }

    /**
     * Get Select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(FormBuilderBL::getSelect($search));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(FormBuilderBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Insert Form Builder Field
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertFormBuilderField(Request $request)
    {
        return response()->success(FormBuilderBL::insertFormBuilderField($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Insert contact in form builder
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertContactFormBuilder(Request $request)
    {
        return response()->success(FormBuilderBL::insertContactFormBuilder($request));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoSelenaViews
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoSelenaViews)
    {
        return response()->success(FormBuilderBL::update($dtoSelenaViews, HttpHelper::getLanguageId()));
    }

    /**
     * Update Form Builder Field
     *
     * @param Request dtoSelenaViews
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateFormBuilderField(Request $dtoSelenaViews)
    {
        return response()->success(FormBuilderBL::updateFormBuilderField($dtoSelenaViews, HttpHelper::getLanguageId()));
    }

    /**
     * Clone
     *
     * @param int $id
     * @param int $idFunctionality
     * @return int
     */
    public function clone(int $id, int $idFunctionality)
    {
        return response()->success(FormBuilderBL::clone($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(FormBuilderBL::delete($id, HttpHelper::getLanguageId()));
    }

    /**
     * delBy Contact And Form Id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delByContactAndFormId(int $id, int $idForm)
    {
        return response()->success(FormBuilderBL::delByContactAndFormId($id, $idForm));
    }

    /**
     * Delete Form Builder Field
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteFormBuilderField(int $id)
    {
        return response()->success(FormBuilderBL::deleteFormBuilderField($id, HttpHelper::getLanguageId()));
    }

    /**
     * delete Value Form Builder Field
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteValueFormBuilderField(int $id)
    {
        return response()->success(FormBuilderBL::deleteValueFormBuilderField($id, HttpHelper::getLanguageId()));
    }

    /**
     * delete Value Father Form Builder Field
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteValueFatherFormBuilderField(int $id)
    {
        return response()->success(FormBuilderBL::deleteValueFatherFormBuilderField($id, HttpHelper::getLanguageId()));
    }

    

    #endregion DELETE
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoCategories;
use App\CategoryLanguage;
use App\BusinessLogic\CategoriesBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\Categories;
use App\Helpers\LogHelper;

class CategoriesController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(CategoriesBL::getById($id));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(CategoriesBL::getSelect($search));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelectCategoryItemAttachment(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(CategoriesBL::getSelectCategoryItemAttachment($search));
    }


    /**
     * Get all principal categories
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelectPrincipal(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(CategoriesBL::getSelect($search));
    }
    

    /**
     * Get all sub categories
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelectPrincipalCategories(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(CategoriesBL::getSelectPrincipalCategories($search));
    }

    /**
     * Get all sub categories
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelectSubCategories(Request $request)
    {
        $search = trim($request->search);
        $idCatFather = trim($request->idCatFather);
        return response()->getSelect(CategoriesBL::getSelectSubCategories($idCatFather, $search));
    }


    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(CategoriesBL::getAll($idLanguage));
    }

    /**
     * Get All List Navigation
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllListNavigation(int $idLanguage, int $idItem)
    {
        return response()->success(CategoriesBL::getAllListNavigation($idLanguage, $idItem));
    }

    /**
     * Get All List Navigation Father
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllListNavigationFather(int $idLanguage, int $idItem)
    {
        return response()->success(CategoriesBL::getAllListNavigationFather($idLanguage, $idItem));
    }

    /**
     * Get All List Navigation
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllDetailListCategories(int $idLanguage, int $idItem)
    {
        return response()->success(CategoriesBL::getAllDetailListCategories($idLanguage, $idItem));
    }

    /**
     * Get All List Navigation
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllDetailListCategoriesFather(int $idLanguage, int $idCategories)
    {
        return response()->success(CategoriesBL::getAllDetailListCategoriesFather($idLanguage, $idCategories));
    }

    /**
     * Get List
     *
     * @return \Illuminate\Http\Response
     */
    public function getList(Request $request)
    {
        return response()->success(CategoriesBL::getList($request));
    }

    /**
     * Get categories photos
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getImages(Int $id)
    {
        return response()->success(CategoriesBL::getImages($id));
    }

    /**
     * Get all categories phptos
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllImagesInFolder(int $id)
    {
        return response()->success(CategoriesBL::getAllImagesInFolder($id));
    }

    /**
     * Get Categories Languages
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getCategoriesLanguages(Int $id)
    {
        return response()->success(CategoriesBL::getCategoriesLanguages($id));
    }

    /**
     * Get getByTranslateDefault 
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByTranslateDefault(Int $id, int $idItem, int $idLanguage)
    {
        return response()->success(CategoriesBL::getByTranslateDefault($id, $idItem, $idLanguage));
    }
    #endregion getByTranslateDefault

    /**
     * Get getByTranslateDefaultNew
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByTranslateDefaultNew(int $idItem, int $idLanguage)
    {
        return response()->success(CategoriesBL::getByTranslateDefaultNew($idItem, $idLanguage));
    }
    #endregion getByTranslateDefault

    /**
     * Get getByTranslate 
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByTranslate(Int $id)
    {
        return response()->success(CategoriesBL::getByTranslate($id));
    }
    
    #endregion GET
    
    #region INSERT

    /**
     * Insert
     *
     * @param Request DtoCategories
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoCategories)
    {
        return response()->success(CategoriesBL::insert($DtoCategories, HttpHelper::getUserId()));
       
    }

    /**
     * insert Categories Father
     *
     * @param Request DtoItems
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertCategoriesFather(Request $request)
    {
        return response()->success(CategoriesBL::insertCategoriesFather($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * insert Category ItemWithout Category
     *
     * @param Request DtoItems
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertCategoryItemWithoutCategory(Request $request)
    {
        return response()->success(CategoriesBL::insertCategoryItemWithoutCategory($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Insert related
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertTranslate(Request $request)
    {
        return response()->success(CategoriesBL::insertTranslate($request, HttpHelper::getUserId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request DtoCategories
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoCategories)
    {
        return response()->success(CategoriesBL::update($DtoCategories));
    }

    /**
     * update Order Category Item
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateOrderCategoryItem(Request $request)
    {
        return response()->success(CategoriesBL::updateOrderCategoryItem($request));
    }
    
    /**
     * update Order Category father
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateOrderCategoryFather(Request $request)
    {
        return response()->success(CategoriesBL::updateOrderCategoryFather($request));
    }

     /**
     * update Order Categories Table
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateOrderCategoriesTable(Request $request)
    {
        return response()->success(CategoriesBL::updateOrderCategoriesTable($request));
    }

    /**
     * update categories Item
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateOnlineCategories(Request $request)
    {
        return response()->success(CategoriesBL::updateOnlineCategories($request));
    }

    /**
     * Update img name
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateImageName(Request $request)
    {
        return response()->success(CategoriesBL::updateImageName($request));
    }

    /**
     * update Categories Images
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateCategoriesImages(Request $request)
    {
        return response()->success(CategoriesBL::updateCategoriesImages($request));
    }

    /**
     * updateTranslate
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */

    public function updateTranslate(Request $request)
    {
        return response()->success(CategoriesBL::updateTranslate($request));
    }

    #endregion UPDATE

/**
     * Clone
     *
     * @param int $id
     * @param int $idFunctionality
     * 
     */
    
    public function clone(int $id, int $idFunctionality){
        return response()->success(CategoriesBL::clone($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }
   
    #endregion Clone

    #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(CategoriesBL::delete($id, HttpHelper::getLanguageId()));
    }

    /**
     * Delete category father
     *
     * @param int $idCategories
     * @param int $idCategoriesFather
     * @return int
     */
    public function deleteCategoriesFather(int $idCategories, int $idCategoriesFather)
    {
        return response()->success(CategoriesBL::deleteCategoriesFather($idCategories, $idCategoriesFather, HttpHelper::getLanguageId()));
    }

    /**
     * Delete Cancel Translate
     *
     * @param int $id
     * @return int
     */
    public function getCancelTranslate(int $id)
    {
        return response()->success(CategoriesBL::getCancelTranslate($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\NegotiationBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class NegotiationController
{
    #region GET

    /**
     * Get by user
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByUser(int $id)
    {
        return response()->success(NegotiationBL::getByUser($id));
    }

    /**
     * Get detail by negotiation id
     *
     * @param int id
     * @param int idUser
     * 
     * @return \Illuminate\Http\Response
     */
    public function getDetailById(int $id, int $idUser)
    {
        return response()->success(NegotiationBL::getDetailById($id, $idUser));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(NegotiationBL::insert($request, HttpHelper::getLanguageId()));
    }

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertDetail(Request $request)
    {
        return response()->success(NegotiationBL::insertDetail($request, HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(NegotiationBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

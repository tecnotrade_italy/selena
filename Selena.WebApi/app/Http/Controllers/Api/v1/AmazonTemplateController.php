<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoAmazonTemplate;
use App\BusinessLogic\AmazonTemplateBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\AmazonTemplate;
use App\Helpers\LogHelper;


class AmazonTemplateController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(AmazonTemplateBL::getById($id));
    }


        /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(AmazonTemplateBL::getAll());
    }

    
    public function getByCode(string $code)
    {
        return response()->success(AmazonTemplateBL::getByCode($code));
    }

    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(AmazonTemplateBL::getSelect($search));
    }

    #endregion GET
    
    public function insert(Request $dtoAmazonTemplate)
    {
        return response()->success(AmazonTemplateBL::insert($dtoAmazonTemplate,HttpHelper::getUserId()));
    }
    
    public function update(Request $dtoAmazonTemplate)
    {
        return response()->success(AmazonTemplateBL::update($dtoAmazonTemplate));
    }

    public function delete(int $id)
    {
        return response()->success(AmazonTemplateBL::delete($id));
    }
   
}

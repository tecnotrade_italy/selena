<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\DictionaryBL;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VideoController
{
    #region PRIVATE

    private static $dirVideo = 'public/videos';
    private static $mimeTypeAllowed = ['video/m4v', 'video/mp4', 'video/webm', 'video/ogm', 'video/ogv'];

    /**
     * Get url
     */
    private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/videos/';
    }

    #endregion PRIVATE

    #region POST

    /**
     * Add video
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        if (!in_array($request->file->getClientMimeType(), static::$mimeTypeAllowed)) {
            return response()->error(DictionaryBL::getTranslate(HttpHelper::getLanguageId(), DictionariesCodesEnum::FileTypeNotAllowed), HttpResultsCodesEnum::InvalidPayload);
        } else {
            $fileName = $request->file->getClientOriginalName();

            if (Storage::disk('public')->exists('videos/' . $fileName)) {
                return response()->error(DictionaryBL::getTranslate(HttpHelper::getLanguageId(), DictionariesCodesEnum::NameFileAlreadyExists), HttpResultsCodesEnum::NotAcceptable);
            } else {
                $request->file->storeAs(static::$dirVideo, $fileName);
                return response()->link(static::getUrl() . $request->file->getClientOriginalName());
            }
        }
    }

    #endregion POST
}

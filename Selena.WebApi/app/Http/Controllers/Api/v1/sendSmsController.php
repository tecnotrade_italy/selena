<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\sendSmsBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class sendSmsController
{
  
    #region POST
    /**
     * Insert
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertSms(Request $request)
    {
        return response()->success(sendSmsBL::insertSms($request));
    }
    #endregion POST

    #region GET

    /**
     * Get number by contact id
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getnumbertelephone(Int $id)
    {
        return response()->success(sendSmsBL::getnumbertelephone($id));
    }

    /**
     * Get list contact by group id
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getListContactByGroup(Int $id)
    {
        return response()->success(sendSmsBL::getListContactByGroup($id));
    }

     public function getListTelephoneQueryGroup(Int $id)
    {
        return response()->success(sendSmsBL::getListTelephoneQueryGroup($id));
    }

    


        /**
     * Get list contact by group id
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getListContactByUser(Int $id)
    {
        return response()->success(sendSmsBL::getListContactByUser($id));
    }


    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(sendSmsBL::getAll());
    }
    #endregion GET

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(sendSmsBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}
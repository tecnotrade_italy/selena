<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\CartDetailBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class CartDetailController
{
    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(CartDetailBL::getAll());
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(CartDetailBL::getById($id));
    }

    public function getAllByCart(int $idCart)
    {
        return response()->success(CartDetailBL::getAllByCart($idCart));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertRequest(Request $request)
    {
        return response()->success(CartDetailBL::insertRequest($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoNation
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoNation)
    {
        return response()->success(CartDetailBL::update($dtoNation, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(CartDetailBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE

    public function modifyQuantity(Request $request){
        return response()->success(CartDetailBL::modifyQuantity($request, HttpHelper::getLanguageId()));
    }

}

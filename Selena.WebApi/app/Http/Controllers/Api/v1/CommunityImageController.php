<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoCommunityImage;
use App\BusinessLogic\CommunityImageBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\CommunityImage;
use App\Helpers\LogHelper;


class CommunityImageController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(CommunityImageBL::getById($id));
    }

    /**
     * get count upload images by user id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function countUserUpload(Int $id)
    {
        return response()->success(CommunityImageBL::countUserUpload($id));
    }

    public function getImages(Int $id)
    {
        return response()->success(CommunityImageBL::getImages($id));
    }

    public function updateImageName(Request $request)
    {
        return response()->success(CommunityImageBL::updateImageName($request));
    }


    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(CommunityImageBL::getAll());
    }

    public function getGallery(string $type)
    {
        return response()->success(CommunityImageBL::getGallery($type));
    }

    /**
     * get image by paginator
     *
     * @return \Illuminate\Http\Response
     */
    public function getGalleryPaginator(Request $request)
    {
        return response()->success(CommunityImageBL::getGalleryPaginator($request->type, $request->idUser, $request->idCategoryImage, $request->idImageToExclude, $request->limit, $request->idCamera, $request->idLens, $request->page));
    }

    public function getAllSearch(Request $request)
    {
        return response()->success(CommunityImageBL::getAllSearch($request));
    }

    /**
     * delete Image community
     *
     * @return \Illuminate\Http\Response
     */
     public function deleteImage(Request $request)
     {
         return response()->success(CommunityImageBL::deleteImage($request));
     }
    

    #endregion GET

    public function insert(Request $dtoCommunityImage)
    {
        return response()->success(CommunityImageBL::insert($dtoCommunityImage, HttpHelper::getUserId()));
    }

    
    public function insertNewImages(Request $dtoCommunityImage)
    {
        return response()->success(CommunityImageBL::insertNewImages($dtoCommunityImage, HttpHelper::getUserId()));
    }

    public function insertToWebP(Request $dtoCommunityImage)
    {
        return response()->success(CommunityImageBL::insertToWebP($dtoCommunityImage, HttpHelper::getUserId()));
    }
    
        
    public function update(Request $dtoCommunityImage)
    {
        return response()->success(CommunityImageBL::update($dtoCommunityImage));
    }

     public function updateOnlineCheckImages(Request $dtoCommunityImage)
    {
        return response()->success(CommunityImageBL::updateOnlineCheckImages($dtoCommunityImage));
    }
    
    public function getPreviousNextUrlByType(Request $dtoCommunityImage)
    {
        return response()->success(CommunityImageBL::getPreviousNextUrlByType($dtoCommunityImage));
    }

    public function delete(int $id)
    {
        return response()->success(CommunityImageBL::delete($id));
    }
   
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\RoleBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;

class RoleController
{
    #region GET

    /**
     * Get by user
     *
     * @return \Illuminate\Http\Response
     */
    public function getByUser(int $idUser, int $idFunctionality)
    {
        return response()->success(RoleBL::getByUser($idUser, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

        /**
     * getAllByRole
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllByRole(Request $request)
    {
        return response()->success(RoleBL::getAllByRole($request));
    }
    #endregion GET

    /**
     * getUserName
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getUserName(Request $request)
    {
        return response()->success(RoleBL::getUserName($request));
    }
    #endregion GET

    

    

    /**
     * insertRole
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertRole(Request $request)
    {
        return response()->success(RoleBL::insertRole($request));
    }

     /**
     * Delete
     *
     * @param int $idCategories
     * @param int $idItem
     * @return int
     */
    public function delRoleUser(int $idRoleUser, int $user_id)
    {
        return response()->success(RoleBL::delRoleUser($idRoleUser, $user_id, HttpHelper::getLanguageId()));
    }

    

}

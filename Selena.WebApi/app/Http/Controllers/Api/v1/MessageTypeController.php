<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoMessageType;
use App\BusinessLogic\MessageTypeBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\MessageType;
use App\Helpers\LogHelper;


class MessageTypeController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(MessageTypeBL::getById($id));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(MessageTypeBL::getSelect($search));
    }


    #endregion GET
    
    #region INSERT

   
}

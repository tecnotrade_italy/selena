<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\PriceListGeneratorBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;

class PriceListGeneratorController
{
    #region GET
 
    /**
     * Get all cities
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(PriceListGeneratorBL::getSelect($search));
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(PriceListGeneratorBL::getAll($idLanguage));
    }

    public function getAllByUser(Request $dtoListGenerator)
    {
        return response()->success(PriceListGeneratorBL::getAllByUser($dtoListGenerator, HttpHelper::getLanguageId()));
    }

     public function getAllByGroup(Request $dtoListGenerator)
    {
        return response()->success(PriceListGeneratorBL::getAllByGroup($dtoListGenerator, HttpHelper::getLanguageId()));
    }
    public function getAllByCategories(Request $dtoListGenerator)
    {
        return response()->success(PriceListGeneratorBL::getAllByCategories($dtoListGenerator, HttpHelper::getLanguageId()));
    }

    public function getAllByCategoriesNewList(Request $dtoListGenerator)
    {
        return response()->success(PriceListGeneratorBL::getAllByCategoriesNewList($dtoListGenerator, HttpHelper::getLanguageId()));
    }
    


    public function getAllByDestinationUsers(Request $dtoListGenerator)
    {
        return response()->success(PriceListGeneratorBL::getAllByDestinationUsers($dtoListGenerator, HttpHelper::getLanguageId()));
    }
    public function getAllByDestinationGroup(Request $dtoListGenerator)
    {
        return response()->success(PriceListGeneratorBL::getAllByDestinationGroup($dtoListGenerator, HttpHelper::getLanguageId()));
    }
    public function getAllByDestinationCategories(Request $dtoListGenerator)
    {
        return response()->success(PriceListGeneratorBL::getAllByDestinationCategories($dtoListGenerator, HttpHelper::getLanguageId()));
    }

    public function getByExecuteResult(Request $dtoListGenerator)
    {
        return response()->success(PriceListGeneratorBL::getByExecuteResult($dtoListGenerator, HttpHelper::getLanguageId()));
    }

     public function updateOnlineCheck(Request $request)
    {
        return response()->success(PriceListGeneratorBL::updateOnlineCheck($request));
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(PriceListGeneratorBL::getById($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $dtoListGenerator)
    {
        return response()->success(PriceListGeneratorBL::insert($dtoListGenerator, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

  
    


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoCities
     * 
     * @return \Illuminate\Http\Response 
     */
    public function update(Request $dtoListGenerator)
    {
        return response()->success(PriceListGeneratorBL::update($dtoListGenerator, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(PriceListGeneratorBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE



}

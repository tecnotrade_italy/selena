<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\StatesQuotesGrBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class StatesQuotesGrController
{
  
 /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function selectStatesQuotesGr(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(StatesQuotesGrBL::selectStatesQuotesGr($search));
    }


}
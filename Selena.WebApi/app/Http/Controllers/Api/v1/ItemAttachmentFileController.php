<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ItemAttachmentFileControllerBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class ItemAttachmentFileController
{
    #region GET

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(ItemAttachmentFileControllerBL::getSelect($search));
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(ItemAttachmentFileControllerBL::getAll($idLanguage));
    }
    

    /**
     * get detail info by id
     * 
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getDetail(int $id, int $ln)
    {
        return response()->success(ItemAttachmentFileControllerBL::getDetail($id, $ln));
    }


       /**
     *  getAllByAttachmentUser
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllByAttachmentUser(Request $request)
    {
        return response()->success(ItemAttachmentFileControllerBL::getAllByAttachmentUser($request));
    }
  


    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(ItemAttachmentFileControllerBL::getById($id));
    }

    /**
     * Get Item By Id Attachment
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getItemByIdAttachment(Request $request)
    {
        return response()->success(ItemAttachmentFileControllerBL::getItemByIdAttachment($request));
    }

    /**
     * Get getByFile 
     *
     * @param request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByFile(request $request)
    {
        return response()->success(ItemAttachmentFileControllerBL::getByFile($request));
    }

     /**
     * delete From Item Attachment File Item
     *
     * @param request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteFromItemAttachmentFileItem(request $request)
    {
        return response()->success(ItemAttachmentFileControllerBL::deleteFromItemAttachmentFileItem($request));
    }

    

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(ItemAttachmentFileControllerBL::insert($request));
    }

    /**
     * insert From Albero
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertFromAlbero(Request $request)
    {
        return response()->success(ItemAttachmentFileControllerBL::insertFromAlbero($request));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoSelenaViews
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoSelenaViews)
    {
        return response()->success(ItemAttachmentFileControllerBL::update($dtoSelenaViews, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(ItemAttachmentFileControllerBL::delete($id, HttpHelper::getLanguageId()));
    }

    /**
     * delete translate by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delTranslation(int $id)
    {
        return response()->success(ItemAttachmentFileControllerBL::delTranslation($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

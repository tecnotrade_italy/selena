<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\CustomerBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class CustomerController
{
    #region GET

    /**
     * Get before ddt sixten
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelectBeforeDDTSixten(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(CustomerBL::getSelectBeforeDDTSixten($search, $request->idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }
    
  /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(CustomerBL::getSelect($search));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(CustomerBL::getAll());
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(CustomerBL::getById($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @param Request dtoContact
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $dtoContact)
    {
        return response()->success(CustomerBL::insert($dtoContact, HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoContact
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoContact)
    {
        return response()->success(CustomerBL::update($dtoContact, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(CustomerBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

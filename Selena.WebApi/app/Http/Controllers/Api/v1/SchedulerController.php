<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\SchedulerBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class SchedulerController
{
    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(SchedulerBL::getAll($idLanguage));
    }

    /**
     * get type Scheduler
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function typeScheduler(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(SchedulerBL::typeScheduler($search));
    }

    /**
     * get repeat Hour Scheduler
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function repeatHourScheduler(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(SchedulerBL::repeatHourScheduler($search));
    }

    /**
     * get repeat Minutes Scheduler
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function repeatMinutesScheduler(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(SchedulerBL::repeatMinutesScheduler($search));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(SchedulerBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoSelenaViews
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoSelenaViews)
    {
        return response()->success(SchedulerBL::update($dtoSelenaViews, HttpHelper::getLanguageId()));
    }

    /**
     * update Check Active
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateCheckActive(Request $request)
    {
        return response()->success(SchedulerBL::updateCheckActive($request, HttpHelper::getLanguageId()));
    }

    /**
     * update Check Notification
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateCheckNotification(Request $request)
    {
        return response()->success(SchedulerBL::updateCheckNotification($request, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(SchedulerBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\GroupNewsletterBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class GroupNewsletterController extends Controller
{
    #region GET

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(GroupNewsletterBL::getAll());
    }

      /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(GroupNewsletterBL::getSelect($search));
    }


    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(GroupNewsletterBL::getById($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @param Request dtoContact
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $dtoContact)
    {
        return response()->success(GroupNewsletterBL::insert($dtoContact, HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoContact
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoContact)
    {
        return response()->success(GroupNewsletterBL::update($dtoContact, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(GroupNewsletterBL::delete($id, HttpHelper::getLanguageId()));
    }
    #endregion DELETE

           /**
     * getAllByRole
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllByUser(Request $request)
    {
        return response()->success(GroupNewsletterBL::getAllByUser($request));
    }
    #endregion GET

    public function getAllByUserGroup(Request $request)
    {
        return response()->success(GroupNewsletterBL::getAllByUserGroup($request));
    }

           /**
     * getAllByRole
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllByContact(Request $request)
    {
        return response()->success(GroupNewsletterBL::getAllByContact($request));
    }
    #endregion GET

      /**
     * insertUser
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertUser(Request $request)
    {
        return response()->success(GroupNewsletterBL::insertUser($request));
    }

    public function insertGroupUser(Request $request)
    {
        return response()->success(GroupNewsletterBL::insertGroupUser($request));
    }

    
    /**
     * getUserSearch
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getUserSearch(Request $request)
    {
        return response()->success(GroupNewsletterBL::getUserSearch($request));
    }


       /**
     * Delete
     *
     * @param int $idCategories
     * @param int $idItem
     * @return int
     */
    public function delUser(int $IdCustomer, int $rowIdSelected)
    {
        return response()->success(GroupNewsletterBL::delUser($IdCustomer, $rowIdSelected, HttpHelper::getLanguageId()));
    }

    public function delGroupUser(int $idGroupUser, int $rowIdSelected)
    {
        return response()->success(GroupNewsletterBL::delGroupUser($idGroupUser, $rowIdSelected, HttpHelper::getLanguageId()));
    }

       /**
     * insertUser
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertContact(Request $request)
    {
        return response()->success(GroupNewsletterBL::insertContact($request));
    }
       /**
     * Delete
     *
     * @param int $idCategories
     * @param int $idItem
     * @return int
     */
    public function delContact(int $IdContact, int $rowIdSelected)
    {
        return response()->success(GroupNewsletterBL::delContact($IdContact, $rowIdSelected, HttpHelper::getLanguageId()));
    }

}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\LanguagePageShareTypeBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;

class LanguagePageShareTypeController
{
    #region GET

    /**
     * Get all page share type
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(LanguagePageShareTypeBL::getSelect($search));
    }

    /**
     * Get page share type by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(LanguagePageShareTypeBL::getById($id));
    }


    #endregion GET
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoCommunityImageReport;
use App\BusinessLogic\CommunityImageReportBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\CommunityImageReport;
use App\Helpers\LogHelper;


class CommunityImageReportController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(CommunityImageReportBL::getById($id));
    }


        /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(CommunityImageReportBL::getAll());
    }


    #endregion GET
    
    public function insert(Request $dtoCommunityImageReport)
    {
        return response()->success(CommunityImageReportBL::insert($dtoCommunityImageReport,HttpHelper::getUserId()));
    }
    
    public function update(Request $dtoCommunityImageReport)
    {
        return response()->success(CommunityImageReportBL::update($dtoCommunityImageReport));
    }

    public function delete(int $id)
    {
        return response()->success(CommunityImageReportBL::delete($id));
    }
   
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\CarrierTypeBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class CarrierTypeController
{
    #region GET

    public function selectCarrierType(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(CarrierTypeBL::selectCarrierType($search));
    }

    #END region GET
}
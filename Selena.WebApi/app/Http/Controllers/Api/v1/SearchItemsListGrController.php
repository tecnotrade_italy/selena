<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\SearchItemsGrListBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class SearchItemsListGrController
{
    
     /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function searchImplementedItemsGr(Request $request)
    {
        return response()->success(SearchItemsGrListBL::searchImplementedItemsGr($request));
    }
    #endregion POST
    

      /**
     * Get by getResultDetail
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getResultDetail(Int $id)
    {
        return response()->success(SearchItemsGrListBL::getResultDetail($id));
    }


  /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function searchImplementedItemsGlobal(Request $request)
    {
        return response()->success(SearchItemsGrListBL::searchImplementedItemsGlobal($request));
    }
    #endregion POST

}

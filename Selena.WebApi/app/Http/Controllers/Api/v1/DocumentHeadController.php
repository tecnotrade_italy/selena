<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\DocumentHeadBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class DocumentHeadController
{
    #region GET

    /**
     * Get sixten confirmed
     *
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getSixtenOrderConfirmed(int $idFunctionality)
    {
        return response()->success(DocumentHeadBL::getSixtenOrderConfirmed($idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get MdMicrodetectors shipping list
     *
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getMdMicrodetectorsShippingList(int $idFunctionality)
    {
        return response()->success(DocumentHeadBL::getMdMicrodetectorsShippingList($idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get month of Before DDT Sixten
     * 
     * @param int idCustomer
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getSelectMonthBeforeDDTSixten(int $idCustomer, int $idFunctionality)
    {
        return response()->getSelect(DocumentHeadBL::getSelectMonthBeforeDDTSixten($idCustomer, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion GET

    #region UPDATE

    /**
     * Confirm before DDT Sixten
     * 
     * @param \Illuminate\Http\Request
     * 
     * @return \Illuminate\Http\Response
     */
    public function confirmBeforeDDTSixten(Request $request)
    {
        return response()->success(DocumentHeadBL::confirmBeforeDDTSixten($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion UPDATE
}

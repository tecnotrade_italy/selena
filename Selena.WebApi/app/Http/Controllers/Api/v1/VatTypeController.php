<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\VatTypeBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;

class VatTypeController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(VatTypeBL::getById($id));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(VatTypeBL::getSelect($search));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(VatTypeBL::getAll());
    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     *
     * @param Request dtoVatType
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $dtoVatType)
    {
        return response()->success(VatTypeBL::insert($dtoVatType, HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoVatType
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoVatType)
    {
        return response()->success(VatTypeBL::update($dtoVatType, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(VatTypeBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\DictionaryBL;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Helpers\LogHelper;

class FileController
{
    #region PRIVATE

    private static $dirFile = 'public/files';
    private static $mimeTypeAllowed = ['application/pdf', 'application/x-zip-compressed', 'application/octet-stream', 'text/plain', 'application/zip', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

    /**
     * Get url
     */
    private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/files/';
    }

    #endregion PRIVATE

    #region POST

    /**
     * Add file
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        if (!in_array($request->file->getClientMimeType(), static::$mimeTypeAllowed)) {
            return response()->error(DictionaryBL::getTranslate(HttpHelper::getLanguageId(), DictionariesCodesEnum::FileTypeNotAllowed), HttpResultsCodesEnum::InvalidPayload);
        } else {
            $fileName = $request->file->getClientOriginalName();

            if (Storage::disk('public')->exists('files/' . $fileName)) {

                //return response()->error(DictionaryBL::getTranslate(HttpHelper::getLanguageId(), DictionariesCodesEnum::NameFileAlreadyExists), HttpResultsCodesEnum::NotAcceptable);
                Storage::disk('public')->delete('files/' . $fileName);
                $request->file->storeAs(static::$dirFile, $fileName);
                return response()->link(static::getUrl() . $request->file->getClientOriginalName());
            } else {
                $request->file->storeAs(static::$dirFile, $fileName);
                return response()->link(static::getUrl() . $request->file->getClientOriginalName());
            }
        }
    }

    /**
     * check Upload File Editor
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function checkUploadFileEditor(Request $request)
    {
        if (Storage::disk('public')->exists('files/' . $request->nameFile)) {
            return response()->success('File presente');
        } else {
            return response()->success('');
        }
    }

    

    #endregion POST
}

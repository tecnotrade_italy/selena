<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ListKeywordsBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class ListKeywordsController
{
    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(ListKeywordsBL::getAll($idLanguage));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(ListKeywordsBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoSelenaViews
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return response()->success(ListKeywordsBL::update($request, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(ListKeywordsBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

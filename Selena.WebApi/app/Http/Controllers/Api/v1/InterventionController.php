<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\InterventionBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;

class InterventionController
{
    #region GET

    /**
     * Get all items
     *
     * @param int $idItem
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        return response()->success(InterventionBL::getAllDto(HttpHelper::getLanguageId()));
    }


    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(InterventionBL::getById($id));
    }

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByViewValue(Int $id)

    {
        return response()->success(InterventionBL::getByViewValue($id));
    }

      /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByInterventionFU(Int $id)

    {
        return response()->success(InterventionBL::getByInterventionFU($id));
    }

    
     /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByModuleGuarantee(Int $id)

    {
        return response()->success(InterventionBL::getByModuleGuarantee($id));
    }

        /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByFaultModule(Int $id)
    {
        return response()->success(InterventionBL::getByFaultModule($id));
    }
    

     /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function SearchOpen(Request $request)
    {
        return response()->success(InterventionBL::SearchOpen($request));
    }

    /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function SearchClose(Request $request)
    {
        return response()->success(InterventionBL::SearchClose($request));
    }

 /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function SearchTrue(Request $request)
    {
        return response()->success(InterventionBL::SearchTrue($request));
    }
    /**
     * Get by getByViewQuotes
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByViewQuotes(Int $id, int $idUser)
    {
        return response()->success(InterventionBL::getByViewQuotes($id, $idUser));
    }

    /**
     * Get by getByViewExternalRepair
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByViewExternalRepair(Int $id)
    {
        return response()->success(InterventionBL::getByViewExternalRepair($id));
    }

   /**
     * Get by getBySelectReferent
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getBySelectReferent(Int $id)
    {
        return response()->success(InterventionBL::getBySelectReferent($id));
    }

    /**
     * getByIdUser
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByIdUser(Int $user_id)
    {
        return response()->success(InterventionBL::getByIdUser($user_id));
    }

    public function getDescriptionAndPriceProcessingSheet(Int $id)
    {
        return response()->success(InterventionBL::getDescriptionAndPriceProcessingSheet($id));
    }

     public function getnumbertelephone(Int $id)
    {
        return response()->success(InterventionBL::getnumbertelephone($id));
    }
      public function getdescription(Int $id)
    {
        return response()->success(InterventionBL::getdescription($id));
    }
  

    /**
     * Get Last id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getLastId()
    {
        return response()->success(InterventionBL::getLastId());
    }

     /*** Get getLastIdLib
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getLastIdLib()
    {
        return response()->success(InterventionBL::getLastIdLib());
    }

    

   

    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(InterventionBL::getSelect($search));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(InterventionBL::getAll($idLanguage));
    }

    #endregion GET


    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllIntervention(int $idLanguage)
    {
        return response()->success(InterventionBL::getAllIntervention($idLanguage));
    }

    #endregion GET


    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllRepairArriving(int $idLanguage)
    {
        return response()->success(InterventionBL::getAllRepairArriving($idLanguage));
    }

      /**
     *  getAllRepairArrivedNumberMenuGlobal
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllRepairArrivedNumberMenuGlobal(int $idLanguage)
    {
        return response()->success(InterventionBL::getAllRepairArrivedNumberMenuGlobal($idLanguage));
    }

    #region INSERT

    /**
     * Insert
     *
     * @param Request DtoInterventions
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoInterventions)
    {
        return response()->success(InterventionBL::insert($DtoInterventions, HttpHelper::getUserId()));
    }

    /**
     * Clone
     *
     * @param int $id
     * @param int $idFunctionality
     * 
     */

    public function clone(int $id, int $idFunctionality)
    {
        return response()->success(InterventionBL::clone($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request DtoInterventions
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoInterventions)
    {
        return response()->success(InterventionBL::update($DtoInterventions, HttpHelper::getLanguageId()));
    }

     /**
     * Update
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateInterventionFU(Request $request)
    {
        return response()->success(InterventionBL::updateInterventionFU($request, HttpHelper::getLanguageId()));
    }


    #endregion UPDATE

    /**
     * UpdateIntervention
     *
     * @param Request DtoInterventions
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateIntervention(Request $DtoInterventions)
    {
        return response()->success(InterventionBL::updateIntervention($DtoInterventions, HttpHelper::getLanguageId()));
    }
    #endregion UpdateIntervention
    
       /**
     * UpdateIntervention
     *
     * @param Request DtoInterventions
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateSelectReferent(Request $DtoInterventions)
    {
        return response()->success(InterventionBL::updateSelectReferent($DtoInterventions, HttpHelper::getLanguageId()));
    }
    #endregion updateSelectReferent
    
        /**
     * insertModule
     *
     * @param Request DtoInterventions
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertModule(Request $DtoInterventions)
    {
        return response()->success(InterventionBL::insertModule($DtoInterventions, HttpHelper::getLanguageId()));
    }
    #endregion UpdateIntervention

    /**
     * updateInterventionUpdateProcessingSheet
     *
     * @param Request DtoInterventions
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateInterventionUpdateProcessingSheet(Request $DtoInterventions)
    {
        return response()->success(InterventionBL::updateInterventionUpdateProcessingSheet($DtoInterventions, HttpHelper::getLanguageId()));
    }

    #endregion updateInterventionUpdateProcessingSheet


    /**
     * updateInterventionUpdateViewQuotes
     *
     * @param Request DtoInterventions
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateInterventionUpdateViewQuotes(Request $DtoQuotes)
    {
        return response()->success(InterventionBL::updateInterventionUpdateViewQuotes($DtoQuotes, HttpHelper::getLanguageId()));
    }

    #endregion updateInterventionUpdateViewQuotes

    /**
     * updateExternalRepair
     *
     * @param Request DtoExternalRepair
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateExternalRepair(Request $DtoExternalRepair)
    {
        return response()->success(InterventionBL::updateExternalRepair($DtoExternalRepair, HttpHelper::getLanguageId()));
    }

     /**
     * updateExternalRepair
     *
     * @param Request DtoExternalRepair
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateCheck(Request $DtoInterventions)
    {
        return response()->success(InterventionBL::updateCheck($DtoInterventions, HttpHelper::getLanguageId()));
    }

   /**
     * updateCheckRepairArrived
     *
     * @param Request DtoExternalRepair
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateCheckRepairArrived(Request $DtoInterventions)
    {
        return response()->success(InterventionBL::updateCheckRepairArrived($DtoInterventions, HttpHelper::getLanguageId()));
    }  
    
 

    /**
     * updateUser
     *
     * @param Request DtoUser
     * 
     * @return \Illuminate\Http\Response
     */

    public function updateUser(Request $DtoUser)
    {
        return response()->success(InterventionBL::updateUser($DtoUser, HttpHelper::getLanguageId()));
    }
    #endregion updateUser

    #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(InterventionBL::delete($id, HttpHelper::getLanguageId()));
    }
    #endregion DELETE
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\AutomationBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;

class AutomationController
{
    #region GET

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(AutomationBL::getAll());
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(AutomationBL::insert($request, HttpHelper::getUserId()));
       
    }

    #endregion INSERT

     #region UPDATE

    /**
     * Update
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return response()->success(AutomationBL::update($request, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE
}

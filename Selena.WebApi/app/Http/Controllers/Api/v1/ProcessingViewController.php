<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ProcessingViewBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;

class ProcessingViewController
{
    #region GET

    /**
     * getByViewValue
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByViewValue(Int $id)

    {
        return response()->success(ProcessingViewBL::getByViewValue($id));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllProcessingView(int $idLanguage)
    {
        return response()->success(ProcessingViewBL::getAllProcessingView($idLanguage));
    }

    #endregion GET

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllProcessingSearch(int $idLanguage)
    {
        return response()->success(ProcessingViewBL::getAllProcessingSearch($idLanguage));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllViewQuoteSearch(int $idLanguage)
    {
        return response()->success(ProcessingViewBL::getAllViewQuoteSearch($idLanguage));
    }

    public function getSearchFdl(Request $request)
    {
        return response()->success(ProcessingViewBL::getSearchFdl($request));
    }

    /**
     * updateInterventionUpdateProcessingSheet
     *
     * @param Request DtoInterventions
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateInterventionUpdateProcessingSheet(Request $DtoInterventions)
    {
        return response()->success(ProcessingViewBL::updateInterventionUpdateProcessingSheet($DtoInterventions, HttpHelper::getLanguageId()));
    }

    #endregion updateInterventionUpdateProcessingSheet

    public function getDescriptionAndPriceProcessingSheet(Int $id)
    {
        return response()->success(ProcessingViewBL::getDescriptionAndPriceProcessingSheet($id));
    }

    #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(ProcessingViewBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE

    #endregion DELETE
}

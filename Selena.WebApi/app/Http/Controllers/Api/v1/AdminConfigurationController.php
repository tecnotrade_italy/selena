<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Routing\Controller;
use App\BusinessLogic\MenuAdminBL;
use App\Helpers\HttpHelper;
use App\BusinessLogic\FieldUserRoleBL;

class AdminConfigurationController extends Controller
{
    #region V1 / GET

    /**
     * Return menu
     *
     * @return \Illuminate\Http\Response
     */
    public function getMenu()
    {
        return response()->success(MenuAdminBL::getDto(HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Return configuration for functionality
     *
     * @param int id
     * @return \Illuminate\Http\Response
     */
    public function getFunctionality(int $id)
    {
        return response()->success(FieldUserRoleBL::getConfiguration($id, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion V1 / GET
}

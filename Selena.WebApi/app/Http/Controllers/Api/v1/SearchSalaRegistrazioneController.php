<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\SearchSalaRegistrazioneBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class SearchSalaRegistrazioneController
{
    
     /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function searchImplemented(Request $request)
    {
        return response()->success(SearchSalaRegistrazioneBL::searchImplemented($request));
    }

    #endregion POST
}

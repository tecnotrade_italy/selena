<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\TemplateEditorBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class TemplateEditorController
{
    #region GET

    /**
     * Get dto
     *
     * @param int idFunctionality
     *
     * @return \Illuminate\Http\Response
     */
    public function getDto(int $idFunctionality)
    {
        return response()->success(TemplateEditorBL::getDto($idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    public function getTemplateDefaultValue()
    {
        return response()->success(TemplateEditorBL::getTemplateDefaultValue());
    }


    public function getTemplateDefaultValueSelect()
    {
        return response()->getSelect(TemplateEditorBL::getTemplateDefaultValueSelect());
    }
    
    
   

    /**
     * Get template by id
     *
     * @param int id
     *
     * @return \Illuminate\Http\Response
     */
    public function getTemplateById(int $id)
    {
        return response()->success(TemplateEditorBL::getTemplateById($id, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    #endregion GET

    #region PUT

    /**
     * Insert
     *
     * @param Request Request
     *
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(TemplateEditorBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Clone
     *
     * @param int $id
     * @param int $idFunctionality
     * @return int
     */
    public function clone(int $id, int $idFunctionality)
    {
        return response()->success(TemplateEditorBL::clone($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion PUT

    #region POST

    /**
     * Update
     *
     * @param Request Request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return response()->success(TemplateEditorBL::update($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Update table
     *
     * @param Request Request
     *
     * @return \Illuminate\Http\Response
     */
    public function updateTable(Request $request)
    {
        return response()->success(TemplateEditorBL::updateTable($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion POST

    #region DELETE

    /**
     * Update
     *
     * @param int id
     * @param int idFunctionality
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id, int $idFunctionality)
    {
        return response()->success(TemplateEditorBL::delete($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

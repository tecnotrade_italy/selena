<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoVoucherType;
use App\BusinessLogic\VoucherTypeBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\VoucherType;
use App\Helpers\LogHelper;


class VoucherTypeController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(VoucherTypeBL::getById($id));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(VoucherTypeBL::getSelect($search));
    }

        /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(VoucherTypeBL::getAll($idLanguage));
    }

    #endregion GET
    
    public function insert(Request $dtoVoucherType)
    {
        return response()->success(VoucherTypeBL::insert($dtoVoucherType, HttpHelper::getLanguageId()));
    }
    
    public function update(Request $dtoVoucherType)
    {
        return response()->success(VoucherTypeBL::update($dtoVoucherType, HttpHelper::getLanguageId()));
    }

    public function delete(int $id)
    {
        return response()->success(VoucherTypeBL::delete($id, HttpHelper::getLanguageId()));
    }
   
}

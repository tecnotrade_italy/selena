<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\DictionaryBL;
use App\Http\Controllers\Controller;

class DictionaryController extends Controller
{
    #region V1 / GET

    /**
     * Return a dictionary by language
     *
     * @param Int idLanguage
     * @return \Illuminate\Http\Response
     */
    public function getByLanguage($idLanguage)
    {
        return response()->dictionary(DictionaryBL::getByLanguage($idLanguage));
    }

    #endregion V1 / GET
}
<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ContactRequestBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ContactRequestController extends Controller
{
    #region GET

    /**
     * Get all
     *
     * @return Response
     */
    public function getAll()
    {
        return response()->success(ContactRequestBL::getAll());
    }

    /**
     * Get by id
     * 
     * @param int id
     * 
     * @return Response
     */
    public function getById(int $id)
    {
        return response()->success(ContactRequestBL::getById($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @param Request dtoContact
     * 
     * @return Response
     */
    public function insert(Request $DtoContactRequest)
    {
        return response()->success(ContactRequestBL::insert($DtoContactRequest));
    }

      /**
     * Insert
     *
     * @param Request dtoContact
     * 
     * @return Response
     */
    public function insertLandingeBookPcb(Request $DtoContactRequest)
    {
        return response()->success(ContactRequestBL::insertLandingeBookPcb($DtoContactRequest));
    }

    public function insertLogsContacts(Request $request)
    {
        return response()->success(ContactRequestBL::insertLogsContacts($request));
    }
    
    /**
     * Insert
     *
     * @param Request dtoContactWorkForUs
     * 
     * @return Response
     */
    public function insertWorkForUs(Request $dtoContactWorkForUs)
    {
        return response()->success(ContactRequestBL::insertWorkForUs($dtoContactWorkForUs));
    }

      /**
     * Insert
     *
     * @param Request dtoContactWorkForUs
     * 
     * @return Response
     */
    public function insertTourAvailable(Request $request)
    {
        return response()->success(ContactRequestBL::insertTourAvailable($request));
    }

        /**
     * Insert
     *
     * @param Request dtoContactWorkForUs
     * 
     * @return Response
     */
    public function insertRequestLanguage(Request $request)
    {
        return response()->success(ContactRequestBL::insertRequestLanguage($request));
    }

    
    
    #endregion INSERT
}

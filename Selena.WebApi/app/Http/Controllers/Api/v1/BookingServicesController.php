<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\BookingServicesBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class BookingServicesController
{
    #region GET
    /**
     * Get all BookingServices
     *
     * @param int $BookingServices
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        return response()->success(BookingServicesBL::getAllDto(HttpHelper::getLanguageId()));
    }

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(BookingServicesBL::getById($id));
    }
 
    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(BookingServicesBL::getSelect($search));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(BookingServicesBL::getAll($idLanguage));
    }
    #endregion GET

    #region INSERT 
    /**
     * Insert
     *
     * @param Request DtoBookingServices
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoBookingServices)
    {
        return response()->success(BookingServicesBL::insert($DtoBookingServices, HttpHelper::getLanguageId()));
       
    }
    #endregion INSERT

    /**
     * Clone
     *
     * @param int $id
     * @param int $idFunctionality
     * 
     */
    
    public function clone(int $id, int $idFunctionality){
        return response()->success(BookingServicesBL::clone($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }



    #REGION UPDATE
    /**
     * Update
     *
     * @param Request DtoBookingServices
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoBookingServices)
    {
        return response()->success(BookingServicesBL::update($DtoBookingServices, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE

    #region DELETE
    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(BookingServicesBL::delete($id, HttpHelper::getLanguageId()));
    }
    #endregion DELETE
}

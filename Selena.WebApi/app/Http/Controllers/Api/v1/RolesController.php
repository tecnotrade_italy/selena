<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\RoleBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;

class RolesController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(RoleBL::getById($id));
    }

   /**
     *getByIdRoles

     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByIdRoles(Int $id)
    {
        return response()->success(RoleBL::getByIdRoles($id));
    }


    /**
      *Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(RoleBL::getSelect($search));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(RoleBL::getAll());
    }

    public function getAllWithoutAdmin()
    {
        return response()->success(RoleBL::getAllWithoutAdmin());
    }

    #endregion GET

    /**
     * Get allNoAuth
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllNoAuth()
    {
        return response()->success(RoleBL::getAllNoAuth());
    }

    #endregion allNoAuth


#region INSERTnoauth

    /**
     * Insert
     *
     * @param Request DtoRole
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertNoAuth(Request $DtoRole)
    {
        return response()->success(RoleBL::insertNoAuth($DtoRole, HttpHelper::getLanguageId()));
    }

    #endregion INSERTnoauth

    #region INSERT

    /**
     * Insert
     *
     * @param Request DtoRole
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoRole)
    {
        return response()->success(RoleBL::insert($DtoRole, HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request DtoRole
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoRole)
    {
        return response()->success(RoleBL::update($DtoRole, HttpHelper::getLanguageId()));
    }


 /**
     * Update
     *
     * @param Request DtoRole
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateNoAuth(Request $DtoRole)
    {
        return response()->success(RoleBL::updateNoAuth($DtoRole, HttpHelper::getLanguageId()));
    }


    
    #endregion UPDATE

    
    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(RoleBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

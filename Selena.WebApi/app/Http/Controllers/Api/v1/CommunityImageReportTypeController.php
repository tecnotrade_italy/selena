<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoCommunityImageReportType;
use App\BusinessLogic\CommunityImageReportTypeBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\CommunityImageReportType;
use App\Helpers\LogHelper;


class CommunityImageReportTypeController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(CommunityImageReportTypeBL::getById($id));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(CommunityImageReportTypeBL::getSelect($search));
    }

        /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(CommunityImageReportTypeBL::getAll($idLanguage));
    }

    #endregion GET
    
    public function insert(Request $dtoCommunityImageReportType)
    {
        return response()->success(CommunityImageReportTypeBL::insert($dtoCommunityImageReportType, HttpHelper::getLanguageId(),HttpHelper::getUserId()));
    }
    
    public function update(Request $dtoCommunityImageReportType)
    {
        return response()->success(CommunityImageReportTypeBL::update($dtoCommunityImageReportType, HttpHelper::getLanguageId()));
    }

    public function delete(int $id)
    {
        return response()->success(CommunityImageReportTypeBL::delete($id, HttpHelper::getLanguageId()));
    }
   
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\VoucherBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\Helpers\LogHelper;

class VoucherController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(VoucherBL::getById($id));
    }

    /**
     * Get by code
     *
     * @param String code
     *
     * @return String
     */

    public function getByCode(string $code)
    {
        return response()->success(VoucherBL::getByCode($code));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(VoucherBL::getAll());
       
    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     *
     * @param Request dtoVoucher
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $dtoVoucher)
    {
        return response()->success(VoucherBL::insert($dtoVoucher, HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoVoucher
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoVoucher)
    {
        return response()->success(VoucherBL::update($dtoVoucher, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(VoucherBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

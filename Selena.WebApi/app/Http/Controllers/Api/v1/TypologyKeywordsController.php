<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\TypologyKeywordsBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class TypologyKeywordsController
{
    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(TypologyKeywordsBL::getAll($idLanguage));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getAllSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(TypologyKeywordsBL::getAllSelect($search));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(TypologyKeywordsBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoSelenaViews
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return response()->success(TypologyKeywordsBL::update($request, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(TypologyKeywordsBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

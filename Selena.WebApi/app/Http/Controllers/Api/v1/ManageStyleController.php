<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ManageStyleBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class ManageStyleController
{
    #region GET

    /**
     * Get
     *
     * @param int idFunctionality
     * @return \Illuminate\Http\Response
     */
    public function get(int $idFunctionality)
    {
        return response()->success(ManageStyleBL::get($idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion GET

    #region POST

    /**
     * Update
     *
     * @param Request request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $fileDir = storage_path() . '\css\web.css';

        return response()->success(ManageStyleBL::update($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion POST

      /**
     * Get
     *
     * @param int idFunctionality
     * @return \Illuminate\Http\Response
     */
    public function getCssEmail(int $idFunctionality)
    {
        return response()->success(ManageStyleBL::getCssEmail($idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get
     *
     * @param int idFunctionality
     * @return \Illuminate\Http\Response
     */
    public function getCssHead(int $idFunctionality)
    {
        return response()->success(ManageStyleBL::getCssHead($idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }
    #endregion GET    

    #region POST

    /**
     * Update
     *
     * @param Request request
     * @return \Illuminate\Http\Response
     */
    public function updateCssEmail(Request $request)
    {
        $fileDir = storage_path() . '\css\webCssEmail.css';

        return response()->success(ManageStyleBL::updateCssEmail($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

     /**
     * Update
     *
     * @param Request request
     * @return \Illuminate\Http\Response
     */
    public function updateSaveGlobal(Request $request)
    {
        $fileDirCss = storage_path() . '\css\web.css';
        $fileDirCssEmail = storage_path() . '\css\webCssEmail.css';
        return response()->success(ManageStyleBL::updateSaveGlobal($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Update
     *
     * @param Request request
     * @return \Illuminate\Http\Response
     */
    public function updateSaveHead(Request $request)
    {
        return response()->success(ManageStyleBL::updateSaveHead($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    


    #endregion POST








}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\SupportsBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class SupportsController
{
    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(SupportsBL::getAll($idLanguage));
    }

    /**
     * Get All Prices Supports
     * 
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllPricesSupports(int $id)
    {
        return response()->success(SupportsBL::getAllPricesSupports($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(SupportsBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertSupportPrice(Request $request)
    {
        return response()->success(SupportsBL::insertSupportPrice($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertItemSupport(Request $request)
    {
        return response()->success(SupportsBL::insertItemSupport($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request data
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $data)
    {
        return response()->success(SupportsBL::update($data, HttpHelper::getLanguageId()));
    }

    /**
     * Update support price
     *
     * @param Request data
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateSupportPrice(Request $data)
    {
        return response()->success(SupportsBL::updateSupportPrice($data, HttpHelper::getLanguageId()));
    }

    /**
     * update Um Support Prices
     *
     * @param Request data
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateUmSupportPrices(Request $data)
    {
        return response()->success(SupportsBL::updateUmSupportPrices($data, HttpHelper::getLanguageId()));
    }
    
    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(SupportsBL::delete($id, HttpHelper::getLanguageId()));
    }

    /**
     * delete Range Detail Supports Price
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteRangeDetailSupportsPrice(int $id)
    {
        return response()->success(SupportsBL::deleteRangeDetailSupportsPrice($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\SearchInterventionBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class SearchInterventionController
{
    
     /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function searchImplemented(Request $request)
    {
        return response()->success(SearchInterventionBL::searchImplemented($request));
    }

    #endregion POST

}

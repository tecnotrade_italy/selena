<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\TemplateEditorGroupBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class TemplateEditorGroupController
{
    #region GET

    /**
     * Get dto
     *
     * @param int idFunctionality
     *
     * @return \Illuminate\Http\Response
     */
    public function getDto(int $idFunctionality)
    {
        return response()->success(TemplateEditorGroupBL::getDto($idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(TemplateEditorGroupBL::getSelect($search, $request->idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(TemplateEditorGroupBL::get($id));
    }


/**
     * Clone
     *
     * @param int $id
     * @param int $idFunctionality
     * @return int
     */
    public function clone(int $id, int $idFunctionality)
    {
        return response()->success(TemplateEditorGroupBL::clone($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get Template
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function selectGroupTemplate(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(TemplateEditorGroupBL::selectGroupTemplate($request->type, $search, $request->idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion GET

    #region PUT

    /**
     * Insert
     *
     * @param Request Request
     *
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(TemplateEditorGroupBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion PUT

    #region POST

    /**
     * Update
     *
     * @param Request Request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return response()->success(TemplateEditorGroupBL::update($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion POST

    #region DELETE

    /**
     * Update
     *
     * @param int id
     * @param int idFunctionality
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id, int $idFunctionality)
    {
        return response()->success(TemplateEditorGroupBL::delete($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

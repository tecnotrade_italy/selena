<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\LanguagePageBL;
use App\BusinessLogic\LanguageBL;
use App\BusinessLogic\PageBL;
use App\BusinessLogic\LogsBL;
use App\DtoModel\DtoPageSetting;
use App\Helpers\HttpHelper;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Helpers\LogHelper;
use App\BusinessLogic\MenuBL;

class PageController extends Controller
{
    #region GET

    /**
     * Get menu list
     * @param int $id
     * @param String $pageType
     * @return \Illuminate\Http\Response
     */
    public function getMenuList(int $id, string $pageType)
    {
        return response()->success(PageBL::getMenuList($id, $pageType));
    }


    /**
     * Get getByFile 
     *
     * @param request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByFile(request $request)
    {
        return response()->success(PageBL::getByFile($request));
    }
    #endregion getByFileNoId



    

    /**
     * Get search File 
     *
     * @param request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function searchFile(request $request)
    {
        return response()->success(PageBL::searchFile($request));
    }
    
    /**
     * Get getByFileEditorAll 
     *
     * @param request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByFileEditorAll()
    {
        return PageBL::getByFileEditorAll();
    }
    #endregion getByFileEditorAll


    /**
     * Get page setting
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getSetting(int $id, int $sUserId)
    {
      

        if(is_null($sUserId) || $sUserId == ''){
            $sUserId = null;
        }else{
            $sUserId = $sUserId;
        }

        return response()->success(PageBL::getSetting($id, $sUserId));
    }

       /**
     * Get page setting
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function getByUserDescription(int $sUserId)

    {
        return response()->success(PageBL::getByUserDescription($sUserId));
    }

    

    /**
     * Get home page
     *
     * @param int idLanguage
     * @return \Illuminate\Http\Response
     */
    public function getHome(int $idLanguage = 0)
    {
        return response()->success(PageBL::getHome($idLanguage));
    }

    /**
     * Get last activity
     *
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getLastActivity(int $idLanguage)
    {
        return response()->success(PageBL::getLastActivity($idLanguage));
    }

    /**
     * Get url list
     *
     * @return \Illuminate\Http\Response
     */
    public function getUrl()
    {
        return response()->success(PageBL::getUrl(HttpHelper::getLanguageId()));
    }

    #endregion GET

    #region PUT

    /**
     * Insert
     *
     * @param Request $request
     * @return int
     */
    public function insert(Request $request)
    {
        return response()->success(PageBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Clone
     *
     * @param int $id
     * @param int $idFunctionality
     * @return int
     */
    public function clone(int $id, int $idFunctionality)
    {
        return response()->success(PageBL::clone($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion PUT

    #region POST

    /**
     * Get render
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
   /* public function getRender(Request $request)
    {
        if(is_null($request->userId) || $request->userId == ''){
            $userId = null;
        }else{
            $userId = $request->userId;
        }
        
        return response()->success(PageBL::getRender($request->url, $userId));
    }*/

    /**
     * Get render
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getRender(Request $request)
    {
        if(is_null($request->cartId)){
            $idCart = "";
        }else{
            $idCart = $request->cartId;
        }
        if (is_null($request->idLanguage) || $request->idLanguage == '') {
            $idLanguage = LanguageBL::getDefault()->id;
        }else{
            $idLanguage = $request->idLanguage;
        }
        
        if(is_null($request->userId)){
            $userId = "";
        }else{
            $userId = $request->userId;
            if($request->url != '/fonts/Simple-Line-Icons.0cb0b9c5.woff2' && $request->url != '/fonts/fontawesome-webfont.af7ae505.woff2' && $request->url != '/fonts/Simple-Line-Icons.d2285965.ttf' && $request->url != '/fonts/fontawesome-webfont.fee66e71.woff' && $request->url != '/fonts/Simple-Line-Icons.78f07e2c.woff' && $request->url != '/fonts/fontawesome-webfont.b06871f2.ttf'){
                LogsBL::insert($userId, '', 'PAGEVIEW', 'Url visualizzato: ' . $request->url);
            }
        }

        if(is_null($request->paramUrl)){
            $paramUrl = "";
        }else{
            if(isset($request->paramUrl)){
                $paramUrl = $request->paramUrl;
            }else{
                $paramUrl = "";
            }
        }

        if(isset($request->salesmanId)){
            $salesmanId = $request->salesmanId;
        }else{
            $salesmanId = "";
        }
        

        return response()->success(PageBL::getRender($request->url, $paramUrl, $userId, $idCart, $idLanguage, $salesmanId));
    }







    /**
     * Get render
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getRenderUser(Request $request)
    {
        if(is_null($request->cartId)){
            $idCart = "";
        }else{
            $idCart = $request->cartId;
        }
        if (is_null($request->idLanguage) || $request->idLanguage == '') {
            $idLanguage = LanguageBL::getDefault()->id;
        }else{
            $idLanguage = $request->idLanguage;
        }
        
        if(is_null($request->userId)){
            $userId = "";
        }else{
            $userId = $request->userId;
            if($request->url != '/fonts/Simple-Line-Icons.0cb0b9c5.woff2' && $request->url != '/fonts/fontawesome-webfont.af7ae505.woff2' && $request->url != '/fonts/Simple-Line-Icons.d2285965.ttf' && $request->url != '/fonts/fontawesome-webfont.fee66e71.woff' && $request->url != '/fonts/Simple-Line-Icons.78f07e2c.woff' && $request->url != '/fonts/fontawesome-webfont.b06871f2.ttf'){
                LogsBL::insert($userId, '', 'PAGEVIEW', 'Url visualizzato: ' . $request->url);
            }
        }

        if(is_null($request->paramUrl)){
            $paramUrl = "";
        }else{
            if(isset($request->paramUrl)){
                $paramUrl = $request->paramUrl;
            }else{
                $paramUrl = "";
            }
        }

        if(isset($request->salesmanId)){
            $salesmanId = $request->salesmanId;
        }else{
            $salesmanId = "";
        }
        

        return response()->success(PageBL::getRenderUser($request->url, $paramUrl, $userId, $idCart, $idLanguage, $salesmanId));
    }

    /**
     * Check exist url
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function checkExistUrl(Request $request)
    {
        return response()->success(LanguagePageBL::checkExistUrl($request));
    }

    /**
     * insert Image
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function insertImage(Request $request)
    {
        return response()->success(PageBL::insertImage($request));
    }

    /**
     * delete Image
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function deleteImage(Request $request)
    {
        return response()->success(PageBL::deleteImage($request));
    }

    /**
     * convert Image To Webp
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function convertImageToWebp(Request $request)
    {
        return response()->success(PageBL::convertImageToWebp($request));
    }
    
    /**
     * Update
     *
     * @param Request $request
     * @return int
     */
    public function update(Request $request)
    {
        return response()->success(PageBL::update($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Update url page
     *
     * @param Request $request
     * @return dtoResults
     */
    public function postUrl(Request $request)
    {
        return response()->success(PageBL::postUrl($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Update online page in list menu
     *
     * @param Request $request
     */
    public function postOnlinePage(Request $request)
    {
        return response()->success(PageBL::setOnlinePage($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Update menu list
     *
     * @param Request $request
     */
    public function postMenuList(Request $request)
    {
        return response()->success(MenuBL::setOrderMenuList($request));
    }


    public function insertItemCategoriesPages(Request $request)
    {
        return response()->success(PageBL::insertItemCategoriesPages($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }  


    public function deleteItemCategoriesPages(int $idCategories, int $idPages)
    {
        return response()->success(PageBL::deleteItemCategoriesPages($idCategories, $idPages, HttpHelper::getLanguageId()));
    }

    #endregion POST

    #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(PageBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

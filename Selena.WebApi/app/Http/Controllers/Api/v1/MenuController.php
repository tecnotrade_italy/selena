<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\MenuBL;
use App\Helpers\HttpHelper;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;

class MenuController extends Controller
{
    #region GET

    /**
     * Get setting
     *
     * @param int id
     * @return \Illuminate\Http\Response
     */
    public function getSettings($id)
    {
        return response()->success(MenuBL::getSettings($id));
    }

    #endregion GET

    #region PUT / POST

    /**
     * Put link
     *
     * @param Request request
     * @return \Illuminate\Http\Response
     */
    public function link(Request $request)
    {
        if ($request->isMethod('post')) {
            MenuBL::updateLink($request, HttpHelper::getUserId(), HttpHelper::getLanguageId());
            return response()->success();
        } else {
            return response()->success(MenuBL::insertLink($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
        }
    }

    /**
     * Put link
     *
     * @param Request request
     * @return \Illuminate\Http\Response
     */
    public function newlink(Request $request)
    {
        /*if ($request->isMethod('post')) {
            MenuBL::updateLink($request, HttpHelper::getUserId(), HttpHelper::getLanguageId());
            return response()->success();
        } else {*/
            return response()->success(MenuBL::insertNewLink($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
        //}
    }

    /**
     * Put link
     *
     * @param Request request
     * @return \Illuminate\Http\Response
     */
    public function updateNewlink(Request $request)
    {
        /*if ($request->isMethod('post')) {
            MenuBL::updateLink($request, HttpHelper::getUserId(), HttpHelper::getLanguageId());
            return response()->success();
        } else {*/
            return response()->success(MenuBL::updateNewlink($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
        //}
    }


    /**
     * Put folder
     *
     * @param Request request
     * @return \Illuminate\Http\Response
     */
    public function folder(Request $request)
    {
        if ($request->isMethod('post')) {
            MenuBL::updateFolder($request, HttpHelper::getUserId(), HttpHelper::getLanguageId());
            return response()->success();
        } else {
            return response()->success(MenuBL::insertFolder($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
        }
    }

    #endregion PUT / POST
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ZoneBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;

class ZoneController
{
    #region GET
 
    /**
     * Get all zone
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(ZoneBL::getSelect($search));
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(ZoneBL::getAll($idLanguage));
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(ZoneBL::getById($id));
    }
    #endregion GET

    #region INSERT
    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(ZoneBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }
    #endregion INSERT

    #region UPDATE
    /**
     * Update
     *
     * @param Request dtoZone
     * 
     * @return \Illuminate\Http\Response 
     */
    public function update(Request $dtoZone)
    {
        return response()->success(ZoneBL::update($dtoZone, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE

    #region DELETE
    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(ZoneBL::delete($id, HttpHelper::getLanguageId()));
    }
    #endregion DELETE
}

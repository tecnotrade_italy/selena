<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\FeedbackNewsletterBL;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;

class FeedbackNewsletterController
{
    #region GET

    /**
     * Get all by id newsletter
     *
     * @param int idNewsletter
     * @return \Illuminate\Http\Response
     */
    public function getAllByNewslletter(int $idNewsletter)
    {
        return response()->success(FeedbackNewsletterBL::getAllByNewslletter($idNewsletter));
    }

    /**
     * Get all chart by id newsletter
     *
     * @param int idNewsletter
     * @return \Illuminate\Http\Response
     */
    public function getChart(int $idNewsletter)
    {
        return response()->success(FeedbackNewsletterBL::getChart($idNewsletter));
    }

      /**
     * Get all chart by id newsletter
     *
     * @param int idNewsletter
     * @return \Illuminate\Http\Response
     */
    public function getChartGraphics(int $idNewsletter)
    {
        return response()->success(FeedbackNewsletterBL::getChartGraphics($idNewsletter));
    }


       /**
     * Get all chart by id newsletter
     *
     * @param int idNewsletter
     * @return \Illuminate\Http\Response
     */
    public function getChartGraphicsforTime(int $idNewsletter)
    {
        return response()->success(FeedbackNewsletterBL::getChartGraphicsforTime($idNewsletter));
    }

    

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @param int idNewsletterRecipient
     * @param string code
     * @return \Illuminate\Http\Response
     */
    public function insert(int $idNewsletterRecipient, string $code)
    {
        
        return response()->success(FeedbackNewsletterBL::insert($idNewsletterRecipient, $code));
    }

    #endregion INSERT
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\RedirectBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class RedirectController
{
    #region GET

    /**
     * Get all 
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(RedirectBL::getSelect($search));
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(RedirectBL::getAll());
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(RedirectBL::getById($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(RedirectBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    public function insertRedirectCheck(Request $request)
    {
        return response()->success(RedirectBL::insertRedirectCheck($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    
    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoRedirect
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoRedirect)
    {
        return response()->success(RedirectBL::update($dtoRedirect, HttpHelper::getLanguageId()));
    }

    public function updateTable(Request $dtoRedirect)
    {
        return response()->success(RedirectBL::updateTable($dtoRedirect, HttpHelper::getLanguageId()));
    }


    

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(RedirectBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

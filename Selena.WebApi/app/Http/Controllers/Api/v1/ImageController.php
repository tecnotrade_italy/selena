<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\DictionaryBL;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use stdClass;

class ImageController
{
    #region PRIVATE

    private static $dirImage = 'public/images';
    private static $mimeTypeAllowed = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];

    /**
     * Get url
     */
    private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/';
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get all
     *
     * @return array
     */
    public function getAll()
    {
        $result = array();

        foreach (Storage::files(static::$dirImage) as $image) {
            $response = new stdClass();
            $response->url = str_replace(static::$dirImage . '/', static::getUrl(), $image);
            array_push($result, $response);
        }

        return $result;
    }


    #endregion GET

    #region POST

    /**
     * Add image
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        if (!in_array($request->file->getClientMimeType(), static::$mimeTypeAllowed)) {
            return response()->error(DictionaryBL::getTranslate(HttpHelper::getLanguageId(), DictionariesCodesEnum::FileTypeNotAllowed), HttpResultsCodesEnum::InvalidPayload);
        } else {
            $fileName = $request->file->getClientOriginalName();

            if (Storage::disk('public')->exists('images/' . $fileName)) {
                return response()->error(DictionaryBL::getTranslate(HttpHelper::getLanguageId(), DictionariesCodesEnum::NameFileAlreadyExists), HttpResultsCodesEnum::NotAcceptable);
            } else {
                $request->file->storeAs(static::$dirImage, $fileName);
                return response()->link(static::getUrl() . $request->file->getClientOriginalName());
            }
        }
    }

    #endregion POST

    #region DELETE

    /**
     * Delete
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $fileName = str_replace(static::getUrl(), '', $request->src);

        if (!Storage::disk('public')->exists('images/' . $fileName)) {
            return response()->error(DictionaryBL::getTranslate(HttpHelper::getLanguageId(), DictionariesCodesEnum::CantDeletedImage), HttpResultsCodesEnum::NotAcceptable);
        } else {
            Storage::disk('public')->delete('images/' . $fileName);
            return;
        }
    }

    #endregion DELETE
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\SearchUserBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class SearchUserController
{
    
     /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function searchImplemented(Request $request)
    {
        return response()->success(SearchUserBL::searchImplemented($request));
    }
    #endregion POST

       /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function SearchViewPrivates(Request $request)
    {
        return response()->success(SearchUserBL::SearchViewPrivates($request));
    }
    #endregion POST

          /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function SearchViewTrueSubscriber(Request $request)
    {
        return response()->success(SearchUserBL::SearchViewTrueSubscriber($request));
    }
    #endregion POST

           /**
     * Search by parameters
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function SearchViewFalseSubscriber(Request $request)
    {
        return response()->success(SearchUserBL::SearchViewFalseSubscriber($request));
    }
    #endregion POST

  
}

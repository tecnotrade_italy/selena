<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\MenuAdminBL;
use App\BusinessLogic\MenuAdminLanguageBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class MenuAdminController
{
    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllCustom(int $idLanguage)
    {
        
        return response()->success(MenuAdminBL::getAllCustom($idLanguage));
    }

    public function getByRole(int $idRole)
    {
        return response()->success(MenuAdminBL::getByRole($idRole));
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(MenuAdminBL::getById($id));
    }

        /**
     * Get by code
     *
     * @param String code
     * @param Int idLanguage
     *
     * @return String
     */

    public function getByCode(string $code)
    {
        return response()->success(MenuAdminBL::getByCode($code));
    }


    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $dtoMenuAdmins)
    {
        return response()->success(MenuAdminBL::insert($dtoMenuAdmins, HttpHelper::getUserId()));
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoMenuAdmins
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoMenuAdmins)
    {
        return response()->success(MenuAdminBL::update($dtoMenuAdmins, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(MenuAdminBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE


    public function disable(int $menu_admin_id, int $role_id)
    {
        return response()->success(MenuAdminBL::disable($menu_admin_id, $role_id));
    }

    public function enable(int $menu_admin_id, int $role_id)
    {
        return response()->success(MenuAdminBL::enable($menu_admin_id, $role_id));
    }
}

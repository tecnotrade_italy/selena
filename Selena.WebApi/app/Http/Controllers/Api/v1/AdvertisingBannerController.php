<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoAdvertisingBanner;
use App\BusinessLogic\AdvertisingBannerBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\AdvertisingBanner;
use App\Helpers\LogHelper;


class AdvertisingBannerController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(AdvertisingBannerBL::getById($id));
    }


        /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(AdvertisingBannerBL::getAll());
    }

    #endregion GET
    
    public function insert(Request $dtoAdvertisingBanner)
    {
        return response()->success(AdvertisingBannerBL::insert($dtoAdvertisingBanner,HttpHelper::getUserId()));
    }
    
    public function update(Request $dtoAdvertisingBanner)
    {
        return response()->success(AdvertisingBannerBL::update($dtoAdvertisingBanner));
    }

    public function delete(int $id)
    {
        return response()->success(AdvertisingBannerBL::delete($id));
    }
   
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\InternalRepairBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;

class InternalRepairController
{
   
    #region INSERT

    /**
     * Insert
     *
     * @param Request DtoInterventions
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(InternalRepairBL::insert($request, HttpHelper::getUserId()));
    }
    #endregion insert

     /**
     * Get by getAllInternalRepairSearch
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getAllInternalRepairSearch(Request $request)
    {
        return response()->success(InternalRepairBL::getAllInternalRepairSearch($request));
    }

    public function getinternalRepairSearchViewOpen(Request $request)
    {
        return response()->success(InternalRepairBL::getinternalRepairSearchViewOpen($request));
    }
    public function getinternalRepairSearchViewClose(Request $request)
    {
        return response()->success(InternalRepairBL::getinternalRepairSearchViewClose($request));
    }

    public function getAllInternalExternalRepairSearch(Request $request)
    {
        return response()->success(InternalRepairBL::getAllInternalExternalRepairSearch($request));
    }
    

 /**
     * getAllInternalRepair
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllInternalRepair(int $idLanguage)
    {
        return response()->success(InternalRepairBL::getAllInternalRepair($idLanguage));
    }

    #endregion getAllInternalRepair


/**
     * getByInternalRepair
     *
     * @return \Illuminate\Http\Response
     */
    public function getByInternalRepair(int $id)
    {
        return response()->success(InternalRepairBL::getByInternalRepair($id));
    }

    #endregion getAllInternalRepair

   /**
     * Get Last id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getLastId()
    {
        return response()->success(InternalRepairBL::getLastId());
    }

    public function getLastIdIntRepair()
    {
        return response()->success(InternalRepairBL::getLastIdIntRepair());
    }


  /**
     * updateInternalRepair
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateInternalRepair(Request $request)
    {
        return response()->success(InternalRepairBL::updateInternalRepair($request, HttpHelper::getLanguageId()));
    }

 #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(InternalRepairBL::delete($id, HttpHelper::getLanguageId()));
    }
    #endregion DELETE

     /**
     * getdescription
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getdescription(Request $request)
    {
        return response()->success(InternalRepairBL::getdescription($request, HttpHelper::getLanguageId()));
    }

      /**
     * getdescription
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getdescriptioninsert(Request $request)
    {
        return response()->success(InternalRepairBL::getdescriptioninsert($request, HttpHelper::getLanguageId()));
    }
    

}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\UserBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use PhpParser\Node\Scalar\String_;
use Symfony\Component\HttpFoundation\Request;

class UserController
{
    #region GET

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(UserBL::getSelect($search));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelectProv(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(UserBL::getSelectProv($search));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelectIntervention(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(UserBL::getSelectIntervention($search));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelectUserPages(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(UserBL::getSelectUserPages($search));
    }

    

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(UserBL::getById($id));
    }

    /**
     * get Community Data by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getCommunityData(Int $id)
    {
        return response()->success(UserBL::getCommunityData($id));
    }

 /**
     * Get by getByIdResult
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByIdResult(Int $id)
    {
        return response()->success(UserBL::getByIdResult($id));
    }

    
/**
     * Get by getDifferentDestination
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getDifferentDestination(Int $id)
    {
        return response()->success(UserBL::getDifferentDestination($id));
    }

    public function getDetailUsers(Int $id)
    {
        return response()->success(UserBL::getDetailUsers($id));
    }
    

    /**
     * Get by username
     *
     * @param String $username
     * @return \Illuminate\Http\Response
     */
    public function getByUsername(String $username)
    {
        return response()->success(UserBL::getByUsername($username));
    }

    /**
     * Get graphic data
     *
     * @return \Illuminate\Http\Response
     */
    public function getGraphic()
    {
        return response()->success(UserBL::getGraphic());
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllUser(int $idLanguage)
    {
        return response()->success(UserBL::getAllUser($idLanguage));
    }

       /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllAddress(int $id)
    {
        return response()->success(UserBL::getAllAddress($id));
    }

    public function getAllUserSales(int $id)
    {
        return response()->success(UserBL::getAllUserSales($id));
    }

    public function getAllUserHistorical(int $id)
    {
        return response()->success(UserBL::getAllUserHistorical($id));
    }

    public function getAllDetailTimeLine(int $id)
    {
        return response()->success(UserBL::getAllDetailTimeLine($id));
    }


    public function getAllDetailValueSalesUser(int $id)
    {

        return response()->success(UserBL::getAllDetailValueSalesUser($id));
    }



       /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getbyIdAddress(int $id)
    {
        return response()->success(UserBL::getbyIdAddress($id));
    }



    #endregion GET

/**
     * Get all data
     *
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getUserData(int $sUserId)
    {
        return response()->success(UserBL::getUserData($sUserId));
    }

/**
     * Get availableCheck
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
 public function availableCheck(int $id)
    {
        return response()->success(UserBL::availableCheck($id));
    }


    /**
     * Get all data
     *
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idFunctionality)
    {
        return response()->success(UserBL::getAll($idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    /**
     * Get number of user
     * 
     * @return \Illuminate\Http\Response
     */
    public function count()
    {
        return response()->success(UserBL::count());
    }

    #endregion GET

    #region PUT

    /**
     * Insert
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(UserBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Insert No Auth
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertNoAuth(Request $request)
    {
        return response()->success(UserBL::insertNoAuth($request, HttpHelper::getLanguageId()));
    }

      /**
     * insertUser
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertUser(Request $request)
    {
        return response()->success(UserBL::insertUser($request, HttpHelper::getLanguageId()));
    }

     /**
     * insertUser
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertUserAddress(Request $request)
    {
        return response()->success(UserBL::insertUserAddress($request, HttpHelper::getLanguageId()));
    }

    public function insertSalesUser(Request $request)
    {
        return response()->success(UserBL::insertSalesUser($request, HttpHelper::getLanguageId()));
    }

    public function insertComment(Request $request)
    {
        return response()->success(UserBL::insertComment($request));
    }

  

       /**
     * insertUser
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateCheckOnlineAddress(Request $request)
    {
        return response()->success(UserBL::updateCheckOnlineAddress($request, HttpHelper::getLanguageId()));
    }

    /**
     * 
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateCheckDetailDefaultAddress(Request $request)
    {
        return response()->success(UserBL::updateCheckDetailDefaultAddress($request, HttpHelper::getLanguageId()));
    }
    #endregion PUT

      /**
     * 
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateCheckOnlineReferentDetail(Request $request)
    {
        return response()->success(UserBL::updateCheckOnlineReferentDetail($request, HttpHelper::getLanguageId()));
    }
    #endregion PUT

    

    #region POST
    /**
     * change Password
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function changePassword(Request $request)
    {
        return response()->success(UserBL::changePassword($request, HttpHelper::getUserId()));
    }

    /**
     * change Password
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function changePasswordRescued(Request $request)
    {
        return response()->success(UserBL::changePasswordRescued($request));
    }


    /**
     * Update
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return response()->success(UserBL::update($request, HttpHelper::getUserId()));
    }

    /**
     * Update
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function rescuePassword(Request $request)
    {
        return response()->success(UserBL::rescuePassword($request));
    }

     /**
     * ResetConfermationEmail
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function ResetConfermationEmail(Request $request)
    {
        return response()->success(UserBL::ResetConfermationEmail($request));
    }


      /**
     * Update
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateNoAuth(Request $request)
    {
        return response()->success(UserBL::updateNoAuth($request, $request->id));
    }

      /**
     * Update
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateUserAddress(Request $request)
    {
        return response()->success(UserBL::updateUserAddress($request));
    }

    public function updateSalesUser(Request $request)
    {
        return response()->success(UserBL::updateSalesUser($request));
    }

    /**
         * updateorcreateDestinationBusinessName
         *
         * @param Request $request
         * 
         * @return \Illuminate\Http\Response
         */
        public function updateorcreateDestinationBusinessName(Request $request)
        {
            return response()->success(UserBL::updateorcreateDestinationBusinessName($request));
        }

        public function updateorcreateUsersAdresses(Request $request)
        {
            return response()->success(UserBL::updateorcreateUsersAdresses($request));
        }
        

        /**  updateVatNumber
         *
         * @param Request $request
         * 
         * @return \Illuminate\Http\Response
         */
        public function updateVatNumber(Request $request)
        {
            return response()->success(UserBL::updateVatNumber($request));
        }

    /**  updateProfileImage
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateProfileImage(Request $request)
    {
        return response()->success(UserBL::updateProfileImage($request));
    }

    /**  uploadImageCoverProfile
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function uploadImageCoverProfile(Request $request)
    {
        return response()->success(UserBL::uploadImageCoverProfile($request));
    }


      /**
     * Update
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateUser(Request $request)
    
    {
        return response()->success(UserBL::updateUser($request, $request->id,HttpHelper::getLanguageId()));
    }

    /**
     * update Field By Id
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateFieldById(Request $request)
    
    {
        return response()->success(UserBL::updateFieldById($request));
    }

    

    /**
     * Check exist email
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function checkExistUsername(Request $request)
    {
        return response()->success(UserBL::checkExistUsername($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Check group user
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function checkGroupUser(Request $request)
    {
        return response()->success(UserBL::checkGroupUser($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Check exist email
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function checkExistEmail(Request $request)
    {
        return response()->success(UserBL::checkExistEmail($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    /**
     * Check if user logged is the same of the user page navigated
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function checkUserLoggedToUserUrl(Request $request)
    {
        return response()->success(UserBL::checkUserLoggedToUserUrl($request, HttpHelper::getLanguageId()));
    }

    #endregion POST

    #region PUT

    /**
     * Insert
     *
     * @param int id
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id, int $idFunctionality)
    {
        return response()->success(UserBL::delete($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

  #endregion delete
 /**
     * Insert
     *
     * @param int id
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function deleteRow(int $id)
    {
        return response()->success(UserBL::deleteRow($id, HttpHelper::getLanguageId()));
    }

    public function deleteAddress(int $id)
    {
        return response()->success(UserBL::deleteAddress($id, HttpHelper::getLanguageId()));
    }
  
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ItemAttachmentTypeControllerBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class ItemAttachmentTypeController
{
    #region GET

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(ItemAttachmentTypeControllerBL::getSelect($search));
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(ItemAttachmentTypeControllerBL::getAll($idLanguage));
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(ItemAttachmentTypeControllerBL::getById($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(ItemAttachmentTypeControllerBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoSelenaViews
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoSelenaViews)
    {
        return response()->success(ItemAttachmentTypeControllerBL::update($dtoSelenaViews, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(ItemAttachmentTypeControllerBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

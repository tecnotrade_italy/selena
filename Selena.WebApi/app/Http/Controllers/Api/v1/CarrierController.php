<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\CarrierBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class CarrierController
{
    #region GET

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(CarrierBL::getAll(HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get carrier area
     *
     * @return \Illuminate\Http\Response
     */
    public function getCarrierArea(int $id, int $idZone)
    {
        return response()->success(CarrierBL::getCarrierArea($id, $idZone));
    }

    /**
     * Get by customer
     *
     * @param int idCustomer
     * @param int idFunctionality
     * @return \Illuminate\Http\Response
     */
    public function getByCustomer(int $idCustomer, int $idFunctionality)
    {
        return response()->success(CarrierBL::getByCustomer($idCustomer, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(CarrierBL::getSelect($search, $request->idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion GET

        /**
     * Get all
     * 
     * @param int idLanguage
     * @param int cartId
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAllCarrierByCartId(int $idLanguage, int $cartId)
    {
        return response()->success(CarrierBL::getAllCarrierByCartId($idLanguage, $cartId));
    }


/**
     * Get selectCarrier
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function selectCarrier(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(CarrierBL::selectCarrier($search));
    }

    #region POST

    /**
     * Insert
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(CarrierBL::insert($request, HttpHelper::getUserId()));
    }

    /**
     * insert Carrier Area
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertCarrierArea(Request $request)
    {
        return response()->success(CarrierBL::insertCarrierArea($request, HttpHelper::getUserId()));
    }

    /**
     * Update
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return response()->success(CarrierBL::update($request));
    }
    
    /**
     * update Carrier Range Area
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateCarrierRangeArea(Request $request)
    {
        return response()->success(CarrierBL::updateCarrierRangeArea($request));
    }

    #endregion POST

    #region DELETE 

    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(CarrierBL::delete($id, HttpHelper::getLanguageId()));
    }
    
    
    /**
     * delete Range Detail Area
     *
     * @param int $id
     * @return int
     */
    public function deleteRangeDetailArea(int $id)
    {
        return response()->success(CarrierBL::deleteRangeDetailArea($id, HttpHelper::getLanguageId()));
    }

    /**
     * delete Zone Range Detail Area
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function deleteZoneRangeDetailArea(Request $request)
    {
        return response()->success(CarrierBL::deleteZoneRangeDetailArea($request, HttpHelper::getLanguageId()));
    }
        

    #endregion DELETE 
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\LanguageBL;
use App\BusinessLogic\TemplateEditorTypeBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class TemplateEditorTypeController
{
    #region GET

    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(TemplateEditorTypeBL::getSelect($search, $request->idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(TemplateEditorTypeBL::get($id));
    }

    #endregion GET
}

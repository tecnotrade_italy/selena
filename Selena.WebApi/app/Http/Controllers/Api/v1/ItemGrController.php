<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ItemGrBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class ItemGrController
{
  

  /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(ItemGrBL::getAll($idLanguage));
    }

    #endregion GET

  /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
      public function getById(Int $id)
      {
         return response()->success(ItemGrBL::getById($id));
      }

      public function getByCmp(Int $id)
      {
         return response()->success(ItemGrBL::getByCmp($id));
      }
      
       /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getBarcodeGrForTypology(Int $idTipology)
    {
       return response()->success(ItemGrBL::getBarcodeGrForTypology($idTipology));
    }

    public function getBarcodeGrForPlantId(Int $idSelectplant_id)
    {
       return response()->success(ItemGrBL::getBarcodeGrForPlantId($idSelectplant_id));
    }
   
    public function getProduct(Int $id)
    {
       return response()->success(ItemGrBL::getProduct($id));
    }
    
 /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(ItemGrBL::getSelect($search));
    }

    public function getSelectListArticle(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(ItemGrBL::getSelectListArticle($search));
    }

    
     /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function selectnameproduct(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(ItemGrBL::selectnameproduct($search));
    }
    

 /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getselectitemGr(Request $request)
    {
        $search = trim($request->search);

        return response()->getSelect(ItemGrBL::getselectitemGr($search));
    }

 #region INSERT
    
    /**
     * Insert
     *
     * @param Request DtoItemGr
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertNoAuth(Request $DtoItemGr)
    {
        return response()->success(ItemGrBL::insertNoAuth($DtoItemGr, HttpHelper::getUserId()));   
    }

 /**
     * Update
     *
     * @param Request DtoItemGr
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoItemGr)
    {
        return response()->success(ItemGrBL::update($DtoItemGr, HttpHelper::getLanguageId()));
    }

     public function updateExploded(Request $DtoItemGr)
    {
        return response()->success(ItemGrBL::updateExploded($DtoItemGr, HttpHelper::getLanguageId()));
    }


    #endregion UPDATE

 #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(ItemGrBL::delete($id, HttpHelper::getLanguageId()));
    }

    public function deleteExploded(int $id)
    {
        return response()->success(ItemGrBL::deleteExploded($id, HttpHelper::getLanguageId()));
    }
    

    #endregion DELETE
}
<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ItemGroupTechnicalSpecificationBL;

class ItemGroupTechnicalSpecificationController
{
    #region GET

    /**
     * Get item without group technical specification
     * 
     * @return \Illuminate\Http\Response
     */
    public function getItemWithoutGroupTechnicalSpecification()
    {
        return response()->success(ItemGroupTechnicalSpecificationBL::getItemWithoutGroupTechnicalSpecification());
    }

    #endregion GET
}

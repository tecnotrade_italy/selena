<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\RobotsSettingBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class RobotsSettingController
{
    #region GET
    /**
     * Get
     *
     * @param int idFunctionality
     * @return \Illuminate\Http\Response
     */
    public function get(int $idFunctionality)
    {
        return response()->success(RobotsSettingBL::get($idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }
    #endregion GET

    #region POST
    /**
     * Update head
     *
     * @param Request request
     * @return \Illuminate\Http\Response
     */
    public function updateRobots(Request $request)
    {
        return response()->success(RobotsSettingBL::updateRobots($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion POST
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\QuotesBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class QuotesController
{
    #region GET

   
    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(QuotesBL::getById($id));
    }

    /**
     * get By User Id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByUserId(int $id)
    {
        return response()->success(QuotesBL::getByUserId($id, HttpHelper::getLanguageId()));
    }

    

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertByCartId(Request $request)
    {
        return response()->success(QuotesBL::insertByCartId($request));
    }

    /**
     * transform Into Cart
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function transformIntoCart(Request $request)
    {
        return response()->success(QuotesBL::transformIntoCart($request));
    }

   
    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoSelenaViews
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoSelenaViews)
    {
        return response()->success(QuotesBL::update($dtoSelenaViews, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(QuotesBL::delete($id, HttpHelper::getLanguageId()));
    }


    #endregion DELETE
}

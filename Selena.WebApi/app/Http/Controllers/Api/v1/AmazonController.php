<?php

namespace App\Http\Controllers\Api\v1;

use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\Helpers\LogHelper;
use App\BusinessLogic\AmazonBL;



class AmazonController
{

    public function getHtml(Request $request)
    {

        return response()->success(AmazonBL::getHtml($request->shortcode));
        
    }


   
}

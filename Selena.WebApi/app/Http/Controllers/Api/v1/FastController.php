<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\FastBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;
use LogHeader;

class FastController
{
    #region INSERT

    /**
     * Insert 
     *
     * @param Request DtoFast
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoFast)
    {
        return response()->success(FastBL::insert($DtoFast, HttpHelper::getUserId()));
    }

    /**
     * Get all data
     *
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
     public function getUserData(int $sUserId)
    {
        return response()->success(FastBL::getUserData($sUserId));
    }

  /**
     * updateFast
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateFast(Request $request)
    {
        return response()->success(FastBL::updateFast($request, $request->id));
    }

       /**
     * Delete
     *
     * @param int id
     
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(FastBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE




 
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoCommunityFavoriteImage;
use App\BusinessLogic\CommunityFavoriteImageBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\CommunityFavoriteImage;
use App\Helpers\LogHelper;


class CommunityFavoriteImageController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(CommunityFavoriteImageBL::getById($id));
    }


        /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(CommunityFavoriteImageBL::getAll());
    }


    #endregion GET
    
    public function insert(Request $dtoCommunityFavoriteImage)
    {
        return response()->success(CommunityFavoriteImageBL::insert($dtoCommunityFavoriteImage));
    }

    public function updateOrInsert(Request $dtoCommunityFavoriteImage)
    {
        return response()->success(CommunityFavoriteImageBL::updateOrInsert($dtoCommunityFavoriteImage));
    }
    
    public function update(Request $dtoCommunityFavoriteImage)
    {
        return response()->success(CommunityFavoriteImageBL::update($dtoCommunityFavoriteImage));
    }

    public function delete(int $id)
    {
        return response()->success(CommunityFavoriteImageBL::delete($id));
    }
   
}

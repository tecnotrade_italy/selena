<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\BookingSettingsBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class BookingSettingsController
{
    #region GET
    /**
     * Get all BookingSettings
     *
     * @param int $BookingSettings
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        return response()->success(BookingSettingsBL::getAllDto(HttpHelper::getLanguageId()));
    }

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(BookingSettingsBL::getById($id));
    }
 
    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(BookingSettingsBL::getSelect($search));
    }


    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(BookingSettingsBL::getAll($idLanguage));
    }
    #endregion GET

    #region INSERT 
    /**
     * Insert
     *
     * @param Request DtoBookingSettings
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoBookingSettings)
    {
        return response()->success(BookingSettingsBL::insert($DtoBookingSettings, HttpHelper::getLanguageId()));
       
    }
    #endregion INSERT

    /**
     * Clone
     *
     * @param int $id
     * @param int $idFunctionality
     * 
     */
    
    public function clone(int $id, int $idFunctionality){
        return response()->success(BookingSettingsBL::clone($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #REGION UPDATE
    /**
     * Update
     *
     * @param Request DtoBookingSettings
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoBookingSettings)
    {
        return response()->success(BookingSettingsBL::update($DtoBookingSettings, HttpHelper::getLanguageId()));
    }
    #endregion UPDATE

    #region DELETE
    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(BookingSettingsBL::delete($id, HttpHelper::getLanguageId()));
    }
    #endregion DELETE
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\NewsletterRecipientBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class NewsletterRecipientController
{
    #region GET

    /**
     * Get Contact by Newsletter
     * 
     * @param int idNewsletter
     * 
     * @return \Illuminate\Http\Response
     */
    public function getContactByNewsletter(int $idNewsletter)
    {
        return response()->success(NewsletterRecipientBL::getContactByNewsletter($idNewsletter));
    }

    /**
     * Get Customer by Newsletter
     * 
     * @param int idNewsletter
     * 
     * @return \Illuminate\Http\Response
     */
    public function getCustomerByNewsletter(int $idNewsletter)
    {
        return response()->success(NewsletterRecipientBL::getCustomerByNewsletter($idNewsletter));
    }

    /**
     * Get Contact by Newsletter
     * 
     * @param int idNewsletter
     * 
     * @return \Illuminate\Http\Response
     */
    public function getGroupByNewsletter(int $idNewsletter)
    {
        return response()->success(NewsletterRecipientBL::getGroupByNewsletter($idNewsletter));
    }

    /**
     * Get number recipients
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getNumberRecipients(int $id)
    {
        return response()->success(NewsletterRecipientBL::getNumberRecipients($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert contact
     * 
     * @param Request
     * 
     * @return int
     */
    public function insertContact(Request $request)
    {
        return response()->success(NewsletterRecipientBL::insertContact($request, HttpHelper::getLanguageId()));
    }

    /**
     * Insert customer
     * 
     * @param Request
     * 
     * @return int
     */
    public function insertCustomer(Request $request)
    {
        return response()->success(NewsletterRecipientBL::insertCustomer($request, HttpHelper::getLanguageId()));
    }

    /**
     * Insert group
     * 
     * @param Request
     */
    public function insertGroup(Request $request)
    {
        return response()->success(NewsletterRecipientBL::insertGroup($request, HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     */
    public function delete(int $id)
    {
        return response()->success(NewsletterRecipientBL::delete($id, HttpHelper::getLanguageId()));
    }

    /**
     * Delete
     * 
     * @param int idNewsletter
     * @param int idGroup
     */
    public function deleteGroup(int $idNewsletter, int $idGroup)
    {
        return response()->success(NewsletterRecipientBL::deleteGroup($idNewsletter, $idGroup, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

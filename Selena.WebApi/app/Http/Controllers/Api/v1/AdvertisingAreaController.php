<?php

namespace App\Http\Controllers\Api\v1;

use App\DtoModel\DtoAdvertisingArea;
use App\BusinessLogic\AdvertisingAreaBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\AdvertisingArea;
use App\Helpers\LogHelper;


class AdvertisingAreaController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(AdvertisingAreaBL::getById($id));
    }


        /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(AdvertisingAreaBL::getAll());
    }

    public function getHtmlBanner(Request $request){
        return response()->success(AdvertisingAreaBL::getHtmlBanner($request));
    }
    
    public function getByCode(string $code)
    {
        return response()->success(AdvertisingAreaBL::getByCode($code));
    }

    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(AdvertisingAreaBL::getSelect($search));
    }

    #endregion GET
    
    public function insert(Request $dtoAdvertisingArea)
    {
        return response()->success(AdvertisingAreaBL::insert($dtoAdvertisingArea,HttpHelper::getUserId()));
    }
    
    public function update(Request $dtoAdvertisingArea)
    {
        return response()->success(AdvertisingAreaBL::update($dtoAdvertisingArea));
    }

    public function delete(int $id)
    {
        return response()->success(AdvertisingAreaBL::delete($id));
    }
   
}

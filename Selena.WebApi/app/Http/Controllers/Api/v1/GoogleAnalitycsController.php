<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\GoogleAnalitycsBL;
use App\DtoModel\DtoChart;
use App\DtoModel\DtoDatasetChart;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class GoogleAnalitycsController
{

    public function getAccounts(Request $request)
    {
        return response()->success(GoogleAnalitycsBL::getAccounts());
    }

    public function setAccount(Request $request)
    {
        $viewID = $request->input('id');
        return response()->success(GoogleAnalitycsBL::setAccount($viewID, HttpHelper::getLanguageId()));
    }

    public function deleteAccount(Request $request)
    {
        $viewID = $request->input('id');
        return response()->success(GoogleAnalitycsBL::deleteAccount($viewID, HttpHelper::getLanguageId()));
    }

    public function insertAccount(Request $request)
    {

        return response()->success(GoogleAnalitycsBL::insertAccount($request, HttpHelper::getLanguageId()));
    }

    public function storeJsonKey(Request $request)
    {

        $jsonFile = json_encode($request->all(), JSON_PRETTY_PRINT);

        return response()->success(GoogleAnalitycsBL::storeJsonKey($jsonFile));
    }

    public function getPagesViews(Request $request)
    {

        $startDate = $request->input('start');
        $endDate = $request->input('end');

        return response()->success(GoogleAnalitycsBL::getPagesViews($startDate, $endDate, HttpHelper::getLanguageId()));
    }

    public function getVisitorsViews(Request $request)
    {

        $startDate = $request->input('start');
        $endDate = $request->input('end');

        return response()->success(GoogleAnalitycsBL::getVisitorsViews($startDate, $endDate, HttpHelper::getLanguageId()));
    }

    public function getUserActiveRealTime(Request $request)
    {

        return response()->success(GoogleAnalitycsBL::getUserActiveRealTime(HttpHelper::getLanguageId()));
    }

    public function getMostVisitedPages(Request $request)
    {

        $startDate = $request->input('start');
        $endDate = $request->input('end');
        $topType = $request->input('topType');
        return response()->success(GoogleAnalitycsBL::getMostVisitedPages($startDate, $endDate, $topType, HttpHelper::getLanguageId()));
    }

    public function getTotalUsers(Request $request)
    {

        $startDate = $request->input('start');
        $endDate = $request->input('end');

        return response()->success(GoogleAnalitycsBL::getTotalUsers($startDate, $endDate, HttpHelper::getLanguageId()));
    }

    public function getTotalSession(Request $request)
    {

        $startDate = $request->input('start');
        $endDate = $request->input('end');

        return response()->success(GoogleAnalitycsBL::getTotalSession($startDate, $endDate, HttpHelper::getLanguageId()));
    }

    public function getTotalPageView(Request $request)
    {

        $startDate = $request->input('start');
        $endDate = $request->input('end');

        return response()->success(GoogleAnalitycsBL::getTotalPageView($startDate, $endDate, HttpHelper::getLanguageId()));
    }

    public function getPageViewPerSession(Request $request)
    {

        $startDate = $request->input('start');
        $endDate = $request->input('end');

        return response()->success(GoogleAnalitycsBL::getPageViewPerSession($startDate, $endDate, HttpHelper::getLanguageId()));
    }

    public function getAvgSessionDuration(Request $request)
    {

        $startDate = $request->input('start');
        $endDate = $request->input('end');

        return response()->success(GoogleAnalitycsBL::getAvgSessionDuration($startDate, $endDate, HttpHelper::getLanguageId()));
    }

    public function getBounceRate(Request $request)
    {

        $startDate = $request->input('start');
        $endDate = $request->input('end');

        return response()->success(GoogleAnalitycsBL::getBounceRate($startDate, $endDate, HttpHelper::getLanguageId()));
    }

    public function getSourceSession(Request $request)
    {

        $startDate = $request->input('start');
        $endDate = $request->input('end');

        return response()->success(GoogleAnalitycsBL::getSourceSession($startDate, $endDate, HttpHelper::getLanguageId()));
    }

    public function getDeviceSession(Request $request)
    {

        $startDate = $request->input('start');
        $endDate = $request->input('end');

        return response()->success(GoogleAnalitycsBL::getDeviceSession($startDate, $endDate, HttpHelper::getLanguageId()));
    }
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\GroupDisplayTechnicalSpecificationTechnicalSpecificationBL;
use App\BusinessLogic\GroupTechnicalSpecificationTechnicalSpecificationBL;
use App\Http\Controllers\Controller;
use App\BusinessLogic\TechnicalSpecificationBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;
use App\BusinessLogic\LanguageTechnicalSpecificationBL;
use App\Helpers\LogHelper;

class TechnicalSpecificationController extends Controller
{
    #region GET

    /**
     * Get all for table
     *
     * @return \Illuminate\Http\Response
     */
    public function getDtoTable()
    {
        return response()->success(TechnicalSpecificationBL::getDtoTable());
    }

    /**
     * Check exists
     *
     * @param string description
     * @param int idLanguage
     * @param int idTechnicalSpecification
     * 
     * @return \Illuminate\Http\Response
     */
    public function checkExists(string $description, int $idLanguage, int $id = null)
    {
        return response()->success(LanguageTechnicalSpecificationBL::checkExists($description, $idLanguage, $id));
    }

    /**
     * Get all group display technical specification included and excluded
     * 
     * @param int $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getGroupDisplayTechnicalSpecification(int $id)
    {
        return response()->success(TechnicalSpecificationBL::getGroupDisplayTechnicalSpecification($id, HttpHelper::getLanguageId()));
    }

    /**
     * Get all group technical specification included and excluded
     * 
     * @param int $id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getGroupTechnicalSpecification(int $id)
    {
        return response()->success(TechnicalSpecificationBL::getGroupTechnicalSpecification($id, HttpHelper::getLanguageId()));
    }

    #endregion GET

    #region PUT

    /**
     * Insert
     *
     * @param Request
     *
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(TechnicalSpecificationBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Add group display technical specification
     * 
     * @param int id
     * @param int idGroupDisplayTechnicalSpecification
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function addGroupDisplayTechnicalSpecification(int $id, int $idGroupDisplayTechnicalSpecification, int $idFunctionality)
    {
        return response()->success(GroupDisplayTechnicalSpecificationTechnicalSpecificationBL::insert($idGroupDisplayTechnicalSpecification, $id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * Add group technical specification
     * 
     * @param int $id
     * @param int $idGroupTechnicalSpecification
     * @param int $idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function addGroupTechnicalSpecification(int $id, int $idGroupTechnicalSpecification, int $idFunctionality)
    {
        return response()->success(GroupTechnicalSpecificationTechnicalSpecificationBL::insert($idGroupTechnicalSpecification, $id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion PUT

    #region POST

    /**
     * Update
     *
     * @param Request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        return response()->success(TechnicalSpecificationBL::update($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion POST

    #region DELETE

    /**
     * delete
     *
     * @param int $id
     * @param int $idFunctionality
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id, int $idFunctionality)
    {
        return response()->success(TechnicalSpecificationBL::delete($id, $idFunctionality,  HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\GoogleAnalyticsBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class GoogleAnalyticsController
{
    #region GET

    /**
     * Most visited pages
     * 
     * @param Request request
     */
    public function getUserActiveRealTime(Request $request)
    {
        return response()->success(GoogleAnalyticsBL::getUserActiveRealTime(HttpHelper::getLanguageId()));
    }

    #endregion GET

    #region POST

    /**
     * Most pages views
     * 
     * @param Request request
     * 
     * @return DtoChartJS
     */
    public function getPagesViews(Request $request)
    {
        return response()->success(GoogleAnalyticsBL::getPagesViews($request, HttpHelper::getLanguageId()));
    }

    /**
     * Most visitors views
     * 
     * @param Request request
     * 
     * @return DtoChartJS
     */
    public function getVisitorsViews(Request $request)
    {
        return response()->success(GoogleAnalyticsBL::getVisitorsViews($request, HttpHelper::getLanguageId()));
    }

    /**
     * Most visited pages
     * 
     * @param Request request
     */
    public function getMostVisitedPages(Request $request)
    {
        return response()->success(GoogleAnalyticsBL::getMostVisitedPages($request, HttpHelper::getLanguageId()));
    }

    /**
     * Get total users
     * 
     * @param Request request
     */
    public function getTotalUsers(Request $request)
    {
        return response()->success(GoogleAnalyticsBL::getTotalUsers($request, HttpHelper::getLanguageId()));
    }

    /**
     * Get total session
     * 
     * @param Request request
     */
    public function getTotalSession(Request $request)
    {
        return response()->success(GoogleAnalyticsBL::getTotalSession($request, HttpHelper::getLanguageId()));
    }

    /**
     * Get total page view
     * 
     * @param Request request
     */
    public function getTotalPageView(Request $request)
    {
        return response()->success(GoogleAnalyticsBL::getTotalPageView($request, HttpHelper::getLanguageId()));
    }

    /**
     * Get Page view per session
     * 
     * @param Request request
     */
    public function getPageViewPerSession(Request $request)
    {
        return response()->success(GoogleAnalyticsBL::getPageViewPerSession($request, HttpHelper::getLanguageId()));
    }

    /**
     * Get avg session duration
     * 
     * @param Request request
     */
    public function getAvgSessionDuration(Request $request)
    {
        return response()->success(GoogleAnalyticsBL::getAvgSessionDuration($request, HttpHelper::getLanguageId()));
    }

    /**
     * Get bounce rate
     * 
     * @param Request request
     */
    public function getBounceRate(Request $request)
    {
        return response()->success(GoogleAnalyticsBL::getBounceRate($request, HttpHelper::getLanguageId()));
    }

    /**
     * Get source session
     * 
     * @param Request request
     * 
     * @return DtoChartJS
     */
    public function getSourceSession(Request $request)
    {
        return response()->success(GoogleAnalyticsBL::getSourceSession($request, HttpHelper::getLanguageId()));
    }
    
    /**
     * Get returning Visitors
     * 
     * @param Request request
     * 
     * @return DtoChartJS
     */
    public function getReturningVisitors(Request $request)
    {
        return response()->success(GoogleAnalyticsBL::getReturningVisitors($request, HttpHelper::getLanguageId()));
    }

    /**
     * Get device session
     * 
     * @param Request request
     * 
     * @return DtoChartJS
     */
    public function getDeviceSession(Request $request)
    {
        return response()->success(GoogleAnalyticsBL::getDeviceSession($request, HttpHelper::getLanguageId()));
    }

    /**
     * Store json key
     * 
     * @param Request request
     */
    public function storeJsonKey(Request $request)
    {
        return response()->success(GoogleAnalyticsBL::storeJsonKey($request));
    }

    #endregion POST
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ProducerBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class ProducerController
{
    #region GET  

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(ProducerBL::getById($id));
    }

    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(ProducerBL::getSelect($search));
    }

     /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelectUserShop(Request $request)
    {
        $search = trim($request->search);
        $url = trim($request->url);

        $description = trim($request->description);
        $priceFrom = trim($request->priceFrom);
        $priceTo = trim($request->priceTo);
        $condition = trim($request->condition);
        $category = trim($request->category);

        return response()->getSelect(ProducerBL::getSelectUserShop($search, $url, $description, $priceFrom, $priceTo, $condition, $category));
    }

    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelectConditionUserShop(Request $request)
    {
        $search = trim($request->search);
        $url = trim($request->url);

        $description = trim($request->description);
        $priceFrom = trim($request->priceFrom);
        $priceTo = trim($request->priceTo);
        $producer = trim($request->producer);
        $category = trim($request->category);

        return response()->getSelect(ProducerBL::getSelectConditionUserShop($search, $url, $description, $priceFrom, $priceTo, $producer, $category));
    }

    /**
     * Get select Categories Filter
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelectCategoriesFilter(Request $request)
    {
        $search = trim($request->search);
        $url = trim($request->url);

        $priceFrom = trim($request->priceFrom);
        $priceTo = trim($request->priceTo);
        $condition = trim($request->condition);

        return response()->getSelect(ProducerBL::getSelectCategoriesFilter($search, $url, $priceFrom, $priceTo, $condition));
    }

    /**
     * Get select Condition Categories Filter
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelectConditionCategoriesFilter(Request $request)
    {
        $search = trim($request->search);
        $url = trim($request->url);

        $priceFrom = trim($request->priceFrom);
        $priceTo = trim($request->priceTo);
        $producer = trim($request->producer);

        return response()->getSelect(ProducerBL::getSelectConditionCategoriesFilter($search, $url, $priceFrom, $priceTo, $producer));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(ProducerBL::getAll($idLanguage));
    }

    #endregion GET

    #region INSERT
    
    /**
     * Insert
     *
     * @param Request DtoProducer
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoProducer)
    {
        return response()->success(ProducerBL::insert($DtoProducer, HttpHelper::getUserId()));
       
    }

    /**
     * Clone
     *
     * @param int $id
     * @param int $idFunctionality
     * 
     */
    
    public function clone(int $id, int $idFunctionality){
        return response()->success(ProducerBL::clone($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request DtoProducer
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoProducer)
    {
        return response()->success(ProducerBL::update($DtoProducer));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(ProducerBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

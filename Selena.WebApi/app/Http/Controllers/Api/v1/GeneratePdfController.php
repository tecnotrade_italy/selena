<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
//use PDF;
use Barryvdh\DomPDF\Facade as PDF;
use App\Helpers\LogHelper;
use App\DtoModel\DtoInterventionsProcessing;
use App\DtoModel\DtoItemGr;
use App\DtoModel\DtoItems;
use App\DtoModel\DtoItemsPhotos;
use App\DtoModel\DtoPaymentGr;
use App\DtoModel\DtoPeople;
use App\DtoModel\DtoQuotes;
use App\DtoModel\DtoUser;
use App\Item;
use App\GrStates;
use App\GrIntervention;
use App\GrInterventionPhoto;
use App\GrInterventionProcessing;
use App\GrQuotes;
use App\Helpers\HttpHelper;
use App\ItemGr;
use App\ItemLanguage;
use App\PaymentGr;
use App\People;
use App\QuotesGrIntervention;
use App\SelenaSqlViews;
use App\Setting;
use App\StatesQuotesGr;
use App\User;
use App\UsersDatas;

class GeneratePdfController extends Controller {
    
    public function pdfview(Request $request){
        $items = DB::table("gr_interventions")->get();
        view()->share('items', $items);
      //  if($request->has('download')){
            $pdf = PDF::loadView('pdfview');
            return $pdf->download('pdfview.pdf');
      //  }
       // return view('pdfview');
    }

        public function pdfviewquotes(Int $id){
            
            $gr_interventions= DB::table('gr_interventions')->where('id', $id)->first();
            $users_datas= DB::table("users_datas")->where('user_id', $gr_interventions->user_id)->first();

            if (isset($users_datas)){
                $DtoUserDatas = new DtoUser;
                $DtoUserDatas->business_name = $users_datas->business_name;
                $DtoUserDatas->address = $users_datas->address;
                $DtoUserDatas->postal_code = $users_datas->postal_code;
                $DtoUserDatas->country = $users_datas->country;   
            }else{
                $DtoUserDatas = new DtoUser;
                $DtoUserDatas->business_name = '';
                $DtoUserDatas->address = '';
                $DtoUserDatas->postal_code = '';
                $DtoUserDatas->country = '';
            }

            $people = People::where('id', $gr_interventions->referent)->first();
             if (isset($people)){
                $DtoPeople = new DtoPeople;
                $DtoPeople->name = $people->name;
            }else{
                 $DtoPeople = new DtoPeople;
                 $DtoPeople->name = '';
            }

            $gr_quotes = GrQuotes::where('id_intervention', $id)->first();
            if (isset($gr_quotes)){
               $DtoGrQuotes = new DtoQuotes;

            if (isset($gr_quotes->date_agg)){
                  $DtoGrQuotes->date_agg = $gr_quotes->date_agg;
            }else{
                  $DtoGrQuotes->date_agg = '';
            }
            if (isset($gr_quotes->object)){
               $DtoGrQuotes->object = $gr_quotes->object;
            }else{
               $DtoGrQuotes->object = '';
            }
            if (isset($gr_quotes->email)){
               $DtoGrQuotes->email = $gr_quotes->email;
            }else{
               $DtoGrQuotes->email = '';
            }
            if (isset($gr_quotes->note_before_the_quote)){
               $DtoGrQuotes->note_before_the_quote = $gr_quotes->note_before_the_quote;
            }else{
               $DtoGrQuotes->note_before_the_quote = '';
            }
            if (isset($gr_quotes->description_and_items_agg)){
               $DtoGrQuotes->description_and_items_agg = $gr_quotes->description_and_items_agg;
            }else{
               $DtoGrQuotes->description_and_items_agg = '';
            }
            if (isset($gr_quotes->tot_quote)){
               $DtoGrQuotes->tot_quote = $gr_quotes->tot_quote;
            }else{
               $DtoGrQuotes->tot_quote = '';
            }
            if ($gr_quotes->total_amount_with_discount !== '€  Netto + Iva') {
               $DtoGrQuotes->total_amount_with_discount = 'Totale Scontato ' . $gr_quotes->total_amount_with_discount;
            }else{
               $DtoGrQuotes->total_amount_with_discount = '';
            }
            if (isset($gr_quotes->discount)){
               $DtoGrQuotes->discount = 'Sconto € ' . $gr_quotes->discount;
            }else{
               $DtoGrQuotes->discount = '';
            }
            if (isset($gr_quotes->kind_attention)){
               $DtoGrQuotes->kind_attention = $gr_quotes->kind_attention;
            }else{
               $DtoGrQuotes->kind_attention = '';
            }

         }

            $gr_payments = PaymentGr::where('id', $gr_quotes->description_payment)->first();
             if (isset($gr_payments)){
                $DtoGrPayments = new DtoPaymentGr;
                $DtoGrPayments->description = $gr_payments->description;
            }else{
                 $DtoGrPayments = new DtoPaymentGr;
                 $DtoGrPayments->description = ''; 
            }

            //$gr_intervention_processing = GrInterventionProcessing::where('id_intervention', $id)->get();
                $DtoInterventionProcessing = new DtoInterventionsProcessing;
                $gr_intervention_processing = GrInterventionProcessing::where('id_intervention', $id)->get();
                foreach ($gr_intervention_processing as $gr_intervention_processings) {

                    if (isset($gr_intervention_processing)){
                        $DtoInterventionProcessings = new DtoInterventionsProcessing;
   
                        $ItemGr=ItemGr::where('id', $gr_intervention_processings->id_gr_items)->first();
                        if (isset($ItemGr)){
                           $DtoInterventionProcessings->code_gr = $ItemGr->code_gr;
                        }else{
                            $DtoInterventionProcessings->code_gr = '';
                        }
                        if (isset($gr_intervention_processings->description)){
                           $DtoInterventionProcessings->description = $gr_intervention_processings->description;
                        }else{
                           $DtoInterventionProcessings->description = '';
                        }
                        if (isset($gr_intervention_processings->n_u)){
                           $DtoInterventionProcessings->n_u = $gr_intervention_processings->n_u;
                        }else{
                           $DtoInterventionProcessings->n_u = '';
                        }
                        if (isset($gr_intervention_processings->qta)){
                           $DtoInterventionProcessings->qta = $gr_intervention_processings->qta;
                        }else{
                           $DtoInterventionProcessings->qta = '';
                        }
                        if (isset($gr_intervention_processings->price_list)){
                           $DtoInterventionProcessings->price_list = $gr_intervention_processings->price_list;
                        }else{
                           $DtoInterventionProcessings->price_list = '';
                        }
                        if (isset($gr_intervention_processings->tot)){
                           $DtoInterventionProcessings->tot = $gr_intervention_processings->tot;
                        }else{
                           $DtoInterventionProcessings->tot = '';
                        }
                     }
                     
                   
                        $DtoInterventionProcessing->arrayresult->push($DtoInterventionProcessings);              
                }
         
            view()->share('gr_interventions',$gr_interventions);  
            view()->share('users_datas',$DtoUserDatas->toArray()); 
            view()->share('gr_quotes',$DtoGrQuotes->toArray()); 
            view()->share('people',$DtoPeople->toArray()); 
            view()->share('gr_payments',$DtoGrPayments->toArray());
            view()->share('gr_intervention_processing',$DtoInterventionProcessing->arrayresult->toArray());

        // if($id->has('download')){     
            $pdf = PDF::loadView('pdfviewquotes');
            return $pdf->download('pdf_preventivo_' . $id . '.pdf');
        //}  
           //return view('pdfviewquotes');         
            }

    public function pdfviewprocessingsheet(Int $id){

            $gr_interventions= GrIntervention::where('id', $id)->first();
            
            $usersdatas= DB::table("users_datas")->where('user_id', $gr_interventions->user_id)->first();
            if (isset($usersdatas)){
                $DtoUserBusinessName = new DtoUser;
                $DtoUserBusinessName->business_name = $usersdatas->business_name;
            }else{
                $DtoUserBusinessName = new DtoUser;
                $DtoUserBusinessName->business_name = '';
            }

            $people= DB::table("people")->where('id',$gr_interventions->referent)->first();
             if (isset($people)){
                $DtoPeople = new DtoPeople;
                $DtoPeople->name = $people->name;
            }else{
                 $DtoPeople = new DtoPeople;
                 $DtoPeople->name = '';
            }
              
            $gr_items=ItemGr::where('id', $gr_interventions->items_id)->first();
             if (isset($gr_items)){
                $DtoItems = new DtoItems;
                $DtoItems->code_gr = $gr_items->code_gr;
            }else{
                 $DtoItems = new DtoItems;
                 $DtoItems->code_gr = '';
            }

            $gr_intervention_processinge = GrInterventionProcessing::where('id_intervention', $id)->first();
            if (isset($gr_intervention_processinge->created_at)){
             $DtoInterventionProcessingCreated_At = new DtoInterventionsProcessing;
             $DtoInterventionProcessingCreated_At->created_at = date_format($gr_intervention_processinge->created_at, 'd/m/Y H:i:s');
             }else{
                   $DtoInterventionProcessingCreated_At = new DtoInterventionsProcessing;
                   $DtoInterventionProcessingCreated_At->created_at = '' ;
             }
      
             $DtoInterventionProcessing = new DtoInterventionsProcessing;
             $gr_intervention_processing = GrInterventionProcessing::where('id_intervention', $id)->get();

             foreach ($gr_intervention_processing as $gr_intervention_processings) {
                 if (isset($gr_intervention_processing)){
                     $DtoInterventionProcessings = new DtoInterventionsProcessing;

                     $ItemGr=ItemGr::where('id', $gr_intervention_processings->id_gr_items)->first();
                     if (isset($ItemGr)){
                        $DtoInterventionProcessings->code_gr = $ItemGr->code_gr;
                     }else{
                         $DtoInterventionProcessings->code_gr = '';
                     }
                     if (isset($gr_intervention_processings->description)){
                        $DtoInterventionProcessings->description = $gr_intervention_processings->description;
                     }else{
                        $DtoInterventionProcessings->description = '';
                     }
                     if (isset($gr_intervention_processings->n_u)){
                        $DtoInterventionProcessings->n_u = $gr_intervention_processings->n_u;
                     }else{
                        $DtoInterventionProcessings->n_u = '';
                     }
                     if (isset($gr_intervention_processings->qta)){
                        $DtoInterventionProcessings->qta = $gr_intervention_processings->qta;
                     }else{
                        $DtoInterventionProcessings->qta = '';
                     }
                     if (isset($gr_intervention_processings->price_list)){
                        $DtoInterventionProcessings->price_list = $gr_intervention_processings->price_list;
                     }else{
                        $DtoInterventionProcessings->price_list = '';
                     }
                     if (isset($gr_intervention_processings->tot)){
                        $DtoInterventionProcessings->tot = $gr_intervention_processings->tot;
                     }else{
                        $DtoInterventionProcessings->tot = '';
                     }

                  }

                     $DtoInterventionProcessing->arrayresult->push($DtoInterventionProcessings);              
             }


            view()->share('gr_interventions',$gr_interventions); 
            view()->share('gr_items',$DtoItems->toArray());
            view()->share('people', $DtoPeople->toArray());
            view()->share('users', $DtoUserBusinessName->toArray()); 
            view()->share('gr_intervention_processingsss', $DtoInterventionProcessingCreated_At->toArray()); 
            view()->share('gr_intervention_processing',$DtoInterventionProcessing->arrayresult->toArray());

        // if($id->has('download')){
            $pdf = PDF::loadView('pdfviewprocessingsheet');
            return $pdf->download('pdf_foglio_di_lavorazione_' . $id . '.pdf');
        //}  
            
           return view('pdfviewprocessingsheet');         
            }   
}

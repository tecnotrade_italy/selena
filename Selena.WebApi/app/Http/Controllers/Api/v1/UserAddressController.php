<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\UserAddressBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;

class UserAddressController
{
    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(UserAddressBL::insert($request));
    }

    /**
     * acrtive address by user
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function activeAddressByUser(Request $request)
    {
        return response()->success(UserAddressBL::activeAddressByUser($request));
    }


    /**
     * reset Address By Cart Id
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function resetAddressByCartId(Request $request)
    {
        return response()->success(UserAddressBL::resetAddressByCartId($request));
    }
    
    #endregion INSERT

    #region GET

    /**
     * Get address by user id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getAllByUser(Int $id)
    {
        return response()->success(UserAddressBL::getAllByUser($id));
    }

    #endregion GET

    #region DELETE

    /**
     * soft delete Address By Id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function deleteAddressById(Int $id)
    {
        return response()->success(UserAddressBL::deleteAddressById($id));
    }

    #endregion DELETE
}

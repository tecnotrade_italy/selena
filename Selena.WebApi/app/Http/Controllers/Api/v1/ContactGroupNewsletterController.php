<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ContactGroupNewsletterBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ContactGroupNewsletterController extends Controller
{
    #region GET

    /**
     * Get included by contact
     * 
     * @param int idContact
     *
     * @return Response
     */
    public function getIncludedByContact(int $idContact)
    {
        return response()->success(ContactGroupNewsletterBL::getIncludedByContact($idContact));
    }

    /**
     * Get excluded by contact
     *
     * @param int idContact
     * 
     * @return Response
     */
    public function getExcludedByContact(int $idContact)
    {
        return response()->success(ContactGroupNewsletterBL::getExcludedByContact($idContact));
    }


    /**
     * Get included by contact
     * 
     * @param int idContact
     *
     * @return Response
     */
    public function getIncludedByContactGroup(int $idContact)
    {
        return response()->success(ContactGroupNewsletterBL::getIncludedByContactGroup($idContact));
    }

    /**
     * Get excluded by contact
     *
     * @param int idContact
     * 
     * @return Response
     */
    public function getExcludedByContactGroup(int $idContact)
    {
        return response()->success(ContactGroupNewsletterBL::getExcludedByContactGroup($idContact));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @param Request dtoContact
     * 
     * @return Response
     */
    public function insert(Request $dtoContact)
    {
        return response()->success(ContactGroupNewsletterBL::insert($dtoContact, HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoContact
     * 
     * @return Response
     */
    public function update(Request $dtoContact)
    {
        return response()->success(ContactGroupNewsletterBL::update($dtoContact, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return Response
     */
    public function delete(int $id)
    {
        return response()->success(ContactGroupNewsletterBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

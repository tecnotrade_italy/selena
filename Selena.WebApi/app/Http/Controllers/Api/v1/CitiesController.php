<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\CitiesBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;

class CitiesController
{
    #region GET
 
    /**
     * Get all cities
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(CitiesBL::getSelect($search));
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(CitiesBL::getAll($idLanguage));
    }

    /**
     * Get by id
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function getById(int $id)
    {
        return response()->success(CitiesBL::getById($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        return response()->success(CitiesBL::insert($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoCities
     * 
     * @return \Illuminate\Http\Response 
     */
    public function update(Request $dtoCities)
    {
        return response()->success(CitiesBL::update($dtoCities, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(CitiesBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE



}

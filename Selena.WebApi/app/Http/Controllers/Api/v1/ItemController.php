<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\ItemBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;
use LogHeader;

class ItemController
{
    #region GET

    /**
     * Get all items
     *
     * @param int $idItem
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        return response()->success(ItemBL::getAllDto(HttpHelper::getLanguageId()));
    }

    /**
     * Get with group technical specification
     * 
     * @return \Illuminate\Http\Response
     */
    public function getWithGroupTechnicalSpecification()
    {
        return response()->success(ItemBL::getWithGroupTechnicalSpecification(HttpHelper::getLanguageId()));
    }

    /**
     * Get all items graphic
     *
     * @param int $idItem
     *
     * @return \Illuminate\Http\Response
     */
    public function getGraphic()
    {
        return response()->success(ItemBL::getGraphic());
    }

    /**
     * Get item photos
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getImages(Int $id)
    {
        return response()->success(ItemBL::getImages($id));
    }

    /**
     * Get number of item
     * 
     * @return \Illuminate\Http\Response
     */
    public function count()
    {
        return response()->success(ItemBL::count());
    }

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(ItemBL::getById($id));
    }

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByPriceList(Int $id)
    {
        return response()->success(ItemBL::getByPriceList($id));
    }

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getByImgAgg(Int $id)
    {
        return response()->success(ItemBL::getByImgAgg($id));
    }

    /**
     * get Document Information By Id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getDocumentInformationById(Int $id)
    {
        return response()->success(ItemBL::getDocumentInformationById($id));
    }


    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getAllRelated(Int $id, int $idLanguage)
    {
        return response()->success(ItemBL::getAllRelated($id, $idLanguage));
    }

    /**
     * getAllRelatedSearch
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getAllRelatedSearch(Request $request)
    {
        //$search = trim($request->search);
        return response()->success(ItemBL::getAllRelatedSearch($request));
    }

    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(ItemBL::getSelect($search));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(int $idLanguage)
    {
        return response()->success(ItemBL::getAll($idLanguage));
    }

    /**
     * Get all items graphic
     *
     * @param int $idItem
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllImagesInFolder(int $id)
    {
        return response()->success(ItemBL::getAllImagesInFolder($id));
    }

    /**
     * Get item by user shop 
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getItemsUserShop(Request $request)
    {
        return response()->success(ItemBL::getItemsUserShop($request));
    }

    /**
     * Get item by category shop 
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getItemsCategoriesFilter(Request $request)
    {
        return response()->success(ItemBL::getItemsCategoriesFilter($request));
    }

    #endregion GET

    /**
     * Get getByFileNoId 
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByFileNoId(Request $request)
    {
        return response()->success(ItemBL::getByFileNoId($request));
    }
    #endregion getByFileNoId

    /**
     * Get getByTranslate 
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByTranslate(Int $id)
    {
        return response()->success(ItemBL::getByTranslate($id));
    }
    #endregion getByTranslate


    /**
     * Get getByTranslateDefault 
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByTranslateDefault(Int $id, int $idItem, int $idLanguage)
    {
        return response()->success(ItemBL::getByTranslateDefault($id, $idItem, $idLanguage));
    }
    #endregion getByTranslateDefault

    /**
     * Get getByTranslateDefaultNew
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByTranslateDefaultNew(int $idItem, int $idLanguage)
    {
        return response()->success(ItemBL::getByTranslateDefaultNew($idItem, $idLanguage));
    }
    #endregion getByTranslateDefault

    /**
     * Get item photos
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getItemLanguages(Int $id)
    {
        return response()->success(ItemBL::getItemLanguages($id));
    }

    /**
     * get All Items Without Category
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getAllItemsWithoutCategory(request $request)
    {
        return response()->success(ItemBL::getAllItemsWithoutCategory($request));
    }

    /**
     * Get getByFile 
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getByFile(Int $id)
    {
        return response()->success(ItemBL::getByFile($id));
    }
    #endregion getByFile

    #region INSERT

    /**
     * Insert
     *
     * @param Request DtoItems
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $DtoItems)
    {
        return response()->success(ItemBL::insert($DtoItems, HttpHelper::getUserId()));
    }

    /**
     * Insert related
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertRelated(Request $request)
    {
        return response()->success(ItemBL::insertRelated($request, HttpHelper::getUserId()));
    }
    /**
     * Insert related
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertTranslate(Request $request)
    {
        return response()->success(ItemBL::insertTranslate($request, HttpHelper::getUserId()));
    }


    /** insert Item Categories
     *
     * @param Request DtoItems
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertItemCategories(Request $request)
    {
        return response()->success(ItemBL::insertItemCategories($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    /**
     * insert Item Images
     *
     * @param Request DtoItems
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertItemImages(Request $request)
    {
        return response()->success(ItemBL::insertItemImages($request, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }


    /**
     * Get getPriceItemSupport 
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getPriceItemSupport(Request $request)
    {
        return response()->success(ItemBL::getPriceItemSupport($request));
    }

     /**
     * Get getPriceItemSupportAndAddParams 
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function getPriceItemSupportAndAddParams(Request $request)
    {
        return response()->success(ItemBL::getPriceItemSupportAndAddParams($request));
    }

    /**
     * Get downloadfile 
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function downloadfile(Request $request)
    {
        return response()->success(ItemBL::downloadfile($request));
    }

    


    /**
     * Clone
     *
     * @param int $id
     * @param int $idFunctionality
     * 
     */

    public function clone(int $id, int $idFunctionality)
    {
        return response()->success(ItemBL::clone($id, $idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request DtoItems
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $DtoItems)
    {
        return response()->success(ItemBL::update($DtoItems));
    }

    /**
     * Update img name
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateImageName(Request $request)
    {
        return response()->success(ItemBL::updateImageName($request));
    }

    /**
     * update Order Categories Item Table
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateOrderCategoriesItemTable(Request $request)
    {
        return response()->success(ItemBL::updateOrderCategoriesItemTable($request));
    }

    

    /**
     * update Item Images
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateItemImages(Request $request)
    {
        return response()->success(ItemBL::updateItemImages($request));
    }


    /**
     * Update Order Imame Photos
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateOrderImamePhotos(Request $request)
    {
        return response()->success(ItemBL::updateOrderImamePhotos($request));
    }

    /**
     * update Item Check Association
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateItemCheckAssociation(Request $request)
    {
        return response()->success(ItemBL::updateItemCheckAssociation($request));
    }

    /**
     * update Online Item
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateOnlineItem(Request $request)
    {
        return response()->success(ItemBL::updateOnlineItem($request));
    }

      /**
     * updateImageAggFile
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateImageAggFile(Request $request)
    {
        return response()->success(ItemBL::updateImageAggFile($request));
    }


    /**
     * update favorites item
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateOnlineItemCheck(Request $request)
    {
        return response()->success(ItemBL::updateOnlineItemCheck($request));
    }

    public function updateAnnouncementsItemCheck(Request $request)
    {
        return response()->success(ItemBL::updateAnnouncementsItemCheck($request));
    }

    public function updatePromotionItemCheck(Request $request)
    {
        return response()->success(ItemBL::updatePromotionItemCheck($request));
    }
    
    public function addFavoritesItem(Request $request)
    {
        return response()->success(ItemBL::addFavoritesItem($request));
    }

        
    public function movePhoto()
    {
        return response()->success(ItemBL::movePhoto());
    }

    public function moveFile()
    {
        return response()->success(ItemBL::moveFile());
    }

    
    #endregion UPDATE

    /**
     * Get all data
     *
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getItemById(int $id)
    {
        return response()->success(ItemBL::getItemById($id));
    }


    /**
     * updateImages
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */

    public function updateImages(Request $request)
    {
        return response()->success(ItemBL::updateImages($request));

        // return response()->success(ItemBL::updateNoAuth($request,HttpHelper::getUserId()));
    }


    /**
     * updateTranslate
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */

    public function updateTranslate(Request $request)
    {
        return response()->success(ItemBL::updateTranslate($request));
    }

    /**
     * Update
     *
     * @param Request $request
     * 
     * @return \Illuminate\Http\Response
     */

    public function updateNoAuth(Request $request)
    {
        return response()->success(ItemBL::updateNoAuth($request, $request->updated_id));

        // return response()->success(ItemBL::updateNoAuth($request,HttpHelper::getUserId()));
    }

    #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id)
    {
        return response()->success(ItemBL::delete($id, HttpHelper::getLanguageId()));
    }

    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function deleteItemsPhotos(int $id)
    {
        return response()->success(ItemBL::deleteItemsPhotos($id, HttpHelper::getLanguageId()));
    }



    /**
     * Delete
     *
     * @param int $idCategories
     * @param int $idItem
     * @return int
     */
    public function deleteItemCategories(int $idCategories, int $idItem)
    {
        return response()->success(ItemBL::deleteItemCategories($idCategories, $idItem, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function RemoveFileDb(int $id)
    {
        return response()->success(ItemBL::RemoveFileDb($id, HttpHelper::getLanguageId()));
    }
    /**
     * Delete Cancel Translate
     *
     * @param int $id
     * @return int
     */
    public function getCancelTranslate(int $id)
    {
        return response()->success(ItemBL::getCancelTranslate($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
    /**
     * Delete
     *
     * @param int $id
     * @return int
     */
    public function removeFile(int $id)
    {
        return response()->success(ItemBL::removeFile($id, HttpHelper::getLanguageId()));
    }


    /**
     * Get list items
     *
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public function getListItems(int $sUserId)
    {
        return response()->success(ItemBL::getListItems($sUserId));
    }

       /**
     * Delete
     *
     * @param int $idCategories
     * @param int $idItem
     * @return int
     */
    public function delRelated(int $idItems, int $rowIdSelected)
    {
        return response()->success(ItemBL::delRelated($idItems, $rowIdSelected, HttpHelper::getLanguageId()));
    }

}

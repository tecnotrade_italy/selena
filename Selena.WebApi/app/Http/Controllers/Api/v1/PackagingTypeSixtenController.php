<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\PackagingTypeSixtenBL;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class PackagingTypeSixtenController
{
    #region GET

    /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(PackagingTypeSixtenBL::getSelect($search, $request->idFunctionality, HttpHelper::getUserId(), HttpHelper::getLanguageId()));
    }

    #endregion GET
}

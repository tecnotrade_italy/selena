<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\PostalCodeBL;
use App\Helpers\HttpHelper;
use Symfony\Component\HttpFoundation\Request;

class PostalCodeController
{
    #region GET

    /**
     * Get by id
     *
     * @param Int $id
     * @return \Illuminate\Http\Response
     */
    public function getById(Int $id)
    {
        return response()->success(PostalCodeBL::getById($id));
    }

    /**
     * Get all
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(PostalCodeBL::getSelect($search));
    }

    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(PostalCodeBL::getAll());
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @param Request dtoPostalCode
     * 
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $dtoPostalCode)
    {
        return response()->success(PostalCodeBL::insert($dtoPostalCode, HttpHelper::getLanguageId()));
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoPostalCode
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $dtoPostalCode)
    {
        return response()->success(PostalCodeBL::update($dtoPostalCode, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(PostalCodeBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}

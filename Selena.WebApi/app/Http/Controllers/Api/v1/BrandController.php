<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\BrandBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class BrandController
{
  

 /**
     * Get select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(BrandBL::getSelect($search));
    }


}
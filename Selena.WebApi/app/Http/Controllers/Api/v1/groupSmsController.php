<?php

namespace App\Http\Controllers\Api\v1;

use App\BusinessLogic\groupSmsBL;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;

class groupSmsController
{
    #region POST
    /**
     * Insert
     *
     * @param Request request
     * 
     * @return \Illuminate\Http\Response
     */
    public function insertGroupSms(Request $request)
    {
        return response()->success(groupSmsBL::insertGroupSms($request));
    }

    #endregion POST

    #region GET
    /**
     * Get all
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        return response()->success(groupSmsBL::getAll());
    }

    /**
     * Get all group select
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getSelect(Request $request)
    {
        $search = trim($request->search);
        return response()->getSelect(groupSmsBL::getSelect($search));
    }
    #endregion GET

    #region UPDATE

    /**
     * Update
     *
     * @param Request dtoData
     * 
     * @return \Illuminate\Http\Response
     */
    public function updateGroupSms(Request $dtoData)
    {
        return response()->success(groupSmsBL::updateGroupSms($dtoData, HttpHelper::getLanguageId()));
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * 
     * @return \Illuminate\Http\Response
     */
    public function delete(int $id)
    {
        return response()->success(groupSmsBL::delete($id, HttpHelper::getLanguageId()));
    }

    #endregion DELETE
}
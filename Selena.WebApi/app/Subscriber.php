<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;

class Subscriber extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'subscriber';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];


    /**
     * Get date_ddt with format
     */
  //  public function getUpdateDateAttribute($value)
    //{
       // if (!is_null($value)) {
         //   return Carbon::parse($value)->format(HttpHelper::getLanguageDateFormat());
       // }
   // }

    /**
     * Set date_ddt from client format date to server format
     */
   // public function setUpdateDateAttribute($value)
//{
       // if (!is_null($value)) {
         //   $this->attributes['subscription_date'] = Carbon::createFromFormat(HttpHelper::getLanguageDateFormat(), $value)->format('d/m/Y');
         //   $this->attributes['expiration_date'] = Carbon::createFromFormat(HttpHelper::getLanguageDateFormat(), $value)->format('d/m/Y');
        //}

  /**
     * Get subscription_date with format
     */
    public function getSubscriptionDateAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set subscription_date from client format date to server format
     */
    public function setSubscriptionDateAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['subscription_date'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d');
        }else{
            $this->attributes['subscription_date'] = null;
        }
    }
     /**
     * Get expiration_date with format
     */
    public function getExpirationDateAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set expiration_date from client format date to server format
     */
    public function setExpirationDateAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['expiration_date'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d');
        }else{
            $this->attributes['expiration_date'] = null;
        }
    }


    }


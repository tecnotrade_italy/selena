<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;

class GrInterventionPhoto extends Model
{
    use ObservantTrait;


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at'
    ];

    #region DATE FORMAT FIELD

    /**
     * Get enabled_from with format
     */
    public function getEnabledFromAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set enabled_from from client format date to server format
     */
    public function setEnabledFromAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['date_ddt'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }
    }

    /**
     * Get enabled_to with format
     */
    public function getEnabledToAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set enabled_to from client format date to server format
     */
    public function setEnabledToAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['date_ddt_gr'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }
    }

    #endregion DATE FORMAT FIELD
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;

class DocumentHead extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'documents_heads';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];

    #region DATE FORMAT FIELD

    /**
     * Get date with format
     */
    public function getDateAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set date from client format date to server format
     */
    public function setDateAttribute($value)
    {
        if (!is_null($value)) {
            if (HttpHelper::getLanguageDateTimeFormat() != 'Y/m/d H:i:s') {
                $this->attributes['date'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
            } else {
                $this->attributes['date'] = $value;
            }
        }
    }

    /**
     * Get shipping_date with format
     */
    public function getShippingDateAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set shipping_date from client format date to server format
     */
    public function setShippingDateAttribute($value)
    {
        if (!is_null($value)) {
            if (HttpHelper::getLanguageDateTimeFormat() != 'Y/m/d H:i:s') {
                $this->attributes['shipping_date'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
            } else {
                $this->attributes['shipping_date'] = $value;
            }
        }
    }

    /**
     * Get expected_shipping_date with format
     */
    public function getExpectedShippingDateAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set expected_shipping_date from client format date to server format
     */
    public function setExpectedShippingDateAttribute($value)
    {
        if (!is_null($value)) {
            if (HttpHelper::getLanguageDateTimeFormat() != 'Y/m/d H:i:s') {
                $this->attributes['expected_shipping_date'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
            } else {
                $this->attributes['expected_shipping_date'] = $value;
            }
        }
    }

    #endregion DATE FORMAT FIELD
}

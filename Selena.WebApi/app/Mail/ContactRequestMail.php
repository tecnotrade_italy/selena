<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\Storage;

class ContactRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The name instance.
     *
     * @var Name
     */
    protected $name;

    /**
     * The surname instance.
     *
     * @var Surname
     */
    protected $surname;

    /**
     * The email instance.
     *
     * @var Email
     */
    protected $email;

    /**
     * The description instance.
     *
     * @var Description
     */
    protected $description;

    /**
     * The telefono instance.
     *
     * @var telefono
     */
    protected $telefono;

    /**
     * The businessName instance.
     *
     * @var businessName
     */
    protected $businessName;

    /**
     * The vatNumber instance.
     *
     * @var vatNumber
     */
    protected $vatNumber;

    /**
     * The province instance.
     *
     * @var province
     */
    protected $province;

    /**
     * The postalCode instance.
     *
     * @var postalCode
     */
    protected $postalCode;

         /**
     * The city instance.
     *
     * @var city
     */
    protected $city;

    /**
     * The address instance.
     *
     * @var address
     */
    protected $address;





    /**
     * Create a new message instance.
     * 
     * @param string name 
     * @param string surname 
     * @param string email 
     * @param string description
     * @param string phoneNumber
     *
     * @return void
     */
    public function __construct(string $name, string $surname, string $email, string $description, string $telefono, string $businessName, string $province, string $vatNumber, string $postalCode, string $processing_of_personal_data, string $city, string $address)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
        $this->description = $description;
        $this->telefono = $telefono;
        $this->businessName = $businessName;
        $this->province = $province;
        $this->vatNumber = $vatNumber;
        $this->postalCode = $postalCode;
        $this->processing_of_personal_data = $processing_of_personal_data;
        $this->city = $city;
        $this->address = $address;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         //implementare estrazione css da file nuovo
        $cssEmail = Storage::disk('public')->get('\css\webCssEmail.css');
   

        return $this
        
            ->subject('Richiesta di contatto da ' . $this->name . ' ' . $this->surname . ' - ' . $this->email . ' - ' . $this->telefono)
            ->view('mail.contactRequest')
            ->with(['name' => $this->name, 'surname' => $this->surname, 'email' => $this->email, 'description' => $this->description, 'telefono' => $this->telefono, 'businessName' => $this->businessName, 'province' => $this->province, 'vatNumber' => $this->vatNumber, 'postalCode' => $this->postalCode,
             'processing_of_personal_data' => $this->processing_of_personal_data,'city' => $this->city, 'address' => $this->address, 'cssEmail' => $cssEmail]);
    }
}

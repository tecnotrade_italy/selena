<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class NegotiationMail extends Mailable
{
    use Queueable, SerializesModels;

     /**
     * The email from instance.
     *
     * @var emailFrom
     */
    protected $emailFrom;


    /**
     * The name instance.
     *
     * @var Name
     */
    protected $name;

    /**
     * The negotiation instance.
     *
     * @var negotiation
     */
    protected $negotiation;

    /**
     * Create a new message instance.
     * 
     * @param string name
     *
     * @return void
     */
    public function __construct(string $emailFrom, string $name, string $negotiation)
    {
        $this->emailFrom = $emailFrom;
        $this->name = $name;
        $this->negotiation = $negotiation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         //implementare estrazione css da file nuovo
        $cssEmail = Storage::disk('public')->get('\css\webCssEmail.css');

        return $this
            ->from($this->emailFrom)
            ->subject('Nuovo messaggio in trattativa')
            ->view('mail.negotiationMail')
            ->with(['name' => $this->name, 'negotiation' => $this->negotiation,'cssEmail' => $cssEmail]);
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\BusinessLogic\SettingBL;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\Storage;

class DynamicMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $subjectMail;
    protected $html;

    public function __construct(string $subjectMail, string $html)
    {

        $this->subjectMail = $subjectMail;
        $this->htmlr = $html;
      
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $emailFrom = SettingBL::getContactsEmail();
        
        //implementare estrazione css da file nuovo
        $cssEmail = Storage::disk('public')->get('\css\webCssEmail.css');
        

        return $this
        ->from($emailFrom)
        ->subject($this->subjectMail)
        ->view('mail.dynamicMail')
        ->with(['htmlr' => $this->htmlr, 'cssEmail' => $cssEmail]);
    }
}

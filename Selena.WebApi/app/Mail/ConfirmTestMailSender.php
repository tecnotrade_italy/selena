<?php

namespace App\Mail;

use App\Helpers\LogHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ConfirmTestMailSender extends Mailable
{
    use Queueable, SerializesModels;

     /**
     * The email from instance.
     *
     * @var emailFrom
     */
    protected $emailFrom;


    /**
     * The name instance.
     *
     * @var html
     */
    protected $html;

    /**
     * The negotiation instance.
     *
     * @var title
     */
    protected $title;

    /**
     * The negotiation instance.
     *
     * @var sender
     */
    protected $sender;

        /**
     * The negotiation instance.
     *
     * @var sender_email
     */
    protected $sender_email;

      /**
     * The negotiation instance.
     *
     * @var object
     */
    protected $object;

      /**
     * The negotiation instance.
     *
     * @var preview_object
     */
    protected $preview_object;

    /**
     * The negotiation instance.
     *
     * @var cssEmail
     */
    protected $cssEmail;
 

    /**
     * Create a new message instance.
     * 
     * @param string name
     *
     * @return void
     */
    public function __construct(string $emailFrom, string $htmls, string $title, string $sender, string $sender_email, string $object, string $preview_object, string $cssEmail)
    {
        $this->emailFrom = $emailFrom;
        $this->htmls = $htmls;   
        $this->title = $title;
        $this->sender = $sender;
        $this->sender_email = $sender_email;
        $this->object = $object;
        $this->preview_object = $preview_object;
        $this->cssEmail = $cssEmail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {  
        return $this
        ->from($this->emailFrom, $this->sender)
        ->subject($this->object)
        ->view('mail.ConfirmTestMailSender')
        ->with(['sender' => $this->sender,'sender_email' => $this->sender_email, 'title' => $this->title, 'object' => $this->object, 'preview_object' => $this->preview_object, 'htmls' => '<p style="display:none;">' . $this->preview_object .   '</p>' . $this->htmls, 'cssEmail' => $this->cssEmail]);      
    }
}

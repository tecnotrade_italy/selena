<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ContactRequestMailWorkForUs extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The name instance.
     *
     * @var Name
     */
    protected $name;

    /**
     * The surname instance.
     *
     * @var Surname
     */
    protected $surname;

    /**
     * The email instance.
     *
     * @var Email
     */
    protected $email;

    /**
     * The phoneNumber instance.
     *
     * @var PhoneNumber
     */
    protected $phoneNumber;

    /**
     * The gender instance.
     *
     * @var gender
     */
    protected $gender;

    /**
     * The date instance.
     *
     * @var date
     */
    protected $date;
    
    /**
     * The position instance.
     *
     * @var position
     */
    protected $position;

    /**
     * The nation instance.
     *
     * @var nation
     */
    protected $nation;

    /**
     * The city instance.
     *
     * @var city
     */
    protected $city;

    /**
     * The info instance.
     *
     * @var info
     */
    protected $info;
    
    /**
     * The actualCompany instance.
     *
     * @var actualCompany
     */
    protected $actualCompany;

    /**
     * The file instance.
     *
     * @var file
     */
    protected $file;
    
    /**
     * The as instance.
     *
     * @var as
     */
    protected $as;

    /**
     * The mime instance.
     *
     * @var mime
     */
    protected $mime;

    /**
     * Create a new message instance.
     * 
     * @param string name 
     * @param string surname 
     * @param string email 
     * @param string phoneNumber
     * @param string gender
     * @param string date
     * @param string position
     * @param string nation
     * @param string city
     * @param string info
     * @param string actualCompany
     * @param string file
     * @param string as
     * @param string mime
     * 
     * @param string cityofborn
     * @param string address
     * @param string cap
     * @param string province
     *
     * @return void
     */
    public function __construct(string $name, string $surname, string $email, string $phoneNumber, string $gender, string $date, string $position, string $nation, string $city, string $info, string $actualCompany, string $file, string $as, string $mime, string $cityofborn, string $address, string $cap, 
    string $province,
    string $countryofresidence,
				string $regionofdomicily,
				string $cityofresidence,
				string $fiscalcode,
				string $website,
				string $license,
				string $meansoftransport,
				string $countryofbirth
    
    )
    {
        	    	
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
        $this->phoneNumber = $phoneNumber;
        $this->gender = $gender;
        $this->date = $date;
        $this->position = $position;
        $this->nation = $nation;
        $this->city = $city;
        $this->info = $info;
        $this->actualCompany = $actualCompany;
        $this->file = $file;
        $this->as = $as;
        $this->mime = $mime;
        $this->cityofborn = $cityofborn;
        $this->address = $address;
        $this->cap = $cap;
        $this->province = $province;
        $this->countryofresidence = $countryofresidence;
        $this->regionofdomicily = $regionofdomicily;
        $this->cityofresidence = $cityofresidence;
        $this->fiscalcode = $fiscalcode;
        $this->website = $website;
        $this->license = $license;
        $this->meansoftransport = $meansoftransport;
        $this->countryofbirth = $countryofbirth;		
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        	

        //implementare estrazione css da file nuovo
        $cssEmail = Storage::disk('public')->get('\css\webCssEmail.css');
       
        return $this
            ->subject('Curriculum da ' . $this->name . ' ' . $this->surname)
            ->view('mail.contactRequestWorkForUs')
            ->with(['name' => $this->name, 'surname' => $this->surname, 'email' => $this->email, 'phoneNumber' => $this->phoneNumber, 'gender' => $this->gender, 'date' => $this->date, 'position' => $this->position, 'nation' => $this->nation, 'city' => $this->city, 'info' => $this->info, 'actualCompany' => $this->actualCompany, 'cityofborn' => $this->cityofborn, 'address' => $this->address, 'cap' => $this->cap, 'province' => $this->province,'cssEmail' => $cssEmail,
            'countryofresidence' => $this->countryofresidence,
            'regionofdomicily' => $this->regionofdomicily,
            'cityofresidence' => $this->cityofresidence,
            'fiscalcode' => $this->fiscalcode,
            'website' => $this->website,
            'license' => $this->license,
            'meansoftransport' => $this->meansoftransport,
            'countryofbirth' => $this->countryofbirth
            ])
            ->attach($this->file,
            [
                'as' => $this->as,
                'mime' => $this->mime,
            ]);
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class UserRescuePasswordMail extends Mailable
{
    use Queueable, SerializesModels;

     /**
     * The email from instance.
     *
     * @var emailFrom
     */
    protected $emailFrom;

    /**
     * The id instance.
     *
     * @var id
     */
    protected $id;

    /**
     * The name instance.
     *
     * @var name
     */
    protected $name;

    /**
     * The url instance.
     *
     * @var url
     */
    protected $url;
    

    /**
     * Create a new message instance.
     * 
     * @param string name
     *
     * @return void
     */
    public function __construct(string $emailFrom, string $name, string $url, string $id)
    {
        $this->emailFrom = $emailFrom;
        $this->name = $name;
        $this->url = $url;
        $this->id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         //implementare estrazione css da file nuovo
        $cssEmail = Storage::disk('public')->get('\css\webCssEmail.css');
        
        return $this
            ->from($this->emailFrom)
            ->subject('Recupero password')
            ->view('mail.userRescuePasswordMail')
            ->with(['name'=> $this->name, 'url' => $this->url, 'id' => $this->id, 'cssEmail' => $cssEmail]);
    }
}

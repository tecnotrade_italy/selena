<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class LogMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The level instance.
     *
     * @var Level
     */
    protected $level;

    /**
     * The log instance.
     *
     * @var Log
     */
    protected $log;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(string $level, string $log)
    {
        $this->level = $level;
        $this->log = $log;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //implementare estrazione css da file nuovo
        $cssEmail = Storage::disk('public')->get('\css\webCssEmail.css');
        
        return $this
            ->subject(str_replace('{0}', $this->level, config('app.log_mail_subject')))
            ->text('mail.log')
            ->with(['level' => $this->level, 'log' => $this->log, 'cssEmail' => $cssEmail]);
    }
}

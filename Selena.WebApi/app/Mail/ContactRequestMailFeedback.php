<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ContactRequestMailFeedback extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The name instance.
     *
     * @var Name
     */
    protected $name;

    /**
     * The surname instance.
     *
     * @var Surname
     */
    protected $surname;

    /**
     * The email instance.
     *
     * @var Email
     */
    protected $email;

    /**
     * The description instance.
     *
     * @var Description
     */
    protected $description;

    /**
     * The telefono instance.
     *
     * @var telefono
     */
    protected $telefono;

    /**
     * Create a new message instance.
     * 
     * @param string name 
     * @param string surname 
     * @param string email 
     * @param string description
     * @param string phoneNumber
     *
     * @return void
     */
    public function __construct(string $name, string $surname, string $email, string $description, string $telefono)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
        $this->description = $description;
        $this->telefono = $telefono;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //implementare estrazione css da file nuovo
        $cssEmail = Storage::disk('public')->get('\css\webCssEmail.css');

        return $this
            ->subject('Richiesta di contatto dal sito')
            ->view('mail.contactRequestFeedback')
            ->with(['name' => $this->name, 'surname' => $this->surname, 'email' => $this->email, 'description' => $this->description, 'telefono' => $this->telefono, 'cssEmail' => $cssEmail]);
    }
}

<?php

namespace App\Mail;

use App\Helpers\LogHelper;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ConfirmRegistrationMail extends Mailable
{
    use Queueable, SerializesModels;

     /**
     * The email from instance.
     *
     * @var emailFrom
     */
    protected $emailFrom;


    /**
     * The name instance.
     *
     * @var Name
     */
    protected $name;

    /**
     * The negotiation instance.
     *
     * @var email
     */
    protected $email;
    /**
     * The negotiation instance.
     *
     * @var id
     */
    protected $id;

    /**
     * Create a new message instance.
     * 
     * @param string name
     *
     * @return void
     */
    public function __construct(string $emailFrom, string $name, string $url, string $email, int $id, string $htmlcontent)
    {
        $this->emailFrom = $emailFrom;
        $this->name = $name;   
        $this->url = $url;
        $this->email = $email;
        $this->id = $id;
        $this->htmlcontent = $htmlcontent;
     
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //implementare estrazione css da file nuovo
        $cssEmail = Storage::disk('public')->get('\css\webCssEmail.css');

        return $this
            ->from($this->emailFrom)
            ->subject('Conferma Mail di Registrazione')
            ->view('mail.ConfirmRegistrationMail')
            ->with(['name' => $this->name,'url' => $this->url, 'email' => $this->email, 'id' => $this->id, 'htmlcontent' => $this->htmlcontent,'cssEmail' => $cssEmail]);
    }
}

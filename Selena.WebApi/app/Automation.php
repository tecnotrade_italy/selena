<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\HttpHelper;

class Automation extends Model
{
    use ObservantTrait;
    /**
     * Table
     *
     * @var table
     */
    protected $table = 'aut_HeadAutomations';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];

    #region DATE FORMAT FIELD

    /**
     * Get from with format
     */
    public function getFromAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set from from client format date to server format
     */
    public function setFromAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['from'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }else{
            $this->attributes['from'] = null;
        }
    }

    /**
     * Get to with format
     */
    public function getToAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set to from client format date to server format
     */
    public function setToAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['to'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }else{
            $this->attributes['to'] = null;
        }
    }
    #endregion DATE FORMAT FIELD

}

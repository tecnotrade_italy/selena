<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersDatas extends Model
{
    use ObservantTrait;

      /**
     * Table
     *
     * @var table
     */
    protected $table = 'users_datas';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}
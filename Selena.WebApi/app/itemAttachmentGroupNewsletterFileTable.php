<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class itemAttachmentGroupNewsletterFileTable extends Model
{
    use ObservantTrait;
    /**
     * Table
     *
     * @var table
     */
    protected $table = 'item_attachment_language_file_group_newsletters_table';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}

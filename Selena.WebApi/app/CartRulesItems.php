<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartRulesItems extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'carts_rules_items';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}
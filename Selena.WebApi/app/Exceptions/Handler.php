<?php

namespace App\Exceptions;

use App\Helpers\LogHelper;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $code = 0;

        if (method_exists($exception, 'getStatusCode') && !empty($exception->getStatusCode())) {
            $code = $exception->getStatusCode();
        } else {
            if (!empty($exception->getCode())) {
                $code = $exception->getCode();
            }
        }

        if (empty($code) || $code < 100 || $code > 600) {
            $code = 500;
        }

        $message = (!empty($exception->getMessage())) ? $exception->getMessage() : '';

        if ($message == 'Unauthenticated.') {
            $code = 401;
        }

        if (empty($message) && $code >= 100 && $code <= 600) {
            $message = trans('errors.' . $code);
        }

        if (empty($message)) {
            $message = trans('errors.default');
        }

        $stacktrace = '';
        if (method_exists($exception, 'getTraceAsString') && !empty($exception->getTraceAsString())) {
            $stacktrace = "\n" . '[stacktrace]' . "\n" . $exception->getTraceAsString();
        }

        $file = '';
        if (method_exists($exception, 'getFile') && !empty($exception->getFile())) {
            $file = ' at ' . $exception->getFile();
            if (method_exists($exception, 'getLine') && !empty($exception->getLine())) {
                $file .= ':' . $exception->getLine();
            }
        }

        $log = '[ERROR #' . $code . '] ' . $message;
        $log .= "\n[info]\n";

        if (!empty(auth()) && !empty(auth()->user())) {
            $log .= 'User ID: ' . auth()->user()->id . "\n";
        }

        $log .= 'File: ' . $file . "\n";
        if (!empty($request->fullUrl())) {
            $log .= 'Method: ' . $request->method() . "\nUrl: " . $request->fullUrl() . "\n";
        }

        if (!empty($request->all())) {
            $log .= 'Parameters: ' . json_encode($request->all()) . "\n";
        }

        $log .= "\n";
        if ($code == 401) {
            LogHelper::warning($log . $stacktrace);
        } elseif ($code < 600) {
            if(!strpos($request->fullUrl(),'/storage/images/') !== false){
                LogHelper::error($log . $stacktrace);
            }
        } else {
            LogHelper::critical($log . $stacktrace);
        }

        if ($request->is('api/*')) {
            // if (!\App::environment('production')) {
            //     return parent::render($request, $exception);
            // } else {
            return response()->error($message, $code);
            // }
        } elseif ($this->isHttpException($exception)) {

            $description = trans('errors.' . $code);
            if (empty($description)) {
                $description = trans('errors.default');
            }

            return response()->make(view('errors.all', compact('code', 'message', 'description')), $code);
        } else {
            return parent::render($request, $exception);
        }
    }

    /**
     * Create a response object from the given validation exception.
     *
     * @param  \Illuminate\Validation\ValidationException  $e
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        $response = parent::convertValidationExceptionToResponse($e, $request);
        if ($response instanceof JsonResponse) {
            $original = $response->getOriginalContent();
            if (!empty($original['message']) || $original['message'] == 'The given data was invalid.') {
                $original['message'] = trans('validation.default');
            }
            if (!empty($original['errors'])) {
                $original['status'] = 'error';
            }
            $response->setContent(json_encode($original));
        }
        return $response;
    }
}

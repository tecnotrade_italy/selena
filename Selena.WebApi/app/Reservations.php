<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\LogHelper;
use App\Helpers\HttpHelper;

class Reservations extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'reservations';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];


     /**
     * Get fixed_end with format
     */
 /*   public function getDateAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set fixed_end from client format date to server format
     */
  /*    public function setDateAttribute($value)
  {
        if (!is_null($value)) {
            $this->attributes['date'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('d-m-Y');
        }else{
            $this->attributes['date'] = null;
        }
    }*/

}

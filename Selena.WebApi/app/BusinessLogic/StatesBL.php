<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoStates;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\LogHelper;
use App\LanguageStates;
use App\GrStates;
use App\Language;
use App\User;
use Illuminate\Support\Facades\DB;
use Exception;
use GMP;
use Illuminate\Http\Request;

class StatesBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request DtoUnitMeasure
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoStates, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoStates->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (is_null($DtoStates->idLanguage)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (GrStates::find($DtoStates->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($DtoStates->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CompanyNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    /**
     * Convert to dto
     * 
     * @param GrStates postalCode
     * 
     * @return DtoStates
     */
    private static function _convertToDto($States)
    {
        $DtoStates = new DtoStates();

        if (isset($States->id)) {
            $DtoStates->id = $States->id;
        }

        if (isset($States->description)) {
            $DtoStates->description = $States->description;
        }

        if (isset($States->language_id)) {
            $DtoStates->language_id = $States->language_id;
        }

        return  $DtoStates;
    }


    /**
     * Convert to VatType
     * 
     * @param DtoStates dtoVatType
     * 
     * @return GrStates
     */
    private static function _convertToModel($DtoStates)
    {
        $States = new GrStates();

        if (isset($DtoStates->id)) {
            $States->id = $DtoStates->id;
        }

        if (isset($DtoStates->description)) {
            $States->description = $DtoStates->description;
        }

        if (isset($DtoStates->language_id)) {
            $States->language_id = $DtoStates->language_id;
        }

        return $States;
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoStates
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(GrStates::find($id));
    }

    /**
     * Get select
     *
     * @param String $search
     * @return States
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return GrStates::select('id', 'description as description')->get();
        } else {
            return GrStates::where('description', 'ilike', $search . '%')->select('id', 'description as description')->get();
        }
    }
    
    /**
     * Get all
     * 
     * @return DtoStates
     */
    public static function getAll($idLanguage)
    {
        $result = collect(); 

        foreach (GrStates::where('language_id',$idLanguage)->get() as $statesAll) {

            $DtoStates = new DtoStates();

            $DtoStates->id = $statesAll->id;
            $DtoStates->description = $statesAll->description;
            $DtoStates->description = $statesAll->description;

            $result->push($DtoStates); 
        }
        return $result;
    }
    

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoStates
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoStates, int $idUser)
    {
        
        static::_validateData($DtoStates, $DtoStates->idLanguage, DbOperationsTypesEnum::INSERT);
        DB::beginTransaction();

        try {
            
            $States = new GrStates();
            
            $States->language_id =$DtoStates->idLanguage;
            $States->description = $DtoStates->description;
            //$States->created_id = $DtoStates->$idUser; 
           
            $States->save();

        
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $States->id;
    }

    #endregion INSERT


#region INSERTNOAUTH

    /**
     * Insert
     * 
     * @param Request DtoStates
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertNoAuth(Request $DtoStates, int $idUser)
    {
        
        static::_validateData($DtoStates, $DtoStates->idLanguage, DbOperationsTypesEnum::INSERT);
        DB::beginTransaction();

        try {
            
            $States = new GrStates();
            
            $States->language_id =$DtoStates->idLanguage;
            $States->description = $DtoStates->description;
            $States->created_id = $DtoStates->$idUser; 
           
            $States->save();

        
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $States->id;
    }

    #endregion INSERTNOAUTH




    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoUnitMeasure
     * @param int idLanguage
     */
   

    public static function update(Request $DtoStates)

    {
        static::_validateData($DtoStates, $DtoStates->idLanguage, DbOperationsTypesEnum::UPDATE);
        $statesOnDb = GrStates::find($DtoStates->id);

       // $LanguageOndb = Language::where('id',  $statesOnDb->id)->where('id', $DtoStates->idLanguage)->first();

        DB::beginTransaction();

        try {

            $statesOnDb->id = $DtoStates->id;
            $statesOnDb->language_id = $DtoStates->idLanguage;
            $statesOnDb->description = $DtoStates->description;
            $statesOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
    
  #endregion UPDATE


 /**
     * UpdateNoAuth
     * 
     * @param Request DtoStates
     * @param int idLanguage
     */
   

    public static function UpdateNoAuth(Request $DtoStates)
    {
        $statesOnDb = GrStates::find($DtoStates->id);

        DB::beginTransaction();

        try {

            $statesOnDb->id = $DtoStates->id;
            $statesOnDb->language_id = $DtoStates->idLanguage;
            $statesOnDb->description = $DtoStates->description;
            $statesOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
    
  #endregion UpdateNoAuth




    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */

    public static function delete(int $id, int $idLanguage)
    {
        $States = GrStates::find($id);

        if (is_null($States)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($States)) {
            
            DB::beginTransaction();

            try {

                GrStates::where('id', $id)->delete();
               
                
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }  
        } 

    }

    #endregion DELETE
}



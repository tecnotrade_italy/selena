<?php

namespace App\BusinessLogic;

use App\Helpers\LogHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\DtoModel\DtoRobots;
use Symfony\Component\Process\Process;

class RobotsSettingBL
{
    #region GET
    /**
     * Get
     * 
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function get(int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
     
        $DtoRobots = new DtoRobots();
        $DtoRobots->txtRobots = Storage::disk('public')->get('\robots\robots.txt');

        return $DtoRobots;
    }

    #endregion GET

    #region POST

      /**
     * Update
     * 
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function updateRobots(Request $request, int $idUser, int $idLanguage)
    {

        Storage::disk('public')->put('\robots\robots.txt', $request->css);
        return;
  
    }

    #endregion POST
}

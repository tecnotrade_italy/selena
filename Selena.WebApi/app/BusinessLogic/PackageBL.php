<?php

namespace App\BusinessLogic;

use App\DocumentRow;
use App\Enums\DictionariesCodesEnum;
use App\Enums\DocumentTypeEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\Helpers\LogHelper;
use App\Package;
use App\PackageSixten;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class PackageBL
{
    #region PRIVATE

    /**
     * Check function enabled
     * 
     * @param int idFunctionlity
     * @param int idUser
     * @param int idLanguage
     */
    private static function _checkFunctionEnabled(int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (is_null($idFunctionality)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Functionality::find($idFunctionality))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get select by idErp
     * 
     * @param string idErp
     * @param string search
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function getSelectByErp(string $idErp, string $search, int $idFunctionality, int $idUser, int $idLanguage)
    {
        return collect(DB::select(
            'SELECT packages.id, packages.description
                    FROM packages
                    INNER JOIN packages_sixten ON packages.id = packages_sixten.package_id
                    WHERE packages_sixten.document_row_id IN (
                        SELECT documents_rows.id
                        FROM documents_rows
                        WHERE documents_rows.document_head_id IN (
                            SELECT id
                            FROM documents_heads
                            WHERE (customer_id, EXTRACT(YEAR FROM expected_shipping_date), EXTRACT(MONTH FROM expected_shipping_date)) in (
                                SELECT documents_heads.customer_id, EXTRACT(YEAR FROM expected_shipping_date) as year,
                                    EXTRACT(MONTH FROM expected_shipping_date) as month
                                FROM documents_heads
                                INNER JOIN documents_rows ON documents_heads.id = documents_rows.document_head_id
                                WHERE documents_rows.erp_id = :idErp
                            )
                            AND document_type_id = (SELECT id FROM documents_types where code = :codeDocumentType)
                        )
                    )
                    AND packages.description LIKE :search',
            ['idErp' => $idErp, 'codeDocumentType' => DocumentTypeEnum::Order, 'search' => $search . '%']
        ));
    }

    /**
     * Get max by erp
     * 
     * @param string idErp
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return Package
     */
    public static function getMaxByErp(string $idErp, int $idFunctionality, int $idUser, int $idLanguage)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $max = DB::select('SELECT MAX(packages.id)
                        FROM packages
                        INNER JOIN packages_sixten ON packages.id = packages_sixten.package_id
                        WHERE packages_sixten.document_row_id IN (
                            SELECT documents_rows.id
                            FROM documents_rows
                            WHERE documents_rows.document_head_id IN (
                                SELECT id
                                FROM documents_heads
                                WHERE (customer_id, EXTRACT(YEAR FROM expected_shipping_date), EXTRACT(MONTH FROM expected_shipping_date)) in (
                                    SELECT documents_heads.customer_id, EXTRACT(YEAR FROM expected_shipping_date) as year,
                                        EXTRACT(MONTH FROM expected_shipping_date) as month
                                    FROM documents_heads
                                    INNER JOIN documents_rows ON documents_heads.id = documents_rows.document_head_id
                                    WHERE documents_rows.erp_id = ?
                            )
                        )
                    )', [$idErp]);

        if (!is_null($max[0]->max)) {
            return Package::find($max[0]->max);
        } else {
            return null;
        }
    }

    #endregion GET

    #region INSERT

    /**
     * Create by idErp
     * 
     * @param string idErp
     * @param string description
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return Package
     */
    public static function createByErp(string $idErp, string $description = null, int $idFunctionality, int $idUser, int $idLanguage)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        DB::beginTransaction();

        try {
            if (is_null($description)) {
                $oldPackage = static::getMaxByErp($idErp, $idFunctionality, $idUser, $idLanguage);
                if (is_null($oldPackage)) {
                    $description = 1;
                } else {
                    $description = $oldPackage->description + 1;
                }
            }

            $package = new Package();
            $package->description = $description;
            $package->created_id = $idUser;
            $package->save();

            $packageSixten = new PackageSixten();
            $packageSixten->document_row_id = DocumentRow::where('erp_id', $idErp)->first()->id;
            $packageSixten->package_id = $package->id;
            $packageSixten->save();

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $package;
    }

    #endregion INSERT
}

<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoChartJS;
use App\DtoModel\DtoChartJSDatasets;
use App\DtoModel\DtoSelect2;
use Mockery\CountValidator\Exception;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Enums\InputTypesEnum;
use App\Functionality;
use App\Helpers\LogHelper;
use App\ItemTechnicalSpecification;
use App\Item;
use App\ItemTechnicalSpecificationRevision;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;

class ItemTechnicalSpecificationBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param int idItem
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    private static function _validateData(int $idItem, int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (is_null($idFunctionality)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Functionality::find($idFunctionality))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotExist), HttpResultsCodesEnum::InvalidPayload);
            } else {
                if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
                }
            }
        }

        if (is_null(Item::find($idItem))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ItemNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    #endregion PRIVATE

    #region GET    

    /**
     * Get by item and language
     * 
     * @param int idItem
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return collect
     */
    public static function get(int $idItem, int $idFunctionality, $idUser, int $idLanguage, int $version = null)
    {
        static::_validateData($idItem, $idFunctionality, $idUser, $idLanguage);

        $configurations = GroupDisplayTechnicalSpecificationBL::getConfiguration($idItem, $idFunctionality, $idUser, $idLanguage);

        if (is_null($configurations)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($version)) {
            $version = ItemTechnicalSpecification::where('item_id', $idItem)->where('language_id', $idLanguage)->max('version');
        }

        $dtoItemTechnicalSpecification = collect();

        foreach ($configurations->tabsConfigurations as $tabsConfigurations) {
            foreach ($tabsConfigurations->pageConfigurations as $pageConfiguration) {
                $itemTechnicalSpecification = ItemTechnicalSpecification::where('item_id', $idItem)->where('technical_specification_id', $pageConfiguration->field)->where('language_id', $idLanguage)->where('version', $version)->first();
                if (!is_null($itemTechnicalSpecification)) {
                    $dtoItemTechnicalSpecification[$pageConfiguration->field] = $itemTechnicalSpecification->value;
                }
            }
        }

        return $dtoItemTechnicalSpecification;
    }

    /**
     * Get version by item and language
     * 
     * @param int idItem
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return collect(DtoSelect2)
     */
    public static function getVersion(int $idItem, int $idFunctionality, $idUser, int $idLanguage)
    {
        static::_validateData($idItem, $idFunctionality, $idUser, $idLanguage);

        $result = collect();

        $lastVersion = ItemTechnicalSpecification::where('item_id', $idItem)->where('language_id', $idLanguage)->max('version');

        foreach (ItemTechnicalSpecification::where('item_id', $idItem)->where('language_id', $idLanguage)->orderBy('version')->groupBy('version')->select('version')->get() as $itemTechnicalSpecification) {
            if ($itemTechnicalSpecification->version != $lastVersion) {
                $dtoSelect2 = new DtoSelect2();
                $dtoSelect2->id = $itemTechnicalSpecification->version;
                $dtoSelect2->description = $itemTechnicalSpecification->version;
                $result->push($dtoSelect2);
            }
        }

        $dtoSelect2 = new DtoSelect2();
        $dtoSelect2->id = DictionariesCodesEnum::Last;
        $dtoSelect2->description = DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::Last);
        $result->push($dtoSelect2);

        return $result;
    }

    /**
     * Get completed stat by language
     * 
     * @param int idLanguage
     * 
     * @return DtoChartJS
     */
    public static function getCompletedStatByLanguage(int $idLanguage)
    {
        $result = new DtoChartJS();
        $result->labels = ["100", "75", "50", "25", "0"];
        $datasets = new DtoChartJSDatasets();
        $datasets->backgroundColor = ["rgba(75, 192, 192, 0.5)", "rgba(255, 205, 86, 0.5)", "rgba(201, 203, 207, 0.5)", "rgba(255, 159, 64, 0.5)", "rgba(255, 99, 132, 0.5)"];

        foreach (DB::select('SELECT SUM("100") AS cento, SUM("75") AS settantacinque, SUM("50") AS cinquanta, SUM("25") AS venticinque, SUM ("0") AS zero
                            FROM (
                                SELECT
                                    SUM(CASE WHEN calcolo = 100 OR (calcolo IS NULL AND g > s) THEN 1 END ) AS "100",
                                    SUM(CASE WHEN calcolo < 100 AND calcolo >= 75 THEN 1 END ) AS "75",
                                    SUM(CASE WHEN calcolo < 75 AND calcolo >= 50 THEN 1 END ) AS "50",
                                    SUM(CASE WHEN calcolo < 50 AND calcolo >= 25 THEN 1 END ) AS "25",
                                    SUM(CASE WHEN (calcolo < 25 AND calcolo >= 0) OR (calcolo IS NULL AND g = s) THEN 1 END ) AS "0"
                                FROM (
                                    SELECT
                                        SUM(CASE WHEN value != \'\' THEN 1 END) * 100 / COUNT(*) AS calcolo, 
                                        COUNT(items_groups_technicals_specifications.group_technical_specification_id) AS g,
                                        COUNT(groups_technicals_specifications_technicals_specifications.group_technical_specification_id) AS s
                                    FROM items
                                    LEFT JOIN items_technicals_specifications ON items.id = items_technicals_specifications.item_id
                                    LEFT JOIN items_groups_technicals_specifications ON items.id = items_groups_technicals_specifications.item_id
                                    LEFT JOIN groups_technicals_specifications_technicals_specifications ON items_groups_technicals_specifications.group_technical_specification_id = groups_technicals_specifications_technicals_specifications.group_technical_specification_id
                                    WHERE (items_technicals_specifications.version, items_technicals_specifications.item_id)
                                            IN (SELECT max("version"), item_id
                                                FROM items_technicals_specifications
                                                WHERE language_id = :idLanguage
                                                GROUP BY item_id)
                                    OR items_technicals_specifications.item_id IS NULL
                                    GROUP BY items_technicals_specifications.version, items_technicals_specifications.item_id, items.id
                                ) AS calc
                                GROUP BY calcolo
                            ) AS calcc', ['idLanguage' => $idLanguage]) as $stat) {
            $datasets->data = [$stat->cento, $stat->settantacinque, $stat->cinquanta, $stat->venticinque, $stat->zero];
        }

        $result->datasets->push($datasets);
        return $result;
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request $request
     * @param int $idUser
     * @param int $idLanguage
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request->idItem, $request->idFunctionality, $idUser, $request->idLanguage);

        $configurations = GroupDisplayTechnicalSpecificationBL::getConfiguration($request->idItem, $request->idFunctionality, $idUser, $request->idLanguage);

        if (is_null($configurations)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        DB::beginTransaction();

        try {
            $newVersion = ItemTechnicalSpecification::where('item_id', $request->idItem)->where('language_id', $request->idLanguage)->max('version') + 1;

            foreach ($configurations->tabsConfigurations as $tabsConfigurations) {
                foreach ($tabsConfigurations->pageConfigurations as $pageConfiguration) {
                    $itemTechnicalSpecification = new ItemTechnicalSpecification();
                    $itemTechnicalSpecification->item_id = $request->idItem;
                    $itemTechnicalSpecification->technical_specification_id = $pageConfiguration->field;
                    $itemTechnicalSpecification->language_id = $request->idLanguage;
                    if (!is_null($request[$pageConfiguration->field])) {
                        $itemTechnicalSpecification->value = $request[$pageConfiguration->field];
                    } else {
                        $itemTechnicalSpecification->value = '';
                    }
                    $itemTechnicalSpecification->version = $newVersion;
                    $itemTechnicalSpecification->save();

                    $itemTechnicalSpecificationRevision = new ItemTechnicalSpecificationRevision();
                    $itemTechnicalSpecificationRevision->item_technical_specification_id = $itemTechnicalSpecification->id;
                    $itemTechnicalSpecificationRevision->group_display_description = $tabsConfigurations->label;
                    $itemTechnicalSpecificationRevision->field_description = $pageConfiguration->label;
                    $itemTechnicalSpecificationRevision->group_display_order = $tabsConfigurations->order;
                    $itemTechnicalSpecificationRevision->field_posx = $pageConfiguration->posX;
                    $itemTechnicalSpecificationRevision->field_posy = $pageConfiguration->posY;
                    $itemTechnicalSpecificationRevision->field_input_type = InputTypesEnum::getValue($pageConfiguration->inputType);
                    $itemTechnicalSpecificationRevision->save();
                }
            }

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion INSERT
}

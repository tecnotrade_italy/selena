<?php

namespace App\BusinessLogic;

use App\Customer;
use App\DtoModel\DtoInterventions;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\LogHelper;
use App\GrIntervention;
use App\Helpers\ArrayHelper;
use App\Helpers\HttpHelper;
use Intervention\Image\Facades\Image;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Http\Request;

class FaultModulesBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request FaultModulesBL
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoInterventions, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoInterventions->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (GrIntervention::find($DtoInterventions->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($DtoInterventions->referent)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoInterventions->nr_ddt)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CodeNotFound), HttpResultsCodesEnum::InvalidPayload);
        }


        if (is_null($DtoInterventions->plant_type)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoInterventions->trolley_type)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CharacteristicNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoInterventions->series)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::LinkNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoInterventions->defect)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::MetaTagKeywordNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoInterventions->voltage)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::MetaTagTTitleNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoInterventions->exit_notes)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::OrderNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

    }


    private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/Categories/';
    }

    /**
     * Convert to dto
     * 
     * @param Intervention postalCode
     * 
     * @return DtoInterventions
     */
    private static function _convertToDto($Intervention)
    {
        $DtoInterventions = new DtoInterventions();


        if (isset($Intervention->id)) {
            $DtoInterventions->id = $Intervention->id;
        }

        if (isset($Intervention->id_customer)) {
            $DtoInterventions->id_customer = $Intervention->id_customer;
        }
        if (isset($Intervention->referent)) {
            $DtoInterventions->referent = $Intervention->referent;
        }

        if (isset($Intervention->nr_ddt)) {
            $DtoInterventions->nr_ddt = $Intervention->nr_ddt;
            
        }

        if (isset($Intervention->plant_type)) {
            $DtoInterventions->plant_type = $Intervention->plant_type;
        }
       
        if (isset($Intervention->trolley_type)) {
            $DtoInterventions->trolley_type = $Intervention->trolley_type;
        }

        if (isset($Intervention->series)) {
            $DtoInterventions->series = $Intervention->series;
        }

        if (isset($Intervention->defect)) {
            $DtoInterventions->defect = $Intervention->defect;
        }


        if (isset($Intervention->voltage)) {
            $DtoInterventions->voltage = $Intervention->voltage;

        }

        if (isset($Intervention->exit_notes)) {
            $DtoInterventions->exit_notes = $Intervention->exit_notes;

        }


        return  $DtoInterventions;
    }


    /**
     * Convert to VatType
     * 
     * @param DtoInterventions dtoVatType
     * 
     * @return Intervention
     */
    private static function _convertToModel($DtoInterventions)
    {
        $Intervention= new GrIntervention();

        if (isset($DtoInterventions->id)) {
            $Intervention->id = $DtoInterventions->id;
        }
        if (isset($DtoInterventions->id_customer)) {
            $Intervention->id_customer = $DtoInterventions->id_customer;
        }
        if (isset($DtoInterventions->referent)) {
            $Intervention->referent = $DtoInterventions->referent;
        }
        
        if (isset($DtoInterventions->nr_ddt)) {
            $Intervention->nr_ddt = $DtoInterventions->nr_ddt;
        }
        if (isset($DtoInterventions->plant_type)) {
            $Intervention->plant_type = $DtoInterventions->plant_type;
        }

        if (isset($DtoInterventions->trolley_type)) {
            $Intervention->trolley_type = $DtoInterventions->trolley_type;
        }
       
        if (isset($DtoInterventions->series)) {
            $Intervention->series = $DtoInterventions->series;
        }
        if (isset($DtoInterventions->defect)) {
            $Intervention->defect = $DtoInterventions->defect;
        }

        if (isset($DtoInterventions->voltage)) {
            $Intervention->voltage = $DtoInterventions->voltage;
        }

        if (isset($DtoInterventions->exit_notes)) {
            $Intervention->exit_notes = $DtoInterventions->exit_notes;
        }

        return $Intervention;
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoInterventions
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(GrIntervention::find($id));
    }

     /**
     * Get select
     *
     * @param String $search
     * @return Intervention
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return GrIntervention::select('id', 'description')->get();
        } else {
            return GrIntervention::where('description', 'like', $search . '%')->select('id', 'description')->get();
        }
    }
    
    /**
     * Get all
     * 
     * @return DtoInterventions
     */
    public static function getAll($idLanguage)
    
    {     
    
        $result = collect();

        foreach (GrIntervention::where('language_id',$idLanguage)->get() as $InterventionAll) {
        

            $DtoInterventions = new DtoInterventions();
            $CustomerId = Customer::where('id', $InterventionAll->customers_id)->first()->id;
            $CustomerDescription = Customer::where('id', $InterventionAll->customers_id)->first()->business_name;
            
            //campi tabella: 
            if (isset($InterventionAll->id)) {
                $DtoInterventions->id = $InterventionAll->id;
            }

            if (isset($CustomerDescription)) {
                $DtoInterventions->customers_id =  $CustomerDescription;
            }
    
            // campi dettaglio

            if (isset($CustomerId)) {
                $DtoInterventions->customer_id = $CustomerId;
            }    

            if (isset($InterventionAll->date_ddt)) {
                $DtoInterventions->date_ddt = $InterventionAll->date_ddt;
            }

            if (isset($InterventionAll->referent)) {
                $DtoInterventions->referent = $InterventionAll->referent;
            }

            if (isset($InterventionAll->nr_ddt)) {
                $DtoInterventions->nr_ddt = $InterventionAll->nr_ddt;
            }
            if (isset($InterventionAll->plant_type)) {
                $DtoInterventions->plant_type = $InterventionAll->plant_type;
            }
            if (isset($InterventionAll->trolley_type)) {
                $DtoInterventions->trolley_type = $InterventionAll->trolley_type;
            }
            if (isset($InterventionAll->series)) {
                $DtoInterventions->series = $InterventionAll->series;
            }
            if (isset($InterventionAll->defect)) {
                $DtoInterventions->defect = $InterventionAll->defect;
            }
            if (isset($InterventionAll->voltage)) {
                $DtoInterventions->voltage = $InterventionAll->voltage;
            }

            if (isset($InterventionAll->exit_notes)) {
                $DtoInterventions->exit_notes = $InterventionAll->exit_notes;
            }

            $result->push($DtoInterventions); 
        }
        return $result;
    }


    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoInterventions
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoInterventions, int $idUser)
    {

        DB::beginTransaction();

        try {

            $Intervention = new GrIntervention();

            if (isset($DtoInterventions->customer_id)) {
                $Intervention->customers_id = $DtoInterventions->customer_id;
             }

             if (isset($DtoInterventions->date_ddt)) {
                $Intervention->date_ddt = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $DtoInterventions->date_ddt)->format('Y/m/d H:i:s');   
            } 
            
            if (isset($DtoInterventions->idLanguage)) {
                $Intervention->language_id = $DtoInterventions->idLanguage;
            }  
        
            if (isset($DtoInterventions->referent)) {
                $Intervention->referent = $DtoInterventions->referent;
            }

             if (isset($DtoInterventions->nr_ddt)) {
                $Intervention->nr_ddt = $DtoInterventions->nr_ddt;
            }
            if (isset($DtoInterventions->plant_type)) {
                $Intervention->plant_type = $DtoInterventions->plant_type;
            }
            if (isset($DtoInterventions->trolley_type)) {
                $Intervention->trolley_type = $DtoInterventions->trolley_type;
            }
            if (isset($DtoInterventions->series)) {
                $Intervention->series = $DtoInterventions->series;
            }
            if (isset($DtoInterventions->defect)) {
                $Intervention->defect = $DtoInterventions->defect;
            }
            if (isset($DtoInterventions->voltage)) {
                $Intervention->voltage = $DtoInterventions->voltage;
            }

            if (isset($DtoInterventions->exit_notes)) {
                $Intervention->exit_notes = $DtoInterventions->exit_notes;
            }

            $Intervention->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $Intervention->id;

    }

    #endregion INSERT

      /**
     * Clone
     * 
     * @param int id
     * @param int idFunctionality
     * @param int idLanguage
     * @param int idUser
     * 
     * @return int
     */
    public static function clone(int $id, int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $Intervention = GrIntervention::find($id);
        $CustomerId = Customer::where('id', $Intervention->customers_id)->first();
       
        
        if (is_null($Intervention)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        DB::beginTransaction();

        try {
           
            $newInterventions = new GrIntervention();

            $newInterventions->referent =  $Intervention->referent;
            $newInterventions->nr_ddt =  $Intervention->nr_ddt;
            $newInterventions->plant_type = $Intervention->plant_type;
            $newInterventions->trolley_type = $Intervention->trolley_type;
            $newInterventions->series = $Intervention->series;
            $newInterventions->defect = $Intervention->defect;
            $newInterventions->voltage = $Intervention->voltage;
            $newInterventions->date_ddt = $Intervention->date_ddt; 
            $newInterventions->customers_id =  $Intervention->customers_id;
            $newInterventions->exit_notes = $Intervention->exit_notes;
            $newInterventions->language_id = $Intervention->language_id;
            $newInterventions->save();

    
            DB::commit();

            return $newInterventions->id;
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoCategories
     * @param int idLanguage
     */

    public static function update(Request $DtoInterventions){

        static::_validateData($DtoInterventions, $DtoInterventions->idLanguage, DbOperationsTypesEnum::UPDATE);

        $interventionOnDb = GrIntervention::find($DtoInterventions->id);

        DB::beginTransaction();

        try {

            if (isset($DtoInterventions->date_ddt)) {
                $interventionOnDb->date_ddt = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $DtoInterventions->date_ddt)->format('Y/m/d H:i:s');   
               }
            $interventionOnDb->language_id =$DtoInterventions->idLanguage;
            $interventionOnDb->customers_id = $DtoInterventions->customer_id; 
            $interventionOnDb->referent = $DtoInterventions->referent;
            $interventionOnDb->nr_ddt = $DtoInterventions->nr_ddt;
            $interventionOnDb->plant_type = $DtoInterventions->plant_type;
            $interventionOnDb->trolley_type = $DtoInterventions->trolley_type;
            $interventionOnDb->series = $DtoInterventions->series;
            $interventionOnDb->defect = $DtoInterventions->defect;
            $interventionOnDb->voltage = $DtoInterventions->voltage;
            $interventionOnDb->exit_notes = $DtoInterventions->exit_notes;
            $interventionOnDb->save();


        DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
    
  #endregion UPDATE

     #region DELETE
    /**
     * Delete
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {   
        $Intervention = GrIntervention::find($id);


        if (is_null($Intervention)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($Intervention)) {
            
            DB::beginTransaction();

            try {
                
                //ItemLanguageBL::deleteByIdCategoriesItems($id);
                //ItemLanguageBL::deleteByItemLanguage($id);
                GrIntervention::where('id', $id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }  
        }

    }

    #endregion DELETE
}

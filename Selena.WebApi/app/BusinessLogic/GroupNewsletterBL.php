<?php

namespace App\BusinessLogic;

use App\Contact;
use App\ContactGroupNewsletter;
use App\CustomerGroupNewsletter;
use App\DtoModel\DtoContact;
use App\DtoModel\DtoGroupNewsletter;
use App\DtoModel\DtoUser;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\GroupNewsletter;
use App\Helpers\DBHelper;
use App\Helpers\LogHelper;
use App\User;
use App\UsersDatas;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GroupNewsletterBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request dtoContact
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validataData(Request $dtoGroupNewsletter, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($dtoGroupNewsletter->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (GroupNewsletter::find($dtoGroupNewsletter->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupNewsletterNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($dtoGroupNewsletter->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }





      /**
     * Get select
     *
     * @param String $search
     * @return Customer
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return GroupNewsletter::select('id', 'description as description')->get();
        } else {
            return GroupNewsletter::where('description', 'ilike', $search . '%')->select('id', 'description as description')->get();
        }
    }

    /**
     * Conver to model
     * 
     * @param Request dtoGroupNewsletter
     * 
     * @return GroupNewsletter
     */
    private static function _convertToModel(Request $dtoGroupNewsletter)
    {
        $groupNewsletter = new GroupNewsletter();

        if (isset($dtoGroupNewsletter->id)) {
            $groupNewsletter->id = $dtoGroupNewsletter->id;
        }

        if (isset($dtoGroupNewsletter->description)) {
            $groupNewsletter->description = $dtoGroupNewsletter->description;
        }

        return $groupNewsletter;
    }

    /**
     * Convert to dto
     * 
     * @param GroupNewsletter groupNewsletter
     * 
     * @return DtoGroupNewsletter
     */
    private static function _convertToDto(GroupNewsletter $groupNewsletter)
    {
        $dtoGroupNewsletter = new DtoGroupNewsletter();
        $dtoGroupNewsletter->id = $groupNewsletter->id;
        $dtoGroupNewsletter->description = $groupNewsletter->description;
        return $dtoGroupNewsletter;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get all
     * 
     * @return DtoGroupNewsletter
     */
    public static function getAll()
    {
        return GroupNewsletter::all()->map(function ($model) {
            return [
                'id' => $model->id,
                'description' => $model->description
            ];
        });
    }

/**
     * getUserSearch
     * 
     * @param int idUserBy
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function getUserSearch(request $request)
    {
        $result = collect();
        $strWhere = "";

        $NameSearch = $request->name;
        if ($NameSearch != "") {
            $strWhere = $strWhere . ' AND ud.name ilike \'%' . $NameSearch . '%\'  OR ud.surname ilike \'%' . $NameSearch . '%\'';
        }

        $BusinessNameSearch = $request->business_name;
        if ($BusinessNameSearch != "") {
            $strWhere = $strWhere . ' AND ud.business_name ilike \'%' . $BusinessNameSearch . '%\'';
        }

        $TelephoneNumberSearch = $request->telephone_number;
        if ($TelephoneNumberSearch != "") {
            $strWhere = $strWhere . ' AND ud.telephone_number ilike \'%' . $TelephoneNumberSearch . '%\'';
        }

       /* foreach (DB::select(
            'SELECT   users_datas.name,users_datas.surname,users_datas.business_name,users_datas.telephone_number,
        FROM users_datas
        INNER JOIN customers_groups_newsletters ON users_datas.user_id = customers_groups_newsletters.customer_id
        WHERE items_languages.language_id = :idLanguage ' . $strWhere, 
        ) as $resultsearch) {*/

            foreach (DB::select(
            'SELECT ud.name,ud.surname,ud.business_name,ud.telephone_number,ud.user_id
            FROM users_datas AS ud
            WHERE ud.language_id = 1 ' . $strWhere, 
            ) as $resultsearch) {
            $CustomersGroupNewsletter = CustomerGroupNewsletter::where('customer_id', $resultsearch->user_id)->get()->first();
            $dtoUser = new DtoUser();
            if (isset($CustomersGroupNewsletter)){
                $dtoUser->check = true;       
            }else{
                $dtoUser->check = false;     
            }

            $dtoUser->user_id = $resultsearch->user_id;
      
            if (isset($resultsearch->business_name)){
                $dtoUser->business_name = $resultsearch->business_name;     
            }else{
                $dtoUser->business_name = '';    
            }
            if (isset($resultsearch->name)){
                $dtoUser->name = $resultsearch->name;     
            }else{
                $dtoUser->name = '';    
            }
            if (isset($resultsearch->surname)){
                $dtoUser->surname = $resultsearch->surname;     
            }else{
                $dtoUser->surname = '';    
            }
            if (isset($resultsearch->telephone_number)){
                $dtoUser->telephone_number = $resultsearch->telephone_number;     
            }else{
                $dtoUser->telephone_number = '';    
            }
            $result->push($dtoUser); 
        }
        return $result;
    }   


    
    public static function getAllByUserGroup(request $request)
    {
        $result = collect();
        foreach (GroupNewsletter::orderBy('id')->get() as $GroupNewsletter) {
         $CustomersGroupNewsletter = CustomerGroupNewsletter::where('customer_id', $request->rowSelected)->where('group_newsletter_id', $GroupNewsletter->id)->get()->first();

         $DtoGroupNewsletter = new DtoGroupNewsletter();
            if (isset($CustomersGroupNewsletter)){
                $DtoGroupNewsletter->checkUserGroup = true;       
            }else{
                $DtoGroupNewsletter->checkUserGroup = false;     
            }
            $DtoGroupNewsletter->id = $GroupNewsletter->id;
            $DtoGroupNewsletter->description = $GroupNewsletter->description;
            $result->push($DtoGroupNewsletter); 
        }
        return $result;
    }

    public static function insertGroupUser(request $request){

             DB::beginTransaction();
                try {
                $CustomerGroupNewsletter = new CustomerGroupNewsletter();   
                $CustomerGroupNewsletter->customer_id = $request->user_id;
                $CustomerGroupNewsletter->group_newsletter_id = $request->idGroupUser;
                $CustomerGroupNewsletter->save(); 

                DB::commit();
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
    }

/**
     * Get by User
     * 
     * @param int idUserBy
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function getAllByUser(request $request)
    {
        $result= collect();
        foreach (UsersDatas::orderBy('user_id')->get() as $user) {
            $CustomersGroupNewsletter = CustomerGroupNewsletter::where('group_newsletter_id', $request->rowSelected)->where('customer_id', $user->user_id)->get()->first();
            $dtoUser = new DtoUser();
            if (isset($CustomersGroupNewsletter)){
                $dtoUser->check1 = true;       
            }else{
                $dtoUser->check1 = false;     
            }

            $dtoUser->user_id = $user->user_id;
      
            if (isset($user->business_name)){
                $dtoUser->business_name = $user->business_name;     
            }else{
                $dtoUser->business_name = '';    
            }
            if (isset($user->name)){
                $dtoUser->name = $user->name;     
            }else{
                $dtoUser->name = '';    
            }
            if (isset($user->surname)){
                $dtoUser->surname = $user->surname;     
            }else{
                $dtoUser->surname = '';    
            }
            if (isset($user->telephone_number)){
                $dtoUser->telephone_number = $user->telephone_number;     
            }else{
                $dtoUser->telephone_number = '';    
            }
            $result->push($dtoUser); 
          
        }
            return $result;
    }  



    /**
     * Get by User
     * 
     * @param int idUserBy
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function getAllByContact(request $request)
    {
        $result= collect();
        foreach (Contact::orderBy('id')->get() as $contact) {
            $ContactGroupNewsletter = ContactGroupNewsletter::where('group_newsletter_id', $request->rowSelected)->where('contact_id', $contact->id)->get()->first();

            $dtoContact = new DtoContact();
            if (isset($ContactGroupNewsletter)){
                $dtoContact->check = true;       
            }else{
                $dtoContact->check = false;     
            }

            $dtoContact->id = $contact->id;
      
            if (isset($contact->business_name)){
                $dtoContact->business_name = $contact->business_name;     
            }else{
                $dtoContact->business_name = '';    
            }
            if (isset($contact->name)){
                $dtoContact->name = $contact->name;     
            }else{
                $dtoContact->name = '';    
            }
            if (isset($contact->surname)){
                $dtoContact->surname = $contact->surname;     
            }else{
                $dtoContact->surname = '';    
            }
            if (isset($contact->phone_number)){
                $dtoContact->telephone_number = $contact->phone_number;     
            }else{
                $dtoContact->telephone_number = '';    
            }
            $result->push($dtoContact); 
        }
            return $result;
    }  


        public static function updateOnlineDetailContact(request $request){
        }        



    public static function insertUser(request $request){
        
             DB::beginTransaction();
                try {
                 $CustomerGroupNewsletter = new CustomerGroupNewsletter();
                 $CustomerGroupNewsletter->customer_id = $request->IdCustomer;
                 $CustomerGroupNewsletter->group_newsletter_id = $request->group_newsletter_id;  
                 $CustomerGroupNewsletter->save();   
                 
                    
                DB::commit();
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
    }

     /**
     * delRoleUser 
     *
     */
    public static function delUser(int $IdCustomer, int $rowIdSelected, int $idLanguage)
    {

        if (is_null($IdCustomer)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, 'Utente Non Trovato'), HttpResultsCodesEnum::InvalidPayload);
        }

        $CustomerGroupNewsletter = CustomerGroupNewsletter::where('group_newsletter_id', $rowIdSelected)->where('customer_id', $IdCustomer);
        $CustomerGroupNewsletter->delete();
    }


    public static function insertContact(request $request){    
             DB::beginTransaction();
                try {
                 $ContactGroupNewsletter = new ContactGroupNewsletter();
                 $ContactGroupNewsletter->contact_id = $request->IdContact;
                 $ContactGroupNewsletter->group_newsletter_id = $request->group_newsletter_id;  
                 $ContactGroupNewsletter->save(); 
                   
                DB::commit();
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
    }

    
    public static function delGroupUser(int $idGroupUser, int $rowIdSelected, int $idLanguage)
    {
       // 
       if (is_null($rowIdSelected)) {
        throw new Exception(DictionaryBL::getTranslate($idLanguage, 'Utente Non Trovato'), HttpResultsCodesEnum::InvalidPayload);
    }

    $CustomerGroupNewsletter = CustomerGroupNewsletter::where('customer_id', $rowIdSelected)->where('group_newsletter_id', $idGroupUser);
    $CustomerGroupNewsletter->delete();


    
    }


     /**
     * delRoleUser 
     *
     */
    public static function delContact(int $IdContact, int $rowIdSelected, int $idLanguage)
    {

        if (is_null($IdContact)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, 'Utente Non Trovato'), HttpResultsCodesEnum::InvalidPayload);
        }

        $ContactGroupNewsletter = ContactGroupNewsletter::where('group_newsletter_id', $rowIdSelected)->where('contact_id', $IdContact);
        $ContactGroupNewsletter->delete();
    }



    /**
     * Get by id
     * 
     * @param int id
     * 
     * @return DtoGroupNewsletter
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(GroupNewsletter::find($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request dtoGroupNewsletter
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $dtoGroupNewsletter, int $idLanguage)
    {
        static::_validataData($dtoGroupNewsletter, $idLanguage, DbOperationsTypesEnum::INSERT);
        $contact = static::_convertToModel($dtoGroupNewsletter);
        $contact->save();
        return $contact->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoGroupNewsletter
     * @param int idLanguage
     */
    public static function update(Request $dtoGroupNewsletter, int $idLanguage)
    {
        static::_validataData($dtoGroupNewsletter, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $contactOnDb = GroupNewsletter::find($dtoGroupNewsletter->id);
        DBHelper::updateAndSave(static::_convertToModel($dtoGroupNewsletter), $contactOnDb);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $contact = GroupNewsletter::find($id);

        if (is_null($contact)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupNewsletterNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $contact->delete();
    }

    #endregion DELETE
}

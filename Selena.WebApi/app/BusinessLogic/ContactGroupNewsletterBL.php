<?php

namespace App\BusinessLogic;

use App\ContactGroupNewsletter;
use App\DtoModel\DtoGroupNewsletter;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContactGroupNewsletterBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request dtoContactGroupNewsletter
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $dtoContactGroupNewsletter, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($dtoContactGroupNewsletter->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (ContactGroupNewsletter::find($dtoContactGroupNewsletter->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactGroupNewsletterNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($dtoContactGroupNewsletter->idContact)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(ContactBL::getById($dtoContactGroupNewsletter->idContact))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($dtoContactGroupNewsletter->idGroupNewsletter)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(GroupNewsletterBL::getById($dtoContactGroupNewsletter->idGroupNewsletter))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get included by contact
     * 
     * @param int idContact
     * 
     * @return DtoContactGroupNewsletter
     */
    public static function getIncludedByContact(int $idContact)
    {
        $result = collect();

        foreach (DB::select(
            '   SELECT cgn.id, gn.description
                FROM contacts_groups_newsletters cgn
                INNER JOIN groups_newsletters gn ON cgn.group_newsletter_id = gn.id
                WHERE cgn.contact_id = :idContact',
            ['idContact' => $idContact]
        ) as $groupNewsletter) {
            $dtoGroupNewsletter = new DtoGroupNewsletter();
            $dtoGroupNewsletter->id = $groupNewsletter->id;
            $dtoGroupNewsletter->description = $groupNewsletter->description;
            $result->push($dtoGroupNewsletter);
        }

        return $result;
    }

    /**
     * Get excluded by contact
     * 
     * @param int idContact
     * 
     * @return DtoContactGroupNewsletter
     */
    public static function getExcludedByContact(int $idContact)
    {
        $result = collect();

        foreach (DB::select(
            '   SELECT id, description
                FROM groups_newsletters
                WHERE id NOT IN (SELECT group_newsletter_id FROM contacts_groups_newsletters WHERE contact_id = :idContact)',
            ['idContact' => $idContact]
        ) as $groupNewsletter) {
            $dtoGroupNewsletter = new DtoGroupNewsletter();
            $dtoGroupNewsletter->id = $groupNewsletter->id;
            $dtoGroupNewsletter->description = $groupNewsletter->description;
            $result->push($dtoGroupNewsletter);
        }

        return $result;
    }


    /**
     * Get included by contact
     * 
     * @param int idContact
     * 
     * @return DtoContactGroupNewsletter
     */
    public static function getIncludedByContactGroup(int $idContact)
    {
        $result = collect();

        foreach (DB::select(
            '   SELECT cgn.id, c.name, c.surname, c.business_name, c.email
            FROM contacts_groups_newsletters cgn
            INNER JOIN contacts c ON cgn.contact_id = c.id
            WHERE cgn.group_newsletter_id = :idContact',
            ['idContact' => $idContact]
        ) as $groupNewsletter) {
            $dtoGroupNewsletter = new DtoGroupNewsletter();
            $dtoGroupNewsletter->id = $groupNewsletter->id;
            $dtoGroupNewsletter->name = $groupNewsletter->name;
            $dtoGroupNewsletter->surname = $groupNewsletter->surname;
            $dtoGroupNewsletter->businessName = $groupNewsletter->business_name;
            $dtoGroupNewsletter->email = $groupNewsletter->email;
            $result->push($dtoGroupNewsletter);
        }

        return $result;
    }

    /**
     * Get excluded by contact
     * 
     * @param int idContact
     * 
     * @return DtoContactGroupNewsletter
     */
    public static function getExcludedByContactGroup(int $idContact)
    {
        $result = collect();

        foreach (DB::select(
            '   SELECT c.id, c.name, c.surname, c.business_name, c.email
                FROM contacts c
                WHERE c.id NOT IN (SELECT contact_id FROM contacts_groups_newsletters WHERE group_newsletter_id = :idContact)
                AND send_newsletter = false
                AND error_newsletter = false',
            ['idContact' => $idContact]
        ) as $groupNewsletter) {
            $dtoGroupNewsletter = new DtoGroupNewsletter();
            $dtoGroupNewsletter->id = $groupNewsletter->id;
            $dtoGroupNewsletter->name = $groupNewsletter->name;
            $dtoGroupNewsletter->surname = $groupNewsletter->surname;
            $dtoGroupNewsletter->businessName = $groupNewsletter->business_name;
            $dtoGroupNewsletter->email = $groupNewsletter->email;
            $result->push($dtoGroupNewsletter);
        }

        return $result;
    }



    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request dtoContactGroupNewsletter
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $dtoContactGroupNewsletter, int $idLanguage)
    {
        static::_validateData($dtoContactGroupNewsletter, $idLanguage, DbOperationsTypesEnum::INSERT);
        $contactGroupNewsletter = new ContactGroupNewsletter();
        $contactGroupNewsletter->contact_id = $dtoContactGroupNewsletter->idContact;
        $contactGroupNewsletter->group_newsletter_id = $dtoContactGroupNewsletter->idGroupNewsletter;
        $contactGroupNewsletter->save();
        return $contactGroupNewsletter->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoContactGroupNewsletter
     * @param int idLanguage
     * 
     * @return int
     */
    public static function update(Request $dtoContactGroupNewsletter, int $idLanguage)
    {
        static::_validateData($dtoContactGroupNewsletter, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $contactGroupNewsletter = ContactGroupNewsletter::find($dtoContactGroupNewsletter->id);
        $contactGroupNewsletter->contact_id = $dtoContactGroupNewsletter->idContact;
        $contactGroupNewsletter->group_newsletter_id = $dtoContactGroupNewsletter->idGroupNewsletter;
        $contactGroupNewsletter->save();
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Update
     * 
     * @param int id
     * @param int idLanguage
     * 
     * @return int
     */
    public static function delete(int $id, int $idLanguage)
    {
        $contactGroupNewsletter = ContactGroupNewsletter::find($id);

        if (is_null($contactGroupNewsletter)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactGroupNewsletterNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $contactGroupNewsletter->delete();
    }

    #endregion DELETE
}

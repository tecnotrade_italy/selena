<?php
namespace App\BusinessLogic;

use App\DtoModel\DtoNation;
use App\DtoModel\DtoSelenaViews;
use App\DtoModel\DtoTagTemplate;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\SelenaSqlViews;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SelenaViewsBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (SelenaSqlViews::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->nameView)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    
    /**
     * Convert to dto
     * 
     * @param Nation nation
     * @param int idLanguage
     * 
     * @return DtoNation
     */
    private static function _convertToDto(SelenaSqlViews $selenaviews)
    {
        $dtoSelenaViews = new DtoSelenaViews();
        $dtoSelenaViews->id = $selenaviews->id;
        $dtoSelenaViews->nameView = $selenaviews->name_view;
        $dtoSelenaViews->description = $selenaviews->description;
        $dtoSelenaViews->select = $selenaviews->select;
        $dtoSelenaViews->from = $selenaviews->from;
        $dtoSelenaViews->where = $selenaviews->where;
        $dtoSelenaViews->orderBy = $selenaviews->order_by;
        $dtoSelenaViews->groupBy = $selenaviews->group_by;
        return $dtoSelenaViews;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoNation
     */
    public static function getAll()
    {
        $result = collect();
        foreach (SelenaSqlViews::all() as $selenaViewsAll) {
            
            $dtoSelenaViews = new DtoSelenaViews();
            $dtoSelenaViews->id = $selenaViewsAll->id;
            $dtoSelenaViews->nameView = $selenaViewsAll->name_view;
            $dtoSelenaViews->description = $selenaViewsAll->description;
            $dtoSelenaViews->select = $selenaViewsAll->select;
            $dtoSelenaViews->from = $selenaViewsAll->from;
            $dtoSelenaViews->where = $selenaViewsAll->where;
            $dtoSelenaViews->orderBy = $selenaViewsAll->order_by;
            $dtoSelenaViews->groupBy = $selenaViewsAll->group_by;
            $result->push($dtoSelenaViews); 
        }
        return $result;
    }

    /**
     * Get by id
     * 
     * @param int id
     * 
     * @return DtoNation
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(SelenaSqlViews::find($id));
    }

    /**
     * get Tag By Type
     * 
     * @param string code
     * 
     * @return DtoNation
     */

    public static function getTag(string $nameView) 
    {
        $selenaSqlView = SelenaSqlViews::where('name_view', $nameView)->first();
        if (isset($selenaSqlView)) {
            $strQuery = "";
            if (isset($selenaSqlView->select)) {
                $strQuery = $strQuery . 'SELECT ' . $selenaSqlView->select;
                if (isset($selenaSqlView->from)) {
                    $strQuery = $strQuery . ' FROM ' . $selenaSqlView->from;

                    if (isset($selenaSqlView->where)) {
                        $strQuery = $strQuery . ' WHERE ' . $selenaSqlView->where;
                    }

                    if (isset($selenaSqlView->order_by)) {
                        $strQuery = $strQuery . ' ORDER BY ' . $selenaSqlView->order_by;
                    }

                    if (isset($selenaSqlView->group_by)) {
                        $strQuery = $strQuery . ' GROUP BY ' . $selenaSqlView->group_by;
                    }

                    $strQuery = $strQuery . ' LIMIT 1';
                }
            }
        }

        $rows = DB::select($strQuery);

        $result = collect();

        foreach ($rows[0] as $key => $value) {
            if($key != 'updated_id' && $key != 'created_at' && $key != 'updated_at' && $key != 'language_id' && $key != '#order#'){
                $dtoTagTemplate = new DtoTagTemplate();
                $dtoTagTemplate->tag = '#' . $key . '#';
                $result->push($dtoTagTemplate);
            }
        }

        return $result;
    }





    public static function getTagByType(string $code) 
    {
        
    if($code == "PageItems" || $code == "FavoritesUserItems"){
        $strCode = "Items";
    }else{   
        if($code == "ShopItemDetail"){
            $strCode = "items";
        }else{ 
            if($code == "DetailShopUser"){
                $strCode = "users_datas";        
            }else{   
                if($code == "ImgAggUser"){
                    $strCode = "users_photos";
                }else{   
                    if($code == "UserDetail" || $code == "DetailSalesOrderUserData" || $code == "UserShortcodeHeaderLoggedIn" || $code == "UserList"){
                        $strCode = "users_datas";
                    }else{ 
                        if($code == "UserSala"){
                            $strCode = "users_datas";
                        }else{    
                            if($code == "DetailAdress"){
                                $strCode = "user_address";
                            }else{    
                                if($code == "User_Profile_Page"){
                                    $strCode = "users_datas";
                                }else{
                                    if($code == "DetailShipmentData"){
                                        $strCode = "users_addresses";
                                    }else{  
                                        if($code == "UsersDetail"){
                                            $strCode = "users_datas";
                                             }else{ 
                                        if($code == "SalesOrderDetailTokenUrl"){
                                            $strCode = "sales_order_tokenlink_user";
                                             }else{ 
                                            if($code == "CartDetailOptional"){
                                                    $strCode = "cart_detail_optional";
                                                }else{  
                                                if($code == "GrSearchListItems"|| $code == "DetailGrItemsSearch"){
                                                    $strCode = "gr_items";
                                                    }else{
                                                    if($code == "DetailRowOrder"|| $code == "DetailMailTableOrderSales" || $code == "UploadFileNew"){
                                                        $strCode = "sales_orders_details";
                                                         }else{
                                                        if($code == "UploadFileDetail"){
                                                        $strCode = "file_sales_orders_details";
                                                        }else{
                                                        if($code == "ListSalesOrder" || $code == "SummaryDescriptionDocument"|| $code == "SummaryAmountOrder"){
                                                            $strCode = "sales_orders";
                                                            }else{
                                                                if($code == "CartUsersAddresses" || $code == "ListUsersAddresses" || $code == "DetailGoodShipmentData" || $code == "OrderDataShippingDocuments" ){
                                                                    $strCode = "users_addresses";
                                                                    }else{
                                                                    if($code == "Cart_Detail_Single_Row"){
                                                                        $strCode = "carts_details";
                                                                    }else{
                                                                        if($code == "ListAttachmentUser"){
                                                                            $strCode = "item_attachment_language_file_table";
                                                                        }else{
                                                                       if($code == "BookingConnectionsDate" || $code == "BookingConnectionsResult"){
                                                                            $strCode = "booking_connections";
                                                                    }else{
                                                                        if($code == "BlogListArticle" || $code == "NewsListArticle"){
                                                                            $strCode = "languages_pages";
                                                                        }else{
                                                                            if($code == "SalesmanShortcodeHeaderLoggedIn" || $code == "SalesmanShortcodeHeaderLoggedOut"){
                                                                                $strCode = "salesman";
                                                                            }else{
                                                                                $strCode = $code;
                                                                            }
                                                                        }
                                                                }
                                                            }
                                                        }
                                                    }                                            
                                                }
                                            }}}}}}
                                        }
                                    }
                                }
                            }
                        }              
                    }
                }
            }
        }
        $viewByCode = SelenaSqlViews::where('from', 'like', '%' . strtolower($strCode) . '%')->first();
       
        $result = collect();
        if(isset($viewByCode)){
            
            $principalTable = DB::getSchemaBuilder()->getColumnListing(strtolower($strCode));
            $secondTable = DB::getSchemaBuilder()->getColumnListing(strtolower($strCode . '_languages'));
            $allTags = array_merge($principalTable,$secondTable);
            foreach($allTags as $tags){
                if($tags != 'updated_id' && $tags != 'created_at' && $tags != 'updated_at' && $tags != 'language_id' && $tags != '#order#'){
                    $dtoTagTemplate = new DtoTagTemplate();
                    $dtoTagTemplate->tag = '#' . $tags . '#';
                    $result->push($dtoTagTemplate);
                }
            }
            
            return $result;
            
        }else{
            return '';
        }
    }

    #endregion get Tag By Type

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $selenaviews = new SelenaSqlViews();
            $selenaviews->name_view = $request->nameView;
            $selenaviews->description = $request->description;
            $selenaviews->select = $request->select;
            $selenaviews->from = $request->from;
            $selenaviews->where = $request->where;
            $selenaviews->order_by = $request->orderBy;
            $selenaviews->group_by = $request->groupBy;
            $selenaviews->created_id = $request->$idUser;
            $selenaviews->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $selenaviews->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoSelenaViews
     * @param int idLanguage
     */
    public static function update(Request $dtoSelenaViews, int $idLanguage)
    {
        static::_validateData($dtoSelenaViews, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $selenaViewsOnDb = SelenaSqlViews::find($dtoSelenaViews->id);
        
        DB::beginTransaction();

        try {
            $selenaViewsOnDb->name_view = $dtoSelenaViews->nameView;
            $selenaViewsOnDb->description = $dtoSelenaViews->description;
            $selenaViewsOnDb->select = $dtoSelenaViews->select;
            $selenaViewsOnDb->from = $dtoSelenaViews->from;
            $selenaViewsOnDb->where = $dtoSelenaViews->where;
            $selenaViewsOnDb->order_by = $dtoSelenaViews->orderBy;
            $selenaViewsOnDb->group_by = $dtoSelenaViews->groupBy;
            $selenaViewsOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {

        $selenaviews = SelenaSqlViews::find($id);

        if (is_null($selenaviews)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $selenaviews->delete();
    }

    #endregion DELETE
}

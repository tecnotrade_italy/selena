<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoCommunityImageGallery;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\CommunityImageGallery;
use Exception;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;

class CommunityImageGalleryBL
{
    #region PRIVATE

    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request dtoCommunityImageGallery
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationsTypesEnum)
    {

       

    }

    /**
     * Convert to dto
     * 
     * @param CommunityImageGallery CommunityImageGallery
     * @return DtoCommunityImageGallery
     */
    private static function _convertToDto($CommunityImageGallery)
    {
        $dtoCommunityImageGallery = new DtoCommunityImageGallery();

        if (isset($CommunityImageGallery->id)) {
            $dtoCommunityImageGallery->id = $CommunityImageGallery->id;
        }

        if (isset($CommunityImageGallery->user_id)) {
            $dtoCommunityImageGallery->user_id = $CommunityImageGallery->user_id;
        }

        if (isset($CommunityImageGallery->name)) {
            $dtoCommunityImageGallery->name = $CommunityImageGallery->name;
        }

        if (isset($CommunityImageGallery->description)) {
            $dtoCommunityImageGallery->description = $CommunityImageGallery->description;
        }

        return $dtoCommunityImageGallery;
    }


    /**
     * Convert to VatType
     * 
     * @param dtoCommunityImageGallery dtoCommunityImageGallery
     * 
     * @return CommunityImageGallery
     */
    private static function _convertToModel($dtoCommunityImageGallery)
    {
        $CommunityImageGallery = new CommunityImageGallery();

        if (isset($dtoCommunityImageGallery->id)) {
            $CommunityImageGallery->id = $dtoCommunityImageGallery->id;
        }

        if (isset($dtoCommunityImageGallery->user_id)) {
            $CommunityImageGallery->user_id = $dtoCommunityImageGallery->user_id;
        }

        if (isset($dtoCommunityImageGallery->name)) {
            $CommunityImageGallery->name = $dtoCommunityImageGallery->name;
        }

        if (isset($dtoCommunityImageGallery->description)) {
            $CommunityImageGallery->description = $dtoCommunityImageGallery->description;
        }

        return $CommunityImageGallery;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCommunityction
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(CommunityImageGallery::find($id));
    }

    /*
     * Get all
     * 
     * @return DtoCommunityImageGallery
     */
    public static function getAll()
    {

        $result = collect(); 

        foreach (CommunityImageGallery::orderBy('id','DESC')->get() as $communityImageGallery) {

            $DtoCommunityImageGallery = new DtoCommunityImageGallery();

            $DtoCommunityImageGallery->id = $communityImageGallery->id;
            $DtoCommunityImageGallery->name = $communityImageGallery->name;
            $DtoCommunityImageGallery->user_id = $communityImageGallery->user_id;
            $DtoCommunityImageGallery->description = $communityImageGallery->description;
            
            $result->push($DtoCommunityImageGallery); 
        }
        return $result;

    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoCommunityImageGallery
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoCommunityImageGallery)
    {
        
        static::_validateData($DtoCommunityImageGallery, $idLanguage, DbOperationsTypesEnum::INSERT);
        $CommunityImageGallery = static::_convertToModel($DtoCommunityImageGallery);

        $CommunityImageGallery->save();

        return $CommunityImageGallery->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoCommunityImageGallery
     * @param int idLanguage
     */
    public static function update(Request $DtoCommunityImageGallery)
    {
        static::_validateData($DtoCommunityImageGallery, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $CommunityImageGallery = CommunityImageGallery::find($DtoCommunityImageGallery->id);
        DBHelper::updateAndSave(static::_convertToModel($DtoCommunityImageGallery), $CommunityImageGallery);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id)
    {
        $CommunityImageGallery = CommunityImageGallery::find($id);

        if (is_null($CommunityImageGallery)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $CommunityImageGallery->delete();
    }

    #endregion DELETE
}

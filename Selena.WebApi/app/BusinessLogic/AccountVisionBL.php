<?php

namespace App\BusinessLogic;

use App\AccountVision;
use App\AccountVisionPhotos;
use App\BusinessName;
use App\DtoModel\DtoAccountVision;
use App\DtoModel\DtoAccountVisionPhotos;
use App\DtoModel\DtoItemGroupTechnicalSpecification;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use App\GrItems;
use App\Helpers\HttpHelper;
use App\Setting;
use App\UsersDatas;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

class AccountVisionBL
{

     public static $dirImageInterventionReportTest = '../storage/app/public/images/Intervention/Foto3/REPORT TEST C-V/';
     public static $dirImageIntervention = '../storage/app/public/images/Intervention/Foto3/CV/';
     public static $dirImageAccountVisionPhotos = '../storage/app/public/images/AccountVisionPhotos/';

    /**
     * getAllAccountVision
     * 
     * @return DtoAccountVision
     */
    public static function getAllAccountVision($idLanguage)
    {
        $result = collect();

        foreach (AccountVision::orderBy('id_contovisione', 'DESC')->take(150)->get() as $AccountVision) {

            $DtoAccountVision = new DtoAccountVision();

            if (isset($AccountVision->id)) {
                $DtoAccountVision->id = $AccountVision->id;
            } else {
                $DtoAccountVision->id = '';
            }
            if (isset($AccountVision->id_contovisione)) {
                $DtoAccountVision->id_contovisione = $AccountVision->id_contovisione;
            } else {
                $DtoAccountVision->id_contovisione = '';
            }

            $timestamp = strtotime($AccountVision->date);
            if (isset($AccountVision->date)) {
                if ($timestamp === FALSE) {
                    $DtoAccountVision->date = $AccountVision->date;
                }else{
                    $DtoAccountVision->date = date('d/m/Y', $timestamp);
                }
            }else{
                $DtoAccountVision->date = '';
            }

          /*  if (isset($AccountVision->date)) {
                $DtoAccountVision->date = $AccountVision->date;
            } else {
                $DtoAccountVision->date = '';
            }*/
             if (isset($AccountVision->ddt_gr)) {
                $DtoAccountVision->ddt_gr = $AccountVision->ddt_gr;
            } else {
                $DtoAccountVision->ddt_gr = '';
            }
            if (isset($AccountVision->code_gr)) {
                $DtoAccountVision->code_gr = $AccountVision->code_gr;
            } else {
                $DtoAccountVision->code_gr = '';
            }
          
              if (isset($AccountVision->description)) {
                $DtoAccountVision->description = $AccountVision->description;
            } else {
                $DtoAccountVision->description = '';
            }
          
              if (isset($AccountVision->note)) {
                $DtoAccountVision->note = $AccountVision->note;
            } else {
                $DtoAccountVision->note = '';
            }

            $timestamp1 = strtotime($AccountVision->date_return);
            if (isset($AccountVision->date_return)) {
                if ($timestamp1 === FALSE) {
                    $DtoAccountVision->date_return = $AccountVision->date_return;
                }else{
                    $DtoAccountVision->date_return = date('d/m/Y', $timestamp1);
                }
            }else{
                $DtoAccountVision->date_return = '';
            }

              /*if (isset($AccountVision->date_return)) {
                $DtoAccountVision->date_return = $AccountVision->date_return;
            } else {
                $DtoAccountVision->date_return = '';
            }*/

              if (isset($AccountVision->ddt_return)) {
                $DtoAccountVision->ddt_return = $AccountVision->ddt_return;
            } else {
                $DtoAccountVision->ddt_return = '';
            }
              if (isset($AccountVision->note_return)) {
                $DtoAccountVision->note_return = $AccountVision->note_return;
            } else {
                $DtoAccountVision->note_return = '';
            }
              if (isset($AccountVision->soll)) {
                $DtoAccountVision->soll = $AccountVision->soll;
            } else {
                $DtoAccountVision->soll = '';
            }
           if (isset($AccountVision->user_id)) {
                $DtoAccountVision->user_id = UsersDatas::where('user_id', $AccountVision->user_id)->get()->first()->business_name;
            } else {
                if (is_null($AccountVision->user_id)) {
                $DtoAccountVision->user_id = '';
                }
            }

   foreach (AccountVisionPhotos::where('account_vision_id', $AccountVision->id)->get() as $AccountVisionCaptures) {
                $DtoAccountVisions = new DtoAccountVision();
                //'/storage/images/Intervention/' + 'Foto3/CV/ ' + numero + '.JPG' as 'img',
                if(!is_null($AccountVisionCaptures->img)){
                    $DtoAccountVisions->img = $AccountVisionCaptures->img;
                }else{
                    if (file_exists(static::$dirImageIntervention . '/C-V ' . $AccountVisionCaptures->id_cv_librone . '.jpg' )) {
                        $DtoAccountVisions->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/CV/C-V ' . $AccountVisionCaptures->id_cv_librone . '.jpg';
                    } else {
                        if (file_exists(static::$dirImageIntervention . '/C-V ' . $AccountVisionCaptures->id_cv_librone . '.JPG' )) {
                            $DtoAccountVisions->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/CV/C-V ' . $AccountVisionCaptures->id_cv_librone . '.JPG';
                        }else{ 
                            if ($AccountVisionCaptures->img != '' && !is_null($AccountVisionCaptures->img)) {
                                $pos = strpos($AccountVisionCaptures->img, strval($AccountVisionCaptures->account_vision_id));
                                if($pos === false){
                                    $DtoAccountVisions->img = '';
                                }else{
                                    $DtoAccountVisions->img = $AccountVisionCaptures->img;
                                }
                            } else {
                                $DtoAccountVisions->img = '';
                            }
                        }  
                    }
                }

              if (file_exists(static::$dirImageInterventionReportTest . 'C-V '. $AccountVisionCaptures->id_cv_librone . '.pdf' )) {
                    $DtoAccountVision->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/REPORT TEST C-V/C-V ' . $AccountVisionCaptures->id_cv_librone . '.pdf';
            } else {
                   if ($AccountVisionCaptures->img_rep_test != '' && $AccountVisionCaptures->img_rep_test != null) {
                        $DtoAccountVision->img_rep_test = $AccountVisionCaptures->img_rep_test;
                    } else {
                        $DtoAccountVision->img_rep_test = '';
                    }
            }

            /*    if (isset($AccountVisionCaptures->img)) {
                $DtoAccountVisions->img = $AccountVisionCaptures->img;
                } else {
                $DtoAccountVision->img = '';
            }
            if (isset($AccountVisionCaptures->img_rep_test)) {
                $DtoAccountVisions->img_rep_test = $AccountVisionCaptures->img_rep_test;
                } else {
                $DtoAccountVision->img_rep_test = '';
            }*/

                $DtoAccountVision->imageaccountvision->push($DtoAccountVisions);
            }

            $result->push($DtoAccountVision);
        }
        return $result;
    }


    private static function getUrlOpenFileImg()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"];
    }


/**
     * SearchAccountVisionOpen
     * 
     * @param Request request
     * 
     */
    public static function SearchAccountVisionOpen(request $request){
        $result = collect();
        
        foreach (DB::select(
                'SELECT account_vision.*
                FROM account_vision
                WHERE account_vision.date_return is null 
                and account_vision.ddt_return is null limit 100'        
            ) as $AccountVision) {

                $DtoAccountVision = new DtoAccountVision();

                if (isset($AccountVision->id)) {
                    $DtoAccountVision->id = $AccountVision->id;
                } else {
                    $DtoAccountVision->id = '';
                }
                if (isset($AccountVision->id_contovisione)) {
                    $DtoAccountVision->id_contovisione = $AccountVision->id_contovisione;
                } else {
                    $DtoAccountVision->id_contovisione = '';
                }
    
                $timestamp = strtotime($AccountVision->date);
                if (isset($AccountVision->date)) {
                    if ($timestamp === FALSE) {
                        $DtoAccountVision->date = $AccountVision->date;
                    }else{
                        $DtoAccountVision->date = date('d/m/Y', $timestamp);
                    }
                }else{
                    $DtoAccountVision->date = '';
                }
    
              /*  if (isset($AccountVision->date)) {
                    $DtoAccountVision->date = $AccountVision->date;
                } else {
                    $DtoAccountVision->date = '';
                }*/
                 if (isset($AccountVision->ddt_gr)) {
                    $DtoAccountVision->ddt_gr = $AccountVision->ddt_gr;
                } else {
                    $DtoAccountVision->ddt_gr = '';
                }
                if (isset($AccountVision->code_gr)) {
                    $DtoAccountVision->code_gr = $AccountVision->code_gr;
                } else {
                    $DtoAccountVision->code_gr = '';
                }
              
                  if (isset($AccountVision->description)) {
                    $DtoAccountVision->description = $AccountVision->description;
                } else {
                    $DtoAccountVision->description = '';
                }
              
                  if (isset($AccountVision->note)) {
                    $DtoAccountVision->note = $AccountVision->note;
                } else {
                    $DtoAccountVision->note = '';
                }
    
                $timestamp1 = strtotime($AccountVision->date_return);
                if (isset($AccountVision->date_return)) {
                    if ($timestamp1 === FALSE) {
                        $DtoAccountVision->date_return = $AccountVision->date_return;
                    }else{
                        $DtoAccountVision->date_return = date('d/m/Y', $timestamp1);
                    }
                }else{
                    $DtoAccountVision->date_return = '';
                }
    
                  /*if (isset($AccountVision->date_return)) {
                    $DtoAccountVision->date_return = $AccountVision->date_return;
                } else {
                    $DtoAccountVision->date_return = '';
                }*/
    
                  if (isset($AccountVision->ddt_return)) {
                    $DtoAccountVision->ddt_return = $AccountVision->ddt_return;
                } else {
                    $DtoAccountVision->ddt_return = '';
                }
                  if (isset($AccountVision->note_return)) {
                    $DtoAccountVision->note_return = $AccountVision->note_return;
                } else {
                    $DtoAccountVision->note_return = '';
                }
                  if (isset($AccountVision->soll)) {
                    $DtoAccountVision->soll = $AccountVision->soll;
                } else {
                    $DtoAccountVision->soll = '';
                }
               if (isset($AccountVision->user_id)) {
                    $DtoAccountVision->user_id = UsersDatas::where('user_id', $AccountVision->user_id)->get()->first()->business_name;
                } else {
                    if (is_null($AccountVision->user_id)) {
                    $DtoAccountVision->user_id = '';
                    }
                }
    
       foreach (AccountVisionPhotos::where('account_vision_id', $AccountVision->id)->get() as $AccountVisionCaptures) {
                    $DtoAccountVisions = new DtoAccountVision();
                    //'/storage/images/Intervention/' + 'Foto3/CV/ ' + numero + '.JPG' as 'img',
                    if(!is_null($AccountVisionCaptures->img)){
                        $DtoAccountVisions->img = $AccountVisionCaptures->img;
                    }else{
                        if (file_exists(static::$dirImageIntervention . '/C-V ' . $AccountVisionCaptures->id_cv_librone . '.jpg' )) {
                            $DtoAccountVisions->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/CV/C-V ' . $AccountVisionCaptures->id_cv_librone . '.jpg';
                        } else {
                            if (file_exists(static::$dirImageIntervention . '/C-V ' . $AccountVisionCaptures->id_cv_librone . '.JPG' )) {
                                $DtoAccountVisions->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/CV/C-V ' . $AccountVisionCaptures->id_cv_librone . '.JPG';
                            }else{ 
                                if ($AccountVisionCaptures->img != '' && !is_null($AccountVisionCaptures->img)) {
                                    $pos = strpos($AccountVisionCaptures->img, strval($AccountVisionCaptures->account_vision_id));
                                    if($pos === false){
                                        $DtoAccountVisions->img = '';
                                    }else{
                                        $DtoAccountVisions->img = $AccountVisionCaptures->img;
                                    }
                                } else {
                                    $DtoAccountVisions->img = '';
                                }
                            }  
                        }
                    }
    
                  if (file_exists(static::$dirImageInterventionReportTest . 'C-V '. $AccountVisionCaptures->id_cv_librone . '.pdf' )) {
                        $DtoAccountVision->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/REPORT TEST C-V/C-V ' . $AccountVisionCaptures->id_cv_librone . '.pdf';
                } else {
                       if ($AccountVisionCaptures->img_rep_test != '' && $AccountVisionCaptures->img_rep_test != null) {
                            $DtoAccountVision->img_rep_test = $AccountVisionCaptures->img_rep_test;
                        } else {
                            $DtoAccountVision->img_rep_test = '';
                        }
                }
    
                /*    if (isset($AccountVisionCaptures->img)) {
                    $DtoAccountVisions->img = $AccountVisionCaptures->img;
                    } else {
                    $DtoAccountVision->img = '';
                }
                if (isset($AccountVisionCaptures->img_rep_test)) {
                    $DtoAccountVisions->img_rep_test = $AccountVisionCaptures->img_rep_test;
                    } else {
                    $DtoAccountVision->img_rep_test = '';
                }*/
    
                    $DtoAccountVision->imageaccountvision->push($DtoAccountVisions);
                }
    
                $result->push($DtoAccountVision);
            }
            return $result;
        }
    
    

/**
     * SearchAccountVisionClose
     * 
     * @param Request request
     * 
     */
    public static function SearchAccountVisionClose(request $request){
        $result = collect();
        
        foreach (DB::select(
                'SELECT account_vision.*
                FROM account_vision
                WHERE account_vision.date_return is not null 
                and account_vision.ddt_return is not null limit 100'  
            ) as $AccountVision) {

                $DtoAccountVision = new DtoAccountVision();

                if (isset($AccountVision->id)) {
                    $DtoAccountVision->id = $AccountVision->id;
                } else {
                    $DtoAccountVision->id = '';
                }
                if (isset($AccountVision->id_contovisione)) {
                    $DtoAccountVision->id_contovisione = $AccountVision->id_contovisione;
                } else {
                    $DtoAccountVision->id_contovisione = '';
                }
    
                $timestamp = strtotime($AccountVision->date);
                if (isset($AccountVision->date)) {
                    if ($timestamp === FALSE) {
                        $DtoAccountVision->date = $AccountVision->date;
                    }else{
                        $DtoAccountVision->date = date('d/m/Y', $timestamp);
                    }
                }else{
                    $DtoAccountVision->date = '';
                }
    
              /*  if (isset($AccountVision->date)) {
                    $DtoAccountVision->date = $AccountVision->date;
                } else {
                    $DtoAccountVision->date = '';
                }*/
                 if (isset($AccountVision->ddt_gr)) {
                    $DtoAccountVision->ddt_gr = $AccountVision->ddt_gr;
                } else {
                    $DtoAccountVision->ddt_gr = '';
                }
                if (isset($AccountVision->code_gr)) {
                    $DtoAccountVision->code_gr = $AccountVision->code_gr;
                } else {
                    $DtoAccountVision->code_gr = '';
                }
              
                  if (isset($AccountVision->description)) {
                    $DtoAccountVision->description = $AccountVision->description;
                } else {
                    $DtoAccountVision->description = '';
                }
              
                  if (isset($AccountVision->note)) {
                    $DtoAccountVision->note = $AccountVision->note;
                } else {
                    $DtoAccountVision->note = '';
                }
    
                $timestamp1 = strtotime($AccountVision->date_return);
                if (isset($AccountVision->date_return)) {
                    if ($timestamp1 === FALSE) {
                        $DtoAccountVision->date_return = $AccountVision->date_return;
                    }else{
                        $DtoAccountVision->date_return = date('d/m/Y', $timestamp1);
                    }
                }else{
                    $DtoAccountVision->date_return = '';
                }
    
                  /*if (isset($AccountVision->date_return)) {
                    $DtoAccountVision->date_return = $AccountVision->date_return;
                } else {
                    $DtoAccountVision->date_return = '';
                }*/
    
                  if (isset($AccountVision->ddt_return)) {
                    $DtoAccountVision->ddt_return = $AccountVision->ddt_return;
                } else {
                    $DtoAccountVision->ddt_return = '';
                }
                  if (isset($AccountVision->note_return)) {
                    $DtoAccountVision->note_return = $AccountVision->note_return;
                } else {
                    $DtoAccountVision->note_return = '';
                }
                  if (isset($AccountVision->soll)) {
                    $DtoAccountVision->soll = $AccountVision->soll;
                } else {
                    $DtoAccountVision->soll = '';
                }
               if (isset($AccountVision->user_id)) {
                    $DtoAccountVision->user_id = UsersDatas::where('user_id', $AccountVision->user_id)->get()->first()->business_name;
                } else {
                    if (is_null($AccountVision->user_id)) {
                    $DtoAccountVision->user_id = '';
                    }
                }
    
       foreach (AccountVisionPhotos::where('account_vision_id', $AccountVision->id)->get() as $AccountVisionCaptures) {
                    $DtoAccountVisions = new DtoAccountVision();
                    //'/storage/images/Intervention/' + 'Foto3/CV/ ' + numero + '.JPG' as 'img',
                    if(!is_null($AccountVisionCaptures->img)){
                        $DtoAccountVisions->img = $AccountVisionCaptures->img;
                    }else{
                        if (file_exists(static::$dirImageIntervention . '/C-V ' . $AccountVisionCaptures->id_cv_librone . '.jpg' )) {
                            $DtoAccountVisions->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/CV/C-V ' . $AccountVisionCaptures->id_cv_librone . '.jpg';
                        } else {
                            if (file_exists(static::$dirImageIntervention . '/C-V ' . $AccountVisionCaptures->id_cv_librone . '.JPG' )) {
                                $DtoAccountVisions->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/CV/C-V ' . $AccountVisionCaptures->id_cv_librone . '.JPG';
                            }else{ 
                                if ($AccountVisionCaptures->img != '' && !is_null($AccountVisionCaptures->img)) {
                                    $pos = strpos($AccountVisionCaptures->img, strval($AccountVisionCaptures->account_vision_id));
                                    if($pos === false){
                                        $DtoAccountVisions->img = '';
                                    }else{
                                        $DtoAccountVisions->img = $AccountVisionCaptures->img;
                                    }
                                } else {
                                    $DtoAccountVisions->img = '';
                                }
                            }  
                        }
                    }
    
                 /* if (file_exists(static::$dirImageInterventionReportTest . 'C-V '. $AccountVisionCaptures->id_cv_librone . '.pdf' )) {
                        $DtoAccountVision->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/REPORT TEST C-V/C-V ' . $AccountVisionCaptures->id_cv_librone . '.pdf';
                } else {
                       if ($AccountVisionCaptures->img_rep_test != '' && $AccountVisionCaptures->img_rep_test != null) {
                            $DtoAccountVision->img_rep_test = $AccountVisionCaptures->img_rep_test;
                        } else {
                            $DtoAccountVision->img_rep_test = '';
                        }
                }*/
    
                /*    if (isset($AccountVisionCaptures->img)) {
                    $DtoAccountVisions->img = $AccountVisionCaptures->img;
                    } else {
                    $DtoAccountVision->img = '';
                }
                if (isset($AccountVisionCaptures->img_rep_test)) {
                    $DtoAccountVisions->img_rep_test = $AccountVisionCaptures->img_rep_test;
                    } else {
                    $DtoAccountVision->img_rep_test = '';
                }*/
    
                    $DtoAccountVision->imageaccountvision->push($DtoAccountVisions);
                }
    
                $result->push($DtoAccountVision);
            }
            return $result;
        }
    
    

/**
     * SearchAccountVision
     * 
     * @param Request request
     * 
     */
    public static function SearchAccountVision(request $request){
        $result = collect();

        $content = '';
        $finalHtml = '';
        $strWhere = "";
        $user_id = $request->user_id_cv;
           
        //$UserSearch = $request->user_id;
        //if ($UserSearch != "") {
          //  $strWhere = $strWhere . ' AND account_vision.user_id = ' . $UserSearch . '';
       // }
       
         $NoteSearch = $request->note;
        if ($NoteSearch != "") {
            $strWhere = $strWhere . ' OR account_vision.note ilike \'%' . $NoteSearch . '%\'';
        }

        $idSearch = $request->id_contovisione;
        if ($idSearch != "") {
          $strWhere = $strWhere . ' OR account_vision.id_contovisione = ' . $idSearch . '';
        }

         $CodeGrSearch = $request->code_gr;
        if ($CodeGrSearch != "") {
            $strWhere = $strWhere . ' OR account_vision.code_gr ilike \'%' . $CodeGrSearch . '%\'';
        }

            foreach (DB::select(
                'SELECT account_vision.*
                FROM account_vision
                WHERE account_vision.user_id = :userId ' . $strWhere,
                ['userId' => $user_id],
            ) as $AccountVision) {

                $DtoAccountVision = new DtoAccountVision();

                if (isset($AccountVision->id)) {
                    $DtoAccountVision->id = $AccountVision->id;
                } else {
                    $DtoAccountVision->id = '';
                }
                if (isset($AccountVision->id_contovisione)) {
                    $DtoAccountVision->id_contovisione = $AccountVision->id_contovisione;
                } else {
                    $DtoAccountVision->id_contovisione = '';
                }
    
                $timestamp = strtotime($AccountVision->date);
                if (isset($AccountVision->date)) {
                    if ($timestamp === FALSE) {
                        $DtoAccountVision->date = $AccountVision->date;
                    }else{
                        $DtoAccountVision->date = date('d/m/Y', $timestamp);
                    }
                }else{
                    $DtoAccountVision->date = '';
                }
    
              /*  if (isset($AccountVision->date)) {
                    $DtoAccountVision->date = $AccountVision->date;
                } else {
                    $DtoAccountVision->date = '';
                }*/
                 if (isset($AccountVision->ddt_gr)) {
                    $DtoAccountVision->ddt_gr = $AccountVision->ddt_gr;
                } else {
                    $DtoAccountVision->ddt_gr = '';
                }
                if (isset($AccountVision->code_gr)) {
                    $DtoAccountVision->code_gr = $AccountVision->code_gr;
                } else {
                    $DtoAccountVision->code_gr = '';
                }
              
                  if (isset($AccountVision->description)) {
                    $DtoAccountVision->description = $AccountVision->description;
                } else {
                    $DtoAccountVision->description = '';
                }
              
                  if (isset($AccountVision->note)) {
                    $DtoAccountVision->note = $AccountVision->note;
                } else {
                    $DtoAccountVision->note = '';
                }
    
                $timestamp1 = strtotime($AccountVision->date_return);
                if (isset($AccountVision->date_return)) {
                    if ($timestamp1 === FALSE) {
                        $DtoAccountVision->date_return = $AccountVision->date_return;
                    }else{
                        $DtoAccountVision->date_return = date('d/m/Y', $timestamp1);
                    }
                }else{
                    $DtoAccountVision->date_return = '';
                }
    
                  /*if (isset($AccountVision->date_return)) {
                    $DtoAccountVision->date_return = $AccountVision->date_return;
                } else {
                    $DtoAccountVision->date_return = '';
                }*/
    
                  if (isset($AccountVision->ddt_return)) {
                    $DtoAccountVision->ddt_return = $AccountVision->ddt_return;
                } else {
                    $DtoAccountVision->ddt_return = '';
                }
                  if (isset($AccountVision->note_return)) {
                    $DtoAccountVision->note_return = $AccountVision->note_return;
                } else {
                    $DtoAccountVision->note_return = '';
                }
                  if (isset($AccountVision->soll)) {
                    $DtoAccountVision->soll = $AccountVision->soll;
                } else {
                    $DtoAccountVision->soll = '';
                }
               if (isset($AccountVision->user_id)) {
                    $DtoAccountVision->user_id = UsersDatas::where('user_id', $AccountVision->user_id)->get()->first()->business_name;
                } else {
                    if (is_null($AccountVision->user_id)) {
                    $DtoAccountVision->user_id = '';
                    }
                }
    
       foreach (AccountVisionPhotos::where('account_vision_id', $AccountVision->id)->get() as $AccountVisionCaptures) {
                    $DtoAccountVisions = new DtoAccountVision();
                    //'/storage/images/Intervention/' + 'Foto3/CV/ ' + numero + '.JPG' as 'img',
                    if(!is_null($AccountVisionCaptures->img)){
                        $DtoAccountVisions->img = $AccountVisionCaptures->img;
                    }else{
                        if (file_exists(static::$dirImageIntervention . '/C-V ' . $AccountVisionCaptures->id_cv_librone . '.jpg' )) {
                            $DtoAccountVisions->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/CV/C-V ' . $AccountVisionCaptures->id_cv_librone . '.jpg';
                        } else {
                            if (file_exists(static::$dirImageIntervention . '/C-V ' . $AccountVisionCaptures->id_cv_librone . '.JPG' )) {
                                $DtoAccountVisions->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/CV/C-V ' . $AccountVisionCaptures->id_cv_librone . '.JPG';
                            }else{ 
                                if ($AccountVisionCaptures->img != '' && !is_null($AccountVisionCaptures->img)) {
                                    $pos = strpos($AccountVisionCaptures->img, strval($AccountVisionCaptures->account_vision_id));
                                    if($pos === false){
                                        $DtoAccountVisions->img = '';
                                    }else{
                                        $DtoAccountVisions->img = $AccountVisionCaptures->img;
                                    }
                                } else {
                                    $DtoAccountVisions->img = '';
                                }
                            }  
                        }
                    }
    
                  if (file_exists(static::$dirImageInterventionReportTest . 'C-V '. $AccountVisionCaptures->id_cv_librone . '.pdf' )) {
                        $DtoAccountVision->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/REPORT TEST C-V/C-V ' . $AccountVisionCaptures->id_cv_librone . '.pdf';
                } else {
                       if ($AccountVisionCaptures->img_rep_test != '' && $AccountVisionCaptures->img_rep_test != null) {
                            $DtoAccountVision->img_rep_test = $AccountVisionCaptures->img_rep_test;
                        } else {
                            $DtoAccountVision->img_rep_test = '';
                        }
                }
    
                /*    if (isset($AccountVisionCaptures->img)) {
                    $DtoAccountVisions->img = $AccountVisionCaptures->img;
                    } else {
                    $DtoAccountVision->img = '';
                }
                if (isset($AccountVisionCaptures->img_rep_test)) {
                    $DtoAccountVisions->img_rep_test = $AccountVisionCaptures->img_rep_test;
                    } else {
                    $DtoAccountVision->img_rep_test = '';
                }*/
    
                    $DtoAccountVision->imageaccountvision->push($DtoAccountVisions);
                }
    
                $result->push($DtoAccountVision);
            }
            return $result;
        }
    
    

/**
     * Get by getById
     * 
     * @param int id
     * @return DtoAccountVision
     */
        public static function getById(int $id)
    {

        $AccountVision = AccountVision::where('id', $id)->first();
       
        $DtoAccountVision = new DtoAccountVision();

        if (isset($AccountVision->id)) {
                $DtoAccountVision->id = $AccountVision->id;
            } else {
                $DtoAccountVision->id = '';
            }
            if (isset($AccountVision->id_contovisione)) {
                $DtoAccountVision->id_contovisione = $AccountVision->id_contovisione;
            } else {
                $DtoAccountVision->id_contovisione = '';
            }

            if (isset($AccountVision->date)) {
                $DtoAccountVision->date = $AccountVision->date;
            } else {
                $DtoAccountVision->date = '';
            }
             if (isset($AccountVision->ddt_gr)) {
                $DtoAccountVision->ddt_gr = $AccountVision->ddt_gr;
            } else {
                $DtoAccountVision->ddt_gr = '';
            }
            if (isset($AccountVision->code_gr)) {
                $DtoAccountVision->code_gr = $AccountVision->code_gr;
            } else {
                $DtoAccountVision->code_gr = '';
            }
          
              if (isset($AccountVision->description)) {
                $DtoAccountVision->description = $AccountVision->description;
            } else {
                $DtoAccountVision->description = '';
            }
          
              if (isset($AccountVision->note)) {
                $DtoAccountVision->note = $AccountVision->note;
            } else {
                $DtoAccountVision->note = '';
            }
              if (isset($AccountVision->date_return)) {
                $DtoAccountVision->date_return = $AccountVision->date_return;
            } else {
                $DtoAccountVision->date_return = '';
            }
              if (isset($AccountVision->ddt_return)) {
                $DtoAccountVision->ddt_return = $AccountVision->ddt_return;
            } else {
                $DtoAccountVision->ddt_return = '';
            }
              if (isset($AccountVision->note_return)) {
                $DtoAccountVision->note_return = $AccountVision->note_return;
            } else {
                $DtoAccountVision->note_return = '';
            }
              if (isset($AccountVision->soll)) {
                $DtoAccountVision->soll = $AccountVision->soll;
            } else {
                $DtoAccountVision->soll = '';
            }
          
            if (isset($AccountVision->user_id)) {
                $DtoAccountVision->user_id = $AccountVision->user_id;
                $DtoAccountVision->DescriptionUser_id = UsersDatas::where('user_id', $AccountVision->user_id)->first()->business_name;
            }

            foreach (AccountVisionPhotos::where('account_vision_id', $AccountVision->id)->get() as $AccountVisionCaptures) {
                $DtoAccountVisions = new DtoAccountVision();
            if(!is_null($AccountVisionCaptures->img)){
                $DtoAccountVisions->img = $AccountVisionCaptures->img;
            }else{
                if (file_exists(static::$dirImageIntervention . '/C-V ' . $AccountVisionCaptures->id_cv_librone . '.jpg' )) {
                    $DtoAccountVisions->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/CV/C-V ' . $AccountVisionCaptures->id_cv_librone . '.jpg';
                } else {
                    if (file_exists(static::$dirImageIntervention . '/C-V ' . $AccountVisionCaptures->id_cv_librone . '.JPG' )) {
                        $DtoAccountVisions->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/CV/C-V ' . $AccountVisionCaptures->id_cv_librone . '.JPG';
                    }else{ 
                        if ($AccountVisionCaptures->img != '' && !is_null($AccountVisionCaptures->img)) {
                            $pos = strpos($AccountVisionCaptures->img, strval($AccountVisionCaptures->account_vision_id));
                            if($pos === false){
                                $DtoAccountVisions->img = '';
                            }else{
                                $DtoAccountVisions->img = $AccountVisionCaptures->img;
                            }
                        } else {
                            $DtoAccountVisions->img = '';
                        }
                    }  
                }
            }
                $DtoAccountVision->imageaccountvision->push($DtoAccountVisions);
        }

            /*  foreach (AccountVisionPhotos::where('account_vision_id', $AccountVision->id)->get() as $AccountVisionCaptures) {
            $DtoAccountVisions = new DtoAccountVision();
            $DtoAccountVisions->img = $AccountVisionCaptures->img;
            $DtoAccountVision->imageaccountvision->push($DtoAccountVisions);
            }     */  
         return $DtoAccountVision;
}


/**
     * Get select
     *
     * @param String $search
     * @return Intervention
     */
    public static function getLastId()
    {

        $LastId = AccountVision::orderBy('id', 'DESC')->first();
        if (isset($LastId->id)) {
            return $LastId->id + 1;
        } else {
            return 1;
        }
    }

    public static function getLastIdContoVisione()
    {

        $LastId = AccountVision::orderBy('id_contovisione', 'DESC')->first();
        if (isset($LastId->id_contovisione)) {
            return $LastId->id_contovisione + 1;
        } else {
            return 1;
        }
    }




 #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request)

    {
        DB::beginTransaction();

        try {

            $AccountVision = new AccountVision();

           if (isset($request->id)) {
              $AccountVision->id = $request->id;
            }
            if (isset($request->id_contovisione)) {
                $AccountVision->id_contovisione = $request->id_contovisione;
              }
            if (isset($request->date)) {
                $AccountVision->date = $request->date;
            }

            if (isset($request->user_id)) {
                $AccountVision->user_id = $request->user_id;
            }
            if (isset($request->description)) {
                $AccountVision->description = $request->description;
            }
            if (isset($request->code_gr)) {
                $AccountVision->code_gr = $request->code_gr;
            }
            if (isset($request->ddt_gr)) {
                $AccountVision->ddt_gr = $request->ddt_gr;
            }
            if (isset($request->note)) {
                $AccountVision->note = $request->note;
            }
            if (isset($request->date_return)) {
                $AccountVision->date_return = $request->date_return;
            }
            if (isset($request->ddt_return)) {
                $AccountVision->ddt_return = $request->ddt_return;
            }
            if (isset($request->note_return)) {
                $AccountVision->note_return = $request->note_return;
            }
             if (isset($request->soll)) {
                $AccountVision->soll = $request->soll;
            }

            $AccountVision->save();

                $cont = 0;
            if (isset($request->imageaccountvision)) {
                foreach ($request->imageaccountvision as $ImageAccounts) {
                    $cont = $cont + 1;
                    Image::make($ImageAccounts)->save(static::$dirImageAccountVisionPhotos . $AccountVision->id . '_' . $cont . '.JPG');
                    $RowAccountVisionPhotos = new AccountVisionPhotos();
                    $RowAccountVisionPhotos->account_vision_id = $AccountVision->id;
                    $RowAccountVisionPhotos->id_cv_librone = $request->id_contovisione;
                    $RowAccountVisionPhotos->img = '/storage/images/AccountVisionPhotos/' . $AccountVision->id . '_' . $cont . '.JPG';
                    $RowAccountVisionPhotos->save();
                }
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $AccountVision->id;
    }

#region DELETE
    /**
     * Delete
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $AccountVision= AccountVision::find($id);
        if (is_null($AccountVision)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }
        if (!is_null($AccountVision)) {
           DB::beginTransaction();
            try {

                AccountVision::where('id', $id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }

    #endregion DELETE

    public static function update(Request $DtoAccountVision)
    {
        $AccountVision = AccountVision::where('id', $DtoAccountVision->id)->first();

        DB::beginTransaction();
        try {
           
                    $AccountVision->id = $DtoAccountVision->id;

                    if (isset($DtoAccountVision->date)) {
                        $AccountVision->date = $DtoAccountVision->date;
                    }else{
                        $AccountVision->date = ''; 
                    }
                    if (isset($DtoAccountVision->id_contovisione)) {
                        $AccountVision->id_contovisione = $DtoAccountVision->id_contovisione;
                    }else{
                        $AccountVision->id_contovisione = ''; 
                    }
        
                    if (isset($DtoAccountVision->ddt_gr)) {
                        $AccountVision->ddt_gr = $DtoAccountVision->ddt_gr;
                    }else{
                        $AccountVision->ddt_gr = ''; 
                    }
                    if (isset($DtoAccountVision->code_gr)) {
                        $AccountVision->code_gr = $DtoAccountVision->code_gr;
                    }else{
                        $AccountVision->code_gr = ''; 
                    }
                    if (isset($DtoAccountVision->user_id)&& $DtoAccountVision->user_id != 'null') {
                        $AccountVision->user_id = $DtoAccountVision->user_id;
                    }else{
                        $AccountVision->user_id = null;
                    }
                    if (isset($DtoAccountVision->description)) {
                        $AccountVision->description = $DtoAccountVision->description;
                    }else{
                        $AccountVision->description = ''; 
                    }
                    if (isset($DtoAccountVision->note)) {
                        $AccountVision->note = $DtoAccountVision->note;
                    }else{
                        $AccountVision->note = ''; 
                    }
                    if (isset($DtoAccountVision->date_return)) {
                        $AccountVision->date_return = $DtoAccountVision->date_return;
                    }else{
                        $AccountVision->date_return = ''; 
                    }
                    if (isset($DtoAccountVision->ddt_return)) {
                        $AccountVision->ddt_return = $DtoAccountVision->ddt_return;
                    }else{
                        $AccountVision->ddt_return = ''; 
                    }
                    if (isset($DtoAccountVision->soll)) {
                        $AccountVision->soll = $DtoAccountVision->soll;
                    }else{
                        $AccountVision->soll = ''; 
                    }
                    if (isset($DtoAccountVision->note_return)) {
                        $AccountVision->note_return = $DtoAccountVision->note_return;
                    }else{
                        $AccountVision->note_return = ''; 
                    }
                    $AccountVision->save();
                 
            $cont = 0;
            if (isset($DtoAccountVision->imageaccountvision)) {
                foreach ($DtoAccountVision->imageaccountvision as $ImageAccounts) {
                    $IdImageAccount = AccountVisionPhotos::where('account_vision_id', $AccountVision->id)->where('img', $ImageAccounts)->first();
                    $cont = $cont + 1;
                    if (!isset($IdImageAccount)) {
                        if (!is_null($ImageAccounts) && $ImageAccounts != '' && $ImageAccounts!= null && $ImageAccounts !='null'){
                            $strPhoto = str_replace('https://www.grsrl.net/storage/','../storage/app/public/', $ImageAccounts);
                            if (!file_exists($strPhoto)) {
                                Image::make($ImageAccounts)->save(static::$dirImageAccountVisionPhotos . $AccountVision->id . '_' . $cont . '.JPG');
                                $RowAccountVisionPhotos = new AccountVisionPhotos();
                                $RowAccountVisionPhotos->account_vision_id = $AccountVision->id;
                                $RowAccountVisionPhotos->id_cv_librone = $DtoAccountVision->id_contovisione;
                                $RowAccountVisionPhotos->img = '/storage/images/AccountVisionPhotos/' . $AccountVision->id . '_' . $cont . '.JPG';
                                $RowAccountVisionPhotos->save();
                            }
                        }
                    }
                }
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

}



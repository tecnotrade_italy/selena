<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoCommunityAction;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\CommunityAction;
use App\CommunityActionType;
use App\CommunityImage;
use App\CommunityImageComment;

use App\UsersDatas;
use App\User;

use App\BusinessLogic\MessageBL;

use App\Mail\DynamicMail;
use Illuminate\Support\Facades\Mail;

use Exception;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;

class CommunityActionBL
{
    #region PRIVATE

    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request dtoCommunityAction
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationsTypesEnum)
    {

       

    }

    /**
     * Convert to dto
     * 
     * @param CommunityAction CommunityAction
     * @return DtoCommunityAction
     */
    private static function _convertToDto($CommunityAction)
    {
        $dtoCommunityAction = new DtoCommunityAction();

        if (isset($CommunityAction->id)) {
            $dtoCommunityAction->id = $CommunityAction->id;
        }

        if (isset($CommunityAction->action_type_id)) {
            $dtoCommunityAction->action_type_id = $CommunityAction->action_type_id;
        }

        if (isset($CommunityAction->user_id)) {
            $dtoCommunityAction->user_id = $CommunityAction->user_id;
        }

        if (isset($CommunityAction->image_id)) {
            $dtoCommunityAction->image_id = $CommunityAction->image_id;
        }

        return $dtoCommunityAction;
    }


    /**
     * Convert to VatType
     * 
     * @param dtoCommunityAction dtoCommunityAction
     * 
     * @return CommunityAction
     */
    private static function _convertToModel($dtoCommunityAction)
    {
        $CommunityAction = new CommunityAction();

        if (isset($dtoCommunityAction->id)) {
            $CommunityAction->id = $dtoCommunityAction->id;
        }

        if (isset($dtoCommunityAction->action_type_id)) {
            $CommunityAction->action_type_id = $dtoCommunityAction->action_type_id;
        }

        if (isset($dtoCommunityAction->user_id)) {
            $CommunityAction->user_id = $dtoCommunityAction->user_id;
        }

        if (isset($dtoCommunityAction->image_id)) {
            $CommunityAction->image_id = $dtoCommunityAction->image_id;
        }

        return $CommunityAction;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCommunityction
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(CommunityAction::find($id));
    }

    /*
     * Get all
     * 
     * @return DtoCommunityAction
     */
    public static function getAll()
    {

        $result = collect(); 

        foreach (CommunityAction::orderBy('id','DESC')->get() as $communityAction) {

            $DtoCommunityAction = new DtoCommunityAction();

            $DtoCommunityAction->id = $communityAction->id;
            $DtoCommunityAction->action_type_id = $communityAction->action_type_id;
            $DtoCommunityAction->user_id = $communityAction->user_id;
            $DtoCommunityAction->image_id = $communityAction->image_id;
            
            $result->push($DtoCommunityAction); 
        }
        return $result;

    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoCommunityAction
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request)
    {
        $return = 0;
        if(isset($request->userId) && isset($request->rate) && isset($request->idImage)){

            $idAction = CommunityActionType::where('code', 'STAR' . $request->rate)->get()->first();
            if(isset($idAction)){
                $communityActionOnDB = DB::select("SELECT count(id) as cont 
                    FROM community_actions 
                    WHERE user_id = " . $request->userId . " AND image_id = " . $request->idImage . " AND 
                    (action_type_id = 1 or action_type_id = 2 or action_type_id = 3 or action_type_id = 4 or action_type_id = 5) 
                    limit 1");
                
                if($communityActionOnDB[0]->cont == 0){
                    $actionIdReturn = CommunityActionBL::addAction($request->userId, $request->idImage, $idAction->id);
                    $actualRate = CommunityActionBL::addOrRemoveRateByActionId($actionIdReturn, true);

                    CommunityActionBL::sendNotificationByActionId($actionIdReturn, 0);
                }
                
                $avgRate = 0;

                $avgRateNumber = DB::select("SELECT AVG(CAST(replace(community_actions_types.code, 'STAR', '') as INTEGER)) as rate
                    FROM community_actions
                    INNER JOIN community_actions_types ON community_actions.action_type_id = community_actions_types.id
                    INNER JOIN community_images ON community_actions.image_id = community_images.id
                    WHERE 
                    (community_actions_types.code = 'STAR1' 
                    OR community_actions_types.code = 'STAR2' 
                    OR community_actions_types.code = 'STAR3' 
                    OR community_actions_types.code = 'STAR4' 
                    OR community_actions_types.code = 'STAR5') AND community_images.id = '" . $request->idImage . "'");
                
                
                $avgRate = $avgRateNumber[0]->rate;

                $communityImage = CommunityImage::where('id', $request->idImage)->first();
                $communityImage->star_rating = $avgRate;
                $communityImage->save();
                    
                $return = str_replace('.',',',substr($avgRate, 0, -3));
                
            }
        }

        return $return;
    }

    /**
     * Insert action
     * 
     * @param Request DtoCommunityAction
     * @param int idLanguage
     * 
     * @return int
     */
    public static function addAction(int $idUser, int $idImage, int $idAction){
        
        $communityAction = new CommunityAction();
        $communityAction->user_id = $idUser;
        $communityAction->image_id = $idImage;
        $communityAction->action_type_id = $idAction;
        $communityAction->save();

        return $communityAction->id;
       
    }

    /**
     * Insert action
     * 
     * @param int idAction
     * 
     * @return int
     */
    public static function sendNotificationByActionId(int $idAction, int $idComment)
    {
        $communityAction = CommunityAction::find($idAction);
        $communityImage = CommunityImage::where('id', $communityAction->image_id)->get()->first();
        $protocol = "";

        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        $url = $protocol . $_SERVER["HTTP_HOST"];

        if(isset($communityAction)){
            if(isset($communityImage)){
                $userIdFromAction = $communityAction->user_id;
                $userIdToAction = $communityImage->user_id;
                $userDatas = UsersDatas::where('user_id', $userIdToAction)->get()->first();
                $userDatasFrom = UsersDatas::where('user_id', $userIdFromAction)->get()->first();
                if(isset($userDatas)){
                    $users = User::where('id', $userIdToAction)->get()->first();
                    $checkSendNotificationStarRates = $userDatas->community_send_notification_star_rates;
                    $checkSendNotificationComments = $userDatas->community_send_notification_comments;
                    $checkSendNotificationFavorites = $userDatas->community_send_notification_favorites;

                    if($idComment > 0){
                        //invio notifica commento tramite id commento
                        //invio email commento
                        $imageComment = CommunityImageComment::where('id', $idComment)->get()->first();
                        if(isset($imageComment)){
                            if(is_null($imageComment->comment_id_response)){
                                if($checkSendNotificationComments){
                                    $textObject= MessageBL::getByCode('COMMUNITY_COMMENT_SUBJECT'); 
                                    $textMail= MessageBL::getByCode('COMMUNITY_COMMENT_TEXT');
                                    $textMail = str_replace('#user_from_action#', $userDatasFrom->name . ' ' . $userDatasFrom->surname . ' ha commentato sotto una tua foto!', $textMail);
                                    $textMail = str_replace('#link#', $url . $communityImage->link, $textMail);
                                    $textMail = str_replace('#url#', $communityImage->img, $textMail);

                                    Mail::to($users->email)->send(new DynamicMail($textObject, $textMail));
                                }
                            }else{
                                $imageCommentResponse = CommunityImageComment::where('id', $imageComment->comment_id_response)->get()->first();
                                if(isset($imageCommentResponse)){
                                    $usersToResponseComment = User::where('id', $imageCommentResponse->user_id)->get()->first();
                                    if(isset($usersToResponseComment)){
                                        $userDatasToResponseComment = UsersDatas::where('user_id', $usersToResponseComment->id)->get()->first();
                                        if(isset($userDatasToResponseComment)){
                                            $checkSendNotificationCommentsToResponse = $userDatasToResponseComment->community_send_notification_comments;
                                            if($checkSendNotificationCommentsToResponse){

                                                $textObject= MessageBL::getByCode('COMMUNITY_COMMENT_SUBJECT'); 
                                                $textMail= MessageBL::getByCode('COMMUNITY_COMMENT_TEXT');
                                                $textMail = str_replace('#user_from_action#', $userDatasFrom->name . ' ' . $userDatasFrom->surname . ' ha risposto ad un tuo commento!', $textMail);
                                                $textMail = str_replace('#link#', $url . $communityImage->link, $textMail);
                                                $textMail = str_replace('#url#', $communityImage->img, $textMail);

                                                Mail::to($usersToResponseComment->email)->send(new DynamicMail($textObject, $textMail));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        $typeAction = CommunityActionType::where('id', $communityAction->action_type_id)->get()->first();
                        if(isset($typeAction)){
                            if($typeAction->code == 'STAR1' || $typeAction->code == 'STAR2' || $typeAction->code == 'STAR3' || $typeAction->code == 'STAR4' || $typeAction->code == 'STAR5'){
                                //è un rating
                                if($checkSendNotificationStarRates){
                                    //invio email rating
                                    
                                    $rate = 0;
                                    switch ($typeAction->code) {
                                        case 'STAR1':
                                            $rate = "1 Stella";
                                            break;
                                        case 'STAR2':
                                            $rate = "2 Stelle";
                                            break;
                                        case 'STAR3':
                                            $rate = "3 Stelle";
                                            break;
                                        case 'STAR4':
                                            $rate = "4 Stelle";
                                            break;
                                        case 'STAR5':
                                            $rate = "5 Stelle";
                                            break;
                                    }

                                    $textObject= MessageBL::getByCode('COMMUNITY_STAR_RATING_SUBJECT'); 
                                    $textMail= MessageBL::getByCode('COMMUNITY_STAR_RATING_TEXT');
                                    $textMail = str_replace('#user_from_action#', $userDatasFrom->name . ' ' . $userDatasFrom->surname, $textMail);
                                    $textMail = str_replace('#rate#', $rate, $textMail);
                                    $textMail = str_replace('#link#', $url . $communityImage->link, $textMail);
                                    $textMail = str_replace('#url#', $communityImage->img, $textMail);
                                    
                                    Mail::to($users->email)->send(new DynamicMail($textObject, $textMail));
                                }
                            }else{
                                //è un commento
                                if($typeAction->code == 'ADDIMAGETOFAVORITE'){
                                    if($checkSendNotificationFavorites){
                                        //invio email preferiti
                                        $textObject= MessageBL::getByCode('COMMUNITY_FAVORITES_SUBJECT'); 
                                        $textMail= MessageBL::getByCode('COMMUNITY_FAVORITES_TEXT');
                                        $textMail = str_replace('#user_from_action#', $userDatasFrom->name . ' ' . $userDatasFrom->surname, $textMail);
                                        $textMail = str_replace('#link#', $url . $communityImage->link, $textMail);
                                        $textMail = str_replace('#url#', $communityImage->img, $textMail);
                                        
                                        Mail::to($users->email)->send(new DynamicMail($textObject, $textMail));

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }        
    }
    

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoCommunityAction
     * @param int idLanguage
     */
    public static function update(Request $DtoCommunityAction)
    {
        static::_validateData($DtoCommunityAction, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $CommunityAction = CommunityAction::find($DtoCommunityAction->id);
        DBHelper::updateAndSave(static::_convertToModel($DtoCommunityAction), $CommunityAction);
    }

    /**
     * add or remove value of community_type_action from community_images -> rating
     * 
     * @param int id community_action
     * @param bool add
     */
    public static function addOrRemoveRateByActionId(int $idCommunityAction, $toAdd)
    {
        $return = "";
        if(isset($idCommunityAction)){
            $communityActionOnDB = CommunityAction::find($idCommunityAction);
            if(isset($communityActionOnDB)){
                $idCommunityImage = $communityActionOnDB->image_id;
                $communityActionTypeOnDB = CommunityActionType::where('id', $communityActionOnDB->action_type_id)->get()->first();
                if(isset($communityActionTypeOnDB)){
                    $communityImageOnDb = CommunityImage::where('id', $idCommunityImage)->get()->first();
                    if(isset($communityActionTypeOnDB)){
                        $rating = 0;
                        if(!is_null($communityImageOnDb->rating)){
                            $rating = $communityImageOnDb->rating;
                        }

                        $valueRate = $communityActionTypeOnDB->rating_value;

                        if($toAdd){
                            if($valueRate < 0){
                                $valueRate = $valueRate * -1;
                                $communityImageOnDb->rating = $rating - $valueRate;
                            }else{
                                $communityImageOnDb->rating = $rating + $valueRate;
                            }
                            
                            $communityImageOnDb->save();
                            $return = "Rating aggiunto correttamente";
                        }else{
                            

                            if($rating - $valueRate > 0){
                                $communityImageOnDb->rating = $rating - $valueRate;
                            }else{
                                $communityImageOnDb->rating = 0;
                            }

                            $communityImageOnDb->save();

                            $return = "Rating rimosso correttamente";
                        }
                    }
                }
            }
        }

        return $return;
    }

    

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id)
    {
        $CommunityAction = CommunityAction::find($id);

        if (is_null($CommunityAction)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $CommunityAction->delete();
    }

    #endregion DELETE
}

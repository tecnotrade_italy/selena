<?php

namespace App\BusinessLogic;

use App\DocumentRowLanguage;
use App\Helpers\LogHelper;

class DocumentRowLanguageBL
{
    #region GET

    /**
     * Get description by idDocumentRow and idLanguage
     * 
     * @param int $idDocumentRow
     * @param int $idLanguage
     * 
     * @return string
     */
    public static function getByDocumentRowAndLanguage(int $idDocumentRow, int $idLanguage)
    {
        $documentRowLanguage = DocumentRowLanguage::where('document_row_id', $idDocumentRow)->where('language_id', $idLanguage)->first();
        if (is_null($documentRowLanguage)) {
            return '';
        } else {
            return $documentRowLanguage->description;
        }
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param int idDocumentRow
     * @param int idItem
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert($idDocumentRow, $idItem, $idLanguage)
    {
        $documentRowLanguage = new DocumentRowLanguage();
        $documentRowLanguage->document_row_id = $idDocumentRow;
        $documentRowLanguage->language_id = $idLanguage;
        $documentRowLanguage->description = ItemLanguageBL::getByItemAndLanguage($idItem, $idLanguage);
        $documentRowLanguage->save();

        return $documentRowLanguage->id;
    }

    #region INSERT
}

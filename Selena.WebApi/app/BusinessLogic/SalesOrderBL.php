<?php

namespace App\BusinessLogic;

use App\CarriageLanguage;
use App\CarriagePaidLanguage;
use App\Carrier;
use App\CartRulesApplied;
use App\Content;
use App\ContentLanguage;
use App\DocumentStatus;
use App\DocumentStatusLanguage;
use App\DtoModel\DtoCartRulesApplied;
use App\DtoModel\DtoOrderMessages;
use App\DtoModel\DtoSalesOrder;
use App\DtoModel\DtoSalesOrderDetail;
use App\DtoModel\DtoSalesOrderDetailOptional;
use App\DtoModel\DtoSalesOrderDetailUploadFile;
use App\DtoModel\DtoUser;
use App\DtoModel\DtoUserAddress;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Item;
use App\ItemLanguage;
use App\LanguagePaymentType;
use App\LanguageProvince;
use App\Mail\DynamicMail;
use App\OptionalLanguages;
use App\OptionalTypology;
use App\OrderMessages;
use App\Payment;
use App\Province;
use App\Provinces;
use App\SalesOrder;
use App\SalesOrderDetail;
use App\SalesOrderDetailOptional;
use App\SalesOrderDetailUploadFile;
use App\SalesOrderStateLanguage;
use App\Supports;
use App\TypeOptionalLanguages;
use App\UnitMeasure;
use App\User;
use App\UserAddress;
use App\UsersAddresses;
use App\UsersDatas;
use App\VatType;
use Carbon\Carbon;
use CreateFileSalesOrdersDetails;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class SalesOrderBL
{
    public static $dirImage = '../storage/app/public/images/File/';

    #region PRIVATE

    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request dtoSalesOrder
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $dtoSalesOrder, $dbOperationType)
    {
        
    }

    /**
     * Convert to dto
     * 
     * @param SalesOrder SalesOrder
     * @return DtoSalesOrder
     */
    private static function _convertToDto($SalesOrder)
    {
        $dtoSalesOrder = new DtoSalesOrder();

        if (isset($SalesOrder->id)) {
            $dtoSalesOrder->id = $SalesOrder->id;
        }

        if (isset($SalesOrder->order_number)) {
            $dtoSalesOrder->order_number = $SalesOrder->order_number;
        }

        if (isset($SalesOrder->date_order)) {
            $dtoSalesOrder->date_order = $SalesOrder->date_order;
        }

        if (isset($SalesOrder->order_state_id)) {
            $dtoSalesOrder->order_state_id = $SalesOrder->order_state_id;
        }

        if (isset($SalesOrder->user_id)) {
            $dtoSalesOrder->user_id = $SalesOrder->user_id;
        }

        if (isset($SalesOrder->currency_id)) {
            $dtoSalesOrder->currency_id = $SalesOrder->currency_id;
        }

        if (isset($SalesOrder->payment_id)) {
            $dtoSalesOrder->payment_id = $SalesOrder->payment_id;
        }        

        if (isset($SalesOrder->net_amount)) {
            $dtoSalesOrder->net_amount = $SalesOrder->net_amount;
        }

        if (isset($SalesOrder->vat_amount)) {
            $dtoSalesOrder->vat_amount = $SalesOrder->vat_amount;
        }

        if (isset($SalesOrder->shipment_amount)) {
            $dtoSalesOrder->shipment_amount = $SalesOrder->shipment_amount;
        }

        if (isset($SalesOrder->payment_cost)) {
            $dtoSalesOrder->payment_cost = $SalesOrder->payment_cost;
        }

        if (isset($SalesOrder->service_amount)) {
            $dtoSalesOrder->service_amount = $SalesOrder->service_amount;
        }      

        if (isset($SalesOrder->discount_percentage)) {
            $dtoSalesOrder->discount_percentage = $SalesOrder->discount_percentage;
        }

        if (isset($SalesOrder->discount_value)) {
            $dtoSalesOrder->discount_value = $SalesOrder->discount_value;
        }

        if (isset($SalesOrder->total_amount)) {
            $dtoSalesOrder->total_amount = $SalesOrder->total_amount;
        }      

        if (isset($SalesOrder->note)) {
            $dtoSalesOrder->note = $SalesOrder->note;
        }

        if (isset($SalesOrder->affiliate_id)) {
            $dtoSalesOrder->affiliate_id = $SalesOrder->affiliate_id;
        }

        if (isset($SalesOrder->carrier_id)) {
            $dtoSalesOrder->carrier_id = $SalesOrder->carrier_id;
        }      

        if (isset($SalesOrder->user_address_goods_id)) {
            $dtoSalesOrder->user_address_goods_id = $SalesOrder->user_address_goods_id;
        }

        if (isset($SalesOrder->user_address_documents_id)) {
            $dtoSalesOrder->user_address_documents_id = $SalesOrder->user_address_documents_id;
        }

        if (isset($SalesOrder->carriage_paid_to_id)) {
            $dtoSalesOrder->carriage_paid_to_id = $SalesOrder->carriage_paid_to_id;
        }

        if (isset($SalesOrder->fulfillment_date)) {
            $dtoSalesOrder->fulfillment_date = $SalesOrder->fulfillment_date;
        }      

        if (isset($SalesOrder->to_be_updated)) {
            $dtoSalesOrder->to_be_updated = $SalesOrder->to_be_updated;
        }

        if (isset($SalesOrder->updated)) {
            $dtoSalesOrder->updated = $SalesOrder->updated;
        }

        return $dtoSalesOrder;

    }

    /**
     * Convert to SalesOrder
     * 
     * @param DtoSalesOrder dtoSalesOrder
     * 
     * @return SalesOrder
     */
    private static function _convertToModel($dtoSalesOrder)
    {
        $SalesOrder = new SalesOrder();

        if (isset($dtoSalesOrder->id)) {
            $SalesOrder->id = $dtoSalesOrder->id;
        }

        if (isset($dtoSalesOrder->order_number)) {
            $SalesOrder->order_number = $dtoSalesOrder->order_number;
        }

        if (isset($dtoSalesOrder->date_order)) {
            $SalesOrder->date_order = $dtoSalesOrder->date_order;
        }

        if (isset($dtoSalesOrder->order_state_id)) {
            $SalesOrder->order_state_id = $dtoSalesOrder->order_state_id;
        }

        if (isset($dtoSalesOrder->user_id)) {
            $SalesOrder->user_id = $dtoSalesOrder->user_id;
        }       
        
        if (isset($dtoSalesOrder->currency_id)) {
            $SalesOrder->currency_id = $dtoSalesOrder->currency_id;
        }

        if (isset($dtoSalesOrder->payment_id)) {
            $SalesOrder->payment_id = $dtoSalesOrder->payment_id;
        }

        if (isset($dtoSalesOrder->net_amount)) {
            $SalesOrder->net_amount = $dtoSalesOrder->net_amount;
        }

        if (isset($dtoSalesOrder->vat_amount)) {
            $SalesOrder->vat_amount = $dtoSalesOrder->vat_amount;
        }

        if (isset($dtoSalesOrder->shipment_amount)) {
            $SalesOrder->shipment_amount = $dtoSalesOrder->shipment_amount;
        }          

        if (isset($dtoSalesOrder->payment_cost)) {
            $SalesOrder->payment_cost = $dtoSalesOrder->payment_cost;
        }

        if (isset($dtoSalesOrder->service_amount)) {
            $SalesOrder->service_amount = $dtoSalesOrder->service_amount;
        }

        if (isset($dtoSalesOrder->discount_percentage)) {
            $SalesOrder->discount_percentage = $dtoSalesOrder->discount_percentage;
        }         

        if (isset($dtoSalesOrder->discount_value)) {
            $SalesOrder->discount_value = $dtoSalesOrder->discount_value;
        }     

        if (isset($dtoSalesOrder->total_amount)) {
            $SalesOrder->total_amount = $dtoSalesOrder->total_amount;
        }     

        if (isset($dtoSalesOrder->note)) {
            $SalesOrder->note = $dtoSalesOrder->note;
        }         

        if (isset($dtoSalesOrder->affiliate_id)) {
            $SalesOrder->affiliate_id = $dtoSalesOrder->affiliate_id;
        }     

        if (isset($dtoSalesOrder->carrier_id)) {
            $SalesOrder->carrier_id = $dtoSalesOrder->carrier_id;
        }     

        if (isset($dtoSalesOrder->user_address_goods_id)) {
            $SalesOrder->user_address_goods_id = $dtoSalesOrder->user_address_goods_id;
        }         

        if (isset($dtoSalesOrder->user_address_documents_id)) {
            $SalesOrder->user_address_documents_id = $dtoSalesOrder->user_address_documents_id;
        }     

        if (isset($dtoSalesOrder->carriage_paid_to_id)) {
            $SalesOrder->carriage_paid_to_id = $dtoSalesOrder->carriage_paid_to_id;
        }    

        if (isset($dtoSalesOrder->fulfillment_date)) {
            $SalesOrder->fulfillment_date = $dtoSalesOrder->fulfillment_date;
        }         

        if (isset($dtoSalesOrder->to_be_updated)) {
            $SalesOrder->to_be_updated = $dtoSalesOrder->to_be_updated;
        }     

        if (isset($dtoSalesOrder->updated)) {
            $SalesOrder->updated = $dtoSalesOrder->updated;
        }    
        return $SalesOrder;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoSalesOrder
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(SalesOrder::find($id));
    }

    /**
     * Get all
     * 
     * @return DtoSalesOrder
     */
    public static function getAll()
    {
        return SalesOrder::all()->map(function ($SalesOrder) {
            return static::_convertToDto($SalesOrder);
        });
    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request dtoSalesOrder
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $dtoSalesOrder)
    {
        static::_validateData($dtoSalesOrder, DbOperationsTypesEnum::INSERT);
        $SalesOrder = static::_convertToModel($dtoSalesOrder);
        $SalesOrder->save();
        return $SalesOrder->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoSalesOrder
     * @param int idLanguage
     */
    public static function update(Request $dtoSalesOrder)
    {
        static::_validateData($dtoSalesOrder, DbOperationsTypesEnum::UPDATE);
        $SalesOrder = SalesOrder::find($dtoSalesOrder->id);
        DBHelper::updateAndSave(static::_convertToModel($dtoSalesOrder), $SalesOrder);
    }

    #endregion UPDATE

   


 
    #endregion PRIVATE


 #region getAllBySales
    /**
     * getAllBySales
     *
     * @param int $id
     */
    public static function getAllBySales(int $id, $idLanguage)
    {
      
        $salesOrder = SalesOrder::where('user_id', $id)->get();
        $finalHtml = "";
        $idContent = Content::where('code', 'ListSalesOrder')->get()->first();
        $contentLanguage = ContentLanguage::where('content_id', $idContent->id)->where('language_id', $idLanguage)->get()->first();

        $tags = SelenaViewsBL::getTagByType('ListSalesOrder');
        foreach (DB::select(
            'SELECT *
            FROM sales_orders 
            WHERE user_id = ' . $id .' ORDER BY sales_orders.date_order DESC'
            ) as $salesorders){
            $newContent = $contentLanguage->content;  
            foreach ($tags as $fieldsDb){
                if(isset($fieldsDb->tag)){                                                              
                    if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                        if(is_numeric($salesorders->{$cleanTag}) && strpos($salesorders->{$cleanTag},'.')){
                            $tag = str_replace('.',',',substr($salesorders->{$cleanTag}, 0, -2));
                        }else{
                            $tag = $salesorders->{$cleanTag};
                        }
                        $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                               
                    }
                }
            }
            if (strpos($newContent, '#order-state#') !== false) {
                $descriptionStates = DocumentStatusLanguage::where('document_status_id', $salesorders->order_state_id)->first();   
                if (isset($descriptionStates)){
                    $newContent = str_replace('#order-state#', $descriptionStates->description,  $newContent); 
                }else{
                    $newContent = str_replace('#order-state#', '',  $newContent);           
                }
            }

            if (strpos($newContent, '#description_payment#') !== false) {
                $descriptionStates = Payment::where('id', $salesorders->payment_id)->first();   
                if (isset($descriptionStates)){
                    $newContent = str_replace('#description_payment#', $descriptionStates->payment_status,  $newContent); 
                }else{
                    $newContent = str_replace('#description_payment#', '',  $newContent);           
                }
            }


              if (strpos($newContent, '#date_order_formatter#') !== false) {
                $dateorderformatter = SalesOrder::where('id', $salesorders->id)->first()->date_order;   
                $dateorderformatters = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $dateorderformatter)->format('d/m/Y H:i:s');
                // Carbon::parse($dateorderformatter)->format('d/m/Y H:i:s');               
                //Carbon::parse($dateorderformatter)->format(HttpHelper::getLanguageDateTimeFormat());
                if (isset($dateorderformatters)){     
                    $newContent = str_replace('#date_order_formatter#',$dateorderformatters,  $newContent); 
                }else{
                    $newContent = str_replace('#date_order_formatter#', '',  $newContent);           
                }
            }

            $finalHtml = $finalHtml . $newContent;
          
        }; 

        //$finalHtml...
                       
        return $finalHtml;

    }



     /**
     * get All orders By Salesman Id
     *
     * @param int $id
     */
    public static function getAllBySalesmanId(Request $request)
    {
        $salesOrder = SalesOrder::where('salesman_id', $request->salesmanId)->get();
        $finalHtml = "";
        $idContent = Content::where('code', 'ListSalesOrder')->get()->first();
        $contentLanguage = ContentLanguage::where('content_id', $idContent->id)->where('language_id', HttpHelper::getLanguageId())->get()->first();


        $strWhere = "";

        if ($request->order_code != "") {
            if($strWhere == ''){
                $strWhere = ' AND sales_orders.order_number ilike \'%' . $request->order_code . '%\'';
            }else{
                $strWhere = $strWhere . ' AND sales_orders.order_number ilike \'%' . $request->order_code . '%\'';
            } 
        }

        if ($request->business_name != "") {
            if($strWhere == ''){
                $strWhere = ' AND users_datas.business_name ilike \'%' . $request->business_name . '%\'';
            }else{
                $strWhere = $strWhere . ' AND users_datas.business_name ilike \'%' . $request->business_name . '%\'';
            } 
        }

        if ($request->user_code != "") {
            if($strWhere == ''){
                $strWhere = ' AND users.code ilike \'%' . $request->user_code . '%\'';
            }else{
                $strWhere = $strWhere . ' AND users.code ilike \'%' . $request->user_code . '%\'';
            } 
        }

        if ($request->date_from != "") {
            if($strWhere == ''){
                $strWhere = ' AND sales_orders.date_order >= \'' . Carbon::createFromFormat(HttpHelper::getLanguageDateFormat(), $request->date_from)->format('m/d/Y') . '\'';
            }else{
                $strWhere = $strWhere . ' AND sales_orders.date_order >= \'' . Carbon::createFromFormat(HttpHelper::getLanguageDateFormat(), $request->date_from)->format('m/d/Y') . '\'';
            } 
        }
        //Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $request->date_to)->format('Y/m/d H:i:s')
        if ($request->date_to != "") {
            if($strWhere == ''){
                $strWhere = ' AND sales_orders.date_order <= \'' . Carbon::createFromFormat(HttpHelper::getLanguageDateFormat(), $request->date_to)->format('m/d/Y') . '\'';
            }else{
                $strWhere = $strWhere . ' AND sales_orders.date_order <= \'' . Carbon::createFromFormat(HttpHelper::getLanguageDateFormat(), $request->date_to)->format('m/d/Y') . '\'';
            } 
        }      


        $tags = SelenaViewsBL::getTagByType('ListSalesOrder');
        foreach (DB::select(
            'SELECT sales_orders.*, users_datas.business_name, users.code as user_code
            FROM sales_orders 
            INNER JOIN users ON sales_orders.user_id = users.id
            INNER JOIN users_datas ON sales_orders.user_id = users_datas.user_id
            WHERE sales_orders.order_number is not null and salesman_id = ' . $request->salesmanId . $strWhere . ' ORDER BY sales_orders.date_order DESC'
            ) as $salesorders){
            $newContent = $contentLanguage->content;  
            foreach ($tags as $fieldsDb){
                if(isset($fieldsDb->tag)){                                                              
                    if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                        if(is_numeric($salesorders->{$cleanTag}) && strpos($salesorders->{$cleanTag},'.')){
                            $tag = str_replace('.',',',substr($salesorders->{$cleanTag}, 0, -2));
                        }else{
                            $tag = $salesorders->{$cleanTag};
                        }
                        $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                               
                    }
                }
            }
            if (strpos($newContent, '#order-state#') !== false) {
                $descriptionStates = DocumentStatusLanguage::where('document_status_id', $salesorders->order_state_id)->first();   
                if (isset($descriptionStates)){
                    $newContent = str_replace('#order-state#', $descriptionStates->description,  $newContent); 
                }else{
                    $newContent = str_replace('#order-state#', '',  $newContent);           
                }
            }

            if (strpos($newContent, '#business_name#') !== false) {
                $userDatas = UsersDatas::where('user_id', $salesorders->user_id)->first();  
                if (isset($userDatas)){
                    $newContent = str_replace('#business_name#', $userDatas->business_name,  $newContent); 
                }else{
                    $newContent = str_replace('#business_name#', '',  $newContent);           
                }
            }
            if (strpos($newContent, '#user_code#') !== false) {
                $users = User::where('id', $salesorders->user_id)->first();  
                if (isset($users)){
                    $newContent = str_replace('#user_code#', $users->code,  $newContent); 
                }else{
                    $newContent = str_replace('#user_code#', '',  $newContent);           
                }
            }

            if (strpos($newContent, '#description_payment#') !== false) {
                $descriptionStates = Payment::where('id', $salesorders->payment_id)->first();   
                if (isset($descriptionStates)){
                    $newContent = str_replace('#description_payment#', $descriptionStates->payment_status,  $newContent); 
                }else{
                    $newContent = str_replace('#description_payment#', '',  $newContent);           
                }
            }


              if (strpos($newContent, '#date_order_formatter#') !== false) {
                $dateorderformatter = SalesOrder::where('id', $salesorders->id)->first()->date_order;   
                $dateorderformatters = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $dateorderformatter)->format('d/m/Y H:i:s');
                // Carbon::parse($dateorderformatter)->format('d/m/Y H:i:s');               
                //Carbon::parse($dateorderformatter)->format(HttpHelper::getLanguageDateTimeFormat());
                if (isset($dateorderformatters)){     
                    $newContent = str_replace('#date_order_formatter#',$dateorderformatters,  $newContent); 
                }else{
                    $newContent = str_replace('#date_order_formatter#', '',  $newContent);           
                }
            }

            $finalHtml = $finalHtml . $newContent;
          
        }; 

        //$finalHtml...
                       
        return $finalHtml;

    }


 #region getByDetailSaleOrder
    /**
     * getByDetailSaleOrder
     *
     * @param int $id
     */
    public static function getByDetailSaleOrder(int $id, $idLanguage)
    {
        $salesOrder = SalesOrder::where('id', $id)->get()->first();
        $finalHtml = "";
        $idContent = Content::where('code', 'DetailSalesOrders')->get()->first();
         
        $contentLanguage = ContentLanguage::where('content_id', $idContent->id)->where('language_id', $idLanguage)->get()->first();    
         
        if (strpos($contentLanguage->content, '#order_user_data#') !== false) {
            $idContentUser = Content::where('code', 'DetailSalesOrderUserData')->get()->first();
            $contentLanguageUser = ContentLanguage::where('content_id', $idContentUser->id)->where('language_id', $idLanguage)->get()->first();    
            $tagsUserDatas = SelenaViewsBL::getTagByType('UserDetail');
            
            $user = UsersDatas::where('user_id', $salesOrder->user_id)->first();  
            $finalHtmlUserDatas= "";

              
            if (isset($user)){
                foreach (DB::select(
                'SELECT *
                FROM users_datas 
                WHERE user_id = ' . $salesOrder->user_id
                ) as $salesordersUsersDetail){
                    $newContentUserDatas = $contentLanguageUser->content;  
                    foreach ($tagsUserDatas as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                              
                            if (strpos($contentLanguageUser->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($salesordersUsersDetail->{$cleanTag}) && strpos($salesordersUsersDetail->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($salesordersUsersDetail->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $salesordersUsersDetail->{$cleanTag};
                                }
                                $newContentUserDatas = str_replace($fieldsDb->tag , $tag,  $newContentUserDatas);                                                                               
                            }
                        }
                    }

                    if (strpos($newContentUserDatas, '#provinces#') !== false) {
                        $descriptionProvince = Province::where('id', $salesordersUsersDetail->province_id)->first();   
                        if (isset($descriptionProvince)){
                            $newContentUserDatas = str_replace('#provinces#', $descriptionProvince->abbreviation,  $newContentUserDatas); 
                        }else{
                            $newContentUserDatas = str_replace('#provinces#', '',  $newContentUserDatas);           
                        }
                    }
                    if (strpos($newContentUserDatas, '#email#') !== false) {
                        $Email = User::where('id', $salesordersUsersDetail->user_id)->first();   
                        if (isset($Email)){
                            $newContentUserDatas = str_replace('#email#', $Email->email,  $newContentUserDatas); 
                        }else{
                            $newContentUserDatas = str_replace('#email#', '',  $newContentUserDatas);           
                        }
                    }
                
                    $finalHtmlUserDatas = $finalHtmlUserDatas . $newContentUserDatas;
                }; 
            }
            $finalHtml = str_replace('#order_user_data#', $finalHtmlUserDatas,  $contentLanguage->content); 
        }


       //#order-goods-shipment-data# 
     if (strpos($contentLanguage->content, '#order-goods-shipment-data#') !== false) {
            $idContentUser = Content::where('code', 'DetailGoodShipmentData')->get()->first();
            $contentLanguageUser = ContentLanguage::where('content_id', $idContentUser->id)->where('language_id', $idLanguage)->get()->first();    
            $tagsUserDatas = SelenaViewsBL::getTagByType('DetailShipmentData');
            $user = UsersAddresses::where('id', $salesOrder->user_address_goods_id)->first();   
            $finalHtmlUserDatas= "";
            if (isset($user)){

                foreach (DB::select(
                    'SELECT *
                    FROM users_addresses
                    WHERE id = ' . $salesOrder->user_address_goods_id
                    ) as $salesordersUsersDetail){
                    $newContentUserDatas = $contentLanguageUser->content;  
                    foreach ($tagsUserDatas as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                              
                            if (strpos($contentLanguageUser->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($salesordersUsersDetail->{$cleanTag}) && strpos($salesordersUsersDetail->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($salesordersUsersDetail->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $salesordersUsersDetail->{$cleanTag};
                                }
                                $newContentUserDatas = str_replace($fieldsDb->tag , $tag,  $newContentUserDatas);                                                                               
                            }
                        }
                    }

            if (strpos($newContentUserDatas, '#province#') !== false) {
                $descriptionProvince = Province::where('id', $salesordersUsersDetail->province_id)->first();   
                if (isset($descriptionProvince)){
                    $newContentUserDatas = str_replace('#province#', $descriptionProvince->abbreviation,  $newContentUserDatas); 
                }else{
                    $newContentUserDatas = str_replace('#province#', '',  $newContentUserDatas);           
                }
            }
                    $finalHtmlUserDatas = $finalHtmlUserDatas . $newContentUserDatas;
                }; 
            }
             $finalHtml = str_replace('#order-goods-shipment-data#' , $finalHtmlUserDatas,  $finalHtml); 
        }




        #order-data-shipping-documents#
          if (strpos($contentLanguage->content, '#order-data-shipping-documents#') !== false) {
            $idContentShippingDocument= Content::where('code', 'OrderDataShippingDocuments')->get()->first();
            $contentLanguageUser = ContentLanguage::where('content_id', $idContentShippingDocument->id)->where('language_id', $idLanguage)->get()->first();    
            $tagsUserDatas = SelenaViewsBL::getTagByType('OrderDataShippingDocuments');
            $OrderDataShippingDocuments = UsersAddresses::where('id', $salesOrder->user_address_documents_id)->first();   
            $finalHtmlUserDatas= "";
            if (isset($OrderDataShippingDocuments)){

                foreach (DB::select(
                    'SELECT *
                    FROM users_addresses
                    WHERE id = ' . $salesOrder->user_address_documents_id
                    ) as $salesOrderDataShippingDocuments){
                    $newContentDataShippingDocuments = $contentLanguageUser->content;  
                    foreach ($tagsUserDatas as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                              
                            if (strpos($contentLanguageUser->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($salesOrderDataShippingDocuments->{$cleanTag}) && strpos($salesOrderDataShippingDocuments->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($salesOrderDataShippingDocuments->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $salesOrderDataShippingDocuments->{$cleanTag};
                                }
                                $newContentDataShippingDocuments = str_replace($fieldsDb->tag , $tag,  $newContentDataShippingDocuments);                                                                               
                            }
                        }
                    }

                    if (strpos($newContentDataShippingDocuments, '#province#') !== false) {
                $descriptionProvince = Province::where('id', $salesOrderDataShippingDocuments->province_id)->first();   
                if (isset($descriptionProvince)){
                   $newContentDataShippingDocuments = str_replace('#province#', $descriptionProvince->abbreviation,  $newContentDataShippingDocuments); 
                }else{
                    $newContentDataShippingDocuments = str_replace('#province#', '',  $newContentDataShippingDocuments);           
                }
            }
                    $finalHtmlUserDatas = $finalHtmlUserDatas . $newContentDataShippingDocuments;
                }; 
            }
             $finalHtml = str_replace('#order-data-shipping-documents#' , $finalHtmlUserDatas,  $finalHtml); 
        }

        

         #summary-description-document#
          if (strpos($contentLanguage->content, '#summary-description-document#') !== false) {
            $SummaryDescriptionDocument= Content::where('code', 'SummaryDescriptionDocument')->get()->first();
            $contentLanguageUser = ContentLanguage::where('content_id', $SummaryDescriptionDocument->id)->where('language_id', $idLanguage)->get()->first();    
            $tagsSummaryDescriptionDocument = SelenaViewsBL::getTagByType('SummaryDescriptionDocument');
            $OrderSummaryDescDocument = SalesOrder::where('id', $salesOrder->id)->first();   
            $finalHtmlOrderSummaryDescDocuments= "";
            if (isset($OrderSummaryDescDocument)){

                foreach (DB::select(
                    'SELECT *
                    FROM sales_orders
                    WHERE id = ' . $salesOrder->id
                    ) as $OrderSummaryDescDocuments){
                    $newContentOrderSummaryDocuments = $contentLanguageUser->content;  
                    foreach ($tagsSummaryDescriptionDocument as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                              
                            if (strpos($contentLanguageUser->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($OrderSummaryDescDocuments->{$cleanTag}) && strpos($OrderSummaryDescDocuments->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($OrderSummaryDescDocuments->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $OrderSummaryDescDocuments->{$cleanTag};
                                }
                                $newContentOrderSummaryDocuments = str_replace($fieldsDb->tag , $tag,  $newContentOrderSummaryDocuments);                                                                               
                            }
                        }
                    }
            if (strpos($newContentOrderSummaryDocuments, '#sales_order_state_description#') !== false) {
                        $descriptionSalesOrderStateDescription = DocumentStatusLanguage::where('document_status_id', $OrderSummaryDescDocuments->order_state_id)->first();   
                        if (isset($descriptionSalesOrderStateDescription)){
                        $newContentOrderSummaryDocuments = str_replace('#sales_order_state_description#', $descriptionSalesOrderStateDescription->description,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#sales_order_state_description#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }

                     if (strpos($newContentOrderSummaryDocuments, '#carriage_description#') !== false) {

                        $descriptionCarriageLanguageDescription = CarriagePaidLanguage::where('carriage_paid_to_id', $salesOrder->carriage_paid_to_id)->first();
                        if (isset($descriptionCarriageLanguageDescription)){
                        $newContentOrderSummaryDocuments = str_replace('#carriage_description#', $descriptionCarriageLanguageDescription->description,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#carriage_description#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }

                      if (strpos($newContentOrderSummaryDocuments, '#carrier_description#') !== false) {
                        $descriptionCarrierDescription = Carrier::where('id', $OrderSummaryDescDocuments->carrier_id)->first();
                        if (isset($descriptionCarrierDescription)){
                        $newContentOrderSummaryDocuments = str_replace('#carrier_description#', $descriptionCarrierDescription->name,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#carrier_description#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }
                    if (strpos($newContentOrderSummaryDocuments, '#payment_description#') !== false) {
                        $PaymentDescription = LanguagePaymentType::where('payment_type_id', $OrderSummaryDescDocuments->payment_type_id)->first();
                        if (isset($PaymentDescription)){
                        $newContentOrderSummaryDocuments = str_replace('#payment_description#', $PaymentDescription->description,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#payment_description#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }


                if (strpos($newContentOrderSummaryDocuments, '#date_order_formatter#') !== false) {
                                $dateorderformatter = SalesOrder::where('id', $salesOrder->id)->first()->date_order;   
                                $dateorderformatters = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $dateorderformatter)->format('d/m/Y H:i:s');
                                //Carbon::parse($dateorderformatter)->format('d/m/Y H:i:s');         
                                if (isset($dateorderformatters)){     
                                    $newContentOrderSummaryDocuments = str_replace('#date_order_formatter#',$dateorderformatters,  $newContentOrderSummaryDocuments); 
                                }else{
                                    $newContentOrderSummaryDocuments = str_replace('#date_order_formatter#', '',  $newContentOrderSummaryDocuments);           
                                }
                            }

                    $finalHtmlOrderSummaryDescDocuments = $finalHtmlOrderSummaryDescDocuments . $newContentOrderSummaryDocuments;
                }; 
            }
             $finalHtml = str_replace('#summary-description-document#' , $finalHtmlOrderSummaryDescDocuments,  $finalHtml); 
        }


            #detail-row-order#
            if (strpos($contentLanguage->content, '#detail-row-order#') !== false) {
            $SummaryDescriptionDocument= Content::where('code', 'DetailRowOrder')->get()->first();
            $contentLanguageUser = ContentLanguage::where('content_id', $SummaryDescriptionDocument->id)->where('language_id', $idLanguage)->get()->first();    
            $tagsSummaryDescriptionDocument = SelenaViewsBL::getTagByType('DetailRowOrder');
            $OrderSummaryDescDocument = SalesOrder::where('id', $salesOrder->id)->first();   
            $finalHtmlOrderSummaryDescDocuments= "";
            if (isset($OrderSummaryDescDocument)){

                foreach (DB::select(
                    'SELECT *
                    FROM sales_orders_details
                    WHERE order_id = ' . $salesOrder->id
                    ) as $OrderSummaryDescDocuments){
                    $newContentOrderSummaryDocuments = $contentLanguageUser->content;  
                    foreach ($tagsSummaryDescriptionDocument as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                              
                            if (strpos($contentLanguageUser->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($OrderSummaryDescDocuments->{$cleanTag}) && strpos($OrderSummaryDescDocuments->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($OrderSummaryDescDocuments->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $OrderSummaryDescDocuments->{$cleanTag};
                                }
                                $newContentOrderSummaryDocuments = str_replace($fieldsDb->tag , $tag,  $newContentOrderSummaryDocuments);                                                                               
                            }
                        }
                    }
          
                    if (strpos($newContentOrderSummaryDocuments, '#vat_type_description#') !== false) {
                        $DescriptionVatType = VatType::where('id', $OrderSummaryDescDocuments->vat_type_id)->first();
                        if (isset($DescriptionVatType)){
                        $newContentOrderSummaryDocuments = str_replace('#vat_type_description#', $DescriptionVatType->rate,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#vat_type_description#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }
                       if (strpos($newContentOrderSummaryDocuments, '#unit_of_measure_description#') !== false) {
                        $UnitMeasureDescription = UnitMeasure::where('id', $OrderSummaryDescDocuments->unit_of_measure_id)->first();
                        if (isset($UnitMeasureDescription)){
                        $newContentOrderSummaryDocuments = str_replace('#unit_of_measure_description#', $UnitMeasureDescription->code,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#unit_of_measure_description#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }
                    if (strpos($newContentOrderSummaryDocuments, '#items_description#') !== false) {
                        $ItemsDescriptionCode = Item::where('id', $OrderSummaryDescDocuments->item_id)->first();
                        if (isset($ItemsDescriptionCode)){
                        $newContentOrderSummaryDocuments = str_replace('#items_description#', $ItemsDescriptionCode->internal_code,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#items_description#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }

                    if (strpos($newContentOrderSummaryDocuments, '#description_language_item#') !== false) {
                        $ItemsDescriptionLanguage = ItemLanguage::where('item_id', $OrderSummaryDescDocuments->item_id)->first();
                        if (isset($ItemsDescriptionLanguage)){
                        $newContentOrderSummaryDocuments = str_replace('#description_language_item#', $ItemsDescriptionLanguage->description,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#description_language_item#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }

                    if (strpos($newContentOrderSummaryDocuments, '#shipment_amount#') !== false) {
                        $shipment_amount = SalesOrder::where('shipment_amount', $salesOrder->shipment_amount)->first();
                        if (isset($shipment_amount)){
                        $shipment_amount->shipment_amount = str_replace('.',',',substr($shipment_amount->shipment_amount, 0, -2));
                        $newContentOrderSummaryDocuments = str_replace('#shipment_amount#', $shipment_amount->shipment_amount,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#shipment_amount#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }

                      if (strpos($newContentOrderSummaryDocuments, '#support_id_description#') !== false) {
                        $DescriptionSupport = Supports::where('id', $OrderSummaryDescDocuments->support_id)->first();
                        if (isset($DescriptionSupport)){
                        $newContentOrderSummaryDocuments = str_replace('#support_id_description#', $DescriptionSupport->description,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#support_id_description#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }

                      if (strpos($newContentOrderSummaryDocuments, '#sales_detail_optional#') !== false) {
                        $newContentOrderSummaryDocuments = static::_salesDetailOptional($OrderSummaryDescDocuments->id, 'SalesDetailOptional', '#sales_detail_optional#', $newContentOrderSummaryDocuments);  
                    }
                      if (strpos($newContentOrderSummaryDocuments, '#upload-file-detail#') !== false) {
                        $newContentOrderSummaryDocuments = static::_salesDetailUploadFile($OrderSummaryDescDocuments->id, 'UploadFileDetail', '#upload-file-detail#', $newContentOrderSummaryDocuments);  
                    }

                    $finalHtmlOrderSummaryDescDocuments = $finalHtmlOrderSummaryDescDocuments . $newContentOrderSummaryDocuments;
                }; 
            }
             $finalHtml = str_replace('#detail-row-order#' , $finalHtmlOrderSummaryDescDocuments,  $finalHtml); 
        }



            #order-amounts-summary#
            if (strpos($contentLanguage->content, '#order-amounts-summary#') !== false) {
            $SummaryAmountOrder= Content::where('code', 'SummaryAmountOrder')->get()->first();
            $contentLanguageSummaryAmountOrder = ContentLanguage::where('content_id', $SummaryAmountOrder->id)->where('language_id', $idLanguage)->get()->first();    
            $tagsSummaryAmountOrder = SelenaViewsBL::getTagByType('SummaryAmountOrder');
            $orderSummaryAmountOrder = SalesOrder::where('id', $salesOrder->id)->first();   
            $finalHtmlSummaryAmountOrder= "";
            if (isset($orderSummaryAmountOrder)){

                foreach (DB::select(
                    'SELECT *
                    FROM sales_orders
                    WHERE id = ' . $salesOrder->id
                    ) as $orderSummaryAmountOrders){
                    $newContentSummaryAmountOrder = $contentLanguageSummaryAmountOrder->content;  
                    foreach ($tagsSummaryAmountOrder as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                              
                            if (strpos($contentLanguageSummaryAmountOrder->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($orderSummaryAmountOrders->{$cleanTag}) && strpos($orderSummaryAmountOrders->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($orderSummaryAmountOrders->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $orderSummaryAmountOrders->{$cleanTag};
                                }
                                $newContentSummaryAmountOrder = str_replace($fieldsDb->tag , $tag,  $newContentSummaryAmountOrder);                                                                               
                            }
                        }
                    }

                    $finalHtmlSummaryAmountOrder = $finalHtmlSummaryAmountOrder . $newContentSummaryAmountOrder;
                }; 
            }
             $finalHtml = str_replace('#order-amounts-summary#' , $finalHtmlSummaryAmountOrder,  $finalHtml); 
        }
          
            return $finalHtml;
    }


    #Selena Admin Ordini    
  private static function _salesDetailOptional(int $SalesDetailId,string $codeContent, string $elementReplaced, string $contentPassed)
    {
        $SalesDetailOptionals = SalesOrderDetailOptional::where('sales_order_detail_id', $SalesDetailId);
        if(isset($SalesDetailOptionals)){
            $idContent = Content::where('code', $codeContent)->first();
            if(isset($idContent)){
                $htmlContent = ContentLanguage::where('content_id', $idContent->id)->first()->content;
                if(isset($htmlContent)){
                    $finalHtml = '';
                    $newContent  = '';
                    foreach (DB::select(
                        'SELECT *
                        FROM sales_orders_detail_optional
                        WHERE sales_orders_detail_optional.sales_order_detail_id = :SalesDetailId',
                        ['SalesDetailId' => $SalesDetailId]
                    ) as $SalesDetailOptional){
                        $newContent = $htmlContent;
                        
                        if (strpos($newContent, '#description_id_optionalPrint#') !== false) {
                            $Description_optionalPrint = OptionalLanguages::where('optional_id', $SalesDetailOptional->optional_id)->get()->first();
                            if (isset($Description_optionalPrint)){
                            $newContent = str_replace('#description_id_optionalPrint#', $Description_optionalPrint->description,  $newContent); 
                            }else{
                                $newContent = str_replace('#description_id_optionalPrint#', '',  $newContent);           
                            }
                        }

                    if (strpos($newContent, '#title_tipology_optional#') !== false) {
                            $Title_Tipology_Optional = OptionalTypology::where('optional_id', $SalesDetailOptional->optional_id)->get()->first();
                            $Title_Tipology_OptionalDescription = TypeOptionalLanguages::where('type_optional_id', $Title_Tipology_Optional->type_optional_id)->get()->first();
                            if (isset($Title_Tipology_OptionalDescription)){
                            $newContent = str_replace('#title_tipology_optional#', $Title_Tipology_OptionalDescription->description,  $newContent); 
                            }else{
                                $newContent = str_replace('#title_tipology_optional#', '',  $newContent);           
                            }
                        }
                         
                      $finalHtml = $finalHtml . $newContent;
                    }

                }
                return str_replace($elementReplaced , $finalHtml,  $contentPassed); 
            }else{
                return "";
            }  
        }else{
            return "";
        }
    }


  private static function _salesDetailUploadFile(int $SalesDetailId,string $codeContent, string $elementReplaced, string $contentPassed)
    {
        $SalesDetailFile = SalesOrderDetailUploadFile::where('sales_order_detail_id', $SalesDetailId)->first();
        if(isset($SalesDetailFile)){
             
            $idContent = Content::where('code', $codeContent)->first();
            $tagsUploadFileDetail = SelenaViewsBL::getTagByType('UploadFileDetail');

            if(isset($idContent)){
                $htmlContent = ContentLanguage::where('content_id', $idContent->id)->first()->content;
                if(isset($htmlContent)){
                    $finalHtml = '';
                    $newContent  = '';
                    foreach (DB::select(
                        'SELECT *
                        FROM file_sales_orders_details
                        WHERE file_sales_orders_details.sales_order_detail_id = :SalesDetailId',
                        ['SalesDetailId' => $SalesDetailId]
                    ) as $SalesDetailUploadFile){      
                    $newContent = $htmlContent;  
                    foreach ($tagsUploadFileDetail as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                              
                            if (strpos($htmlContent, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($SalesDetailUploadFile->{$cleanTag}) && strpos($SalesDetailUploadFile->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($SalesDetailUploadFile->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $SalesDetailUploadFile->{$cleanTag};
                                }
                                $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);    
                            }
                        }
                    }                      
                      $finalHtml = $finalHtml . $newContent;
                    }
                }      
                return str_replace($elementReplaced , $finalHtml,  $contentPassed); 
            }   
        }else{  
            $SalesOrderDetail = SalesOrderDetail::where('id', $SalesDetailId);
            if(isset($SalesOrderDetail)){   
                $idContentDetail= Content::where('code', 'UploadFileNew')->first();
                $tagsUploadFileDetail = SelenaViewsBL::getTagByType('UploadFileNew');
                if(isset($idContentDetail)){
                    $htmlContent = ContentLanguage::where('content_id', $idContentDetail->id)->first()->content;
                    if(isset($htmlContent)){
                        
                        $finalHtml = '';
                        $newContent  = '';
                        foreach (DB::select(
                            'SELECT *
                            FROM sales_orders_details
                            WHERE sales_orders_details.id = :SalesDetailId',
                            ['SalesDetailId' => $SalesDetailId]
                        ) as $SalesDetailOrder){      
                            $newContent = $htmlContent;  
                            foreach ($tagsUploadFileDetail as $fieldsDb){
                                if(isset($fieldsDb->tag)){                                                              
                                    if (strpos($htmlContent, $fieldsDb->tag) !== false) {
                                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                        if(is_numeric($SalesDetailOrder->{$cleanTag}) && strpos($SalesDetailOrder->{$cleanTag},'.')){
                                            $tag = str_replace('.',',',substr($SalesDetailOrder->{$cleanTag}, 0, -2));
                                        }else{
                                            $tag = $SalesDetailOrder->{$cleanTag};
                                        }
                                        $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                             
                                    }
                                }
                            }                      
                            $finalHtml = $finalHtml . $newContent;
                        }

                    }
                    $finalHtml = str_replace('#upload-file-detail#' , $newContent,  $finalHtml); 
                    return str_replace($elementReplaced , $finalHtml,  $contentPassed); 
                }
            }
        }
    }



 /**
     * getAllSalesOrder
     * 
     * @return DtoSalesOrder
     */
    public static function getAllSalesOrder()
    {

        $result = collect(); 
        foreach (SalesOrder::orderBy('date_order','DESC')->get() as $SalesOrder) {

               $DtoSalesOrder = new DtoSalesOrder();

               $DtoSalesOrder->code = $SalesOrder->id;  

                if (isset($SalesOrder->date_order)){
                    $DtoSalesOrder->date_order = $SalesOrder->date_order;   
                }else{
                    $DtoSalesOrder->date_order ='-';  
                }
                 if (isset($SalesOrder->user_id)){
                    $UsersBusinessName = UsersDatas::where('user_id', $SalesOrder->user_id)->first();
                    if(isset($UsersBusinessName)){
                        if($UsersBusinessName->business_name != '' && !is_null($UsersBusinessName->business_name)){
                            $DtoSalesOrder->user_id = $UsersBusinessName->business_name;
                        }else{
                            $DtoSalesOrder->user_id = $UsersBusinessName->name . ' ' . $UsersBusinessName->surname;
                        }
                        

                    }else{
                        $DtoSalesOrder->user_id ='-'; 
                    }
                }else{
                    $DtoSalesOrder->user_id ='-';  
                }

                if (isset($SalesOrder->note)){
                    $DtoSalesOrder->note = $SalesOrder->note;  
                }else{
                    $DtoSalesOrder->note ='-';  
                }
              
                if (isset($SalesOrder->payment_type_id)){
                    $LanguagePaymentType = LanguagePaymentType::where('payment_type_id', $SalesOrder->payment_type_id)->first()->description;
                    $DtoSalesOrder->payment_type_id =$LanguagePaymentType;
                }else{
                    $DtoSalesOrder->payment_type_id ='-';  
                }

                if (isset($SalesOrder->order_state_id)){
                    $DtoSalesOrder->order_state_id = DocumentStatusLanguage::where('document_status_id', $SalesOrder->order_state_id)->first()->description;                   
                }else{
                    $DtoSalesOrder->order_state_id = '-';   
                }

                if (isset($SalesOrder->net_amount)){
                    $DtoSalesOrder->net_amount = '€ ' . str_replace('.',',',substr($SalesOrder->net_amount, 0, -2));
                    ;  
                }else{
                    $DtoSalesOrder->net_amount ='-';  
                }

                if (isset($SalesOrder->total_amount)){
                    $DtoSalesOrder->total_amount = '€ ' . str_replace('.',',',substr($SalesOrder->total_amount, 0, -2));
                     
                }else{
                    $DtoSalesOrder->total_amount ='-';  
                }
                
                $result->push($DtoSalesOrder); 
        }
        return $result;
    }

    #endregion GET

    public static function getDetailPopupSelect(int $id)
    {
       $result = collect();

       $salesorder = SalesOrder::where('id', $id)->first();
       $DtoSalesOrder = new DtoSalesOrder();

       $DtoSalesOrders = new DtoSalesOrder();
       $DtoSalesOrders->id = $id;
       $DtoSalesOrders->select_order = $salesorder->order_state_id;

       $DocumentStatusLanguage = DocumentStatusLanguage::where('document_status_id', $salesorder->order_state_id)->first();  
      // $DocumentStatus = DocumentStatus::where('id', $salesorder->order_state_id)->first(); 
        if (isset($DocumentStatusLanguage)){
                $DtoSalesOrders->description_order = $DocumentStatusLanguage->description;
        }else{
                $DtoSalesOrders->description_order = '';   
        }
       $User = User::where('id', $salesorder->user_id)->first();
        if (isset($User)){
            $DtoSalesOrders->description_email_user = $User->email;
        }else{
            $DtoSalesOrders->description_email_user = '';   
        }
            $DtoSalesOrder->DetailStatesOrder->push($DtoSalesOrders);
        
        $result->push($DtoSalesOrder);

        return $result; 
    }


    public static function saveChangeStatus(Request $request)
    {
            //$DocumentStatus = DocumentStatus::where('id', $request->select_order)->first(); 
            $DocumentStatusLanguage = DocumentStatusLanguage::where('document_status_id', $request->select_order)->first();  
        DB::beginTransaction();
        try {
            $SalesOrder= SalesOrder::where('id', $request->idOrder)->first();
        if($request->checksendemailstatesorders == true ){
            if($request->select_order != $SalesOrder->order_state_id){
                /* invio email cliente*/
                $User = User::where('id', $SalesOrder->user_id)->first(); 
                if (isset($User)){
                    $emailuser = $User->email;
                }else{
                    $emailuser = 'Attenzione, email non presente';
                }
                    $nr_order = $SalesOrder->id;
                    $date_order = $SalesOrder->date_order;
                    $statesOrder = $DocumentStatusLanguage->description;

                $emailFrom = SettingBL::getContactsEmail();
                $textObject= MessageBL::getByCode('ORDER_STATUS_MAIL_OBJECT');
                $textMail= MessageBL::getByCode('ORDER_STATUS_CHANGE_TEXT');
                
                $textObject =str_replace ('#nr_order#', $nr_order, $textObject); 
                $textMail =str_replace ('#nr_order#', $nr_order, $textMail); 
                $textMail =str_replace ('#date_order#', $date_order, $textMail); 
                $textMail =str_replace ('#statesOrder#', $statesOrder, $textMail); 
                $textMail =str_replace ('#emailuser#', $emailuser, $textMail); 
                Mail::to($emailuser)->send(new DynamicMail($textObject,$textMail));
                /* end invio email cliente */
            }
                $SalesOrder->order_state_id = $request->select_order;
                $SalesOrder->save(); 
        }else{
            $SalesOrder->order_state_id = $request->select_order;
            $SalesOrder->save();
        }
        DB::commit();
    } catch (Exception $ex) {
        DB::rollback();
        throw $ex;
    }
    
    }

 /**
     * getBySalesOrder
     *
     * @param int $id
     */
    public static function getBySalesOrder(int $id)
    {
        $result = collect();

        $salesorder = SalesOrder::where('id', $id)->first();
        $usersdatas = UsersDatas::where('user_id', $salesorder->user_id)->first();
        $user = User::where('id', $salesorder->user_id)->first();
        $UsersAddressesGoods = UsersAddresses::where('id', $salesorder->user_address_goods_id)->first(); 
        $UsersAddressesDocuments = UsersAddresses::where('id', $salesorder->user_address_documents_id)->first(); 

        $usersOrder = DB::table('sales_orders')
             ->select(DB::raw('count(id) as num_id, user_id'))
             ->where('user_id', '=', $salesorder->user_id)
             ->groupBy('user_id')
             ->get();

        $sum_total_amount = DB::table('sales_orders')
                ->select('user_id', DB::raw('SUM(total_amount) as total_sales'))
                ->where('sales_orders.user_id', '=', $salesorder->user_id)
                ->groupBy('user_id')
                ->get();

        $medium_amount =  DB::table('sales_orders')
                ->where('sales_orders.user_id', '=', $salesorder->user_id)
                ->avg('total_amount');

        $sum_quantity = DB::table('sales_orders_details')
                ->select('order_id', DB::raw('SUM(quantity) as total_quantity'))
                ->where('sales_orders_details.order_id', '=',$id)
                ->groupBy('order_id')
                ->get();        

        $DtoSalesOrder = new DtoSalesOrder();
        $DtoSalesOrder->medium_amount =str_replace('.',',',substr($medium_amount, 0, 5));
        $DtoSalesOrder->sum_quantity = $sum_quantity;
        $DtoSalesOrder->number_order = $usersOrder;
        $DtoSalesOrder->sum_total_amount =str_replace('.',',',substr($sum_total_amount[0]->total_sales, 0, -2));
        $DtoSalesOrder->tracking_shipment = $salesorder->tracking_shipment;
        $DtoSalesOrder->id = $id;
        $DtoSalesOrder->user_id = $salesorder->user_id;

        if (isset($salesorder->payment_type_id)){
            $LanguagePaymentType =  LanguagePaymentType::where('payment_type_id', $salesorder->payment_type_id)->first()->description;       
            $DtoSalesOrder->payment_type_id = $LanguagePaymentType;
        }else{
            $DtoSalesOrder->payment_type_id ='';
        }

       // $DtoSalesOrder->payment_type_id =LanguagePaymentType::where('payment_type_id', $salesorder->payment_type_id)->first()->description;
        if (isset($salesorder->carrier_id)){
            $Carrier= Carrier::where('id', $salesorder->carrier_id)->first()->name;
            $DtoSalesOrder->carrier_id = $Carrier;
        }else{
            $DtoSalesOrder->carrier_id='';
        }
        //$DtoSalesOrder->carrier_id = Carrier::where('id', $salesorder->carrier_id)->first()->name;
        if (isset($salesorder->carriage_paid_to_id)){
            $CarriagePaidLanguage= CarriagePaidLanguage::where('carriage_paid_to_id', $salesorder->carriage_paid_to_id)->first()->description;   
            $DtoSalesOrder->carriage_paid_to_id = $CarriagePaidLanguage;
        }else{
            $DtoSalesOrder->carriage_paid_to_id = '';
        }
        //$DtoSalesOrder->carriage_paid_to_id = CarriagePaidLanguage::where('carriage_paid_to_id', $salesorder->carriage_paid_to_id)->first()->description;
        
        if (isset($salesorder->date_order)){
            $DtoSalesOrder->date_order = $salesorder->date_order; 
        }else{
            $DtoSalesOrder->date_order = '';
        }

        if (isset($salesorder->date_order)){
            $DtoSalesOrder->note = $salesorder->note;
        }else{
            $DtoSalesOrder->note = '';
        }

        if (isset($salesorder->order_state_id)){
            $DocumentStatusLanguage = DocumentStatusLanguage::where('document_status_id', $salesorder->order_state_id)->first()->description;  
            $DtoSalesOrder->order_state_id = $DocumentStatusLanguage;
        }else{
            $DtoSalesOrder->order_state_id ='';
        }
        //$DtoSalesOrder->order_state_id = DocumentStatusLanguage::where('document_status_id', $salesorder->order_state_id)->first()->description; 

        $DtoSalesOrder->net_amount = str_replace('.',',',substr($salesorder->net_amount, 0, -2));
        $DtoSalesOrder->shipment_amount = str_replace('.',',',substr($salesorder->shipment_amount, 0, -2));
        $DtoSalesOrder->payment_cost = str_replace('.',',',substr($salesorder->payment_cost, 0, -2));
        $DtoSalesOrder->vat_amount =str_replace('.',',',substr($salesorder->vat_amount, 0, -2));
        $DtoSalesOrder->discount_value =str_replace('.',',',substr($salesorder->discount_value, 0, -2));
        $DtoSalesOrder->total_amount = str_replace('.',',',substr($salesorder->total_amount, 0, -2));
        $DtoSalesOrder->service_amount = str_replace('.',',',substr($salesorder->service_amount, 0, -2));
        $DtoUser = new DtoUser(); 
        if (isset($usersdatas->business_name)){
            $DtoUser->business_name = $usersdatas->business_name;
        }else{
            $DtoUser->business_name = '';
        }
        if (isset($usersdatas->name)){
            $DtoUser->name = $usersdatas->name;
        }else{
            $DtoUser->name = '';
        }
        if (isset($usersdatas->surname)){
            $DtoUser->surname = $usersdatas->surname;
        }else{
            $DtoUser->surname = '';
        }
        if (isset($usersdatas->address)){
            $DtoUser->address = $usersdatas->address;
        }else{
            $DtoUser->address = '';
        }
        if (isset($usersdatas->fiscal_code)){
            $DtoUser->fiscal_code = $usersdatas->fiscal_code;
        }else{
            $DtoUser->fiscal_code = '';
        }
        if (isset($usersdatas->telephone_number)){
            $DtoUser->telephone_number = $usersdatas->telephone_number;
        }else{
            $DtoUser->telephone_number = '';
        }
        if (isset($usersdatas->postal_code)){
            $DtoUser->postal_code = $usersdatas->postal_code;
        }else{
            $DtoUser->postal_code = '';
        }
        if (isset($usersdatas->country)){
            $DtoUser->country = $usersdatas->country;
        }else{
            $DtoUser->country = '';
        }
        if (isset($usersdatas->fax_number)){
            $DtoUser->fax_number = $usersdatas->fax_number;
        }else{
            $DtoUser->fax_number = '';
        }
        if (isset($user->email)){
            $DtoUser->email = $user->email; 
        }else{
            $DtoUser->email = '';
        }
         if (isset($usersdatas->vat_number)){
            $DtoUser->vat_number = $usersdatas->vat_number;
        }else{
            $DtoUser->vat_number = '';
        }

        if (isset($usersdatas->created_at) && !is_null($usersdatas->created_at) ){
        $DtoUser->created_at =  date_format($usersdatas->created_at, 'd/m/Y'); 
        }else{
            $DtoUser->created_at = '';
        }

        if (isset($usersdatas->province_id)){
            $Province = Province::where('id', $usersdatas->province_id)->first()->abbreviation;
            $DtoUser->province_id = $Province;
        }else{
            $DtoUser->province_id = '';
        }
        $DtoSalesOrder->UsersDatas->push($DtoUser);

        $DtoUserAddress = new DtoUserAddress(); 
    if (isset($UsersAddressesGoods)){
        if (isset($UsersAddressesGoods->business_name)){
            $DtoUserAddress->business_name =  'Ragione Sociale: ' . $UsersAddressesGoods->business_name;
        }else{
            $DtoUserAddress->business_name = '';   
        }
         if (isset($UsersAddressesGoods->name)){
            $DtoUserAddress->name = 'Nome e Cognome: ' . $UsersAddressesGoods->name;
        }else{
            $DtoUserAddress->name = '';   
        }
         if (isset($UsersAddressesGoods->surname)){
            $DtoUserAddress->surname = $UsersAddressesGoods->surname;
        }else{
            $DtoUserAddress->surname = '';   
        }

        if (isset($UsersAddressesGoods->address)){
            $DtoUserAddress->address = 'Indirizzo: ' . $UsersAddressesGoods->address;
        }else{
            $DtoUserAddress->address = '';   
        }
        if (isset($UsersAddressesGoods->postal_code)){
            $DtoUserAddress->postal_code = $UsersAddressesGoods->postal_code;
        }else{
            $DtoUserAddress->postal_code = '';  
        }    
        if (isset($UsersAddressesGoods->locality)){   
            $DtoUserAddress->locality = $UsersAddressesGoods->locality;
        }else{
            $DtoUserAddress->locality ='';  
        }
         if (isset($UsersAddressesGoods->province_id)){
            $Province = Province::where('id', $UsersAddressesGoods->province_id)->first()->abbreviation;
            $DtoUserAddress->province_id = $Province;
        }else{
            $DtoUserAddress->province_id = '';
        }
        //$DtoUserAddress->province_id = LanguageProvince::where('province_id', $UsersAddressesGoods->province_id)->first()->description;
        if (isset($UsersAddressesGoods->phone_number)){
            $DtoUserAddress->phone_number = 'Telefono: ' . $UsersAddressesGoods->phone_number;
        }else{
            $DtoUserAddress->phone_number = '';
        }
        if (isset($UsersAddressesGoods->vat_number)){
            $DtoUserAddress->vat_number = 'P.IVA: ' . $UsersAddressesGoods->vat_number;
        }else{
            $DtoUserAddress->vat_number = '';
        }
        if (isset($UsersAddressesGoods->fiscal_code)){
            $DtoUserAddress->fiscal_code = 'Codice Fiscale: ' . $UsersAddressesGoods->fiscal_code;
        }else{
            $DtoUserAddress->fiscal_code = '';
        }

    }else{
         if (isset($usersdatas->business_name)){
            $DtoUserAddress->business_name = 'Ragione Sociale: ' . $usersdatas->business_name;
        }else{
            $DtoUserAddress->business_name = '';   
        }
         if (isset($usersdatas->name)){
            $DtoUserAddress->name = 'Nome e Cognome: '  . $usersdatas->name;
        }else{
            $DtoUserAddress->name = '';   
        }
         if (isset($usersdatas->surname)){
            $DtoUserAddress->surname = $usersdatas->surname;
        }else{
            $DtoUserAddress->surname = '';   
        }

        if (isset($usersdatas->address)){
            $DtoUserAddress->address = 'Indirizzo: ' . $usersdatas->address;
        }else{
            $DtoUserAddress->address = '';   
        }
        if (isset($usersdatas->postal_code)){
            $DtoUserAddress->postal_code = $usersdatas->postal_code;
        }else{
            $DtoUserAddress->postal_code = '';  
        }    
        if (isset($usersdatas->country)){   
            $DtoUserAddress->locality = $usersdatas->country;
        }else{
            $DtoUserAddress->locality ='';  
        }
         if (isset($usersdatas->province_id)){
            $Province = Province::where('id', $usersdatas->province_id)->first()->abbreviation;
            $DtoUserAddress->province_id = $Province;
        }else{
            $DtoUserAddress->province_id = '';
        }
        if (isset($usersdatas->telephone_number)){
            $DtoUserAddress->phone_number = 'Telefono: ' . $usersdatas->telephone_number;
        }else{
            $DtoUserAddress->phone_number = '';
        }
        if (isset($usersdatas->vat_number)){
            $DtoUserAddress->vat_number = 'P.IVA: ' . $usersdatas->vat_number;
        }else{
            $DtoUserAddress->vat_number = '';
        }
        if (isset($usersdatas->fiscal_code)){
            $DtoUserAddress->fiscal_code = 'Codice Fiscale: ' . $usersdatas->fiscal_code;
        }else{
            $DtoUserAddress->fiscal_code = '';
        }

    }
        $DtoSalesOrder->UserAddressesGoods->push($DtoUserAddress);
            
        $DtoUserAddress = new DtoUserAddress(); 
    if (isset($UsersAddressesDocuments)){
        if (isset($UsersAddressesDocuments->business_name)){
            $DtoUserAddress->business_name = 'Ragione Sociale: ' . $UsersAddressesDocuments->business_name;
        }else{
            $DtoUserAddress->business_name = '';
        }
        if (isset($UsersAddressesDocuments->name)){
            $DtoUserAddress->name = 'Nome e Cognome: ' . $UsersAddressesDocuments->name;
        }else{
            $DtoUserAddress->name = '';
        }
        if (isset($UsersAddressesDocuments->surname)){
            $DtoUserAddress->surname = $UsersAddressesDocuments->surname;
        }else{
            $DtoUserAddress->surname = '';
        }

        if (isset($UsersAddressesDocuments->address)){
            $DtoUserAddress->address = 'Indirizzo: ' . $UsersAddressesDocuments->address;
        }else{
            $DtoUserAddress->address = '';
        }
        if (isset($UsersAddressesDocuments->postal_code)){
            $DtoUserAddress->postal_code = $UsersAddressesDocuments->postal_code;
         }else{   
            $DtoUserAddress->postal_code = '';
        }
        if (isset($UsersAddressesDocuments->locality)){
            $DtoUserAddress->locality = $UsersAddressesDocuments->locality;
        }else{
            $DtoUserAddress->locality = '';
        }  
            
        if (isset($UsersAddressesDocuments->province_id)){
                $Province = Province::where('id', $UsersAddressesDocuments->province_id)->first()->abbreviation;
                $DtoUserAddress->province_id = $Province;
        }else{
                $DtoUserAddress->province_id = '';
        }
        if (isset($UsersAddressesDocuments->phone_number)){
            $DtoUserAddress->phone_number = 'Telefono: ' . $UsersAddressesDocuments->phone_number;
        }else{
            $DtoUserAddress->phone_number = '';
        }  
        if (isset($UsersAddressesDocuments->vat_number)){
            $DtoUserAddress->vat_number = 'P.IVA: ' . $UsersAddressesDocuments->vat_number;
        }else{
            $DtoUserAddress->vat_number = '';
        }  

        if (isset($UsersAddressesDocuments->fiscal_code)){
            $DtoUserAddress->fiscal_code = 'Codice Fiscale: ' . $UsersAddressesDocuments->fiscal_code;
        }else{
            $DtoUserAddress->fiscal_code = '';
        }  
    }else{
        if (isset($usersdatas->business_name)){
            $DtoUserAddress->business_name = 'Ragione Sociale: ' . $usersdatas->business_name;
        }else{
            $DtoUserAddress->business_name = '';   
        }
        if (isset($usersdatas->name)){
            $DtoUserAddress->name = 'Nome e Cognome: ' . $usersdatas->name;
        }else{
            $DtoUserAddress->name = '';   
        }
         if (isset($usersdatas->surname)){
            $DtoUserAddress->surname = $usersdatas->surname;
        }else{
            $DtoUserAddress->surname = '';   
        }
        
        if (isset($usersdatas->address)){
            $DtoUserAddress->address = 'Indirizzo: ' . $usersdatas->address;
        }else{
            $DtoUserAddress->address = '';   
        }
        if (isset($usersdatas->postal_code)){
            $DtoUserAddress->postal_code = $usersdatas->postal_code;
        }else{
            $DtoUserAddress->postal_code = '';  
        }    
        if (isset($usersdatas->country)){   
            $DtoUserAddress->locality = $usersdatas->country;
        }else{
            $DtoUserAddress->locality ='';  
        }
         if (isset($usersdatas->province_id)){
            $Province = Province::where('id', $usersdatas->province_id)->first()->abbreviation;
            $DtoUserAddress->province_id = $Province;
        }else{
            $DtoUserAddress->province_id = '';
        }
        if (isset($usersdatas->telephone_number)){
            $DtoUserAddress->phone_number = 'Telefono: ' . $usersdatas->telephone_number;
        }else{
            $DtoUserAddress->phone_number = '';
        }
        if (isset($usersdatas->vat_number)){
            $DtoUserAddress->vat_number = 'P.IVA: ' . $usersdatas->vat_number;
        }else{
            $DtoUserAddress->vat_number = '';
        }
        if (isset($usersdatas->fiscal_code)){
            $DtoUserAddress->fiscal_code = 'Codice Fiscale: ' . $usersdatas->fiscal_code;
        }else{
            $DtoUserAddress->fiscal_code = '';
        }
    }
            $DtoSalesOrder->UserAddressesDocuments->push($DtoUserAddress);

        $CartRulesApplied = CartRulesApplied::where('sales_order_id', $id)->first();
        $DtoCartRulesApplied = new DtoCartRulesApplied(); 
        if (isset($CartRulesApplied->description)) {
        $DtoCartRulesApplied->description = $CartRulesApplied->description;
        }else{
        $DtoCartRulesApplied->description = '';    
        }
        $DtoSalesOrder->CartRulesApplied->push($DtoCartRulesApplied);
       
            $salesorderdetail = SalesOrderDetail::where('order_id', $id)->get();
            if (isset($salesorderdetail)) {
                foreach ($salesorderdetail as $SalesOrderDetail) {
                    $DtoSalesOrderDetail= new DtoSalesOrderDetail();
                    $DtoSalesOrderDetail->id = $SalesOrderDetail->id;
                    $DtoSalesOrderDetail->item_id = Item::where('id', $SalesOrderDetail->item_id)->first()->internal_code;
                    $DtoSalesOrderDetail->available = Item::where('id', $SalesOrderDetail->item_id)->first()->available;

                foreach (DB::select(
                "select SUM(weight),sod.order_id, sod.item_id from sales_orders_details sod
                inner join items i2 on i2.id=sod.item_id
                where sod.order_id =  $id
                GROUP BY sod.order_id,sod.item_id"
                ) as $sum_total_weight);
     
                    $DtoSalesOrderDetail->sum_total_weight = str_replace('.',',',substr($sum_total_weight->sum, 0, -2));
                   //$DtoSalesOrderDetail->sum_total_weight = $sum_total_weight;
                   if (isset($SalesOrderDetail->item_id)) {  
                        $DtoSalesOrderDetail->description_language_item = ItemLanguage::where('item_id', $SalesOrderDetail->item_id)->first()->description; 
                   }else{
                        $DtoSalesOrderDetail->description_language_item = '';
                   }
                    if (isset($SalesOrderDetail->unit_of_measure_id)) {  
                        $DtoSalesOrderDetail->unit_of_measure_id = UnitMeasure::where('id', $SalesOrderDetail->unit_of_measure_id)->first()->code;
                   }else{
                        $DtoSalesOrderDetail->unit_of_measure_id= '';
                   }

                    $DtoSalesOrderDetail->quantity = $SalesOrderDetail->quantity;
                    $DtoSalesOrderDetail->unit_price = str_replace('.',',',substr($SalesOrderDetail->unit_price, 0, -2));
                    $DtoSalesOrderDetail->discount_percentage = $SalesOrderDetail->discount_percentage;
                    $DtoSalesOrderDetail->net_amount = str_replace('.',',',substr($SalesOrderDetail->net_amount, 0, -2));
                    $DtoSalesOrderDetail->vat_amount = str_replace('.',',',substr($SalesOrderDetail->vat_amount, 0, -2));

                    if (isset($SalesOrderDetail->vat_type_id)) {  
                        $DtoSalesOrderDetail->vat_type_id =VatType::where('id', $SalesOrderDetail->vat_type_id)->first()->rate; 
                    }else {
                      $DtoSalesOrderDetail->vat_type_id = '';  
                    }
                    $DtoSalesOrderDetail->total_amount =  str_replace('.',',',substr($SalesOrderDetail->total_amount, 0, -2));

                    if (isset($SalesOrderDetail->order_detail_state_id)) {  
                    $DtoSalesOrderDetail->order_detail_state_id = DocumentStatusLanguage::where('document_status_id', $SalesOrderDetail->order_detail_state_id)->first()->description;     
                    }else{
                      $DtoSalesOrderDetail->order_detail_state_id = '';  
                    }
                    if (isset($SalesOrderDetail->width)) {  
                        $DtoSalesOrderDetail->width = $SalesOrderDetail->width;    
                    }else{
                       $DtoSalesOrderDetail->width = ''; 
                    }   
                    if (isset($SalesOrderDetail->depth)) {  
                        $DtoSalesOrderDetail->depth = $SalesOrderDetail->depth;    
                    }else{
                       $DtoSalesOrderDetail->depth = ''; 
                    }     
                    if (isset($SalesOrderDetail->height)) {  
                        $DtoSalesOrderDetail->height = $SalesOrderDetail->height;    
                    }else{
                       $DtoSalesOrderDetail->height = ''; 
                    }  
                    if (isset($SalesOrderDetail->support_id)) {  
                        $DtoSalesOrderDetail->support_id = Supports::where('id', $SalesOrderDetail->support_id)->first()->description; 
                    }else{
                       $DtoSalesOrderDetail->support_id = ''; 
                    }  

                    $fileSalesOrderDetail = SalesOrderDetailUploadFile::where('sales_order_detail_id', $SalesOrderDetail->id)->orderBy('created_at')->get()->first();
                    if (isset($fileSalesOrderDetail)) {  
                        $DtoSalesOrderDetail->file = $fileSalesOrderDetail->file;  
                    }else{
                        $DtoSalesOrderDetail->file = '';
                    }

                    $DtoSalesOrder->SalesOrderDetail->push($DtoSalesOrderDetail);
                    $DtoSalesOrder->SalesOrderDet->push($DtoSalesOrderDetail);



                $SalesDetailOptional = SalesOrderDetailOptional::where('sales_order_detail_id', $SalesOrderDetail->id)->get();
                    
                if (isset($SalesDetailOptional)) {
                foreach ($SalesDetailOptional as $SalesDetailOptionals) {
                $DtoSalesOrderDetailOptional= new DtoSalesOrderDetailOptional();
                $DtoSalesOrderDetailOptional->id = $SalesDetailOptionals->id;
                $DtoSalesOrderDetailOptional->sales_order_detail_id = $SalesOrderDetail->id;
                $DtoSalesOrderDetailOptional->optional_id = OptionalLanguages::where('optional_id', $SalesDetailOptionals->optional_id)->first()->description;   
                $DtoSalesOrder->SalesOrderDetailOptional->push($DtoSalesOrderDetailOptional);

                $Title_Tipology_Optional = OptionalTypology::where('optional_id', $SalesDetailOptionals->optional_id)->get();
                if (isset($Title_Tipology_Optional)) {
                foreach ($Title_Tipology_Optional as $Title_Tipology_Optionals) {
                $DtoSalesOrderDetailOptional->title_tipology_optional = TypeOptionalLanguages::where('type_optional_id', $Title_Tipology_Optionals->type_optional_id)->first()->description;
                $DtoSalesOrder->SalesOrderDetailTitleTipology_Optional->push($DtoSalesOrderDetailOptional);
            }
          }
            }
        }

        
                    
                }


            }

                  

            $messages = OrderMessages::where('order_id', $id)->get();
            if (isset($messages)) {
                foreach ($messages as $MessageDetail) {
                    $DtoOrderMessages= new DtoOrderMessages();
                    $DtoOrderMessages->id = $MessageDetail->id;
                    if (isset($MessageDetail->id_sender)) {  
                        $DtoOrderMessages->id_sender = User::where('id', $MessageDetail->id_sender)->first()->name;
                    }else{
                        $DtoOrderMessages->id_sender = '';  
                    }
                    if (isset($MessageDetail->id_receiver)) {  
                        $DtoOrderMessages->id_receiver =  User::where('id', $MessageDetail->id_receiver)->first()->name;
                    }else{  
                        $DtoOrderMessages->id_receiver = '';  
                    }

                    $DtoOrderMessages->message = $MessageDetail->message;
                    $DtoOrderMessages->order_id = $MessageDetail->order_id;
                    $DtoOrderMessages->created_at = date_format($MessageDetail->created_at, 'd/m/Y');
                    $DtoSalesOrder->OrderMessages->push($DtoOrderMessages);
             }
        }
        
        $result->push($DtoSalesOrder);

        return $result; 

}


 public static function updateNote(Request $request){
    $SalesOrder= SalesOrder::where('id', $request->id)->first();

        DB::beginTransaction();
            try {
                $SalesOrder->id = $request->id;
                $SalesOrder->note = $request->note;
                
                 $SalesOrder->save();

        DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }

#endSelena Admin Ordini
 public static function updateSalesOrderDetail(Request $request){
    $SalesOrderDetail = SalesOrderDetail::where('id', $request->idOrder)->first();

        DB::beginTransaction();
            try {
         
                 $SalesOrderDetail->id = $request->idOrder;
                 $SalesOrderDetail->quantity = $request->quantity;
                 $SalesOrderDetail->unit_price = $request->unit_price;
                 $SalesOrderDetail->save();

        DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }

    public static function SendMailCustomer(Request $request){
                $SalesOrder = SalesOrder::where('id', $request->id)->first();
                $usersdatas = UsersDatas::where('user_id',$SalesOrder->user_id)->first();
                $user = User::where('id', $SalesOrder->user_id)->first();   
                $UsersAddressesGoods = UsersAddresses::where('id', $SalesOrder->user_address_goods_id)->first(); 
                $UsersAddressesDocuments = UsersAddresses::where('id', $SalesOrder->user_address_documents_id)->first(); 

                //REGION EMAIL SEND ORDER
                 $emailFrom = SettingBL::getContactsEmail();

                 //dati anagrafici
                    if (isset($usersdatas->name)) {  
                        $name = $usersdatas->name;
                    }else{
                        $name = '';  
                    }
                    if (isset($usersdatas->surname)) {  
                        $surname = $usersdatas->surname;
                    }else{
                        $surname = '';  
                    }
                    if (isset($usersdatas->business_name)) {  
                        $business_name = $usersdatas->business_name;
                    }else{
                        $business_name = '';  
                    }
                     if (isset($usersdatas->address)) {  
                        $address = $usersdatas->address;
                    }else{
                        $address = '';  
                    }
                    if (isset($usersdatas->postal_code)) {  
                        $postal_code = $usersdatas->postal_code;
                    }else{
                        $postal_code = '';  
                    }
                    if (isset($usersdatas->country)) {  
                        $country = $usersdatas->country;
                    }else{
                        $country = '';  
                    }
                    if (isset($usersdatas->province_id)) {  
                        $province = LanguageProvince::where('province_id', $usersdatas->province_id)->first()->description;                 
                    }else{
                        $province = '';  
                    }
                    if (isset($usersdatas->telephone_number)) {  
                        $telephone_number = $usersdatas->telephone_number;
                    }else{
                        $telephone_number = '';  
                    }
                    if (isset($user->email)) {  
                        $email = $user->email;
                    }else{
                        $email = '';  
                    }
                    if (isset($usersdatas->vat_number)) {  
                        $vat_number = $usersdatas->vat_number;
                    }else{
                        $vat_number = '';  
                    }
                    if (isset($usersdatas->fiscal_code)) {  
                        $fiscal_code = $usersdatas->fiscal_code;
                    }else{
                        $fiscal_code = '';  
                    }


                 //$name = $usersdatas->name;
                 //$surname = $usersdatas->surname; 
                 //$business_name = $usersdatas->business_name; 
                 //$address = $usersdatas->address; 
                 //$postal_code = $usersdatas->postal_code;  
                 //$country = $usersdatas->country; 
                 //$province = LanguageProvince::where('province_id', $usersdatas->province_id)->first()->description; 
                 //$telephone_number = $usersdatas->telephone_number; 
                // $email = $user->email;

                 //spedizione merce 

    if (isset($UsersAddressesGoods)){
                    if (isset($UsersAddressesGoods->business_name)) {  
                        $UserAddressGoodBusiness_name = $UsersAddressesGoods->business_name;
                    }else{
                        $UserAddressGoodBusiness_name = '';  
                    }
                    if (isset($UsersAddressesGoods->name)) {  
                        $UserAddressGood_name = $UsersAddressesGoods->name;
                    }else{
                        $UserAddressGood_name = '';  
                    }
                    if (isset($UsersAddressesGoods->surname)) {  
                        $UserAddressGood_surname = $UsersAddressesGoods->surname;
                    }else{
                        $UserAddressGood_surname = '';  
                    }
                    if (isset($UsersAddressesGoods->address)) {  
                        $UserAddressGoodAddress = $UsersAddressesGoods->address;
                    }else{
                        $UserAddressGoodAddress = '';  
                    }
                    if (isset($UsersAddressesGoods->postal_code)) {  
                        $UserAddressGoodPostal_code = $UsersAddressesGoods->postal_code;
                    }else{
                        $UserAddressGoodPostal_code = '';  
                    }
                    if (isset($UsersAddressesGoods->locality)) {  
                        $UserAddressGoodLocality = $UsersAddressesGoods->locality;
                    }else{
                        $UserAddressGoodLocality = '';  
                    }
                     if (isset($UsersAddressesGoods->province_id)) {  
                        $UserAddressGoodProvince_id = LanguageProvince::where('province_id', $UsersAddressesGoods->province_id)->first()->description;
                    }else{
                        $UserAddressGoodProvince_id = '';  
                    }
                    if (isset($UsersAddressesGoods->phone_number)) {  
                        $UserAddressGoodPhone_number = $UsersAddressesGoods->phone_number;
                    }else{
                        $UserAddressGoodPhone_number = '';  
                    }
    }else{
         if (isset($usersdatas->business_name)){
            $UserAddressGoodBusiness_name = $usersdatas->business_name;
        }else{
            $UserAddressGoodBusiness_name = '';   
        }
         if (isset($usersdatas->name)){
            $UserAddressGood_name = $usersdatas->name;
        }else{
            $UserAddressGood_name = '';   
        }
         if (isset($usersdatas->surname)){
            $UserAddressGood_surname = $usersdatas->surname;
        }else{
            $UserAddressGood_surname = '';   
        }

        if (isset($usersdatas->address)){
            $UserAddressGoodAddress  = $usersdatas->address;
        }else{
            $UserAddressGoodAddress  = '';   
        }
        if (isset($usersdatas->postal_code)){
            $UserAddressGoodPostal_code = $usersdatas->postal_code;
        }else{
            $UserAddressGoodPostal_code = '';  
        }    
        if (isset($usersdatas->country)){   
            $UserAddressGoodLocality = $usersdatas->country;
        }else{
            $UserAddressGoodLocality ='';  
        }
         if (isset($usersdatas->province_id)){
            $Province = Province::where('id', $usersdatas->province_id)->first()->abbreviation;
            $UserAddressGoodProvince_id = $Province;
        }else{
            $UserAddressGoodProvince_id = '';
        }
        if (isset($usersdatas->telephone_number)){
            $UserAddressGoodPhone_number = $usersdatas->telephone_number;
        }else{
            $UserAddressGoodPhone_number = '';
        }
    }
   
                /*$UserAddressGoodBusiness_name = $UsersAddressesGoods->business_name;
                $UserAddressGood_name = $UsersAddressesGoods->name;
                $UserAddressGood_surname = $UsersAddressesGoods->surname;
                $UserAddressGoodAddress = $UsersAddressesGoods->address;
                $UserAddressGoodPostal_code = $UsersAddressesGoods->postal_code;
                $UserAddressGoodLocality = $UsersAddressesGoods->locality;
                $UserAddressGoodProvince_id = LanguageProvince::where('province_id', $UsersAddressesGoods->province_id)->first()->description;
                $UserAddressGoodPhone_number = $UsersAddressesGoods->phone_number;*/

                //spedizione documenti
                /*$UserAddressDocumentBusiness_name = $UsersAddressesDocuments->business_name;
                $UserAddressDocumentaddress = $UsersAddressesDocuments->address;
                $UserAddressDocumentPostal_code = $UsersAddressesDocuments->postal_code;
                $UserAddressDocumentLocality = $UsersAddressesDocuments->locality;
                $UserAddressDocumentProvince_id = LanguageProvince::where('province_id', $UsersAddressesDocuments->province_id)->first()->description;
                $UserAddressDocumentPhone_number = $UsersAddressesDocuments->phone_number;*/
 if (isset($UsersAddressesDocuments)){
                if (isset($UsersAddressesDocuments->business_name)) {  
                        $UserAddressDocumentBusiness_name = $UsersAddressesDocuments->business_name;
                    }else{
                        $UserAddressDocumentBusiness_name = '';  
                }
                if (isset($UsersAddressesDocuments->address)) {  
                        $UserAddressDocumentaddress = $UsersAddressesDocuments->address;
                    }else{
                        $UserAddressDocumentaddress = '';  
                }
                if (isset($UsersAddressesDocuments->postal_code)) {  
                        $UserAddressDocumentPostal_code = $UsersAddressesDocuments->postal_code;
                    }else{
                        $UserAddressDocumentPostal_code = '';  
                }
                if (isset($UsersAddressesDocuments->locality)) {  
                        $UserAddressDocumentLocality = $UsersAddressesDocuments->locality;
                    }else{
                        $UserAddressDocumentLocality = '';  
                }
                if (isset($UsersAddressesDocuments->province_id)) {  
                        $UserAddressDocumentProvince_id = LanguageProvince::where('province_id', $UsersAddressesDocuments->province_id)->first()->description;
                    }else{
                        $UserAddressDocumentProvince_id = '';  
                }
                 if (isset($UsersAddressesDocuments->phone_number)) {  
                        $UserAddressDocumentPhone_number = $UsersAddressesDocuments->phone_number;
                    }else{
                        $UserAddressDocumentPhone_number = ''; 
                } 
        }else{
                    if (isset($usersdatas->business_name)){
                        $UserAddressDocumentBusiness_name = $usersdatas->business_name;
                    }else{
                        $UserAddressDocumentBusiness_name = '';   
                    }
                    if (isset($usersdatas->name)){
                        $UserAddressDocument_name  = $usersdatas->name;
                    }else{
                        $UserAddressDocument_name = '';   
                    }
                    if (isset($usersdatas->surname)){
                        $UserAddressDocument_surname = $usersdatas->surname;
                    }else{
                        $UserAddressDocument_surname = '';   
                    }

                    if (isset($usersdatas->address)){
                        $UserAddressDocumentaddress  = $usersdatas->address;
                    }else{
                        $UserAddressDocumentaddress  = '';   
                    }
                    if (isset($usersdatas->postal_code)){
                        $UserAddressDocumentPostal_code = $usersdatas->postal_code;
                    }else{
                        $UserAddressDocumentPostal_code = '';  
                    }    
                    if (isset($usersdatas->country)){   
                        $UserAddressDocumentLocality = $usersdatas->country;
                    }else{
                        $UserAddressDocumentLocality ='';  
                    }
                    if (isset($usersdatas->province_id)){
                        $Province = Province::where('id', $usersdatas->province_id)->first()->abbreviation;
                        $UserAddressDocumentProvince_id = $Province;
                    }else{
                        $UserAddressDocumentProvince_id = '';
                    }
                    if (isset($usersdatas->telephone_number)){
                        $UserAddressDocumentPhone_number = $usersdatas->telephone_number;
                    }else{
                        $UserAddressDocumentPhone_number = '';
                    }
                }

                $textMailTag= MessageBL::getByCode('ORDER_CONFIRM_SUMMARY_MAIL_TEXT');
                $textObject= MessageBL::getByCode('ORDER_CONFIRM_MAIL_SUMMARY_OBJECT');
   
            #DetailMailTableOrderSales#
            $textMail = "";          
            if (strpos($textMailTag, '#DetailMailTableOrderSales#') !== false) {

            $DetailMailOrderSales= Content::where('code', 'DetailMailTableOrderSales')->get()->first();
            $contentLanguageDetailMailOrderSales= ContentLanguage::where('content_id', $DetailMailOrderSales->id)->where('language_id', HttpHelper::getLanguageId())->get()->first();    
            $tagsDetailMailOrderSales = SelenaViewsBL::getTagByType('DetailMailTableOrderSales');
            $OrderSummaryDescDocument = SalesOrder::where('id', $SalesOrder->id)->first();   
            $finalHtmlOrderSummaryDescDocuments= "";
            if (isset($OrderSummaryDescDocument)){
                foreach (DB::select(
                    'SELECT *
                    FROM sales_orders_details
                    WHERE order_id = ' . $SalesOrder->id
                    ) as $SummaryDetailsOrdersMailCustomer){
                    $newContentDetailMailOrderSales = $contentLanguageDetailMailOrderSales->content;  
                    foreach ($tagsDetailMailOrderSales as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                              
                            if (strpos($contentLanguageDetailMailOrderSales->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($SummaryDetailsOrdersMailCustomer->{$cleanTag}) && strpos($SummaryDetailsOrdersMailCustomer->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($SummaryDetailsOrdersMailCustomer->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $SummaryDetailsOrdersMailCustomer->{$cleanTag};
                                }
                                $newContentDetailMailOrderSales = str_replace($fieldsDb->tag , $tag,  $newContentDetailMailOrderSales);                                                                               
                            }
                        }
                    }      
                    if (strpos($newContentDetailMailOrderSales, '#items_description#') !== false) {
                        $ItemsDescriptionCode = Item::where('id', $SummaryDetailsOrdersMailCustomer->item_id)->first();
                        if (isset($ItemsDescriptionCode)){
                        $newContentDetailMailOrderSales = str_replace('#items_description#', $ItemsDescriptionCode->internal_code,  $newContentDetailMailOrderSales); 
                        }else{
                            $newContentDetailMailOrderSales = str_replace('#items_description#', '',  $newContentDetailMailOrderSales);           
                        }
                    }
                    if (strpos($newContentDetailMailOrderSales, '#description_language_item#') !== false) {
                        $ItemsDescriptionLanguage = ItemLanguage::where('item_id', $SummaryDetailsOrdersMailCustomer->item_id)->first();
                        if (isset($ItemsDescriptionLanguage)){
                        $newContentDetailMailOrderSales = str_replace('#description_language_item#', $ItemsDescriptionLanguage->description,  $newContentDetailMailOrderSales); 
                        }else{
                            $newContentDetailMailOrderSales = str_replace('#description_language_item#', '',  $newContentDetailMailOrderSales);           
                        }
                    }                    
                        $finalHtmlOrderSummaryDescDocuments = $finalHtmlOrderSummaryDescDocuments . $newContentDetailMailOrderSales;
                }; 
            }
                        $textMail = str_replace('#DetailMailTableOrderSales#' , $finalHtmlOrderSummaryDescDocuments, $textMailTag); 
        }

                    //dati generali
                    $SalesOrder_order_id = $SalesOrder->id;   
                    if (isset($SalesOrder->payment_type_id)) {
                        $SalesOrder_payment_type_id = LanguagePaymentType::where('payment_type_id', $SalesOrder->payment_type_id)->first()->description;   
                    }else{
                        $SalesOrder_payment_type_id = ''; 
                    }
                     if (isset($SalesOrder->carrier_id)) {
                        $SalesOrder_carrier_id = Carrier::where('id', $SalesOrder->carrier_id)->first()->name;
                    }else{
                        $SalesOrder_carrier_id = ''; 
                    }

                    if (isset($SalesOrder->carriage_paid_to_id)) {
                        $SalesOrder_carriage_paid_to_id = CarriagePaidLanguage::where('carriage_paid_to_id', $SalesOrder->carriage_paid_to_id)->first()->description;
                    }else{
                        $SalesOrder_carriage_paid_to_id = ''; 
                    }
                    if (isset($SalesOrder->date_order)) {
                        $SalesOrder_date_order = $SalesOrder->date_order;
                    }else{
                        $SalesOrder_date_order = ''; 
                    }
                    if (isset($SalesOrder->note)) {
                        $SalesOrder_note = $SalesOrder->note;
                    }else{
                        $SalesOrder_note = ''; 
                    }
                    if (isset($SalesOrder->order_state_id)) {
                        $SalesOrder_order_state_id = DocumentStatusLanguage::where('document_status_id', $SalesOrder->order_state_id)->first()->description;   
                    }else{
                        $SalesOrder_order_state_id = ''; 
                    }
                    if (isset($SalesOrder->net_amount)) {
                        $SalesOrder_net_amount = str_replace('.',',',substr($SalesOrder->net_amount, 0, -2));
                    }else{
                        $SalesOrder_net_amount = ''; 
                    }
                    if (isset($SalesOrder->shipment_amount)) {
                        $SalesOrder_shipment_amount = str_replace('.',',',substr($SalesOrder->shipment_amount, 0, -2));
                    }else{
                        $SalesOrder_shipment_amount = ''; 
                    }
                    if (isset($SalesOrder->payment_cost)) {
                        $SalesOrder_payment_cost = str_replace('.',',',substr($SalesOrder->payment_cost, 0, -2));
                    }else{
                        $SalesOrder_payment_cost = ''; 
                    }
                    if (isset($SalesOrder->vat_amount)) {
                        $SalesOrder_vat_amount =str_replace('.',',',substr($SalesOrder->vat_amount, 0, -2));
                    }else{
                        $SalesOrder_vat_amount = ''; 
                    }
                    if (isset($SalesOrder->discount_value)) {
                        $SalesOrder_discount_value =str_replace('.',',',substr($SalesOrder->discount_value, 0, -2));
                    }else{
                        $SalesOrder_discount_value = ''; 
                    }
                    if (isset($SalesOrder->total_amount)) {
                        $SalesOrder_total_amount = str_replace('.',',',substr($SalesOrder->total_amount, 0, -2));
                    }else{
                        $SalesOrder_total_amount = ''; 
                    }
                     if (isset($SalesOrder->tracking_shipment)) {
                        $SalesOrder_tracking_shipment = $SalesOrder->tracking_shipment;
                    }else{
                        $SalesOrder_tracking_shipment = ''; 
                    }


                    //$SalesOrder_payment_type_id =LanguagePaymentType::where('payment_type_id', $SalesOrder->payment_type_id)->first()->description;
                   //$SalesOrder_carrier_id = Carrier::where('id', $SalesOrder->carrier_id)->first()->name;
                  //$SalesOrder_carriage_paid_to_id = CarriagePaidLanguage::where('carriage_paid_to_id', $SalesOrder->carriage_paid_to_id)->first()->description;
                    //$SalesOrder_date_order = $SalesOrder->date_order;
                    //$SalesOrder_note = $SalesOrder->note;
                    //$SalesOrder_order_state_id = DocumentStatusLanguage::where('document_status_id', $SalesOrder->order_state_id)->first()->description;   
                    /*$SalesOrder_net_amount = str_replace('.',',',substr($SalesOrder->net_amount, 0, -2));
                    $SalesOrder_shipment_amount = str_replace('.',',',substr($SalesOrder->shipment_amount, 0, -2));
                    $SalesOrder_payment_cost = str_replace('.',',',substr($SalesOrder->payment_cost, 0, -2));
                    $SalesOrder_vat_amount =str_replace('.',',',substr($SalesOrder->vat_amount, 0, -2));
                    $SalesOrder_discount_value =str_replace('.',',',substr($SalesOrder->discount_value, 0, -2));
                    $SalesOrder_total_amount = str_replace('.',',',substr($SalesOrder->total_amount, 0, -2));
                    $SalesOrder_tracking_shipment = $SalesOrder->tracking_shipment;*/
                    //end dati ordine generale  

                    //dettaglio sped.ne ordine
                    $textMail =str_replace ('#name#', $name, $textMail);
                    $textMail =str_replace ('#surname#', $surname, $textMail);
                    $textMail =str_replace ('#business_name#', $business_name, $textMail);
                    $textMail =str_replace ('#address#', $address, $textMail);
                    $textMail =str_replace ('#postal_code#', $postal_code, $textMail);
                    $textMail =str_replace ('#country#', $country, $textMail);
                    $textMail =str_replace ('#province#', $province, $textMail);
                    $textMail =str_replace ('#telephone_number#', $telephone_number, $textMail);
                    $textMail =str_replace ('#fiscal_code#', $fiscal_code, $textMail);
                    $textMail =str_replace ('#vat_number#', $vat_number, $textMail);
                    $textMail =str_replace ('#email#', $email, $textMail);   
            
                    $textMail =str_replace ('#id#', $SalesOrder_order_id, $textMail);  
                    
                    $textMail =str_replace ('#SalesOrder_date_order#', $SalesOrder_date_order, $textMail); 
                    //dettaglio sped.ne : 
                    $textMail =str_replace ('#SalesOrder_carrier_id#', $SalesOrder_carrier_id, $textMail); 
                    $textMail =str_replace ('#SalesOrder_tracking_shipment#', $SalesOrder_tracking_shipment, $textMail); 
                    //destinazione merce
                    $textMail =str_replace ('#UserAddressGoodBusiness_name#', $UserAddressGoodBusiness_name, $textMail); 
                    $textMail =str_replace ('#UserAddressGood_surname#', $UserAddressGood_surname, $textMail); 
                    $textMail =str_replace ('#UserAddressGood_name#', $UserAddressGood_name, $textMail); 
                    $textMail =str_replace ('#UserAddressGoodAddress#', $UserAddressGoodAddress, $textMail); 
                    $textMail =str_replace ('#UserAddressGoodPostal_code#', $UserAddressGoodPostal_code, $textMail); 
                    $textMail =str_replace ('#UserAddressGoodLocality#', $UserAddressGoodLocality, $textMail); 
                    $textMail =str_replace ('#UserAddressGoodProvince_id#', $UserAddressGoodProvince_id, $textMail);   
                    $textMail =str_replace ('#UserAddressGoodPhone_number#', $UserAddressGoodPhone_number, $textMail);  

                    //riepilogo totali 
                    $textMail =str_replace ('#SalesOrder_net_amount#', $SalesOrder_net_amount, $textMail);
                    $textMail =str_replace ('#SalesOrder_shipment_amount#', $SalesOrder_shipment_amount, $textMail);
                    $textMail =str_replace ('#SalesOrder_payment_cost#', $SalesOrder_payment_cost, $textMail);
                    $textMail =str_replace ('#SalesOrder_vat_amount#', $SalesOrder_vat_amount, $textMail);
                    $textMail =str_replace ('#SalesOrder_discount_value#', $SalesOrder_discount_value, $textMail);
                    $textMail =str_replace ('#SalesOrder_total_amount#', $SalesOrder_total_amount, $textMail);
                
                    Mail::to($user->email)->send(new DynamicMail($textObject,$textMail));                  
}
//-----end send mail customer----//

public static function insertAndSendMessages(Request $request){
        $Messages= new OrderMessages();
        $Messages->order_id = $request->id;
        $Messages->id_receiver = $request->user_id;
        $Messages->id_sender = $request->sUserId;   
        $Messages->message = $request->messages;
        $Messages->save();

        $user = User::where('id', $Messages->id_receiver)->first(); 
        $order_id = $request->id;
        $id_receiver = User::where('id', $request->user_id)->first()->name;
        $id_sender =  User::where('id', $request->sUserId)->first()->name;
        $created_at = OrderMessages::where('message', $request->messages)->first()->created_at;
        $message = $request->messages;

            $emailFrom = SettingBL::getContactsEmail();
               // $emailFrom = User::where('id', $request->sUserId)->first()->email;
                $textMail= MessageBL::getByCode('ORDER_CONFIRM_MESSAGE_MAIL_TEXT');
                $textObject= MessageBL::getByCode('ORDER_CONFIRM_MESSAGE_MAIL_OBJECT');
                
                $textObject =str_replace ('#id_sender#', $id_sender, $textObject); 
                $textMail =str_replace ('#order_id#', $order_id, $textMail); 
                $textMail =str_replace ('#id_receiver#', $id_receiver, $textMail); 
                $textMail =str_replace ('#id_sender#', $id_sender, $textMail); 
                $textMail =str_replace ('#created_at#', $created_at, $textMail); 
                $textMail =str_replace ('#message#', $message, $textMail); 

                Mail::to($user->email)->send(new DynamicMail($textObject,$textMail));
    }

public static function SendShipment(Request $request){
    $SalesOrder = SalesOrder::where('id', $request->id)->first();
    
        DB::beginTransaction();
            try {
                 $SalesOrder->id = $request->id;
                 $SalesOrder->tracking_shipment = $request->tracking_shipment;
                 $SalesOrder->save();
                //end modifica codice tracking spedizione 
                    
                $UsersAddressesGoods = UsersAddresses::where('id', $SalesOrder->user_address_goods_id)->first(); 
                $user = User::where('id', $SalesOrder->user_id)->first(); 
                $carriers = Carrier::where('id', $SalesOrder->carrier_id)->first();
                $usersdatas = UsersDatas::where('user_id', $SalesOrder->user_id)->first();

                 //spedizione merce 
        if (isset($UsersAddressesGoods)){
                        if (isset($UsersAddressesGoods->business_name)) {
                            $UserAddressGoodBusiness_name = $UsersAddressesGoods->business_name;
                        }else{
                            $UserAddressGoodBusiness_name = '';
                        }
                        if (isset($UsersAddressesGoods->name)) {
                            $UserAddressGood_name = $UsersAddressesGoods->name;
                        }else{
                            $UserAddressGood_name = '';
                        }
                        if (isset($UsersAddressesGoods->surname)) {
                            $UserAddressGood_surname = $UsersAddressesGoods->surname;
                        }else{
                            $UserAddressGood_surname = '';
                        }
                        if (isset($UsersAddressesGoods->address)) {
                            $UserAddressGoodAddress = $UsersAddressesGoods->address;
                        }else{
                            $UserAddressGoodAddress = '';
                        }
                        if (isset($UsersAddressesGoods->postal_code)) {
                            $UserAddressGoodPostal_code = $UsersAddressesGoods->postal_code;
                        }else{
                            $UserAddressGoodPostal_code = '';
                        }
                        if (isset($UsersAddressesGoods->locality)) {
                            $UserAddressGoodLocality = $UsersAddressesGoods->locality;
                        }else{
                            $UserAddressGoodLocality = '';
                        }
                        if (isset($UsersAddressesGoods->province_id)) {
                            $UserAddressGoodProvince_id = LanguageProvince::where('province_id', $UsersAddressesGoods->province_id)->first()->description;
                        }else{
                            $UserAddressGoodProvince_id = '';
                        }
                        if (isset($UsersAddressesGoods->phone_number)) {
                            $UserAddressGoodPhone_number = $UsersAddressesGoods->phone_number;
                        }else{
                            $UserAddressGoodPhone_number = '';
                        }
        }else{
            if (isset($usersdatas->business_name)){
                    $UserAddressGoodBusiness_name = $usersdatas->business_name;
                }else{
                    $UserAddressGoodBusiness_name = '';   
                }
                if (isset($usersdatas->name)){
                    $UserAddressGood_name = $usersdatas->name;
                }else{
                    $UserAddressGood_name = '';   
                }
                if (isset($usersdatas->surname)){
                    $UserAddressGood_surname = $usersdatas->surname;
                }else{
                    $UserAddressGood_surname = '';   
                }

                if (isset($usersdatas->address)){
                    $UserAddressGoodAddress  = $usersdatas->address;
                }else{
                    $UserAddressGoodAddress  = '';   
                }
                if (isset($usersdatas->postal_code)){
                    $UserAddressGoodPostal_code = $usersdatas->postal_code;
                }else{
                    $UserAddressGoodPostal_code = '';  
                }    
                if (isset($usersdatas->country)){   
                    $UserAddressGoodLocality = $usersdatas->country;
                }else{
                    $UserAddressGoodLocality ='';  
                }
                if (isset($usersdatas->province_id)){
                    $Province = Province::where('id', $usersdatas->province_id)->first()->abbreviation;
                    $UserAddressGoodProvince_id = $Province;
                }else{
                    $UserAddressGoodProvince_id = '';
                }
                if (isset($usersdatas->telephone_number)){
                    $UserAddressGoodPhone_number = $usersdatas->telephone_number;
                }else{
                    $UserAddressGoodPhone_number = '';
                }
            }
               
                //dati generali
                $SalesOrder_order_id = $SalesOrder->id;   
                
                if (isset($SalesOrder->carrier_id)) {
                    $SalesOrder_carrier_id = Carrier::where('id', $SalesOrder->carrier_id)->first()->name;
                }else{
                    $SalesOrder_carrier_id = '';
                }
                if (isset($SalesOrder->date_order)) {
                    $SalesOrder_date_order = $SalesOrder->date_order;
                }else{
                    $SalesOrder_date_order = '';
                }
                if (isset($SalesOrder->tracking_shipment)) {
                    $SalesOrder_tracking_shipment = $SalesOrder->tracking_shipment;
                }else{
                    $SalesOrder_tracking_shipment = '';
                }

                
                //tracking url corriere 
                if (isset($$carriers->tracking_url)) {
                    $tracking_url = $carriers->tracking_url;
                }else{
                    $tracking_url = '';
                }
             
                $emailFrom = SettingBL::getContactsEmail();
                $textMail= MessageBL::getByCode('ORDER_CONFIRM_TRACKING_SHIPMENT_MAIL_TEXT');
                $textObject= MessageBL::getByCode('ORDER_CONFIRM_TRACKING_SHIPMENT_MAIL_OBJECT');

                //dettaglio sped.ne ordine 
                $textMail =str_replace ('#id#', $SalesOrder_order_id, $textMail);  
                $textMail =str_replace ('#SalesOrder_date_order#', $SalesOrder_date_order, $textMail); 

                //dettaglio sped.ne 
                $textMail =str_replace ('#SalesOrder_carrier_id#', $SalesOrder_carrier_id, $textMail); 
                $textMail =str_replace ('#SalesOrder_tracking_shipment#', $SalesOrder_tracking_shipment, $textMail); 
                $textMail =str_replace ('#tracking_url#', $tracking_url, $textMail); 
                
                //destinazione merce
                $textMail =str_replace ('#UserAddressGoodBusiness_name#', $UserAddressGoodBusiness_name, $textMail); 
                $textMail =str_replace ('#UserAddressGood_surname#', $UserAddressGood_surname, $textMail); 
                $textMail =str_replace ('#UserAddressGood_name#', $UserAddressGood_name, $textMail); 
                $textMail =str_replace ('#UserAddressGoodAddress#', $UserAddressGoodAddress, $textMail); 
                $textMail =str_replace ('#UserAddressGoodPostal_code#', $UserAddressGoodPostal_code, $textMail); 
                $textMail =str_replace ('#UserAddressGoodLocality#', $UserAddressGoodLocality, $textMail); 
                $textMail =str_replace ('#UserAddressGoodProvince_id#', $UserAddressGoodProvince_id, $textMail);   
                $textMail =str_replace ('#UserAddressGoodPhone_number#', $UserAddressGoodPhone_number, $textMail);

                Mail::to($user->email)->send(new DynamicMail($textObject,$textMail));
        
        DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }


public static function updateTrackingShipment(Request $request){
    $SalesOrder = SalesOrder::where('id', $request->id)->first();
    
        DB::beginTransaction();
            try {
                 $SalesOrder->id = $request->id;
                 $SalesOrder->tracking_shipment = $request->tracking_shipment;
                 $SalesOrder->save();
                   
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
  /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertMessages(Request $request)
    {
        $Messages= new OrderMessages();
        $Messages->order_id = $request->id;
        $Messages->id_receiver = $request->user_id;
        $Messages->id_sender = $request->sUserId;   
        $Messages->message = $request->messages;
        $Messages->save();
        return $Messages->id;
    }

  /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertFileOrderSales(Request $request)
    {
         DB::beginTransaction();
        try {
        $SalesOrderDetailUploadFile = new SalesOrderDetailUploadFile();

        if (isset($request->sales_order_detail_id)) {
                $SalesOrderDetailUploadFile->sales_order_detail_id = $request->sales_order_detail_id;
            }

        if (isset($request->created_id)) {
                $SalesOrderDetailUploadFile->created_id = $request->created_id;
        }

        $SalesOrderDetailUploadFile->file = static::getUrl() . $request->imgName;
            if (!is_null($request->imgName)) {
        $data = base64_decode(str_replace('data:application/pdf;base64,', '', $request->file));
                file_put_contents(static::$dirImage . $request->imgName, $data);
                }
       
        $SalesOrderDetailUploadFile->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $SalesOrderDetailUploadFile->id;
    }

    
        public static function sendEmailDetailSummaryOrder(Request $request){

            DB::beginTransaction();
            try {
               $emailFrom = SettingBL::getContactsEmail();
        
                $textMail= MessageBL::getByCode('ORDER_CONFIRM_RECEIVE_UPLOAD_FILE_MAIL_TEXT');
                $textObject= MessageBL::getByCode('ORDER_CONFIRM_RECEIVE_UPLOAD_FILE_MAIL_OBJECT');
                $salesorder=SalesOrder::where('id', $request->sales_order_id)->first();
                $salesorderdetail = SalesOrderDetail::where('order_id',$request->sales_order_id)->get()->first();
                if (isset($salesorderdetail->id)) {
                      $SalesOrderDetailUploadFile= SalesOrderDetailUploadFile::where('sales_order_detail_id', $salesorderdetail->id)->get()->first();
                }else{
                      $SalesOrderDetailUploadFile='';  
                }
                $UsersDatas = UsersDatas::where('user_id', $salesorder->user_id)->first(); 
                $user = User::where('id', $salesorder->user_id)->first(); 
                $province = Provinces::where('id', $UsersDatas->province_id)->first(); 


                if (isset($salesorder->user_address_goods_id)) {
                    $UsersAddressesGoods = UsersAddresses::where('id', $salesorder->user_address_goods_id)->first(); 
                }
                //else{
                //    $UsersAddressesGoods = '';
               // }
                if (isset($salesorder->user_address_documents_id)) {
                    $UsersAddressesDocuments = UsersAddresses::where('id', $salesorder->user_address_documents_id)->first();
                }
                //else{
                //    $UsersAddressesDocuments = '';
               // }

if (isset($UsersAddressesGoods)){
                //spedizione merce 
                if (isset($UsersAddressesGoods->business_name)) {
                    $UserAddressGoodBusiness_name = $UsersAddressesGoods->business_name;
                }else{
                    $UserAddressGoodBusiness_name = '';
                }
                if (isset($UsersAddressesGoods->name)) {
                    $UserAddressGood_name = $UsersAddressesGoods->name;
                }else{
                    $UserAddressGood_name = '';
                }
                if (isset($UsersAddressesGoods->surname)) {
                    $UserAddressGood_surname = $UsersAddressesGoods->surname;
                }else{
                    $UserAddressGood_surname = '';
                }
                if (isset($UsersAddressesGoods->address)) {
                    $UserAddressGoodAddress = $UsersAddressesGoods->address;
                }else{
                    $UserAddressGoodAddress = '';
                }
                if (isset($UsersAddressesGoods->postal_code)) {
                    $UserAddressGoodPostal_code = $UsersAddressesGoods->postal_code;
                }else{
                    $UserAddressGoodPostal_code = '';
                }
                if (isset($UsersAddressesGoods->locality)) {
                    $UserAddressGoodLocality = $UsersAddressesGoods->locality;
                }else{
                    $UserAddressGoodLocality = '';
                }
                if (isset($UsersAddressesGoods->province_id)) {
                    $UserAddressGoodProvince_id = Province::where('id', $UsersAddressesGoods->province_id)->first()->abbreviation;
                }else{
                    $UserAddressGoodProvince_id = '';
                }
                if (isset($UsersAddressesGoods->phone_number)) {
                    $UserAddressGoodPhone_number = $UsersAddressesGoods->phone_number;
                }else{
                    $UserAddressGoodPhone_number = '';
                }

}else{
            if (isset($UsersDatas->business_name)){
                    $UserAddressGoodBusiness_name = $UsersDatas->business_name;
                }else{
                    $UserAddressGoodBusiness_name = '';   
                }
                if (isset($UsersDatas->name)){
                    $UserAddressGood_name = $UsersDatas->name;
                }else{
                    $UserAddressGood_name = '';   
                }
                if (isset($UsersDatas->surname)){
                    $UserAddressGood_surname = $UsersDatas->surname;
                }else{
                    $UserAddressGood_surname = '';   
                }

                if (isset($UsersDatas->address)){
                    $UserAddressGoodAddress  = $UsersDatas->address;
                }else{
                    $UserAddressGoodAddress  = '';   
                }
                if (isset($UsersDatas->postal_code)){
                    $UserAddressGoodPostal_code = $UsersDatas->postal_code;
                }else{
                    $UserAddressGoodPostal_code = '';  
                }    
                if (isset($UsersDatas->country)){   
                    $UserAddressGoodLocality = $UsersDatas->country;
                }else{
                    $UserAddressGoodLocality ='';  
                }
                if (isset($UsersDatas->province_id)){
                    $Province = Province::where('id', $UsersDatas->province_id)->first()->abbreviation;
                    $UserAddressGoodProvince_id = $Province;
                }else{
                    $UserAddressGoodProvince_id = '';
                }
                if (isset($UsersDatas->telephone_number)){
                    $UserAddressGoodPhone_number = $UsersDatas->telephone_number;
                }else{
                    $UserAddressGoodPhone_number = '';
                }
            }


 if (isset($UsersAddressesDocuments)) {
                //spedizione documenti
                if (isset($UsersAddressesDocuments->surname)) {
                    $UserAddressDocumentsurname = $UsersAddressesDocuments->surname;
                }else{
                    $UserAddressDocumentsurname = '';
                }
                if (isset($UsersAddressesDocuments->name)) {
                    $UserAddressDocumentname = $UsersAddressesDocuments->name;
                }else{
                    $UserAddressDocumentname = '';
                } 
                if (isset($UsersAddressesDocuments->business_name)) {
                    $UserAddressDocumentBusiness_name = $UsersAddressesDocuments->business_name;
                }else{
                    $UserAddressDocumentBusiness_name = '';
                }
                if (isset($UsersAddressesDocuments->address)) {
                    $UserAddressDocumentaddress = $UsersAddressesDocuments->address;
                }else{
                    $UserAddressDocumentaddress = '';
                }
                if (isset($UsersAddressesDocuments->postal_code)) {
                    $UserAddressDocumentPostal_code = $UsersAddressesDocuments->postal_code;
                }else{
                    $UserAddressDocumentPostal_code = '';
                }
                if (isset($UsersAddressesDocuments->locality)) {
                     $UserAddressDocumentLocality = $UsersAddressesDocuments->locality;
                }else{
                    $UserAddressDocumentLocality = '';
                }
                if (isset($UsersAddressesDocuments->province_id)) {
                     $UserAddressDocumentProvince_id = Province::where('id', $UsersAddressesDocuments->province_id)->first()->abbreviation;
                }else{
                    $UserAddressDocumentProvince_id = '';
                }
                if (isset($UsersAddressesDocuments->phone_number)) {
                     $UserAddressDocumentPhone_number = $UsersAddressesDocuments->phone_number;
                }else{
                    $UserAddressDocumentPhone_number = '';
                }
}else{
              
            if (isset($UsersDatas->business_name)){
                    $UserAddressDocumentBusiness_name = $UsersDatas->business_name;
                }else{
                    $UserAddressDocumentBusiness_name = '';   
                }
                if (isset($UsersDatas->name)){
                    $UserAddressDocumentname = $UsersDatas->name;
                }else{
                    $UserAddressDocumentname = '';   
                }
                if (isset($UsersDatas->surname)){
                    $UserAddressDocumentsurname = $UsersDatas->surname;
                }else{
                    $UserAddressDocumentsurname = '';   
                }

                if (isset($UsersDatas->address)){
                    $UserAddressDocumentaddress  = $UsersDatas->address;
                }else{
                    $UserAddressDocumentaddress  = '';   
                }
                if (isset($UsersDatas->postal_code)){
                    $UserAddressDocumentPostal_code = $UsersDatas->postal_code;
                }else{
                    $UserAddressDocumentPostal_code = '';  
                }    
                if (isset($UsersDatas->country)){   
                    $UserAddressDocumentLocality = $UsersDatas->country;
                }else{
                    $UserAddressDocumentLocality ='';  
                }
                if (isset($UsersDatas->province_id)){
                    $Province = Province::where('id', $UsersDatas->province_id)->first()->abbreviation;
                    $UserAddressDocumentProvince_id = $Province;
                }else{
                    $UserAddressDocumentProvince_id = '';
                }
                if (isset($UsersDatas->telephone_number)){
                    $UserAddressDocumentPhone_number = $UsersDatas->telephone_number;
                }else{
                    $UserAddressDocumentPhone_number = '';
                }
            }

                    $SalesOrder_net_amount = str_replace('.',',',substr($salesorder->net_amount, 0, -2));
                    $SalesOrder_shipment_amount = str_replace('.',',',substr($salesorder->shipment_amount, 0, -2));
                    $SalesOrder_payment_cost = str_replace('.',',',substr($salesorder->payment_cost, 0, -2));
                    $SalesOrder_vat_amount =str_replace('.',',',substr($salesorder->vat_amount, 0, -2));
                    $SalesOrder_discount_value =str_replace('.',',',substr($salesorder->discount_value, 0, -2));
                    $SalesOrder_total_amount = str_replace('.',',',substr($salesorder->total_amount, 0, -2));
              

                $textMail =str_replace ('#UsersAddressesGoodsBusinessName#', $UserAddressGoodBusiness_name, $textMail); 
                $textMail =str_replace ('#UsersAddressesGoodsName#', $UserAddressGood_name, $textMail); 
                $textMail =str_replace ('#UsersAddressesGoodsSurname#', $UserAddressGood_surname, $textMail); 
                $textMail =str_replace ('#UsersAddressesGoodsAddress#', $UserAddressGoodAddress, $textMail); 
                $textMail =str_replace ('#UsersAddressesGoodsPostalCode#', $UserAddressGoodPostal_code, $textMail); 
                $textMail =str_replace ('#UsersAddressesGoodsLocality#', $UserAddressGoodLocality, $textMail); 
                $textMail =str_replace ('#UsersAddressesGoodsProvince#', $UserAddressGoodProvince_id, $textMail); 
                $textMail =str_replace ('#UsersAddressesGoodsPhoneNumber#', $UserAddressGoodPhone_number, $textMail); 

                $textMail =str_replace ('#UsersAddressesDocumentsBusinessName#', $UserAddressDocumentBusiness_name, $textMail); 
                $textMail =str_replace ('#UsersAddressesDocumentsName#', $UserAddressDocumentname, $textMail); 
                $textMail =str_replace ('#UsersAddressesDocumentsSurname#', $UserAddressDocumentsurname, $textMail); 
                $textMail =str_replace ('#UsersAddressesDocumentsAddress#', $UserAddressDocumentaddress, $textMail); 
                $textMail =str_replace ('#UsersAddressesDocumentsPostalCode#', $UserAddressDocumentPostal_code, $textMail); 
                $textMail =str_replace ('#UsersAddressesDocumentsLocality#', $UserAddressDocumentLocality, $textMail); 
                $textMail =str_replace ('#UsersAddressesDocumentsProvince#', $UserAddressDocumentProvince_id, $textMail); 
                $textMail =str_replace ('#UsersAddressesDocumentsPhoneNumber#', $UserAddressDocumentPhone_number, $textMail); 

                $textObject = str_replace ('#order_id#', $salesorderdetail->order_id, $textObject); 
                $textMail =str_replace ('#order_id#', $salesorderdetail->order_id, $textMail); 
                $textMail =str_replace ('#date_order#', $salesorder->date_order, $textMail);

                $textMail =str_replace ('#file#', $SalesOrderDetailUploadFile, $textMail); 

                $textMail =str_replace ('#user_name#', $UsersDatas->name, $textMail); 
                $textMail =str_replace ('#user_surname#', $UsersDatas->surname, $textMail);
                $textMail =str_replace ('#business_name#', $UsersDatas->business_name, $textMail);
                $textMail =str_replace ('#address#', $UsersDatas->address, $textMail);
                $textMail =str_replace ('#postal_code#', $UsersDatas->postal_code, $textMail);
                $textMail =str_replace ('#country#', $UsersDatas->country, $textMail);
                $textMail =str_replace ('#province#', $province->abbreviation, $textMail);
                $textMail =str_replace ('#telephone_number#', $UsersDatas->telephone_number, $textMail);
                $textMail =str_replace ('#email#', $user->email, $textMail);

                 //riepilogo totali 
                    $textMail =str_replace ('#SalesOrder_net_amount#', $SalesOrder_net_amount, $textMail);
                    $textMail =str_replace ('#SalesOrder_shipment_amount#', $SalesOrder_shipment_amount, $textMail);
                    $textMail =str_replace ('#SalesOrder_payment_cost#', $SalesOrder_payment_cost, $textMail);
                    $textMail =str_replace ('#SalesOrder_vat_amount#', $SalesOrder_vat_amount, $textMail);
                    $textMail =str_replace ('#SalesOrder_discount_value#', $SalesOrder_discount_value, $textMail);
                    $textMail =str_replace ('#SalesOrder_total_amount#', $SalesOrder_total_amount, $textMail);
           
                //riga dettaglio ordine
                  #DetailMailTableOrderSales#
            //$textMail = "";        
            if (strpos($textMail, '#DetailMailTableOrderSales#') !== false) {

            $DetailMailOrderSales= Content::where('code', 'DetailMailTableOrderSales')->get()->first();
            $contentLanguageDetailMailOrderSales= ContentLanguage::where('content_id', $DetailMailOrderSales->id)->where('language_id', HttpHelper::getLanguageId())->get()->first();    
            $tagsDetailMailOrderSales = SelenaViewsBL::getTagByType('DetailMailTableOrderSales');
            $OrderSummaryDescDocument = SalesOrder::where('id', $salesorder->id)->first();   
            $finalHtmlOrderSummaryDescDocuments= "";
            if (isset($OrderSummaryDescDocument)){
                foreach (DB::select(
                    'SELECT *
                    FROM sales_orders_details
                    WHERE order_id = ' . $salesorder->id
                    ) as $SummaryDetailsOrdersMail){
                    $newContentDetailMailOrderSales = $contentLanguageDetailMailOrderSales->content;  
                    foreach ($tagsDetailMailOrderSales as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                              
                            if (strpos($contentLanguageDetailMailOrderSales->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($SummaryDetailsOrdersMail->{$cleanTag}) && strpos($SummaryDetailsOrdersMail->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($SummaryDetailsOrdersMail->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $SummaryDetailsOrdersMail->{$cleanTag};
                                }
                                $newContentDetailMailOrderSales = str_replace($fieldsDb->tag , $tag,  $newContentDetailMailOrderSales);                                                                               
                            }
                        }
                    }      
                    if (strpos($newContentDetailMailOrderSales, '#items_description#') !== false) {
                        $ItemsDescriptionCode = Item::where('id', $SummaryDetailsOrdersMail->item_id)->first();
                        if (isset($ItemsDescriptionCode)){
                        $newContentDetailMailOrderSales = str_replace('#items_description#', $ItemsDescriptionCode->internal_code,  $newContentDetailMailOrderSales); 
                        }else{
                            $newContentDetailMailOrderSales = str_replace('#items_description#', '',  $newContentDetailMailOrderSales);           
                        }
                    }
                    if (strpos($newContentDetailMailOrderSales, '#description_language_item#') !== false) {
                        $ItemsDescriptionLanguage = ItemLanguage::where('item_id', $SummaryDetailsOrdersMail->item_id)->first();
                        if (isset($ItemsDescriptionLanguage)){
                        $newContentDetailMailOrderSales = str_replace('#description_language_item#', $ItemsDescriptionLanguage->description,  $newContentDetailMailOrderSales); 
                        }else{
                            $newContentDetailMailOrderSales = str_replace('#description_language_item#', '',  $newContentDetailMailOrderSales);           
                        }
                    }  
                    
                    if (strpos($newContentDetailMailOrderSales, '#support_id_description#') !== false) {
                        $DescriptionSupport = Supports::where('id', $SummaryDetailsOrdersMail->support_id)->first();
                        if (isset($DescriptionSupport)){
                        $newContentDetailMailOrderSales = str_replace('#support_id_description#', $DescriptionSupport->description,  $newContentDetailMailOrderSales); 
                        }else{
                            $newContentDetailMailOrderSales = str_replace('#support_id_description#', '',  $newContentDetailMailOrderSales);           
                        }
                    }

                    if (strpos($newContentDetailMailOrderSales, '#DetailMailUploadFileSalesOrder#') !== false) {
                        $SalesOrderDetailUploadFile = SalesOrderDetailUploadFile::where('sales_order_detail_id', $SummaryDetailsOrdersMail->id)->first();
                        if (isset($SalesOrderDetailUploadFile)){
                        $newContentDetailMailOrderSales = str_replace('#DetailMailUploadFileSalesOrder#', $SalesOrderDetailUploadFile->file,  $newContentDetailMailOrderSales); 
                        }else{
                            $newContentDetailMailOrderSales = str_replace('#DetailMailUploadFileSalesOrder#', '',  $newContentDetailMailOrderSales);           
                        }
                    }
                        $finalHtmlOrderSummaryDescDocuments = $finalHtmlOrderSummaryDescDocuments . $newContentDetailMailOrderSales;
                }; 
            }
                        $textMail = str_replace('#DetailMailTableOrderSales#' , $finalHtmlOrderSummaryDescDocuments, $textMail); 
        }

                Mail::to('info@lineaprint.it')->send(new DynamicMail($textObject,$textMail));  

  
        DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }


  private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/File/';
    }

 /**
     * delete Items Photos
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function deleteOrderDetail(int $id, int $idLanguage)
    {
        $SalesOrderDetail = SalesOrderDetail::find($id);


        if (is_null($SalesOrderDetail)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($SalesOrderDetail)) {

            DB::beginTransaction();

            try {

                SalesOrderDetail::where('id', $id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }
//end function

    
     #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id)
    {
        $SalesOrder = SalesOrder::find($id);

        if (!is_null($SalesOrder)) {

            DB::beginTransaction();

            try {

                $SalesOrderDetail = SalesOrderDetail::find($id);
                if (!is_null($SalesOrderDetail)) {
                    SalesOrderDetail::where('id', $id)->delete();
                }

                $SalesOrder->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
        

        
    }

    #endregion DELETE

}

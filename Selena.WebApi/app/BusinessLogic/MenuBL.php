<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoLanguageMenu;
use App\DtoModel\DtoMenu;
use App\DtoModel\DtoPageSetting;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Enums\MenusTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\LogHelper;
use App\Helpers\ModelHelper;
use App\LanguageMenu;
use App\Menu;
use App\LanguagePage;
use App\Page;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Symfony\Component\HttpFoundation\Request;
use App\DtoModel\DtoPageMenuList;

class MenuBL
{
    #region PRIVATE

    /**
     * Validate data request
     *
     * @param array dtoMenu
     * @param int idLanguage
     * @param int idMenuType
     * @param string dbOperationType
     */
    private static function _validateData(Request $dtoMenu, $idLanguage, $idMenuType, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE && is_null($dtoMenu->id)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($dtoMenu->languagesMenus) && $dtoMenu->languagesMenus->contains('url', null) && $idMenuType == MenusTypesEnum::Link) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UrlNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        foreach ($dtoMenu->languagesMenus as $languageMenu) {
            if (is_null($languageMenu['title'])) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TitleNotFound), HttpResultsCodesEnum::InvalidPayload);
            }

            if (is_null($languageMenu['idLanguage']) || is_null(LanguageBL::get($languageMenu['idLanguage']))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::LanguageNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }
    }

    /**
     * Check configuration folder
     *
     * @param MenusTypeEnum idMenuType
     * @param DtoMenu dtoMenu
     * @param DtoCondiguration dtoFunctionConfiguration
     * @param DtoMenu dtoOnDB
     * @return DtoMenu
     */
    private static function _checkConfiguration($idMenuType, $dtoMenu, $dtoFunctionConfiguration, $dtoOnDB = null)
    {
        switch ($idMenuType) {
            case MenusTypesEnum::Folder:
                $tabConfiguration = $dtoFunctionConfiguration->tabsConfigurations->where('code', 'Folder')->first()->pageConfigurations;
                break;
            case MenusTypesEnum::Link:
                //$tabConfiguration = $dtoFunctionConfiguration->tabsConfigurations->where('code', 'Link')->first()->pageConfigurations;

                break;
        }

        $dtoMenuToReturn = ModelHelper::checkConfiguration(new DtoMenu(), $dtoMenu, $tabConfiguration, 'languagesMenus', new DtoLanguageMenu);

        if (!is_null($dtoMenu->order)) {
            $dtoMenuToReturn->order = $dtoMenu->order;
        }

        return $dtoMenuToReturn;
    }

    /**
     * Get max order
     *
     * @return int
     */
    private static function _getMaxOrder()
    {
        $order = Menu::max('order');

        if (is_null($order)) {
            $order = 0;
        }

        return $order;
    }

    /**
     * Set order menu list
     *
     * @param Request $dtoPageMenuList
     */
    private static function _setOrderMenuList(Request $dtoPageMenuLists)
    {
        for ($i = 0; count($dtoPageMenuLists->all()) > $i; $i++) {
            if(isset($dtoPageMenuLists[$i])){
                $menu = Menu::find($dtoPageMenuLists[$i]['idMenu']);
                $menu->order = $i;
                $menu->menu_id = null;
                $menu->save();

                if (count($dtoPageMenuLists[$i]['subMenu']) > 0) {
                    static::_setOrderSubMenuList($dtoPageMenuLists[$i]['subMenu'], $dtoPageMenuLists[$i]['idMenu']);
                }
            }
        }
    }

    /**
     * Set order sub menu
     *
     * @param DtoPageMenuList $dtoPageMenuLists
     * @param int $idMenuFather
     */
    private static function _setOrderSubMenuList($dtoPageMenuLists, int $idMenuFather)
    {
        for ($i = 0; count($dtoPageMenuLists) > $i; $i++) {
            $menu = Menu::find($dtoPageMenuLists[$i]['idMenu']);
            $menu->order = $i;
            $menu->menu_id = $idMenuFather;
            $menu->save();

            if (count($dtoPageMenuLists[$i]['subMenu']) > 0) {
                static::_setOrderSubMenuList($dtoPageMenuLists[$i]['subMenu'], $dtoPageMenuLists[$i]['idMenu']);
            }
        }
    }

    #region CONVERT

    /**
     * Convert from DtoMenu to Model
     *
     * @param DtoMenu dtoMenu
     * @return Menu
     */
    private static function _convertToModel($dtoMenu)
    {
        $menu = new Menu();
        $menu->page_id = isset($dtoMenu->idPage) ? $dtoMenu->idPage : null;
        $menu->new_window = isset($dtoMenu->newWindow) ? $dtoMenu->newWindow : false;
        $menu->order = $dtoMenu->order;
        $menu->icon_id = isset($dtoMenu->idIcon) ? $dtoMenu->idIcon : null;
        $menu->hide_mobile = isset($dtoMenu->hideMobile) ? $dtoMenu->hideMobile : false;
        $menu->menu_id = isset($dtoMenu->idMenu) ? $dtoMenu->idMenu : null;

        return $menu;
    }

    /**
     * Convert Model to DtoMenu
     *
     * @param Menu menu
     * @return DtoMenu
     */
    private static function _convertToDto($menu)
    {
        $dtoMenu = new DtoMenu();
        $dtoMenu->id = $menu->id;
        $dtoMenu->idPage = $menu->page_id;
        $dtoMenu->newWindow = $menu->new_window;
        $dtoMenu->idIcon = $menu->icon_id;
        $dtoMenu->hideMobile = $menu->hide_mobile;
        $dtoMenu->order = $menu->order;

        foreach (LanguageMenu::where('menu_id', $menu->id)->get() as $languageMenu) {
            $dtoLanguageMenu = new DtoLanguageMenu();
            $dtoLanguageMenu->idLanguage = $languageMenu->language_id;
            $dtoLanguageMenu->title = $languageMenu->description;
            $dtoLanguageMenu->url = $languageMenu->link;
            $dtoMenu->languagesMenus->push($dtoLanguageMenu);
        }

        return $dtoMenu;
    }

    /**
     * Convert from DtoPageSetting to Model
     *
     * @param DtoPageSetting $dtoPageSetting
     * @return \App\Menu
     */
    private static function _convertFromDtoPageSetting($dtoPageSetting)
    {
        $menu = new Menu();
        $menu->icon_id = $dtoPageSetting->idIcon;
        $menu->page_id = $dtoPageSetting->id;
        $menu->hide_mobile = $dtoPageSetting->hideMenuMobile;
        return $menu;
    }

    #endregion CONVERT

    /**
     * Convert from DtoPageSetting to Model
     *
     * @param DtoPageSetting $dtoPageSetting
     * @return \App\Menu
     */
    private static function _convertFromDtoPageSettingPage(DtoPageSetting $dtoPageSetting)
    {
        $menu = new Menu();
        $menu->icon_id = $dtoPageSetting->idIcon;
        $menu->page_id = $dtoPageSetting->id;
        $menu->hide_mobile = $dtoPageSetting->hideMenuMobile;
        return $menu;
    }


    #region CRUD

    /**
     * Insert
     *
     * @param array dtoMenu
     * @param int idUser
     * @param int idLanguage
     * @param MenusTypesEnum idMenuType
     * @return int new_menu_id
     */
    private static function _insert(Request $dtoMenu, $idUser, $idLanguage, $idMenuType)
    {
        DB::beginTransaction();

        try {
            static::_validateData($dtoMenu, $idLanguage, $idMenuType, DbOperationsTypesEnum::INSERT);

            $config = FieldUserRoleBL::getConfiguration($dtoMenu->idFunctionality, $idUser, $idLanguage);
            $dtoMenuToInsert = static::_checkConfiguration($idMenuType, $dtoMenu, $config);

            if (is_null($dtoMenu->order)) {
                $dtoMenuToInsert->order = static::_getMaxOrder() + 10;
            }

            $menu = static::_convertToModel($dtoMenuToInsert);
            $menu->save();
            $dtoMenuToInsert->id = $menu->id;

            foreach ($dtoMenuToInsert->languagesMenus as $dtoLanguageMenu) {
                LanguageMenuBL::insertOrUpdateDto($dtoLanguageMenu, $dtoMenuToInsert->id, $idLanguage);
            }

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $menu->id;
    }

    /**
     * Update
     *
     * @param array dtoMenu
     * @param int idUser
     * @param int idLanguage
     * @param MenusTypesEnum idMenuType
     */
    private static function _update(Request $dtoMenu, $idUser, $idLanguage, $idMenuType)
    {
        DB::beginTransaction();

        try {
            #region CHECK

            static::_validateData($dtoMenu, $idLanguage, $idMenuType, DbOperationsTypesEnum::UPDATE);

            $menuOnDB = Menu::find($dtoMenu->id);

            if (
                is_null($menuOnDB) || isset($menuOnDB->page_id)
                || ($idMenuType == MenusTypesEnum::Folder && LanguageMenu::where('menu_id', $dtoMenu->id)->whereNotNull('link')->count() > 0)
            ) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::MenuNotExist), HttpResultsCodesEnum::InvalidPayload);
            }

            #endregion CHECK

            $dtoMenuOnDB = static::_convertToDto($menuOnDB);
            $config = FieldUserRoleBL::getConfiguration($dtoMenu->idFunctionality, $idUser, $idLanguage);
            $dtoMenuToUpdate = static::_checkConfiguration($idMenuType, $dtoMenu, $config, $dtoMenuOnDB);

            if (is_null($dtoMenu->order)) {
                $dtoMenuToUpdate->order = static::_getMaxOrder() + 10;
            }

            DBHelper::updateAndSave(static::_convertToModel($dtoMenuToUpdate), $menuOnDB);

            foreach ($dtoMenuToUpdate->languagesMenus as $dtoLanguageMenu) {
                LanguageMenuBL::insertOrUpdateDto($dtoLanguageMenu, $dtoMenu->id, $idLanguage);
            }

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion CRUD

    #endregion PRIVATE

    #region GET

    /**
     * Get setting
     *
     * @param int id
     * @return array
     */
    public static function getSettings($id)
    {
        return static::_convertToDto(Menu::find($id));
    }

    /**
     * Return menu by idPage
     *
     * @param int $idPage
     * @return \App\Menu
     */
    public static function getByIdPage(int $idPage)
    {
        return Menu::where('page_id', $idPage)->first();
    }

    #endregion GET

    #region INSERT

    /**
     * Insert link
     *
     * @param array dtoMenu
     * @param int idUser
     * @param int idLanguage
     * @return int new_menu_id
     */
    public static function insertLink(Request $dtoMenu, $idUser, $idLanguage)
    {
        return static::_insert($dtoMenu, $idUser, $idLanguage, MenusTypesEnum::Link);
    }

     /**
     * Insert link
     *
     * @param array dtoMenu
     * @param int idUser
     * @param int idLanguage
     * @return int new_menu_id
     */
    public static function insertNewLink(Request $dtoMenu, $idUser, $idLanguage)
    {
        DB::beginTransaction();

        try {

            $page = new Page();
            $page->created_id = $idUser;
            $page->blocked = false;
            $page->page_type = "Link";
            $page->online = true;
            $page->save();


            $languagePage = new LanguagePage();
            $languagePage->page_id = $page->id;
            $languagePage->language_id = $dtoMenu->idLanguage;
            $languagePage->title = $dtoMenu->description;
            $languagePage->url = $dtoMenu->url;
            $languagePage->save();


            $menu = new Menu();
            $menu->page_id = $page->id;
            $menu->new_window = isset($dtoMenu->newWindow) ? $dtoMenu->newWindow : false;
            $menu->order = 1000;
            $menu->icon_id = isset($dtoMenu->idIcon) ? $dtoMenu->idIcon : null;
            $menu->hide_mobile = isset($dtoMenu->hideMobile) ? $dtoMenu->hideMobile : false;
            $menu->menu_id = isset($dtoMenu->idMenu) ? $dtoMenu->idMenu : null;
            $menu->save();
        
            $languageMenu = new LanguageMenu();
            $languageMenu->menu_id = $menu->id;
            $languageMenu->language_id = isset($dtoMenu->idLanguage) ? $dtoMenu->idLanguage : null;
            $languageMenu->description = isset($dtoMenu->description) ? $dtoMenu->description : null;
            $languageMenu->link = isset($dtoMenu->url) ? $dtoMenu->url : null;
            $languageMenu->save();

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $menu->id;


    }

    /**
     * Insert link
     *
     * @param array dtoMenu
     * @param int idUser
     * @param int idLanguage
     * @return int new_menu_id
     */
    public static function updateNewlink(Request $dtoMenu, $idUser, $idLanguage)
    {

        $languagePageOnDb = LanguagePage::where('page_id', $dtoMenu->id)->where('language_id', $dtoMenu->idLanguage)->get()->first();
        if(!isset($languagePageOnDb)){
            throw new Exception('Ops qualcosa è andato storto!', HttpResultsCodesEnum::InvalidPayload);
        }

        DB::beginTransaction();

        try {

            $languagePageOnDb->title = $dtoMenu->description;
            $languagePageOnDb->url = $dtoMenu->url;
            $languagePageOnDb->save();


            $menuOnDb = Menu::where('page_id', $dtoMenu->id)->get()->first();
            if(isset($menuOnDb)){
                $menuOnDb->new_window = isset($dtoMenu->newWindow) ? $dtoMenu->newWindow : false;
                $menuOnDb->save();

                $languageMenuOnDb = LanguageMenu::where('menu_id', $menuOnDb->id)->get()->first();
                $languageMenuOnDb->description = isset($dtoMenu->description) ? $dtoMenu->description : null;
                $languageMenuOnDb->link = isset($dtoMenu->url) ? $dtoMenu->url : null;
                $languageMenuOnDb->save();
            }       
            

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $languagePageOnDb->page_id;


    }

    /**
     * Insert folder
     *
     * @param array dtoMenu
     * @param int idUser
     * @param int idLanguage
     * @return int new_menu_id
     */
    public static function insertFolder(Request $dtoMenu, $idUser, $idLanguage)
    {
        return static::_insert($dtoMenu, $idUser, $idLanguage, MenusTypesEnum::Folder);
    }

    #endregion INSERT

    #region INSERT OR UPDATE

    /**
     * Insert or update page menu
     *
     * @param DtoPageSetting $dtoPageSetting
     * @param int $idLanguage
     * @return int
     */
    public static function insertOrUpdatePage($dtoPageSetting, int $idLanguage)
    {

        $menu = Menu::where('page_id', $dtoPageSetting->id);
        $count = $menu->count();
        if ($count > 1) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::MoreLanguageMenuForSameLanguageMenuFound), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if ($count) {
                $menu = $menu->first();
                if ($dtoPageSetting->page_type == 'Page') {
                    DBHelper::updateAndSave(static::_convertFromDtoPageSettingPage($dtoPageSetting), $menu);
                } else {
                    DBHelper::updateAndSave(static::_convertFromDtoPageSetting($dtoPageSetting), $menu);
                }
            } else {
                $menu = static::_convertFromDtoPageSetting($dtoPageSetting);
                $menu->order = static::_getMaxOrder() + 10;
                $menu->save();
            }
        }

        return $menu->id;
    }
    #endregion INSERT OR UPDATE

    #region UPDATE

    /**
     * Update link
     *
     * @param array dtoMenu
     * @param int idUser
     * @param int idLanguage
     */
    public static function updateLink(Request $dtoMenu, $idUser, $idLanguage)
    {
        static::_update($dtoMenu, $idUser, $idLanguage, MenusTypesEnum::Link);
    }

    /**
     * Update folder
     *
     * @param array dtoMenu
     * @param int idUser
     * @param int idLanguage
     */
    public static function updateFolder(Request $dtoMenu, $idUser, $idLanguage)
    {
        static::_update($dtoMenu, $idUser, $idLanguage, MenusTypesEnum::Folder);
    }

    /**
     * Set order menu list
     *
     * @param Request $dtoPageMenuList
     * @param int $idUser
     * @param int $idLanguage
     */
    public static function setOrderMenuList(Request $dtoPageMenuLists)
    {
        static::_setOrderMenuList($dtoPageMenuLists);
    }

    #endregion UPDATE

    #region DELETE

    public static function delete(int $id)
    {
        LanguageMenuBL::deleteByIdMenu($id);
        Menu::find($id)->delete();
    }

    #endregion DELETE
}

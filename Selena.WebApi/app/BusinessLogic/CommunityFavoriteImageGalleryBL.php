<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoCommunityFavoriteImageGallery;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\CommunityFavoriteImageGallery;
use Exception;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;

class CommunityFavoriteImageGalleryBL
{
    #region PRIVATE

    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request dtoCommunityFavoriteImageGallery
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationsTypesEnum)
    {

       

    }

    /**
     * Convert to dto
     * 
     * @param CommunityFavoriteImageGallery CommunityFavoriteImageGallery
     * @return DtoCommunityFavoriteImageGallery
     */
    private static function _convertToDto($CommunityFavoriteImageGallery)
    {
        $dtoCommunityFavoriteImageGallery = new DtoCommunityFavoriteImageGallery();

        if (isset($CommunityFavoriteImageGallery->id)) {
            $dtoCommunityFavoriteImageGallery->id = $CommunityFavoriteImageGallery->id;
        }

        if (isset($CommunityFavoriteImageGallery->user_id)) {
            $dtoCommunityFavoriteImageGallery->user_id = $CommunityFavoriteImageGallery->user_id;
        }

        if (isset($CommunityFavoriteImageGallery->name)) {
            $dtoCommunityFavoriteImageGallery->name = $CommunityFavoriteImageGallery->name;
        }

        if (isset($CommunityFavoriteImageGallery->description)) {
            $dtoCommunityFavoriteImageGallery->description = $CommunityFavoriteImageGallery->description;
        }

        return $dtoCommunityFavoriteImageGallery;
    }


    /**
     * Convert to VatType
     * 
     * @param dtoCommunityFavoriteImageGallery dtoCommunityFavoriteImageGallery
     * 
     * @return CommunityFavoriteImageGallery
     */
    private static function _convertToModel($dtoCommunityFavoriteImageGallery)
    {
        $CommunityFavoriteImageGallery = new CommunityFavoriteImageGallery();

        if (isset($dtoCommunityFavoriteImageGallery->id)) {
            $CommunityFavoriteImageGallery->id = $dtoCommunityFavoriteImageGallery->id;
        }

        if (isset($dtoCommunityFavoriteImageGallery->user_id)) {
            $CommunityFavoriteImageGallery->user_id = $dtoCommunityFavoriteImageGallery->user_id;
        }

        if (isset($dtoCommunityFavoriteImageGallery->name)) {
            $CommunityFavoriteImageGallery->name = $dtoCommunityFavoriteImageGallery->name;
        }

        if (isset($dtoCommunityFavoriteImageGallery->description)) {
            $CommunityFavoriteImageGallery->description = $dtoCommunityFavoriteImageGallery->description;
        }

        return $CommunityFavoriteImageGallery;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCommunityction
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(CommunityFavoriteImageGallery::find($id));
    }

    /*
     * Get all
     * 
     * @return DtoCommunityFavoriteImageGallery
     */
    public static function getAll()
    {

        $result = collect(); 

        foreach (CommunityFavoriteImageGallery::orderBy('id','DESC')->get() as $communityFavoriteImageGallery) {

            $DtoCommunityFavoriteImageGallery = new DtoCommunityFavoriteImageGallery();

            $DtoCommunityFavoriteImageGallery->id = $communityFavoriteImageGallery->id;
            $DtoCommunityFavoriteImageGallery->name = $communityFavoriteImageGallery->name;
            $DtoCommunityFavoriteImageGallery->user_id = $communityFavoriteImageGallery->user_id;
            $DtoCommunityFavoriteImageGallery->description = $communityFavoriteImageGallery->description;
            
            $result->push($DtoCommunityFavoriteImageGallery); 
        }
        return $result;

    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoCommunityFavoriteImageGallery
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoCommunityFavoriteImageGallery)
    {
        
        static::_validateData($DtoCommunityFavoriteImageGallery, $idLanguage, DbOperationsTypesEnum::INSERT);
        $CommunityFavoriteImageGallery = static::_convertToModel($DtoCommunityFavoriteImageGallery);

        $CommunityFavoriteImageGallery->save();

        return $CommunityFavoriteImageGallery->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoCommunityFavoriteImageGallery
     * @param int idLanguage
     */
    public static function update(Request $DtoCommunityFavoriteImageGallery)
    {
        static::_validateData($DtoCommunityFavoriteImageGallery, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $CommunityFavoriteImageGallery = CommunityFavoriteImageGallery::find($DtoCommunityFavoriteImageGallery->id);
        DBHelper::updateAndSave(static::_convertToModel($DtoCommunityFavoriteImageGallery), $CommunityFavoriteImageGallery);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id)
    {
        $CommunityFavoriteImageGallery = CommunityFavoriteImageGallery::find($id);

        if (is_null($CommunityFavoriteImageGallery)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $CommunityFavoriteImageGallery->delete();
    }

    #endregion DELETE
}

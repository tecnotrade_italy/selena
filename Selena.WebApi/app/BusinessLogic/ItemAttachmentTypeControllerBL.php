<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoItemAttachmentLanguageTypeTable;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\ItemAttachmentLanguageTypeTable;
use App\ItemAttachmentType;
use App\Language;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ItemAttachmentTypeControllerBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (ItemAttachmentType::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get
     *
     * @param String $search
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return ItemAttachmentLanguageTypeTable::select('item_attachment_type_id as id', 'description')->where('language_id', 1)->orderBy('description')->get();
        } else {
            return ItemAttachmentLanguageTypeTable::where('description', 'ilike','%'. $search . '%')->where('language_id', 1)->select('item_attachment_type_id as id', 'description')->orderBy('description')->get();
        }
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoOptional
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (DB::table('item_attachment_type')
                ->select('item_attachment_type.*', 'item_attachment_language_type_table.description', 'item_attachment_language_type_table.language_id')
                ->join('item_attachment_language_type_table', 'item_attachment_type.id', '=', 'item_attachment_language_type_table.item_attachment_type_id')
                ->where('item_attachment_language_type_table.language_id', $idLanguage)
                ->orderBy('item_attachment_type.order')
                ->get()
        as $itemAttachmentAll) {
            
            $dtoItemAttachmentLanguageTypeTable = new DtoItemAttachmentLanguageTypeTable();
            $dtoItemAttachmentLanguageTypeTable->id = $itemAttachmentAll->id;
            $dtoItemAttachmentLanguageTypeTable->language_id = $itemAttachmentAll->language_id;
            $dtoItemAttachmentLanguageTypeTable->code = $itemAttachmentAll->code;
            $dtoItemAttachmentLanguageTypeTable->order = $itemAttachmentAll->order;
            $dtoItemAttachmentLanguageTypeTable->online = $itemAttachmentAll->online;
            $dtoItemAttachmentLanguageTypeTable->description = $itemAttachmentAll->description;

            if(!is_null($itemAttachmentAll->item_attachment_father_type_id) && $itemAttachmentAll->item_attachment_father_type_id!= ''){
                $itemAttachmentFather = ItemAttachmentLanguageTypeTable::where('item_attachment_type_id', $itemAttachmentAll->item_attachment_father_type_id)->where('language_id', $idLanguage)->get()->first();
                if(isset($itemAttachmentFather)){
                    $dtoItemAttachmentLanguageTypeTable->item_attachment_father_type_id_description = $itemAttachmentFather->description;
                }else{
                    $dtoItemAttachmentLanguageTypeTable->item_attachment_father_type_id_description = '';
                }
            }
            $dtoItemAttachmentLanguageTypeTable->item_attachment_father_type_id = $itemAttachmentAll->item_attachment_father_type_id;

            $itemAttachmentTypeTableAdds = ItemAttachmentLanguageTypeTable::where('item_attachment_type_id', $itemAttachmentAll->id)->where('language_id', '<>', $idLanguage)->get();
            
            if (isset($itemAttachmentTypeTableAdds)) {
                foreach ($itemAttachmentTypeTableAdds as $itemAttachmentTypeTableAdd) {
                    $dtoItemAttachmentTypeTableAdd = new DtoItemAttachmentLanguageTypeTable();
                    $dtoItemAttachmentTypeTableAdd->id = $itemAttachmentTypeTableAdd['id'];
                    $dtoItemAttachmentTypeTableAdd->language_id = $itemAttachmentTypeTableAdd['language_id'];
                    $dtoItemAttachmentTypeTableAdd->descriptionLanguage = Language::where('id', '=', $itemAttachmentTypeTableAdd['language_id'])->first()->description;
                    $dtoItemAttachmentTypeTableAdd->description = $itemAttachmentTypeTableAdd['description'];
                    
                    $dtoItemAttachmentLanguageTypeTable->itemAttachmentLanguageType->push($dtoItemAttachmentTypeTableAdd);
                } 
            }   
                        
            $result->push($dtoItemAttachmentLanguageTypeTable); 
        }
        
        return $result;
    }
    
    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $itemAttachmentType = new ItemAttachmentType();
            $itemAttachmentType->order = $request->order;
            $itemAttachmentType->online = $request->online;
            $itemAttachmentType->code = $request->code;
            $itemAttachmentType->created_id = $idUser;
            $itemAttachmentType->item_attachment_father_type_id = $request->item_attachment_father_type_id;
            $itemAttachmentType->save();
            
            $itemAttachmentTypeLanguages = new ItemAttachmentLanguageTypeTable();
            $itemAttachmentTypeLanguages->item_attachment_type_id = $itemAttachmentType->id;
            $itemAttachmentTypeLanguages->description = $request->description;
            $itemAttachmentTypeLanguages->language_id = $idLanguage;
            $itemAttachmentTypeLanguages->created_id = $idUser;
            $itemAttachmentTypeLanguages->save();

            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        //return $optional->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoOptional
     * @param int idLanguage
     */
    public static function update(Request $request, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $itemAttachmentTypeOnDb = ItemAttachmentType::find($request->id);
        
        DB::beginTransaction();

        try {
            $itemAttachmentTypeOnDb->order = $request->order;
            $itemAttachmentTypeOnDb->code = $request->code;
            $itemAttachmentTypeOnDb->online = $request->online;
            $itemAttachmentTypeOnDb->item_attachment_father_type_id = $request->item_attachment_father_type_id;
            $itemAttachmentTypeOnDb->save();
            
            $itemAttachmentTypeLanguageOnDb = ItemAttachmentLanguageTypeTable::where('item_attachment_type_id', $itemAttachmentTypeOnDb->id)->where('language_id', $idLanguage)->get()->first();
            $itemAttachmentTypeLanguageOnDb->description = $request->description;
            $itemAttachmentTypeLanguageOnDb->save();

            $itemAttachmentTypeLanguagesDelete = ItemAttachmentLanguageTypeTable::where('item_attachment_type_id', $itemAttachmentTypeOnDb->id)->where('language_id', '<>', $idLanguage);
            $itemAttachmentTypeLanguagesDelete->delete();

            foreach ($request->translateDescription as $res) {
                if (!is_null($res['optional_id'])){
                    $itemAttachmentTypeLanguageNew = new ItemAttachmentLanguageTypeTable();
                    $itemAttachmentTypeLanguageNew->item_attachment_type_id = $itemAttachmentTypeOnDb->id;
                    $itemAttachmentTypeLanguageNew->description = $res['cost'];
                    $itemAttachmentTypeLanguageNew->language_id = $res['optional_id'];
                    $itemAttachmentTypeLanguageNew->created_id = 1;
                    $itemAttachmentTypeLanguageNew->save();
                }
            }


            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $itemAttachmentType = ItemAttachmentType::find($id);
        
        if (is_null($itemAttachmentType)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            
            $itemAttachmentTypeLanguages = ItemAttachmentLanguageTypeTable::where('item_attachment_type_id', $id);
            $itemAttachmentTypeLanguages->delete();

            $itemAttachmentType->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    #endregion DELETE
}

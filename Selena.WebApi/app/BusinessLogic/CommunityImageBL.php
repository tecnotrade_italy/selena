<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoCommunityImage;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\CommunityImage;
use App\CommunityAction;
use App\CommunityFavoriteImage;
use App\CommunityImageComment;
use App\CommunityImageReport;
use App\CommunityImageCategoryLanguage;
use Exception;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use App\ItemLanguage;
use App\ItemExifDataMatch;
use App\UsersDatas;
use App\Content;
use App\ContentLanguage;
use App\DtoModel\DtoItem;
use App\DtoModel\DtoItemImages;
use App\DtoModel\DtoPageSitemap;
use App\ImageResize;
use Illuminate\Support\Facades\Storage;

class CommunityImageBL
{

    public static $dirImageUser = '../storage/app/public/imagesUser/';
    
    #region PRIVATE

    /**
     * clean str
     *
     * @param String $str
     *
     * @return str
     */
    private static function _cleanStr($str){
        return strtolower(str_replace(',', '-', str_replace('?', '-', str_replace('!', '-', str_replace('\'', '-', str_replace(' ', '-', $str))))));
    }


    private static function getUrlImageUsers()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/imagesUser/';
    }


    /**
     * Convert to dto
     * 
     * @param CommunityImage CommunityImage
     * @return DtoCommunityImage
     */
    private static function _convertToDto($CommunityImage)
    {
        $dtoCommunityImage = new DtoCommunityImage();

        if (isset($CommunityImage->id)) {
            $dtoCommunityImage->id = $CommunityImage->id;
        }

        if (isset($CommunityImage->user_id)) {
            $dtoCommunityImage->user_id = $CommunityImage->user_id;
        }

        if (isset($CommunityImage->image_title)) {
            $dtoCommunityImage->image_title = $CommunityImage->image_title;
        }
        if (isset($CommunityImage->image_category_id)) {
            $dtoCommunityImage->image_category_id = $CommunityImage->image_category_id;
        }

        if (isset($CommunityImage->gallery_id)) {
            $dtoCommunityImage->gallery_id = $CommunityImage->gallery_id;
        }

        if (isset($CommunityImage->image_description)) {
            $dtoCommunityImage->image_description = $CommunityImage->image_description;
        }

        if (isset($CommunityImage->location_latitude)) {
            $dtoCommunityImage->location_latitude = $CommunityImage->location_latitude;
        }        

        if (isset($CommunityImage->location_longitude)) {
            $dtoCommunityImage->location_longitude = $CommunityImage->location_longitude;
        }

        if (isset($CommunityImage->keywords)) {
            $dtoCommunityImage->keywords = $CommunityImage->keywords;
        }

        if (isset($CommunityImage->susceptible_content)) {
            $dtoCommunityImage->susceptible_content = $CommunityImage->susceptible_content;
        }

        if (isset($CommunityImage->upload_datetime)) {
            $dtoCommunityImage->upload_datetime = $CommunityImage->upload_datetime;
        }   

        if (isset($CommunityImage->star_rating)) {
            $dtoCommunityImage->star_rating = $CommunityImage->star_rating;
        }  

        if (isset($CommunityImage->nr_views)) {
            $dtoCommunityImage->nr_views = $CommunityImage->nr_views;
        }

        if (isset($CommunityImage->rating)) {
            $dtoCommunityImage->rating = $CommunityImage->rating;
        }   

        if (isset($CommunityImage->EXIF_camera)) {
            $dtoCommunityImage->EXIF_camera = $CommunityImage->EXIF_camera;
        }  

        if (isset($CommunityImage->EXIF_camera_id)) {
            $dtoCommunityImage->EXIF_camera_id = $CommunityImage->EXIF_camera_id;
        }  

        if (isset($CommunityImage->EXIF_lens)) {
            $dtoCommunityImage->EXIF_lens = $CommunityImage->EXIF_lens;
        }  

        if (isset($CommunityImage->EXIF_lens_id)) {
            $dtoCommunityImage->EXIF_lens_id = $CommunityImage->EXIF_lens_id;
        }

        if (isset($CommunityImage->EXIF_iso)) {
            $dtoCommunityImage->EXIF_iso = $CommunityImage->EXIF_iso;
        }   

        if (isset($CommunityImage->EXIF_shutter_speed)) {
            $dtoCommunityImage->EXIF_shutter_speed = $CommunityImage->EXIF_shutter_speed;
        }  

        if (isset($CommunityImage->EXIF_aperture)) {
            $dtoCommunityImage->EXIF_aperture = $CommunityImage->EXIF_aperture;
        }  

        if (isset($CommunityImage->EXIF_focal_lenght)) {
            $dtoCommunityImage->EXIF_focal_lenght = $CommunityImage->EXIF_focal_lenght;
        }   

        if (isset($CommunityImage->editor_pick)) {
            $dtoCommunityImage->editor_pick = $CommunityImage->editor_pick;
        }  

        if (isset($CommunityImage->editor_pick_date)) {
            $dtoCommunityImage->editor_pick_date = $CommunityImage->editor_pick_date;
        }  

        if (isset($CommunityImage->online)) {
            $dtoCommunityImage->online = $CommunityImage->online;
        }

        return $dtoCommunityImage;
    }



    /**
     * Convert to VatType
     * 
     * @param DtoCommunityImage dtoCommunityImage
     * 
     * @return CommunityImage
     */
    private static function _convertToModel($dtoCommunityImage)
    {

        $CommunityImage = new CommunityImage();

        if (isset($dtoCommunityImage->id)) {
            $CommunityImage->id = $dtoCommunityImage->id;
        }

        if (isset($dtoCommunityImage->users_id)) {
            $CommunityImage->user_id = $dtoCommunityImage->users_id;
        }

        if (isset($dtoCommunityImage->image_title)) {
            $CommunityImage->image_title = $dtoCommunityImage->image_title;
        }

        if (isset($dtoCommunityImage->image_category_id)) {
            $CommunityImage->image_category_id = $dtoCommunityImage->image_category_id;
        }

        if (isset($dtoCommunityImage->gallery_id)) {
            $CommunityImage->gallery_id = $dtoCommunityImage->gallery_id;
        } else
            $CommunityImage->gallery_id = null;

        if (isset($dtoCommunityImage->image_description)) {
            $CommunityImage->image_description = $dtoCommunityImage->image_description;
        } else
            $CommunityImage->image_description = null;
        
        if (isset($dtoCommunityImage->location_latitude)) {
            $CommunityImage->location_latitude = $dtoCommunityImage->location_latitude;
        } else
            $CommunityImage->location_latitude = null;

        if (isset($dtoCommunityImage->location_longitude)) {
            $CommunityImage->location_longitude = $dtoCommunityImage->location_longitude;
        } else
            $CommunityImage->location_longitude =  null;

        if (isset($dtoCommunityImage->keywords)) {
            $CommunityImage->keywords = $dtoCommunityImage->keywords;
        }else
            $CommunityImage->keywords = null;

        if (isset($dtoCommunityImage->susceptible_content)) {
            $CommunityImage->susceptible_content = $dtoCommunityImage->susceptible_content;
        }

        if (isset($dtoCommunityImage->upload_datetime)) {
            $CommunityImage->upload_datetime = $dtoCommunityImage->upload_datetime;
        }

        if (isset($dtoCommunityImage->star_rating)) {
            $CommunityImage->star_rating = $dtoCommunityImage->star_rating;
        }   else
            $CommunityImage->star_rating = 0;

        if (isset($dtoCommunityImage->nr_views)) {
            $CommunityImage->nr_views = $dtoCommunityImage->nr_views;
        } else 
            $CommunityImage->nr_views = 0;

        if (isset($dtoCommunityImage->rating)) {
            $CommunityImage->rating = $dtoCommunityImage->rating;
        }   else
            $CommunityImage->rating = 0;

        if (isset($dtoCommunityImage->EXIF_camera)) {
            $CommunityImage->EXIF_camera = $dtoCommunityImage->EXIF_camera;
        }   else
            $CommunityImage->EXIF_camera = null;

        if (isset($dtoCommunityImage->EXIF_camera_id)) {
            $CommunityImage->EXIF_camera_id = $dtoCommunityImage->EXIF_camera_id;
        } else 
            $CommunityImage->EXIF_camera_id = null;

        if (isset($dtoCommunityImage->EXIF_lens)) {
            $CommunityImage->EXIF_lens = $dtoCommunityImage->EXIF_lens;
        }   else
            $CommunityImage->EXIF_lens = null;            

        if (isset($dtoCommunityImage->EXIF_lens_id)) {
            $CommunityImage->EXIF_lens_id = $dtoCommunityImage->EXIF_lens_id;
        }   else
            $CommunityImage->EXIF_lens_id = null;

        if (isset($dtoCommunityImage->EXIF_iso)) {
            $CommunityImage->EXIF_iso = $dtoCommunityImage->EXIF_iso;
        } else 
            $CommunityImage->EXIF_iso = null;

        if (isset($dtoCommunityImage->EXIF_shutter_speed)) {
            $CommunityImage->EXIF_shutter_speed = $dtoCommunityImage->EXIF_shutter_speed;
        }   else
            $CommunityImage->EXIF_shutter_speed = null;  
            
        if (isset($dtoCommunityImage->EXIF_aperture)) {
            $CommunityImage->EXIF_aperture = $dtoCommunityImage->EXIF_aperture;
        }   else
            $CommunityImage->EXIF_aperture = null;

        if (isset($dtoCommunityImage->EXIF_focal_lenght)) {
            $CommunityImage->EXIF_focal_lenght = $dtoCommunityImage->EXIF_focal_lenght;
        } else 
            $CommunityImage->EXIF_focal_lenght = null;

        if (isset($dtoCommunityImage->editor_pick)) {
            $CommunityImage->editor_pick = $dtoCommunityImage->editor_pick;
        }   else
            $CommunityImage->editor_pick = 0;  
      
        if (isset($dtoCommunityImage->editor_pick_date)) {
            $CommunityImage->editor_pick_date = $dtoCommunityImage->editor_pick_date;
        } else 
            $CommunityImage->editor_pick_date = null;
       
        if (isset($dtoCommunityImage->online)) {
            $CommunityImage->online = $dtoCommunityImage->online;
        }
        if (isset($dtoCommunityImage->link)) {
            $CommunityImage->link = $dtoCommunityImage->link;
        }
         
            /* webp resize  */
      /*   if (!is_null($dtoCommunityImage->imgName)) {
                Image::make($dtoCommunityImage->imgBase64)->save(static::$dirImageUser . $dtoCommunityImage->imgName);
                $CommunityImage->img = static::getUrlImageUsers() . $dtoCommunityImage->imgName;
                $CommunityImage->img_original = static::getUrlImageUsers() . $dtoCommunityImage->imgName;
                $CommunityImage->img_detail = static::getUrlImageUsers() . $dtoCommunityImage->imgName;
        }
        */

      

        /* conversion on webP */
       /* $url =  static::$dirImageUser . 'preview/'.  $dtoCommunityImage->imgName;
        LogHelper::debug($dtoCommunityImage);
        $ext = pathinfo($url, PATHINFO_EXTENSION);
        $image = file_get_contents($url);

        $urlImageWebpToSave = static::$dirImageUser . '/' . str_replace('.' . $ext, '', $dtoCommunityImage->imgName) . '.webp';
        LogHelper::debug($urlImageWebpToSave);

        if (!file_exists($urlImageWebpToSave)) {
            if ($image !== false){
                $imageBase64 = 'data:image/' . $ext . ';base64,' . base64_encode($image);
                LogHelper::debug($imageBase64);
                $imageWebp = Image::make($imageBase64)->encode('webp', 70)->save($urlImageWebpToSave, 70);
                Storage::disk('public')->put( '/imagesUser/preview/'. $dtoCommunityImage->imgName, $imageWebp->stream()->__toString(), 'public'); 
        
                $imageWebps = Image::make($imageBase64)->encode('webp', 70)->save($urlImageWebpToSave, 70);
                Storage::disk('public')->put('/imagesUser/main/'. $dtoCommunityImage->imgName, $imageWebps->stream()->__toString(), 'public'); 
    
                $filenamewithextension = basename($urlImageWebpToSave);
                $nameFile = basename($filenamewithextension);
                $srcSet = static::_resizeImageByWidth($imageBase64, $nameFile . '.webp');
                if($srcSet !=''){
                    $return = 'ok';
                }else{
                    $return = 'Errore durante il ridimensionamento della conversione immagine';
                }
            }else{
                $return = 'Errore durante la conversione immagine';
            }
        }else{
            $return = 'Immagine in formato .webp già presente!';
        }    */
        /* end conversion on webP */

       
        return $CommunityImage;
    }

    #endregion PRIVATE

    public static function getAllSearch(Request $request)
    {
        LogHelper::debug($request);
        $content = '';
        $strWhere = '';
        $strOrderBy = '';
        $referencesName = '';
        $rateImage = "0,0";
        $userLink = "";

        $idCategoryImage = $request->image_category_id;
         if ($idCategoryImage != "") {
            $strWhere = $strWhere . ' AND Community_Images.image_category_id = ' . $idCategoryImage;
        }

        $search = $request->search;
        if ($search != "") {
         $strWhere = $strWhere . ' AND (Community_Images.link ilike \'%' . $search . '%\'
                                   OR Community_Images.img ilike \'%' .$search. '%\'
                                   OR Community_Images.image_description ilike \'%' .$search. '%\'
                                   OR Community_Images.keywords ilike \'%' .$search. '%\'
                                   OR Community_Images.image_title ilike \'%' . $search . '%\')';
        }
       
        $strOrderBy = "order by CASE WHEN upload_datetime >  now() - INTERVAL '24 hour' THEN case WHEN rating IS NULL 
        THEN 0 else rating END + 10000 ELSE case WHEN rating IS NULL THEN 0 else rating END end desc, upload_datetime DESC";
                
                $Images = DB::select( 
                'SELECT link, img, star_rating, user_id, id, susceptible_content, image_title 
                 FROM Community_Images 
                 WHERE online = true' . $strWhere . ' ' . $strOrderBy . ' ');
                
             

                if(isset($Images) && count($Images)>0){

                    foreach ($Images as $key => $cv) 
                    {
                    
                        $previous = array_key_exists($key - 1, $Images) ? $Images[$key - 1]->link : '';
                        $next = array_key_exists($key + 1, $Images) ? $Images[$key + 1]->link : '';

                        $usersDatas = UsersDatas::where('user_id', $Images[$key]->user_id)->get()->first();
                        if(isset($usersDatas)){
                            $referencesName = $usersDatas->name . ' ' . $usersDatas->surname;
                            $userLink = $usersDatas->link;
                        }

                        if(!is_null($Images[$key]->star_rating)){
                            $rateImage = str_replace('.',',',substr($Images[$key]->star_rating, 0, -3));
                        }else{
                            $rateImage = "";
                        }

                        $htmlSusceptibleContent = "";

                        if($Images[$key]->susceptible_content){
                            $htmlSusceptibleContent = '<div class="susceptible-content" data-attr-id="' . $Images[$key]->id . '">Contenuto sensibile<br><span class="btn-vedi-foto">Vedi comunque</span></div>';
                        }

                        $htmlModify = '<div class="cont-event-image"><i class="fa fa-trash" data-attr-id="' . $Images[$key]->id . '"></i><i class="fa fa-ellipsis-h" data-attr-id="' . $Images[$key]->id . '"></i></div>';
                        $content = $content . '<a id="immagineGalleria' . $Images[$key]->id . '" 
                        class="img img-community-images-list" href="'.$Images[$key]->link.'">
                        <img alt="'.$Images[$key]->image_title.'" loading="lazy" data-src="'.$Images[$key]->img.'" 
                        class="fr-fic fr-dii" style="width: 393px; height: 250px; margin-left: -196.5px; margin-top: -125px;" src="'.$Images[$key]->img.'">
                        <div class="div-over-gallery"><p class="p-left">' . $referencesName . '</p>
                        <p class="p-right">' . $rateImage . '</p></div>' . $htmlModify . $htmlSusceptibleContent . '</a>';
                    }
             
                }else{
                    $content = "<h3 class='title-no-foto'>Nessuna immagine presente</h3>";
                }
                
                LogHelper::debug($content);
            return $content;


    }
  /**
     * Return stringSuccess
     *
     * @param string base64
     * @return stringSuccess
     */
    private static function _resizeImageByWidth($bas64, $imgName)
    {
        $stringReturnSrcSet = '';
        $pathResize = static::getUrlImageUsers();
        $imageResize = ImageResize::where('online', true)->get();

        $extension = substr($imgName, -3, 3);
        if($extension != 'gif'){
            if ($extension == 'jpg' || $extension == 'png'){
                $realExt = $extension;
                $imgNameWithoutExt = str_replace("." . $extension, "", $imgName);
            }else{
                if ($extension == 'peg'){
                    $realExt = 'jpeg';
                    $imgNameWithoutExt = str_replace(".jpeg", "", $imgName);
                }else{
                    if ($extension == 'ebp'){
                        $realExt = 'webp';
                        $imgNameWithoutExt = str_replace(".webp", "", $imgName);
                        
                    }
                }
            }
    
            if(isset($imageResize)){
                foreach ($imageResize as $imageResizeAll) {
                    $imgResized = Image::make($bas64);
                    $imgResized->resize($imageResizeAll->width, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $imgResized->save(static::$dirImageUser . '/resize/' . $imgNameWithoutExt . 'x' . $imageResizeAll->width . '.' . $realExt, 100);

                    $fullPath = $pathResize . 'resize/' . $imgNameWithoutExt . 'x' . $imageResizeAll->width . '.' . $realExt;
                    if($stringReturnSrcSet == ''){
                        $stringReturnSrcSet = $fullPath . ' ' . $imageResizeAll->width . 'w';
                    }else{
                        $stringReturnSrcSet = $stringReturnSrcSet . ', ' . $fullPath . ' ' . $imageResizeAll->width . 'w';
                    }
                }
            }
        }
        
        return $stringReturnSrcSet;
    
    }
    

    public static function updateOnlineCheckImages(Request $request)
    {
        $CommunityImage = CommunityImage::find($request->id);

        if (isset($CommunityImage)) {
            if(is_null($CommunityImage->online)){
                $CommunityImage->online = true;
            }else{
                $CommunityImage->online = !$CommunityImage->online;
            }
            $CommunityImage->save();
        }
    }

    public static function insertNewImages($dtoCommunityImage)
    { 

        LogHelper::debug($dtoCommunityImage);
        $CommunityImage = new CommunityImage();

        if (isset($dtoCommunityImage->id)) {
            $CommunityImage->id = $dtoCommunityImage->id;
        }

        if (isset($dtoCommunityImage->users_id)) {
            $CommunityImage->user_id = $dtoCommunityImage->users_id;
        }

        if (isset($dtoCommunityImage->image_title)) {
            $CommunityImage->image_title = $dtoCommunityImage->image_title;
        }

        if (isset($dtoCommunityImage->image_category_id)) {
            $CommunityImage->image_category_id = $dtoCommunityImage->image_category_id;
        }

        if (isset($dtoCommunityImage->gallery_id)) {
            $CommunityImage->gallery_id = $dtoCommunityImage->gallery_id;
        } else
            $CommunityImage->gallery_id = null;

        if (isset($dtoCommunityImage->image_description)) {
            $CommunityImage->image_description = $dtoCommunityImage->image_description;
        } else
            $CommunityImage->image_description = null;
        
        if (isset($dtoCommunityImage->location_latitude)) {
            $CommunityImage->location_latitude = $dtoCommunityImage->location_latitude;
        } else
            $CommunityImage->location_latitude = null;

        if (isset($dtoCommunityImage->location_longitude)) {
            $CommunityImage->location_longitude = $dtoCommunityImage->location_longitude;
        } else
            $CommunityImage->location_longitude =  null;

        if (isset($dtoCommunityImage->keywords)) {
            $CommunityImage->keywords = $dtoCommunityImage->keywords;
        }else
            $CommunityImage->keywords = null;

        if (isset($dtoCommunityImage->susceptible_content)) {
            $CommunityImage->susceptible_content = $dtoCommunityImage->susceptible_content;
        }

        if (isset($dtoCommunityImage->upload_datetime)) {
            $CommunityImage->upload_datetime = $dtoCommunityImage->upload_datetime;
        }

        if (isset($dtoCommunityImage->star_rating)) {
            $CommunityImage->star_rating = $dtoCommunityImage->star_rating;
        }   else
            $CommunityImage->star_rating = 0;

        if (isset($dtoCommunityImage->nr_views)) {
            $CommunityImage->nr_views = $dtoCommunityImage->nr_views;
        } else 
            $CommunityImage->nr_views = 0;

        if (isset($dtoCommunityImage->rating)) {
            $CommunityImage->rating = $dtoCommunityImage->rating;
        }   else
            $CommunityImage->rating = 0;

        if (isset($dtoCommunityImage->EXIF_camera)) {
            $CommunityImage->EXIF_camera = $dtoCommunityImage->EXIF_camera;
        }   else
            $CommunityImage->EXIF_camera = null;

        if (isset($dtoCommunityImage->EXIF_camera_id)) {
            $CommunityImage->EXIF_camera_id = $dtoCommunityImage->EXIF_camera_id;
        } else 
            $CommunityImage->EXIF_camera_id = null;

        if (isset($dtoCommunityImage->EXIF_lens)) {
            $CommunityImage->EXIF_lens = $dtoCommunityImage->EXIF_lens;
        }   else
            $CommunityImage->EXIF_lens = null;            

        if (isset($dtoCommunityImage->EXIF_lens_id)) {
            $CommunityImage->EXIF_lens_id = $dtoCommunityImage->EXIF_lens_id;
        }   else
            $CommunityImage->EXIF_lens_id = null;

        if (isset($dtoCommunityImage->EXIF_iso)) {
            $CommunityImage->EXIF_iso = $dtoCommunityImage->EXIF_iso;
        } else 
            $CommunityImage->EXIF_iso = null;

        if (isset($dtoCommunityImage->EXIF_shutter_speed)) {
            $CommunityImage->EXIF_shutter_speed = $dtoCommunityImage->EXIF_shutter_speed;
        }   else
            $CommunityImage->EXIF_shutter_speed = null;  
            
        if (isset($dtoCommunityImage->EXIF_aperture)) {
            $CommunityImage->EXIF_aperture = $dtoCommunityImage->EXIF_aperture;
        }   else
            $CommunityImage->EXIF_aperture = null;

        if (isset($dtoCommunityImage->EXIF_focal_lenght)) {
            $CommunityImage->EXIF_focal_lenght = $dtoCommunityImage->EXIF_focal_lenght;
        } else 
            $CommunityImage->EXIF_focal_lenght = null;

        if (isset($dtoCommunityImage->editor_pick)) {
            $CommunityImage->editor_pick = $dtoCommunityImage->editor_pick;
        }   else
            $CommunityImage->editor_pick = 0;  
      
        if (isset($dtoCommunityImage->editor_pick_date)) {
            $CommunityImage->editor_pick_date = $dtoCommunityImage->editor_pick_date;
        } else 
            $CommunityImage->editor_pick_date = null;
       
        if (isset($dtoCommunityImage->online)) {
            $CommunityImage->online = $dtoCommunityImage->online;
        } else
            $CommunityImage->online = false;
        
        
        if (isset($dtoCommunityImage->link)) {
            $CommunityImage->link = $dtoCommunityImage->link;
        }

       /* if (!is_null($dtoCommunityImage->imgName)) {
            Image::make($dtoCommunityImage->imgBase64)->save(static::$dirImageUser . $dtoCommunityImage->imgName);
            $CommunityImage->img = static::getUrlImageUsers() . $dtoCommunityImage->imgName;
            $CommunityImage->img_original = static::getUrlImageUsers() . $dtoCommunityImage->imgName;
            $CommunityImage->img_detail = static::getUrlImageUsers() . $dtoCommunityImage->imgName;
        }*/


    if (!Storage::disk('public')->exists($dtoCommunityImage->id)) {
        // creazione cartella utente
        Storage::disk('public')->makeDirectory($dtoCommunityImage->id);
        //creazione cartella resize contiente le immagini ridimensionate per altezza a 400px
        Storage::disk('public')->makeDirectory($dtoCommunityImage->id . '/imagesUser/preview');

        //creazione cartella resize contente le immagini ridimensionate per larghezza a 1200px
        Storage::disk('public')->makeDirectory($dtoCommunityImage->id . '/imagesUser/main');
    }else{
        if (!Storage::disk('public')->exists($dtoCommunityImage->id . '/imagesUser/preview')) {
            Storage::disk('public')->makeDirectory($dtoCommunityImage->id . 'imagesUser/preview');
        }

        if (!Storage::disk('public')->exists($dtoCommunityImage->id . '/imagesUser/main')) {
                Storage::disk('public')->makeDirectory($dtoCommunityImage->id . '/imagesUser/main');
        }
    }
        if (!Storage::disk('public')->exists($dtoCommunityImage->id)) {
                Storage::disk('public')->makeDirectory($dtoCommunityImage->id . '/imagesUser/original');
        }

        if($dtoCommunityImage->title != '') {
            $imgNameByTitle = $dtoCommunityImage->imgName . '-' . static::_cleanStr($dtoCommunityImage->titletitle) . '.webp';
            $imgNameByTitleShare = $dtoCommunityImage->imgName . '-' . static::_cleanStr($dtoCommunityImage->titletitle) . '.jpg';
        }else{
            $imgNameByTitle = $dtoCommunityImage->imgName; 
            $imgNameByTitleShare = $dtoCommunityImage->imgName; 
        }

        $data = explode( ',', $dtoCommunityImage->imgBase64);
        Storage::disk('public')->put($dtoCommunityImage->userId . '/imagesUser/original/'. $imgNameByTitleShare, base64_decode($data[1]));  

        $resizedImage = Image::make($dtoCommunityImage->imgBase64)->encode('jpg', 100)->resize(null, 300, function ($constraint) {
            $constraint->aspectRatio();
        });
        Storage::disk('public')->put($dtoCommunityImage->userId . '/imagesUser/preview/'. $imgNameByTitle, $resizedImage->stream()->__toString(), 'public'); 

        $resizedImagedetail = Image::make($dtoCommunityImage->imgBase64)->encode('jpg', 100)->resize(null, 1920, function ($constraint) {
            $constraint->aspectRatio();
        });
        Storage::disk('public')->put($dtoCommunityImage->userId . '/imagesUser/main/'. $imgNameByTitle, $resizedImagedetail->stream()->__toString(), 'public'); 
     
        $CommunityImage->img = static::getUrlImageUsers() . 'preview/' . $imgNameByTitle;
        $CommunityImage->img_original = static::getUrlImageUsers() . 'original/' . $imgNameByTitle;
        $CommunityImage->img_detail = static::getUrlImageUsers() . 'main/' . $imgNameByTitle;
        $CommunityImage->image_share = static::getUrlImageUsers() . 'main/' . $imgNameByTitle;

        $CommunityImage->save();
        return $CommunityImage;
    }

    #region GET


   /**
     * Get Content for link
     * 
     * @return DtoItems
     */
    public static function getImages($id)
    {
        $CommunityImage = CommunityImage::find($id);
        if(!is_null($id)){
            if (is_null($id)) {
                throw new Exception(DictionaryBL::getTranslate(1, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if ($id == 0){
                return '';
            }
           /* if (is_null($CommunityImage)) {
                throw new Exception(DictionaryBL::getTranslate(1, DictionariesCodesEnum::ItemNotFound), HttpResultsCodesEnum::InvalidPayload);
            }*/


            $listImages = new DtoItemImages();
            $listImages->id = $id;
            $listImages->itemId = $id;
            $listImages->img = $CommunityImage->img;

            if ($CommunityImage->img != '') {
                $path_parts = pathinfo($CommunityImage->img);
                $listImages->imgName = $path_parts['basename'];

                $ch = curl_init($CommunityImage->img);
                curl_setopt($ch, CURLOPT_NOBODY, true);
                curl_exec($ch);
                $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                
                if ($code == 200) {
                    $size = getimagesize($CommunityImage->img);
                    $listImages->width = $size[0];
                    $listImages->height = $size[1];
                } else {
                    $listImages->width = 0;
                    $listImages->height = 0;
                }
                $weight = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
                curl_close($ch);
                $dimension = static::_convertImageSize($weight);
                $listImages->weight = $dimension;
            } else {
                $listImages->imgName = 'Nessuna immagine presente';
                $listImages->width = 0;
                $listImages->height = 0;
                $listImages->weight = 0;
            }
            return $listImages;
        }else{
            return '';
        }
    }

    private static function _convertImageSize(int $bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }


 
    public static function updateImageName(Request $request)
    {
        $CommunityImage = CommunityImage::find($request->idItem);
        if (isset($CommunityImage)) {
            if ($request->imgBase64 == '') {
                if($request->principal){
                    $CommunityImage->img = static::getUrlImageUsers() . $request->imgName;
                    $CommunityImage->img_original = static::getUrlImageUsers() . $request->imgName;
                    $CommunityImage->img_detail = static::getUrlImageUsers() . $request->imgName;
                }else{
                    $CommunityImage->img = static::getUrlImageUsers() . $request->imgName;
                    $CommunityImage->img_original = static::getUrlImageUsers() . $request->imgName;
                    $CommunityImage->img_detail = static::getUrlImageUsers() . $request->imgName;
                }
                    $CommunityImage->save();
            } else {
                    Image::make($request->imgBase64)->save(static::$dirImageUser . $request->imgName);
                if($request->principal){
                    $CommunityImage->img = static::getUrlImageUsers() . $request->imgName;
                    $CommunityImage->img_original = static::getUrlImageUsers() . $request->imgName;
                    $CommunityImage->img_detail = static::getUrlImageUsers() . $request->imgName;
                }else{
                    $CommunityImage->img = static::getUrlImageUsers() . $request->imgName;
                    $CommunityImage->img_original = static::getUrlImageUsers() . $request->imgName;
                    $CommunityImage->img_detail = static::getUrlImageUsers() . $request->imgName;
                }
                    $CommunityImage->save();
            }
        }
    }

    public static function getPages(string $page_type, int $idLanguage, int $page)
    {
        //perchè il conteggio parte da 0
        $paginator = $page + 1;
        $totItems = DB::select('SELECT count(community_images.id) AS cont FROM community_images');

        $strSqlPagination = "";
        if($totItems[0]->cont <= 50000){
            $strSqlPagination = "";
        }else{
            $limit = 50000;
            $offset = 50000 * ($paginator - 1);

            $strSqlPagination = " limit " . $limit . " offset " . $offset;
        }

        $result = collect();
        foreach (DB::select(
            "SELECT community_images.image_title, community_images.updated_at, community_images.link
            FROM community_images
            ORDER BY community_images.updated_at desc " . $strSqlPagination
        ) as $PagesAll) {
            $dtoPageSitemap = new DtoPageSitemap();
            $dtoPageSitemap->url = $PagesAll->link;
            $dtoPageSitemap->title = $PagesAll->image_title;
            $dtoPageSitemap->created_at = $PagesAll->updated_at;

            $result->push($dtoPageSitemap);
        }
        return $result;
    }


    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCommunityImage
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(CommunityImage::find($id));
    }


    /**
     * get count upload images by user id
     * 
     * @param int id
     * @return count
     */
    public static function countUserUpload(int $id){
        $totImagesUploaded = DB::select("SELECT count(id) as cont FROM community_images WHERE upload_datetime > CURRENT_DATE - 7 AND user_id=" . $id);
        $cont = 0;
        if($totImagesUploaded[0]->cont >= 7){
            $cont = 0;
        }else{
            $cont = 7 - $totImagesUploaded[0]->cont;
        }

        return $cont;
    }

    /*
     * Get all
     * 
     * @return DtoCommunityImage
     */
    public static function getAll()
    {

        $result = collect(); 

        foreach (CommunityImage::orderBy('upload_datetime','DESC')->get() as $communityImage) {

            $DtoCommunityImage = new DtoCommunityImage();

            $DtoCommunityImage->id = $communityImage->id;
            $DtoCommunityImage->users_id = $communityImage->user_id;
            $DtoCommunityImage->image_title = $communityImage->image_title;
            $DtoCommunityImage->image_category_id = $communityImage->image_category_id;
            $DtoCommunityImage->gallery_id = $communityImage->gallery_id;
            $DtoCommunityImage->image_description = $communityImage->image_description;
            $DtoCommunityImage->location_latitude = $communityImage->location_latitude;
            $DtoCommunityImage->location_longitude = $communityImage->location_longitude;
            $DtoCommunityImage->keywords = $communityImage->keywords;
            $DtoCommunityImage->susceptible_content = $communityImage->susceptible_content;
            $DtoCommunityImage->upload_datetime = $communityImage->upload_datetime;
            $DtoCommunityImage->star_rating = $communityImage->star_rating;
            $DtoCommunityImage->link = $communityImage->link;
            $DtoCommunityImage->nr_views = $communityImage->nr_views;
            $DtoCommunityImage->rating = $communityImage->rating;
            $DtoCommunityImage->EXIF_camera = $communityImage->EXIF_camera;
            $DtoCommunityImage->EXIF_camera_id = $communityImage->EXIF_camera_id;
            $DtoCommunityImage->EXIF_lens = $communityImage->EXIF_lens;
            $DtoCommunityImage->EXIF_lens_id = $communityImage->EXIF_lens_id;
            $DtoCommunityImage->EXIF_iso = $communityImage->EXIF_iso;
            $DtoCommunityImage->EXIF_shutter_speed = $communityImage->EXIF_shutter_speed;
            $DtoCommunityImage->EXIF_aperture = $communityImage->EXIF_aperture;
            $DtoCommunityImage->EXIF_focal_lenght = $communityImage->EXIF_focal_lenght;
            $DtoCommunityImage->editor_pick = $communityImage->editor_pick;
            $DtoCommunityImage->editor_pick_date = $communityImage->editor_pick_date;
            $DtoCommunityImage->online = $communityImage->online;


            $DtoCommunityImage->ImageCategoryId = CommunityImageCategoryLanguage::where('image_category_id', $communityImage->image_category_id)->get()->first()->description;
            $DtoCommunityImage->UploadDateTime = $communityImage->upload_datetime;
            
            $DtoCommunityImage->img = $communityImage->img;
 
            if ($communityImage->img != '') {
                $path_parts = pathinfo($communityImage->img);
                $DtoCommunityImage->imgName = $path_parts['basename'];

                $ch = curl_init($communityImage->img);
                curl_setopt($ch, CURLOPT_NOBODY, true);
                curl_exec($ch);
                $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                
                if ($code == 200) {
                    $size = getimagesize($communityImage->img);
                    $DtoCommunityImage->width = $size[0];
                    $DtoCommunityImage->height = $size[1];
                } else {
                    $DtoCommunityImage->width = 0;
                    $DtoCommunityImage->height = 0;
                }
                $weight = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
                curl_close($ch);
                $dimension = static::_convertImageSize($weight);
                $DtoCommunityImage->weight = $dimension;
            } else {
                $DtoCommunityImage->imgName = 'Nessuna immagine presente';
                $DtoCommunityImage->width = 0;
                $DtoCommunityImage->height = 0;
                $DtoCommunityImage->weight = 0;
            }

            $result->push($DtoCommunityImage); 
        }
        return $result;

    }

    public static function getGallery($type, $idUser, $idCategoryImage, $idImageToExclude, $limit, $idCamera, $idLens)
    {

        $content = '';
        $strWhere = '';
        $strOrderBy = '';
        $strLimit = '';
        $referencesName = '';
        $rateImage = "0,0";
        $userLink = "";

        if ($idUser != "") {
            if (strtolower($type)== "favorite")
                $strWhere = $strWhere . ' AND community_favorite_images.user_id = ' . $idUser;
            else
                $strWhere = $strWhere . ' AND Community_Images.user_id = ' . $idUser;
        }

        if ($idCategoryImage != "") {
            $strWhere = $strWhere . ' AND Community_Images.image_category_id = ' . $idCategoryImage;
        }
        if ($idImageToExclude != "") {
            $strWhere = $strWhere . ' AND Community_Images.id <> ' . $idImageToExclude;
        }
        if ($idCamera != "") {
            $strWhere = $strWhere . ' AND Community_Images."EXIF_camera_id" = ' . $idCamera;
        }
        if ($idLens != "") {
            $strWhere = $strWhere . ' AND Community_Images."EXIF_lens_id" = ' . $idLens;
        }
        if ($limit != "" && $limit != "0") 
            $strLimit = $strLimit . ' LIMIT ' . $limit;

        switch (strtolower($type)) {    
            /************************************************************/
            // GALLERIA CON LE FOTO PIU' POPOLARI DELLE ULTIME 24 ORE
            /************************************************************/
            case 'popular':
                $strOrderBy = "order by CASE WHEN upload_datetime >  now() - INTERVAL '24 hour' THEN case WHEN rating IS NULL THEN 0 else rating END + 10000 ELSE case WHEN rating IS NULL THEN 0 else rating END end desc, upload_datetime DESC";
                
                $Images = DB::select(' SELECT link, img, star_rating, user_id, id, susceptible_content, image_title FROM Community_Images WHERE online = \'True\'' . $strWhere . ' ' . $strOrderBy . ' ' . $strLimit);
                if(isset($Images) && count($Images)>0){
                    foreach ($Images as $key => $cv) 
                    {

                        $previous = array_key_exists($key - 1, $Images) ? $Images[$key - 1]->link : '';
                        $next = array_key_exists($key + 1, $Images) ? $Images[$key + 1]->link : '';

                        $usersDatas = UsersDatas::where('user_id', $Images[$key]->user_id)->get()->first();
                        if(isset($usersDatas)){
                            $referencesName = $usersDatas->name . ' ' . $usersDatas->surname;
                            $userLink = $usersDatas->link;
                        }

                        if(!is_null($Images[$key]->star_rating)){
                            $rateImage = str_replace('.',',',substr($Images[$key]->star_rating, 0, -3));
                        }else{
                            $rateImage = "";
                        }

                        $htmlSusceptibleContent = "";

                        if($Images[$key]->susceptible_content){
                            $htmlSusceptibleContent = '<div class="susceptible-content" data-attr-id="' . $Images[$key]->id . '">Contenuto sensibile<br><span class="btn-vedi-foto">Vedi comunque</span></div>';
                        }

                        $htmlModify = '<div class="cont-event-image"><i class="fa fa-trash" data-attr-id="' . $Images[$key]->id . '"></i><i class="fa fa-ellipsis-h" data-attr-id="' . $Images[$key]->id . '"></i></div>';
                        $content = $content . '<a id="immagineGalleria' . $Images[$key]->id . '" data-attr-id-imageexclude="' . $idImageToExclude . '" data-attr-id-category="' . $idCategoryImage . '" data-attr-id-user="' . $idUser . '" data-attr-id-lens="' . $idLens . '" data-attr-id-camera="' . $idCamera . '" data-attr-type="' . $type . '" class="img img-community-images-list" href="'.$Images[$key]->link.'"><img alt="'.$Images[$key]->image_title.'" loading="lazy" data-src="'.$Images[$key]->img.'" class="fr-fic fr-dii" style="width:100%;"><div class="div-over-gallery"><p class="p-left">' . $referencesName . '</p><p class="p-right">' . $rateImage . '</p></div>' . $htmlModify . $htmlSusceptibleContent . '</a>';
                    }
                }else{
                    $content = "<h3 class='title-no-foto'>Nessuna immagine presente</h3>";
                }
                break;

             /************************************************************/
            // GALLERIA CON LE FOTO NUOVE
            /************************************************************/               
            case 'fresh':
                $strOrderBy = "ORDER By Id DESC";
                $Images = DB::select('SELECT link, img, star_rating, user_id, id, susceptible_content, image_title FROM Community_Images
                            WHERE online = \'True\' ' . $strWhere . ' ' . $strOrderBy. ' ' . $strLimit);
                if(isset($Images) && count($Images)>0){
                    foreach ($Images as $key => $cv) 
                    {
                        $previous = array_key_exists($key - 1, $Images) ? $Images[$key - 1]->link : '';
                        $next = array_key_exists($key + 1, $Images) ? $Images[$key + 1]->link : '';

                        $usersDatas = UsersDatas::where('user_id', $Images[$key]->user_id)->get()->first();
                        if(isset($usersDatas)){
                            $referencesName = $usersDatas->name . ' ' . $usersDatas->surname;
                            $userLink = $usersDatas->link;
                        }
                        if(!is_null($Images[$key]->star_rating)){
                            $rateImage = str_replace('.',',',substr($Images[$key]->star_rating, 0, -3));
                        }else{
                            $rateImage = "";
                        }
                        $htmlSusceptibleContent = "";

                        if($Images[$key]->susceptible_content){
                            $htmlSusceptibleContent = '<div class="susceptible-content" data-attr-id="' . $Images[$key]->id . '">Contenuto sensibile<br><span class="btn-vedi-foto">Vedi comunque</span></div>';
                        }
                        $content = $content . '<a id="immagineGalleria' . $Images[$key]->id . '" data-attr-id-imageexclude="' . $idImageToExclude . '" data-attr-id-category="' . $idCategoryImage . '" data-attr-id-user="' . $idUser . '" data-attr-id-lens="' . $idLens . '" data-attr-id-camera="' . $idCamera . '" data-attr-type="' . $type . '"  class="img img-community-images-list" href="'.$Images[$key]->link.'"><img alt="'.$Images[$key]->image_title.'" loading="lazy" data-src="'.$Images[$key]->img.'" class="fr-fic fr-dii" style="width:100%;"><div class="div-over-gallery"><p class="p-left">' . $referencesName . '</p><p class="p-right">' . $rateImage . '</p></div>' . $htmlSusceptibleContent . '</a>';
                    }     
                }else{
                    $content = "<h3 class='title-no-foto'>Nessuna immagine presente</h3>";
                }          
                break;

             /************************************************************/
            // GALLERIA CON LE FOTO PREFERITE
            /************************************************************/               
            case 'favorite':
                $strOrderBy = "ORDER By community_favorite_images.Id DESC";

                $Images = DB::select('SELECT link, img, community_images.star_rating, community_images.user_id, community_images.id, susceptible_content, image_title FROM community_favorite_images
                            inner join community_images on community_favorite_images.image_id = community_images.id 
                            WHERE online = \'True\''. $strWhere . ' ' . $strOrderBy. ' ' . $strLimit);
                if(isset($Images) && count($Images)>0){
                    foreach ($Images as $key => $cv) 
                    {

                        $previous = array_key_exists($key - 1, $Images) ? $Images[$key - 1]->link : '';
                        $next = array_key_exists($key + 1, $Images) ? $Images[$key + 1]->link : '';

                        $usersDatas = UsersDatas::where('user_id', $Images[$key]->user_id)->get()->first();
                        if(isset($usersDatas)){
                            $referencesName = $usersDatas->name . ' ' . $usersDatas->surname;
                            $userLink = $usersDatas->link;
                        }
                        if(!is_null($Images[$key]->star_rating)){
                            $rateImage = str_replace('.',',',substr($Images[$key]->star_rating, 0, -3));
                        }else{
                            $rateImage = "";
                        }

                        $htmlSusceptibleContent = "";

                        if($Images[$key]->susceptible_content){
                            $htmlSusceptibleContent = '<div class="susceptible-content" data-attr-id="' . $Images[$key]->id . '">Contenuto sensibile<br><span class="btn-vedi-foto">Vedi comunque</span></div>';
                        }

                        $content = $content . '<a id="immagineGalleria' . $Images[$key]->id . '" data-attr-id-imageexclude="' . $idImageToExclude . '" data-attr-id-category="' . $idCategoryImage . '" data-attr-id-user="' . $idUser . '" data-attr-id-lens="' . $idLens . '" data-attr-id-camera="' . $idCamera . '" data-attr-type="' . $type . '" class="img img-community-images-list" href="'.$Images[$key]->link.'"><img alt="'.$Images[$key]->image_title.'" loading="lazy" data-src="'.$Images[$key]->img.'" class="fr-fic fr-dii" style="width:100%;"><div class="div-over-gallery"><p class="p-left">' . $referencesName . '</p><p class="p-right">' . $rateImage . '</p></div>' . $htmlSusceptibleContent . '</a>';
                        
                    } 
                }else{
                    $content = "<h3 class='title-no-foto'>Nessuna immagine presente</h3>";
                }              
                break;

             /************************************************************/
            // GALLERIA CON LE FOTO SCELTE DA NOI
            /************************************************************/               
            case 'editorpick':
                $strOrderBy = "ORDER By Editor_Pick_Date DESC";
                $Images = DB::select('SELECT link, img, star_rating, user_id, susceptible_content, image_title, id FROM Community_Images WHERE online = \'True\' AND editor_pick = \'True\''. $strWhere . ' ' . $strOrderBy. ' ' . $strLimit);
                if(isset($Images) && count($Images)>0){
                    foreach ($Images as $key => $cv)  
                    {
                        $previous = array_key_exists($key - 1, $Images) ? $Images[$key - 1]->link : '';
                        $next = array_key_exists($key + 1, $Images) ? $Images[$key + 1]->link : '';

                        $usersDatas = UsersDatas::where('user_id', $Images[$key]->user_id)->get()->first();
                        if(isset($usersDatas)){
                            $referencesName = $usersDatas->name . ' ' . $usersDatas->surname;
                            $userLink = $usersDatas->link;
                        }
                        if(!is_null($Images[$key]->star_rating)){
                            $rateImage = str_replace('.',',',substr($Images[$key]->star_rating, 0, -3));
                        }else{
                            $rateImage = "";
                        }

                        $htmlSusceptibleContent = "";

                        if($Images[$key]->susceptible_content){
                            $htmlSusceptibleContent = '<div class="susceptible-content" data-attr-id="' . $Images[$key]->id . '">Contenuto sensibile<br><span class="btn-vedi-foto">Vedi comunque</span></div>';
                        }
                        $content = $content . '<a id="immagineGalleria' . $Images[$key]->id . '" data-attr-id-imageexclude="' . $idImageToExclude . '" data-attr-id-category="' . $idCategoryImage . '" data-attr-id-user="' . $idUser . '" data-attr-id-lens="' . $idLens . '" data-attr-id-camera="' . $idCamera . '" data-attr-type="' . $type . '" class="img img-community-images-list" href="'.$Images[$key]->link.'"><img alt="'.$Images[$key]->image_title.'" loading="lazy" data-src="'.$Images[$key]->img.'" class="fr-fic fr-dii" style="width:100%;"><div class="div-over-gallery"><p class="p-left">' . $referencesName . '</p><p class="p-right">' . $rateImage . '</p></div>' . $htmlSusceptibleContent . '</a>';
                    }  
                }else{
                    $content = "<h3 class='title-no-foto'>Nessuna immagine presente</h3>";
                }              
                break;
             /************************************************************/
            // ELENCO DEGLI ULTIMI N COMMENTI
            /************************************************************/               
            case 'last_comment':
                $s = 'select ci.link , ci.img, ci.image_title, concat(ud."name", \' \', ud.surname) as user_name, cic."comment" ';
                $s = $s . 'from community_images_comments as cic ';
                $s = $s . 'inner join community_images ci on ci.id = cic.image_id ';
                $s = $s . 'left outer join users_datas ud on cic.user_id = ud.user_id ';
                $s = $s . 'order by comment_datetime desc ' . $strLimit;
                $Images = DB::select($s);
                if(isset($Images) && count($Images)>0){
                    
                    $htmlApp = ContentBL::getByCode('Community_Image_Comment_Homepage');

                    foreach ($Images as $key => $cv)  
                    {

                        $htmlTemp = $htmlApp->content;
                        $htmlTemp = str_replace('#link#', $Images[$key]->link,  $htmlTemp);  
                        $htmlTemp = str_replace('#img#', $Images[$key]->img,  $htmlTemp); 
                        $htmlTemp = str_replace('#image_title#', $Images[$key]->image_title,  $htmlTemp); 
                        $htmlTemp = str_replace('#user_name#', $Images[$key]->user_name,  $htmlTemp); 
                        $htmlTemp = str_replace('#comment#', $Images[$key]->comment,  $htmlTemp); 
                       
                        $content = $content . $htmlTemp;
                    }  
                }else{
                    $content = "<h3 class='title-no-foto'>Nessuna commento presente</h3>";
                }              
                break;

            default:
                
                foreach (CommunityImage::orderBy('id','DESC')->get() as $communityImage) {
                    $usersDatas = UsersDatas::where('user_id', $communityImage->user_id)->get()->first();
                    if(isset($usersDatas)){
                        $referencesName = $usersDatas->name . ' ' . $usersDatas->surname;
                        $userLink = $usersDatas->link;
                    }
                    if(!is_null($communityImage->star_rating)){
                        $rateImage = str_replace('.',',',substr($communityImage->star_rating, 0, -3));
                    }else{
                        $rateImage = "";
                    }
                    $htmlSusceptibleContent = "";

                    if($communityImage->susceptible_content){
                        $htmlSusceptibleContent = '<div class="susceptible-content" data-attr-id="' . $communityImage->id . '">Contenuto sensibile<span class="btn-vedi-foto">Vedi comunque</span></div>';
                    }
                    $content = $content . '<a id="immagineGalleria' . $communityImage->id . '" class="img img-community-images-list" data-attr-id-imageexclude="' . $idImageToExclude . '" data-attr-id-category="' . $idCategoryImage . '" data-attr-id-user="' . $idUser . '" data-attr-id-lens="' . $idLens . '" data-attr-id-camera="' . $idCamera . '" data-attr-type="" href="'.$communityImage->link.'"><img alt="'.$communityImage->image_title.'" loading="lazy" data-src="'.$communityImage->img.'" class="fr-fic fr-dii" style="width:100%;"><div class="div-over-gallery"><p class="p-left">' . $referencesName . '</p><p class="p-right">' . $rateImage . '</p></div>' . $htmlSusceptibleContent . '</a>';
                }
        }

        return $content;

    }

    public static function getGalleryPaginator($type, $idUser, $idCategoryImage, $idImageToExclude, $limit, $idCamera, $idLens, $page)
    {
        $content = '';
        $strWhere = '';
        $strOrderBy = '';
        $strLimit = '';
        $referencesName = '';
        $rateImage = "0,0";
        $userLink = "";
        $strSqlPagination = "";

        if ($idUser != "") {
            if (strtolower($type)== "favorite")
                $strWhere = $strWhere . ' AND community_favorite_images.user_id = ' . $idUser;
            else
                $strWhere = $strWhere . ' AND Community_Images.user_id = ' . $idUser;
        }


        if ($idCategoryImage != "") {
            $strWhere = $strWhere . ' AND Community_Images.image_category_id = ' . $idCategoryImage;
        }
        if ($idImageToExclude != "") {
            $strWhere = $strWhere . ' AND Community_Images.id <> ' . $idImageToExclude;
        }
        if ($idCamera != "") {
            $strWhere = $strWhere . ' AND Community_Images."EXIF_camera_id" = ' . $idCamera;
        }
        if ($idLens != "") {
            $strWhere = $strWhere . ' AND Community_Images."EXIF_lens_id" = ' . $idLens;
        }
        if ($limit != "" && $limit != "0") 
            $strLimit = $strLimit . ' LIMIT ' . $limit;


        if ($limit != "" && $limit != "0") {
            $itemsPerPage = $limit;
        }else{
            $itemsPerPage = 20;
        }

        $pageSelected = 1;
        if ($page != "" && $page != "0") {
            $pageSelected = $page; 
        }

        switch (strtolower($type)) {    
            /************************************************************/
            // GALLERIA CON LE FOTO PIU' POPOLARI DELLE ULTIME 24 ORE
            /************************************************************/
            case 'popular':
                $strOrderBy = "order by CASE WHEN upload_datetime >  now() - INTERVAL '24 hour' THEN case WHEN rating IS NULL THEN 0 else rating END + 10000 ELSE case WHEN rating IS NULL THEN 0 else rating END end desc, upload_datetime DESC";
                                
                $totItems = DB::select('SELECT count(id) as cont FROM Community_Images WHERE online = \'True\'' . $strWhere);
                
                if($totItems[0]->cont <= $itemsPerPage){
                    $strSqlPagination = "";
                    $content = "";
                }else{
                    $limit = $itemsPerPage;
                    $offset = $itemsPerPage * ($pageSelected - 1);

                    $strSqlPagination = " offset " . $offset;
                

                    $Images = DB::select(' SELECT link, img, star_rating, user_id, id FROM Community_Images WHERE online = \'True\'' . $strWhere . ' ' . $strOrderBy . ' ' . $strLimit . ' ' . $strSqlPagination);

                    foreach ($Images as $key => $cv) 
                    {
                        $previous = array_key_exists($key - 1, $Images) ? $Images[$key - 1]->link : '';
                        $next = array_key_exists($key + 1, $Images) ? $Images[$key + 1]->link : '';

                        $usersDatas = UsersDatas::where('user_id', $Images[$key]->user_id)->get()->first();
                        if(isset($usersDatas)){
                            $referencesName = $usersDatas->name . ' ' . $usersDatas->surname;
                            $userLink = $usersDatas->link;
                        }

                        if(!is_null($Images[$key]->star_rating)){
                            $rateImage = str_replace('.',',',substr($Images[$key]->star_rating, 0, -3));
                        }else{
                            $rateImage = "";
                        }

                        $content = $content . '<a id="immagineGalleria'.$Images[$key]->id.'" data-attr-id-imageexclude="' . $idImageToExclude . '" data-attr-id-category="' . $idCategoryImage . '" data-attr-id-user="' . $idUser . '" data-attr-id-lens="' . $idLens . '" data-attr-id-camera="' . $idCamera . '" data-attr-type="' . $type . '" class="img img-community-images-list" href="'.$Images[$key]->link.'"><img loading="lazy" src="'.$Images[$key]->img.'" class="fr-fic fr-dii" style="width:100%;"><div class="div-over-gallery"><p class="p-left">' . $referencesName . '</p><p class="p-right">' . $rateImage . '</p></div></a>';
                    }
                }
                break;

             /************************************************************/
            // GALLERIA CON LE FOTO NUOVE
            /************************************************************/               
            case 'fresh':
                $strOrderBy = "ORDER By Id DESC";

                $totItems = DB::select('SELECT count(id) as cont FROM Community_Images WHERE online = \'True\'' . $strWhere);
                
                if($totItems[0]->cont <= $itemsPerPage){
                    $strSqlPagination = "";
                }else{
                    $limit = $itemsPerPage;
                    $offset = $itemsPerPage * ($pageSelected - 1);

                    $strSqlPagination = " offset " . $offset;
                }


                $Images = DB::select('SELECT id, link, img, star_rating, user_id FROM Community_Images
                            WHERE online = \'True\''. $strWhere . ' ' . $strOrderBy. ' ' . $strLimit . ' ' . $strSqlPagination);
                foreach ($Images as $key => $cv) 
                {
                    $previous = array_key_exists($key - 1, $Images) ? $Images[$key - 1]->link : '';
                    $next = array_key_exists($key + 1, $Images) ? $Images[$key + 1]->link : '';

                    $usersDatas = UsersDatas::where('user_id', $Images[$key]->user_id)->get()->first();
                    if(isset($usersDatas)){
                        $referencesName = $usersDatas->name . ' ' . $usersDatas->surname;
                        $userLink = $usersDatas->link;
                    }
                    if(!is_null($Images[$key]->star_rating)){
                        $rateImage = str_replace('.',',',substr($Images[$key]->star_rating, 0, -3));
                    }else{
                        $rateImage = "";
                    }
                    $content = $content . '<a id="immagineGalleria'.$Images[$key]->id.'" data-attr-id-imageexclude="' . $idImageToExclude . '" data-attr-id-category="' . $idCategoryImage . '" data-attr-id-user="' . $idUser . '" data-attr-id-lens="' . $idLens . '" data-attr-id-camera="' . $idCamera . '" data-attr-type="' . $type . '"  class="img img-community-images-list" href="'.$Images[$key]->link.'"><img loading="lazy" src="'.$Images[$key]->img.'" class="fr-fic fr-dii" style="width:100%;"><div class="div-over-gallery"><p class="p-left">' . $referencesName . '</p><p class="p-right">' . $rateImage . '</p></div></a>';
                }               
                break;

             /************************************************************/
            // GALLERIA CON LE FOTO PREFERITE
            /************************************************************/               
            case 'favorite':
                $strOrderBy = "ORDER By community_favorite_images.Id DESC";

                $Images = DB::select('SELECT link, img, community_images.star_rating, community_images.user_id, community_images.id FROM community_favorite_images
                            inner join community_images on community_favorite_images.image_id = community_images.id 
                            WHERE online = \'True\''. $strWhere . ' ' . $strOrderBy. ' ' . $strLimit);

                foreach ($Images as $key => $cv) 
                {

                    $previous = array_key_exists($key - 1, $Images) ? $Images[$key - 1]->link : '';
                    $next = array_key_exists($key + 1, $Images) ? $Images[$key + 1]->link : '';

                    $usersDatas = UsersDatas::where('user_id', $Images[$key]->user_id)->get()->first();
                    if(isset($usersDatas)){
                        $referencesName = $usersDatas->name . ' ' . $usersDatas->surname;
                        $userLink = $usersDatas->link;
                    }
                    if(!is_null($Images[$key]->star_rating)){
                        $rateImage = str_replace('.',',',substr($Images[$key]->star_rating, 0, -3));
                    }else{
                        $rateImage = "";
                    }
                    $content = $content . '<a id="immagineGalleria'.$Images[$key]->id.'" data-attr-id-imageexclude="' . $idImageToExclude . '" data-attr-id-category="' . $idCategoryImage . '" data-attr-id-user="' . $idUser . '" data-attr-id-lens="' . $idLens . '" data-attr-id-camera="' . $idCamera . '" data-attr-type="' . $type . '" class="img img-community-images-list" href="'.$Images[$key]->link.'"><img loading="lazy" src="'.$Images[$key]->img.'" class="fr-fic fr-dii" style="width:100%;"><div class="div-over-gallery"><p class="p-left">' . $referencesName . '</p><p class="p-right">' . $rateImage . '</p></div></a>';
                    
                }               
                break;

             /************************************************************/
            // GALLERIA CON LE FOTO SCELTE DA NOI
            /************************************************************/               
            case 'editorpick':
                $strOrderBy = "ORDER By Editor_Pick_Date DESC";
                $Images = DB::select('SELECT link, img, star_rating, user_id, id FROM Community_Images WHERE online = \'True\' AND editor_pick = \'True\''. $strWhere . ' ' . $strOrderBy. ' ' . $strLimit);
                
                foreach ($Images as $key => $cv)  
                {
                    $previous = array_key_exists($key - 1, $Images) ? $Images[$key - 1]->link : '';
                    $next = array_key_exists($key + 1, $Images) ? $Images[$key + 1]->link : '';

                    $usersDatas = UsersDatas::where('user_id', $Images[$key]->user_id)->get()->first();
                    if(isset($usersDatas)){
                        $referencesName = $usersDatas->name . ' ' . $usersDatas->surname;
                        $userLink = $usersDatas->link;
                    }
                    if(!is_null($Images[$key]->star_rating)){
                        $rateImage = str_replace('.',',',substr($Images[$key]->star_rating, 0, -3));
                    }else{
                        $rateImage = "";
                    }
                    $content = $content . '<a id="immagineGalleria'.$Images[$key]->user_id.'" data-attr-id-imageexclude="' . $idImageToExclude . '" data-attr-id-category="' . $idCategoryImage . '" data-attr-id-user="' . $idUser . '" data-attr-id-lens="' . $idLens . '" data-attr-id-camera="' . $idCamera . '" data-attr-type="' . $type . '" class="img img-community-images-list" href="'.$Images[$key]->link.'"><img loading="lazy" src="'.$Images[$key]->img.'" class="fr-fic fr-dii" style="width:100%;"><div class="div-over-gallery"><p class="p-left">' . $referencesName . '</p><p class="p-right">' . $rateImage . '</p></div></a>';
                }               
                break;

            default:
                foreach (CommunityImage::orderBy('id','DESC')->get() as $communityImage) {
                    $usersDatas = UsersDatas::where('user_id', $communityImage->user_id)->get()->first();
                    if(isset($usersDatas)){
                        $referencesName = $usersDatas->name . ' ' . $usersDatas->surname;
                        $userLink = $usersDatas->link;
                    }
                    if(!is_null($communityImage->star_rating)){
                        $rateImage = str_replace('.',',',substr($communityImage->star_rating, 0, -3));
                    }else{
                        $rateImage = "";
                    }
                    $content = $content . '<a id="immagineGalleria'.$communityImage->id.'" class="img img-community-images-list" data-attr-id-imageexclude="' . $idImageToExclude . '" data-attr-id-category="' . $idCategoryImage . '" data-attr-id-user="' . $idUser . '" data-attr-id-lens="' . $idLens . '" data-attr-id-camera="' . $idCamera . '" data-attr-type="" href="'.$communityImage->link.'"><img loading="lazy" src="'.$communityImage->img.'" class="fr-fic fr-dii" style="width:100%;"><div class="div-over-gallery"><p class="p-left">' . $referencesName . '</p><p class="p-right">' . $rateImage . '</p></div></a>';
                }
        }

        return $content;

    }

    public static function getHtml($url, $idUser)
    {
        if (strpos($url, '/photo/') !== false) {
            
            $content = CommunityImage::where('link', $url)->first();
            
            if (isset($content)) {
                
                if (is_null($idUser) || $idUser == '') {
                    $idUserNew = null;
                }else{
                    $idUserNew = $idUser;
                }
                $idContent = Content::where('code', 'Community_Image_Detail')->first();
                $languageContent = ContentLanguage::where('content_id', $idContent->id)->first();
                $contentHtml = ContentLanguageBL::getByCodeRender($idContent->id, $languageContent->language_id, $url, null, null, $idUserNew);
                $dtoPageItemRender = new DtoItem();
                $dtoPageItemRender->html = $contentHtml;
                $dtoPageItemRender->description = $content->description;
                
                $dtoPageItemRender->meta_tag_title = $content->image_title;

                if(!is_null($content->description)){
                    $dtoPageItemRender->meta_tag_description = $content->description;
                    $dtoPageItemRender->share_description = $content->description;
                }else{
                    $dtoPageItemRender->meta_tag_description = $content->image_title . ' - Camera Nation community';
                    $dtoPageItemRender->share_description = $content->image_title . ' - Camera Nation community';
                }
                $dtoPageItemRender->share_title = $content->image_title;

                if(!is_null($content->keywords)){
                    $dtoPageItemRender->meta_tag_keyword = $content->keywords . ' - Camera Nation community';
                    $dtoPageItemRender->seo_keyword = $content->keywords . ' - Camera Nation community';
                }else{
                    $dtoPageItemRender->seo_keyword = $content->image_title;
                    $dtoPageItemRender->meta_tag_keyword = $content->image_title . ' - Camera Nation community';
                }


                $dtoPageItemRender->seo_description = $content->description;
                $dtoPageItemRender->seo_title = $content->image_title;
                
                
                if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
                    $protocol = "https://";
                } else {
                    $protocol = "http://";
                }

                //if (strpos($content->img, "https://") !== false || strpos($content->img, "http://") !== false) {
                    $dtoPageItemRender->url_cover_image = $content->img_share;
                //}else{
                //    $dtoPageItemRender->url_cover_image = $protocol . $_SERVER["HTTP_HOST"] . $content->img;
                //}

                return $dtoPageItemRender;
            
            } else {
                return '';
            }
        }else
            return '';
    }


    #endregion GET


      #region INSERT

    /**
     * Insert
     * 
     * @param Request dtoCommunityImage
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $dtoCommunityImage)
    {
        DB::beginTransaction();

        try {
            // controllo immagini valorizzate
            if(isset($dtoCommunityImage->images)){
                // ciclo array immagini con generazione iniziale dell'immagine principale (cartella /storage/imagesUser/)
		    foreach ($dtoCommunityImage->images as $arrayImages) {
			   
                    // controllo che sia esistente la cartella dell'utente altrimenti creo i percorsi per salvare le foto
                    
                    /* IMPLEMENTAZIONE AMAZON AWS S3 */

			if (!Storage::disk('s3')->exists($dtoCommunityImage->userId)) {
	
                        // creazione cartella utente
                        Storage::disk('s3')->makeDirectory($dtoCommunityImage->userId);

                        // creazione cartella resize contiente le immagini ridimensionate per altezza a 400px
                        Storage::disk('s3')->makeDirectory($dtoCommunityImage->userId . '/preview');

                        // creazione cartella resize contente le immagini ridimensionate per larghezza a 1200px
                        Storage::disk('s3')->makeDirectory($dtoCommunityImage->userId . '/main');

                    }else{
			
                        if (!Storage::disk('s3')->exists($dtoCommunityImage->userId . '/preview')) {
                            Storage::disk('s3')->makeDirectory($dtoCommunityImage->userId . '/preview');
                        }

			if (!Storage::disk('s3')->exists($dtoCommunityImage->userId . '/main')) {

                            Storage::disk('s3')->makeDirectory($dtoCommunityImage->userId . '/main');
                        }
                    }

			if (!Storage::disk('s4')->exists($dtoCommunityImage->userId)) {
	                        Storage::disk('s4')->makeDirectory($dtoCommunityImage->userId . '/original');
                    }

                    /* END IMPLEMENTAZIONE AMAZON AWS S3 */

                    //creo la riga nel database così da rendere univoci l'url e il nome dell'immagine successivamente
                    $CommunityImage = new CommunityImage();
                    $CommunityImage->user_id = $dtoCommunityImage->userId;
                    $CommunityImage->image_title = $arrayImages['title'];
                    $CommunityImage->image_category_id = $arrayImages['category'];
                    $CommunityImage->image_description = $arrayImages['description'];
                    $CommunityImage->upload_datetime = date("Y-m-d H:i:s");
                    $CommunityImage->online = true;
                    $CommunityImage->susceptible_content = $arrayImages['explicitContent'];
                    $CommunityImage->save();

                    //salvo l'id della riga dell'immagine
                    $idCommunityImage = $CommunityImage->id;

                    if($arrayImages['title'] != ''){
                        // funzione per inserire l'immagine con il nome "pulito" e univoco tramite l'id
                        $imgNameByTitle = $idCommunityImage . '-' . static::_cleanStr($arrayImages['title']) . '.webp';
                        $imgNameByTitleShare = $idCommunityImage . '-' . static::_cleanStr($arrayImages['title']) . '.jpg';
                    }else{
                        $imgNameByTitle = $idCommunityImage . '-' . $arrayImages['imgName']; 
                        $imgNameByTitleShare = $idCommunityImage . '-' . $arrayImages['imgName']; 
                    }

                    //creazione immagine principale da cui poi verranno ricavati i dati exif
                   
                    /* IMPLEMENTAZIONE AMAZON AWS S3 */
		   
                    $data = explode( ',', $arrayImages['imgBase64']);
                    Storage::disk('s4')->put($dtoCommunityImage->userId . '/original/'. $imgNameByTitle, base64_decode($data[1]));  
		  
                    /* END IMPLEMENTAZIONE AMAZON AWS S3 */

                    $exif_ifd0 = @exif_read_data($arrayImages['imgBase64']);      
                    $exif_exif = @exif_read_data($arrayImages['imgBase64']);

		    
                    //error control
                    $notFound = "";

                    // Make EXIF_camera
                    if (@array_key_exists('Make', $exif_ifd0)) {
                        $camMake = $exif_ifd0['Make'];
                    } else { 
                        $camMake = $notFound; 
                    }
                    
                    // Model EXIF_camera
                    if (@array_key_exists('Model', $exif_ifd0)) {
                        $camModel = $exif_ifd0['Model'];
                    } else { 
                        $camModel = $notFound; 
                    }
                    
                    // Exposure EXIF_shutter_speed
                    if (@array_key_exists('ExposureTime', $exif_ifd0)) {
                        $camExposure = $exif_ifd0['ExposureTime'];
                    } else { 
                        $camExposure = $notFound; 
                    }

                    // Aperture EXIF_aperture
                    if (@array_key_exists('ApertureFNumber', $exif_ifd0['COMPUTED'])) {
                        $camAperture = $exif_ifd0['COMPUTED']['ApertureFNumber'];
                    } else { 
                        $camAperture = $notFound; 
                    }
                    
                    // LENS EXIF_focal_lenght
                    if (@array_key_exists('FocalLength', $exif_exif)) {
                        $camFocalLength = $exif_exif['FocalLength'];
                    } else { 
                        $camFocalLength = $notFound; 
                    }
                    
                    // ISO EXIF_iso
                    if (@array_key_exists('ISOSpeedRatings', $exif_exif)) {
                        $camIso = $exif_exif['ISOSpeedRatings'];
                    } else { 
                        $camIso = $notFound; 
                    }

                    // LENS EXIF_lens
                    if (@array_key_exists('UndefinedTag:0xA434', $exif_exif)) {
                        $camLens = $exif_exif['UndefinedTag:0xA434'];
                    } else { 
                        $camLens = $notFound; 
                    }
                    
                    /* IMPLEMENTAZIONE AMAZON AWS S3 */

                    $resizedImage = Image::make($arrayImages['imgBase64'])->encode('webp', 100)->resize(null, 400, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    Storage::disk('s3')->put($dtoCommunityImage->userId . '/preview/'. $imgNameByTitle, $resizedImage->stream()->__toString(), 'public'); 


                    $resizedImageShare = Image::make($arrayImages['imgBase64'])->resize(null, 400, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    Storage::disk('s3')->put($dtoCommunityImage->userId . '/preview/'. $imgNameByTitleShare, $resizedImageShare->stream()->__toString(), 'public'); 
                    

                    $resizedImagedetail = Image::make($arrayImages['imgBase64'])->encode('webp', 100)->resize(1200, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });

                    Storage::disk('s3')->put($dtoCommunityImage->userId . '/main/'. $imgNameByTitle, $resizedImagedetail->stream()->__toString(), 'public'); 
                    
                    /* END IMPLEMENTAZIONE AMAZON AWS S3 */

                    $CommunityImageOnDb = CommunityImage::find($idCommunityImage);
                    if(isset($CommunityImageOnDb)){

                        $CommunityImageOnDb->EXIF_iso = $camIso;
                        $CommunityImageOnDb->EXIF_focal_lenght = $camFocalLength;
                        $CommunityImageOnDb->EXIF_aperture = $camAperture;
                        $CommunityImageOnDb->EXIF_shutter_speed = $camExposure;

                        $CommunityImageOnDb->EXIF_camera = $camMake . ' ' . $camModel;
                        $CommunityImageOnDb->EXIF_lens = $camLens;

                        if (rtrim(ltrim($camMake . ' ' . $camModel))!=''){
                            $itemLanguagesCamera = ItemLanguage::where('description', 'ilike', $camMake . ' ' . $camModel)->orWhere('description', 'ilike', $camModel)->get()->first();
                
                            if(isset($itemLanguagesCamera)){
                                $CommunityImageOnDb->EXIF_camera_id = $itemLanguagesCamera->item_id;
                            }else{
                                //se non lo trovo provo a vedere dentro la tabella items_exif_data_match
                                $itemLanguagesCamera = ItemExifDataMatch::where('EXIF_item_description', 'ilike', $camMake . ' ' . $camModel)->get()->first();
                                if(isset($itemLanguagesCamera)){
                                    $CommunityImageOnDb->EXIF_camera_id = $itemLanguagesCamera->item_id;
                                }else{
                                    //lo inserisco così da averlo le prossime volte convertito in modo corretto.
                                    $itemsExifMiss = new ItemExifDataMatch();
                                    $itemsExifMiss->EXIF_item_description = $camMake . ' ' . $camModel;
                                    $itemsExifMiss->created_id = $request->$idUser;
                                    $itemsExifMiss->save();
                                }
                            }
                        }

                        if (rtrim(ltrim($camLens))!=''){
                            $itemLanguagesLens = ItemLanguage::where('description', 'ilike', $camLens)->get()->first();
                            if(isset($itemLanguagesLens)){
                                $CommunityImageOnDb->EXIF_lens_id = $itemLanguagesLens->item_id;
                            }
                        }

                        $CommunityImageOnDb->link = '/photo/' . static::_cleanStr($arrayImages['title']) . '-' . $idCommunityImage;

                        $CommunityImageOnDb->img = '/s3/' . $dtoCommunityImage->userId . '/preview/' . $imgNameByTitle;

                        $CommunityImageOnDb->img_share = 'https://cameranation.it/s3/' . $dtoCommunityImage->userId . '/preview/' . $imgNameByTitleShare;
                        //$CommunityImageOnDb->img = 'https://community-cameranation.s3.eu-central-1.amazonaws.com/' . $dtoCommunityImage->userId . '/preview/' . $imgNameByTitle;

                        $CommunityImageOnDb->img_original = 'https://community-private-cameranation.s3.eu-central-1.amazonaws.com/' . $dtoCommunityImage->userId . '/original/' . $imgNameByTitle;

                        $CommunityImageOnDb->img_detail = '/s3/' . $dtoCommunityImage->userId . '/main/' . $imgNameByTitle;
                        //$CommunityImageOnDb->img_detail = 'https://community-cameranation.s3.eu-central-1.amazonaws.com/' . $dtoCommunityImage->userId . '/main/' . $imgNameByTitle;

                        $CommunityImageOnDb->save();

                        $userDatas = UsersDatas::where('user_id', $dtoCommunityImage->userId)->get()->first();

                        if(isset($userDatas)){
                            $return = $userDatas->link;
                        }else{
                            $return = "Immagini caricate correttamente!";
                        }

                    }else{
                        $return = "Ops qualcosa è andato storto, riprovare più tardi!";
                    }

                }
            }else{
                $return = "Immagini non presenti";
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $return;

        
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoCommunityImage
     * @param int idLanguage
     */
    public static function update(Request $dtoCommunityImage)
    {
      //  $CommunityImage = CommunityImage::find($dtoCommunityImage->id);
      //  DBHelper::updateAndSave(static::_convertToModel($dtoCommunityImage), $CommunityImage);

      $CommunityImage = CommunityImage::where('id', $dtoCommunityImage->id)->first();

      DB::beginTransaction();
      try {

        if (isset($dtoCommunityImage->id)) {
            $CommunityImage->id = $dtoCommunityImage->id;
        }

        if (isset($dtoCommunityImage->users_id)) {
            $CommunityImage->user_id = $dtoCommunityImage->users_id;
        }

        if (isset($dtoCommunityImage->image_title)) {
            $CommunityImage->image_title = $dtoCommunityImage->image_title;
        }

        if (isset($dtoCommunityImage->image_category_id)) {
            $CommunityImage->image_category_id = $dtoCommunityImage->image_category_id;
        }

        if (isset($dtoCommunityImage->gallery_id)) {
            $CommunityImage->gallery_id = $dtoCommunityImage->gallery_id;
        } else
            $CommunityImage->gallery_id = null;

        if (isset($dtoCommunityImage->image_description)) {
            $CommunityImage->image_description = $dtoCommunityImage->image_description;
        } else
            $CommunityImage->image_description = null;
        
        if (isset($dtoCommunityImage->location_latitude)) {
            $CommunityImage->location_latitude = $dtoCommunityImage->location_latitude;
        } else
            $CommunityImage->location_latitude = null;

        if (isset($dtoCommunityImage->location_longitude)) {
            $CommunityImage->location_longitude = $dtoCommunityImage->location_longitude;
        } else
            $CommunityImage->location_longitude =  null;

        if (isset($dtoCommunityImage->keywords)) {
            $CommunityImage->keywords = $dtoCommunityImage->keywords;
        }else
            $CommunityImage->keywords = null;

        if (isset($dtoCommunityImage->susceptible_content)) {
            $CommunityImage->susceptible_content = $dtoCommunityImage->susceptible_content;
        }

        if (isset($dtoCommunityImage->upload_datetime)) {
            $CommunityImage->upload_datetime = $dtoCommunityImage->upload_datetime;
        }

        if (isset($dtoCommunityImage->star_rating)) {
            $CommunityImage->star_rating = $dtoCommunityImage->star_rating;
        }   else
            $CommunityImage->star_rating = 0;

        if (isset($dtoCommunityImage->nr_views)) {
            $CommunityImage->nr_views = $dtoCommunityImage->nr_views;
        } else 
            $CommunityImage->nr_views = 0;

        if (isset($dtoCommunityImage->rating)) {
            $CommunityImage->rating = $dtoCommunityImage->rating;
        }   else
            $CommunityImage->rating = 0;

        if (isset($dtoCommunityImage->EXIF_camera)) {
            $CommunityImage->EXIF_camera = $dtoCommunityImage->EXIF_camera;
        }   else
            $CommunityImage->EXIF_camera = null;

        if (isset($dtoCommunityImage->EXIF_camera_id)) {
            $CommunityImage->EXIF_camera_id = $dtoCommunityImage->EXIF_camera_id;
        } else 
            $CommunityImage->EXIF_camera_id = null;

        if (isset($dtoCommunityImage->EXIF_lens)) {
            $CommunityImage->EXIF_lens = $dtoCommunityImage->EXIF_lens;
        }   else
            $CommunityImage->EXIF_lens = null;            

        if (isset($dtoCommunityImage->EXIF_lens_id)) {
            $CommunityImage->EXIF_lens_id = $dtoCommunityImage->EXIF_lens_id;
        }   else
            $CommunityImage->EXIF_lens_id = null;

        if (isset($dtoCommunityImage->EXIF_iso)) {
            $CommunityImage->EXIF_iso = $dtoCommunityImage->EXIF_iso;
        } else 
            $CommunityImage->EXIF_iso = null;

        if (isset($dtoCommunityImage->EXIF_shutter_speed)) {
            $CommunityImage->EXIF_shutter_speed = $dtoCommunityImage->EXIF_shutter_speed;
        }   else
            $CommunityImage->EXIF_shutter_speed = null;  
            
        if (isset($dtoCommunityImage->EXIF_aperture)) {
            $CommunityImage->EXIF_aperture = $dtoCommunityImage->EXIF_aperture;
        }   else
            $CommunityImage->EXIF_aperture = null;

        if (isset($dtoCommunityImage->EXIF_focal_lenght)) {
            $CommunityImage->EXIF_focal_lenght = $dtoCommunityImage->EXIF_focal_lenght;
        } else 
            $CommunityImage->EXIF_focal_lenght = null;

        if (isset($dtoCommunityImage->editor_pick)) {
            $CommunityImage->editor_pick = $dtoCommunityImage->editor_pick;
        }   else
            $CommunityImage->editor_pick = 0;  
      
        if (isset($dtoCommunityImage->editor_pick_date)) {
            $CommunityImage->editor_pick_date = $dtoCommunityImage->editor_pick_date;
        } else 
            $CommunityImage->editor_pick_date = null;
       
        if (isset($dtoCommunityImage->online)) {
            $CommunityImage->online = $dtoCommunityImage->online;
        }
        if (isset($dtoCommunityImage->link)) {
            $CommunityImage->link = $dtoCommunityImage->link;
        }

        /*if ($dtoCommunityImage->imgBase64 != '') {
            Image::make($dtoCommunityImage->imgBase64)->save(static::$dirImageUser . $dtoCommunityImage->imgName);
            $CommunityImage->img = static::getUrlImageUsers() . $dtoCommunityImage->imgName;
            $CommunityImage->img_original = static::getUrlImageUsers() . $dtoCommunityImage->imgName;
            $CommunityImage->img_detail = static::getUrlImageUsers() . $dtoCommunityImage->imgName;
        }*/
           
           
        if ($dtoCommunityImage->imgBase64 != '') {
            if($dtoCommunityImage->title != '') {
                $imgNameByTitle = $dtoCommunityImage->imgName . '-' . static::_cleanStr($dtoCommunityImage->titletitle) . '.webp';
                $imgNameByTitleShare = $dtoCommunityImage->imgName . '-' . static::_cleanStr($dtoCommunityImage->titletitle) . '.jpg';
            }else{
                $imgNameByTitle = $dtoCommunityImage->imgName; 
                $imgNameByTitleShare = $dtoCommunityImage->imgName; 
            }

            $data = explode( ',', $dtoCommunityImage->imgBase64);
            Storage::disk('public')->put($dtoCommunityImage->userId . '/imagesUser/original/'. $imgNameByTitleShare, base64_decode($data[1]));  

            $resizedImage = Image::make($dtoCommunityImage->imgBase64)->encode('jpg', 100)->resize(null, 300, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::disk('public')->put($dtoCommunityImage->userId . '/imagesUser/preview/'. $imgNameByTitle, $resizedImage->stream()->__toString(), 'public'); 

            $resizedImagedetail = Image::make($dtoCommunityImage->imgBase64)->encode('jpg', 100)->resize(null, 1920, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::disk('public')->put($dtoCommunityImage->userId . '/imagesUser/main/'. $imgNameByTitle, $resizedImagedetail->stream()->__toString(), 'public'); 
        
            $CommunityImage->img = static::getUrlImageUsers() . 'preview/' . $imgNameByTitle;
            $CommunityImage->img_original = static::getUrlImageUsers() . 'original/' . $imgNameByTitle;
            $CommunityImage->img_detail = static::getUrlImageUsers() . 'main/' . $imgNameByTitle;
            $CommunityImage->image_share = static::getUrlImageUsers() . 'main/' . $imgNameByTitle;
    }
            $CommunityImage->save();        
          DB::commit();
      } catch (Exception $ex) {
          DB::rollback();
          throw $ex;
      }
  }


  public static function insertToWebP(Request $dtoCommunityImage)
  {
    LogHelper::debug($dtoCommunityImage);
    $CommunityImage = CommunityImage::where('id', $dtoCommunityImage->id)->first();

    DB::beginTransaction();
    try {
     
        $url =  static::$dirImageUser . 'preview/'.  $dtoCommunityImage->imgName;
        $ext = pathinfo($url, PATHINFO_EXTENSION);
        $image = file_get_contents($url);
        $imageBase64 = 'data:image/' . $ext . ';base64,' . base64_encode($image);
    
        $urlMain =  static::$dirImageUser . 'main/'.  $dtoCommunityImage->imgName;
        $extMain = pathinfo($urlMain, PATHINFO_EXTENSION);
        $imageMain = file_get_contents($urlMain);
        $imageBase64Main = 'data:image/' . $extMain . ';base64,' . base64_encode($imageMain);

           // preview 
            $resizedImage = Image::make($imageBase64)->encode('webp', 100)->resize(null, 300, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::disk('public')->put($dtoCommunityImage->userId . '/imagesUser/preview/'. str_replace('.' . $ext, '', $dtoCommunityImage->imgName) . '.webp', $resizedImage->stream()->__toString(), 'public'); 

              // main 
              $resizedImage = Image::make($imageBase64Main)->encode('webp', 100)->resize(null, 1920, function ($constraint) {
                $constraint->aspectRatio();
            });
            Storage::disk('public')->put($dtoCommunityImage->userId . '/imagesUser/main/'. str_replace('.' . $extMain, '', $dtoCommunityImage->imgName) . '.webp', $resizedImage->stream()->__toString(), 'public'); 

            $CommunityImage->img = static::getUrlImageUsers() . 'preview/' . str_replace('.' . $ext, '', $dtoCommunityImage->imgName) . '.webp';
            $CommunityImage->img_detail = static::getUrlImageUsers() . 'main/' . str_replace('.' . $extMain, '', $dtoCommunityImage->imgName) . '.webp';
            $CommunityImage->save();


        DB::commit();
    } catch (Exception $ex) {
        DB::rollback();
        throw $ex;
    }
}


    /**
     * getPreviousNextUrlByType
     * 
     * @param Request dtoCommunityImage
     */
    public static function getPreviousNextUrlByType(Request $request)
    {
        $content = '';
        $strWhere = '';
        $strOrderBy = '';
        $strLimit = '';
        $referencesName = '';
        $rateImage = "0,0";
        $userLink = "";
        $limit = "";
        $dtoCommunityImage = new DtoCommunityImage();

        if ($request->idUser != "") {
            if (strtolower($request->type)== "favorite")
                $strWhere = $strWhere . ' AND community_favorite_images.user_id = ' . $request->idUser;
            else
                $strWhere = $strWhere . ' AND Community_Images.user_id = ' . $request->idUser;
        }


        if ($request->idCategoryImage != "") {
            $strWhere = $strWhere . ' AND Community_Images.image_category_id = ' . $request->idCategoryImage;
        }
        if ($request->idImageToExclude != "") {
            $strWhere = $strWhere . ' AND Community_Images.id <> ' . $request->idImageToExclude;
        }
        if ($request->idCamera != "") {
            $strWhere = $strWhere . ' AND Community_Images."EXIF_camera_id" = ' . $request->idCamera;
        }
        if ($request->idLens != "") {
            $strWhere = $strWhere . ' AND Community_Images."EXIF_lens_id" = ' . $request->idLens;
        }

        if ($request->limit != "" && $request->limit != "0") 
            $strLimit = $strLimit . ' LIMIT ' . $limit;

        switch (strtolower($request->type)) {    
            /************************************************************/
            // GALLERIA CON LE FOTO PIU' POPOLARI DELLE ULTIME 24 ORE
            /************************************************************/
            case 'popular':
                $strOrderBy = "order by CASE WHEN upload_datetime >  now() - INTERVAL '24 hour' THEN Rating + 10000 ELSE  Rating end desc, upload_datetime DESC";

                $Images = DB::select(' SELECT link, img, star_rating, user_id, lag(link) OVER (ORDER BY CASE WHEN upload_datetime >  now() - INTERVAL \'24 hour\' THEN Rating + 10000 ELSE  Rating end desc, upload_datetime DESC) as prev_to,
                lead(link) OVER (ORDER BY CASE WHEN upload_datetime >  now() - INTERVAL \'24 hour\' THEN Rating + 10000 ELSE  Rating end desc, upload_datetime DESC) as next_from
                 FROM Community_Images WHERE online = \'True\'' . $strWhere . ' ' . $strOrderBy . ' ' . $strLimit);

                foreach ($Images as $key => $cv) 
                {
                    if($request->url == $Images[$key]->link){
                        $dtoCommunityImage->prev = $Images[$key]->prev_to;
                        $dtoCommunityImage->next = $Images[$key]->next_from;
                    }
                }
                break;

             /************************************************************/
            // GALLERIA CON LE FOTO NUOVE
            /************************************************************/               
            case 'fresh':
                $strOrderBy = "ORDER By Id DESC";
                $Images = DB::select('SELECT link, img, star_rating, user_id, lag(link) OVER (ORDER By Id DESC) as prev_to,
                lead(link) OVER (ORDER By Id DESC) as next_from FROM Community_Images
                            WHERE online = \'True\''. $strWhere . ' ' . $strOrderBy. ' ' . $strLimit);
                foreach ($Images as $key => $cv) 
                {
                    if($request->url == $Images[$key]->link){
                        $dtoCommunityImage->prev = $Images[$key]->prev_to;
                        $dtoCommunityImage->next = $Images[$key]->next_from;
                    }
                }               
                break;

             /************************************************************/
            // GALLERIA CON LE FOTO PREFERITE
            /************************************************************/               
            case 'favorite':
                $strOrderBy = "ORDER By community_favorite_images.Id DESC";

                $Images = DB::select('SELECT link, img, community_images.star_rating, community_images.user_id, lag(community_images.link) OVER (ORDER By community_favorite_images.Id DESC) as prev_to,
                lead(community_images.link) OVER (ORDER By community_favorite_images.Id DESC) as next_from FROM community_favorite_images
                            inner join community_images on community_favorite_images.image_id = community_images.id 
                            WHERE online = \'True\''. $strWhere . ' ' . $strOrderBy. ' ' . $strLimit);

                foreach ($Images as $key => $cv) 
                {
                    if($request->url == $Images[$key]->link){
                        $dtoCommunityImage->prev = $Images[$key]->prev_to;
                        $dtoCommunityImage->next = $Images[$key]->next_from;
                    }
                }               
                break;

             /************************************************************/
            // GALLERIA CON LE FOTO SCELTE DA NOI
            /************************************************************/               
            case 'editorpick':
                $strOrderBy = "ORDER By Editor_Pick_Date DESC";
                $Images = DB::select('SELECT link, img, star_rating, user_id, lag(link) OVER (ORDER By Editor_Pick_Date DESC) as prev_to,
                lead(link) OVER (ORDER By Editor_Pick_Date DESC) as next_from FROM Community_Images WHERE online = \'True\' AND editor_pick = \'True\''. $strWhere . ' ' . $strOrderBy. ' ' . $strLimit);
                
                foreach ($Images as $key => $cv)  
                {
                    if($request->url == $Images[$key]->link){
                        $dtoCommunityImage->prev = $Images[$key]->prev_to;
                        $dtoCommunityImage->next = $Images[$key]->next_from;
                    } 
                }               
                break;

            default:
                $strOrderBy = "ORDER By id DESC";
                $Images = DB::select('SELECT link, img, star_rating, user_id, lag(link) OVER (ORDER By id DESC) as prev_to,
                lead(link) OVER (ORDER By id DESC) as next_from FROM Community_Images WHERE online = \'True\' AND editor_pick = \'True\''. $strWhere . ' ' . $strOrderBy. ' ' . $strLimit);
                
                foreach ($Images as $key => $cv)
                {
                    if($request->url == $Images[$key]->link){
                        $dtoCommunityImage->prev = $Images[$key]->prev_to;
                        $dtoCommunityImage->next = $Images[$key]->next_from;
                    }
                }
        }

        return $dtoCommunityImage;
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param Request request
     */
     public static function deleteImage(request $request)
     {
        $CommunityImage = CommunityImage::find($request->idImage);
        $return = "";
        if (is_null($CommunityImage)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            $communityAction = CommunityAction::where('image_id', $request->idImage);
            if(isset($communityAction)){
                $communityAction->delete();
            }

            $CommunityFavoriteImage = CommunityFavoriteImage::where('image_id', $request->idImage);
            if(isset($CommunityFavoriteImage)){
                $CommunityFavoriteImage->delete();
            }

            $CommunityImageComment = CommunityImageComment::where('image_id', $request->idImage);
            if(isset($CommunityImageComment)){
                $CommunityImageComment->delete();
            }

            $CommunityImageReport = CommunityImageReport::where('image_id', $request->idImage);
            if(isset($CommunityImageReport)){
                $CommunityImageReport->delete();
            }

            $CommunityImage = CommunityImage::find($request->idImage);
            if(isset($CommunityFavoriteImage)){
                $CommunityImage->delete();
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $return;
     }

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id)
    {
       /*$communityAction = CommunityAction::where('image_id', $request->idImage)->get();
        if(isset($communityAction)){
            $communityAction->delete();
        }

        $CommunityFavoriteImage = CommunityFavoriteImage::where('image_id', $request->idImage)->get();
        if(isset($CommunityFavoriteImage)){
            $CommunityFavoriteImage->delete();
        }*/

        $CommunityImage = CommunityImage::find($id);
        if(isset($CommunityImage)){
            $CommunityImage->delete();
        }

    }

    #endregion DELETE
}

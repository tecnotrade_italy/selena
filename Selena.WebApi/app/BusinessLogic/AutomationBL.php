<?php

namespace App\BusinessLogic;

use Illuminate\Support\Facades\DB;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use App\Automation;
use Illuminate\Http\Request;

class AutomationBL
{
    #region GET

    /**
     * Get all
     *
     *
     * @return Automation
     */
    public static function getAll()
    {
        return Automation::all();
    }


    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser)
    {
        
        //static::_validateData($DtoStates, $DtoStates->idLanguage, DbOperationsTypesEnum::INSERT);
        DB::beginTransaction();

        try {
            
            $automation = new Automation();
            
            $automation->description =$request->description;
            $automation->from = $request->from;
            $automation->to = $request->to;
            $automation->online = $request->online;
            //$States->created_id = $DtoStates->$idUser; 
           
            $automation->save();

        
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $automation->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request request
     * @param int idLanguage
     */
   

    public static function update(Request $request){
        //static::_validateData($DtoStates, $DtoStates->idLanguage, DbOperationsTypesEnum::UPDATE);
        $automationOnDb = Automation::find($request->id);

        DB::beginTransaction();

        try {

            $automationOnDb->description = $request->description;
            $automationOnDb->from = $request->from;
            $automationOnDb->to = $request->to;
            $automationOnDb->online = $request->online;
            $automationOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
       // return $automationOnDb->id;
    }
    
    #endregion UPDATE
}

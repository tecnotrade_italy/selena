<?php

namespace App\BusinessLogic;

use App\BookingConnectionAppointmentsCartDetail;
use App\DtoModel\DtoCartDetail;
use App\DtoModel\DtoCartReturn;

use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\CartDetail;
use App\Cart;
use App\CartDetailOptional;
use App\DtoModel\DtoCartDetailOptional;
use App\Item;
use App\VatType;
use App\UsersAddresses;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\LogHelper;
use App\Enums\SettingEnum;

class CartDetailBL
{
    #region PRIVATE

    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request dtoCartDetail
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $dtoCartDetail, int $idLanguage, $dbOperationType)
    {
        
    }

    /**
     * Convert to dto
     * 
     * @param CartDetail cartDetail
     * @return DtoCartDetail
     */
    private static function _convertToDto($cartDetail)
    {
        $dtoCartDetail = new DtoCartDetail();

        if (isset($cartDetail->id)) {
            $dtoCartDetail->id = $cartDetail->id;
        }

        if (isset($cartDetail->cart_id)) {
            $dtoCartDetail->cart_id = $cartDetail->cart_id;
        }

        if (isset($cartDetail->nr_row)) {
            $dtoCartDetail->nr_row = $cartDetail->nr_row;
        }

        if (isset($cartDetail->item_id)) {
            $dtoCartDetail->item_id = $cartDetail->item_id;
        }

        if (isset($cartDetail->description)) {
            $dtoCartDetail->description = $cartDetail->description;
        }

        if (isset($cartDetail->unit_of_measure_id)) {
            $dtoCartDetail->unit_of_measure_id = $cartDetail->unit_of_measure_id;
        }

        if (isset($cartDetail->unit_price)) {
            $dtoCartDetail->unit_price = $cartDetail->unit_price;
        }        

        if (isset($cartDetail->quantity)) {
            $dtoCartDetail->quantity = $cartDetail->quantity;
        }

        if (isset($cartDetail->discount_percentage)) {
            $dtoCartDetail->discount_percentage = $cartDetail->discount_percentage;
        }

        if (isset($cartDetail->discount_value)) {
            $dtoCartDetail->discount_value = $cartDetail->discount_value;
        }

        if (isset($cartDetail->vat_type_id)) {
            $dtoCartDetail->vat_type_id = $cartDetail->vat_type_id;
        }

        if (isset($cartDetail->total_amount)) {
            $dtoCartDetail->total_amount = $cartDetail->total_amount;
        }

        if (isset($cartDetail->note)) {
            $dtoCartDetail->note = $cartDetail->note;
        }
 
        if (isset($cartDetail->net_amount)) {
            $dtoCartDetail->net_amount = $cartDetail->net_amount;
        }

        if (isset($cartDetail->vat_amount)) {
            $dtoCartDetail->vat_amount = $cartDetail->vat_amount;
        }
         if (isset($cartDetail->width)) {
            $dtoCartDetail->width = $cartDetail->width;
        }
         if (isset($cartDetail->depth)) {
            $dtoCartDetail->depth = $cartDetail->depth;
        }
         if (isset($cartDetail->height)) {
            $dtoCartDetail->height = $cartDetail->height;
        }
        if (isset($cartDetail->support_id)) {
            $dtoCartDetail->support_id = $cartDetail->support_id;
        }

        return $dtoCartDetail;
    }

    /**
     * Convert to VatType
     * 
     * @param DtoCartDetail dtoCartDetail
     * 
     * @return CartDetail
     */
    private static function _convertToModel($dtoCartDetail)
    {
        $cartDetail = new CartDetail();

        if (isset($dtoCartDetail->id)) {
            $cartDetail->id = $dtoCartDetail->id;
        }

        if (isset($dtoCartDetail->cart_id)) {
            $cartDetail->cart_id = $dtoCartDetail->cart_id;
        }

        if (isset($dtoCartDetail->nr_row)) {
            $cartDetail->nr_row = $dtoCartDetail->nr_row;
        }

        if (isset($dtoCartDetail->item_id)) {
            $cartDetail->item_id = $dtoCartDetail->item_id;
        }

        if (isset($dtoCartDetail->description)) {
            $cartDetail->description = $dtoCartDetail->description;
        }       
        
        if (isset($dtoCartDetail->unit_of_measure_id)) {
            $cartDetail->unit_of_measure_id = $dtoCartDetail->unit_of_measure_id;
        }

        if (isset($dtoCartDetail->unit_price)) {
            $cartDetail->unit_price = $dtoCartDetail->unit_price;
        }

        if (isset($dtoCartDetail->quantity)) {
            $cartDetail->quantity = $dtoCartDetail->quantity;
        }

        if (isset($dtoCartDetail->discount_percentage)) {
            $cartDetail->discount_percentage = $dtoCartDetail->discount_percentage;
        }

        if (isset($dtoCartDetail->discount_value)) {
            $cartDetail->discount_value = $dtoCartDetail->discount_value;
        }

        if (isset($dtoCartDetail->total_amount)) {
            $cartDetail->total_amount = $dtoCartDetail->total_amount;
        }

        if (isset($dtoCartDetail->vat_type_id)) {
            $cartDetail->vat_type_id = $dtoCartDetail->vat_type_id;
        }

        if (isset($dtoCartDetail->note)) {
            $cartDetail->note = $dtoCartDetail->note;
        }         

        if (isset($dtoCartDetail->net_amount)) {
            $cartDetail->net_amount = $dtoCartDetail->net_amount;
        }     

        if (isset($dtoCartDetail->vat_amount)) {
            $cartDetail->vat_amount = $dtoCartDetail->vat_amount;
        }     
        return $cartDetail;
    }

    /**
     * Convert to VatType
     * 
     * @param string discount
     * @param string price
     * 
     * @return discountCalculated
     */
    private static function _calculateDiscountValue($discount, $price){
        $discount_value = 0;
        $newPrice = $price;

        $pos = strpos($discount, '+');
        if ($pos !== false) {
            $everyDiscountValue = explode("+", $discount);
            foreach($everyDiscountValue as $everyDiscountValues) {
                $discount_value_new = (($newPrice  / 100) * $everyDiscountValues);
                $discount_value = $discount_value + $discount_value_new;
                $newPrice = $newPrice - $discount_value_new;
            }
        }else{
            $discount_value = ($price / 100) * $discount;
        }

        return $discount_value;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCartDetail
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(CartDetail::find($id));
    }


    public static function getAllByCart(string $idCart)
    {
        $result = collect();
        foreach (DB::table('carts_details')->where('cart_id', $idCart)->orderBy('nr_row')->get() as $cartRow) {
            $result->push(static::_convertToDto($cartRow));
        }
        
        return $result;
    }  


    public static function getMaxRow(int $cart_id)
    {
        $nr_row = DB::table('carts_details')->where('cart_id', $cart_id)->max('nr_row');

        if (is_null($nr_row)) {
            return 10;
        } else {
            return $nr_row+10;
        }
    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request dtoCartDetail
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertRequest(Request $request)
    {
        DB::beginTransaction();

        try {

            $dtoReturn = new DtoCartReturn();
            $cart_id = $request->cart_id;
            $PriceIncludingVat = SettingBL::getByCode(SettingEnum::PriceIncludingVat);
            
            //se il carrello esiste già procedo con l'insert della sola riga, altrimenti devo prima inserire anche la testata
            if (is_null($cart_id) || $cart_id == '' || $cart_id == 'null'){
                if(is_null($request->user_id) || $request->user_id == '' || $request->user_id == 'null'){
                    $cartByUserAnonymus = Cart::where('session_token', $request->session_token)->get()->first();
                    if(isset($cartByUserAnonymus) && $cartByUserAnonymus!= ''){
                        $cart_id = $cartByUserAnonymus->id;
                    }else{
                        $cart = new Cart();
                        $cart->user_id = $request->user_id;
                        $cart->session_token = $request->session_token;
                        //$cart->currency_id = $request->currency_id;
                        $cart->save();
                        $cart_id = $cart->id;
                    }
                }else{
                    $cartByUser = Cart::where('user_id', $request->user_id)->get()->first();

                    if(isset($cartByUser) && $cartByUser!= ''){
                        $cart_id = $cartByUser->id;
                        if(is_null($cartByUser->user_address_goods_id)){
                            $userAddressDefault = UsersAddresses::where('user_id', $request->user_id)->where('default_address', true)->get()->first();
                            if(isset($userAddressDefault)){
                                $cartByUser->user_address_goods_id = $userAddressDefault->id;
                                $cartByUser->user_address_documents_id = $userAddressDefault->id;
                                $cartByUser->save();
                            }
                        }
                    }else{
                        $cart = new Cart();
                        $cart->user_id = $request->user_id;
                        $cart->session_token = $request->session_token;
                        
                        //$cart->currency_id = $request->currency_id;

                        //associazione sede di default se è presente per il cliente
                        $userAddressDefault = UsersAddresses::where('user_id', $request->user_id)->where('default_address', true)->get()->first();
                        if(isset($userAddressDefault)){
                            $cart->user_address_goods_id = $userAddressDefault->id;
                            $cart->user_address_documents_id = $userAddressDefault->id;
                        }

                        $cart->save();
                        $cart_id = $cart->id;
                    }
                }
            }

            //mi memorizzo l'articolo e carico le sue proprietà
            $item = Item::find($request->item_id);

            //inserisco la nuova riga

            $cartDetailOnDb = CartDetail::where('cart_id', $cart_id)->where('item_id', $item->id)->get()->first();
            /*if(isset($cartDetailOnDb)){
                
                //se l'articolo ha un iva, me la memorizzo e poi controllo se devo scorporarla al prezzo unitario o aggiungerla
                if (isset($item->vat_type_id)) {
                    $cartDetailOnDb->vat_type_id = $item->vat_type_id;
                    
                    $vatTypeRate = VatType::where('id', $item->vat_type_id)->first()->rate;

                    if ($vatTypeRate != null && $vatTypeRate>0){
                        //verifico se devo scorporare l'iva o aggiungerla
                        if ($PriceIncludingVat=='True'){
                            $baseImponibile = (100*$item->price*($cartDetailOnDb->quantity + $request->quantity))/(100+$vatTypeRate);

                            $cartDetailOnDb->net_amount = $baseImponibile;
                            $cartDetailOnDb->vat_amount = ($item->price*($cartDetailOnDb->quantity + $request->quantity)) - $baseImponibile;
                            $cartDetailOnDb->total_amount = $item->price*($cartDetailOnDb->quantity + $request->quantity);
                        }else{
                            $cartDetailOnDb->net_amount = $item->price*($cartDetailOnDb->quantity + $request->quantity);
                            $cartDetailOnDb->vat_amount = ($cartDetailOnDb->net_amount/100)*$vatTypeRate;
                            $cartDetailOnDb->total_amount = $cartDetailOnDb->net_amount + $cartDetailOnDb->vat_amount;
                        }
                    }
                    
                }else{
                    $cartDetailOnDb->vat_amount = 0;
                    $cartDetailOnDb->net_amount = $item->price*($cartDetailOnDb->quantity + $request->quantity);
                    $cartDetailOnDb->total_amount = $item->price*($cartDetailOnDb->quantity + $request->quantity);
                }

                $cartDetailOnDb->quantity = $cartDetailOnDb->quantity + $request->quantity;
                $cartDetailOnDb->save();
                $returnId = $cartDetailOnDb->id;
                $dtoReturn->cart_detail_id = $cartDetailOnDb->id;
            }else{*/
                $newCartDetail = new CartDetail();
                $newCartDetail->cart_id = $cart_id;
                $newCartDetail->nr_row = static::getMaxRow($cart_id);            
                $newCartDetail->item_id = $item->id;
                $newCartDetail->description = ItemLanguageBL::getByItemAndLanguage($item->id, 1);
                $newCartDetail->unit_price = $item->price;
                $newCartDetail->quantity = $request->quantity;

                if(!is_null($request->user_id)){
                    LogsBL::insert($request->user_id, '', 'ADDTOCART', 'Aggiunto/i ' . $request->quantity . 'pz dell\' articolo ' . ItemLanguageBL::getByItemAndLanguage($item->id, 1) . ' al carrello');
                }

                //se l'articolo ha un iva, me la memorizzo e poi controllo se devo scorporarla al prezzo unitario o aggiungerla
                if (isset($item->vat_type_id)) {
                    $newCartDetail->vat_type_id = $item->vat_type_id;
                    
                    $vatTypeRate = VatType::where('id', $item->vat_type_id)->first()->rate;

                    if ($vatTypeRate != null && $vatTypeRate>0){
                        //verifico se devo scorporare l'iva o aggiungerla
                        if ($PriceIncludingVat=='True'){
                            $baseImponibile = (100*$item->price*$request->quantity)/(100+$vatTypeRate);

                            $newCartDetail->net_amount = $baseImponibile;
                            $newCartDetail->vat_amount = ($item->price*$request->quantity) - $baseImponibile;
                            $newCartDetail->total_amount = $item->price*$request->quantity;
                        }else{
                            $newCartDetail->net_amount = $item->price*$request->quantity;
                            $newCartDetail->vat_amount = ($newCartDetail->net_amount/100)*$vatTypeRate;
                            $newCartDetail->total_amount = $newCartDetail->net_amount + $newCartDetail->vat_amount;
                        }
                    }
                }else{
                    $newCartDetail->vat_amount = 0;
                    $newCartDetail->net_amount = $item->price*$request->quantity;
                    $newCartDetail->total_amount = $item->price*$request->quantity;
                }
            
                $newCartDetail->save();
                $returnId = $newCartDetail->id;
                $dtoReturn->cart_detail_id = $newCartDetail->id;
            //}

            //aggiornamento e controllo regole carrello su righe per articoli specifici
            CartBL::applyRulesFromItemsPresentInActionTab($returnId, $request->user_id);

            //aggiorno il totale del carrello
            CartBL::applyRulesAndRefreshTotalAmount($cart_id);
  
            $dtoReturn->cart_id = $cart_id;

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $dtoReturn;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoCartDetail
     * @param int idLanguage
     */
    public static function update(Request $dtoCartDetail, int $idLanguage)
    {
        static::_validateData($dtoCartDetail, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $cartDetail = CartDetail::find($dtoCartDetail->id);
        DBHelper::updateAndSave(static::_convertToModel($dtoCartDetail), $cartDetail);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        
        $cartDetail = CartDetail::where('id', $id)->first();

        
        
        if (is_null($cartDetail)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        if (!is_null($cartDetail)) {
            DB::beginTransaction();

            try {

                //mi memorizzo per scrupolo l'id del carrello
                $idCart = $cartDetail->cart_id;
                
                $CartDetailOptional = CartDetailOptional::where('cart_detail_id', $cartDetail->id)->get();
                $BookingConnectionAppointmentsCartDetail = BookingConnectionAppointmentsCartDetail::where('cart_id', $idCart)->get();

                if(isset($CartDetailOptional)){                                   
                        CartDetailOptional::where('cart_detail_id', $cartDetail->id)->delete();             
                } 
                
                if(isset($BookingConnectionAppointmentsCartDetail)){                                   
                        BookingConnectionAppointmentsCartDetail::where('cart_id', $idCart)->delete();             
                } 
                //cancello la riga del carrello
                $cartDetail->delete();

                 
                //aggiorno il totale del carrello
                CartBL::applyRulesAndRefreshTotalAmount($idCart);

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }  
        }
    }

    #endregion DELETE

       #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function modifyQuantity(Request $request, int $idLanguage){
        
        $newQuantity = $request->newQuantity;
        $id = $request->id;
        $cartDetail = CartDetail::where('id', $id)->first();
        if (is_null($cartDetail)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        DB::beginTransaction();

        try {
            
            //mi memorizzo per scrupolo l'id del carrello
            $idCart = $cartDetail->cart_id;
            $PriceIncludingVat = SettingBL::getByCode(SettingEnum::PriceIncludingVat);

            //mi memorizzo l'articolo e carico le sue proprietà
            $item = Item::find($cartDetail->item_id);

            //modifico la quantità e il totale riga (unit price *quantità)
            $cartDetail->quantity = $newQuantity;

            //sconto percentuale
            if (!is_null($cartDetail->discount_percentage) && $cartDetail->discount_percentage > 0){
                //$discount_value = ($item->price / 100) * $cartDetail->discount_percentage;
                $discount_value = static::_calculateDiscountValue($cartDetail->discount_percentage, $item->price);
                $cartDetail->discount_percentage = $cartDetail->discount_percentage;
                $newPrice = $item->price - $discount_value;
                $cartDetail->unit_price= $newPrice;
            //Sconto in valore
            }else if (!is_null($cartDetail->discount_value) && $cartDetail->discount_value > 0){
                $cartDetail->discount_value = $cartDetail->discount_value;
                $newPrice = $item->price - $cartDetail->discount_value;
                $cartDetail->unit_price= $newPrice;
            }else{
                $newPrice = $item->price;
                $cartDetail->unit_price = $newPrice;
            }

            //se l'articolo ha un iva, me la memorizzo e poi controllo se devo scorporarla al prezzo unitario o aggiungerla
            if (isset($item->vat_type_id)) {
                $cartDetail->vat_type_id = $item->vat_type_id;
                
                $vatTypeRate = VatType::where('id', $item->vat_type_id)->first()->rate;

                if ($vatTypeRate != null && $vatTypeRate>0){
                    //verifico se devo scorporare l'iva o aggiungerla
                    if ($PriceIncludingVat=='True'){
                        $baseImponibile = (100*$newPrice*$newQuantity)/(100+$vatTypeRate);

                        $cartDetail->net_amount = $baseImponibile;
                        $cartDetail->vat_amount = ($newPrice*$newQuantity) - $baseImponibile;
                        $cartDetail->total_amount = $newPrice*$newQuantity;
                    }else{
                        $cartDetail->net_amount = $newPrice*$newQuantity;
                        $cartDetail->vat_amount = ($cartDetail->net_amount/100)*$vatTypeRate;
                        $cartDetail->total_amount = $cartDetail->net_amount + $cartDetail->vat_amount;
                    }
                }
            }else{
                $cartDetail->vat_amount = 0;
                $cartDetail->net_amount = $newPrice*$newQuantity;
                $cartDetail->total_amount = $newPrice*$newQuantity;
            }

            $cartDetail->save();

            //aggiorno il totale del carrello
            CartBL::applyRulesAndRefreshTotalAmount($idCart);

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $cartDetail->id;
    }
}

<?php

namespace App\BusinessLogic;

use App\GroupTechnicalSpecification;
use App\DtoModel\DtoGroupTechnicalSpecification;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\GroupTechnicalSpecificationTechnicalSpecification;
use App\Helpers\LogHelper;
use App\TechincalSpecification;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use Symfony\Component\HttpFoundation\Request;
use App\Helpers\ArrayHelper;

class GroupTechnicalSpecificationBL
{
    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request $dtoGroupTechnicalSpecification
     * @param int $idLanguage
     * @param DbOperationsTypesEnum $dbOperationType
     */
    private static function _validateData(Request $dtoGroupTechnicalSpecification, int $idLanguage, $dbOperationType)
    {
        if (is_null($dtoGroupTechnicalSpecification->idFunctionality)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Functionality::find($dtoGroupTechnicalSpecification->idFunctionality))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (count($dtoGroupTechnicalSpecification->groupTechnicalSpecificationLanguage) == 0) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupTechnicalSpecificationNotEmpty), HttpResultsCodesEnum::InvalidPayload);
        }

        $defaultLanguageFound = false;

        foreach ($dtoGroupTechnicalSpecification->groupTechnicalSpecificationLanguage as $dtoGroupTechnicalSpecificationLanguage) {
            if ($dtoGroupTechnicalSpecificationLanguage['idLanguage'] == LanguageBL::getDefault()->id) {
                $defaultLanguageFound = true;
                if (is_null($dtoGroupTechnicalSpecificationLanguage['description'])) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DefaultLanguageRequired), HttpResultsCodesEnum::InvalidPayload);
                }
            }
        }

        if (!$defaultLanguageFound) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DefaultLanguageRequired), HttpResultsCodesEnum::InvalidPayload);
        }

        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($dtoGroupTechnicalSpecification->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            } else {
                if (is_null(GroupTechnicalSpecification::find($dtoGroupTechnicalSpecification->id))) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupTechnicalSpecificationNotFound), HttpResultsCodesEnum::InvalidPayload);
                }
            }
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get DtoGroupTechnicalSpecification
     *
     * @return DtoGroupTechnicalSpecification
     */
    public static function getDtoTable()
    {
        $result = collect();

        foreach (GroupTechnicalSpecification::all() as $technicalSpecification) {
            $dtoGroupTechnicalSpecification = new DtoGroupTechnicalSpecification();
            $dtoGroupTechnicalSpecification = GroupTechnicalSpecificationLanguageBL::getDtoTable($technicalSpecification->id);
            $dtoGroupTechnicalSpecification['id'] = $technicalSpecification->id;
            $result->push($dtoGroupTechnicalSpecification);
        }

        return $result;
    }



    /**
     * Get getGraphic
     *
     * @return getGraphic
     */
    public static function getGraphic()
    {
        $rows = DB::select("select count(id) as number from groups_technicals_specifications GROUP BY DATE(created_at)");
        $array1 = array();
        foreach (ArrayHelper::toCollection($rows) as $row) {
          array_push($array1,$row->number);
        }

        return $array1;
    }

    /**
     * Get all items included and excluded
     *
     * @param int $id
     * @param int $idLanguage
     *
     * @return DtoIncludedExcluded
     */
    public static function getItems(int $id, int $idLanguage)
    {
        $dtoIncludedExcluded = new DtoIncludedExcluded();
        $dtoIncludedExcluded->included = ItemGroupTechnicalSpecificationBL::getInlucedInGroup($id, $idLanguage);
        $dtoIncludedExcluded->excluded = ItemGroupTechnicalSpecificationBL::getExlucedInGroup($id, $idLanguage);
        return $dtoIncludedExcluded;
    }

    /**
     * Get all technical specification included and excluded
     *
     * @param int $id
     * @param int $idLanguage
     *
     * @return DtoIncludedExcluded
     */
    public static function getTechnicalSpecification(int $id, int $idLanguage)
    {
        $dtoIncludedExcluded = new DtoIncludedExcluded();
        $dtoIncludedExcluded->included = GroupTechnicalSpecificationTechnicalSpecificationBL::getInlucedInGroup($id, $idLanguage);
        $dtoIncludedExcluded->excluded = GroupTechnicalSpecificationTechnicalSpecificationBL::getExlucedInGroup($id, $idLanguage);
        return $dtoIncludedExcluded;
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @param Request $dtoGroupTechnicalSpecification
     * @param int $idUser
     * @param int $idLanguage
     *
     * @return int id
     */
    public static function insert(Request $dtoGroupTechnicalSpecification, int $idUser, int $idLanguage)
    {
        if (!FieldUserRoleBL::checkFunctionalityEnabled($dtoGroupTechnicalSpecification->idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        static::_validateData($dtoGroupTechnicalSpecification, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $groupTechnicalSpecification = new GroupTechnicalSpecification();
            $groupTechnicalSpecification->save();

            foreach ($dtoGroupTechnicalSpecification->groupTechnicalSpecificationLanguage as $dtoGroupTechnicalSpecificationLanguage) {
                GroupTechnicalSpecificationLanguageBL::insertRequest($groupTechnicalSpecification->id, $dtoGroupTechnicalSpecificationLanguage, $idLanguage);
            }

            if (!is_null($dtoGroupTechnicalSpecification->duplicate)) {
                GroupTechnicalSpecificationTechnicalSpecificationBL::clone($dtoGroupTechnicalSpecification->duplicate, $groupTechnicalSpecification->id);
            }

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $groupTechnicalSpecification->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request $dtoGroupTechnicalSpecification
     * @param int $idUser
     * @param int $idLanguage
     */
    public static function update(Request $dtoGroupTechnicalSpecification, int $idUser, int $idLanguage)
    {
        if (!FieldUserRoleBL::checkFunctionalityEnabled($dtoGroupTechnicalSpecification->idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        static::_validateData($dtoGroupTechnicalSpecification, $idLanguage, DbOperationsTypesEnum::UPDATE);

        DB::beginTransaction();

        try {
            foreach ($dtoGroupTechnicalSpecification->groupTechnicalSpecificationLanguage as $dtoGroupTechnicalSpecificationLanguage) {
                GroupTechnicalSpecificationLanguageBL::updateRequest($dtoGroupTechnicalSpecification->id, $dtoGroupTechnicalSpecificationLanguage, $idLanguage);
            }

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @param int $idFunctionality
     * @param int $idUser
     * @param int $idLanguage
     */
    public static function delete(int $id, int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(GroupTechnicalSpecification::find($id))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupTechincalSpecificationNotFound), HttpResponseException::InvalidPayload);
        }

        DB::beginTransaction();

        try {
            GroupTechnicalSpecificationTechnicalSpecificationBL::deleteByGroupTechnicalSpecification($id);
            GroupTechnicalSpecificationLanguageBL::deleteByGroupTechnicalSpecification($id);
            GroupTechnicalSpecification::destroy($id);

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion DELETE
}

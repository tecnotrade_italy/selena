<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoItemAttachmentLanguageFileTable;
use App\DtoModel\DtoItemAttachmentFileTable;
use App\DtoModel\DtoItemAttachmentFileCategoryTable;
use App\DtoModel\DtoItemAttachmentFileItemTable;
use App\DtoModel\DtoItemAttachmentUserFileTable;
use App\DtoModel\DtoImage;
use App\DtoModel\DtoItemFilesUrl;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\ItemAttachmentLanguageFileTable;
use App\ItemAttachmentFile;
use App\itemAttachmentUserFileTable;
use App\ItemAttachmentLanguageFileItemTable;
use App\ItemAttachmentLanguageFileCategoryTable;
use Illuminate\Support\Facades\Storage;
use App\ItemLanguage;
use App\Item;
use App\User;
use App\Category;

use App\ItemAttachmentLanguageTypeTable;
use App\CategoryLanguage;
use App\Content;
use App\ContentLanguage;
use App\CustomerGroupNewsletter;
use App\DtoModel\DtoItemAttachmentGroupNewsletterFileTable;

use App\GroupNewsletter;
use App\itemAttachmentGroupNewsletterFileTable;
use App\Language;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ItemAttachmentFileControllerBL
{

    public static $dirImagePdfModule = '../storage/app/public/files/';

    public static $dirImageAttachment = '../storage/app/public/files/';
    public static $dirImageAttachmentImages = '../storage/app/public/files';
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (ItemAttachmentType::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    private static function getUrlImage()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/api/storage/files/';
    }

    /**
     * Convert image size
     * 
     * @param int byte
     *  
     */
    private static function _convertImageSize(int $bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

     

    /**
     * Return dir
     *
     * @param string dir
     * @param string year
     * @param string month
     * @return resultImage
     */
    private static function _getFileDir($dir, $year, $month)
    {
        $result = collect();
        if (file_exists($dir)) {
            $cdir = scandir($dir, 1);
            foreach ($cdir as $key => $value){
                if (!in_array($value, array(".",".."))){
                    if (is_dir($dir . DIRECTORY_SEPARATOR . $value)){
                        if($value != 'TemplateEditor' && $value != 'Previews' && $value != 'Items' && $value != 'pdf' && $value != 'Categories' && $value != 'AdvertisingBanner' && $value != 'resize'){
                            
                            $arrayMerge = static::_getFileDir($dir . DIRECTORY_SEPARATOR . $value, $year, $month);
                            if(isset($arrayMerge)){
                                if(count($arrayMerge->toArray()) > 0){
                                    foreach ($arrayMerge as $arrayMerges) {
                                        /* info img */
                                        $listImages = new DtoImage();
                                        $listImages->name = $value . DIRECTORY_SEPARATOR . $arrayMerges->name;
                                        $listImages->nameWithOutPath = $arrayMerges->name;

                                        if($year != '' && $month == ''){
                                            $listImages->url = '/api/storage/files/' . $year . '/' . $value . DIRECTORY_SEPARATOR . $arrayMerges->name;
                                        }else{
                                            $listImages->url = '/api/storage/files/' . $value . DIRECTORY_SEPARATOR . $arrayMerges->name;
                                        }

                                        if($year != ''){
                                            $data = static::$dirImageAttachmentImages . '/' . $year . '/' . $value . DIRECTORY_SEPARATOR . $arrayMerges->name;
                                        }else{
                                            $data = static::$dirImageAttachmentImages . '/' . $value . DIRECTORY_SEPARATOR . $arrayMerges->name;
                                        }
                                        
                                        $extension = substr($data, -3, 3); 
                                        if ($extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'peg' || $extension == 'ebp'){
                                            if (file_exists($data)) {
                                                $date = filemtime($data);
                                                $listImages->date = date('d/m/Y', $date);
                                                if(filesize($data) > 0){
                                                    $dimension = getimagesize($data);
                                                    if($dimension){
                                                        $listImages->width = $dimension[0];
                                                        $listImages->height = $dimension[1];
                                                    }else{
                                                        $listImages->width = 0;
                                                        $listImages->height = 0;
                                                    }
                                                    $listImages->weight = static::_convertImageSize(filesize($data));
                                                    $listImages->ext = pathinfo($data, PATHINFO_EXTENSION);
                                                    $filenamewithextension = basename($data);
                                                    $listImages->nameFile = basename($filenamewithextension);
                                                }else{
                                                    $listImages->width = 0;
                                                    $listImages->height = 0;
                                                    $listImages->weight = 0;
                                                    $listImages->ext = '';
                                                    $listImages->nameFile = '';
                                                }
                                            }else{
                                                $listImages->date = '';
                                                $listImages->weight = 0;
                                                $listImages->width = 0;
                                                $listImages->height = 0;
                                                $listImages->ext = '';
                                                $listImages->nameFile = '';
                                            }
                                        }else{
                                            $listImages->date = '';
                                            $listImages->weight = 0;
                                            $listImages->width = 0;
                                            $listImages->height = 0;
                                            $listImages->ext = '';
                                            $listImages->nameFile = '';
                                        }
                                        $result->push($listImages);
                                    }
                                }
                            }
                        }
                    }else{
                        /* info img */
                        $listImages = new DtoImage();
                        $listImages->name = $value;
                        $listImages->nameWithOutPath = $value;
                        $dirNew = str_replace('../storage/app/public/files', '', $dir);
                        if($year != '' && $month == ''){
                            $listImages->url = '/api/storage/files/' . $year . '/' . $dirNew . '/' . $value;
                        }else{
                            $listImages->url = '/api/storage/files/' . $dirNew . '/' . $value;
                        }
                        $data = $dir . '/' . $value;
                        $extension = substr($data, -3, 3); 
                        if ($extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'peg' || $extension == 'ebp' || $extension == 'pdf' || $extension == 'zip' || $extension == 'xls' || $extension == 'doc' || $extension == 'lsx'){
                            if (file_exists($data)) {
                                $date = filemtime($data);
                                $listImages->date = date('d/m/Y', $date);
                                if(filesize($data) > 0){
                                    $dimension = getimagesize($data);
                                    if($dimension){
                                        $listImages->width = $dimension[0];
                                        $listImages->height = $dimension[1];
                                    }else{
                                        $listImages->width = 0;
                                        $listImages->height = 0;
                                    }
                                    $listImages->weight = static::_convertImageSize(filesize($data));
                                    $listImages->ext = pathinfo($data, PATHINFO_EXTENSION);
                                    $filenamewithextension = basename($data);
                                    $listImages->nameFile = basename($filenamewithextension);
                                }else{
                                    $listImages->width = 0;
                                    $listImages->height = 0;
                                    $listImages->weight = 0;
                                    $listImages->ext = '';
                                    $listImages->nameFile = '';
                                }
                            }else{
                                $listImages->date = '';
                                $listImages->weight = 0;
                                $listImages->width = 0;
                                $listImages->height = 0;
                                $listImages->ext = '';
                                $listImages->nameFile = '';
                            }
                        }else{
                            $listImages->date = '';
                            $listImages->weight = 0;
                            $listImages->width = 0;
                            $listImages->height = 0;
                            $listImages->ext = '';
                            $listImages->nameFile = '';
                        }

                        $result->push($listImages);
                    
                    }
                }
            }
        }
        return $result;
    }


    #endregion PRIVATE

    #region GET


    public static function getByFile(request $request)
    {
        $result = collect();
        $dir = static::$dirImageAttachment;
        $dirNew = static::$dirImageAttachmentImages;
        $cont = 0;
        if($request->page == 1){
            $checkPageStart = 1;
        }else{
            $checkPageStart = ($request->page * 100) - 100;
        }
        $checkPageEnd = $request->page * 100;
        if($request->year != ''){
            if ($request->month != ''){
                $dirNew = $dirNew . '/'. $request->year . '/' . $request->month;
            }else{
                $dirNew = $dirNew . '/'. $request->year;
            }
        }else{
            if ($request->month != ''){
                $dirNew = $dirNew . '/'. date('Y') . '/' . $request->month;
            }
        }

        $extRequest = ['.png', '.jpg', '.jpeg', '.gif', '.webp', '.pdf', '.zip', '.xls', 'xlsx', '.doc'];

        if(isset($request->ext)){
            if($request->ext!=''){
                if($request->ext=='.jpg'){
                    $extRequest = ['.jpg', '.jpeg'];
                }else{
                    $extRequest = [$request->ext];
                }
            }
        }

        $resultImage = static::_getFileDir($dirNew, $request->year, $request->month);
        foreach ($resultImage as $f) {
        //foreach (\File::files($dir) as $f) {
            if (ends_with($f->name, $extRequest)) {
                if($request->search != '' && !is_null($request->search)){
                    if (strpos(strtolower($f->name), strtolower($request->search)) !== false) {
                        $cont = $cont + 1;
                        if($cont >= $checkPageStart && $cont < $checkPageEnd){
                            $listImages = new DtoImage();
                            $listImages->id = $cont;
                            $listImages->idImgAgg = '';
                            $listImages->img =  $f->url;
                            $listImages->imgName = $f->name;
                            $listImages->title = $f->nameFile;
                            $listImages->visible = true;
                            $listImages->selected = false;
                            $listImages->active = false;
                            $listImages->alt = '';
                            $listImages->didascalia = '';
                            $listImages->width = $f->width;
                            $listImages->height = $f->height;
                            $listImages->weight = $f->weight;
                            $listImages->date = $f->date;
                            $listImages->url = $f->url;
                            $listImages->ext = $f->ext;
                            $result->push($listImages);
                        }
                    }
                }else{
                    $cont = $cont + 1;
                    if($cont >= $checkPageStart && $cont < $checkPageEnd){
                        $listImages = new DtoImage();
                        $listImages->id = $cont;
                        $listImages->idImgAgg = '';
                        $listImages->img = $f->url;
                        $listImages->imgName = $f->name;
                        $listImages->title = $f->nameFile;
                        $listImages->visible = true;
                        $listImages->selected = false;
                        $listImages->active = false;
                        $listImages->alt = '';
                        $listImages->didascalia = '';
                        $listImages->width = $f->width;
                        $listImages->height = $f->height;
                        $listImages->weight = $f->weight;
                        $listImages->date = $f->date;
                        $listImages->url = $f->url;
                        $listImages->ext = $f->ext;
                        $result->push($listImages);
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Get
     *
     * @param String $search
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return ItemAttachmentLanguageFileTable::select('item_attachment_type_id as id', 'description')->orderBy('description')->get();
        } else {
            return ItemAttachmentLanguageFileTable::where('description', 'ilike','%'. $search . '%')->select('item_attachment_type_id as id', 'description')->orderBy('description')->get();
        }
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoOptional
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (DB::table('item_attachment_file')
                ->select('item_attachment_file.*', 'item_attachment_language_file_table.description', 'item_attachment_language_file_table.url', 'item_attachment_language_file_table.language_id')
                ->join('item_attachment_language_file_table', 'item_attachment_file.id', '=', 'item_attachment_language_file_table.item_attachment_file_id')
                ->where('item_attachment_language_file_table.language_id', $idLanguage)
                ->get()
        as $itemAttachmentAll) {
            
            $dtoItemAttachmentLanguageTypeTable = new DtoItemAttachmentFileTable();
            $dtoItemAttachmentLanguageTypeTable->id = $itemAttachmentAll->id;
            $dtoItemAttachmentLanguageTypeTable->language_id = $itemAttachmentAll->language_id;
            $dtoItemAttachmentLanguageTypeTable->attachment = $itemAttachmentAll->url;
            
            $stringItems = '';
            
            /*$itemAttachment = ItemAttachmentLanguageFileItemTable::join('items_languages', 'items_languages.item_id', '=', 'item_attachment_language_file_item_table.item_id')->where('item_attachment_file_id', $itemAttachmentAll->id)->count();
            if(isset($itemAttachment)){
                foreach ($itemAttachment as $itemAttachments) {
                    if($stringItems == ''){
                        $stringItems = $itemAttachments->description;
                    }else{
                        $stringItems = $stringItems . ', ' . $itemAttachments->description;
                    }
                    
                }
                $stringItems = $itemAttachment;
            }*/

            //if(strlen($stringItems) > 50){
            //    $dtoItemAttachmentLanguageTypeTable->items = substr($stringItems,0,47).'...';
            //}else{
                $dtoItemAttachmentLanguageTypeTable->items = $stringItems;
            //}


            $stringCategory = '';
            /*$categoryAttachment = ItemAttachmentLanguageFileCategoryTable::join('categories_languages', 'categories_languages.category_id', '=', 'item_attachment_language_file_category_table.category_id')->where('item_attachment_file_id', $itemAttachmentAll->id)->count();
            if(isset($categoryAttachment)){
                foreach ($categoryAttachment as $categoryAttachments) {
                    if($stringCategory == ''){
                        $stringCategory = $categoryAttachments->description;
                    }else{
                        $stringCategory = $stringCategory . ', ' . $categoryAttachments->description;
                    }
                    
                }
                $stringCategory = $categoryAttachment;
            }*/

            //if(strlen($stringCategory) > 50){
            //    $dtoItemAttachmentLanguageTypeTable->categories = substr($stringCategory,0,47).'...';
            //}else{
                $dtoItemAttachmentLanguageTypeTable->categories = $stringCategory;
            //}
                       

            $itemAttachmentFather = ItemAttachmentLanguageTypeTable::where('item_attachment_type_id', $itemAttachmentAll->item_attachment_type_id)->where('language_id', $idLanguage)->get()->first();
            if(isset($itemAttachmentFather)){
                $dtoItemAttachmentLanguageTypeTable->typeAttachmentId = $itemAttachmentAll->item_attachment_type_id;
                $dtoItemAttachmentLanguageTypeTable->typeAttachmentDescription = $itemAttachmentFather->description;
            }else{
                $dtoItemAttachmentLanguageTypeTable->typeAttachmentId = '';
                $dtoItemAttachmentLanguageTypeTable->typeAttachmentDescription = '';
            }
                        
            $dtoItemAttachmentLanguageTypeTable->description = $itemAttachmentAll->description;


            /*
            $itemAttachmentTypeTableAdds = ItemAttachmentLanguageFileTable::where('item_attachment_file_id', $itemAttachmentAll->id)->get();
            
            if (isset($itemAttachmentTypeTableAdds)) {
                foreach ($itemAttachmentTypeTableAdds as $itemAttachmentTypeTableAdd) {
                    $dtoItemAttachmentTypeTableAdd = new DtoItemAttachmentLanguageFileTable();
                    $dtoItemAttachmentTypeTableAdd->id = $itemAttachmentTypeTableAdd['id'];
                    $dtoItemAttachmentTypeTableAdd->language_id = $itemAttachmentTypeTableAdd['language_id'];
                    $dtoItemAttachmentTypeTableAdd->language_description = Language::where('id', '=', $itemAttachmentTypeTableAdd['language_id'])->first()->description;
                    $dtoItemAttachmentTypeTableAdd->description = $itemAttachmentTypeTableAdd['description'];
                    $dtoItemAttachmentTypeTableAdd->url = $itemAttachmentTypeTableAdd['url'];
                    $dtoItemAttachmentTypeTableAdd->file = str_replace('/storage/files/','', $itemAttachmentTypeTableAdd['url']);
                    
                    $dtoItemAttachmentLanguageTypeTable->translateAttachment->push($dtoItemAttachmentTypeTableAdd);
                } 
            }   
            */
             

            /*
            $itemAttachmentItemTableAdds = ItemAttachmentLanguageFileItemTable::where('item_attachment_file_id', $itemAttachmentAll->id)->get();
            
            if (isset($itemAttachmentItemTableAdds)) {
                foreach ($itemAttachmentItemTableAdds as $itemAttachmentItemTableAdd) {
                    $itemLanguage = ItemLanguage::where('item_id', $itemAttachmentItemTableAdd['item_id'])->where('language_id', '=', $idLanguage)->first();
                    if(isset($itemLanguage)){
                        $dtoItemAttachmentItemTableAdd = new DtoItemAttachmentFileItemTable();
                        $dtoItemAttachmentItemTableAdd->id = $itemAttachmentItemTableAdd['item_id'];
                        $dtoItemAttachmentItemTableAdd->item_id = $itemAttachmentItemTableAdd['item_id'];

                        $item = Item::where('id', $itemAttachmentItemTableAdd['item_id'])->first();
                        if(isset($item)){
                            if($item['internal_code'] != ''){
                                $dtoItemAttachmentItemTableAdd->code = $item['internal_code'];
                            }else{
                                $dtoItemAttachmentItemTableAdd->code = '-';
                            }
                        }else{
                            $dtoItemAttachmentItemTableAdd->code = '-';
                        }
                        
                        //if(strlen($itemLanguage['description']) > 17){
                        //    $dtoItemAttachmentItemTableAdd->description = substr($itemLanguage['description'],0,14).'...';
                        //}else{
                            $dtoItemAttachmentItemTableAdd->description = $itemLanguage['description'];
                        //}

                        $dtoItemAttachmentLanguageTypeTable->itemIncluded->push($dtoItemAttachmentItemTableAdd);
                    }
                } 
            } 
            
            $itemAttachmentCategoryTableAdds = ItemAttachmentLanguageFileCategoryTable::where('item_attachment_file_id', $itemAttachmentAll->id)->get();
            
            if (isset($itemAttachmentCategoryTableAdds)) {
                foreach ($itemAttachmentCategoryTableAdds as $itemAttachmentCategoryTableAdd) {
                    $categoryLanguage = CategoryLanguage::where('category_id', $itemAttachmentCategoryTableAdd['category_id'])->where('language_id', '=', $idLanguage)->first();
                    if(isset($categoryLanguage)){
                        $dtoItemAttachmentCategoryTableAdd = new DtoItemAttachmentFileCategoryTable();
                        $dtoItemAttachmentCategoryTableAdd->id = $itemAttachmentCategoryTableAdd['category_id'];
                        $dtoItemAttachmentCategoryTableAdd->category_id = $itemAttachmentCategoryTableAdd['category_id'];

                        $category = Category::where('id', $itemAttachmentCategoryTableAdd['category_id'])->first();
                        if(isset($category)){
                            $dtoItemAttachmentCategoryTableAdd->code = $category['code'];
                        }else{
                            $dtoItemAttachmentCategoryTableAdd->code = '';
                        }

                        $dtoItemAttachmentCategoryTableAdd->description = $categoryLanguage['description'];
                        $dtoItemAttachmentLanguageTypeTable->categoryIncluded->push($dtoItemAttachmentCategoryTableAdd);
                    }
                } 
            }

            $itemAttachmentGroupNewsletterFileTable = itemAttachmentGroupNewsletterFileTable::where('item_attachment_file_id', $itemAttachmentAll->id)->get();
            
            if (isset($itemAttachmentGroupNewsletterFileTable)) {
                foreach ($itemAttachmentGroupNewsletterFileTable as $itemAttachmentGroupNewsletterFileTables) {
                    $DtoItemAttachmentGroupNewsletterFileTable = new DtoItemAttachmentGroupNewsletterFileTable();

                    $DtoItemAttachmentGroupNewsletterFileTable->id = $itemAttachmentGroupNewsletterFileTables['id'];
                    if(isset($itemAttachmentGroupNewsletterFileTables)){
                        $DtoItemAttachmentGroupNewsletterFileTable->customergroup = $itemAttachmentGroupNewsletterFileTables['group_newsletter_id'];
                    }else{
                        $DtoItemAttachmentGroupNewsletterFileTable->customergroup = '';
                    }

                    $groupNewsletter= GroupNewsletter::where('id', $itemAttachmentGroupNewsletterFileTables['group_newsletter_id'])->first();
                    if(isset($groupNewsletter)){
                        $DtoItemAttachmentGroupNewsletterFileTable->description = $groupNewsletter['description'];
                    }else{
                        $DtoItemAttachmentGroupNewsletterFileTable->description = '';
                    }
                    $dtoItemAttachmentLanguageTypeTable->groupNewsletter->push($DtoItemAttachmentGroupNewsletterFileTable);
                } 
            }*/
        
            $result->push($dtoItemAttachmentLanguageTypeTable); 
        }

        return $result;
    }


    /**
     * get Detail by id
     * 
     * @param int idLanguage
     * 
     * @return DtoOptional
     */
    public static function getDetail($id, $idLanguage)
    {
        $result = collect();
        foreach (DB::table('item_attachment_file')
                ->select('item_attachment_file.*', 'item_attachment_language_file_table.description', 'item_attachment_language_file_table.url', 'item_attachment_language_file_table.language_id')
                ->join('item_attachment_language_file_table', 'item_attachment_file.id', '=', 'item_attachment_language_file_table.item_attachment_file_id')
                ->where('item_attachment_language_file_table.language_id', $idLanguage)
                ->where('item_attachment_file.id', $id)
                ->get()
        as $itemAttachmentAll) {
            
            $dtoItemAttachmentLanguageTypeTable = new DtoItemAttachmentFileTable();
            $dtoItemAttachmentLanguageTypeTable->id = $itemAttachmentAll->id;
            $dtoItemAttachmentLanguageTypeTable->language_id = $itemAttachmentAll->language_id;
            $dtoItemAttachmentLanguageTypeTable->attachment = $itemAttachmentAll->url;
            
            $stringItems = '';
            $stringCategory = '';

            $dtoItemAttachmentLanguageTypeTable->items = $stringItems;
            $dtoItemAttachmentLanguageTypeTable->categories = $stringCategory;
            
            $itemAttachmentFather = ItemAttachmentLanguageTypeTable::where('item_attachment_type_id', $itemAttachmentAll->item_attachment_type_id)->where('language_id', $idLanguage)->get()->first();
            if(isset($itemAttachmentFather)){
                $dtoItemAttachmentLanguageTypeTable->typeAttachmentId = $itemAttachmentAll->item_attachment_type_id;
                $dtoItemAttachmentLanguageTypeTable->typeAttachmentDescription = $itemAttachmentFather->description;
            }else{
                $dtoItemAttachmentLanguageTypeTable->typeAttachmentId = '';
                $dtoItemAttachmentLanguageTypeTable->typeAttachmentDescription = '';
            }
                        
            $dtoItemAttachmentLanguageTypeTable->description = $itemAttachmentAll->description;

            $itemAttachmentTypeTableAdds = ItemAttachmentLanguageFileTable::where('item_attachment_file_id', $itemAttachmentAll->id)->get();
            
            if (isset($itemAttachmentTypeTableAdds)) {
                foreach ($itemAttachmentTypeTableAdds as $itemAttachmentTypeTableAdd) {
                    $dtoItemAttachmentTypeTableAdd = new DtoItemAttachmentLanguageFileTable();
                    $dtoItemAttachmentTypeTableAdd->id = $itemAttachmentTypeTableAdd['id'];
                    $dtoItemAttachmentTypeTableAdd->language_id = $itemAttachmentTypeTableAdd['language_id'];
                    $dtoItemAttachmentTypeTableAdd->language_description = Language::where('id', '=', $itemAttachmentTypeTableAdd['language_id'])->first()->description;
                    $dtoItemAttachmentTypeTableAdd->description = $itemAttachmentTypeTableAdd['description'];
                    $dtoItemAttachmentTypeTableAdd->url = $itemAttachmentTypeTableAdd['url'];
                    $dtoItemAttachmentTypeTableAdd->file = str_replace('/storage/files/','', $itemAttachmentTypeTableAdd['url']);
                    
                    $dtoItemAttachmentLanguageTypeTable->translateAttachment->push($dtoItemAttachmentTypeTableAdd);
                } 
            }   
            
            $itemAttachmentItemTableAdds = ItemAttachmentLanguageFileItemTable::where('item_attachment_file_id', $itemAttachmentAll->id)->get();
            
            if (isset($itemAttachmentItemTableAdds)) {
                foreach ($itemAttachmentItemTableAdds as $itemAttachmentItemTableAdd) {
                    $itemLanguage = ItemLanguage::where('item_id', $itemAttachmentItemTableAdd['item_id'])->where('language_id', '=', $idLanguage)->first();
                    if(isset($itemLanguage)){
                        $dtoItemAttachmentItemTableAdd = new DtoItemAttachmentFileItemTable();
                        $dtoItemAttachmentItemTableAdd->id = $itemAttachmentItemTableAdd['item_id'];
                        $dtoItemAttachmentItemTableAdd->item_id = $itemAttachmentItemTableAdd['item_id'];

                        $item = Item::where('id', $itemAttachmentItemTableAdd['item_id'])->first();
                        if(isset($item)){
                            if($item['internal_code'] != ''){
                                $dtoItemAttachmentItemTableAdd->code = $item['internal_code'];
                            }else{
                                $dtoItemAttachmentItemTableAdd->code = '-';
                            }
                        }else{
                            $dtoItemAttachmentItemTableAdd->code = '-';
                        }
                        
                        //if(strlen($itemLanguage['description']) > 17){
                        //    $dtoItemAttachmentItemTableAdd->description = substr($itemLanguage['description'],0,14).'...';
                        //}else{
                            $dtoItemAttachmentItemTableAdd->description = $itemLanguage['description'];
                        //}

                        $dtoItemAttachmentLanguageTypeTable->itemIncluded->push($dtoItemAttachmentItemTableAdd);
                    }
                } 
            } 
            
            $itemAttachmentCategoryTableAdds = ItemAttachmentLanguageFileCategoryTable::where('item_attachment_file_id', $itemAttachmentAll->id)->get();
            
            if (isset($itemAttachmentCategoryTableAdds)) {
                foreach ($itemAttachmentCategoryTableAdds as $itemAttachmentCategoryTableAdd) {
                    $categoryLanguage = CategoryLanguage::where('category_id', $itemAttachmentCategoryTableAdd['category_id'])->where('language_id', '=', $idLanguage)->first();
                    if(isset($categoryLanguage)){
                        $dtoItemAttachmentCategoryTableAdd = new DtoItemAttachmentFileCategoryTable();
                        $dtoItemAttachmentCategoryTableAdd->id = $itemAttachmentCategoryTableAdd['category_id'];
                        $dtoItemAttachmentCategoryTableAdd->category_id = $itemAttachmentCategoryTableAdd['category_id'];

                        $category = Category::where('id', $itemAttachmentCategoryTableAdd['category_id'])->first();
                        if(isset($category)){
                            $dtoItemAttachmentCategoryTableAdd->code = $category['code'];
                        }else{
                            $dtoItemAttachmentCategoryTableAdd->code = '';
                        }

                        $dtoItemAttachmentCategoryTableAdd->description = $categoryLanguage['description'];
                        $dtoItemAttachmentLanguageTypeTable->categoryIncluded->push($dtoItemAttachmentCategoryTableAdd);
                    }
                } 
            }

            $itemAttachmentGroupNewsletterFileTable = itemAttachmentGroupNewsletterFileTable::where('item_attachment_file_id', $itemAttachmentAll->id)->get();
            
            if (isset($itemAttachmentGroupNewsletterFileTable)) {
                foreach ($itemAttachmentGroupNewsletterFileTable as $itemAttachmentGroupNewsletterFileTables) {
                    $DtoItemAttachmentGroupNewsletterFileTable = new DtoItemAttachmentGroupNewsletterFileTable();

                    $DtoItemAttachmentGroupNewsletterFileTable->id = $itemAttachmentGroupNewsletterFileTables['id'];
                    if(isset($itemAttachmentGroupNewsletterFileTables)){
                        $DtoItemAttachmentGroupNewsletterFileTable->customergroup = $itemAttachmentGroupNewsletterFileTables['group_newsletter_id'];
                    }else{
                        $DtoItemAttachmentGroupNewsletterFileTable->customergroup = '';
                    }

                    $groupNewsletter= GroupNewsletter::where('id', $itemAttachmentGroupNewsletterFileTables['group_newsletter_id'])->first();
                    if(isset($groupNewsletter)){
                        $DtoItemAttachmentGroupNewsletterFileTable->description = $groupNewsletter['description'];
                    }else{
                        $DtoItemAttachmentGroupNewsletterFileTable->description = '';
                    }
                    $dtoItemAttachmentLanguageTypeTable->groupNewsletter->push($DtoItemAttachmentGroupNewsletterFileTable);
                } 
            }

            $itemAttachmentUserFileTable = itemAttachmentUserFileTable::where('item_attachment_file_id', $itemAttachmentAll->id)->get();
            
            if (isset($itemAttachmentUserFileTable)) {
                foreach ($itemAttachmentUserFileTable as $itemAttachmentUserFileTables) {
                    $DtoitemAttachmentUserFileTable = new DtoItemAttachmentGroupNewsletterFileTable();

                    $DtoitemAttachmentUserFileTable->id = $itemAttachmentUserFileTables['id'];
                    if(isset($itemAttachmentUserFileTables)){
                        $DtoitemAttachmentUserFileTable->userid = $itemAttachmentUserFileTables['user_id'];
                    }else{
                        $DtoitemAttachmentUserFileTable->userid = '';
                    }

                    $user = User::where('id', $itemAttachmentUserFileTables['user_id'])->first();
                    if(isset($user)){
                        $DtoitemAttachmentUserFileTable->description = $user['name'];
                    }else{
                        $DtoitemAttachmentUserFileTable->description = '';
                    }
                    $dtoItemAttachmentLanguageTypeTable->userIncluded->push($DtoitemAttachmentUserFileTable);
                } 
            }
        
            $result->push($dtoItemAttachmentLanguageTypeTable); 
        }

        return $result;
    }
    

    public static function getAllByAttachmentUser(Request $request)
    {
        /*

        $finalHtml = "";
        $idContent = Content::where('code', 'ListAttachmentUser')->get()->first();
        $contentLanguage = ContentLanguage::where('content_id', $idContent->id)->where('language_id', $request->IdLanguage)->get()->first();
        $strWhere = "";

        if ($request->name_file != "") {
            if($strWhere == ''){
                $strWhere = ' AND item_attachment_language_file_table.url like \'%' . $request->name_file . '%\'';
            }else{
                $strWhere = $strWhere . ' AND item_attachment_language_file_table.url like \'%' . $request->name_file . '%\'';
            } 
        }

        if ($request->description != "") {
            if($strWhere == ''){
                $strWhere = ' AND item_attachment_language_file_table.description like \'%' . $request->description . '%\'';
            }else{
                $strWhere = $strWhere . ' AND item_attachment_language_file_table.description like \'%' . $request->description . '%\'';
            } 
        }

        if ($request->tipology != "") {
            if($strWhere == ''){
                $strWhere = ' AND item_attachment_language_type_table.item_attachment_type_id = ' . $request->tipology . '';
            }else{
                $strWhere = $strWhere . ' AND item_attachment_language_type_table.item_attachment_type_id like ' . $request->tipology . '';
            } 
        }

        $tags = SelenaViewsBL::getTagByType('ListAttachmentUser');
        foreach (DB::select(
                'SELECT 
                item_attachment_language_file_table.url,
                item_attachment_language_file_table.description,
                item_attachment_language_type_table.item_attachment_type_id
                FROM item_attachment_language_file_group_newsletters_table 
                INNER JOIN item_attachment_language_file_table 
                ON item_attachment_language_file_table.item_attachment_file_id = 
                item_attachment_language_file_group_newsletters_table.item_attachment_file_id
                INNER JOIN item_attachment_file ON item_attachment_file.id = item_attachment_language_file_table.item_attachment_file_id
                inner join item_attachment_language_type_table on item_attachment_language_type_table.item_attachment_type_id = item_attachment_file.item_attachment_type_id 
                WHERE item_attachment_language_file_group_newsletters_table.group_newsletter_id in 
                (select group_newsletter_id from customers_groups_newsletters where customer_id = ' . $request->UserId . ')'. $strWhere .'
                ORDER BY item_attachment_language_file_table.description DESC'
                ) as $groupnewsletter){

            $newContent = $contentLanguage->content;  
            foreach ($tags as $fieldsDb){
                if(isset($fieldsDb->tag)){                                                              
                    if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                        if(is_numeric($groupnewsletter->{$cleanTag}) && strpos($groupnewsletter->{$cleanTag},'.')){
                            $tag = str_replace('.',',',substr($groupnewsletter->{$cleanTag}, 0, -2));
                        }else{
                            $tag = $groupnewsletter->{$cleanTag};
                        }
                        $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                               
                    }
                }
            }
            if (strpos($newContent, '#tipology#') !== false) {
                $descriptionStates = ItemAttachmentLanguageTypeTable::where('item_attachment_type_id', $groupnewsletter->item_attachment_type_id)->first();   
                if (isset($descriptionStates)){
                    $newContent = str_replace('#tipology#', $descriptionStates->description,  $newContent); 
                }else{
                    $newContent = str_replace('#tipology#', '',  $newContent);           
                }
            }

         
            $finalHtml = $finalHtml . $newContent;
          
        }; 
                       
        return $finalHtml;*/

        $strWhere = "";

        if ($request->name_file != "") {
            if($strWhere == ''){
                $strWhere = ' AND ialft.url like \'%' . $request->name_file . '%\'';
            }else{
                $strWhere = $strWhere . ' AND ialft.url like \'%' . $request->name_file . '%\'';
            } 
        }

        if ($request->description != "") {
            if($strWhere == ''){
                $strWhere = ' AND ialft.description like \'%' . $request->description . '%\'';
            }else{
                $strWhere = $strWhere . ' AND ialft.description like \'%' . $request->description . '%\'';
            } 
        }

        if ($request->tipology != "") {
            if($strWhere == ''){
                $strWhere = ' AND ialtt.item_attachment_type_id = ' . $request->tipology . '';
            }else{
                $strWhere = $strWhere . ' AND ialtt.item_attachment_type_id = ' . $request->tipology . '';
            } 
        }

        $finalHtml = "";
        $idContent = Content::where('code', 'ItemAttachmentType')->get()->first();
        $contentLanguage = ContentLanguage::where('content_id', $idContent->id)->where('language_id', $request->IdLanguage)->get()->first();

        foreach (DB::select(
        'SELECT DISTINCT
        ialtt.description,
        iaf.item_attachment_type_id
        FROM customers_groups_newsletters AS cgn
        INNER JOIN item_attachment_language_file_group_newsletters_table AS ia ON ia.group_newsletter_id = cgn.group_newsletter_id 
        INNER JOIN item_attachment_file iaf ON iaf.id = ia.item_attachment_file_id 
        INNER JOIN item_attachment_language_file_table AS ialft ON ialft.item_attachment_file_id = iaf.id
        INNER JOIN item_attachment_language_type_table ialtt ON ialtt.item_attachment_type_id = iaf.item_attachment_type_id 
        WHERE cgn.customer_id = ' . $request->UserId . ''. $strWhere .'' 
        ) as $Typology) {

            $newContent = $contentLanguage->content;
            $newContent = str_replace('#description#' , $Typology->description ,  $newContent);

            if (strpos($newContent, '#item_attachment_file#') !== false) {
                $contentItemAttachmentFile = Content::where('code', 'ItemAttachmentFile')->first();
                $contentItemAttachmentFileLanguage = ContentLanguage::where('content_id', $contentItemAttachmentFile->id)->where('language_id', $request->IdLanguage)->first();
                $finalHtmlItemTechnicalSpecifications = "";

                foreach (DB::select(
                'SELECT 
                ialft.description,
                ialft.url
                FROM item_attachment_language_file_table ialft 
                INNER JOIN item_attachment_file iaf ON iaf.id = ialft.item_attachment_file_id 
                INNER JOIN item_attachment_language_type_table ialtt ON ialtt.item_attachment_type_id = iaf.item_attachment_type_id 
                INNER JOIN item_attachment_language_file_group_newsletters_table ialfgnt ON ialfgnt.item_attachment_file_id 
                = iaf.id 
                INNER JOIN customers_groups_newsletters cgn ON cgn.group_newsletter_id = ialfgnt.group_newsletter_id 
                WHERE cgn.customer_id = ' . $request->UserId . '
                AND ialtt.item_attachment_type_id = ' . $Typology->item_attachment_type_id . ''. $strWhere .'' ) as $DetailFile) {
                     
                    $replaceUrl = str_replace('/storage/files/', '', $DetailFile->url); 
                    $fileSize = static::_convertImageSize(filesize(storage_path("app/public/files/". $replaceUrl)));


                    $newitemAttachmentFile = $contentItemAttachmentFileLanguage->content;
                    $newitemAttachmentFile = str_replace('#description#' , $DetailFile->description ,  $newitemAttachmentFile);
                    $newitemAttachmentFile = str_replace('#url#' , $DetailFile->url ,  $newitemAttachmentFile);
                    $newitemAttachmentFile = str_replace('#filesize#' , $fileSize ,  $newitemAttachmentFile);
                    $finalHtmlItemTechnicalSpecifications = $finalHtmlItemTechnicalSpecifications . $newitemAttachmentFile;
                }
                    $newContent = str_replace('#item_attachment_file#' , $finalHtmlItemTechnicalSpecifications ,  $newContent); 

            }
   
                     $finalHtml = $finalHtml . $newContent;
       
        }
        return $finalHtml;  
    }

    private static function getUrlImageSize()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '';
    }


    /**
     * get Item By Id Attachment
     * 
     * @param Request $request
     * 
     * @return DtoOptional
     */
    public static function getItemByIdAttachment($request)
    {
        $result = collect();

        $strSearch = "";
        if($request->description != ''){
            $strSearch = " AND (items_languages.description like '%" . $request->description . "%' or items.internal_code like '%" . $request->description . "%')";
        }

        if($request->producer_id != ''){
            $strSearch = $strSearch . " AND producer.id =" . $request->producer_id;
        }

        if($request->id == '' || is_null($request->id)){
            $idQuery = 0;
        }else{
            $idQuery = $request->id;
        }

        if($request->online){
            $strSearch = $strSearch . " AND items.online = true ";
        }

        if($request->category_id  != ''){
            $strSearch = $strSearch . " AND categories_languages.id = " . $request->category_id;
        }

        $itemAttachmentTypeOnDb = ItemAttachmentFile::find($request->id);
        if(isset($itemAttachmentTypeOnDb)){
            foreach (DB::select(
                "select items.id, items.internal_code, items_languages.description, producer.business_name, categories_languages.description as descriptioncat from items_languages
                inner join items on items.id = items_languages.item_id
                left join producer on producer.id = items.producer_id
                left join categories_items on categories_items.item_id = items.id
                left join categories on categories.id = categories_items.category_id
                left join categories_languages on categories_languages.category_id = categories.id
                where items_languages.item_id not in (select item_attachment_language_file_item_table.item_id from item_attachment_language_file_item_table where item_attachment_language_file_item_table.item_attachment_file_id = " . $idQuery . ") 
                and items_languages.language_id = 1 " . $strSearch . " 
                order by items_languages.description"
                ) as $itemsExcluded){
                    $dtoItemAttachmentItemTableAdd = new DtoItemAttachmentFileItemTable();
                    $dtoItemAttachmentItemTableAdd->id = $itemsExcluded->id;
                    if($itemsExcluded->internal_code==''){
                        $dtoItemAttachmentItemTableAdd->code = '-';
                    }else{
                        $dtoItemAttachmentItemTableAdd->code = $itemsExcluded->internal_code;
                    }
                    
                    //if(strlen($itemsExcluded->description) > 17){
                    //    $dtoItemAttachmentItemTableAdd->description = substr($itemsExcluded->description,0,14).'...';
                    //}else{
                        $dtoItemAttachmentItemTableAdd->description = $itemsExcluded->description;
                    //}
                    $dtoItemAttachmentItemTableAdd->producer = $itemsExcluded->business_name;
                    $dtoItemAttachmentItemTableAdd->category = $itemsExcluded->descriptioncat;

                    $result->push($dtoItemAttachmentItemTableAdd);

            }
        }

        return $result;
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request)
    {
        //static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $itemAttachmentType = new ItemAttachmentFile();
            $itemAttachmentType->item_attachment_type_id = $request->typeAttachmentId;
            //$itemAttachmentType->created_id = $idUser;
            $itemAttachmentType->save();

            if(count($request['translateAttachment'])>0){
                foreach ($request['translateAttachment'] as $translateAttachments) {
                    if(!is_null($translateAttachments['fileBase64'])){
                        $bin = $translateAttachments['fileBase64'];
                        $data = explode(',', $bin);
                        file_put_contents(static::$dirImagePdfModule . $translateAttachments['file'], base64_decode($data[1]));
                    }
                    
                    $itemAttachmentTypeLanguages = new ItemAttachmentLanguageFileTable();
                    $itemAttachmentTypeLanguages->item_attachment_file_id = $itemAttachmentType->id;
                    $itemAttachmentTypeLanguages->description = $translateAttachments['description'];
                    $itemAttachmentTypeLanguages->url = $translateAttachments['url'];
                    $itemAttachmentTypeLanguages->language_id = $translateAttachments['language_id'];
                   // $itemAttachmentTypeLanguages->created_id = $idUser;
                    $itemAttachmentTypeLanguages->save();
                }
            }

            if(count($request['itemIncluded'])>0){
                foreach ($request['itemIncluded'] as $ItemAttachments) {
                    $itemAttachmentItemFile = new ItemAttachmentLanguageFileItemTable();
                    $itemAttachmentItemFile->item_attachment_file_id = $itemAttachmentType->id;
                    $itemAttachmentItemFile->item_id = $ItemAttachments['id'];
                    //$itemAttachmentItemFile->created_id = $idUser;
                    $itemAttachmentItemFile->save();
                }
            }

            if(count($request['categoryIncluded'])>0){
                foreach ($request['categoryIncluded'] as $CategoryAttachments) {
                    $itemAttachmentCategoryFile = new ItemAttachmentLanguageFileCategoryTable();
                    $itemAttachmentCategoryFile->item_attachment_file_id = $itemAttachmentType->id;
                    $categoryLanguage = CategoryLanguage::where('id', $CategoryAttachments['id'])->first();
                    if(isset($categoryLanguage)){
                        $itemAttachmentCategoryFile->category_id = $categoryLanguage->category_id;
                    }
                  //  $itemAttachmentCategoryFile->created_id = $idUser;
                    $itemAttachmentCategoryFile->save();
                }
            }

          
            if(count($request['groupNewsletter'])>0){
                foreach ($request['groupNewsletter'] as $groupNewsletters) {
                $itemAttachmentGroupNewsletterFileTable = new itemAttachmentGroupNewsletterFileTable();
                $itemAttachmentGroupNewsletterFileTable->item_attachment_file_id = $itemAttachmentType->id;
                $itemAttachmentGroupNewsletterFileTable->group_newsletter_id = $groupNewsletters['customergroup'];
              //  $itemAttachmentGroupNewsletterFileTable->created_id = $idUser;
                $itemAttachmentGroupNewsletterFileTable->save();
                }
            }

            if(count($request['userIncluded'])>0){
                foreach ($request['userIncluded'] as $users) {
                $itemAttachmentUserFileTable = new itemAttachmentUserFileTable();
                $itemAttachmentUserFileTable->item_attachment_file_id = $itemAttachmentType->id;
                $itemAttachmentUserFileTable->user_id = $users['userid'];
              //  $itemAttachmentGroupNewsletterFileTable->created_id = $idUser;
                $itemAttachmentUserFileTable->save();
                }
            }
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $itemAttachmentType->id;
    }


    /**
     * insert From Albero
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertFromAlbero(Request $request)
    {
        //static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $itemAttachmentType = new ItemAttachmentFile();
            $itemAttachmentType->item_attachment_type_id = $request->idType;
            $itemAttachmentType->save();

            if($request->newAttachmentFileBase64 != ''){
                if(!is_null($request->newAttachmentFileBase64)){
                    $bin = $request->newAttachmentFileBase64;
                    $data = explode(',', $bin);
                    file_put_contents(static::$dirImagePdfModule . $request->newAttachmentFile, base64_decode($data[1]));
                }
                
                $itemAttachmentTypeLanguages = new ItemAttachmentLanguageFileTable();
                $itemAttachmentTypeLanguages->item_attachment_file_id = $itemAttachmentType->id;
                $itemAttachmentTypeLanguages->description = $request->newAttachmentFileDescription;
                $itemAttachmentTypeLanguages->url = $request->newAttachmentFileUrl;
                $itemAttachmentTypeLanguages->language_id = 1;
                $itemAttachmentTypeLanguages->save();
            }

            if(!is_null($request->idItem)){
                $itemAttachmentItemFile = new ItemAttachmentLanguageFileItemTable();
                $itemAttachmentItemFile->item_attachment_file_id = $itemAttachmentType->id;
                $itemAttachmentItemFile->item_id = $request->idItem;
                $itemAttachmentItemFile->save();
            }

            $dtoItemFilesUrl = new DtoItemFilesUrl();
            $dtoItemFilesUrl->id = $itemAttachmentType->id;
            $dtoItemFilesUrl->description = $request->newAttachmentFileDescription;
            $dtoItemFilesUrl->url = $request->newAttachmentFileUrl;
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $dtoItemFilesUrl;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoOptional
     * @param int idLanguage
     */
    public static function update(Request $request, int $idLanguage)
    {
        //static::_validateData($request, $idLanguage, DbOperationsTypesEnum::UPDATE);
        
        $itemAttachmentTypeOnDb = ItemAttachmentFile::find($request->id);
        
        DB::beginTransaction();

        try {
            $itemAttachmentTypeOnDb->item_attachment_type_id = $request->typeAttachmentId;
            $itemAttachmentTypeOnDb->save();

            $itemAttachmentFileLanguagesOnDb = ItemAttachmentLanguageFileTable::where('item_attachment_file_id', $request->id);
            $itemAttachmentFileLanguagesOnDb->delete();
            
            if(count($request['translateAttachment'])>0){
                foreach ($request['translateAttachment'] as $translateAttachments) {
                    if(!is_null($translateAttachments['fileBase64'])){
                        $bin = $translateAttachments['fileBase64'];
                        $data = explode(',', $bin);
                        file_put_contents(static::$dirImagePdfModule . $translateAttachments['file'], base64_decode($data[1]));
                    }
                    
                    $itemAttachmentTypeLanguages = new ItemAttachmentLanguageFileTable();
                    $itemAttachmentTypeLanguages->item_attachment_file_id = $itemAttachmentTypeOnDb->id;
                    $itemAttachmentTypeLanguages->description = $translateAttachments['description'];
                    $itemAttachmentTypeLanguages->url = $translateAttachments['url'];
                    $itemAttachmentTypeLanguages->language_id = $translateAttachments['language_id'];
                    $itemAttachmentTypeLanguages->created_id = 1;
                    $itemAttachmentTypeLanguages->save();
                }
            }

            $itemAttachmentFileItemOnDb = ItemAttachmentLanguageFileItemTable::where('item_attachment_file_id', $request->id);
            $itemAttachmentFileItemOnDb->delete();

            if(count($request['itemIncluded'])>0){
                foreach ($request['itemIncluded'] as $ItemAttachments) {
                    $itemAttachmentItemFile = new ItemAttachmentLanguageFileItemTable();
                    $itemAttachmentItemFile->item_attachment_file_id = $itemAttachmentTypeOnDb->id;
                    $itemAttachmentItemFile->item_id = $ItemAttachments['id'];
                    $itemAttachmentItemFile->created_id = 1;
                    $itemAttachmentItemFile->save();
                }
            }

            $itemAttachmentFileCategoryOnDb = ItemAttachmentLanguageFileCategoryTable::where('item_attachment_file_id', $request->id);
            $itemAttachmentFileCategoryOnDb->delete();

            if(count($request['categoryIncluded'])>0){
                foreach ($request['categoryIncluded'] as $CategoryAttachments) {
                    $itemAttachmentCategoryFile = new ItemAttachmentLanguageFileCategoryTable();
                    $itemAttachmentCategoryFile->item_attachment_file_id = $itemAttachmentTypeOnDb->id;
                    $categoryLanguage = CategoryLanguage::where('id', $CategoryAttachments['id'])->first();
                    if(isset($categoryLanguage)){
                        $itemAttachmentCategoryFile->category_id = $categoryLanguage->category_id;
                    }
                    $itemAttachmentCategoryFile->created_id = 1;
                    $itemAttachmentCategoryFile->save();
                }
            }

            $itemAttachmentGroupNewsletterFileTableOnDb = itemAttachmentGroupNewsletterFileTable::where('item_attachment_file_id', $request->id);
            $itemAttachmentGroupNewsletterFileTableOnDb->delete();

            if(count($request['groupNewsletter'])>0) {
                foreach ($request['groupNewsletter'] as $groupNewsletters) {
                $itemAttachmentGroupNewsletterFileTable = new itemAttachmentGroupNewsletterFileTable();

                $itemAttachmentGroupNewsletterFileTable->item_attachment_file_id = $itemAttachmentTypeOnDb->id;
                $itemAttachmentGroupNewsletterFileTable->group_newsletter_id = $groupNewsletters['customergroup'];
                $itemAttachmentGroupNewsletterFileTable->created_id = 1;
                $itemAttachmentGroupNewsletterFileTable->save();
                }
            }


            $itemAttachmentUserFileTableOnDb = itemAttachmentUserFileTable::where('item_attachment_file_id', $request->id);
            $itemAttachmentUserFileTableOnDb->delete();
            if(count($request['userIncluded'])>0){
                foreach ($request['userIncluded'] as $users) {
                $itemAttachmentUserFileTable = new itemAttachmentUserFileTable();
                $itemAttachmentUserFileTable->item_attachment_file_id = $itemAttachmentTypeOnDb->id;
                $itemAttachmentUserFileTable->user_id = $users['userid'];
              //  $itemAttachmentGroupNewsletterFileTable->created_id = $idUser;
                $itemAttachmentUserFileTable->save();
                }
            }

            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }


    /**
     * delete From Item Attachment File Item
     * 
     * @param Request dtoOptional
     * @param int idLanguage
     */
    public static function deleteFromItemAttachmentFileItem(Request $request)
    {
        DB::beginTransaction();

        try {
            $itemAttachmentTypeLanguages = ItemAttachmentLanguageFileItemTable::where('item_attachment_file_id', $request->idAttachmentFile)->where('item_id', $request->idItem);
            $itemAttachmentTypeLanguages->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }
    

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $itemAttachmentType = ItemAttachmentFile::find($id);
        
        if (is_null($itemAttachmentType)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            
            $itemAttachmentTypeLanguages = ItemAttachmentLanguageFileTable::where('item_attachment_file_id', $id);
            $itemAttachmentTypeLanguages->delete();

            $itemAttachmentCategoryLanguages = ItemAttachmentLanguageFileCategoryTable::where('item_attachment_file_id', $id);
            $itemAttachmentCategoryLanguages->delete();

            $itemAttachmentItemLanguages = ItemAttachmentLanguageFileItemTable::where('item_attachment_file_id', $id);
            $itemAttachmentItemLanguages->delete();

            $itemAttachmentGroupNewsletterFileTable = itemAttachmentGroupNewsletterFileTable::where('item_attachment_file_id', $id);
            $itemAttachmentGroupNewsletterFileTable->delete();
            
            $itemAttachmentUserFileTable = itemAttachmentUserFileTable::where('item_attachment_file_id', $id);
            $itemAttachmentUserFileTable->delete();

            $itemAttachmentType->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    /**
     * delete translation by id
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delTranslation(int $id, int $idLanguage)
    {
        $itemAttachmentType = ItemAttachmentLanguageFileTable::find($id);

        $returnId = '';
        
        if (is_null($itemAttachmentType)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            $returnId = $itemAttachmentType->item_attachment_file_id;
            $itemAttachmentType->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $returnId;
    }

    #endregion DELETE
}

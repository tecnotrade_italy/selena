<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoSalesOrderDetail;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\LogHelper;
use App\SalesOrder;
use App\SalesOrderDetail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class SalesOrderDetailBL
{
    #region PRIVATE

    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request dtoSalesOrderDetail
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $dtoSalesOrderDetail, $dbOperationType)
    {
        
    }

    /**
     * Convert to dto
     * 
     * @param SalesOrderDetail SalesOrderDetailßß
     * @return DtoSalesOrderDetail
     */
    private static function _convertToDto($SalesOrderDetail)
    {
        $dtoSalesOrderDetail = new DtoSalesOrderDetail();

        if (isset($SalesOrderDetail->id)) {
            $dtoSalesOrderDetail->id = $SalesOrderDetail->id;
        }

        if (isset($SalesOrderDetail->order_id)) {
            $dtoSalesOrderDetail->order_id = $SalesOrderDetail->order_id;
        }

        if (isset($SalesOrderDetail->row_number)) {
            $dtoSalesOrderDetail->row_number = $SalesOrderDetail->row_number;
        }

        if (isset($SalesOrderDetail->sales_order_row_type_id)) {
            $dtoSalesOrderDetail->sales_order_row_type_id = $SalesOrderDetail->sales_order_row_type_id;
        }

        if (isset($SalesOrderDetail->order_detail_state_id)) {
            $dtoSalesOrderDetail->order_detail_state_id = $SalesOrderDetail->order_detail_state_id;
        }

        if (isset($SalesOrderDetail->item_id)) {
            $dtoSalesOrderDetail->item_id = $SalesOrderDetail->item_id;
        }

        if (isset($SalesOrderDetail->description)) {
            $dtoSalesOrderDetail->description = $SalesOrderDetail->description;
        }        

        if (isset($SalesOrderDetail->unit_of_measure_id)) {
            $dtoSalesOrderDetail->unit_of_measure_id = $SalesOrderDetail->unit_of_measure_id;
        }

        if (isset($SalesOrderDetail->unit_price)) {
            $dtoSalesOrderDetail->unit_price = $SalesOrderDetail->unit_price;
        }

        if (isset($SalesOrderDetail->quantity)) {
            $dtoSalesOrderDetail->quantity = $SalesOrderDetail->quantity;
        }

        if (isset($SalesOrderDetail->net_amount)) {
            $dtoSalesOrderDetail->net_amount = $SalesOrderDetail->net_amount;
        }

        if (isset($SalesOrderDetail->vat_amount)) {
            $dtoSalesOrderDetail->vat_amount = $SalesOrderDetail->vat_amount;
        }      

        if (isset($SalesOrderDetail->discount_percentage)) {
            $dtoSalesOrderDetail->discount_percentage = $SalesOrderDetail->discount_percentage;
        }

        if (isset($SalesOrderDetail->discount_value)) {
            $dtoSalesOrderDetail->discount_value = $SalesOrderDetail->discount_value;
        }

        if (isset($SalesOrderDetail->vat_type_id)) {
            $dtoSalesOrderDetail->vat_type_id = $SalesOrderDetail->vat_type_id;
        }      
        
        if (isset($SalesOrderDetail->total_amount)) {
            $dtoSalesOrderDetail->total_amount = $SalesOrderDetail->total_amount;
        }

        if (isset($SalesOrderDetail->note)) {
            $dtoSalesOrderDetail->note = $SalesOrderDetail->note;
        }

        if (isset($SalesOrderDetail->fulfillment_date)) {
            $dtoSalesOrderDetail->fulfillment_date = $SalesOrderDetail->fulfillment_date;
        }      

        if (isset($SalesOrderDetail->shipped_quantity)) {
            $dtoSalesOrderDetail->shipped_quantity = $SalesOrderDetail->shipped_quantity;
        }

        return $dtoSalesOrderDetail;

    }

    /**
     * Convert to SalesOrderDetail
     * 
     * @param DtoSalesOrderDetail dtoSalesOrderDetail
     * 
     * @return SalesOrderDetail
     */
    private static function _convertToModel($dtoSalesOrderDetail)
    {
        $SalesOrderDetail = new SalesOrderDetail();

        if (isset($dtoSalesOrderDetail->id)) {
            $SalesOrderDetail->id = $dtoSalesOrderDetail->id;
        }

        if (isset($dtoSalesOrderDetail->order_id)) {
            $SalesOrderDetail->order_id = $dtoSalesOrderDetail->order_id;
        }

        if (isset($dtoSalesOrderDetail->row_number)) {
            $SalesOrderDetail->row_number = $dtoSalesOrderDetail->row_number;
        }

        if (isset($dtoSalesOrderDetail->sales_order_row_type_id)) {
            $SalesOrderDetail->sales_order_row_type_id = $dtoSalesOrderDetail->sales_order_row_type_id;
        }

        if (isset($dtoSalesOrderDetail->order_detail_state_id)) {
            $SalesOrderDetail->order_detail_state_id = $dtoSalesOrderDetail->order_detail_state_id;
        }       
        
        if (isset($dtoSalesOrderDetail->item_id)) {
            $SalesOrderDetail->item_id = $dtoSalesOrderDetail->item_id;
        }

        if (isset($dtoSalesOrderDetail->description)) {
            $SalesOrderDetail->description = $dtoSalesOrderDetail->description;
        }

        if (isset($dtoSalesOrderDetail->unit_of_measure_id)) {
            $SalesOrderDetail->unit_of_measure_id = $dtoSalesOrderDetail->unit_of_measure_id;
        }

        if (isset($dtoSalesOrderDetail->unit_price)) {
            $SalesOrderDetail->unit_price = $dtoSalesOrderDetail->unit_price;
        }

        if (isset($dtoSalesOrderDetail->quantity)) {
            $SalesOrderDetail->quantity = $dtoSalesOrderDetail->quantity;
        }          

        if (isset($dtoSalesOrderDetail->net_amount)) {
            $SalesOrderDetail->net_amount = $dtoSalesOrderDetail->net_amount;
        }

        if (isset($dtoSalesOrderDetail->vat_amount)) {
            $SalesOrderDetail->vat_amount = $dtoSalesOrderDetail->vat_amount;
        }

        if (isset($dtoSalesOrderDetail->discount_percentage)) {
            $SalesOrderDetail->discount_percentage = $dtoSalesOrderDetail->discount_percentage;
        }         

        if (isset($dtoSalesOrderDetail->discount_value)) {
            $SalesOrderDetail->discount_value = $dtoSalesOrderDetail->discount_value;
        }     

        if (isset($dtoSalesOrderDetail->vat_type_id)) {
            $SalesOrderDetail->vat_type_id = $dtoSalesOrderDetail->vat_type_id;
        }     

        if (isset($dtoSalesOrderDetail->total_amount)) {
            $SalesOrderDetail->total_amount = $dtoSalesOrderDetail->total_amount;
        }         

        if (isset($dtoSalesOrderDetail->note)) {
            $SalesOrderDetail->note = $dtoSalesOrderDetail->note;
        }     

        if (isset($dtoSalesOrderDetail->fulfillment_date)) {
            $SalesOrderDetail->fulfillment_date = $dtoSalesOrderDetail->fulfillment_date;
        }         

        if (isset($dtoSalesOrderDetail->shipped_quantity)) {
            $SalesOrderDetail->shipped_quantity = $dtoSalesOrderDetail->shipped_quantity;
        }     

        return $SalesOrderDetail;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoSalesOrderDetail
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(SalesOrderDetail::find($id));
    }

    /**
     * Get all
     * 
     * @return DtoSalesOrderDetail
     */
    public static function getAllBySalesOrder()
    {
        $result = collect();
        foreach (DB::table('sales_orders_details')->where('order_id', $idCart)->orderBy('row_number')->get() as $orderRow) {
            $result->push(static::_convertToDto($orderRow));
        }
        
        return $result;
    }
   

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request dtoSalesOrderDetail
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $dtoSalesOrderDetail)
    {
        static::_validateData($dtoSalesOrderDetail, DbOperationsTypesEnum::INSERT);
        $SalesOrderDetail = static::_convertToModel($dtoSalesOrderDetail);
        $SalesOrderDetail->save();
        return $SalesOrderDetail->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoSalesOrderDetail
     * @param int idLanguage
     */
    public static function update(Request $dtoSalesOrderDetail)
    {
        static::_validateData($dtoSalesOrderDetail, DbOperationsTypesEnum::UPDATE);
        $SalesOrderDetail = SalesOrderDetail::find($dtoSalesOrderDetail->id);
        DBHelper::updateAndSave(static::_convertToModel($dtoSalesOrderDetail), $SalesOrderDetail);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id)
    {
        $SalesOrderDetail = SalesOrderDetail::find($id);

        $SalesOrderDetail->delete();
    }

    #endregion DELETE

}

<?php

namespace App\BusinessLogic;

use App\BookingAppointments;
use App\BookingConnectionAppointmentsCartDetail;
use App\BookingConnectionOptions;
use App\BookingServices;
use App\BookingSettings;
use App\Cart;
use App\CartDetail;
use App\CartDetailOptional;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoConnections;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\LanguageNation;
use App\LanguageProvince;
use App\User;
use App\Connections;
use App\Content;
use App\ContentLanguage;
use App\DtoModel\DtoBookingAppointments;
use App\DtoModel\DtoBookingConnectionAddDayHour;
use App\DtoModel\DtoBookingConnectionOptions;
use App\Employees;
use App\Message;
use App\Places;
use App\Exceptions;
use App\OptionalLanguages;
use App\OptionalTypology;
use App\TypeOptionalLanguages;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DateTime;
use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;

class ConnectionsBL
{

     #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request DtoPlaces
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoConnections, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoConnections->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Connections::find($DtoConnections->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }
    }

    /**
     * Convert to dto
     * 
     * @param Connections
     * 
     * @return DtoConnections
     */
    private static function _convertToDto($Connections)
    {
        $DtoConnections = new DtoConnections();

        if (isset($Connections->place_id)) {
            $DtoConnections->place_id = $Connections->place_id;
        }
        if (isset($Connections->service_id)) {
            $DtoConnections->service_id = $Connections->service_id;
        }
        if (isset($Connections->employee_id)) {
            $DtoConnections->employee_id = $Connections->employee_id;
        }
        if (isset($Connections->nr_place)) {
            $DtoConnections->nr_place = $Connections->nr_place;
        }
        if (isset($Connections->show_availability_calendar)) {
            $DtoConnections->show_availability_calendar = $Connections->show_availability_calendar;
        }

         if ($Connections->type_of_date == 1){
                $DtoConnections->check_fixed_date = $Connections->type_of_date; 
        }else {
                $DtoConnections->check_fixed_date = null;
         }
         
        if ($Connections->type_of_date == 2){
            $DtoConnections->check_datefrom_and_dateto = $Connections->type_of_date; 
        }else {
            $DtoConnections->check_datefrom_and_dateto = null;
        }
        if ($Connections->type_of_date == 3){
            $DtoConnections->check_repetition = $Connections->type_of_date; 
        }else {
            $DtoConnections->check_repetition = null;
        }
        
        if (isset($Connections->MONDAY)) {
            $DtoConnections->monday = $Connections->MONDAY;
        }
        if (isset($Connections->TUESDAY)) {
            $DtoConnections->tuesday = $Connections->TUESDAY;
        }
        if (isset($Connections->WEDNESDAY)) {
            $DtoConnections->wednesday = $Connections->WEDNESDAY;
        }
        if (isset($Connections->THURSDAY)) {
            $DtoConnections->thursday = $Connections->THURSDAY;
        }
        if (isset($Connections->FRIDAY)) {
            $DtoConnections->friday = $Connections->FRIDAY;
        }
        if (isset($Connections->SATURDAY)) {
            $DtoConnections->saturday = $Connections->SATURDAY;
        }
        if (isset($Connections->SUNDAY)) {
            $DtoConnections->sunday = $Connections->SUNDAY;
        }
        if (isset($Connections->exclude_holidays)) {
            $DtoConnections->exclude_holidays = $Connections->exclude_holidays;
        }
        if (isset($Connections->fixed_date)) {
            $DtoConnections->fixed_date = $Connections->fixed_date;
        }else{
            $DtoConnections->fixed_date = NULL;
        }
        if (isset($Connections->date_from)) {
            $DtoConnections->date_from = $Connections->date_from;
        }else{
            $DtoConnections->date_from = NULL;
        }
        if (isset($Connections->date_to)) {
            $DtoConnections->date_to = $Connections->date_to;
        }else{
            $DtoConnections->date_to = NULL;
        } 
        if (isset($Connections->start_time)) {
            $DtoConnections->start_time = $Connections->start_time;
        }else{
            $DtoConnections->start_time = NULL;
        }
        if (isset($Connections->end_time)) {
            $DtoConnections->end_time = $Connections->end_time;
        }else{
            $DtoConnections->end_time = NULL;
        }
        if (isset($Connections->start_time_pm)) {
            $DtoConnections->start_time_pm = substr($Connections->start_time_pm, 0, -3);
        }else{
            $DtoConnections->start_time_pm = NULL;
        }
        if (isset($Connections->end_time_pm)) {
            $DtoConnections->end_time_pm = substr($Connections->end_time_pm, 0, -3);
        }else{
            $DtoConnections->end_time_pm = NULL;
        }

        if (isset($Connections->description)) {
            $DtoConnections->description = $Connections->description;
        }
        if (isset($Connections->min_day)) {
            $DtoConnections->min_day = $Connections->min_day;
        }
        if (isset($Connections->check_multiple)) {
            $DtoConnections->check_multiple = $Connections->check_multiple;
        }
        return $DtoConnections;   
    }

    #endregion PRIVATE

    #region GET
    /**
     * Get by id
     * 
     * @param int id
     * @return DtoPlaces
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Connections::find($id));
    }

    /**
     * Get select
     *
     * @param String $search
     * @param int $idUser
     * 
     */
    public static function getSelect(string $search)
    {
                if ($search == "*") {
                    return Connections::select('id', 'name as description')->get();
                } else {
                    return Connections::where('name', 'ilike' , $search . '%')->select('id', 'name as description')->get();
          } 
    }


    /**
     * Get all
     * 
     * @return DtoPlaces
     */
    public static function getAll()
    {
        $result = collect();

        foreach (Connections::orderBy('id','DESC')->get() as $Connections) {    

                $DtoConnections = new DtoConnections();
            
                //table
                if (isset($Connections->id)) {
                    $DtoConnections->id = $Connections->id;
                }
                if (isset($Connections->place_id)) {
                    $DtoConnections->PlaceId = Places::where('id', $Connections->place_id)->first()->name;
                }
                if (isset($Connections->service_id)) {
                    $DtoConnections->ServiceId = BookingServices::where('id', $Connections->service_id)->first()->name;
                }
                if (isset($Connections->employee_id)) {
                    $DtoConnections->EmployeeId = Employees::where('id', $Connections->employee_id)->first()->name;
                }
                if (isset($Connections->nr_place)) {
                    $DtoConnections->NrPlace = $Connections->nr_place;
                }
                if (isset($Connections->show_availability_calendar)) {
                    $DtoConnections->ShowAvailabilityCalendar = $Connections->show_availability_calendar;
                }
                if (isset($Connections->fixed_date)) {
                    $DtoConnections->FixedDate = Carbon::parse($Connections->fixed_date)->format(HttpHelper::getLanguageDateFormat());
                    
                }
                if (isset($Connections->date_from)) {
                    $DtoConnections->DateFrom = Carbon::parse($Connections->date_from)->format(HttpHelper::getLanguageDateFormat());
                }
             
                if (isset($Connections->start_time)) {
                    $DtoConnections->StartTime = substr($Connections->start_time, 0, -3);
                    //str_replace(':00', '', );
                }
                if (isset($Connections->end_time)) {
                    $DtoConnections->EndTime = substr($Connections->end_time, 0, -3);
                    //str_replace(':00', '', $Connections->end_time);
                }

                //DETAIL
                if (isset($Connections->place_id)) {
                    $DtoConnections->place_id = $Connections->place_id;
                }
                if (isset($Connections->service_id)) {
                    $DtoConnections->service_id = $Connections->service_id;
                }
                if (isset($Connections->employee_id)) {
                    $DtoConnections->employee_id = $Connections->employee_id;
                }
                if (isset($Connections->nr_place)) {
                    $DtoConnections->nr_place = $Connections->nr_place;
                }
                if (isset($Connections->show_availability_calendar)) {
                    $DtoConnections->show_availability_calendar = $Connections->show_availability_calendar;
                }

                if ($Connections->type_of_date == 1){
                        $DtoConnections->check_fixed_date = $Connections->type_of_date; 
                }else {
                        $DtoConnections->check_fixed_date = null;
                 }
                 
                if ($Connections->type_of_date == 2){
                    $DtoConnections->check_datefrom_and_dateto = $Connections->type_of_date; 

                }else {
                    $DtoConnections->check_datefrom_and_dateto = null;
                }
                if ($Connections->type_of_date == 3){
                    $DtoConnections->check_repetition = $Connections->type_of_date; 
                }else {
                    $DtoConnections->check_repetition = null;
                }
                
                if (isset($Connections->MONDAY)) {
                    $DtoConnections->monday = $Connections->MONDAY;
                }
                if (isset($Connections->TUESDAY)) {
                    $DtoConnections->tuesday = $Connections->TUESDAY;
                }
                if (isset($Connections->WEDNESDAY)) {
                    $DtoConnections->wednesday = $Connections->WEDNESDAY;
                }
                if (isset($Connections->THURSDAY)) {
                    $DtoConnections->thursday = $Connections->THURSDAY;
                }
                if (isset($Connections->FRIDAY)) {
                    $DtoConnections->friday = $Connections->FRIDAY;
                }
                if (isset($Connections->SATURDAY)) {
                    $DtoConnections->saturday = $Connections->SATURDAY;
                }
                if (isset($Connections->SUNDAY)) {
                    $DtoConnections->sunday = $Connections->SUNDAY;
                }
                if (isset($Connections->exclude_holidays)) {
                    $DtoConnections->exclude_holidays = $Connections->exclude_holidays;
                }
                if (isset($Connections->fixed_date)) {
                    $DtoConnections->fixed_date = Carbon::parse($Connections->fixed_date)->format(HttpHelper::getLanguageDateFormat());
                }else{
                    $DtoConnections->fixed_date = NULL;
                }
                if (isset($Connections->date_from)) {
                    $DtoConnections->date_from = Carbon::parse($Connections->date_from)->format(HttpHelper::getLanguageDateFormat());
                }else{
                    $DtoConnections->date_from = NULL;
                }
                if (isset($Connections->date_to)) {
                    $DtoConnections->date_to = Carbon::parse($Connections->date_to)->format(HttpHelper::getLanguageDateFormat());
                }else{
                    $DtoConnections->date_to = NULL;
                } 
                if (isset($Connections->start_time)) {
                    $DtoConnections->start_time =  substr($Connections->start_time, 0, -3);
                    //str_replace(':00', '', $Connections->start_time);
                }else{
                    $DtoConnections->start_time = NULL;
                }
                if (isset($Connections->end_time)) {
                    $DtoConnections->end_time = substr($Connections->end_time, 0, -3);
                    //str_replace(':00', '', $Connections->end_time);
                }else{
                    $DtoConnections->end_time = NULL;
                }

                if (isset($Connections->start_time_pm)) {
                    $DtoConnections->start_time_pm = substr($Connections->start_time_pm, 0, -3);
                    //str_replace(':00', '', $Connections->end_time);
                }else{
                    $DtoConnections->start_time_pm = NULL;
                }

                if (isset($Connections->end_time_pm)) {
                    $DtoConnections->end_time_pm = substr($Connections->end_time_pm, 0, -3);
                    //str_replace(':00', '', $Connections->end_time);
                }else{
                    $DtoConnections->end_time_pm = NULL;
                }
                
                if (isset($Connections->description)) {
                    $DtoConnections->description = $Connections->description;
                }
                if (isset($Connections->check_multiple)) {
                    $DtoConnections->check_multiple = $Connections->check_multiple;
                }
                if (isset($Connections->min_day)) {
                    $DtoConnections->min_day = $Connections->min_day;
                }

                $BookingConnectionOptions = BookingConnectionOptions::where('connections_id', $Connections->id)->get();
                if (isset($BookingConnectionOptions)) {
                    foreach ($BookingConnectionOptions as $BookingConnectionOptionss) { 
                        $DtoBookingConnectionOptions = new DtoBookingConnectionOptions();
                        $DtoBookingConnectionOptions->id = $BookingConnectionOptionss['id'];
                        $DtoBookingConnectionOptions->connections_id = $Connections->id;

                        $DtoBookingConnectionOptions->optional_id = $BookingConnectionOptionss['optional_id'];
                        $tipeOptions = OptionalLanguages::where('optional_id', $BookingConnectionOptionss->optional_id)->first()->description;
                        $DtoBookingConnectionOptions->options = $tipeOptions;

                       // $DtoBookingConnectionOptions->optional_id = $BookingConnectionOptionss['optional_id'];
                       // $tipeOptions = TypeOptionalLanguages::where('type_optional_id', $BookingConnectionOptionss->optional_id)->first()->description;
                       // $DtoBookingConnectionOptions->options = $tipeOptions;

                        $DtoBookingConnectionOptions->cost = $BookingConnectionOptionss['cost']; 
                        $DtoBookingConnectionOptions->check_each = $BookingConnectionOptionss['check_each']; 
                        $DtoConnections->listOptions->push($DtoBookingConnectionOptions);
                    } 
                }
                
                $IdConnectionss = Connections::where('id', $Connections->id)->get();
                foreach ($IdConnectionss as $IdConnectionsss) { 

                        if ($IdConnectionsss->MONDAY == true) {
                            $DtoBookingConnectionAddDayHour = new DtoBookingConnectionAddDayHour();
                            $DtoBookingConnectionAddDayHour->monday_agg = true;
                            $DtoBookingConnectionAddDayHour->tuesday_agg = false;  
                            $DtoBookingConnectionAddDayHour->wednesday_agg = false;  
                            $DtoBookingConnectionAddDayHour->thursday_agg = false;  
                            $DtoBookingConnectionAddDayHour->friday_agg = false;  
                            $DtoBookingConnectionAddDayHour->saturday_agg = false;  
                            $DtoBookingConnectionAddDayHour->sunday_agg = false;

                            if (isset($IdConnectionsss->monday_start_time)) {
                                $DtoBookingConnectionAddDayHour->start_time_am_agg = $IdConnectionsss->monday_start_time;
                            }else{
                                $DtoBookingConnectionAddDayHour->start_time_am_agg = ''; 
                            }

                            if (isset($IdConnectionsss->monday_end_time)) {
                                $DtoBookingConnectionAddDayHour->end_time_am_agg = $IdConnectionsss->monday_end_time;
                            }else{
                                $DtoBookingConnectionAddDayHour->end_time_am_agg = ''; 
                            }
                            if (isset($IdConnectionsss->monday_start_time_pm)) {
                                $DtoBookingConnectionAddDayHour->start_time_pm_agg = $IdConnectionsss->monday_start_time_pm;
                            }else{
                                $DtoBookingConnectionAddDayHour->start_time_pm_agg = ''; 
                            }

                            if (isset($IdConnectionsss->monday_end_time_pm)) {
                                $DtoBookingConnectionAddDayHour->end_time_pm_agg = $IdConnectionsss->monday_end_time_pm;
                            }else{
                                $DtoBookingConnectionAddDayHour->end_time_pm_agg = ''; 
                            }

                            $DtoBookingConnectionAddDayHour->show_availability_calendar_agg = $IdConnectionsss->show_availability_calendar_monday;

                            if (isset($IdConnectionsss->monday_nr_place)) {
                                $DtoBookingConnectionAddDayHour->nr_place_agg = $IdConnectionsss->monday_nr_place;
                            }else{
                                $DtoBookingConnectionAddDayHour->nr_place_agg = ''; 
                            }
                            if (isset($IdConnectionsss->monday_nr_place)) {
                                $DtoBookingConnectionAddDayHour->nr_place_agg = $IdConnectionsss->monday_nr_place;
                            }else{
                                $DtoBookingConnectionAddDayHour->nr_place_agg = ''; 
                            }
                            if (isset($IdConnectionsss->hour_monday)) {
                                $DtoBookingConnectionAddDayHour->hour = $IdConnectionsss->hour_monday;
                            }else{
                                $DtoBookingConnectionAddDayHour->hour = ''; 
                            }
                            $DtoBookingConnectionAddDayHour->limit_reservations = $IdConnectionsss->limit_reservations_monday;

                            $DtoConnections->DetailAddDayHour->push($DtoBookingConnectionAddDayHour);
                        }

                        if ($IdConnectionsss->TUESDAY == true) {
                            $DtoBookingConnectionAddDayHour = new DtoBookingConnectionAddDayHour();
                            $DtoBookingConnectionAddDayHour->monday_agg = false;
                            $DtoBookingConnectionAddDayHour->tuesday_agg = true;  
                            $DtoBookingConnectionAddDayHour->wednesday_agg = false;  
                            $DtoBookingConnectionAddDayHour->thursday_agg = false;  
                            $DtoBookingConnectionAddDayHour->friday_agg = false;  
                            $DtoBookingConnectionAddDayHour->saturday_agg = false;  
                            $DtoBookingConnectionAddDayHour->sunday_agg = false;

                            if (isset($IdConnectionsss->tuesday_start_time)) {
                                $DtoBookingConnectionAddDayHour->start_time_am_agg = $IdConnectionsss->tuesday_start_time;
                            }else{
                                $DtoBookingConnectionAddDayHour->start_time_am_agg = '';
                            }
                            if (isset($IdConnectionsss->tuesday_end_time)) {
                                $DtoBookingConnectionAddDayHour->end_time_am_agg = $IdConnectionsss->tuesday_end_time;
                            }else{
                                $DtoBookingConnectionAddDayHour->end_time_am_agg = '';
                            }
                            if (isset($IdConnectionsss->tuesday_start_time_pm)) {
                                $DtoBookingConnectionAddDayHour->start_time_pm_agg = $IdConnectionsss->tuesday_start_time_pm;
                            }else{
                                $DtoBookingConnectionAddDayHour->start_time_pm_agg = '';
                            }
                            if (isset($IdConnectionsss->tuesday_end_time_pm)) {
                                $DtoBookingConnectionAddDayHour->end_time_pm_agg = $IdConnectionsss->tuesday_end_time_pm;
                            }else{
                                $DtoBookingConnectionAddDayHour->end_time_pm_agg = '';
                            }

                                $DtoBookingConnectionAddDayHour->show_availability_calendar_agg = $IdConnectionsss->show_availability_calendar_tuesday;
                        
                            if (isset($IdConnectionsss->tuesday_nr_place)) {
                                $DtoBookingConnectionAddDayHour->nr_place_agg = $IdConnectionsss->tuesday_nr_place;
                            }else{
                                $DtoBookingConnectionAddDayHour->nr_place_agg = '';
                            }
                            if (isset($IdConnectionsss->hour_tuesday)) {
                                $DtoBookingConnectionAddDayHour->hour = $IdConnectionsss->hour_tuesday;
                            }else{
                                $DtoBookingConnectionAddDayHour->hour = ''; 
                            }
                            $DtoBookingConnectionAddDayHour->limit_reservations = $IdConnectionsss->limit_reservations_tuesday;

                            $DtoConnections->DetailAddDayHour->push($DtoBookingConnectionAddDayHour);
                        }

                        if ($IdConnectionsss->WEDNESDAY == true) {
                            $DtoBookingConnectionAddDayHour = new DtoBookingConnectionAddDayHour();
                            $DtoBookingConnectionAddDayHour->monday_agg = false;
                            $DtoBookingConnectionAddDayHour->tuesday_agg = false;  
                            $DtoBookingConnectionAddDayHour->wednesday_agg = true;  
                            $DtoBookingConnectionAddDayHour->thursday_agg = false;  
                            $DtoBookingConnectionAddDayHour->friday_agg = false;  
                            $DtoBookingConnectionAddDayHour->saturday_agg = false;  
                            $DtoBookingConnectionAddDayHour->sunday_agg = false;

                            if (isset($IdConnectionsss->wednesday_start_time)) {
                                $DtoBookingConnectionAddDayHour->start_time_am_agg = $IdConnectionsss->wednesday_start_time;
                            }else{
                                $DtoBookingConnectionAddDayHour->start_time_am_agg = '';
                            }
                            if (isset($IdConnectionsss->wednesday_end_time)) {
                                $DtoBookingConnectionAddDayHour->end_time_am_agg = $IdConnectionsss->wednesday_end_time;
                            }else{
                                $DtoBookingConnectionAddDayHour->end_time_am_agg = '';
                            }
                            if (isset($IdConnectionsss->wednesday_start_time_pm)) {
                                $DtoBookingConnectionAddDayHour->start_time_pm_agg = $IdConnectionsss->wednesday_start_time_pm;
                            }else{
                                $DtoBookingConnectionAddDayHour->start_time_pm_agg = '';
                            }
                            if (isset($IdConnectionsss->wednesday_end_time_pm)) {
                                $DtoBookingConnectionAddDayHour->end_time_pm_agg = $IdConnectionsss->wednesday_end_time_pm;
                            }else{
                                $DtoBookingConnectionAddDayHour->end_time_pm_agg = '';
                            }

                            $DtoBookingConnectionAddDayHour->show_availability_calendar_agg = $IdConnectionsss->show_availability_calendar_wednesday;

                            if (isset($IdConnectionsss->wednesday_nr_place)) {
                                $DtoBookingConnectionAddDayHour->nr_place_agg = $IdConnectionsss->wednesday_nr_place;
                            }else{
                                $DtoBookingConnectionAddDayHour->nr_place_agg = '';
                            }

                            if (isset($IdConnectionsss->hour_wednesday)) {
                                $DtoBookingConnectionAddDayHour->hour = $IdConnectionsss->hour_wednesday;
                            }else{
                                $DtoBookingConnectionAddDayHour->hour = ''; 
                            }
                            $DtoBookingConnectionAddDayHour->limit_reservations = $IdConnectionsss->limit_reservations_wednesday;

                            $DtoConnections->DetailAddDayHour->push($DtoBookingConnectionAddDayHour);
                        
                        }

                        if ($IdConnectionsss->THURSDAY == true) {
                            $DtoBookingConnectionAddDayHour = new DtoBookingConnectionAddDayHour();
                            $DtoBookingConnectionAddDayHour->monday_agg = false;
                            $DtoBookingConnectionAddDayHour->tuesday_agg = false;  
                            $DtoBookingConnectionAddDayHour->wednesday_agg = false;  
                            $DtoBookingConnectionAddDayHour->thursday_agg = true;  
                            $DtoBookingConnectionAddDayHour->friday_agg = false;  
                            $DtoBookingConnectionAddDayHour->saturday_agg = false;  
                            $DtoBookingConnectionAddDayHour->sunday_agg = false;

                            if (isset($IdConnectionsss->thursday_start_time)) {
                                $DtoBookingConnectionAddDayHour->start_time_am_agg = $IdConnectionsss->thursday_start_time;
                            }else{
                                $DtoBookingConnectionAddDayHour->start_time_am_agg = '';
                            }
                            if (isset($IdConnectionsss->thursday_end_time)) {
                                $DtoBookingConnectionAddDayHour->end_time_am_agg = $IdConnectionsss->thursday_end_time;
                            }else{
                                $DtoBookingConnectionAddDayHour->end_time_am_agg = '';
                            }
                            if (isset($IdConnectionsss->thursday_start_time_pm)) {
                                $DtoBookingConnectionAddDayHour->start_time_pm_agg = $IdConnectionsss->thursday_start_time_pm;
                            }else{
                                $DtoBookingConnectionAddDayHour->start_time_pm_agg = '';
                            }
                            if (isset($IdConnectionsss->thursday_end_time_pm)) {
                                $DtoBookingConnectionAddDayHour->end_time_pm_agg = $IdConnectionsss->thursday_end_time_pm;
                            }else{
                                $DtoBookingConnectionAddDayHour->end_time_pm_agg = '';
                            }
                            $DtoBookingConnectionAddDayHour->show_availability_calendar_agg = $IdConnectionsss->show_availability_calendar_thursday;

                            if (isset($IdConnectionsss->thursday_nr_place)) {
                                $DtoBookingConnectionAddDayHour->nr_place_agg = $IdConnectionsss->thursday_nr_place;
                            }else{
                                $DtoBookingConnectionAddDayHour->nr_place_agg = '';
                            }
                            if (isset($IdConnectionsss->hour_thursday)) {
                                $DtoBookingConnectionAddDayHour->hour = $IdConnectionsss->hour_thursday;
                            }else{
                                $DtoBookingConnectionAddDayHour->hour = ''; 
                            }
                            $DtoBookingConnectionAddDayHour->limit_reservations = $IdConnectionsss->limit_reservations_thursday;

                            $DtoConnections->DetailAddDayHour->push($DtoBookingConnectionAddDayHour);
                        }

                        if ($IdConnectionsss->FRIDAY == true) {
                            $DtoBookingConnectionAddDayHour = new DtoBookingConnectionAddDayHour();   
                            $DtoBookingConnectionAddDayHour->monday_agg = false;
                            $DtoBookingConnectionAddDayHour->tuesday_agg = false;  
                            $DtoBookingConnectionAddDayHour->wednesday_agg = false;  
                            $DtoBookingConnectionAddDayHour->thursday_agg = false;  
                            $DtoBookingConnectionAddDayHour->friday_agg = true;  
                            $DtoBookingConnectionAddDayHour->saturday_agg = false;  
                            $DtoBookingConnectionAddDayHour->sunday_agg = false;


                            if (isset($IdConnectionsss->friday_start_time)) {
                                $DtoBookingConnectionAddDayHour->start_time_am_agg = $IdConnectionsss->friday_start_time;
                            }else{
                                $DtoBookingConnectionAddDayHour->start_time_am_agg = '';
                            }
                            if (isset($IdConnectionsss->friday_end_time)) {
                                $DtoBookingConnectionAddDayHour->end_time_am_agg = $IdConnectionsss->friday_end_time;
                            }else{
                                $DtoBookingConnectionAddDayHour->end_time_am_agg = '';
                            }
                            if (isset($IdConnectionsss->friday_start_time_pm)) {
                                $DtoBookingConnectionAddDayHour->start_time_pm_agg = $IdConnectionsss->friday_start_time_pm;
                            }else{
                                $DtoBookingConnectionAddDayHour->start_time_pm_agg = '';
                            }
                            if (isset($IdConnectionsss->friday_end_time_pm)) {
                                $DtoBookingConnectionAddDayHour->end_time_pm_agg = $IdConnectionsss->friday_end_time_pm;
                            }else{
                                $DtoBookingConnectionAddDayHour->end_time_pm_agg = '';
                            }
                            
                            $DtoBookingConnectionAddDayHour->show_availability_calendar_agg = $IdConnectionsss->show_availability_calendar_friday;

                            if (isset($IdConnectionsss->friday_nr_place)) {
                                $DtoBookingConnectionAddDayHour->nr_place_agg = $IdConnectionsss->friday_nr_place;
                            }else{
                                $DtoBookingConnectionAddDayHour->nr_place_agg = '';
                            }
                            if (isset($IdConnectionsss->hour_friday)) {
                                $DtoBookingConnectionAddDayHour->hour = $IdConnectionsss->hour_friday;
                            }else{
                                $DtoBookingConnectionAddDayHour->hour = ''; 
                            }
                            $DtoBookingConnectionAddDayHour->limit_reservations = $IdConnectionsss->limit_reservations_friday;

                            $DtoConnections->DetailAddDayHour->push($DtoBookingConnectionAddDayHour);
                            
                        }

                        if ($IdConnectionsss->SATURDAY == true) {
                            $DtoBookingConnectionAddDayHour = new DtoBookingConnectionAddDayHour();
                            $DtoBookingConnectionAddDayHour->monday_agg = false;
                            $DtoBookingConnectionAddDayHour->tuesday_agg = false;  
                            $DtoBookingConnectionAddDayHour->wednesday_agg = false;  
                            $DtoBookingConnectionAddDayHour->thursday_agg = false;  
                            $DtoBookingConnectionAddDayHour->friday_agg = false;  
                            $DtoBookingConnectionAddDayHour->saturday_agg = true;  
                            $DtoBookingConnectionAddDayHour->sunday_agg = false;


                            if (isset($IdConnectionsss->saturday_start_time)) {
                                $DtoBookingConnectionAddDayHour->start_time_am_agg = $IdConnectionsss->saturday_start_time;
                            }else{
                                $DtoBookingConnectionAddDayHour->start_time_am_agg = '';
                            }
                            if (isset($IdConnectionsss->saturday_end_time)) {
                                $DtoBookingConnectionAddDayHour->end_time_am_agg = $IdConnectionsss->saturday_end_time;
                            }else{
                                $DtoBookingConnectionAddDayHour->end_time_am_agg = '';
                            }
                            if (isset($IdConnectionsss->saturday_start_time_pm)) {
                                $DtoBookingConnectionAddDayHour->start_time_pm_agg = $IdConnectionsss->saturday_start_time_pm;
                            }else{
                                $DtoBookingConnectionAddDayHour->start_time_pm_agg = '';
                            }
                            if (isset($IdConnectionsss->saturday_end_time_pm)) {
                                $DtoBookingConnectionAddDayHour->end_time_pm_agg = $IdConnectionsss->saturday_end_time_pm;
                            }else{
                                $DtoBookingConnectionAddDayHour->end_time_pm_agg = '';
                            }

                            $DtoBookingConnectionAddDayHour->show_availability_calendar_agg = $IdConnectionsss->show_availability_calendar_saturday;

                            if (isset($IdConnectionsss->saturday_nr_place)) {
                                $DtoBookingConnectionAddDayHour->nr_place_agg = $IdConnectionsss->saturday_nr_place;
                            }else{
                                $DtoBookingConnectionAddDayHour->nr_place_agg = '';
                            }
                            if (isset($IdConnectionsss->hour_saturday)) {
                                $DtoBookingConnectionAddDayHour->hour = $IdConnectionsss->hour_saturday;
                            }else{
                                $DtoBookingConnectionAddDayHour->hour = ''; 
                            }
                            $DtoBookingConnectionAddDayHour->limit_reservations = $IdConnectionsss->limit_reservations_saturday;

                            $DtoConnections->DetailAddDayHour->push($DtoBookingConnectionAddDayHour);
                        }

                        if ($IdConnectionsss->SUNDAY == true) {
                            $DtoBookingConnectionAddDayHour = new DtoBookingConnectionAddDayHour();
                            $DtoBookingConnectionAddDayHour->monday_agg = false;
                            $DtoBookingConnectionAddDayHour->tuesday_agg = false;  
                            $DtoBookingConnectionAddDayHour->wednesday_agg = false;  
                            $DtoBookingConnectionAddDayHour->thursday_agg = false;  
                            $DtoBookingConnectionAddDayHour->friday_agg = false;  
                            $DtoBookingConnectionAddDayHour->saturday_agg = false;  
                            $DtoBookingConnectionAddDayHour->sunday_agg = true;

                            if (isset($IdConnectionsss->sunday_start_time)) {
                                $DtoBookingConnectionAddDayHour->start_time_am_agg = $IdConnectionsss->sunday_start_time;
                            }else{
                                $DtoBookingConnectionAddDayHour->start_time_am_agg = '';
                            }
                            if (isset($IdConnectionsss->sunday_end_time)) {
                                $DtoBookingConnectionAddDayHour->end_time_am_agg = $IdConnectionsss->sunday_end_time;
                            }else{
                                $DtoBookingConnectionAddDayHour->end_time_am_agg = '';
                            }
                            if (isset($IdConnectionsss->sunday_start_time_pm)) {
                                $DtoBookingConnectionAddDayHour->start_time_pm_agg = $IdConnectionsss->sunday_start_time_pm;
                            }else{
                                $DtoBookingConnectionAddDayHour->start_time_pm_agg = '';
                            }
                            if (isset($IdConnectionsss->sunday_end_time_pm)) {
                                $DtoBookingConnectionAddDayHour->end_time_pm_agg = $IdConnectionsss->sunday_end_time_pm;
                            }else{
                                $DtoBookingConnectionAddDayHour->end_time_pm_agg = '';
                            }
                            $DtoBookingConnectionAddDayHour->show_availability_calendar_agg = $IdConnectionsss->show_availability_calendar_sunday;

                            if (isset($IdConnectionsss->sunday_nr_place)) {
                                $DtoBookingConnectionAddDayHour->nr_place_agg = $IdConnectionsss->sunday_nr_place;
                            }else{
                                $DtoBookingConnectionAddDayHour->nr_place_agg = '';
                            }
                            if (isset($IdConnectionsss->hour_sunday)) {
                                $DtoBookingConnectionAddDayHour->hour = $IdConnectionsss->hour_sunday;
                            }else{
                                $DtoBookingConnectionAddDayHour->hour = ''; 
                            }
                            $DtoBookingConnectionAddDayHour->limit_reservations = $IdConnectionsss->limit_reservations_sunday;

                            $DtoConnections->DetailAddDayHour->push($DtoBookingConnectionAddDayHour);
                        }
                }
                    $result->push($DtoConnections);
            }
            return $result;

    }
    #endregion GET

    #region INSERT
    /**
     * Insert
     * 
     * @param Request DtoConnections
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoConnections, int $idUser)
    {
       DB::beginTransaction();
        try {
            $Connections = new Connections();
            
        if (isset($DtoConnections->place_id)) {
            $Connections->place_id = $DtoConnections->place_id;
        } 
         if (isset($DtoConnections->service_id)) {
            $Connections->service_id = $DtoConnections->service_id;
        } 
         if (isset($DtoConnections->employee_id)) {
            $Connections->employee_id = $DtoConnections->employee_id;
        } 
        if (isset($DtoConnections->nr_place)) {
            $Connections->nr_place = $DtoConnections->nr_place;
        }else{
            $Connections->nr_place = NULL;
        } 
        if (isset($DtoConnections->show_availability_calendar)) {
            $Connections->show_availability_calendar = $DtoConnections->show_availability_calendar;
        } 
        if (isset($DtoConnections->description)) {
            $Connections->description = $DtoConnections->description;
        }else{
            $Connections->description = NULL;
        }  

        //
        if (isset($DtoConnections->check_fixed_date)) {
            $Connections->type_of_date = $DtoConnections->check_fixed_date;
        } 
        if (isset($DtoConnections->check_repetition)) {
            $Connections->type_of_date = $DtoConnections->check_repetition;
        } 
        if (isset($DtoConnections->check_datefrom_and_dateto)) {
            $Connections->type_of_date = $DtoConnections->check_datefrom_and_dateto;
        } 
     
        if (isset($DtoConnections->monday)) {
            $Connections->MONDAY = $DtoConnections->monday;
        }
        if (isset($DtoConnections->tuesday)) {
            $Connections->TUESDAY = $DtoConnections->tuesday;
        } 
        if (isset($DtoConnections->wednesday)) {
            $Connections->WEDNESDAY = $DtoConnections->wednesday;
        }
        if (isset($DtoConnections->thursday)) {
            $Connections->THURSDAY = $DtoConnections->thursday;
        }
        if (isset($DtoConnections->friday)) {
            $Connections->FRIDAY = $DtoConnections->friday;
        }
        if (isset($DtoConnections->saturday)) {
            $Connections->SATURDAY = $DtoConnections->saturday;
        }
        if (isset($DtoConnections->sunday)) {
            $Connections->SUNDAY = $DtoConnections->sunday;
        }
        if (isset($DtoConnections->exclude_holidays)) {
            $Connections->exclude_holidays = $DtoConnections->exclude_holidays;
        }
        if (isset($DtoConnections->fixed_date)) {
            $Connections->fixed_date = $DtoConnections->fixed_date;
        }else{
            $Connections->fixed_date = NULL;
        }
        if (isset($DtoConnections->date_from)) {
            $Connections->date_from = $DtoConnections->date_from;
        }else{
            $Connections->date_from = NULL;
        }
        if (isset($DtoConnections->date_to)) {
            $Connections->date_to = $DtoConnections->date_to;
        }else{
            $Connections->date_to = NULL;
        }
       if (isset($DtoConnections->start_time)) {
            $Connections->start_time = $DtoConnections->start_time;
        }else{
            $Connections->start_time = NULL;
        }
        if (isset($DtoConnections->end_time)) {
            $Connections->end_time = $DtoConnections->end_time;
        }else{
            $Connections->end_time = NULL;
        }
        if (isset($DtoConnections->end_time_pm)) {
            $Connections->end_time_pm = $DtoConnections->end_time_pm;
        }else{
            $Connections->end_time_pm = NULL;
        }
        if (isset($DtoConnections->start_time_pm)) {
            $Connections->start_time_pm = $DtoConnections->start_time_pm;
        }else{
            $Connections->start_time_pm = NULL;
        }
        
        if (isset($DtoConnections->min_day)) {
            $Connections->min_day = $DtoConnections->min_day;
        }else{
            $Connections->min_day = NULL;
        }
        if (isset($DtoConnections->check_multiple)) {
            $Connections->check_multiple = $DtoConnections->check_multiple;
        }
        $Connections->save();

        foreach ($DtoConnections->DetailOption as $res) {
            if (!is_null($res['optional_id'])){
                $BookingConnectionOptions = new BookingConnectionOptions();
                $BookingConnectionOptions->connections_id = $DtoConnections->id;
               /* $BookingConnectionOptions->optional_id = $res['optional_id'];
                $BookingConnectionOptions->options = TypeOptionalLanguages::where('type_optional_id', $res['optional_id'])->first()->description;*/

               /* $BookingConnectionOptions->optional_id = OptionalTypology::where('optional_id', $res['optional_id'])->first()->optional_id;
                $Optional_Typology = OptionalTypology::where('optional_id', $res['optional_id'])->first();
                $BookingConnectionOptions->options = OptionalLanguages::where('optional_id', $Optional_Typology->optional_id)->first()->description;*/

                $BookingConnectionOptions->optional_id = OptionalTypology::where('type_optional_id', $res['optional_id'])->first()->optional_id;
                $Optional_Typology = OptionalTypology::where('type_optional_id', $res['optional_id'])->first();
                $BookingConnectionOptions->options = OptionalLanguages::where('optional_id', $Optional_Typology->optional_id)->first()->description;
                
                $BookingConnectionOptions->cost = $res['cost'];
                $BookingConnectionOptions->check_each = $res['check_each'];
                $BookingConnectionOptions->save();
            }
        }

        foreach ($DtoConnections->DetailAddDayHour as $resDetailAddDayHour) {

            $IdConnectionss = Connections::where('id', $Connections->id)->get()->first();

            if ($resDetailAddDayHour['monday_agg'] == true) {
                $IdConnectionss->MONDAY = $resDetailAddDayHour['monday_agg'];
    
              if (isset($resDetailAddDayHour['start_time_am_agg'])) {
                    $IdConnectionss->monday_start_time = $resDetailAddDayHour['start_time_am_agg'];
                }else{
                    $IdConnectionss->monday_start_time = NULL;
                }
                if (isset($resDetailAddDayHour['end_time_am_agg'])) {
                    $IdConnectionss->monday_end_time = $resDetailAddDayHour['end_time_am_agg'];
                }else{
                    $IdConnectionss->monday_end_time = NULL;
                }
                if (isset($resDetailAddDayHour['start_time_pm_agg'])) {
                    $IdConnectionss->monday_start_time_pm = $resDetailAddDayHour['start_time_pm_agg'];
                }else{
                    $IdConnectionss->monday_start_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['end_time_pm_agg'])) {
                    $IdConnectionss->monday_end_time_pm = $resDetailAddDayHour['end_time_pm_agg'];
                }else{
                    $IdConnectionss->monday_end_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['show_availability_calendar_agg'])) {
                    $IdConnectionss->show_availability_calendar_monday = $resDetailAddDayHour['show_availability_calendar_agg'];
                }else{
                    $IdConnectionss->show_availability_calendar_monday = false;
                } 
                if (isset($resDetailAddDayHour['nr_place_agg'])) {
                    $IdConnectionss->monday_nr_place = $resDetailAddDayHour['nr_place_agg'];
                }else{
                    $IdConnectionss->monday_nr_place = '';
                }  
                if (isset($resDetailAddDayHour['hour'])) {
                    $IdConnectionss->hour_monday = $resDetailAddDayHour['hour'];
                }else{
                    $IdConnectionss->hour_monday = NULL;
                } 
                if (isset($resDetailAddDayHour['limit_reservations'])) {
                    $IdConnectionss->limit_reservations_monday = $resDetailAddDayHour['limit_reservations'];
                }else{
                    $IdConnectionss->limit_reservations_monday = false;
                } 

                $IdConnectionss->save();
            }

            if ($resDetailAddDayHour['tuesday_agg'] == true){
                $IdConnectionss->TUESDAY = $resDetailAddDayHour['tuesday_agg'];

                if (isset($resDetailAddDayHour['start_time_am_agg'])) {
                    $IdConnectionss->tuesday_start_time = $resDetailAddDayHour['start_time_am_agg'];
                }else{
                    $IdConnectionss->tuesday_start_time = NULL;
                }
                if (isset($resDetailAddDayHour['end_time_am_agg'])) {
                    $IdConnectionss->tuesday_end_time = $resDetailAddDayHour['end_time_am_agg'];
                }else{
                    $IdConnectionss->tuesday_end_time = NULL;
                }
                if (isset($resDetailAddDayHour['start_time_pm_agg'])) {
                    $IdConnectionss->tuesday_start_time_pm = $resDetailAddDayHour['start_time_pm_agg'];
                }else{
                    $IdConnectionss->tuesday_start_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['end_time_pm_agg'])) {
                    $IdConnectionss->tuesday_end_time_pm = $resDetailAddDayHour['end_time_pm_agg'];
                }else{
                    $IdConnectionss->tuesday_end_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['show_availability_calendar_agg'])) {
                    $IdConnectionss->show_availability_calendar_tuesday= $resDetailAddDayHour['show_availability_calendar_agg'];
                }else{
                    $IdConnectionss->show_availability_calendar_tuesday = false;
                } 
                if (isset($resDetailAddDayHour['nr_place_agg'])) {
                    $IdConnectionss->tuesday_nr_place = $resDetailAddDayHour['nr_place_agg'];
                }else{
                    $IdConnectionss->tuesday_nr_place = '';
                }  
                if (isset($resDetailAddDayHour['hour'])) {
                    $IdConnectionss->hour_tuesday = $resDetailAddDayHour['hour'];
                }else{
                    $IdConnectionss->hour_tuesday = NULL;
                } 
                if (isset($resDetailAddDayHour['limit_reservations'])) {
                    $IdConnectionss->limit_reservations_tuesday = $resDetailAddDayHour['limit_reservations'];
                }else{
                    $IdConnectionss->limit_reservations_tuesday = false;
                } 
                $IdConnectionss->save();
            }

            if ($resDetailAddDayHour['wednesday_agg'] == true){
                $IdConnectionss->WEDNESDAY = $resDetailAddDayHour['wednesday_agg'];

                if (isset($resDetailAddDayHour['start_time_am_agg'])) {
                    $IdConnectionss->wednesday_start_time = $resDetailAddDayHour['start_time_am_agg'];
                }else{
                    $IdConnectionss->wednesday_start_time = NULL;
                }
                if (isset($resDetailAddDayHour['end_time_am_agg'])) {
                    $IdConnectionss->wednesday_end_time = $resDetailAddDayHour['end_time_am_agg'];
                }else{
                    $IdConnectionss->wednesday_end_time = NULL;
                }
                if (isset($resDetailAddDayHour['start_time_pm_agg'])) {
                    $IdConnectionss->wednesday_start_time_pm = $resDetailAddDayHour['start_time_pm_agg'];
                }else{
                    $IdConnectionss->wednesday_start_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['end_time_pm_agg'])) {
                    $IdConnectionss->wednesday_end_time_pm = $resDetailAddDayHour['end_time_pm_agg'];
                }else{
                    $IdConnectionss->wednesday_end_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['show_availability_calendar_agg'])) {
                    $IdConnectionss->show_availability_calendar_wednesday= $resDetailAddDayHour['show_availability_calendar_agg'];
                }else{
                    $IdConnectionss->show_availability_calendar_wednesday = false;
                } 
                if (isset($resDetailAddDayHour['nr_place_agg'])) {
                    $IdConnectionss->wednesday_nr_place = $resDetailAddDayHour['nr_place_agg'];
                }else{
                    $IdConnectionss->wednesday_nr_place = '';
                }  
                if (isset($resDetailAddDayHour['hour'])) {
                    $IdConnectionss->hour_wednesday = $resDetailAddDayHour['hour'];
                }else{
                    $IdConnectionss->hour_wednesday = NULL;
                } 
                if (isset($resDetailAddDayHour['limit_reservations'])) {
                    $IdConnectionss->limit_reservations_wednesday = $resDetailAddDayHour['limit_reservations'];
                }else{
                    $IdConnectionss->limit_reservations_wednesday = false;
                } 
                $IdConnectionss->save();
            }

            if ($resDetailAddDayHour['thursday_agg'] == true){
                $IdConnectionss->THURSDAY = $resDetailAddDayHour['thursday_agg'];

                if (isset($resDetailAddDayHour['start_time_am_agg'])) {
                    $IdConnectionss->thursday_start_time = $resDetailAddDayHour['start_time_am_agg'];
                }else{
                    $IdConnectionss->thursday_start_time = NULL;
                }
                if (isset($resDetailAddDayHour['end_time_am_agg'])) {
                    $IdConnectionss->thursday_end_time = $resDetailAddDayHour['end_time_am_agg'];
                }else{
                    $IdConnectionss->thursday_end_time = NULL;
                }
                if (isset($resDetailAddDayHour['start_time_pm_agg'])) {
                    $IdConnectionss->thursday_start_time_pm = $resDetailAddDayHour['start_time_pm_agg'];
                }else{
                    $IdConnectionss->thursday_start_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['end_time_pm_agg'])) {
                    $IdConnectionss->thursday_end_time_pm = $resDetailAddDayHour['end_time_pm_agg'];
                }else{
                    $IdConnectionss->thursday_end_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['show_availability_calendar_agg'])) {
                    $IdConnectionss->show_availability_calendar_thursday = $resDetailAddDayHour['show_availability_calendar_agg'];
                }else{
                    $IdConnectionss->show_availability_calendar_thursday = false;
                } 
                if (isset($resDetailAddDayHour['nr_place_agg'])) {
                    $IdConnectionss->thursday_nr_place = $resDetailAddDayHour['nr_place_agg'];
                }else{
                    $IdConnectionss->thursday_nr_place = '';
                } 
                if (isset($resDetailAddDayHour['hour'])) {
                    $IdConnectionss->hour_thursday = $resDetailAddDayHour['hour'];
                }else{
                    $IdConnectionss->hour_thursday = NULL;
                } 
                if (isset($resDetailAddDayHour['limit_reservations'])) {
                    $IdConnectionss->limit_reservations_thursday = $resDetailAddDayHour['limit_reservations'];
                }else{
                    $IdConnectionss->limit_reservations_thursday = false;
                } 
                $IdConnectionss->save();
           
            }

            if ($resDetailAddDayHour['friday_agg'] == true){
                $IdConnectionss->FRIDAY = $resDetailAddDayHour['friday_agg'];

                if (isset($resDetailAddDayHour['start_time_am_agg'])) {
                    $IdConnectionss->friday_start_time = $resDetailAddDayHour['start_time_am_agg'];
                }else{
                    $IdConnectionss->friday_start_time = NULL;
                }
                if (isset($resDetailAddDayHour['end_time_am_agg'])) {
                    $IdConnectionss->friday_end_time = $resDetailAddDayHour['end_time_am_agg'];
                }else{
                    $IdConnectionss->friday_end_time = NULL;
                }
                if (isset($resDetailAddDayHour['start_time_pm_agg'])) {
                    $IdConnectionss->friday_start_time_pm = $resDetailAddDayHour['start_time_pm_agg'];
                }else{
                    $IdConnectionss->friday_start_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['end_time_pm_agg'])) {
                    $IdConnectionss->friday_end_time_pm = $resDetailAddDayHour['end_time_pm_agg'];
                }else{
                    $IdConnectionss->friday_end_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['show_availability_calendar_agg'])) {
                    $IdConnectionss->show_availability_calendar_friday = $resDetailAddDayHour['show_availability_calendar_agg'];
                }else{
                    $IdConnectionss->show_availability_calendar_friday= false;
                } 
                if (isset($resDetailAddDayHour['nr_place_agg'])) {
                    $IdConnectionss->friday_nr_place = $resDetailAddDayHour['nr_place_agg'];
                }else{
                    $IdConnectionss->friday_nr_place = '';
                }  
                if (isset($resDetailAddDayHour['hour'])) {
                    $IdConnectionss->hour_friday = $resDetailAddDayHour['hour'];
                }else{
                    $IdConnectionss->hour_friday = NULL;
                } 
                if (isset($resDetailAddDayHour['limit_reservations'])) {
                    $IdConnectionss->limit_reservations_friday = $resDetailAddDayHour['limit_reservations'];
                }else{
                    $IdConnectionss->limit_reservations_friday = false;
                } 
                $IdConnectionss->save();
             
            }

            if ($resDetailAddDayHour['saturday_agg'] == true){
                $IdConnectionss->SATURDAY = $resDetailAddDayHour['saturday_agg'];

                if (isset($resDetailAddDayHour['start_time_am_agg'])) {
                    $IdConnectionss->saturday_start_time = $resDetailAddDayHour['start_time_am_agg'];
                }else{
                    $IdConnectionss->saturday_start_time = NULL;
                }
                if (isset($resDetailAddDayHour['end_time_am_agg'])) {
                    $IdConnectionss->saturday_end_time = $resDetailAddDayHour['end_time_am_agg'];
                }else{
                    $IdConnectionss->saturday_end_time = NULL;
                }
                if (isset($resDetailAddDayHour['start_time_pm_agg'])) {
                    $IdConnectionss->saturday_start_time_pm = $resDetailAddDayHour['start_time_pm_agg'];
                }else{
                    $IdConnectionss->saturday_start_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['end_time_pm_agg'])) {
                    $IdConnectionss->saturday_end_time_pm = $resDetailAddDayHour['end_time_pm_agg'];
                }else{
                    $IdConnectionss->saturday_end_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['show_availability_calendar_agg'])) {
                    $IdConnectionss->show_availability_calendar_saturday = $resDetailAddDayHour['show_availability_calendar_agg'];
                }else{
                    $IdConnectionss->show_availability_calendar_saturday = false;
                } 
                if (isset($resDetailAddDayHour['nr_place_agg'])) {
                    $IdConnectionss->saturday_nr_place = $resDetailAddDayHour['nr_place_agg'];
                }else{
                    $IdConnectionss->saturday_nr_place = '';
                }  
                if (isset($resDetailAddDayHour['hour'])) {
                    $IdConnectionss->hour_saturday = $resDetailAddDayHour['hour'];
                }else{
                    $IdConnectionss->hour_saturday = NULL;
                } 
                if (isset($resDetailAddDayHour['limit_reservations'])) {
                    $IdConnectionss->limit_reservations_saturday = $resDetailAddDayHour['limit_reservations'];
                }else{
                    $IdConnectionss->limit_reservations_saturday = false;
                } 
                $IdConnectionss->save();
                
            }

            if ($resDetailAddDayHour['sunday_agg'] == true){
                $IdConnectionss->SUNDAY = $resDetailAddDayHour['sunday_agg'];

                if (isset($resDetailAddDayHour['start_time_am_agg'])) {
                    $IdConnectionss->sunday_start_time = $resDetailAddDayHour['start_time_am_agg'];
                }else{
                    $IdConnectionss->sunday_start_time = NULL;
                }
                if (isset($resDetailAddDayHour['end_time_am_agg'])) {
                    $IdConnectionss->sunday_end_time = $resDetailAddDayHour['end_time_am_agg'];
                }else{
                    $IdConnectionss->sunday_end_time = NULL;
                }
                if (isset($resDetailAddDayHour['start_time_pm_agg'])) {
                    $IdConnectionss->sunday_start_time_pm = $resDetailAddDayHour['start_time_pm_agg'];
                }else{
                    $IdConnectionss->sunday_start_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['end_time_pm_agg'])) {
                    $IdConnectionss->sunday_end_time_pm = $resDetailAddDayHour['end_time_pm_agg'];
                }else{
                    $IdConnectionss->sunday_end_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['show_availability_calendar_agg'])) {
                    $IdConnectionss->show_availability_calendar_sunday = $resDetailAddDayHour['show_availability_calendar_agg'];
                }else{
                    $IdConnectionss->show_availability_calendar_sunday = false;
                } 
                if (isset($resDetailAddDayHour['nr_place_agg'])) {
                    $IdConnectionss->sunday_nr_place = $resDetailAddDayHour['nr_place_agg'];
                }else{
                    $IdConnectionss->sunday_nr_place = '';
                }  
                if (isset($resDetailAddDayHour['hour'])) {
                    $IdConnectionss->hour_sunday = $resDetailAddDayHour['hour'];
                }else{
                    $IdConnectionss->hour_sunday = NULL;
                } 
                if (isset($resDetailAddDayHour['limit_reservations'])) {
                    $IdConnectionss->limit_reservations_sunday = $resDetailAddDayHour['limit_reservations'];
                }else{
                    $IdConnectionss->limit_reservations_sunday = false;
                } 
                $IdConnectionss->save();
            }

}
/* end foreach */

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $Connections->id;
    }
    #endregion INSERT


    public static function getByEventResultForParameter(Request $request, int $idUser)
    {
       $result = collect();
       $ConnectionsControl= Connections::where('id', $request->connection_id)->first()->check_multiple;
       $numeroPostiOrdinabili = 0;
       $BookingSettings = BookingSettings::all();

       if (isset($BookingSettings)){
           $numeroPostiOrdinabili = $BookingSettings[0]->nr_appointments;          
       }
       

       if ($ConnectionsControl == false){
        $Connections = Connections::where('id', $request->connection_id)->first();
        $BookingServices = BookingServices::where('id', $Connections->service_id)->first();

        $dayOfDate = date('w', strtotime($request->date_from));

        switch ($dayOfDate) {
            case 0:
                //domenica
                $ConnStartTime = $Connections->sunday_start_time;
                $ConnEndTime = $Connections->sunday_end_time;
                $ConnStartTimePm = $Connections->sunday_start_time_pm;
                $ConnEndTimePm = $Connections->sunday_end_time_pm;
                break;
            case 1:
                //lunedi
                $ConnStartTime = $Connections->monday_start_time;
                $ConnEndTime = $Connections->monday_end_time;
                $ConnStartTimePm = $Connections->monday_start_time_pm;
                $ConnEndTimePm = $Connections->monday_end_time_pm;
                break;
            case 2:
                //martedi 
                $ConnStartTime = $Connections->tuesday_start_time;
                $ConnEndTime = $Connections->tuesday_end_time;
                $ConnStartTimePm = $Connections->tuesday_start_time_pm;
                $ConnEndTimePm = $Connections->tuesday_end_time_pm;
                break;
            case 3:
                //mercoledi 
                $ConnStartTime = $Connections->wednesday_start_time;
                $ConnEndTime = $Connections->wednesday_end_time;
                $ConnStartTimePm = $Connections->wednesday_start_time_pm;
                $ConnEndTimePm = $Connections->wednesday_end_time_pm;
                break;
            case 4:
                //giovedi
                $ConnStartTime = $Connections->thursday_start_time;
                $ConnEndTime = $Connections->thursday_end_time;
                $ConnStartTimePm = $Connections->thursday_start_time_pm;
                $ConnEndTimePm = $Connections->thursday_end_time_pm;
                break;
            case 5:
                //venerdi
                $ConnStartTime = $Connections->friday_start_time;
                $ConnEndTime = $Connections->friday_end_time;
                $ConnStartTimePm = $Connections->friday_start_time_pm;
                $ConnEndTimePm = $Connections->friday_end_time_pm;
                break;
            case 6:
                //sabato
                $ConnStartTime = $Connections->saturday_start_time;
                $ConnEndTime = $Connections->saturday_end_time;
                $ConnStartTimePm = $Connections->saturday_start_time_pm;
                $ConnEndTimePm = $Connections->saturday_end_time_pm;
                break;
            default:
                //base
                $ConnStartTime = $Connections->start_time;
                $ConnEndTime = $Connections->end_time;
                $ConnStartTimePm = $Connections->start_time_pm;
                $ConnEndTimePm = $Connections->end_time_pm;
        }


        $Services_SlotStep = $BookingServices->slot_step; 
        $visualizzazioneDispo = $Connections->show_availability_calendar;
        
        $dateFrom = strtotime($ConnStartTime);
        $dateEnd = strtotime($ConnEndTime);
        $dateFromPm = strtotime($ConnStartTimePm);
        $dateEndPm = strtotime($ConnEndTimePm);


        if ($Services_SlotStep > 0 && !is_null($Services_SlotStep) && $Services_SlotStep != ''){ 
            $contPosti = 0; 
            for($i = $dateFrom;  $i < $dateEnd; $i = strtotime('+'. $Services_SlotStep .' minutes', $i)){

                $contPosti = $contPosti + 1;

                $iconvert= date("H:i:s", $i);
                $starttimedb= date("H:i:s", strtotime('+'. $Services_SlotStep .' minutes', $i));

                $dateFromException = Carbon::parse($request->date_from)->format('Y-m-d') . ' ' . $iconvert;
                $dateEndException =  Carbon::parse($request->date_from)->format('Y-m-d') . ' ' . $starttimedb;

                //query eccezioni
                $Exceptions = Exceptions::where('service_id', $Connections->service_id)
                ->where('employee_id', $Connections->employee_id)
                ->where('place_id', $Connections->place_id)
                ->where('date_start', '<=', $dateFromException)
                ->where('date_end', '>=', $dateEndException)->get()->first();
                
                if (!isset($Exceptions)){
                
                    //end query eccezioni
                    $numeroPostiLiberi = $Connections->nr_place;
                    
                    $nr_posti = DB::select(
                        'SELECT SUM(nr_place) as posti_prenotati 
                        FROM booking_appointments 
                        WHERE connections_id =  ' . $request->connection_id . ' 
                        AND date_from = \'' . Carbon::parse($request->date_from)->format('Y-m-d') . '\' 
                        AND date_to = \'' . Carbon::parse($request->date_from)->format('Y-m-d') . '\'  AND end_time = \'' . $starttimedb . '\' 
                        AND start_time = \'' . $iconvert . '\' 
                        LIMIT 1'); 

                      // LogHelper::debug($nr_posti);
                
                    if ($nr_posti[0]->posti_prenotati == null || $nr_posti[0]->posti_prenotati == NULL){

                        $numeriPostiRimanenti = $numeroPostiLiberi;
                     

                    }else{
                        $numeriPostiRimanenti = $numeroPostiLiberi - $nr_posti[0]->posti_prenotati;
                    }
    

                    $DtoBookingAppointments = new DtoBookingAppointments();
                    if($numeriPostiRimanenti > 0){
                        $DtoBookingAppointments->time = $iconvert;
                        $DtoBookingAppointments->end_time = $starttimedb;
                        $DtoBookingAppointments->price = $BookingServices->price;
                        $DtoBookingAppointments->reduced_price = $BookingServices->reduced_price;
                        $DtoBookingAppointments->description = $Connections->description;
                        $DtoBookingAppointments->contPosti = $contPosti;
                        $DtoBookingAppointments->connection_id = $request->connection_id;
                        if($visualizzazioneDispo){
                            $DtoBookingAppointments->disponibilita = $numeriPostiRimanenti;
                        }else{
                            $DtoBookingAppointments->disponibilita = 0;
                        }

                        if($numeriPostiRimanenti - ($request->nr_place + $request->reduced_price) >= 0){
                            $DtoBookingAppointments->show_button = true;
                        }else{
                            $DtoBookingAppointments->show_button = false;
                        }   
                       
                        $result->push($DtoBookingAppointments);
                   
                    }
                }
            } 

            //ciclo per visualizzazione ore pm
            for($i = $dateFromPm;  $i < $dateEndPm; $i = strtotime('+'. $Services_SlotStep .' minutes', $i)){

                $contPosti = $contPosti + 1;

                $iconvert= date("H:i:s", $i);
                $starttimedb= date("H:i:s", strtotime('+'. $Services_SlotStep .' minutes', $i));

                $dateFromException = Carbon::parse($request->date_from)->format('Y-m-d') . ' ' . $iconvert;
                $dateEndException =  Carbon::parse($request->date_from)->format('Y-m-d') . ' ' . $starttimedb;

                //query eccezioni
                $Exceptions = Exceptions::where('service_id', $Connections->service_id)
                ->where('employee_id', $Connections->employee_id)
                ->where('place_id', $Connections->place_id)
                ->where('date_start', '<=', $dateFromException)
                ->where('date_end', '>=', $dateEndException)->get()->first();
                
                if (!isset($Exceptions)){

                    $numeroPostiLiberi = $Connections->nr_place;

                    $nr_posti = DB::select(
                        'SELECT SUM(nr_place) as posti_prenotati 
                        FROM booking_appointments 
                        WHERE connections_id =  ' . $request->connection_id . ' 
                        AND date_from = \'' . Carbon::parse($request->date_from)->format('Y-m-d') . '\' 
                        AND date_to = \'' . Carbon::parse($request->date_from)->format('Y-m-d') . '\' AND end_time = \'' . $starttimedb . '\' 
                        AND start_time = \'' . $iconvert . '\' 
                        LIMIT 1'); 
                
                    if ($nr_posti[0]->posti_prenotati == null){
                        $numeriPostiRimanenti = $numeroPostiLiberi;
                    }else{
                        $numeriPostiRimanenti = $numeroPostiLiberi - $nr_posti[0]->posti_prenotati;
                    }
    
                    $DtoBookingAppointments = new DtoBookingAppointments();
                    if($numeriPostiRimanenti > 0){
                        $DtoBookingAppointments->time = $iconvert;
                        $DtoBookingAppointments->end_time = $starttimedb;
                        $DtoBookingAppointments->price = $BookingServices->price;
                        $DtoBookingAppointments->reduced_price = $BookingServices->reduced_price;
                        $DtoBookingAppointments->description = $Connections->description;
                        $DtoBookingAppointments->contPosti = $contPosti;
                        $DtoBookingAppointments->connection_id = $request->connection_id;
                        if($visualizzazioneDispo){
                            $DtoBookingAppointments->disponibilita = $numeriPostiRimanenti;
                        }else{
                            $DtoBookingAppointments->disponibilita = 0;
                        }

                        if($numeriPostiRimanenti - ($request->nr_place + $request->reduced_price) >= 0){
                            $DtoBookingAppointments->show_button = true;
                        }else{
                            $DtoBookingAppointments->show_button = false;
                        }      
                        $result->push($DtoBookingAppointments);
                    }
                }
            } 
            //end ciclo per visualizzazione ore pm


                $Content= Content::where('code', 'BookingConnectionsResultSingolaData')->get()->first();
                $contentLanguageConnectionsDate = ContentLanguage::where('content_id', $Content->id)->where('language_id', $request->languageId)->get()->first();    
                $finalHtmlConnections= "";
                $cont =0;  

                foreach ($result as $fieldsDb){
                  

                    $costoOpzione= 0;
                    $Connections = $contentLanguageConnectionsDate->content; 
                    $idContentOptionBooking = Content::where('code', 'DetailOptionBooking')->get()->first();
                    $contentLanguageOptionBooking = ContentLanguage::where('content_id', $idContentOptionBooking->id)->where('language_id', HttpHelper::getLanguageId())->get()->first();             
                    $finalHtmlDetailOptionalBooking= "";

                    $BookingConnectionOptions = BookingConnectionOptions::where('connections_id', $request->connection_id)->get();
                    if (isset($BookingConnectionOptions)){
                        $costoOpzione = ($BookingServices->price * $request->nr_place) + ($BookingServices->reduced_price * $request->reduced_price);
                       
                        foreach ($BookingConnectionOptions as $res) { 
                            $OptionalLanguages = OptionalLanguages::where('optional_id', $res->optional_id)->where('language_id', $request->languageId)->get()->first();
                            $cont = $cont+1;
                            $newContentOptions = $contentLanguageOptionBooking->content;  
                                $newContentOptions = str_replace('#options#', $OptionalLanguages->description,  $newContentOptions); 
                                $newContentOptions = str_replace('#connections_id#', $res->connections_id,  $newContentOptions); 
                            
                                if ($res->check_each == true){
                                    $newContentOptions = str_replace('#cost_optional#', $res->cost * ($request->nr_place + $request->reduced_price),  $newContentOptions); 
                                    //$costoOpzione = $costoOpzione + ($res->cost * ($request->nr_place + $request->reduced_price));
                                    $inputquantity = '<label>Qta</label><input type="number" class="form-control qta" style="margin-top: -37px;margin-left: 27px; "min="0" max="' .  $numeroPostiOrdinabili . '" data-numero-cont-posti="' .$fieldsDb->contPosti . '" data-check-each="' . $res->check_each . '" data-nr-total_price="'. $costoOpzione . '" data-cost-optional="'. $res->cost . '" data-id-qta="'.$cont.'" id="qta'.$cont.'" name="qta'.$cont.'"></input>';
                                    $newContentOptions = str_replace('#inputquantity#',  $inputquantity,  $newContentOptions);
                                    $newContentOptions = str_replace('#identificativo#',  $cont,  $newContentOptions);
                                    $newContentOptions = str_replace('#contposti#',  $fieldsDb->contPosti,  $newContentOptions); 
                                    $newContentOptions = str_replace('#costoption#',  $res->cost,  $newContentOptions); 
                                    $newContentOptions = str_replace('#total_price#',  $costoOpzione,  $newContentOptions);
                                }else{
                                    $newContentOptions = str_replace('#cost_optional#', $res->cost,  $newContentOptions);
                                    $inputquantity = '<input type="number" class="form-control qta" style="margin-top: -37px;margin-left: 27px; min="0" max="' .  $numeroPostiOrdinabili . '" data-numero-cont-posti="' .$fieldsDb->contPosti . '" data-check-each="' . $res->check_each . '" data-nr-total_price="'. $costoOpzione . '" data-cost-optional="'. $res->cost . '"  data-id-qta="qta'.$cont.'"  id="qta'.$cont.'" name="qta'.$cont.'" style="display:none;"></input>';
                                    $newContentOptions = str_replace('#inputquantity#',  $inputquantity,  $newContentOptions); 
                                    $newContentOptions = str_replace('#contposti#',  $fieldsDb->contPosti,  $newContentOptions); 
                                    $newContentOptions = str_replace('#costoption#',  $res->cost,  $newContentOptions); 
                                    $newContentOptions = str_replace('#identificativo#',  $cont,  $newContentOptions);
                                    $newContentOptions = str_replace('#total_price#',  $costoOpzione,  $newContentOptions);
                                } 

                                    if($res->cost > 0){
                                        $replaceClassOption = "initial";
                                        $newContentOptions = str_replace('#replaceClassOption#', $replaceClassOption,  $newContentOptions);      
                                    }else{
                                        $replaceClassOption = "none";
                                       // $costoOpzioneNone = 0;
                                        //$Connections = str_replace('#total_price#', $costoOpzioneNone,  $Connections); 
                                        $newContentOptions = str_replace('#replaceClassOption#', $replaceClassOption,  $newContentOptions); 
                                    }

                                $finalHtmlDetailOptionalBooking = $finalHtmlDetailOptionalBooking . $newContentOptions; 

                        }
                    }else{
                      $costoOpzione = ($BookingServices->price * $request->nr_place) + ($BookingServices->reduced_price * $request->reduced_price);
                    }

                    if(isset($fieldsDb->time)){
                            if($fieldsDb->show_button){
                                $replaceClassDispBtn = "block";
                                $replaceClassDispError = "none";
                            }else{
                                $replaceClassDispBtn = "none";
                                $replaceClassDispError = "block";
                            }
                        
                            if($BookingServices->reduced_price > 0){
                                $replaceClassDispBtnSelect = "block";
                                $Connections = str_replace('#replaceClassReducedPrice#', $replaceClassDispBtnSelect,  $Connections);      
                            }else{
                                $replaceClassDispErrorSelect = "none";
                                $Connections = str_replace('#replaceClassReducedPrice#', $replaceClassDispErrorSelect,  $Connections); 
                            } 
                            if($BookingServices->price > 0){
                                $replaceClassDispInt = "block";
                                $Connections = str_replace('#replaceClassPriceInt#', $replaceClassDispInt,  $Connections);   

                            }else{
                                $replaceClassDispInt = "none";
                                $Connections = str_replace('#replaceClassPriceInt#', $replaceClassDispInt,  $Connections); 
                            } 
                            

                            $Connections = str_replace('#disponibilita#', $fieldsDb->disponibilita, 
                                           str_replace('#connection_id#', $fieldsDb->connection_id,
                                           str_replace('#price#', $fieldsDb->price,
                                           str_replace('#description#', $fieldsDb->description,
                                           str_replace('#booking_options_connections#', $finalHtmlDetailOptionalBooking,
                                           str_replace('#total_price#', $costoOpzione,
                                           str_replace('#end_time#', substr($fieldsDb->end_time, 0, -3),
                                           str_replace('#date_selected#', Carbon::parse($request->date_from)->format('Y-m-d'),
                                           str_replace('#contposti#', $fieldsDb->contPosti,
                                           str_replace('#reduced_price#', $fieldsDb->reduced_price,
                                           str_replace('#classdispbtn#', $replaceClassDispBtn,
                                           str_replace('#classdisperror#', $replaceClassDispError,
                                           str_replace('#start_time#', substr($fieldsDb->time, 0, -3),  $Connections))))))))))))); 
                            $finalHtmlConnections = $finalHtmlConnections . $Connections;                                                                                      
                    }
                }  
            }
    }else{
        $Content= Content::where('code', 'BookingConnectionsResult')->get()->first();
        $contentLanguage = ContentLanguage::where('content_id', $Content->id)->where('language_id', $request->languageId)->get()->first();    
        $tagsConnections = SelenaViewsBL::getTagByType('BookingConnectionsResult');
        $finalHtmlConnections= "";
    
        $strWhere = "";
        $date_from = Carbon::parse($request->date_from)->format('Y-m-d');
        $date_to = Carbon::parse($request->date_to)->format('Y-m-d');
  
        if ($date_to != "") {
             $strWhere = $strWhere . ' OR booking_connections.date_to = \'' . $date_to . '\'';
        }
            foreach (DB::select(
                'SELECT *
                FROM booking_connections
                WHERE date_from = :date_from ' . $strWhere,
                ['date_from' => $date_from
            ]) as $ConnectionsResult){
                $newContentConnectionsResult = $contentLanguage->content;  
                foreach ($tagsConnections as $fieldsDb){
                    if(isset($fieldsDb->tag)){                                                              
                        if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                            $cleanTag = str_replace('#', '', $fieldsDb->tag);
                            if(is_numeric($ConnectionsResult->{$cleanTag}) && strpos($ConnectionsResult->{$cleanTag},'.')){
                                $tag = str_replace('.',',',substr($ConnectionsResult->{$cleanTag}, 0, -2));
                            }else{
                                $tag = $ConnectionsResult->{$cleanTag};
                            }
                            $newContentConnectionsResult = str_replace($fieldsDb->tag , $tag,  $newContentConnectionsResult);                                                                               
                        }
                    }
                }
                if (strpos($newContentConnectionsResult, '#price#') !== false) {
                    $BookingServicesPrice = BookingServices::where('id', $ConnectionsResult->service_id)->get()->first();
                    if (isset($BookingServicesPrice)){
                        $newContentConnectionsResult = str_replace('#price#', $BookingServicesPrice->price,  $newContentConnectionsResult); 
                    }else{
                        $newContentConnectionsResult = str_replace('#price#', '',  $newContentConnectionsResult);           
                    }
                }
                $finalHtmlConnections = $finalHtmlConnections . $newContentConnectionsResult;
            }; 
        }
      
        return $finalHtmlConnections;

    }
    
    
    public static function getByIdConnections(Request $request, int $idUser)
    {
        $result = collect();

        $ConnectionsOnDb = Connections::where('id', $request->id)->get()->first();
        
        if($ConnectionsOnDb->type_of_date == 2){
            $DtoConnections = new DtoConnections();
            $DtoConnections->date = array();
                $newDateString1 = date_format(date_create($ConnectionsOnDb->date_from), 'd-m-Y');
                $newDateString2 = date_format(date_create($ConnectionsOnDb->date_to), 'd-m-Y');
                $Date1 = $newDateString1;
                $Date2 = $newDateString2;
                $Variable1 = strtotime($Date1);
                $Variable2 = strtotime($Date2);  
                
                for ($currentDate = $Variable1; $currentDate <= $Variable2; $currentDate += (86400)) {                                 
                    //$Store = date('Y-m-d', $currentDate);
                    //$DtoConnections->date[] = $Store;
                }

                if (!is_null($ConnectionsOnDb->MONDAY) && $ConnectionsOnDb->MONDAY) {
                    $date = new DateTime('first Monday of this month');
                    $thisYear = $date->format('Y');
                    while ($date->format('Y') <= $thisYear + 3) {
                        $oreDaAggiungere = 0;
                        $datadacontrollare = strtotime($date->format('Y-m-d H:m:s'));

                        if ($ConnectionsOnDb->hour_monday != '' ||  $ConnectionsOnDb->hour_monday > 0 || !is_null($ConnectionsOnDb->hour_monday)) {
                            $oreDaAggiungere = $ConnectionsOnDb->hour_monday;
                        }

                        $datadiadesso = date("Y-m-d H:m:s" , strtotime('+' . $oreDaAggiungere . ' hours'));
                        $dataattuale= strtotime($datadiadesso);
                        
                        if ($dataattuale < $datadacontrollare) {
                            $Store = $date->format('Y-m-d');
                            $DtoConnections->date[] = $Store;
                        }
                        $date->modify('next Monday');
                    }
                }

                if (!is_null($ConnectionsOnDb->TUESDAY) && $ConnectionsOnDb->TUESDAY) {
                    $date = new DateTime('first Tuesday of this month');
                    $thisYear = $date->format('Y');
                    while ($date->format('Y') <= $thisYear + 3) {
                        $oreDaAggiungere = 0;
                        $datadacontrollare = strtotime($date->format('Y-m-d H:m:s'));
                        if ($ConnectionsOnDb->hour_tuesday != '' ||  $ConnectionsOnDb->hour_tuesday > 0 || !is_null($ConnectionsOnDb->hour_tuesday)) {
                            $oreDaAggiungere = $ConnectionsOnDb->hour_tuesday;
                        }
                        

                        $datadiadesso = date("Y-m-d H:m:s" , strtotime('+' . $oreDaAggiungere . ' hours'));
                        $dataattuale= strtotime($datadiadesso);  


                        if ($dataattuale < $datadacontrollare) {
                            $Store = $date->format('Y-m-d');
                            $DtoConnections->date[] = $Store;
                        }

                        $date->modify('next Tuesday');
                    }     
                }  
                if (!is_null($ConnectionsOnDb->WEDNESDAY) && $ConnectionsOnDb->WEDNESDAY) {
                    $date = new DateTime('first Wednesday of this month');
                    $thisYear = $date->format('Y');
                    while ($date->format('Y') <= $thisYear + 3) {
                        $oreDaAggiungere = 0;
                        $datadacontrollare = strtotime($date->format('Y-m-d H:m:s'));
                        if ($ConnectionsOnDb->hour_wednesday != '' ||  $ConnectionsOnDb->hour_wednesday > 0 || !is_null($ConnectionsOnDb->hour_wednesday)) {
                            $oreDaAggiungere = $ConnectionsOnDb->hour_wednesday;
                        }

                        $datadiadesso = date("Y-m-d H:m:s" , strtotime('+' . $oreDaAggiungere . ' hours'));
                        $dataattuale= strtotime($datadiadesso);  

                        if ($dataattuale < $datadacontrollare) {
                            $Store = $date->format('Y-m-d');
                            $DtoConnections->date[] = $Store;
                        }

                        $date->modify('next Wednesday');
                    }    
                }
                if (!is_null($ConnectionsOnDb->THURSDAY) && $ConnectionsOnDb->THURSDAY) {
                    $date = new DateTime('first Thursday of this month');
                    $thisYear = $date->format('Y');
                    while ($date->format('Y') <= $thisYear + 3) {
                        $oreDaAggiungere = 0;
                        $datadacontrollare = strtotime($date->format('Y-m-d H:m:s'));

                        if ($ConnectionsOnDb->hour_thursday != '' ||  $ConnectionsOnDb->hour_thursday > 0 || !is_null($ConnectionsOnDb->hour_thursday)) {
                            $oreDaAggiungere = $ConnectionsOnDb->hour_thursday;
                        }

                        $datadiadesso = date("Y-m-d H:m:s" , strtotime('+' . $oreDaAggiungere . ' hours'));
                        $dataattuale= strtotime($datadiadesso);  

                        if ($dataattuale < $datadacontrollare) {
                            $Store = $date->format('Y-m-d');
                            $DtoConnections->date[] = $Store;
                        }
                        $date->modify('next Thursday');
                    }   
                }
                if (!is_null($ConnectionsOnDb->FRIDAY) && $ConnectionsOnDb->FRIDAY) {
                    $date = new DateTime('first Friday of this month');
                    $thisYear = $date->format('Y');
                    while ($date->format('Y') <= $thisYear + 3) {
                        $oreDaAggiungere = 0;
                        $datadacontrollare = strtotime($date->format('Y-m-d H:m:s'));

                        if ($ConnectionsOnDb->hour_friday != '' ||  $ConnectionsOnDb->hour_friday > 0 || !is_null($ConnectionsOnDb->hour_friday)) {
                            $oreDaAggiungere = $ConnectionsOnDb->hour_friday;
                        }

                        $datadiadesso = date("Y-m-d H:m:s" , strtotime('+' . $oreDaAggiungere . ' hours'));
                        $dataattuale= strtotime($datadiadesso);  

                        if ($dataattuale < $datadacontrollare) {
                            $Store = $date->format('Y-m-d');
                            $DtoConnections->date[] = $Store;
                        }

                        $date->modify('next Friday');
                    }    
                }
                if (!is_null($ConnectionsOnDb->SATURDAY) && $ConnectionsOnDb->SATURDAY) {
                    $date = new DateTime('first Saturday of this month');
                    $thisYear = $date->format('Y');
                    while ($date->format('Y') <= $thisYear + 3) {
                        $oreDaAggiungere = 0;
                        $datadacontrollare = strtotime($date->format('Y-m-d H:m:s'));

                        if ($ConnectionsOnDb->hour_saturday != '' ||  $ConnectionsOnDb->hour_saturday > 0 || !is_null($ConnectionsOnDb->hour_saturday)) {
                            $oreDaAggiungere = $ConnectionsOnDb->hour_saturday;
                        }

                        $datadiadesso = date("Y-m-d H:m:s" , strtotime('+' . $oreDaAggiungere . ' hours'));
                        $dataattuale= strtotime($datadiadesso);  

                        if ($dataattuale < $datadacontrollare) {
                            $Store = $date->format('Y-m-d');
                            $DtoConnections->date[] = $Store;
                        }
                        $date->modify('next Saturday');
                    }
                }
                if (!is_null($ConnectionsOnDb->SUNDAY) && $ConnectionsOnDb->SUNDAY) {
                    $date = new DateTime('first Sunday of this month');
                    $thisYear = $date->format('Y');
                    while ($date->format('Y') <= $thisYear + 3) {
                        $oreDaAggiungere = 0;
                        $datadacontrollare = strtotime($date->format('Y-m-d H:m:s'));

                        if ($ConnectionsOnDb->hour_sunday != '' ||  $ConnectionsOnDb->hour_sunday > 0 || !is_null($ConnectionsOnDb->hour_sunday)) {
                            $oreDaAggiungere = $ConnectionsOnDb->hour_sunday;
                        }
                        $datadiadesso = date("Y-m-d H:m:s" , strtotime('+' . $oreDaAggiungere . ' hours'));
                        $dataattuale= strtotime($datadiadesso);  

                        if ($dataattuale < $datadacontrollare) {
                            $Store = $date->format('Y-m-d');
                            $DtoConnections->date[] = $Store;
                        }
                        $date->modify('next Sunday');
                    }
                } 
                $result->push($DtoConnections); 
        }else{
            if($ConnectionsOnDb->type_of_date == 1){
                $DtoConnections = new DtoConnections();
                $DtoConnections->date[] = $ConnectionsOnDb->fixed_date;
                $result->push($DtoConnections); 
            }
        }

        return $result;
    }


    /**
     * Update
     * 
     * @param Request [DtoConnections]
     * @param int idLanguage
     */
    public static function update(Request $DtoConnections, int $idUser)
    {     

        $ConnectionsOnDb = Connections::find($DtoConnections->id);
      
        DB::beginTransaction();

        try {
            if (isset($DtoConnections->id)) {
                $ConnectionsOnDb->id = $DtoConnections->id;
            } 
            if (isset($DtoConnections->place_id)) {
                $ConnectionsOnDb->place_id = $DtoConnections->place_id;
            } 
             if (isset($DtoConnections->service_id)) {
                $ConnectionsOnDb->service_id = $DtoConnections->service_id;
            } 
             if (isset($DtoConnections->employee_id)) {
                $ConnectionsOnDb->employee_id = $DtoConnections->employee_id;
            } 
             if (isset($DtoConnections->nr_place)) {
                $ConnectionsOnDb->nr_place = $DtoConnections->nr_place;
            }else{
                $ConnectionsOnDb->nr_place = NULL;
            }
            if (isset($DtoConnections->show_availability_calendar)) {
                $ConnectionsOnDb->show_availability_calendar = $DtoConnections->show_availability_calendar;
            } 
            if (isset($DtoConnections->description)) {
                $ConnectionsOnDb->description = $DtoConnections->description;
            }else{
                $ConnectionsOnDb->description = '';
            }
            //
          if (isset($DtoConnections->check_fixed_date)) {
                $ConnectionsOnDb->type_of_date = $DtoConnections->check_fixed_date;
            }else{
                $ConnectionsOnDb->type_of_date = '';
            } 
            if (isset($DtoConnections->check_repetition)) {
                $ConnectionsOnDb->type_of_date = $DtoConnections->check_repetition;
            }else{
                $ConnectionsOnDb->type_of_date = '';
            }  
            if (isset($DtoConnections->check_datefrom_and_dateto)) {
                $ConnectionsOnDb->type_of_date = $DtoConnections->check_datefrom_and_dateto;
            }else{
                $ConnectionsOnDb->type_of_date = '';
            }  

         /*   if (!is_null($DtoConnections->check_fixed_date)) {
                $ConnectionsOnDb->type_of_date = $DtoConnections->check_fixed_date;
            }else{
                $ConnectionsOnDb->type_of_date = '';
            } 
            if (!is_null($DtoConnections->check_repetition)) {
                $ConnectionsOnDb->type_of_date = $DtoConnections->check_repetition;
            }else{
                $ConnectionsOnDb->type_of_date = '';
            }  
            if (!is_null($DtoConnections->check_datefrom_and_dateto)) {
                $ConnectionsOnDb->type_of_date = $DtoConnections->check_datefrom_and_dateto;
            }else{
                $ConnectionsOnDb->type_of_date = '';
            }*/


         
            if (isset($DtoConnections->monday)) {
                $ConnectionsOnDb->MONDAY = $DtoConnections->monday;
            }
            if (isset($DtoConnections->tuesday)) {
                $ConnectionsOnDb->TUESDAY = $DtoConnections->tuesday;
            } 
            if (isset($DtoConnections->wednesday)) {
                $ConnectionsOnDb->WEDNESDAY = $DtoConnections->wednesday;
            }
            if (isset($DtoConnections->thursday)) {
                $ConnectionsOnDb->THURSDAY = $DtoConnections->thursday;
            }
            if (isset($DtoConnections->friday)) {
                $ConnectionsOnDb->FRIDAY = $DtoConnections->friday;
            }
            if (isset($DtoConnections->saturday)) {
                $ConnectionsOnDb->SATURDAY = $DtoConnections->saturday;
            }
            if (isset($DtoConnections->sunday)) {
                $ConnectionsOnDb->SUNDAY = $DtoConnections->sunday;
            }
            if (isset($DtoConnections->exclude_holidays)) {
                $ConnectionsOnDb->exclude_holidays = $DtoConnections->exclude_holidays;
            }
          
            if (is_null($DtoConnections->fixed_date)) {
                $ConnectionsOnDb->fixed_date = NULL;
            }else{ 
                $ConnectionsOnDb->fixed_date = $DtoConnections->fixed_date;
            } 
                
            if (is_null($DtoConnections->date_from)) {
                $ConnectionsOnDb->date_from = NULL;
            }else{
                $ConnectionsOnDb->date_from = $DtoConnections->date_from;
            } 
            if (is_null($DtoConnections->date_to)) {
                $ConnectionsOnDb->date_to = NULL;
            }else{
                $ConnectionsOnDb->date_to = $DtoConnections->date_to;
            }
           
            if (isset($DtoConnections->start_time)) {
                $ConnectionsOnDb->start_time = $DtoConnections->start_time;
            }else{
                $ConnectionsOnDb->start_time = NULL;
            }
            if (isset($DtoConnections->end_time)) {
                $ConnectionsOnDb->end_time = $DtoConnections->end_time;
            }else{
                $ConnectionsOnDb->end_time = NULL;
            }
            if (isset($DtoConnections->start_time_pm)) {
                $ConnectionsOnDb->start_time_pm = $DtoConnections->start_time_pm;
            }else{
                $ConnectionsOnDb->start_time_pm = NULL;
            }
            if (isset($DtoConnections->end_time_pm)) {
                $ConnectionsOnDb->end_time_pm = $DtoConnections->end_time_pm;
            }else{
                $ConnectionsOnDb->end_time_pm = NULL;
            }
        
            if (isset($DtoConnections->check_multiple)) {
                $ConnectionsOnDb->check_multiple = $DtoConnections->check_multiple;
            }
            if (isset($DtoConnections->min_day)) {
                $ConnectionsOnDb->min_day = $DtoConnections->min_day;
            }else{
                $ConnectionsOnDb->min_day = NULL;
            } 
            $ConnectionsOnDb->save();

            $IdConnections = BookingConnectionOptions::where('connections_id', $DtoConnections->id)->get();
            if (isset($IdConnections)) {
                BookingConnectionOptions::where('connections_id', $DtoConnections->id)->delete();
            }
           /* foreach ($DtoConnections->DetailOption as $res) {
                if (isset($res)){
                    $BookingConnectionOptions = new BookingConnectionOptions();
                    $BookingConnectionOptions->connections_id = $DtoConnections->id;
                    $BookingConnectionOptions->optional_id = $res['optional_id'];
                    $BookingConnectionOptions->options = TypeOptionalLanguages::where('type_optional_id', $res['optional_id'])->first()->description;
                    if (!is_null($res['cost'])){
                        $BookingConnectionOptions->cost = $res['cost'];
                    }else{
                        $BookingConnectionOptions->cost = 0;
                    }
                    $BookingConnectionOptions->check_each = $res['check_each'];
                    $BookingConnectionOptions->save();
                }*/

                foreach ($DtoConnections->DetailOption as $res) {
                    if (!is_null($res['optional_id'])){
                        $BookingConnectionOptions = new BookingConnectionOptions();
                        $BookingConnectionOptions->connections_id = $DtoConnections->id;
                       /* $BookingConnectionOptions->optional_id = $res['optional_id'];
                        $BookingConnectionOptions->options = TypeOptionalLanguages::where('type_optional_id', $res['optional_id'])->first()->description;*/

                        $BookingConnectionOptions->optional_id = OptionalTypology::where('type_optional_id', $res['optional_id'])->first()->optional_id;
                        $Optional_Typology = OptionalTypology::where('type_optional_id', $res['optional_id'])->first();
                        $BookingConnectionOptions->options = OptionalLanguages::where('optional_id', $Optional_Typology->optional_id)->first()->description;

                        $BookingConnectionOptions->cost = $res['cost'];
                        $BookingConnectionOptions->check_each = $res['check_each'];
                        $BookingConnectionOptions->save();
                    }
                }

                foreach ($DtoConnections->DetailAddDayHour as $resDetailAddDayHour) {

                    $IdConnectionss = Connections::where('id', $DtoConnections->id)->get()->first();

                    if ($resDetailAddDayHour['monday_agg'] == true) {
    
                $IdConnectionss->MONDAY = $resDetailAddDayHour['monday_agg'];
              if (isset($resDetailAddDayHour['start_time_am_agg'])) {
                    $IdConnectionss->monday_start_time = $resDetailAddDayHour['start_time_am_agg'];
                }else{
                    $IdConnectionss->monday_start_time = NULL;
                }
                if (isset($resDetailAddDayHour['end_time_am_agg'])) {
                    $IdConnectionss->monday_end_time = $resDetailAddDayHour['end_time_am_agg'];
                }else{
                    $IdConnectionss->monday_end_time = NULL;
                }
                if (isset($resDetailAddDayHour['start_time_pm_agg'])) {
                    $IdConnectionss->monday_start_time_pm = $resDetailAddDayHour['start_time_pm_agg'];
                }else{
                    $IdConnectionss->monday_start_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['end_time_pm_agg'])) {
                    $IdConnectionss->monday_end_time_pm = $resDetailAddDayHour['end_time_pm_agg'];
                }else{
                    $IdConnectionss->monday_end_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['show_availability_calendar_agg'])) {
                    $IdConnectionss->show_availability_calendar_monday = $resDetailAddDayHour['show_availability_calendar_agg'];
                }else{
                    $IdConnectionss->show_availability_calendar_monday = false;
                } 
                if (isset($resDetailAddDayHour['nr_place_agg'])) {
                    $IdConnectionss->monday_nr_place = $resDetailAddDayHour['nr_place_agg'];
                }else{
                    $IdConnectionss->monday_nr_place = '';
                }  
                if (isset($resDetailAddDayHour['hour'])) {
                    $IdConnectionss->hour_monday = $resDetailAddDayHour['hour'];
                }else{
                    $IdConnectionss->hour_monday = NULL;
                } 
                if (isset($resDetailAddDayHour['limit_reservations'])) {
                    $IdConnectionss->limit_reservations_monday = $resDetailAddDayHour['limit_reservations'];
                }else{
                    $IdConnectionss->limit_reservations_monday = false;
                } 
                $IdConnectionss->save();
            }

            if ($resDetailAddDayHour['tuesday_agg'] == true){
                $IdConnectionss->TUESDAY = $resDetailAddDayHour['tuesday_agg'];

                if (isset($resDetailAddDayHour['start_time_am_agg'])) {
                    $IdConnectionss->tuesday_start_time = $resDetailAddDayHour['start_time_am_agg'];
                }else{
                    $IdConnectionss->tuesday_start_time = NULL;
                }
                if (isset($resDetailAddDayHour['end_time_am_agg'])) {
                    $IdConnectionss->tuesday_end_time = $resDetailAddDayHour['end_time_am_agg'];
                }else{
                    $IdConnectionss->tuesday_end_time = NULL;
                }
                if (isset($resDetailAddDayHour['start_time_pm_agg'])) {
                    $IdConnectionss->tuesday_start_time_pm = $resDetailAddDayHour['start_time_pm_agg'];
                }else{
                    $IdConnectionss->tuesday_start_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['end_time_pm_agg'])) {
                    $IdConnectionss->tuesday_end_time_pm = $resDetailAddDayHour['end_time_pm_agg'];
                }else{
                    $IdConnectionss->tuesday_end_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['show_availability_calendar_agg'])) {
                    $IdConnectionss->show_availability_calendar_tuesday= $resDetailAddDayHour['show_availability_calendar_agg'];
                }else{
                    $IdConnectionss->show_availability_calendar_tuesday = false;
                } 
                if (isset($resDetailAddDayHour['nr_place_agg'])) {
                    $IdConnectionss->tuesday_nr_place = $resDetailAddDayHour['nr_place_agg'];
                }else{
                    $IdConnectionss->tuesday_nr_place = '';
                }  
                if (isset($resDetailAddDayHour['hour'])) {
                    $IdConnectionss->hour_tuesday = $resDetailAddDayHour['hour'];
                }else{
                    $IdConnectionss->hour_tuesday = NULL;
                } 
                if (isset($resDetailAddDayHour['limit_reservations'])) {
                    $IdConnectionss->limit_reservations_tuesday = $resDetailAddDayHour['limit_reservations'];
                }else{
                    $IdConnectionss->limit_reservations_tuesday= false;
                } 
                $IdConnectionss->save();
            }

            if ($resDetailAddDayHour['wednesday_agg'] == true){
                $IdConnectionss->WEDNESDAY = $resDetailAddDayHour['wednesday_agg'];

                if (isset($resDetailAddDayHour['start_time_am_agg'])) {
                    $IdConnectionss->wednesday_start_time = $resDetailAddDayHour['start_time_am_agg'];
                }else{
                    $IdConnectionss->wednesday_start_time = NULL;
                }
                if (isset($resDetailAddDayHour['end_time_am_agg'])) {
                    $IdConnectionss->wednesday_end_time = $resDetailAddDayHour['end_time_am_agg'];
                }else{
                    $IdConnectionss->wednesday_end_time = NULL;
                }
                if (isset($resDetailAddDayHour['start_time_pm_agg'])) {
                    $IdConnectionss->wednesday_start_time_pm = $resDetailAddDayHour['start_time_pm_agg'];
                }else{
                    $IdConnectionss->wednesday_start_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['end_time_pm_agg'])) {
                    $IdConnectionss->wednesday_end_time_pm = $resDetailAddDayHour['end_time_pm_agg'];
                }else{
                    $IdConnectionss->wednesday_end_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['show_availability_calendar_agg'])) {
                    $IdConnectionss->show_availability_calendar_wednesday= $resDetailAddDayHour['show_availability_calendar_agg'];
                }else{
                    $IdConnectionss->show_availability_calendar_wednesday = false;
                } 
                if (isset($resDetailAddDayHour['nr_place_agg'])) {
                    $IdConnectionss->wednesday_nr_place = $resDetailAddDayHour['nr_place_agg'];
                }else{
                    $IdConnectionss->wednesday_nr_place = '';
                } 
                if (isset($resDetailAddDayHour['hour'])) {
                    $IdConnectionss->hour_wednesday = $resDetailAddDayHour['hour'];
                }else{
                    $IdConnectionss->hour_wednesday= NULL;
                } 
                if (isset($resDetailAddDayHour['limit_reservations'])) {
                    $IdConnectionss->limit_reservations_wednesday = $resDetailAddDayHour['limit_reservations'];
                }else{
                    $IdConnectionss->limit_reservations_wednesday = false;
                }  
                $IdConnectionss->save();
            }

            if ($resDetailAddDayHour['thursday_agg'] == true){
                $IdConnectionss->THURSDAY = $resDetailAddDayHour['thursday_agg'];

                if (isset($resDetailAddDayHour['start_time_am_agg'])) {
                    $IdConnectionss->thursday_start_time = $resDetailAddDayHour['start_time_am_agg'];
                }else{
                    $IdConnectionss->thursday_start_time = NULL;
                }
                if (isset($resDetailAddDayHour['end_time_am_agg'])) {
                    $IdConnectionss->thursday_end_time = $resDetailAddDayHour['end_time_am_agg'];
                }else{
                    $IdConnectionss->thursday_end_time = NULL;
                }
                if (isset($resDetailAddDayHour['start_time_pm_agg'])) {
                    $IdConnectionss->thursday_start_time_pm = $resDetailAddDayHour['start_time_pm_agg'];
                }else{
                    $IdConnectionss->thursday_start_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['end_time_pm_agg'])) {
                    $IdConnectionss->thursday_end_time_pm = $resDetailAddDayHour['end_time_pm_agg'];
                }else{
                    $IdConnectionss->thursday_end_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['show_availability_calendar_agg'])) {
                    $IdConnectionss->show_availability_calendar_thursday = $resDetailAddDayHour['show_availability_calendar_agg'];
                }else{
                    $IdConnectionss->show_availability_calendar_thursday = false;
                } 
                if (isset($resDetailAddDayHour['nr_place_agg'])) {
                    $IdConnectionss->thursday_nr_place = $resDetailAddDayHour['nr_place_agg'];
                }else{
                    $IdConnectionss->thursday_nr_place = '';
                } 
                if (isset($resDetailAddDayHour['hour'])) {
                    $IdConnectionss->hour_thursday = $resDetailAddDayHour['hour'];
                }else{
                    $IdConnectionss->hour_thursday= NULL;
                } 
                if (isset($resDetailAddDayHour['limit_reservations'])) {
                    $IdConnectionss->limit_reservations_thursday = $resDetailAddDayHour['limit_reservations'];
                }else{
                    $IdConnectionss->limit_reservations_thursday = false;
                }  
                $IdConnectionss->save();
           
            }

            if ($resDetailAddDayHour['friday_agg'] == true){
                $IdConnectionss->FRIDAY = $resDetailAddDayHour['friday_agg'];

                if (isset($resDetailAddDayHour['start_time_am_agg'])) {
                    $IdConnectionss->friday_start_time = $resDetailAddDayHour['start_time_am_agg'];
                }else{
                    $IdConnectionss->friday_start_time = NULL;
                }
                if (isset($resDetailAddDayHour['end_time_am_agg'])) {
                    $IdConnectionss->friday_end_time = $resDetailAddDayHour['end_time_am_agg'];
                }else{
                    $IdConnectionss->friday_end_time = NULL;
                }
                if (isset($resDetailAddDayHour['start_time_pm_agg'])) {
                    $IdConnectionss->friday_start_time_pm = $resDetailAddDayHour['start_time_pm_agg'];
                }else{
                    $IdConnectionss->friday_start_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['end_time_pm_agg'])) {
                    $IdConnectionss->friday_end_time_pm = $resDetailAddDayHour['end_time_pm_agg'];
                }else{
                    $IdConnectionss->friday_end_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['show_availability_calendar_agg'])) {
                    $IdConnectionss->show_availability_calendar_friday = $resDetailAddDayHour['show_availability_calendar_agg'];
                }else{
                    $IdConnectionss->show_availability_calendar_friday= false;
                } 
                if (isset($resDetailAddDayHour['nr_place_agg'])) {
                    $IdConnectionss->friday_nr_place = $resDetailAddDayHour['nr_place_agg'];
                }else{
                    $IdConnectionss->friday_nr_place = '';
                }  
                if (isset($resDetailAddDayHour['hour'])) {
                    $IdConnectionss->hour_friday = $resDetailAddDayHour['hour'];
                }else{
                    $IdConnectionss->hour_friday= NULL;
                } 
                if (isset($resDetailAddDayHour['limit_reservations'])) {
                    $IdConnectionss->limit_reservations_friday = $resDetailAddDayHour['limit_reservations'];
                }else{
                    $IdConnectionss->limit_reservations_friday = false;
                }  
                $IdConnectionss->save();
             
            }

            if ($resDetailAddDayHour['saturday_agg'] == true){
                $IdConnectionss->SATURDAY = $resDetailAddDayHour['saturday_agg'];

                if (isset($resDetailAddDayHour['start_time_am_agg'])) {
                    $IdConnectionss->saturday_start_time = $resDetailAddDayHour['start_time_am_agg'];
                }else{
                    $IdConnectionss->saturday_start_time = NULL;
                }
                if (isset($resDetailAddDayHour['end_time_am_agg'])) {
                    $IdConnectionss->saturday_end_time = $resDetailAddDayHour['end_time_am_agg'];
                }else{
                    $IdConnectionss->saturday_end_time = NULL;
                }
                if (isset($resDetailAddDayHour['start_time_pm_agg'])) {
                    $IdConnectionss->saturday_start_time_pm = $resDetailAddDayHour['start_time_pm_agg'];
                }else{
                    $IdConnectionss->saturday_start_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['end_time_pm_agg'])) {
                    $IdConnectionss->saturday_end_time_pm = $resDetailAddDayHour['end_time_pm_agg'];
                }else{
                    $IdConnectionss->saturday_end_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['show_availability_calendar_agg'])) {
                    $IdConnectionss->show_availability_calendar_saturday = $resDetailAddDayHour['show_availability_calendar_agg'];
                }else{
                    $IdConnectionss->show_availability_calendar_saturday = false;
                } 
                if (isset($resDetailAddDayHour['nr_place_agg'])) {
                    $IdConnectionss->saturday_nr_place = $resDetailAddDayHour['nr_place_agg'];
                }else{
                    $IdConnectionss->saturday_nr_place = '';
                }  
                if (isset($resDetailAddDayHour['hour'])) {
                    $IdConnectionss->hour_saturday = $resDetailAddDayHour['hour'];
                }else{
                    $IdConnectionss->hour_saturday= NULL;
                } 
                if (isset($resDetailAddDayHour['limit_reservations'])) {
                    $IdConnectionss->limit_reservations_saturday = $resDetailAddDayHour['limit_reservations'];
                }else{
                    $IdConnectionss->limit_reservations_saturday = false;
                }  
                $IdConnectionss->save();
                
            }

            if ($resDetailAddDayHour['sunday_agg'] == true){
                $IdConnectionss->SUNDAY = $resDetailAddDayHour['sunday_agg'];

                if (isset($resDetailAddDayHour['start_time_am_agg'])) {
                    $IdConnectionss->sunday_start_time = $resDetailAddDayHour['start_time_am_agg'];
                }else{
                    $IdConnectionss->sunday_start_time = NULL;
                }
                if (isset($resDetailAddDayHour['end_time_am_agg'])) {
                    $IdConnectionss->sunday_end_time = $resDetailAddDayHour['end_time_am_agg'];
                }else{
                    $IdConnectionss->sunday_end_time = NULL;
                }
                if (isset($resDetailAddDayHour['start_time_pm_agg'])) {
                    $IdConnectionss->sunday_start_time_pm = $resDetailAddDayHour['start_time_pm_agg'];
                }else{
                    $IdConnectionss->sunday_start_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['end_time_pm_agg'])) {
                    $IdConnectionss->sunday_end_time_pm = $resDetailAddDayHour['end_time_pm_agg'];
                }else{
                    $IdConnectionss->sunday_end_time_pm = NULL;
                } 
                if (isset($resDetailAddDayHour['show_availability_calendar_agg'])) {
                    $IdConnectionss->show_availability_calendar_sunday = $resDetailAddDayHour['show_availability_calendar_agg'];
                }else{
                    $IdConnectionss->show_availability_calendar_sunday = false;
                } 
                if (isset($resDetailAddDayHour['nr_place_agg'])) {
                    $IdConnectionss->sunday_nr_place = $resDetailAddDayHour['nr_place_agg'];
                }else{
                    $IdConnectionss->sunday_nr_place = '';
                }  
                if (isset($resDetailAddDayHour['hour'])) {
                    $IdConnectionss->hour_sunday = $resDetailAddDayHour['hour'];
                }else{
                    $IdConnectionss->hour_sunday = NULL;
                } 
                if (isset($resDetailAddDayHour['limit_reservations'])) {
                    $IdConnectionss->limit_reservations_sunday = $resDetailAddDayHour['limit_reservations'];
                }else{
                    $IdConnectionss->limit_reservations_sunday = false;
                }  
                $IdConnectionss->save();
            }

        }
        /* end foreach */

                
        

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return  $DtoConnections->id;
    }
    #endregion UPDATE


        /**
             * Clone
             * 
             * @param int id
             * @param int idFunctionality
             * @param int idLanguage
             * @param int idUser
             * 
             * @return int
             */
            public static function clone(int $id, int $idFunctionality, int $idUser, int $idLanguage)
            {
            
                $Connections = Connections::find($id);            
                DB::beginTransaction();

                try {
 
                    $newEmployees = new Connections();
                    $newEmployees->place_id = $Connections->place_id;
                    $newEmployees->service_id = $Connections->service_id;
                    $newEmployees->employee_id = $Connections->employee_id;
                    $newEmployees->nr_place = $Connections->nr_place . ' - Copy';
                    $newEmployees->show_availability_calendar = $Connections->show_availability_calendar;
                    $newEmployees->description = $Connections->description;
                    $newEmployees->check_multiple = $Connections->check_multiple;
                    $newEmployees->min_day = $Connections->min_day;
                    $newEmployees->save();
   
                    DB::commit();
                    return $newEmployees->id;
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
            }
        #endregion CLONE


        public static function getDateForId($tipoBooking, $idUser, $idLanguage)
        {
                $Content= Content::where('code', 'BookingConnectionsDate')->get()->first();
                $contentLanguageConnectionsDate = ContentLanguage::where('content_id', $Content->id)->where('language_id', $idLanguage)->get()->first();    
                $tagsConnections = SelenaViewsBL::getTagByType('BookingConnectionsDate');
                $Connections = Connections::where('id', $tipoBooking);

                $finalHtmlConnections= "";
                if (isset($Connections)){
    
                    foreach (DB::select(
                        'SELECT *
                        FROM booking_connections
                        WHERE id = ' . $tipoBooking
                        ) as $ResultConnections){
                        $Connections = $contentLanguageConnectionsDate->content;  
                        foreach ($tagsConnections as $fieldsDb){
                            if(isset($fieldsDb->tag)){                                                              
                                if (strpos($contentLanguageConnectionsDate->content, $fieldsDb->tag) !== false) {
                                    $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                    if(is_numeric($ResultConnections->{$cleanTag}) && strpos($ResultConnections->{$cleanTag},'.')){
                                        $tag = str_replace('.',',',substr($ResultConnections->{$cleanTag}, 0, -2));
                                    }else{
                                        $tag = $ResultConnections->{$cleanTag};
                                    }
                                    $Connections = str_replace($fieldsDb->tag , $tag,  $Connections);                                                                               
                                }
                            }
                        }
                        if (strpos($Connections, '#nr_appointments#') !== false) {
                            $BookingSettings = BookingSettings::all();
                            if (isset($BookingSettings)){
                                $Connections = str_replace('#nr_appointments#', $BookingSettings[0]->nr_appointments,  $Connections); 
                            }else{
                                $Connections = str_replace('#nr_appointments#', '',  $Connections);           
                            }
                        }
                        if (strpos($Connections, '#activate_purchase#') !== false) {
                            $BookingServices = BookingServices::where('id', $ResultConnections->service_id)->first();
                            if (isset($BookingServices)){
                                $Connections = str_replace('#activate_purchase#', $BookingServices->activate_purchase,  $Connections);  
                            }else{
                                $Connections = str_replace('#activate_purchase#', '',  $Connections);         
                            }
                        }

                        if($BookingServices->reduced_price >= 0){
                            $replaceClassDispBtnSelect = "block";
                            $Connections = str_replace('#replaceClassDispBtnSelect#', $replaceClassDispBtnSelect,  $Connections);      
                        }else{
                            $replaceClassDispErrorSelect = "none";
                            $Connections = str_replace('#replaceClassDispBtnSelect#', $replaceClassDispErrorSelect,  $Connections); 
                        }  

                        $finalHtmlConnections = $finalHtmlConnections . $Connections;
                    }; 
                }
                return $finalHtmlConnections;
                
        }

        
  

            #region DELETE
                    /**
                     * Delete
                     * 
                     * @param int id
                     * @param int idLanguage
                     */
                    public static function delete(int $id, int $idLanguage)
                    {
                        $Connections = Connections::find($id);                   

                        if (is_null($Connections)) {
                            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PeopleRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
                        }
                        DB::beginTransaction();
                        try {
                            $Connections->delete();
                            
                            DB::commit();
                        } catch (Exception $ex) {
                            DB::rollback();
                            throw $ex;
                        }
                    }
                    #endregion DELETE



                    public static function insertPrenotazioneForCliBooking(Request $request)
                    {      
                        DB::beginTransaction();
                       
                       try {

                        $BookingConnectionAppointmentsCartDetail = new BookingConnectionAppointmentsCartDetail;
                        
                        if (isset($request->cart_id)) {
                            $BookingConnectionAppointmentsCartDetail->cart_id = $request->cart_id;
                            }
                            if (isset($request->connection_id)) {
                            $BookingConnectionAppointmentsCartDetail->connections_id = $request->connection_id;
                            }
                            if (isset($request->start_time)) {
                            $BookingConnectionAppointmentsCartDetail->start_time = $request->start_time;
                            }
                            if (isset($request->disponibilita)) {
                            $BookingConnectionAppointmentsCartDetail->available = $request->disponibilita;
                            }
                            if (isset($request->price)) {
                            $BookingConnectionAppointmentsCartDetail->price = $request->price;
                            }
                            if (isset($request->total_price)) {
                            $BookingConnectionAppointmentsCartDetail->total_price = $request->total_price;
                            }
                            if (isset($request->nr_place_selected)) {
                            $BookingConnectionAppointmentsCartDetail->nr_place = $request->nr_place_selected;
                            }
                    
                            if (isset($request->reduced_price_selected)) {
                            $BookingConnectionAppointmentsCartDetail->reduced_price = $request->reduced_price_selected;
                            }
                            if (isset($request->end_time)) {
                                $BookingConnectionAppointmentsCartDetail->end_time = $request->end_time;
                            }
                            if (isset($request->date_from)) {
                                $BookingConnectionAppointmentsCartDetail->date_from = $request->date_from;
                            }   
                            $BookingConnectionAppointmentsCartDetail->save();

                            $Cart = Cart::where('id', $request->cart_id)->get()->first();
                            $CartDetail = CartDetail::where('id', $request->cart_detail_id)->get()->first();
                            $Connections = Connections::where('id', $request->connection_id)->get()->first();

                            if (isset($Cart)){
                                $Cart->total_amount = $Cart->total_amount + $BookingConnectionAppointmentsCartDetail->total_price;  
                                $Cart->save();  
                            }
                            if (isset($CartDetail)){
                                $CartDetail->quantity = $BookingConnectionAppointmentsCartDetail->nr_place + $BookingConnectionAppointmentsCartDetail->reduced_price;    
                                $CartDetail->total_amount = $BookingConnectionAppointmentsCartDetail->total_price;   
                                $CartDetail->description = $Connections->description;
                                //$CartDetail->description = $Connections->description . 'Data: ' . $BookingConnectionAppointmentsCartDetail->date_from . ' Data Inizio:' . $BookingConnectionAppointmentsCartDetail->start_time  . ' Data Fine:' . $BookingConnectionAppointmentsCartDetail->end_time;
                                $CartDetail->note = 'Data: ' . $BookingConnectionAppointmentsCartDetail->date_from .  ' Data Inizio:' . $BookingConnectionAppointmentsCartDetail->start_time  .  ' Data Fine:' . $BookingConnectionAppointmentsCartDetail->end_time;
                                $CartDetail->net_amount = $BookingConnectionAppointmentsCartDetail->total_price; 
                                $CartDetail->save();  
                            }

                           
                            
                            if ($request->guida == true){
                                $Guida = "Guida";
                                    $CartDetailOptional = new CartDetailOptional;                  
                                    $CartDetailOptional->cart_detail_id = $CartDetail->id; 
                                    $CartDetailOptional->optional_id = BookingConnectionOptions::where('options', $Guida)->get()->first()->optional_id;
                                    $CartDetailOptional->save();
                            }

                            if ($request->audioGuida == true){
                                $AudioGuida = "Audio Guida";

                                $CartDetailOptional = new CartDetailOptional;                  
                                $CartDetailOptional->cart_detail_id = $CartDetail->id; 
                                //$BookingConnectionOptions = BookingConnectionOptions::where('options', $AudioGuida)->get()->first()->optional_id;
                                //if (isset($BookingConnectionOptions)){
                                    $CartDetailOptional->optional_id = BookingConnectionOptions::where('options', $AudioGuida)->get()->first()->optional_id;
                                    $CartDetailOptional->save();
                                //}
                            }

                            if ($request->RidottamobilitaNecessitaascensore == true){
                                $RidottamobilitaNecessitaascensore = "Ridotta mobilità-Necessità ascensore";

                                $CartDetailOptional = new CartDetailOptional;                  
                                $CartDetailOptional->cart_detail_id = $CartDetail->id; 
                                //$BookingConnectionOptions = BookingConnectionOptions::where('options', $AudioGuida)->get()->first()->optional_id;
                                //if (isset($BookingConnectionOptions)){
                                    $CartDetailOptional->optional_id = BookingConnectionOptions::where('options', $RidottamobilitaNecessitaascensore)->get()->first()->optional_id;
                                    $CartDetailOptional->save();
                                //}
                            }






                   /* $BookingConnectionOptions = BookingConnectionOptions::where('connections_id', $Connections->id)->get();
                    if (isset($BookingConnectionOptions)){
                        foreach ($BookingConnectionOptions as $BookingConnectionOptionss){
                            $CartDetailOptional = new CartDetailOptional;                  
                            $CartDetailOptional->cart_detail_id = $CartDetail->id; 
                            $CartDetailOptional->optional_id = $BookingConnectionOptionss->optional_id;
                            $CartDetailOptional->save();
                        }
                    } */
                          

                            $BookingConnectionAppointmentsCartDetailid = $BookingConnectionAppointmentsCartDetail->id;
                        DB::commit();
                    } catch (Exception $ex) {
                        DB::rollback();
                        throw $ex;
                    }
                        return $BookingConnectionAppointmentsCartDetailid;    
                }
            }


                

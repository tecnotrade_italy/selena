<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoSalesman;
use App\DtoModel\DtoSalesmanUsers;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Salesman;
use App\SalesmanUsers;
use App\User;
use App\UsersDatas;
use App\Content;
use App\ContentLanguage;


use App\RoleUser;
use App\Cart;
use App\Role;
use Carbon\Carbon;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SalesmanBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Salesman::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoSalesman
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (Salesman::orderBy('id')->get() as $salesmanAll) {
            
            $DtoSalesman = new DtoSalesman();
            $DtoSalesman->id = $salesmanAll->id;
            $DtoSalesman->code = $salesmanAll->code;
            $DtoSalesman->business_name = $salesmanAll->business_name;
            $DtoSalesman->name = $salesmanAll->name;
            $DtoSalesman->surname = $salesmanAll->surname;
            $DtoSalesman->email = $salesmanAll->email;
            $DtoSalesman->phone_number = $salesmanAll->phone_number;
            $DtoSalesman->vat_number = $salesmanAll->vat_number;
            $DtoSalesman->fiscal_code = $salesmanAll->fiscal_code;
            $DtoSalesman->address = $salesmanAll->address;
            $DtoSalesman->city = $salesmanAll->city;
            $DtoSalesman->province = $salesmanAll->province;
            $DtoSalesman->postal_code = $salesmanAll->postal_code;
            $DtoSalesman->username = $salesmanAll->username;
            $DtoSalesman->password = $salesmanAll->password;
            $DtoSalesman->can_create_new_customer = $salesmanAll->can_create_new_customer;
            $DtoSalesman->can_edit_price = $salesmanAll->can_edit_price;
            $DtoSalesman->can_edit_discount = $salesmanAll->can_edit_discount;
            $DtoSalesman->can_edit_order = $salesmanAll->can_edit_order;
            $DtoSalesman->can_active_customer = $salesmanAll->can_active_customer;
            $DtoSalesman->view_all_customer = $salesmanAll->view_all_customer;
            $DtoSalesman->online = $salesmanAll->online;


            $SalesmanUsers = SalesmanUsers::where('salesmans_id', $salesmanAll->id)->get()->first();
            if(isset($SalesmanUsers)){
                foreach (SalesmanUsers::where('salesmans_id', $salesmanAll->id)->get() as $salesmanUsersIncluded) {
                    $DtoSalesmanUsersIncluded = new DtoSalesmanUsers();
                    $DtoSalesmanUsersIncluded->id = $salesmanUsersIncluded->id;
                    $DtoSalesmanUsersIncluded->salesman_id = $salesmanAll->id;
                    $DtoSalesmanUsersIncluded->user_id = $salesmanUsersIncluded->user_id;

                    $userdata = UsersDatas::where('user_id', $salesmanUsersIncluded->user_id)->first();
                    if(isset($userdata)){
                        $DtoSalesmanUsersIncluded->description = $userdata->business_name . ' - ' . $userdata->name;
                    }else{
                        $DtoSalesmanUsersIncluded->description = '';
                    }
                    $DtoSalesman->salesmanUsersIncluded->push($DtoSalesmanUsersIncluded);
                }

                

                foreach (DB::select(
                    "SELECT ud.user_id FROM users_datas as ud WHERE ud.user_id not in (SELECT user_id FROM salesman_users WHERE salesmans_id='" . $salesmanAll->id . "')"
                ) as $salesmanUsersExcluded) {
                    $DtoSalesmanUsersExcluded = new DtoSalesmanUsers();
                    $DtoSalesmanUsersExcluded->salesman_id = $salesmanAll->id;
                    $DtoSalesmanUsersExcluded->user_id = $salesmanUsersExcluded->user_id;

                    $userdataExcluded = UsersDatas::where('user_id', $salesmanUsersExcluded->user_id)->first();
                    if(isset($userdataExcluded)){
                        $DtoSalesmanUsersExcluded->description = $userdataExcluded->business_name . ' - ' . $userdataExcluded->name;
                    }else{
                        $DtoSalesmanUsersExcluded->description = '';
                    }
                    $DtoSalesman->salesmanUsersExcluded->push($DtoSalesmanUsersExcluded);
                }
            }else{
                foreach (DB::select('SELECT ud.user_id FROM users_datas as ud') as $salesmanUsersExcluded) {
                    $DtoSalesmanUsersExcluded = new DtoSalesmanUsers();
                    $DtoSalesmanUsersExcluded->salesman_id = $salesmanAll->id;
                    $DtoSalesmanUsersExcluded->user_id = $salesmanUsersExcluded->user_id;

                    $userdataExcluded = UsersDatas::where('user_id', $salesmanUsersExcluded->user_id)->first();
                    if(isset($userdataExcluded)){
                        $DtoSalesmanUsersExcluded->description = $userdataExcluded->business_name . ' - ' . $userdataExcluded->name;
                    }else{
                        $DtoSalesmanUsersExcluded->description = '';
                    }
                    $DtoSalesman->salesmanUsersExcluded->push($DtoSalesmanUsersExcluded);
                }
            }
                        
            $result->push($DtoSalesman); 
        }
        
        return $result;
    }

    /**
     * get All Users By Salesman
     *
     * @param request $request
     */
    public static function getAllUsersBySalesman(request $request)
    {
        $finalHtml = "";
        $idContent = Content::where('code', 'UserList')->get()->first();
        $contentLanguage = ContentLanguage::where('content_id', $idContent->id)->where('language_id', HttpHelper::getLanguageId())->get()->first();

        $strWhere = "";

        $SalesmanOnDb = Salesman::find($request->id);
        $whereSalesmanId = ' AND salesman_users.salesmans_id = ' . $request->id;
        if($SalesmanOnDb->view_all_customer){
            $whereSalesmanId = "";
        }
        
        if ($request->business_name != "") {
            if($strWhere == ''){
                $strWhere = ' AND users_datas.business_name ilike \'%' . $request->business_name . '%\'';
            }else{
                $strWhere = $strWhere . ' AND users_datas.business_name ilike \'%' . $request->users_datas . '%\'';
            } 
        }

        if ($request->province != "") {
            if($strWhere == ''){
                $strWhere = ' AND users_datas.province ilike \'%' . $request->province . '%\'';
            }else{
                $strWhere = $strWhere . ' AND users_datas.province ilike \'%' . $request->province . '%\'';
            } 
        }

        if ($request->code != "") {
            if($strWhere == ''){
                $strWhere = ' AND users.code ilike \'%' . $request->code . '%\'';
            }else{
                $strWhere = $strWhere . ' AND users.code ilike \'%' . $request->code . '%\'';
            } 
        }
        if ($request->vat_number != "") {
            if($strWhere == ''){
                $strWhere = ' AND users_datas.vat_number ilike \'%' . $request->vat_number . '%\'';
            }else{
                $strWhere = $strWhere . ' AND users_datas.vat_number ilike \'%' . $request->vat_number . '%\'';
            } 
        }
        
        $tags = SelenaViewsBL::getTagByType('UserList');

        foreach (DB::select(
            'SELECT users_datas.*
            FROM salesman_users 
            INNER JOIN users on salesman_users.user_id = users.id
            inner join users_datas on salesman_users.user_id = users_datas.user_id
            WHERE users.online = true ' . $whereSalesmanId . $strWhere
            ) as $user){
            $newContent = $contentLanguage->content;  
            foreach ($tags as $fieldsDb){
                if(isset($fieldsDb->tag)){                                                              
                    if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                        if(is_numeric($user->{$cleanTag}) && strpos($user->{$cleanTag},'.')){
                            $tag = str_replace('.',',',substr($user->{$cleanTag}, 0, -2));
                        }else{
                            $tag = $user->{$cleanTag};
                        }
                        $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                               
                    }
                }
            }

            $finalHtml = $finalHtml . $newContent;
        };
        
        return $finalHtml;

    }

    /**
     * active UserBy Id
     *
     * @param request $request
     */
    public static function activeUserById(request $request)
    {
        $user = User::where('id', $request->user_id)->get()->first();
        if(isset($user)){
            if ($user->online == 'true' && $user->confirmed == 'true'){
                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;
                $roleUser = RoleUser::where('user_id', $user->id)->get()->first();
                if(isset($roleUser)){
                    $roleDescription = Role::where('id', $roleUser->role_id)->first()->name;
                }else{
                    $roleDescription = "";
                }
                $usersDatas = UsersDatas::where('user_id', $user->id)->get()->first();
                if(isset($usersDatas)){
                    $userDescription = $usersDatas->business_name;
                }
                
                if ($request->remember_me) {
                    $token->expires_at = Carbon::now()->addWeeks(1);
                }
                $token->save();

                //Funzione di tracciamento login
                //LogsBL::insert($user->id, $request->ip(), 'LOGIN', 'Login Effettuato');
            }
            
            $cartUser = Cart::where('user_id', $user->id)->get()->first();
            $cartid = '';
            if(isset($cartUser)){
                $cartUser->salesman_id = $request->salesman_id;
                $cartUser->save();
                $cartid = $cartUser->id;
            }else{
                $cartNewUser = new Cart();
                $cartNewUser->user_id = $user->id;
                $cartNewUser->salesman_id = $request->salesman_id;
                $cartNewUser->session_token = 'Bearer ' . $tokenResult->accessToken;
                $cartNewUser->save();
                $cartid = $cartNewUser->id;       
            }

            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
                'username' => $user->name,
                'role' => $roleDescription,
                'idLanguage' => $user->language_id,
                'id' => $user->id,
                'cart_id' => $cartid,
                'user_description' => $userDescription,
                'code' => '200'
            ]);
        }else{
            return response()->json([
                'access_token' => '',
                'token_type' => 'Bearer',
                'expires_at' => '',
                'username' => '',
                'role' => '',
                'idLanguage' => '',
                'id' => '',
                'cart_id' => '',
                'user_description' => '',
                'code' => '200'
            ]);
        }

    }

    
    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $Salesman = new Salesman();
            $Salesman->code = $request->code;
            $Salesman->business_name = $request->business_name;
            $Salesman->name = $request->name;
            $Salesman->surname = $request->surname;
            $Salesman->email = $request->email;
            $Salesman->phone_number = $request->phone_number;
            $Salesman->vat_number = $request->vat_number;
            $Salesman->fiscal_code = $request->fiscal_code;
            $Salesman->address = $request->address;
            $Salesman->city = $request->city;
            $Salesman->province = $request->province;
            $Salesman->postal_code = $request->postal_code;
            $Salesman->username = $request->username;
            $Salesman->password = $request->password;
            $Salesman->can_create_new_customer = $request->can_create_new_customer;
            $Salesman->can_edit_price = $request->can_edit_price;
            $Salesman->can_edit_discount = $request->can_edit_discount;
            $Salesman->can_edit_order = $request->can_edit_order;
            $Salesman->can_active_customer = $request->can_active_customer;
            $Salesman->view_all_customer = $request->view_all_customer;
            $Salesman->online = $request->online;

            $Salesman->save();
            

            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        //return $Salesman->id;
    }

    /**
     * insert Salesman Users
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertSalesmanUsers(Request $request, int $idUser, int $idLanguage)
    {
        //static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $SalesmanUsers = new SalesmanUsers();
            $SalesmanUsers->user_id = $request->user_id;
            $SalesmanUsers->salesmans_id = $request->salesman_id;
            $SalesmanUsers->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        //return $Salesman->id;
    }

     /**
     * remove Salesman Users
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function removeSalesmanUsers(Request $request, int $idUser, int $idLanguage)
    {
        //static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $salsemanUsers = SalesmanUsers::where('salesmans_id', $request->salesman_id)->where('user_id',  $request->user_id);
            if(isset($salsemanUsers)){
                $salsemanUsers->delete();
            }
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        //return $Salesman->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request request
     * @param int idLanguage
     */
    public static function update(Request $request, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $SalesmanOnDb = Salesman::find($request->id);
        
        DB::beginTransaction();

        try {
            $SalesmanOnDb->code = $request->code;
            $SalesmanOnDb->business_name = $request->business_name;
            $SalesmanOnDb->name = $request->name;
            $SalesmanOnDb->surname = $request->surname;
            $SalesmanOnDb->email = $request->email;
            $SalesmanOnDb->phone_number = $request->phone_number;
            $SalesmanOnDb->vat_number = $request->vat_number;
            $SalesmanOnDb->fiscal_code = $request->fiscal_code;
            $SalesmanOnDb->address = $request->address;
            $SalesmanOnDb->city = $request->city;
            $SalesmanOnDb->province = $request->province;
            $SalesmanOnDb->postal_code = $request->postal_code;
            $SalesmanOnDb->username = $request->username;
            $SalesmanOnDb->password = $request->password;
            $SalesmanOnDb->can_create_new_customer = $request->can_create_new_customer;
            $SalesmanOnDb->can_edit_price = $request->can_edit_price;
            $SalesmanOnDb->can_edit_discount = $request->can_edit_discount;
            $SalesmanOnDb->can_edit_order = $request->can_edit_order;
            $SalesmanOnDb->can_active_customer = $request->can_active_customer;
            $SalesmanOnDb->view_all_customer = $request->view_all_customer;
            $SalesmanOnDb->online = $request->online;
            $SalesmanOnDb->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $Salesman = Salesman::find($id);
        
        if (is_null($Salesman)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            $salsemanUsers = SalesmanUsers::where('salesmans_id', $id);
            $salsemanUsers->delete();

            $Salesman->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    #endregion DELETE
}

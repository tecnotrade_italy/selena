<?php

namespace App\BusinessLogic;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoStatesQuotesGr;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\StatesQuotesGr;
use App\ItemGroupTechnicalSpecification;
use Intervention\Image\Facades\Image;
use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;


class StatesQuotesGrBL
{
 #region GET

 /**
     * Get select
     *
     * @param String $search
     * @return StatesQuotesGr
     */
    public static function selectStatesQuotesGr(string $search)
    {
        if ($search == "*") {
            return StatesQuotesGr::select('id', 'description as description')->get();
        } else {
            return StatesQuotesGr::where('description', 'like', $search . '%')->select('id', 'description as description')->get();
        }
    }

 }
       
<?php

namespace App\BusinessLogic;

use App\Content;
use App\ContentLanguage;
use App\CategoryLanguage;
use App\CategoryItem;
use App\Category;
use App\DtoModel\DtoProducer;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use App\PostalCode;
use App\Producer;
use App\User;
use App\UsersDatas;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

class ProducerBL
{
    #region PRIVATE
    /**
     * Validate data
     * 
     * @param Request ProducerBL
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoProducer, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoProducer->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Producer::find($DtoProducer->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($DtoProducer->code)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CodeNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoProducer->business_name)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CompanyNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoProducer->address)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::AddressNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoProducer->resort)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ResortNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoProducer->province)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ProvinceNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoProducer->postal_code_id)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PostalCodeNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoProducer->telephone_number)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TelephoneNumberNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoProducer->email)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoProducer->online)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::onlineNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    /**
     * Convert to dto
     * 
     * @param Producer postalCode
     * 
     * @return DtoProducer
     */
    private static function _convertToDto($Producer)
    {
        $DtoProducer = new DtoProducer();

        //detail
        if (isset($Producer->id)) {
            $DtoProducer->id = $Producer->id;
        }

        if (isset($Producer->code)) {
            $DtoProducer->code = $Producer->code;
        }

        if (isset($Producer->business_name)) {
            $DtoProducer->business_name = $Producer->business_name;
        }

        if (isset($Producer->address)) {
            $DtoProducer->address = $Producer->address;
        }

        if (isset($Producer->resort)) {
            $DtoProducer->resort = $Producer->resort;
        }

        if (isset($Producer->province)) {
            $DtoProducer->province = $Producer->province;
        }
        if (isset($Producer->postal_code_id)) {
            $DtoProducer->postal_code_id = $Producer->postal_code_id;
        }

        if (isset($Producer->telephone_number)) {
            $DtoProducer->telephone_number = $Producer->telephone_number;
        }
        if (isset($Producer->email)) {
            $DtoProducer->email = $Producer->email;
        }
        if (isset($Producer->online)) {
            $DtoProducer->online = $Producer->online;
        }

        return  $DtoProducer;
    }

    /**
     * Convert to VatType
     * 
     * @param DtoProducer
     * 
     * @return Producer
     */
    private static function _convertToModel($DtoProducer)
    {
        $Producer = new Producer();

        if (isset($DtoProducer->id)) {
            $Producer->id = $DtoProducer->id;
        }

        if (isset($DtoProducer->code)) {
            $Producer->code = $DtoProducer->code;
        }
        if (isset($DtoProducer->business_name)) {
            $Producer->business_name = $DtoProducer->business_name;
        }

        if (isset($DtoProducer->address)) {
            $Producer->address = $DtoProducer->address;
        }
        if (isset($DtoProducer->resort)) {
            $Producer->resort = $DtoProducer->resort;
        }
        if (isset($DtoProducer->province)) {
            $Producer->province = $DtoProducer->province;
        }
        if (isset($DtoProducer->postal_code_id)) {
            $Producer->postal_code_id = $DtoProducer->postal_code_id;
        }
        if (isset($DtoProducer->telephone_number)) {
            $Producer->telephone_number = $DtoProducer->telephone_number;
        }
        if (isset($DtoProducer->email)) {
            $Producer->email = $DtoProducer->email;
        }
        if (isset($DtoProducer->online)) {
            $Producer->online = $DtoProducer->online;
        }

        return $Producer;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get
     *
     * @param int id
     *
     * @return Producer
     */
    public static function get(int $id)
    {
        return Producer::find($id);
    }


    /**
     * Get dto
     *
     * @param int id
     * @param int idLanguage
     *
     * @return DtoItem
     */
    public static function getDto(int $id, int $idLanguage)
    {
        $dtoItem = new DtoItem();
        $dtoItem->id = $id;
        $item = Item::find($id);

        if (!is_null($item)) {
            $dtoItem->internalCode = $item->internal_code;
            $dtoItem->description = ItemLanguageBL::getByItemAndLanguage($id, $idLanguage);
        }

        return $dtoItem;
    }


    /**
     * Get by id
     * 
     * @param int id
     * @return DtoProducer
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Producer::find($id));
    }

    /**
     * Get select
     *
     * @param String $search
     * @return Producer
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return Producer::select('id', 'business_name as description')->where('online', 'true')->orderBy('business_name', 'ASC')->get();
        } else {
            return Producer::where('business_name', 'ilike', $search . '%')->where('online', 'true')->select('id', 'business_name as description')->orderBy('business_name', 'ASC')->get();
        }
    }

    /**
     * Get select
     *
     * @param String $search
     * @param String $url
     * @return Producer
     */
    public static function getSelectUserShop(string $search, string $url, string $description, string $priceFrom, string $priceTo, string $condition, string $category)
    {
        $strWhere = "";
        if ($description != "") {
            $strWhere = $strWhere . ' AND items.description ilike \'' . $description . '%\'';
        }

        if ($priceFrom != "") {
            $strWhere = $strWhere . ' AND items.price ilike >= ' . $priceFrom;
        }

        if ($priceTo != "") {
            $strWhere = $strWhere . ' AND items.price ilike <= ' . $priceTo;
        }

        if ($condition != "") {
            $strWhere = $strWhere . ' AND items.new = \'' . $condition . '\'';
        }


        /*if($category!=""){
            $strWhere = $strWhere . ' AND items.new = \'' . $condition . '\''; 
        }*/
        $idUser = UsersDatas::select('user_id')->where('link', $url)->get()->first()->user_id;

        if ($search == "*" || $search == '') {
            return collect(DB::select(
                "SELECT distinct p1.id, p1.business_name || ' (' ||
                    (select count(items.id) as nItem 
                    from items
                    where items.created_id = :idUser and items.producer_id = p1.id) 
                || ') '  as description,
                (select count(items.id) as nItem 
                    from items
                    where items.created_id = :idUser and items.producer_id = p1.id) as cont
                from producer as p1
                inner join items on items.producer_id = p1.id
                where items.created_id = :idUser " . $strWhere . ' ORDER BY cont DESC',
                ['idUser' => $idUser]
            ));
        } else {
            return collect(DB::select(
                "SELECT distinct p1.id, p1.business_name || ' (' ||
                    (select count(items.id) as nItem 
                    from items
                    where items.created_id = :idUser and items.producer_id = p1.id) 
                || ') '  as description,
                (select count(items.id) as nItem 
                    from items
                    where items.created_id = :idUser and items.producer_id = p1.id) as cont
                from producer as p1
                inner join items on items.producer_id = p1.id
                where items.created_id = :idUser AND p1.business_name ilike '" . $search . "%' " . $strWhere . ' ORDER BY cont DESC', 
                ['idUser' => $idUser]
            ));
        }
    }

    /**
     * Get select
     *
     * @param String $search
     * @param String $url
     * @return Producer
     */
    public static function getSelectConditionUserShop(string $search, string $url, string $description, string $priceFrom, string $priceTo, string $producer, string $category)
    {
        $strWhere = "";
        if ($description != "") {
            $strWhere = $strWhere . ' AND it.description ilike \'' . $description . '%\'';
        }

        if ($priceFrom != "") {
            $strWhere = $strWhere . ' AND it.price ilike >= ' . $priceFrom;
        }

        if ($priceTo != "") {
            $strWhere = $strWhere . ' AND it.price ilike <= ' . $priceTo;
        }

        if ($producer != "") {
            $strWhere = $strWhere . ' AND it.producer_id = \'' . $producer . '\'';
        }

        $idUser = UsersDatas::select('user_id')->where('link', $url)->get()->first()->user_id;

        if ($search == "*" || $search == '') {
            return collect(DB::select(
                "SELECT distinct it.new as id, it.new || ' (' ||
                    (select count(it1.id) as nItem 
                    from items as it1
                    where it1.created_id = :idUser and it.new = it1.new) 
                || ') '  as description
                from items as it
                where it.created_id = :idUser " . $strWhere,
                ['idUser' => $idUser]
            ));
        } else {
            return collect(DB::select(
                "SELECT distinct it.new as id, it.new || ' (' ||
                (select count(it1.id) as nItem 
                from items as it1
                where it1.created_id = :idUser and it.new = it1.new) 
                || ') '  as description
                from items as it
                where it.created_id = :idUser AND it.new ilike '" . $search . "%' " . $strWhere,
                ['idUser' => $idUser]
            ));
        }
    }



    /**
     * Get select
     *
     * @param String $search
     * @param String $url
     * @return Producer
     */
    public static function getSelectCategoriesFilter(string $search, string $url, string $priceFrom, string $priceTo, string $condition)
    {
        $strWhere = "";
        
        if($priceFrom!=""){
            $strWhere = $strWhere . ' AND it.price ilike >= ' . $priceFrom; 
        }

        if($priceTo!=""){
            $strWhere = $strWhere . ' AND it.price ilike <= ' . $priceTo; 
        }

        if($condition!=""){
            $strWhere = $strWhere . ' AND it.new = \'' . $condition . '\''; 
        }

        $idCategories = CategoryLanguage::select('category_id')->where('meta_tag_link', $url)->get()->first()->category_id;
        
        if ($search == "*" || $search=='') {
            return collect(DB::select(
                "SELECT distinct p1.id, p1.business_name || ' (' ||
                    (select count(items.id) as nItem 
                    from items
                    inner join categories_items on items.id = categories_items.item_id
                    where categories_items.category_id  = :idCategories and items.producer_id = p1.id) 
                || ') '  as description,
                (select count(items.id) as nItem 
                    from items
                    inner join categories_items on items.id = categories_items.item_id
                    where categories_items.category_id  = :idCategories and items.producer_id = p1.id) as cont
                from producer as p1
                inner join items on items.producer_id = p1.id
                inner join categories_items on items.id = categories_items.item_id
                where categories_items.category_id = :idCategories " . $strWhere .  ' ORDER BY cont DESC',
                ['idCategories' => $idCategories]
            ));
        } else {
            return collect(DB::select(
                "SELECT distinct p1.id, p1.business_name || ' (' ||
                    (select count(items.id) as nItem 
                    from items
                    inner join categories_items on items.id = categories_items.item_id
                    where categories_items.category_id  = :idCategories and items.producer_id = p1.id) 
                || ') '  as description,
                (select count(items.id) as nItem 
                    from items
                    inner join categories_items on items.id = categories_items.item_id
                    where categories_items.category_id  = :idCategories and items.producer_id = p1.id) as cont
                from producer as p1
                inner join items on items.producer_id = p1.id
                inner join categories_items on items.id = categories_items.item_id
                where categories_items.category_id = :idCategories AND p1.business_name ilike '" . $search . "%' " . $strWhere .  ' ORDER BY cont DESC',
                ['idCategories' => $idCategories]
            ));
        }
    }

    /**
     * Get select condition
     *
     * @param String $search
     * @param String $url
     * @return Producer
     */
    public static function getSelectConditionCategoriesFilter(string $search, string $url, string $priceFrom, string $priceTo, string $producer)
    {
        $strWhere = "";
        
        if($priceFrom!=""){
            $strWhere = $strWhere . ' AND it.price ilike >= ' . $priceFrom; 
        }

        if($priceTo!=""){
            $strWhere = $strWhere . ' AND it.price ilike <= ' . $priceTo; 
        }

        if($producer!=""){
            $strWhere = $strWhere . ' AND it.producer_id = \'' . $producer . '\''; 
        }

        $idCategories = CategoryLanguage::select('category_id')->where('meta_tag_link', $url)->get()->first()->category_id;
        
        if ($search == "*" || $search=='') {
            return collect(DB::select(
                "SELECT distinct it.new as id, it.new || ' (' ||
                    (select count(it1.id) as nItem 
                    from items as it1
                    inner join categories_items on it1.id = categories_items.item_id
                    where categories_items.category_id = :idCategories and it.new = it1.new) 
                || ') '  as description
                from items as it
                inner join categories_items on it.id = categories_items.item_id
                where categories_items.category_id = :idCategories " . $strWhere,
                ['idCategories' => $idCategories]
            ));
        } else {
            return collect(DB::select(
                "SELECT distinct it.new as id, it.new || ' (' ||
                    (select count(it1.id) as nItem 
                    from items as it1
                    inner join categories_items on it1.id = categories_items.item_id
                    where categories_items.category_id = :idCategories and it.new = it1.new) 
                || ') '  as description
                from items as it
                inner join categories_items on it.id = categories_items.item_id
                where categories_items.category_id = :idCategories AND it.new ilike '" . $search . "%' " . $strWhere,
                ['idCategories' => $idCategories]
            ));
        }
    }



    /**
     * Get all
     * 
     * @return DtoProducer
     */
    public static function getAll($idLanguage)
    {
        $result = collect();

        foreach (Producer::where('language_id', $idLanguage)->get() as $ProducerAll) {

            $DtoProducer = new DtoProducer();
            if (isset($ProducerAll->postal_code_id)) {
                $descriptionPostalCode = PostalCode::where('id', $ProducerAll->postal_code_id)->first()->code;

                $IdPostalCode = PostalCode::where('id', $ProducerAll->postal_code_id)->first()->id;
            }

            //TABELLA

            if (isset($ProducerAll->id)) {
                $DtoProducer->id = $ProducerAll->id;
            }
            if (isset($ProducerAll->code)) {
                $DtoProducer->Code = $ProducerAll->code;
            }

            if (isset($ProducerAll->business_name)) {
                $DtoProducer->BusinessName = $ProducerAll->business_name;
            }

            if (isset($ProducerAll->address)) {
                $DtoProducer->Address = $ProducerAll->address;
            }

            if (isset($ProducerAll->resort)) {
                $DtoProducer->Resort = $ProducerAll->resort;
            }
            if (isset($ProducerAll->province)) {
                $DtoProducer->Province = $ProducerAll->province;
            }

            if (isset($descriptionPostalCode)) {
                $DtoProducer->PostalCode = $descriptionPostalCode;
            }

            //if (isset($ProducerAll->postal_code_id)) {
            // $DtoProducer->PostalCode = $ProducerAll->postal_code_id;
            // }

            if (isset($ProducerAll->email)) {
                $DtoProducer->Email = $ProducerAll->email;
            }

            // DETTAGLIO


            if (isset($ProducerAll->code)) {
                $DtoProducer->code = $ProducerAll->code;
            }

            if (isset($ProducerAll->business_name)) {
                $DtoProducer->business_name = $ProducerAll->business_name;
            }
            if (isset($ProducerAll->address)) {
                $DtoProducer->address = $ProducerAll->address;
            }
            if (isset($ProducerAll->resort)) {
                $DtoProducer->resort = $ProducerAll->resort;
            }
            if (isset($ProducerAll->province)) {
                $DtoProducer->province = $ProducerAll->province;
            }

            if (isset($IdPostalCode)) {
                $DtoProducer->postal_code_id = $IdPostalCode;
            }

            if (isset($ProducerAll->telephone_number)) {
                $DtoProducer->telephone_number = $ProducerAll->telephone_number;
            }
            if (isset($ProducerAll->email)) {
                $DtoProducer->email = $ProducerAll->email;
            }
            if (isset($ProducerAll->online)) {
                $DtoProducer->online = $ProducerAll->online;
            }


            $result->push($DtoProducer);
        }

        return $result;
    }

    #endregion GET

    #region UPDATE

    /**
     * Set completed
     * 
     * @param Request request
     */
    public static function complete(Request $request, int $idUser, int $idLanguage)
    {
        if (is_null($request->idItem)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        $item = Item::find($request->idItem);

        if (is_null($item)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ItemNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $item->complete_technical_specification = $request->complete;

        $item->save();
    }

    #endregion UPDATE

    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoProducer
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoProducer, int $idUser)
    {

        DB::beginTransaction();

        try {

            $Producer = new Producer();

            if (isset($DtoProducer->code)) {
                $Producer->code = $DtoProducer->code;
            }
            if (isset($DtoProducer->business_name)) {
                $Producer->business_name = $DtoProducer->business_name;
            }

            if (isset($DtoProducer->address)) {
                $Producer->address = $DtoProducer->address;
            }
            if (isset($DtoProducer->resort)) {
                $Producer->resort = $DtoProducer->resort;
            }
            if (isset($DtoProducer->province)) {
                $Producer->province = $DtoProducer->province;
            }
            if (isset($DtoProducer->postal_code_id)) {
                $Producer->postal_code_id = $DtoProducer->postal_code_id;
            }

            if (isset($DtoProducer->telephone_number)) {
                $Producer->telephone_number = $DtoProducer->telephone_number;
            }

            if (isset($DtoProducer->email)) {
                $Producer->email = $DtoProducer->email;
            }
            if (isset($DtoProducer->online)) {
                $Producer->online = $DtoProducer->online;
            }

            if (isset($DtoProducer->idLanguage)) {
                $Producer->language_id = $DtoProducer->idLanguage;

                $Producer->save();
            }


            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $Producer->id;
    }

    /**
     * Clone
     * 
     * @param int id
     * @param int idFunctionality
     * @param int idLanguage
     * @param int idUser
     * 
     * @return int
     */
    public static function clone(int $id, int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $Producer =  Producer::find($id);

        if (is_null($Producer)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        DB::beginTransaction();

        try {

            $newProducer = new Producer();

            $newProducer->language_id = $Producer->language_id;
            $newProducer->code = $Producer->code;
            $newProducer->business_name = $Producer->business_name . ' - Copy';
            $newProducer->address = $Producer->address;
            $newProducer->resort = $Producer->resort;
            $newProducer->province = $Producer->province;
            $newProducer->postal_code_id = $Producer->postal_code_id;
            $newProducer->online = $Producer->online;
            $newProducer->email = $Producer->email;
            $newProducer->telephone_number = $Producer->telephone_number;

            $newProducer->save();

            DB::commit();

            return $newProducer->id;
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion clone

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoCategories
     * @param int idLanguage
     */

    public static function update(Request $DtoProducer)
    {

        static::_validateData($DtoProducer, $DtoProducer->idLanguage, DbOperationsTypesEnum::UPDATE);

        $ProductOnDb = Producer::find($DtoProducer->id);

        DB::beginTransaction();
        try {
            //dettaglio
            $ProductOnDb->code = $DtoProducer->code;
            $ProductOnDb->language_id = $DtoProducer->idLanguage;
            $ProductOnDb->business_name = $DtoProducer->business_name;
            $ProductOnDb->address = $DtoProducer->address;
            $ProductOnDb->resort = $DtoProducer->resort;
            $ProductOnDb->province = $DtoProducer->province;
            $ProductOnDb->postal_code_id = $DtoProducer->postal_code_id;
            $ProductOnDb->telephone_number = $DtoProducer->telephone_number;
            $ProductOnDb->email = $DtoProducer->email;
            $ProductOnDb->online = $DtoProducer->online;
            $ProductOnDb->save();


            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }


    #endregion UPDATE

    #region DELETE
    /**
     * Delete
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $Producer = Producer::find($id);


        if (is_null($Producer)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($Producer)) {

            DB::beginTransaction();

            try {


                Producer::where('id', $id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }

    #endregion DELETE
}

<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoContact;
use App\DtoModel\DtoCustomer;
use App\DtoModel\DtoGroupNewsletter;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\NewsletterRecipient;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NewsletterRecipientBL
{
    #region PRIVATE

    #region CONTACT

    /**
     * Get Contact included by Newsletter
     * 
     * @param int idNewsletter
     * 
     * @param DtoContact
     */
    private static function _getContactIncludedByNewsletter(int $idNewsletter)
    {
        $result = collect();

        foreach (NewsletterRecipient::where('newsletter_id', $idNewsletter)->whereNotNull('contact_id')->get() as $contactNewsletter) {
            $contact = ContactBL::getById($contactNewsletter->contact_id);

            $dtoContact = new DtoContact();
            $dtoContact->id = $contactNewsletter->id;
            $dtoContact->name = $contact->name;
            $dtoContact->surname = $contact->surname;
            if (isset($contact->businessName)) {
                $dtoContact->businessName = $contact->businessName;
            }
            $result->push($dtoContact);
        }

        return $result;
    }

    /**
     * Get Contact excluded by Newsletter
     * 
     * @param int idNewsletter
     * 
     * @param DtoContact
     */
    private static function _getContactExcludedByNewsletter(int $idNewsletter)
    {
        $result = collect();

        foreach (DB::select(
            'SELECT *
            FROM contacts
            WHERE id NOT IN (SELECT COALESCE(contact_id, 0) FROM newsletters_recipients WHERE newsletter_id = :idNewsletter)
            AND send_newsletter = true
            AND error_newsletter = false',
            ['idNewsletter' => $idNewsletter]
        )     as $contact) {
            $dtoContact = new DtoContact();
            $dtoContact->id = $contact->id;
            $dtoContact->name = $contact->name;
            $dtoContact->surname = $contact->surname;
            if (isset($contact->business_name)) {
                $dtoContact->businessName = $contact->business_name;
            }
            $result->push($dtoContact);
        }

        return $result;
    }

    #endregion CONTACT

    #region CUSTOMER

    /**
     * Get Customer included by Newsletter
     * 
     * @param int idNewsletter
     * 
     * @param DtoCustomer
     */
    private static function _getCustomerIncludedByNewsletter(int $idNewsletter)
    {
        $result = collect();

        foreach (NewsletterRecipient::where('newsletter_id', $idNewsletter)->whereNotNull('customer_id')->get() as $customerNewsletter) {
            $customer = CustomerBL::getById($customerNewsletter->customer_id);

            $dtoCustomer = new DtoCustomer();
            $dtoCustomer->id = $customerNewsletter->id;
            $dtoCustomer->name = $customer->name;
            $dtoCustomer->surname = $customer->surname;
            if (isset($customer->businessName)) {
                $dtoCustomer->businessName = $customer->businessName;
            }
            $result->push($dtoCustomer);
        }

        return $result;
    }

    /**
     * Get Customer excluded by Newsletter
     * 
     * @param int idNewsletter
     * 
     * @param DtoCustomer
     */
    private static function _getCustomerExcludedByNewsletter(int $idNewsletter)
    {
        $result = collect();

        foreach (DB::select(
            'SELECT *
            FROM customers
            WHERE id NOT IN (SELECT COALESCE(customer_id, 0) FROM newsletters_recipients WHERE newsletter_id = :idNewsletter)
            AND send_newsletter = true
            AND error_newsletter = false',
            ['idNewsletter' => $idNewsletter]
        )     as $customer) {
            $dtoCustomer = new DtoCustomer();
            $dtoCustomer->id = $customer->id;
            $dtoCustomer->name = $customer->name;
            $dtoCustomer->surname = $customer->surname;
            if (isset($customer->business_name)) {
                $dtoCustomer->businessName = $customer->business_name;
            }
            $result->push($dtoCustomer);
        }

        return $result;
    }

    #endregion CUSTOMER

    #region GROUP

    /**
     * Get GroupNewsletter included by Newsletter
     * 
     * @param int idNewsletter
     * 
     * @param DtoCustomer
     */
    private static function _getGroupIncludedByNewsletter(int $idNewsletter)
    {
        $result = collect();

        foreach (NewsletterRecipient::where('newsletter_id', $idNewsletter)->whereNotNull('group_newsletter_id')->groupBy('group_newsletter_id')->select(
            'group_newsletter_id'
        )->get() as $idGroupNewsletter) {
            $group = GroupNewsletterBL::getById($idGroupNewsletter->group_newsletter_id);

            $dtoGroupNewsletter = new DtoGroupNewsletter();
            $dtoGroupNewsletter->id = $group->id;
            $dtoGroupNewsletter->description = $group->description;
            $result->push($dtoGroupNewsletter);
        }

        return $result;
    }

    /**
     * Get GroupNewsletter excluded by Newsletter
     * 
     * @param int idNewsletter
     * 
     * @param DtoCustomer
     */
    private static function _getGroupExcludedByNewsletter(int $idNewsletter)
    {
        $result = collect();

        foreach (DB::select(
            'SELECT *
            FROM groups_newsletters
            WHERE id NOT IN (SELECT COALESCE(group_newsletter_id, 0) FROM newsletters_recipients WHERE newsletter_id = :idNewsletter)',
            ['idNewsletter' => $idNewsletter]
        )     as $group) {
            $dtoGroupNewsletter = new DtoGroupNewsletter();
            $dtoGroupNewsletter->id = $group->id;
            $dtoGroupNewsletter->description = $group->description;
            $result->push($dtoGroupNewsletter);
        }

        return $result;
    }

    #endregion GROUP

    #endregion PRIVATE

    #region GET

    /**
     * Get Contact by Newsletter
     * 
     * @param int idNewsletter
     * 
     * @return DtoIncludedExcluded
     */
    public static function getContactByNewsletter(int $idNewsletter)
    {
        $result = new DtoIncludedExcluded();
        $result->included = static::_getContactIncludedByNewsletter($idNewsletter);
        $result->excluded = static::_getContactExcludedByNewsletter($idNewsletter);
        return $result;
    }

    /**
     * Get Customer by Newsletter
     * 
     * @param int idNewsletter
     * 
     * @return DtoIncludedExcluded
     */
    public static function getCustomerByNewsletter(int $idNewsletter)
    {
        $result = new DtoIncludedExcluded();
        $result->included = static::_getCustomerIncludedByNewsletter($idNewsletter);
        $result->excluded = static::_getCustomerExcludedByNewsletter($idNewsletter);
        return $result;
    }

    /**
     * Get Group by Newsletter
     * 
     * @param int idNewsletter
     * 
     * @return DtoIncludedExcluded
     */
    public static function getGroupByNewsletter(int $idNewsletter)
    {
        $result = new DtoIncludedExcluded();
        $result->included = static::_getGroupIncludedByNewsletter($idNewsletter);
        $result->excluded = static::_getGroupExcludedByNewsletter($idNewsletter);
        return $result;
    }

    /**
     * Get by id
     * 
     * @param int idNewsletter
     * 
     * @return NewsletterRecipient
     */
    public static function getById(int $id)
    {
        return NewsletterRecipient::find($id);
    }


    /**
     * Get number recipients
     *
     * @param Int id
     *
     * @return Newsletter
     */
    public static function getNumberRecipients($id)
    {
        $totalCount = 0;
        $countContact = NewsletterRecipient::where('newsletter_id', $id)->whereNotNull('contact_id')->count();
        $countCustomer = NewsletterRecipient::where('newsletter_id', $id)->whereNotNull('customer_id')->count();
        $totalCount = $countContact + $countCustomer;

        return $totalCount;
    }


    #endregion GET

    #region INSERT

    /**
     * Insert contact
     * 
     * @param Request request
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertContact(Request $request, int $idLanguage)
    {
        #region VALIDATE

        if (is_null($request->idContact)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        $contact = ContactBL::getById($request->idContact);

        if (is_null($contact)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->idNewsletter)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NewsletterNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(NewsletterBL::getById($request->idNewsletter))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NewsletterNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        #endregion VALIDATE

        $newsletterRecipient = new NewsletterRecipient();
        $newsletterRecipient->contact_id = $contact->id;
        $newsletterRecipient->newsletter_id = $request->idNewsletter;
        if (isset($contact->business_name)) {
            $newsletterRecipient->name = $contact->name . ' ' . $contact->surname . ' - ' . $contact->business_name;
        } else {
            $newsletterRecipient->name = $contact->name . ' ' . $contact->surname;
        }
        $newsletterRecipient->email = $contact->email;
        $newsletterRecipient->save();

        return $newsletterRecipient->id;
    }

    /**
     * Insert customer
     * 
     * @param Request request
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertCustomer(Request $request, int $idLanguage)
    {
        #region VALIDATE

        if (is_null($request->idCustomer)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        $customer = CustomerBL::getById($request->idCustomer);

        if (is_null($customer)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->idNewsletter)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NewsletterNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(NewsletterBL::getById($request->idNewsletter))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NewsletterNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        #endregion VALIDATE

        $newsletterRecipient = new NewsletterRecipient();
        $newsletterRecipient->customer_id = $customer->id;
        $newsletterRecipient->newsletter_id = $request->idNewsletter;
        if (isset($customer->business_name)) {
            $newsletterRecipient->name = $customer->name . ' ' . $customer->surname . ' - ' . $customer->business_name;
        } else {
            $newsletterRecipient->name = $customer->name . ' ' . $customer->surname;
        }
        $newsletterRecipient->email = $customer->email;
        $newsletterRecipient->save();

        return $newsletterRecipient->id;
    }

    /**
     * Insert group
     * 
     * @param Request request
     * @param int idLanguage
     */
    public static function insertGroup(Request $request, int $idLanguage)
    {
        #region VALIDATE

        if (is_null($request->idGroupNewsletter)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupNewsletterNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        $groupNewsletter = GroupNewsletterBL::getById($request->idGroupNewsletter);

        if (is_null($groupNewsletter)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupNewsletterNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->idNewsletter)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NewsletterNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(NewsletterBL::getById($request->idNewsletter))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NewsletterNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        #endregion VALIDATE

        #region CONTACT

        foreach (DB::select(
            'SELECT contacts.id, contacts.name, contacts.surname, contacts.business_name, contacts.email
            FROM contacts
            INNER JOIN contacts_groups_newsletters ON contacts.id = contacts_groups_newsletters.contact_id
            WHERE contacts_groups_newsletters.group_newsletter_id = :idGroupNewsletter
            AND contacts.id NOT IN (SELECT COALESCE(contact_id, 0) FROM newsletters_recipients WHERE newsletter_id = :idNewsletter)
            AND contacts.send_newsletter = true
            AND contacts.error_newsletter = false',
            ['idGroupNewsletter' => $request->idGroupNewsletter, 'idNewsletter' => $request->idNewsletter]
        ) as $contact) {
            $newsletterRecipient = new NewsletterRecipient();
            $newsletterRecipient->contact_id = $contact->id;
            $newsletterRecipient->group_newsletter_id = $request->idGroupNewsletter;
            $newsletterRecipient->newsletter_id = $request->idNewsletter;
            if (isset($contact->business_name)) {
                $newsletterRecipient->name = $contact->name . ' ' . $contact->surname . ' - ' . $contact->business_name;
            } else {
                $newsletterRecipient->name = $contact->name . ' ' . $contact->surname;
            }
            $newsletterRecipient->email = $contact->email;
            $newsletterRecipient->save();
        }

        #endregion CONTACT

        #region CUSTOMER

        foreach (DB::select(
            'SELECT customers.id, customers.name, customers.surname, customers.business_name, customers.email
            FROM customers
            INNER JOIN customers_groups_newsletters ON customers.id = customers_groups_newsletters.customer_id
            WHERE customers_groups_newsletters.group_newsletter_id = :idGroupNewsletter
            AND customers.id NOT IN (SELECT COALESCE(customer_id, 0) FROM newsletters_recipients WHERE newsletter_id = :idNewsletter)
            AND customers.send_newsletter = true
            AND customers.error_newsletter = false',
            ['idGroupNewsletter' => $request->idGroupNewsletter, 'idNewsletter' => $request->idNewsletter]
        ) as $customer) {
            $newsletterRecipient = new NewsletterRecipient();
            $newsletterRecipient->customer_id = $customer->id;
            $newsletterRecipient->group_newsletter_id = $request->idGroupNewsletter;
            $newsletterRecipient->newsletter_id = $request->idNewsletter;
            $newsletterRecipient->name = $customer->name . ' ' . $customer->surname . ' - ' . $customer->business_name;
            $newsletterRecipient->email = $customer->email;
            $newsletterRecipient->save();
        }

        #endregion CUSTOMER
    }

    #endregion INSERT

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        #region VALIDATE

        if (is_null($id)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        $newsletterRecipient = NewsletterRecipient::find($id);

        if (is_null($newsletterRecipient)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NewsletterRecipientNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        #endregion VALIDATE

        $newsletterRecipient->delete();
    }

    /**
     * Delete
     * 
     * @param int idNewsletter
     * @param int idGroup
     * @param int idLanguage
     */
    public static function deleteGroup(int $idNewsletter, int $idGroup, int $idLanguage)
    {
        #region VALIDATE

        if (is_null($idGroup)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        $newsletter = NewsletterBL::getById($idNewsletter);

        if (is_null($newsletter)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NewsletterNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $groupNewsletter = GroupNewsletterBL::getById($idGroup);

        if (is_null($groupNewsletter)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupNewsletterNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        #endregion VALIDATE

        DB::beginTransaction();

        try {

            #region CONTACT

            DB::delete(
                'DELETE FROM newsletters_recipients
                WHERE newsletter_id = :idNewsletter
                AND group_newsletter_id = :idGroupNewsletter
                AND contact_id IS NOT NULL
                AND contact_id NOT IN
                    (SELECT COALESCE(contacts_groups_newsletters.contact_id, 0)
                    FROM groups_newsletters
                    INNER JOIN contacts_groups_newsletters ON groups_newsletters.id = contacts_groups_newsletters.group_newsletter_id
                    WHERE groups_newsletters.id IN 
                        (SELECT COALESCE(group_newsletter_id, 0)
                        FROM newsletters_recipients
                        WHERE newsletter_id = :idNewsletter
                        AND group_newsletter_id <> :idGroupNewsletter
                        AND group_newsletter_id IS NOT NULL
                        GROUP BY group_newsletter_id))',
                ['idNewsletter' => $idNewsletter, 'idGroupNewsletter' => $idGroup]
            );

            foreach (DB::select(
                'SELECT newsletters_recipients.id
                FROM newsletters_recipients
                WHERE newsletter_id = :idNewsletter
                AND group_newsletter_id = :idGroupNewsletter
                AND contact_id IS NOT NULL',
                ['idNewsletter' => $idNewsletter, 'idGroupNewsletter' => $idGroup]
            ) as $idNewsletterRecipient) {
                $newsletterRecipient = NewsletterRecipient::find($idNewsletterRecipient);
                $newsletterRecipient->group_newsletter_id = DB::select(
                    'SELECT id
                    FROM groups_newsletters
                    INNER JOIN contacts_groups_newsletters ON groups_newsletters.id = contacts_groups_newsletters.group_newsletter_id
                    WHERE contacts_groups_newsletters.contact_id = :idContact
                    AND groups_newsletters.id IN
                        (SELECT group_newsletter_id
                        FROM newsletters_recipients
                        WHERE newsletter_id = :idNewsletter
                        GROUP BY group_newsletter_id)
                    LIMIT 1',
                    ['idContact' => $newsletterRecipient->contact_id, 'idNewsletter' => $idNewsletter]
                );
                $newsletterRecipient->save();
            }

            #endregion CONTACT

            #region CUSTOMER

            DB::delete(
                'DELETE FROM newsletters_recipients
                WHERE newsletter_id = :idNewsletter
                AND group_newsletter_id = :idGroupNewsletter
                AND customer_id IS NOT NULL
                AND customer_id NOT IN
                    (SELECT COALESCE(customers_groups_newsletters.customer_id, 0)
                    FROM groups_newsletters
                    INNER JOIN customers_groups_newsletters ON groups_newsletters.id = customers_groups_newsletters.group_newsletter_id
                    WHERE groups_newsletters.id IN 
                        (SELECT COALESCE(group_newsletter_id, 0)
                        FROM newsletters_recipients
                        WHERE newsletter_id = :idNewsletter
                        AND group_newsletter_id <> :idGroupNewsletter
                        AND group_newsletter_id IS NOT NULL
                        GROUP BY group_newsletter_id))',
                ['idNewsletter' => $idNewsletter, 'idGroupNewsletter' => $idGroup]
            );

            foreach (DB::select(
                'SELECT newsletters_recipients.id
                FROM newsletters_recipients
                WHERE newsletter_id = :idNewsletter
                AND group_newsletter_id = :idGroupNewsletter
                AND customer_id IS NOT NULL',
                ['idNewsletter' => $idNewsletter, 'idGroupNewsletter' => $idGroup]
            ) as $idNewsletterRecipient) {
                $newsletterRecipient = NewsletterRecipient::find($idNewsletterRecipient);
                $newsletterRecipient->group_newsletter_id = DB::select(
                    'SELECT id
                    FROM groups_newsletters
                    INNER JOIN customers_groups_newsletters ON groups_newsletters.id = customers_groups_newsletters.group_newsletter_id
                    WHERE customers_groups_newsletters.customer_id = :idCustomer
                    AND groups_newsletters.id IN
                        (SELECT group_newsletter_id
                        FROM newsletters_recipients
                        WHERE newsletter_id = :idNewsletter
                        GROUP BY group_newsletter_id)
                    LIMIT 1',
                    ['idCustomer' => $newsletterRecipient->customer_id, 'idNewsletter' => $idNewsletter]
                );
                $newsletterRecipient->save();
            }

            #endregion CUSTOMER

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion DELETE
}

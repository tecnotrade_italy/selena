<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoLanguageMenu;
use App\DtoModel\DtoLanguagePageSetting;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\LanguageMenu;
use Mockery\CountValidator\Exception;

class LanguageMenuBL
{
    #region PRIVATE

    /**
     * Convert DtoLanguageMenu to LanguageMenu
     *
     * @param array dtoLanguageMenu
     * @param int idMenu
     * @return App\LanguageMenu
     */
    private static function _convertFromDto($dtoLanguageMenu, $idMenu)
    {
        $languageMenu = new LanguageMenu();
        $languageMenu->menu_id = $idMenu;
        $languageMenu->language_id = $dtoLanguageMenu->idLanguage;
        $languageMenu->description = $dtoLanguageMenu->title;
        $languageMenu->link = $dtoLanguageMenu->url;
        return $languageMenu;
    }


    /**
     * Convert DtoLanguageMenu to LanguageMenu
     *
     * @param array dtoLanguageMenu
     * @param int idMenu
     * @return App\LanguageMenu
     */
    private static function _convertFromDtoPages($dtoLanguageMenu, $idMenu)
    {
        $languageMenu = new LanguageMenu();
        $languageMenu->menu_id = $idMenu;
        $languageMenu->language_id = $dtoLanguageMenu->idLanguage;
        $languageMenu->description = $dtoLanguageMenu->title;
        $languageMenu->link = $dtoLanguageMenu->url;
        return $languageMenu;
    }

    #endregion PRIVATE

    /**
     * Convert DtoLanguagePageSetting to DtoLanguageMenu
     *
     * @param DtoLanguagePageSetting $dtoLanguagePageSetting
     * @return DtoLanguageMenu
     */
    public static function convertDtoLanguagePageSettingToDtoLanguageMenu($dtoLanguagePageSetting)

    {
        $languageMenu = new DtoLanguageMenu();
        $languageMenu->idLanguage = $dtoLanguagePageSetting['idLanguage'];
        $languageMenu->title = $dtoLanguagePageSetting['title'];
        $languageMenu->url = $dtoLanguagePageSetting['url'];
        return $languageMenu;
    }



    /**
     * Convert DtoLanguagePageSetting to DtoLanguageMenu
     *
     * @param DtoLanguagePageSetting $dtoLanguagePageSetting
     * @return DtoLanguageMenu
     */
    public static function convertDtoLanguagePageSettingToDtoLanguageMenuPages(DtoLanguagePageSetting $dtoLanguagePageSetting)
    {
        $languageMenu = new DtoLanguageMenu();
        $languageMenu->idLanguage = $dtoLanguagePageSetting->idLanguage;
        $languageMenu->title = $dtoLanguagePageSetting->title;
        $languageMenu->url = $dtoLanguagePageSetting->url;
        return $languageMenu;
    }

    #region INSERT OR UPDATE

    /**
     * Insert DtoLanguageMenu
     *
     * @param array dtoLanguageMenu
     * @param int idMenu
     * @return int language_menu_id
     */
    public static function insertOrUpdateDto($dtoLanguageMenu, $idMenu, $idLanguage)
    {
        $languageMenu = LanguageMenu::where(['language_id' => $dtoLanguageMenu->idLanguage, 'menu_id' => $idMenu]);
        $count = $languageMenu->count();

        if ($count > 1) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::MoreLanguageMenuForSameLanguageMenuFound), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if ($count) {
                $languageMenu = $languageMenu->first();
                DBHelper::updateAndSave(static::_convertFromDto($dtoLanguageMenu, $idMenu), $languageMenu);
            } else {
                $languageMenu = static::_convertFromDto($dtoLanguageMenu, $idMenu);
                $languageMenu->save();
            }
        }

        return $languageMenu->id;
    }

    #endregion INSERT OR UPDATE

    /**
     * Insert DtoLanguageMenu
     *
     * @param array dtoLanguageMenu
     * @param int idMenu
     * @return int language_menu_id
     */
    public static function insertOrUpdateDtoPages($dtoLanguageMenu, $idMenu, $idLanguage)
    {
        $languageMenu = LanguageMenu::where(['language_id' => $dtoLanguageMenu->idLanguage, 'menu_id' => $idMenu]);
        $count = $languageMenu->count();

        if ($count > 1) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::MoreLanguageMenuForSameLanguageMenuFound), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if ($count) {
                $languageMenu = $languageMenu->first();
                DBHelper::updateAndSave(static::_convertFromDtoPages($dtoLanguageMenu, $idMenu), $languageMenu);
            } else {
                $languageMenu = static::_convertFromDtoPages($dtoLanguageMenu, $idMenu);
                $languageMenu->save();
            }
        }

        return $languageMenu->id;
    }



    #region DELETE

    /**
     * Delete by idMenu
     * 
     * @param int idMenu
     */
    public static function deleteByIdMenu(int $idMenu)
    {
        LanguageMenu::where('menu_id', $idMenu)->delete();
    }

    #endregion DELETE
}

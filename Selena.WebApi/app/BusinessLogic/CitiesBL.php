<?php

namespace App\BusinessLogic;

use App\Contact;
use App\DtoModel\DtoCities;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Mail\ContactRequestMail;
use App\Cities;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CitiesBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Cities::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->order)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SurnameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->istat)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SurnameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

    }

    /**
     * Conver to model
     * 
     * @param Request request
     * 
     * @return Contact
     */
    private static function _convertToModel(Request $request)
    {
        $cities = new Cities();

        if (isset($request->id)) {
            $cities->id = $request->id;
        }

        if (isset($request->order)) {
            $cities->order = $request->order;
        }

        if (isset($request->description)) {
            $cities->description = $request->description;
        }

        if (isset($request->istat)) {
            $cities->istat = $request->istat;
        }

        return $cities;
    }

    /**
     * Convert to dto
     * 
     * @param Cities region
     * 
     * @return DtoCities
     */
    private static function _convertToDto(Cities $cities)
    {
        $dtoCities = new DtoCities();
        $dtoCities->id = $cities->id;
        $dtoCities->order = $cities->order;
        $dtoCities->description = $cities->description;
        $dtoCities->istat = $cities->istat;
        return $dtoCities;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get
     *
     * @param String $search
     * @return Cities
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return Cities::select('id', 'description')->get()->take(100);
        } else {
            return Cities::where('description', 'ilike','%'. $search . '%')->select('id', 'description')->get();
        }
    }

    /**
     * Get all
     * 
     * @return DtoCities
     */
    public static function getAll()
    {
        $result = collect();
        foreach (Cities::orderBy('order')->get() as $cities) {
       
            $dtoCities = new DtoCities();
            $dtoCities->id = $cities->id;
            $dtoCities->order = $cities->order;
            $dtoCities->description = $cities->description;
            $dtoCities->istat = $cities->istat;
            $result->push($dtoCities); 

        }
        return $result;

    } 

    /**
     * Get by id 
     * 
     * @param int id
     * 
     * @return DtoCities
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Cities::find($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);
        DB::beginTransaction();

        try {
            $cities = new Cities();
            $cities->order = $request->order;
            $cities->description = $request->description;
            $cities->istat = $request->istat;
            $cities->created_id = $request->$idUser;
            $cities->save();
           
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $cities->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoCities
     * @param int idLanguage
     */
    public static function update(Request $dtoCities, int $idLanguage)
    {
        static::_validateData($dtoCities, $idLanguage, DbOperationsTypesEnum::UPDATE);
        
        $citiesOnDb = Cities::find($dtoCities->id);
        DB::beginTransaction();

        try {
            $citiesOnDb->order = $dtoCities->order;
            $citiesOnDb->description = $dtoCities->description;
            $citiesOnDb->istat = $dtoCities->istat;
            $citiesOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE


    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {

        $cities = Cities::find($id);

        if (is_null($cities)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $cities->delete();
    }

    #endregion DELETE






    
}

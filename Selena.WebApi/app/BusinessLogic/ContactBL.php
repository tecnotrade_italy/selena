<?php

namespace App\BusinessLogic;

use App\Contact;
use App\ContactGroupNewsletter;
use App\ContactRequest;
use App\Customer;
use App\CustomerGroupNewsletter;
use App\DtoModel\DtoContact;
use App\DtoModel\DtoRequestContact;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\GroupNewsletter;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Import\ContactImport;
use App\Imports\ContactImport as ImportsContactImport;
use App\Mail\ContactRequestMail;
use App\Mail\DynamicMail;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class ContactBL
{
    #region PRIVATE
    private static $dirImage = '../storage/app/public/file/';

    /**
     * Validate data
     * 
     * @param Request dtoContact
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $dtoContact, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($dtoContact->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Contact::find($dtoContact->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($dtoContact->name)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

     //   if (is_null($dtoContact->surname)) {
          //  throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SurnameNotValued), HttpResultsCodesEnum::InvalidPayload);
      //  }
      

        if (is_null($dtoContact->email)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!filter_var($dtoContact->email, FILTER_VALIDATE_EMAIL)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailNotValid), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    /**
     * Conver to model
     * 
     * @param Request dtoContact
     * 
     * @return Contact
     */
    private static function _convertToModel(Request $dtoContact)
    {

      
        $contact = new Contact();

        if (isset($dtoContact->id)) {
            $contact->id = $dtoContact->id;
        }

        if (isset($dtoContact->name)) {
            $contact->name = $dtoContact->name;
        }

        if (isset($dtoContact->surname)) {
            $contact->surname = $dtoContact->surname;
        }

        if (isset($dtoContact->businessName)) {
            $contact->business_name = $dtoContact->businessName;
        }

        if (isset($dtoContact->email)) {
            $contact->email = $dtoContact->email;
        }

        if (isset($dtoContact->phoneNumber)) {
            $contact->phone_number = $dtoContact->phoneNumber;
        }else{
            $contact->phone_number = ''; 
        }

        if (isset($dtoContact->faxNumber)) {
            $contact->fax_number = $dtoContact->faxNumber;
        }else{
            $contact->fax_number = ''; 
        }
        
        if (isset($dtoContact->province)) {
            $contact->province = $dtoContact->province;
        }

        if (isset($dtoContact->postalCode)) {
            $contact->postal_code = $dtoContact->postalCode;
        }

        if (isset($dtoContact->group_sms_id)) {
            $contact->group_sms_id = $dtoContact->group_sms_id;
        }

        if (isset($dtoContact->vatNumber)) {
            $contact->vat_number = $dtoContact->vatNumber;
        }

        if (isset($dtoContact->sendNewsletter)) {
            $contact->send_newsletter = $dtoContact->sendNewsletter;
        }

        if (isset($dtoContact->errorNewsletter)) {
            $contact->error_newsletter = $dtoContact->errorNewsletter;
        }

        return $contact;
    }

    /**
     * Convert to dto
     * 
     * @param Contact contact
     * 
     * @return DtoContact
     */
    private static function _convertToDto(Contact $contact)
    {
        $dtoContact = new DtoContact();
        $dtoContact->id = $contact->id;
        $dtoContact->name = $contact->name;
        $dtoContact->surname = $contact->surname;
        $dtoContact->businessName = $contact->business_name;
        $dtoContact->email = $contact->email;
        $dtoContact->group_sms_id = $contact->group_sms_id;
        $dtoContact->phoneNumber = $contact->phone_number;
        $dtoContact->sendNewsletter = $contact->send_newsletter;
        $dtoContact->errorNewsletter = $contact->error_newsletter;
        return $dtoContact;
    }

    #endregion PRIVATE

    #region GET
    
    /**
     * Get select
     *
     * @param String $search
     * @return User
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return Contact::select('id', 'name as description')->all();
        } else {
            return Contact::where('name', 'ilike', $search . '%')->select('id', 'name as description')->get();
        }
    }



    /**
     * Get all request
     * 
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoRequestContact
     */
    public static function getAllRequest(int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $result = collect();

        foreach (ContactRequestBL::getAll() as $contactRequest) {
            $contact = Contact::find($contactRequest->contact_id);
            $dtoRequestContact = new DtoRequestContact();
            $dtoRequestContact->id = $contactRequest->id;
            $dtoRequestContact->name = $contact->name;
            $dtoRequestContact->surname = $contact->surname;
            $dtoRequestContact->email = $contact->email;
            $dtoRequestContact->date = Carbon::parse($contactRequest->created_at)->format(HttpHelper::getLanguageDateTimeFormat());
            $dtoRequestContact->description = $contactRequest->description;
            $result->push($dtoRequestContact);
        }

        return $result;
    }

    /**
     * Get all
     * 
     * @return DtoContact
     */
    public static function getAll()
    {
        return Contact::all()->map(function ($contact) {
            return static::_convertToDto($contact);
        });
    }

    /**
     * Get by id
     * 
     * @param int id
     * 
     * @return DtoContact
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Contact::find($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     * 
     * @param Request request
     */
    public static function insertRequest(Request $request, int $idLanguage)
    {
        $idLanguage = LanguageBL::getDefault()->id; 
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::RequestNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        DB::beginTransaction();

        try {

            $contact = new Contact();
            $contact->name = $request->name;
            $contact->surname = $request->surname;
            $contact->email = $request->email;
            $contact->send_newsletter = true;
            $contact->created_id = UserBL::getContactRequest();

            $contact->save();

            ContactRequestBL::insert($contact->id, $request->description);

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        Mail::to(SettingBL::getContactsEmail())->send(new ContactRequestMail($request->name, $request->surname, $request->email, $request->description));
    }

    /**
     * import Data
     * 
     * @param Request request
     * 
     * @return int
     */
    public static function importData(Request $request)
    {
        $data = Excel::load(static::$dirImage . 'test-import.xls')->get();
        if ($data->count() > 0) {
            foreach ($data->toArray() as $key => $value) {
                foreach ($value as $row) {
                    $contact = new Contact();
                    $contact->name = $row[0];
                    $contact->surname = $row[1];
                    $contact->businessName = $row[2];
                    $contact->email = $row[3];
                    $contact->phoneNumber = $row[3];
                    $contact->send_newsletter = $row[5];
                    $contact->created_id = UserBL::getContactRequest();
                    $contact->save();
                }
            }
        }
        return back();
    }

    /**
     * Insert
     * 
     * @param Request dtoContact
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $dtoContact, int $idLanguage)
    {
        static::_validateData($dtoContact, $idLanguage, DbOperationsTypesEnum::INSERT);

       /* if (Contact::where('email', $dtoContact->email)->count() > 0) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
        }*/

        if (is_null($dtoContact->email)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        $contact = static::_convertToModel($dtoContact);
        $contact->save();
        return $contact->id;
    }

    /**
     * Subscribe
     * 
     * @param Request dtoContact
     * @param int idLanguage
     * 
     * @return int
     */
    public static function subscribe(Request $dtoContact, int $idLanguage)
    {
        if (is_null($dtoContact->email)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        $contactOnDb = Contact::where('email', $dtoContact->email)->first();
        if (!is_null($contactOnDb)) {
          $contactOnDb->send_newsletter=true;
          $contactOnDb->save();
          $id = $contactOnDb->id;

        }else{
            $contact = new Contact();
            $contact->name = $dtoContact->email;
            $contact->surname = ' ';
            $contact->email = $dtoContact->email;
            $contact->send_newsletter = true;
            $contact->created_id = UserBL::getContactRequest();
            $contact->save();

            $contactRequest = new ContactRequest();
            $contactRequest->contact_id = $contact->id;
            if ($dtoContact->processing_of_personal_data == true) {
                $contactRequest->processing_of_personal_data = true;
            }else{
                $contactRequest->processing_of_personal_data = false; 
            }

           // $contactRequest->processing_of_personal_data = $dtoContact->processing_of_personal_data;
            $contactRequest->description = 'iscrizione newsletter';
            $contactRequest->save();

            $id = $contact->id;
        }


        $ContactGroupNewsletterOnDb= ContactGroupNewsletter::where('contact_id', $id)->first();
        if (!isset($ContactGroupNewsletterOnDb)) {
            $ContactGroupNewsletter = new ContactGroupNewsletter();
            $ContactGroupNewsletter->contact_id = $id;   
            $ContactGroupNewsletter->group_newsletter_id = GroupNewsletter::where('description', 'SubscribedToTheNewsletter')->first()->id; 
            $ContactGroupNewsletter->save();
        }

        //invio email 
                $emailFrom = SettingBL::getContactsEmail();
                $textMail= MessageBL::getByCode('SUBSCRIBE_CONFIRM_NEWSLETTER_MAIL_TEXT');
                $textObject= MessageBL::getByCode('SUBSCRIBE_CONFIRM_NEWSLETTER_MAIL_OBJECT'); 
                Mail::to($dtoContact->email)->send(new DynamicMail($textObject,$textMail));
    }
    #endregion INSERT

    #region UPDATE

    /**
     * Set error on newsletter
     * 
     * @param string email
     */
    public static function setNewsletterError(string $email)
    {

        $return = 0;
        $contact = Contact::where('email', $email)->first();
        if(isset($contact)){
            $contact->error_newsletter = true;
            $contact->save();
            $return =  $contact->id;
        }
        return $return;
    }

    /**
     * Update
     * 
     * @param Request dtoContact
     * @param int idLanguage
     */
    public static function update(Request $dtoContact, int $idLanguage)
    {
        static::_validateData($dtoContact, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $contactOnDb = Contact::find($dtoContact->id);
        DBHelper::updateAndSave(static::_convertToModel($dtoContact), $contactOnDb);
    }

    /**
     * unsubscribe
     * 
     * @param Request dtoContact
     * @param int idLanguage
     */
    public static function unsubscribe(Request $dtoContact, int $idLanguage)
    
    {
        if (is_null($dtoContact->email)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        $check = false;

        $contactOnDb = Contact::where('email', $dtoContact->email)->first();

        if (!is_null($contactOnDb)) {
            $contactGroup = ContactGroupNewsletter::where('contact_id', $contactOnDb->id)->first();

            if (!is_null($contactGroup)) {
                $contactGroup->delete();
            }

            $check = true;
            $contactOnDb->send_newsletter = false;
            $contactOnDb->save();
        }

        $customerOnDb = Customer::where('email', $dtoContact->email)->first();

        if (!is_null($customerOnDb)) {
            $customerGroup = CustomerGroupNewsletter::where('customer_id', $customerOnDb->id)->first();

            if (!is_null($customerGroup)) {
                $customerGroup->delete();
            }

            $check = true;
            $customerOnDb->send_newsletter = false;
            $customerOnDb->save();
        }

        if (!$check) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    /**
     * unsubscribeNewsletter
     * 
     * @param Request dtoContact
     * @param int idLanguage
     */
    public static function unsubscribeNewsletter(Request $dtoContact, int $idLanguage)
    {
        $result = '';

        if (is_null($dtoContact->email)) {
            $result = 'Email Non Valida'; 
          //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        $check = false;

        $contactOnDb = Contact::where('email', $dtoContact->email)->first();

        if (!is_null($contactOnDb)) {
            $contactGroup = ContactGroupNewsletter::where('contact_id', $contactOnDb->id)->first();

            if (!is_null($contactGroup)) {
                $contactGroup->delete();
            }

            $check = true;
            $contactOnDb->send_newsletter = false;
            $contactOnDb->save();
            
            $text= MessageBL::getByCode('UNSUBSCRIBERNEWSLETTER_TEXT', $idLanguage);
            $result = $text;
            //$result = 'La tua richiesta di disiscrizione è stata registrata correttamente: da questo momento in poi, non riceverai più la nostra newsletter. Nel caso in cui dovessi ricevere ancora email, è perché sono state pianificate prima della ricezione della tua richiesta di disiscrizione';
            
        }

        $customerOnDb = Customer::where('email', $dtoContact->email)->first();

        if (!is_null($customerOnDb)) {
            $customerGroup = CustomerGroupNewsletter::where('customer_id', $customerOnDb->id)->first();

            if (!is_null($customerGroup)) {
                $customerGroup->delete();
            }

            if (!is_null($customerOnDb)) {
                $customerOnDb->delete();
            }

            $check = true;
            $customerOnDb->send_newsletter = false;
            $customerOnDb->save();
            $text= MessageBL::getByCode('UNSUBSCRIBERNEWSLETTER_TEXT', $idLanguage);
            $result = $text;
                
            //$result = 'La tua richiesta di disiscrizione è stata registrata correttamente: da questo momento in poi, non riceverai più la nostra newsletter. Nel caso in cui dovessi ricevere ancora email, è perché sono state pianificate prima della ricezione della tua richiesta di disiscrizione';
         
        }

        if (!$check) {
             $result = 'Contatto Non Trovato';
            
            //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }         
        return $result;

     
    }
    

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $contact = Contact::find($id);

        if (is_null($contact)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $contact->delete();
    }

    #endregion DELETE
}

<?php

namespace App\BusinessLogic;

use Illuminate\Support\Facades\DB;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use App\Icon;

class IconBL
{
    #region GET

    /**
     * Get
     *
     * @param String $search
     * @return Icon
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return Icon::select('id', 'description','css')->all();
        } else {
            return Icon::where('description', 'ilike','%'. $search . '%')->select('id', 'description','css')->get();
        }
    }

    /**
     * Get by id
     *
     * @param int $id
     * @return Icon
     */
    public static function getById(int $id)
    {
        return Icon::find($id);
    }


    #endregion GET
}

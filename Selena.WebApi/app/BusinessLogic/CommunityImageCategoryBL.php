<?php

namespace App\BusinessLogic;

use App\Contact;
use App\DtoModel\DtoCommunityImageCategory;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\CommunityImageCategoryLanguage;
use App\Mail\ContactRequestMail;
use App\CommunityImageCategory;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CommunityImageCategoryBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {

    }

    /**
     * Conver to model
     * 
     * @param Request request
     * 
     * @return Contact
     */
    private static function _convertToModel(Request $request)
    {
        $communityImageCategory = new CommunityImageCategory();

        if (isset($request->id)) {
            $communityImageCategory->id = $request->id;
        }

        if (isset($request->order)) {
            $communityImageCategory->order = $request->order;
        }

        if (isset($request->online)) {
            $communityImageCategory->online = $request->online;
        }

        if (isset($request->description)) {
            $communityImageCategory->description = $request->description;
        }

        return $communityImageCategory;
    }

    /**
     * Convert to dto
     * 
     * @param CommunityImageCategory communityImageCategory
     * @param int idLanguage
     * 
     * @return DtoCommunityImageCategory
     */
    private static function _convertToDto(CommunityImageCategory $communityImageCategory)
    {
        $dtoCommunityImageCategory = new DtoCommunityImageCategory();
        $dtoCommunityImageCategory->id = $communityImageCategory->id;
        $dtoCommunityImageCategory->order = $communityImageCategory->order;
        $dtoCommunityImageCategory->online = $communityImageCategory->online;
        $dtoCommunityImageCategory->description = CommunityImageCategoryLanguage::where('image_category_id', $communityImageCategory->id)->first()->description;
        return $dtoCommunityImageCategory;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get
     *
     * @param String $search
     * @return CommunityImageCategoryLanguage
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return CommunityImageCategoryLanguage::select('image_category_id as id', 'description')->orderBy('description')->get();
        } else {
            return CommunityImageCategoryLanguage::where('description', 'ilike', $search . '%')->select('image_category_id as id', 'description')->orderBy('description')->get();
        }
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoCommunityImageCategory
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (CommunityImageCategoryLanguage::where('language_id',$idLanguage)->get() as $communityImageCategoryAll) {
            $dtoCommunityImageCategory = new DtoCommunityImageCategory();
            $dtoCommunityImageCategory->id = CommunityImageCategory::where('id', $communityImageCategoryAll->image_category_id)->first()->id;
            $dtoCommunityImageCategory->order = CommunityImageCategory::where('id', $communityImageCategoryAll->image_category_id)->first()->order;
            $dtoCommunityImageCategory->online = CommunityImageCategory::where('id', $communityImageCategoryAll->image_category_id)->first()->online;
            $dtoCommunityImageCategory->description = $communityImageCategoryAll->description;
            $result->push($dtoCommunityImageCategory); 
        }
        return $result;
    }

    /**
     * Get by id
     * 
     * @param int id
     * 
     * @return DtoCommunityImageCategory
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(CommunityImageCategory::find($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);
        //$communityImageCategoryModel = static::_convertToModel($request);
        DB::beginTransaction();

        try {
            $communityImageCategory = new CommunityImageCategory();
            $communityImageCategory->order = $request->order;
            $communityImageCategory->online = $request->online;
            $communityImageCategory->created_id = $request->$idUser;
            $communityImageCategory->save();

            $languageCommunityImageCategory = new CommunityImageCategoryLanguage();
            $languageCommunityImageCategory->language_id = $idLanguage;
            $languageCommunityImageCategory->image_category_id = $communityImageCategory->id;
            $languageCommunityImageCategory->description = $request->description;
            $languageCommunityImageCategory->created_id = $request->$idUser;
            $languageCommunityImageCategory->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $communityImageCategory->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoCommunityImageCategory
     * @param int idLanguage
     */
    public static function update(Request $dtoCommunityImageCategory, int $idLanguage)
    {
        static::_validateData($dtoCommunityImageCategory, $idLanguage, DbOperationsTypesEnum::UPDATE);
        
        $communityImageCategoryOnDb = CommunityImageCategory::find($dtoCommunityImageCategory->id);
        $languageCommunityImageCategoryOnDb = CommunityImageCategoryLanguage::where('image_category_id', $dtoCommunityImageCategory->id)->first();
        
        DB::beginTransaction();

        try {
            $communityImageCategoryOnDb->order = $dtoCommunityImageCategory->order;
            $communityImageCategoryOnDb->online = $dtoCommunityImageCategory->online;
            $communityImageCategoryOnDb->save();

            $languageCommunityImageCategoryOnDb->language_id = $idLanguage;
            $languageCommunityImageCategoryOnDb->image_category_id = $dtoCommunityImageCategory->id;
            $languageCommunityImageCategoryOnDb->description = $dtoCommunityImageCategory->description;
            $languageCommunityImageCategoryOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $languageCommunityImageCategory = CommunityImageCategoryLanguage::where('image_category_id', $id)->first();

        if (is_null($languageCommunityImageCategory)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $languageCommunityImageCategory->delete();

        $communityImageCategory = CommunityImageCategory::find($id);

        if (is_null($communityImageCategory)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $communityImageCategory->delete();
    }

    #endregion DELETE
}

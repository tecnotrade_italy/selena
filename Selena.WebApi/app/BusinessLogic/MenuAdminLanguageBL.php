<?php

namespace App\BusinessLogic;

use App\MenuAdmin;
use App\MenuAdminLanguage;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\UsersDatas;
use Exception;
use Illuminate\Support\Facades\DB;

class MenuAdminLanguageBL
{

    public static function getByCodeAndLanguage(int $idMenuAdmin, int $idLanguageMenuAdmin, int $idFunctionality = null, int $idUser = null, int $idLanguage = null)
    {
        if (!is_null($idFunctionality)) {
            FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
        }

        $MenuAdmin = '';
        $MenuAdminLanguage = MenuAdminLanguage::where('menu_admin_id', $idMenuAdmin)->where('language_id', $idLanguageMenuAdmin)->first();

        if (is_null($MenuAdminLanguage)) {
            $language = LanguageBL::getDefault();
            if ($language->id != $idLanguageMenuAdmin) {
                $MenuAdminLanguage = MenuAdminLanguage::where('menu_admin_id', $idMenuAdmin)->where('language_id', $language->id)->first();
                if (!is_null($MenuAdminLanguage)) {
                    $MenuAdmin = $MenuAdminLanguage->content;
                }
            }
        } else {
            $MenuAdmin = $MenuAdminLanguage->content;
        }
        return $MenuAdmin;
    }
}
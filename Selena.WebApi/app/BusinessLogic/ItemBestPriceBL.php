<?php

namespace App\BusinessLogic;

use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoPeople;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Item;
use App\ItemLanguage;
use App\CategoryItem;
use App\BusinessLogic\OffertaTrovaPrezziBL;
use App\ItemBestPrice;

use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;


class ItemBestPriceBL
{

    public static function GetDailyBestPrice()
    {
        try {
            
            $oggi = date('Y-m-d');

            DB::delete(
                'DELETE FROM items_bestprice WHERE date = :oggi',
                ['oggi' => $oggi]
            ); 

            foreach (DB::select(
                "SELECT item_id, description
                FROM items_languages
                "
            ) as $items) {

                $categoryItemOnDb = CategoryItem::where('item_id', $items->item_id)->get()->first();
                if ($categoryItemOnDb->category_id>=11 && $categoryItemOnDb->category_id<=15)
                    $migliorOfferta = OffertaTrovaPrezziBL::migliorOfferta("cameranationit", $items->description, "5");
                else if ($categoryItemOnDb->category_id==16)
                    $migliorOfferta = OffertaTrovaPrezziBL::migliorOfferta("cameranationit", $items->description, "20124");
                else if ($categoryItemOnDb->category_id==17)
                    $migliorOfferta = OffertaTrovaPrezziBL::migliorOfferta("cameranationit", $items->description, "7");
                else
                    $migliorOfferta = OffertaTrovaPrezziBL::migliorOfferta("cameranationit", $items->description, "0");

                if ($migliorOfferta->price=='' || $migliorOfferta->price==0)
                {

                }
                //non faccio nulla
                else
                {
                    $itemBestPrice = new ItemBestPrice();
                    $itemBestPrice->item_id = $items->item_id;
                    $itemBestPrice->price = floatval(str_replace(',', '.', $migliorOfferta->price));
                    $itemBestPrice->date = $oggi;
                    $itemBestPrice->save();
                }
                   
            }
            
        } catch (\Throwable $ex) {
            LogHelper::error('' . $ex);
        }
    }
}
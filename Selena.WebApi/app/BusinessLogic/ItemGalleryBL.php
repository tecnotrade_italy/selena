<?php

namespace App\BusinessLogic;

use App\CorrelationPeopleUser;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoPeople;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\User;
use App\People;
use App\PeopleUser;
use App\Role;
use App\RolesPeople;
use App\RoleUser;
use App\UsersDatas;
use App\Item;
use App\ItemLanguage;
use App\CategoryItem;
use App\DtoModel\DtoPageSitemap;
use App\BusinessLogic\OffertaTrovaPrezziBL;
use App\Content;
use App\ContentLanguage;
use App\DtoModel\DtoItem;

use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;


class ItemGalleryBL
{
    


    public static function getHtml($url, $idUser)
    {
        if (strpos($url, '/foto/') !== false ) {
            $content = ItemLanguage::where('link_gallery', $url)->first();
            if (isset($content)) {

                $itemOnDb = Item::where('id', $content->item_id)->get()->first();
                
                if (is_null($idUser) || $idUser == '') {
                    $idUserNew = null;
                }else{
                    $idUserNew = $idUser;
                }
                $idContent = Content::where('code', 'ItemGalleryPage')->first();
                
                $languageContent = ContentLanguage::where('content_id', $idContent->id)->first();
                $contentHtml = ContentLanguageBL::getByCodeRender($idContent->id, $languageContent->language_id, $url, null, null, $idUserNew);

                $dtoPageItemRender = new DtoItem();
                $dtoPageItemRender->html = $contentHtml;
                $dtoPageItemRender->description = $content->meta_tag_title_gallery;
                $dtoPageItemRender->meta_tag_description = $content->meta_tag_description_gallery;
                $dtoPageItemRender->meta_tag_keyword = $content->meta_tag_keyword_gallery;
                $dtoPageItemRender->meta_tag_title = $content->meta_tag_title_gallery;
                
                if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
                    $protocol = "https://";
                } else {
                    $protocol = "http://";
                }

                if (strpos($itemOnDb->img, "https://") !== false && strpos($itemOnDb->img, "http://") !== false) {
                    $dtoPageItemRender->url_cover_image = $itemOnDb->img;
                }else{
                    $dtoPageItemRender->url_cover_image = $protocol . $_SERVER["HTTP_HOST"] . $itemOnDb->img;
                }
        

                return $dtoPageItemRender;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }
}
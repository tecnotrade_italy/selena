<?php

namespace App\BusinessLogic;

use App\BusinessName;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoFast;
use App\DtoModel\DtoInternalRepair;
use App\DtoModel\DtoInternalRepairResult;
use App\DtoModel\DtoInterventions;
use App\Enums\DbOperationsTypesEnum;
use App\ExternalRepair;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Fast;
use App\GrIntervention;
use App\GrInterventionPhoto;
use App\InternalRepair;
use App\InternalRepairPhotos;
use App\ItemGr;
use App\ItemGroupTechnicalSpecification;
use App\Tipology;
use App\User;
use DateTime;
use Intervention\Image\Facades\Image;
use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;


class InternalRepairBL
{
    public static $dirImage = '../storage/app/public/images/InternalRepair/';

    public static $dirImageIntervention = '../storage/app/public/images/Intervention/Foto3/RE/';
    public static $dirImageInterventionReportTest = '../storage/app/public/images/Intervention/Foto3/RE_FORN/';
  


  private static function getUrlOpenFileImg()
  {
      if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
          $protocol = "https://";
      } else {
          $protocol = "http://";
      }
      return $protocol . $_SERVER["HTTP_HOST"];
  }



 private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/InternalRepair/';
    }


  #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request)
    {
        DB::beginTransaction();
        try {

            $InternalRepair = new InternalRepair();
           if (isset($request->id)) {
              $InternalRepair->id = $request->id;
            }

           
               /* $date = new DateTime($request->date);
                $InternalRepair->date = $date->format('Y-m-d H:i:s');*/
                if (isset($request->date)) {
                    $old_date = date($request->date);           
                    $middle = strtotime($old_date);            
                    $new_date = date('Y-d-m H:i:s', $middle);
                    $InternalRepair->date = $new_date;
                }else{
                    $InternalRepair->date = ''; 
                }

            if (isset($request->ddt_exit)) {
                $InternalRepair->ddt_exit = $request->ddt_exit;
            }
            if (isset($request->supplier)) {
                $InternalRepair->supplier = $request->supplier;
            }
            if (isset($request->description)) {
                $InternalRepair->description = $request->description;
            }
            if (isset($request->code_gr)) {
                $InternalRepair->code_gr = $request->code_gr;
            }
            if (isset($request->price)) {
                $InternalRepair->price = $request->price;
            }
           /* if (isset($request->numero)) {
                $InternalRepair->numero = 0;
            }*/

            if (isset($request->nr_rip_interna)) {
                $InternalRepair->numero = $request->nr_rip_interna;
            }
            
            $InternalRepair->save();

          
            $cont = 0;
            if (isset($request->imagecaptures)) {
                foreach ($request->imagecaptures as $InternalRepairs) {
                    $cont = $cont + 1;
                    Image::make($InternalRepairs)->save(static::$dirImage . $InternalRepair->id . '_' . $cont . '.JPG');
                    $RowInternalRepairPhotos = new InternalRepairPhotos();
                    $RowInternalRepairPhotos->internal_repair_id = $InternalRepair->id;
                    $RowInternalRepairPhotos->img = '/storage/images/InternalRepair/' . $InternalRepair->id . '_' . $cont . '.JPG';
                    $RowInternalRepairPhotos->save();
                }
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $InternalRepair->id;
    }         

  /**
     * getAllexternalRepair
     * 
     * @return DtoInterventions
     */
    public static function getAllInternalRepair($idLanguage)
    {
        $result = collect();

        foreach (InternalRepair::orderBy('numero', 'DESC')->take(400)->get() as $InternalRepair) {

            $DtoInternalRepair = new DtoInternalRepair();

            if (isset($InternalRepair->id)) {
                $DtoInternalRepair->id = $InternalRepair->id;
            } else {
                $DtoInternalRepair->id = '';
            }
            if (isset($InternalRepair->numero)) {
                $DtoInternalRepair->numero = $InternalRepair->numero;
            } else {
                $DtoInternalRepair->numero = '';
            }
         
           /*  if (isset($InternalRepair->date)) {
                $DtoInternalRepair->date = $InternalRepair->date;
            } else {
                $DtoInternalRepair->date = '';
            } */


            $timestamp = strtotime($InternalRepair->date);
        if (isset($InternalRepair->date)) {
            if ($timestamp === FALSE) {
                $DtoInternalRepair->date = $InternalRepair->date;
            }else{
                $DtoInternalRepair->date = date('d/m/Y', $timestamp);
            }
        }else{
            $DtoInternalRepair->date = '';
        }

            if (isset($InternalRepair->ddt_exit)) {
                $DtoInternalRepair->ddt_exit = $InternalRepair->ddt_exit;
            } else {
                $DtoInternalRepair->ddt_exit = '';
            }
            if (isset($InternalRepair->supplier)) {
                $DtoInternalRepair->supplier = BusinessName::where('id', $InternalRepair->supplier)->first()->business_name;
            } else {
                $DtoInternalRepair->supplier = '';
            }
            if (isset($InternalRepair->description)) {
                $DtoInternalRepair->description = $InternalRepair->description;
            } else {
                $DtoInternalRepair->description = '';
            }
            if (isset($InternalRepair->code_gr)) {
                $DtoInternalRepair->code_gr = $InternalRepair->code_gr;
            } else {
                $DtoInternalRepair->code_gr = '';
            }

            /* if (isset($InternalRepair->date_return_product_from_the_supplier)) {
                $DtoInternalRepair->date_return_product_from_the_supplier = $InternalRepair->date_return_product_from_the_supplier;
            } else {
                $DtoInternalRepair->date_return_product_from_the_supplier = '';
            }*/

            $timestamp1 = strtotime($InternalRepair->date_return_product_from_the_supplier);
            if (isset($InternalRepair->date_return_product_from_the_supplier)) {
                if ($timestamp1 === FALSE) {
                    $DtoInternalRepair->date_return_product_from_the_supplier = $InternalRepair->date_return_product_from_the_supplier;
                }else{
                    $DtoInternalRepair->date_return_product_from_the_supplier = date('d/m/Y', $timestamp1);
                }
            }else{
                $DtoInternalRepair->date_return_product_from_the_supplier = '';
            }

              if (isset($InternalRepair->ddt_supplier)) {
                $DtoInternalRepair->ddt_supplier = $InternalRepair->ddt_supplier;
            } else {
                $DtoInternalRepair->ddt_supplier = '';
            }
              if (isset($InternalRepair->reference_supplier)) {
                $DtoInternalRepair->reference_supplier = $InternalRepair->reference_supplier;
            } else {
                $DtoInternalRepair->reference_supplier = '';
            }
              if (isset($InternalRepair->notes_from_repairman)) {
                $DtoInternalRepair->notes_from_repairman = $InternalRepair->notes_from_repairman;
            } else {
                $DtoInternalRepair->notes_from_repairman = '';
            }
              if (isset($InternalRepair->price)) {
                $DtoInternalRepair->price = $InternalRepair->price;
            } else {
                $DtoInternalRepair->price = '';
            }

                /*foreach (InternalRepairPhotos::where('internal_repair_id', $InternalRepair->id)->get() as $InternalRepairCaptures) {
                    if ($InternalRepairCaptures->img != null){
                                $DtoInternalRepairs = new DtoInternalRepair();
                                $DtoInternalRepairs->img = $InternalRepairCaptures->img;
                                $DtoInternalRepair->imagecaptures->push($DtoInternalRepairs);
                            }
                        }*/
                        foreach (InternalRepairPhotos::where('internal_repair_id', $InternalRepair->id)->get() as $InternalRepairCaptures) {
                            $DtoInternalRepairs = new DtoInternalRepair();
                                        if(!is_null($InternalRepairCaptures->img)){
                                            $DtoInternalRepairs->img = $InternalRepairCaptures->img;
                                        }else{
                                            if (file_exists(static::$dirImageIntervention . $InternalRepairCaptures->numero . '.jpg')) {
                                                $DtoInternalRepairs->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE/' . $InternalRepairCaptures->numero . '.jpg';
                                            } else {
                                              
                                                if (file_exists(static::$dirImageIntervention . $InternalRepairCaptures->numero . '.JPG')) {
                                                    $DtoInternalRepairs->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE/' . $InternalRepairCaptures->numero . '.JPG';
                                                }else{ 
                                                    if ($InternalRepairCaptures->img != '' && !is_null($InternalRepairCaptures->img)) {
                                                        $pos = strpos($InternalRepairCaptures->img, strval($InternalRepairCaptures->internal_repair_id));
                                                        if($pos === false){
                                                            $DtoInternalRepairs->img = '';
                                                        }else{
                                                            $DtoInternalRepairs->img = $InternalRepairCaptures->img;
                                                        }
                                                    } else {
                                                        $DtoInternalRepairs->img = '';
                                                    }
                                                }  
                                            }
                                        }
        
                                    if (file_exists(static::$dirImageInterventionReportTest . $InternalRepairCaptures->numero . '.pdf' )) {
                                            $DtoInternalRepairs->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE_FORN/' . $InternalRepairCaptures->numero . '.pdf';
                                    } else {
                                           if ($InternalRepairCaptures->img_report_test != '' && $InternalRepairCaptures->img_report_test != null) {
                                                $DtoInternalRepairs->img_rep_test = $InternalRepairCaptures->img_report_test;
                                            } else {
                                                $DtoInternalRepairs->img_rep_test = '';
                                            }
                                    }
                                       // $DtoInternalRepairs->img = $InternalRepairCaptures->img;
                                        $DtoInternalRepair->imagecaptures->push($DtoInternalRepairs);
                                }
                        $result->push($DtoInternalRepair);
                    }
                    return $result;
            }
        

    public static function  getdescription(Request $request)
    {
     
         foreach (DB::select(
            "SELECT i.tipology, i.plant, i.note
            FROM gr_items as i
            WHERE i.code_gr = '$request->code_gr'" 
        ) as $GrItems) {
        $DtoInterventions = new DtoInterventions();  

       /* if (isset($GrItems->description)){
            $DtoInterventions->description = $GrItems->description; 
        }else{
            $DtoInterventions->description = '';
        }*/
        if (isset($GrItems->note)){
            $DtoInterventions->note = $GrItems->note; 
        }else{
            $DtoInterventions->note = '';
        }
        if (isset($GrItems->plant)){
            $DtoInterventions->plant = $GrItems->plant;  
        }else{
            $DtoInterventions->plant = '';
        }
        if (isset($GrItems->tipology)){
            $DtoInterventions->tipology = Tipology::where('id',$GrItems->tipology)->first()->description;  
        }else{
            $DtoInterventions->tipology = '';
        }
        return $DtoInterventions; 
        } 
    }

     public static function  getdescriptioninsert(Request $request)
    {
     
         foreach (DB::select(
            "SELECT i.tipology, i.plant, i.note
            FROM gr_items as i
            WHERE i.code_gr = '$request->code_gr'" 
        ) as $GrItems) {
        $DtoInterventions = new DtoInterventions();  

       /* if (isset($GrItems->description)){
            $DtoInterventions->description = $GrItems->description; 
        }else{
            $DtoInterventions->description = '';
        }*/
        if (isset($GrItems->note)){
            $DtoInterventions->note = $GrItems->note; 
        }else{
            $DtoInterventions->note = '';
        }
        if (isset($GrItems->plant)){
            $DtoInterventions->plant = $GrItems->plant;  
        }else{
            $DtoInterventions->plant = '';
        }
        if (isset($GrItems->tipology)){
            $DtoInterventions->tipology = Tipology::where('id',$GrItems->tipology)->first()->description;  
        }else{
            $DtoInterventions->tipology = '';
        }
        return $DtoInterventions; 
        } 
    }
 /**
     * Get by id
     * 
     * @param int id
     * @return DtoInternalRepair
     */
    public static function getByInternalRepair(int $id)
    {
        $InternalRepair= InternalRepair::where('id', $id)->first();

         $DtoInternalRepair = new DtoInternalRepair();

            if (isset($InternalRepair->id)) {
                $DtoInternalRepair->id = $InternalRepair->id;
            } else {
                $DtoInternalRepair->id = '';
            }

            if (isset($InternalRepair->numero)) {
                $DtoInternalRepair->nr_rip_interna = $InternalRepair->numero;
            } else {
                $DtoInternalRepair->nr_rip_interna = '';
            }


            if (isset($InternalRepair->date)) {
                $DtoInternalRepair->date = $InternalRepair->date;
            } else {
                $DtoInternalRepair->date = '';
            }

            if (isset($InternalRepair->ddt_exit)) {
                $DtoInternalRepair->ddt_exit = $InternalRepair->ddt_exit;
            } else {
                $DtoInternalRepair->ddt_exit = '';
            }
            if (isset($InternalRepair->supplier)) {
                $DtoInternalRepair->supplier =$InternalRepair->supplier;
                $DtoInternalRepair->Supplier = BusinessName::where('id', $InternalRepair->supplier)->first()->business_name;
            } else {
                $DtoInternalRepair->supplier = '';
            }

            if (isset($InternalRepair->description)) {
                $DtoInternalRepair->description = $InternalRepair->description;
            } else {
                $DtoInternalRepair->description = '';
            }
            if (isset($InternalRepair->code_gr)) {
                $DtoInternalRepair->code_gr = $InternalRepair->code_gr;
            } else {
                $DtoInternalRepair->code_gr = '';
            }
            if (isset($InternalRepair->price)) {
                $DtoInternalRepair->price = $InternalRepair->price;
            } else {
                $DtoInternalRepair->price = '';
            }

            if (isset($InternalRepair->date_return_product_from_the_supplier)) {
                $DtoInternalRepair->date_return_product_from_the_supplier = $InternalRepair->date_return_product_from_the_supplier;
            } else {
                $DtoInternalRepair->date_return_product_from_the_supplier = '';
            }
            if (isset($InternalRepair->ddt_supplier)) {
                $DtoInternalRepair->ddt_supplier = $InternalRepair->ddt_supplier;
            } else {
                $DtoInternalRepair->ddt_supplier = '';
            }
            if (isset($InternalRepair->reference_supplier)) {
                $DtoInternalRepair->reference_supplier = $InternalRepair->reference_supplier;
            } else {
                $DtoInternalRepair->reference_supplier = '';
            }
            if (isset($InternalRepair->notes_from_repairman)) {
                $DtoInternalRepair->notes_from_repairman = $InternalRepair->notes_from_repairman;
            } else {
                $DtoInternalRepair->notes_from_repairman = '';
            }

  /*  foreach (InternalRepairPhotos::where('internal_repair_id', $InternalRepair->id)->get() as $InternalRepairCaptures) {
            $DtoInternalRepairs = new DtoInternalRepair();
            $DtoInternalRepairs->img = $InternalRepairCaptures->img;
            $DtoInternalRepair->imagecaptures->push($DtoInternalRepairs);
            }   */
            
            foreach (InternalRepairPhotos::where('internal_repair_id', $InternalRepair->id)->get() as $InternalRepairCaptures) {
                $DtoInternalRepairs = new DtoInternalRepair();
                            if(!is_null($InternalRepairCaptures->img)){
                                $DtoInternalRepairs->img = $InternalRepairCaptures->img;
                            }else{
                                if (file_exists(static::$dirImageIntervention . $InternalRepairCaptures->numero . '.jpg')) {
                                    $DtoInternalRepairs->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE/' . $InternalRepairCaptures->numero . '.jpg';
                                } else {
                                  
                                    if (file_exists(static::$dirImageIntervention . $InternalRepairCaptures->numero . '.JPG')) {
                                        $DtoInternalRepairs->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE/' . $InternalRepairCaptures->numero . '.JPG';
                                    }else{ 
                                        if ($InternalRepairCaptures->img != '' && !is_null($InternalRepairCaptures->img)) {
                                            $pos = strpos($InternalRepairCaptures->img, strval($InternalRepairCaptures->internal_repair_id));
                                            if($pos === false){
                                                $DtoInternalRepairs->img = '';
                                            }else{
                                                $DtoInternalRepairs->img = $InternalRepairCaptures->img;
                                            }
                                        } else {
                                            $DtoInternalRepairs->img = '';
                                        }
                                    }  
                                }
                            }
                            $DtoInternalRepair->imagecaptures->push($DtoInternalRepairs);
                        }
        return  $DtoInternalRepair;
    }

/**
     * Update
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateInternalRepair(Request $request)
    {
                $InternalRepair= InternalRepair::find($request->id);
                $InternalRepairPhotos= InternalRepairPhotos::find($request->id);
        DB::beginTransaction();
                try {
                $InternalRepair->id = $request->id;

               /* if (isset($request->date)) {
                    $old_date = date($request->date);           
                    $middle = strtotime($old_date);            
                    $new_date = date('Y-m-d H:i:s', $middle);
                    $InternalRepair->date = $new_date;
                }else{
                    $InternalRepair->date = ''; 
                }*/
                $InternalRepair->date = $request->date;
                $InternalRepair->ddt_exit = $request->ddt_exit;
                $InternalRepair->supplier = $request->supplier;
                $InternalRepair->description = $request->description;
                $InternalRepair->code_gr = $request->code_gr;
                $InternalRepair->date_return_product_from_the_supplier = $request->date_return_product_from_the_supplier;
                $InternalRepair->ddt_supplier = $request->ddt_supplier;
                $InternalRepair->reference_supplier = $request->reference_supplier;
                $InternalRepair->notes_from_repairman = $request->notes_from_repairman;
                $InternalRepair->price = $request->price;
                $InternalRepair->numero = $request->nr_rip_interna;
                $InternalRepair->save();

            $cont = 0;
            if (isset($request->imagecaptures)) {
                foreach ($request->imagecaptures as $InternalRepairPhoto) {
                    $IdInternalPhoto = InternalRepairPhotos::where('internal_repair_id', $InternalRepair->id)->where('img', $InternalRepairPhoto)->first();
                    $cont = $cont + 1;
                    if (!isset($IdInternalPhoto)) {
                        if (!is_null($InternalRepairPhoto) && $InternalRepairPhoto != '' && $InternalRepairPhoto!= null && $InternalRepairPhoto !='null'){
                            
                            $strPhoto = str_replace('https://www.grsrl.net/storage/','../storage/app/public/', $InternalRepairPhoto);
                            if (!file_exists($strPhoto)) {
                                Image::make($InternalRepairPhoto)->save(static::$dirImage . $InternalRepair->id . '_' . $cont . '.JPG');
                                $RowInternalRepairPhotos = new InternalRepairPhotos();
                                $RowInternalRepairPhotos->internal_repair_id = $InternalRepair->id;
                                $RowInternalRepairPhotos->img = '/storage/images/InternalRepair/' . $InternalRepair->id . '_' . $cont . '.JPG';
                                $RowInternalRepairPhotos->save();
                            }
                        }
                    }
                }
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }


/**
     * Get select
     *
     * @param String $search
     * @return Intervention
     */
    public static function getLastId()
    {

        $LastId = InternalRepair::orderBy('id', 'DESC')->first();
        if (isset($LastId->id)) {
            return $LastId->id + 1;
        } else {
            return 1;
        }
    }

    public static function getLastIdIntRepair()
    {

        $LastId = InternalRepair::orderBy('numero', 'DESC')->first();
        if (isset($LastId->numero)) {
            return $LastId->numero + 1;
        } else {
            return 1;
        }
    }


 public static function getinternalRepairSearchViewClose(request $request)
    {
        $result = collect();
   
            foreach (DB::select(
               'SELECT internal_repair.*
                FROM internal_repair
                WHERE internal_repair.date_return_product_from_the_supplier is not null 
                and internal_repair.ddt_supplier is not null limit 100'        
            ) as $InternalRepair) {

                $DtoInternalRepair = new DtoInternalRepair();

                if (isset($InternalRepair->id)) {
                    $DtoInternalRepair->id = $InternalRepair->id;
                } else {
                    $DtoInternalRepair->id = '';
                }
                 if (isset($InternalRepair->numero)) {
                    $DtoInternalRepair->numero = $InternalRepair->numero;
                } else {
                    $DtoInternalRepair->numero = '';
                }
             
               /*  if (isset($InternalRepair->date)) {
                    $DtoInternalRepair->date = $InternalRepair->date;
                } else {
                    $DtoInternalRepair->date = '';
                } */
    
    
                $timestamp = strtotime($InternalRepair->date);
            if (isset($InternalRepair->date)) {
                if ($timestamp === FALSE) {
                    $DtoInternalRepair->date = $InternalRepair->date;
                }else{
                    $DtoInternalRepair->date = date('d/m/Y', $timestamp);
                }
            }else{
                $DtoInternalRepair->date = '';
            }
    
                if (isset($InternalRepair->ddt_exit)) {
                    $DtoInternalRepair->ddt_exit = $InternalRepair->ddt_exit;
                } else {
                    $DtoInternalRepair->ddt_exit = '';
                }
                if (isset($InternalRepair->supplier)) {
                    $DtoInternalRepair->supplier = BusinessName::where('id', $InternalRepair->supplier)->first()->business_name;
                } else {
                    $DtoInternalRepair->supplier = '';
                }
                if (isset($InternalRepair->description)) {
                    $DtoInternalRepair->description = $InternalRepair->description;
                } else {
                    $DtoInternalRepair->description = '';
                }
                if (isset($InternalRepair->code_gr)) {
                    $DtoInternalRepair->code_gr = $InternalRepair->code_gr;
                } else {
                    $DtoInternalRepair->code_gr = '';
                }
    
                /* if (isset($InternalRepair->date_return_product_from_the_supplier)) {
                    $DtoInternalRepair->date_return_product_from_the_supplier = $InternalRepair->date_return_product_from_the_supplier;
                } else {
                    $DtoInternalRepair->date_return_product_from_the_supplier = '';
                }*/
    
                $timestamp1 = strtotime($InternalRepair->date_return_product_from_the_supplier);
                if (isset($InternalRepair->date_return_product_from_the_supplier)) {
                    if ($timestamp1 === FALSE) {
                        $DtoInternalRepair->date_return_product_from_the_supplier = $InternalRepair->date_return_product_from_the_supplier;
                    }else{
                        $DtoInternalRepair->date_return_product_from_the_supplier = date('d/m/Y', $timestamp1);
                    }
                }else{
                    $DtoInternalRepair->date_return_product_from_the_supplier = '';
                }
    
                  if (isset($InternalRepair->ddt_supplier)) {
                    $DtoInternalRepair->ddt_supplier = $InternalRepair->ddt_supplier;
                } else {
                    $DtoInternalRepair->ddt_supplier = '';
                }
                  if (isset($InternalRepair->reference_supplier)) {
                    $DtoInternalRepair->reference_supplier = $InternalRepair->reference_supplier;
                } else {
                    $DtoInternalRepair->reference_supplier = '';
                }
                  if (isset($InternalRepair->notes_from_repairman)) {
                    $DtoInternalRepair->notes_from_repairman = $InternalRepair->notes_from_repairman;
                } else {
                    $DtoInternalRepair->notes_from_repairman = '';
                }
                  if (isset($InternalRepair->price)) {
                    $DtoInternalRepair->price = $InternalRepair->price;
                } else {
                    $DtoInternalRepair->price = '';
                }
    
                    /*foreach (InternalRepairPhotos::where('internal_repair_id', $InternalRepair->id)->get() as $InternalRepairCaptures) {
                        if ($InternalRepairCaptures->img != null){
                                    $DtoInternalRepairs = new DtoInternalRepair();
                                    $DtoInternalRepairs->img = $InternalRepairCaptures->img;
                                    $DtoInternalRepair->imagecaptures->push($DtoInternalRepairs);
                                }
                            }*/
                            foreach (InternalRepairPhotos::where('internal_repair_id', $InternalRepair->id)->get() as $InternalRepairCaptures) {
                                $DtoInternalRepairs = new DtoInternalRepair();
                                            if(!is_null($InternalRepairCaptures->img)){
                                                $DtoInternalRepairs->img = $InternalRepairCaptures->img;
                                            }else{
                                                if (file_exists(static::$dirImageIntervention . $InternalRepairCaptures->numero . '.jpg')) {
                                                    $DtoInternalRepairs->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE/' . $InternalRepairCaptures->numero . '.jpg';
                                                } else {
                                                  
                                                    if (file_exists(static::$dirImageIntervention . $InternalRepairCaptures->numero . '.JPG')) {
                                                        $DtoInternalRepairs->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE/' . $InternalRepairCaptures->numero . '.JPG';
                                                    }else{ 
                                                        if ($InternalRepairCaptures->img != '' && !is_null($InternalRepairCaptures->img)) {
                                                            $pos = strpos($InternalRepairCaptures->img, strval($InternalRepairCaptures->internal_repair_id));
                                                            if($pos === false){
                                                                $DtoInternalRepairs->img = '';
                                                            }else{
                                                                $DtoInternalRepairs->img = $InternalRepairCaptures->img;
                                                            }
                                                        } else {
                                                            $DtoInternalRepairs->img = '';
                                                        }
                                                    }  
                                                }
                                            }
            
                                        if (file_exists(static::$dirImageInterventionReportTest . $InternalRepairCaptures->numero . '.pdf' )) {
                                                $DtoInternalRepairs->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE_FORN/' . $InternalRepairCaptures->numero . '.pdf';
                                        } else {
                                               if ($InternalRepairCaptures->img_report_test != '' && $InternalRepairCaptures->img_report_test != null) {
                                                    $DtoInternalRepairs->img_rep_test = $InternalRepairCaptures->img_report_test;
                                                } else {
                                                    $DtoInternalRepairs->img_rep_test = '';
                                                }
                                        }
                                           // $DtoInternalRepairs->img = $InternalRepairCaptures->img;
                                            $DtoInternalRepair->imagecaptures->push($DtoInternalRepairs);
                                    }
                            $result->push($DtoInternalRepair);
                        }
                        return $result;
                }


    public static function getinternalRepairSearchViewOpen(request $request)
    {
        $result = collect();

            foreach (DB::select(
               'SELECT internal_repair.*
                FROM internal_repair
                WHERE internal_repair.date_return_product_from_the_supplier is null 
                and internal_repair.ddt_supplier is null limit 100'        
            ) as $InternalRepair) {

                $DtoInternalRepair = new DtoInternalRepair();

                if (isset($InternalRepair->id)) {
                    $DtoInternalRepair->id = $InternalRepair->id;
                } else {
                    $DtoInternalRepair->id = '';
                }
                 if (isset($InternalRepair->numero)) {
                    $DtoInternalRepair->numero = $InternalRepair->numero;
                } else {
                    $DtoInternalRepair->numero = '';
                }
             
               /*  if (isset($InternalRepair->date)) {
                    $DtoInternalRepair->date = $InternalRepair->date;
                } else {
                    $DtoInternalRepair->date = '';
                } */
    
    
                $timestamp = strtotime($InternalRepair->date);
            if (isset($InternalRepair->date)) {
                if ($timestamp === FALSE) {
                    $DtoInternalRepair->date = $InternalRepair->date;
                }else{
                    $DtoInternalRepair->date = date('d/m/Y', $timestamp);
                }
            }else{
                $DtoInternalRepair->date = '';
            }
    
                if (isset($InternalRepair->ddt_exit)) {
                    $DtoInternalRepair->ddt_exit = $InternalRepair->ddt_exit;
                } else {
                    $DtoInternalRepair->ddt_exit = '';
                }
                if (isset($InternalRepair->supplier)) {
                    $DtoInternalRepair->supplier = BusinessName::where('id', $InternalRepair->supplier)->first()->business_name;
                } else {
                    $DtoInternalRepair->supplier = '';
                }
                if (isset($InternalRepair->description)) {
                    $DtoInternalRepair->description = $InternalRepair->description;
                } else {
                    $DtoInternalRepair->description = '';
                }
                if (isset($InternalRepair->code_gr)) {
                    $DtoInternalRepair->code_gr = $InternalRepair->code_gr;
                } else {
                    $DtoInternalRepair->code_gr = '';
                }
    
                /* if (isset($InternalRepair->date_return_product_from_the_supplier)) {
                    $DtoInternalRepair->date_return_product_from_the_supplier = $InternalRepair->date_return_product_from_the_supplier;
                } else {
                    $DtoInternalRepair->date_return_product_from_the_supplier = '';
                }*/
    
                $timestamp1 = strtotime($InternalRepair->date_return_product_from_the_supplier);
                if (isset($InternalRepair->date_return_product_from_the_supplier)) {
                    if ($timestamp1 === FALSE) {
                        $DtoInternalRepair->date_return_product_from_the_supplier = $InternalRepair->date_return_product_from_the_supplier;
                    }else{
                        $DtoInternalRepair->date_return_product_from_the_supplier = date('d/m/Y', $timestamp1);
                    }
                }else{
                    $DtoInternalRepair->date_return_product_from_the_supplier = '';
                }
    
                  if (isset($InternalRepair->ddt_supplier)) {
                    $DtoInternalRepair->ddt_supplier = $InternalRepair->ddt_supplier;
                } else {
                    $DtoInternalRepair->ddt_supplier = '';
                }
                  if (isset($InternalRepair->reference_supplier)) {
                    $DtoInternalRepair->reference_supplier = $InternalRepair->reference_supplier;
                } else {
                    $DtoInternalRepair->reference_supplier = '';
                }
                  if (isset($InternalRepair->notes_from_repairman)) {
                    $DtoInternalRepair->notes_from_repairman = $InternalRepair->notes_from_repairman;
                } else {
                    $DtoInternalRepair->notes_from_repairman = '';
                }
                  if (isset($InternalRepair->price)) {
                    $DtoInternalRepair->price = $InternalRepair->price;
                } else {
                    $DtoInternalRepair->price = '';
                }
    
                            foreach (InternalRepairPhotos::where('internal_repair_id', $InternalRepair->id)->get() as $InternalRepairCaptures) {
                                $DtoInternalRepairs = new DtoInternalRepair();
                                            if(!is_null($InternalRepairCaptures->img)){
                                                $DtoInternalRepairs->img = $InternalRepairCaptures->img;
                                            }else{
                                                if (file_exists(static::$dirImageIntervention . $InternalRepairCaptures->numero . '.jpg')) {
                                                    $DtoInternalRepairs->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE/' . $InternalRepairCaptures->numero . '.jpg';
                                                } else {
                                                  
                                                    if (file_exists(static::$dirImageIntervention . $InternalRepairCaptures->numero . '.JPG')) {
                                                        $DtoInternalRepairs->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE/' . $InternalRepairCaptures->numero . '.JPG';
                                                    }else{ 
                                                        if ($InternalRepairCaptures->img != '' && !is_null($InternalRepairCaptures->img)) {
                                                            $pos = strpos($InternalRepairCaptures->img, strval($InternalRepairCaptures->internal_repair_id));
                                                            if($pos === false){
                                                                $DtoInternalRepairs->img = '';
                                                            }else{
                                                                $DtoInternalRepairs->img = $InternalRepairCaptures->img;
                                                            }
                                                        } else {
                                                            $DtoInternalRepairs->img = '';
                                                        }
                                                    }  
                                                }
                                            }
            
                                        if (file_exists(static::$dirImageInterventionReportTest . $InternalRepairCaptures->numero . '.pdf' )) {
                                                $DtoInternalRepairs->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE_FORN/' . $InternalRepairCaptures->numero . '.pdf';
                                        } else {
                                               if ($InternalRepairCaptures->img_report_test != '' && $InternalRepairCaptures->img_report_test != null) {
                                                    $DtoInternalRepairs->img_rep_test = $InternalRepairCaptures->img_report_test;
                                                } else {
                                                    $DtoInternalRepairs->img_rep_test = '';
                                                }
                                        }
                                            $DtoInternalRepair->imagecaptures->push($DtoInternalRepairs);
                                    }
                            $result->push($DtoInternalRepair);
                        }
                        return $result;
                }


    public static function getAllInternalExternalRepairSearch($request)
    {  
        $result = collect();
        $strWhere = "";

        $business_name = $request->business_name_rip_int_est;
        $CodeGrSearch = $request->code_gr_rip_int_est;
        if ($CodeGrSearch != "") {
            $strWhere = $strWhere . ' OR internal_repair.code_gr ilike \'%' . $CodeGrSearch . '%\'';
        }

        $CodeGr = $request->code_gr_rip_int_est;
        if ($CodeGr != "") {
            $strWhere = $strWhere . ' OR gi.code_gr ilike \'%' . $CodeGr . '%\'';
        }
       


        $countOpeneds = DB::select(
            'SELECT internal_repair.id as id,
            internal_repair.numero as numero,
            internal_repair."date" as date,
            internal_repair.ddt_exit as ddt_exit,
            internal_repair.description as description,
            internal_repair.code_gr as code_gr,
            internal_repair.supplier as supplier,
            internal_repair.date_return_product_from_the_supplier as date_return_product_from_the_supplier,
            internal_repair.ddt_supplier as ddt_supplier,
            internal_repair.reference_supplier as reference_supplier,
            internal_repair.notes_from_repairman as notes_from_repairman 
            FROM internal_repair
            WHERE internal_repair.supplier = :business_name ' . $strWhere . '
            UNION ALL
            SELECT
            external_repair.id as id,
            external_repair.numero as numero,
            external_repair."date" as date,
            external_repair.ddt_exit as ddt_exit,
            external_repair.description as description,
            gi.code_gr as code_gr,
            external_repair.id_business_name_supplier as supplier,
            external_repair.date_return_product_from_the_supplier as date_return_product_from_the_supplier,
            external_repair.ddt_supplier as ddt_supplier,
            external_repair.reference_supplier as reference_supplier,
            external_repair.notes_from_repairman as notes_from_repairman 
            FROM external_repair
            left join gr_interventions as gr_i on gr_i.id = external_repair.id_intervention
            left join gr_items as gi on gi.id = gr_i.items_id
            WHERE external_repair.id_business_name_supplier = :business_name ' . $strWhere . '',
            ['business_name' => $business_name]);

         
            //external_repair.code_gr as code_gr,

            foreach ($countOpeneds as $key) {
                $DtoInternalRepairResult = new DtoInternalRepairResult();

                if (isset($key->id)) {
                    $DtoInternalRepairResult->id = $key->id;
                } else {
                    $DtoInternalRepairResult->id = '';
                }
                if (isset($key->numero)) {
                    $DtoInternalRepairResult->numero = $key->numero;
                } else {
                    $DtoInternalRepairResult->numero = '';
                }
               
                $timestamp = strtotime($key->date);
                if (isset($key->date)) {
                    if ($timestamp === FALSE) {
                        $DtoInternalRepairResult->date = $key->date;
                    }else{
                        $DtoInternalRepairResult->date = date('d/m/Y', $timestamp);
                    }
                    }else{
                        $DtoInternalRepairResult->date = '';
                    }

                if (isset($key->ddt_exit)) {
                        $DtoInternalRepairResult->ddt_exit = $key->ddt_exit;
                } else {
                        $DtoInternalRepairResult->ddt_exit = '';
                }
                if (isset($key->supplier)) {
                        $DtoInternalRepairResult->supplier = BusinessName::where('id', $key->supplier)->first()->business_name;
                } else {
                        $DtoInternalRepairResult->supplier = '';
                }
                if (isset($key->supplier)) {
                    $DtoInternalRepairResult->supplier = BusinessName::where('id', $key->supplier)->first()->business_name;
                } else {
                    $DtoInternalRepairResult->supplier = '';
                }
                if (isset($key->description)) {
                    $DtoInternalRepairResult->description = $key->description;
                } else {
                    $DtoInternalRepairResult->description = '';
                }
                if (isset($key->code_gr)) {
                    $DtoInternalRepairResult->code_gr = $key->code_gr;
                } else {
                    $DtoInternalRepairResult->code_gr = '';
                }
                $timestamp1 = strtotime($key->date_return_product_from_the_supplier);
                if (isset($key->date_return_product_from_the_supplier)) {
                    if ($timestamp1 === FALSE) {
                        $DtoInternalRepairResult->date_return_product_from_the_supplier = $key->date_return_product_from_the_supplier;
                    }else{
                        $DtoInternalRepairResult->date_return_product_from_the_supplier = date('d/m/Y', $timestamp1);
                }
                }else{
                    $DtoInternalRepairResult->date_return_product_from_the_supplier = '';
                }
                  if (isset($key->ddt_supplier)) {
                    $DtoInternalRepairResult->ddt_supplier = $key->ddt_supplier;
                } else {
                    $DtoInternalRepairResult->ddt_supplier = '';
                }
                  if (isset($key->reference_supplier)) {
                    $DtoInternalRepairResult->reference_supplier = $key->reference_supplier;
                } else {
                    $DtoInternalRepairResult->reference_supplier = '';
                }
                  if (isset($key->notes_from_repairman)) {
                    $DtoInternalRepairResult->notes_from_repairman = $key->notes_from_repairman;
                } else {
                    $DtoInternalRepairResult->notes_from_repairman = '';
                }
                  if (isset($key->price)) {
                    $DtoInternalRepairResult->price = $key->price;
                } else {
                    $DtoInternalRepairResult->price = '';
                }    


                foreach (InternalRepairPhotos::where('internal_repair_id', $key->id)->get() as $InternalRepairCaptures) {
                    $DtoInternalRepairs = new DtoInternalRepair();
                                if(!is_null($InternalRepairCaptures->img)){
                                    $DtoInternalRepairs->img = $InternalRepairCaptures->img;
                                }else{
                                    if (file_exists(static::$dirImageIntervention . $InternalRepairCaptures->numero . '.jpg')) {
                                        $DtoInternalRepairs->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE/' . $InternalRepairCaptures->numero . '.jpg';
                                    } else {
                                      
                                        if (file_exists(static::$dirImageIntervention . $InternalRepairCaptures->numero . '.JPG')) {
                                            $DtoInternalRepairs->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE/' . $InternalRepairCaptures->numero . '.JPG';
                                        }else{ 
                                            if ($InternalRepairCaptures->img != '' && !is_null($InternalRepairCaptures->img)) {
                                                $pos = strpos($InternalRepairCaptures->img, strval($InternalRepairCaptures->internal_repair_id));
                                                if($pos === false){
                                                    $DtoInternalRepairs->img = '';
                                                }else{
                                                    $DtoInternalRepairs->img = $InternalRepairCaptures->img;
                                                }
                                            } else {
                                                $DtoInternalRepairs->img = '';
                                            }
                                        }  
                                    }
                                }
                            if (file_exists(static::$dirImageInterventionReportTest . $InternalRepairCaptures->numero . '.pdf' )) {
                                    $DtoInternalRepairs->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE_FORN/' . $InternalRepairCaptures->numero . '.pdf';
                            } else {
                                   if ($InternalRepairCaptures->img_report_test != '' && $InternalRepairCaptures->img_report_test != null) {
                                        $DtoInternalRepairs->img_rep_test = $InternalRepairCaptures->img_report_test;
                                    } else {
                                        $DtoInternalRepairs->img_rep_test = '';
                                    }
                            }
                                $DtoInternalRepairResult->imagecaptures->push($DtoInternalRepairs);
                        }

                   $result->push($DtoInternalRepairResult);
            }
                    return $result;
    }

    public static function getAllInternalRepairSearch($request)
    {  
        $result = collect();
        $content = '';
        $finalHtml = '';
        $strWhere = "";

        $business_name = $request->business_name;
        $CodeGrSearch = $request->code_gr;
        if ($CodeGrSearch != "") {
          $strWhere = $strWhere . ' OR e.code_gr ilike \'%' . $CodeGrSearch . '%\'';
        }

        $IdInterventionSearch = $request->code_intervention_gr;
        if ($IdInterventionSearch != "") {
          $strWhere = $strWhere . ' OR e.numero = ' . $IdInterventionSearch . '';
        }
        $DateSearch = $request->date;
        if ($DateSearch != "") {
          $strWhere = $strWhere . ' OR e.date like \'%' . $DateSearch . '%\'';
        }
            foreach (DB::select(
                'SELECT e.*
                FROM internal_repair as e
                WHERE e.supplier = :business_name ' . $strWhere,
            ['business_name' => $business_name],
            ) as $InternalRepair) {

               $DtoInternalRepair = new DtoInternalRepair();

            if (isset($InternalRepair->id)) {
                $DtoInternalRepair->id = $InternalRepair->id;
            } else {
                $DtoInternalRepair->id = '';
            }
             if (isset($InternalRepair->numero)) {
                $DtoInternalRepair->numero = $InternalRepair->numero;
            } else {
                $DtoInternalRepair->numero = '';
            }
        
            $timestamp = strtotime($InternalRepair->date);
        if (isset($InternalRepair->date)) {
            if ($timestamp === FALSE) {
                $DtoInternalRepair->date = $InternalRepair->date;
            }else{
                $DtoInternalRepair->date = date('d/m/Y', $timestamp);
            }
        }else{
            $DtoInternalRepair->date = '';
        }

            if (isset($InternalRepair->ddt_exit)) {
                $DtoInternalRepair->ddt_exit = $InternalRepair->ddt_exit;
            } else {
                $DtoInternalRepair->ddt_exit = '';
            }
            if (isset($InternalRepair->supplier)) {
                $DtoInternalRepair->supplier = BusinessName::where('id', $InternalRepair->supplier)->first()->business_name;
            } else {
                $DtoInternalRepair->supplier = '';
            }
            if (isset($InternalRepair->description)) {
                $DtoInternalRepair->description = $InternalRepair->description;
            } else {
                $DtoInternalRepair->description = '';
            }
            if (isset($InternalRepair->code_gr)) {
                $DtoInternalRepair->code_gr = $InternalRepair->code_gr;
            } else {
                $DtoInternalRepair->code_gr = '';
            }
            $timestamp1 = strtotime($InternalRepair->date_return_product_from_the_supplier);
            if (isset($InternalRepair->date_return_product_from_the_supplier)) {
                if ($timestamp1 === FALSE) {
                    $DtoInternalRepair->date_return_product_from_the_supplier = $InternalRepair->date_return_product_from_the_supplier;
                }else{
                    $DtoInternalRepair->date_return_product_from_the_supplier = date('d/m/Y', $timestamp1);
                }
            }else{
                $DtoInternalRepair->date_return_product_from_the_supplier = '';
            }

              if (isset($InternalRepair->ddt_supplier)) {
                $DtoInternalRepair->ddt_supplier = $InternalRepair->ddt_supplier;
            } else {
                $DtoInternalRepair->ddt_supplier = '';
            }
              if (isset($InternalRepair->reference_supplier)) {
                $DtoInternalRepair->reference_supplier = $InternalRepair->reference_supplier;
            } else {
                $DtoInternalRepair->reference_supplier = '';
            }
              if (isset($InternalRepair->notes_from_repairman)) {
                $DtoInternalRepair->notes_from_repairman = $InternalRepair->notes_from_repairman;
            } else {
                $DtoInternalRepair->notes_from_repairman = '';
            }
              if (isset($InternalRepair->price)) {
                $DtoInternalRepair->price = $InternalRepair->price;
            } else {
                $DtoInternalRepair->price = '';
            }

    
                        foreach (InternalRepairPhotos::where('internal_repair_id', $InternalRepair->id)->get() as $InternalRepairCaptures) {
                            $DtoInternalRepairs = new DtoInternalRepair();
                                        if(!is_null($InternalRepairCaptures->img)){
                                            $DtoInternalRepairs->img = $InternalRepairCaptures->img;
                                        }else{
                                            if (file_exists(static::$dirImageIntervention . $InternalRepairCaptures->numero . '.jpg')) {
                                                $DtoInternalRepairs->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE/' . $InternalRepairCaptures->numero . '.jpg';
                                            } else {
                                              
                                                if (file_exists(static::$dirImageIntervention . $InternalRepairCaptures->numero . '.JPG')) {
                                                    $DtoInternalRepairs->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE/' . $InternalRepairCaptures->numero . '.JPG';
                                                }else{ 
                                                    if ($InternalRepairCaptures->img != '' && !is_null($InternalRepairCaptures->img)) {
                                                        $pos = strpos($InternalRepairCaptures->img, strval($InternalRepairCaptures->internal_repair_id));
                                                        if($pos === false){
                                                            $DtoInternalRepairs->img = '';
                                                        }else{
                                                            $DtoInternalRepairs->img = $InternalRepairCaptures->img;
                                                        }
                                                    } else {
                                                        $DtoInternalRepairs->img = '';
                                                    }
                                                }  
                                            }
                                        }
        
                                    if (file_exists(static::$dirImageInterventionReportTest . $InternalRepairCaptures->numero . '.pdf' )) {
                                            $DtoInternalRepairs->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE_FORN/' . $InternalRepairCaptures->numero . '.pdf';
                                    } else {
                                           if ($InternalRepairCaptures->img_report_test != '' && $InternalRepairCaptures->img_report_test != null) {
                                                $DtoInternalRepairs->img_rep_test = $InternalRepairCaptures->img_report_test;
                                            } else {
                                                $DtoInternalRepairs->img_rep_test = '';
                                            }
                                    }
                                        $DtoInternalRepair->imagecaptures->push($DtoInternalRepairs);
                                }
                        $result->push($DtoInternalRepair);
                    }
                    return $result;
    }
            
            

    #region DELETE
    /**
     * Delete
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $InternalRepairPhotos= InternalRepairPhotos::where('internal_repair_id', $id);
        $InternalRepair= InternalRepair::find($id);

        if (is_null($InternalRepair)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($InternalRepair)) {

            DB::beginTransaction();

            try {
                InternalRepairPhotos::where('internal_repair_id', $InternalRepair->id)->delete();
                InternalRepair::where('id', $id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }

    #endregion DELETE

//updateInternalRepair
  /**
     * Update
     * 
     * @param Request DtoInterventions
     * @param int idLanguage
     */

    public static function updateIntervention(Request $DtoInterventions)
    {

        $interventionOnDb = GrIntervention::find($DtoInterventions->id);
        $interventionOnDbPhoto = GrInterventionPhoto::find($DtoInterventions->intervention_id);
        //$ItemsId = ItemGr::where('id',  $interventionOnDb->items_id)->first()->id;

        DB::beginTransaction();
        try {
            $interventionOnDb->language_id = $DtoInterventions->idLanguage;
            $interventionOnDb->customers_id = $DtoInterventions->customer_id;
            $interventionOnDb->user_id = $DtoInterventions->user_id;
            $interventionOnDb->states_id = $DtoInterventions->states_id;
            //$interventionOnDb->items_id = ItemLanguage::where('id',$DtoInterventions->items_id)->first()->item_id; 
            $interventionOnDb->items_id = $DtoInterventions->items_id;
            $interventionOnDb->description = $DtoInterventions->description;
            $interventionOnDb->defect = $DtoInterventions->defect;
            $interventionOnDb->nr_ddt = $DtoInterventions->nr_ddt;
            $interventionOnDb->nr_ddt_gr = $DtoInterventions->nr_ddt_gr;
            $interventionOnDb->date_ddt_gr = $DtoInterventions->date_ddt_gr;
            if (isset($DtoInterventions->date_ddt)) {
                $interventionOnDb->date_ddt = $DtoInterventions->date_ddt;
            }
            $interventionOnDb->referent = $DtoInterventions->referent;
            $interventionOnDb->plant_type = $DtoInterventions->plant_type;
            $interventionOnDb->trolley_type = $DtoInterventions->trolley_type;
            $interventionOnDb->series = $DtoInterventions->series;
            $interventionOnDb->voltage = $DtoInterventions->voltage;
            $interventionOnDb->exit_notes = $DtoInterventions->exit_notes;
            $interventionOnDb->unrepairable = $DtoInterventions->unrepairable;
            $interventionOnDb->send_to_customer = $DtoInterventions->send_to_customer;
            $interventionOnDb->create_quote = $DtoInterventions->create_quote;
            $interventionOnDb->save();

            $cont = 0;
            if (isset($DtoInterventions->captures)) {
                foreach ($DtoInterventions->captures as $InterventionPhoto) {
                    $IdInterventionPhoto = GrInterventionPhoto::where('intervention_id', $interventionOnDb->id)->where('img', $InterventionPhoto)->first();
                    $cont = $cont + 1;
                    if (!isset($IdInterventionPhoto)) {
                        Image::make($InterventionPhoto)->save(static::$dirImage . $interventionOnDb->id . '_' . $cont . '.JPG');
                        $RowInterventionPhoto = new GrInterventionPhoto();
                        $RowInterventionPhoto->intervention_id = $interventionOnDb->id;
                        $RowInterventionPhoto->img = '/storage/images/Items/' . $interventionOnDb->id . '_' . $cont . '.JPG';
                        $RowInterventionPhoto->save();
                    }
                }
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
 } 

<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoSupports;
use App\DtoModel\DtoSupportsPrices;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\Supports;
use App\SupportsPrices;
use App\ItemSupport;
use App\LanguageUnitMeasure;
use App\UnitMeasure;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SupportsBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Supports::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    /**
     * Validate data support prices 
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateDataSupportPrices(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (SupportsPrices::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return dtoSupports
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (DB::table('supports')->get() as $supportsAll) {
            $dtoSupports = new DtoSupports();
            $dtoSupports->id = $supportsAll->id;
            $dtoSupports->description = $supportsAll->description; 
            $dtoSupports->width = $supportsAll->width;  
            $dtoSupports->height = $supportsAll->height;  
            $dtoSupports->depth = $supportsAll->depth;  
            $dtoSupports->area = $supportsAll->area;  
            $dtoSupports->margins = $supportsAll->margins;  
            $dtoSupports->um_dimension_id = $supportsAll->um_dimension_id;  
            $dtoSupports->um_area_id = $supportsAll->um_area_id;  
            if(!is_null($supportsAll->um_prices_id) && $supportsAll->um_prices_id!= ''){
                $codeUm = UnitMeasure::where('id', $supportsAll->um_prices_id)->get()->first();
                if(isset($codeUm)){
                    $dtoSupports->um_prices_id = $supportsAll->um_prices_id;
                    $dtoSupports->um_description_prices = $codeUm->code;
                }else{
                    $dtoSupports->um_prices_id = $supportsAll->um_prices_id;
                    $dtoSupports->um_description_prices = ' - ';
                }
            }else{
                $dtoSupports->um_prices_id = ' - ';
                $dtoSupports->um_description_prices = ' - ';
            }
            

            $result->push($dtoSupports); 
        }
        
        return $result;
    }

    /**
     * Get All Prices Supports
     * 
     * @param int id
     * 
     * @return dtoSupportsPrices
     */
    public static function getAllPricesSupports($id)
    {
        $result = collect();
        foreach (DB::table('supports_prices')->where('supports_id', $id)->get() as $supportsAllPrices) {
            $dtoSupportsPrices = new DtoSupportsPrices();
            $dtoSupportsPrices->id = $supportsAllPrices->id;
            $dtoSupportsPrices->supports_id = $supportsAllPrices->supports_id;
            $dtoSupportsPrices->from = $supportsAllPrices->from;
            $dtoSupportsPrices->to = $supportsAllPrices->to;
            $dtoSupportsPrices->price = $supportsAllPrices->price;
           
            $result->push($dtoSupportsPrices); 
        }
        
        return $result;
    }
    
    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $supports = new Supports();
            $supports->description = $request->description;

            if(strpos($request->width, ',')){
                $width = str_replace(',', '.', $request->width);
            }else{
                $width = $request->width;
            }
            $supports->width = $width;

            if(strpos($request->height, ',')){
                $height = str_replace(',', '.', $request->height);
            }else{
                $height = $request->height;
            }
            $supports->height = $height;

            if(strpos($request->depth, ',')){
                $depth = str_replace(',', '.', $request->depth);
            }else{
                $depth = $request->depth;
            }
            $supports->depth = $depth;

            if(strpos($request->area, ',')){
                $area = str_replace(',', '.', $request->area);
            }else{
                $area = $request->area;
            }
            $supports->area = $area;

            if(strpos($request->margins, ',')){
                $margins = str_replace(',', '.', $request->margins);
            }else{
                $margins = $request->margins;
            }
            $supports->margins = $margins;

            $supports->um_dimension_id = $request->um_dimension_id;
            $supports->um_area_id = $request->um_area_id;
            $supports->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $supports->id;
    }

    /**
     * insert Support Prices
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertSupportPrice(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateDataSupportPrices($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $supportsPrices = new SupportsPrices();
            $supportsPrices->supports_id = $request->id;

            if(strpos($request->from, ',')){
                $from = str_replace(',', '.', $request->from);
            }else{
                $from = $request->from;
            }
            $supportsPrices->from = $from;

            if(strpos($request->to, ',')){
                $to = str_replace(',', '.', $request->to);
            }else{
                $to = $request->to;
            }
            $supportsPrices->to = $to;

            if(strpos($request->price, ',')){
                $price = str_replace(',', '.', $request->price);
            }else{
                $price = $request->price;
            }
            $supportsPrices->price = $price;
            
            $supportsPrices->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $supportsPrices->id;
    }

    /**
     * insert Item Support
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertItemSupport(Request $request, int $idUser, int $idLanguage)
    {
        $itemSupport = ItemSupport::where('supports_id', $request->idSupport)->where('item_id', $request->idItem)->get()->first();
        if(isset($itemSupport)){
            $itemSupportOnDb = ItemSupport::find($itemSupport->id);
            $itemSupportOnDb->delete();
            $return = 0;
        }else{
            $itemSupportsNew = new ItemSupport();
            $itemSupportsNew->item_id = $request->idItem;
            $itemSupportsNew->supports_id = $request->idSupport;
            $itemSupportsNew->save();
            $return = $itemSupportsNew->id;
        }
        return $return;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request request
     * @param int idLanguage
     */
    public static function update(Request $request, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $supportsOnDb = Supports::find($request->id);
        
        DB::beginTransaction();

        try {
            $supportsOnDb->description = $request->description;
            if(strpos($request->width, ',')){
                $width = str_replace(',', '.', $request->width);
            }else{
                $width = $request->width;
            }
            $supportsOnDb->width = $width;

            if(strpos($request->height, ',')){
                $height = str_replace(',', '.', $request->height);
            }else{
                $height = $request->height;
            }
            $supportsOnDb->height = $height;

            if(strpos($request->depth, ',')){
                $depth = str_replace(',', '.', $request->depth);
            }else{
                $depth = $request->depth;
            }
            $supportsOnDb->depth = $depth;

            if(strpos($request->area, ',')){
                $area = str_replace(',', '.', $request->area);
            }else{
                $area = $request->area;
            }
            $supportsOnDb->area = $area;

            if(strpos($request->margins, ',')){
                $margins = str_replace(',', '.', $request->margins);
            }else{
                $margins = $request->margins;
            }
            $supportsOnDb->margins = $margins;

            $supportsOnDb->um_dimension_id = $request->um_dimension_id;
            $supportsOnDb->um_area_id = $request->um_area_id;

            $supportsOnDb->save();
           
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }


    /**
     * Update support price
     * 
     * @param Request request
     * @param int idLanguage
     */
    public static function updateSupportPrice(Request $request, int $idLanguage)
    {
        static::_validateDataSupportPrices($request, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $supportsPricesOnDb = SupportsPrices::find($request->id);
        
        DB::beginTransaction();

        try {
            if(strpos($request->from, ',')){
                $from = str_replace(',', '.', $request->from);
            }else{
                $from = $request->from;
            }
            $supportsPricesOnDb->from = $from;

            if(strpos($request->to, ',')){
                $to = str_replace(',', '.', $request->to);
            }else{
                $to = $request->to;
            }
            $supportsPricesOnDb->to = $to;

            if(strpos($request->price, ',')){
                $price = str_replace(',', '.', $request->price);
            }else{
                $price = $request->price;
            }
            $supportsPricesOnDb->price = $price;

            $supportsPricesOnDb->save();
           
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    /**
     * Update Um support price
     * 
     * @param Request request
     * @param int idLanguage
     */
    public static function updateUmSupportPrices(Request $request, int $idLanguage)
    {
        //static::_validateDataSupportPrices($request, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $supportsOnDb = Supports::find($request->id);
        
        DB::beginTransaction();

        try {
            
            $supportsOnDb->um_prices_id = $request->idUm;

            $supportsOnDb->save();
           
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
    
    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $supports = Supports::find($id);
        
        if (is_null($supports)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, 'Supporto non trovato!'), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();
        try {
            
            $supports->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    /**
     * delete Range Detail Supports Price
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function deleteRangeDetailSupportsPrice(int $id, int $idLanguage)
    {
        $supportsPrices = SupportsPrices::find($id);
        
        if (is_null($supportsPrices)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, 'Range prezzo supporto non trovato!'), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();
        try {
            
            $supportsPrices->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    #endregion DELETE
}

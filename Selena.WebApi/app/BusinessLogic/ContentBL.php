<?php

namespace App\BusinessLogic;

use App\Content;
use App\ContentLanguage;
use App\DocumentRowLanguage;
use App\LanguagePage;
use App\ItemLanguage;
use App\CategoryLanguage;
use App\CategoryFather;
use App\CategoryItem;


use App\LinkPageCategory;
use App\LanguagePageCategory;
use App\PageCategoryFather;
use App\Enums\ContentEnum;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\Language;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
 
 
        

class ContentBL
{
    #region PRIVATE

    /**
     * Get sub categories
     *
     * @param int $idCategoryFather
     */
    private static function _getSubAllListCategories($idCategoryFather, $contentNew, $ln)
    {
        foreach (CategoryFather::where('category_id', $idCategoryFather)->orderBy('order')->get() as $categoryAll) {
            $categoryLanguage = CategoryLanguage::where('category_id', $categoryAll->category_id)->where('language_id', $ln)->get()->first();
            
            if(!is_null($categoryAll->category_father_id)){
                $contentNew = static::_getSubAllListCategories($categoryAll->category_father_id, $contentNew, $ln);
            }
            $contentNew = $contentNew . '<li class="breadcrumb-item" aria-current="page"><a href="' . $categoryLanguage->meta_tag_link . '">' . $categoryLanguage->description . '</a></li>';
        }

        return $contentNew;
    }

    /**
     * Check validation
     *
     * @param Request request
     * @param Int idUser
     * @param Int idLanguage
     * @param DictionariesCodesEnum dbOperationsTypesEnum
     */
    private static function _checkValidation(Request $request, int $idUser, int $idLanguage, $dbOperationsTypesEnum)
    {
        FunctionBL::checkFunctionEnabled($request->idFunctionality, $idUser, $idLanguage);

        if (!ContentEnum::hasValue($request->code)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContentNotDecode, HttpResultsCodesEnum::InvalidPayload));
        }

        if (is_null(LanguageBL::get($request->idLanguage))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::LanguageNotExist, HttpResultsCodesEnum::InvalidPayload));
        }

        if ($dbOperationsTypesEnum == DbOperationsTypesEnum::INSERT && !is_null(Content::where('code', $request->code))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContentAlreadyExists, HttpResultsCodesEnum::InvalidPayload));
        }

        if ($dbOperationsTypesEnum == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued, HttpResultsCodesEnum::InvalidPayload));
            }

            $content = Content::where('code', $request->code)->first();

            if (!is_null($content) && $content->id != $request->id) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContentNotFound, HttpResultsCodesEnum::InvalidPayload));
            }
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get all
     *
     * @param Int idFunctionality
     * @param Int idUser
     * @param Int idLanguage
     *
     * @return Content
     */
    public static function getAll(int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
        
        //return Content::all();
        $Languages = Language::where('description', 'Italiano')->first();
        return DB::table('content')->join('content_languages', 'content.id', '=', 'content_languages.content_id')->where('content_languages.language_id', $Languages->id)
        ->select('content.id','content.code','content_languages.description')->get();
    }

    /**
     * Get
     *
     * @param Int id
     *
     * @return Content
     */
    public static function get($id)
    {
        return Content::find($id);
    }

    /**
     * Get by code
     *
     * @param String code
     * @param Int idLanguage
     *
     * @return String
     */
    public static function getByCode(string $code, int $idLanguage = null)
    {
        if (is_null($code)) {
            LogHelper::error('Code of content not found');
            return '';
        } else {
            $content = Content::where('code', $code)->first();

            if (is_null($content)) {
                LogHelper::error('Content not found');
                return '';
            } else {
                if (is_null($idLanguage)) {
                    $idLanguageFinal = LanguageBL::getDefault()->id;
                }else{
                    $idLanguageFinal= $idLanguage;
                  }
                return ContentLanguageBL::getByCodeAndLanguage($content->id, $idLanguageFinal);
            }
        }
    }

    /**
     * Get by code User
     *
     * @param String code
     * @param Int idLanguage
     *
     * @return String
     */
    public static function getByCodeUser(Request $request)
    {
        if (is_null($request->code)) {
            LogHelper::error('Code of content not found');
            return '';
        } else {
            $content = Content::where('code', $request->code)->first();

            if (is_null($content)) {
                LogHelper::error('Content not found');
                return '';
            } else {
                if (is_null($request->idLanguage)) {
                    $idLanguage = LanguageBL::getDefault()->id;
                }else{
                    $idLanguage =  $request->idLanguage;
                }

                if(isset($request->salesmanId)){
                    $salesmanId = $request->salesmanId;
                }else{
                    $salesmanId = "";
                }
                
                return ContentLanguageBL::getByCodeUserAndLanguage($content->id, $idLanguage, $request->userId, $request->cartId, $request->url, $salesmanId);
            }
        }
    }

    /**
     * Get by code
     *
     * @param String code
     * @param Int idLanguage
     *
     * @return String
     */
    public static function getByCodeRender(Request $request)
    {
        if (is_null($request->code)) {
            LogHelper::error('Code of content not found');
            return '';
        } else {
            $content = Content::where('code', $request->code)->first();

            if (is_null($content)) {
                //LogHelper::error('Content not found');
                return '';
            } else {
                if (is_null($request->ln) || $request->ln == '') {
                    $idLanguage = LanguageBL::getDefault()->id;
                }else{
                    $idLanguage = $request->ln;
                }

                if (is_null($request->userId) || $request->userId == '') {
                    $userId = null;
                }else{
                    $userId = $request->userId;
                }

                if (is_null($request->param) || $request->param == '') {
                    $param = null;
                }else{
                    $param = $request->param;
                }

                if (is_null($request->cart_id) || $request->cart_id == '') {
                    return ContentLanguageBL::getByCodeRender($content->id, $idLanguage, $request->url, null, null, $userId, null, $param);
                }else{
                    return ContentLanguageBL::getByCodeRender($content->id, $idLanguage, $request->url, $request->cart_id, null, $userId, null, $param);    
                }
            }
        }
    }

    /**
     * get Tag By Type
     *
     * @param String code
     *
     * @return String
     */
    public static function getTagByType(string $code)
    {
        if (is_null($code)) {
            return '';
        } else {
           return SelenaViewsBL::getTagByType($code);
        }
    }

    

    /**
     * get breadcrumbs
     *
     * @param Request request
     *
     * @return Request
     */
    public static function getBreadcrumbs(Request $request)
    {
        $content = '';
        if(isset($request->url)){
            if($request->url != '/'){
                $page = LanguagePage::where('url', $request->url)->where('language_id', $request->ln)->get()->first();
                if(isset($page)){
                    $content = $content . '<nav aria-label="breadcrumb">';
                    $content = $content . '<ol class="breadcrumb">';
                    $content = $content . '<li class="breadcrumb-item active" aria-current="page">' . $page->title . '</li>';
                    $content = $content . '<li class="breadcrumb-item" aria-current="page"><a href="/">Home</a></li>';
                    $content = $content . '</ol>';
                    $content = $content . '</nav>';
                }else{
                    $item = ItemLanguage::where('link', $request->url)->where('language_id', $request->ln)->get()->first();
                    if(isset($item)){
                        $content = $content . '<nav aria-label="breadcrumb">';
                        $content = $content . '<ol class="breadcrumb">';
                        $content = $content . '<li class="breadcrumb-item" aria-current="page"><a href="/">Home</a></li>';
                        
                        $categoryItem = CategoryItem::where('item_id', $item->item_id)->get()->first();
                        if(isset($categoryItem)){
                            $CategoryFather = CategoryFather::where('category_id', $categoryItem->category_id)->get()->first();
                            if(isset($CategoryFather)){
                                if(!is_null($CategoryFather->category_father_id)){
                                    $content = $content . static::_getSubAllListCategories($CategoryFather->category_father_id, '', $request->ln);
                                }
                            }
                            //$content = $content . static::_getSubAllListCategories($categoryItem->category_id, '', $request->ln);
                            $categoryLanguage = CategoryLanguage::where('category_id', $categoryItem->category_id)->get()->first();
                            if(isset($categoryLanguage)){
                                $content = $content . '<li class="breadcrumb-item" aria-current="page"><a href="' . $categoryLanguage->meta_tag_link . '">' . $categoryLanguage->description . '</a></li>';
                            }
                        }

                        $content = $content . '<li class="breadcrumb-item active" aria-current="page">' . $item->description . '</li>';
                        $content = $content . '</ol>';
                        $content = $content . '</nav>';
                    }else{
                        $categoryLanguage = CategoryLanguage::where('meta_tag_link', $request->url)->where('language_id', $request->ln)->get()->first();
                        if(isset($categoryLanguage)){
                            $content = $content . '<nav aria-label="breadcrumb">';
                            $content = $content . '<ol class="breadcrumb">';
                            $content = $content . '<li class="breadcrumb-item" aria-current="page"><a href="/">Home</a></li>';
                            $CategoryFather = CategoryFather::where('category_id', $categoryLanguage->category_id)->get()->first();
                            
                            if(isset($CategoryFather)){
                                if(!is_null($CategoryFather->category_father_id)){
                                    $content = $content . static::_getSubAllListCategories($CategoryFather->category_father_id, '', $request->ln);
                                }
                            }
                            $content = $content . '<li class="breadcrumb-item active" aria-current="page">' . $categoryLanguage->description . '</li>';
                            $content = $content . '</ol>';
                            $content = $content . '</nav>';

                        }
                    }
                }
            } 
        }
        return $content;
    }

    #endregion GET




  /**
     * get _getBreadCrumbs
     *
     * @param String url
     *
     * @return string $content
     */
    public static function getBreadCrumbsPages($url)
    {
        $content = '';
        if(isset($url)){
            if($url != '/'){
                $linkcategory = LinkPageCategory::where('url', $url)->get()->first();
                if(isset($linkcategory)){
                    $item = LanguagePageCategory::where('page_category_id', $linkcategory->page_category_id)->get()->first();  
                    if(isset($item)){
                        $content = $content . '<nav aria-label="breadcrumb">';
                        $content = $content . '<ol class="breadcrumb">';
                        $content = $content . '<li class="breadcrumb-item" aria-current="page"><a href="/">Home</a></li>';
                        $CategoryFather = PageCategoryFather::where('page_category_id', $item->page_category_id)->get()->first();
                        if(!is_null($CategoryFather->page_category_father_id)){
                            $content = $content . static::_getSubAllListCategoriesPages($CategoryFather->page_category_father_id, '');
                        }
                        $content = $content . '<li class="breadcrumb-item active" aria-current="page">' . $item->description . '</li>';
                        $content = $content . '</ol>';
                        $content = $content . '</nav>';
                    }  
                }
            } 
        }
        return $content;
    }

 /**
     *
     * @param int $idCategoryFather
     */
    private static function _getSubAllListCategoriesPages($idCategoryFather, $contentNew)
    {
        foreach (PageCategoryFather::where('page_category_id', $idCategoryFather)->orderBy('order')->get() as $categoryAll) {
            $categoryLanguage = LanguagePageCategory::where('page_category_id', $categoryAll->page_category_id)->get()->first();
            $linkcategory = LinkPageCategory::where('page_category_id', $categoryAll->page_category_id)->get()->first();
            if(!is_null($categoryAll->page_category_father_id)){
                $contentNew = static::_getSubAllListCategoriesPages($categoryAll->page_category_father_id, $contentNew);
            }
            $contentNew = $contentNew . '<li class="breadcrumb-item" aria-current="page"><a href="' . $linkcategory->url . '">' . $categoryLanguage->description . '</a></li>';
            
        }
        return $contentNew;
    }



    #region INSERT

    /**
     * Insert
     *
     * @param Request request
     * @param Int idUser
     * @param Int idLanguage
     *
     * @return Int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_checkValidation($request, $idUser, $idLanguage, DbOperationsTypesEnum::INSERT);
        $content = new Content();
        DB::beginTransaction();

        try {
            $content->code = $request->code;
            $content->save();

            ContentLanguageBL::insertOrUpdate($content->id, $request->idLanguage, $request->content);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $content->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request request
     * @param Int idUser
     * @param Int idLanguage
     */
    public static function update(Request $request, int $idUser, int $idLanguage)
    {
        static::_checkValidation($request, $idUser, $idLanguage, DbOperationsTypesEnum::UPDATE);

        

        DB::beginTransaction();

        try {
            $content = Content::find($request->id);
            $content->code = $request->code;
            $content->save();
            $description = '';
        
            if(isset($request->description)){
                $description = $request->description;
            }else{
                $description = '';
            }

            ContentLanguageBL::insertOrUpdate($content->id, $request->idLanguage, $request->content, $description);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param Int id
     * @param Int idFunctionality
     * @param Int idUser
     * @param Int idLanguage
     */
    public static function delete(int $id, int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        DB::beginTransaction();

        try {
            $content = Content::find($id);

            if (is_null($content)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContentNotFound), HttpResultsCodesEnum::InvalidPayload);
            }

            ContentLanguageBL::deleteByContent($id);

            $content->delete();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion DELETE
}

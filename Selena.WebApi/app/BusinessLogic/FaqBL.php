<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoFaq;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\FaqAdmin;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FaqBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Optional::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoOptional
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (DB::table('faq_admin')
                ->orderBy('faq_admin.order')
                ->get()
        as $optionalAll) {
            
            $dtoOptional = new DtoFaq();
            $dtoOptional->id = $optionalAll->id;
            $dtoOptional->shortcode = $optionalAll->shortcode;
            $dtoOptional->description = $optionalAll->description;
            $dtoOptional->where = $optionalAll->where;
            $dtoOptional->how = $optionalAll->how;
            $dtoOptional->what = $optionalAll->what;
            $dtoOptional->order = $optionalAll->order;
            $result->push($dtoOptional); 
        }
        
        return $result;
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $optional = new FaqAdmin();
            $optional->shortcode = $request->shortcode;
            $optional->description = $request->description;
            $optional->where = $request->where;
            $optional->how = $request->how;
            $optional->what = $request->what;
            $optional->order = $request->order;
            $optional->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        //return $optional->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoOptional
     * @param int idLanguage
     */
    public static function update(Request $dtoOptional, int $idLanguage)
    {
        static::_validateData($dtoOptional, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $optionalOnDb = FaqAdmin::find($dtoOptional->id);
        
        DB::beginTransaction();

        try {
            $optionalOnDb->shortcode = $dtoOptional->shortcode;
            $optionalOnDb->description = $dtoOptional->description;
            $optionalOnDb->where = $dtoOptional->where;
            $optionalOnDb->how = $dtoOptional->how;
            $optionalOnDb->what = $dtoOptional->what;
            $optionalOnDb->order = $dtoOptional->order;
            $optionalOnDb->save();
            

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $optional = FaqAdmin::find($id);
        
        if (is_null($optional)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            
            $optional->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    #endregion DELETE
}

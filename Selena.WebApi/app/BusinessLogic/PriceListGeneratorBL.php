<?php

namespace App\BusinessLogic;

use App\Category;
use App\CategoryLanguage;
use App\Contact;
use App\DtoModel\DtoCities;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Mail\ContactRequestMail;
use App\Cities;
use App\DtoModel\DtoCategories;
use App\DtoModel\DtoList;
use App\DtoModel\DtoListGenerator;
use App\DtoModel\DtoUser;
use App\GroupNewsletter;
use App\Item;
use App\ListGenerator;
use App\ListGeneratorDestinationCategories;
use App\ListGeneratorDestinationGroup;
use App\ListGeneratorDestinationUsers;
use App\ListGeneratorStartingCategories;
use App\ListGeneratorStartingGroup;
use App\ListGeneratorStartingUsers;
use App\PriceList;
use App\Scheduler;
use App\User;
use App\UsersDatas;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class PriceListGeneratorBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Cities::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->order)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SurnameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->istat)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SurnameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

    }

    /**
     * Conver to model
     * 
     * @param Request request
     * 
     * @return Contact
     */
    private static function _convertToModel(Request $request)
    {
        $cities = new Cities();

        if (isset($request->id)) {
            $cities->id = $request->id;
        }

        if (isset($request->order)) {
            $cities->order = $request->order;
        }

        if (isset($request->description)) {
            $cities->description = $request->description;
        }

        if (isset($request->istat)) {
            $cities->istat = $request->istat;
        }

        return $cities;
    }

    /**
     * create string for scheduler action
     * 
     * @param Request request
     * 
     * @return action
     */
    private static function _createStringCalculatedPrice($dtoListGenerator, $stringPrice)
    {
        $strReturn = "";
        $operator = "+";
        $percent = floatval($dtoListGenerator->percent);
        $valueFix = floatval($dtoListGenerator->value);

        if ($dtoListGenerator->check_noprice_change == 0){
            $strReturn = $stringPrice;
        }
        if ($dtoListGenerator->check_noprice_change_percentage == 1){
            if($percent < 0){
                $operator = "-";
                $percent = $percent * -1;
            }
            $strReturn = $stringPrice . $operator . "((" . $stringPrice . "*" . $percent . ")/100)";

        }

        if ($dtoListGenerator->check_price_change_value == 2){
            if($valueFix < 0){
                $operator = "-";
                $valueFix = $valueFix * -1;
            }
            $strReturn = $stringPrice . $operator . $valueFix;
        }
        return $strReturn;
    }
     /**
     * create string for scheduler action
     * 
     * @param Request request
     * 
     * @return action
     */
    private static function _createStringAction($dtoListGenerator)
    {
        $strQuery = "";
        $strQueryDelete = "";
        $strQueryInsert = "";
        $strQueryWhere = "";

        $strQueryFinal = "";

        if ($dtoListGenerator->check_destination_baseprice === 0){
            // update price on table items
            $strQuery = "UPDATE items SET price = ";
        }
        if ($dtoListGenerator->check_destination_list == 1){
            if ($dtoListGenerator->id_destination_price_list != 'null') {
                //delete item price list where price_list_id = id_destination_price_list

                $strQueryDelete = " DELETE FROM items_pricelist WHERE pricelist_id = " . $dtoListGenerator->id_destination_price_list;
                //insert into item price list 
                $strQueryInsert = " INSERT INTO items_pricelist (price, item_id, pricelist_id, created_id) SELECT ";
            }
        }

        if ($dtoListGenerator->check_starting_purchase_cost === 0){
            
            //select cost in items table
            $getStrCalculated = static::_createStringCalculatedPrice($dtoListGenerator, "cost");
            if($strQuery!= ''){
                $strQuery = $strQuery . $getStrCalculated;
            }
            // se stringa di insert è diversa da niente allora vuol dire che sono in modalità di inserimento in listino
            if($strQueryInsert!=''){
                $strQueryInsert = $strQueryInsert . $getStrCalculated . ", items.id, " . $dtoListGenerator->id_destination_price_list . ", 1 ";
                $strQueryInsert = $strQueryInsert . " FROM items ";
            }
        }

        if ($dtoListGenerator->check_starting_base_price === '1'){
            //select price in items table
            $getStrCalculated = static::_createStringCalculatedPrice($dtoListGenerator, "price");
            if($strQuery!= ''){
                $strQuery = $strQuery . $getStrCalculated;
            }
            // se stringa di insert è diversa da niente allora vuol dire che sono in modalità di inserimento in listino
            if($strQueryInsert!=''){
                $strQueryInsert = $strQueryInsert . $getStrCalculated . ", items.id, " . $dtoListGenerator->id_destination_price_list . ", 1 ";
                $strQueryInsert = $strQueryInsert . " FROM items ";
            }
        }

        if ($dtoListGenerator->check_starting_list === '2'){
            if (isset($dtoListGenerator->id_starting_price_list) || ($dtoListGenerator->id_starting_price_list != 'null')){
                //select item_price_list in items table
                $getStrCalculated = static::_createStringCalculatedPrice($dtoListGenerator, "CAST(items_pricelist.price as FLOAT)");
                
                if($strQuery!= ''){
                    $strQuery = $strQuery . $getStrCalculated;
                    $strQuery = $strQuery . " FROM items INNER JOIN items_pricelist ON items.id = items_pricelist.id ";    
                }

                if($strQueryInsert!=''){
                    $strQueryInsert = $strQueryInsert . $getStrCalculated . ", items.id, " . $dtoListGenerator->id_destination_price_list . ", 1 ";
                    $strQueryInsert = $strQueryInsert . " FROM items INNER JOIN items_pricelist ON items.id = items_pricelist.id ";
                }
            }               
        }

        /* where section */
        if ($dtoListGenerator->check_starting_categories == true){
            $listIdCat = "";
            //$strQueryWhere = $strQueryWhere . " FROM items as b INNER JOIN categories_items ON categories_items.item_id = b.id ";
            foreach ($dtoListGenerator->tableDataUserCategoriesStarting as $ListCategoryResult) {
                if ($ListCategoryResult['checkCategoriesStarting']== true){
                    if($listIdCat ==''){
                        $listIdCat = $ListCategoryResult['id'];
                    }else{
                        $listIdCat = $listIdCat . "," . $ListCategoryResult['id'];
                    }
                }
            }
            //$strQueryWhere = $strQueryWhere . " WHERE categories_items.category_id in (" . $listIdCat . ")";
            $strQueryWhere = $strQueryWhere . " WHERE id in (SELECT categories_items.item_id FROM categories_items WHERE categories_items.category_id in (" . $listIdCat . "))";
            
        }
        /* end where section */


        
        if($strQuery != '' && $strQueryWhere != '' && $strQueryInsert==''){
            $strQuery = $strQuery . $strQueryWhere;
        }


        $strQueryFinal = $strQuery . "; ";
        
        if($strQueryDelete != ''){
            $strQueryFinal = $strQueryFinal . $strQueryDelete . '; ';
        }

        if($strQueryInsert != ''){
            $strQueryFinal = $strQueryFinal . $strQueryInsert . '; ';
        }
        
        return $strQueryFinal;
    }
    


    public static function getAllByUser(Request $dtoListGenerator)
    {
        $result = collect();
  
        foreach (UsersDatas::orderBy('user_id')->get() as $usersdatas) {
            $ListGeneratorStartingUsers = ListGeneratorStartingUsers::where('id_list_generator', $dtoListGenerator->rowSelected)->where('user_id', $usersdatas->user_id)->get()->first();
            $DtoUsers = new DtoUser();
            if (isset($ListGeneratorStartingUsers)){
                $DtoUsers->checkUser = true;       
            }else{
                $DtoUsers->checkUser = false;     
            }

            $DtoUsers->id = $usersdatas->user_id;
            $DtoUsers->name = $usersdatas->name;
            $DtoUsers->surname = $usersdatas->surname;
            $DtoUsers->business_name = $usersdatas->business_name;
            $result->push($DtoUsers);
        }
            return $result;
    }    

    public static function getByExecuteResult(Request $dtoListGenerator)
    {
        $result = collect();

        if ($dtoListGenerator->check_starting_purchase_cost === 0){
            if ($dtoListGenerator->check_starting_categories == true){
                foreach ($dtoListGenerator->tableDataUserCategoriesStarting as $ListCategoryResult) {
                    if ($ListCategoryResult['checkCategoriesStarting']== true){

                        foreach (DB::select(
                        'SELECT i.id, i.internal_code,il.description, i.cost
                        FROM items i 
                        INNER JOIN categories_items ci ON ci.item_id = i.id
                        INNER JOIN items_languages il on il.item_id = i.id
                        WHERE ci.category_id = ' . $ListCategoryResult['id']) as $itemsResultC) {
                        
                            $DtoListGenerator = new DtoListGenerator();
                            $DtoListGenerator->id = $itemsResultC->id;
                            $DtoListGenerator->internal_code = $itemsResultC->internal_code;
                            $DtoListGenerator->description = $itemsResultC->description;
                            
                            if(is_null($itemsResultC->cost) || $itemsResultC->cost == ''){
                                $DtoListGenerator->valore_attuale = '0 €';
                            }else{
                                $DtoListGenerator->valore_attuale = $itemsResultC->cost . ' €';
                            }

                            if ($dtoListGenerator->check_noprice_change == true){
                                $DtoListGenerator->variation = ''; 
                                $DtoListGenerator->value_calculate = $itemsResultC->cost . ' €';
                            }

                            if ($dtoListGenerator->check_noprice_change_percentage == true){
                                $DtoListGenerator->variation = $dtoListGenerator->percent .' %'; 

                            if ($dtoListGenerator->percent !== '-'){
                                 $valorepercentuale = ($itemsResultC->cost * $dtoListGenerator->percent)/100;
                                 $DtoListGenerator->value_calculate = $valorepercentuale + $itemsResultC->cost . ' €';
                                }else{
                                     $valorepercentuale = ($itemsResultC->cost * $dtoListGenerator->percent)/100;
                                     $DtoListGenerator->value_calculate = $valorepercentuale - $itemsResultC->cost . ' €';
                                }   
                            }
                            if ($dtoListGenerator->check_price_change_value == true){
                                $DtoListGenerator->variation = $dtoListGenerator->value .' €';
                                if ($dtoListGenerator->value !== '-'){
                                    $DtoListGenerator->value_calculate = $dtoListGenerator->value + $itemsResultC->cost . ' €';
                                }else{
                                    $DtoListGenerator->value_calculate = $dtoListGenerator->value - $itemsResultC->cost . ' €';
                                }
                            }
                            $result->push($DtoListGenerator); 
                        }
                    }
                }
            }else{
                foreach (DB::select(
                'SELECT i.id, i.internal_code,il.description, i.cost
                FROM items i 
                INNER JOIN items_languages il on il.item_id = i.id') as $itemsResult) {

                    $DtoListGenerator = new DtoListGenerator();
                    $DtoListGenerator->id = $itemsResult->id;
                    $DtoListGenerator->internal_code = $itemsResult->internal_code;
                    $DtoListGenerator->description = $itemsResult->description;

                    if(is_null($itemsResult->cost) || $itemsResult->cost == ''){
                        $DtoListGenerator->valore_attuale = '0 €';
                    }else{
                        $DtoListGenerator->valore_attuale = $itemsResult->cost . ' €';
                    }
                    if ($dtoListGenerator->check_noprice_change == true){
                        $DtoListGenerator->variation = ''; 
                        $DtoListGenerator->value_calculate = $itemsResult->cost . ' €';
                    }

                    if ($dtoListGenerator->check_noprice_change_percentage == true){
                            $DtoListGenerator->variation = $dtoListGenerator->percent .' %'; 

                        if ($dtoListGenerator->percent !== '-'){
                             $valorepercentuale = ($itemsResult->cost * $dtoListGenerator->percent)/100;
                             $DtoListGenerator->value_calculate = $valorepercentuale + $itemsResult->cost . ' €';
                        }else{
                             $valorepercentuale = ($itemsResult->cost * $dtoListGenerator->percent)/100;
                             $DtoListGenerator->value_calculate = $valorepercentuale - $itemsResult->cost . ' €';
                        }   
                    }

                    if ($dtoListGenerator->check_price_change_value == true){
                        $DtoListGenerator->variation = $dtoListGenerator->value .' €';
                        if ($dtoListGenerator->value !== '-'){
                            $DtoListGenerator->value_calculate = $dtoListGenerator->value + $itemsResult->cost . ' €';
                        }else{
                            $DtoListGenerator->value_calculate = $dtoListGenerator->value - $itemsResult->cost . ' €';
                        }
                    }
                    $result->push($DtoListGenerator);
                
                }   
            }
        }else{

            if ($dtoListGenerator->check_starting_base_price === 1){
              
                if ($dtoListGenerator->check_starting_categories == true){

                    foreach ($dtoListGenerator->tableDataUserCategoriesStarting as $ListCategoryResult) {

                        if ($ListCategoryResult['checkCategoriesStarting']== true){

                            foreach (DB::select(
                            'SELECT i.id, i.internal_code,il.description, i.price
                            FROM items i 
                            INNER JOIN categories_items ci ON ci.item_id = i.id
                            INNER JOIN items_languages il on il.item_id = i.id
                            WHERE ci.category_id = ' . $ListCategoryResult['id']) as $itemsResulst) {

                                $DtoListGenerator = new DtoListGenerator();
                                $DtoListGenerator->id = $itemsResulst->id;
                                $DtoListGenerator->internal_code = $itemsResulst->internal_code;
                                $DtoListGenerator->description = $itemsResulst->description;
                                
                                if(is_null($itemsResulst->price) || $itemsResulst->price == ''){
                                    $DtoListGenerator->valore_attuale = '0 €';
                                }else{
                                    $DtoListGenerator->valore_attuale = $itemsResulst->price . ' €';
                                }
                                if ($dtoListGenerator->check_noprice_change == true){
                                    $DtoListGenerator->variation = ''; 
                                    $DtoListGenerator->value_calculate = $itemsResulst->price . ' €';
                                }

                                if ($dtoListGenerator->check_noprice_change_percentage == true){
                                    $DtoListGenerator->variation = $dtoListGenerator->percent .' %'; 
                                    if ($dtoListGenerator->percent !== '-'){
                                         $valorepercentuale = ($itemsResulst->price * $dtoListGenerator->percent)/100;
                                         $DtoListGenerator->value_calculate = $valorepercentuale + $itemsResulst->price . ' €';
                                    }else{
                                         $valorepercentuale = ($itemsResulst->price * $dtoListGenerator->percent)/100;
                                         $DtoListGenerator->value_calculate = $valorepercentuale - $itemsResulst->price . ' €';
                                    }   
                                }
                                if ($dtoListGenerator->check_price_change_value == true){
                                    $DtoListGenerator->variation = $dtoListGenerator->value .' €';
                                    if ($dtoListGenerator->value !== '-'){
                                        $DtoListGenerator->value_calculate = $dtoListGenerator->value + $itemsResulst->price . ' €';
                                    }else{
                                        $DtoListGenerator->value_calculate = $dtoListGenerator->value - $itemsResulst->price . ' €';
                                    }
                                }
                                $result->push($DtoListGenerator); 
                            
                            }
                        }  
                    }       
                }else{
                    
                    foreach (DB::select(
                    'SELECT i.id, i.internal_code,il.description, i.price
                    FROM items i 
                    INNER JOIN items_languages il on il.item_id = i.id') as $itemsResult) {
                    
                        if ($itemsResult->price != ''){
                            $DtoListGenerator = new DtoListGenerator();
                            $DtoListGenerator->id = $itemsResult->id;
                            $DtoListGenerator->internal_code =  $itemsResult->internal_code;
                            $DtoListGenerator->description = $itemsResult->description;
                            $DtoListGenerator->valore_attuale = $itemsResult->price;

                            if ($dtoListGenerator->check_noprice_change == true){
                                $DtoListGenerator->variation = ''; 
                                $DtoListGenerator->value_calculate =  $itemsResult->price;
                            }
                            if ($dtoListGenerator->check_noprice_change_percentage == true){
                                $DtoListGenerator->variation = $dtoListGenerator->percent .' %'; 
                                if ($dtoListGenerator->percent !== '-'){
                                      $valorepercentuale = ($itemsResult->price * $dtoListGenerator->percent)/100;
                                      $DtoListGenerator->value_calculate = $valorepercentuale + $itemsResult->price;
                                }else{
                                      $valorepercentuale = ($itemsResult->price * $dtoListGenerator->percent)/100;
                                      $DtoListGenerator->value_calculate = $valorepercentuale - $itemsResult->price;
                                }   
                            }
                            if ($dtoListGenerator->check_price_change_value == true){
                                $DtoListGenerator->variation = $dtoListGenerator->value .' €';
                                if ($dtoListGenerator->value !== '-'){
                                    $DtoListGenerator->value_calculate = $dtoListGenerator->value +  $itemsResult->price;
                                }else{
                                    $DtoListGenerator->value_calculate = $dtoListGenerator->value -  $itemsResult->price;
                                }
                            }
                            $result->push($DtoListGenerator); 
                        } 
                    } 
                }  
            }else{
                if ($dtoListGenerator->check_starting_list === 2){
                    /* if (isset($dtoListGenerator->check_starting_customer)  && 
                    ($dtoListGenerator->check_starting_customer == true) && ($dtoListGenerator->check_starting_group == true) && 
                    ($dtoListGenerator->check_starting_categories == true))
                    {*/
                    $strWhere = "";
                    foreach ($dtoListGenerator->tableDataDestinationUsers as $ListListUsersResult) {
                        if ($ListListUsersResult['checkUser']== true){
                            if ($ListListUsersResult['id'] != "") {
                                $strWhere = $strWhere . ' OR ud.user_id = ' . $ListListUsersResult['id'] . '';
                            } 
                        }
                    }
                    foreach ($dtoListGenerator->tableDataDestinationGroup as $ListListGroupResult) {
                        if ($ListListGroupResult['checkGroupStarting']== true){
                            if ($ListListGroupResult['id'] != "") {
                                $strWhere = $strWhere . ' OR gn.id = ' . $ListListGroupResult['id'] . '';
                            } 
                        }
                    }

                    foreach ($dtoListGenerator->tableDataUserCategoriesStarting as $ListCategoryResult) {
                        if ($ListCategoryResult['checkCategoriesStarting']== true){
                            if ($ListCategoryResult['id'] != "") {
                                $strWhere = $strWhere . ' OR ci.category_id = ' . $ListCategoryResult['id'] . '';
                            } 
                        }   
                    } 

                    foreach (DB::select(
                    'SELECT DISTINCT ip.item_id, ud.business_name, ip.price, i.internal_code, il.description
                    FROM items_pricelist ip 
                    LEFT JOIN items i on i.id = ip.item_id 
                    LEFT JOIN items_languages il on il.item_id = i.id
                    LEFT JOIN users_datas ud on ud.id_price_list = ip.pricelist_id 
                    LEFT JOIN customers_groups_newsletters cgn2 on cgn2.customer_id = ud.user_id
                    LEFT JOIN groups_newsletters gn on gn.id = cgn2.group_newsletter_id  
                    LEFT JOIN categories_items ci on ci.item_id = i.id
                    WHERE ip.pricelist_id = ' . $dtoListGenerator->id_starting_price_list . ' ' . $strWhere) as $itemsResultSL) {

                        $DtoListGenerator = new DtoListGenerator();
                        $DtoListGenerator->business_name = $itemsResultSL->business_name;
                        $DtoListGenerator->id = $itemsResultSL->item_id;
                        $DtoListGenerator->internal_code = $itemsResultSL->internal_code;
                        $DtoListGenerator->description = $itemsResultSL->description;
                        $DtoListGenerator->valore_attuale = $itemsResultSL->price;
                        
                        if ($dtoListGenerator->check_noprice_change == true){
                            $DtoListGenerator->variation = ''; 
                            $DtoListGenerator->value_calculate = $itemsResultSL->price;
                        }
                        if ($dtoListGenerator->check_noprice_change_percentage == true){
                            $DtoListGenerator->variation = $dtoListGenerator->percent .' %'; 
                            if ($dtoListGenerator->percent !== '-'){
                                $valorepercentuale = ($itemsResultSL->price * $dtoListGenerator->percent)/100;
                                $DtoListGenerator->value_calculate = $valorepercentuale + $itemsResultSL->price;
                            }else{
                                $valorepercentuale = ($itemsResultSL->price * $dtoListGenerator->percent)/100;
                                $DtoListGenerator->value_calculate = $valorepercentuale - $itemsResultSL->price;
                            }   
                        }
                        if ($dtoListGenerator->check_price_change_value == true){
                            $DtoListGenerator->variation = $dtoListGenerator->value .' €';
                            if ($dtoListGenerator->value !== '-'){
                                $DtoListGenerator->value_calculate = $dtoListGenerator->value + $itemsResultSL->price;
                            }else{
                                $DtoListGenerator->value_calculate = $dtoListGenerator->value - $itemsResultSL->price;
                            }
                        }
                            $result->push($DtoListGenerator);  
                    }  
                }
            }
        }
        return $result;   
    }


   
    public static function getAllByGroup(Request $dtoListGenerator)
    {
        $result = collect();

        foreach (GroupNewsletter::orderBy('id')->get() as $GroupNewsletters) {
            $ListGeneratorStartingGroup = ListGeneratorStartingGroup::where('id_list_generator', $dtoListGenerator->rowSelected)->where('group_id', $GroupNewsletters->id)->get()->first();

            $DtoUsers = new DtoUser();
            if (isset($ListGeneratorStartingGroup)){
                $DtoUsers->checkGroupStarting = true;       
            }else{
                $DtoUsers->checkGroupStarting = false;     
            }

            $DtoUsers->id = $GroupNewsletters->id;
            $DtoUsers->description = $GroupNewsletters->description;
             $result->push($DtoUsers);
        }
            return $result;
    }


    public static function getAllByCategories(Request $dtoListGenerator)
    {
        $result = collect();

        foreach (CategoryLanguage::orderBy('category_id')->get() as $Categories) {
            $ListGeneratorStartingCategories = ListGeneratorStartingCategories::where('id_list_generator', $dtoListGenerator->rowSelected)->where('categories_id', $Categories->category_id)->get()->first();

            $DtoUsers = new DtoUser();
            $DtoUsers->id = $Categories->category_id;
            $DtoUsers->description = $Categories->description;
            $DtoUsers->checkCategoriesStarting = false;  

            if (isset($ListGeneratorStartingCategories)){
                $DtoUsers->checkCategoriesStarting = true;       
            }else{
                $DtoUsers->checkCategoriesStarting = false;     
            }
      
            $result->push($DtoUsers);
        }
            return $result;
    }

    public static function getAllByCategoriesNewList(Request $dtoListGenerator)
    {
        $result = collect();
        foreach (CategoryLanguage::orderBy('category_id')->get() as $Categories) {

            $DtoUsers = new DtoUser();
            $DtoUsers->id = $Categories->category_id;
            $DtoUsers->description = $Categories->description;
            $DtoUsers->checkCategoriesStarting = false;  
            $result->push($DtoUsers);
        }
            return $result;
    }


    public static function getAllByDestinationUsers(Request $dtoListGenerator)
    {
    
           $result = collect();
           
           foreach (UsersDatas::orderBy('user_id')->get() as $usersdatas) {
               $ListGeneratorStartingUsers = ListGeneratorDestinationUsers::where('id_list_generator', $dtoListGenerator->rowSelected)->where('user_id', $usersdatas->user_id)->get()->first();
              
               $DtoUsers = new DtoUser();
               if (isset($ListGeneratorStartingUsers)){
                   $DtoUsers->checkDestinationUser = true;       
               }else{
                   $DtoUsers->checkDestinationUser = false;     
               }
   
               $DtoUsers->id = $usersdatas->user_id;
               $DtoUsers->name = $usersdatas->name;
               $DtoUsers->surname = $usersdatas->surname;
               $DtoUsers->business_name = $usersdatas->business_name;
               $result->push($DtoUsers);
           }
               return $result;
    }

    public static function getAllByDestinationGroup(Request $dtoListGenerator)
    {
        $result = collect();

        foreach (GroupNewsletter::orderBy('id')->get() as $GroupNewsletters) {
            $ListGeneratorStartingGroup = ListGeneratorDestinationGroup::where('id_list_generator', $dtoListGenerator->rowSelected)->where('group_id', $GroupNewsletters->id)->get()->first();

            $DtoUsers = new DtoUser();
            if (isset($ListGeneratorStartingGroup)){
                $DtoUsers->checkGroupDestination = true;       
            }else{
                $DtoUsers->checkGroupDestination = false;     
            }

            $DtoUsers->id = $GroupNewsletters->id;
            $DtoUsers->description = $GroupNewsletters->description;
            $result->push($DtoUsers);
        }
            return $result;
    }

    public static function getAllByDestinationCategories(Request $dtoListGenerator)
    {
        $result = collect();

        foreach (CategoryLanguage::orderBy('category_id')->get() as $Categories) {
            $ListGeneratorStartingCategories = ListGeneratorDestinationCategories::where('id_list_generator', $dtoListGenerator->rowSelected)->where('categories_id', $Categories->category_id)->get()->first();

            $DtoUsers = new DtoUser();
            if (isset($ListGeneratorStartingCategories)){
                $DtoUsers->checkCategoriesDestination = true;       
            }else{
                $DtoUsers->checkCategoriesDestination = false;     
            }
            $DtoUsers->id = $Categories->category_id;
            $DtoUsers->description = $Categories->description;
            $result->push($DtoUsers);
        }
            return $result;
    }
    

    /**
     * Convert to dto
     * 
     * @param Cities region
     * 
     * @return DtoCities
     */
    private static function _convertToDto(Cities $cities)
    {
        $dtoCities = new DtoCities();
        $dtoCities->id = $cities->id;
        $dtoCities->order = $cities->order;
        $dtoCities->description = $cities->description;
        $dtoCities->istat = $cities->istat;
        return $dtoCities;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get
     *
     * @param String $search
     * @return Cities
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return Cities::select('id', 'description')->get()->take(100);
        } else {
            return Cities::where('description', 'ilike','%'. $search . '%')->select('id', 'description')->get();
        }
    }

    /**
     * Get all
     * 
     * @return DtoCities
     */
    public static function getAll()
    {
        $result = collect();
        foreach (ListGenerator::orderBy('id')->get() as $ListGenerator) {
            $DtoListGenerator = new DtoListGenerator();
            $DtoListGenerator->id = $ListGenerator->id;

            $DtoListGenerator->description_save_rule = $ListGenerator->description_save_rule;
            $DtoListGenerator->description = $ListGenerator->description_save_rule;

            $DtoListGenerator->populate_only_if_empty = $ListGenerator->populate_only_if_empty;
            $DtoListGenerator->save_rule = $ListGenerator->save_rule;
            $DtoListGenerator->round_to_decimals = $ListGenerator->round_to_decimals;

            $DtoListGenerator->schedule = $ListGenerator->schedule;
            $DtoListGenerator->type_schedule = $ListGenerator->type_schedule;

            if ($ListGenerator->type_schedule == 'D'){
                $DtoListGenerator->daily = true;
                $DtoListGenerator->weekly = false;
                $DtoListGenerator->monthly = false;
            }
            if ($ListGenerator->type_schedule == 'W'){
                $DtoListGenerator->weekly = true;
                $DtoListGenerator->daily = false;
                $DtoListGenerator->monthly = false;
            }
            if ($ListGenerator->type_schedule == 'M'){
                $DtoListGenerator->monthly = true;
                $DtoListGenerator->weekly = false;
                $DtoListGenerator->daily = false;
            }
            if ($ListGenerator->type_schedule == 'O'){
                $DtoListGenerator->monthly = false;
                $DtoListGenerator->weekly = false;
                $DtoListGenerator->daily = false;  
            }

            $DtoListGenerator->days_of_the_week = $ListGenerator->days_of_the_week;
            $DtoListGenerator->days_of_the_month = $ListGenerator->days_of_the_month;
            $DtoListGenerator->month = $ListGenerator->month;
       
            $DtoListGenerator->repeat_after_hour = $ListGenerator->repeat_after_hour;
            $DtoListGenerator->repeat_after_min = $ListGenerator->repeat_after_min;
            $DtoListGenerator->repeat_times = $ListGenerator->repeat_times;
            $DtoListGenerator->execute = $ListGenerator->execute;
            $DtoListGenerator->description_execute_time = $ListGenerator->description_execute_time;
            $DtoListGenerator->starting_price_type = $ListGenerator->starting_price_type;

            if ($ListGenerator->starting_price_type == 0){
                $DtoListGenerator->check_starting_list = false;
                $DtoListGenerator->check_starting_purchase_cost = true;
                $DtoListGenerator->check_starting_base_price = false;
            }
            if ($ListGenerator->starting_price_type == 1){
                $DtoListGenerator->check_starting_list = false;
                $DtoListGenerator->check_starting_purchase_cost = false;
                $DtoListGenerator->check_starting_base_price = true;
            }

            $DtoListGenerator->id_starting_price_list = $ListGenerator->id_starting_price_list;
            if ($ListGenerator->starting_price_type == 2){ 
                $DtoListGenerator->description_starting_list = PriceList::where('id', $ListGenerator->id_starting_price_list)->get()->first()->description;
                $DtoListGenerator->check_starting_list = true;
                $DtoListGenerator->check_starting_purchase_cost = false;
                $DtoListGenerator->check_starting_base_price = false;
            }

            $DtoListGenerator->destination_type = $ListGenerator->destination_type;
            if ($ListGenerator->destination_type == 0){
                $DtoListGenerator->check_destination_baseprice = true;
                $DtoListGenerator->check_destination_list = false;
            }

            $DtoListGenerator->id_destination_price_list = $ListGenerator->id_destination_price_list;
            if ($ListGenerator->destination_type == 1){    
                if (isset($ListGenerator->id_destination_price_list)){
                    $DtoListGenerator->description_destination_list = PriceList::where('id', $ListGenerator->id_destination_price_list)->get()->first()->description;
                }else{
                    $DtoListGenerator->description_destination_list = '';
                }
              
                $DtoListGenerator->check_destination_list = true;
                $DtoListGenerator->check_destination_baseprice = false;
            }

            $DtoListGenerator->type_of_price_change = $ListGenerator->type_of_price_change;
            if ($ListGenerator->type_of_price_change == 1){
                $DtoListGenerator->percent = $ListGenerator->price_change;
                $DtoListGenerator->check_noprice_change_percentage = true;
                $DtoListGenerator->check_price_change_value = false;
                $DtoListGenerator->check_noprice_change = false;
            }

            if ($ListGenerator->type_of_price_change == 2){
                $DtoListGenerator->value = $ListGenerator->price_change;
                $DtoListGenerator->check_price_change_value = true;
                $DtoListGenerator->check_noprice_change = false;
                $DtoListGenerator->check_noprice_change_percentage = false;
            }

            if ($ListGenerator->type_of_price_change == 0){
                $DtoListGenerator->value = $ListGenerator->price_change;
                $DtoListGenerator->check_noprice_change = true;
                $DtoListGenerator->check_noprice_change_percentage = false;
                $DtoListGenerator->check_price_change_value = false;
            }

            $Scheduler = Scheduler::where('id_task_type', $ListGenerator->id)->where('type', 'List Generator')->get()->first();

            if (isset($Scheduler->last_run)){
                $DtoListGenerator->last_run = $Scheduler->last_run;
            }   
            if (isset($Scheduler->next_run)){
                $DtoListGenerator->next_run = $Scheduler->next_run;     
            }
            if (isset($Scheduler->status)){
                $DtoListGenerator->status = $Scheduler->status;     
            }
              if (isset($Scheduler->active)){
                $DtoListGenerator->active = $Scheduler->active;     
            }
       
            $ListGeneratorStartingCategories = ListGeneratorStartingCategories::where('id_list_generator', $ListGenerator->id)->get();
            if (isset($ListGeneratorStartingCategories)) {
                foreach ($ListGeneratorStartingCategories as $ListGeneratorStartingCategoriess) {
                    $DtoCategories= new DtoCategories();
                    $DtoCategories->id =$ListGeneratorStartingCategoriess->categories_id;
                    $DtoCategories->description = CategoryLanguage::where('category_id', $ListGeneratorStartingCategoriess->categories_id)->get()->first()->description;
                    $DtoCategories->checkCategoriesStarting = true;
                    $DtoListGenerator->check_starting_categories = true;
                    $DtoListGenerator->ListCategory->push($DtoCategories);  
                }
            }

            $ListGeneratorStartingGroup = ListGeneratorStartingGroup::where('id_list_generator', $ListGenerator->id)->get();
            if (isset($ListGeneratorStartingGroup)){ 
                foreach ($ListGeneratorStartingGroup as $ListGeneratorStartingGroups) {  
                    $DtoUsers = new DtoUser();  
                    $DtoUsers->checkGroupStarting = true;
                    $DtoUsers->id = $ListGeneratorStartingGroups->id;
                    $DtoUsers->description = $ListGeneratorStartingGroups->description;

                    $DtoListGenerator->check_starting_group = true;
                    $DtoListGenerator->ListGroup->push($DtoUsers);  
                }
            }

            $ListGeneratorStartingUsers = ListGeneratorStartingUsers::where('id_list_generator', $ListGenerator->id)->get();
            if (isset($ListGeneratorStartingUsers)){ 
                foreach ($ListGeneratorStartingUsers as $ListGeneratorStartingUserss) {  
                    $DtoUsers = new DtoUser();  
                    $DtoUsers->checkUser= true;
                    $DtoUsers->ListGeneratorStartingUserss = true;
                    $DtoUsers->id = $ListGeneratorStartingUserss->user_id;
                    $DtoUsers->name = $ListGeneratorStartingUserss->name;
                    $DtoUsers->surname = $ListGeneratorStartingUserss->surname;
                    $DtoUsers->business_name = $ListGeneratorStartingUserss->business_name;

                    $DtoListGenerator->check_starting_customer = true;
                    
                    $DtoListGenerator->ListUsers->push($DtoUsers);  
                }
            }

            $ListGeneratorDestinationUsers = ListGeneratorDestinationUsers::where('id_list_generator', $ListGenerator->id)->get();
            if (isset($ListGeneratorDestinationUsers)){ 
                foreach ($ListGeneratorDestinationUsers as $ListGeneratorDestinationUserss) {  
                    $DtoUsers = new DtoUser();  
                    $DtoUsers->checkDestinationUser = true;
                    $DtoUsers->ListGeneratorDestinationUserss = true;
                    $DtoUsers->id = $ListGeneratorDestinationUserss->user_id;
                    $DtoUsers->name = $ListGeneratorDestinationUserss->name;
                    $DtoUsers->surname = $ListGeneratorDestinationUserss->surname;
                    $DtoUsers->business_name = $ListGeneratorDestinationUserss->business_name;

                    $DtoListGenerator->check_destination_users = true;
                    $DtoListGenerator->ListDestinationUsers->push($DtoUsers);  
                }
            }

            $ListGeneratorDestinationGroup = ListGeneratorDestinationGroup::where('id_list_generator', $ListGenerator->id)->get();
            if (isset($ListGeneratorDestinationGroup)){ 
                foreach ($ListGeneratorDestinationGroup as $ListGeneratorDestinationGroups) {  
                    $DtoUsers = new DtoUser();  
                    $DtoUsers->checkGroupDestination = true;
                    $DtoUsers->id = $ListGeneratorDestinationGroups->id;
                    $DtoUsers->description = $ListGeneratorDestinationGroups->description;
                    
                    $DtoListGenerator->check_destination_groups = true;
                    $DtoListGenerator->ListDestinationGroup->push($DtoUsers);  
                }
            }

            $ListGeneratorDestinationCategories = ListGeneratorDestinationCategories::where('id_list_generator', $ListGenerator->id)->get();
            if (isset($ListGeneratorDestinationCategories)) {
                foreach ($ListGeneratorDestinationCategories as $ListGeneratorDestinationCategoriess) {
                    $DtoCategories= new DtoCategories();
                    $DtoCategories->id =$ListGeneratorDestinationCategoriess->categories_id;
                    $DtoCategories->description = CategoryLanguage::where('category_id', $ListGeneratorDestinationCategoriess->categories_id)->get()->first()->description;
                    $DtoCategories->checkCategoriesDestination = true;
                    
                    $DtoListGenerator->check_destination_categories = true;
                    $DtoListGenerator->ListDestinationCategory->push($DtoCategories);  
                }
            }
            $result->push($DtoListGenerator);
    }
         
        return $result;

    } 

    /**
     * Get by id 
     * 
     * @param int id
     * 
     * @return DtoCities
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Cities::find($id));
    }

    #endregion GET

    #region INSERT
    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $dtoListGenerator, int $idUser, int $idLanguage)
    {

       //static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);
        DB::beginTransaction();
        try {
            $ListGenerator = new ListGenerator();

            $ListGenerator->description_save_rule = $dtoListGenerator->description_save_rule;
            $ListGenerator->populate_only_if_empty = $dtoListGenerator->populate_only_if_empty;
            $ListGenerator->save_rule = $dtoListGenerator->save_rule;
            $ListGenerator->round_to_decimals = $dtoListGenerator->round_to_decimals;

            $ListGenerator->schedule = $dtoListGenerator->schedule;
           // $ListGenerator->type_schedule = $dtoListGenerator->type_schedule;

            if ($dtoListGenerator->daily = true){
                $ListGenerator->type_schedule = 'D';
            }
            if ($dtoListGenerator->weekly = true){
                $ListGenerator->type_schedule = 'W';
            }
            if ($dtoListGenerator->monthly = true){
                $ListGenerator->type_schedule = 'M';
            }
            if ($dtoListGenerator->daily = false && $dtoListGenerator->weekly = false  && $dtoListGenerator->monthly = false && $dtoListGenerator->schedule == true){
                $ListGenerator->type_schedule = 'O';
            }

            $ListGenerator->days_of_the_week = $dtoListGenerator->days_of_the_week;
            $ListGenerator->days_of_the_month = $dtoListGenerator->days_of_the_month;
            $ListGenerator->month = $dtoListGenerator->month;
            $ListGenerator->repeat_after_hour = $dtoListGenerator->repeat_after_hour;
            $ListGenerator->repeat_after_min = $dtoListGenerator->repeat_after_min;
            $ListGenerator->repeat_times = $dtoListGenerator->repeat_times;
            $ListGenerator->execute = $dtoListGenerator->execute;
            $ListGenerator->description_execute_time = $dtoListGenerator->description_execute_time;

           //$ListGenerator->starting_price_type = $dtoListGenerator->starting_price_type;
            if ($dtoListGenerator->check_starting_purchase_cost == 0){
                $ListGenerator->starting_price_type == 0;
            }
            if ($dtoListGenerator->check_starting_base_price == 1){
                $ListGenerator->starting_price_type == 1;
            }
            $ListGenerator->id_starting_price_list = $dtoListGenerator->id_starting_price_list;

            if ($dtoListGenerator->check_starting_list == 2){
                $ListGenerator->starting_price_type == 2;
            }
            //$ListGenerator->destination_type = $dtoListGenerator->destination_type;

            if ($dtoListGenerator->check_destination_baseprice == 0){
                $ListGenerator->destination_type = 0;
            }
            $ListGenerator->id_destination_price_list = $dtoListGenerator->id_destination_price_list;

            if ($dtoListGenerator->check_destination_list == 1){
                $ListGenerator->destination_type = 1;
            }
             //$ListGenerator->type_of_price_change = $dtoListGenerator->type_of_price_change;
            if ($dtoListGenerator->check_noprice_change == 0){
                $ListGenerator->type_of_price_change = 0;
            }
            if ($dtoListGenerator->check_noprice_change_percentage == 1){
                $ListGenerator->type_of_price_change = 1;
                $ListGenerator->price_change = $dtoListGenerator->percent;
            }
            if ($dtoListGenerator->check_price_change_value == 2){
                $ListGenerator->type_of_price_change = 2;
                $ListGenerator->price_change = $dtoListGenerator->value;
            }
            $ListGenerator->created_id = $dtoListGenerator->$idUser;
            $ListGenerator->save();

                    $user_id = Auth::user()->id; 
                    $RowListScheduler= new Scheduler();
                        $RowListScheduler->id_task_type = $ListGenerator->id;
                        $RowListScheduler->user_id = $user_id;

                    if (isset($dtoListGenerator->description_save_rule)) {
                        $RowListScheduler->name = $dtoListGenerator->description_save_rule;
                    }
                    if (isset($dtoListGenerator->description_save_rule)) {
                        $RowListScheduler->description = $dtoListGenerator->description_save_rule;
                    }
                        $action = static::_createStringAction($dtoListGenerator);
                        $RowListScheduler->action = $action;
                        $RowListScheduler->arguments = '';
                    
                    if (isset($dtoListGenerator->description_execute_time)) {
                        $dateStart = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $dtoListGenerator->description_execute_time)->format('Y-m-d H:i:s');
                        $RowListScheduler->start_time_action = $dateStart;
                        $RowListScheduler->next_run = $dateStart;
                    }
                    
                    $RowListScheduler->schedule = $ListGenerator->type_schedule;
                    if (isset($dtoListGenerator->frequency)) {
                        $RowListScheduler->frequency = $dtoListGenerator->frequency;
                    }
                    if (isset($dtoListGenerator->repeat_after_hour)) {
                        $RowListScheduler->repeat_after_hour = $dtoListGenerator->repeat_after_hour;
                    }
                    if (isset($dtoListGenerator->repeat_after_min)) {
                        $RowListScheduler->repeat_after_min = $dtoListGenerator->repeat_after_min;
                    }
                    if (isset($dtoListGenerator->repeat_times)) {
                        $RowListScheduler->repeat_times = $dtoListGenerator->repeat_times;
                    }
                        $RowListScheduler->status = 'New';
                        $RowListScheduler->notification = true;
                        $RowListScheduler->active = true;
                        $RowListScheduler->type = 'List Generator';

                    if (isset($dtoListGenerator->repeat_left)) {
                        $RowListScheduler->repeat_left = $dtoListGenerator->repeat_left;
                    }
                    if (isset($dtoListGenerator->days_of_the_month)) {
                        $RowListScheduler->days_of_the_month = $dtoListGenerator->days_of_the_month;
                    }
                    if (isset($dtoListGenerator->month)) {
                        $RowListScheduler->month = $dtoListGenerator->month;
                    }
                        $RowListScheduler->save(); 
            
            $ListGeneratorStartingUsers = ListGeneratorStartingUsers::where('id_list_generator', $ListGenerator->id)->get();
            if (isset($ListGeneratorStartingUsers)) {
                ListGeneratorStartingUsers::where('id_list_generator', $ListGenerator->id)->delete();
            }
                foreach ($dtoListGenerator->tableDataUserStarting as $ListGeneratorStartingUsers) {
                if ($ListGeneratorStartingUsers['checkUser'] == true){
                        $RowListGeneratorStartingUsers= new ListGeneratorStartingUsers();
                    if (isset($ListGenerator->id)) {
                        $RowListGeneratorStartingUsers->id_list_generator = $ListGenerator->id;
                    }
                        $RowListGeneratorStartingUsers->user_id = $ListGeneratorStartingUsers['id'];
                        $RowListGeneratorStartingUsers->save();  
                }            
            }
            
            $ListGeneratorStartingGroup = ListGeneratorStartingGroup::where('id_list_generator', $ListGenerator->id)->get();
            if (isset($ListGeneratorStartingGroup)) {
                ListGeneratorStartingGroup::where('id_list_generator', $ListGenerator->id)->delete();
            }
            foreach ($dtoListGenerator->tableDataUserGroupStarting as $ListGeneratorStartingGroupResult) {
                if ($ListGeneratorStartingGroupResult['checkGroupStarting']== true){
                        $RowListGeneratorStartingGroupRs = new ListGeneratorStartingGroup();
                    if (isset($ListGenerator->id)) {
                        $RowListGeneratorStartingGroupRs->id_list_generator = $ListGenerator->id;
                    }
                        $RowListGeneratorStartingGroupRs->group_id = $ListGeneratorStartingGroupResult['id'];
                        $RowListGeneratorStartingGroupRs->save(); 
                }                         
            }

            $ListGeneratorStartingCategories = ListGeneratorStartingCategories::where('id_list_generator', $ListGenerator->id)->get();
            if (isset($ListGeneratorStartingCategories)) {
                ListGeneratorStartingCategories::where('id_list_generator', $ListGenerator->id)->delete();
            }
            foreach ($dtoListGenerator->tableDataUserCategoriesStarting as $ListGeneratorStartingCategoriesResult) {
                if ($ListGeneratorStartingCategoriesResult['checkCategoriesStarting']== true){
                        $RowListGeneratorStartingCategories= new ListGeneratorStartingCategories();
                    if (isset($ListGenerator->id)) {
                        $RowListGeneratorStartingCategories->id_list_generator = $ListGenerator->id;
                    }
                        $RowListGeneratorStartingCategories->categories_id = $ListGeneratorStartingCategoriesResult['id'];
                        $RowListGeneratorStartingCategories->save();  
                }          
            }
            $ListGeneratorDestinationUsers = ListGeneratorDestinationUsers::where('id_list_generator', $ListGenerator->id)->get();
            if (isset($ListGeneratorDestinationUsers)) {
                ListGeneratorDestinationUsers::where('id_list_generator', $ListGenerator->id)->delete();
            }
                foreach ($dtoListGenerator->tableDataDestinationUsers as $ListGeneratorDestinationUsersResult) {
                if ($ListGeneratorDestinationUsersResult['checkDestinationUser'] == true){
                        $RowListGeneratorDestinationUsers= new ListGeneratorDestinationUsers();
                    if (isset($ListGenerator->id)) {
                        $RowListGeneratorDestinationUsers->id_list_generator = $ListGenerator->id;
                    }
                        $RowListGeneratorDestinationUsers->user_id = $ListGeneratorDestinationUsersResult['id'];
                        $RowListGeneratorDestinationUsers->save();   
                }           
            }
            $ListGeneratorDestinationGroup = ListGeneratorDestinationGroup::where('id_list_generator', $ListGenerator->id)->get();
            if (isset($ListGeneratorDestinationGroup)) {
                ListGeneratorDestinationGroup::where('id_list_generator', $ListGenerator->id)->delete();
            }
            foreach ($dtoListGenerator->tableDataDestinationGroup as $ListGeneratorDestinationGroupResult) {
                if ($ListGeneratorDestinationGroupResult['checkGroupDestination']== true){
                        $ListGeneratorDestinationGroupR = new ListGeneratorDestinationGroup();
                    if (isset($ListGenerator->id)) {
                        $ListGeneratorDestinationGroupR->id_list_generator = $ListGenerator->id;
                    }
                        $ListGeneratorDestinationGroupR->group_id = $ListGeneratorDestinationGroupResult['id'];
                        $ListGeneratorDestinationGroupR->save();  
                }            
            }

            $ListGeneratorDestinationCategories = ListGeneratorDestinationCategories::where('id_list_generator', $ListGenerator->id)->get();
            if (isset($ListGeneratorDestinationCategories)) {
                ListGeneratorDestinationCategories::where('id_list_generator', $ListGenerator->id)->delete();
            }
            foreach ($dtoListGenerator->tableDataDestinationCategories as $ListGeneratorDestinationCategoriesResult) {
                if ($ListGeneratorDestinationCategoriesResult['checkCategoriesDestination']== true){
                        $RowListGeneratorDestinationCategories= new ListGeneratorDestinationCategories();
                    if (isset($ListGenerator->id)) {
                        $RowListGeneratorDestinationCategories->id_list_generator = $ListGenerator->id;
                    }
                        $RowListGeneratorDestinationCategories->categories_id = $ListGeneratorDestinationCategoriesResult['id'];
                        $RowListGeneratorDestinationCategories->save(); 
                }       
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $ListGenerator->id;
    }
    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoCities
     * @param int idLanguage
     */
    public static function update(Request $dtoListGenerator, int $idLanguage)
    {
        $ListGenerator = ListGenerator::find($dtoListGenerator->id);
        DB::beginTransaction();

        try {

            $strSelect = "";
            $strWhere = "";
            $strUpdate = "";
            $strInsert = "";

            $ListGenerator->description_save_rule = $dtoListGenerator->description_save_rule;
            $ListGenerator->populate_only_if_empty = $dtoListGenerator->populate_only_if_empty;
            $ListGenerator->save_rule = $dtoListGenerator->save_rule;
            $ListGenerator->round_to_decimals = $dtoListGenerator->round_to_decimals;
            $ListGenerator->schedule = $dtoListGenerator->schedule;
            $ListGenerator->days_of_the_week = $dtoListGenerator->count_day_of_week;
            $ListGenerator->days_of_the_month = $dtoListGenerator->days_of_the_month;
            $ListGenerator->month = $dtoListGenerator->countmonth;
            $ListGenerator->repeat_after_hour = $dtoListGenerator->repeat_after_hour;
            $ListGenerator->repeat_after_min = $dtoListGenerator->repeat_after_min;
            $ListGenerator->repeat_times = $dtoListGenerator->repeat_times;

            if ($dtoListGenerator->check_starting_purchase_cost == '0'){
                $ListGenerator->starting_price_type = 0;
            }

            if ($dtoListGenerator->check_starting_base_price == '1'){
                $ListGenerator->starting_price_type = 1;
            }

            if ($dtoListGenerator->check_starting_list == '2'){
                $ListGenerator->starting_price_type = 2;
                if (isset($dtoListGenerator->id_starting_price_list) || ($dtoListGenerator->id_starting_price_list != 'null')){
                    $ListGenerator->id_starting_price_list = $dtoListGenerator->id_starting_price_list;
                }else{
                     $ListGenerator->id_starting_price_list = null; 
                }               
            }
            
            if ($dtoListGenerator->check_noprice_change == 0){
                $ListGenerator->type_of_price_change = 0;
            }
            if ($dtoListGenerator->check_noprice_change_percentage == 1){
                $ListGenerator->type_of_price_change = 1;
                $ListGenerator->price_change = $dtoListGenerator->percent;
            }
            if ($dtoListGenerator->check_price_change_value == 2){
                $ListGenerator->type_of_price_change = 2;
                $ListGenerator->price_change = $dtoListGenerator->value;
            }
            if ($dtoListGenerator->check_destination_baseprice == 0){
                $ListGenerator->destination_type = 0;
            }
            if ($dtoListGenerator->check_destination_list == 1){
                $ListGenerator->destination_type = 1;                
                if ($dtoListGenerator->id_destination_price_list != 'null') {
                    $ListGenerator->id_destination_price_list = $dtoListGenerator->id_destination_price_list;
                }else{
                    $ListGenerator->id_destination_price_list = null;
                }
            }

            if ($dtoListGenerator->populate_only_if_empty == true){
                $ListGenerator->populate_only_if_empty = true;
            }else{
                $ListGenerator->populate_only_if_empty = false;
            }

            if ($dtoListGenerator->round_to_decimals == true){
                $ListGenerator->round_to_decimals = true;
            }else{
                $ListGenerator->round_to_decimals = false;
            }

            if ($dtoListGenerator->save_rule == true){
                $ListGenerator->save_rule = true;
            }else{
                $ListGenerator->save_rule = false;
            }
            if ($dtoListGenerator->execute == true){
                $ListGenerator->execute = true;
            }else{
                $ListGenerator->execute = false;
            }

            if (isset($dtoListGenerator->description_save_rule)){
                $ListGenerator->description_save_rule = $dtoListGenerator->description_save_rule;
            }else{
                $ListGenerator->description_save_rule = null;
            }
            if (isset($dtoListGenerator->description_execute_time)){
                $ListGenerator->description_execute_time = $dtoListGenerator->description_execute_time;
            }else{
                $ListGenerator->description_execute_time = null;
            }
            if (isset($dtoListGenerator->repeat_after_hour)){
                $ListGenerator->repeat_after_hour = $dtoListGenerator->repeat_after_hour;
            }else{
                $ListGenerator->repeat_after_hour = null;
            }
            if (isset($dtoListGenerator->repeat_after_min)){
                $ListGenerator->repeat_after_min = $dtoListGenerator->repeat_after_min;
            }else{
                $ListGenerator->repeat_after_min = null;
            }
            if (isset($dtoListGenerator->repeat_times)){
                $ListGenerator->repeat_times = $dtoListGenerator->repeat_times;
            }else{
                $ListGenerator->repeat_times = null;
            }
            if ($dtoListGenerator->schedule == true && $dtoListGenerator->daily == true){
                $ListGenerator->type_schedule = 'D';
            }
            if ($dtoListGenerator->schedule == true && $dtoListGenerator->weekly == true){
                $ListGenerator->type_schedule = 'W';
            }
            if ($dtoListGenerator->schedule == true && $dtoListGenerator->monthly == true){
                $ListGenerator->type_schedule = 'M';
            }
            if ($dtoListGenerator->schedule == true && $dtoListGenerator->daily == false && $dtoListGenerator->weekly == false && $dtoListGenerator->monthly == false){
                $ListGenerator->type_schedule = 'O';
            }
            $ListGenerator->save();


            
            $ListGeneratorStartingUsers = ListGeneratorStartingUsers::where('id_list_generator', $dtoListGenerator->id)->get();
            if (isset($ListGeneratorStartingUsers)) {
                ListGeneratorStartingUsers::where('id_list_generator', $dtoListGenerator->id)->delete();
            }
            foreach ($dtoListGenerator->tableDataUserStarting as $ListGeneratorStartingUsers) {
                if ($ListGeneratorStartingUsers['checkUser'] == true){
                    $RowListGeneratorStartingUsers= new ListGeneratorStartingUsers();

                    if (isset($dtoListGenerator->id)) {
                        $RowListGeneratorStartingUsers->id_list_generator = $dtoListGenerator->id;
                    }
                    $RowListGeneratorStartingUsers->user_id = $ListGeneratorStartingUsers['id'];
                    $RowListGeneratorStartingUsers->save();  
                }            
            }

            $ListGeneratorStartingGroup = ListGeneratorStartingGroup::where('id_list_generator', $dtoListGenerator->id)->get();
            if (isset($ListGeneratorStartingGroup)) {
                ListGeneratorStartingGroup::where('id_list_generator', $dtoListGenerator->id)->delete();
            }
            foreach ($dtoListGenerator->tableDataUserGroupStarting as $ListGeneratorStartingGroupResult) {
                if ($ListGeneratorStartingGroupResult['checkGroupStarting']== true){
                    $RowListGeneratorStartingGroupRs = new ListGeneratorStartingGroup();
                    if (isset($dtoListGenerator->id)) {
                        $RowListGeneratorStartingGroupRs->id_list_generator = $dtoListGenerator->id;
                    }
                    $RowListGeneratorStartingGroupRs->group_id = $ListGeneratorStartingGroupResult['id'];
                    $RowListGeneratorStartingGroupRs->save(); 
                }     
            }

            $ListGeneratorStartingCategories = ListGeneratorStartingCategories::where('id_list_generator', $dtoListGenerator->id)->get();
            if (isset($ListGeneratorStartingCategories)) {
                ListGeneratorStartingCategories::where('id_list_generator', $dtoListGenerator->id)->delete();
            }
            foreach ($dtoListGenerator->tableDataUserCategoriesStarting as $ListGeneratorStartingCategoriesResult) {
                if ($ListGeneratorStartingCategoriesResult['checkCategoriesStarting']== true){
                    $RowListGeneratorStartingCategories= new ListGeneratorStartingCategories();
                    if (isset($dtoListGenerator->id)) {
                        $RowListGeneratorStartingCategories->id_list_generator = $dtoListGenerator->id;
                    }
                    $RowListGeneratorStartingCategories->categories_id = $ListGeneratorStartingCategoriesResult['id'];
                    $RowListGeneratorStartingCategories->save();  
                }          
            }

            $ListGeneratorDestinationUsers = ListGeneratorDestinationUsers::where('id_list_generator', $dtoListGenerator->id)->get();
            if (isset($ListGeneratorDestinationUsers)) {
                ListGeneratorDestinationUsers::where('id_list_generator', $dtoListGenerator->id)->delete();
            }
            foreach ($dtoListGenerator->tableDataDestinationUsers as $ListGeneratorDestinationUsersResult) {
                if (isset($ListGeneratorDestinationUsersResult['checkDestinationUser'])) {
                    if ($ListGeneratorDestinationUsersResult['checkDestinationUser'] == true){
                        $RowListGeneratorDestinationUsers= new ListGeneratorDestinationUsers();
                        if (isset($dtoListGenerator->id)) {
                            $RowListGeneratorDestinationUsers->id_list_generator = $dtoListGenerator->id;
                        }
                        $RowListGeneratorDestinationUsers->user_id = $ListGeneratorDestinationUsersResult['id'];
                        $RowListGeneratorDestinationUsers->save();   
                    }
                }   
            }

            $ListGeneratorDestinationGroup = ListGeneratorDestinationGroup::where('id_list_generator', $dtoListGenerator->id)->get();
            if (isset($ListGeneratorDestinationGroup)) {
                ListGeneratorDestinationGroup::where('id_list_generator', $dtoListGenerator->id)->delete();
            }
            foreach ($dtoListGenerator->tableDataDestinationGroup as $ListGeneratorDestinationGroupResult) {
                if (isset($ListGeneratorDestinationGroupResult['checkGroupDestination'])){
                    if ($ListGeneratorDestinationGroupResult['checkGroupDestination']== true){
                        $ListGeneratorDestinationGroupR = new ListGeneratorDestinationGroup();
                        if (isset($dtoListGenerator->id)) {
                            $ListGeneratorDestinationGroupR->id_list_generator = $dtoListGenerator->id;
                        }
                        $ListGeneratorDestinationGroupR->group_id = $ListGeneratorDestinationGroupResult['id'];
                        $ListGeneratorDestinationGroupR->save();  
                    }  
                }           
            }

            $ListGeneratorDestinationCategories = ListGeneratorDestinationCategories::where('id_list_generator', $dtoListGenerator->id)->get();
            if (isset($ListGeneratorDestinationCategories)) {
                ListGeneratorDestinationCategories::where('id_list_generator', $dtoListGenerator->id)->delete();
            }
            foreach ($dtoListGenerator->tableDataDestinationCategories as $ListGeneratorDestinationCategoriesResult) {
                if (isset($ListGeneratorDestinationCategoriesResult['checkCategoriesDestination'])){ 
                    if ($ListGeneratorDestinationCategoriesResult['checkCategoriesDestination']== true){
                        $RowListGeneratorDestinationCategories= new ListGeneratorDestinationCategories();
                        if (isset($dtoListGenerator->id)) {
                            $RowListGeneratorDestinationCategories->id_list_generator = $dtoListGenerator->id;
                        }
                        $RowListGeneratorDestinationCategories->categories_id = $ListGeneratorDestinationCategoriesResult['id'];
                        $RowListGeneratorDestinationCategories->save(); 
                    }       
                }
            }

            $ListPriceGeneratorSchedule = Scheduler::where('id_task_type', $dtoListGenerator->id)->get();
            if (isset($ListPriceGeneratorSchedule)) {
                Scheduler::where('id_task_type', $dtoListGenerator->id)->delete();
                $user_id = Auth::user()->id;

                $RowListScheduler= new Scheduler();
                $RowListScheduler->id_task_type = $dtoListGenerator->id;
                $RowListScheduler->user_id = $user_id;
                $RowListScheduler->name = $dtoListGenerator->description_save_rule;
                $RowListScheduler->description = $dtoListGenerator->description_save_rule;

                $action = static::_createStringAction($dtoListGenerator);
                $RowListScheduler->action = $action;
                $RowListScheduler->arguments = '';

                if (isset($dtoListGenerator->description_execute_time)) {
                    $dateStart = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $dtoListGenerator->description_execute_time)->format('Y-m-d H:i:s');
                    $RowListScheduler->start_time_action = $dateStart;
                    $RowListScheduler->next_run = $dateStart;
                }

                $RowListScheduler->schedule = $ListGenerator->type_schedule;
                $RowListScheduler->frequency = $dtoListGenerator->frequency;
                $RowListScheduler->repeat_after_hour = $dtoListGenerator->repeat_after_hour;
                $RowListScheduler->repeat_after_min = $dtoListGenerator->repeat_after_min;
                $RowListScheduler->repeat_times = $dtoListGenerator->repeat_times;
                $RowListScheduler->status = 'Updated';
                $RowListScheduler->notification = true;
                $RowListScheduler->active = true;
                $RowListScheduler->type = 'List Generator';
                $RowListScheduler->repeat_left = $dtoListGenerator->repeat_left;
                $RowListScheduler->days_of_the_month = $dtoListGenerator->days_of_the_month;
                $RowListScheduler->month = $dtoListGenerator->month;
                $RowListScheduler->save(); 
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
    #endregion UPDATE


     public static function updateOnlineCheck(Request $request)
    {
       // $itemsOnDb = Scheduler::find($request->id);
        $Scheduler = Scheduler::where('id_task_type', $request->id)->get()->first();
        if (isset($Scheduler)) {
            $Scheduler->active = !$Scheduler->active;
            $Scheduler->save();
        } else {
             $Scheduler->online = true;
             $Scheduler->save();
            }
    }


    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {

        $ListGeneratorStartingUsers = ListGeneratorStartingUsers::where('id_list_generator', $id)->get()->first();
        $ListGeneratorStartingGroup = ListGeneratorStartingGroup::where('id_list_generator', $id)->get()->first();
        $ListGeneratorStartingCategories = ListGeneratorStartingCategories::where('id_list_generator', $id)->get()->first();
        $ListGeneratorDestinationUsers =  ListGeneratorDestinationUsers::where('id_list_generator', $id)->get()->first();
        $ListGeneratorDestinationGroup =  ListGeneratorDestinationGroup::where('id_list_generator', $id)->get()->first();
        $ListGeneratorDestinationCategories = ListGeneratorDestinationCategories::where('id_list_generator', $id)->get()->first();
        $Scheduler = Scheduler::where('id_task_type', $id)->get()->first();
        $ListGenerator = ListGenerator::find($id);

        if (is_null($ListGenerator)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
          
         
            if (isset($ListGeneratorStartingUsers)){ 
                $ListGeneratorStartingUsers->delete();
            }
            if (isset($ListGeneratorStartingGroup)){ 
                $ListGeneratorStartingGroup->delete();  
            }
            if (isset($ListGeneratorStartingCategories)){ 
                $ListGeneratorStartingCategories->delete();  
            }
            if (isset($ListGeneratorDestinationUsers)){ 
                $ListGeneratorDestinationUsers->delete();
            }
            if (isset($ListGeneratorDestinationGroup)){ 
                $ListGeneratorDestinationGroup->delete();
            }
            if (isset($ListGeneratorDestinationCategories)){ 
                $ListGeneratorDestinationCategories->delete();
            }
            if (isset($Scheduler)){ 
                $Scheduler->delete();
            }
            if (isset($ListGenerator)){ 
                $ListGenerator->delete();
            }

    }

    #endregion DELETE
    
}

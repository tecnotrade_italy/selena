<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoCategories;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\LogHelper;
use App\CategoryLanguage;
use App\Categories;
use App\User;
use Illuminate\Support\Facades\DB;



use Exception;
use Illuminate\Http\Request;

class CategoriesLanguagesBL
{

   #region DELETE

 /**
 * Delete by idPage
 *
 * @param int $idPage
 */

  public static function deleteByIdCategories(int $idCategories){
    CategoryLanguage::where('category_id', $idCategories)->delete();
  }

  #endregion DELETE
}

<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoNewsletter;
use App\DtoModel\DtoRecipientNewsletter;
use App\DtoModel\DtoNewsletterGroupQueryEmail;
use App\Newsletter;
use App\Contact;
use App\DtoModel\DtoFeedbackNewsletter;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\FeedbackNewsletterEnum;
use App\Enums\HttpResultsCodesEnum;
use App\FeedbackNewsletter;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\NewsletterRecipient;
use App\SelenaSqlViews;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mail\ConfirmTestMailSender;
use Illuminate\Support\Facades\Mail;
use App\Mail\DynamicMail;
use App\Setting;
use Carbon\Carbon;
use DateInterval;
use DateTime;
use Illuminate\Support\Facades\Storage;

class NewsletterBL
{
    #region PRIVATE

    /**
     * Check validation
     *
     * @param Request dtoNewsletter
     * @param Int idLanguage
     * @param DictionariesCodesEnum dbOperationsTypesEnum
     */
    private static function _validateData(Request $dtoNewsletter, int $idLanguage, $dbOperationsTypesEnum)
    {
        if ($dbOperationsTypesEnum == DbOperationsTypesEnum::UPDATE) {
            if (is_null($dtoNewsletter->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued, HttpResultsCodesEnum::InvalidPayload));
            }

            if (!is_null(Newsletter::find($dtoNewsletter->id))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NewsletterNotFound, HttpResultsCodesEnum::InvalidPayload));
            }
        }

        if (is_null($dtoNewsletter->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound, HttpResultsCodesEnum::InvalidPayload));
        }

        if (is_null($dtoNewsletter->html)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::HtmlNotFound, HttpResultsCodesEnum::InvalidPayload));
        }

        if (is_null($dtoNewsletter->title)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TitleNotFound, HttpResultsCodesEnum::InvalidPayload));
        }
    }

    /**
     * Convert to Dto
     * 
     * @param Newsletter newsletter
     * 
     * @return DtoNewsletter
     */
    private static function _convertToDto(Newsletter $newsletter)
    {
        $dtoNewsletter = new DtoNewsletter();

        if (isset($newsletter->id)) {
            $dtoNewsletter->id = $newsletter->id;
        }

        if (isset($newsletter->description)) {
            $dtoNewsletter->description = $newsletter->description;
        }

        if (isset($newsletter->html)) {
            $dtoNewsletter->html = $newsletter->html;
        }

        if (isset($newsletter->draft)) {
            $dtoNewsletter->draft = $newsletter->draft;
        }

        if (isset($newsletter->url)) {
            $dtoNewsletter->url = $newsletter->url;
        }

        if (isset($newsletter->schedule_date_time)) {
            $dtoNewsletter->scheduleDateTime = $newsletter->schedule_date_time;
        }

        if (isset($newsletter->start_send_date_time)) {
            $dtoNewsletter->startSendDateTime = $newsletter->start_send_date_time;
        }

        if (isset($newsletter->end_send_date_time)) {
            $dtoNewsletter->endSendDateTime = $newsletter->end_send_date_time;
        }
    
        if (isset($newsletter->title)) {
            $dtoNewsletter->title = $newsletter->title;
        }

        return $dtoNewsletter;
    }

    /**
     * Converto to model
     * 
     * @param Request dtoNewsletter
     * 
     * @return Newsletter
     */
    private static function _convertToModel(Request $dtoNewsletter)
    {
        $newsletter = new Newsletter();

        if (isset($dtoNewsletter->id)) {
            $newsletter->id = $dtoNewsletter->id;
        }

        if (isset($dtoNewsletter->description)) {
            $newsletter->description = $dtoNewsletter->description;
        }

        if (isset($dtoNewsletter->html)) {
            $newsletter->html = $dtoNewsletter->html;
        }

        if (isset($dtoNewsletter->draft)) {
            $newsletter->draft = $dtoNewsletter->draft;
        }

        if (isset($dtoNewsletter->url)) {
            $newsletter->url = $dtoNewsletter->url;
        }

        if (isset($dtoNewsletter->scheduleDateTime)) {
            $newsletter->schedule_date_time = $dtoNewsletter->scheduleDateTime;
        }

        if (isset($dtoNewsletter->startSendDateTime)) {
            $newsletter->start_send_date_time = $dtoNewsletter->startSendDateTime;
        }

        if (isset($dtoNewsletter->endSendDateTime)) {
            $newsletter->end_send_date_time = $dtoNewsletter->endSendDateTime;
        }

        if (isset($dtoNewsletter->title)) {
            $newsletter->title = $dtoNewsletter->title;
        }

        return $newsletter;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get all
     *
     * @return Newsletter
     */
    public static function getAll()
    {
        return Newsletter::all()->map(function ($newsletter) {
            return static::_convertToDto($newsletter);
        });
    }

     /**
     * getAllCampaign
     *
     * @return Newsletter
     */
    public static function getAllCampaign()
    {
       $result = collect();
        foreach (Newsletter::orderBy('id','DESC')->get() as $newsletter) {

           $dtoNewsletter = new DtoNewsletter();

        if (isset($newsletter->id)) {
            $dtoNewsletter->id = $newsletter->id;
        }

        if (isset($newsletter->title)) {
            $dtoNewsletter->title = $newsletter->title;
        }
         if (isset($newsletter->schedule_date_time)) {
            $dtoNewsletter->schedule_date_time = Carbon::parse($newsletter->schedule_date_time)->format(HttpHelper::getLanguageDateTimeFormat()); 
        }     

          if (isset($newsletter->draft)) {
               $dtoNewsletter->draft = $newsletter->draft;
            }else {
                $dtoNewsletter->draft=false;
            }

          if (isset($newsletter->sender)) {
            $dtoNewsletter->sender = $newsletter->sender;
        }
             if (isset($newsletter->sender_email)) {
            $dtoNewsletter->sender_email = $newsletter->sender_email;
        }
         if (isset($newsletter->object)) {
            $dtoNewsletter->object = $newsletter->object;
        }
         if (isset($newsletter->preview_object)) {
            $dtoNewsletter->preview_object = $newsletter->preview_object;
        }
        if (isset($newsletter->start_send_date_time)) {
            $dtoNewsletter->start_send_date_time = $newsletter->start_send_date_time;
        }
        if (isset($newsletter->end_send_date_time)) {
            $dtoNewsletter->end_send_date_time = $newsletter->end_send_date_time;
        }else{
           $dtoNewsletter->end_send_date_time = ''; 
        }
        if (isset($newsletter->url)) {
            $dtoNewsletter->url = $newsletter->url;
        }else{
           $dtoNewsletter->url = ''; 
        }

        $nrsender = DB::select(
            'SELECT COUNT(*) as countToSend
            FROM newsletters_recipients 
            inner join feedback_newsletters on feedback_newsletters.newsletter_recipient_id = newsletters_recipients.id
            WHERE feedback_newsletters.code = \'SENT\' AND newsletters_recipients.newsletter_id = :idNewsletter',
            ['idNewsletter' => $newsletter->id]
        )[0]->counttosend;

        if (isset($nrsender)) {
             $dtoNewsletter->nr_sender = $nrsender;   
        }else{
           $dtoNewsletter->nr_sender = ''; 
        }

        $countemailsend = DB::table('newsletters_recipients')
             ->select(DB::raw('count(id) as num_id'))
             ->where('newsletter_id', '=', $newsletter->id)
             ->groupBy('newsletter_id')
             ->get();

        if (isset($countemailsend) && count($countemailsend) > 0) {
             $dtoNewsletter->nr_recipients = $countemailsend[0]->num_id;   
        }else{
           $dtoNewsletter->nr_recipients = ''; 
        }

       /* $countOpened = DB::select( 
        'SELECT count(*) as countopened
        FROM newsletters_recipients
        INNER JOIN feedback_newsletters ON newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
        WHERE newsletters_recipients.newsletter_id = :idNewsletter
        AND feedback_newsletters.code = :open
        group by feedback_newsletters.code',
        ['idNewsletter' => $newsletter->id, 'open' => FeedbackNewsletterEnum::Open]);
*/

       /*$countOpened = DB::select(
            'SELECT COUNT(*) AS countopened
            FROM (
                SELECT feedback_newsletters.code, feedback_newsletters.newsletter_recipient_id,feedback_newsletters.created_at
                FROM newsletters_recipients
                INNER JOIN feedback_newsletters ON newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
                WHERE newsletters_recipients.newsletter_id = :idNewsletter
                AND feedback_newsletters.code = :open
                GROUP BY feedback_newsletters.code, feedback_newsletters.newsletter_recipient_id, feedback_newsletters.created_at
                ) AS c',
            ['idNewsletter' => $newsletter->id, 'open' => FeedbackNewsletterEnum::Open]
        )[0]->countopened;*/

        $countOpened = DB::select(
        'SELECT count(*) as count
        FROM newsletters_recipients
        INNER JOIN feedback_newsletters ON newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
        WHERE newsletters_recipients.newsletter_id = :idNewsletter
        AND feedback_newsletters.code = :open
        group by feedback_newsletters.code',
        ['idNewsletter' => $newsletter->id, 'open' => FeedbackNewsletterEnum::Open]
        );

        

        if (isset($countOpened) && count($countOpened) > 0) {
             $dtoNewsletter->open = $countOpened[0]->count;
             //$countOpened[0]->count;   
        }else{
           $dtoNewsletter->open = '';
        }
        
            $result->push($dtoNewsletter);
        }
         return $result;
    }
    

     /**
             * Get
             *
             * @param Int id
             *
             * @return Newsletter
             */
            public static function confirmSendMailTestSender(Request $request, int $idLanguage)
            {

            $emailFrom = $request->sender_email;
           // $html = $request->html;

            if (is_null($request->html)){
                $html = '';
            }else{
                $html = $request->html;
            }

           // $title = $request->title;

             if (is_null($request->title)){
                $title = '';
            }else{
                $title = $request->title;
            }
            //$sender = $request->sender;

            if (is_null($request->sender)){
                $sender = '';
            }else{
                $sender = $request->sender;
            }

            $sender_email = $request->sender_email;
       

            if (is_null($request->object)){
                $object = '';
            }else{
                $object = $request->object;
            }

            if (is_null($request->preview_object)){
                $preview_object = '';
            }else{
                $preview_object = $request->preview_object;
            }
           
            #region SEND EMAIL 
            $cssEmail = Storage::disk('public')->get('\css\webCssEmail.css');    
            Mail::to($request->sender_email)->send(new ConfirmTestMailSender($emailFrom, $html, $title,$sender, $sender_email, $object, $preview_object,$cssEmail));
           // $return = $user->id;
            #endregion SEND EMAIL     
        }   



    /**
     * Get
     *
     * @param Int id
     *
     * @return Newsletter
     */
    public static function getById($id)
    {
        return static::_convertToDto(Newsletter::find($id));
    }



 /**
     * Get list email by group id
     * 
     * @param int id
     * @return DtoNewsletter
     */
    public static function getListEmailByGroup(int $id)
    {
        $result = collect();
        
        foreach (DB::select(
            '   SELECT c.email
            FROM contacts_groups_newsletters cgn
            INNER JOIN contacts c ON cgn.contact_id = c.id
            WHERE cgn.group_newsletter_id = :idContact',
            ['idContact' => $id]
        ) as $groupEmail) {
            if(isset($groupEmail)){
                $DtoNewsletter = new DtoNewsletter();
                $DtoNewsletter->email = $groupEmail->email;
                $result->push($DtoNewsletter);
            }
        }

        foreach (DB::select(
            '   SELECT  u.email
            FROM customers_groups_newsletters cgn
            INNER JOIN users u ON cgn.customer_id = u.id
            INNER JOIN users_datas c ON c.user_id = u.id
            WHERE cgn.group_newsletter_id = :idContact',
            ['idContact' => $id]
        ) as $groupNewsletter) {
            if(isset($groupNewsletter)){
                $DtoNewsletter = new DtoNewsletter();
                $DtoNewsletter->email = $groupNewsletter->email;
                $result->push($DtoNewsletter);
            }
        }

        return $result;  
    }


    /**
     * Get html
     * 
     * @param string url
     * 
     * @return html
     */
    public static function getHtml(string $url)
    {
        $newsletter = Newsletter::where('url', $url)->first();
        if (isset($newsletter)) {
            return $newsletter->html;
        } else {
            return '';
        }
    }

    /**
     * Check exist url
     */
    public static function checkExistUrl(string $url, int $id = null)
    {
        if (is_null($id)) {
            return Newsletter::where('url', $url)->count();
        } else {
            return Newsletter::where('url', $url)->whereNotIn('id', [$id])->count();
        }
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @param Request dtoNewsletter
     * @param Int idLanguage
     *
     * @return Int
     */
    public static function insert(Request $dtoNewsletter, int $idLanguage)
    {
        static::_validateData($dtoNewsletter, $idLanguage, DbOperationsTypesEnum::INSERT);
        $newsletter = static::_convertToModel($dtoNewsletter);
        $newsletter->save();
        return $newsletter->id;
    }

    #endregion INSERT


 /**
     * insert insertCampaign
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertCampaign(Request $request)
    {

        DB::beginTransaction();

        try {
            $newsletter = new Newsletter();
            
            if (isset($request->title)) {
                $newsletter->title = $request->title;
            }
             if (isset($request->schedule_date_time)) {
                $newsletter->schedule_date_time = $request->schedule_date_time;
            }
            if (isset($request->draft)) {
                $newsletter->draft = $request->draft;
            }

            if (isset($request->sender)) {
                $newsletter->sender = $request->sender;
            }
            if (isset($request->sender_email)) {
                $newsletter->sender_email = $request->sender_email;
            }
            if (isset($request->object)) {
                $newsletter->object = $request->object;
            }
            if (isset($request->preview_object)) {
                $newsletter->preview_object = $request->preview_object;
            }
            $newsletter->description = '';

            if (isset($request->url)) {
                $newsletter->url = $request->url;
            }else{
                $newsletter->url = '';
            }


        foreach ($request->html as $html) {
                $newsletter->html = $request->html['html'];          
        }

            $newsletter->save();

            foreach ($request->res as $res) {
                $NewsletterRecipient = new NewsletterRecipient();
                $NewsletterRecipient->newsletter_id = $newsletter->id;
                $NewsletterRecipient->name = '';
              // if (isset($res) && !is_null($res)) {
                    $NewsletterRecipient->email = $res['email'];
                    $NewsletterRecipient->group = $res['group'];
              //  }
                $NewsletterRecipient->save();
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $newsletter->id;
    }


    /**
     * getAllviewlistmailerror
     *
     * @param int $id
     */
    public static function getAllviewlistmailerror(int $id)
    {
        $result = collect();
        $DtoNewsletter = new DtoNewsletter();
        $stringerror= 'ERROR'; 
    foreach (DB::select(
    '   SELECT  
        feedback_newsletters.newsletter_recipient_id,
        feedback_newsletters.code,
        contacts.name,
        contacts.surname,
        contacts.business_name,
        newsletters_recipients.email,
        newsletters_recipients.updated_email,
        contacts.send_newsletter
        FROM newsletters_recipients
        INNER JOIN feedback_newsletters ON newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
        LEFT JOIN contacts on newsletters_recipients.email = contacts.email
        LEFT JOIN users on newsletters_recipients.email = users.email
        WHERE newsletters_recipients.newsletter_id = :idNewsletter AND feedback_newsletters.code = \'ERROR\'
        order by feedback_newsletters.created_at desc',
        ['idNewsletter' => $id]
        ) as $groupEmailError) {
            if(isset($groupEmailError)){
                $DtoFeedbackNewsletter = new DtoFeedbackNewsletter();
                $DtoFeedbackNewsletter->newsletter_recipient_id = $groupEmailError->newsletter_recipient_id;
                $DtoFeedbackNewsletter->name = $groupEmailError->name;
                $DtoFeedbackNewsletter->send_newsletter = $groupEmailError->send_newsletter;
                $DtoFeedbackNewsletter->surname = $groupEmailError->surname;
                $DtoFeedbackNewsletter->business_name = $groupEmailError->business_name;
                $DtoFeedbackNewsletter->email = $groupEmailError->email;
                $DtoFeedbackNewsletter->code = $groupEmailError->code;
                $DtoFeedbackNewsletter->updated_email = $groupEmailError->updated_email;
                $DtoNewsletter->ListEmailError->push($DtoFeedbackNewsletter);              
            }
        }
     
        $result->push($DtoNewsletter);
        return $result;  
    }

      /**
     * getAllviewlistmailopen
     *
     * @param int $id
     */
    public static function getAllviewlistmailopen(int $id)
    {
        $result = collect();
        $DtoNewsletter = new DtoNewsletter();

        foreach (DB::select(
            ' select newsletter_recipient_id,
            code,
            name,
            surname,
            business_name,
            email,
            (select feedback_newsletters.created_at
            from feedback_newsletters inner join newsletters_recipients on newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
            where newsletters_recipients.email = resultA.email and newsletters_recipients.newsletter_id = :idNewsletter  AND feedback_newsletters.code = \'OPEN\'
            order by feedback_newsletters.created_at limit 1 ) as created_at,
            (select count(feedback_newsletters.id)
            from feedback_newsletters inner join newsletters_recipients on newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
            where newsletters_recipients.email = resultA.email and newsletters_recipients.newsletter_id = :idNewsletter  AND feedback_newsletters.code = \'OPEN\') as cont
            from (
            select
            feedback_newsletters.newsletter_recipient_id,
            feedback_newsletters.code,
            contacts.name,
            contacts.surname,
            contacts.business_name,
            newsletters_recipients.email
            FROM newsletters_recipients
            LEFT JOIN feedback_newsletters ON newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
            LEFT JOIN contacts on newsletters_recipients.email = contacts.email
            WHERE newsletters_recipients.newsletter_id = :idNewsletter AND feedback_newsletters.code = \'OPEN\'
            group by
            feedback_newsletters.newsletter_recipient_id,
            feedback_newsletters.code,
            contacts.name,
            contacts.surname,
            contacts.business_name,
            newsletters_recipients.email
            ) as resultA
            order by created_at desc',
            ['idNewsletter' => $id]
            ) as $groupEmailOpen) {


  /*  foreach (DB::select(
    '   SELECT  
        feedback_newsletters.newsletter_recipient_id,
        feedback_newsletters.code,
        contacts.name,
        contacts.surname,
        contacts.business_name,
        newsletters_recipients.email,
        feedback_newsletters.created_at
        FROM newsletters_recipients
        LEFT JOIN feedback_newsletters ON newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
        LEFT JOIN contacts on newsletters_recipients.email = contacts.email
        WHERE newsletters_recipients.newsletter_id = :idNewsletter AND feedback_newsletters.code = \'OPEN\'
        order by feedback_newsletters.created_at desc',
        ['idNewsletter' => $id]
        ) as $groupEmailOpen) {*/
            if(isset($groupEmailOpen)){
                $DtoFeedbackNewsletter = new DtoFeedbackNewsletter();
                
                $DtoFeedbackNewsletter->newsletter_recipient_id = $groupEmailOpen->newsletter_recipient_id;
                $DtoFeedbackNewsletter->name = $groupEmailOpen->name;
                $DtoFeedbackNewsletter->created_at = $groupEmailOpen->created_at;
                $DtoFeedbackNewsletter->surname = $groupEmailOpen->surname;
                $DtoFeedbackNewsletter->business_name = $groupEmailOpen->business_name;
                $DtoFeedbackNewsletter->email = $groupEmailOpen->email;
                $DtoFeedbackNewsletter->code = $groupEmailOpen->code;
                $DtoFeedbackNewsletter->cont = $groupEmailOpen->cont;
                $DtoNewsletter->ListEmailOpen->push($DtoFeedbackNewsletter);              
            }
        }
     
        $result->push($DtoNewsletter);
        return $result;  
    }



 /**
     * updateemailnewsletter
     *
     */
    public static function updateemailnewsletter(Request $dtoNewsletter)
    {
        $newsletterrecipients = NewsletterRecipient::where('id', $dtoNewsletter->newsletter_recipient_id)->first();
        $contacts = Contact::where('email', $newsletterrecipients->email)->first();
        $users = User::where('email', $newsletterrecipients->email)->first();

         DB::beginTransaction();
            try {

                if (isset($contacts)) {
                  $contacts->email = $dtoNewsletter->email;    
                }
                $contacts->save();

                if (isset($users)) {
                  $users->email = $dtoNewsletter->email; 
                  $users->save();   
                }

                 $newsletterrecipients->updated_email = true;
                 $newsletterrecipients->save(); 

                 DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
    }


    /**
     * unsubscribefornewsletter
     *
     */
    public static function unsubscribefornewsletter(Request $dtoNewsletter)
    {
        $newsletterrecipients = NewsletterRecipient::where('id', $dtoNewsletter->newsletter_recipient_id)->first();
        $contacts = Contact::where('email', $newsletterrecipients->email)->first();
        $users = User::where('email', $newsletterrecipients->email)->first();


         DB::beginTransaction();
            try {

                if (isset($contacts)) {
                  $contacts->send_newsletter = false;    
                }
                $contacts->save();

                if (isset($users)) {
                  $users->send_newsletter = false; 
                  $users->save();   
                }
                  
                 DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
    }

     public static function unsubscribeallfornewsletter(Request $request)
    {
          DB::beginTransaction();
        try {

        foreach (NewsletterRecipient::where('newsletter_id', $request->id)->get() as $NewsletterRecipient) {
             $contacts = Contact::where('email', $NewsletterRecipient->email)->get()->first();
             $users = User::where('email', $NewsletterRecipient->email)->get()->first();
              
            if (isset($contacts)) {
                $contacts->send_newsletter = false;  
                 $contacts->save();  
            }
               
            if (isset($users)) {
                  $users->send_newsletter = false;
                  $users->save();    
            }
               

        }
          DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
   
/**
     * getByListDateForId
     *
     * @param int $id
     */
    public static function getByListDateForId(int $id)
    {
       
        $result = collect();

        $newsletter= Newsletter::where('id', $id)->first();

        $DtoNewsletter = new DtoNewsletter();
        $DtoNewsletter->id = $id;
        $DtoNewsletter->title = $newsletter->title;
        $DtoNewsletter->schedule_date_time = Carbon::parse($newsletter->schedule_date_time)->format(HttpHelper::getLanguageDateTimeFormat());
        //$newsletter->schedule_date_time;
        //Carbon::parse($newsletter->schedule_date_time)->format(HttpHelper::getLanguageDateTimeFormat());
        $DtoNewsletter->endSendDateTime = $newsletter->end_send_date_time;
        $DtoNewsletter->draft = $newsletter->draft;
        $DtoNewsletter->sender = $newsletter->sender;
        $DtoNewsletter->sender_email = $newsletter->sender_email;
        $DtoNewsletter->object = $newsletter->object;
        $DtoNewsletter->preview_object = $newsletter->preview_object;
        $DtoNewsletter->html = $newsletter->html;
        $DtoNewsletter->url = $newsletter->url;

          
        $newsletter_recipient = NewsletterRecipient::where('newsletter_id', $id)->get(); 
        if (isset($newsletter_recipient)) {
        foreach ($newsletter_recipient as $NewsletterRecipient) {
            $DtoRecipientNewsletter = new DtoRecipientNewsletter();
            $DtoRecipientNewsletter->id = $NewsletterRecipient->id;
            $DtoRecipientNewsletter->newsletter_id = $id;
            $DtoRecipientNewsletter->email = $NewsletterRecipient->email; 
            $DtoRecipientNewsletter->group = $NewsletterRecipient->group; 
            $DtoRecipientNewsletter->visible=true;
            $DtoRecipientNewsletter->checked=false;
                          
            $countemailsend = DB::table('newsletters_recipients')
             ->select(DB::raw('count(id) as num_id'))
             ->where('newsletter_id', '=', $id)
             ->groupBy('newsletter_id')
             ->get();
            $DtoNewsletter->nrdestinatari = $countemailsend[0]->num_id;   
            
            //Estrapolare totale range mail + secondi dalle Configurazioni:
             $parameterNewsletterBlockDelay = 'NewsletterBlockDelay';  
             $NewsletterBlockDelay = Setting::where('code', $parameterNewsletterBlockDelay)->first()->value;
             $parameterNewsletterBlockItems = 'NewsletterBlockItems';  
             $NewsletterBlockItems = Setting::where('code', $parameterNewsletterBlockItems)->first()->value;

            if ($DtoNewsletter->nrdestinatari > $NewsletterBlockItems) {
                 //calcolo numero blocchi
                $nrblocchi = $DtoNewsletter->nrdestinatari/$NewsletterBlockItems;
                $DtoNewsletter->nrblocchi = $nrblocchi;
       
                //durata invio: 
                $duratainvio = ($nrblocchi * $NewsletterBlockDelay) + $nrblocchi;
                $duratainvio1 = $duratainvio + ($DtoNewsletter->nrdestinatari * 1.5);
                $DtoNewsletter->sending_duration = $duratainvio1;
                //ora di fine stimata:
                $sommaorafinestimata= date('Y-m-d H:i:s', (strtotime(date($newsletter->schedule_date_time)) + $duratainvio1));
                $DtoNewsletter->sommaorafinestimata = $sommaorafinestimata;
            }else{
                //durata invio: 
               $duratainviosec = $NewsletterBlockDelay;
               $DtoNewsletter->sending_duration = $duratainviosec; 
                //ora di fine stimata:
                $sommaorafinestimatasingolo= date('Y-m-d H:i:s', (strtotime(date($newsletter->schedule_date_time)) + $duratainviosec));
                $DtoNewsletter->sommaorafinestimata = $sommaorafinestimatasingolo;
            }
         
            $DtoNewsletter->listEmail->push($DtoRecipientNewsletter);
            }
        }
        $result->push($DtoNewsletter);
        return $result;
}


     /**
             * Clone
             * 
             * @param int id
             * @param int idFunctionality
             * @param int idLanguage
             * @param int idUser
             * 
             * @return int
             */
            public static function clone(Request $request, int $idLanguage)
            {
                 $Newsletter = Newsletter::find($request->id);

                DB::beginTransaction();
                try {

            if ($request->duplicatecontent == true) {
                $newNewsletter = new Newsletter();
                $newNewsletter->title = $Newsletter->title . ' - Copy';
                $newNewsletter->sender = $Newsletter->sender . ' - Copy';
                $newNewsletter->draft = $Newsletter->draft;
                $newNewsletter->sender_email = $Newsletter->sender_email;
                $newNewsletter->html = $Newsletter->html;
                $newNewsletter->object = $Newsletter->object;
                $newNewsletter->preview_object = $Newsletter->preview_object;
                $newNewsletter->description = '';
                $newNewsletter->url = '';
                $newNewsletter->save();    
            }else{
                $newNewsletter = new Newsletter();
                $newNewsletter->description = '';
                $newNewsletter->url = '';
                $newNewsletter->draft = true;
                $newNewsletter->html = '';
                $newNewsletter->title ='';
                $newNewsletter->sender = '';
                $newNewsletter->sender_email = '';
                $newNewsletter->object = '';
                $newNewsletter->preview_object = '';
                $newNewsletter->save();    
            }
              if ($request->duplicaterecipients == true) {
                $NewsletterRecipient = NewsletterRecipient::where('newsletter_id', $request->id)->get();  
                foreach ($NewsletterRecipient as $resultNewsletterRecipient) {
                $newNewsletterRecipient = new NewsletterRecipient();
                $newNewsletterRecipient->newsletter_id = $newNewsletter->id;    
                $newNewsletterRecipient->name = '';
                $newNewsletterRecipient->email = $resultNewsletterRecipient->email;
                $newNewsletterRecipient->group = $resultNewsletterRecipient->group;
                $newNewsletterRecipient->save();   
            }
            }else{
                $newNewsletterRecipient = new NewsletterRecipient();
                $newNewsletterRecipient->newsletter_id = $newNewsletter->id;    
                $newNewsletterRecipient->name = '';
                $newNewsletterRecipient->email = '';
                $newNewsletterRecipient->group = '';
                $newNewsletterRecipient->save();   
            }

                DB::commit();

                return $newNewsletter->id;
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
            }

        #endregion CLONE

/**
     * getemail
     * 
     * @param int id
     * @return DtoNewsletter
     */
    public static function getemail(int $id)
    {
        $Contact = Contact::where('id', $id)->first();

        $DtoNewsletter = new DtoNewsletter();
        if(isset($Contact)){
            if(isset($Contact->email)){
                $DtoNewsletter->email_select_contact = $Contact->email;
            }else{
                throw new Exception('Email non trovata', HttpResultsCodesEnum::InvalidPayload);
            }
        }else{
            throw new Exception('Contatto non presente', HttpResultsCodesEnum::InvalidPayload);
        }
        
        return $DtoNewsletter;  
    }

    
/**
     * getemailusers
     * 
     * @param int id
     * @return DtoNewsletter
     */
    public static function getemailusers(int $id)
    {
        $User = User::where('id', $id)->first();

        $DtoNewsletter = new DtoNewsletter();
        if(isset($User)){
            if(isset($User->email)){
                $DtoNewsletter->emailuser_select_contact = $User->email;
            }else{
                throw new Exception('Email non trovata', HttpResultsCodesEnum::InvalidPayload);
            }
        }else{
            throw new Exception('Utente non presente', HttpResultsCodesEnum::InvalidPayload);
        }
        
        return $DtoNewsletter;  
    }



  #region updateCampaign

    /**
     * Update
     *
     * @param Request dtoNewsletter
     * @param Int idLanguage
     */
    public static function updateCampaign(Request $dtoNewsletter, int $idLanguage)
    {
      $Newsletter= Newsletter::where('id', $dtoNewsletter->id)->first();
        
        DB::beginTransaction();
            try {
                $Newsletter->id = $dtoNewsletter->id;
                $Newsletter->title = $dtoNewsletter->title;
                $Newsletter->schedule_date_time = $dtoNewsletter->schedule_date_time;
                // date_format('Y-m-d H:i:s', $dtoNewsletter->schedule_date_time);
                //
                //Carbon::parse($dtoNewsletter->schedule_date_time)->format(HttpHelper::getLanguageDateTimeFormat());
                $Newsletter->draft = $dtoNewsletter->draft;
                $Newsletter->sender = $dtoNewsletter->sender;
                $Newsletter->sender_email = $dtoNewsletter->sender_email;
                $Newsletter->object = $dtoNewsletter->object;
                $Newsletter->preview_object = $dtoNewsletter->preview_object;
                $Newsletter->html = $dtoNewsletter->html;
                $Newsletter->url = $dtoNewsletter->url;
                $Newsletter->save();

                $NewsletterRecipient = NewsletterRecipient::where('newsletter_id', $dtoNewsletter->id)->get();       
                if (isset($NewsletterRecipient)) {
                    NewsletterRecipient::where('newsletter_id', $dtoNewsletter->id)->delete();
                }


                foreach ($dtoNewsletter->res as $res) {
                    if (!is_null($res['email'])){
                        $NewsletterRecipient = new NewsletterRecipient();
                        $NewsletterRecipient->newsletter_id = $Newsletter->id;
                        $NewsletterRecipient->name = '';
                        $NewsletterRecipient->email = $res['email'];
                        $NewsletterRecipient->group = $res['group'];
                        $NewsletterRecipient->save();
                    }
                }

            DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    #endregion updateCampaign

  
     /**
     * updateOnlineNewsletterCheck
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateOnlineNewsletterCheck(Request $request)
    {
        $NewsletterOnDb = Newsletter::find($request->id);

        if (isset($NewsletterOnDb)) {
            $NewsletterOnDb->draft = !$NewsletterOnDb->draft;
            $NewsletterOnDb->save();
        } else {
             $NewsletterOnDb->draft = true;
             $NewsletterOnDb->save();
            }
    }

    #region UPDATE
    /**
     * Update
     *
     * @param Request dtoNewsletter
     * @param Int idLanguage
     */
    public static function update(Request $dtoNewsletter, int $idLanguage)
    {
        static::_validateData($dtoNewsletter, $idLanguage, DbOperationsTypesEnum::INSERT);
        $newsletter = Newsletter::find($dtoNewsletter->id);
        DBHelper::updateAndSave(static::_convertToModel($dtoNewsletter), $newsletter);
    }

    #endregion UPDATE

     /**
     * getListEmailQueryGroupNewsletter
     *
     * 
     * 
     */
    public static function getListEmailQueryGroupNewsletter(int $id)
    {
        $EmailQueryGroupNewsletterSelenaSqlView = SelenaSqlViews::where('id', $id)->first();
        if (isset($EmailQueryGroupNewsletterSelenaSqlView)) {
            $QueryResult = "";
            if (isset($EmailQueryGroupNewsletterSelenaSqlView->select)) {
                $QueryResult = $QueryResult . 'SELECT ' . $EmailQueryGroupNewsletterSelenaSqlView->select;
                if (isset($EmailQueryGroupNewsletterSelenaSqlView->from)) {
                    $QueryResult = $QueryResult . ' FROM ' . $EmailQueryGroupNewsletterSelenaSqlView->from;

                    if (isset($EmailQueryGroupNewsletterSelenaSqlView->where)) {
                        $QueryResult = $QueryResult . ' WHERE ' . $EmailQueryGroupNewsletterSelenaSqlView->where;
                    }

                    if (isset($EmailQueryGroupNewsletterSelenaSqlView->order_by)) {
                        $QueryResult = $QueryResult . ' ORDER BY ' . $EmailQueryGroupNewsletterSelenaSqlView->order_by;
                    }

                    if (isset($EmailQueryGroupNewsletterSelenaSqlView->group_by)) {
                        $QueryResult = $QueryResult . ' GROUP BY ' . $EmailQueryGroupNewsletterSelenaSqlView->group_by;
                    }
                }
            }
        }

        $rowsResult = DB::select($QueryResult);
        $result = collect();
        if (isset($rowsResult)) {
          foreach ($rowsResult as $RowsResult) {  
            $DtoNewsletter = new DtoNewsletter(); 
            $DtoNewsletterGroupQueryEmail = new DtoNewsletterGroupQueryEmail();
            $DtoNewsletterGroupQueryEmail->email = $RowsResult->email;  
            $result->push($DtoNewsletterGroupQueryEmail);   
        } 
       return  $result;
        } else {
            return '';
        }
    }

    #endregion getListEmailQueryGroupNewsletter
 
    #region DELETE

    /**
     * Delete
     *
     * @param Int id
     * @param Int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $newsletter = Newsletter::find($id);
        $newsletterRecipient= NewsletterRecipient::where('newsletter_id', $id)->delete();

        if (is_null($newsletter)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NewsletterNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        //$newsletterRecipient->delete();

        $newsletter->delete();
    }

    #endregion DELETE
}

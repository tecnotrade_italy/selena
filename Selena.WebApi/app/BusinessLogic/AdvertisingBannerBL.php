<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoAdvertisingBanner;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\AdvertisingBanner;
use App\AdvertisingArea;
use Exception;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class AdvertisingBannerBL
{
    #region PRIVATE

    private static $dirImage = '../storage/app/public/images/AdvertisingBanner/Previews/';
    private static $mimeTypeAllowed = ['jpeg', 'jpg', 'png', 'gif'];

    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request dtoAdvertisingBanner
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, $dbOperationsTypesEnum)
    {

        $idLanguage = LanguageBL::getDefault()->id;

        if (!is_null($request->imgName)) {
            if (!in_array(\explode('/', \explode(':', substr($request->img, 0, strpos($request->img, ';')))[1])[1], static::$mimeTypeAllowed)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FileTypeNotAllowed), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Storage::disk('public')->exists('images/TemplateEditor/Previews/' . $request->imgName)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameFileAlreadyExists), HttpResultsCodesEnum::NotAcceptable);
            }
        }

        if (is_null($request->advertising_area_id)|| $request->advertising_area_id=='') {
            throw new Exception("L'area del banner è obbligatoria");
        }
        if (is_null($request->description)|| $request->description=='') {
            throw new Exception("La descrizione è obbligatoria");
        }
        if (is_null($request->start_date)|| $request->start_date=='') {
            throw new Exception("La data inizio è obbligatoria");
        }
        if (is_null($request->weight)|| $request->weight==''|| $request->weight<='0') {
            throw new Exception("Il peso è obbligatorio");
        }
        if (!is_numeric($request->weight)) {
            throw new Exception("Il peso deve essere numerico");
        }

        //la somma di tutti i pesi dei banner attivi in quell'area deve essere <= 100
        $sommaDeiPesi = DB::table('advertising_banners')->where('id','<>', $request->id)->where('advertising_area_id', $request->advertising_area_id)->where('active', '1')->sum('weight');
        if ($sommaDeiPesi+$request->weight > 100)
        {
            throw new Exception("La somma dei pesi dei banner ATTIVI in quest'area deve essere <= 100");
        }

    }

    /**
     * Convert to dto
     * 
     * @param AdvertisingBanner AdvertisingBanner
     * @return DtoAdvertisingBanner
     */
    private static function _convertToDto($AdvertisingBanner)
    {
        $dtoAdvertisingBanner = new DtoAdvertisingBanner();

        if (isset($AdvertisingBanner->id)) {
            $dtoAdvertisingBanner->id = $AdvertisingBanner->id;
        }

        if (isset($AdvertisingBanner->advertising_area_id)) {
            $dtoAdvertisingBanner->advertising_area_id = $AdvertisingBanner->advertising_area_id;
        }

        if (isset($AdvertisingBanner->start_date)) {
            $dtoAdvertisingBanner->start_date = $AdvertisingBanner->start_date;
        }

        if (isset($AdvertisingBanner->stop_date)) {
            $dtoAdvertisingBanner->stop_date = $AdvertisingBanner->stop_date;
        }

        if (isset($AdvertisingBanner->image_file)) {
            $dtoAdvertisingBanner->image_file = $AdvertisingBanner->image_file;
        }

        if (isset($AdvertisingBanner->image_alt)) {
            $dtoAdvertisingBanner->image_alt = $AdvertisingBanner->image_alt;
        }

        if (isset($AdvertisingBanner->link_url)) {
            $dtoAdvertisingBanner->link_url = $AdvertisingBanner->link_url;
        }

        if (isset($AdvertisingBanner->target_blank)) {
            $dtoAdvertisingBanner->target_blank = $AdvertisingBanner->target_blank;
        }

        if (isset($AdvertisingBanner->active)) {
            $dtoAdvertisingBanner->active = $AdvertisingBanner->active;
        }

        if (isset($AdvertisingBanner->script_Js)) {
            $dtoAdvertisingBanner->script_Js = $AdvertisingBanner->script_Js;
        }

        if (isset($AdvertisingBanner->maximum_impressions)) {
            $dtoAdvertisingBanner->maximum_impressions = $AdvertisingBanner->maximum_impressions;
        }

        if (isset($AdvertisingBanner->progressive_impressions)) {
            $dtoAdvertisingBanner->progressive_impressions = $AdvertisingBanner->progressive_impressions;
        }

        if (isset($AdvertisingBanner->weight)) {
            $dtoAdvertisingBanner->weight = $AdvertisingBanner->weight;
        }

        if (isset($AdvertisingBanner->user_id)) {
            $dtoAdvertisingBanner->user_id = $AdvertisingBanner->user_id;
        }

        if (isset($AdvertisingBanner->active_reports)) {
            $dtoAdvertisingBanner->active_reports = $AdvertisingBanner->active_reports;
        }

        if (isset($AdvertisingBanner->description)) {
            $dtoAdvertisingBanner->description = $AdvertisingBanner->description;
        }

        return $dtoAdvertisingBanner;
    }


    /**
     * Convert to VatType
     * 
     * @param dtoAdvertisingBanner dtoAdvertisingBanner
     * 
     * @return AdvertisingBanner
     */
    private static function _convertToModel($dtoAdvertisingBanner)
    {
        $AdvertisingBanner = new AdvertisingBanner();

        if (isset($dtoAdvertisingBanner->id)) {
            $AdvertisingBanner->id = $dtoAdvertisingBanner->id;
        }

        if (isset($dtoAdvertisingBanner->advertising_area_id)) {
            $AdvertisingBanner->advertising_area_id = $dtoAdvertisingBanner->advertising_area_id;
        }

        if (isset($dtoAdvertisingBanner->start_date)) {
            $AdvertisingBanner->start_date = $dtoAdvertisingBanner->start_date;
        } 

        if (isset($dtoAdvertisingBanner->stop_date)) {
            $AdvertisingBanner->stop_date = $dtoAdvertisingBanner->stop_date;
        } else
            $AdvertisingBanner->stop_date = null;

        if (isset($dtoAdvertisingBanner->image_file)) {
            if (strpos($dtoAdvertisingBanner->image_file, '/storage/app/public/images/AdvertisingBanner/Previews/') === false) {
                Image::make($dtoAdvertisingBanner->image_file)->save(static::$dirImage . $dtoAdvertisingBanner->image_fileName);
                $AdvertisingBanner->image_file =  static::getUrl() . $dtoAdvertisingBanner->image_fileName;
            }
        }else
            $AdvertisingBanner->image_file = null;

        if (isset($dtoAdvertisingBanner->image_alt)) {
            $AdvertisingBanner->image_alt = $dtoAdvertisingBanner->image_alt;
        }else
            $AdvertisingBanner->image_alt = null;

        if (isset($dtoAdvertisingBanner->link_url)) {
            $AdvertisingBanner->link_url = $dtoAdvertisingBanner->link_url;
        }else
             $AdvertisingBanner->link_url = null;

        if (isset($dtoAdvertisingBanner->target_blank)) {
            $AdvertisingBanner->target_blank = $dtoAdvertisingBanner->target_blank;
        }

        if (isset($dtoAdvertisingBanner->active)) {
            $AdvertisingBanner->active = $dtoAdvertisingBanner->active;
        }

        if (isset($dtoAdvertisingBanner->script_Js)) {
            $AdvertisingBanner->script_Js = $dtoAdvertisingBanner->script_Js;
        }else
            $AdvertisingBanner->script_Js = null;

        if (isset($dtoAdvertisingBanner->maximum_impressions)) {
            $AdvertisingBanner->maximum_impressions = $dtoAdvertisingBanner->maximum_impressions;
        }else
            $AdvertisingBanner->maximum_impressions = 0;

        if (isset($dtoAdvertisingBanner->progressive_impressions)) {
            $AdvertisingBanner->progressive_impressions = $dtoAdvertisingBanner->progressive_impressions;
        }  else
            $AdvertisingBanner->progressive_impressions = 0;

        if (isset($dtoAdvertisingBanner->weight)) {
            $AdvertisingBanner->weight = $dtoAdvertisingBanner->weight;
        }else
            $AdvertisingBanner->weight = null;

        if (isset($dtoAdvertisingBanner->user_id)) {
            $AdvertisingBanner->user_id = $dtoAdvertisingBanner->user_id;
        }else
            $AdvertisingBanner->user_id = null;

        if (isset($dtoAdvertisingBanner->active_reports)) {
            $AdvertisingBanner->active_reports = $dtoAdvertisingBanner->active_reports;
        }
        
        if (isset($dtoAdvertisingBanner->description)) {
            $AdvertisingBanner->description = $dtoAdvertisingBanner->description;
        }

        return $AdvertisingBanner;
    }

    #endregion PRIVATE

    #region GET

    private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/AdvertisingBanner/Previews/';
    }

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCommunityction
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(AdvertisingBanner::find($id));
    }

    /*
     * Get all
     * 
     * @return DtoAdvertisingBanner
     */
    public static function getAll()
    {

        $result = collect(); 

        foreach (AdvertisingBanner::orderBy('id','DESC')->get() as $advertisingBanner) {

            $DtoAdvertisingBanner = new DtoAdvertisingBanner();

            $DtoAdvertisingBanner->id = $advertisingBanner->id;
            $DtoAdvertisingBanner->advertising_area_id = $advertisingBanner->advertising_area_id;
            $DtoAdvertisingBanner->start_date = $advertisingBanner->start_date;
            $DtoAdvertisingBanner->stop_date = $advertisingBanner->stop_date;
            $DtoAdvertisingBanner->image_file = $advertisingBanner->image_file;
            $DtoAdvertisingBanner->image_alt = $advertisingBanner->image_alt;
            $DtoAdvertisingBanner->link_url = $advertisingBanner->link_url;
            $DtoAdvertisingBanner->target_blank = $advertisingBanner->target_blank;
            $DtoAdvertisingBanner->active = $advertisingBanner->active;
            $DtoAdvertisingBanner->script_Js = $advertisingBanner->script_Js;
            $DtoAdvertisingBanner->maximum_impressions = $advertisingBanner->maximum_impressions;
            $DtoAdvertisingBanner->progressive_impressions = $advertisingBanner->progressive_impressions;
            $DtoAdvertisingBanner->weight = $advertisingBanner->weight;
            $DtoAdvertisingBanner->user_id = $advertisingBanner->user_id;
            $DtoAdvertisingBanner->active_reports = $advertisingBanner->active_reports;
            $DtoAdvertisingBanner->description = $advertisingBanner->description;

            $AdvertisingArea = AdvertisingArea::find($advertisingBanner->advertising_area_id);
            $DtoAdvertisingBanner->advertising_area = $AdvertisingArea->description;

            $result->push($DtoAdvertisingBanner); 
        }
        return $result;

    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoAdvertisingBanner
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoAdvertisingBanner)
    {
        
        static::_validateData($DtoAdvertisingBanner, DbOperationsTypesEnum::INSERT);
        $AdvertisingBanner = static::_convertToModel($DtoAdvertisingBanner);

        $AdvertisingBanner->save();

        return $AdvertisingBanner->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoAdvertisingBanner

     */
    public static function update(Request $DtoAdvertisingBanner)
    {
        static::_validateData($DtoAdvertisingBanner, DbOperationsTypesEnum::UPDATE);
        $AdvertisingBanner = AdvertisingBanner::find($DtoAdvertisingBanner->id);
        DBHelper::updateAndSave(static::_convertToModel($DtoAdvertisingBanner), $AdvertisingBanner);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id

     */
    public static function delete(int $id)
    {
        $AdvertisingBanner = AdvertisingBanner::find($id);

        $AdvertisingBanner->delete();
    }

    #endregion DELETE
}

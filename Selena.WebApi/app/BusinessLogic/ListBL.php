<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoList;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\LogHelper;
use App\PriceList;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Http\Request;

class ListBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request DtoList
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoList, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoList->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (PriceList::find($DtoList->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($DtoList->code_list)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CompanyNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoList->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CompanyNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }
    /**
     * Convert to dto
     * 
     * @param List postalCode
     * 
     * @return DtoList
     */
    private static function _convertToDto($List)
    {
        $DtoList = new DtoList();

        if (isset($List->id)) {
            $DtoList->id = $List->id;
        }
        if (isset($List->code_list)) {
            $DtoList->code_list = $List->code_list;
        }
        if (isset($List->description)) {
            $DtoList->description = $List->description;
        }
        if (isset($List->beginning_validity)) {
            $DtoList->beginning_validity = $List->beginning_validity;
        }
        if (isset($List->end_validity)) {
            $DtoList->end_validity = $List->end_validity;
        }
        if (isset($List->value_list)) {
            $DtoList->value_list = $List->value_list;
        }
        return  $DtoList;
    }
    /**
     * Convert to VatType
     * 
     * @param DtoList dtoList
     * 
     * @return List
     */
    private static function _convertToModel($DtoList)
    {
        $List = new PriceList();

        if (isset($DtoList->id)) {
            $List->id = $DtoList->id;
        }
        if (isset($DtoList->code_list)) {
            $List->code_list = $DtoList->code_list;
        }
        if (isset($DtoList->description)) {
            $List->description = $DtoList->description;
        }
        if (isset($DtoList->beginning_validity)) {
            $List->beginning_validity = $DtoList->beginning_validity;
        }
        if (isset($DtoList->end_validity)) {
            $List->end_validity = $DtoList->end_validity;
        }
        if (isset($DtoList->value_list)) {
            $List->value_list = $DtoList->value_list;
        }
        return $List;
    }
    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoList
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(PriceList::find($id));
    }

    /**
     * Get select
     *
     * @param String $search
     * @return List
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return PriceList::select('id', 'code_list as description')->get();
        } else {
            return PriceList::where('code_list', 'ilike', $search . '%')->select('id', 'code_list as description')->get();
        }
    }

    /**
     * Get all
     * 
     * @return DtoList
     */
    public static function getAll()
    {
        $result = collect();
        foreach (PriceList::orderBy('id', 'DESC')->get() as $List) {
            $DtoList = new DtoList();
            $DtoList->id = $List->id;
            $DtoList->code_list = $List->code_list;
            $DtoList->description = $List->description;
            $DtoList->beginning_validity = $List->beginning_validity;
            $DtoList->end_validity = $List->end_validity;
            $DtoList->value_list = $List->value_list;
            $result->push($DtoList);
        }
        return $result;
    }


    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoList
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoList, int $idUser)
    {
        static::_validateData($DtoList, $DtoList->idLanguage, DbOperationsTypesEnum::INSERT);
        DB::beginTransaction();

        try {
            $List = new PriceList();
            //$List->id = $DtoList->id;
            $List->code_list = $DtoList->code_list;
            $List->description = $DtoList->description;
            $List->beginning_validity = $DtoList->beginning_validity;
            $List->end_validity = $DtoList->end_validity;
            $List->value_list = $DtoList->value_list;
            $List->created_id = $DtoList->$idUser;
            $List->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $List->id;
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoList
     * @param int idLanguage
     */

    public static function update(Request $DtoList)
    {
        static::_validateData($DtoList, $DtoList->idLanguage, DbOperationsTypesEnum::UPDATE);
        $ListOnDb = PriceList::find($DtoList->id);

        DB::beginTransaction();

        try {

            $ListOnDb->code_list = $DtoList->code_list;
            $ListOnDb->description = $DtoList->description;
            $ListOnDb->beginning_validity = $DtoList->beginning_validity;
            $ListOnDb->end_validity = $DtoList->end_validity;
            $ListOnDb->value_list = $DtoList->value_list;
            //$ListOnDb->created_id = $DtoList->$idUser;
            $ListOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */

    public static function delete(int $id)
    {
        $List = PriceList::find($id);

        if (is_null($List)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $List->delete();
    }

    #endregion DELETE
}

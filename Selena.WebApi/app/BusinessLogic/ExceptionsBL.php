<?php

namespace App\BusinessLogic;

use App\BookingServices;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoExceptions;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\LanguageNation;
use App\LanguageProvince;
use App\User;
use App\Employees;
use App\Exceptions;
use App\Message;
use App\Places;
use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;

class ExceptionsBL
{

     #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request DtoPlaces
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoExceptions, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoExceptions->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Exceptions::find($DtoExceptions->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }
    }

    /**
     * Convert to dto
     * 
     * @param Employees
     * 
     * @return DtoExceptions
     */
    private static function _convertToDto($Exceptions)
    {
        $DtoExceptions = new DtoExceptions();

        if (isset($Exceptions->id)) {
            $DtoExceptions->id = $Exceptions->id;
        }
        if (isset($Exceptions->place_id)) {
            $DtoExceptions->place_id = $Exceptions->place_id;
        }
        if (isset($Exceptions->service_id)) {
            $DtoExceptions->service_id = $Exceptions->service_id;
        }
        if (isset($Exceptions->employee_id)) {
            $DtoExceptions->employee_id = $Exceptions->employee_id;
        }
        if (isset($Exceptions->tooltip)) {
            $DtoExceptions->tooltip = $Exceptions->tooltip;
        }
        if (isset($Exceptions->date_start)) {
            $DtoExceptions->date_start = $Exceptions->date_start;
        }
        if (isset($Exceptions->date_end)) {
            $DtoExceptions->date_end = $Exceptions->date_end;
        }
        return $DtoExceptions;   
    }

    #endregion PRIVATE

    #region GET
    /**
     * Get by id
     * 
     * @param int id
     * @return DtoExceptions
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Exceptions::find($id));
    }

    /**
     * Get select
     *
     * @param String $search
     * @param int $idUser
     * 
     */
    public static function getSelect(string $search)
    {
                if ($search == "*") {
                    return Exceptions::select('id', 'name as description')->get();
                } else {
                    return Exceptions::where('name', 'ilike' , $search . '%')->select('id', 'name as description')->get();
          } 
    }


    /**
     * Get all
     * 
     * @return DtoExceptions
     */
    public static function getAll()
    {
        $result = collect();

        foreach (Exceptions::orderBy('id','DESC')->get() as $Exceptions) {    

                $DtoExceptions = new DtoExceptions();
            
                if (isset($Exceptions->id)) {
                    $DtoExceptions->id = $Exceptions->id;
                }
                if (isset($Exceptions->place_id)) {
                    $DtoExceptions->place_id = $Exceptions->place_id;
                }else{
                    $DtoExceptions->place_id = '-';  
                }
                if (isset($Exceptions->service_id)) {
                    $DtoExceptions->service_id = $Exceptions->service_id;
                }else{
                    $DtoExceptions->service_id = '-';  
                }
                if (isset($Exceptions->employee_id)) {
                    $DtoExceptions->employee_id = $Exceptions->employee_id;
                }else{
                    $DtoExceptions->employee_id = '-';  
                }
                if (isset($Exceptions->place_id)) {
                    $DtoExceptions->PlaceId = Places::where('id', $Exceptions->place_id)->first()->name;
                }else{
                    $DtoExceptions->PlaceId = '-';  
                }
                if (isset($Exceptions->service_id)) {
                    $DtoExceptions->ServiceId = BookingServices::where('id', $Exceptions->service_id)->first()->name;
                }else{
                    $DtoExceptions->ServiceId = '-';  
                }
                if (isset($Exceptions->employee_id)) {
                    $DtoExceptions->EmployeeId = Employees::where('id', $Exceptions->employee_id)->first()->name;
                }else{
                    $DtoExceptions->EmployeeId = '-';  
                }

                if (isset($Exceptions->tooltip)) {
                    $DtoExceptions->tooltip = $Exceptions->tooltip;
                }else{
                    $DtoExceptions->tooltip = '-';  
                }
                if (isset($Exceptions->date_start)) {
                    $DtoExceptions->date_start = $Exceptions->date_start;
                }else{
                    $DtoExceptions->date_start = '-';  
                }
                 if (isset($Exceptions->date_end)) {
                    $DtoExceptions->date_end = $Exceptions->date_end;
                }else{
                    $DtoExceptions->date_end = '-';  
                }
                    $result->push($DtoExceptions);
                }
                return $result;
    }
    #endregion GET

    #region INSERT
    /**
     * Insert
     * 
     * @param Request DtoExceptions
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoExceptions, int $idUser)
    {
       DB::beginTransaction();
        try {

            $Exceptions = new Exceptions();
            
            if (isset($DtoExceptions->place_id)) {
                $Exceptions->place_id = $DtoExceptions->place_id;
            } 
            if (isset($DtoExceptions->service_id)) {
                $Exceptions->service_id = $DtoExceptions->service_id;
            } 
            if (isset($DtoExceptions->employee_id)) {
                $Exceptions->employee_id = $DtoExceptions->employee_id;
            } 
            if (isset($DtoExceptions->tooltip)) {
                $Exceptions->tooltip = $DtoExceptions->tooltip;
            } 
            if (isset($DtoExceptions->date_start)) {
                $Exceptions->date_start = $DtoExceptions->date_start;
            } 
            if (isset($DtoExceptions->date_end)) {
                $Exceptions->date_end = $DtoExceptions->date_end;
            } 
            $Exceptions->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $Exceptions->id;
    }
    #endregion INSERT

    /**
     * Update
     * 
     * @param Request [DtoEmployees]
     * @param int idLanguage
     */
    public static function update(Request $DtoExceptions, int $idUser)
    {
       
        $ExceptionsOnDb = Exceptions::find($DtoExceptions->id);
      
        DB::beginTransaction();

        try {
            if (isset($DtoExceptions->id)) {
                $ExceptionsOnDb->id = $DtoExceptions->id;
            } 
            if (isset($DtoExceptions->place_id)) {
                $ExceptionsOnDb->place_id = $DtoExceptions->place_id;
            }
            if (isset($DtoExceptions->service_id)) {
                $ExceptionsOnDb->service_id = $DtoExceptions->service_id;
            }
            if (isset($DtoExceptions->employee_id)) {
                $ExceptionsOnDb->employee_id = $DtoExceptions->employee_id;
            }
            if (isset($DtoExceptions->tooltip)) {
                $ExceptionsOnDb->tooltip = $DtoExceptions->tooltip;
            }
            if (isset($DtoExceptions->date_start)) {
                $ExceptionsOnDb->date_start = $DtoExceptions->date_start;
            }
            if (isset($DtoExceptions->date_end)) {
                $ExceptionsOnDb->date_end = $DtoExceptions->date_end;
            }
            $ExceptionsOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return  $DtoExceptions->id;
    }
    #endregion UPDATE


        /**
             * Clone
             * 
             * @param int id
             * @param int idFunctionality
             * @param int idLanguage
             * @param int idUser
             * 
             * @return int
             */
            public static function clone(int $id, int $idFunctionality, int $idUser, int $idLanguage)
            {
            
                $Exceptions = Exceptions::find($id);            
                DB::beginTransaction();

                try {

                    $newExceptions = new Exceptions();
                    $newExceptions->place_id = $Exceptions->place_id;
                    $newExceptions->service_id = $Exceptions->service_id;
                    $newExceptions->employee_id = $Exceptions->employee_id;
                    $newExceptions->tooltip = $Exceptions->tooltip . ' - Copy';
                    $newExceptions->date_start = $Exceptions->date_start;
                    $newExceptions->date_end = $Exceptions->date_end;
                    $newExceptions->save();
   
                    DB::commit();
                    return $newExceptions->id;
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
            }
        #endregion CLONE

            #region DELETE
                    /**
                     * Delete
                     * 
                     * @param int id
                     * @param int idLanguage
                     */
                    public static function delete(int $id, int $idLanguage)
                    {
                        $Exceptions = Exceptions::find($id);                   

                        if (is_null($Exceptions)) {
                            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PeopleRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
                        }
                        DB::beginTransaction();
                        try {
                            $Exceptions->delete();
                            
                            DB::commit();
                        } catch (Exception $ex) {
                            DB::rollback();
                            throw $ex;
                        }
                    }
                
                }
        #endregion DELETE
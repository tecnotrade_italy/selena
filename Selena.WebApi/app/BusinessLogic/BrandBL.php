<?php

namespace App\BusinessLogic;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoBrand;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Brand;
use App\ItemGroupTechnicalSpecification;
use Intervention\Image\Facades\Image;
use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;


class BrandBL
{
 #region GET

 /**
     * Get select
     *
     * @param String $search
     * @return Intervention
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return Brand::select('id', 'brand as description')->get();
        } else {
            return Brand::where('brand', 'ilike', $search . '%')->select('id', 'brand as description')->get();
        }
    }

 }
       
<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoRole;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\Role;
use App\RoleUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class RoleUserBL
{
    #region PRIVATE

    /**
     * Check function enabled
     * 
     * @param int idFunctionlity
     * @param int idUser
     * @param int idLanguage
     */
    private static function _checkFunctionEnabled(int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (is_null($idFunctionality)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Functionality::find($idFunctionality))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    /**
     * Validate data
     * 
     * @param int idUserChk
     * @param int idRole
     * @param int idFunctionlity
     * @param int idUser
     * @param int idLanguage
     */
    private static function _validate(int $idUserChk, int $idRole, int $idFunctionality, int $idUser, int $idLanguage)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        if (is_null($idUserChk)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEmpty), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(User::find($idUserChk))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($idRole)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::RoleNotEmpty), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Role::find($idRole))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::RoleNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get Role included by User
     * 
     * @param int idUser
     * 
     * @return DtoRole
     */
    public static function getRoleIncludedByUser(int $idUser)
    {
        $result = collect();

        foreach (DB::select('SELECT role_user.id, roles.display_name
                            FROM roles
                            INNER JOIN role_user ON roles.id = role_user.role_id
                            WHERE roles.name != \'admin\'
                            AND role_user.user_id = :idUser', ['idUser' => $idUser]) as $role) {
            $dtoRole = new DtoRole();
            $dtoRole->id = $role->id;
            $dtoRole->description = $role->display_name;
            $result->push($dtoRole);
        }

        return $result;
    }

    /**
     * Get all Role included by User
     * 
     * @param int idUser
     * 
     * @return DtoRole
     */
    public static function getRoleAllIncludedByUser(int $idUser)
    {
        $result = collect();

        foreach (DB::select('SELECT role_user.id, roles.display_name, roles.name
                            FROM roles
                            INNER JOIN role_user ON roles.id = role_user.role_id
                            AND role_user.user_id = :idUser', ['idUser' => $idUser]) as $role) {
            $dtoRole = new DtoRole();
            $dtoRole->id = $role->id;
            $dtoRole->name = $role->name;
            $dtoRole->description = $role->display_name;
            $result->push($dtoRole);
        }

        return $result;
    }

    /**
     * Get Role excluded by User
     * 
     * @param int idUser
     * 
     * @return DtoRole
     */
    public static function getRoleExcludedByUser(int $idUser)
    {
        $result = collect();

        foreach (DB::select(
            'SELECT roles.id, roles.display_name FROM roles WHERE roles.name != \'admin\' AND id NOT IN (SELECT role_id FROM role_user WHERE user_id = :idUser)',
            ['idUser' => $idUser]
        ) as $role) {
            $dtoRole = new DtoRole();
            $dtoRole->id = $role->id;
            $dtoRole->description = $role->display_name;
            $result->push($dtoRole);
        }

        return $result;
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validate($request->idUser, $request->idRole, $request->idFunctionality, $idUser, $idLanguage);

        $roleUser = new RoleUser();
        $roleUser->role_id = $request->idRole;
        $roleUser->user_id = $request->idUser;
        $roleUser->user_type = 'App\User';
        $roleUser->save();

        return $roleUser->id;
    }

    #endregion INSERT

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function delete(int $id, int $idFunctionality, int $idUser, int $idLanguage)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $roleUser = RoleUser::find($id);
        $roleUser->delete();
    }

    #endregion DELETE
}

<?php

namespace App\BusinessLogic;

use App\ItemGroupTechnicalSpecification;
use App\DtoModel\DtoItemGroupTechnicalSpecification;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use PHPUnit\Framework\Constraint\Exception;
use App\Item;
use App\GroupTechnicalSpecification;
use Illuminate\Support\Facades\DB;

class ItemGroupTechnicalSpecificationBL
{

    #region PRIVATE

    /**
     * Validate data
     * 
     * @param int idGroupTechnicalSpecification
     * @param int idItem
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    private static function _validateData(int $idGroupTechnicalSpecification, int $idItem, int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (is_null($idFunctionality)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null(Item::find($idItem))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ItemNotFound), HttpResponseException::InvalidPayload);
        }

        if (is_null(GroupTechnicalSpecification::find($idGroupTechnicalSpecification))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupTechnicalSpecificationNotFound), HttpResponseException::InvalidPayload);
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by item and language
     * 
     * @param int $idItem
     * @param int $idLanguage
     * 
     * @return DtoItemGroupTechnicalSpecification
     */
    public static function getByItem(int $idItem, int $idLanguage)
    {
        $result = collect();

        foreach (ItemGroupTechnicalSpecification::where('item_id', $idItem)->get() as $itemGroupTechnicalSpecification) {
            $dtoItemGroupTechnicalSpecification = new DtoItemGroupTechnicalSpecification();
            $dtoItemGroupTechnicalSpecification->idItem = $idItem;
            $dtoItemGroupTechnicalSpecification->descriptionItem = ItemLanguageBL::getByItemAndLanguage($idItem, $idLanguage);
            $dtoItemGroupTechnicalSpecification->idGroupTechnicalSpecification = $itemGroupTechnicalSpecification->group_technical_specification_id;
            $dtoItemGroupTechnicalSpecification->descriptionTechnicalSpecification = GroupTechnicalSpecificationLanguageBL::getByGroupTechnicalSpecificationAndLanguage($itemGroupTechnicalSpecification->group_technical_specification_id, $idLanguage);
            $result->push($dtoItemGroupTechnicalSpecification);
        }

        return $result;
    }

    /**
     * Get by group technical specification and language
     * 
     * @param int $idGroupTechnicalSpecification
     * @param int $idLanguage
     * 
     * @return DtoItemGroupTechnicalSpecification
     */
    public static function getInlucedInGroup(int $idGroupTechnicalSpecification, int $idLanguage)
    {
        $result = collect();

        foreach (ItemGroupTechnicalSpecification::where('group_technical_specification_id', $idGroupTechnicalSpecification)->get() as $itemGroupTechnicalSpecification) {
            $dtoItemGroupTechnicalSpecification = new DtoItemGroupTechnicalSpecification();
            $dtoItemGroupTechnicalSpecification->id = $itemGroupTechnicalSpecification->id;
            $item = ItemBL::getDto($itemGroupTechnicalSpecification->item_id, $idLanguage);
            $dtoItemGroupTechnicalSpecification->internalCodeItem = $item->internalCode;
            $dtoItemGroupTechnicalSpecification->descriptionItem = $item->description;
            $result->push($dtoItemGroupTechnicalSpecification);
        }

        return $result;
    }

    /**
     * Get excluded by group technical specification and language
     * 
     * @param int $idGroupTechnicalSpecification
     * @param int $idLanguage
     * 
     * @return DtoItemGroupTechnicalSpecification
     */
    public static function getExlucedInGroup(int $idGroupTechnicalSpecification, int $idLanguage)
    {
        $result = collect();

        foreach (DB::select('   SELECT *
                                FROM items
                                WHERE id NOT IN (
                                                SELECT item_id
                                                FROM items_groups_technicals_specifications
                                                WHERE group_technical_specification_id = ' . $idGroupTechnicalSpecification . ')')
            as $itemGroupTechnicalSpecification) {
            $dtoItemGroupTechnicalSpecification = new DtoItemGroupTechnicalSpecification();
            $item = ItemBL::getDto($itemGroupTechnicalSpecification->id, $idLanguage);
            $dtoItemGroupTechnicalSpecification->id = $item->id;
            $dtoItemGroupTechnicalSpecification->internalCodeItem = $item->internalCode;
            $dtoItemGroupTechnicalSpecification->descriptionItem = $item->description;
            $result->push($dtoItemGroupTechnicalSpecification);
        }

        return $result;
    }

    /**
     * Get item without group technical specification
     * 
     * @return int
     */
    public static function getItemWithoutGroupTechnicalSpecification()
    {
        return DB::select('SELECT COUNT(*) AS C FROM items WHERE id NOT IN (SELECT item_id FROM items_groups_technicals_specifications)')[0]->c;
    }

    #endregion GET

    #region INSERT

    /**
     * Add technical specification
     * 
     * @param int idGroupTechnicalSpecification
     * @param int idItem
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int id
     */
    public static function insert(int $idGroupTechnicalSpecification, int $idItem, int $idFunctionality, int $idUser, int $idLanguage)
    {
        static::_validateData($idGroupTechnicalSpecification, $idItem, $idFunctionality, $idUser, $idLanguage);

        ItemGroupTechnicalSpecification::where('item_id', $idItem)->delete();

        $itemGroupTechnicalSpecification = new ItemGroupTechnicalSpecification();
        $itemGroupTechnicalSpecification->item_id = $idItem;
        $itemGroupTechnicalSpecification->group_technical_specification_id = $idGroupTechnicalSpecification;
        $itemGroupTechnicalSpecification->save();

        return $itemGroupTechnicalSpecification->id;
    }


    #endregion INSERT

    #region DELETE

    /**
     * Delete
     * 
     * @param int $id
     */
    public static function delete(int $id, int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(ItemGroupTechnicalSpecification::find($id))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ItemGroupTechnicalSpecificationNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        ItemGroupTechnicalSpecification::destroy($id);
    }

    #endregion DELETE
}

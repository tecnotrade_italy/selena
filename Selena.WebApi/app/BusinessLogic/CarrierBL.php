<?php

namespace App\BusinessLogic;

use App\CarrierLanguage;
use App\Carrier;
use App\CarrierArea;
use App\Cart;
use App\DtoModel\DtoSelect2;
use App\DtoModel\DtoCarrier;
use App\DtoModel\DtoCarrierArea;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Mockery\Exception;
use App\Helpers\LogHelper;

class CarrierBL
{
    public static $dirImage = '../storage/app/public/images/';
    #region PRIVATE

    /**
     * Check function enabled
     * 
     * @param int idFunctionlity
     * @param int idUser
     * @param int idLanguage
     */
    private static function _checkFunctionEnabled(int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (is_null($idFunctionality)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Functionality::find($idFunctionality))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/';
        //return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/Items/';
    }

    #endregion PRIVATE

    #region GET

    

    /**
     * Get by customer
     * 
     * @param int idCustomer
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoSelect2
     */
    public static function getByCustomer(int $idCustomer, int $idFunctionality, int $idUser, $idLanguage)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idFunctionality);

        $carrier = DB::select(
            'SELECT carriers.id, carriers_languages.description
                                    FROM carriers
                                    INNER JOIN carriers_languages ON carriers.id = carriers_languages.carrier_id
                                    INNER JOIN customers ON carriers.id = customers.carrier_id
                                    WHERE carriers_languages.language_id = :idLanguage
                                    AND customers.id = :idCustomer',
            ['idLanguage' => $idLanguage, 'idCustomer' => $idCustomer]
        );

        $result = new DtoSelect2();

        if (count($carrier)) {
            $result->id = $carrier[0]->id;
            $result->description = $carrier[0]->description;
        }

        return $result;
    }

    /**
     * Get select
     * 
     * @param string search
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoSelect2
     */
    public static function getSelect(int $idFunctionality, int $idUser,$idLanguage, string $search)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idFunctionality);
        return CarrierLanguage::where('language_id', $idLanguage)->where('description', 'ILIKE', $search . '%')->select('carrier_id as id', 'description')->get();
    }


     /**
     * Get select
     *
     * @param String $search
     * @return CarrierLanguage
     */
    public static function selectCarrier(string $search)
    {
        if ($search == "*") {
            return Carrier::select('id', 'description as description')->get();
        } else {
            return Carrier::where('description', 'ilike', $search . '%')->select('id', 'description as description')->get();
        }
    }


    /**
     * Get all
     * 
     * @param int idUser
     * @param int idLanguage
     * 
     */
    public static function getAll(int $idUser, $idLanguage)
    {
        $carrier = Carrier::all();

        $result = collect();

        foreach($carrier as $carrierAll){
            $dtoCarrier = new DtoCarrier();
            $dtoCarrier->id =  $carrierAll->id;
            $dtoCarrier->name =  $carrierAll->name;
            $dtoCarrier->description =  $carrierAll->description;
            $dtoCarrier->insurance =  $carrierAll->insurance;
            $dtoCarrier->img =  $carrierAll->img;
            $dtoCarrier->order =  $carrierAll->order;
            $dtoCarrier->vat_type_id =  $carrierAll->vat_type_id;
            $dtoCarrier->tracking_url =  $carrierAll->tracking_url;
            $dtoCarrier->nation_id =  $carrierAll->nation_id;
            $dtoCarrier->region_id =  $carrierAll->region_id;
            $dtoCarrier->province_id =  $carrierAll->province_id;
            $dtoCarrier->custom_id =  $carrierAll->custom_id;
            $dtoCarrier->custom_field =  $carrierAll->custom_field;
            $result->push($dtoCarrier);
        }

        return $result;        
    }

    /**
     * Get Carrier Area
     * 
     * @param int idUser
     * @param int idLanguage
     * 
     */
    public static function getCarrierArea(int $id, int $idZone)
    {
        switch ($idZone) {
            case 0:
                $carrierArea = DB::select("SELECT carrier_area.carrier_id, carrier_area.nation_id, carrier_area.region_id, carrier_area.province_id, languages_nations.description
                                   FROM carrier_area
                                   INNER JOIN languages_nations ON languages_nations.nation_id = carrier_area.nation_id
                                   WHERE carrier_area.carrier_id = " . $id . " 
                                   GROUP BY carrier_area.carrier_id, carrier_area.nation_id, carrier_area.region_id, carrier_area.province_id, languages_nations.description");
                break;
            case 1:
                $carrierArea = DB::select("SELECT carrier_area.carrier_id, carrier_area.nation_id, carrier_area.region_id, carrier_area.province_id, region.description 
                                   FROM carrier_area
                                   INNER JOIN region ON region.id = carrier_area.region_id
                                   WHERE carrier_area.carrier_id = " . $id . " 
                                   GROUP BY carrier_area.carrier_id, carrier_area.nation_id, carrier_area.region_id, carrier_area.province_id, region.description");
                break;
            case 2:
                $carrierArea = DB::select("SELECT carrier_area.carrier_id, carrier_area.nation_id, carrier_area.region_id, carrier_area.province_id, languages_provinces.description
                                   FROM carrier_area
                                   INNER JOIN languages_provinces ON languages_provinces.province_id = carrier_area.province_id
                                   WHERE carrier_area.carrier_id = " . $id . " 
                                   GROUP BY carrier_area.carrier_id, carrier_area.nation_id, carrier_area.region_id, carrier_area.province_id, languages_provinces.description");
                break;
        }
        


        $result = collect();
        if(isset($carrierArea)){
            foreach($carrierArea as $carrierAreas){
                $dtoCarrierArea = new DtoCarrierArea();
                if(!is_null($carrierAreas->nation_id)){
                    $dtoCarrierArea->id = $carrierAreas->nation_id;
                }else{
                    if(!is_null($carrierAreas->region_id)){
                        $dtoCarrierArea->id = $carrierAreas->region_id;
                    }else{
                        if(!is_null($carrierAreas->province_id)){
                            $dtoCarrierArea->id = $carrierAreas->province_id;
                        }
                    }
                }
                $dtoCarrierArea->zone = $carrierAreas->description;
                $dtoCarrierArea->carrier_id = $carrierAreas->carrier_id;
                switch ($idZone) {
                    case 0:
                        $carrierAreaDetail = CarrierArea::where('carrier_id', $id)->where('nation_id', $carrierAreas->nation_id)->orderBy('from', 'asc')->get();
                        break;
                    case 1:
                        $carrierAreaDetail = CarrierArea::where('carrier_id', $id)->where('region_id', $carrierAreas->region_id)->orderBy('from', 'asc')->get();
                        break;
                    case 2:
                        $carrierAreaDetail = CarrierArea::where('carrier_id', $id)->where('province_id', $carrierAreas->province_id)->orderBy('from', 'asc')->get();
                        break;
                }
                
                foreach($carrierAreaDetail as $carrierAreaDetails){
                    $dtoCarrierAreaDetail = new DtoCarrierArea();
                    $dtoCarrierAreaDetail->id = $carrierAreaDetails->id;
                    $dtoCarrierAreaDetail->carrier_id = $carrierAreaDetails->carrier_id;
                    $dtoCarrierAreaDetail->nation_id = $carrierAreaDetails->nation_id;
                    $dtoCarrierAreaDetail->region_id = $carrierAreaDetails->region_id;
                    $dtoCarrierAreaDetail->province_id = $carrierAreaDetails->province_id;
                    $dtoCarrierAreaDetail->from = $carrierAreaDetails->from;
                    $dtoCarrierAreaDetail->to = $carrierAreaDetails->to;
                    $dtoCarrierAreaDetail->price = $carrierAreaDetails->price;
                    $dtoCarrierArea->carrierAreaListPrice->push($dtoCarrierAreaDetail);
                }

                $result->push($dtoCarrierArea);
            }
        }

        return $result;        
    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * 
     * @return int
     */
    public static function insert($request, int $idUser)
    {
        DB::beginTransaction();

        try {
            $carrier = new Carrier();

            $carrier->name =  $request->name;
            $carrier->description =  $request->description;
            $carrier->insurance =  $request->insurance;

            if(isset($request->img)){
                if (strpos($request->img, '/storage/app/public/images/') === false) {
                    Image::make($request->img)->save(static::$dirImage . $request->imgName);
                    $carrier->img =  static::getUrl() . $request->imgName;
                }
            }

            $carrier->order =  $request->order;
            $carrier->vat_type_id =  $request->vat_type_id;
            $carrier->tracking_url =  $request->tracking_url;
            $carrier->nation_id =  true;
            $carrier->region_id =  true;
            $carrier->province_id =  true;
            $carrier->custom_id =  true;

            $carrier->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $carrier->id;
    }

    /**
     * insert Carrier Area
     * 
     * @param Request request
     * @param int idUser
     * 
     * @return int
     */
    public static function insertCarrierArea($request, int $idUser)
    {
        DB::beginTransaction();

        try {
            $carrierArea = new CarrierArea();
            $carrierArea->carrier_id = $request->carrierId;
            $carrierArea->{$request->field} = $request->fieldId;
            $carrierArea->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $carrierArea->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function update($request){
        $carrierOnDb = Carrier::find($request->id);
        
        DB::beginTransaction();
        try { 
            $carrierOnDb->name =  $request->name;
            $carrierOnDb->description =  $request->description;
            $carrierOnDb->insurance =  $request->insurance;
            
            if(isset($request->imgName)){
                if (strpos($request->img, '/storage/app/public/images/') === false) {
                    Image::make($request->img)->save(static::$dirImage . $request->imgName);
                    $carrierOnDb->img =  static::getUrl() . $request->imgName;
                }
            }

            $carrierOnDb->order =  $request->order;
            $carrierOnDb->vat_type_id =  $request->vat_type_id;
            $carrierOnDb->tracking_url =  $request->tracking_url;

            $carrierOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    /**
     * update Carrier Range Area
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateCarrierRangeArea($request){
        $carrierAreaOnDb = CarrierArea::find($request->id);
        
        DB::beginTransaction();
        try { 
            $carrierAreaOnDb->from = $request->from;
            $carrierAreaOnDb->to = $request->to;
            $carrierAreaOnDb->price = $request->price;
            $carrierAreaOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {   
        $carrier = Carrier::find($id);


        if (is_null($carrier)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($carrier)) {
            
            DB::beginTransaction();

            try {
                
                //implementare eliminazione nella tabella aggiuntiva di configurazione aree
                CarrierArea::where('carrier_id', $id)->delete();
                //delete tabella carrier per id
                Carrier::where('id', $id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }  
        }

    }

    /**
     * delete Range Detail Area
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function deleteRangeDetailArea(int $id, int $idLanguage)
    {   
        $carrierArea = CarrierArea::find($id);


        if (is_null($carrierArea)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($carrierArea)) {
            
            DB::beginTransaction();

            try {
                
                //delete tabella carrierarea per id
                CarrierArea::where('id', $id)->delete();
                
                
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }  
        }

    }


    /**
     * delete Zone Range Detail Area
     *
     * @param Request $request
     * @param int $idLanguage
     */
    public static function deleteZoneRangeDetailArea($request, int $idLanguage)
    {   
        $carrierArea = CarrierArea::where('carrier_id', $request->id)->get();


        if (is_null($carrierArea)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($carrierArea)) {
            
            DB::beginTransaction();

            try {
                
                //delete tabella carrier per id
                CarrierArea::where('carrier_id', $request->id)->delete();

                $carrier = Carrier::find($request->id);
                $carrier->nation_id = false;
                $carrier->region_id = false;
                $carrier->province_id = false;
                $carrier->custom_id = false;
                $carrier->save();

                $carrier2 = Carrier::find($request->id);
                $carrier2->{$request->zone} = true;
                $carrier2->save();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }  
        }

    }
    #endregion DELETE

    public static function getAllCarrierByCartId($idLanguage, $cartId)
    {
        $result = collect();
        foreach (Carrier::orderBy('code','DESC')->get() as $CarrierAll) {
            $CarrierLanguage = CarrierLanguage::where('carrier_id', $CarrierAll->id)->where('language_id', $idLanguage)->get()->first();
            if(isset($CarrierLanguage)){
                $DtoCarrier = new DtoCarrier();
                $DtoCarrier->id = $CarrierAll->id;
                $DtoCarrier->code = $CarrierAll->code;
                $cart = Cart::where('id', $cartId)->get()->first();
                if(isset($cart)){
                    if($cart->carrier_id == $CarrierAll->id){
                        $DtoCarrier->selected = "selected";
                    }else{
                        $DtoCarrier->selected = "";
                    }
                }
                $DtoCarrier->description = $CarrierLanguage->description;
                $result->push($DtoCarrier); 
            }
        }
        return $result;
    }

}

<?php

namespace App\BusinessLogic;

use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\User;
use App\BookingStates;
use Illuminate\Http\Request;
//use Mockery\Exception;

class BookingStatesBL
{


    /**
     * Get select
     *
     * @param String $search
     * @param int $idUser
     * @return People
     */
    public static function getSelect(string $search)
    {
                if ($search == "*") {
                    return BookingStates::select('id', 'description as description')->get();
                } else {
                    return BookingStates::where('description', 'ilike' , $search . '%')->select('id', 'description as description')->get();
          } 
    }
             
}
  
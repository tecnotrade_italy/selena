<?php

namespace App\BusinessLogic;

use App\TemplateEditorType;

class TemplateEditorTypeBL
{
    #region GET

    /**
     * Get
     *
     * @param int id
     *
     * @return TemplateEditorType
     */
    public static function get(int $id)
    {
        return TemplateEditorType::find($id);
    }

    /**
     * Get by code
     *
     * @param String type
     *
     * @return TemplateEditorType
     */
    public static function getByCode(String $type)
    {
        return TemplateEditorType::where('code', $type)->first();
    }

    /**
     * Get select
     *
     * @param string search
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     *
     * @return DtoSelect2
     */
    public static function getSelect(string $search, int $idFunctionality, int $idUser, $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        if ($search == "*") {
            return TemplateEditorType::select('id', 'description')->get();
        } else {
            return TemplateEditorType::where('description', 'ILIKE', $search . '%')->select('id', 'description')->get();
        }
    }

    #endregion GET
}

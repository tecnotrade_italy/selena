<?php

namespace App\BusinessLogic;

use App\Contact;
use App\DtoModel\DtoNation;
use App\DtoModel\DtoNegotiation;
use App\DtoModel\DtoNegotiationDetail;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Item;
use App\ItemLanguage;
use App\LanguageNation;
use App\Mail\ContactRequestMail;
use App\Mail\NegotiationMail;
use App\Nation;
use App\Negotiation;
use App\NegotiationsDetail;
use App\User;
use App\UsersDatas;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class NegotiationBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if (is_null($request->user_id_shopkeeper)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserShopNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->user_id_negotiator)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNegotiatiorNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::MessageNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    /**
     * Send email negotiation
     * 
     * @param int idUser
     * @param int idNegotiation
     */
    private static function _sendEmailNegotiation(int $idUser, int $idNegotiation)
    {
        $emailFrom = SettingBL::getContactsEmail();
        $user = User::where('id', $idUser)->first();
        $userData = UsersDatas::where('user_id', $idUser)->first();
        if(isset($userData)){
            $userName = $userData->name;
        }else{
            $userName = $user->name;
        }

        $negotiation = Negotiation::where('id', $idNegotiation)->first();
        if(isset($negotiation)){
            $itemDescription = ItemLanguage::where('item_id', $negotiation->item_id)->first()->description;
        }else{
            $itemDescription = $userName;
        }
                
        Mail::to($user->email)->send(new NegotiationMail($emailFrom, $userName, $itemDescription));
    }


    

    #endregion PRIVATE

    #region GET

    /**
     * Get by user
     * 
     * @param int id
     * 
     * @return DtoNegotiation
     */
    public static function getByUser(int $id)
    {
        $result = collect();
        foreach (Negotiation::where('user_id_negotiator', $id)->orWhere('user_id_shopkeeper', $id)->orderBy('created_at')->get() as $negotiationAll) {
            $itemLang = ItemLanguage::where('item_id',  $negotiationAll->item_id)->first();
            if(isset($itemLang)){
                $dtoNegotiation = new DtoNegotiation();
                $dtoNegotiation->id = $negotiationAll->id;
                $dtoNegotiation->item = ItemLanguage::where('item_id',  $negotiationAll->item_id)->first()->description;
                $dtoNegotiation->img = Item::where('id',  $negotiationAll->item_id)->first()->img;
                $dtoNegotiation->link = ItemLanguage::where('item_id',  $negotiationAll->item_id)->first()->link;
                if($negotiationAll->user_id_negotiator != $id){
                    $userId = $negotiationAll->user_id_negotiator;
                }else{
                    $userId = $negotiationAll->user_id_shopkeeper;
                }
                
                $userNegotiatior = UsersDatas::where('user_id', $userId)->first();
                if(isset($userNegotiatior)){
                    $dtoNegotiation->userNegotiatior= $userNegotiatior->name;
                }else{
                    $dtoNegotiation->userNegotiatior = User::where('id', $userId)->first()->name;
                }

                if($negotiationAll->user_id_negotiator == $id){
                    if(is_null($negotiationAll->read_negotiator)){
                        $dtoNegotiation->toRead = false;
                    }else{
                        $dtoNegotiation->toRead = $negotiationAll->read_negotiator;
                    }
                }else{
                    if(is_null($negotiationAll->read_shopkeeper)){
                        $dtoNegotiation->toRead = false;
                    }else{
                        $dtoNegotiation->toRead = $negotiationAll->read_shopkeeper;
                    }
                }
                
                $dtoNegotiation->data = $negotiationAll->created_data;
                
                $result->push($dtoNegotiation); 
            }
        }
        return $result;
    }

    /**
     * Get detail by negotiation id
     * 
     * @param int id
     * @param int idUser
     * 
     * @return DtoNegotiation
     */
    public static function getDetailById(int $id, int $idUser)
    {
        $negotiation = Negotiation::where('id', $id)->first();
        $dtoNegotiationDetail = new DtoNegotiationDetail();

        if($negotiation->user_id_negotiator == $idUser){
            $negotiation->read_negotiator = false; 
        }else{
            $negotiation->read_shopkeeper = false; 
        }
        $negotiation->save();

        if(isset($negotiation)){
            $userNegotiatior = UsersDatas::where('user_id', $negotiation->user_id_negotiator)->first();
            if(isset($userNegotiatior)){
                $txtUserNegotiatior = $userNegotiatior->name;
            }else{
                $txtUserNegotiatior = User::where('id', $negotiation->user_id_negotiator)->first()->name;
            }

            $userShopkeeper = UsersDatas::where('user_id', $negotiation->user_id_shopkeeper)->first();
            if(isset($userShopkeeper)){
                $txtUserShopkeeper = $userShopkeeper->name;
            }else{
                $txtUserShopkeeper = User::where('id', $negotiation->user_id_shopkeeper)->first()->name;
            }

            $dtoNegotiationDetail->id = $id;
            $dtoNegotiationDetail->idUserNegotiatior = $negotiation->user_id_negotiator;
            $dtoNegotiationDetail->txtUserNegotiatior = $txtUserNegotiatior;

            $dtoNegotiationDetail->idUserShopkeeper = $negotiation->user_id_shopkeeper;
            $dtoNegotiationDetail->txtUserShopkeeper = $txtUserShopkeeper;

            $dtoNegotiationDetail->item = ItemLanguage::where('item_id',  $negotiation->item_id)->first()->description;
            $dtoNegotiationDetail->data = $negotiation->created_data;

            foreach (NegotiationsDetail::where('negotiation_id', $id)->orderBy('created_at')->get() as $negotiationAllDetail) {
                $dtoNegotiationMsg = new DtoNegotiationDetail();
                $dtoNegotiationMsg->id = $negotiationAllDetail->id;
                $dtoNegotiationMsg->idUserMsg = $negotiationAllDetail->user_id;
                $dtoNegotiationMsg->description = $negotiationAllDetail->description;
                $dtoNegotiationMsg->data = $negotiationAllDetail->created_data;
                $dtoNegotiationDetail->msg->push($dtoNegotiationMsg);
            }
        }

       return $dtoNegotiationDetail;
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);
        
        DB::beginTransaction();

        try {
            $negotiation = new Negotiation();
            $negotiation->item_id = $request->item_id;
            $negotiation->user_id_shopkeeper = $request->user_id_shopkeeper;
            $negotiation->user_id_negotiator = $request->user_id_negotiator;
            $negotiation->created_id = $request->user_id_negotiator;
            $negotiation->read_shopkeeper = true;
            $negotiation->created_data = Carbon::now()->toDateTimeString();

            $negotiation->save();

            $negotiationDetail = new NegotiationsDetail();
            $negotiationDetail->negotiation_id = $negotiation->id;
            $negotiationDetail->user_id= $request->user_id_negotiator;
            $negotiationDetail->description = $request->description;
            $negotiationDetail->created_id = $request->user_id_negotiator;
            $negotiationDetail->created_data = Carbon::now()->toDateTimeString();

            $negotiationDetail->save();

            /* INVIO EMAIL */
            $user = User::where('id', $request->user_id_shopkeeper)->first();

            static::_sendEmailNegotiation($user->id, $negotiation->id);
            
            /* END INVIO EMAIL */

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    /**
     * Insert detail
     * 
     * @param Request request
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertDetail(Request $request, int $idLanguage)
    {
        //static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);
        DB::beginTransaction();

        try {
            $negotiation = Negotiation::where('id', $request->negotiation_id)->first();
            if($negotiation->user_id_negotiator == $request->user_id){
                $negotiation->read_shopkeeper = true; 
            }else{
                $negotiation->read_negotiator = true; 
            }
            $negotiation->save();

            $negotiationDetail = new NegotiationsDetail();
            $negotiationDetail->negotiation_id = $request->negotiation_id;
            $negotiationDetail->user_id = $request->user_id;
            $negotiationDetail->description = $request->description;
            $negotiationDetail->created_id = $request->user_id;
            $negotiationDetail->created_data = Carbon::now()->toDateTimeString();

            $negotiationDetail->save();

            /* INVIO EMAIL */

            if($negotiation->user_id_negotiator == $request->user_id){
                $user = User::where('id', $negotiation->user_id_shopkeeper)->first();
            }else{
                $user = User::where('id', $negotiation->user_id_negotiator)->first();
            }
            
            static::_sendEmailNegotiation($user->id, $request->negotiation_id);
            
            /* END INVIO EMAIL */

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $request->negotiation_id;
    }

    #endregion INSERT

    #region UPDATE

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $negotiation = Negotiation::find($id);


        if (is_null($negotiation)) {
            throw new Exception('Trattativa non trovata', HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($negotiation)) {

            DB::beginTransaction();

            try {

                NegotiationsDetail::where('negotiation_id', $id)->delete();
                Negotiation::where('id', $id)->delete();
                
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }

    #endregion DELETE
}

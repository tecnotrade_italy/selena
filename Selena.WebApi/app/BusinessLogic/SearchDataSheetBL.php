<?php

namespace App\BusinessLogic;

use App\Content;
use App\ContentLanguage;
use App\UsersDatas;
use App\ItemLanguage;
use App\CategoryItem;
use App\DtoModel\DtoNation;
use App\DtoModel\DtoSelenaViews;
use App\DtoModel\DtoTagTemplate;
use App\DtoModel\DtoPagination;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\SelenaSqlViews;
use App\BusinessLogic\SettingBL;
use App\BusinessLogic\OffertaTrovaPrezziBL;
use App\Enums\SettingEnum;
use App\Helpers\HttpHelper;
use App\Item;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchDataSheetBL
{

    /**
     * Search implemented
     * 
     * @param Request request
     * 
     */
    public static function searchDataSheet(Request $request)
    {
        $content = '';
        $finalHtml = '';
        $strWhere = " AND items.online = True AND categories.online = True ";
        $strOrderBy = "";
        $idLanguageContent = $request->idLanguage; 
        $contentCode = Content::where('code', 'Items')->first();
        $contentLanguage = ContentLanguage::where('content_id', $contentCode->id)->where('language_id', $idLanguageContent)->first();
        foreach($request->request as $key => $value){
            if($request->{$key} != ""){
                if($key != 'idLanguage'){
                    $operator = ' = ';
                    $val = $request->{$key};
                    $keyNew = $key;
                    if($key == 'description'){
                        $keyNew = 'items_languages.description';
                        $operator = ' ilike ';
                        $val = '\'%' . $request->{$key} . '%\'';
                    }else{
                        if($key == 'categories_id'){
                            $keyNew = 'categories_languages.id';
                            $operator = ' = ';
                            $val = $request->{$key};
                        }else{
                            if($key == 'producer_id'){
                                $keyNew = 'items.producer_id';
                                $operator = ' = ';
                                $val = $request->{$key};
                            }else{
                                $operator = ' = ';
                                $val = $request->{$key};
                            }
                        }
                    }
                    if($request->{$key} == "on"){
                        $keyNew = $key;
                        $val = "true";
                    }
                    $strWhere = $strWhere . ' AND ' . $keyNew . $operator . $val;
                }
            }
        }

        if($request->orderby != ''){
            $strOrderBy = " ORDER BY " . $request->orderby;
        }
        


        $tags = SelenaViewsBL::getTagByType('Items');
        foreach (DB::select(
            'SELECT items.*, items_languages.*
            FROM items
            INNER JOIN items_languages ON items.id = items_languages.item_id
            INNER JOIN categories_items ON items.id = categories_items.item_id
            INNER JOIN categories ON categories_items.category_id  = categories.id
            INNER JOIN categories_languages ON categories_items.category_id = categories_languages.category_id
            AND items_languages.language_id = :idLanguage ' . $strWhere . $strOrderBy,
            ['idLanguage' => $idLanguageContent]
        ) as $items){
            $newContent = $contentLanguage->content;
            foreach ($tags as $fieldsDb){
                if(isset($fieldsDb->tag)){
                    if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                        if(is_numeric($items->{$cleanTag})){
                            $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                        }else{
                            $tag = $items->{$cleanTag};
                        }
                        $newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                    }
                }
            }
            $finalHtml = $finalHtml . $newContent;
        };
        $content = $finalHtml;
        if($content == ""){
            $content = '<div class="col-12 div-no-info"><h1>Nessuna informazione trovata</h1></div>';
        }
        return $content;
    }
    #endregion Search implemented

 
       /**
     * Search header
     * 
     * @param Request request
     * 
     */
    public static function searchPaginatorDataSheet(Request $request)
    {
        
        $strLimit = "";
        $content = '';
        $idLanguageContent = $request->idLanguage; 
        $contentCode = Content::where('code', 'Items')->first();
        $contentLanguage = ContentLanguage::where('content_id', $contentCode->id)->where('language_id', $idLanguageContent)->first();
        
        $strWhereDescription = " AND items.online = True AND categories.online = True ";
        $strSqlPagination = "";

       $description = $request->description; 
       if ($description != "") {
            $strWhereDescription = ' AND items_languages.description ilike \'%' . $description . '%\'';
        } 

        $producer_id = $request->producer_id;
         if ($producer_id != "") {
            $strWhereDescription = $strWhereDescription . ' AND items.producer_id = ' . $producer_id . '';
        }

         $categories_id = $request->categories_id;
         if ($categories_id != "") {
            $strWhereDescription = $strWhereDescription . ' AND categories_languages.id = ' . $categories_id . '';
        }
        
            $strOrderBy = "";
            $finalHtml = '';
            $tags = SelenaViewsBL::getTagByType('Items');
            $itemsPerPage =  SettingBL::getByCode(SettingEnum::NumberOfItemsPerPage);

            $strSqlPagination = "";
            $pageSelected = $request->paginator;

            if($request->autocomplete){
                $strSqlPagination = " limit 10 ";
            }

            if($request->orderby != ''){
                $strOrderBy = " ORDER BY " . $request->orderby . " ";
            }

            $totItems = DB::select(
                'SELECT count(items.id) as cont
                FROM items
                INNER JOIN items_languages ON items.id = items_languages.item_id
                INNER JOIN categories_items ON items.id = categories_items.item_id
                INNER JOIN categories ON categories_items.category_id  = categories.id
                INNER JOIN categories_languages ON categories_items.category_id = categories_languages.category_id
                AND items_languages.language_id = :idLanguage' . $strWhereDescription  . $strSqlPagination,
                ['idLanguage' => $idLanguageContent]);
            
            if($totItems[0]->cont <= $itemsPerPage){
                $strSqlPagination = "";
            }else{
                $limit = $itemsPerPage;
                $offset = $itemsPerPage * ($pageSelected - 1);

                $strSqlPagination = " limit " . $limit . " offset " . $offset;
            }
    
            if($request->autocomplete){
                $strSqlPagination = " limit 10 ";
            }

            foreach (DB::select(
                'SELECT items.*, items_languages.*
                FROM items
                INNER JOIN items_languages ON items.id = items_languages.item_id
                INNER JOIN categories_items ON items.id = categories_items.item_id
                INNER JOIN categories ON categories_items.category_id  = categories.id
                INNER JOIN categories_languages ON categories_items.category_id = categories_languages.category_id
                AND items_languages.language_id = :idLanguage' . $strWhereDescription . $strOrderBy . $strSqlPagination,
                ['idLanguage' => $idLanguageContent]
            ) as $items){
                $newContent = $contentLanguage->content;
                    foreach ($tags as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                                 
                            if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($items->{$cleanTag}) && strpos($items->{$cleanTag},'.')){
                                    if($fieldsDb->tag == '#enabled_from#'){
                                        $tag = date_format(date_create($items->{$cleanTag}), 'd/m/Y');
                                    }else{
                                        $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                    }
                                }else{
                                    if($fieldsDb->tag == '#enabled_from#'){
                                        $tag = date_format(date_create($items->{$cleanTag}), 'd/m/Y');
                                    }else{
                                        $tag = $items->{$cleanTag};
                                    }
                                }

                                $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);
                                
                                

                            }
                        }
                    }

                    if (strpos($newContent, '#enabled_from#') !== false) {
                        $newContent = str_replace('#enabled_from#', date_format(date_create($items->publish), 'd/m/Y'),  $newContent);
                    }


                    if (strpos($newContent, '[miglior prezzo') !== false) {
                        $newContent = str_replace('&quot;','"', $newContent);
                        //mi memorizzo in un array tutte le posizioni dove ho un box
                        $indices = [];
                        
                        for($pos = strpos($newContent,'[miglior prezzo'); $pos !== false; $pos = strpos($newContent, '[miglior prezzo', $pos + 1)) {
                            //$indices.push($pos);
                            array_push($indices, $pos);
                        }
                        $ind = 0;
                        $lengthIndices = count($indices);
                        for($ind = $lengthIndices - 1; $ind>=0;  $ind--){
                            //se ho lo stesso shortcode riputo tante volte, dopo la prima non ne ho più e non devo più ciclare
                            if (strpos($newContent, '[miglior prezzo') === false){
                                break;
                            } 
                
                            $inizio = $indices[$ind];
                            
                            $fine = strpos($newContent, ']', $inizio);
                            
                            $stringaMigliorPrezzo = '';
                            
                            $stringaMigliorPrezzo = substr($newContent, $inizio, $fine-$inizio+1);
                            
                            //se esco e stringaAmazon = "" significa che mi sono scordato di chiudere la parentesi quadra.
                            //Lascio tutto così ed esco dal ciclo while...altrimenti va in loop infinito
                            if ($stringaMigliorPrezzo == ''){
                                break;
                            }
            
                            $descriptionItem = str_replace('[miglior prezzo ', '',  $stringaMigliorPrezzo);
                            $descriptionItem = str_replace(']', '',  $descriptionItem);
            
                            $itemLanguageOnDb = ItemLanguage::where('description', $descriptionItem)->get()->first();
                            $categoryItemOnDb = CategoryItem::where('item_id', $itemLanguageOnDb->item_id)->get()->first();
                            if ($categoryItemOnDb->category_id>=11 && $categoryItemOnDb->category_id<=15)
                                $migliorOfferta = OffertaTrovaPrezziBL::migliorOfferta("cameranationit", $itemLanguageOnDb->description, "5");
                            else if ($categoryItemOnDb->category_id==16)
                                $migliorOfferta = OffertaTrovaPrezziBL::migliorOfferta("cameranationit", $itemLanguageOnDb->description, "20124");
                            else if ($categoryItemOnDb->category_id==17)
                                $migliorOfferta = OffertaTrovaPrezziBL::migliorOfferta("cameranationit", $itemLanguageOnDb->description, "7");
                            else
                                $migliorOfferta = OffertaTrovaPrezziBL::migliorOfferta("cameranationit", $itemLanguageOnDb->description, "0");
            
                            if ($migliorOfferta->price=='' || $migliorOfferta->price==0)
                                $htmlFinale = '<h4>Prezzo non disponibile</h4>';
                            else{
                                $euro = explode(',', $migliorOfferta->price);
                                if (count($euro)==1) 
                                    $htmlFinale = '<a target="_blank" rel="nofollow" href="'. $migliorOfferta->url . '"><h2>'. $euro[0] .'<span>,00 €</span></h2><h4>Miglior Prezzo</h4></a>';
                                else
                                    $htmlFinale = '<a target="_blank" rel="nofollow" href="'. $migliorOfferta->url . '"><h2>'. $euro[0] .'<span>,' . $euro[1] . ' €</span></h2><h4>Miglior Prezzo</h4></a>';
                                }

                            $newContent = str_replace($stringaMigliorPrezzo, $htmlFinale,  $newContent);
            
                        }
                    }


                $finalHtml = $finalHtml . $newContent;
            };
            
            if($finalHtml == ""){
                $finalHtml = '<div class="col-12 div-no-info"><h1>Nessuna informazione trovata</h1></div>';
            }

            $contentDto = new DtoPagination();
            $contentDto->pageSelected = $pageSelected;
            $contentDto->totItems = $totItems[0]->cont;
            $contentDto->itemsPerPage = $itemsPerPage;
            if($totItems[0]->cont <= $itemsPerPage){
                $contentDto->paginationActive = false;
                $contentDto->totalPages = "1";
            }else{
                $contentDto->paginationActive = true;
                $contentDto->totalPages = ceil($totItems[0]->cont / $itemsPerPage);
            }
            $contentDto->html = $finalHtml;
            
            $content = $contentDto;
            
            return $content;
        }
}

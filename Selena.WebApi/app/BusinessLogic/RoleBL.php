<?php

namespace App\BusinessLogic;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoRole;
use App\DtoModel\DtoUser;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\LogHelper;
use App\User;
use App\Role;
use App\RoleUser;
use App\RolesPeople;
use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;


class RoleBL
{
    #region PRIVATE

    /**
     * Check function enabled
     * 
     * @param int idFunctionlity
     * @param int idUser
     * @param int idLanguage
     */
    private static function _checkFunctionEnabled(int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (is_null($idFunctionality)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Functionality::find($idFunctionality))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by User
     * 
     * @param int idUserBy
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoRole
     */
    public static function getByUser(int $idUserBy, int $idFunctionality, int $idUser, int $idLanguage)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $result = new DtoIncludedExcluded();
        $result->included = RoleUserBL::getRoleIncludedByUser($idUserBy);
        $result->excluded = RoleUserBL::getRoleExcludedByUser($idUserBy);
        return $result;
    }

    #endregion GET

 /**
     * Get by User
     * 
     * @param int idUserBy
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoRole
     */
    public static function getAllByRole(request $request)
    {

         $result = collect();
        foreach (Role::orderBy('id')->get() as $role) {
            $RoleUser = RoleUser::where('user_id', $request->rowSelected)->where('role_id', $role->id)->get()->first();

            $dtoRole = new DtoRole();
            if (isset($RoleUser)){
                $dtoRole->check = true;       
            }else{
                $dtoRole->check = false;     
            }

            $dtoRole->id = $role->id;
            $dtoRole->name = $role->name;
            $dtoRole->description = $role->display_name;
            $result->push($dtoRole); 
        }
            return $result;
    }

    /**
     * getUserName
     * 
     * @param int idUserBy
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoRole
     */
    public static function getUserName(request $request)
    {
        $result = collect();

            $User = User::where('id', $request->rowSelected)->first();
            $dtoUsers = new DtoUser();
            $dtoUsers->nameUser = $User->name;
            $result->push($dtoUsers);

        return $result;


    }


    public static function insertRole(request $request){

             DB::beginTransaction();
                try {
                 $RoleUser = new RoleUser();
                 $RoleUser->user_id = $request->user_id;
                 $RoleUser->role_id = $request->idRoleUser;  
                 $RoleUser->user_type = 'App\User';
                 $RoleUser->save();   
                    
                DB::commit();
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
    }

      /**
     * delRoleUser Categories
     *
     * @param int $idCategories
     * @param int $idItem
     * @param int $idLanguage
     */
    public static function delRoleUser(int $idRoleUser, int $user_id, int $idLanguage)
    {
        if (is_null($user_id)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, 'Utente Non Trovato'), HttpResultsCodesEnum::InvalidPayload);
        }
        $RoleUser = RoleUser::where('role_id', $idRoleUser)->where('user_id', $user_id);
        $RoleUser->delete();
    }

    #endregion DELETE


     #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request DtoRole
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoRole, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoRole->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Role::find($DtoRole->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($DtoRole->name)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoRole->display_name)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoRole->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    /**
     * Convert to dto
     * 
     * @param Role
     * 
     * @return DtoRole
     */
    private static function _convertToDto($DtoRole)
    {
        $Roles = new DtoRole();

        if (isset($Roles->id)) {
            $DtoRole->id = $Roles->id;
        }

        if (isset($Roles->name)) {
            $DtoRole->name = $Roles->name;
        }

        if (isset($Roles->display_name)) {
            $DtoRole->display_name = $Roles->display_name;
        }

        if (isset($Roles->description)) {
            $DtoRole->description = $Roles->description;
        }


        return $DtoRole;
    }


    /**
     * Convert to VatType
     * 
     * @param DtoRole DtoRole
     * 
     * @return Role
     */
    private static function _convertToModel($DtoRole)
    {
        $Roles = new Role();

        if (isset($DtoRole->id)) {
            $Roles->id = $DtoRole->id;
        }

        if (isset($DtoRole->name)) {
            $Roles->name = $DtoRole->name;
        }

        if (isset($DtoRole->display_name)) {
            $Roles->display_name = $DtoRole->display_name;
        }
        if (isset($DtoRole->description)) {
            $Roles->description = $DtoRole->description;
        }
        return $Roles;
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoRole
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Role::find($id));
    }


    /**
     * Get by id
     * 
     * @param int id
     * @return DtoRole
     */
    public static function getByIdRoles(int $id)
    {
        return static::_convertToDtoRole(Role::find($id));
    }

     /**
     * _convertToDtoRole
     * 
     * @param Role
     * 
     * @return DtoRole
     */
    private static function _convertToDtoRole($DtoRole)
    {
        $Role = new DtoRole();

        if (isset($Role->id)) {
            $DtoRole->id = $Role->id;
        }

        if (isset($Role->name)) {
            $DtoRole->name = $Role->name;
        }

        if (isset($Role->display_name)) {
            $DtoRole->display_name = $Role->display_name;
        }

        if (isset($Role->description)) {
            $DtoRole->description = $Role->description;
        }

        return $DtoRole;
    }


    /**
     * Get select
     *
     * @param String $search
     * @return Roles
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return Role::where('name','<>','admin')->select('id', 'name as description')->get();
        } else {
            return Role::where('name', 'ilike', $search . '%')->where('name','<>','admin')->select('id', 'name as description')->get();
        }
    }


    

    /**
     * Get all
     * 
     * @return DtoRole
     */
    public static function getAllWithoutAdmin()
    {
        $result = collect();

        foreach (Role::where('name','<>','admin')->orderBy('id','DESC')->get() as $Role) {
            
            $DtoRole = new DtoRole();

                //valori tabella 
                if (isset($Role->id)) {
                    $DtoRole->id = $Role->id;
                }
                if (isset($Role->name)) {
                    $DtoRole->name = $Role->name;
                }

                if (isset($Role->display_name)) {
                    $DtoRole->display_name = $Role->display_name;
                }

                //end valori tabella 


                //valori dettagli

                if (isset($Role->id)) {
                    $DtoRole->id = $Role->id;
                }

                if (isset($Role->name)) {
                    $DtoRole->name = $Role->name;
                }  
                if (isset($Role->display_name)) {
                    $DtoRole->display_name = $Role->display_name;
                }  

                if (isset($Role->description)) {
                    $DtoRole->description = $Role->description;
                }  
                
                    $result->push($DtoRole);
            }

        return $result;

        }

    /**
     * Get all
     * 
     * @return DtoRole
     */
    public static function getAll()
    {
        $result = collect();

        foreach (Role::orderBy('id','DESC')->get() as $Role) {
            
            $DtoRole = new DtoRole();

                //valori tabella 
                if (isset($Role->id)) {
                    $DtoRole->id = $Role->id;
                }
                if (isset($Role->name)) {
                    $DtoRole->name = $Role->name;
                }

                if (isset($Role->display_name)) {
                    $DtoRole->display_name = $Role->display_name;
                }

                //end valori tabella 


                //valori dettagli

                if (isset($Role->id)) {
                    $DtoRole->id = $Role->id;
                }

                if (isset($Role->name)) {
                    $DtoRole->name = $Role->name;
                }  
                if (isset($Role->display_name)) {
                    $DtoRole->display_name = $Role->display_name;
                }  

                if (isset($Role->description)) {
                    $DtoRole->description = $Role->description;
                }  
                
                    $result->push($DtoRole);
                }

                return $result;

                }

    #endregion GET

   /**
     * getAllNoAuth
     * 
     * @return DtoRole
     */
    public static function getAllNoAuth()
    {
        $result = collect();

        //foreach (Role::where('language_id',$idLanguage)->get() as $Role) {

        foreach (Role::orderBy('id','DESC')->get() as $Role) {
            
            $DtoRole = new DtoRole();
     
                if (isset($Role->id)) {
                    $DtoRole->id = $Role->id;
                }
                if (isset($Role->name)) {
                    $DtoRole->name = $Role->name;
                }  
                if (isset($Role->display_name)) {
                    $DtoRole->display_name = $Role->display_name;
                }  

                if (isset($Role->description)) {
                    $DtoRole->description = $Role->description;
                }                  
                    $result->push($DtoRole);
                }
                return $result;

                }

    #endregion getAllNoAuth

    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoRole
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoRole, int $idUser)
    {
        static::_validateData($DtoRole, $idUser, DbOperationsTypesEnum::INSERT);
        //$Roles = static::_convertToModel($DtoRole);
       // $Roles->save();
       // return $Roles->id;


 try {
            $Roles = new Role();
            //$Roles->id = $DtoRole->id;
            $Roles->name = $DtoRole->name;
            $Roles->display_name = $DtoRole->display_name;
            $Roles->description = $DtoRole->description;
            $Roles->created_id =$DtoRole->$idUser;
            $Roles->save();
         
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $Roles->id;
    }

    #endregion INSERT


  #region INSERT

    /**
     * insertNoAuth
     * 
     * @param Request DtoRole
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertNoAuth(Request $DtoRole, int $idUser)
    {

 try {
            $Roles = new Role();
            //$Roles->id = $DtoRole->id;
            $Roles->name = $DtoRole->name;
            $Roles->display_name = $DtoRole->display_name;
            $Roles->description = $DtoRole->description;
            $Roles->created_id =$DtoRole->$idUser;
            $Roles->save();
         
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $Roles->id;
    }

    #endregion insertNoAuth

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoRole
     * @param int idLanguage
     */
    public static function update(Request $DtoRole, int $idLanguage)
    {
        static::_validateData($DtoRole, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $RolesOnDb = Role::find($DtoRole->id);
        //DBHelper::updateAndSave(static::_convertToModel($DtoRole), $RolesOnDb);

 
        DB::beginTransaction();

        try {
            $RolesOnDb->id = $DtoRole->id;
            $RolesOnDb->name = $DtoRole->name;
            $RolesOnDb->display_name = $DtoRole->display_name;
            $RolesOnDb->description = $DtoRole->description;

            $RolesOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

 

/**
     * updateNoAuth
     * 
     * @param Request DtoRole
     * @param int idLanguage
     */
    public static function updateNoAuth(Request $DtoRole, int $idLanguage)
    {
        $RolesOnDb = Role::find($DtoRole->id);

        DB::beginTransaction();

        try {
            $RolesOnDb->id = $DtoRole->id;
            $RolesOnDb->name = $DtoRole->name;
            $RolesOnDb->display_name = $DtoRole->display_name;
            $RolesOnDb->description = $DtoRole->description;

            $RolesOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion updateNoAuth

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $Roles = Role::find($id);
        $RolesPeople= RolesPeople::where('roles_id',$Roles->id)->first();

        if (is_null($Roles)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $RolesPeople->delete();
        $Roles->delete();
    }

}

<?php

namespace App\BusinessLogic;

use App\Content;
use App\ContentLanguage;
use App\CorrelationPeopleUser;
use App\DtoModel\DtoInterventions;
use App\DtoModel\DtoItemGroupTechnicalSpecification;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use App\Customer;
use App\DtoModel\DtoInterventionsPhotos;
use App\DtoModel\DtoInterventionsProcessing;
use App\DtoModel\DtoItemGr;
use App\DtoModel\DtoPeople;
use App\DtoModel\DtoQuotes;
use App\DtoModel\DtoQuotesMessages;
use App\DtoModel\DtoUser;
use App\Item;
use App\GrStates;
use App\GrIntervention;
use App\GrInterventionPhoto;
use App\GrInterventionProcessing;
use App\GrQuotes;
use App\GrQuotesEmailSend;
use App\Helpers\HttpHelper;
use App\ItemGr;
use App\ItemLanguage;
use App\Mail\DynamicMail;
use App\PaymentGr;
use App\People;
use App\PeopleUser;
use App\QuotesGrIntervention;
use App\QuotesMessages;
use App\Role;
use App\RolesPeople;
use App\RoleUser;
use App\SelenaSqlViews;
use App\Setting;
use App\StatesQuotesGr;
use App\User;
use App\UsersDatas;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use DateTime;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class QuotesViewBL
{
    /**
     * Get by getViewPdfQuotes
     * 
     * @param int id
     * @return DtoInterventions
     */
    public static function getViewPdfQuotes(int $id)
    {       
    
       if($id->has('download')){
        $pdf = PDF::loadView('pdfview');
        return $pdf->download('pdfview.pdf');
        }
        return view('pdfview');
    }

   /**
     * Get by getByViewQuotes
     * 
     * @param int id
     * @return DtoInterventions
     */
    public static function getByViewQuotes(int $id, $idUser)
    {
        $idIntervention= GrIntervention::where('id', $id)->first();
        $DtoQuotes = new DtoQuotes();
        $DtoQuotes->id = $id;

      /*  if (isset($idIntervention->user_id)) {
            $DtoQuotes->id_customer= UsersDatas::where('user_id', $idIntervention->user_id)->first()->business_name;
        }else{
            $DtoQuotes->id_customer = '';  
        }*/

         if (isset($idIntervention->user_id)) {
            $User = UsersDatas::where('user_id', $idIntervention->user_id)->first();
        if (isset($User->business_name)){
            $DtoQuotes->id_customer = $User->business_name;
            $DtoQuotes->id_user_quotes = $idIntervention->user_id;
          }else{
              $DtoQuotes->id_customer =  $User->name . ' ' . $User->surname; 
              $DtoQuotes->id_user_quotes = $idIntervention->user_id;
          }
        } else {
            $DtoQuotes->id_customer = '';
        }

        if (isset($idIntervention->code_intervention_gr)) {
            $DtoQuotes->code_intervention_gr = $idIntervention->code_intervention_gr;
        }else{
            $DtoQuotes->code_intervention_gr = '';
        }
        
        if (isset ($idIntervention->referent)){
            $DtoQuotes->referent = People::where('id', $idIntervention->referent)->first()->name;
        }else{
            $DtoQuotes->referent= '';  
        }

        /*if (isset ($idIntervention->description)){
            $DtoQuotes->description = $idIntervention->description;
        }else{
            $DtoQuotes->description= '';  
        }*/


        if (isset ($idIntervention->date_ddt)){
            $DtoQuotes->date_ddt = $idIntervention->date_ddt;
        }else{
            $DtoQuotes->date_ddt= '';  
        }

        if (isset ($idIntervention->nr_ddt)){
            $DtoQuotes->nr_ddt = $idIntervention->nr_ddt;
        }else{
            $DtoQuotes->nr_ddt= '';  
        }

        foreach (PeopleUser::where('user_id', $idIntervention->user_id)->get() as $PeopleUser) {
            if (isset($PeopleUser->people_id)) {
                $Roles= Role::where('name', 'Preventivi')->get()->first();
                $RoleUserPeople = RolesPeople::where('roles_id', $Roles->id)->where('people_id', $PeopleUser->people_id)->get();
                foreach ($RoleUserPeople as $RoleUserPeoples) {
                    $DtoPeople = new DtoPeople();
                    $DtoPeople->user_id = $PeopleUser->user_id;
                    $DtoPeople->people_id = $RoleUserPeoples->people_id;
                    $DtoPeople->EmailReferente = People::where('id', $RoleUserPeoples->people_id)->get()->first()->email;
                    $DtoQuotes->ReferentEmailSendPreventivi->push($DtoPeople);
                }
            }                        
        } 
 
        if (isset ($idIntervention->tot_final)){
            $DtoQuotes->tot_quote = $idIntervention->tot_final;
        }else{
                $DtoQuotes->tot_quote ='';
             } 

        foreach (GrInterventionProcessing::where('id_intervention', $id)->get() as $InterventionProcessing) {  
            //$DtoQuotes->description = $InterventionProcessing->description;
           // if (isset ($InterventionProcessing->id_gr_items)){
               // $DtoQuotes->code_gr = ItemGr::where('id', $InterventionProcessing->id_gr_items)->first()->code_gr;
            //}  
            $DtoInterventionProcessing = new DtoInterventionsProcessing();
            if (isset ($InterventionProcessing->id_gr_items)){
                $DtoInterventionProcessing->code_gr = ItemGr::where('id', $InterventionProcessing->id_gr_items)->first()->code_gr;
            } 
            $DtoInterventionProcessing->description = $InterventionProcessing->description;
            $DtoInterventionProcessing->qta = $InterventionProcessing->qta;
            $DtoQuotes->Articles->push($DtoInterventionProcessing);  
        }

            $IdQuotes = GrQuotes::where('id_intervention', $id)->first();
            //$DtoQuotes = new DtoQuotes();
            if (isset ($IdQuotes->email)){
                $DtoQuotes->email = $IdQuotes->email;
            }
            if (isset ($IdQuotes->object)){
                $DtoQuotes->object = $IdQuotes->object;
            }

            if (isset($IdQuotes->date_agg)) {
            $timestamp = strtotime($IdQuotes->date_agg);
                if ($timestamp === FALSE) {
                    $DtoQuotes->date_agg = $IdQuotes->date_agg;
                }else{
                    $DtoQuotes->date_agg = date('d/m/Y', $timestamp);
                }
                }else {
                    $dt = new DateTime();
                    $DtoQuotes->date_agg = $dt->format('d-m-Y'); 
            }

            if (isset ($IdQuotes->note_before_the_quote)){
                $DtoQuotes->note_before_the_quote = $IdQuotes->note_before_the_quote;   
            }else{
                $DtoQuotes->note_before_the_quote =  "In riferimento al Vs DDT n. " . $idIntervention->nr_ddt . "  del " . $idIntervention->date_ddt .  " con la presente siamo ad inviarVi il seguente preventivo di riparazione relativo a: ";   
            }
    

             if (isset($IdQuotes->discount)) {
                $DtoQuotes->discount = $IdQuotes->discount;
            }else{
                $DtoQuotes->discount ='';
             } 
            if (isset($IdQuotes->discount)&&($idIntervention->tot_final)) { 
                $DtoQuotes->total_amount_with_discount = $idIntervention->tot_final - $IdQuotes->discount;
            }else{
                $DtoQuotes->total_amount_with_discount ='';
             }  
           
            if (isset($IdQuotes->description_payment)){
                $DtoQuotes->description_payment = $IdQuotes->description_payment;
                $DtoQuotes->Description_payment = PaymentGr::where('id', $IdQuotes->description_payment)->first()->description;
            }else{      
                $PaymentsUsersDatas = UsersDatas::where('user_id', $idIntervention->user_id)->first();
                if (isset($PaymentsUsersDatas)){
                    $GrPayments= PaymentGr::where('id', $PaymentsUsersDatas->id_payments)->first();
                }
    
                if (isset($GrPayments)){
                    $DtoQuotes->description_payment = $PaymentsUsersDatas->id_payments;
                    $DtoQuotes->Description_payment = $GrPayments->description;
                }else{
                    $DtoQuotes->description_payment = '';
                    $DtoQuotes->Description_payment = '';
                }
            } 

            if (isset ($IdQuotes->id_states_quote)){
                $DtoQuotes->id_states_quote = $IdQuotes->id_states_quote;
                $DtoQuotes->States_Description = StatesQuotesGr::where('id', $IdQuotes->id_states_quote)->first()->description;
            }
            if (isset ($IdQuotes->note)){
                $DtoQuotes->note = $IdQuotes->note;
            }
             if (isset ($IdQuotes->note_customer)){
                $DtoQuotes->note_customer = $IdQuotes->note_customer;
            }
            if (isset ($IdQuotes->id_intervention)){
                $DtoQuotes->id = $IdQuotes->id_intervention;
            }
            if (isset ($IdQuotes->updated_at)){
                $DtoQuotes->updated_at = date_format($IdQuotes->updated_at, 'd/m/Y');
            }else{
                $DtoQuotes->updated_at = '';
            }
            if (isset ($IdQuotes->description_and_items_agg)){
                $DtoQuotes->description_and_items_agg = $IdQuotes->description_and_items_agg;
            }
       //  $User = User::where('id', $IdQuotes->created_id)->first();
           /* if (isset($IdQuotes->created_id)) {
                $DtoQuotes->created_id = User::where('id', $IdQuotes->created_id)->first()->name;
            }else{
                $DtoQuotes->created_id = null;
            } */

            if (isset($IdQuotes->created_id)) {
                $DtoQuotes->created_id = UsersDatas::where('user_id', $IdQuotes->created_id)->first()->name;
            }else{
                $DtoQuotes->created_id = UsersDatas::where('user_id', $idUser)->first()->name;
            } 

       
            if (isset ($IdQuotes->kind_attention)){
                $DtoQuotes->kind_attention = $IdQuotes->kind_attention;
            }else{
                $DtoQuotes->kind_attention ='';
             }  
             
             $messages = QuotesMessages::where('id_intervention', $IdQuotes->id_intervention)->get();
             if (isset($messages)) {
                 foreach ($messages as $MessageDetail) {
                     $DtoQuotesMessages= new DtoQuotesMessages();
                     $DtoQuotesMessages->id = $MessageDetail->id;
     
                     if (isset($MessageDetail->id_sender)) { 
                         $DtoQuotesMessages->id_sender_id = $MessageDetail->id_sender;
                         $DtoQuotesMessages->id_sender = User::where('id', $MessageDetail->id_sender)->first()->name;
                     }else{
                         $DtoQuotesMessages->id_sender = '';  
                     }
                     if (isset($MessageDetail->id_receiver)) {  
                        $DtoQuotesMessages->id_receiver_id = $MessageDetail->id_receiver;
                         $DtoQuotesMessages->id_receiver =  User::where('id', $MessageDetail->id_receiver)->first()->name;
                     }else{  
                         $DtoQuotesMessages->id_receiver = '';  
                     }
                     if (isset($IdQuotes->created_id)) {
                        $DtoQuotesMessages->created_quotes_user_id = $IdQuotes->created_id;
                    }else{
                        $DtoQuotesMessages->created_quotes_user_id = $idUser;
                    } 

                    $UserRole = RoleUser::where('user_id', $idUser)->where('role_id', 3)->get()->first();
                    if (isset($UserRole)){
                        $DtoQuotesMessages->id_user_id_quotes = $UserRole->user_id;
                    }
                    
                     
                    
                     $DtoQuotesMessages->messages = $MessageDetail->message;
                     $DtoQuotesMessages->id_intervention = $MessageDetail->id_intervention;
                     $DtoQuotesMessages->created_at = date_format($MessageDetail->created_at, 'd/m/Y h:m:s');
                     $DtoQuotes->Messages->push($DtoQuotesMessages);
              }
         }
 
        return $DtoQuotes;
    }
    

    
    public static function insertReferent(Request $request) { 
        DB::beginTransaction();
      
        try {
            $People = new People();
            
        if (isset($request->name)) {
                $People->name = $request->name;
            }  

        if (isset($request->surname)) {
                $People->surname = $request->surname;
            } 
        if (isset($request->email)) {
                $People->email = $request->email;
            } 
            if (isset($request->phone_number)) {
                $People->phone_number = $request->phone_number;
            }   
            if (isset($idUserConnection)) {
                $People->created_id = $idUserConnection;
            }  
            if ($request->ref == 'Esterno'){
                $People->type_referent = 'Esterno';
            }

            if ($request->ref == 'Interno'){
                $People->type_referent = 'Interno';
            }
            $People->save();

            $PeopleUser = new PeopleUser();
            $PeopleUser->user_id = $request->id_user;        
            if (isset($People->id)) {
                $PeopleUser->people_id = $People->id;
            }
            if (isset($idUser)) {
                $PeopleUser->created_id = $idUser;
            } 
            $PeopleUser->save();
 
             $RolesPeople = new RolesPeople();   
              if (isset($People->id)) {   
             $RolesPeople->people_id = $People->id;
            }   
             $RolesPeople->roles_id = Role::where('name', 'Preventivi')->get()->first()->id;

            if (isset($idUser)) {
            $RolesPeople ->created_id = $idUser;
            } 
            if (isset($idUser)) {
             $RolesPeople ->created_at = $idUser;
            } 
            $RolesPeople->save();

            $user = new User();
            $user->name = $request->email;
            $user->password = bcrypt($request->password);
            $user->email = $request->email;
            $user->language_id =1;
            $user->online = 'true';
            $user->confirmed = 'true';
            $user->save();

             $UsersData = new UsersDatas();
             $UsersData->user_id = $user->id;
             $UsersData->language_id = 1; 
            if (isset($request->name)) {    
                $UsersData->name = $request->name;
            }else{
              $UsersData->name = '-';
            }  
            if (isset($request->surname)) {
                $UsersData->surname = $request->surname;
            }else{
              $UsersData->surname = '-';
            } 
            if (isset($request->phone_number)) {
                $UsersData->telephone_number = $request->phone_number;
            } 
            $UsersData->save(); 

            $userRole = new RoleUser();
            $userRole->role_id = $RolesPeople->roles_id;
            $userRole->user_id = $user->id;
            $userRole->user_type = 'App\User';
            $userRole->save();
            
            $CorrelationPeopleUser = new CorrelationPeopleUser();
            $CorrelationPeopleUser->id_user = $user->id;
            $CorrelationPeopleUser->id_people = $People->id;  
            $CorrelationPeopleUser->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
            return $People->id;

    }


    public static function insertMessageQuotes(Request $request)
    { 

        DB::beginTransaction();
 
         try {
             $QuotesMessages= new QuotesMessages();

            if (isset($request->id_sender)) {
                $QuotesMessages->id_sender = $request->id_sender;
            } 
            if (isset($request->id_intervention)) {
                $QuotesMessages->id_intervention = $request->id_intervention;
            } 
            if (isset($request->messages)) {
                $QuotesMessages->message = $request->messages;
            } 
           
            $grquotes= GrQuotes::where('id_intervention', $request->id_intervention)->first();
            if (isset($grquotes)) {
                if (isset($grquotes->code_intervention_gr)) {
                    $QuotesMessages->id_code_intervention_gr_librone = $grquotes->code_intervention_gr;     
                }   
            }
            $GrIntervention= GrIntervention::where('id', $request->id_intervention)->first();
            if (isset($GrIntervention)) {
                if (isset($GrIntervention->user_id)) {
                    $QuotesMessages->id_receiver = $GrIntervention->user_id;  
                }   
            }


            $QuotesMessages->save();

            $user = User::where('id', $QuotesMessages->id_receiver)->first(); 
            $id_intervention = $request->id_intervention;
            $id_receiver = User::where('id',  $QuotesMessages->id_receiver)->first()->name;
            $id_sender =  User::where('id', $request->id_sender)->first()->name;
            $created_at = QuotesMessages::where('message', $request->messages)->first()->created_at;
            $message = $request->messages;
    
                $emailFrom = SettingBL::getContactsEmail();

                    $textMail= MessageBL::getByCode('ORDER_CONFIRM_MESSAGE_MAIL_TEXT');
                    $textObject= MessageBL::getByCode('ORDER_CONFIRM_MESSAGE_MAIL_OBJECT');
                    
                    $textObject =str_replace ('#id_sender#', $id_sender, $textObject); 
                    $textMail =str_replace ('#id_intervention#', $id_intervention, $textMail); 
                    $textMail =str_replace ('#id_receiver#', $id_receiver, $textMail); 
                    $textMail =str_replace ('#id_sender#', $id_sender, $textMail); 
                    $textMail =str_replace ('#created_at#', $created_at, $textMail); 
                    $textMail =str_replace ('#message#', $message, $textMail); 
    
                    Mail::to($user->email)->send(new DynamicMail($textObject,$textMail));
      
             DB::commit();
         } catch (Exception $ex) {
             DB::rollback();
             throw $ex;
         }
 
         return $QuotesMessages->id;
     }



    

    public static function SearchQuotes($request)
    {    
        $result = collect();
        $content = '';
        $finalHtml = '';
        $strWhere = "";

        $code_intervention_gr = $request->code_intervention_gr;
        $ClienteSearch = $request->cliente_quotes;
        $StatesSearch = $request->states_quotes;
        
        if ($ClienteSearch != "") {
          $strWhere = $strWhere . ' OR c.user_id = ' . $ClienteSearch . '';
        }
        if ($StatesSearch != "") {
            $strWhere = $strWhere . ' OR d.id_states_quote = ' . $StatesSearch . '';
        }

        foreach (DB::select(
            'select c.id, d.code_intervention_gr,c.user_id, d.created_at,c.description,d.note,d.id_states_quote,d.date_agg
            from gr_quotes d
            inner join gr_interventions c on c.id = d.id_intervention 
            WHERE d.code_intervention_gr = :code_intervention_gr ' . $strWhere,
        ['code_intervention_gr' => $code_intervention_gr],
        ) as $InterventionAll) {
            $CustomerDescription = UsersDatas::where('user_id', $InterventionAll->user_id)->first();
            // $CustomerDescription = User::where('id', $InterventionAll->user_id)->first();
             $InterventionQuotes = GrQuotes::where('id_intervention', $InterventionAll->id)->first();
             $DtoInterventions = new DtoInterventions();
 
             if (isset($InterventionAll->id)) {
                 $DtoInterventions->idIntervention = $InterventionAll->id;
                 $DtoInterventions->id = $InterventionAll->id;
             }
             if (isset($InterventionAll->code_intervention_gr)) {
                 $DtoInterventions->code_intervention_gr = $InterventionAll->code_intervention_gr;
             }
 
             if (isset($CustomerDescription->business_name)) {
                 $DtoInterventions->descriptionCustomer = $CustomerDescription->business_name;
             }else{
                 $DtoInterventions->descriptionCustomer = '';
             }
             /*if (isset($InterventionQuotes->created_at)) {
                 $DtoInterventions->date_aggs = date_format($InterventionQuotes->created_at, 'd/m/Y');
             }else{
                 $DtoInterventions->date_aggs = '';
             }*/

             if (isset($InterventionAll->date_agg)) {
                $DtoInterventions->date_aggs = $InterventionAll->date_agg;
            }else{
                $DtoInterventions->date_aggs = '';
            }

 
           /*  if (isset($InterventionQuotes->date_agg)) {
                 $DtoInterventions->date_aggs = $InterventionQuotes->date_agg;
             }else{
                 $DtoInterventions->date_aggs = '';
             }*/
             if (isset($InterventionAll->description)) {
                 $DtoInterventions->description = $InterventionAll->description;            
             }else{
                 $DtoInterventions->description = '';
             }
 
             if (isset($InterventionQuotes->note)) {
                 $DtoInterventions->note = $InterventionQuotes->note;
             }else{
                 $DtoInterventions->note = '';
             }
             $CreateChecksendMailSi = "Si";
             $CreateChecksendMailNo = "No";
 
             if ($InterventionQuotes->checksendmail == "true") {
                 $DtoInterventions->checksendmail = $CreateChecksendMailSi;
             } else {
                 $DtoInterventions->checksendmail = $CreateChecksendMailNo;
             }
             
             if (isset($InterventionQuotes->id_states_quote)) {
                 $DtoInterventions->id_states_quote = $InterventionQuotes->id_states_quote;
                 $DtoInterventions->States_Description = StatesQuotesGr::where('id', $InterventionQuotes->id_states_quote)->first()->description;
             }else{
                 $DtoInterventions->States_Description = '';
             }        
             $result->push($DtoInterventions);
        }
            return $result;  
    }


/**
     * getAllProcessingView
     * 
     * @return DtoInterventionProcessing
     * @return DtoIntervention
     */
    public static function getAllViewQuote($idLanguage){       
        $result = collect();

        //$escape="true";
      /*  SELECT c.id, c.name
                FROM carriers c
                WHERE c.id NOT IN (SELECT carrier_id FROM carts_rules_carriers WHERE cart_rule_id = :id)'.$strWhere,
        foreach (GrIntervention::orderBy('id','DESC')->take(50)->get() as $InterventionAll) {*/

            foreach (DB::select(
           "select c.id, d.code_intervention_gr,c.user_id, d.created_at,c.description,d.note,d.id_states_quote,d.date_agg
                from gr_quotes d
                inner join gr_interventions c on c.id = d.id_intervention 
                where d.id not in (select id from gr_interventions where id=d.id_intervention)
                order by d.code_intervention_gr desc limit 100"
            ) as $InterventionAll) {

            $CustomerDescription = UsersDatas::where('user_id', $InterventionAll->user_id)->first();

           // $CustomerDescription = User::where('id', $InterventionAll->user_id)->first();
            $InterventionQuotes = GrQuotes::where('id_intervention', $InterventionAll->id)->first();
  
            $DtoInterventions = new DtoInterventions();

            if (isset($InterventionAll->id)) {
                $DtoInterventions->idIntervention = $InterventionAll->id;
                $DtoInterventions->id = $InterventionAll->id;
            }
            if (isset($InterventionAll->code_intervention_gr)) {
                $DtoInterventions->code_intervention_gr = $InterventionAll->code_intervention_gr;
            }

            if (isset($CustomerDescription->business_name)) {
                $DtoInterventions->descriptionCustomer = $CustomerDescription->business_name;
            }else{
                $DtoInterventions->descriptionCustomer = '';
            }
           /* if (isset($InterventionQuotes->created_at)) {
                $DtoInterventions->date_aggs = date_format($InterventionQuotes->created_at, 'd/m/Y');
            }else{
                $DtoInterventions->date_aggs = '';
            }*/

            if (isset($InterventionAll->date_agg)) {
                $DtoInterventions->date_aggs = $InterventionAll->date_agg;
            }else{
                $DtoInterventions->date_aggs = '';
            }


            if (isset($InterventionAll->description)) {
                $DtoInterventions->description = $InterventionAll->description;            
            }else{
                $DtoInterventions->description = '';
            }

            if (isset($InterventionQuotes->note)) {
                $DtoInterventions->note = $InterventionQuotes->note;
            }else{
                $DtoInterventions->note = '';
            }

            $CreateChecksendMailSi = "Si";
            $CreateChecksendMailNo = "No";

            if ($InterventionQuotes->checksendmail == "true") {
                $DtoInterventions->checksendmail = $CreateChecksendMailSi;
            } else {
                $DtoInterventions->checksendmail = $CreateChecksendMailNo;
            }
            
            if (isset($InterventionQuotes->id_states_quote)) {
                $DtoInterventions->id_states_quote = $InterventionQuotes->id_states_quote;
                $DtoInterventions->States_Description = StatesQuotesGr::where('id', $InterventionQuotes->id_states_quote)->first()->description;
            }else{
                $DtoInterventions->States_Description = '';
            }        
            $result->push($DtoInterventions);
        }
        return $result;  
    }
                

  //updateInterventionUpdateViewQuotes
  public static function updateInterventionUpdateViewQuotes(Request $DtoQuotes){


    $IdQuotes = GrQuotes::where('id_intervention', $DtoQuotes->id)->first();
    DB::beginTransaction();
    try {
            if(!isset($IdQuotes->id_intervention)){
                $RowQuotes = new GrQuotes();
                $RowQuotes->id_intervention =  $DtoQuotes->id;
                $RowQuotes->email = $DtoQuotes->email; 
                $RowQuotes->description_payment = $DtoQuotes->description_payment; 
                $RowQuotes->id_states_quote = $DtoQuotes->id_states_quote; 
                $RowQuotes->code_intervention_gr = $DtoQuotes->code_intervention_gr; 
                $RowQuotes->note = $DtoQuotes->note; 
                $RowQuotes->note_before_the_quote = $DtoQuotes->note_before_the_quote;
                $RowQuotes->object = $DtoQuotes->object;
                $RowQuotes->tot_quote = $DtoQuotes->tot_quote;
                $RowQuotes->date_agg = $DtoQuotes->date_agg;
                $RowQuotes->total_amount_with_discount = $DtoQuotes->total_amount_with_discount;
                $RowQuotes->discount = $DtoQuotes->discount;
                $RowQuotes->kind_attention = $DtoQuotes->kind_attention;
                $RowQuotes->description_and_items_agg = $DtoQuotes->description_and_items_agg;
                $RowQuotes->created_id = $DtoQuotes->updated_id;
                $RowQuotes->save();

                $IdGrQuotesEmailSend = GrQuotesEmailSend::where('id_quotes', $DtoQuotes->id)->get();
                if (isset($IdGrQuotesEmailSend)) {
                    GrQuotesEmailSend::where('id_quotes', $DtoQuotes->id)->delete();
                }
                    foreach ($DtoQuotes->AggOptions as $res) {
                        if (!is_null($res['people_id'])){
                                $RowQuotesEmailSend = new GrQuotesEmailSend();
                                $RowQuotesEmailSend->email = $res['emailreferente'];
                                $RowQuotesEmailSend->id_people = $res['people_id'];
                                $RowQuotesEmailSend->id_user = $res['user_id'];
                                $RowQuotesEmailSend->id_quotes = $DtoQuotes->id;
                                $RowQuotesEmailSend->save();
                        }
                    }

            }else{
            if(isset($IdQuotes->id)) {
                $IdQuotes->id_intervention = $DtoQuotes->id;             
                $IdQuotes->email = $DtoQuotes->email;             
                $IdQuotes->note = $DtoQuotes->note; 
                $IdQuotes->note_customer = $DtoQuotes->note_customer;                             
                $IdQuotes->id_states_quote = $DtoQuotes->id_states_quote;
                $IdQuotes->description_payment = $DtoQuotes->description_payment;
                $IdQuotes->note_before_the_quote = $DtoQuotes->note_before_the_quote;
                $IdQuotes->code_intervention_gr = $DtoQuotes->code_intervention_gr; 
                $IdQuotes->object = $DtoQuotes->object; 
                $IdQuotes->tot_quote = $DtoQuotes->tot_quote; 
                $IdQuotes->total_amount_with_discount = $DtoQuotes->total_amount_with_discount;
                $IdQuotes->discount = $DtoQuotes->discount;
                $IdQuotes->date_agg = $DtoQuotes->date_agg;
                $IdQuotes->kind_attention = $DtoQuotes->kind_attention;
                $IdQuotes->description_and_items_agg = $DtoQuotes->description_and_items_agg;
                $IdQuotes->created_id = $DtoQuotes->updated_id;
                $IdQuotes->updated_id = $DtoQuotes->updated_id;
                $IdQuotes->save();

                $IdGrQuotesEmailSend = GrQuotesEmailSend::where('id_quotes', $DtoQuotes->id)->get();
                if (isset($IdGrQuotesEmailSend)) {
                    GrQuotesEmailSend::where('id_quotes', $DtoQuotes->id)->delete();
                }
                    foreach ($DtoQuotes->AggOptions as $res) {
                        if (!is_null($res['people_id'])){
                                $RowQuotesEmailSend = new GrQuotesEmailSend();
                                $RowQuotesEmailSend->email = $res['emailreferente'];
                                $RowQuotesEmailSend->id_people = $res['people_id'];
                                $RowQuotesEmailSend->id_user = $res['user_id'];
                                $RowQuotesEmailSend->id_quotes = $DtoQuotes->id;
                                $RowQuotesEmailSend->save();
                        }
                    }

              }
            }   
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
}
 #region updateInterventionUpdateViewQuotes



 //updateandSendEmailQuotes
  public static function updateandSendEmailQuotes(Request $DtoQuotes){

    $IdQuotes = GrQuotes::where('id_intervention', $DtoQuotes->id)->first();
    DB::beginTransaction();
    try {
            if(!isset($IdQuotes->id_intervention)){
                $RowQuotes = new GrQuotes();
                $RowQuotes->id_intervention =  $DtoQuotes->id;
                $RowQuotes->email = $DtoQuotes->email; 
                $RowQuotes->description_payment = $DtoQuotes->description_payment; 
                $RowQuotes->id_states_quote = $DtoQuotes->id_states_quote; 
                $RowQuotes->note = $DtoQuotes->note; 
                $RowQuotes->note_before_the_quote = $DtoQuotes->note_before_the_quote;
                $RowQuotes->object = $DtoQuotes->object;
                $RowQuotes->tot_quote = $DtoQuotes->tot_quote;
                $RowQuotes->date_agg = $DtoQuotes->date_agg;
                $RowQuotes->total_amount_with_discount = $DtoQuotes->total_amount_with_discount;
                $RowQuotes->discount = $DtoQuotes->discount;
                $RowQuotes->kind_attention = $DtoQuotes->kind_attention;
                $RowQuotes->description_and_items_agg = $DtoQuotes->description_and_items_agg;
                $RowQuotes->created_id = $DtoQuotes->updated_id;
                $RowQuotes->save();
                $IdGrQuotesEmailSend = GrQuotesEmailSend::where('id_quotes', $DtoQuotes->id)->get();
                if (isset($IdGrQuotesEmailSend)) {
                    GrQuotesEmailSend::where('id_quotes', $DtoQuotes->id)->delete();
                }
                    foreach ($DtoQuotes->AggOptions as $res) {
                        if (!is_null($res['people_id'])){
                                $RowQuotesEmailSend = new GrQuotesEmailSend();
                                $RowQuotesEmailSend->email = $res['emailreferente'];
                                $RowQuotesEmailSend->id_people = $res['people_id'];
                                $RowQuotesEmailSend->id_user = $res['user_id'];
                                $RowQuotesEmailSend->id_quotes = $DtoQuotes->id;
                                $RowQuotesEmailSend->save();
                        }
                    }

            }else{
            if(isset($IdQuotes->id)) {
                $IdQuotes->id_intervention = $DtoQuotes->id;             
                $IdQuotes->email = $DtoQuotes->email;             
                $IdQuotes->note = $DtoQuotes->note; 
                $IdQuotes->note_customer = $DtoQuotes->note_customer;                             
                $IdQuotes->id_states_quote = $DtoQuotes->id_states_quote;
                $IdQuotes->description_payment = $DtoQuotes->description_payment;
                $IdQuotes->note_before_the_quote = $DtoQuotes->note_before_the_quote;
                $IdQuotes->object = $DtoQuotes->object; 
                $IdQuotes->tot_quote = $DtoQuotes->tot_quote; 
                $IdQuotes->total_amount_with_discount = $DtoQuotes->total_amount_with_discount;
                $IdQuotes->discount = $DtoQuotes->discount;
                $IdQuotes->date_agg = $DtoQuotes->date_agg;
                $IdQuotes->kind_attention = $DtoQuotes->kind_attention;
                $IdQuotes->description_and_items_agg = $DtoQuotes->description_and_items_agg;
                $IdQuotes->updated_id = $DtoQuotes->updated_id;
                $IdQuotes->save();

                $IdGrQuotesEmailSend = GrQuotesEmailSend::where('id_quotes', $DtoQuotes->id)->get();
                if (isset($IdGrQuotesEmailSend)) {
                    GrQuotesEmailSend::where('id_quotes', $DtoQuotes->id)->delete();
                }
                    foreach ($DtoQuotes->AggOptions as $res) {
                        if (!is_null($res['people_id'])){
                                $RowQuotesEmailSend = new GrQuotesEmailSend();
                                $RowQuotesEmailSend->email = $res['emailreferente'];
                                $RowQuotesEmailSend->id_people = $res['people_id'];
                                $RowQuotesEmailSend->id_user = $res['user_id'];
                                $RowQuotesEmailSend->id_quotes = $DtoQuotes->id;
                                $RowQuotesEmailSend->save();
                        }
                    }

              }
            }  

            $Content= Content::where('code', 'ResultEmailGrQuotes')->get()->first();
            $contentLanguage = ContentLanguage::where('content_id', $Content->id)->where('language_id', HttpHelper::getLanguageId())->get()->first(); 
            $htmlFinale = "";

            $DtoInterventionProcessing = new DtoInterventionsProcessing;
            $gr_intervention_processing = GrInterventionProcessing::where('id_intervention', $DtoQuotes->id)->get();
            foreach ($gr_intervention_processing as $gr_intervention_processings) {
                $newContentConnectionsResult = $contentLanguage->content;
                if (isset($gr_intervention_processing)){
                    $DtoInterventionProcessings = new DtoInterventionsProcessing;
                    $itemsgr= ItemGr::where('id', $gr_intervention_processings->id_gr_items)->first();
                    if (isset($itemsgr)){
                        $DtoInterventionProcessings->code_gr = $itemsgr->code_gr;
                    }else{
                        $DtoInterventionProcessings->code_gr = '';
                    }
                    $DtoInterventionProcessings->description = $gr_intervention_processings->description;
                    $DtoInterventionProcessings->n_u = $gr_intervention_processings->n_u;
                    $DtoInterventionProcessings->qta = $gr_intervention_processings->qta;
                    $DtoInterventionProcessings->price_list = $gr_intervention_processings->price_list;
                    $DtoInterventionProcessings->tot = $gr_intervention_processings->tot;
                 }else{
                     $DtoInterventionProcessings = new DtoInterventionsProcessing;
                     $DtoInterventionProcessings->code_gr = '';
                     $DtoInterventionProcessings->description = '';
                     $DtoInterventionProcessings->n_u = '';
                     $DtoInterventionProcessings->qta = '';
                     $DtoInterventionProcessings->price_list = '';
                     $DtoInterventionProcessings->tot = '';
                 }
                     $DtoInterventionProcessing->arrayresult->push($DtoInterventionProcessings);  

                        if (isset($gr_intervention_processings->description)){
                            $newContentConnectionsResult = str_replace('#description#', $gr_intervention_processings->description,  $newContentConnectionsResult); 
                        }else{
                            $newContentConnectionsResult = str_replace('#description#', '',  $newContentConnectionsResult);           
                        }
                        if (isset($gr_intervention_processings->id_gr_items)){
                            $newContentConnectionsResult = str_replace('#code_gr#', $itemsgr,  $newContentConnectionsResult); 
                        }else{
                            $newContentConnectionsResult = str_replace('#code_gr#', '',  $newContentConnectionsResult);           
                        }
                        $htmlFinale = $htmlFinale . $newContentConnectionsResult;
            }
               
                if (isset($DtoQuotes->total_amount_with_discount)){
                    $totale =  ' Totale Scontato:' . $DtoQuotes->total_amount_with_discount;
                }else{
                    $totale = '';          
                }
                if (isset($DtoQuotes->discount)){
                        $sconto = ' Sconto:' . $DtoQuotes->discount;
                }else{
                        $sconto = '';          
                }

                $pdfprev =  'https://grsrl.net/api/api/pdfviewquotes/'. $DtoQuotes->id .'';
                $GrPayments= PaymentGr::where('id', $DtoQuotes->description_payment)->first()->description;
                $GrIntervention=GrIntervention::where('id',$DtoQuotes->id )->first();
                $User= UsersDatas::where('user_id', $GrIntervention->user_id)->first();

                //EMAIL-CLIENTE 
                $emailFrom = SettingBL::getContactsEmail();
                $textMail= MessageBL::getByCode('CONFIRM_MAIL_CUSTOMER_QUOTES_TEXT', $DtoQuotes->idLanguage);       
                $textObject= MessageBL::getByCode('CONFIRM_MAIL_CUSTOMER_QUOTES_OBJECT', $DtoQuotes->idLanguage);

                $textObject =str_replace ('#date_agg#', $DtoQuotes->date_agg, $textObject);  
                $textObject = str_replace ('#name#', $User->business_name, $textObject);  
                $textMail = str_replace ('#business_name#', $User->business_name, $textMail);  
                $textMail= str_replace ('#id#', $DtoQuotes->id, $textMail);  
               
                $textMail =str_replace ('#resultcodeedescr#', $htmlFinale, $textMail);
                $textMail =str_replace ('#email#', $DtoQuotes->email, $textMail);
                $textMail =str_replace ('#description_payment#', $GrPayments, $textMail);
                $textMail =str_replace ('#id_states_quote#', $DtoQuotes->id_states_quote, $textMail);
                $textMail =str_replace ('#note#', $DtoQuotes->note, $textMail);
                $textMail =str_replace ('#note_before_the_quote#', $DtoQuotes->note_before_the_quote, $textMail);
                $textMail =str_replace ('#object#', $DtoQuotes->object, $textMail);
                $textMail =str_replace ('#tot_quote#', $DtoQuotes->tot_quote, $textMail);
                $textMail =str_replace ('#date_agg#', $DtoQuotes->date_agg, $textMail);
                $textMail =str_replace ('#total_amount_with_discount#', $totale, $textMail);
                $textMail =str_replace ('#discount#', $sconto, $textMail);
                $textMail =str_replace ('#kind_attention#', $DtoQuotes->kind_attention, $textMail);
                $textMail =str_replace ('#description_and_items_agg#', $DtoQuotes->description_and_items_agg, $textMail);
                $textMail =str_replace ('#pdfprev#', $pdfprev, $textMail);

                foreach ($DtoQuotes->AggOptions as $res) {
                    Mail::to($res['emailreferente'])->send(new DynamicMail($textObject,$textMail));  
                      
                } 

                //EMAIL-NOTIFICA A GR INVIO AL CLIENTE MAIL RIEPILOGO PREVENTIVO 
                $emailFrom = SettingBL::getContactsEmail();
                $textMail= MessageBL::getByCode('CONFIRM_MAIL_INFORMATION_GR_QUOTES_TEXT', $DtoQuotes->idLanguage);       
                $textObject= MessageBL::getByCode('CONFIRM_MAIL_INFORMATION_GR_QUOTES_OBJECT', $DtoQuotes->idLanguage );
                $textObject =str_replace ('#id#', $DtoQuotes->id, $textObject);  
                $textObject =str_replace ('#email#', $DtoQuotes->email, $textObject);
                $textObject =str_replace ('#name#', $User->business_name, $textObject);
                $textMail =str_replace ('#name#', $User->business_name, $textMail);  
                $textMail =str_replace ('#id#', $DtoQuotes->id, $textMail);  
                $textMail =str_replace ('#email#', $DtoQuotes->email, $textMail);
                $textMail =str_replace ('#description_and_items_agg#', $DtoQuotes->description_and_items_agg, $textMail);

                $textMail =str_replace ('#resultcodeedescr#', $htmlFinale, $textMail);
                $textMail =str_replace ('#description_payment#', $GrPayments, $textMail);
                $textMail =str_replace ('#id_states_quote#', $DtoQuotes->id_states_quote, $textMail);
                $textMail =str_replace ('#note#', $DtoQuotes->note, $textMail);
                $textMail =str_replace ('#note_before_the_quote#', $DtoQuotes->note_before_the_quote, $textMail);
                $textMail =str_replace ('#object#', $DtoQuotes->object, $textMail);
                $textMail =str_replace ('#tot_quote#', $DtoQuotes->tot_quote, $textMail);
                $textMail =str_replace ('#date_agg#', $DtoQuotes->date_agg, $textMail);
                $textMail =str_replace ('#total_amount_with_discount#', $totale, $textMail);
                $textMail =str_replace ('#discount#', $sconto, $textMail);
                $textMail =str_replace ('#kind_attention#', $DtoQuotes->kind_attention, $textMail);

                $mailsendRiep = User::where('id', $IdQuotes->updated_id)->first();
                Mail::to($mailsendRiep->email)->send(new DynamicMail($textObject,$textMail));

                $IdQuotes->checksendmail = true;
                $IdQuotes->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
}
 #region updateandSendEmailQuotes


 
public static function sendAccess(Request $DtoQuotes)
{

    DB::beginTransaction();
    try {
    $User = User::where('language_id', 1)->get(); 

    foreach ($User as $Users) {
        $name = $Users->name;
        if (isset($Users->email)){
        $email = $Users->email; 
             if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
              $password = $Users->password;

            $UsersDatas = UsersDatas::where('user_id', $Users->id)->get()->first(); 
            if (isset($UsersDatas)){
                if (isset($UsersDatas->business_name)){
                    $business_name = $UsersDatas->business_name;
                }else{
                    $business_name = '';    
                }
                if (isset($UsersDatas->name)){
                    $name = $UsersDatas->name;
                }else{
                    $name = '';    
                }
                if (isset($UsersDatas->surname)){
                    $surname = $UsersDatas->surname;
                }else{
                    $surname = '';    
                }

            }else{
                $business_name = '';
                $name = '';
                $surname = '';
            }
            
            //INVIO EMAIL A TUTTI GLI UTENTI 
            $emailFrom = SettingBL::getContactsEmail();
            $textMail= MessageBL::getByCode('SEND_EMAIL_ACCOUNT_USERS_GR', 1);       
            $textObject= 'Credenziali di accesso riservate';

            $textObject = str_replace ('#business_name#', $business_name, $textObject); 
            $textMail =str_replace ('#email#', $email, $textMail);
             $textMail =str_replace ('#name#', $name, $textMail);
            $textMail =str_replace ('#password#', 'GRSRL', $textMail);
            LogHelper::debug($email);
            Mail::to($email)->send(new DynamicMail($textObject,$textMail));          
        }
    }
    }
  
       } catch (\Throwable $ex) {
           LogHelper::error('Errore invio Email\n\n' . $ex);
        }
}

 #region DELETE
    /**
     * Delete
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {   
        $Intervention = GrIntervention::find($id);
        $IdGrQuotes = GrQuotes::where('id_intervention', $Intervention->id);

        if (is_null($IdGrQuotes)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($IdGrQuotes)) {
            
            DB::beginTransaction();

            try {           
                GrQuotes::where('id_intervention', $Intervention->id)->delete();
                
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }  
        }

    }




}

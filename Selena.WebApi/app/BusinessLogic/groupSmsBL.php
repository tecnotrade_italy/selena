<?php

namespace App\BusinessLogic;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoSendSms;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\sendSms;
use App\ItemGroupTechnicalSpecification;
use App\RecipientSms;
use App\GroupSms;
use App\Contact;
use App\GroupNewsletter;
use App\UsersDatas;
use Intervention\Image\Facades\Image;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
//use Mockery\Exception;

class groupSmsBL
{
#region GET

    /**
     * insert SMS
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertGroupSms(Request $request)
    {

        DB::beginTransaction();

        try {
            $GroupSms = new GroupSms();
            
            if (isset($request->description)) {
                $GroupSms->description = $request->description;
            }

            $GroupSms->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $GroupSms->id;
    }

    #region GET
    /**
     * Get all
     * 
     * @return DtoItems
     */
    public static function getAll()
    {
        $result = collect();

        foreach (GroupSms::all() as $Sms) {
            $DtoSendSms = new DtoSendSms();    
            if (isset($Sms->id)) {
                $DtoSendSms->id = $Sms->id;
            }

            if (isset($Sms->description)) {
                $DtoSendSms->description = $Sms->description;
            }
                        
            $result->push($DtoSendSms);
        }

        return $result;
    }

    /**
     * Get
     *
     * @param String $search
     * @return GroupSms
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return GroupNewsletter::select('id', 'description','css')->all();
        } else {
            return GroupNewsletter::where('description', 'ilike','%'. $search . '%')->select('id', 'description')->get();
        }
    }

    #endregion GET

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoGroup
     * @param int idLanguage
     */
    public static function updateGroupSms(Request $dtoGroup, int $idLanguage)
    {
        //static::_validateData($dtoGroup, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $groupSms = GroupSms::find($dtoGroup->id);
        
        DB::beginTransaction();

        try {
            $groupSms->description = $dtoGroup->description;
            $groupSms->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {

        $sms = GroupSms::find($id);

        if (is_null($sms)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        DB::beginTransaction();

        try {
            $sms->delete();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return '';
    }

    #endregion DELETE


}
       
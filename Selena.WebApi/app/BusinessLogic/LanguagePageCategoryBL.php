<?php

namespace App\BusinessLogic;

use Illuminate\Support\Facades\DB;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use App\LanguagePageCategory;
use App\PagePageCategory;
use App\PageCategoryFather;
use App\PageCategory;
use App\DtoModel\DtoLanguagePageCategory;
use App\DtoModel\DtoCategories;

class LanguagePageCategoryBL
{
    #region GET

    /**
     * Get
     *
     * @param String $search
     * @return LanguagePageCategory
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return LanguagePageCategory::select('page_category_id AS id', 'description')->all();
        } else {
            return LanguagePageCategory::where('description', 'ilike', '%' . $search . '%')->select('page_category_id AS id', 'description')->get();
        }
    }

    /**
     * Get by id
     *
     * @param int $id
     * @return LanguagePageCategory
     */
    public static function getById(int $id)
    {
        return LanguagePageCategory::find($id);
    }

    /**
     * Get all by page id
     *
     * @param int $id
     * @return LanguagePageCategory
     */
    public static function getAllByPageId(int $id)
    {
        $result = collect();
        foreach (LanguagePageCategory::orderBy('languages_page_categories.description','ASC')->get() as $pageCategory) {
            $dtoPageCategory = new DtoLanguagePageCategory();
            $dtoPageCategory->id = $pageCategory->page_category_id;
            $dtoPageCategory->description = $pageCategory->description;

            $pagePageCategory = PagePageCategory::where('page_category_id', $pageCategory->page_category_id)->where('page_id', $id)->get()->first();

            if(isset($pagePageCategory)){
                $dtoPageCategory->active = true;
            }else{
                $dtoPageCategory->active = false;
            }
            
            $result->push($dtoPageCategory);
        }
        return $result;
    }

    /**
     * Get by idPageCategory and idLanguage
     *
     * @param int idPageCategory
     * @param int idLanguage
     * 
     * @return LanguagePageCategory
     */
    public static function getByIdAndLanguage(int $idPageCategory, int $idLanguage)
    {
        return LanguagePageCategory::where('page_category_id', $idPageCategory)->where('language_id', $idLanguage)->first();
    }

    /**
     * Get idPageCategory by description and language
     * 
     * @param string description
     * @param int idLanguage
     * 
     * @return int
     */
    public static function getIdPageCategoryByDescriptionAndLanguage(string $description, int $idLanguage)
    {
        $languagePageCategory = LanguagePageCategory::where('description', $description)->where('language_id', $idLanguage)->first();

        if (is_null($languagePageCategory)) {
            return 0;
        } else {
            return $languagePageCategory->page_category_id;
        }
    }

    public static function getAllListCategories($idLanguage, $idPages)
    {
        $result = collect();

        foreach (PageCategoryFather::whereNull('page_category_father_id')->orderBy('order')->get() as $categoryAll) {
            $DtoCategories = new DtoCategories();
            $Categories = PageCategory::where('id', $categoryAll->page_category_id)->first();
            $CategoriesLanguages = LanguagePageCategory::where('page_category_id', $categoryAll->page_category_id)->first();

            $DtoCategories->id = $categoryAll->page_category_id;
            $DtoCategories->order = $Categories->order;
            $DtoCategories->description = $CategoriesLanguages->description;
            $itemCategory = PagePageCategory::where('page_id', $idPages)->where('page_category_id', $categoryAll->page_category_id)->get()->first();

            $DtoCategories->checked = false;
            if (isset($itemCategory)) {
                $DtoCategories->checked = true;
            }

            $DtoCategories->categoriesList = static::_getSubAllListCategories($categoryAll->page_category_id, $idPages);
            $result->push($DtoCategories);
        }
        return $result;
    }
    

    private static function _getSubAllListCategories($idCategoryFather, $idPages)
    {
        $dtoSubCategoriesLists = collect();

        foreach (PageCategoryFather::where('page_category_father_id', $idCategoryFather)->orderBy('order')->get() as $categoryAll) {

            $dtoSubCategoriesList = new DtoCategories();

            $Categories = PageCategory::where('id', $categoryAll->page_category_id)->first();
            $CategoriesLanguages = LanguagePageCategory::where('page_category_id', $categoryAll->page_category_id)->first();

            $dtoSubCategoriesList->id = $categoryAll->page_category_id;
            $dtoSubCategoriesList->order = $Categories->order;
            $dtoSubCategoriesList->description = $CategoriesLanguages->description;

            $itemCategory = PagePageCategory::where('page_id', $idPages)->where('page_category_id', $categoryAll->page_category_id)->get()->first();

            $dtoSubCategoriesList->checked = false;
            if (isset($itemCategory)) {
                $dtoSubCategoriesList->checked = true;
            }

            $dtoSubCategoriesList->categoriesList = static::_getSubAllListCategories($categoryAll->page_category_id, $idPages);

            $dtoSubCategoriesLists->push($dtoSubCategoriesList);
        }

        return $dtoSubCategoriesLists;
    }




    #endregion GET
}

<?php

namespace App\BusinessLogic;

use App\Content;
use App\ContentLanguage;
use App\DtoModel\DtoInterventions;
use App\DtoModel\DtoItemGroupTechnicalSpecification;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use App\Customer;
use App\DtoModel\DtoInterventionsPhotos;
use App\DtoModel\DtoInterventionsProcessing;
use App\DtoModel\DtoItemGr;
use App\DtoModel\DtoQuotes;
use App\DtoModel\DtoUser;
use App\Item;
use App\GrStates;
use App\GrIntervention;
use App\GrInterventionPhoto;
use App\GrInterventionProcessing;
use App\GrQuotes;
use App\Helpers\HttpHelper;
use App\ItemGr;
use App\ItemLanguage;
use App\PaymentGr;
use App\People;
use App\QuotesGrIntervention;
use App\SelenaSqlViews;
use App\Setting;
use App\StatesQuotesGr;
use App\Tipology;
use App\User;
use App\UsersDatas;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;


class ProcessingViewBL
{

    /**
     * Get by getByViewValue
     * 
     * @param int id
     * @return DtoInterventions
     */
    public static function getByViewValue(int $id)
    {
        $valueConfigHour = "value_config_hour";
        $consumables = "consumables";

        $idIntervention = GrIntervention::where('id', $id)->first();

        $DtoInterventions = new DtoInterventions();
        $DtoInterventions->id = $id;
        $DtoInterventions->date_ddt = $idIntervention->date_ddt;

        if (isset($idIntervention->user_id)) {
            $User = UsersDatas::where('user_id', $idIntervention->user_id)->first();
          if (isset($User->business_name)){
              $DtoInterventions->user_id_view_processing = $User->business_name;
          }else{
              $DtoInterventions->user_id_view_processing =  $User->name . ' ' . $User->surname; 
          }
        } else {
            $DtoInterventions->user_id_view_processing = '';
            $DtoInterventions->user_id = '';
        }

       /* $User=UsersDatas::where('user_id', $idIntervention->user_id)->first();
        if (isset($User)) {
            $DtoInterventions->user_id_view_processing = $User->business_name;
        }*/
        if (isset($idIntervention->code_intervention_gr)) {
            $DtoInterventions->code_lib_gr = $idIntervention->code_intervention_gr;
        }else{
            $DtoInterventions->code_lib_gr = '';
        }
        $Item=ItemGr::where('id', $idIntervention->items_id)->first();
        if (isset($Item)) {
            $DtoInterventions->items_id = $Item->code_gr;
        }
        $People=People::where('id', $idIntervention->referent)->first();
        if (isset($People)) {  
            $DtoInterventions->referent = $People->name;
        }else{
           $DtoInterventions->referent = ''; 
        }
        $DtoInterventions->value_config_hour = Setting::where('code', $valueConfigHour)->first()->value;
        $DtoInterventions->consumables = Setting::where('code', $consumables)->first()->value;
        if (isset($idIntervention->imgmodule)) {
            $DtoInterventions->imgName = $idIntervention->imgmodule;
        }
          if (isset($idIntervention->working_with_testing)) {
            $DtoInterventions->working_with_testing = $idIntervention->working_with_testing;
        }
          if (isset($idIntervention->working_without_testing)) {
            $DtoInterventions->working_without_testing = $idIntervention->working_without_testing;
        }

        $dateProcessing = GrInterventionProcessing::where('id_intervention', $id)->first();
        if (isset($dateProcessing->updated_at)) {
            $DtoInterventions->updated_at = date_format($dateProcessing->updated_at, 'd/m/Y H:i:s');
        }else{
            $DtoInterventions->updated_at = '';
        }
        if (isset($dateProcessing->created_at)) {
            $DtoInterventions->created_at = date_format($dateProcessing->created_at, 'd/m/Y H:i:s');
        }else{
            $DtoInterventions->created_at = '';
        }
       
        $DtoInterventions->description = $idIntervention->description;
        $DtoInterventions->defect = $idIntervention->defect;
        $DtoInterventions->flaw_detection = $idIntervention->flaw_detection;
        $DtoInterventions->done_works = $idIntervention->done_works;
        $DtoInterventions->to_call = $idIntervention->to_call;
        $DtoInterventions->create_quote = $idIntervention->create_quote;
        $DtoInterventions->escape_from = $idIntervention->escape_from;
        $DtoInterventions->working = $idIntervention->working;
        $DtoInterventions->search_product = $idIntervention->search_product;
        $DtoInterventions->unrepairable = $idIntervention->unrepairable;
        if ($idIntervention->hour == '' || $idIntervention->hour == null){
          $DtoInterventions->hour = 0;  
        } else { 
        $DtoInterventions->hour = $idIntervention->hour;
        }

  if ($idIntervention->tot_value_config == '' || $idIntervention->tot_value_config == null){
          $DtoInterventions->tot_value_config = 0;  
        } else { 
        $DtoInterventions->tot_value_config = $idIntervention->tot_value_config;
        }

        $DtoInterventions->tot_final = $idIntervention->tot_final;
        $DtoInterventions->total_calculated = $idIntervention->total_calculated;

        foreach (GrInterventionProcessing::where('id_intervention', $id)->get() as $InterventionProcessing) {
            $DtoInterventionProcessing = new DtoInterventionsProcessing();
            $DtoInterventionProcessing->description = $InterventionProcessing->description;
            $DtoInterventionProcessing->qta = $InterventionProcessing->qta;
            $DtoInterventionProcessing->n_u = $InterventionProcessing->n_u;
            $DtoInterventionProcessing->id_intervention = $InterventionProcessing->id_intervention;
            if (isset($InterventionProcessing->id_gr_items)) {
                $DtoInterventionProcessing->code_gr = $InterventionProcessing->id_gr_items;
                $DtoInterventionProcessing->description_codeGr = ItemGr::where('id', $InterventionProcessing->id_gr_items)->first()->code_gr;
            }
            //$DtoInterventionProcessing->id_gr_items = $InterventionProcessing->id_gr_items;
            $DtoInterventionProcessing->price_list = $InterventionProcessing->price_list;
            $DtoInterventionProcessing->tot = $InterventionProcessing->tot;
            $DtoInterventions->Articles->push($DtoInterventionProcessing);
        }
        return $DtoInterventions;
    }


    /**
     * getDescriptionAndPriceProcessingSheet
     * 
     * @param int user_id
     * @return DtoInterventions
     */
    public static function getDescriptionAndPriceProcessingSheet(int $id)
    {
        $codeProduct = ItemGr::where('id', $id)->first();
        $dtoItemGr = new DtoItemGr();

        $dtoItemGr->id = $id;

        if (isset($codeProduct->description)){
            $dtoItemGr->description = $codeProduct->description;
        }else{
            $dtoItemGr->description = '';
        }
        if (isset($codeProduct->note)){
            $dtoItemGr->note = $codeProduct->note;
        }else{
            $dtoItemGr->note = '';
        }
        if (isset($codeProduct->plant)){
            $dtoItemGr->plant = $codeProduct->plant;
        }else{
            $dtoItemGr->plant = '';
        }
        if (isset($codeProduct->tipology)){
            $dtoItemGr->tipology = Tipology::where('id',$codeProduct->tipology)->first()->description;
        }else{
            $dtoItemGr->tipology = '';
        }
        if (isset($codeProduct->plant)){
            $dtoItemGr->price_list = $codeProduct->price_list;
        }else{
            $dtoItemGr->price_list = '';
        }
        return  $dtoItemGr;
    }
    //end getDescriptionAndPriceProcessingSheet


    /**
     * getAllProcessingView
     * 
     * @return DtoInterventionProcessing
     * @return DtoIntervention
     */
    public static function getAllProcessingView($idLanguage)
    {
        $result = collect();

           foreach (DB::select(
           /*"select distinct c.id, d.code_intervention_gr, c.user_id,c.defect,c.imgmodule,d.date_aggs
                from gr_intervention_processing d
                inner join gr_interventions c on c.id = d.id_intervention 
                where d.code_intervention_gr not in (select id from gr_interventions where id=d.id_intervention) order by d.code_intervention_gr desc limit 100"*/
    
                "select distinct c.id, c.code_intervention_gr, c.user_id,c.defect,c.imgmodule,d.date_aggs
                from gr_intervention_processing d
                inner join gr_interventions c on c.id = d.id_intervention 
                where d.id_intervention in (select id from gr_interventions where id=d.id_intervention) 
                order by c.code_intervention_gr desc limit 100"
            ) as $InterventionAll) {

         //   $CustomerDescription = UsersDatas::where('user_id', $InterventionAll->user_id)->first();
            $InterventionProcessing = GrInterventionProcessing::where('id_intervention', $InterventionAll->id)->first();
            $DtoInterventions = new DtoInterventions();

            if (isset($InterventionAll->id)) {
                $DtoInterventions->idIntervention = $InterventionAll->id;
                $DtoInterventions->id = $InterventionAll->id;
            }
            if (isset($InterventionAll->code_intervention_gr)) {
                $DtoInterventions->code_intervention_gr = $InterventionAll->code_intervention_gr;
            }

            if (isset($InterventionAll->user_id)) {
                $userdatas = UsersDatas::where('user_id', $InterventionAll->user_id)->first();
                if (isset($userdatas->business_name)){
                    $DtoInterventions->descriptionCustomer = $userdatas->business_name;
                }else{
                    $DtoInterventions->descriptionCustomer =  $userdatas->name . ' ' . $userdatas->surname; 
                }
            }else{
                $DtoInterventions->descriptionCustomer = '';
                $DtoInterventions->user_id = '';
            }

            if (isset($InterventionAll->defect)) {
                $DtoInterventions->defect = $InterventionAll->defect;
            } else {
                $DtoInterventions->defect = '';
            }
            if (isset($InterventionAll->imgmodule)) {
                $DtoInterventions->imgmodule = $InterventionAll->imgmodule;
            } else {
                $DtoInterventions->imgmodule = '';
            }

            if (isset($InterventionProcessing->created_at)) {
                $DtoInterventions->date_aggs = date_format($InterventionProcessing->created_at, 'd/m/Y');
            } else {
                $DtoInterventions->date_aggs = '';
            }

            $result->push($DtoInterventions);
        }
        return $result;
    }

    #region 
    /**
     * updateInterventionUpdateProcessingSheet
     * 
     * @param Request DtoInterventions
     * @param int idLanguage
     */

    public static function updateInterventionUpdateProcessingSheet(Request $DtoInterventions)
    {
        $interventionOnDb = GrIntervention::where('id', $DtoInterventions->id)->first();

        DB::beginTransaction();
        try {
            if (isset($DtoInterventions->id)) {
                $interventionOnDb->id = $DtoInterventions->id;
            }
            if (isset($DtoInterventions->idLanguage)) {
                $interventionOnDb->language_id = $DtoInterventions->idLanguage;
            }
            if (isset($DtoInterventions->updated_id)) {
                $interventionOnDb->updated_id = $DtoInterventions->updated_id;
            }
            if (isset($DtoInterventions->unrepairable)) {
                $interventionOnDb->unrepairable = $DtoInterventions->unrepairable;
                $interventionOnDb->preventivo_nuovo_creato = false;
            }
            if (isset($DtoInterventions->code_lib_gr)) {
                $interventionOnDb->code_intervention_gr = $DtoInterventions->code_lib_gr;
            }else{
                $interventionOnDb->code_intervention_gr = '';
            }
            
            if (isset($DtoInterventions->create_quote)) {
                $interventionOnDb->create_quote = $DtoInterventions->create_quote;
            }
            if (isset($DtoInterventions->to_call)) {
                $interventionOnDb->to_call = $DtoInterventions->to_call;
                $interventionOnDb->preventivo_nuovo_creato = false;
            }
            if (isset($DtoInterventions->escape_from)) {
                $interventionOnDb->escape_from = $DtoInterventions->escape_from;
                $interventionOnDb->preventivo_nuovo_creato = false;
            }else{
                $interventionOnDb->escape_from = false;
            }
            if (isset($DtoInterventions->working)) {
                $interventionOnDb->working = $DtoInterventions->working;
            }
            if (isset($DtoInterventions->search_product)) {
                $interventionOnDb->search_product = $DtoInterventions->search_product;
            }
            
            if (isset($DtoInterventions->flaw_detection)) {
                $interventionOnDb->flaw_detection = $DtoInterventions->flaw_detection;
            }
            if (isset($DtoInterventions->done_works)) {
                $interventionOnDb->done_works = $DtoInterventions->done_works;
            }
            if (isset($DtoInterventions->hour)) {
                $interventionOnDb->hour = $DtoInterventions->hour;
            }
            if (isset($DtoInterventions->value_config_hour)) {
                $interventionOnDb->value_config_hour = $DtoInterventions->value_config_hour;
            }
            if (isset($DtoInterventions->tot_value_config)) {
                $interventionOnDb->tot_value_config = $DtoInterventions->tot_value_config;
            }
            if (isset($DtoInterventions->consumables)) {
                $interventionOnDb->consumables = $DtoInterventions->consumables;
            }
            if (isset($DtoInterventions->tot_final)) {
                $interventionOnDb->tot_final = $DtoInterventions->tot_final;
            }
            if (isset($DtoInterventions->total_calculated)) {
                $interventionOnDb->total_calculated = $DtoInterventions->total_calculated;
            }
            if (isset($DtoInterventions->working_without_testing)) {
                $interventionOnDb->working_without_testing = $DtoInterventions->working_without_testing;
                $interventionOnDb->preventivo_nuovo_creato = false;
            }
            if (isset($DtoInterventions->working_with_testing)) {
                $interventionOnDb->working_with_testing = $DtoInterventions->working_with_testing;
                $interventionOnDb->preventivo_nuovo_creato = false;
            }

            $interventionOnDb->save();

            $IdInterventionProcessing = GrInterventionProcessing::where('id_intervention', $DtoInterventions->id)->get();
            if (isset($IdInterventionProcessing)) {
                GrInterventionProcessing::where('id_intervention', $DtoInterventions->id)->delete();
            }

            //$Rowgrprocessing = new GrInterventionProcessing();
            //if(isset($DtoInterventions->date_aggs)) {
            // $Rowgrprocessing->date_aggs = $DtoInterventions->date_aggs;             
            // }
            // $Rowgrprocessing->save();

            foreach ($DtoInterventions->Articles as $InterventionArticles) {
                //$IdInterventionArticles =  GrInterventionProcessing::where('id_intervention', $DtoInterventions->id)->first();

                //if(isset($InterventionArticles->description)){ 
                $RowInterventionArticles = new GrInterventionProcessing();

                if (isset($DtoInterventions->id)) {
                    $RowInterventionArticles->id_intervention = $DtoInterventions->id;
                }
                if (isset($InterventionArticles['description'])) {
                    $RowInterventionArticles->description = $InterventionArticles['description'];
                }else{
                   $RowInterventionArticles->description =''; 
                }
                if (isset($InterventionArticles['qta'])) {
                    $RowInterventionArticles->qta = $InterventionArticles['qta'];
                }else{
                   $RowInterventionArticles->qta =''; 
                }

                if (isset($InterventionArticles['n_u'])) {
                    $RowInterventionArticles->n_u = $InterventionArticles['n_u'];
                }else{
                   $RowInterventionArticles->n_u =''; 
                }

                if (isset($InterventionArticles['code_gr'])&& $InterventionArticles['code_gr'] != 'null') {
                    $RowInterventionArticles->id_gr_items = $InterventionArticles['code_gr'];
                }else{
                   $RowInterventionArticles->id_gr_items = null; 
                }
                if (isset($InterventionArticles['price_list'])) {
                    $RowInterventionArticles->price_list = $InterventionArticles['price_list'];
                }else{
                   $RowInterventionArticles->price_list = ''; 
                }
                if (isset($InterventionArticles['tot'])) {
                    $RowInterventionArticles->tot = $InterventionArticles['tot'];
                }else{
                   $RowInterventionArticles->tot = ''; 
                }
                if (isset($DtoInterventions->date_aggs)) {
                    $RowInterventionArticles->date_aggs = str_replace('Data:', '', $DtoInterventions->date_aggs);
                }
                    $RowInterventionArticles->code_intervention_gr = $DtoInterventions->code_lib_gr;
               
                    $RowInterventionArticles->save();
                //}
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    public static function getSearchFdl(Request $request)
    {
        $result = collect();
        $content = '';
        $finalHtml = '';
        $strWhere = "";

        $code_intervention_gr = $request->code_intervention_gr;
        $ClienteSearch = $request->cliente;
        if ($ClienteSearch != "") {
          $strWhere = $strWhere . ' OR c.user_id = ' . $ClienteSearch . '';
        }

        foreach (DB::select(
            'select distinct c.id, d.code_intervention_gr, c.user_id,c.defect,c.imgmodule,d.date_aggs
            from gr_intervention_processing d
            inner join gr_interventions c on c.id = d.id_intervention 
            WHERE d.code_intervention_gr = :code_intervention_gr ' . $strWhere,
        ['code_intervention_gr' => $code_intervention_gr],
        ) as $InterventionAll) {

            //$CustomerDescription = UsersDatas::where('user_id', $InterventionAll->user_id)->first();
            $InterventionProcessing = GrInterventionProcessing::where('id_intervention', $InterventionAll->id)->first();

            $DtoInterventions = new DtoInterventions();

            if (isset($InterventionAll->id)) {
                $DtoInterventions->idIntervention = $InterventionAll->id;
                $DtoInterventions->id = $InterventionAll->id;
            }
            if (isset($InterventionAll->code_intervention_gr)) {
                $DtoInterventions->code_intervention_gr = $InterventionAll->code_intervention_gr;
            }
            
          /*  if (isset($CustomerDescription->business_name)) {
                $DtoInterventions->descriptionCustomer =  $CustomerDescription->business_name;
            } else {
                $DtoInterventions->descriptionCustomer = '';
            }*/
            if (isset($InterventionAll->user_id)) {
                $userdatas = UsersDatas::where('user_id', $InterventionAll->user_id)->first();
              if (isset($userdatas->business_name)){
                  $DtoInterventions->descriptionCustomer = $userdatas->business_name;
              }else{
                  $DtoInterventions->descriptionCustomer =  $userdatas->name . ' ' . $userdatas->surname; 
              }
            } else {
                $DtoInterventions->descriptionCustomer = '';
                $DtoInterventions->user_id = '';
            }

            if (isset($InterventionAll->defect)) {
                $DtoInterventions->defect = $InterventionAll->defect;
            } else {
                $DtoInterventions->defect = '';
            }
            if (isset($InterventionAll->imgmodule)) {
                $DtoInterventions->imgmodule = $InterventionAll->imgmodule;
            } else {
                $DtoInterventions->imgmodule = '';
            }

            if (isset($InterventionProcessing->created_at)) {
                $DtoInterventions->date_aggs = date_format($InterventionProcessing->created_at, 'd/m/Y');
            } else {
                $DtoInterventions->date_aggs = '';
            }
            $result->push($DtoInterventions);
        }
            return $result;
    }

    #region DELETE
    /**
     * Delete
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $IdIntervention = GrIntervention::where('id', $id);
        $GrInterventionProcessing = GrInterventionProcessing::find($id);

        DB::beginTransaction();

        try {
            GrInterventionProcessing::where('id_intervention', $id)->delete();

            if (isset($IdIntervention->to_call)) {
                $IdIntervention->to_call = "";
            }
            if (isset($IdIntervention->escape_from)) {
                $IdIntervention->escape_from = false;
            }
            if (isset($IdIntervention->working)) {
                $IdIntervention->working = false;
            }
             if (isset($IdIntervention->search_product)) {
                $IdIntervention->search_product = false;
            }
            if (isset($IdIntervention->unrepairable)) {
                $IdIntervention->unrepairable = false;
            }
            if (isset($IdIntervention->working_with_testing)) {
                $IdIntervention->working_with_testing = false;
            }
             if (isset($IdIntervention->working_without_testing)) {
                $IdIntervention->working_without_testing = false;
            }
            
            if (isset($IdIntervention->flaw_detection)) {
                $IdIntervention->flaw_detection = "";
            }
            if (isset($IdIntervention->done_works)) {
                $IdIntervention->done_works  = "";
            }
            if (isset($IdIntervention->hour)) {
                $IdIntervention->hour = "";
            }
            if (isset($IdIntervention->value_config_hour)) {
                $IdIntervention->value_config_hour = "";
            }

            if (isset($IdIntervention->tot_value_config)) {
                $IdIntervention->tot_value_config = "";
            }
            if (isset($IdIntervention->tot_final)) {
                $IdIntervention->tot_final = "";
            }
            if (isset($IdIntervention->total_calculated)) {
                $IdIntervention->total_calculated = "";
            }
            if (isset($IdIntervention->consumables)) {
                $IdIntervention->consumables  = "";
            }


            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
    #endregion DELETE


/* ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

//GESTIONE MATERIALI TABELLA LAVORAZIONI ASSEGNATE 
    /**
     * getAllProcessingSearch
     * 
     * @return DtoInterventionProcessing
     * @return DtoIntervention
     */
    public static function getAllProcessingSearch($idLanguage)
    {
        $result = collect();
            foreach (DB::select(

                "select distinct c.code_intervention_gr, c.id, c.to_call, 
                c.escape_from, c.search_product,c.working,c.user_id,
                c.referent, c.date_ddt_gr, c.nr_ddt_gr, c.defect,c.ready, 
                c.imgmodule,c.working_without_testing,
                c.working_with_testing,c.unrepairable, c.preventivo_nuovo_creato, 
                c.create_quote
                FROM gr_interventions as c 
                where (c.date_ddt_gr is null and c.nr_ddt_gr is null and c.escape_from = true and c.preventivo_nuovo_creato = false and c.code_intervention_gr not in (select code_intervention_gr from gr_quotes gq where c.code_intervention_gr = gq.code_intervention_gr)) 
                or (c.date_ddt_gr is null and c.nr_ddt_gr is null and c.to_call = true and c.preventivo_nuovo_creato = false and c.code_intervention_gr not in (select code_intervention_gr from gr_quotes gq where c.code_intervention_gr = gq.code_intervention_gr)) 
                or (c.date_ddt_gr is null and c.nr_ddt_gr is null and c.working_without_testing = true and c.preventivo_nuovo_creato = false and c.code_intervention_gr not in (select code_intervention_gr from gr_quotes gq where c.code_intervention_gr = gq.code_intervention_gr)) 
                or (c.date_ddt_gr is null and c.nr_ddt_gr is null and c.working_with_testing = true and c.preventivo_nuovo_creato = false and c.code_intervention_gr not in (select code_intervention_gr from gr_quotes gq where c.code_intervention_gr = gq.code_intervention_gr))
                or (c.date_ddt_gr is null and c.nr_ddt_gr is null and c.unrepairable = true and c.preventivo_nuovo_creato = false and c.code_intervention_gr not in (select code_intervention_gr from gr_quotes gq where c.code_intervention_gr = gq.code_intervention_gr)) 
                order by c.code_intervention_gr desc 
                limit 50"
           /* "select distinct c.code_intervention_gr, c.id, c.to_call, c.escape_from, c.search_product,c.working,c.user_id,
            c.referent, c.date_ddt_gr, c.nr_ddt_gr, c.defect,c.ready, c.imgmodule,c.working_without_testing,
            c.working_with_testing,c.unrepairable, c.create_quote
            FROM gr_interventions as c 
            where(c.date_ddt_gr is null and c.nr_ddt_gr is null and c.escape_from = true) or (c.date_ddt_gr is null and c.nr_ddt_gr is null and c.to_call = true) or
            (c.date_ddt_gr is null and c.nr_ddt_gr is null and c.working_without_testing = true) or (c.date_ddt_gr is null and c.nr_ddt_gr is null and c.working_with_testing = true)
            or (c.date_ddt_gr is null and c.nr_ddt_gr is null and c.unrepairable = true) 
            order by c.code_intervention_gr desc
            limit 50"*/

            ) as $InterventionAll) {    
           // $CustomerDescription = UsersDatas::where('user_id', $InterventionAll->user_id)->first();
            //$CustomerDescription = User::where('id', $InterventionAll->user_id)->first();
            //$InterventionProcessing = GrInterventionProcessing::where('id_intervention', $InterventionAll->id)->first();

            $DtoInterventions = new DtoInterventions();

            if (isset($InterventionAll->id)) {
                $DtoInterventions->idIntervention = $InterventionAll->id;
                $DtoInterventions->id = $InterventionAll->id;
            }
            if (isset($InterventionAll->code_intervention_gr)) {
                $DtoInterventions->code_intervention_gr = $InterventionAll->code_intervention_gr;
            }

            if (isset($InterventionAll->to_call)) {
                $DtoInterventions->to_call = $InterventionAll->to_call;
            }
            if (isset($InterventionAll->escape_from)) {
                $DtoInterventions->escape_from = $InterventionAll->escape_from;
            }
            if (isset($InterventionAll->search_product)) {
                $DtoInterventions->search_product = $InterventionAll->search_product;
            }
            if (isset($InterventionAll->unrepairable)) {
                $DtoInterventions->unrepairable = $InterventionAll->unrepairable;
            }
            
            if (isset($InterventionAll->working)) {
                $DtoInterventions->working = $InterventionAll->working;
            }

            if (isset($InterventionAll->user_id)) {
                $userdatas = UsersDatas::where('user_id', $InterventionAll->user_id)->first();
              if (isset($userdatas->business_name)){
                  $DtoInterventions->descriptionCustomer = $userdatas->business_name;
              }else{
                  $DtoInterventions->descriptionCustomer =  $userdatas->name . ' ' . $userdatas->surname; 
              }
            } else {
                    $DtoInterventions->descriptionCustomer = '';
                    $DtoInterventions->user_id = '';
            }
        
           /* if (isset($CustomerDescription->business_name)) {
                $DtoInterventions->descriptionCustomer =  $CustomerDescription->business_name;
            } else {
                $DtoInterventions->descriptionCustomer = '';
            }*/

             if (isset($InterventionAll->nr_ddt_gr)) {
                $DtoInterventions->nr_ddt_gr =  $InterventionAll->nr_ddt_gr;
            } else {
                $DtoInterventions->nr_ddt_gr = '';
            }
            if (isset($InterventionAll->date_ddt_gr)) {
                $DtoInterventions->date_ddt_gr =  $InterventionAll->date_ddt_gr;
            } else {
                $DtoInterventions->date_ddt_gr = '';
            }

            if (isset($InterventionAll->defect)) {
                $DtoInterventions->defect = $InterventionAll->defect;
            } else {
                $DtoInterventions->defect = '';
            }
             if (isset($InterventionAll->ready)) {
                 $DtoInterventions->ready = $InterventionAll->ready;
                }else {
                $DtoInterventions->ready = false;
            }
            if (isset($InterventionAll->working_without_testing)) {
                 $DtoInterventions->working_without_testing = $InterventionAll->working_without_testing;
                }else {
                $DtoInterventions->working_without_testing = false;
            }
            if (isset($InterventionAll->working_with_testing)) {
                 $DtoInterventions->working_with_testing = $InterventionAll->working_with_testing;
                }else {
                $DtoInterventions->working_with_testing = false;
            }

            if (isset($InterventionAll->imgmodule)) {
                $DtoInterventions->imgmodule = $InterventionAll->imgmodule;
            } else {
                $DtoInterventions->imgmodule = '';
            }

            $people= People::where('id', $InterventionAll->referent)->first();

            if (isset($people)) {
                $DtoInterventions->referent = $people->name;
            } else {
                $DtoInterventions->referent = '';
            }
            $result->push($DtoInterventions);
        }
        return $result;
    }
    //--------- END GESTIONE MATERIALI TABELLA LAVORAZIONI ASSEGNATE   


    //--------- END GESTIONE MATERIALI TABELLA PREVENTIVI
    /**
     * getAllViewQuoteSearch
     * 
     * @return DtoInterventionProcessing
     * @return DtoIntervention
     */
    public static function getAllViewQuoteSearch($idLanguage)
    {
        $result = collect();
         foreach (DB::select(
            "SELECT i.code_intervention_gr, i.id, i.imgmodule, i.user_id, i.description,c.id_states_quote,
            i.nr_ddt_gr,i.date_ddt_gr, i.ready,i.unrepairable
            FROM gr_interventions as i
            inner join gr_quotes as c on c.id_intervention = i.id
            WHERE (i.date_ddt_gr is null)
            AND (i.nr_ddt_gr is null
            OR i.nr_ddt_gr  = '')
            AND c.id_states_quote <>1
            order by c.id_states_quote desc, i.code_intervention_gr desc
            limit 100"             

        ) as $InterventionAll) {

           // $CustomerDescription = UsersDatas::where('user_id', $InterventionAll->user_id)->first();
            //$CustomerDescription = User::where('id', $InterventionAll->user_id)->first();
            $InterventionQuotes = GrQuotes::where('id_intervention', $InterventionAll->id)->first();

            $DtoInterventions = new DtoInterventions();

            if (isset($InterventionAll->code_intervention_gr)) {
                $DtoInterventions->code_intervention_gr = $InterventionAll->code_intervention_gr;
            }

            if (isset($InterventionAll->imgmodule)) {
                $DtoInterventions->imgmodule = $InterventionAll->imgmodule;
            } else {
                $DtoInterventions->imgmodule = '';
            }

            if (isset($InterventionAll->ready)) {
                $DtoInterventions->ready = $InterventionAll->ready;
            }else {
                $DtoInterventions->ready = false;
            }
             if (isset($InterventionAll->unrepairable)) {
                $DtoInterventions->unrepairable = $InterventionAll->unrepairable;
            }else {
                $DtoInterventions->unrepairable = false;
            }
            
            if (isset($InterventionAll->id)) {
                $DtoInterventions->idIntervention = $InterventionAll->id;
                $DtoInterventions->id = $InterventionAll->id;
            }
             if (isset($InterventionQuotes->id_states_quote)) {
                $DtoInterventions->id_states_quote = $InterventionQuotes->id_states_quote;
            }
            if (isset($InterventionQuotes->updated_at)) {
                $DtoInterventions->updated_at = date_format($InterventionQuotes->updated_at, 'd/m/Y');
            }
            
           /* if (isset($CustomerDescription->business_name)) {
                $DtoInterventions->descriptionCustomer = $CustomerDescription->business_name;
            } else {
                $DtoInterventions->descriptionCustomer = '';
            }*/
            if (isset($InterventionAll->user_id)) {
                $userdatas = UsersDatas::where('user_id', $InterventionAll->user_id)->first();
              if (isset($userdatas->business_name)){
                  $DtoInterventions->descriptionCustomer = $userdatas->business_name;
              }else{
                  $DtoInterventions->descriptionCustomer =  $userdatas->name . ' ' . $userdatas->surname; 
              }
            } else {
                   $DtoInterventions->descriptionCustomer = '';
                   $DtoInterventions->user_id = '';
            }

            if (isset($InterventionAll->description)) {
                $DtoInterventions->description = $InterventionAll->description;
            }else {
                $DtoInterventions->description = '';
            }

            if (isset($InterventionAll->date_ddt_gr)) {
                $DtoInterventions->date_ddt_gr = $InterventionAll->date_ddt_gr;
            }else {
                $DtoInterventions->date_ddt_gr = '';
            }

            if (isset($InterventionAll->nr_ddt_gr)) {
                $DtoInterventions->nr_ddt_gr = $InterventionAll->nr_ddt_gr;
            }else {
                $DtoInterventions->nr_ddt_gr = '';
            }

            if (isset($InterventionAll->working)) {
                $DtoInterventions->working = $InterventionAll->working;
            }
            $result->push($DtoInterventions);
        }
        return $result;
    }
         //------------------------------------------ END GESTIONE MATERIALI TABELLA PREVENTIVI --------------------------------------- //
}

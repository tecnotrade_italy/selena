<?php
namespace App\BusinessLogic;

use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\LogHelper;
use App\DtoModel\DtoCategoryType;
use App\CategoryTypeLanguage;
use App\CategoryType;
use App\User;
use Illuminate\Support\Facades\DB;



use Exception;
use Illuminate\Http\Request;

class CategoryTypeBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request CategoryTypeBL
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoCategoryType, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoCategoryType->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (CategoryType::find($DtoCategoryType->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($DtoCategoryType->order)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CompanyNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoCategoryType->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CompanyNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    /**
     * Convert to dto
     * 
     * @param CategoryType postalCode
     * 
     * @return DtoCategoryType
     */
    private static function _convertToDto($CategoryType)
    {
        $DtoCategoryType = new DtoCategoryType();
        
        if (isset($CategoryType->id)) {
            $DtoCategoryType->id = $CategoryType->id;
        }

        if (isset($CategoryType->order)) {
            $DtoCategoryType->order = $CategoryType->order;
        }
        if (isset($CategoryType->description)) {
            $DtoCategoryType->description = $CategoryType->description;
        }

        return  $DtoCategoryType;
    }


    /**
     * Convert to VatType
     * 
     * @param DtoCategoryType dtoVatType
     * 
     * @return CategoryType
     */
    private static function _convertToModel($DtoCategoryType)
    {
        $CategoryType = new CategoryType();

        if (isset($DtoCategoryType->id)) {
            $CategoryType->id = $DtoCategoryType->id;
        }

        if (isset($DtoCategoryType->order)) {
            $CategoryType->order = $DtoCategoryType->order;
        }

        if (isset($DtoCategoryType->description)) {
            $CategoryType->description = $DtoCategoryType->description;
        }

        if (isset($DtoCategoryType->language_id)) {
            $CategoryType->language_id = $DtoCategoryType->language_id;
        }

        return $CategoryType;
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCategoryType
     */

    public static function getById(int $id)
    {
        return static::_convertToDto(CategoryType::find($id));
    }

 
    /**
     * Get select
     *
     * @param String $search
     * @return CategoryTypeLanguage
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return CategoryTypeLanguage::select('id', 'description')->get();
        } else {
            return CategoryTypeLanguage::where('description', 'like', $search . '%')->select('id', 'description')->get();
        }
    }


    /**
     * Get all
     * 
     * @return DtoCategoryType
     */
    public static function getAll($idLanguage)
    {
        $result = collect(); 
        foreach (CategoryTypeLanguage::where('language_id',$idLanguage)->get() as $categoryAll) {
            $DtoCategoryType = new DtoCategoryType();
            $DtoCategoryType->id = CategoryType::where('id', $categoryAll->category_type_id)->first()->id;
            $DtoCategoryType->order = CategoryType::where('id', $categoryAll->category_type_id)->first()->order;
            $DtoCategoryType->description = $categoryAll->description;
            $result->push($DtoCategoryType); 
        }
        return $result;
    }
    

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoCategoryType
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoCategoryType, int $idUser)
    {
        
        static::_validateData($DtoCategoryType, $DtoCategoryType->idLanguage, DbOperationsTypesEnum::INSERT);
        DB::beginTransaction();

        try {
            $CategoryType = new CategoryType();
            //$UnitMeasure->id = $DtoUnitMeasure->id;
            $CategoryType->order = $DtoCategoryType->order;
            $CategoryType->created_id = $DtoCategoryType->$idUser;
            $CategoryType->save();

            $CategoryTypeLanguage = new CategoryTypeLanguage();
            $CategoryTypeLanguage->language_id =$DtoCategoryType->idLanguage;
            $CategoryTypeLanguage->category_type_id = $CategoryType->id;
            $CategoryTypeLanguage->description = $DtoCategoryType->description;
            $CategoryTypeLanguage->created_id = $DtoCategoryType->$idUser;
            $CategoryTypeLanguage->save();

            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $CategoryType->id;
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoCategoryType
     * @param int idLanguage
     */
    //::debug($DtoCategory->toArray());

    public static function update(Request $DtoCategoryType)

    {
        static::_validateData($DtoCategoryType, $DtoCategoryType->idLanguage, DbOperationsTypesEnum::UPDATE);
        $customerOnDb = CategoryType::find($DtoCategoryType->id);

        //DBHelper::updateAndSave(static::_convertToModel($DtoUnitMeasure), $customerOnDb);

        $languageNationOnDb = CategoryTypeLanguage::where('category_type_id', $DtoCategoryType->id, $DtoCategoryType->idLanguage)->first();
        
        DB::beginTransaction();

        try {
            $customerOnDb->id = $DtoCategoryType->id;
            $customerOnDb->order = $DtoCategoryType->order;
            $customerOnDb->save();


            //$languageNationOnDb->language_id = $idLanguage;
            $languageNationOnDb->language_id = $DtoCategoryType->idLanguage;
            $languageNationOnDb->category_type_id = $DtoCategoryType->id;
            $languageNationOnDb->description = $DtoCategoryType->description;
            $languageNationOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
    
  #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */

    public static function delete(int $id, int $idLanguage)
    {
        $CategoryTypeLanguage= CategoryTypeLanguage::where('category_type_id', $id)->first();

        if (is_null($CategoryTypeLanguage)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $CategoryTypeLanguage->delete();

        $CategoryType = CategoryType::find($id);

        if (is_null($CategoryType)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $CategoryType->delete();
    }

    #endregion DELETE
}
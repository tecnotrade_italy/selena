<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoChartJS;
use App\DtoModel\DtoChartJSDatasets;
use App\DtoModel\DtoChartMulti;
use App\DtoModel\DtoFeedbackNewsletter;
use App\DtoModel\DtoGraphicFeedbackNewsletter;
use App\DtoModel\DtoNewsletter;
use App\Newsletter;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\FeedbackNewsletterEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Enums\SettingEnum;
use App\FeedbackNewsletter;
use App\Helpers\DBHelper;
use App\Helpers\LogHelper;
use App\Logs;
use App\NewsletterRecipient;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FeedbackNewsletterBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param int idNewsletterRecipient
     * @param string code
     */
    private static function _validateData(int $idNewsletterRecipient, string $code)
    {
        if (!NewsletterRecipientBL::getByid($idNewsletterRecipient)) {
            throw new Exception(DictionaryBL::getTranslate(LanguageBL::getDefault()->id, DictionariesCodesEnum::NewsletterRecipientNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($code)) {
            throw new Exception(DictionaryBL::getTranslate(LanguageBL::getDefault()->id, DictionariesCodesEnum::CodeNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    /**
     * Convert to Dto
     * 
     * @param DBRaw feedbackNewsletter
     * 
     * @return DtoFeedbackNewsletter
     */
    private static function _convertToDto($feedbackNewsletter)
    {
        $dtoFeedbackNewsletter = new DtoFeedbackNewsletter();

        if (isset($feedbackNewsletter->id)) {
            $dtoFeedbackNewsletter->id = $feedbackNewsletter->id;
        }

        if (isset($feedbackNewsletter->code)) {
            $dtoFeedbackNewsletter->code = $feedbackNewsletter->code;
        }

        if (isset($feedbackNewsletter->name)) {
            $dtoFeedbackNewsletter->name = $feedbackNewsletter->name;
        }

        if (isset($feedbackNewsletter->email)) {
            $dtoFeedbackNewsletter->email = $feedbackNewsletter->email;
        }

        return $dtoFeedbackNewsletter;
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get all by id Newsletter
     * 
     * @param int idNewsletter
     *
     * @return DtoFeedbackNewsletter
     */
    public static function getAllByNewslletter(int $idNewsletter)
    {
        $result = collect();

        foreach (DB::select(
            '   SELECT feedback_newsletters.id, feedback_newsletters.code, newsletters_recipients.name, newsletters_recipients.email
                FROM newsletters_recipients
                INNER JOIN feedback_newsletters ON newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
                WHERE newsletters_recipients.newsletter_id = :idNewsletter',
            ['idNewsletter' => $idNewsletter]
        ) as $feedbackNewsletter) {
            $result->push(static::_convertToDto($feedbackNewsletter));
        }

        return $result;
    }



    public static function getChartGraphics(int $idNewsletter)
    {
       $dtoChartMulti = new DtoChartMulti();
       $dtoChartMulti->count = 0;

        $countOpeneds = DB::select(
            'SELECT COUNT(*) AS countopened
            FROM (
                SELECT feedback_newsletters.code, feedback_newsletters.newsletter_recipient_id, feedback_newsletters.created_at
                FROM newsletters_recipients
                INNER JOIN feedback_newsletters ON newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
                WHERE newsletters_recipients.newsletter_id = :idNewsletter
                AND feedback_newsletters.code = :open
                GROUP BY feedback_newsletters.code, feedback_newsletters.newsletter_recipient_id, feedback_newsletters.created_at
                ) AS c',
            ['idNewsletter' => $idNewsletter, 'open' => FeedbackNewsletterEnum::Open]
        )[0]->countopened;

            $dtoChartMulti->countOpened = $countOpeneds;
    
            $resultlabels = collect();
            $resultdata = collect();

            $feedbackNewslettersss = DB::select(
            'SELECT feedback_newsletters.code, to_char(feedback_newsletters.created_at, \'DD Month\') as created_at , count(*) as count
            FROM newsletters_recipients
            INNER JOIN feedback_newsletters ON newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
            WHERE newsletters_recipients.newsletter_id = :idNewsletter
            AND feedback_newsletters.code = :open 
            group by feedback_newsletters.code, to_char(feedback_newsletters.created_at, \'DD Month\')',
            ['idNewsletter' => $idNewsletter, 'open' => FeedbackNewsletterEnum::Open]);

            foreach ($feedbackNewslettersss as $key => $fn) {
                $resultlabels->push($feedbackNewslettersss[$key]->created_at);
                $resultdata->push($feedbackNewslettersss[$key]->count);   
            }

            $dtoChartJS = new DtoChartJS();
            $dtoChartJS->labels = $resultlabels;
            $dtoChartJSDatasets = new DtoChartJSDatasets();
            $dtoChartJSDatasets->backgroundColor = '#00ffff';     
            $dtoChartJSDatasets->data = $resultdata;
            $dtoChartJSDatasets->borderColor = ["rgba(75, 192, 192, 0.5)", "rgba(255, 205, 86, 0.5)"];

            $dtoChartJS->datasets->push($dtoChartJSDatasets);
            $dtoChartMulti->chartsres->push($dtoChartJS); 
            $dtoChartMulti->count = $dtoChartMulti->count + 1;        
            return $dtoChartMulti;
    }

    public static function getChartGraphicsforTime(int $idNewsletter)
    {
       $dtoChartMulti = new DtoChartMulti();
       $dtoChartMulti->count = 0;

        $countOpeneds = DB::select(
            'SELECT COUNT(*) AS countopened
            FROM (
                SELECT feedback_newsletters.code, feedback_newsletters.newsletter_recipient_id, feedback_newsletters.created_at
                FROM newsletters_recipients
                INNER JOIN feedback_newsletters ON newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
                WHERE newsletters_recipients.newsletter_id = :idNewsletter
                AND feedback_newsletters.code = :open
                GROUP BY feedback_newsletters.code, feedback_newsletters.newsletter_recipient_id, feedback_newsletters.created_at
                ) AS c',
            ['idNewsletter' => $idNewsletter, 'open' => FeedbackNewsletterEnum::Open]
        )[0]->countopened;

            $dtoChartMulti->countOpened = $countOpeneds;
    
            $resultlabels = collect();
            $resultdata = collect();

            $feedbackNewsletterTime = DB::select('SELECT
            EXTRACT(hour from feedback_newsletters.created_at) as created_at,
            COUNT(feedback_newsletters.created_at) as count
            FROM newsletters_recipients
            INNER JOIN feedback_newsletters ON 
            newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
            WHERE newsletters_recipients.newsletter_id = :idNewsletter
            AND feedback_newsletters.code = :open 
            group by EXTRACT(hour from feedback_newsletters.created_at)',
            ['idNewsletter' => $idNewsletter, 'open' => FeedbackNewsletterEnum::Open]);

             $array = array('0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
    
                foreach ($array as $arrayKey) {

                $result_query = DB::select('SELECT
                EXTRACT(hour from feedback_newsletters.created_at) as created_at,
                COUNT(feedback_newsletters.created_at) as count
                FROM newsletters_recipients
                INNER JOIN feedback_newsletters ON 
                newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
                WHERE newsletters_recipients.newsletter_id = :idNewsletter
                AND feedback_newsletters.code = :open  
                and EXTRACT(hour from feedback_newsletters.created_at) = ' . $arrayKey . '  
                group by EXTRACT(hour from feedback_newsletters.created_at)',
                ['idNewsletter' => $idNewsletter, 'open' => FeedbackNewsletterEnum::Open]);

                $resultlabels->push($arrayKey);
                if (isset($result_query) && count($result_query)>0){
                  $resultdata->push($result_query[0]->count);      
                }else{
                    $resultdata->push(0);
                }

        }
            $dtoChartJS = new DtoChartJS();
            $dtoChartJS->labels = $resultlabels;
            $dtoChartJSDatasets = new DtoChartJSDatasets();
            $dtoChartJSDatasets->backgroundColor = '#f86c6b';     
            $dtoChartJSDatasets->data = $resultdata;
            $dtoChartJSDatasets->borderColor = ["#9e4545"];
            $dtoChartJS->datasets->push($dtoChartJSDatasets);
            $dtoChartMulti->chartsrestime->push($dtoChartJS); 

          return $dtoChartMulti;

    }

    /**
     * Get all chart by id Newsletter
     * 
     * @param int idNewsletter
     *
     * @return DtoFeedbackNewsletter
     */
    public static function getChart(int $idNewsletter)
    {
        $dtoChartMulti = new DtoChartMulti();
        $dtoChartMulti->count = 0;

        $countToError = DB::select(
           'SELECT COUNT(*) AS counttoerror
            FROM (
                SELECT feedback_newsletters.code, feedback_newsletters.newsletter_recipient_id
                FROM newsletters_recipients
                INNER JOIN feedback_newsletters ON newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
                WHERE newsletters_recipients.newsletter_id = :idNewsletter
                AND feedback_newsletters.code = :error
                GROUP BY feedback_newsletters.code, feedback_newsletters.newsletter_recipient_id
                ) AS c',
                ['idNewsletter' => $idNewsletter, 'error' => FeedbackNewsletterEnum::Error]
            );

        if (isset($countToError) && count($countToError) > 0) {
            $dtoChartMulti->countToError = $countToError[0]->counttoerror; 
        }else{
            $dtoChartMulti->countToError = ''; 
        }

        $countToSend = DB::select(
            'SELECT COUNT(*) as countToSend
            FROM newsletters_recipients 
            WHERE newsletters_recipients.newsletter_id = :idNewsletter',
            ['idNewsletter' => $idNewsletter]
        );

        if (isset($countToSend) && count($countToSend) > 0) {
            $dtoChartMulti->countToSend = $countToSend[0]->counttosend;  
        }else{
            $dtoChartMulti->countToSend = ''; 
        }

             $countOpened = DB::select(
            'SELECT count(*) as count
            FROM newsletters_recipients
            INNER JOIN feedback_newsletters ON newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
            WHERE newsletters_recipients.newsletter_id = :idNewsletter
            AND feedback_newsletters.code = :open
            group by feedback_newsletters.code',
            ['idNewsletter' => $idNewsletter, 'open' => FeedbackNewsletterEnum::Open]);

            if (isset($countOpened) && count($countOpened) > 0) {
                $dtoChartMulti->countOpened = $countOpened[0]->count;           
           }else{
            $dtoChartMulti->countOpened  = '';
           }

                    
        foreach (DB::select(
            '   SELECT code, count(*) AS count
                FROM (
                    SELECT feedback_newsletters.code, feedback_newsletters.newsletter_recipient_id
                    FROM newsletters_recipients
                    INNER JOIN feedback_newsletters ON newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
                    WHERE newsletters_recipients.newsletter_id = :idNewsletter
                     AND feedback_newsletters.code = :sent
                    GROUP BY feedback_newsletters.code, feedback_newsletters.newsletter_recipient_id
                ) AS c
                GROUP BY code',
            ['idNewsletter' => $idNewsletter, 'sent' => FeedbackNewsletterEnum::Sent]
        ) as $feedbackNewsletter) {
            $dtoChartJS = new DtoChartJS();
            $dtoChartJS->labels = ["Non " . $feedbackNewsletter->code, $feedbackNewsletter->code];
            $dtoChartJSDatasets = new DtoChartJSDatasets();
            $dtoChartJSDatasets->backgroundColor = ["rgba(75, 192, 192, 0.5)", "rgba(255, 205, 86, 0.5)"];

            if ($feedbackNewsletter->code == FeedbackNewsletterEnum::Sent) {
                $dtoChartJSDatasets->data = [$countToSend[0]->counttosend - $feedbackNewsletter->count, $feedbackNewsletter->count];
            } else {
                $dtoChartJSDatasets->data = [$countOpened[0]->count - $feedbackNewsletter->count, $feedbackNewsletter->count];
            }
            $dtoChartJS->datasets->push($dtoChartJSDatasets);
            $dtoChartMulti->charts->push($dtoChartJS);
            $dtoChartMulti->count = $dtoChartMulti->count + 1;
        }

           $newsletter= Newsletter::where('id', $idNewsletter)->first(); 
            $dtoChartMulti->idNewsletter = $idNewsletter;
           if (isset($idNewsletter)){
            $dtoChartMulti->title = $newsletter->title;
           }else{
            $dtoChartMulti->title = '';
           }
           if (isset($idNewsletter)){
            $dtoChartMulti->object = $newsletter->object; 
           }else{
            $dtoChartMulti->object = '';
           }
           if (isset($idNewsletter)){
            $dtoChartMulti->start_send_date_time = $newsletter->start_send_date_time; 
           }else{
            $dtoChartMulti->start_send_date_time  = '';
           }
           $countemailsend = DB::table('newsletters_recipients')
             ->select(DB::raw('count(id) as num_id'))
             ->where('newsletter_id', '=', $idNewsletter)
             ->groupBy('newsletter_id')
             ->get();
             $dtoChartMulti->nr_recipients = $countemailsend[0]->num_id; 
                
        return $dtoChartMulti;
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param int idNewsletterRecipient
     * @param string code
     * 
     * @return int
     */
    public static function insert(int $idNewsletterRecipient, string $code)
    {
        static::_validateData($idNewsletterRecipient, $code);

        $feedbackNewsletter = new FeedbackNewsletter();
        $feedbackNewsletter->newsletter_recipient_id = $idNewsletterRecipient;
        $feedbackNewsletter->code = $code;
        $feedbackNewsletter->created_id = UserBL::getIdByName('newsletter');
        $feedbackNewsletter->save();

        //section timeline da rivedere;//
        $logs = new Logs();
        $NewsletterRecipient = NewsletterRecipient::where ('id', $idNewsletterRecipient)->first();
        if (isset ($NewsletterRecipient->email)){
            $User= User::where('email', $NewsletterRecipient->email)->first();
            if (isset($User)) {
                $logs->user_id = $User->id; 
                $logs->action =  $code;
                $logs->description = 'Apertura Newsletter';
                $logs->newsletter_id = $NewsletterRecipient->newsletter_id;
                $logs->save();
            } else {
                $logs->action =  $code;
                $logs->description = 'Apertura Newsletter';
                $logs->newsletter_id = $NewsletterRecipient->newsletter_id;
                $logs->save();
            }      
        }
        
        return $feedbackNewsletter->id;
    }

    #endregion INSERT
}

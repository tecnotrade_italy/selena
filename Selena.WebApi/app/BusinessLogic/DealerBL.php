<?php

namespace App\BusinessLogic;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoGrDealer;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\GrDealer;
use App\ItemGroupTechnicalSpecification;
use Intervention\Image\Facades\Image;
use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;


class DealerBL
{
 #region GET

 /**
     * Get select
     *
     * @param String $search
     * @return Intervention
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return GrDealer::select('id', 'business_name as description')->get();
        } else {
            return GrDealer::where('business_name', 'ilike', $search . '%')->select('id', 'business_name as description')->get();
        }
    }
 }
       
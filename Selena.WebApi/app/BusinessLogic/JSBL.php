<?php

namespace App\BusinessLogic;

use App\Helpers\LogHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\DtoModel\DtoJs;

class JSBL
{
    #region GET
    /**
     * Get
     * 
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function get(int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);


        $DtoJs = new DtoJs();
        $DtoJs->jsBody = Storage::disk('public')->get('\js\sitemin.js');
        $DtoJs->jsHead = Storage::disk('public')->get('\js\siteminhead.js');
        //return Storage::disk('public')->get('\js\site.js');
        return $DtoJs;
    }

    #endregion GET

    #region POST

    /**
     * Update
     * 
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function update(Request $request, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($request->idFunctionality, $idUser, $idLanguage);
        Storage::disk('public')->put('\js\sitemin.js', $request->css);
       // Storage::disk('public')->put('\js\site.js', $request->css);
       // return;

        $url = 'https://www.toptal.com/developers/javascript-minifier/api/raw';
        $js = $request->css;
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => ["Content-Type: application/x-www-form-urlencoded"],
            CURLOPT_POSTFIELDS => http_build_query([ "input" => $js ])
        ]);
    
        $minified = curl_exec($ch);
        curl_close($ch);

        Storage::disk('public')->put('\js\site.js', $minified);
        return;


    }

    /**
     * Update
     * 
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function updateHead(Request $request, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($request->idFunctionality, $idUser, $idLanguage);
        Storage::disk('public')->put('\js\siteminhead.js', $request->css);
       // Storage::disk('public')->put('\js\site.js', $request->css);
       // return;

        $url = 'https://www.toptal.com/developers/javascript-minifier/api/raw';
        $js = $request->css;
        $ch = curl_init();
    
        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => ["Content-Type: application/x-www-form-urlencoded"],
            CURLOPT_POSTFIELDS => http_build_query([ "input" => $js ])
        ]);
    
        $minified = curl_exec($ch);
        curl_close($ch);

        Storage::disk('public')->put('\js\sitehead.js', $minified);
        return;
    }

    #endregion POST
}

<?php

namespace App\BusinessLogic;

use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoPlaces;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\LanguageNation;
use App\LanguageProvince;
use App\User;
use App\BookingSettings;
use App\DtoModel\DtoBookingServices;
use App\DtoModel\DtoBookingSettings;
use App\Message;
use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;

class BookingSettingsBL
{

     #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request DtoPlaces
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoBookingSettings, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoBookingSettings->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (BookingSettings::find($DtoBookingSettings->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }
    }

    /**
     * Convert to dto
     * 
     * @param BookingSettings
     * 
     * @return DtoBookingSettings
     */
    private static function _convertToDto($BookingSettings)
    {
        $DtoBookingSettings = new DtoBookingSettings();

        if (isset($BookingSettings->id)) {
            $DtoBookingSettings->id = $BookingSettings->id;
        }
        if (isset($BookingSettings->nr_appointments)) {
            $DtoBookingSettings->nr_appointments = $BookingSettings->nr_appointments;
        }
         if (isset($BookingSettings->send_email_to_user)) {
            $DtoBookingSettings->send_email_to_user = $BookingSettings->send_email_to_user;
        }
        if (isset($BookingSettings->send_email_to_employee)) {
            $DtoBookingSettings->send_email_to_employee = $BookingSettings->send_email_to_employee;
        }
        if (isset($BookingSettings->allow_cancellation)) {
            $DtoBookingSettings->allow_cancellation = $BookingSettings->allow_cancellation;
        }
        if (isset($BookingSettings->acceptance_of_mandatory_conditions)) {
            $DtoBookingSettings->acceptance_of_mandatory_conditions = $BookingSettings->acceptance_of_mandatory_conditions;
        }
        if (isset($BookingSettings->text_gdpr)) {
            $DtoBookingSettings->text_gdpr = $BookingSettings->text_gdpr;
        }
        if (isset($BookingSettings->email_receives_notifications)) {
            $DtoBookingSettings->email_receives_notifications = $BookingSettings->email_receives_notifications;
        }
        if (isset($BookingSettings->email_send_notifications)) {
            $DtoBookingSettings->email_send_notifications = $BookingSettings->email_send_notifications;
        }
        if (isset($BookingSettings->notification_object_send_to_administrator)) {                
            $DtoBookingSettings->notification_object_send_to_administrator = $BookingSettings->notification_object_send_to_administrator;
        }
        if (isset($BookingSettings->notification_object_send_to_visitor)) {                
            $DtoBookingSettings->notification_object_send_to_visitor = $BookingSettings->notification_object_send_to_visitor;
        }
        if (isset($BookingSettings->time_limit)) {                
            $DtoBookingSettings->time_limit = $BookingSettings->time_limit;
        }

        $DescriptionMessage = Message::where('id', $BookingSettings->booking_notification_id)->first()->code;
        if (isset($DescriptionMessage)) {
            $DtoBookingSettings->booking_notification_id = $DescriptionMessage;
        }else{
           $DtoBookingSettings->booking_notification_id = '';  
        }

        $DescriptionMessageb = Message::where('id', $BookingSettings->cancellation_notification_id)->first()->code;
        if (isset($DescriptionMessageb)) {
            $DtoBookingSettings->cancellation_notification_id = $DescriptionMessageb;
        }else{
           $DtoBookingSettings->cancellation_notification_id = '';  
        }
         $DescriptionMessagec = Message::where('id', $BookingSettings->confirmation_notification_id)->first()->code;
        if (isset($DescriptionMessagec)) {
            $DtoBookingSettings->confirmation_notification_id = $DescriptionMessagec;
        }else{
           $DtoBookingSettings->confirmation_notification_id = '';  
        }
        $DescriptionMessaged = Message::where('id', $BookingSettings->admin_notification_id)->first()->code;
        if (isset($DescriptionMessaged)) {
            $DtoBookingSettings->admin_notification_id = $DescriptionMessaged;
        }else{
           $DtoBookingSettings->admin_notification_id = '';  
        }

        if (isset($BookingSettings->check_user)) {                
            $DtoBookingSettings->check_user = $BookingSettings->check_user;
        }
        if (isset($BookingSettings->check_service)) {                
            $DtoBookingSettings->check_service = $BookingSettings->check_service;
        }
        if (isset($BookingSettings->check_states)) {                
            $DtoBookingSettings->check_states = $BookingSettings->check_states;
        }
        if (isset($BookingSettings->check_employee)) {                
            $DtoBookingSettings->check_employee = $BookingSettings->check_employee;
        }
        if (isset($BookingSettings->check_place)) {                
            $DtoBookingSettings->check_place = $BookingSettings->check_place;
        }
        if (isset($BookingSettings->check_price)) {                
            $DtoBookingSettings->check_price = $BookingSettings->check_price;
        }

        if (isset($BookingSettings->check_user_day)) {                
            $DtoBookingSettings->check_user_day = $BookingSettings->check_user_day;
        }
        if (isset($BookingSettings->check_service_day)) {                
            $DtoBookingSettings->check_service_day = $BookingSettings->check_service_day;
        }
        if (isset($BookingSettings->check_employee_day)) {                
            $DtoBookingSettings->check_employee_day = $BookingSettings->check_employee_day;
        }
        if (isset($BookingSettings->check_place_day)) {                
            $DtoBookingSettings->check_place_day = $BookingSettings->check_place_day;
        }
        if (isset($BookingSettings->check_price_day)) {                
            $DtoBookingSettings->check_price_day = $BookingSettings->check_price_day;
        }
        if (isset($BookingSettings->check_states_day)) {                
            $DtoBookingSettings->check_states_day = $BookingSettings->check_states_day;
        }
        if (isset($BookingSettings->order_user_week)) {                
            $DtoBookingSettings->order_user_week = $BookingSettings->order_user_week;
        }
        if (isset($BookingSettings->order_service_week)) {                
            $DtoBookingSettings->order_service_week = $BookingSettings->order_service_week;
        }
        if (isset($BookingSettings->order_employee_week)) {                
            $DtoBookingSettings->order_employee_week = $BookingSettings->order_employee_week;
        }
        if (isset($BookingSettings->order_place_week)) {                
            $DtoBookingSettings->order_place_week = $BookingSettings->order_place_week;
        }
        if (isset($BookingSettings->order_price_week)) {                
            $DtoBookingSettings->order_price_week = $BookingSettings->order_price_week;
        }
        if (isset($BookingSettings->order_states_week)) {                
            $DtoBookingSettings->order_states_week = $BookingSettings->order_states_week;
        }
        if (isset($BookingSettings->order_user_day)) {                
            $DtoBookingSettings->order_user_day = $BookingSettings->order_user_day;
        } 
        if (isset($BookingSettings->order_service_day)) {                
            $DtoBookingSettings->order_service_day = $BookingSettings->order_service_day;
        } 
        if (isset($BookingSettings->order_employee_day)) {                
            $DtoBookingSettings->order_employee_day = $BookingSettings->order_employee_day;
        } 
        if (isset($BookingSettings->order_place_day)) {                
            $DtoBookingSettings->order_place_day = $BookingSettings->order_place_day;
        } 
        if (isset($BookingSettings->order_price_day)) {                
            $DtoBookingSettings->order_price_day = $BookingSettings->order_price_day;
        } 
        if (isset($BookingSettings->order_states_day)) {                
            $DtoBookingSettings->order_states_day = $BookingSettings->order_states_day;
        }
        if (isset($BookingSettings->order_user_month)) {                
            $DtoBookingSettings->order_user_month = $BookingSettings->order_user_month;
        }
        if (isset($BookingSettings->order_service_month)) {                
            $DtoBookingSettings->order_service_month = $BookingSettings->order_service_month;
        }
        if (isset($BookingSettings->order_employee_month)) {                
            $DtoBookingSettings->order_employee_month = $BookingSettings->order_employee_month;
        }
        if (isset($BookingSettings->order_place_month)) {                
            $DtoBookingSettings->order_place_month = $BookingSettings->order_place_month;
        }
        if (isset($BookingSettings->order_price_month)) {                
            $DtoBookingSettings->order_price_month = $BookingSettings->order_price_month;
        }
        if (isset($BookingSettings->order_states_month)) {                
            $DtoBookingSettings->order_states_month = $BookingSettings->order_states_month;
        }
        if (isset($BookingSettings->check_user_week)) {                
            $DtoBookingSettings->check_user_week = $BookingSettings->check_user_week;
        }
        if (isset($BookingSettings->check_service_week)) {                
            $DtoBookingSettings->check_service_week = $BookingSettings->check_service_week;
        }
        if (isset($BookingSettings->check_employee_week)) {                
            $DtoBookingSettings->check_employee_week = $BookingSettings->check_employee_week;
        }
        if (isset($BookingSettings->check_place_week)) {                
            $DtoBookingSettings->check_place_week = $BookingSettings->check_place_week;
        }
        if (isset($BookingSettings->check_price_week)) {                
            $DtoBookingSettings->check_price_week = $BookingSettings->check_price_week;
        }
        if (isset($BookingSettings->check_states_week)) {                
            $DtoBookingSettings->check_states_week = $BookingSettings->check_states_week;
        }

        return $DtoBookingSettings;   
    }

    #endregion PRIVATE

    #region GET
    /**
     * Get by id
     * 
     * @param int id
     * @return DtoPlaces
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(BookingSettings::find($id));
    }

    /**
     * Get select
     *
     * @param String $search
     * @param int $idUser
     * 
     */
    public static function getSelect(string $search)
    {
                if ($search == "*") {
                    return BookingSettings::select('id', 'name as description')->get();
                } else {
                    return BookingSettings::where('name', 'ilike' , $search . '%')->select('id', 'name as description')->get();
          } 
    }


    /**
     * Get all
     * 
     * @return DtoPlaces
     */
    public static function getAll()
    {
        $result = collect();

        foreach (BookingSettings::orderBy('id','DESC')->get() as $BookingSettings) {    

                $DtoBookingSettings = new DtoBookingSettings();
            
                if (isset($BookingSettings->id)) {
                    $DtoBookingSettings->id = $BookingSettings->id;
                }
                if (isset($BookingSettings->nr_appointments)) {
                    $DtoBookingSettings->nr_appointments = $BookingSettings->nr_appointments;
                }
                if (isset($BookingSettings->send_email_to_user)) {
                    $DtoBookingSettings->send_email_to_user = $BookingSettings->send_email_to_user;
                }
                if (isset($BookingSettings->send_email_to_employee)) {
                    $DtoBookingSettings->send_email_to_employee = $BookingSettings->send_email_to_employee;
                }
                if (isset($BookingSettings->allow_cancellation)) {
                    $DtoBookingSettings->allow_cancellation = $BookingSettings->allow_cancellation;
                }
                if (isset($BookingSettings->acceptance_of_mandatory_conditions)) {
                    $DtoBookingSettings->acceptance_of_mandatory_conditions = $BookingSettings->acceptance_of_mandatory_conditions;
                }
                if (isset($BookingSettings->text_gdpr)) {
                    $DtoBookingSettings->text_gdpr = $BookingSettings->text_gdpr;
                }
                if (isset($BookingSettings->email_receives_notifications)) {
                    $DtoBookingSettings->email_receives_notifications = $BookingSettings->email_receives_notifications;
                }
                if (isset($BookingSettings->email_send_notifications)) {
                    $DtoBookingSettings->email_send_notifications = $BookingSettings->email_send_notifications;
                }
                if (isset($BookingSettings->notification_object_send_to_administrator)) {
                    $DtoBookingSettings->notification_object_send_to_administrator = $BookingSettings->notification_object_send_to_administrator;
                }
                if (isset($BookingSettings->notification_object_send_to_visitor)) {
                    $DtoBookingSettings->notification_object_send_to_visitor = $BookingSettings->notification_object_send_to_visitor;
                }
                if (isset($BookingSettings->time_limit)) {
                    $DtoBookingSettings->time_limit = $BookingSettings->time_limit;
                }

                //tabella:
                if (isset($BookingSettings->booking_notification_id)) {
                    $DtoBookingSettings->DescriptionNotificationBooking = Message::where('id', $BookingSettings->booking_notification_id)->first()->code;
                }
                if (isset($BookingSettings->cancellation_notification_id)) {
                    $DtoBookingSettings->CancellationNotification = Message::where('id', $BookingSettings->cancellation_notification_id)->first()->code;
                }
                if (isset($BookingSettings->confirmation_notification_id)) {
                    $DtoBookingSettings->ConfirmationNotification = Message::where('id', $BookingSettings->confirmation_notification_id)->first()->code;
                }
                if (isset($BookingSettings->admin_notification_id)) {
                    $DtoBookingSettings->AdminNotification = Message::where('id', $BookingSettings->admin_notification_id)->first()->code;
                }

                //dettaglio
                  if (isset($BookingSettings->booking_notification_id)) {
                    $DtoBookingSettings->booking_notification_id = $BookingSettings->booking_notification_id;
                }
                if (isset($BookingSettings->cancellation_notification_id)) {
                    $DtoBookingSettings->cancellation_notification_id = $BookingSettings->cancellation_notification_id;
                }
                if (isset($BookingSettings->confirmation_notification_id)) {
                    $DtoBookingSettings->confirmation_notification_id = $BookingSettings->confirmation_notification_id;
                }
                if (isset($BookingSettings->admin_notification_id)) {
                    $DtoBookingSettings->admin_notification_id = $BookingSettings->admin_notification_id;
                }
                if (isset($BookingSettings->check_user)) {
                    $DtoBookingSettings->check_user = $BookingSettings->check_user;
                }
                if (isset($BookingSettings->check_service)) {
                    $DtoBookingSettings->check_service = $BookingSettings->check_service;
                }
                if (isset($BookingSettings->check_employee)) {
                    $DtoBookingSettings->check_employee = $BookingSettings->check_employee;
                }
                if (isset($BookingSettings->check_place)) {
                    $DtoBookingSettings->check_place = $BookingSettings->check_place;
                }
                if (isset($BookingSettings->check_price)) {
                    $DtoBookingSettings->check_price = $BookingSettings->check_price;
                }
                if (isset($BookingSettings->check_states)) {
                    $DtoBookingSettings->check_states = $BookingSettings->check_states;
                }

                if (isset($BookingSettings->check_user_day)) {                
                    $DtoBookingSettings->check_user_day = $BookingSettings->check_user_day;
                }
                if (isset($BookingSettings->check_service_day)) {                
                    $DtoBookingSettings->check_service_day = $BookingSettings->check_service_day;
                }
                if (isset($BookingSettings->check_employee_day)) {                
                    $DtoBookingSettings->check_employee_day = $BookingSettings->check_employee_day;
                }
                if (isset($BookingSettings->check_place_day)) {                
                    $DtoBookingSettings->check_place_day = $BookingSettings->check_place_day;
                }
                if (isset($BookingSettings->check_price_day)) {                
                    $DtoBookingSettings->check_price_day = $BookingSettings->check_price_day;
                }
                if (isset($BookingSettings->check_states_day)) {                
                    $DtoBookingSettings->check_states_day = $BookingSettings->check_states_day;
                }
                if (isset($BookingSettings->order_user_week)) {                
                    $DtoBookingSettings->order_user_week = $BookingSettings->order_user_week;
                }
                if (isset($BookingSettings->order_service_week)) {                
                    $DtoBookingSettings->order_service_week = $BookingSettings->order_service_week;
                }
                if (isset($BookingSettings->order_employee_week)) {                
                    $DtoBookingSettings->order_employee_week = $BookingSettings->order_employee_week;
                }
                if (isset($BookingSettings->order_place_week)) {                
                    $DtoBookingSettings->order_place_week = $BookingSettings->order_place_week;
                }
                if (isset($BookingSettings->order_price_week)) {                
                    $DtoBookingSettings->order_price_week = $BookingSettings->order_price_week;
                }
                if (isset($BookingSettings->order_states_week)) {                
                    $DtoBookingSettings->order_states_week = $BookingSettings->order_states_week;
                }
                if (isset($BookingSettings->order_user_day)) {                
                    $DtoBookingSettings->order_user_day = $BookingSettings->order_user_day;
                } 
                if (isset($BookingSettings->order_service_day)) {                
                    $DtoBookingSettings->order_service_day = $BookingSettings->order_service_day;
                } 
                if (isset($BookingSettings->order_employee_day)) {                
                    $DtoBookingSettings->order_employee_day = $BookingSettings->order_employee_day;
                } 
                if (isset($BookingSettings->order_place_day)) {                
                    $DtoBookingSettings->order_place_day = $BookingSettings->order_place_day;
                } 
                if (isset($BookingSettings->order_price_day)) {                
                    $DtoBookingSettings->order_price_day = $BookingSettings->order_price_day;
                } 
                if (isset($BookingSettings->order_states_day)) {                
                    $DtoBookingSettings->order_states_day = $BookingSettings->order_states_day;
                }
                if (isset($BookingSettings->order_user_month)) {                
                    $DtoBookingSettings->order_user_month = $BookingSettings->order_user_month;
                }
                if (isset($BookingSettings->order_service_month)) {                
                    $DtoBookingSettings->order_service_month = $BookingSettings->order_service_month;
                }
                if (isset($BookingSettings->order_employee_month)) {                
                    $DtoBookingSettings->order_employee_month = $BookingSettings->order_employee_month;
                }
                if (isset($BookingSettings->order_place_month)) {                
                    $DtoBookingSettings->order_place_month = $BookingSettings->order_place_month;
                }
                if (isset($BookingSettings->order_price_month)) {                
                    $DtoBookingSettings->order_price_month = $BookingSettings->order_price_month;
                }
                if (isset($BookingSettings->order_states_month)) {                
                    $DtoBookingSettings->order_states_month = $BookingSettings->order_states_month;
                }
                if (isset($BookingSettings->check_user_week)) {                
                    $DtoBookingSettings->check_user_week = $BookingSettings->check_user_week;
                }
                if (isset($BookingSettings->check_service_week)) {                
                    $DtoBookingSettings->check_service_week = $BookingSettings->check_service_week;
                }
                if (isset($BookingSettings->check_employee_week)) {                
                    $DtoBookingSettings->check_employee_week = $BookingSettings->check_employee_week;
                }
                if (isset($BookingSettings->check_place_week)) {                
                    $DtoBookingSettings->check_place_week = $BookingSettings->check_place_week;
                }
                if (isset($BookingSettings->check_price_week)) {                
                    $DtoBookingSettings->check_price_week = $BookingSettings->check_price_week;
                }
                if (isset($BookingSettings->check_states_week)) {                
                    $DtoBookingSettings->check_states_week = $BookingSettings->check_states_week;
                }

                    $result->push($DtoBookingSettings);
                }

                return $result;

                }
    #endregion GET

    #region INSERT
    /**
     * Insert
     * 
     * @param Request DtoBookingSettings
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoBookingSettings, int $idUser)
    {
       DB::beginTransaction();
        try {
            $BookingSettings = new BookingSettings();
            
        if (isset($DtoBookingSettings->nr_appointments)) {
            $BookingSettings ->nr_appointments = $DtoBookingSettings->nr_appointments;
        } else{
            $BookingSettings->nr_appointments = NULL;
        }
        if (isset($DtoBookingSettings->send_email_to_user)) {
            $BookingSettings ->send_email_to_user = $DtoBookingSettings->send_email_to_user;
        } 
        if (isset($DtoBookingSettings->send_email_to_employee)) {
            $BookingSettings ->send_email_to_employee = $DtoBookingSettings->send_email_to_employee;
        } 
         if (isset($DtoBookingSettings->allow_cancellation)) {
            $BookingSettings ->allow_cancellation = $DtoBookingSettings->allow_cancellation;
        } 
          if (isset($DtoBookingSettings->acceptance_of_mandatory_conditions)) {
            $BookingSettings ->acceptance_of_mandatory_conditions = $DtoBookingSettings->acceptance_of_mandatory_conditions;
        } 
         if (isset($DtoBookingSettings->text_gdpr)) {
            $BookingSettings ->text_gdpr = $DtoBookingSettings->text_gdpr;
        } else{
            $BookingSettings->text_gdpr = NULL;
        }
         if (isset($DtoBookingSettings->email_receives_notifications)) {
            $BookingSettings ->email_receives_notifications = $DtoBookingSettings->email_receives_notifications;
        } else{
            $BookingSettings->email_receives_notifications = NULL;
        }
        if (isset($DtoBookingSettings->email_send_notifications)) {
            $BookingSettings ->email_send_notifications = $DtoBookingSettings->email_send_notifications;
        } else{
            $BookingSettings->email_send_notifications = NULL;
        }
         if (isset($DtoBookingSettings->notification_object_send_to_administrator)) {
            $BookingSettings ->notification_object_send_to_administrator = $DtoBookingSettings->notification_object_send_to_administrator;
        }else{
            $BookingSettings->notification_object_send_to_administrator = NULL;
        } 
         if (isset($DtoBookingSettings->notification_object_send_to_visitor)) {
            $BookingSettings ->notification_object_send_to_visitor = $DtoBookingSettings->notification_object_send_to_visitor;
        } else{
            $BookingSettings->notification_object_send_to_visitor = NULL;
        } 
        if (isset($DtoBookingSettings->time_limit)) {
            $BookingSettings ->time_limit = $DtoBookingSettings->time_limit;
        } else{
            $BookingSettings->time_limit = NULL;
        } 
        if (isset($DtoBookingSettings->booking_notification_id)) {
            $BookingSettings ->booking_notification_id = $DtoBookingSettings->booking_notification_id;
        } 
        if (isset($DtoBookingSettings->cancellation_notification_id)) {
            $BookingSettings ->cancellation_notification_id = $DtoBookingSettings->cancellation_notification_id;
        } 
        if (isset($DtoBookingSettings->confirmation_notification_id)) {
            $BookingSettings ->confirmation_notification_id = $DtoBookingSettings->confirmation_notification_id;
        }
         if (isset($DtoBookingSettings->admin_notification_id)) {
            $BookingSettings ->admin_notification_id = $DtoBookingSettings->admin_notification_id;
        }
        if (isset($DtoBookingSettings->check_user)) {
            $BookingSettings ->check_user = $DtoBookingSettings->check_user;
        }
        if (isset($DtoBookingSettings->check_service)) {
            $BookingSettings ->check_service = $DtoBookingSettings->check_service;
        }
        if (isset($DtoBookingSettings->check_employee)) {
            $BookingSettings ->check_employee = $DtoBookingSettings->check_employee;
        }
        if (isset($DtoBookingSettings->check_place)) {
            $BookingSettings ->check_place = $DtoBookingSettings->check_place;
        }
        if (isset($DtoBookingSettings->check_price)) {
            $BookingSettings ->check_price = $DtoBookingSettings->check_price;
        }
        if (isset($DtoBookingSettings->check_states)) {
            $BookingSettings ->check_states = $DtoBookingSettings->check_states;
        }
        if (isset($DtoBookingSettings->check_user_day)) {                
            $BookingSettings->check_user_day = $DtoBookingSettings->check_user_day;
        }
        if (isset($DtoBookingSettings->check_service_day)) {                
            $BookingSettings->check_service_day = $DtoBookingSettings->check_service_day;
        }
        if (isset($DtoBookingSettings->check_employee_day)) {                
            $BookingSettings->check_employee_day = $DtoBookingSettings->check_employee_day;
        }
        if (isset($DtoBookingSettings->check_place_day)) {                
            $BookingSettings->check_place_day = $DtoBookingSettings->check_place_day;
        }
        if (isset($DtoBookingSettings->check_price_day)) {                
            $BookingSettings->check_price_day = $DtoBookingSettings->check_price_day;
        }
        if (isset($DtoBookingSettings->check_states_day)) {                
            $BookingSettings->check_states_day = $DtoBookingSettings->check_states_day;
        }
        if (isset($DtoBookingSettings->order_user_week)) {                
            $BookingSettings->order_user_week = $DtoBookingSettings->order_user_week;
        }else{
            $BookingSettings->order_user_week = NULL;
        } 
        if (isset($DtoBookingSettings->order_service_week)) {                
            $BookingSettings->order_service_week = $DtoBookingSettings->order_service_week;
        }else{
            $BookingSettings->order_service_week = NULL;
        } 
        if (isset($DtoBookingSettings->order_employee_week)) {                
            $BookingSettings->order_employee_week = $DtoBookingSettings->order_employee_week;
        }else{
            $BookingSettings->order_employee_week = NULL;
        } 
        if (isset($DtoBookingSettings->order_place_week)) {                
            $BookingSettings->order_place_week = $DtoBookingSettings->order_place_week;
        }else{
            $BookingSettings->order_place_week = NULL;
        } 
        if (isset($DtoBookingSettings->order_price_week)) {                
            $BookingSettings->order_price_week = $DtoBookingSettings->order_price_week;
        }else{
            $BookingSettings->order_price_week = NULL;
        } 
        if (isset($DtoBookingSettings->order_states_week)) {                
            $BookingSettings->order_states_week = $DtoBookingSettings->order_states_week;
        }else{
            $BookingSettings->order_states_week = NULL;
        } 
        if (isset($DtoBookingSettings->order_user_day)) {                
            $BookingSettings->order_user_day = $DtoBookingSettings->order_user_day;
        } else{
            $BookingSettings->order_user_day = NULL;
        } 
        if (isset($DtoBookingSettings->order_service_day)) {                
            $BookingSettings->order_service_day = $DtoBookingSettings->order_service_day;
        }else{
            $BookingSettings->order_service_day = NULL;
        }  
        if (isset($DtoBookingSettings->order_employee_day)) {                
            $BookingSettings->order_employee_day = $DtoBookingSettings->order_employee_day;
        } else{
            $BookingSettings->order_employee_day = NULL;
        }  
        if (isset($DtoBookingSettings->order_place_day)) {                
            $BookingSettings->order_place_day = $DtoBookingSettings->order_place_day;
        } else{
            $BookingSettings->order_place_day = NULL;
        }  
        if (isset($DtoBookingSettings->order_price_day)) {                
            $BookingSettings->order_price_day = $DtoBookingSettings->order_price_day;
        }else{
            $BookingSettings->order_price_day = NULL;
        }   
        if (isset($DtoBookingSettings->order_states_day)) {                
            $BookingSettings->order_states_day = $DtoBookingSettings->order_states_day;
        }else{
            $BookingSettings->order_states_day = NULL;
        } 
        if (isset($DtoBookingSettings->order_user_month)) {                
            $BookingSettings->order_user_month = $DtoBookingSettings->order_user_month;
        }else{
            $BookingSettings->order_user_month = NULL;
        } 
        if (isset($DtoBookingSettings->order_service_month)) {                
            $BookingSettings->order_service_month = $DtoBookingSettings->order_service_month;
        }else{
            $BookingSettings->order_service_month = NULL;
        } 
        if (isset($DtoBookingSettings->order_employee_month)) {                
            $BookingSettings->order_employee_month = $DtoBookingSettings->order_employee_month;
        }else{
            $BookingSettings->order_employee_month = NULL;
        } 
        if (isset($DtoBookingSettings->order_place_month)) {                
            $BookingSettings->order_place_month = $DtoBookingSettings->order_place_month;
        }else{
            $BookingSettings->order_place_month = NULL;
        } 
        if (isset($DtoBookingSettings->order_price_month)) {                
            $BookingSettings->order_price_month = $DtoBookingSettings->order_price_month;
        }else{
            $BookingSettings->order_price_month = NULL;
        } 
        if (isset($DtoBookingSettings->order_states_month)) {                
            $BookingSettings->order_states_month = $DtoBookingSettings->order_states_month;
        }else{
            $BookingSettings->order_states_month = NULL;
        } 
        if (isset($DtoBookingSettings->check_user_week)) {                
            $BookingSettings->check_user_week = $DtoBookingSettings->check_user_week;
        }
        if (isset($DtoBookingSettings->check_service_week)) {                
            $BookingSettings->check_service_week = $DtoBookingSettings->check_service_week;
        }
         if (isset($DtoBookingSettings->check_employee_week)) {                
            $BookingSettings->check_employee_week = $DtoBookingSettings->check_employee_week;
        }
        if (isset($DtoBookingSettings->check_place_week)) {                
            $BookingSettings->check_place_week = $DtoBookingSettings->check_place_week;
        }
        if (isset($DtoBookingSettings->check_price_week)) {                
            $BookingSettings->check_price_week = $DtoBookingSettings->check_price_week;
        }
        if (isset($DtoBookingSettings->check_states_week)) {                
            $BookingSettings->check_states_week = $DtoBookingSettings->check_states_week;
        }
        $BookingSettings->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $BookingSettings->id;
    }

    #endregion INSERT

    /**
     * Update
     * 
     * @param Request [DtoBookingSettings]
     * @param int idLanguage
     */
    public static function update(Request $DtoBookingSettings, int $idUser)
    {
       
        $BookingSettingsOnDb = BookingSettings::find($DtoBookingSettings->id);
      
        DB::beginTransaction();

        try {
            if (isset($DtoBookingSettings->id)) {
                $BookingSettingsOnDb->id = $DtoBookingSettings->id;
            } 
             if (isset($DtoBookingSettings->nr_appointments)) {
                $BookingSettingsOnDb->nr_appointments = $DtoBookingSettings->nr_appointments;
            } else{
                $BookingSettingsOnDb->nr_appointments = NULL;
            }
            if (isset($DtoBookingSettings->send_email_to_user)) {
                $BookingSettingsOnDb->send_email_to_user = $DtoBookingSettings->send_email_to_user;
            } 
              if (isset($DtoBookingSettings->send_email_to_employee)) {
                $BookingSettingsOnDb->send_email_to_employee = $DtoBookingSettings->send_email_to_employee;
            } 
            if (isset($DtoBookingSettings->allow_cancellation)) {
                $BookingSettingsOnDb->allow_cancellation = $DtoBookingSettings->allow_cancellation;
            } 
            if (isset($DtoBookingSettings->acceptance_of_mandatory_conditions)) {
                $BookingSettingsOnDb->acceptance_of_mandatory_conditions = $DtoBookingSettings->acceptance_of_mandatory_conditions;
            }
            if (isset($DtoBookingSettings->text_gdpr)) {
                $BookingSettingsOnDb->text_gdpr = $DtoBookingSettings->text_gdpr;
            }else{
                $BookingSettingsOnDb->text_gdpr = NULL;
            }
            if (isset($DtoBookingSettings->email_receives_notifications)) {
                $BookingSettingsOnDb->email_receives_notifications = $DtoBookingSettings->email_receives_notifications;
            }else{
                $BookingSettingsOnDb->email_receives_notifications = NULL;
            }
            if (isset($DtoBookingSettings->email_send_notifications)) {
                $BookingSettingsOnDb->email_send_notifications = $DtoBookingSettings->email_send_notifications;
            }else{
                $BookingSettingsOnDb->email_receives_notifications = NULL;
            }
            if (isset($DtoBookingSettings->notification_object_send_to_administrator)) {
                $BookingSettingsOnDb->notification_object_send_to_administrator = $DtoBookingSettings->notification_object_send_to_administrator;
            }else{
                $BookingSettingsOnDb->notification_object_send_to_administrator = NULL;
            }
            if (isset($DtoBookingSettings->notification_object_send_to_visitor)) {
                $BookingSettingsOnDb->notification_object_send_to_visitor = $DtoBookingSettings->notification_object_send_to_visitor;
            }else{
                $BookingSettingsOnDb->notification_object_send_to_visitor = NULL;
            }
            if (isset($DtoBookingSettings->time_limit)) {
                $BookingSettingsOnDb->time_limit = $DtoBookingSettings->time_limit;
            }else{
                $BookingSettingsOnDb->time_limit = NULL;
            }
            if (isset($DtoBookingSettings->booking_notification_id)) {
                $BookingSettingsOnDb->booking_notification_id = $DtoBookingSettings->booking_notification_id;
            }
            if (isset($DtoBookingSettings->cancellation_notification_id)) {
                $BookingSettingsOnDb->cancellation_notification_id = $DtoBookingSettings->cancellation_notification_id;
            }
            if (isset($DtoBookingSettings->confirmation_notification_id)) {
                $BookingSettingsOnDb->confirmation_notification_id = $DtoBookingSettings->confirmation_notification_id;
            }
            if (isset($DtoBookingSettings->admin_notification_id)) {
                $BookingSettingsOnDb->admin_notification_id = $DtoBookingSettings->admin_notification_id;
            }
            if (isset($DtoBookingSettings->check_user)) {
                $BookingSettingsOnDb->check_user = $DtoBookingSettings->check_user;
            }
            if (isset($DtoBookingSettings->check_service)) {
                $BookingSettingsOnDb->check_service = $DtoBookingSettings->check_service;
            }
            if (isset($DtoBookingSettings->check_employee)) {
                $BookingSettingsOnDb->check_employee = $DtoBookingSettings->check_employee;
            }
            if (isset($DtoBookingSettings->check_place)) {
                $BookingSettingsOnDb->check_place = $DtoBookingSettings->check_place;
            }
            if (isset($DtoBookingSettings->check_price)) {
                $BookingSettingsOnDb->check_price = $DtoBookingSettings->check_price;
            }
            if (isset($DtoBookingSettings->check_states)) {
                $BookingSettingsOnDb->check_states = $DtoBookingSettings->check_states;
            }
            if (isset($DtoBookingSettings->check_user_day)) {                
                $BookingSettingsOnDb->check_user_day = $DtoBookingSettings->check_user_day;
            }
            if (isset($DtoBookingSettings->check_service_day)) {                
                $BookingSettingsOnDb->check_service_day = $DtoBookingSettings->check_service_day;
            }
            if (isset($DtoBookingSettings->check_employee_day)) {                
                $BookingSettingsOnDb->check_employee_day = $DtoBookingSettings->check_employee_day;
            }
            if (isset($DtoBookingSettings->check_place_day)) {                
                $BookingSettingsOnDb->check_place_day = $DtoBookingSettings->check_place_day;
            }
            if (isset($DtoBookingSettings->check_price_day)) {                
                $BookingSettingsOnDb->check_price_day = $DtoBookingSettings->check_price_day;
            }
            if (isset($DtoBookingSettings->check_states_day)) {                
                $BookingSettingsOnDb->check_states_day = $DtoBookingSettings->check_states_day;
            }
            if (isset($DtoBookingSettings->order_user_week)) {                
                $BookingSettingsOnDb->order_user_week = $DtoBookingSettings->order_user_week;
            }else{
                $BookingSettingsOnDb->order_user_week = NULL;
            }
            if (isset($DtoBookingSettings->order_service_week)) {                
                $BookingSettingsOnDb->order_service_week = $DtoBookingSettings->order_service_week;
            }else{
                $BookingSettingsOnDb->order_service_week = NULL;
            }
            if (isset($DtoBookingSettings->order_employee_week)) {                
                $BookingSettingsOnDb->order_employee_week = $DtoBookingSettings->order_employee_week;
            }else{
                $BookingSettingsOnDb->order_employee_week = NULL;
            }
            if (isset($DtoBookingSettings->order_place_week)) {                
                $BookingSettingsOnDb->order_place_week = $DtoBookingSettings->order_place_week;
            }else{
                $BookingSettingsOnDb->order_place_week = NULL;
            }
            if (isset($DtoBookingSettings->order_price_week)) {                
                $BookingSettingsOnDb->order_price_week = $DtoBookingSettings->order_price_week;
            }else{
                $BookingSettingsOnDb->order_price_week = NULL;
            }
            if (isset($DtoBookingSettings->order_states_week)) {                
                $BookingSettingsOnDb->order_states_week = $DtoBookingSettings->order_states_week;
            }else{
                $BookingSettingsOnDb->order_states_week = NULL;
            }
            if (isset($DtoBookingSettings->order_user_day)) {                
                $BookingSettingsOnDb->order_user_day = $DtoBookingSettings->order_user_day;
            } else{
                $BookingSettingsOnDb->order_user_day = NULL;
            }
            if (isset($DtoBookingSettings->order_service_day)) {                
                $BookingSettingsOnDb->order_service_day = $DtoBookingSettings->order_service_day;
            }else{
                $BookingSettingsOnDb->order_service_day = NULL;
            } 
            if (isset($DtoBookingSettings->order_employee_day)) {                
                $BookingSettingsOnDb->order_employee_day = $DtoBookingSettings->order_employee_day;
            } else{
                $BookingSettingsOnDb->order_employee_day = NULL;
            } 
            if (isset($DtoBookingSettings->order_place_day)) {                
                $BookingSettingsOnDb->order_place_day = $DtoBookingSettings->order_place_day;
            } else{
                $BookingSettingsOnDb->order_place_day = NULL;
            } 
            if (isset($DtoBookingSettings->order_price_day)) {                
                $BookingSettingsOnDb->order_price_day = $DtoBookingSettings->order_price_day;
            } else{
                $BookingSettingsOnDb->order_price_day = NULL;
            } 
            if (isset($DtoBookingSettings->order_states_day)) {                
                $BookingSettingsOnDb->order_states_day = $DtoBookingSettings->order_states_day;
            }else{
                $BookingSettingsOnDb->order_states_day = NULL;
            } 
            if (isset($DtoBookingSettings->order_user_month)) {                
                $BookingSettingsOnDb->order_user_month = $DtoBookingSettings->order_user_month;
            }else{
                $BookingSettingsOnDb->order_user_month = NULL;
            } 
            if (isset($DtoBookingSettings->order_service_month)) {                
                $BookingSettingsOnDb->order_service_month = $DtoBookingSettings->order_service_month;
            }else{
                $BookingSettingsOnDb->order_service_month = NULL;
            } 
            if (isset($DtoBookingSettings->order_employee_month)) {                
                $BookingSettingsOnDb->order_employee_month = $DtoBookingSettings->order_employee_month;
            }else{
                $BookingSettingsOnDb->order_employee_month = NULL;
            } 
            if (isset($DtoBookingSettings->order_place_month)) {                
                $BookingSettingsOnDb->order_place_month = $DtoBookingSettings->order_place_month;
            }else{
                $BookingSettingsOnDb->order_place_month = NULL;
            }
            if (isset($DtoBookingSettings->order_price_month)) {                
                $BookingSettingsOnDb->order_price_month = $DtoBookingSettings->order_price_month;
            }else{
                $BookingSettingsOnDb->order_price_month = NULL;
            }
            if (isset($DtoBookingSettings->order_states_month)) {                
                $BookingSettingsOnDb->order_states_month = $DtoBookingSettings->order_states_month;
            }else{
                $BookingSettingsOnDb->order_states_month = NULL;
            }
            if (isset($DtoBookingSettings->check_user_week)) {                
                $BookingSettingsOnDb->check_user_week = $DtoBookingSettings->check_user_week;
            }
            if (isset($DtoBookingSettings->check_service_week)) {                
                $BookingSettingsOnDb->check_service_week = $DtoBookingSettings->check_service_week;
            }
            if (isset($DtoBookingSettings->check_employee_week)) {                
                $BookingSettingsOnDb->check_employee_week = $DtoBookingSettings->check_employee_week;
            }
            if (isset($DtoBookingSettings->check_place_week)) {                
                $BookingSettingsOnDb->check_place_week = $DtoBookingSettings->check_place_week;
            }
            if (isset($DtoBookingSettings->check_price_week)) {                
                $BookingSettingsOnDb->check_price_week = $DtoBookingSettings->check_price_week;
            }
            if (isset($DtoBookingSettings->check_states_week)) {                
                $BookingSettingsOnDb->check_states_week = $DtoBookingSettings->check_states_week;
            }

            $BookingSettingsOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return  $DtoBookingSettings->id;
    }
    #endregion UPDATE


        /**
             * Clone
             * 
             * @param int id
             * @param int idFunctionality
             * @param int idLanguage
             * @param int idUser
             * 
             * @return int
             */
            public static function clone(int $id, int $idFunctionality, int $idUser, int $idLanguage)
            {
            
                $BookingSettings = BookingSettings::find($id);            
                DB::beginTransaction();

                try {

                    $newBookingSettings = new BookingSettings();
                    $newBookingSettings->nr_appointments = $BookingSettings->nr_appointments;
                    $newBookingSettings->send_email_to_user = $BookingSettings->send_email_to_user;
                    $newBookingSettings->send_email_to_employee = $BookingSettings->send_email_to_employee;
                    $newBookingSettings->allow_cancellation = $BookingSettings->allow_cancellation;
                    $newBookingSettings->acceptance_of_mandatory_conditions = $BookingSettings->acceptance_of_mandatory_conditions;
                    $newBookingSettings->text_gdpr = $BookingSettings->text_gdpr;
                    $newBookingSettings->email_receives_notifications = $BookingSettings->email_receives_notifications . ' - Copy';
                    $newBookingSettings->email_send_notifications = $BookingSettings->email_send_notifications;
                    $newBookingSettings->notification_object_send_to_administrator = $BookingSettings->notification_object_send_to_administrator;
                    $newBookingSettings->notification_object_send_to_visitor = $BookingSettings->notification_object_send_to_visitor;
                    $newBookingSettings->time_limit = $BookingSettings->time_limit;
                    $newBookingSettings->booking_notification_id = $BookingSettings->booking_notification_id;
                    $newBookingSettings->cancellation_notification_id = $BookingSettings->cancellation_notification_id;
                    $newBookingSettings->confirmation_notification_id = $BookingSettings->confirmation_notification_id;
                    $newBookingSettings->admin_notification_id = $BookingSettings->admin_notification_id; 

                    $newBookingSettings->check_user = $BookingSettings->check_user; 
                    $newBookingSettings->check_service = $BookingSettings->check_service; 
                    $newBookingSettings->check_employee = $BookingSettings->check_employee; 
                    $newBookingSettings->check_place = $BookingSettings->check_place; 
                    $newBookingSettings->check_price = $BookingSettings->check_price; 
                    $newBookingSettings->check_states = $BookingSettings->check_states; 

                    $newBookingSettings->check_user_week = $BookingSettings->check_user_week; 
                    $newBookingSettings->check_service_week = $BookingSettings->check_service_week; 
                    $newBookingSettings->check_employee_week = $BookingSettings->check_employee_week; 
                    $newBookingSettings->check_place_week = $BookingSettings->check_place_week; 
                    $newBookingSettings->check_price_week = $BookingSettings->check_price_week; 
                    $newBookingSettings->check_states_week = $BookingSettings->check_states_week; 

                    $newBookingSettings->check_user_day = $BookingSettings->check_user_day;
                    $newBookingSettings->check_service_day = $BookingSettings->check_service_day;                  
                    $newBookingSettings->check_employee_day = $BookingSettings->check_employee_day;                   
                    $newBookingSettings->check_place_day = $BookingSettings->check_place_day;          
                    $newBookingSettings->check_price_day = $BookingSettings->check_price_day;                
                    $newBookingSettings->check_states_day = $BookingSettings->check_states_day;                
                    $newBookingSettings->order_user_week = $BookingSettings->order_user_week;              
                    $newBookingSettings->order_service_week = $BookingSettings->order_service_week;                   
                    $newBookingSettings->order_employee_week = $BookingSettings->order_employee_week;        
                    $newBookingSettings->order_place_week = $BookingSettings->order_place_week;                    
                    $newBookingSettings->order_price_week = $BookingSettings->order_price_week;                   
                    $newBookingSettings->order_states_week = $BookingSettings->order_states_week;                 
                    $newBookingSettings->order_user_day = $BookingSettings->order_user_day;                   
                    $newBookingSettings->order_service_day = $BookingSettings->order_service_day;                 
                    $newBookingSettings->order_employee_day = $BookingSettings->order_employee_day;                    
                    $newBookingSettings->order_place_day = $BookingSettings->order_place_day;        
                    $newBookingSettings->order_price_day = $BookingSettings->order_price_day;                    
                    $newBookingSettings->order_states_day = $BookingSettings->order_states_day;                    
                    $newBookingSettings->order_user_month = $BookingSettings->order_user_month;                    
                    $newBookingSettings->order_service_month = $BookingSettings->order_service_month;                   
                    $newBookingSettings->order_employee_month = $BookingSettings->order_employee_month;                   
                    $newBookingSettings->order_place_month = $BookingSettings->order_place_month;                      
                    $newBookingSettings->order_price_month = $BookingSettings->order_price_month;                    
                    $newBookingSettings->order_states_month = $BookingSettings->order_states_month;


                    $newBookingSettings->save();
   
                    DB::commit();
                    return $newBookingSettings->id;
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
            }
        #endregion CLONE

            #region DELETE
                    /**
                     * Delete
                     * 
                     * @param int id
                     * @param int idLanguage
                     */
                    public static function delete(int $id, int $idLanguage)
                    {
                        $BookingSettings = BookingSettings::find($id);                   

                        if (is_null($BookingSettings)) {
                            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PeopleRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
                        }
                        DB::beginTransaction();
                        try {
                            $BookingSettings->delete();
                            
                            DB::commit();
                        } catch (Exception $ex) {
                            DB::rollback();
                            throw $ex;
                        }
                    }
                
                }
        #endregion DELETE
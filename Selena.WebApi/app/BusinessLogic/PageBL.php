<?php

namespace App\BusinessLogic;

use App\CategoryLanguage;
use App\DtoModel\DtoBlogNewsData;
use App\DtoModel\DtoConfiguration;
use App\DtoModel\DtoLanguagePageSetting;
use App\DtoModel\DtoLastActivity;
use App\DtoModel\DtoPageList;
use App\DtoModel\DtoPageMenuList;
use App\DtoModel\DtoPageRender;
use App\DtoModel\DtoPageSetting;
use App\DtoModel\DtoPageSitemap;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Enums\MenusTypesEnum;
use App\Enums\PageTypesEnum;
use Intervention\Image\Facades\Image;
use App\Functionality;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use App\Helpers\ModelHelper;
use App\Content;
use App\ContentLanguage;
use App\CartDetail;

use App\DtoModel\DtoImage;
use App\Icon;
use App\Language;
use App\LanguagePage;
use App\Page;
use App\PageCategory;
use App\PageShareType;
use App\PagePageCategory;
use App\LanguagePageCategory;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Menu;
use App\Helpers\DBHelper;
use App\DtoModel\DtoResult;
use Illuminate\Support\Facades\Log;
use App\DtoModel\DtoUrlList;
use App\DtoModel\DtoUrlListDetail;
use App\Enums\SettingEnum;
use Carbon\Carbon;
use App\Item;
use App\ItemLanguage;
use App\CategoryItem;
use App\ImageResize;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local as Adapter;
use stdClass;

use Exception;
use Soundasleep\Html2Text;

class PageBL
{

    public static $dirImageAttachment = '../storage/app/public/images/';
    public static $dirImageAttachmentImages = '../storage/app/public/images';


    #region PRIVATE

    /**
     * Return page to render
     *
     * @param string url
     * @param int idLanguage
     * 
     * @return DBRow
     */
    private static function _getRender(string $url, int $idLanguage)
    {  
        if(strpos($url, '_preview') !== false){
            $urlNew = str_replace('_preview', '', $url);

            return DB::select(
                'SELECT *, users.name as Autore, Title as Titolo, page_type as TipoPagina
                                FROM pages
                                INNER JOIN languages_pages on pages.id = languages_pages.page_id
                                left outer join users on pages.author_id = users.id 
                                WHERE pages.page_type <> \'Link\' AND languages_pages.id = COALESCE(
                                                    (   SELECT languages_pages.id
                                                        FROM pages
                                                        INNER JOIN languages_pages on pages.id = languages_pages.page_id
                                                        WHERE languages_pages.content IS NOT NULL
                                                        AND languages_pages.url = ? 
                                                        AND languages_pages.language_id = ?) ,
                                                    (   SELECT languages_pages.id
                                                        FROM pages
                                                        INNER JOIN languages_pages on pages.id = languages_pages.page_id
                                                        WHERE languages_pages.language_id = ( SELECT l.id
                                                                                            FROM languages l
                                                                                            WHERE l.default = true )
                                                        AND pages.id = (    SELECT page_id
                                                                            FROM languages_pages
                                                                            WHERE url = ? AND languages_pages.language_id = ? limit 1) ) ) ',
                [$urlNew, $idLanguage, $urlNew, $idLanguage]
            );
        }else{
            return DB::select(
                'SELECT *, users.name as Autore, Title as Titolo, page_type as TipoPagina
                                FROM pages
                                INNER JOIN languages_pages on pages.id = languages_pages.page_id
                                left outer join users on pages.author_id = users.id 
                                WHERE pages.page_type <> \'Link\' AND languages_pages.id = COALESCE(
                                                    (   SELECT languages_pages.id
                                                        FROM pages
                                                        INNER JOIN languages_pages on pages.id = languages_pages.page_id
                                                        WHERE pages.online = true
                                                        AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() )
                                                        AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() )
                                                        AND languages_pages.content IS NOT NULL
                                                        AND languages_pages.url = ? 
                                                        AND languages_pages.language_id = ?) ,
                                                    (   SELECT languages_pages.id
                                                        FROM pages
                                                        INNER JOIN languages_pages on pages.id = languages_pages.page_id
                                                        WHERE online = true
                                                        AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() )
                                                        AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() )
                                                        AND languages_pages.language_id = ( SELECT l.id
                                                                                            FROM languages l
                                                                                            WHERE l.default = true )
                                                        AND pages.id = (    SELECT page_id
                                                                            FROM languages_pages
                                                                            WHERE url = ? AND languages_pages.language_id = ? limit 1) ) ) ',
                [$url, $idLanguage, $url, $idLanguage]
            );
        }
        
    }

    /**
     * Convert image size
     * 
     * @param int byte
     *  
     */
    private static function _convertImageSize(int $bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }


    /**
     * render paginator by parameter of max page and page selected
     * 
     * @param int pageSelected
     * @param int maxPage
     * @param bool paginationActive
     * @param string urlOriginal
     *  
     */
    private static function _getRenderPaginator(int $pageSelected, int $maxPage, $paginationActive, $urlOriginal)
    {
        $html = '';
        $htmlPaginator = '';
        $minus10Page = 1;
        $plus10Page = 1;
        $minus1Page = 1;
        $plus1Page = 1;
        $msgSearch = 'tutto il catalogo';
        

        /* funzione gestione paginatore */
        if ($paginationActive) {
            $htmlPaginator = $htmlPaginator . "<ul class='pagination'>";
            /* controllo freccia -10 pagine */
            if ($pageSelected - 10 <= 1) {
                $minus10Page = 1;
            } else {
                $minus10Page = $pageSelected - 10;
            }

            $htmlPaginator = $htmlPaginator . '<li class="page-item" data-page="' . $minus10Page . '"><a href="' . $urlOriginal . '?page=' . $minus10Page . '"><i class="fa fa-angle-double-left"></i></a></li>';
            /* end controllo freccia -10 pagine */
            /* controllo freccia -1 pagine */
            if ($pageSelected - 1 <= 1) {
                $minus1Page = 1;
            } else {
                $minus1Page = $pageSelected - 1;
            }

            $htmlPaginator = $htmlPaginator . '<li class="page-item" data-page="' . $minus1Page . '"><a href="' . $urlOriginal . '?page=' . $minus1Page . '"><i class="fa fa-angle-left"></i></a></li>';
            /* end controllo freccia -1 pagine */

            /* controllo per pagine intermedie */
            if ($maxPage > 4) {
                if ($pageSelected > 1) {
                    if ($pageSelected + 2 > $maxPage) {
                        if ($pageSelected == $maxPage) {
                            for ($j = $pageSelected - 3; $j <= $maxPage; $j++) {
                                if ($pageSelected == $j) {
                                    $htmlPaginator = $htmlPaginator . '<li class="page-item active" data-page="' . $j . '"><a href="' . $urlOriginal . '?page=' . $j . '">' . $j . '</a></li>';
                                } else {
                                    $htmlPaginator = $htmlPaginator . '<li class="page-item" data-page="'  . $j . '"><a href="' . $urlOriginal . '?page=' . $j . '">' . $j . '</a></li>';
                                }
                            }
                        } else {
                            for ($j = $pageSelected - 2; $j <= $maxPage; $j++) {
                                if ($pageSelected == $j) {
                                    $htmlPaginator = $htmlPaginator . '<li class="page-item active" data-page="' . $j . '"><a href="' . $urlOriginal . '?page=' . $j . '">' . $j . '</a></li>';
                                } else {
                                    $htmlPaginator = $htmlPaginator . '<li class="page-item" data-page="' . $j . '"><a href="' . $urlOriginal . '?page=' . $j . '">' . $j . '</a></li>';
                                }
                            }
                        }
                    } else {
                        for ($j = $pageSelected - 1; $j <= $pageSelected + 2; $j++) {
                            if ($pageSelected == $j) {
                                $htmlPaginator = $htmlPaginator . '<li class="page-item active" data-page="' . $j . '"><a href="' . $urlOriginal . '?page=' . $j . '">' . $j .'</a></li>';
                            } else {
                                $htmlPaginator = $htmlPaginator . '<li class="page-item" data-page="' . $j . '"><a href="' . $urlOriginal . '?page=' . $j . '">' . $j . '</a></li>';
                            }
                        }
                    }
                } else {
                    for ($j = 1; $j <= 4; $j++) {
                        if ($pageSelected == $j) {
                            $htmlPaginator = $htmlPaginator . '<li class="page-item active" data-page="' . $j . '"><a href="' . $urlOriginal . '?page=' . $j . '">' . $j . '</a></li>';
                        } else {
                            $htmlPaginator = $htmlPaginator . '<li class="page-item" data-page="' . $j . '"><a href="' . $urlOriginal . '?page=' . $j . '">' . $j . '</a></li>';
                        }
                    }
                }
            } else {
                for ($j = 1; $j <= $maxPage; $j++) {
                    if ($pageSelected == $j) {
                        $htmlPaginator = $htmlPaginator . '<li class="page-item active" data-page="' . $j  . '"><a href="' . $urlOriginal . '?page=' . $j . '">' . $j  . '</a></li>';
                    } else {
                        $htmlPaginator = $htmlPaginator . '<li class="page-item" data-page="' . $j . '"><a href="' . $urlOriginal . '?page=' . $j . '">' . $j  . '</a></li>';
                    }
                }
            }
            /* end controllo per pagine intermedie */
            /* controllo freccia + 1 pagine */
            if ($pageSelected + 1 >= $maxPage) {
                $plus1Page = $maxPage;
            } else {
                $plus1Page = $pageSelected + 1;
            }

            $htmlPaginator = $htmlPaginator . '<li class="page-item" data-page="' . $plus1Page . '"><a href="' . $urlOriginal . '?page=' . $plus1Page . '"><i class="fa fa-angle-right"></i></a></li>';
            /* end controllo freccia + 1 pagine */
            /* controllo freccia + 10 pagine */
            if ($pageSelected + 10 >= $maxPage) {
                $plus10Page = $maxPage;
            } else {
                $plus10Page = $pageSelected + 10;
            }

            $htmlPaginator = $htmlPaginator . '<li class="page-item" data-page="' . $plus10Page . '"><a href="' . $urlOriginal . '?page=' . $plus10Page . '"><i class="fa fa-angle-double-right"></i></a></li>';
            /* end controllo freccia + 10 pagine */

            $htmlPaginator = $htmlPaginator . '</ul>';
            return $htmlPaginator;
        } else {
            return '';
        }
        /* end funzione gestione paginatore */
        
        
    }

    /**
     * Return srcSet
     *
     * @param string imgName
     * @param string ext
     * @return srcSet
     */
    private static function _getStringImageResized($imgName, $ext, $year, $month)
    {
        $stringReturnSrcSet = "";

        $imgNameWithoutExt = str_replace("." . $ext, "", $imgName);
        //$pathResize = static::getUrlImage();
        $pathResize = '/storage/images/';
        $imageResize = ImageResize::where('online', true)->orderBy('width', 'asc')->get();
        if(isset($imageResize)){
            foreach ($imageResize as $imageResizeAll) {
                $fullPathDir = static::$dirImageAttachmentImages . '/resize/' . $imgNameWithoutExt . 'x' . $imageResizeAll->width . '.' . $ext;
                
                if (file_exists($fullPathDir)) {
                    $fullPath = $pathResize . 'resize/' . $imgNameWithoutExt . 'x' . $imageResizeAll->width . '.' . $ext;
                    if($stringReturnSrcSet == ''){
                        $stringReturnSrcSet = $fullPath . ' ' . $imageResizeAll->width . 'w';
                    }else{
                        $stringReturnSrcSet = $stringReturnSrcSet . ', ' . $fullPath . ' ' . $imageResizeAll->width . 'w';
                    }
                }
            }
        }

        return $stringReturnSrcSet;
    }

    /**
     * Return stringSuccess
     *
     * @param string base64
     * @return stringSuccess
     */
    private static function _resizeImageByWidth($bas64, $imgName)
    {
        $stringReturnSrcSet = '';
        $pathResize = static::getUrlImage();
        $imageResize = ImageResize::where('online', true)->get();

        $extension = substr($imgName, -3, 3);
        if($extension != 'gif'){
            if ($extension == 'jpg' || $extension == 'png'){
                $realExt = $extension;
                $imgNameWithoutExt = str_replace("." . $extension, "", $imgName);
            }else{
                if ($extension == 'peg'){
                    $realExt = 'jpeg';
                    $imgNameWithoutExt = str_replace(".jpeg", "", $imgName);
                }else{
                    if ($extension == 'ebp'){
                        $realExt = 'webp';
                        $imgNameWithoutExt = str_replace(".webp", "", $imgName);
                        
                    }
                }
            }
    
            if(isset($imageResize)){
                foreach ($imageResize as $imageResizeAll) {
                    $imgResized = Image::make($bas64);
                    $imgResized->resize($imageResizeAll->width, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $imgResized->save(static::$dirImageAttachmentImages . '/resize/' . $imgNameWithoutExt . 'x' . $imageResizeAll->width . '.' . $realExt, 100);

                    $fullPath = $pathResize . 'resize/' . $imgNameWithoutExt . 'x' . $imageResizeAll->width . '.' . $realExt;
                    if($stringReturnSrcSet == ''){
                        $stringReturnSrcSet = $fullPath . ' ' . $imageResizeAll->width . 'w';
                    }else{
                        $stringReturnSrcSet = $stringReturnSrcSet . ', ' . $fullPath . ' ' . $imageResizeAll->width . 'w';
                    }
                }
            }
        }
        
        return $stringReturnSrcSet;
    
    }
    
    

    /**
     * Return dir
     *
     * @param string dir
     * @param string year
     * @param string month
     * @return resultImage
     */
    private static function _getFileDir($dir, $year, $month)
    {
        $result = collect();
        if (file_exists($dir)) {
            $cdir = scandir($dir, 1);
            foreach ($cdir as $key => $value){
                if (!in_array($value, array(".",".."))){
                    if (is_dir($dir . DIRECTORY_SEPARATOR . $value)){
                        if($value != 'TemplateEditor' && $value != 'Previews' && $value != 'Items' && $value != 'pdf' && $value != 'Categories' && $value != 'AdvertisingBanner' && $value != 'resize'){
                            
                            $arrayMerge = static::_getFileDir($dir . DIRECTORY_SEPARATOR . $value, $year, $month);
                            if(isset($arrayMerge)){
                                if(count($arrayMerge->toArray()) > 0){
                                    foreach ($arrayMerge as $arrayMerges) {
                                        /* info img */
                                        $listImages = new DtoImage();
                                        $listImages->name = $value . DIRECTORY_SEPARATOR . $arrayMerges->name;
                                        $listImages->nameWithOutPath = $arrayMerges->name;

                                        if($year != '' && $month == ''){
                                            $listImages->url = static::getUrlImage() . $year . '/' . $value . DIRECTORY_SEPARATOR . $arrayMerges->name;
                                        }else{
                                            $listImages->url = static::getUrlImage() . $value . DIRECTORY_SEPARATOR . $arrayMerges->name;
                                        }

                                        if($year != ''){
                                            $data = static::$dirImageAttachmentImages . '/' . $year . '/' . $value . DIRECTORY_SEPARATOR . $arrayMerges->name;
                                        }else{
                                            $data = static::$dirImageAttachmentImages . '/' . $value . DIRECTORY_SEPARATOR . $arrayMerges->name;
                                        }
                                        
                                        $extension = substr($data, -3, 3); 
                                        if ($extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'peg' || $extension == 'ebp'){
                                            if (file_exists($data)) {
                                                $date = filemtime($data);
                                                $listImages->dateorder = $date;
                                                $listImages->date = date('d/m/Y', $date);
                                                if(filesize($data) > 0){
                                                    $dimension = getimagesize($data);
                                                    if($dimension){
                                                        $listImages->width = $dimension[0];
                                                        $listImages->height = $dimension[1];
                                                    }else{
                                                        $listImages->width = 0;
                                                        $listImages->height = 0;
                                                    }
                                                    $listImages->weight = static::_convertImageSize(filesize($data));
                                                    $listImages->ext = pathinfo($data, PATHINFO_EXTENSION);
                                                    $filenamewithextension = basename($data);
                                                    $listImages->nameFile = basename($filenamewithextension);
                                                }else{
                                                    $listImages->dateorder = ''; 
                                                    $listImages->date = '';
                                                    $listImages->width = 0;
                                                    $listImages->height = 0;
                                                    $listImages->weight = 0;
                                                    $listImages->ext = '';
                                                    $listImages->nameFile = '';
                                                }
                                            }else{
                                                $listImages->dateorder = '';
                                                $listImages->date = '';
                                                $listImages->weight = 0;
                                                $listImages->width = 0;
                                                $listImages->height = 0;
                                                $listImages->ext = '';
                                                $listImages->nameFile = '';
                                            }
                                        }else{
                                            $listImages->dateorder = '';
                                            $listImages->date = '';
                                            $listImages->weight = 0;
                                            $listImages->width = 0;
                                            $listImages->height = 0;
                                            $listImages->ext = '';
                                            $listImages->nameFile = '';
                                        }
                                        $result->push($listImages);
                                    }
                                }
                            }
                        }
                    }else{
                        /* info img */
                        $listImages = new DtoImage();
                        $listImages->name = $value;
                        $listImages->nameWithOutPath = $value;
                        $dirNew = str_replace('../storage/app/public/images', '', $dir);
                        if($year != '' && $month == ''){
                            $listImages->url = static::getUrlImage() . $year . '/' . $dirNew . '/' . $value;
                        }else{
                            //if($year != '' && $month != ''){
                                $listImages->url = static::getUrlImage() . $dirNew . '/' . $value;
                            /*}else{
                                $listImages->url = static::getUrlImage() . $dirNew . '/' . $value;
                            }*/
                        }
                        $data = $dir . '/' . $value;
                        $extension = substr($data, -3, 3); 
                        if ($extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'peg' || $extension == 'ebp'){
                            if (file_exists($data)) {
                                $date = filemtime($data);
                                $listImages->dateorder = $date;
                                $listImages->date = date('d/m/Y', $date);
                                if(filesize($data) > 0){
                                    $dimension = getimagesize($data);
                                    if($dimension){
                                        $listImages->width = $dimension[0];
                                        $listImages->height = $dimension[1];
                                    }else{
                                        $listImages->width = 0;
                                        $listImages->height = 0;
                                    }
                                    $listImages->weight = static::_convertImageSize(filesize($data));
                                    $listImages->ext = pathinfo($data, PATHINFO_EXTENSION);
                                    $filenamewithextension = basename($data);
                                    $listImages->nameFile = basename($filenamewithextension);
                                }else{
                                    $listImages->dateorder = '';
                                    $listImages->date = '';
                                    $listImages->width = 0;
                                    $listImages->height = 0;
                                    $listImages->weight = 0;
                                    $listImages->ext = '';
                                    $listImages->nameFile = '';
                                }
                            }else{
                                $listImages->dateorder = '';
                                $listImages->date = '';
                                $listImages->weight = 0;
                                $listImages->width = 0;
                                $listImages->height = 0;
                                $listImages->ext = '';
                                $listImages->nameFile = '';
                            }
                        }else{
                            $listImages->dateorder = '';
                            $listImages->date = '';
                            $listImages->weight = 0;
                            $listImages->width = 0;
                            $listImages->height = 0;
                            $listImages->ext = '';
                            $listImages->nameFile = '';
                        }

                        $result->push($listImages);
                    
                    }
                }
            }
        }
        return $result;
    }


    /**
     * Return dtoPage to render
     *
     * @param htmlContentWithShortcode
     * @param string idUser
     * @param int idCart
     * @param int idLanguage
     * @param string url
     * @return DtoPageRender
     */
    private static function _convertHtmlWithoutShortcode($htmlContentWithShortcode, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId)
    {

     
        /*** applicazione sidebar generale ***/
        
        //prelievo impostazione se sidebar attiva o no (true o false)
        $isSidebarVisible = SettingBL::getByCode('SidebarVisible');

        if($isSidebarVisible == "True"){
            //controllo tramite url il page type della pagina, se blog o news visualizzo la sidebar
            $page = DB::select("SELECT * FROM pages 
            INNER JOIN languages_pages on pages.id = languages_pages.page_id
            WHERE (pages.page_type = 'Blog' OR pages.page_type = 'News') 
            AND languages_pages.url = :url 
            AND language_id = :idLanguage",
            ['idLanguage' => $idLanguage, 'url' => $url]);
            if(isset($page)){
                if(count($page) > 0){
                    //prelievo impostazione se sidebar deve apparire a destra o sinistra (left right)
                    $sidebarPosition = SettingBL::getByCode('SidebarPosition');
                    //prelievo impostazione numero colonne sidebar
                    $sidebarWidth = (int)SettingBL::getByCode('SidebarWidth');
                    //prelievo impostazione numero colonne sidebar medium
                    $sidebarWidthMedium = (int)SettingBL::getByCode('SidebarWidthMedium');
                    //prelievo impostazione numero colonne sidebar small
                    $sidebarWidthSmall = (int)SettingBL::getByCode('SidebarWidthSmall');
                    
                    //prelievo contenuto sidebar
                    $idContentSidebar = Content::where('code', 'Sidebar')->first();
                    $contentLanguageSidebar = ContentLanguage::where('content_id', $idContentSidebar->id)->first();
                                    
                    $requestBannerTop = new Request([
                        'code'   =>  'content_top',
                        'url'  => $url,
                    ]);
                    $htmlBannerTop = AdvertisingAreaBL::getHtmlBanner($requestBannerTop);
                    if (strpos($htmlContentWithShortcode, '</p>') !== false) {
                        $htmlContentWithShortcode = substr($htmlContentWithShortcode, 0, strpos($htmlContentWithShortcode, '</p>')+4) . '<div class="0fa27fd7604a5c0c02fee26641695d47" style="width:300px;height:200px;" data-index="2" style="float: left; margin: 10px 10px 10px 0;">' . $htmlBannerTop . '</div>' . substr($htmlContentWithShortcode, strpos($htmlContentWithShortcode, '</p>')+4);
                    }

                    $requestBannerBottom = new Request([
                        'code'   =>  'content_bottom',
                        'url'  => $url,
                    ]);
                    $htmlBannerBottom = AdvertisingAreaBL::getHtmlBanner($requestBannerBottom);

                    $htmlContentWithShortcode = $htmlContentWithShortcode . $htmlBannerBottom;

                    if($sidebarPosition == "Left"){
                        $htmlContentWithShortcode = '<div class="container"><div class="row"><div class="col-sm-' . $sidebarWidthSmall . ' col-md-' . $sidebarWidthMedium . ' col-lg-' . $sidebarWidthMedium . '">' . $contentLanguageSidebar->content . '</div><div class="col-sm-' . (string)(12 - $sidebarWidthSmall) . ' col-md-' . (string)(12 - $sidebarWidthMedium) . ' col-lg-' . (string)(12 - $sidebarWidth) . '">' . $htmlContentWithShortcode . '</div></div></div>';
                    }else{
                        $htmlContentWithShortcode = '<div class="container"><div class="row"><div class="col-sm-' . (string)(12 - $sidebarWidthSmall) . ' col-md-' . (string)(12 - $sidebarWidthMedium) . ' col-lg-' . (string)(12 - $sidebarWidth) . '">' . $htmlContentWithShortcode . '</div><div class="col-sm-' . $sidebarWidthSmall . ' col-md-' . $sidebarWidthMedium . ' col-lg-' . $sidebarWidth . '">' . $contentLanguageSidebar->content . '</div></div></div>';
                    }
                }                
            }
        }
        /*** end applicazione sidebar generale ***/

        /*** blog ***/
        
        if (strpos($htmlContentWithShortcode, '#blog#') !== false) {
            $strLimit = "";
            $strSqlPagination = "";
            $htmlPaginator = '';
           // $pageSelected = 1;
            $itemsBlogPerPage = SettingBL::getByCode(SettingEnum::BlogList);
            

            $numberArticleBlogNewsHome = SettingBL::getByCode(SettingEnum::HomeBlogList);

            if(is_null($numberArticleBlogNewsHome)){
                $numberArticleBlogNewsHome = 3;
            }


            if($url == '/'){
                $strSqlPagination = " limit " . $numberArticleBlogNewsHome;

            }else{
                $totItemsBlog = DB::select(
                    "SELECT count(pages.id) as cont
                    FROM pages 
                    INNER JOIN languages_pages on pages.id = languages_pages.page_id
                    WHERE pages.page_type = 'Blog' 
                    AND pages.online = true
                    AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() )
                    AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() )
                    AND languages_pages.language_id = " . $idLanguage . "
                    AND languages_pages.content IS NOT NULL");
                
                if($totItemsBlog[0]->cont <= $itemsBlogPerPage){
               
                    $strSqlPagination = "";
                    $htmlPaginator = '';
                }else{
                    
                    /* gestione paginatore */
                    //prelievo dato pagina selezionata da url
                    
                    if($param != ""){
                        if(isset($param)){

                            if(array_key_exists('page', $param)){
                                
                                $pageSelected = $param['page'];
                            }else{
                            
                                $pageSelected = 1; 
                            }
                        }else{
                            $pageSelected = 1;  
                        }
                    }else{
                        $pageSelected = 1;
                    }

                    $limit = $itemsBlogPerPage;
                    $offset = $itemsBlogPerPage * ($pageSelected - 1);
    
                    $strSqlPagination = " limit " . $limit . " offset " . $offset;

                    $paginationActive = true;
                    $maxPage = ceil($totItemsBlog[0]->cont / $itemsBlogPerPage);

                    $htmlPaginator = static::_getRenderPaginator($pageSelected, $maxPage, $paginationActive, $url);

                    $htmlPaginator = '<div class="container" id="paginator"><div class="row"><div class="col-12">' . $htmlPaginator . '</div></div></div>';
                    /* end gestione paginatore */
                }

                
            }

            $idContent = Content::where('code', 'BlogListArticle')->first();
            if(isset($idContent)){
                $contentLanguage = ContentLanguage::where('content_id', $idContent->id)->where('language_id', $idLanguage)->first();
                $finalHtml = '';
                $newContent = '';
                $tags = SelenaViewsBL::getTagByType('BlogListArticle');                    

                foreach (DB::select(
                    "SELECT languages_pages.*, pages.publish as publish
                    FROM pages 
                    INNER JOIN languages_pages on pages.id = languages_pages.page_id
                    WHERE pages.page_type = 'Blog' 
                    AND pages.online = true
                    AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() )
                    AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() )
                    AND languages_pages.language_id = " . $idLanguage . "
                    AND languages_pages.content IS NOT NULL order by pages.publish DESC" . $strSqlPagination
                ) as $listBlog){
                    $newContent = $contentLanguage->content;                        
                    
                    foreach ($tags as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                                 
                            if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($listBlog->{$cleanTag}) && strpos($listBlog->{$cleanTag},'.')){
                                    
                                    if($fieldsDb->tag == '#publish#'){
                                        $tag = date_format(date_create($listBlog->{$cleanTag}), 'd/m/Y');
                                    }else{
                                        $tag = str_replace('.',',',substr($listBlog->{$cleanTag}, 0, -2));
                                    }
                                }else{
                                    //if($cleanTag  == '#url_cover_image#'){
                                    //    $tag = str_replace(' ','%20', $listBlog->{$cleanTag});
                                    //}else{
                                        if($fieldsDb->tag == '#publish#'){
                                            $tag = date_format(date_create($listBlog->{$cleanTag}), 'd/m/Y');
                                        }else{
                                            $tag = $listBlog->{$cleanTag};
                                        }
                                    //}
                                }
                                $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                               
                            }
                        }
                    }
                    
                    if (strpos($newContent, '#publish#') !== false) {
                        $newContent = str_replace('#publish#', date_format(date_create($listBlog->publish), SettingBL::getByCode(SettingEnum::DateFormat)), $newContent);
                    }

                    if (strpos($newContent, '#category_name#') !== false) {
                        $PagePageCategory = PagePageCategory::where('page_id', $listBlog->page_id)->first(); 
                    if (isset($PagePageCategory)){
                        $LanguagePageCategory = LanguagePageCategory::where('page_category_id', $PagePageCategory->page_category_id)->first(); 
                    }
                    if (isset($LanguagePageCategory)){
                        $newContent = str_replace('#category_name#', $LanguagePageCategory->description,  $newContent); 
                        }else{
                            $newContent = str_replace('#category_name#', '',  $newContent);           
                        }
                    }
                    
                    $finalHtml =  $finalHtml . $newContent;
                }


                $htmlContentWithShortcode = str_replace('#blog#', $htmlPaginator . $finalHtml . $htmlPaginator,  $htmlContentWithShortcode);
            }else{
                $htmlContentWithShortcode = str_replace('#blog#', '',  $htmlContentWithShortcode);
            }
        }

        /*** end blog ***/

        /*** news ***/

        if (strpos($htmlContentWithShortcode, '#news#') !== false) {
            $finalHtml = '';
            $newContent = '';  

            $strLimit = "";
            $numberArticleBlogNewsHome = SettingBL::getByCode(SettingEnum::HomeNewsList);

            if(is_null($numberArticleBlogNewsHome)){
                $numberArticleBlogNewsHome = 3;
            }
            
            if($url == '/'){
                $strLimit = " limit " . $numberArticleBlogNewsHome;
            }

            //section /news 
            $numberArticleBlogNewsHomeGlobal = SettingBL::getByCode(SettingEnum::NewsList);

            if($url == '/news'){
                $strLimit = " limit " . $numberArticleBlogNewsHomeGlobal;
            }
             if(is_null($numberArticleBlogNewsHomeGlobal)){
                $numberArticleBlogNewsHomeGlobal = 3;
            }

            $idContent = Content::where('code', 'NewsListArticle')->first();
            if(isset($idContent)){
                $contentLanguage = ContentLanguage::where('content_id', $idContent->id)->where('language_id', $idLanguage)->first();
                $finalHtml = '';
                $newContent = '';
                $tags = SelenaViewsBL::getTagByType('NewsListArticle');

                foreach (DB::select(
                    "SELECT languages_pages.*, pages.publish as publish
                    FROM pages 
                    INNER JOIN languages_pages on pages.id = languages_pages.page_id
                    WHERE pages.page_type = 'News' 
                    AND pages.online = true
                    AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() )
                    AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() )
                    AND languages_pages.language_id = " . $idLanguage . "
                    AND languages_pages.content IS NOT NULL order by pages.publish DESC " . $strLimit
                ) as $listBlog){
                    $newContent = $contentLanguage->content;                        
                    
                    foreach ($tags as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                                 
                            if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($listBlog->{$cleanTag}) && strpos($listBlog->{$cleanTag},'.')){
                                    if($fieldsDb->tag == '#publish#'){
                                        $tag = date_format(date_create($listBlog->{$cleanTag}), 'd/m/Y');
                                    }else{
                                        $tag = str_replace('.',',',substr($listBlog->{$cleanTag}, 0, -2));
                                    }
                                }else{
                                    if($fieldsDb->tag == '#publish#'){
                                        $tag = date_format(date_create($listBlog->{$cleanTag}), 'd/m/Y');
                                    }else{
                                        $tag = $listBlog->{$cleanTag};
                                    }
                                }

                                $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);
                                                                                                               
                            }
                        }
                    }

                    if (strpos($newContent, '#publish#') !== false) {
                        $newContent = str_replace('#publish#', date_format(date_create($listBlog->publish), SettingBL::getByCode(SettingEnum::DateFormat)), $newContent);
                    }

                    $finalHtml = $finalHtml . $newContent;
                }

                $htmlContentWithShortcode = str_replace('#news#', $finalHtml,  $htmlContentWithShortcode);
            }else{
                $htmlContentWithShortcode = str_replace('#news#', '',  $htmlContentWithShortcode);
            }
        }

        /*** end news ***/

        /*** breadcrumbs ***/

        if (strpos($htmlContentWithShortcode, '#breadcrumbs#') !== false) {
            $request = new Request([
                'url'   =>  $url,
                'ln'  => $idLanguage,
            ]);
            $newHtml = ContentBL::getBreadcrumbs($request);
            $htmlContentWithShortcode = str_replace('#breadcrumbs#', $newHtml,  $htmlContentWithShortcode);
        }
        
        /*** end breadcrumbs ***/

        /*** categories ***/

        if (strpos($htmlContentWithShortcode, '#categories#') !== false) {
            $request = new Request([
                'code'   =>  'Categories',
                'ln'  => $idLanguage,
                'url' => $url,
                'cart_id' => $idCart,
                'userId' => $idUser  
            ]);
            $newHtml = ContentBL::getByCodeRender($request);
            $htmlContentWithShortcode = str_replace('#categories#', $newHtml,  $htmlContentWithShortcode);
        }

        /*** end categories ***/

        /*** categorieslist ***/

        if (strpos($htmlContentWithShortcode, '#categorieslist#') !== false) {
            /*$request = new Request([
                'code'   =>  'categorieslist',
                'ln'  => $idLanguage,
                'url' => $url,
                'cart_id' => $idCart,
                'userId' => $idUser  
            ]);
            $newHtml = ContentBL::getByCodeRender($request);
            $htmlContentWithShortcode = str_replace('#categorieslist#', $newHtml,  $htmlContentWithShortcode);*/
            //to do render lista categorie 
        }

        /*** end categorieslist ***/

        /*** itemslist ***/

        if (strpos($htmlContentWithShortcode, '#itemslist#') !== false) {
            $request = new Request([
                'code'   =>  'Items',
                'ln'  => $idLanguage,
                'url' => $url,
                'cart_id' => $idCart,
                'userId' => $idUser,
                'param' => $param
            ]);
            $newHtml = ContentBL::getByCodeRender($request);
            $htmlContentWithShortcode = str_replace('#itemslist#', $newHtml->html,  $htmlContentWithShortcode);
        }

        /*** end itemslist ***/

        /*** favoritesuseritems ***/

        if (strpos($htmlContentWithShortcode, '#favoritesuseritems#') !== false) {
            if($idUser>0){
                $request = new Request([
                    'code'   =>  'FavoritesUserItems',
                    'ln'  => $idLanguage,
                    'url' => $url,
                    'cart_id' => $idCart,
                    'userId' => $idUser  
                ]);
                $newHtml = ContentBL::getByCodeRender($request);
                $htmlContentWithShortcode = str_replace('#favoritesuseritems#', $newHtml,  $htmlContentWithShortcode);
            }
        }

        /*** end favoritesuseritems ***/

        /*** categorieslist ***/

        if (strpos($htmlContentWithShortcode, '#categoriesfilter#') !== false) {
            $request = new Request([
                'code'   =>  'CategoriesFilter',
                'ln'  => $idLanguage,
                'url' => $url,
                'cart_id' => $idCart,
                'userId' => $idUser,
                'param' => $param
            ]);
            $newHtml = ContentBL::getByCodeRender($request);
            $htmlContentWithShortcode = str_replace('#categoriesfilter#', $newHtml,  $htmlContentWithShortcode);
            //to do render lista categorie 
        }

        /*** end categorieslist ***/

        /*** cart_summary ***/

        if (strpos($htmlContentWithShortcode, '#cart_summary#') !== false) {
            if($idUser>0 || $idCart > 0){
                $request = new Request([
                    'code'   =>  'Cart_Summary',
                    'ln'  => $idLanguage,
                    'url' => $url,
                    'cart_id' => $idCart,
                    'userId' => $idUser  
                ]);
                $newHtml = ContentBL::getByCodeRender($request);
                $htmlContentWithShortcode = str_replace('#cart_summary#', $newHtml,  $htmlContentWithShortcode);
            }else{
                $htmlContentWithShortcode = str_replace('#cart_summary#', '',  $htmlContentWithShortcode);
            }

        }

        /*** end cart_summary ***/

        /*** cart_detail_single_row ***/

        if (strpos($htmlContentWithShortcode, '#cart_detail_single_row#') !== false) {
            if($idUser>0 || $idCart > 0){

                $cartDetail = CartDetail::where('cart_id', $idCart)->get()->first();
                if(isset($cartDetail)){
                    $request = new Request([
                        'code'   =>  'Cart_Detail_Single_Row',
                        'ln'  => $idLanguage,
                        'url' => $url,
                        'cart_id' => $idCart,
                        'userId' => $idUser  
                    ]);
                    $newHtml = ContentBL::getByCodeRender($request);
                    $htmlContentWithShortcode = str_replace('#cart_detail_single_row#', $newHtml,  $htmlContentWithShortcode);
                }else{
                    $newHtml= MessageBL::getByCode('EMPTY_CART', $idLanguage);
                    $htmlContentWithShortcode = str_replace('#cart_detail_single_row#',  $newHtml,  $htmlContentWithShortcode);
                }
            }else{
                $htmlContentWithShortcode = str_replace('#cart_detail_single_row#', '',  $htmlContentWithShortcode);
            }

        }

        /*** end cart_detail_single_row ***/

        /*** cart_user_address ***/

        if (strpos($htmlContentWithShortcode, '#cart_user_address#') !== false) {
            if($idUser>0 || $idCart > 0){
                $request = new Request([
                    'code'   =>  'CartUsersAddresses',
                    'ln'  => $idLanguage,
                    'url' => $url,
                    'cart_id' => $idCart,
                    'userId' => $idUser  
                ]);
                $newHtml = ContentBL::getByCodeRender($request);
                $htmlContentWithShortcode = str_replace('#cart_user_address#', $newHtml,  $htmlContentWithShortcode);
            }else{
                $htmlContentWithShortcode = str_replace('#cart_user_address#', '',  $htmlContentWithShortcode);
            }
        }

        /*** end cart_user_address ***/

        /*** Box Affiliazione Amazon ***/
  
        if (strpos($htmlContentWithShortcode, '[amazon') !== false) {
            
            $htmlContentWithShortcode = str_replace('&quot;','"',$htmlContentWithShortcode);
            //mi memorizzo in un array tutte le posizioni dove ho un box
            $indices = [];
            
            $pos = 0;
            while(($pos = strpos($htmlContentWithShortcode, '[amazon', $pos)) !== false ) {
                array_push($indices, $pos);
                $pos = $pos + 7;
            }


            $ind = 0;
            $lengthIndices = count($indices);
            for($ind = $lengthIndices - 1; $ind>=0;  $ind--){
                
                //se ho lo stesso shortcode riputo tante volte, dopo la prima non ne ho più e non devo più ciclare
                if (strpos($htmlContentWithShortcode, '[amazon') === false){
                    break;
                } 
    
                $inizio = $indices[$ind];
                
                $fine = strpos($htmlContentWithShortcode, ']', $inizio);
                
                $stringaAmazon = '';
                
                $stringaAmazon = substr($htmlContentWithShortcode, $inizio, $fine-$inizio+1);
                
                //se esco e stringaAmazon = "" significa che mi sono scordato di chiudere la parentesi quadra.
                //Lascio tutto così ed esco dal ciclo while...altrimenti va in loop infinito
                if ($stringaAmazon == ''){
                    break;
                }

                $strReplace = AmazonBL::getHtml($stringaAmazon);

                $htmlContentWithShortcode = str_replace($stringaAmazon, $strReplace,  $htmlContentWithShortcode);

            }
        }

        if (strpos($htmlContentWithShortcode, '[miglior prezzo') !== false) {
            $htmlContentWithShortcode = str_replace('&quot;','"',$htmlContentWithShortcode);
            //mi memorizzo in un array tutte le posizioni dove ho un box
            $indices = [];
            
            for($pos = strpos($htmlContentWithShortcode,'[miglior prezzo'); $pos !== false; $pos = strpos($htmlContentWithShortcode, '[miglior prezzo', $pos + 1)) {
                //$indices.push($pos);
                array_push($indices, $pos);
            }
            $ind = 0;
            $lengthIndices = count($indices);
            for($ind = $lengthIndices - 1; $ind>=0;  $ind--){
                //se ho lo stesso shortcode riputo tante volte, dopo la prima non ne ho più e non devo più ciclare
                if (strpos($htmlContentWithShortcode, '[miglior prezzo') === false){
                    break;
                } 
    
                $inizio = $indices[$ind];
                
                $fine = strpos($htmlContentWithShortcode, ']', $inizio);
                
                $stringaMigliorPrezzo = '';
                
                $stringaMigliorPrezzo = substr($htmlContentWithShortcode, $inizio, $fine-$inizio+1);
                
                //se esco e stringaAmazon = "" significa che mi sono scordato di chiudere la parentesi quadra.
                //Lascio tutto così ed esco dal ciclo while...altrimenti va in loop infinito
                if ($stringaMigliorPrezzo == ''){
                    break;
                }

                $descriptionItem = str_replace('[miglior prezzo ', '',  $stringaMigliorPrezzo);
                $descriptionItem = str_replace(']', '',  $descriptionItem);

                $itemLanguageOnDb = ItemLanguage::where('description', $descriptionItem)->get()->first();

                if (isset($itemLanguageOnDb->item_id))
                {
                    $categoryItemOnDb = CategoryItem::where('item_id', $itemLanguageOnDb->item_id)->get()->first();
                    if ($categoryItemOnDb->category_id>=11 && $categoryItemOnDb->category_id<=15)
                        $migliorOfferta = OffertaTrovaPrezziBL::migliorOfferta("cameranationit", $itemLanguageOnDb->description, "5");
                    else if ($categoryItemOnDb->category_id==16)
                        $migliorOfferta = OffertaTrovaPrezziBL::migliorOfferta("cameranationit", $itemLanguageOnDb->description, "20124");
                    else if ($categoryItemOnDb->category_id==17)
                        $migliorOfferta = OffertaTrovaPrezziBL::migliorOfferta("cameranationit", $itemLanguageOnDb->description, "7");
                    else
                        $migliorOfferta = OffertaTrovaPrezziBL::migliorOfferta("cameranationit", $itemLanguageOnDb->description, "0");

                    if ($migliorOfferta->price=='' || $migliorOfferta->price==0)
                        $htmlFinale = '<h4>Prezzo non disponibile</h4>';
                    else{
                        $euro = explode(',', $migliorOfferta->price);
                        if (count($euro)==1) 
                            $htmlFinale = '<a target="_blank" rel="nofollow" href="'. $migliorOfferta->url . '"><h2>'. $euro[0] .'<span>,00 €</span></h2><h4>Miglior Prezzo</h4><img src="https://cameranation.it/storage/images/Logo_Trovaprezzi_2018_150px.png"></a>';
                        else
                        $htmlFinale = '<a target="_blank" rel="nofollow" href="'. $migliorOfferta->url . '"><h2>'. $euro[0] .'<span>,' . $euro[1] . ' €</span></h2><h4>Miglior Prezzo</h4><img src="https://cameranation.it/storage/images/Logo_Trovaprezzi_2018_150px.png"></a>';
                    }
                }    
                else
                    $htmlFinale = '<h4>Prezzo non disponibile</h4>';
                $htmlContentWithShortcode = str_replace($stringaMigliorPrezzo, $htmlFinale,  $htmlContentWithShortcode);

            }
        }
        /*** END Box Affiliazione Amazon ***/

  /******************************************************************************************/
        // booking
        /******************************************************************************************/
     
        if (strpos($htmlContentWithShortcode, '[booking') !== false) {
         
            $htmlContentWithShortcode = str_replace('&quot;','"',$htmlContentWithShortcode);
            
            //mi memorizzo in un array tutte le posizioni dove ho un box
            $indices = [];
            
            for($pos = strpos($htmlContentWithShortcode,'[booking'); $pos !== false; $pos = strpos($htmlContentWithShortcode, '[booking', $pos + 1)) {
                array_push($indices, $pos);
            }
            $ind = 0;
            $lengthIndices = count($indices);
            for($ind = $lengthIndices - 1; $ind>=0;  $ind--){
                //se ho lo stesso shortcode riputo tante volte, dopo la prima non ne ho più e non devo più ciclare
                if (strpos($htmlContentWithShortcode, '[booking') === false){
                    break;
                } 
    
                $inizio = $indices[$ind];
                $fine = strpos($htmlContentWithShortcode, ']', $inizio);
                
                $stringaBooking = '';
                $stringaBooking = substr($htmlContentWithShortcode, $inizio, $fine-$inizio+1);
                
                if ($stringaBooking == ''){
                    break;
                }

                $pieces = explode(" ", $stringaBooking);
                $tipoBooking = '';
                $idUser = ''; 

                for($i = 0; $i<count($pieces);$i++){
                    if (strpos($pieces[$i], "id") !== false ) {
                        $tipoBooking = substr($pieces[$i],2);
                    }
                
                    if (strpos($pieces[$i], "language") !== false ) {
                        $tipolingua = substr($pieces[$i],3);
                    }


                } 
                $strReplace = ConnectionsBL::getDateForId($tipoBooking, $idUser, $idLanguage);
                $htmlContentWithShortcode = str_replace($stringaBooking, $strReplace,  $htmlContentWithShortcode);
            }
        }
        //end booking 


        /******************************************************************************************/
        // estrazione di una galleria dalla community con uno shortcode nell'html
        //    [gallery type="popular" gallery]  per esempio deve iniziare e finire per 'gallery'
        /******************************************************************************************/
        if (strpos($htmlContentWithShortcode, '[gallery') !== false) {
            
            //esempio di shortcode gallery
            // [gallery type=popular user_id=#user_id# image_id_to_exclude=#id# limit=10 gallery]  -> 10 foto popolari dell'utente X esclusa quella indicata
            // [gallery type=popular image_category_id=#image_category_id# gallery] --> tutte le foto di quella categoria
            
            $htmlContentWithShortcode = str_replace('&quot;','"',$htmlContentWithShortcode);
            
            //mi memorizzo in un array tutte le posizioni dove ho un box
            $indices = [];
            
            for($pos = strpos($htmlContentWithShortcode,'[gallery'); $pos !== false; $pos = strpos($htmlContentWithShortcode, '[gallery', $pos + 1)) {
                array_push($indices, $pos);
            }
            $ind = 0;
            $lengthIndices = count($indices);
            for($ind = $lengthIndices - 1; $ind>=0;  $ind--){
                //se ho lo stesso shortcode riputo tante volte, dopo la prima non ne ho più e non devo più ciclare
                if (strpos($htmlContentWithShortcode, '[gallery') === false){
                    break;
                } 
    
                $inizio = $indices[$ind];
                $fine = strpos($htmlContentWithShortcode, ']', $inizio);
                
                $stringaGallery = '';
                $stringaGallery = substr($htmlContentWithShortcode, $inizio, $fine-$inizio+1);
                
                //se esco e stringaAmazon = "" significa che mi sono scordato di chiudere la parentesi quadra.
                //Lascio tutto così ed esco dal ciclo while...altrimenti va in loop infinito
                if ($stringaGallery == ''){
                    break;
                }

                $pieces = explode(" ", $stringaGallery);

                $tipoGalleria = '';
                $idUser = ''; 
                $idCategoryImage = '';
                $idImageToExclude = '';
                $limit = '';
                $idCamera = '';
                $idLens = '';

                for($i = 0; $i<count($pieces);$i++){
                    if (strpos($pieces[$i], "type") !== false ) {
                        $tipoGalleria = substr($pieces[$i],5);
                    }
                    if (strpos($pieces[$i], "user_id") !== false ) {
                        $idUser = substr($pieces[$i],8);
                    }
                    if (strpos($pieces[$i], "image_category_id") !== false ) {
                        $idCategoryImage = substr($pieces[$i],18);
                    }
                    if (strpos($pieces[$i], "image_id_to_exclude") !== false ) {
                        $idImageToExclude = substr($pieces[$i],20);
                    }
                    if (strpos($pieces[$i], "camera_id") !== false ) {
                        $idCamera = substr($pieces[$i],10);
                    }
                    if (strpos($pieces[$i], "lens_id") !== false ) {
                        $idLens = substr($pieces[$i],8);
                    }
                    if (strpos($pieces[$i], "limit") !== false ) {
                        $limit = substr($pieces[$i],6);
                    }
                } 

                $strReplace = CommunityImageBL::getGallery($tipoGalleria, $idUser, $idCategoryImage, $idImageToExclude, $limit, $idCamera, $idLens);
                
                $htmlContentWithShortcode = str_replace($stringaGallery, $strReplace,  $htmlContentWithShortcode);

            }

        }

        /******************************************************************************************/
        // estrazione dell'elenco prodotti con uno shortcode nell'html
        //    [item type="popular" item]  per esempio deve iniziare e finire per 'item'
        //
        /******************************************************************************************/
        if (strpos($htmlContentWithShortcode, '[item') !== false) {
            
            //esempio di shortcode item
            // [item type=promotion limit=10 item]  -> 10 articoli in promozione
            // [item type=bestseller limit=10 item] -> 10 articoli più venduti
            
            $htmlContentWithShortcode = str_replace('&quot;','"',$htmlContentWithShortcode);
            
            //mi memorizzo in un array tutte le posizioni dove ho un box
            $indices = [];
            
            for($pos = strpos($htmlContentWithShortcode,'[item'); $pos !== false; $pos = strpos($htmlContentWithShortcode, '[item', $pos + 1)) {
                array_push($indices, $pos);
            }
            $ind = 0;
            $lengthIndices = count($indices);
            for($ind = $lengthIndices - 1; $ind>=0;  $ind--){
                //se ho lo stesso shortcode riputo tante volte, dopo la prima non ne ho più e non devo più ciclare
                if (strpos($htmlContentWithShortcode, '[item') === false){
                    break;
                } 
    
                $inizio = $indices[$ind];
                $fine = strpos($htmlContentWithShortcode, ']', $inizio);
                
                $stringaGallery = '';
                $stringaGallery = substr($htmlContentWithShortcode, $inizio, $fine-$inizio+1);
                
                //se esco e stringaAmazon = "" significa che mi sono scordato di chiudere la parentesi quadra.
                //Lascio tutto così ed esco dal ciclo while...altrimenti va in loop infinito
                if ($stringaGallery == ''){
                    break;
                }

                $pieces = explode(" ", $stringaGallery);

                $tipoProdotto = '';
                $limit = '';
                $idCategoryItem = '';
                $idProducerItem = '';
                $orderBy = '';

                for($i = 0; $i<count($pieces);$i++){
                    if (strpos($pieces[$i], "type") !== false ) {
                        $tipoProdotto = substr($pieces[$i],5);
                    }
                    if (strpos($pieces[$i], "item_category_id") !== false ) {
                        $idCategoryItem = substr($pieces[$i],17);
                    }

                    if (strpos($pieces[$i], "item_producer_id") !== false ) {
                        $idProducerItem = substr($pieces[$i],17);
                    }
                    if (strpos($pieces[$i], "order_by") !== false ) {
                        $orderBy = substr($pieces[$i],9);
                    }
                    if (strpos($pieces[$i], "limit") !== false ) {
                        $limit = substr($pieces[$i],6);
                    }
                } 

                $strReplace = ItemBL::getItemShortcode($tipoProdotto, $idCategoryItem, $idProducerItem, $limit, $orderBy, $idLanguage, $idUser);
                
                $htmlContentWithShortcode = str_replace($stringaGallery, $strReplace,  $htmlContentWithShortcode);

            }
        }

        /******************************************************************************************/
        // estrazione dinamica dei form
        /******************************************************************************************/
        if (strpos($htmlContentWithShortcode, '[form') !== false) {
            
            //esempio di shortcode item
            
            $htmlContentWithShortcode = str_replace('&quot;','"',$htmlContentWithShortcode);
            
            //mi memorizzo in un array tutte le posizioni dove ho un box
            $indices = [];
            
            for($pos = strpos($htmlContentWithShortcode,'[form'); $pos !== false; $pos = strpos($htmlContentWithShortcode, '[form', $pos + 1)) {
                array_push($indices, $pos);
            }
            $ind = 0;
            $lengthIndices = count($indices);
            for($ind = $lengthIndices - 1; $ind>=0;  $ind--){
                //se ho lo stesso shortcode riputo tante volte, dopo la prima non ne ho più e non devo più ciclare
                if (strpos($htmlContentWithShortcode, '[form') === false){
                    break;
                } 
    
                $inizio = $indices[$ind];
                $fine = strpos($htmlContentWithShortcode, ']', $inizio);
                
                $stringaGallery = '';
                $stringaGallery = substr($htmlContentWithShortcode, $inizio, $fine-$inizio+1);
                
                //Lascio tutto così ed esco dal ciclo while...altrimenti va in loop infinito
                if ($stringaGallery == ''){
                    break;
                }

                $pieces = explode(" ", $stringaGallery);

                $idForm = '';

                for($i = 0; $i<count($pieces);$i++){
                    if (strpos($pieces[$i], "id") !== false ) {
                        $idForm = substr($pieces[$i],3);
                    }
                } 

                $strReplace = FormBuilderBL::getFormShortcode($idForm, $idLanguage, $idUser, $param);
                $htmlContentWithShortcode = str_replace($stringaGallery, $strReplace, $htmlContentWithShortcode);

            }
        }


        //[article type=blog items=5 order=random category=canon|sony|nikon article]
        if (strpos($htmlContentWithShortcode, '[article') !== false) {
            $htmlContentWithShortcode = str_replace('&quot;','"',$htmlContentWithShortcode);
            //mi memorizzo in un array tutte le posizioni dove ho uno shortcode
            $indices = [];
            
            for($pos = strpos($htmlContentWithShortcode,'[article'); $pos !== false; $pos = strpos($htmlContentWithShortcode, '[article', $pos + 1)) {
                //$indices.push($pos);
                array_push($indices, $pos);
            }
            $ind = 0;
            $lengthIndices = count($indices);
            for($ind = $lengthIndices - 1; $ind>=0;  $ind--){
                //se ho lo stesso shortcode riputo tante volte, dopo la prima non ne ho più e non devo più ciclare
                if (strpos($htmlContentWithShortcode, '[article') === false){
                    break;
                } 

                $inizio = $indices[$ind];
                
                $fine = strpos($htmlContentWithShortcode, ']', $inizio);
                
                $stringaShortcodeArticle = '';
                
                $stringaShortcodeArticle = substr($htmlContentWithShortcode, $inizio, $fine-$inizio+1);
                
                //se esco e stringaShortcodeArticle = "" significa che mi sono scordato di chiudere la parentesi quadra.
                //Lascio tutto così ed esco dal ciclo while...altrimenti va in loop infinito
                if ($stringaShortcodeArticle == ''){
                    break;
                }
                $strReplace = static::getHtmlShortcode($stringaShortcodeArticle);

                $htmlContentWithShortcode = str_replace($stringaShortcodeArticle, $strReplace,  $htmlContentWithShortcode);
            }
        }

                //articoli aggiuntivi da inserire //
                //[articleAdd type=blog items=3 order=esc category=famiglia articleAdd]//
                if (strpos($htmlContentWithShortcode, '[addarticle') !== false) {
                    $htmlContentWithShortcode = str_replace('&quot;','"',$htmlContentWithShortcode);
                    //mi memorizzo in un array tutte le posizioni dove ho uno shortcode
                    $indices = [];
                    
                    for($pos = strpos($htmlContentWithShortcode,'[addarticle'); $pos !== false; $pos = strpos($htmlContentWithShortcode, '[addarticle', $pos + 1)) {
                        //$indices.push($pos);
                        array_push($indices, $pos);
                    }
                    $ind = 0;
                    $lengthIndices = count($indices);
                    for($ind = $lengthIndices - 1; $ind>=0;  $ind--){
                        //se ho lo stesso shortcode riputo tante volte, dopo la prima non ne ho più e non devo più ciclare
                        if (strpos($htmlContentWithShortcode, '[addarticle') === false){
                            break;
                        } 
        
                        $inizio = $indices[$ind];
                        $fine = strpos($htmlContentWithShortcode, ']', $inizio);
                        
                        $stringaShortcodeArticle = '';
                        $stringaShortcodeArticle = substr($htmlContentWithShortcode, $inizio, $fine-$inizio+1);
                        
                        if ($stringaShortcodeArticle == ''){
                            break;
                        }
                        $strReplace = static::getHtmlShortcodeAdd($stringaShortcodeArticle);
                        $htmlContentWithShortcode = str_replace($stringaShortcodeArticle, $strReplace,  $htmlContentWithShortcode);
                    }
                }



        /*** technical specification ***/

        if (strpos($htmlContentWithShortcode, '#group_technicals_specifications#') !== false) {
            $request = new Request([
                'code'   =>  'GroupTechnicalsSpecifications',
                'ln'  => $idLanguage,
                'url' => $url,
                'cart_id' => $idCart,
                'userId' => $idUser  
            ]);
            $newHtml = ContentBL::getByCodeRender($request);
            $htmlContentWithShortcode = str_replace('#group_technicals_specifications#', $newHtml,  $htmlContentWithShortcode);
        }

        /*** end technical specification ***/

        /*** item attachment ***/

        if (strpos($htmlContentWithShortcode, '#item_attachment#') !== false) {
            $request = new Request([
                'code'   =>  'ItemAttachmentType',
                'ln'  => $idLanguage,
                'url' => $url,
                'cart_id' => $idCart,
                'userId' => $idUser  
            ]);
            $newHtml = ContentBL::getByCodeRender($request);
            
            if($newHtml != ''){
                $htmlContentWithShortcode = str_replace('#item_attachment#', $newHtml,  $htmlContentWithShortcode);
            }else{
                $htmlContentWithShortcode = str_replace('#item_attachment#', 'Nessun allegato presente',  $htmlContentWithShortcode);
            }
        }

        /*** end item attachment ***/

        /*** optional ***/

        if (strpos($htmlContentWithShortcode, '#optional#') !== false) {
            $request = new Request([
                'url'   =>  $url,
                'ln'  => $idLanguage,
            ]);
            $newHtml = ItemBL::getOptional($request);
            $htmlContentWithShortcode = str_replace('#optional#', $newHtml,  $htmlContentWithShortcode);
        }
        
        /*** end optional ***/

        /*** supports ***/

        if (strpos($htmlContentWithShortcode, '#supports#') !== false) {
            $request = new Request([
                'url'   =>  $url,
                'ln'  => $idLanguage,
            ]);
            $newHtml = ItemBL::getSupports($request);
            $htmlContentWithShortcode = str_replace('#supports#', $newHtml,  $htmlContentWithShortcode);
        }
        
        /*** end supports ***/
        
      
        return $htmlContentWithShortcode;
    }


    /**
     * Return dtoPage to render
     *
     * @param pageRow
     * @param string category
     * @param int page
     * @return DtoPageRender
     */
    private static function _convertToDto($pageRow, string $category = null, int $page = 1)
    {

        $dtoPageRender = new DtoPageRender;

        if (count($pageRow) > 0) {
            $pageArray = ArrayHelper::toCollection($pageRow)[0];
            $dtoPageRender->content = $pageArray->content;

            if (isset($pageArray->hide_breadcrumb)) {
                $dtoPageRender->hideBreadcrumb = $pageArray->hide_breadcrumb;
            } else {
                $dtoPageRender->hideBreadcrumb = true;
            }

            if (isset($pageArray->hide_footer)) {
                $dtoPageRender->hideFooter = $pageArray->hide_footer;
            } else {
                $dtoPageRender->hideFooter = true;
            }

            if (isset($pageArray->hide_header)) {
                $dtoPageRender->hideHeader = $pageArray->hide_header;
            } else {
                $dtoPageRender->hideHeader = true;
            }

            if (isset($pageArray->share_title)) {
                $dtoPageRender->shareTitle = $pageArray->share_title;
            }

            if (isset($pageArray->share_description)) {
                $dtoPageRender->shareDescription = $pageArray->share_description;
            }

            if (isset($pageArray->url_cover_image)) {
                $dtoPageRender->urlCoverImage = $pageArray->url_cover_image;
            }

            if (isset($pageArray->url_share_image)) {
                $dtoPageRender->urlShareImage = $pageArray->url_share_image;
            }

            if (isset($pageArray->visible_from)) {
                $dtoPageRender->visibleFrom = $pageArray->visible_from;
            }

            if (isset($pageArray->visible_end)) {
                $dtoPageRender->visibleEnd = $pageArray->visible_end;
            }

            if (isset($pageArray->hide_search_engine)) {
                $dtoPageRender->hideSearchEngine = $pageArray->hide_search_engine;
            } else {
                $dtoPageRender->hideSearchEngine = true;
            }

            if (isset($pageArray->seo_description)) {
                $dtoPageRender->seoDescription = $pageArray->seo_description;
            }

            if (isset($pageArray->seo_keyword)) {
                $dtoPageRender->seoKeyword = $pageArray->seo_keyword;
            }

            if (isset($pageArray->seo_title)) {
                $dtoPageRender->seoTitle = $pageArray->seo_title;
            }

            if (isset($pageArray->title)) {
                $dtoPageRender->title = $pageArray->title;
            }

            if (isset($pageArray->js)) {
                $dtoPageRender->js = $pageArray->js;
            }

            if (isset($pageArray->enable_js)) {
                $dtoPageRender->enableJS = $pageArray->enable_js;
            } else {
                $dtoPageRender->enableJS = false;
            }

            if (isset($pageArray->protected)) {
                $dtoPageRender->protected = $pageArray->protected;
            } else {
                $dtoPageRender->protected = false;
            }

            #region BLOG

            if (strpos($dtoPageRender->content, '#blog#') !== false) {
                $idCategory = 0;

                
                if (is_null($pageArray->language_id)) {      
                $pageArraylanguage_id = LanguageBL::getDefault()->id;   
               }else{
                $pageArraylanguage_id = $pageArray->language_id;
               }

                if (!is_null($category)) {
                    $idCategory = LanguagePageCategoryBL::getIdPageCategoryByDescriptionAndLanguage($category, $pageArraylanguage_id);
                    if ($idCategory > 0) {
                        $dtoPageRender->blog->title = $category;
                    }
                }

                $idUser = 0;
                if (!is_null($category) && $idCategory == 0) {
                    $idUser = UserBL::getIdByName($category);
                }

                $settingBlogList = SettingEnum::BlogList;
                if ($pageArray->homepage) {
                    $settingBlogList = SettingEnum::HomeBlogList;
                }
       
                $dtoPageRender->blog->data = static::_getBlogsNews($pageArray->language_id, PageTypesEnum::Blog, $idCategory, $idUser, $page, $settingBlogList);
            }

            #endregion BLOG

            #region NEWS

            if (strpos($dtoPageRender->content, '#news#') !== false) {
                $idCategory = 0;


                if (is_null($pageArray->language_id)) {      
                $pageArraylanguage_id = LanguageBL::getDefault()->id;   
               }else{
                $pageArraylanguage_id = $pageArray->language_id;
               }
            

                if (!is_null($category)) {
                    $idCategory = LanguagePageCategoryBL::getIdPageCategoryByDescriptionAndLanguage($category, $pageArraylanguage_id);
                    if ($idCategory > 0) {
                        $dtoPageRender->news->title = $category;
                    }
                }

                $idUser = 0;
                if (!is_null($category) && $idCategory == 0) {
                    $idUser = UserBL::getIdByName($category);
                }

                $settingBlogList = SettingEnum::NewsList;
                if ($pageArray->homepage) {
                    $settingBlogList = SettingEnum::HomeNewsList;
                }

                $dtoPageRender->news->data = static::_getBlogsNews($pageArray->language_id, PageTypesEnum::News, $idCategory, $idUser, $page, $settingBlogList);
            }

            #endregion NEWS
        }

        return $dtoPageRender;
    }


    public static function getByFile(request $request)
    {
        $result = collect();
        $dir = static::$dirImageAttachment;
        $dirNew = static::$dirImageAttachmentImages;
        $cont = 0;
        if($request->page == 1){
            $checkPageStart = 1;
        }else{
            $checkPageStart = ($request->page * 100) - 100;
        }
        $checkPageEnd = $request->page * 100;
        if($request->year != ''){
            if ($request->month != ''){
                $dirNew = $dirNew . '/'. $request->year . '/' . $request->month;
            }else{
                $dirNew = $dirNew . '/'. $request->year;
            }
        }else{
            if ($request->month != ''){
                $dirNew = $dirNew . '/'. date('Y') . '/' . $request->month;
            }
        }

        $extRequest = ['.png', '.jpg', '.jpeg', '.gif', '.webp'];

        if(isset($request->ext)){
            if($request->ext!=''){
                if($request->ext=='.jpg'){
                    $extRequest = ['.jpg', '.jpeg'];
                }else{
                    $extRequest = [$request->ext];
                }
            }
        }

        $resultImage = static::_getFileDir($dirNew, $request->year, $request->month);
        $arrayS = $resultImage->toArray();

        usort($arrayS, function ($a, $b) {
            return $a->dateorder < $b->dateorder;
        });

        foreach ($arrayS as $f) {
        //foreach (\File::files($dir) as $f) {
            if (ends_with($f->name, $extRequest)) {
                if($request->search != '' && !is_null($request->search)){
                    if (strpos(strtolower($f->name), strtolower($request->search)) !== false) {
                        $cont = $cont + 1;
                        if($cont >= $checkPageStart && $cont < $checkPageEnd){
                            $listImages = new DtoImage();
                            $listImages->id = $cont;
                            $listImages->idImgAgg = '';
                            $listImages->img =  $f->url;
                            $listImages->imgName = $f->name;

                            //$listImages->title = $f->nameFile;
                            $listImages->title = str_replace('.' . $f->ext, '', str_replace('-', ' ', $f->nameFile));
                            $listImages->visible = true;


                            if($request->imgActive != '' && $cont == 1){
                                $listImages->active = true;
                                $listImages->selected = true;
                            }else{
                                $listImages->active = false;
                                $listImages->selected = false;
                            }

                            $listImages->alt = str_replace('.' . $f->ext, '', str_replace('-', ' ', $f->nameFile));
                            $listImages->didascalia = '';
                            $listImages->width = $f->width;
                            $listImages->height = $f->height;
                            $listImages->weight = $f->weight;
                            $listImages->date = $f->date;
                            $listImages->dateorder = $f->dateorder;
                            $listImages->url = $f->url;

                            //controllo su tabella image_resize
                            $getSrcSet = static::_getStringImageResized($f->nameFile, $f->ext, $request->year, $request->month);
                            $listImages->srcset = $getSrcSet;

                            $listImages->ext = $f->ext;
                            $result->push($listImages);
                        }
                    }
                }else{
                    $cont = $cont + 1;
                    if($cont >= $checkPageStart && $cont < $checkPageEnd){
                        $listImages = new DtoImage();
                        $listImages->id = $cont;
                        $listImages->idImgAgg = '';
                        $listImages->img = $f->url;
                        $listImages->imgName = $f->name;
                        $listImages->title = str_replace('.' . $f->ext, '', str_replace('-', ' ', $f->nameFile));
                        $listImages->visible = true;

                        if($request->imgActive != '' && $cont == 1){
                            $listImages->active = true;
                            $listImages->selected = true;
                        }else{
                            $listImages->active = false;
                            $listImages->selected = false;
                        }

                        $listImages->alt = str_replace('.' . $f->ext, '', str_replace('-', ' ', $f->nameFile));
                        $listImages->didascalia = '';
                        $listImages->width = $f->width;
                        $listImages->height = $f->height;
                        $listImages->weight = $f->weight;
                        $listImages->date = $f->date;
                        $listImages->dateorder = $f->dateorder;
                        $listImages->url = $f->url;

                        //controllo su tabella image_resize
                        $getSrcSet = static::_getStringImageResized($f->nameFile, $f->ext, $request->year, $request->month);
                        $listImages->srcset = $getSrcSet;

                        $listImages->ext = $f->ext;
                        $result->push($listImages);
                    }
                }
            }
        }
        return $result;
    }


    public static function searchFile(request $request)
    {
        $result = '';
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        
        if (file_exists('../storage/app/public/' . $request->path . '/' . $request->search . '.' . $request->ext)) {
            $result = $protocol . $_SERVER["HTTP_HOST"] . '/storage/' . $request->path . '/' . $request->search . '.' . $request->ext;
        }else{
            if (file_exists('../storage/app/public/' . $request->path . '/' . $request->search . '.' . strtoupper($request->ext))) {
                $result = $protocol . $_SERVER["HTTP_HOST"] . '/storage/' . $request->path . '/' . $request->search . '.' . strtoupper($request->ext);
            }
        }
        
        return $result;
    }


    public static function getByFileEditorAll()
    {
        $result = array();

        $year = "";
        $month = "";

        $dir = static::$dirImageAttachment;
        $dirNew = static::$dirImageAttachmentImages;
        
        $resultImage = static::_getFileDir($dirNew, $year, $month);

        foreach ($resultImage as $f) {
            if (ends_with($f->name, ['.png', '.jpg', '.jpeg', '.gif', '.webp'])) {
                $response = new stdClass();
                $response->url = $f->url;
                array_push($result, $response);
            }
        }

        return $result;
    }

    private static function getUrlImage()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/';
    }


    /**
     * Return dtoPage to render
     *
     * @param pageRow
     * @param string category
     * @param int page
     * @return DtoPageRender
     */
    private static function _convertToDtoUser($pageRow, string $category = null, int $page = 1, string $idUser = null , string $idCart = null, string $idLanguage = null, string $url = null, $param = null, string $salesmanId = null)
    {

        $dtoPageRender = new DtoPageRender;

        if (count($pageRow) > 0) {
            $pageArray = ArrayHelper::toCollection($pageRow)[0];

            //inserimento funzione per replace tutti gli shortcode
            $cleanContent = static::_convertHtmlWithoutShortcode($pageArray->content, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
            
            if(isset($cleanContent)){
                $dtoPageRender->content = $cleanContent;
            }else{
                $dtoPageRender->content = $pageArray->content;
            }

            //se è una pagina di tipo "BLOG", devo aggiungere titolo, autore, categoria, ecc...all'articolo  
            if(isset($pageArray->tipopagina) && ($pageArray->tipopagina == 'Blog'|| $pageArray->tipopagina == 'News')){
                //mi prendo il contenuto 
               
                $request = new Request([
                    'code'   =>  'HeaderArticle',
                    'ln'  => $idLanguage,
                    'url' => $url
                ]);
                $newHtml = ContentBL::getByCodeRender($request);
                $dtoPageRender->content = $newHtml.$dtoPageRender->content;
            }
            
            if (isset($pageArray->hide_breadcrumb)) {
                $dtoPageRender->hideBreadcrumb = $pageArray->hide_breadcrumb;
            } else {
                $dtoPageRender->hideBreadcrumb = true;
            }

            if (isset($pageArray->hide_footer)) {
                $dtoPageRender->hideFooter = $pageArray->hide_footer;
            } else {
                $dtoPageRender->hideFooter = true;
            }

            if (isset($pageArray->hide_header)) {
                $dtoPageRender->hideHeader = $pageArray->hide_header;
            } else {
                $dtoPageRender->hideHeader = true;
            }

            if (isset($pageArray->share_title)) {
                $dtoPageRender->shareTitle = $pageArray->share_title;
            }

            if (isset($pageArray->share_description)) {
                $dtoPageRender->shareDescription = $pageArray->share_description;
            }

            if (isset($pageArray->url_cover_image)) {
                $dtoPageRender->urlCoverImage = $pageArray->url_cover_image;
            }

            if (isset($pageArray->url_share_image) && !is_null($pageArray->url_share_image)) {
                $dtoPageRender->urlCoverImage = $pageArray->url_share_image;
            }

            if (isset($pageArray->visible_from)) {
                $dtoPageRender->visibleFrom = $pageArray->visible_from;
            }

            if (isset($pageArray->visible_from)) {
                $dtoPageRender->visibleFrom = $pageArray->visible_from;
            }

            if (isset($pageArray->publish)) {
                $dtoPageRender->publishDate = $pageArray->publish;
            }else{
                if (isset($pageArray->created_at)) {
                    $dtoPageRender->publishDate = $pageArray->created_at;
                }else{
                    $dtoPageRender->publishDate = '';
                }
            }

            if (isset($pageArray->hide_search_engine)) {
                $dtoPageRender->hideSearchEngine = $pageArray->hide_search_engine;
            } else {
                $dtoPageRender->hideSearchEngine = true;
            }

            if (isset($pageArray->seo_description)) {
                $dtoPageRender->seoDescription = $pageArray->seo_description;
            }

            if (isset($pageArray->seo_keyword)) {
                $dtoPageRender->seoKeyword = $pageArray->seo_keyword;
            }

            if (isset($pageArray->seo_title)) {
                $dtoPageRender->seoTitle = $pageArray->seo_title;
                $dtoPageRender->title = $pageArray->seo_title;
            }else{
                $dtoPageRender->seoTitle = $pageArray->title;
                $dtoPageRender->title = $pageArray->title;
            }

            /*if (isset($pageArray->title)) {
                $dtoPageRender->title = $pageArray->title;
            }*/

            if (isset($pageArray->js)) {
                $dtoPageRender->js = $pageArray->js;
            }

            if (isset($pageArray->redirect)) {
                $dtoPageRender->redirect = $pageArray->redirect;
            }else{
                $dtoPageRender->redirect = false;
            }

            if (isset($pageArray->enable_js)) {
                $dtoPageRender->enableJS = $pageArray->enable_js;
            } else {
                $dtoPageRender->enableJS = false;
            }

            if (isset($pageArray->protected)) {
                $dtoPageRender->protected = $pageArray->protected;
            } else {
                $dtoPageRender->protected = false;
            }

            if (isset($pageArray->rss)) {
                $dtoPageRender->rss = $pageArray->rss;
            } else {
                $dtoPageRender->rss = false;
            }

            #region BLOG
            
            if (strpos($dtoPageRender->content, '#blog#') !== false) {
                $idCategory = 0;
             
                if (is_null($pageArray->language_id)) {      
                $pageArraylanguage_id = LanguageBL::getDefault()->id;   
               }else{
                $pageArraylanguage_id = $pageArray->language_id;
               }

                if (!is_null($category)) {
                    $idCategory = LanguagePageCategoryBL::getIdPageCategoryByDescriptionAndLanguage($category, $pageArraylanguage_id);
                    if ($idCategory > 0) {
                        $dtoPageRender->blog->title = $category;
                    }
                }

                $idUser = 0;
                if (!is_null($category) && $idCategory == 0) {
                    $idUser = UserBL::getIdByName($category);
                }

                $settingBlogList = SettingEnum::BlogList;
                if ($pageArray->homepage) {
                    $settingBlogList = SettingEnum::HomeBlogList;
                }
                

                $dtoPageRender->blog->data = static::_getBlogsNews($pageArray->language_id, PageTypesEnum::Blog, $idCategory, $idUser, $page, $settingBlogList);
            }

            #endregion BLOG

            #region NEWS

            if (strpos($dtoPageRender->content, '#news#') !== false) {
                $idCategory = 0;
                if (!is_null($category)) {
                    $idCategory = LanguagePageCategoryBL::getIdPageCategoryByDescriptionAndLanguage($category, $pageArray->language_id);
                    if ($idCategory > 0) {
                        $dtoPageRender->news->title = $category;
                    }
                }

                $idUser = 0;
                if (!is_null($category) && $idCategory == 0) {
                    $idUser = UserBL::getIdByName($category);
                }

                $settingBlogList = SettingEnum::NewsList;
                if ($pageArray->homepage) {
                    $settingBlogList = SettingEnum::HomeNewsList;
                }

                $dtoPageRender->news->data = static::_getBlogsNews($pageArray->language_id, PageTypesEnum::News, $idCategory, $idUser, $page, $settingBlogList);
            }

            #endregion NEWS
        }

        return $dtoPageRender;
    }

    /**
     * Return blogs or news Dto
     * 
     * @param int idLanguage
     * @param string pageType
     * @param int idCategory
     * @param int idUser
     * @param int page
     * @param string settingList
     * 
     * @return DtoBlogNewsData
     */
    private static function _getBlogsNews(int $idLanguage, string $pageType, string $idCategory, int $idUser, int $page = 1, string $settingList = null)
    {
        $result = collect();

        $query = "  SELECT *
                    FROM pages
                    INNER JOIN languages_pages ON pages.id = languages_pages.page_id
                    WHERE pages.online = true
                    AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() )
                    AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() )
                    AND languages_pages.content IS NOT NULL
                    AND pages.page_type = '" . $pageType . "'
                    AND languages_pages.language_id = " . $idLanguage;

        if ($idCategory > 0) {  
            $query = $query . " AND pages.page_category_id = " . $idCategory;
        }

        if ($idUser > 0) {
            $query = $query . " AND pages.author_id = " . $idUser;
        }
        $query = $query . " ORDER BY pages.publish DESC";
        $query = $query . " OFFSET ((" . $page . " - 1) * (SELECT VALUE FROM settings WHERE code = '" . $settingList . "')::INTEGER) 
                            LIMIT (SELECT VALUE FROM settings WHERE code = '" . $settingList . "')::INTEGER";

        foreach (DB::select($query) as $blogNews) {
            $blogNewsData = new DtoBlogNewsData();
            if (is_null($blogNews->seo_title)) {
                $blogNewsData->title = $blogNews->title;
            } else {
                $blogNewsData->title = $blogNews->seo_title;
            }
            $blogNewsData->urlCoverImage = $blogNews->url_cover_image;
            $blogNewsData->url = $blogNews->url;
            //$blogNewsData->text = substr(Html2Text::convert($blogNews->content), 0, 100);
            $blogNewsData->text = $blogNews->seo_description;
            if (!is_null($blogNews->page_category_id)) {
                array_push($blogNewsData->category, LanguagePageCategoryBL::getByIdAndLanguage($blogNews->page_category_id, $blogNews->language_id));
            }
            $blogNewsData->date = $blogNews->publish;
            if (!is_null($blogNews->author_id)) {
                $blogNewsData->author = UserBL::getById($blogNews->author_id)->name;
            }
            $result->push($blogNewsData);
        }

        return $result;
    }

    /**
     * Validate data
     *
     * @param Request $dtoPageSetting
     * @param int $idLanguage
     */
    private static function _validateData(Request $dtoPageSetting, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE && is_null($dtoPageSetting->id)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($dtoPageSetting->idAuthor) && is_null(User::find($dtoPageSetting->idAuthor))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::AuthorNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($dtoPageSetting->idIcon) && is_null(Icon::find($dtoPageSetting->idIcon))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IconNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($dtoPageSetting->idFunctionality)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Functionality::find($dtoPageSetting->idFunctionality))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (!is_null($dtoPageSetting->idPageCategory) && is_null(PageCategory::find($dtoPageSetting->idPageCategory))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PageCategoryNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($dtoPageSetting->idPageShareType) && is_null(PageShareType::find($dtoPageSetting->idPageShareType))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PageShareTypeNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        foreach ($dtoPageSetting->languagePageSetting as $languagePageSetting) {
            if (count($languagePageSetting)>0){
                if(!is_array($languagePageSetting)){
                    if (!isset($languagePageSetting['idLanguage'])) {
                        throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::LanguageNotValued), HttpResultsCodesEnum::InvalidPayload);
                    } else {
                        if (is_null(Language::find($languagePageSetting['idLanguage']))) {
                            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::LanguageNotExist), HttpResultsCodesEnum::InvalidPayload);
                        }
                    }
                    $requestUrl = new Request();

                    if(isset($languagePageSetting['url'])){
                        $requestUrl->merge(['url' => $languagePageSetting['url']]);
                        $requestUrl->merge(['idPage' => $dtoPageSetting->id]);

                        if (!is_null($languagePageSetting['url']) && (($dbOperationType == DbOperationsTypesEnum::INSERT && LanguagePageBL::checkExistUrl($requestUrl)) 
                        //|| ($dbOperationType == DbOperationsTypesEnum::UPDATE && LanguagePageBL::checkExistUrl($requestUrl))
                        )) {
                            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UrlAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                        }
                    }
                }
            }
        }
    }

    /**
     * Check configuration and return data to save
     *
     * @param Request $dtoPageSetting
     * @param DtoConfiguration $config
     * @param DtoPageSetting $dtoOnDB
     * @return DtoPageSetting
     */
    private static function _checkConfiguration(Request $dtoPageSetting, DtoConfiguration $config, DtoPageSetting $dtoOnDB = null)
    {
        $dtoPageSettingToReturn = ModelHelper::checkConfiguration(new DtoPageSetting(), $dtoPageSetting, $config->tabsConfigurations->where('code', 'Settings')->first()->pageConfigurations, 'languagePageSetting', new DtoLanguagePageSetting);

        if (!is_null($dtoPageSetting->online)) {
            $dtoPageSettingToReturn->online = $dtoPageSetting->online;
        }

        $cont = 0;
        if ($config->tabsConfigurations->where('code', 'Content')->count() > 0) {
            foreach ($dtoPageSetting->languagePageSetting as $languagePageSetting) {
                if(is_array($languagePageSetting)){
                    if(count($languagePageSetting) < 6){

                    }else{
                        $dtoPageSettingToReturn->languagePageSetting->where('idLanguage', $languagePageSetting['idLanguage'])->first()->content = $languagePageSetting['content'];
                    }
                }
            }
        }

        if (
            $config->tabsConfigurations->where('code', 'Content')->count() > 0 &&
            $config->tabsConfigurations->where('code', 'Content')->first()->pageConfigurations->where('field', 'js')->count() > 0
        ) {
            $dtoPageSettingToReturn->js = $dtoPageSetting->js;
        }

        if ($dtoPageSetting->pageType == 'Blog' || $dtoPageSetting->pageType == 'News') {
            return $dtoPageSetting;
        } else {
            if ($dtoPageSetting->pageType == 'Page' || $dtoPageSetting->pageType == '') {
                return $dtoPageSetting;
            } else {
                return $dtoPageSettingToReturn;
            }
        }
    }

    /**
     * Convert from dtoPageSetting to Page
     *
     * @param $dtoPageSetting
     * @return Page
     */
    private static function _convertToModelNewsBlog($dtoPageSetting)
    {
        
        $page = new Page();

        if (isset($dtoPageSetting->fixed)) {
            $page->fixed = $dtoPageSetting->fixed;
        }

        if (isset($dtoPageSetting->fixedEnd)) {
            $page->fixed_end = $dtoPageSetting->fixedEnd;
        }else{
            $page->fixed_end = null;
        }

        if (isset($dtoPageSetting->hideSearchEngine)) {
            $page->hide_search_engine = $dtoPageSetting->hideSearchEngine;
        } else {
            $page->hide_search_engine = false;
        }

        if (isset($dtoPageSetting->homepage)) {
            $page->homepage = $dtoPageSetting->homepage;
        }else{
            $page->homepage = false;
        }

        if (isset($dtoPageSetting->id)) {
            $page->id = $dtoPageSetting->id;
        }

       /* if (isset($dtoPageSetting->idAuthor)) {
            $page->author_id = $dtoPageSetting->idAuthor;
        }*/

       if ($dtoPageSetting->languagePageSetting[0]['url'] == '/') {
            $page->icon_id = Icon::where('css', 'fa fa-home')->first()->id; 
        }else{
            $page->icon_id = Icon::where('css', 'fa fa-list-alt')->first()->id; 
        } 
        
        if (isset($dtoPageSetting->idUser)) {
            $page->author_id = $dtoPageSetting->idUser;
        }

       /* if (isset($dtoPageSetting->idIcon)) {
            $page->icon_id = $dtoPageSetting->idIcon;
        }*/

        if (isset($dtoPageSetting->idPageCategory)) {
            $page->page_category_id = $dtoPageSetting->idPageCategory;
        }

        if (isset($dtoPageSetting->idPageShareType)) {
            $page->page_share_type_id = $dtoPageSetting->idPageShareType;
        }

        if (isset($dtoPageSetting->online)) {
            $page->online = $dtoPageSetting->online;
        }

        if (isset($dtoPageSetting->publish)) {
            $page->publish = $dtoPageSetting->publish;
        }else{
            $page->publish = null;
        }

        if (isset($dtoPageSetting->visibleEnd)) {
            $page->visible_end = $dtoPageSetting->visibleEnd;
        }else{
            $page->visible_end = null;
        }
        
        if (isset($dtoPageSetting->visibleFrom)) {
            $page->visible_from = $dtoPageSetting->visibleFrom;
        }else{
            $page->visible_from = null;
        }

        if (isset($dtoPageSetting->pageType)) {
            $page->page_type = $dtoPageSetting->pageType;
        }

        if (isset($dtoPageSetting->js)) {
            $page->js = $dtoPageSetting->js;
        }

        if (isset($dtoPageSetting->enableJS)) {
            $page->enable_js = $dtoPageSetting->enableJS;
        }else{
            $page->enable_js = true;
        }

        if (isset($dtoPageSetting->protected)) {
            $page->protected = $dtoPageSetting->protected;
        }

        return $page;
    }

    /**
     * Convert from dtoPageSetting to Page
     *
     * @param DtoPageSetting $dtoPageSetting
     * @return Page
     */
    private static function _convertToModel($dtoPageSetting)
    {
        $page = new Page();

        if (isset($dtoPageSetting->fixed)) {
            $page->fixed = $dtoPageSetting->fixed;
        }

        if (isset($dtoPageSetting->fixedEnd)) {
            $page->fixed_end = $dtoPageSetting->fixedEnd;
        }

        if (isset($dtoPageSetting->hideSearchEngine)) {
            $page->hide_search_engine = $dtoPageSetting->hideSearchEngine;
        } else {
            $page->hide_search_engine = false;
        }

        if (isset($dtoPageSetting->homepage)) {
            $page->homepage = $dtoPageSetting->homepage;
        }

        if (isset($dtoPageSetting->id)) {
            $page->id = $dtoPageSetting->id;
        }

        if (isset($dtoPageSetting->idAuthor)) {
            $page->author_id = $dtoPageSetting->idAuthor;
        }

        if ($dtoPageSetting->languagePageSetting[0]['url'] == '/') {
            $page->icon_id = Icon::where('css', 'fa fa-home')->first()->id; 
        }else{
            $page->icon_id = Icon::where('css', 'fa fa-list-alt')->first()->id; 
        } 

       /* if (isset($dtoPageSetting->idIcon)) {
            $page->icon_id = $dtoPageSetting->idIcon;
        }*/

        if (isset($dtoPageSetting->idPageCategory)) {
            $page->page_category_id = $dtoPageSetting->idPageCategory;
        }

        if (isset($dtoPageSetting->idPageShareType)) {
            $page->page_share_type_id = $dtoPageSetting->idPageShareType;
        }

        if (isset($dtoPageSetting->online)) {
            $page->online = $dtoPageSetting->online;
        }

        if (isset($dtoPageSetting->publish)) {
            $page->publish = $dtoPageSetting->publish;
        }

        if (isset($dtoPageSetting->visibleEnd)) {
            $page->visible_end = $dtoPageSetting->visibleEnd;
        }

        if (isset($dtoPageSetting->visibleFrom)) {
            $page->visible_from = $dtoPageSetting->visibleFrom;
        }
        
        if (isset($dtoPageSetting->pageType)) {
            $page->page_type = $dtoPageSetting->pageType;
        } else {
            $page->page_type = 'Page';
        }

        if (isset($dtoPageSetting->js)) {
            $page->js = $dtoPageSetting->js;
        }

        if (isset($dtoPageSetting->enableJS)) {
            $page->enable_js = $dtoPageSetting->enableJS;
        }else{
            $page->enable_js = false;
        }

        if (isset($dtoPageSetting->protected)) {
            $page->protected = $dtoPageSetting->protected;
        }
        return $page;
    }

    /**
     * Set home
     *
     * @param int $id
     */
    private static function _setHome(int $id)
    {
        Page::where('homepage', true)->update(['homepage' => false]);
        Page::find($id)->update(['homepage' => true]);
    }

    /**
     * Insert or update url
     *
     * @param DtoUrlList $dtoUrlList
     * @param int $idLanguage
     *
     * @return DtoResult
     */
    private static function _insertOrUpdateUrl($dtoUrlList, int $idLanguage)
    {
        $dtoResult = new DtoResult();
        $dtoResult->code = 200;

        if (is_null($dtoUrlList['idPage'])) {
            $dtoResult->code = HttpResultsCodesEnum::InvalidPayload;
            $dtoResult->message = DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PageNotValued);
        } else {
            if (Page::find($dtoUrlList['idPage'])->count() == 0) {
                $dtoResult->code = HttpResultsCodesEnum::InvalidPayload;
                $dtoResult->message = DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PageNotExist);
            } else {
                foreach ($dtoUrlList['urlListDetail'] as $dtoUrlListDetail) {
                    if (!is_null($dtoUrlListDetail['idLanguage'])) {
                        $dtoResult->code = HttpResultsCodesEnum::InvalidPayload;
                        $dtoResult->message = DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::LanguageNotValued);
                    } else {
                        if (Language::find($dtoUrlListDetail['idLanguage'])) {
                            $dtoResult->code = HttpResultsCodesEnum::InvalidPayload;
                            $dtoResult->message = DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::LanguageNotExist);
                        } else {
                            if (!is_null($dtoUrlListDetail->url)) {
                                LanguagePageBL::insertOrUpdateUrl($dtoUrlListDetail, $dtoUrlList['idPage'], $idLanguage);
                            } else {
                                LanguagePageBL::deleteById($dtoUrlListDetail['id']);
                            }
                        }
                    }
                }
            }
        }

        return $dtoResult;
    }

    /**
     * Get sub menu
     *
     * @param $rows
     * @param int $idMenuFather
     */
    private static function _getSubMenu($rows, $idMenuFather)
    {
        $dtoSubPageMenuLists = collect();

        foreach (ArrayHelper::toCollection($rows)->whereNotIn('menu_id', '')->whereIn('menu_father_id', $idMenuFather)->sortBy('order_menu') as $subRow) {
            $dtoSubPageMenuList = new DtoPageMenuList();
            $dtoSubPageMenuList->id = $subRow->id;
            $dtoSubPageMenuList->online = $subRow->online;
            $dtoSubPageMenuList->title = $subRow->title;
            $dtoSubPageMenuList->idMenu = $subRow->menu_id;
            $dtoSubPageMenuList->idMenuFather = $subRow->menu_father_id;
            $dtoSubPageMenuList->orderMenu = $subRow->order_menu;
            $dtoSubPageMenuList->cssIcon = $subRow->css_icon;
            $dtoSubPageMenuList->menuType = $subRow->menu_type;
            $dtoSubPageMenuList->url = $subRow->url;
            $dtoSubPageMenuList->subMenu = static::_getSubMenu($rows, $subRow->menu_id);
            $dtoSubPageMenuLists->push($dtoSubPageMenuList);
        }

        return $dtoSubPageMenuLists;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get menu list
     *
     * @param int id
     * @param string pageType
     * @return DtoPageList
     */
    public static function getMenuList(int $id, $pageType)
    {
        $dtoPageList = new DtoPageList();
        if ($pageType == PageTypesEnum::Page) {
            $rows = DB::select("-- PAGE
                              SELECT  PAG.id, PAG.online AS online, PAG.blocked AS blocked, LPA.title AS title, MEN.id AS menu_id, MEN.menu_id AS menu_father_id, MEN.order AS order_menu,
                                      ICO.css AS css_icon, PAG.page_type AS menu_type, LPA.url as url
                              FROM pages PAG
                              INNER JOIN languages_pages LPA ON PAG.id = LPA.page_id
                              LEFT JOIN icons ICO ON PAG.icon_id = ICO.id
                              LEFT JOIN menus MEN ON PAG.ID = MEN.page_id
                              WHERE ((PAG.page_type='" . $pageType . "' OR PAG.page_type='Link') AND LPA.language_id = " . $id . ") ORDER BY LPA.title ASC 
                            
                              --MENU WITHOUT PAGE
                            /*  SELECT  NULL AS id, NULL AS online, NULL AS blocked, LMN.description AS title, MEN.id AS menu_id, MEN.menu_id AS menu_father_id,
                                        MEN.order AS order_menu, ICO.css AS css_icon,
                                        CASE LMN.link
                                            WHEN NULL THEN " . MenusTypesEnum::Folder . "
                                            ELSE " . MenusTypesEnum::Link . "
                                        END AS menu_type,
                                        '' as url
                                FROM menus MEN
                                INNER JOIN languages_menus LMN ON MEN.id = LMN.menu_id
                                LEFT JOIN icons ICO ON MEN.icon_id = ICO.id
                                WHERE MEN.page_id IS NULL
                                AND ( LMN.language_id = " . $id . ")*/
                                
                              ");
        } else {
            $rows = DB::select("SELECT  PAG.id, PAG.online AS online, PAG.blocked AS blocked, LPA.title AS title, MEN.id AS menu_id, MEN.menu_id AS menu_father_id, MEN.order AS order_menu,
                                        ICO.css AS css_icon, " . MenusTypesEnum::Page . " AS menu_type, LPA.url as url
                                FROM pages PAG
                                INNER JOIN languages_pages LPA ON PAG.id = LPA.page_id
                                LEFT JOIN icons ICO ON PAG.icon_id = ICO.id
                                LEFT JOIN menus MEN ON PAG.ID = MEN.page_id
                                WHERE (PAG.page_type='" . $pageType . "' AND LPA.language_id = " . $id . ") ORDER BY PAG.publish DESC");
        }

        foreach (ArrayHelper::toCollection($rows)->whereNotIn('menu_id', '')->whereIn('menu_father_id', '')->sortBy('order_menu') as $row) {
            $dtoPageMenuList = new DtoPageMenuList();
            $dtoPageMenuList->id = $row->id;
            $dtoPageMenuList->online = $row->online;
            $dtoPageMenuList->title = $row->title;
            $dtoPageMenuList->idMenu = $row->menu_id;
            $dtoPageMenuList->orderMenu = $row->order_menu;
            $dtoPageMenuList->cssIcon = $row->css_icon;
            $dtoPageMenuList->menuType = $row->menu_type;
            $dtoPageMenuList->url = $row->url; 

            $Page = Page::where('id', $row->id)->get()->first();
            if(isset($Page)){
                $dtoPageMenuList->visible_from = $Page->visible_from;
            }else{
                $dtoPageMenuList->visible_from = '';
            }
            if(isset($Page)){
                $dtoPageMenuList->publish = $Page->publish;
            }else{
                $dtoPageMenuList->publish = '';
            }
            $dtoPageMenuList->blocked = $row->blocked;
            $dtoPageMenuList->subMenu = static::_getSubMenu($rows, $row->menu_id);
            $dtoPageList->menuList->push($dtoPageMenuList);
        }

        foreach (ArrayHelper::toCollection($rows)->whereIn('menu_id', '') as $row) {
            $dtoPageMenuList = new DtoPageMenuList();
            $dtoPageMenuList->id = $row->id;
            $dtoPageMenuList->online = $row->online;
            $dtoPageMenuList->title = $row->title;
            $dtoPageMenuList->idMenu = $row->menu_id;
            $dtoPageMenuList->idMenuFather = $row->menu_father_id;
            $dtoPageMenuList->orderMenu = $row->order_menu;
            $dtoPageMenuList->cssIcon = $row->css_icon;
            $dtoPageMenuList->menuType = $row->menu_type;
            $dtoPageMenuList->url = $row->url;

            if ($row->url== '/'){
                //$dtoPageMenuList->cssStyle = '"color:#1ea076!important;font-size: 1rem!important;margin-top: 2px!important;"';
                   $dtoPageMenuList->cssStyle = 'home';
            }else{
                    $dtoPageMenuList->cssStyle = '';
            }
         
            $Page = Page::where('id', $row->id)->get()->first();
            if(isset($Page)){
                $dtoPageMenuList->visible_from = $Page->visible_from;
            }else{
                $dtoPageMenuList->visible_from = '';
            }
            if(isset($Page)){
                $dtoPageMenuList->publish = $Page->publish;
            }else{
                $dtoPageMenuList->publish = '';
            }

            $LanguagesPages= LanguagePage::where('page_id', $Page->id)->get()->first();
            if(isset($Page)){
                $dtoPageMenuList->seotitle = $LanguagesPages->seo_title;
            }else{
                $dtoPageMenuList->seotitle = '';
            }
            if(isset($Page)){
                $dtoPageMenuList->seodescription = $LanguagesPages->seo_description;
            }else{
                $dtoPageMenuList->seodescription = '';
            }


            $explodeseokeyword = explode(",", $LanguagesPages->seo_keyword);
            $cont=0;
            foreach ($explodeseokeyword as $valore) {
                
                    if (ltrim(rtrim($valore))){
                        $cont= $cont+1;  
                    }
                }
                $dtoPageMenuList->seokeyword = $cont;

           

            $dtoPageMenuList->blocked = $row->blocked;
            $dtoPageList->pageList->push($dtoPageMenuList);
        }
        return $dtoPageList;
    }


    public static function insertItemCategoriesPages(Request $request, int $idUser, int $idLanguage)
    {
        if (is_null($request->idCategories)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        $newPagePageCategory = new PagePageCategory();
        $newPagePageCategory->page_id = $request->idPages;
        $newPagePageCategory->page_category_id = $request->idCategories;
        $newPagePageCategory->created_id = $idUser;
        $newPagePageCategory->save();

        return $newPagePageCategory->id;
    }

    public static function deleteItemCategoriesPages(int $idCategories, int $idPages, int $idLanguage)
    {
        if (is_null($idCategories)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }
        $PagePageCategory = PagePageCategory::where('page_id', $idPages)->where('page_category_id', $idCategories);
        $PagePageCategory->delete();
    }

    /**
     * Get page setting
     *
     * @param int $id
     * @return DtoPageSetting
     */
    public static function getSetting(int $id, int $sUserId)
    {
        $page = Page::find($id);
        $menu = Menu::where('page_id', $id)->first();

        $dtoPageSetting = new DtoPageSetting();
        $dtoPageSetting->fixed = $page->fixed;
        $dtoPageSetting->fixedEnd = $page->fixed_end;
        if ($page->page_type != 'Blog' || $page->page_type != 'News') {
            if (!is_null($menu)) {
                $dtoPageSetting->hideMenu = false;
                $dtoPageSetting->hideMenuMobile = $menu->hide_mobile;
                $dtoPageSetting->newWindow = $menu->new_window;
            } else {
                $dtoPageSetting->hideMenu = true;
                $dtoPageSetting->hideMenuMobile = true;
            }
        }

        $dtoPageSetting->hideSearchEngine = $page->hide_search_engine;
        $dtoPageSetting->homePage = $page->homepage;
        $dtoPageSetting->id = $page->id;
        //$dtoPageSetting->idAuthor = $page->author_id;
        
        $dtoPageSetting->idIcon = $page->icon_id;


        $dtoPageSetting->idPageCategory = $page->page_category_id;
        $dtoPageSetting->idPageShareType = $page->page_share_type_id;
        $dtoPageSetting->pageType = $page->page_type;

        foreach (LanguagePage::where('page_id', $id)->get() as $languagePage) {
            $dtoLanguagePageSetting = new DtoLanguagePageSetting;
            $dtoLanguagePageSetting->content = $languagePage->content;
            $dtoLanguagePageSetting->idLanguage = $languagePage->language_id;
            $dtoLanguagePageSetting->seoTitle = $languagePage->seo_title;
            $dtoLanguagePageSetting->seoDescription = $languagePage->seo_description;
            $dtoLanguagePageSetting->SeoKeyword = $languagePage->seo_keyword;
            $dtoLanguagePageSetting->title = $languagePage->seo_title;
            $dtoLanguagePageSetting->shareDescription = $languagePage->share_description;
            $dtoLanguagePageSetting->shareTitle = $languagePage->share_title;
            $dtoLanguagePageSetting->hideBreadcrumb = $languagePage->hide_breadcrumb;
            $dtoLanguagePageSetting->hideFooter = $languagePage->hide_footer;
            $dtoLanguagePageSetting->hideHeader = $languagePage->hide_header;
            $dtoLanguagePageSetting->title = $languagePage->title;
            $dtoLanguagePageSetting->url = $languagePage->url;
            $dtoLanguagePageSetting->urlCanonical = $languagePage->urlCanonical;
            $dtoLanguagePageSetting->urlCoverImage = $languagePage->url_cover_image;
            $dtoLanguagePageSetting->urlShareImage = $languagePage->url_share_image;
            $dtoPageSetting->languagePageSetting->push($dtoLanguagePageSetting);
        }

       if (!is_null($page->author_id)) {
        $userName= User::where('id', $page->author_id)->first()->name; 
        $dtoPageSetting->idUser = $page->author_id;
        $dtoPageSetting->userName = $userName;
       }else{
       // if (is_null($sUserId)) {
        $userNameU= User::where('id', $sUserId)->get()->first(); 
        if (isset($userNameU)) {
        $dtoPageSetting->idUser = $sUserId;
        $dtoPageSetting->userName = $userNameU;
        } 
       //}
       }

        $dtoPageSetting->online = $page->online;
        $dtoPageSetting->publish = $page->publish;
        $dtoPageSetting->visibleEnd = $page->visible_end;
        $dtoPageSetting->visibleFrom = $page->visible_from;
        $dtoPageSetting->js = $page->js;
        $dtoPageSetting->enableJS = $page->enable_js;
        $dtoPageSetting->protected = $page->protected;

        return $dtoPageSetting;
       
    }


    //sUserId
//getByUserDescription
   /**
     * Get home page
     *
     * @param int $idLanguage
     * @return DtoPageRender
     */
    public static function getByUserDescription(int $sUserId)
    {
        $userName= User::where('id', $sUserId)->first()->name;

        $dtoPageSetting = new DtoPageSetting();
        $dtoPageSetting->idUser = $sUserId;
        $dtoPageSetting->userName = $userName;
        return $dtoPageSetting;
    }


    /**
     * Get home page
     *
     * @param int $idLanguage
     * @return DtoPageRender
     */
    public static function getHome(int $idLanguage = 0)
    {
        return static::_convertToDto(DB::select("  SELECT *
                                                FROM pages
                                                INNER JOIN languages_pages ON pages.id = languages_pages.page_id
                                                WHERE pages.online = true
                                                AND pages.homepage = true
                                                AND languages_pages.language_id =
                                                COALESCE ( (  SELECT languages_pages.language_id
                                                            FROM pages
                                                            INNER JOIN languages_pages ON pages.id = languages_pages.page_id
                                                            WHERE pages.online = true
                                                            AND pages.homepage = true
                                                            AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() )
                                                            AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() )
                                                            AND  languages_pages.language_id = " . $idLanguage . " )
                                                        , ( SELECT l.id FROM languages l WHERE l.default = true ) )"));
    }

    /**
     * Get last activity
     *
     * @param int $idLanguage
     * @return DtoLastActivity
     */
    public static function getLastActivity(int $idLanguage = 0)
    {
        $result = collect();
        foreach (DB::select(
            'SELECT languages_pages.created_at, languages_pages.updated_at, languages_pages.title, pages.page_type, pages.online
            FROM pages
            INNER JOIN languages_pages ON pages.id = languages_pages.page_id
            AND languages_pages.language_id = :idLanguage  
            WHERE languages_pages.updated_at is not null
            ORDER BY languages_pages.updated_at desc limit 10',
            ['idLanguage' => $idLanguage]
        ) as $lastActivity) {
            $dtoLastActivity = new DtoLastActivity();
            if(!is_null($lastActivity->updated_at)){
                $dtoLastActivity->data = date_format(date_create($lastActivity->updated_at), 'd/m/Y H:m');
            }else{
                if(!is_null($lastActivity->created_at)){
                    $dtoLastActivity->data = date_format(date_create($lastActivity->created_at), 'd/m/Y H:m');
                }else{
                    $dtoLastActivity->data = '';
                }
            }
            $dtoLastActivity->oggetto = $lastActivity->title;
            $dtoLastActivity->type = $lastActivity->page_type;
            $dtoLastActivity->online = $lastActivity->online;

            $result->push($dtoLastActivity);
        }
        return $result;
    }


 /**
     * Get page render
     *
     * @param string $url
     * @return DtoRender
     */
    public static function getRender(String $url, $paramUrl, $idUser , $idCart, $idLanguage, $salesmanId)
    {
        
        if(isset($paramUrl)){
            $param = $paramUrl;
        }else{
            $param = "";
        }
        $pageToRender = static::_getRender($url, $idLanguage);

        if (count($pageToRender) > 0) {
            return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
        } else {
            $page = 1;
            $categoryPage = substr(strrchr($url, "/"), 1);
            
            if ($categoryPage != '') {
                if (strpos($url, $categoryPage) > 1) {
                    $findUrl = substr($url, 0, strpos($url, $categoryPage) - 1);
                    
                    if (is_numeric($categoryPage)) {
                        $page = $categoryPage;
                        $categoryPage = substr(strrchr($findUrl, "/"), 1);
                    }

                    $pageToRender = static::_getRender($findUrl, $idLanguage);
                    if (count($pageToRender) > 0) {
                        return static::_convertToDtoUser($pageToRender, $categoryPage, $page, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                    } else {
                        $urlCategories = CategoryLanguage::where('meta_tag_link', $url)->first();
                        if (strpos($url, $categoryPage) > 1 && !isset($urlCategories)) {
                            $html = LinkPagesCategoryBL::getHtml($url);
                            if (isset($html->html) && $html->html != '') {
                                $pageToRender = collect();
                                $dtoPageRender = new DtoPageRender();
                                $dtoPageRender->content = $html->html;
                                $dtoPageRender->hide_footer = false;
                                $dtoPageRender->hide_header = false;
                                $dtoPageRender->hideSearchEngine = false;
                                $dtoPageRender->title = $html->description;
                                $dtoPageRender->share_title = $html->share_title;
                                $dtoPageRender->share_description = $html->share_description;
                                $dtoPageRender->seo_title = $html->seo_title;
                                $dtoPageRender->seo_description = $html->seo_description;
                                $dtoPageRender->seo_keyword = $html->seo_keyword;
                                $dtoPageRender->url_cover_image = $html->url_cover_image;
                                $dtoPageRender->availability = '';
                                $pageToRender->push($dtoPageRender);
                                return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                            } else {
                                $html = CompareItemBL::getHtml($url, $idUser, $idCart, $idLanguage);
                                if (isset($html->html) && $html->html != '') {
                                    $pageToRender = collect();
                                    $dtoPageRender = new DtoPageRender();
                                    $dtoPageRender->content = $html->html;
                                    $dtoPageRender->hide_footer = false;
                                    $dtoPageRender->hide_header = false;
                                    $dtoPageRender->hideSearchEngine = false;
                                    $dtoPageRender->title = $html->description;
                                    $dtoPageRender->share_title = $html->share_title;
                                    $dtoPageRender->share_description = $html->share_description;
                                    $dtoPageRender->seo_title = $html->seo_title;
                                    $dtoPageRender->seo_description = $html->seo_description;
                                    $dtoPageRender->seo_keyword = $html->seo_keyword;
                                    $dtoPageRender->url_cover_image = $html->url_cover_image;
                                    $dtoPageRender->availability = '';
                                    $pageToRender->push($dtoPageRender);
                                    return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                } else {
                                    $html = ItemPriceBL::getHtml($url, $idUser);
                                    if (isset($html->html) && $html->html != '') {
                                        $pageToRender = collect();
                                        $dtoPageRender = new DtoPageRender();
                                        $dtoPageRender->content = $html->html;
                                        $dtoPageRender->hide_footer = false;
                                        $dtoPageRender->hide_header = false;
                                        $dtoPageRender->hideSearchEngine = false;
                                        $dtoPageRender->share_description = $html->meta_tag_description;
                                        $dtoPageRender->share_title = $html->meta_tag_title;
                                        $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                        $dtoPageRender->seo_description = $html->meta_tag_description;
                                        $dtoPageRender->seo_title = $html->meta_tag_title;
                                        $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                        $dtoPageRender->availability = $html->availability;
                                        $dtoPageRender->url_cover_image = $html->url_cover_image;
                                        $dtoPageRender->title = $html->meta_tag_title;
                                        $pageToRender->push($dtoPageRender);
                                        return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                    } else {
                                        $html = ItemGalleryBL::getHtml($url, $idUser);
                                        if (isset($html->html) && $html->html != '') {
                                            $pageToRender = collect();
                                            $dtoPageRender = new DtoPageRender();
                                            $dtoPageRender->content = $html->html;
                                            $dtoPageRender->hide_footer = false;
                                            $dtoPageRender->hide_header = false;
                                            $dtoPageRender->hideSearchEngine = false;
                                            $dtoPageRender->share_description = $html->meta_tag_description;
                                            $dtoPageRender->share_title = $html->meta_tag_title;
                                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                            $dtoPageRender->seo_description = $html->meta_tag_description;
                                            $dtoPageRender->seo_title = $html->meta_tag_title;
                                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                            $dtoPageRender->availability = $html->availability;
                                            $dtoPageRender->url_cover_image = $html->url_cover_image;
                                            $dtoPageRender->title = $html->meta_tag_title;
                                            $pageToRender->push($dtoPageRender);
                                            return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                        } else {
                                            $html = UserBL::getHtml($url);
                                            
                                            if (isset($html->html) && $html->html != '') {
                                                
                                                $pageToRender = collect();
                                                $dtoPageRender = new DtoPageRender();
                                                $dtoPageRender->content = $html->html;
                                                $dtoPageRender->hide_footer = false;
                                                $dtoPageRender->hide_header = false;
                                                $dtoPageRender->hideSearchEngine = false;
                                                $dtoPageRender->share_description = $html->meta_tag_description;
                                                $dtoPageRender->url_cover_image = $html->url_cover_image;
                                                $dtoPageRender->share_title = $html->meta_tag_title;
                                                $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                $dtoPageRender->seo_description = $html->meta_tag_description;
                                                $dtoPageRender->seo_title = $html->meta_tag_title;
                                                $dtoPageRender->title = $html->meta_tag_title;
                                                $pageToRender->push($dtoPageRender);
                                                return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                            } else {
                                                $html = CommunityImageBL::getHtml($url, $idUser);
                                                
                                                if (isset($html->html) && $html->html != '') {
                                                    $pageToRender = collect();
                                                    $dtoPageRender = new DtoPageRender();
                                                    $dtoPageRender->content = $html->html;
                                                    $dtoPageRender->hide_footer = false;
                                                    $dtoPageRender->hide_header = false;
                                                    $dtoPageRender->hideSearchEngine = false;
                                                    $dtoPageRender->share_description = $html->meta_tag_description;
                                                    $dtoPageRender->share_title = $html->meta_tag_title;
                                                    $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                    $dtoPageRender->seo_description = $html->meta_tag_description;
                                                    $dtoPageRender->seo_title = $html->meta_tag_title;
                                                    $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                    $dtoPageRender->availability = $html->availability;
                                                    $dtoPageRender->url_cover_image = $html->url_cover_image;
                                                    $dtoPageRender->title = $html->meta_tag_title;
                                                    $pageToRender->push($dtoPageRender);
                                                    return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                } else {
                                                    $html = NewsletterBL::getHtml($url);
                                                    if (isset($html) && $html != '') {
                                                        $pageToRender = collect();
                                                        $dtoPageRender = new DtoPageRender();
                                                        $dtoPageRender->content = $html;
                                                        $pageToRender->push($dtoPageRender);
                                                        return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                    } else {
                                                        $html = ItemBL::getHtml($url, $idUser, $idLanguage);
                                                        if (isset($html->html) && $html->html != '') {
                                                            $pageToRender = collect();
                                                            $dtoPageRender = new DtoPageRender();
                                                            $dtoPageRender->content = $html->html;
                                                            $dtoPageRender->hide_footer = false;
                                                            $dtoPageRender->hide_header = false;
                                                            $dtoPageRender->hideSearchEngine = false;
                                                            $dtoPageRender->share_description = $html->meta_tag_description;
                                                            $dtoPageRender->share_title = $html->meta_tag_title;
                                                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                            $dtoPageRender->seo_description = $html->meta_tag_description;
                                                            $dtoPageRender->seo_title = $html->meta_tag_title;
                                                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                            $dtoPageRender->availability = $html->availability;
                                                            $dtoPageRender->url_cover_image = $html->url_cover_image;
                                                            $dtoPageRender->title = $html->meta_tag_title;
                                                            $pageToRender->push($dtoPageRender);
                                                            return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                        } else {
                                                            $html404 = MessageBL::getByCode('ERROR_404_PAGE_NOT_FOUND'); 
                                                            if (isset($html404) && $html404 != '') {
                                                                $pageToRender = collect();
                                                                $dtoPageRender = new DtoPageRender();
                                                                $dtoPageRender->content = $html404;
                                                                $dtoPageRender->hide_footer = false;
                                                                $dtoPageRender->hide_header = false;
                                                                $dtoPageRender->hideSearchEngine = false;
                                                                $dtoPageRender->share_description = 'Pagina non trovata';
                                                                $dtoPageRender->share_title = 'Pagina non trovata';
                                                                $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                                $dtoPageRender->seo_description = 'Pagina non trovata';
                                                                $dtoPageRender->seo_title = 'Pagina non trovata';
                                                                $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                                $dtoPageRender->title = 'Pagina non trovata';
                                                                $pageToRender->push($dtoPageRender);
                                                                return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                            }else{
                                                                return '404';
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } 
                            }
                        } else {
                            $html = NewsletterBL::getHtml($url);
                            if (isset($html) && $html != '') {
                                $pageToRender = collect();
                                $dtoPageRender = new DtoPageRender();
                                $dtoPageRender->content = $html;
                                $pageToRender->push($dtoPageRender);
                                return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                            } else {
                                $html = CategoriesBL::getHtml($url, $idLanguage);
                                if (isset($html->html) && $html->html != '') {
                                    $pageToRender = collect();

                                    $dtoPageRender = new DtoPageRender();

                                    $dtoPageRender->content = $html->html;
                                    $dtoPageRender->hide_footer = false;
                                    $dtoPageRender->hide_header = false;
                                    $dtoPageRender->hideSearchEngine = false;
                                    $dtoPageRender->share_description = $html->meta_tag_description;
                                    $dtoPageRender->share_title = $html->meta_tag_title;
                                    $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                    $dtoPageRender->seo_description = $html->meta_tag_description;
                                    $dtoPageRender->seo_title = $html->meta_tag_title;
                                    $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                    $dtoPageRender->title = $html->meta_tag_title;
                                    $pageToRender->push($dtoPageRender);
                                    return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                } else {
                                    $html = ItemBL::getHtml($url, $idUser, $idLanguage);
                                    if (isset($html->html) && $html->html != '') {
                                        $pageToRender = collect();
                                        $dtoPageRender = new DtoPageRender();
                                        $dtoPageRender->content = $html->html;
                                        $dtoPageRender->hide_footer = false;
                                        $dtoPageRender->hide_header = false;
                                        $dtoPageRender->hideSearchEngine = false;
                                        $dtoPageRender->share_description = $html->meta_tag_description;
                                        $dtoPageRender->share_title = $html->meta_tag_title;
                                        $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                        $dtoPageRender->seo_description = $html->meta_tag_description;
                                        $dtoPageRender->seo_title = $html->meta_tag_title;
                                        $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                        $dtoPageRender->availability = $html->availability;
                                        $dtoPageRender->url_cover_image = $html->url_cover_image;
                                        $dtoPageRender->title = $html->meta_tag_title;
                                        $pageToRender->push($dtoPageRender);
                                        return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                    } else {
                                        $html = UserBL::getHtml($url);
                                        if (isset($html->html) && $html->html != '') {
                                            $pageToRender = collect();
                                            $dtoPageRender = new DtoPageRender();
                                            $dtoPageRender->content = $html->html;
                                            $dtoPageRender->hide_footer = false;
                                            $dtoPageRender->hide_header = false;
                                            $dtoPageRender->hideSearchEngine = false;
                                            $dtoPageRender->share_description = $html->meta_tag_description;
                                            $dtoPageRender->url_cover_image = $html->url_cover_image;
                                            $dtoPageRender->share_title = $html->meta_tag_title;
                                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                            $dtoPageRender->seo_description = $html->meta_tag_description;
                                            $dtoPageRender->seo_title = $html->meta_tag_title;
                                            $dtoPageRender->title = $html->meta_tag_title;
                                            $pageToRender->push($dtoPageRender);
                                            return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                        } else {
                                            $html = LinkPagesCategoryBL::getHtml($url);
                                            if (isset($html->html) && $html->html != '') {
                                                $pageToRender = collect();
                                                $dtoPageRender = new DtoPageRender();
                                                $dtoPageRender->content = $html->html;
                                                $dtoPageRender->hide_footer = false;
                                                $dtoPageRender->hide_header = false;
                                                $dtoPageRender->hideSearchEngine = false;
                                                $dtoPageRender->title = $html->description;
                                                $dtoPageRender->share_title = $html->share_title;
                                                $dtoPageRender->share_description = $html->share_description;
                                                $dtoPageRender->seo_title = $html->seo_title;
                                                $dtoPageRender->seo_description = $html->seo_description;
                                                $dtoPageRender->seo_keyword = $html->seo_keyword;
                                                $dtoPageRender->url_cover_image = $html->url_cover_image;
                                                $dtoPageRender->availability = '';
                                                $pageToRender->push($dtoPageRender);
                                                return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                            } else {
                                                $html404 = MessageBL::getByCode('ERROR_404_PAGE_NOT_FOUND'); 
                                                if (isset($html404) && $html404 != '') {
                                                    $pageToRender = collect();
                                                    $dtoPageRender = new DtoPageRender();
                                                    $dtoPageRender->content = $html404;
                                                    $dtoPageRender->hide_footer = false;
                                                    $dtoPageRender->hide_header = false;
                                                    $dtoPageRender->hideSearchEngine = false;
                                                    $dtoPageRender->share_description = 'Pagina non trovata';
                                                    $dtoPageRender->share_title = 'Pagina non trovata';
                                                    $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                    $dtoPageRender->seo_description = 'Pagina non trovata';
                                                    $dtoPageRender->seo_title = 'Pagina non trovata';
                                                    $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                    $dtoPageRender->title = 'Pagina non trovata';
                                                    $pageToRender->push($dtoPageRender);
                                                    return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                }else{
                                                    return '404';
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    
                    $html = NewsletterBL::getHtml($url);
                    if (isset($html) && $html != '') {
                        $pageToRender = collect();
                        $dtoPageRender = new DtoPageRender();
                        $dtoPageRender->content = $html;
                        $pageToRender->push($dtoPageRender);
                        return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                    } else {
                        $html = CategoriesBL::getHtml($url, $idLanguage);
                        if (isset($html->html) && $html->html != '') {
                            $pageToRender = collect();
                            $dtoPageRender = new DtoPageRender();
                            $dtoPageRender->content = $html->html;
                            $dtoPageRender->hide_footer = false;
                            $dtoPageRender->hide_header = false;
                            $dtoPageRender->hideSearchEngine = false;
                            $dtoPageRender->share_description = $html->meta_tag_description;
                            $dtoPageRender->share_title = $html->meta_tag_title;
                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                            $dtoPageRender->seo_description = $html->meta_tag_description;
                            $dtoPageRender->seo_title = $html->meta_tag_title;
                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                            //$dtoPageRender->seo_availability = $html->availability;
                            $dtoPageRender->title = $html->meta_tag_title;
                            $pageToRender->push($dtoPageRender);
                            return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                        } else {
                            $html = ItemBL::getHtml($url, $idUser, $idLanguage);
                            if (isset($html->html) && $html->html != '') {
                                $pageToRender = collect();
                                $dtoPageRender = new DtoPageRender();
                                $dtoPageRender->content = $html->html;
                                $dtoPageRender->hide_footer = false;
                                $dtoPageRender->hide_header = false;
                                $dtoPageRender->hideSearchEngine = false;
                                $dtoPageRender->share_description = $html->meta_tag_description;
                                $dtoPageRender->share_title = $html->meta_tag_title;
                                $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                $dtoPageRender->seo_description = $html->meta_tag_description;
                                $dtoPageRender->seo_title = $html->meta_tag_title;
                                $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                $dtoPageRender->availability = $html->availability;
                                $dtoPageRender->url_cover_image = $html->url_cover_image;
                                $dtoPageRender->title = $html->meta_tag_title;
                                $pageToRender->push($dtoPageRender);
                                return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                            } else {
                                $html = UserBL::getHtml($url);
                                if (isset($html->html) && $html->html != '') {
                                    $pageToRender = collect();
                                    $dtoPageRender = new DtoPageRender();
                                    $dtoPageRender->content = $html->html;
                                    $dtoPageRender->hide_footer = false;
                                    $dtoPageRender->hide_header = false;
                                    $dtoPageRender->hideSearchEngine = false;
                                    $dtoPageRender->share_description = $html->name;
                                    $dtoPageRender->share_title = $html->name;
                                    $dtoPageRender->seo_keyword = $html->name;
                                    $dtoPageRender->seo_description = $html->name;
                                    $dtoPageRender->seo_title = $html->name;
                                    $dtoPageRender->seo_keyword = $html->name;
                                    $dtoPageRender->title = $html->business_name;
                                    $pageToRender->push($dtoPageRender);
                                    return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                } else {
                                    $html = LinkPagesCategoryBL::getHtml($url);
                                    if (isset($html->html) && $html->html != '') {
                                        $pageToRender = collect();
                                        $dtoPageRender = new DtoPageRender();
                                        $dtoPageRender->content = $html->html;
                                        $dtoPageRender->hide_footer = false;
                                        $dtoPageRender->hide_header = false;
                                        $dtoPageRender->hideSearchEngine = false;
                                        $dtoPageRender->title = $html->description;
                                        $dtoPageRender->share_title = $html->share_title;
                                        $dtoPageRender->share_description = $html->share_description;
                                        $dtoPageRender->seo_title = $html->seo_title;
                                        $dtoPageRender->seo_description = $html->seo_description;
                                        $dtoPageRender->seo_keyword = $html->seo_keyword;
                                        $dtoPageRender->url_cover_image = $html->url_cover_image;
                                        $dtoPageRender->availability = '';
                                        $pageToRender->push($dtoPageRender);
                                        return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                    } else {
                                        $html = CompareItemBL::getHtml($url, $idUser, $idCart, $idLanguage);
                                        if (isset($html->html) && $html->html != '') {
                                            $pageToRender = collect();
                                            $dtoPageRender = new DtoPageRender();
                                            $dtoPageRender->content = $html->html;
                                            $dtoPageRender->hide_footer = false;
                                            $dtoPageRender->hide_header = false;
                                            $dtoPageRender->hideSearchEngine = false;
                                            $dtoPageRender->title = $html->description;
                                            $dtoPageRender->share_title = $html->share_title;
                                            $dtoPageRender->share_description = $html->share_description;
                                            $dtoPageRender->seo_title = $html->seo_title;
                                            $dtoPageRender->seo_description = $html->seo_description;
                                            $dtoPageRender->seo_keyword = $html->seo_keyword;
                                            $dtoPageRender->url_cover_image = $html->url_cover_image;
                                            $dtoPageRender->availability = '';
                                            $pageToRender->push($dtoPageRender);
                                            return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                        } else {
                                            $html = CommunityImageBL::getHtml($url, $idUser);
                                            
                                            if (isset($html->html) && $html->html != '') {
                                                $pageToRender = collect();
                                                $dtoPageRender = new DtoPageRender();
                                                $dtoPageRender->content = $html->html;
                                                $dtoPageRender->hide_footer = false;
                                                $dtoPageRender->hide_header = false;
                                                $dtoPageRender->hideSearchEngine = false;
                                                $dtoPageRender->share_description = $html->meta_tag_description;
                                                $dtoPageRender->share_title = $html->meta_tag_title;
                                                $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                $dtoPageRender->seo_description = $html->meta_tag_description;
                                                $dtoPageRender->seo_title = $html->meta_tag_title;
                                                $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                $dtoPageRender->availability = $html->availability;
                                                $dtoPageRender->url_cover_image = $html->url_cover_image;
                                                $dtoPageRender->title = $html->meta_tag_title;
                                                $pageToRender->push($dtoPageRender);
                                                return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId); 
                                            } else {
                                                $html = ItemGalleryBL::getHtml($url, $idUser);
                                                if (isset($html->html) && $html->html != '') {
                                                    $pageToRender = collect();
                                                    $dtoPageRender = new DtoPageRender();
                                                    $dtoPageRender->content = $html->html;
                                                    $dtoPageRender->hide_footer = false;
                                                    $dtoPageRender->hide_header = false;
                                                    $dtoPageRender->hideSearchEngine = false;
                                                    $dtoPageRender->share_description = $html->meta_tag_description;
                                                    $dtoPageRender->share_title = $html->meta_tag_title;
                                                    $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                    $dtoPageRender->seo_description = $html->meta_tag_description;
                                                    $dtoPageRender->seo_title = $html->meta_tag_title;
                                                    $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                    $dtoPageRender->availability = $html->availability;
                                                    $dtoPageRender->url_cover_image = $html->url_cover_image;
                                                    $dtoPageRender->title = $html->meta_tag_title;
                                                    $pageToRender->push($dtoPageRender);
                                                    return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                } else { 
                                                    $html = ItemPriceBL::getHtml($url, $idUser);
                                                    if (isset($html->html) && $html->html != '') {
                                                        $pageToRender = collect();
                                                        $dtoPageRender = new DtoPageRender();
                                                        $dtoPageRender->content = $html->html;
                                                        $dtoPageRender->hide_footer = false;
                                                        $dtoPageRender->hide_header = false;
                                                        $dtoPageRender->hideSearchEngine = false;
                                                        $dtoPageRender->share_description = $html->meta_tag_description;
                                                        $dtoPageRender->share_title = $html->meta_tag_title;
                                                        $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                        $dtoPageRender->seo_description = $html->meta_tag_description;
                                                        $dtoPageRender->seo_title = $html->meta_tag_title;
                                                        $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                        $dtoPageRender->availability = $html->availability;
                                                        $dtoPageRender->url_cover_image = $html->url_cover_image;
                                                        $dtoPageRender->title = $html->meta_tag_title;
                                                        $pageToRender->push($dtoPageRender);
                                                        return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                    } else {
                                                        $html404 = MessageBL::getByCode('ERROR_404_PAGE_NOT_FOUND'); 
                                                        if (isset($html404) && $html404 != '') {
                                                            $pageToRender = collect();
                                                            $dtoPageRender = new DtoPageRender();
                                                            $dtoPageRender->content = $html404;
                                                            $dtoPageRender->hide_footer = false;
                                                            $dtoPageRender->hide_header = false;
                                                            $dtoPageRender->hideSearchEngine = false;
                                                            $dtoPageRender->share_description = 'Pagina non trovata';
                                                            $dtoPageRender->share_title = 'Pagina non trovata';
                                                            $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                            $dtoPageRender->seo_description = 'Pagina non trovata';
                                                            $dtoPageRender->seo_title = 'Pagina non trovata';
                                                            $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                            $dtoPageRender->title = 'Pagina non trovata';
                                                            $pageToRender->push($dtoPageRender);
                                                            return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                        }else{
                                                            return '404';
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                         }
                    }
                }
            }
        }
    }


    /**
     * Get page render
     *
     * @param string $url
     * @return DtoRender
     */
  /*   public static function getRender(String $url, int $idUser = null)
    {
       $pageToRender = static::_getRender($url, 1);

        if (count($pageToRender) > 0) {
            return static::_convertToDto($pageToRender, substr(strrchr($url, "/"), 1));
        } else {
            $page = 1;
            $categoryPage = substr(strrchr($url, "/"), 1);
            if ($categoryPage != '') {
                if (strpos($url, $categoryPage) > 1) {
                    $findUrl = substr($url, 0, strpos($url, $categoryPage) - 1);

                    if (is_numeric($categoryPage)) {
                        $page = $categoryPage;
                        $categoryPage = substr(strrchr($findUrl, "/"), 1);
                    }

                    $pageToRender = static::_getRender($findUrl, 1);
                    if (count($pageToRender) > 0) {
                        return static::_convertToDto($pageToRender, $categoryPage, $page);
                    } else {
                        
                        $urlCategories = CategoryLanguage::where('meta_tag_link', $url)->first();
                        if (strpos($url, $categoryPage) > 1 && !isset($urlCategories)) {
                            $html = LinkPagesCategoryBL::getHtml($url);
                            if (isset($html->html) && $html->html != '') {
                                $pageToRender = collect();
                                $dtoPageRender = new DtoPageRender();
                                $dtoPageRender->content = $html->html;
                                $dtoPageRender->hide_footer = false;
                                $dtoPageRender->hide_header = false;
                                $dtoPageRender->hideSearchEngine = false;
                                $dtoPageRender->title = $html->description;
                                $dtoPageRender->share_title = $html->share_title;
                                $dtoPageRender->share_description = $html->share_description;
                                $dtoPageRender->seo_title = $html->seo_title;
                                $dtoPageRender->seo_description = $html->seo_description;
                                $dtoPageRender->seo_keyword = $html->seo_keyword;
                                $dtoPageRender->url_cover_image = $html->url_cover_image;
                                $dtoPageRender->availability = '';
                                $pageToRender->push($dtoPageRender);
                                return static::_convertToDto($pageToRender, substr(strrchr($url, "/"), 1));
                                
                            } else {
                                $categoryPage = substr(strrchr($findUrl, "/"), 1);
                                $findUrl = substr($url, 0, strpos($findUrl, $categoryPage) - 1);
                                return static::_convertToDto(static::_getRender($findUrl, 1), $categoryPage, $page);
                            }

                        } else {
                            $html = NewsletterBL::getHtml($url);
                            if (isset($html) && $html != '') {
                                $pageToRender = collect();
                                $dtoPageRender = new DtoPageRender();
                                $dtoPageRender->content = $html;
                                $pageToRender->push($dtoPageRender);
                                return static::_convertToDto($pageToRender, substr(strrchr($url, "/"), 1));
                            } else {
                                $html = CategoriesBL::getHtml($url);
                                if (isset($html->html) && $html->html != '') {
                                    $pageToRender = collect();

                                    $dtoPageRender = new DtoPageRender();

                                    $dtoPageRender->content = $html->html;
                                    $dtoPageRender->hide_footer = false;
                                    $dtoPageRender->hide_header = false;
                                    $dtoPageRender->hideSearchEngine = false;
                                    $dtoPageRender->share_description = $html->meta_tag_description;
                                    $dtoPageRender->share_title = $html->meta_tag_title;
                                    $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                    $dtoPageRender->seo_description = $html->meta_tag_description;
                                    $dtoPageRender->seo_title = $html->meta_tag_title;
                                    $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                    $dtoPageRender->title = $html->meta_tag_title;
                                    $pageToRender->push($dtoPageRender);
                                    return static::_convertToDto($pageToRender, substr(strrchr($url, "/"), 1));
                                } else {
                                    $html = ItemBL::getHtml($url, $idUser);
                                    if (isset($html->html) && $html->html != '') {
                                        $pageToRender = collect();
                                        $dtoPageRender = new DtoPageRender();
                                        $dtoPageRender->content = $html->html;
                                        $dtoPageRender->hide_footer = false;
                                        $dtoPageRender->hide_header = false;
                                        $dtoPageRender->hideSearchEngine = false;
                                        $dtoPageRender->share_description = $html->meta_tag_description;
                                        $dtoPageRender->share_title = $html->meta_tag_title;
                                        $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                        $dtoPageRender->seo_description = $html->meta_tag_description;
                                        $dtoPageRender->seo_title = $html->meta_tag_title;
                                        $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                        $dtoPageRender->availability = $html->availability;
                                        $dtoPageRender->url_cover_image = $html->url_cover_image;
                                        $dtoPageRender->title = $html->description;
                                        $pageToRender->push($dtoPageRender);
                                        return static::_convertToDto($pageToRender, substr(strrchr($url, "/"), 1));
                                    } else {
                                          
                                        $html = UserBL::getHtml($url);
                                        if (isset($html->html) && $html->html != '') {
                                            $pageToRender = collect();
                                            $dtoPageRender = new DtoPageRender();
                                            $dtoPageRender->content = $html->html;
                                            $dtoPageRender->hide_footer = false;
                                            $dtoPageRender->hide_header = false;
                                            $dtoPageRender->hideSearchEngine = false;
                                            $dtoPageRender->share_description = $html->meta_tag_description;
                                            $dtoPageRender->share_title = $html->name;
                                            $dtoPageRender->seo_keyword = $html->name;
                                            $dtoPageRender->seo_description = $html->name;
                                            $dtoPageRender->seo_title = $html->name;
                                            $dtoPageRender->seo_keyword = $html->name;
                                            $dtoPageRender->title = $html->business_name;
                                            $pageToRender->push($dtoPageRender);
                                            return static::_convertToDto($pageToRender, substr(strrchr($url, "/"), 1));
                                        } else {
                                            $html = LinkPagesCategoryBL::getHtml($url);
                                            if (isset($html->html) && $html->html != '') {
                                                $pageToRender = collect();
                                                $dtoPageRender = new DtoPageRender();
                                                $dtoPageRender->content = $html->html;
                                                $dtoPageRender->hide_footer = false;
                                                $dtoPageRender->hide_header = false;
                                                $dtoPageRender->hideSearchEngine = false;
                                                $dtoPageRender->title = $html->description;
                                                $dtoPageRender->share_title = $html->share_title;
                                                $dtoPageRender->share_description = $html->share_description;
                                                $dtoPageRender->seo_title = $html->seo_title;
                                                $dtoPageRender->seo_description = $html->seo_description;
                                                $dtoPageRender->seo_keyword = $html->seo_keyword;
                                                $dtoPageRender->url_cover_image = $html->url_cover_image;
                                                $dtoPageRender->availability = '';
                                                $pageToRender->push($dtoPageRender);
                                                return static::_convertToDto($pageToRender, substr(strrchr($url, "/"), 1));
                                            } else {
                                                return '404';
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $html = NewsletterBL::getHtml($url);
                    if (isset($html) && $html != '') {
                        $pageToRender = collect();
                        $dtoPageRender = new DtoPageRender();
                        $dtoPageRender->content = $html;
                        $pageToRender->push($dtoPageRender);
                        return static::_convertToDto($pageToRender, substr(strrchr($url, "/"), 1));
                    } else {
                        $html = CategoriesBL::getHtml($url, 1);
                        if (isset($html->html) && $html->html != '') {
                            $pageToRender = collect();
                            $dtoPageRender = new DtoPageRender();
                            $dtoPageRender->content = $html->html;
                            $dtoPageRender->hide_footer = false;
                            $dtoPageRender->hide_header = false;
                            $dtoPageRender->hideSearchEngine = false;
                            $dtoPageRender->share_description = $html->meta_tag_description;
                            $dtoPageRender->share_title = $html->meta_tag_title;
                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                            $dtoPageRender->seo_description = $html->meta_tag_description;
                            $dtoPageRender->seo_title = $html->meta_tag_title;
                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                            //$dtoPageRender->seo_availability = $html->availability;
                            $dtoPageRender->title = $html->meta_tag_title;
                            $pageToRender->push($dtoPageRender);
                            return static::_convertToDto($pageToRender, substr(strrchr($url, "/"), 1));
                        } else {
                            $html = ItemBL::getHtml($url, $idUser, 1);
                            if (isset($html->html) && $html->html != '') {
                                $pageToRender = collect();
                                $dtoPageRender = new DtoPageRender();
                                $dtoPageRender->content = $html->html;
                                $dtoPageRender->hide_footer = false;
                                $dtoPageRender->hide_header = false;
                                $dtoPageRender->hideSearchEngine = false;
                                $dtoPageRender->share_description = $html->meta_tag_description;
                                $dtoPageRender->share_title = $html->meta_tag_title;
                                $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                $dtoPageRender->seo_description = $html->meta_tag_description;
                                $dtoPageRender->seo_title = $html->meta_tag_title;
                                $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                $dtoPageRender->availability = $html->availability;
                                $dtoPageRender->url_cover_image = $html->url_cover_image;
                                $dtoPageRender->title = $html->description;
                                $pageToRender->push($dtoPageRender);
                                return static::_convertToDto($pageToRender, substr(strrchr($url, "/"), 1));
                            } else {
                                $html = UserBL::getHtml($url);
                                if (isset($html->html) && $html->html != '') {
                                    $pageToRender = collect();
                                    $dtoPageRender = new DtoPageRender();
                                    $dtoPageRender->content = $html->html;
                                    $dtoPageRender->hide_footer = false;
                                    $dtoPageRender->hide_header = false;
                                    $dtoPageRender->hideSearchEngine = false;
                                    $dtoPageRender->share_description = $html->name;
                                    $dtoPageRender->share_title = $html->name;
                                    $dtoPageRender->seo_keyword = $html->name;
                                    $dtoPageRender->seo_description = $html->name;
                                    $dtoPageRender->seo_title = $html->name;
                                    $dtoPageRender->seo_keyword = $html->name;
                                    $dtoPageRender->title = $html->business_name;
                                    $pageToRender->push($dtoPageRender);
                                    return static::_convertToDto($pageToRender, substr(strrchr($url, "/"), 1));
                                } else {
                                    $html = LinkPagesCategoryBL::getHtml($url);
                                    if (isset($html->html) && $html->html != '') {
                                        $pageToRender = collect();
                                        $dtoPageRender = new DtoPageRender();
                                        $dtoPageRender->content = $html->html;
                                        $dtoPageRender->hide_footer = false;
                                        $dtoPageRender->hide_header = false;
                                        $dtoPageRender->hideSearchEngine = false;
                                        $dtoPageRender->title = $html->description;
                                        $dtoPageRender->share_title = $html->share_title;
                                        $dtoPageRender->share_description = $html->share_description;
                                        $dtoPageRender->seo_title = $html->seo_title;
                                        $dtoPageRender->seo_description = $html->seo_description;
                                        $dtoPageRender->seo_keyword = $html->seo_keyword;
                                        $dtoPageRender->url_cover_image = $html->url_cover_image;
                                        $dtoPageRender->availability = '';
                                        $pageToRender->push($dtoPageRender);
                                        return static::_convertToDto($pageToRender, substr(strrchr($url, "/"), 1));
                                    } else {
                                        return '404';
                                    }
                                }
                            }
                         }
                    }
                }
            }
        }
    }*/




    /* end get_render  */


    /**
     * Get page render
     *
     * @param string $url
     * @return DtoRender
     */
    public static function getRenderUser(String $url, $paramUrl, $idUser , $idCart, $idLanguage, $salesmanId)
    {
        
        if(isset($paramUrl)){
            $param = $paramUrl;
        }else{
            $param = "";
        }
        $pageToRender = static::_getRender($url, $idLanguage);

        if (count($pageToRender) > 0) {
            return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
        } else {
            $page = 1;
            $categoryPage = substr(strrchr($url, "/"), 1);
            
            if ($categoryPage != '') {
                if (strpos($url, $categoryPage) > 1) {
                    $findUrl = substr($url, 0, strpos($url, $categoryPage) - 1);
                    
                    if (is_numeric($categoryPage)) {
                        $page = $categoryPage;
                        $categoryPage = substr(strrchr($findUrl, "/"), 1);
                    }

                    $pageToRender = static::_getRender($findUrl, $idLanguage);
                    if (count($pageToRender) > 0) {
                        return static::_convertToDtoUser($pageToRender, $categoryPage, $page, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                    } else {
                        
                        $urlCategories = CategoryLanguage::where('meta_tag_link', $url)->first();
                        if (strpos($url, $categoryPage) > 1 && !isset($urlCategories)) {
                            $html = LinkPagesCategoryBL::getHtml($url);
                            if (isset($html->html) && $html->html != '') {
                                $pageToRender = collect();
                                $dtoPageRender = new DtoPageRender();
                                $dtoPageRender->content = $html->html;
                                $dtoPageRender->redirect = false;
                                $dtoPageRender->hide_footer = false;
                                $dtoPageRender->hide_header = false;
                                $dtoPageRender->hideSearchEngine = false;
                                $dtoPageRender->title = $html->description;
                                $dtoPageRender->share_title = $html->share_title;
                                $dtoPageRender->share_description = $html->share_description;
                                $dtoPageRender->seo_title = $html->seo_title;
                                $dtoPageRender->seo_description = $html->seo_description;
                                $dtoPageRender->seo_keyword = $html->seo_keyword;
                                $dtoPageRender->url_cover_image = $html->url_cover_image;
                                $dtoPageRender->availability = '';
                                $pageToRender->push($dtoPageRender);
                                return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                            } else {
                                $html = CompareItemBL::getHtml($url, $idUser, $idCart, $idLanguage);
                                if (isset($html->html) && $html->html != '') {
                                    $pageToRender = collect();
                                    $dtoPageRender = new DtoPageRender();
                                    $dtoPageRender->content = $html->html;
                                    $dtoPageRender->redirect = false;
                                    $dtoPageRender->hide_footer = false;
                                    $dtoPageRender->hide_header = false;
                                    $dtoPageRender->hideSearchEngine = false;
                                    $dtoPageRender->title = $html->description;
                                    $dtoPageRender->share_title = $html->share_title;
                                    $dtoPageRender->share_description = $html->share_description;
                                    $dtoPageRender->seo_title = $html->seo_title;
                                    $dtoPageRender->seo_description = $html->seo_description;
                                    $dtoPageRender->seo_keyword = $html->seo_keyword;
                                    $dtoPageRender->url_cover_image = $html->url_cover_image;
                                    $dtoPageRender->availability = '';
                                    $pageToRender->push($dtoPageRender);
                                    return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                } else {
                                    $html = ItemPriceBL::getHtml($url, $idUser);
                                    if (isset($html->html) && $html->html != '') {
                                        $pageToRender = collect();
                                        $dtoPageRender = new DtoPageRender();
                                        $dtoPageRender->content = $html->html;
                                        $dtoPageRender->redirect = false;
                                        $dtoPageRender->hide_footer = false;
                                        $dtoPageRender->hide_header = false;
                                        $dtoPageRender->hideSearchEngine = false;
                                        $dtoPageRender->share_description = $html->meta_tag_description;
                                        $dtoPageRender->share_title = $html->meta_tag_title;
                                        $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                        $dtoPageRender->seo_description = $html->meta_tag_description;
                                        $dtoPageRender->seo_title = $html->meta_tag_title;
                                        $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                        $dtoPageRender->availability = $html->availability;
                                        $dtoPageRender->url_cover_image = $html->url_cover_image;
                                        $dtoPageRender->title = $html->meta_tag_title;
                                        $pageToRender->push($dtoPageRender);
                                        return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                    } else {
                                        $html = ItemGalleryBL::getHtml($url, $idUser);
                                        if (isset($html->html) && $html->html != '') {
                                            $pageToRender = collect();
                                            $dtoPageRender = new DtoPageRender();
                                            $dtoPageRender->content = $html->html;
                                            $dtoPageRender->redirect = false;
                                            $dtoPageRender->hide_footer = false;
                                            $dtoPageRender->hide_header = false;
                                            $dtoPageRender->hideSearchEngine = false;
                                            $dtoPageRender->share_description = $html->meta_tag_description;
                                            $dtoPageRender->share_title = $html->meta_tag_title;
                                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                            $dtoPageRender->seo_description = $html->meta_tag_description;
                                            $dtoPageRender->seo_title = $html->meta_tag_title;
                                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                            $dtoPageRender->availability = $html->availability;
                                            $dtoPageRender->url_cover_image = $html->url_cover_image;
                                            $dtoPageRender->title = $html->meta_tag_title;
                                            $pageToRender->push($dtoPageRender);
                                            return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                        } else {
                                            $html = UserBL::getHtml($url);
                                            
                                            if (isset($html->html) && $html->html != '') {
                                                
                                                $pageToRender = collect();
                                                $dtoPageRender = new DtoPageRender();
                                                $dtoPageRender->content = $html->html;
                                                $dtoPageRender->redirect = false;
                                                $dtoPageRender->hide_footer = false;
                                                $dtoPageRender->hide_header = false;
                                                $dtoPageRender->hideSearchEngine = false;
                                                $dtoPageRender->share_description = $html->meta_tag_description;
                                                $dtoPageRender->url_cover_image = $html->url_cover_image;
                                                $dtoPageRender->share_title = $html->meta_tag_title;
                                                $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                $dtoPageRender->seo_description = $html->meta_tag_description;
                                                $dtoPageRender->seo_title = $html->meta_tag_title;
                                                $dtoPageRender->title = $html->meta_tag_title;
                                                $pageToRender->push($dtoPageRender);
                                                return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                            } else {
                                                $html = CommunityImageBL::getHtml($url, $idUser);
                                                
                                                if (isset($html->html) && $html->html != '') {
                                                    $pageToRender = collect();
                                                    $dtoPageRender = new DtoPageRender();
                                                    $dtoPageRender->content = $html->html;
                                                    $dtoPageRender->redirect = false;
                                                    $dtoPageRender->hide_footer = false;
                                                    $dtoPageRender->hide_header = false;
                                                    $dtoPageRender->hideSearchEngine = false;
                                                    $dtoPageRender->share_description = $html->meta_tag_description;
                                                    $dtoPageRender->share_title = $html->meta_tag_title;
                                                    $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                    $dtoPageRender->seo_description = $html->meta_tag_description;
                                                    $dtoPageRender->seo_title = $html->meta_tag_title;
                                                    $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                    $dtoPageRender->availability = $html->availability;
                                                    $dtoPageRender->url_cover_image = $html->url_cover_image;
                                                    $dtoPageRender->title = $html->meta_tag_title;
                                                    $pageToRender->push($dtoPageRender);
                                                    return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                } else {
                                                    $html = NewsletterBL::getHtml($url);
                                                    if (isset($html) && $html != '') {
                                                        $pageToRender = collect();
                                                        $dtoPageRender = new DtoPageRender();
                                                        $dtoPageRender->content = $html;
                                                        $dtoPageRender->redirect = false;
                                                        $pageToRender->push($dtoPageRender);
                                                        return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                    } else {
                                                        $html = ItemBL::getHtml($url, $idUser, $idLanguage);
                                                        if (isset($html->html) && $html->html != '') {
                                                            $pageToRender = collect();
                                                            $dtoPageRender = new DtoPageRender();
                                                            $dtoPageRender->content = $html->html;
                                                            $dtoPageRender->redirect = false;
                                                            $dtoPageRender->hide_footer = false;
                                                            $dtoPageRender->hide_header = false;
                                                            $dtoPageRender->hideSearchEngine = false;
                                                            $dtoPageRender->share_description = $html->meta_tag_description;
                                                            $dtoPageRender->share_title = $html->meta_tag_title;
                                                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                            $dtoPageRender->seo_description = $html->meta_tag_description;
                                                            $dtoPageRender->seo_title = $html->meta_tag_title;
                                                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                            $dtoPageRender->availability = $html->availability;
                                                            $dtoPageRender->url_cover_image = $html->url_cover_image;
                                                            $dtoPageRender->title = $html->meta_tag_title;
                                                            $pageToRender->push($dtoPageRender);
                                                            return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                        } else {
                                                            $html = ListKeywordsBL::getHtml($url, $idUser, $idLanguage);
                                                            if (isset($html->html) && $html->html != '') {
                                                                $pageToRender = collect();
                                                                $dtoPageRender = new DtoPageRender();
                                                                $dtoPageRender->content = $html->html;
                                                                $dtoPageRender->redirect = false;
                                                                $dtoPageRender->hide_footer = false;
                                                                $dtoPageRender->hide_header = false;
                                                                $dtoPageRender->hideSearchEngine = false;
                                                                $dtoPageRender->share_description = $html->meta_tag_description;
                                                                $dtoPageRender->share_title = $html->meta_tag_title;
                                                                $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                                $dtoPageRender->seo_description = $html->meta_tag_description;
                                                                $dtoPageRender->seo_title = $html->meta_tag_title;
                                                                $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                                $dtoPageRender->url_cover_image = '';
                                                                $dtoPageRender->title = $html->meta_tag_title;
                                                                $pageToRender->push($dtoPageRender);
                                                                return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                            } else {
                                                                $html = RedirectBL::getHtml($url);
                                                                if (isset($html->url_new) && $html->url_new != '') {
                                                                    $pageToRender = collect();
                                                                    $dtoPageRender = new DtoPageRender();
                                                                    $dtoPageRender->content = $html->url_new;
                                                                    $dtoPageRender->redirect = true;
                                                                    $dtoPageRender->hide_footer = false;
                                                                    $dtoPageRender->hide_header = false;
                                                                    $dtoPageRender->hideSearchEngine = false;
                                                                    $dtoPageRender->share_description = 'Pagina non trovata';
                                                                    $dtoPageRender->share_title = 'Pagina non trovata';
                                                                    $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                                    $dtoPageRender->seo_description = 'Pagina non trovata';
                                                                    $dtoPageRender->seo_title = 'Pagina non trovata';
                                                                    $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                                    $dtoPageRender->title = 'Pagina non trovata';
                                                                    $pageToRender->push($dtoPageRender);
                                                                    return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                                
                                                                }else{
                                                                    $html404 = MessageBL::getByCode('ERROR_404_PAGE_NOT_FOUND'); 
                                                                    if (isset($html404) && $html404 != '') {
                                                                        $pageToRender = collect();
                                                                        $dtoPageRender = new DtoPageRender();
                                                                        $dtoPageRender->content = $html404;
                                                                        $dtoPageRender->redirect = false;
                                                                        $dtoPageRender->hide_footer = false;
                                                                        $dtoPageRender->hide_header = false;
                                                                        $dtoPageRender->hideSearchEngine = false;
                                                                        $dtoPageRender->share_description = 'Pagina non trovata';
                                                                        $dtoPageRender->share_title = 'Pagina non trovata';
                                                                        $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                                        $dtoPageRender->seo_description = 'Pagina non trovata';
                                                                        $dtoPageRender->seo_title = 'Pagina non trovata';
                                                                        $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                                        $dtoPageRender->title = 'Pagina non trovata';
                                                                        $pageToRender->push($dtoPageRender);
                                                                        return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                                    }else{
                                                                        return '404';
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } 
                            }
                        } else {
                            $html = NewsletterBL::getHtml($url);
                            if (isset($html) && $html != '') {
                                $pageToRender = collect();
                                $dtoPageRender = new DtoPageRender();
                                $dtoPageRender->content = $html;
                                $dtoPageRender->redirect = false;
                                $pageToRender->push($dtoPageRender);
                                return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                            } else {
                                $html = CategoriesBL::getHtml($url, $idLanguage);
                                if (isset($html->html) && $html->html != '') {
                                    $pageToRender = collect();
                                    $dtoPageRender = new DtoPageRender();
                                    $dtoPageRender->content = $html->html;
                                    $dtoPageRender->redirect = false;
                                    $dtoPageRender->hide_footer = false;
                                    $dtoPageRender->hide_header = false;
                                    $dtoPageRender->hideSearchEngine = false;
                                    $dtoPageRender->share_description = $html->meta_tag_description;
                                    $dtoPageRender->share_title = $html->meta_tag_title;
                                    $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                    $dtoPageRender->seo_description = $html->meta_tag_description;
                                    $dtoPageRender->seo_title = $html->meta_tag_title;
                                    $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                    $dtoPageRender->title = $html->meta_tag_title;
                                    $pageToRender->push($dtoPageRender);
                                    return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                } else {
                                    $html = ItemBL::getHtml($url, $idUser, $idLanguage);
                                    if (isset($html->html) && $html->html != '') {
                                        $pageToRender = collect();
                                        $dtoPageRender = new DtoPageRender();
                                        $dtoPageRender->content = $html->html;
                                        $dtoPageRender->redirect = false;
                                        $dtoPageRender->hide_footer = false;
                                        $dtoPageRender->hide_header = false;
                                        $dtoPageRender->hideSearchEngine = false;
                                        $dtoPageRender->share_description = $html->meta_tag_description;
                                        $dtoPageRender->share_title = $html->meta_tag_title;
                                        $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                        $dtoPageRender->seo_description = $html->meta_tag_description;
                                        $dtoPageRender->seo_title = $html->meta_tag_title;
                                        $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                        $dtoPageRender->availability = $html->availability;
                                        $dtoPageRender->url_cover_image = $html->url_cover_image;
                                        $dtoPageRender->title = $html->meta_tag_title;
                                        $pageToRender->push($dtoPageRender);
                                        return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                    } else {
                                          
                                        $html = UserBL::getHtml($url);
                                        if (isset($html->html) && $html->html != '') {
                                            $pageToRender = collect();
                                            $dtoPageRender = new DtoPageRender();
                                            $dtoPageRender->content = $html->html;
                                            $dtoPageRender->redirect = false;
                                            $dtoPageRender->hide_footer = false;
                                            $dtoPageRender->hide_header = false;
                                            $dtoPageRender->hideSearchEngine = false;
                                            $dtoPageRender->share_description = $html->meta_tag_description;
                                            $dtoPageRender->url_cover_image = $html->url_cover_image;
                                            $dtoPageRender->share_title = $html->meta_tag_title;
                                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                            $dtoPageRender->seo_description = $html->meta_tag_description;
                                            $dtoPageRender->seo_title = $html->meta_tag_title;
                                            $dtoPageRender->title = $html->meta_tag_title;
                                            $pageToRender->push($dtoPageRender);
                                            return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                        } else {
                                            $html = LinkPagesCategoryBL::getHtml($url);
                                            if (isset($html->html) && $html->html != '') {
                                                $pageToRender = collect();
                                                $dtoPageRender = new DtoPageRender();
                                                $dtoPageRender->content = $html->html;
                                                $dtoPageRender->redirect = false;
                                                $dtoPageRender->hide_footer = false;
                                                $dtoPageRender->hide_header = false;
                                                $dtoPageRender->hideSearchEngine = false;
                                                $dtoPageRender->title = $html->description;
                                                $dtoPageRender->share_title = $html->share_title;
                                                $dtoPageRender->share_description = $html->share_description;
                                                $dtoPageRender->seo_title = $html->seo_title;
                                                $dtoPageRender->seo_description = $html->seo_description;
                                                $dtoPageRender->seo_keyword = $html->seo_keyword;
                                                $dtoPageRender->url_cover_image = $html->url_cover_image;
                                                $dtoPageRender->availability = '';
                                                $pageToRender->push($dtoPageRender);
                                                return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                            } else {
                                                $html = RedirectBL::getHtml($url);
                                                if (isset($html->url_new) && $html->url_new != '') {
                                                    $pageToRender = collect();
                                                    $dtoPageRender = new DtoPageRender();
                                                    $dtoPageRender->content = $html->url_new;
                                                    $dtoPageRender->redirect = true;
                                                    $dtoPageRender->hide_footer = false;
                                                    $dtoPageRender->hide_header = false;
                                                    $dtoPageRender->hideSearchEngine = false;
                                                    $dtoPageRender->share_description = 'Pagina non trovata';
                                                    $dtoPageRender->share_title = 'Pagina non trovata';
                                                    $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                    $dtoPageRender->seo_description = 'Pagina non trovata';
                                                    $dtoPageRender->seo_title = 'Pagina non trovata';
                                                    $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                    $dtoPageRender->title = 'Pagina non trovata';
                                                    $pageToRender->push($dtoPageRender);
                                                    return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                
                                                }else{
                                                    $html404 = MessageBL::getByCode('ERROR_404_PAGE_NOT_FOUND'); 
                                                    if (isset($html404) && $html404 != '') {
                                                        $pageToRender = collect();
                                                        $dtoPageRender = new DtoPageRender();
                                                        $dtoPageRender->content = $html404;
                                                        $dtoPageRender->redirect = false;
                                                        $dtoPageRender->hide_footer = false;
                                                        $dtoPageRender->hide_header = false;
                                                        $dtoPageRender->hideSearchEngine = false;
                                                        $dtoPageRender->share_description = 'Pagina non trovata';
                                                        $dtoPageRender->share_title = 'Pagina non trovata';
                                                        $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                        $dtoPageRender->seo_description = 'Pagina non trovata';
                                                        $dtoPageRender->seo_title = 'Pagina non trovata';
                                                        $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                        $dtoPageRender->title = 'Pagina non trovata';
                                                        $pageToRender->push($dtoPageRender);
                                                        return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                    }else{
                                                        return '404';
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    
                    $html = NewsletterBL::getHtml($url);
                    if (isset($html) && $html != '') {
                        $pageToRender = collect();
                        $dtoPageRender = new DtoPageRender();
                        $dtoPageRender->content = $html;
                        $dtoPageRender->redirect = false;
                        $pageToRender->push($dtoPageRender);
                        return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                    } else {
                        $html = CategoriesBL::getHtml($url, $idLanguage);
                        if (isset($html->html) && $html->html != '') {
                            $pageToRender = collect();
                            $dtoPageRender = new DtoPageRender();
                            $dtoPageRender->content = $html->html;
                            $dtoPageRender->redirect = false;
                            $dtoPageRender->hide_footer = false;
                            $dtoPageRender->hide_header = false;
                            $dtoPageRender->hideSearchEngine = false;
                            $dtoPageRender->share_description = $html->meta_tag_description;
                            $dtoPageRender->share_title = $html->meta_tag_title;
                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                            $dtoPageRender->seo_description = $html->meta_tag_description;
                            $dtoPageRender->seo_title = $html->meta_tag_title;
                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                            //$dtoPageRender->seo_availability = $html->availability;
                            $dtoPageRender->title = $html->meta_tag_title;
                            $pageToRender->push($dtoPageRender);
                            return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                        } else {
                            $html = ItemBL::getHtml($url, $idUser, $idLanguage);
                            if (isset($html->html) && $html->html != '') {
                                $pageToRender = collect();
                                $dtoPageRender = new DtoPageRender();
                                $dtoPageRender->content = $html->html;
                                $dtoPageRender->redirect = false;
                                $dtoPageRender->hide_footer = false;
                                $dtoPageRender->hide_header = false;
                                $dtoPageRender->hideSearchEngine = false;
                                $dtoPageRender->share_description = $html->meta_tag_description;
                                $dtoPageRender->share_title = $html->meta_tag_title;
                                $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                $dtoPageRender->seo_description = $html->meta_tag_description;
                                $dtoPageRender->seo_title = $html->meta_tag_title;
                                $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                $dtoPageRender->availability = $html->availability;
                                $dtoPageRender->url_cover_image = $html->url_cover_image;
                                $dtoPageRender->title = $html->meta_tag_title;
                                $pageToRender->push($dtoPageRender);
                                return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                            } else {
                                $html = UserBL::getHtml($url);
                                if (isset($html->html) && $html->html != '') {
                                    $pageToRender = collect();
                                    $dtoPageRender = new DtoPageRender();
                                    $dtoPageRender->content = $html->html;
                                    $dtoPageRender->redirect = false;
                                    $dtoPageRender->hide_footer = false;
                                    $dtoPageRender->hide_header = false;
                                    $dtoPageRender->hideSearchEngine = false;
                                    $dtoPageRender->share_description = $html->name;
                                    $dtoPageRender->share_title = $html->name;
                                    $dtoPageRender->seo_keyword = $html->name;
                                    $dtoPageRender->seo_description = $html->name;
                                    $dtoPageRender->seo_title = $html->name;
                                    $dtoPageRender->seo_keyword = $html->name;
                                    $dtoPageRender->title = $html->business_name;
                                    $pageToRender->push($dtoPageRender);
                                    return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                } else {
                                    $html = LinkPagesCategoryBL::getHtml($url);
                                    if (isset($html->html) && $html->html != '') {
                                        $pageToRender = collect();
                                        $dtoPageRender = new DtoPageRender();
                                        $dtoPageRender->content = $html->html;
                                        $dtoPageRender->redirect = false;
                                        $dtoPageRender->hide_footer = false;
                                        $dtoPageRender->hide_header = false;
                                        $dtoPageRender->hideSearchEngine = false;
                                        $dtoPageRender->title = $html->description;
                                        $dtoPageRender->share_title = $html->share_title;
                                        $dtoPageRender->share_description = $html->share_description;
                                        $dtoPageRender->seo_title = $html->seo_title;
                                        $dtoPageRender->seo_description = $html->seo_description;
                                        $dtoPageRender->seo_keyword = $html->seo_keyword;
                                        $dtoPageRender->url_cover_image = $html->url_cover_image;
                                        $dtoPageRender->availability = '';
                                        $pageToRender->push($dtoPageRender);
                                        return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                    } else {
                                        $html = CompareItemBL::getHtml($url, $idUser, $idCart, $idLanguage);
                                        if (isset($html->html) && $html->html != '') {
                                            $pageToRender = collect();
                                            $dtoPageRender = new DtoPageRender();
                                            $dtoPageRender->content = $html->html;
                                            $dtoPageRender->redirect = false;
                                            $dtoPageRender->hide_footer = false;
                                            $dtoPageRender->hide_header = false;
                                            $dtoPageRender->hideSearchEngine = false;
                                            $dtoPageRender->title = $html->description;
                                            $dtoPageRender->share_title = $html->share_title;
                                            $dtoPageRender->share_description = $html->share_description;
                                            $dtoPageRender->seo_title = $html->seo_title;
                                            $dtoPageRender->seo_description = $html->seo_description;
                                            $dtoPageRender->seo_keyword = $html->seo_keyword;
                                            $dtoPageRender->url_cover_image = $html->url_cover_image;
                                            $dtoPageRender->availability = '';
                                            $pageToRender->push($dtoPageRender);
                                            return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                        } else {
                                            $html = CommunityImageBL::getHtml($url, $idUser);
                                            
                                            if (isset($html->html) && $html->html != '') {
                                                $pageToRender = collect();
                                                $dtoPageRender = new DtoPageRender();
                                                $dtoPageRender->content = $html->html;
                                                $dtoPageRender->redirect = false;
                                                $dtoPageRender->hide_footer = false;
                                                $dtoPageRender->hide_header = false;
                                                $dtoPageRender->hideSearchEngine = false;
                                                $dtoPageRender->share_description = $html->meta_tag_description;
                                                $dtoPageRender->share_title = $html->meta_tag_title;
                                                $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                $dtoPageRender->seo_description = $html->meta_tag_description;
                                                $dtoPageRender->seo_title = $html->meta_tag_title;
                                                $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                $dtoPageRender->availability = $html->availability;
                                                $dtoPageRender->url_cover_image = $html->url_cover_image;
                                                $dtoPageRender->title = $html->meta_tag_title;
                                                $pageToRender->push($dtoPageRender);
                                                return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId); 
                                            } else {
                                                $html = ItemGalleryBL::getHtml($url, $idUser);
                                                if (isset($html->html) && $html->html != '') {
                                                    $pageToRender = collect();
                                                    $dtoPageRender = new DtoPageRender();
                                                    $dtoPageRender->content = $html->html;
                                                    $dtoPageRender->redirect = false;
                                                    $dtoPageRender->hide_footer = false;
                                                    $dtoPageRender->hide_header = false;
                                                    $dtoPageRender->hideSearchEngine = false;
                                                    $dtoPageRender->share_description = $html->meta_tag_description;
                                                    $dtoPageRender->share_title = $html->meta_tag_title;
                                                    $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                    $dtoPageRender->seo_description = $html->meta_tag_description;
                                                    $dtoPageRender->seo_title = $html->meta_tag_title;
                                                    $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                    $dtoPageRender->availability = $html->availability;
                                                    $dtoPageRender->url_cover_image = $html->url_cover_image;
                                                    $dtoPageRender->title = $html->meta_tag_title;
                                                    $pageToRender->push($dtoPageRender);
                                                    return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                } else { 
                                                    $html = ItemPriceBL::getHtml($url, $idUser);
                                                    if (isset($html->html) && $html->html != '') {
                                                        $pageToRender = collect();
                                                        $dtoPageRender = new DtoPageRender();
                                                        $dtoPageRender->content = $html->html;
                                                        $dtoPageRender->redirect = false;
                                                        $dtoPageRender->hide_footer = false;
                                                        $dtoPageRender->hide_header = false;
                                                        $dtoPageRender->hideSearchEngine = false;
                                                        $dtoPageRender->share_description = $html->meta_tag_description;
                                                        $dtoPageRender->share_title = $html->meta_tag_title;
                                                        $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                        $dtoPageRender->seo_description = $html->meta_tag_description;
                                                        $dtoPageRender->seo_title = $html->meta_tag_title;
                                                        $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                        $dtoPageRender->availability = $html->availability;
                                                        $dtoPageRender->url_cover_image = $html->url_cover_image;
                                                        $dtoPageRender->title = $html->meta_tag_title;
                                                        $pageToRender->push($dtoPageRender);
                                                        return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                    } else {
                                                        $html = ListKeywordsBL::getHtml($url, $idUser, $idLanguage);
                                                        if (isset($html->html) && $html->html != '') {
                                                            $pageToRender = collect();
                                                            $dtoPageRender = new DtoPageRender();
                                                            $dtoPageRender->content = $html->html;
                                                            $dtoPageRender->redirect = false;
                                                            $dtoPageRender->hide_footer = false;
                                                            $dtoPageRender->hide_header = false;
                                                            $dtoPageRender->hideSearchEngine = false;
                                                            $dtoPageRender->share_description = $html->meta_tag_description;
                                                            $dtoPageRender->share_title = $html->meta_tag_title;
                                                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                            $dtoPageRender->seo_description = $html->meta_tag_description;
                                                            $dtoPageRender->seo_title = $html->meta_tag_title;
                                                            $dtoPageRender->seo_keyword = $html->meta_tag_keyword;
                                                            $dtoPageRender->url_cover_image = '';
                                                            $dtoPageRender->title = $html->meta_tag_title;
                                                            $pageToRender->push($dtoPageRender);
                                                            return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                        } else {
                                                            $html = RedirectBL::getHtml($url);
                                                            if (isset($html->url_new) && $html->url_new != '') {
                                                                $pageToRender = collect();
                                                                $dtoPageRender = new DtoPageRender();
                                                                $dtoPageRender->content = $html->url_new;
                                                                $dtoPageRender->redirect = true;
                                                                $dtoPageRender->hide_footer = false;
                                                                $dtoPageRender->hide_header = false;
                                                                $dtoPageRender->hideSearchEngine = false;
                                                                $dtoPageRender->share_description = 'Pagina non trovata';
                                                                $dtoPageRender->share_title = 'Pagina non trovata';
                                                                $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                                $dtoPageRender->seo_description = 'Pagina non trovata';
                                                                $dtoPageRender->seo_title = 'Pagina non trovata';
                                                                $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                                $dtoPageRender->title = 'Pagina non trovata';
                                                                $pageToRender->push($dtoPageRender);
                                                                return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                            
                                                            }else{
                                                                $html404 = MessageBL::getByCode('ERROR_404_PAGE_NOT_FOUND'); 
                                                                if (isset($html404) && $html404 != '') {
                                                                    $pageToRender = collect();
                                                                    $dtoPageRender = new DtoPageRender();
                                                                    $dtoPageRender->content = $html404;
                                                                    $dtoPageRender->redirect = false;
                                                                    $dtoPageRender->hide_footer = false;
                                                                    $dtoPageRender->hide_header = false;
                                                                    $dtoPageRender->hideSearchEngine = false;
                                                                    $dtoPageRender->share_description = 'Pagina non trovata';
                                                                    $dtoPageRender->share_title = 'Pagina non trovata';
                                                                    $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                                    $dtoPageRender->seo_description = 'Pagina non trovata';
                                                                    $dtoPageRender->seo_title = 'Pagina non trovata';
                                                                    $dtoPageRender->seo_keyword = 'Pagina non trovata';
                                                                    $dtoPageRender->title = 'Pagina non trovata';
                                                                    $pageToRender->push($dtoPageRender);
                                                                    return static::_convertToDtoUser($pageToRender, substr(strrchr($url, "/"), 1), 1, $idUser , $idCart, $idLanguage, $url, $param, $salesmanId);
                                                                }else{
                                                                    return '404';
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                         }
                    }
                }
            }
        }
    }

    /**
     * Get url list
     *
     * @param int idLanguage
     * @return DtoUrlList
     */
    public static function getUrl(int $idLanguage)
    {
        $dtoUrlLinks = collect();
        $idDefaultLanguage = LanguageBL::getDefault();

        foreach (Page::all() as $page) {
            $languageFound = false;
            
            $dtoUrlList = new DtoUrlList();
            $dtoUrlList->idPage = $page->id;
            $LanguagePageControl = LanguagePage::where('page_id', $page->id)->where('language_id', $idLanguage)->get();
            if(isset($LanguagePageControl)){
                foreach ($LanguagePageControl as $languagePage) {
                    if ($languagePage->language_id == $idLanguage || ($languagePage->language_id == $idDefaultLanguage && !$languageFound)) {
                        $dtoUrlList->title = $languagePage->title;
                        $languageFound = true;
                    }

                    $dtoUrlLinkDetail = new DtoUrlListDetail();
                    $dtoUrlLinkDetail->id = $languagePage->id;
                    $dtoUrlLinkDetail->idLanguage = $languagePage->language_id;
                    $dtoUrlLinkDetail->url = $languagePage->url;
                    $dtoUrlList->urlListDetail->push($dtoUrlLinkDetail);
                }
            }

            $dtoUrlLinks->push($dtoUrlList);
        }

        return $dtoUrlLinks;
    }

    /**
     * Get id language by url
     *
     * @param String url
     * @return idLanguage
     */
    public static function getIdLanguageUrl(String $url)
    {
        $dtoRender = DB::select(
            "   SELECT *
                FROM pages
                INNER JOIN languages_pages on pages.id = languages_pages.page_id
                WHERE languages_pages.id = COALESCE(
                                    (   SELECT languages_pages.id
                                        FROM pages
                                        INNER JOIN languages_pages on pages.id = languages_pages.page_id
                                        WHERE pages.online = true
                                        AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() )
                                        AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() )
                                        AND languages_pages.content IS NOT NULL
                                        AND languages_pages.url = ? ) ,
                                    (   SELECT languages_pages.id
                                        FROM pages
                                        INNER JOIN languages_pages on pages.id = languages_pages.page_id
                                        WHERE online = true
                                        AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() )
                                        AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() )
                                        AND languages_pages.language_id = ( SELECT l.id
                                                                            FROM languages l
                                                                            WHERE l.default = true )
                                        AND pages.id = (    SELECT page_id
                                                            FROM languages_pages
                                                            WHERE url = ? ) ) ) ",
            [$url, $url]
        );

        if (count($dtoRender) > 0) {
            return ArrayHelper::toCollection($dtoRender)[0]->language_id;
        } else {
            return LanguageBL::getDefault()->id;
        }
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @param DtoPageSetting $dtoPageSetting
     * @param int idUser
     * @param int idLanguage
     * @return int
     */
    public static function insert(Request $dtoPageSetting, int $idUser, $idLanguage)
    {
        DB::beginTransaction();

        try {
            static::_validateData($dtoPageSetting, $idLanguage, DbOperationsTypesEnum::INSERT);
            $config = FieldUserRoleBL::getConfiguration($dtoPageSetting->idFunctionality, $idUser, $idLanguage);
            $dtoPageSettingToInsert = static::_checkConfiguration($dtoPageSetting, $config);

            if ($dtoPageSettingToInsert->pageType == 'Blog' || $dtoPageSettingToInsert->pageType == 'News') {

                // $page = static::_convertToModelNewsBlog($dtoPageSettingToInsert);
                $page = new Page();
                if (isset($dtoPageSetting->fixed)) {
                    $page->fixed = $dtoPageSetting->fixed;
                }

                if (isset($dtoPageSetting->fixedEnd)) {
                    $page->fixed_end = $dtoPageSetting->fixedEnd;
                }

                if (isset($dtoPageSetting->hideSearchEngine)) {
                    $page->hide_search_engine = $dtoPageSetting->hideSearchEngine;
                }

                if (isset($dtoPageSetting->homepage)) {
                    $page->homepage = $dtoPageSetting->homepage;
                }else{
                    $page->homepage = false; 
                }

              /*  if (isset($dtoPageSetting->idAuthor)) {
                    $page->author_id = $dtoPageSetting->idAuthor;
                }*/

                if (isset($dtoPageSetting->idUser)) {
                    $page->author_id = $dtoPageSetting->idUser;
                }
                
                if (isset($dtoPageSetting->idIcon)) {
                    $page->icon_id = $dtoPageSetting->idIcon;
                }

                if (isset($dtoPageSetting->idPageCategory)) {
                    $page->page_category_id = $dtoPageSetting->idPageCategory;
                }

                if (isset($dtoPageSetting->idPageShareType)) {
                    $page->page_share_type_id = $dtoPageSetting->idPageShareType;
                }

                if (isset($dtoPageSetting->online)) {
                    $page->online = $dtoPageSetting->online;
                }

                if (isset($dtoPageSetting->publish)) {
                    $page->publish = $dtoPageSetting->publish;
                }


                if (isset($dtoPageSetting->visibleEnd)) {
                    $page->visible_end = $dtoPageSetting->visibleEnd;
                }

                if (isset($dtoPageSetting->visibleFrom)) {
                    $page->visible_from = $dtoPageSetting->visibleFrom;
                }

                if (isset($dtoPageSetting->pageType)) {
                    $page->page_type = $dtoPageSetting->pageType;
                }

                if (isset($dtoPageSetting->js)) {
                    $page->js = $dtoPageSetting->js;
                }

                if (isset($dtoPageSetting->enableJS)) {
                    $page->enable_js = $dtoPageSetting->enableJS;
                }else{
                    $page->enable_js = false;
                }

                if (isset($dtoPageSetting->protected)) {
                    $page->protected = $dtoPageSetting->protected;
                }
                $page->save();

                if(isset($dtoPageSetting->pageCategory)){
                    if(count($dtoPageSetting->pageCategory) > 0){
                        $pagesPagesCategory = PagePageCategory::where('page_id', $page->id);
                        if(isset($pagesPagesCategory)){
                            $pagesPagesCategory->delete();
                        }
            
                        foreach ($dtoPageSetting->pageCategory as $pageCategory) {
                            if($pageCategory['active']){
                                $pagesPagesCategoryNew = new PagePageCategory();
                                $pagesPagesCategoryNew->page_id = $page->id;
                                $pagesPagesCategoryNew->page_category_id = $pageCategory['id'];
                                $pagesPagesCategoryNew->created_id = $idUser;
                                $pagesPagesCategoryNew->save();
                            }
                        }
                    }
                }
                

            }

            if(isset($dtoPageSettingToInsert->pageType)){
                if ($dtoPageSettingToInsert->pageType == 'Page') {
                    $page = static::_convertToModel($dtoPageSettingToInsert);
                    $page->save();
                    $dtoPageSettingToInsert->id = $page->id;
                }
            }else{
                $page = static::_convertToModel($dtoPageSettingToInsert);
                $page->save();
                $dtoPageSettingToInsert->id = $page->id;
            }
            

            if (!is_null($dtoPageSettingToInsert->homePage) && $dtoPageSettingToInsert->homePage) {
                static::_setHome($dtoPageSettingToInsert->id);
            }

            if(isset($dtoPageSettingToInsert->hideMenu)){
                $hideMenu = $dtoPageSettingToInsert->hideMenu;
            }else{
                $hideMenu = false;
            }

            if (!$hideMenu) {
                if($dtoPageSettingToInsert->pageType == 'Page'){
                    $idMenu = MenuBL::insertOrUpdatePage($dtoPageSettingToInsert, $idLanguage);
                }else{
                    $idMenu = '';
                }
            }else{
               $idMenu = ''; 
            }
            
            foreach ($dtoPageSettingToInsert->languagePageSetting as $languagePageSetting) {
                if ($dtoPageSettingToInsert->pageType == 'Blog' || $dtoPageSettingToInsert->pageType == 'News' || $dtoPageSettingToInsert->pageType == '' || !isset($dtoPageSettingToInsert->pageType) 
                || $dtoPageSettingToInsert->pageType == 'Page' ) {
                    // LanguagePageBL::insertOrUpdateDtoNewsBlog($languagePageSetting, $dtoPageSetting->id, $idLanguage);
                    $languagePage = new LanguagePage();
                    $languagePage->page_id = $page->id;

                    if (isset($languagePageSetting['title'])) {
                        if (isset($languagePageSetting['idLanguage'])) {
                            $languagePage->language_id = $languagePageSetting['idLanguage'];
                        }                       

                        if (isset($languagePageSetting['title'])) {
                            $languagePage->title = $languagePageSetting['title'];
                        }

                        if (isset($languagePageSetting['content'])) {
                            $languagePage->content = $languagePageSetting['content'];
                        }

                        if (isset($languagePageSetting['url'])) {
                            $languagePage->url = $languagePageSetting['url'];
                        }

                        if (isset($languagePageSetting['urlCanonical'])) {
                            $languagePage->urlCanonical = $languagePageSetting['urlCanonical'];
                        }

                        if (isset($languagePageSetting['hideHeader'])) {
                            $languagePage->hide_header = $languagePageSetting['hideHeader'];
                        } else {
                            $languagePage->hide_header = false;
                        }

                        if (isset($languagePageSetting['hideFooter'])) {
                            $languagePage->hide_footer = $languagePageSetting['hideFooter'];
                        } else {
                            $languagePage->hide_footer = false;
                        }

                        if (isset($languagePageSetting['hideBreadcrumb'])) {
                            $languagePage->hide_breadcrumb = $languagePageSetting['hideBreadcrumb'];
                        } else {
                            $languagePage->hide_breadcrumb = false;
                        }

                        if (isset($languagePageSetting['seoTitle'])) {
                            $languagePage->seo_title = $languagePageSetting['seoTitle'];
                        }

                        if (isset($languagePageSetting['seoDescription'])) {
                            $languagePage->seo_description = $languagePageSetting['seoDescription'];
                        }

                        if (isset($languagePageSetting['SeoKeyword'])) {
                            $languagePage->seo_keyword = $languagePageSetting['SeoKeyword'];
                        }
                       

                        if (isset($languagePageSetting['shareTitle'])) {
                            $languagePage->share_title = $languagePageSetting['shareTitle'];
                        }

                        if (isset($languagePageSetting['shareDescription'])) {
                            $languagePage->share_description = $languagePageSetting['shareDescription'];
                        }

                        if (isset($languagePageSetting['urlCoverImage'])) {
                            $languagePage->url_cover_image = $languagePageSetting['urlCoverImage'];
                        }
                        
                        if(isset($languagePageSetting['imgName'])){
                            if(is_null($languagePageSetting['imgName'])){
                                $languagePage->url_cover_image = $languagePageSetting['urlCoverImage'];
                            }else{
                                Image::make($languagePageSetting['urlCoverImage'])->save(static::$dirImageAttachment . $languagePageSetting['imgName']);
                                $languagePage->url_cover_image = static::getUrlImage() . $languagePageSetting['imgName'];
                            }
                        }
                        if (isset($languagePageSetting['urlShareImage'])) {
                            $languagePage->url_share_image = $languagePageSetting['urlShareImage'];
                        }
                        
                        if(isset($languagePageSetting['imgNameShare'])){
                            if(is_null($languagePageSetting['imgNameShare'])){
                                $languagePage->url_share_image = $languagePageSetting['urlShareImage'];
                            }else{
                                Image::make($languagePageSetting['urlShareImage'])->save(static::$dirImageAttachment . $languagePageSetting['imgNameShare']);
                                $languagePage->url_share_image = static::getUrlImage() . $languagePageSetting['imgNameShare'];
                            }
                        }
                        $languagePage->save();
                    }
                }
 
                // if ($dtoPageSettingToInsert->pageType == '') {
                //LanguagePageBL::insertOrUpdateDto($languagePageSetting, $dtoPageSettingToInsert->id, $idLanguage);
                //}

                if (!is_null($hideMenu) && !$hideMenu && $idMenu != '') {
                    LanguageMenuBL::insertOrUpdateDto(
                        LanguageMenuBL::convertDtoLanguagePageSettingToDtoLanguageMenu($languagePageSetting),  
                        $idMenu,
                        $idLanguage
                    );  
                }
            }
            
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        //return $dtoPageSettingToInsert->id;
        return $languagePage->page_id;
    }

    /**
     * Clone
     * 
     * @param int id
     * @param int idFunctionality
     * @param int idLanguage
     * 
     * @return int
     */
    public static function clone(int $id, int $idFunctionality, int $idUser, $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $page = Page::find($id);

        if (is_null($page)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PageNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        DB::beginTransaction();

        try {
            #region PAGE

            $newPage = new Page();
            $newPage->fixed = $page->fixed;
            $newPage->fixed_end = $page->fixed_end;
            $newPage->hide_search_engine = $page->hide_search_engine;
            $newPage->homepage = false;
            $newPage->author_id = $page->author_id;
            $newPage->icon_id = $page->icon_id;
            $newPage->page_category_id = $page->page_category_id;
            $newPage->page_share_type_id = $page->page_share_type_id;
            if ($page->page_type == PageTypesEnum::Page) {
                $newPage->online = false;
            } else {
                $newPage->online = $page->online;
            }
            $newPage->publish = $page->publish;
            $newPage->visible_end = $page->visible_end;
            $newPage->visible_from = $page->visible_from;
            $newPage->page_type = $page->page_type;
            $newPage->js = $page->js;
            $newPage->enable_js = $page->enable_js;
            $newPage->protected = $page->protected;
            $newPage->save();

            #endregion PAGE

            LanguagePageBL::cloneByIdPage($id, $newPage->id);

            DB::commit();

            return $newPage->id;
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion INSERT
    /**
     * Get max order
     *
     * @return int
     */
    private static function _getMaxOrder()
    {
        $order = Menu::max('order');

        if (is_null($order)) {
            $order = 0;
        }

        return $order;
    }



    #endregion CONVERT

    /**
     * Convert from DtoPageSetting to Model
     *
     * @param DtoPageSetting $dtoPageSetting
     * @return \App\Menu
     */
    private static function _convertFromDtoPageSettingPage(DtoPageSetting $dtoPageSetting)
    {
        $menu = new Menu();
        $menu->icon_id = $dtoPageSetting->idIcon;
        $menu->page_id = $dtoPageSetting->id;
        $menu->hide_mobile = $dtoPageSetting->hideMenuMobile;
        return $menu;
    }


    #region UPDATE

    /**
     * update
     *
     * @param DtoPageSetting $dtoPageSetting
     * @param int idUser
     * @param int idLanguage
     * @return int
     */
    public static function update(Request $dtoPageSetting, int $idUser, int $idLanguage)
    {
        DB::beginTransaction();

        try {
            static::_validateData($dtoPageSetting, $idLanguage, DbOperationsTypesEnum::UPDATE);

            $pageOnDB = Page::find($dtoPageSetting->id);

            if (is_null($pageOnDB)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PageNotExist), HttpResultsCodesEnum::InvalidPayload);
            }

            $dtoPageSettingOnDB = static::getSetting($dtoPageSetting->id, $dtoPageSetting->idUser =='');
  
            
           /* if(isset($dtoPageSetting->pageCategory)){
                if(count($dtoPageSetting->pageCategory) > 0){
                    $pagesPagesCategory = PagePageCategory::where('page_id', $dtoPageSetting->id);
                    if(isset($pagesPagesCategory)){
                        $pagesPagesCategory->delete();
                    }
        
                    foreach ($dtoPageSetting->pageCategory as $pageCategory) {
                        if($pageCategory['active']){
                            $pagesPagesCategoryNew = new PagePageCategory();
                            $pagesPagesCategoryNew->page_id = $dtoPageSetting->id;
                            $pagesPagesCategoryNew->page_category_id = $pageCategory['id'];
                            $pagesPagesCategoryNew->created_id = $idUser;
                            $pagesPagesCategoryNew->save();
                        }
                    }
                }
            }*/

            $config = FieldUserRoleBL::getConfiguration($dtoPageSetting->idFunctionality, $idUser, $idLanguage);
            $dtoPageSettingToUpdate = static::_checkConfiguration($dtoPageSetting, $config, $dtoPageSettingOnDB);
            $dtoPageSettingToUpdate->id = $dtoPageSetting->id;

            if ($dtoPageSettingToUpdate->pageType == 'Blog' || $dtoPageSettingToUpdate->pageType == 'News' || $dtoPageSettingToUpdate->pageType == 'Page') {
                DBHelper::updateAndSave(static::_convertToModelNewsBlog($dtoPageSettingToUpdate), $pageOnDB);
            } else {
                DBHelper::updateAndSave(static::_convertToModel($dtoPageSettingToUpdate), $pageOnDB);
            }

            if (!is_null($dtoPageSettingToUpdate->homePage) && $dtoPageSettingToUpdate->homePage) {
                static::_setHome($dtoPageSettingToUpdate->id);
            }

            if ($dtoPageSetting->pageType == 'Page') {
                if (!is_null($dtoPageSetting->hideMenu) && !$dtoPageSetting->hideMenu) {
                    $idMenu = MenuBL::insertOrUpdatePage($dtoPageSetting, $idLanguage);
                } else {
                    $menu = MenuBL::getByIdPage($dtoPageSetting->id);
                    if (!is_null($menu)) {
                        MenuBL::delete($menu->id);
                    }
                }
            }



            if ($dtoPageSetting->pageType == '') {
                if (!is_null($dtoPageSettingToUpdate->hideMenu) && !$dtoPageSettingToUpdate->hideMenu) {
                    $idMenu = MenuBL::insertOrUpdatePage($dtoPageSettingToUpdate, $idLanguage);
                } else {
                    $menu = MenuBL::getByIdPage($dtoPageSetting->id);
                    if (!is_null($menu)) {
                        MenuBL::delete($menu->id);
                    }
                }
            }

            foreach ($dtoPageSettingToUpdate->languagePageSetting as $languagePageSetting) {
                if(is_array($languagePageSetting)){
                    if ($dtoPageSettingToUpdate->pageType == 'Blog' || $dtoPageSettingToUpdate->pageType == 'News' || $dtoPageSettingToUpdate->pageType == 'Page') {
                        
                        if(count($languagePageSetting) < 6){

                        }else{
                            LanguagePageBL::insertOrUpdateDtoNewsBlog($languagePageSetting, $dtoPageSetting->id, $idLanguage);
                        }
                    } else {
                        LanguagePageBL::insertOrUpdateDto($languagePageSetting, $dtoPageSetting->id, $idLanguage);
                    }

                    if ($dtoPageSetting->pageType == '') {
                        if (!is_null($dtoPageSettingToUpdate->hideMenu) && !$dtoPageSettingToUpdate->hideMenu) {
                            LanguageMenuBL::insertOrUpdateDto(
                                LanguageMenuBL::convertDtoLanguagePageSettingToDtoLanguageMenu($languagePageSetting),
                                $idMenu,
                                $idLanguage
                            );
                        }
                    }
                }
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    /**
     * Update url list
     *
     * @param Request $dtoPageSettings
     * @param int $idLanguage
     * @return DtoResult
     */
    public static function postUrl(Request $dtoUrlLists, int $idLanguage)
    {
        $dtoResults = collect();

        if (!is_null($dtoUrlLists->idPage)) {
            $dtoResults->push(static::_insertOrUpdateUrl($dtoUrlLists, $idLanguage));
        } else {
            for ($i = 0; $i < count($dtoUrlLists->toArray()); $i++) {
                try {
                    $dtoResults->push(static::_insertOrUpdateUrl($dtoUrlLists[$i], $idLanguage));
                } catch (Exception $ex) {
                    $dtoResult = new DtoResult();
                    $dtoResult->code = HttpResultsCodesEnum::InternalServerError;
                    $dtoResult->message = $ex->getMessage();
                    $dtoResults->push($dtoResult);
                    LogHelper::error($ex);
                }
            }
        }

        return $dtoResults;
    }

    /**
     * insertImage
     *
     * @param Request $request
     * @return DtoResult
     */
    public static function insertImage(Request $request)
    {
        $date = date("Y-m-d");
        $dateElements = explode('-', $date);
        $year = $dateElements[0];
        $month = $dateElements[1];

        $dir = static::$dirImageAttachmentImages;
        $return = '';

        if (!file_exists(static::$dirImageAttachmentImages . '/' . $year)) {
            mkdir(static::$dirImageAttachmentImages . '/' . $year, 0777, true);
            mkdir(static::$dirImageAttachmentImages . '/' . $year . '/' . $month, 0777, true);
            // insert here images in the folder static::$dirImageAttachmentImages . '/' . $year . '/' . $month . '/'
            Image::make($request->imgBase64)->save(static::$dirImageAttachmentImages . '/' . $year . '/' . $month . '/'. $request->imgName);

            $srcset = static::_resizeImageByWidth($request->imgBase64, $request->imgName);

            $return = 'ok';

            $listImages = new DtoImage();

            $data = static::$dirImageAttachmentImages . '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
            $extension = substr($data, -3, 3); 
            if ($extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'peg' || $extension == 'ebp'){
                if (file_exists($data)) {
                    $date = filemtime($data);
                    $listImages->date = date('d/m/Y', $date);
                    if(filesize($data) > 0){
                        $dimension = getimagesize($data);
                        if($dimension){
                            $listImages->width = $dimension[0];
                            $listImages->height = $dimension[1];
                        }else{
                            $listImages->width = 0;
                            $listImages->height = 0;
                        }
                        $listImages->weight = static::_convertImageSize(filesize($data));
                        $listImages->ext = pathinfo($data, PATHINFO_EXTENSION);
                        $filenamewithextension = basename($data);
                        $listImages->nameFile = basename($filenamewithextension);
                    }else{
                        $listImages->width = 0;
                        $listImages->height = 0;
                        $listImages->weight = 0;
                        $listImages->ext = '';
                        $listImages->nameFile = '';
                    }
                }else{
                    $listImages->date = '';
                    $listImages->weight = 0;
                    $listImages->width = 0;
                    $listImages->height = 0;
                    $listImages->ext = '';
                    $listImages->nameFile = '';
                }
            }else{
                $listImages->date = '';
                $listImages->weight = 0;
                $listImages->width = 0;
                $listImages->height = 0;
                $listImages->ext = '';
                $listImages->nameFile = '';
            }


            $listImages->id = 10002;
            $listImages->return = $return;
            $listImages->idImgAgg = '';
            $listImages->img =  static::getUrlImage() . '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
            $listImages->imgName = '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
            $listImages->title = '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
            $listImages->visible = true;
            $listImages->selected = true;
            $listImages->active = true;
            $listImages->alt = '';
            $listImages->didascalia = '';
            $listImages->srcset = $srcset;
            
            $listImages->url = static::getUrlImage() . '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
            $return = $listImages;


        }else{
            if (!file_exists(static::$dirImageAttachmentImages . '/' . $year . '/' . $month)) {
                mkdir(static::$dirImageAttachmentImages . '/' . $year . '/' . $month, 0777, true);
                //insert here images in the folder static::$dirImageAttachmentImages . '/' . $year . '/' . $month . '/'
                Image::make($request->imgBase64)->save(static::$dirImageAttachmentImages . '/' . $year . '/' . $month . '/'. $request->imgName);
                $srcset = static::_resizeImageByWidth($request->imgBase64, $request->imgName);
                
                $return = 'ok';

                $listImages = new DtoImage();

                $data = static::$dirImageAttachmentImages . '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
                $extension = substr($data, -3, 3); 
                if ($extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'peg' || $extension == 'ebp'){
                    if (file_exists($data)) {
                        $date = filemtime($data);
                        $listImages->date = date('d/m/Y', $date);
                        if(filesize($data) > 0){
                            $dimension = getimagesize($data);
                            if($dimension){
                                $listImages->width = $dimension[0];
                                $listImages->height = $dimension[1];
                            }else{
                                $listImages->width = 0;
                                $listImages->height = 0;
                            }
                            $listImages->weight = static::_convertImageSize(filesize($data));
                            $listImages->ext = pathinfo($data, PATHINFO_EXTENSION);
                            $filenamewithextension = basename($data);
                            $listImages->nameFile = basename($filenamewithextension);
                        }else{
                            $listImages->width = 0;
                            $listImages->height = 0;
                            $listImages->weight = 0;
                            $listImages->ext = '';
                            $listImages->nameFile = '';
                        }
                    }else{
                        $listImages->date = '';
                        $listImages->weight = 0;
                        $listImages->width = 0;
                        $listImages->height = 0;
                        $listImages->ext = '';
                        $listImages->nameFile = '';
                    }
                }else{
                    $listImages->date = '';
                    $listImages->weight = 0;
                    $listImages->width = 0;
                    $listImages->height = 0;
                    $listImages->ext = '';
                    $listImages->nameFile = '';
                }


                $listImages->id = 10002;
                $listImages->return = $return;
                $listImages->idImgAgg = '';
                $listImages->img =  static::getUrlImage() . '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
                $listImages->imgName = '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
                $listImages->title = '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
                $listImages->visible = true;
                $listImages->selected = true;
                $listImages->active = true;
                $listImages->alt = '';
                $listImages->didascalia = '';
                $listImages->srcset = $srcset;
                $listImages->url = static::getUrlImage() . '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
                $return = $listImages;

            }else{
                //check if image exists
                //if not exist 
                //insert here images static::$dirImageAttachmentImages . '/' . $year . '/' . $month . '/'
                //else
                //return images
                if (file_exists(static::$dirImageAttachmentImages . '/' . $year . '/' . $month . '/' . $request->imgName)) {
                    if($request->replaceImg){
                        $dir = static::$dirImageAttachmentImages . '/' . $year . '/' . $month . '/';
                        $filesystem = new Filesystem(new Adapter($dir));
                        $filesystem->delete($request->imgName);

                        Image::make($request->imgBase64)->save(static::$dirImageAttachmentImages . '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName));
                        $srcset = static::_resizeImageByWidth($request->imgBase64, str_replace(' ', '-', $request->imgName));
                        $return = 'ok';

                        $listImages = new DtoImage();

                        $data = static::$dirImageAttachmentImages . '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
                        $extension = substr($data, -3, 3); 
                        if ($extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'peg' || $extension == 'ebp'){
                            if (file_exists($data)) {
                                $date = filemtime($data);
                                $listImages->date = date('d/m/Y', $date);
                                if(filesize($data) > 0){
                                    $dimension = getimagesize($data);
                                    if($dimension){
                                        $listImages->width = $dimension[0];
                                        $listImages->height = $dimension[1];
                                    }else{
                                        $listImages->width = 0;
                                        $listImages->height = 0;
                                    }
                                    $listImages->weight = static::_convertImageSize(filesize($data));
                                    $listImages->ext = pathinfo($data, PATHINFO_EXTENSION);
                                    $filenamewithextension = basename($data);
                                    $listImages->nameFile = basename($filenamewithextension);
                                }else{
                                    $listImages->width = 0;
                                    $listImages->height = 0;
                                    $listImages->weight = 0;
                                    $listImages->ext = '';
                                    $listImages->nameFile = '';
                                }
                            }else{
                                $listImages->date = '';
                                $listImages->weight = 0;
                                $listImages->width = 0;
                                $listImages->height = 0;
                                $listImages->ext = '';
                                $listImages->nameFile = '';
                            }
                        }else{
                            $listImages->date = '';
                            $listImages->weight = 0;
                            $listImages->width = 0;
                            $listImages->height = 0;
                            $listImages->ext = '';
                            $listImages->nameFile = '';
                        }


                        $listImages->id = 10002;
                        $listImages->return = $return;
                        $listImages->idImgAgg = '';
                        $listImages->img =  static::getUrlImage() . '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
                        $listImages->imgName = '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
                        $listImages->title = '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
                        $listImages->visible = true;
                        $listImages->selected = true;
                        $listImages->active = true;
                        $listImages->alt = '';
                        $listImages->didascalia = '';
                        $listImages->srcset = $srcset;
                        $listImages->url = static::getUrlImage() . '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
                        $return = $listImages;

                    }else{
                        $return = 'Immagine ' . $request->imgName . ' già esistente';

                        $listImages = new DtoImage();
                        $listImages->return = $return;
                        $return = $listImages;
                    }
                }else{
                    Image::make($request->imgBase64)->save(static::$dirImageAttachmentImages . '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName));
                    
                    $srcset = static::_resizeImageByWidth($request->imgBase64, str_replace(' ', '-', $request->imgName));

                    $return = 'ok';

                    
                    $listImages = new DtoImage();

                    $data = static::$dirImageAttachmentImages . '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
                    $extension = substr($data, -3, 3); 
                    if ($extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'peg' || $extension == 'ebp'){
                        if (file_exists($data)) {
                            $date = filemtime($data);
                            $listImages->date = date('d/m/Y', $date);
                            if(filesize($data) > 0){
                                $dimension = getimagesize($data);
                                if($dimension){
                                    $listImages->width = $dimension[0];
                                    $listImages->height = $dimension[1];
                                }else{
                                    $listImages->width = 0;
                                    $listImages->height = 0;
                                }
                                $listImages->weight = static::_convertImageSize(filesize($data));
                                $listImages->ext = pathinfo($data, PATHINFO_EXTENSION);
                                $filenamewithextension = basename($data);
                                $listImages->nameFile = basename($filenamewithextension);
                            }else{
                                $listImages->width = 0;
                                $listImages->height = 0;
                                $listImages->weight = 0;
                                $listImages->ext = '';
                                $listImages->nameFile = '';
                            }
                        }else{
                            $listImages->date = '';
                            $listImages->weight = 0;
                            $listImages->width = 0;
                            $listImages->height = 0;
                            $listImages->ext = '';
                            $listImages->nameFile = '';
                        }
                    }else{
                        $listImages->date = '';
                        $listImages->weight = 0;
                        $listImages->width = 0;
                        $listImages->height = 0;
                        $listImages->ext = '';
                        $listImages->nameFile = '';
                    }

                    $listImages->id = 10002;
                    $listImages->return = $return;
                    $listImages->idImgAgg = '';
                    $listImages->img =  static::getUrlImage() . '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
                    $listImages->imgName = '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
                    $listImages->title = '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
                    $listImages->visible = true;
                    $listImages->selected = true;
                    $listImages->active = true;
                    $listImages->alt = '';
                    $listImages->didascalia = '';
                    $listImages->srcset = $srcset;
                    $listImages->url = static::getUrlImage() . '/' . $year . '/' . $month . '/'. str_replace(' ', '-', $request->imgName);
                    $return = $listImages;

                }
            }
        }
        return $return;
        
    }

    /**
     * convert Image To Webp
     *
     * @param Request $request
     * @return DtoResult
     */
    public static function convertImageToWebp(Request $request)
    {
        $return = "";
        $date = date("Y-m-d");
        $dateElements = explode('-', $date);
        $year = $dateElements[0];
        $month = $dateElements[1];
        $pathAdd = '';

        if($request->month != '' && $request->year != ''){
            $pathAdd = $request->year . '/' . $request->month . '/';
        }else{
            if($request->month != ''){
                $pathAdd = $year . '/' . $request->month . '/';
            }
            if($request->year != ''){
                $pathAdd = $request->year . '/';
            }
        }

        $url = $request->data['url'];
        $image = file_get_contents($url);

        $urlImageWebpToSave = static::$dirImageAttachmentImages . '/' . $pathAdd . str_replace('.' . $request->data['ext'], '', $request->data['imgName']) . '.webp';

        if (!file_exists($urlImageWebpToSave)) {
            if ($image !== false){
                $imageBase64 = 'data:image/' . $request->data['ext'] . ';base64,' . base64_encode($image);
                
                $imageWebp = Image::make($imageBase64)->encode('webp', 70)->save($urlImageWebpToSave, 70);
                
                /*$resultTiny = Tinify::fromBuffer($imageBase64);
                $dataTiny = $resultTiny->toBuffer();
                $imageWebp = Image::make($dataTiny)->encode('webp', 70)->save($urlImageWebpToSave);*/

                $filenamewithextension = basename($urlImageWebpToSave);
                $nameFile = basename($filenamewithextension);
                $srcSet = static::_resizeImageByWidth($imageBase64, $nameFile . '.webp');
                if($srcSet !=''){
                    $return = 'ok';
                }else{
                    $return = 'Errore durante il ridimensionamento della conversione immagine';
                }
            }else{
                $return = 'Errore durante la conversione immagine';
            }
        }else{
            $return = 'Immagine in formato .webp già presente!';
        }
        return $return;
    }

    /**
     * delete image
     *
     * @param Request $request
     * @return DtoResult
     */
    public static function deleteImage(Request $request)
    {

       // $filesystem = new Filesystem(new Adapter($dir));

        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        $str = str_replace($protocol . $_SERVER["HTTP_HOST"] . '/storage/images', static::$dirImageAttachmentImages, $request->img);

        $dirNew = str_replace($protocol . $_SERVER["HTTP_HOST"] . '/storage/images', '', $request->img);
        $dirNew = str_replace($request->imgName, '', $dirNew);

        if (file_exists($str)) {
            $dir = static::$dirImageAttachmentImages . $dirNew;
            $filesystem = new Filesystem(new Adapter($dir));
            $filesystem->delete($request->imgName);
            
            $return = 'Immagine eliminata correttamente.';
        }else{
            $return = 'Immagine impossibile da eliminare poichè l\'url è errato.';
        }

        //$return = static::$dirImageAttachmentImages . $dirNew;
        return $return;
    }

    /**
     * Post online page from list menu
     *
     * @param Request $request
     * @param int $idLanguage
     */
    public static function setOnlinePage(Request $request, int $idLanguage)
    {
        $online = $request->online;
        $id = $request->id;
        $pageOnDB = Page::find($id);
        if (is_null($pageOnDB)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PageNotExist), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null($id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            } else {
                if (is_null($online)) {
                    $online = false;
                }
                $pageOnDB->online = $online;
                $pageOnDB->save();
            }
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $page = Page::find($id);

        if (is_null($page)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PageNotExist), HttpResultsCodesEnum::InvalidPayload);
        } else {
            $menu = MenuBL::getByIdPage($id);

            if (!is_null($menu)) {
                MenuBL::delete($menu->id);
            }

            $PagePageCategory = PagePageCategory::where('page_id', $id);
            if (!is_null($PagePageCategory)) {
                $PagePageCategory->delete();
            }

            LanguagePageBL::deleteByIdPage($id);
            $page->delete();

        }
    }

    public static function getPages(string $page_type, int $idLanguage)
    {

        $result = collect();
        foreach (DB::select(
            "SELECT languages_pages.title, pages.updated_at, languages_pages.url
            FROM pages
            INNER JOIN languages_pages ON pages.id = languages_pages.page_id
            AND languages_pages.language_id = " . $idLanguage . "AND pages.page_type = '" . $page_type . "' ORDER BY pages.updated_at desc"
        ) as $PagesAll) {
            $dtoPageSitemap = new DtoPageSitemap();
            $dtoPageSitemap->url = $PagesAll->url;
            $dtoPageSitemap->title = $PagesAll->title;
            $dtoPageSitemap->updated_at = $PagesAll->updated_at;

            $result->push($dtoPageSitemap);
        }
        return $result;
    }

    
    public static function getPageIDByUrl($url){
        $languagePage = LanguagePage::where('url', $url)->first();
        if(isset($languagePage)){
            return $languagePage->page_id;
        }else
            return 0;
    }

    public static function getHtmlShortcode(string $shortcode){ 

        $type = '';
        $items = 3;
        $order = 'DESC';
        $category = '';
        
        //gli spazi ci devono essere solo tra i parametri
        $arrayParametri = explode(" ", $shortcode);

        foreach ($arrayParametri as $valore) {
       
            if (strpos($valore, 'type=')!==false){
                $type = substr($valore, strpos($valore, 'type=')+5);
            }
            if (strpos($valore, 'items=')!==false){
                $items = substr($valore, strpos($valore, 'items=')+6);
            }
            if (strpos($valore, 'order=')!==false){
                $order = substr($valore, strpos($valore, 'order=')+6);
            }
            if (strpos($valore, 'category=')!==false){
                $category = substr($valore, strpos($valore, 'category=')+9);
                $category = str_replace('|', '\',\'', $category);
                $category = str_replace('_', ' ', $category);
                $category = str_replace('&agrave;', 'à', $category);

                $category = '\''.$category.'\'';
            }
        }

        $idContent = Content::where('code', 'ArticlesList')->first();
        
        if(isset($idContent)){
            $contentLanguage = ContentLanguage::where('content_id', $idContent->id)->first();

            $finalHtml = '';
            $newContent = '';
            $tags = SelenaViewsBL::getTagByType('BlogListArticle');                    
            $idLanguage = LanguageBL::getDefault()->id; 

            /*
            $query = '';
            $query = " SELECT url, url_cover_image, title, seo_description FROM materialized_view_article ";
            $query = $query . " WHERE online = true ";
            $query = $query . " AND ( visible_from IS NULL OR visible_from <= NOW() ) ";
            $query = $query . " AND ( visible_end IS NULL OR visible_end >= NOW() ) ";
            $query = $query . " AND publish is not NULL ";
            */
            if ($category != ''){

                $query = " SELECT languages_pages.url,  languages_pages.url_cover_image,pages.id,";
                $query = $query . " languages_pages.title, languages_pages.seo_description, pages.publish";
                $query = $query . " from pages_pages_category "; 
                $query = $query . " inner join pages on pages.id = pages_pages_category.page_id ";
                $query = $query . " INNER JOIN languages_pages ON pages.id = languages_pages.page_id AND languages_pages.language_id = " . $idLanguage;
                $query = $query . " INNER JOIN languages_page_categories ON languages_page_categories.page_category_id = pages_pages_category.page_category_id AND languages_page_categories.language_id = " . $idLanguage;
                $query = $query . " where pages.online = true ";
                $query = $query . " AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() ) ";
                $query = $query . " AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() ) ";
                $query = $query . " AND publish is not NULL ";
                $query = $query . " AND languages_page_categories.description = (" .$category. ")";
                

             }else{
                 
                $query = " SELECT languages_pages.url,  languages_pages.url_cover_image,pages.id,";
                $query = $query . " languages_pages.title, languages_pages.seo_description, pages.publish";
                $query = $query . " FROM pages ";
                $query = $query . " INNER JOIN languages_pages ON pages.id = languages_pages.page_id ";
                $query = $query . " WHERE pages.online = true ";
                $query = $query . " AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() ) ";
                $query = $query . " AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() ) ";
                $query = $query . " AND languages_pages.content IS NOT NULL ";
                $query = $query . " AND publish is not NULL ";
                $query = $query . " AND languages_pages.language_id = " . $idLanguage; 
                
            }

            if ($type != '') {
                $query = $query . ' AND pages.page_type = \''.$type.'\'';
                //$query = $query . ' AND page_type = \''.$type.'\'';
            }   

            if (strtolower($order)=='random')
                $query = $query . " ORDER BY random()";
            else
                $query = $query . " ORDER BY pages.publish ".$order;

            $query = $query . " LIMIT ".$items;

            $finalHtml = '';
            
            foreach (DB::select($query) as $listBlog){
                $newContent = $contentLanguage->content;                        
                foreach ($tags as $fieldsDb){
                    if(isset($fieldsDb->tag)){                                                                 
                        if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                            $cleanTag = str_replace('#', '', $fieldsDb->tag);
                            if(is_numeric($listBlog->{$cleanTag}) && strpos($listBlog->{$cleanTag},'.')){
                                $tag = str_replace('.',',',substr($listBlog->{$cleanTag}, 0, -2));
                            }else{
                                $tag = $listBlog->{$cleanTag};
                            }
                            $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                               
                        }
                    }
                }
                if (strpos($newContent, '#publish#') !== false) {
                    $newContent = str_replace('#publish#', date_format(date_create($listBlog->publish), SettingBL::getByCode(SettingEnum::DateFormat)), $newContent);
                }
                if (strpos($newContent, '#category_name#') !== false) {
                    $PagePageCategory = PagePageCategory::where('page_id', $listBlog->id)->first(); 
                if (isset($PagePageCategory)){
                    $LanguagePageCategory = LanguagePageCategory::where('page_category_id', $PagePageCategory->page_category_id)->first(); 
                }
                if (isset($LanguagePageCategory)){
                    $newContent = str_replace('#category_name#', $LanguagePageCategory->description,  $newContent); 
                    }else{
                        $newContent = str_replace('#category_name#', '',  $newContent);           
                    }
                }
                
                $finalHtml = $finalHtml . $newContent;
            }

            return $finalHtml;
        }else{
            return '';
        }
    }
    public static function getHtmlShortcodeAdd(string $shortcode){ 

        $type = '';
        $items = 3;
        $order = 'DESC';
        $category = '';
        
        //gli spazi ci devono essere solo tra i parametri
        $arrayParametri = explode(" ", $shortcode);

        foreach ($arrayParametri as $valore) {
       
            if (strpos($valore, 'type=')!==false){
                $type = substr($valore, strpos($valore, 'type=')+5);
            }
            if (strpos($valore, 'items=')!==false){
                $items = substr($valore, strpos($valore, 'items=')+6);
            }
            if (strpos($valore, 'order=')!==false){
                $order = substr($valore, strpos($valore, 'order=')+6);
            }
            if (strpos($valore, 'category=')!==false){
                $category = substr($valore, strpos($valore, 'category=')+9);
                $category = str_replace('|', '\',\'', $category);
                $category = str_replace('_', ' ', $category);
                $category = str_replace('&agrave;', 'à', $category);

                $category = '\''.$category.'\'';
            }
        }

        $idContent = Content::where('code', 'ArticlesListAdd')->first();
        
        if(isset($idContent)){
            $contentLanguage = ContentLanguage::where('content_id', $idContent->id)->first();

            $finalHtml = '';
            $newContent = '';
            $tags = SelenaViewsBL::getTagByType('BlogListArticle');                    
            $idLanguage = LanguageBL::getDefault()->id; 

            if ($category != ''){
                $query = " SELECT languages_pages.url,  languages_pages.url_cover_image,";
                $query = $query . " languages_pages.title, languages_pages.seo_description, pages.publish";
                $query = $query . " from pages_pages_category "; 
                $query = $query . " inner join pages on pages.id = pages_pages_category.page_id ";
                $query = $query . " INNER JOIN languages_pages ON pages.id = languages_pages.page_id AND languages_pages.language_id = " . $idLanguage;
                $query = $query . " INNER JOIN languages_page_categories ON languages_page_categories.page_category_id = pages_pages_category.page_category_id AND languages_page_categories.language_id = " . $idLanguage;
                $query = $query . " where pages.online = true ";
                $query = $query . " AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() ) ";
                $query = $query . " AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() ) ";
                $query = $query . " AND publish is not NULL ";
                $query = $query . " AND languages_page_categories.description = (" .$category. ")"; 

             }else{
                 
                $query = " SELECT languages_pages.url,  languages_pages.url_cover_image,";
                $query = $query . " languages_pages.title, languages_pages.seo_description, pages.publish";
                $query = $query . " FROM pages ";
                $query = $query . " INNER JOIN languages_pages ON pages.id = languages_pages.page_id ";
                $query = $query . " WHERE pages.online = true ";
                $query = $query . " AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() ) ";
                $query = $query . " AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() ) ";
                $query = $query . " AND languages_pages.content IS NOT NULL ";
                $query = $query . " AND publish is not NULL ";
                $query = $query . " AND languages_pages.language_id = " . $idLanguage; 
                
            }

            if ($type != '') {
                $query = $query . ' AND pages.page_type = \''.$type.'\'';
                //$query = $query . ' AND page_type = \''.$type.'\'';
            }   

            if (strtolower($order)=='random')
                $query = $query . " ORDER BY random()";
            else
                $query = $query . " ORDER BY pages.publish ".$order;

            $query = $query . " LIMIT ".$items;

            $finalHtml = '';
            
            foreach (DB::select($query) as $listBlog){
                $newContent = $contentLanguage->content;                        
                foreach ($tags as $fieldsDb){
                    if(isset($fieldsDb->tag)){                                                                 
                        if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                            $cleanTag = str_replace('#', '', $fieldsDb->tag);
                            if(is_numeric($listBlog->{$cleanTag}) && strpos($listBlog->{$cleanTag},'.')){
                                $tag = str_replace('.',',',substr($listBlog->{$cleanTag}, 0, -2));
                            }else{
                                $tag = $listBlog->{$cleanTag};
                            }
                            $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                               
                        }
                    }
                }
                if (strpos($newContent, '#publish#') !== false) {
                    $newContent = str_replace('#publish#', date_format(date_create($listBlog->publish), SettingBL::getByCode(SettingEnum::DateFormat)), $newContent);
                }
                if (strpos($newContent, '#category_name#') !== false) {
                    $PagePageCategory = PagePageCategory::where('page_id', $listBlog->id)->first(); 
                if (isset($PagePageCategory)){
                    $LanguagePageCategory = LanguagePageCategory::where('page_category_id', $PagePageCategory->page_category_id)->first(); 
                }
                if (isset($LanguagePageCategory)){
                    $newContent = str_replace('#category_name#', $LanguagePageCategory->description,  $newContent); 
                    }else{
                        $newContent = str_replace('#category_name#', '',  $newContent);           
                    }
                }
                
                $finalHtml = $finalHtml . $newContent;
            }

            return $finalHtml;
        }else{
            return '';
        }
    }


}

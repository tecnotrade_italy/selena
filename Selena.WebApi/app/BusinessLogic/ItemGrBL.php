<?php

namespace App\BusinessLogic;

use App\Brand;
use App\BusinessName;
use App\DtoModel\DtoExploded;
use App\DtoModel\DtoGrItemsAgg;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoItemGr;
use App\DtoModel\DtoPlant;
use App\Enums\DbOperationsTypesEnum;
use App\Exploded;
use App\GrItemsAgg;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Item;
use App\ItemGr;
use App\ItemGroupTechnicalSpecification;
use App\Plant;
use App\Tipology;
use Intervention\Image\Facades\Image;
use Exception;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Else_;

//use Mockery\Exception;


class ItemGrBL
{
    public static $dirImage = '../storage/app/public/images/GrItems/';
    private static $mimeTypeAllowed = ['jpeg', 'jpg', 'png', 'gif'];

   #region PRIVATE
    /**
     * Validate data
     * 
     * @param Request ItemGr
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoItemGr, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoItemGr->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (ItemGr::find($DtoItemGr->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

       /* if (is_null($DtoItemGr->code_product)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CodeNotFound), HttpResultsCodesEnum::InvalidPayload);
        }*/

       if ($dbOperationType != DbOperationsTypesEnum::UPDATE) {
        //if (strpos($DtoItems->img, '/storage/app/public/images/Items/') === false) {
            if (strpos($DtoItemGr->img, '/storage/images/GrItems/') === false) {
                if (!in_array(\explode('/', \explode(':', substr($DtoItemGr->img, 0, strpos($DtoItemGr->img, ';')))[1])[1], static::$mimeTypeAllowed)) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FileTypeNotAllowed), HttpResultsCodesEnum::InvalidPayload);
                }
            }
        }
    }

    /**
     * Get all
     * 
     * @return DtoItemGr
     */
    public static function getAll()
    {
        $result = collect();
       // ->order_by('datetime', 'desc')->limit(1);

     /*  $query->select('id')->where('datetime', DB::raw("(select max('datetime') from table)"));
         })->orderBy('id', 'desc')->first();*/
         //where('usual_supplier', 'SI')->
        foreach (ItemGr::orderBy('update_date','DESC')->limit(1)->take(100)->get() as $ItemGr) {  
            
      //  foreach (ItemGr::where('usual_supplier', 'SI')->orderBy('id','DESC')->take(100)->get() as $ItemGr) {   
            
            $Tipology = Tipology::where('id',$ItemGr->tipology)->first();
            $Brand = Brand::where('id',$ItemGr->brand)->first();
            $Businessname = BusinessName::where('id',$ItemGr->business_name_supplier)->first();

                $DtoItemGr = new DtoItemGr();
            
                if (isset($ItemGr->id)) {
                    $DtoItemGr->id = $ItemGr->id;
                }
                if (isset($Tipology->tipology)) {
                    $DtoItemGr->tipology = $Tipology->tipology;
                }else {
                    $DtoItemGr->tipology ='';
                }
                if (isset($ItemGr->note)) {
                    $DtoItemGr->note = $ItemGr->note;
                }else {
                    $DtoItemGr->note ='';
                }
                if (isset($ItemGr->img)) {
                    $DtoItemGr->img = $ItemGr->img;
                }else {
                    $DtoItemGr->img ='';
                }
                if (isset($ItemGr->imageitems)) {
                    $DtoItemGr->imageitems = $ItemGr->imageitems;
                }else {
                    $DtoItemGr->imageitems ='';
                }
                if (isset($ItemGr->original_code)) {
                    $DtoItemGr->original_code = $ItemGr->original_code;
                }else {
                    $DtoItemGr->original_code ='';
                }
                if (isset($ItemGr->code_product)) {
                    $DtoItemGr->code_product = $ItemGr->code_product;
                }else {
                    $DtoItemGr->code_product ='';
                }

                if (isset($ItemGr->code_gr)) {
                    $DtoItemGr->code_gr = $ItemGr->code_gr;
                }else {
                    $DtoItemGr->code_gr ='';
                }
                if (isset($ItemGr->plant)) {
                    $DtoItemGr->plant = $ItemGr->plant;
                }else {
                    $DtoItemGr->plant ='';
                }
                if (isset($Brand->brand)) {
                    $DtoItemGr->brand = $Brand->brand;
                } else {
                    $DtoItemGr->brand ='';
                }
                if (isset($Businessname)) {
                    $DtoItemGr->business_name_supplier = $Businessname->business_name;
                }else {
                    $DtoItemGr->business_name_supplier ='';
                }
                if (isset($ItemGr->code_supplier)) {
                    $DtoItemGr->code_supplier = $ItemGr->code_supplier;
                }else {
                    $DtoItemGr->code_supplier ='';
                }
                if (isset($ItemGr->price_purchase)) {
                    $DtoItemGr->price_purchase = '€ ' . $ItemGr->price_purchase;
                    //'€ ' . str_replace('.',',',substr($ItemGr->price_purchase, 0, -2));
                }else {
                    $DtoItemGr->price_purchase = '';
                }
                if (isset($ItemGr->price_list)) {
                    $DtoItemGr->price_list = '€ ' . $ItemGr->price_list;
                    //'€ ' . str_replace('.',',',substr($ItemGr->price_list, 0, -2));
                }else {
                    $DtoItemGr->price_list = '';
                }
                if (isset($ItemGr->repair_price)) {
                    $DtoItemGr->repair_price = '€ ' . $ItemGr->repair_price;
                    //'€ ' . str_replace('.',',',substr($ItemGr->repair_price, 0, -2));
                }else {
                    $DtoItemGr->repair_price ='';
                }
                if (isset($ItemGr->price_new)) {
                    $DtoItemGr->price_new = '€ ' . $ItemGr->price_new;
                    //'€ ' . str_replace('.',',',substr($ItemGr->price_new, 0, -2));
                }else {
                    $DtoItemGr->price_new ='';
                }
                if (isset($ItemGr->update_date)) {
                    $DtoItemGr->update_date = str_replace('00:00', '', $ItemGr->update_date);
                }else {
                    $DtoItemGr->update_date ='';
                }
                if (isset($ItemGr->usual_supplier)) {
                    $DtoItemGr->usual_supplier = $ItemGr->usual_supplier;
                }else {
                    $DtoItemGr->usual_supplier ='';
                }
                if (isset($ItemGr->pr_rip_conc)) {
                    $DtoItemGr->pr_rip_conc = '€ ' . str_replace('.',',',substr($ItemGr->pr_rip_conc, 0, -2));
                }else {
                    $DtoItemGr->pr_rip_conc ='';
                }

                /*$countOpened = DB::select( 
                    'SELECT count(*) as countopened
                    FROM newsletters_recipients
                    INNER JOIN feedback_newsletters ON newsletters_recipients.id = feedback_newsletters.newsletter_recipient_id
                    WHERE newsletters_recipients.newsletter_id = :idNewsletter
                    AND feedback_newsletters.code = :open
                    group by feedback_newsletters.code',
                    ['idNewsletter' => $newsletter->id, 'open' => FeedbackNewsletterEnum::Open]);*/

                    $queryallcolumn = DB::select(
                        'select count(*) from gr_items');
                    if (isset($queryallcolumn)) {
                        $DtoItemGr->totcolumn = $queryallcolumn;
                    }else {
                        $DtoItemGr->totcolumn ='';
                    }

                    $result->push($DtoItemGr);
                }

                return $result;

                }

    #endregion GET

    private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/GrItems/';
        //return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/Items/';
    }


/**
     * Get getselectitemGr
     *
     * @param String $search
     * @return Producer
     */
    public static function getselectitemGr(string $search){

   if ($search == "*" || $search =='') {
    return collect(DB::select(
        "SELECT distinct gr_items.id as id,  gr_items.code_gr || ' - ' || gr_items.name_product as description
        from gr_items
        where gr_items.code_gr ilike '" . $search . "%' AND gr_items.name_product ilike '" . $search . "%' "));  
     }else{
        /* return collect (DB::select(
             "SELECT distinct id as id, gr_items.code_gr as description
             from gr_items 
             where gr_items.code_gr ilike '" . $search . "%' OR gr_items.name_product ilike '" . $search . "%' "));  */
            return ItemGr::where('code_gr', 'like', $search . '%')->select('id', 'code_gr as description')->get();
    }
}    


public static function getSelectListArticle(string $search)
{
    if ($search == "*") {
        return ItemGr::select('id as id', 'code_gr as description')->all();
    } else {
        return ItemGr::where('code_gr', 'ilike', $search . '%')->select('id as id', 'code_gr as description')->get();
    }
}


#region getselectitemGr
 /**
     * Get select
     *
     * @param String $search
     * @return Intervention
     */
    public static function getSelect(string $search)
    {
    /*    if ($search == "*") {
            return ItemGr::select('id', 'code_gr as description')->get();
        } else {
            return ItemGr::where('code_gr', 'like', $search . '%')
           ->orWhere('name_product','like', $search . '%')
           ->orWhere('original_code','like', $search . '%')
           ->select('gr_items.id as id,  gr_items.code_gr || ' - ' || gr_items.name_product as description')->get();
            //->select('id', 'code_gr as description')->get();
        }*/

        if ($search == "*" || $search=='') {
            return collect(DB::select(
                "SELECT distinct gr_items.id as id,  gr_items.code_gr || ' - ' || gr_items.code_product || ' - ' || gr_items.original_code as description
                from gr_items
                where gr_items.code_gr ilike '" . $search . "%' AND gr_items.code_product ilike '" . $search . "%' AND gr_items.original_code ilike '" . $search . "%' "));  
            }else{
              return collect(DB::select(       
                "SELECT distinct gr_items.id as id,  gr_items.code_gr || ' - ' || gr_items.code_product || ' - ' || gr_items.original_code as description
                from gr_items  
                where gr_items.code_gr ilike '" . $search . "%' OR gr_items.code_product ilike '" . $search . "%' OR gr_items.original_code ilike '" . $search . "%' "));
               /* return ItemGr::where('code_gr', 'ilike', $search . '%')
                ->orWhere('name_product','ilike', $search . '%')
                ->orWhere('original_code','ilike', $search . '%')
                ->select('id', 'original_code as description')->orSelect('id', 'name_product as description')->orSelect('id', 'code_gr as description')->get();*/
            }
    }
 

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoItemGr
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(ItemGr::find($id));
    }

    public static function getProduct(int $id)
    {

        $DtoItemGr = new DtoItemGr();

        $ItemsGr = ItemGr::where('id', $id)->get()->first()->code_gr;

        foreach (ItemGr::where('code_gr', $ItemsGr)->get() as $ItemGr) {
        $DtoItemGrs = new DtoItemGr();

        if (isset($ItemGr->id)) {
            $DtoItemGrs->id = $ItemGr->id;
        }
        if (isset($ItemGr->tipology)) {
            $DtoItemGrs->tipology = $ItemGr->tipology;
            $DtoItemGrs->Tipology = Tipology::where('id',$ItemGr->tipology)->first()->tipology;  
        }
        if (isset($ItemGr->plant_id)) {
            $DtoItemGrs->plant_id = $ItemGr->plant_id;
            $DtoItemGrs->PlantId = Plant::where('id', $ItemGr->plant_id)->first()->description_plant;  
        }
        
        if (isset($ItemGr->note)) {
            $DtoItemGrs->note = $ItemGr->note;
        }else{
            $DtoItemGrs->note = '';
        }
        if (isset($ItemGr->img)) {
            $DtoItemGrs->imgName = $ItemGr->img;
        }
        if (isset($ItemGr->original_code)) {
            $DtoItemGrs->original_code = $ItemGr->original_code;
        }else{
            $DtoItemGrs->original_code = '';
        }
        if (isset($ItemGr->code_product)) {
            $DtoItemGrs->code_product = $ItemGr->code_product;
        }else{
            $DtoItemGrs->code_product = '';
        }
        if (isset($ItemGr->code_gr)) {
            $DtoItemGrs->code_gr = $ItemGr->code_gr;
        }else{
            $DtoItemGrs->code_gr = '';
        }
        if (isset($ItemGr->plant)) {
            $DtoItemGrs->plant = $ItemGr->plant;
        }else{
            $DtoItemGrs->plant = '';
        }
        if (isset($ItemGr->brand)) {
            $DtoItemGrs->brand = $ItemGr->brand;
            $DtoItemGrs->Brand = Brand::where('id',$ItemGr->brand)->first()->brand;  
        }
        if (isset($ItemGr->business_name_supplier)) {
            $DtoItemGrs->business_name_supplier = $ItemGr->business_name_supplier;
            $DtoItemGrs->supplier = BusinessName::where('id',$ItemGr->business_name_supplier)->first()->business_name;
        }

        if (isset($ItemGr->code_supplier)) {
            $DtoItemGrs->code_supplier = $ItemGr->code_supplier;
        }else{
            $DtoItemGrs->code_supplier = '';
        }
        if (isset($ItemGr->price_purchase)) {
            $DtoItemGrs->price_purchase = '€ ' . str_replace('.',',',substr($ItemGr->price_purchase, 0, -2));
        }else{
            $DtoItemGrs->price_purchase = '';
        }
        if (isset($ItemGr->price_list)) {
            $DtoItemGrs->price_list = '€ ' . str_replace('.',',',substr($ItemGr->price_list, 0, -2));
        }else{
            $DtoItemGrs->price_list = '';
        }
        if (isset($ItemGr->repair_price)) {
            $DtoItemGrs->repair_price = '€ ' . str_replace('.',',',substr($ItemGr->repair_price, 0, -2));
        }else{
            $DtoItemGrs->repair_price = '';
        }
        if (isset($ItemGr->price_new)) {
            $DtoItemGrs->price_new = '€ ' . str_replace('.',',',substr($ItemGr->price_new, 0, -2));
        }else{
            $DtoItemGrs->price_new = '';
        }
       
        if (isset($ItemGr->update_date)) {
            $DtoItemGrs->update_date = $ItemGr->update_date;
        }else{
            $DtoItemGrs->update_date = '';
        }
        if (isset($ItemGr->usual_supplier)) {
            $DtoItemGrs->usual_supplier = $ItemGr->usual_supplier;
        }else{
            $DtoItemGrs->usual_supplier = '';
        }
        if (isset($ItemGr->pr_rip_conc)) {
            $DtoItemGrs->pr_rip_conc = '€ ' . str_replace('.',',',substr($ItemGr->pr_rip_conc, 0, -2));
        }else{
            $DtoItemGrs->pr_rip_conc = '';
        }
            $DtoItemGr->AggProduct->push($DtoItemGrs);
        }
            return $DtoItemGr;
    }

    


    public static function getByCmp(int $id)
    {
      
        return static::_convertToDto(ItemGr::find($id));
    }
    

    public static function getBarcodeGrForTypology(int $idTipology)
    {
        $Tipology = Tipology::where('id', $idTipology)->first()->description;

        $DtoItemGr = new DtoItemGr();
        if (isset($Tipology)) {
            $DtoItemGr->tipologyDescription = $Tipology;
        }
        $Plant = Plant::where('description_plant', $Tipology)->get()->first();

        if (isset($Plant)) {
            $DtoPlant = new DtoPlant();
            $DtoPlant->from = $Plant->from;
            $DtoPlant->to = $Plant->to;
        
            $nrmaxcodegr = DB::select(
                'SELECT MAX(gi.code_gr)
                FROM gr_items gi 
                WHERE gi.code_gr >= \''. $DtoPlant->from . '\' and gi.code_gr <= \'' . $DtoPlant->to . '\'');

                if (isset($nrmaxcodegr[0]->max)) {
                    return $nrmaxcodegr[0]->max + 1;
                } else {
                    return '';
                }
        }
    }
 
    public static function getBarcodeGrForPlantId(int $idSelectplant_id)
    {
        $Plant = Plant::where('id', $idSelectplant_id)->first();

        if (isset($Plant)) {

            $DtoPlant = new DtoPlant();
            $DtoPlant->from = $Plant->from;
            $DtoPlant->to = $Plant->to;
        
            $nrmaxcodegr = DB::select(
                'SELECT MAX(gi.code_gr)
                FROM gr_items gi 
                WHERE gi.code_gr >= \''. $DtoPlant->from . '\' and gi.code_gr <= \'' . $DtoPlant->to . '\' AND gi.plant_id = ' . $idSelectplant_id .' ');

                if (isset($nrmaxcodegr[0]->max)) {
                    return $nrmaxcodegr[0]->max + 1;
                } else {
                    return '';
                }
        }
    }
 
    


    /**
     * Get select
     * Convert to dto
     * 
     * @param ItemGr
     * 
     * @return DtoItemGr
     */
    private static function _convertToDto($ItemGr)
    {
        $DtoItemGr = new DtoItemGr();

        if (isset($ItemGr->id)) {
            $DtoItemGr->id = $ItemGr->id;
        }
        if (isset($ItemGr->tipology)) {
            $DtoItemGr->tipology = $ItemGr->tipology;
            $DtoItemGr->Tipology = Tipology::where('id',$ItemGr->tipology)->first()->tipology;  
        }
        if (isset($ItemGr->plant_id)) {
            $DtoItemGr->plant_id = $ItemGr->plant_id;
            $DtoItemGr->PlantId = Plant::where('id', $ItemGr->plant_id)->first()->description_plant;  
        }
        
        if (isset($ItemGr->note)) {
            $DtoItemGr->note = $ItemGr->note;
        }else{
            $DtoItemGr->note = '';
        }
        if (isset($ItemGr->img)) {
            $DtoItemGr->imgName = $ItemGr->img;
        }
        if (isset($ItemGr->original_code)) {
            $DtoItemGr->original_code = $ItemGr->original_code;
        }else{
            $DtoItemGr->original_code = '';
        }
        if (isset($ItemGr->code_product)) {
            $DtoItemGr->code_product = $ItemGr->code_product;
        }else{
            $DtoItemGr->code_product = '';
        }
        if (isset($ItemGr->code_gr)) {
            $DtoItemGr->code_gr = $ItemGr->code_gr;
        }else{
            $DtoItemGr->code_gr = '';
        }
        if (isset($ItemGr->plant)) {
            $DtoItemGr->plant = $ItemGr->plant;
        }else{
            $DtoItemGr->plant = '';
        }
        if (isset($ItemGr->brand)) {
            $DtoItemGr->brand = $ItemGr->brand;
            $DtoItemGr->Brand = Brand::where('id',$ItemGr->brand)->first()->brand;  
        }
        if (isset($ItemGr->business_name_supplier)) {
            $DtoItemGr->business_name_supplier = $ItemGr->business_name_supplier;
            $DtoItemGr->BusinessName = BusinessName::where('id',$ItemGr->business_name_supplier)->first()->business_name;
        }

        if (isset($ItemGr->code_supplier)) {
            $DtoItemGr->code_supplier = $ItemGr->code_supplier;
            //substr($ItemGr->code_supplier, 0, -2);
        }else{
            $DtoItemGr->code_supplier = '';
        }
        if (isset($ItemGr->price_purchase)) {
            $DtoItemGr->price_purchase = '€ ' . str_replace('.',',',substr($ItemGr->price_purchase, 0, -2));
            //substr($ItemGr->price_purchase, 0, -2);
        }else{
            $DtoItemGr->price_purchase = '';
        }
        if (isset($ItemGr->price_list)) {
            $DtoItemGr->price_list = '€ ' . str_replace('.',',',substr($ItemGr->price_list, 0, -2));
            //$ItemGr->price_list;
           // substr($ItemGr->price_list, 0, -2);
        }else{
            $DtoItemGr->price_list = '';
        }
        if (isset($ItemGr->repair_price)) {
            $DtoItemGr->repair_price = '€ ' . str_replace('.',',',substr($ItemGr->repair_price, 0, -2));
            //$ItemGr->repair_price;
            // substr($ItemGr->repair_price, 0, -2);
        }else{
            $DtoItemGr->repair_price = '';
        }
        if (isset($ItemGr->price_new)) {
            $DtoItemGr->price_new = '€ ' . str_replace('.',',',substr($ItemGr->price_new, 0, -2));
            //$ItemGr->price_new;
            // substr($ItemGr->price_new, 0, -2);
        }else{
            $DtoItemGr->price_new = '';
        }
       
        if (isset($ItemGr->update_date)) {
            $DtoItemGr->update_date = $ItemGr->update_date;
        }else{
            $DtoItemGr->update_date = '';
        }
        if (isset($ItemGr->usual_supplier)) {
            $DtoItemGr->usual_supplier = $ItemGr->usual_supplier;
        }else{
            $DtoItemGr->usual_supplier = '';
        }
        if (isset($ItemGr->pr_rip_conc)) {
            $DtoItemGr->pr_rip_conc = '€ ' . str_replace('.',',',substr($ItemGr->pr_rip_conc, 0, -2));
            //$ItemGr->pr_rip_conc;
            //substr($ItemGr->pr_rip_conc, 0, -2);
        }else{
            $DtoItemGr->pr_rip_conc = '';
        }

          $GrItemsAgg = GrItemsAgg::where('id_gritems', $ItemGr->id)->get();
            if (isset($GrItemsAgg)) {
                foreach ($GrItemsAgg as $GrItemsagg) {
                 $DtoGrItemsAgg = new DtoGrItemsAgg();
                 if (isset($GrItemsagg->business_name_supplier)) {
                    $DtoGrItemsAgg->business_name_suppliers = $GrItemsagg->business_name_supplier;
                    $DtoGrItemsAgg->BusinessName = BusinessName::where('id',$GrItemsagg->business_name_supplier)->first()->business_name;
                 }else{
                    $DtoGrItemsAgg->business_name_suppliers = '';
                    $DtoGrItemsAgg->BusinessName = '';
                 }       
                 $DtoGrItemsAgg->code_supplier = $GrItemsagg->code_supplier;
                 $DtoGrItemsAgg->price_purchase = $GrItemsagg->price_purchase;
                 $DtoGrItemsAgg->update_date = $GrItemsagg->update_date;
                 $DtoGrItemsAgg->usual_supplier = $GrItemsagg->usual_supplier;
                 $DtoGrItemsAgg->id_gritems = $GrItemsagg->id_gritems;
                 $DtoItemGr->AggItems->push($DtoGrItemsAgg);  
                }
            }
           
         foreach (Exploded::where('id_items', $ItemGr->id)->get() as $Exploded) {
                if (isset($Exploded->id_items)) {
                    $DtoExploded = new DtoExploded();
                    $DtoExploded->id = $Exploded->id;
                    $DtoExploded->id_items = $Exploded->id_items;
                    $DtoExploded->description = $Exploded->description;
                    $DtoExploded->code_gr = $Exploded->code_gr;
                    $DtoExploded->supplier = $Exploded->supplier;
                    $DtoExploded->code_supplier = $Exploded->code_supplier;
                    $DtoExploded->qta = $Exploded->qta;
                    $DtoExploded->unit_price = $Exploded->unit_price;
                    $DtoExploded->total_price = $Exploded->total_price;

                    $DtoItemGr->AggExploded->push($DtoExploded);
                }                       
            }

        return $DtoItemGr;
    }

   /**
     * Insert
     * 
     * @param Request DtoItemGr
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertNoAuth(Request $DtoItemGr)

    {   
       /* if (strpos($DtoItemGr->img, '/storage/app/public/images/GrItems/') === false) {
            Image::make($DtoItemGr->img)->save(static::$dirImage . $DtoItemGr->imgName);    
        }*/

        $languageId= 1;

        DB::beginTransaction();
        
        try {

            $ItemGr = new ItemGr();

            if (isset($DtoItemGr->tipology)) {
                $ItemGr->tipology = $DtoItemGr->tipology;
            }
            if (isset($DtoItemGr->plant_id)) {
                $ItemGr->plant_id = $DtoItemGr->plant_id;
            }

            if (isset($DtoItemGr->note)) {
                $ItemGr->note = $DtoItemGr->note;
            }   
            if (isset($DtoItemGr->original_code)) {
                $ItemGr->original_code = $DtoItemGr->original_code;
            }  
            if (isset($DtoItemGr->code_product)) {
                $ItemGr->code_product = $DtoItemGr->code_product;
            }  
            if (isset($DtoItemGr->code_gr)) {
                $ItemGr->code_gr = $DtoItemGr->code_gr;
            }  
            if (isset($DtoItemGr->plant)) {
                $ItemGr->plant = $DtoItemGr->plant;
            }  
            if (isset($DtoItemGr->brand)) {
                $ItemGr->brand = $DtoItemGr->brand;
            }  
            if (isset($DtoItemGr->business_name_supplier)) {
                $ItemGr->business_name_supplier = $DtoItemGr->business_name_supplier;
            }  
            if (isset($DtoItemGr->code_supplier)) {
                $ItemGr->code_supplier = $DtoItemGr->code_supplier;
            }  
            if (isset($DtoItemGr->price_purchase)) {
                $ItemGr->price_purchase = $DtoItemGr->price_purchase;
            }  
            if (isset($DtoItemGr->price_list)) {
                $ItemGr->price_list = $DtoItemGr->price_list;
            }  
            if (isset($DtoItemGr->repair_price)) {
                $ItemGr->repair_price = $DtoItemGr->repair_price;
            }  
            if (isset($DtoItemGr->price_new)) {
                $ItemGr->price_new = $DtoItemGr->price_new;
            }  
            if (isset($DtoItemGr->update_date)) {
                $ItemGr->update_date = $DtoItemGr->update_date;
            }  
            if (isset($DtoItemGr->usual_supplier)) {
                $ItemGr->usual_supplier = $DtoItemGr->usual_supplier;
            }  
            if (isset($DtoItemGr->pr_rip_conc)) {
                $ItemGr->pr_rip_conc = $DtoItemGr->pr_rip_conc;
            }     
                $ItemGr->language_id =  $languageId;
                
                if (!is_null($DtoItemGr->imgName)) {
                    Image::make($DtoItemGr->img)->save(static::$dirImage . $DtoItemGr->imgName);
                    $ItemGr->img = static::getUrl() . $DtoItemGr->imgName;
                }

              /*  if (isset($DtoItemGr->img)) {
                    $ItemGr->img = static::getUrl() . $DtoItemGr->imgName;
                }*/

            $ItemGr->save();
        
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $ItemGr->id;

    }

   #region  update

   
   public static function updateExploded(Request $DtoItemGr)
   {

  
    DB::beginTransaction();
    try {
        
            if (!$DtoItemGr['0']['isNew']) {
            $Exploded = Exploded::where('id', $DtoItemGr['0']['id'])->first();
            $Exploded->id = $DtoItemGr['0']['id'];    
            $Exploded->description = $DtoItemGr['0']['description'];
            $Exploded->code_gr = $DtoItemGr['0']['code_gr'];
            $Exploded->supplier = $DtoItemGr['0']['supplier'];
            $Exploded->code_supplier = $DtoItemGr['0']['code_supplier'];
            $Exploded->qta = $DtoItemGr['0']['qta'];
            $Exploded->unit_price = $DtoItemGr['0']['unit_price'];
            $Exploded->total_price = $DtoItemGr['0']['total_price'];
            $Exploded->id_items = $DtoItemGr['0']['id_items'];
            $Exploded->save();
            }else {
                $Exploded = new Exploded();
                if (isset($DtoItemGr['0']['description'])) {
                    $Exploded->description = $DtoItemGr['0']['description'];
                }
                if (isset($DtoItemGr['0']['code_gr'])) {
                    $Exploded->code_gr = $DtoItemGr['0']['code_gr'];
                }
                if (isset($DtoItemGr['0']['supplier'])) {
                    $Exploded->supplier = $DtoItemGr['0']['supplier'];
                }
                if (isset($DtoItemGr['0']['code_supplier'])) {
                    $Exploded->code_supplier = $DtoItemGr['0']['code_supplier'];
                }
                if (isset($DtoItemGr['0']['qta'])) {
                    $Exploded->qta = $DtoItemGr['0']['qta'];
                }
                if (isset($DtoItemGr['0']['unit_price'])) {
                    $Exploded->unit_price = $DtoItemGr['0']['unit_price'];
                }
                if (isset($DtoItemGr['0']['total_price'])) {
                    $Exploded->total_price = $DtoItemGr['0']['total_price'];
                }   
               if (isset($DtoItemGr['0']['id_items'])) {
                    $Exploded->id_items = $DtoItemGr['0']['id_items'];
                }
                $Exploded->save();  
        }

        DB::commit();
    } catch (Exception $ex) {
        DB::rollback();
        throw $ex;
    }

    }


/**
     * Update
     * 
     * @param Request DtoItemGr
     * @param int idLanguage
     */
    public static function update(Request $DtoItemGr)
    {

        static::_validateData($DtoItemGr, $DtoItemGr->idLanguage, DbOperationsTypesEnum::UPDATE);

        $ItemGrOndb= ItemGr::find($DtoItemGr->id);
        
        DB::beginTransaction();
   
        try {

            $ItemGrOndb->id = $DtoItemGr->id;
            $ItemGrOndb->tipology = $DtoItemGr->tipology;

            if (isset($DtoItemGr->plant_id)) {
                $ItemGrOndb->plant_id = $DtoItemGr->plant_id;
            }

            $ItemGrOndb->note = $DtoItemGr->note;

            if (!is_null($DtoItemGr->imgName)) {
                Image::make($DtoItemGr->img)->save(static::$dirImage . $DtoItemGr->imgName);
                $ItemGrOndb->img = static::getUrl() . $DtoItemGr->imgName;
            }
            $ItemGrOndb->original_code = $DtoItemGr->original_code;
            $ItemGrOndb->code_product = $DtoItemGr->code_product;
            $ItemGrOndb->code_gr = $DtoItemGr->code_gr;
            $ItemGrOndb->plant = $DtoItemGr->plant;
            $ItemGrOndb->brand = $DtoItemGr->brand;
            $ItemGrOndb->business_name_supplier = $DtoItemGr->business_name_supplier;
            $ItemGrOndb->code_supplier = $DtoItemGr->code_supplier;
            $ItemGrOndb->price_purchase = $DtoItemGr->price_purchase;
            $ItemGrOndb->repair_price = $DtoItemGr->repair_price;
            $ItemGrOndb->price_new = $DtoItemGr->price_new;
            $ItemGrOndb->price_list = $DtoItemGr->price_list;
            $ItemGrOndb->update_date = $DtoItemGr->update_date;
            $ItemGrOndb->usual_supplier = $DtoItemGr->usual_supplier;
            $ItemGrOndb->pr_rip_conc = $DtoItemGr->pr_rip_conc;
            $ItemGrOndb->save();

          if (isset($DtoItemGr->AggItems)) {
                     $AggItemss = GrItemsAgg::where('id_gritems', $DtoItemGr->id)->get();
                            if (isset($AggItemss)) {
                                GrItemsAgg::where('id_gritems', $DtoItemGr->id)->delete();                            
                            }

                    foreach ($DtoItemGr->AggItems as $AggItems) { 
                    $GrItemsAgg = new GrItemsAgg();  

                    $GrItemsAgg->id_gritems = $DtoItemGr->id;
                    if (isset($AggItems['business_name_suppliers'])) {
                        $GrItemsAgg->business_name_supplier = $AggItems['business_name_suppliers'];
                    }
                     if (isset($AggItems['code_supplier'])) {
                        $GrItemsAgg->code_supplier = $AggItems['code_supplier'];
                    }
                     if (isset($AggItems['price_purchase'])) {
                        $GrItemsAgg->price_purchase = $AggItems['price_purchase'];
                    }
                     if (isset($AggItems['update_date'])) {
                        $GrItemsAgg->update_date = $AggItems['update_date'];
                    }
                     if (isset($AggItems['usual_supplier'])) {
                        $GrItemsAgg->usual_supplier = $AggItems['usual_supplier'];
                    }
                    $GrItemsAgg->save();
                    }
                }
   
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

     #endregion UPDATE

 #region DELETE
    /**
     * Delete
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {   
        $ItemGr = ItemGr::find($id);

        if (is_null($ItemGr)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($ItemGr)) {
            
            DB::beginTransaction();

            try {
                ItemGr::where('id', $id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }  
        }

    }


    public static function deleteExploded(int $id, int $idLanguage)
    {   
        $Exploded = Exploded::find($id);

        if (is_null($Exploded)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($Exploded)) {
            
            DB::beginTransaction();

            try {
                Exploded::where('id', $id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }  
        }
    }
    
    #endregion DELETE
 }
       
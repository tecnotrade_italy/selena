<?php

namespace App\BusinessLogic;

use App\Dictionary;

class DictionaryBL
{
    #region GET

    /**
     * Return a dictionary by language
     *
     * @param Int language_id
     * @return Collection.App\Dictionary
     */
    public static function getByLanguage($language_id)
    {
        //return Dictionary::where('language_id', '=', $language_id)->get();
        return Dictionary::where('language_id', '=', LanguageBL::getDefault()->id)->get();
    }

    /**
     * Return translation of message
     *
     * @param int language_id
     * @param string code message
     * @return string
     */
    public static function getTranslate($language_id, $code)
    {
        $message = Dictionary::where('language_id', '=', $language_id)->where('code', '=', $code)->first();

        if ($message == null) {
            $message = Dictionary::where('language_id', '=', LanguageBL::getDefault()->id)->where('code', '=', $code)->first();
        }

        if ($message != null) {
            return $message->description;
        } else {
            return null;
        }
    }

    #endregion GET
}

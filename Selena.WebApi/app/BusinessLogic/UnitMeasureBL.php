<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoUnitMeasure;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\LogHelper;
use App\LanguageUnitMeasure;
use App\UnitMeasure;
use App\User;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Http\Request;

class UnitMeasureBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request DtoUnitMeasure
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoUnitMeasure, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoUnitMeasure->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (UnitMeasure::find($DtoUnitMeasure->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($DtoUnitMeasure->code)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CompanyNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoUnitMeasure->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CompanyNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    /**
     * Convert to dto
     * 
     * @param UnitMeasure postalCode
     * 
     * @return DtoUnitMeasure
     */
    private static function _convertToDto($UnitMeasure)
    {
        $DtoUnitMeasure = new DtoUnitMeasure();

        if (isset($UnitMeasure->id)) {
            $DtoUnitMeasure->id = $UnitMeasure->id;
        }

        if (isset($UnitMeasure->code)) {
            $DtoUnitMeasure->code = $UnitMeasure->code;
        }
        if (isset($UnitMeasure->description)) {
            $DtoUnitMeasure->description = $UnitMeasure->description;
        }

        return  $DtoUnitMeasure;
    }


    /**
     * Convert to VatType
     * 
     * @param DtoUnitMeasure dtoVatType
     * 
     * @return UnitMeasure
     */
    private static function _convertToModel($DtoUnitMeasure)
    {
        $UnitMeasure = new UnitMeasure();

        if (isset($DtoUnitMeasure->id)) {
            $UnitMeasure->id = $DtoUnitMeasure->id;
        }

        if (isset($DtoUnitMeasure->code)) {
            $UnitMeasure->code = $DtoUnitMeasure->code;
        }

        if (isset($DtoUnitMeasure->description)) {
            $UnitMeasure->description = $DtoUnitMeasure->description;
        }

        if (isset($DtoUnitMeasure->language_id)) {
            $UnitMeasure->language_id = $DtoUnitMeasure->language_id;
        }

        return $UnitMeasure;
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoUnitMeasure
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(UnitMeasure::find($id));
    }

    /**
     * Get select
     *
     * @param String $search
     * @return UnitMeasure
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return UnitMeasure::select('id', 'code as description')->get();
        } else {
            return UnitMeasure::where('code', 'ilike', $search . '%')->select('id', 'code as description')->get();
        }
    }
    
    /**
     * Get all
     * 
     * @return DtoUnitMeasure
     */
    public static function getAll($idLanguage)
    {
        $result = collect(); 
        foreach (LanguageUnitMeasure::where('language_id',$idLanguage)->get() as $nationAll) {
            $DtoUnitMeasure = new DtoUnitMeasure();
            $DtoUnitMeasure->id = UnitMeasure::where('id', $nationAll->unit_of_measure_id)->first()->id;
            $DtoUnitMeasure->code = UnitMeasure::where('id', $nationAll->unit_of_measure_id)->first()->code;
            $DtoUnitMeasure->description = $nationAll->description;
            $result->push($DtoUnitMeasure); 
        }
        return $result;
    }
    

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoUnitMeasure
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoUnitMeasure, int $idUser)
    {
        
        static::_validateData($DtoUnitMeasure, $DtoUnitMeasure->idLanguage, DbOperationsTypesEnum::INSERT);
        DB::beginTransaction();

        try {
            $UnitMeasure = new UnitMeasure();
            //$UnitMeasure->id = $DtoUnitMeasure->id;
            $UnitMeasure->code = $DtoUnitMeasure->code;
            $UnitMeasure->created_id = $DtoUnitMeasure->$idUser;
            $UnitMeasure->save();

            $LanguageUnitMeasure = new LanguageUnitMeasure();
            $LanguageUnitMeasure->language_id =$DtoUnitMeasure->idLanguage;
            $LanguageUnitMeasure->unit_of_measure_id = $UnitMeasure->id;
            $LanguageUnitMeasure->description = $DtoUnitMeasure->description;
            $LanguageUnitMeasure->created_id = $DtoUnitMeasure->$idUser;
            $LanguageUnitMeasure->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $UnitMeasure->id;
    }


       // $customer = static::_convertToModel($DtoUnitMeasure);
       // $customer->save();
        //return $customer->id;

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoUnitMeasure
     * @param int idLanguage
     */

    public static function update(Request $DtoUnitMeasure)

    {
        static::_validateData($DtoUnitMeasure, $DtoUnitMeasure->idLanguage, DbOperationsTypesEnum::UPDATE);
        $customerOnDb = UnitMeasure::find($DtoUnitMeasure->id);

        //DBHelper::updateAndSave(static::_convertToModel($DtoUnitMeasure), $customerOnDb);

        $languageNationOnDb = LanguageUnitMeasure::where('unit_of_measure_id', $DtoUnitMeasure->id, $DtoUnitMeasure->idLanguage)->first();
        
        DB::beginTransaction();

        try {
            $customerOnDb->id = $DtoUnitMeasure->id;
            $customerOnDb->code = $DtoUnitMeasure->code;
            $customerOnDb->save();


            //$languageNationOnDb->language_id = $idLanguage;
            $languageNationOnDb->language_id = $DtoUnitMeasure->idLanguage;
            $languageNationOnDb->unit_of_measure_id = $DtoUnitMeasure->id;
            $languageNationOnDb->description = $DtoUnitMeasure->description;
            $languageNationOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
    
  #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */

    public static function delete(int $id)
    {
        $LanguageUnitMeasure= LanguageUnitMeasure::where('unit_of_measure_id', $id)->first();

        if (is_null($LanguageUnitMeasure)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $LanguageUnitMeasure->delete();

        $UnitMeasure = UnitMeasure::find($id);

        if (is_null($UnitMeasure)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $UnitMeasure->delete();
    }

    #endregion DELETE
}
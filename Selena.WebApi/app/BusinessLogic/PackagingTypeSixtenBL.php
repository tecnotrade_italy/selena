<?php

namespace App\BusinessLogic;

use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\Helpers\LogHelper;
use App\PackagingTypeSixten;
use Mockery\Exception;

class PackagingTypeSixtenBL
{
    #region PRIVATE

    /**
     * Check function enabled
     * 
     * @param int idFunctionlity
     * @param int idUser
     * @param int idLanguage
     */
    private static function _checkFunctionEnabled(int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (is_null($idFunctionality)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Functionality::find($idFunctionality))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get select
     *
     * @param string $search
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoSelect2
     */
    public static function getSelect(string $search, int $idFunctionality, $idUser, $idLanguage)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        return PackagingTypeSixten::where('description', 'ilike', $search . '%')->select('id', 'description')->get();
    }

    #endregion GET
}

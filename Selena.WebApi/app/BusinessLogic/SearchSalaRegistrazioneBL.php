<?php

namespace App\BusinessLogic;

use App\Content;
use App\ContentLanguage;
use App\DtoModel\DtoNation;
use App\DtoModel\DtoSelenaViews;
use App\DtoModel\DtoTagTemplate;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\SelenaSqlViews;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchSalaRegistrazioneBL
{
 
  /**
     * Search implemented
     * 
     * @param Request request
     * 
     */
    public static function searchImplemented(Request $request)
    {
        $content = '';
        $finalHtml = '';
        $strWhere = "";
        $idLanguageContent = $request->idLanguage; 
        $contentCode = Content::where('code', 'UsersDetail')->first();
        $contentLanguage = ContentLanguage::where('content_id', $contentCode->id)->where('language_id', $idLanguageContent)->first();
    
        foreach($request->request as $key => $value){
            if($request->{$key} != ""){
                if($key != 'idLanguage'){
                    $operator = ' = ';
                    $val = $request->{$key};
                    $keyNew = $key;
                        if($key == 'business_name'){
                            $keyNew = 'users_datas.business_name';
                            $operator = ' ilike ';
                            $val = '\'%' . $request->{$key} . '%\'';
                        }else{
                            if($key == 'surname'){
                                $keyNew = 'users_datas.surname';
                                $operator = ' ilike ';
                                $val = '\'%' . $request->{$key} . '%\'';
                            }else{
                                if($key == 'address'){
                                    $keyNew = 'users_datas.address';
                                    $operator = ' ilike ';
                                    $val = '\'%' . $request->{$key} . '%\'';
                                }else{
                                    if($key == 'country'){
                                        $keyNew = 'users_datas.country';
                                        $operator = ' ilike ';
                                        $val = '\'%' . $request->{$key} . '%\'';
                                    }else{
                                        if($key == 'province'){
                                            $keyNew = 'users_datas.province';
                                            $operator = ' ilike ';
                                            $val = '\'%' . $request->{$key} . '%\'';
                        }else{
                            if($key == 'name'){
                                $keyNew = 'users_datas.name';
                                $operator = ' ilike ';
                                $val = '\'%' . $request->{$key} . '%\'';
                            }else{
                                $operator = ' = ';
                                $val = $request->{$key};
                            }
                        }
                    }
                }
            }
                        }
                        if($request->{$key} == "on"){
                            $keyNew = $key;
                            $val = "true";
                        }
                        $strWhere = $strWhere . ' AND ' . $keyNew . $operator . $val;
                    }
                    
                }
            }
    
        $tags = SelenaViewsBL::getTagByType('UserSala');
        foreach (DB::select(
            'SELECT users.*, users_datas.*
            FROM users
            INNER JOIN users_datas ON users.id = users_datas.user_id
            INNER JOIN role_user ON role_user.user_id = users_datas.user_id
            INNER JOIN roles ON roles.id = role_user.role_id
            AND roles.name = \'sala\'
            AND users_datas.language_id = :idLanguage ' . $strWhere,
            ['idLanguage' => $idLanguageContent]
        ) as $users){
            $newContent = $contentLanguage->content;
            foreach ($tags as $fieldsDb){      
                if(isset($fieldsDb->tag)){
                    if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                        if(is_numeric($users->{$cleanTag})){
                            $tag = str_replace('.',',',substr($users->{$cleanTag}, 0, -2));
                        }else{
                            $tag = $users->{$cleanTag};
                        }
                        $newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                    }
                }
            }
            $finalHtml = $finalHtml . $newContent;
        };
        $content = $finalHtml;
        if($content == ""){
            $content = '<div class="col-12 div-no-info"><h1>Nessuna informazione trovata</h1></div>';
        }
        return $content;
}

#endregion INSERT

#region UPDATE

#endregion UPDATE
#endregion DELETE

}
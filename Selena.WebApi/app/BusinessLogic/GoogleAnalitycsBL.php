<?php

namespace App\BusinessLogic;

use App\Customer;
use App\Enums\DictionariesCodesEnum;
use App\Enums\DocumentTypeEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Analytics;
use Spatie\Analytics\Period;
use Carbon\Carbon;
use App\GoogleAnalyticsAccounts;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use App\BusinessLogic\SettingBL;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\DtoModel\DtoChartJS;
use App\DtoModel\DtoChartJSDatasets;
use App\Helpers\HttpHelper;
use App\Enums\SettingEnum;

class GoogleAnalitycsBL
{
    protected static function getActiveAccount($idLanguage)
    {
        $viewIDActive = Setting::where('code', SettingEnum::GoogleAnalyticsViewId)->first();
        $return = '';
        if (empty($viewIDActive)) {
            $return = '';
            //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ViewNotSet), HttpResultsCodesEnum::InvalidPayload);
        }else{
            $return = $viewIDActive->value;
        }

        return $return;
    }

    protected static function secondMinute($seconds)
    {
        $minutes = floor($seconds / 60);
        $secondsleft = $seconds % 60;

        if ($minutes < 10) {
            $minutes = "0" . $minutes;
        }

        if ($secondsleft < 10) {
            $secondsleft = "0" . $secondsleft;
        }

        return "$minutes:$secondsleft";
    }

    protected static function createPeriod($startDate, $endDate)
    {
        if (empty($startDate)) {
            $startDate = Carbon::now()->subDays(30);
        } else {
            $startDate = Carbon::parse($startDate);
        }

        if (empty($endDate)) {
            $endDate = Carbon::now();
        } else {
            $endDate = Carbon::parse($endDate);
        }

        return $period = Period::create($startDate, $endDate);
    }

    private static function _validateData(Request $request, int $idLanguage)
    {
        if (is_null($request->description)) {
            sleep(1);
            //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($request->view_id)) {
            sleep(1);
            //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ViewNotSet), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    public static function getAccounts()
    {
        return GoogleAnalyticsAccounts::orderBy('description')->get();
    }

    public static function getTotalUsers($startDate, $endDate, int $idLanguage)
    {
        $vewId = self::getActiveAccount($idLanguage);
        $return = '';
        if(empty($vewId)){
            $return = ''; 
        }else{
            Analytics::setViewId($vewId);
            $analyticsData = Analytics::performQuery(self::createPeriod($startDate, $endDate), 'ga:users', ["segment" => "gaid::-1"]);
            $return = $analyticsData->totalsForAllResults['ga:users'];
        }
        return $return;
    }

    public static function getTotalPageView($startDate, $endDate, int $idLanguage)
    {
        Analytics::setViewId(self::getActiveAccount($idLanguage));

        $analyticsData = Analytics::performQuery(self::createPeriod($startDate, $endDate), 'ga:pageviews', ["segment" => "gaid::-1"]);

        return $analyticsData->totalsForAllResults['ga:pageviews'];
    }

    public static function getTotalSession($startDate, $endDate, int $idLanguage)
    {
        Analytics::setViewId(self::getActiveAccount($idLanguage));

        $analyticsData = Analytics::performQuery(self::createPeriod($startDate, $endDate), 'ga:sessions');

        return $analyticsData->totalsForAllResults['ga:sessions'];
    }

    public static function getPageViewPerSession($startDate, $endDate, int $idLanguage)
    {

        Analytics::setViewId(self::getActiveAccount($idLanguage));


        $analyticsData = Analytics::performQuery(self::createPeriod($startDate, $endDate), 'ga:pageviewsPerSession');


        return number_format((float) $analyticsData->totalsForAllResults['ga:pageviewsPerSession'], 2);
    }

    public static function getAvgSessionDuration($startDate, $endDate, int $idLanguage)
    {

        Analytics::setViewId(self::getActiveAccount($idLanguage));

        $analyticsData = Analytics::performQuery(self::createPeriod($startDate, $endDate), 'ga:avgSessionDuration');

        $seconds = $analyticsData->totalsForAllResults['ga:avgSessionDuration'];

        return self::secondMinute($seconds);
    }

    public static function getBounceRate($startDate, $endDate, int $idLanguage)
    {

        Analytics::setViewId(self::getActiveAccount($idLanguage));

        $analyticsData = Analytics::performQuery(self::createPeriod($startDate, $endDate), 'ga:bounceRate');

        return number_format((float) $analyticsData->totalsForAllResults['ga:bounceRate'], 2);
    }

    public static function getVisitorsViews($startDate, $endDate, int $idLanguage)
    {

        Analytics::setViewId(self::getActiveAccount($idLanguage));

        $analyticsData = Analytics::fetchVisitorsAndPageViews(self::createPeriod($startDate, $endDate));

        $labels = [];

        $resultAnalytic = [];

        foreach ($analyticsData as $data) {

            $labels[] = $data['date']->format('d-m');

            $resultAnalytic[] = (int) $data['visitors'];
        }

        $result = new DtoChartJS();

        $result->labels = $labels;

        $datasets = new DtoChartJSDatasets();

        $datasets->backgroundColor = SettingBL::getRenderBackgroundColorChart();
        if (is_null($datasets->backgroundColor)) {
            sleep(1);
            //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::BackGroundEmpty), HttpResultsCodesEnum::InvalidPayload);
        }
        $datasets->borderColor = SettingBL::getRenderBorderColorChart();
        if (is_null($datasets->borderColor)) {
            sleep(1);
            //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::BorderEmpty), HttpResultsCodesEnum::InvalidPayload);
        }
        $datasets->data = $resultAnalytic;

        $result->datasets = ($datasets);
        return $result;
    }

    public static function getPagesViews($startDate, $endDate, int $idLanguage)
    {

        Analytics::setViewId(self::getActiveAccount($idLanguage));

        $analyticsData = Analytics::fetchVisitorsAndPageViews(self::createPeriod($startDate, $endDate));

        $labels = [];

        $resultAnalytic = [];

        foreach ($analyticsData as $data) {

            $labels[] = $data['date']->format('d-m');

            $resultAnalytic[] = (int) $data['pageViews'];
        }

        $result = new DtoChartJS();

        $result->labels = $labels;

        $datasets = new DtoChartJSDatasets();

        $datasets->backgroundColor = SettingBL::getRenderBackgroundColorChart();

        if (is_null($datasets->backgroundColor)) {
            sleep(1);
            //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::BackGroundEmpty), HttpResultsCodesEnum::InvalidPayload);
        }

        $datasets->borderColor = SettingBL::getRenderBorderColorChart();

        if (is_null($datasets->borderColor)) {
            sleep(1);
            //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::BorderEmpty), HttpResultsCodesEnum::InvalidPayload);
        }

        $datasets->data = $resultAnalytic;

        $result->datasets = ($datasets);
        return $result;
    }

    public static function getMostVisitedPages($startDate, $endDate, $topType, int $idLanguage)
    {
        Analytics::setViewId(self::getActiveAccount($idLanguage));
        if (is_null($topType)) {
            $topType = 10;
        }
        $analyticsData = Analytics::fetchMostVisitedPages(self::createPeriod($startDate, $endDate), (int) $topType);
        $list = [];
        foreach ($analyticsData as $data) {

            $list[] = ['url' => $data['url'], 'pageview' => $data['pageViews']];
        }
        return $list;
    }

    public static function getUserActiveRealTime(int $idLanguage)
    {
        Analytics::setViewId(self::getActiveAccount($idLanguage));

        $analyticsData = Analytics::getAnalyticsService()->data_realtime
            ->get('ga:' . self::getActiveAccount($idLanguage), 'rt:activeVisitors')
            ->totalsForAllResults['rt:activeVisitors'];
        return $analyticsData;
    }

    public static function getDeviceSession($startDate, $endDate, int $idLanguage)
    {

        Analytics::setViewId(self::getActiveAccount($idLanguage));

        $analyticsData = Analytics::performQuery(self::createPeriod($startDate, $endDate), 'ga:sessionDuration', ["dimensions" => "ga:deviceCategory"]);

        $labels = [];

        $resultAnalytic = [];

        foreach ($analyticsData as $data) {

            $labels[] = $data['0'];

            $resultAnalytic[] = (int) $data['1'];
        }

        $result = new DtoChartJS();

        $result->labels = $labels;

        $datasets = new DtoChartJSDatasets();

        $datasets->backgroundColor = SettingBL::getRenderBackgroundColorPieChart();

        if (is_null($datasets->backgroundColor)) {
            sleep(1);
            //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::BackGroundEmpty), HttpResultsCodesEnum::InvalidPayload);
        }


        $datasets->data = $resultAnalytic;

        $result->datasets = ($datasets);
        return $result;
    }

    public static function getSourceSession($startDate, $endDate, int $idLanguage)
    {

        Analytics::setViewId(self::getActiveAccount($idLanguage));

        $analyticsData = Analytics::performQuery(self::createPeriod($startDate, $endDate), 'ga:sessionDuration', ["dimensions" => "ga:source"]);

        $labels = [];

        $resultAnalytic = [];

        foreach ($analyticsData as $data) {

            $labels[] = $data['0'];

            $resultAnalytic[] = (int) $data['1'];
        }

        $result = new DtoChartJS();

        $result->labels = $labels;

        $datasets = new DtoChartJSDatasets();

        $datasets->backgroundColor = SettingBL::getRenderBackgroundColorPieChart();

        if (is_null($datasets->backgroundColor)) {
            sleep(1);
            //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::BackGroundEmpty), HttpResultsCodesEnum::InvalidPayload);
        }

        $datasets->data = $resultAnalytic;

        $result->datasets = ($datasets);
        return $result;
    }

    public static function insertAccount(Request $request, $idLanguage)
    {
        static::_validateData($request, $idLanguage);

        return GoogleAnalyticsAccounts::create([
            'view_id' => $request->view_id,
            'description' => $request->description
        ]);
    }

    public static function storeJsonKey($jsonFile)
    {
        Storage::disk('local')->put('analytics/service-account-credentials.json', $jsonFile);
        return true;
    }

    public static function setAccount(int $viewID, $idLanguage)
    {
        $GAAccount = GoogleAnalyticsAccounts::find($viewID);

        if (!$GAAccount) {
            //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GoogleAnalyticsViewNotFound), HttpResultsCodesEnum::BadRequest);
        }

        DB::beginTransaction();

        try {

            SettingBL::insertOrUpdate(SettingEnum::GoogleAnalyticsViewId, $GAAccount->view_id, $GAAccount->created_id);

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return true;
    }

    public static function deleteAccount(int $viewID, $idLanguage)
    {
        $GAAccount = GoogleAnalyticsAccounts::find($viewID);

        if (!$GAAccount) {
            //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GoogleAnalyticsViewNotFound), HttpResultsCodesEnum::BadRequest);
        }
        DB::beginTransaction();

        try {

            $setting = Setting::where('code', SettingEnum::GoogleAnalyticsViewId)->where('created_id', $GAAccount->created_id)->first();

            SettingBL::delete($setting->id, $idLanguage);

            $GAAccount->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return true;
    }
}

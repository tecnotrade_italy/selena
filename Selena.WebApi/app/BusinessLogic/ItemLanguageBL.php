<?php

namespace App\BusinessLogic;

use App\CategoryItem;
use App\Helpers\LogHelper;
use App\ItemLanguage;

class ItemLanguageBL
{

    #region GET

    /**
     * Get by item and language
     * 
     * @param int $idItem
     * @param int $idLanguage
     * 
     * @return string
     */
    public static function getByItemAndLanguage(int $idItem, int $idLanguage)
    {
        return ItemLanguage::where('item_id', $idItem)->where('language_id', $idLanguage)->first()->description;
    }

    public static function deleteByItemLanguage(int $idItem){
        ItemLanguage::where('item_id', $idItem)->delete();
    }
    
    
    public static function deleteByIdCategoriesItems(int $idCategories){
        CategoryItem::where('item_id', $idCategories)->delete();
    }

    #endregion GET
}

<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoTemplateEditorGroup;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\TemplateEditorGroup;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\LogHelper;

class TemplateEditorGroupBL
{
    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationsTypesEnum
     */
    private static function _validateData(Request $request, int $idLanguage,  $dbOperationsTypesEnum)
    {
        if (is_null($request->code)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CodeNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->idTemplateEditorType) || is_null(TemplateEditorTypeBL::get($request->idTemplateEditorType))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TemplateEditorTypeNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if ($dbOperationsTypesEnum == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (is_null(TemplateEditorGroup::find($request->id))) {

                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TemplateEditorGroupNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get
     *
     * @param int id
     *
     * @return TemplateEditorGroup
     */
    public static function get(int $id)
    {
        return TemplateEditorGroup::find($id);
    }

    /**
     * Get dto
     *
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     *
     * @return DtoTemplateEditorGroup
     */
    public static function getDto(int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $result = collect();

        foreach (TemplateEditorGroup::all() as $templateEditorGroup) {
            $dtoTemplateEditorGroup = new DtoTemplateEditorGroup();
            $dtoTemplateEditorGroup->id = $templateEditorGroup->id;
            $dtoTemplateEditorGroup->code = $templateEditorGroup->code;
            $dtoTemplateEditorGroup->description = $templateEditorGroup->description;
            $dtoTemplateEditorGroup->idTemplateEditorType = $templateEditorGroup->template_editor_type_id;
            $dtoTemplateEditorGroup->templateeditortypeDescription = TemplateEditorTypeBL::get($templateEditorGroup->template_editor_type_id)->description;
            $dtoTemplateEditorGroup->order = $templateEditorGroup->order;
            $result->push($dtoTemplateEditorGroup);
        }

        return $result;
    }

    /**
     * Get select
     *
     * @param string search
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     *
     * @return DtoSelect2
     */
    public static function getSelect(string $search, int $idFunctionality, int $idUser, $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        if ($search == "*") {
            return TemplateEditorGroup::select('id', 'description')->orderBy('order')->get();
        } else {
            return TemplateEditorGroup::where('description', 'ilike', $search . '%')->select('id', 'description')->orderBy('order')->get();
        }
    }


    /**
     * Get Template
     *
     * @param String type
     * @param int idUser
     * @param int idLanguage
     *
     * @return DtoTemplateEditorGroup
     */
    public static function selectGroupTemplate(String $type, String $search, int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
        $code = TemplateEditorTypeBL::getByCode($type);
        if (is_null($code)) {
            return;
        } else {
            return TemplateEditorGroup::where('description', 'ILIKE', $search . '%')->where('template_editor_type_id', $code->id)->select('id', 'description')->orderBy('order')->get();
        }
    }


    #endregion GET

    #region INSERT

    /**
     * Insert
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($request->idFunctionality, $idUser, $idLanguage);
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        $templateEditorGroup = new TemplateEditorGroup();
        $templateEditorGroup->code = $request->code;
        $templateEditorGroup->description = $request->description;
        $templateEditorGroup->template_editor_type_id = $request->idTemplateEditorType;
        $templateEditorGroup->order = $request->order;
        $templateEditorGroup->save();

        return $templateEditorGroup->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     */
    public static function update(Request $request, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($request->idFunctionality, $idUser, $idLanguage);
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::UPDATE);

        $templateEditorGroup = TemplateEditorGroup::find($request->id);
        $templateEditorGroup->code = $request->code;
        $templateEditorGroup->description = $request->description;
        $templateEditorGroup->template_editor_type_id = $request->idTemplateEditorType;
        $templateEditorGroup->order = $request->order;
        $templateEditorGroup->save();
    }

    #endregion UPDATE

    public static function clone(int $id, int $idFunctionality, int $idUser, $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $templateEditorGroupToClone = TemplateEditorGroup::find($id);
        if (is_null($templateEditorGroupToClone)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PageNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        DB::beginTransaction();

        try {
            $TemplateEditorGroup = new TemplateEditorGroup();
            $TemplateEditorGroup->code = $templateEditorGroupToClone->code . ' - Copy';
            $TemplateEditorGroup->description = $templateEditorGroupToClone->description . ' - Copy';
            $TemplateEditorGroup->template_editor_type_id = $templateEditorGroupToClone->template_editor_type_id;
            $TemplateEditorGroup->order = $templateEditorGroupToClone->order;
            $TemplateEditorGroup->save();
            
            DB::commit();

            return $TemplateEditorGroup->id;
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }



    #region DELETE
    /**
     * Delete
     *
     * @param int id
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function delete(int $id, int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        if (is_null(TemplateEditorGroup::find($id))) {

            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TemplateEditorGroupNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $templateEditorGroup = TemplateEditorGroup::find($id);
        $templateEditorGroup->delete();
    }

    #endregion DELETE
}

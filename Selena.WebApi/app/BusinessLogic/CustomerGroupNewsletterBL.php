<?php

namespace App\BusinessLogic;

use App\CustomerGroupNewsletter;
use App\DtoModel\DtoGroupNewsletter;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerGroupNewsletterBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request dtoContactGroupNewsletter
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $dtoContactGroupNewsletter, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($dtoContactGroupNewsletter->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (CustomerGroupNewsletter::find($dtoContactGroupNewsletter->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactGroupNewsletterNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($dtoContactGroupNewsletter->idContact)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(CustomerBL::getById($dtoContactGroupNewsletter->idContact))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($dtoContactGroupNewsletter->idGroupNewsletter)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupNewsletterNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(GroupNewsletterBL::getById($dtoContactGroupNewsletter->idGroupNewsletter))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupNewsletterNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get included by contact
     * 
     * @param int idContact
     * 
     * @return DtoContactGroupNewsletter
     */
    public static function getIncludedByCustomer(int $idContact)
    {
        $result = collect();

        foreach (DB::select(
            '   SELECT cgn.id, gn.description
                FROM customers_groups_newsletters cgn
                INNER JOIN groups_newsletters gn ON cgn.group_newsletter_id = gn.id
                WHERE cgn.customer_id = :idContact',
            ['idContact' => $idContact]
        ) as $groupNewsletter) {
            $dtoGroupNewsletter = new DtoGroupNewsletter();
            $dtoGroupNewsletter->id = $groupNewsletter->id;
            $dtoGroupNewsletter->description = $groupNewsletter->description;
            $result->push($dtoGroupNewsletter);
        }

        return $result;
    }

    /**
     * Get excluded by contact
     * 
     * @param int idContact
     * 
     * @return DtoContactGroupNewsletter
     */
    public static function getExcludedByCustomer(int $idContact)
    {
        $result = collect();

        foreach (DB::select(
            '   SELECT id, description
                FROM groups_newsletters
                WHERE id NOT IN (SELECT group_newsletter_id FROM customers_groups_newsletters WHERE customer_id = :idContact)
                AND send_newsletter = true
                AND error_newsletter = false',
            ['idContact' => $idContact]
        ) as $groupNewsletter) {
            $dtoGroupNewsletter = new DtoGroupNewsletter();
            $dtoGroupNewsletter->id = $groupNewsletter->id;
            $dtoGroupNewsletter->description = $groupNewsletter->description;
            $result->push($dtoGroupNewsletter);
        }

        return $result;
    }


    /**
     * Get included by contact
     * 
     * @param int idContact
     * 
     * @return DtoContactGroupNewsletter
     */
    public static function getIncludedByCustomerGroup(int $idContact)
    {
        $result = collect();

        foreach (DB::select(
            '   SELECT cgn.id, c.name, c.surname, u.email
            FROM customers_groups_newsletters cgn
            INNER JOIN users u ON cgn.customer_id = u.id
            INNER JOIN users_datas c ON c.user_id = u.id
            WHERE cgn.group_newsletter_id = :idContact',
            ['idContact' => $idContact]
        ) as $groupNewsletter) {
            $dtoGroupNewsletter = new DtoGroupNewsletter();
            $dtoGroupNewsletter->id = $groupNewsletter->id;
            $dtoGroupNewsletter->name = $groupNewsletter->name;
            $dtoGroupNewsletter->surname = $groupNewsletter->surname;
            $dtoGroupNewsletter->email = $groupNewsletter->email;
            $result->push($dtoGroupNewsletter);
        }

        return $result;
    }

    /**
     * Get excluded by contact
     * 
     * @param int idContact
     * 
     * @return DtoContactGroupNewsletter
     */
    public static function getExcludedByCustomerGroup(int $idContact)
    {
        $result = collect();

        foreach (DB::select(
            '   SELECT u.id, c.name, c.surname, u.email 
            FROM users u
            inner join users_datas c on c.user_id = u.id WHERE u.id NOT IN (SELECT customer_id FROM customers_groups_newsletters WHERE group_newsletter_id = :idContact)',
            ['idContact' => $idContact]
        ) as $groupNewsletter) {
            $dtoGroupNewsletter = new DtoGroupNewsletter();
            $dtoGroupNewsletter->id = $groupNewsletter->id;
            $dtoGroupNewsletter->name = $groupNewsletter->name;
            $dtoGroupNewsletter->surname = $groupNewsletter->surname;
            $dtoGroupNewsletter->email = $groupNewsletter->email;
            $result->push($dtoGroupNewsletter);
        }

        return $result;
    }



    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request dtoContactGroupNewsletter
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $dtoContactGroupNewsletter, int $idLanguage)
    {
        static::_validateData($dtoContactGroupNewsletter, $idLanguage, DbOperationsTypesEnum::INSERT);
        $contactGroupNewsletter = new CustomerGroupNewsletter();
        $contactGroupNewsletter->customer_id = $dtoContactGroupNewsletter->idContact;
        $contactGroupNewsletter->group_newsletter_id = $dtoContactGroupNewsletter->idGroupNewsletter;
        $contactGroupNewsletter->save();
        return $contactGroupNewsletter->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoContactGroupNewsletter
     * @param int idLanguage
     * 
     * @return int
     */
    public static function update(Request $dtoContactGroupNewsletter, int $idLanguage)
    {
        static::_validateData($dtoContactGroupNewsletter, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $contactGroupNewsletter = CustomerGroupNewsletter::find($dtoContactGroupNewsletter->id);
        $contactGroupNewsletter->customer_id = $dtoContactGroupNewsletter->idContact;
        $contactGroupNewsletter->group_newsletter_id = $dtoContactGroupNewsletter->idGroupNewsletter;
        $contactGroupNewsletter->save();
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Update
     * 
     * @param int id
     * @param int idLanguage
     * 
     * @return int
     */
    public static function delete(int $id, int $idLanguage)
    {
        $contactGroupNewsletter = CustomerGroupNewsletter::find($id);

        if (is_null($contactGroupNewsletter)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactGroupNewsletterNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $contactGroupNewsletter->delete();
    }

    #endregion DELETE
}

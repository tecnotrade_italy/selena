<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoTypologyKeywords;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;

use App\TypologyKeywords;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TypologyKeywordsBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (TypologyKeywords::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoOptional
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (DB::table('typology_keywords')
                ->select('*')
                ->orderBy('typology_keywords.id')
                ->get()
        as $typologyKeywordsAll) {
            $dtoTypologyKeywords = new DtoTypologyKeywords();
            $dtoTypologyKeywords->id = $typologyKeywordsAll->id;
            $dtoTypologyKeywords->description = $typologyKeywordsAll->description;
            $dtoTypologyKeywords->html = $typologyKeywordsAll->html;
            $result->push($dtoTypologyKeywords); 
        }
        
        return $result;
    }

    /**
     * Get select
     *
     * @param String $search
     * @return TypologyKeywords
     */
    public static function getAllSelect(string $search)
    {
        if ($search == "*") {
            return TypologyKeywords::select('typology_keywords.id', 'typology_keywords.description')->orderBy('typology_keywords.description','ASC')->get();
        } else {
            return TypologyKeywords::select('typology_keywords.id', 'typology_keywords.description')->where('typology_keywords.description', 'ilike', $search . '%')->orderBy('typology_keywords.description','ASC')->get();
        }
    }
    
    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $typologyKeywords = new TypologyKeywords();
            $typologyKeywords->description = $request->description;
            $typologyKeywords->html = $request->html;
            $typologyKeywords->save();
                        
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $typologyKeywords->id;
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoOptional
     * @param int idLanguage
     */
    public static function update(Request $request, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $typologyKeywordsOnDb = TypologyKeywords::find($request->id);
        
        DB::beginTransaction();

        try {
            $typologyKeywordsOnDb->description = $request->description;
            $typologyKeywordsOnDb->html = $request->html;
            $typologyKeywordsOnDb->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $typologyKeywords = TypologyKeywords::find($id);
        
        if (is_null($typologyKeywords)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            
            $typologyKeywords->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    
    #endregion DELETE
}

<?php

namespace App\BusinessLogic;

use App\Content;
use App\ContentLanguage;
use App\UsersDatas;
use App\Producer;
use App\DtoModel\DtoNation;
use App\DtoModel\DtoSelenaViews;
use App\DtoModel\DtoTagTemplate;
use App\DtoModel\DtoPagination;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\SelenaSqlViews;
use App\BusinessLogic\SettingBL;
use App\Enums\SettingEnum;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchBL
{
    #region PRIVATE

    #endregion PRIVATE

    #region GET

    #endregion GET

    #region INSERT

    /**
     * Search header
     * 
     * @param Request request
     * 
     */
    public static function search(Request $request)
    {
        
        $strLimit = "";
        $content = '';
        $idLanguageContent = $request->idLanguage; 
        $contentCode = Content::where('code', 'Items')->first();
        $contentLanguage = ContentLanguage::where('content_id', $contentCode->id)->where('language_id', $idLanguageContent)->first();
        
        $strWhereDescription = "";
        $strSqlPagination = "";
        if (strpos($request->search, ' ') !== false) {
            $strSearchSplit = explode(' ', $request->search);
            for ($i = 0; $i < count($strSearchSplit); $i++) {
                if(ltrim(rtrim($strSearchSplit[$i]))!=''){
                    if($strWhereDescription == ''){
                        $strWhereDescription = $strWhereDescription . " items_languages.description ilike '%" . $strSearchSplit[$i] . "%'";
                    }else{
                        $strWhereDescription = $strWhereDescription . " OR items_languages.description ilike '%" . $strSearchSplit[$i] . "%'";
                    }
                }
            }
            if($strWhereDescription!=''){
                $strWhereDescription = " AND (" . $strWhereDescription . " or items_languages.description ilike '%" . $request->search . "%')";
            }else{
                $strWhereDescription = " AND items_languages.description ilike '%" . $request->search . "%'";
            }
        }else{
            $strWhereDescription = " AND items_languages.description ilike '%" . $request->search . "%'";
        }
        

        $finalHtml = '';
        $tags = SelenaViewsBL::getTagByType('Items');
        $itemsPerPage =  SettingBL::getByCode(SettingEnum::NumberOfItemsPerPage);


        $strSqlPagination = "";
        $pageSelected = $request->paginator;

        if($request->autocomplete){
            $strSqlPagination = " limit 10 ";
        }

        $totItems = DB::select(
            'SELECT count(items.id) as cont
            FROM items
            INNER JOIN items_languages ON items.id = items_languages.item_id
            AND items.online = true and items_languages.language_id = :idLanguage' . $strWhereDescription . $strSqlPagination,
            ['idLanguage' => $idLanguageContent]);
        
        if($totItems[0]->cont <= $itemsPerPage){
            $strSqlPagination = "";
        }else{
            $limit = $itemsPerPage;
            $offset = $itemsPerPage * ($pageSelected - 1);

            $strSqlPagination = " limit " . $limit . " offset " . $offset;
        }

        
        if($request->autocomplete){
            $strSqlPagination = " limit 10 ";
        }


        foreach (DB::select(
            'SELECT *
            FROM items
            INNER JOIN items_languages ON items.id = items_languages.item_id
            AND items.online = true and items_languages.language_id = :idLanguage' . $strWhereDescription . $strSqlPagination,
            ['idLanguage' => $idLanguageContent]
        ) as $items){
            $newContent = $contentLanguage->content;
            foreach ($tags as $fieldsDb){
                if(isset($fieldsDb->tag)){
                    if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                        if(is_numeric($items->{$cleanTag})){
                            $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                        }else{
                            $tag = $items->{$cleanTag};
                        }
                        $newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                    }
                }
            }
            $user = UsersDatas::where("user_id", $items->created_id)->get()->first();
            if(isset($user)){
                if (strpos($newContent, '#userInfo#') !== false) {
                    $newContent = str_replace('#userInfo#', $user->business_name,  $newContent);
                }
            }
            $finalHtml = $finalHtml . $newContent;
        };
        
        if($finalHtml == ""){
            $finalHtml = '<div class="col-12 div-no-info"><h1>Nessuna informazione trovata</h1></div>';
        }

        $contentDto = new DtoPagination();
        $contentDto->pageSelected = $pageSelected;
        $contentDto->totItems = $totItems[0]->cont;
        $contentDto->itemsPerPage = $itemsPerPage;
        if($totItems[0]->cont <= $itemsPerPage){
            $contentDto->paginationActive = false;
            $contentDto->totalPages = "1";
        }else{
            $contentDto->paginationActive = true;
            $contentDto->totalPages = ceil($totItems[0]->cont / $itemsPerPage);
        }
        $contentDto->html = $finalHtml;
        $content = $contentDto;
        return $content;
    }

    /**
     * Search implemented
     * 
     * @param Request request
     * 
     */
    public static function searchImplemented(Request $request)
    {
        $content = '';
        $finalHtml = '';
        $strWhere = "";
        $idLanguageContent = $request->idLanguage; 
        $contentCode = Content::where('code', 'Items')->first();
        $contentLanguage = ContentLanguage::where('content_id', $contentCode->id)->where('language_id', $idLanguageContent)->first();
        foreach($request->request as $key => $value){
            if($request->{$key} != ""){
                if($key != 'idLanguage'){
                    $operator = ' = ';
                    $val = $request->{$key};
                    $keyNew = $key;
                    if($key == 'description'){
                        $keyNew = 'items_languages.description';
                        $operator = ' ilike ';
                        $val = '\'%' . $request->{$key} . '%\'';
                    }else{
                        if($key == 'price_from'){
                            $keyNew = 'price';
                            $operator = ' >= ';
                            $val = $request->{$key};
                        }else{
                            if($key == 'price_to'){
                                $keyNew = 'price';
                                $operator = ' <= ';
                                $val = $request->{$key};
                            }else{
                                $operator = ' = ';
                                $val = $request->{$key};
                            }
                        }
                    }
                    if($request->{$key} == "on"){
                        $keyNew = $key;
                        $val = "true";
                    }
                    $strWhere = $strWhere . ' AND ' . $keyNew . $operator . $val;
                }
            }
        }
        


        $tags = SelenaViewsBL::getTagByType('Items');
        foreach (DB::select(
            'SELECT items.*, items_languages.*
            FROM items
            INNER JOIN items_languages ON items.id = items_languages.item_id
            INNER JOIN categories_items ON items.id = categories_items.item_id
            INNER JOIN categories_languages ON categories_items.category_id = categories_languages.category_id
            AND items_languages.language_id = :idLanguage ' . $strWhere,
            ['idLanguage' => $idLanguageContent]
        ) as $items){
            $newContent = $contentLanguage->content;
            foreach ($tags as $fieldsDb){
                if(isset($fieldsDb->tag)){
                    if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                        if(is_numeric($items->{$cleanTag})){
                            $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                        }else{
                            $tag = $items->{$cleanTag};
                        }
                        $newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                    }
                }
            }
            $user = UsersDatas::where("user_id", $items->created_id)->get()->first();
            if(isset($user)){
                if (strpos($newContent, '#userInfo#') !== false) {
                    $newContent = str_replace('#userInfo#', $user->business_name,  $newContent);
                }
            }
            $finalHtml = $finalHtml . $newContent;
        };
        $content = $finalHtml;
        if($content == ""){
            $content = '<div class="col-12 div-no-info"><h1>Nessuna informazione trovata</h1></div>';
        }
        return $content;
    }
    #endregion INSERT


    public static function searchGeneral(Request $request)
    {
        $content = '';
        $contentPage = '';
        $finalHtml = '';

        if($request->pages){
            $strWhere = "";
            $idLanguageContent = $request->idLanguage;
            $contentCode = Content::where('code', 'NewsListArticle')->first();
            $contentLanguage = ContentLanguage::where('content_id', $contentCode->id)->where('language_id', $idLanguageContent)->first();
            $tags = SelenaViewsBL::getTagByType('NewsListArticle');

            foreach (DB::select( 
                'SELECT languages_pages.title,languages_pages.url,languages_pages.url_cover_image,pages.publish,languages_pages.seo_description
                FROM languages_pages 
                INNER JOIN pages on pages.id = languages_pages.page_id
                WHERE languages_pages.language_id = :idLanguage
                AND (languages_pages.content ilike \'%' . $request->searchGeneral . '%\' 
                OR languages_pages.title ilike \'%' . $request->searchGeneral . '%\'
                OR languages_pages.seo_description ilike \'%' . $request->searchGeneral . '%\')
                AND pages.online = true
                AND pages.homepage <> true
                AND (pages.visible_from IS NULL OR pages.visible_from <= NOW() )
                AND (pages.visible_end IS NULL OR pages.visible_end >= NOW() )
                order by pages.publish DESC limit 50',
                ['idLanguage' => $idLanguageContent]
                ) as $pages) {

                $newContent = $contentLanguage->content;
                foreach ($tags as $fieldsDb) {
                    if (isset($fieldsDb->tag)) {
                        if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                            $cleanTag = str_replace('#', '', $fieldsDb->tag);
                            if (is_numeric($pages->{$cleanTag})) {
                                if($fieldsDb->tag == '#publish#'){
                                    $tag = date_format(date_create($pages->{$cleanTag}), 'd/m/Y');
                                }else{
                                    $tag = str_replace('.',',',substr($pages->{$cleanTag}, 0, -2));
                                }
                            } else {
                                if($fieldsDb->tag == '#publish#'){
                                    $tag = date_format(date_create($pages->{$cleanTag}), 'd/m/Y');
                                }else{
                                    $tag = $pages->{$cleanTag};
                                }
                            }
                            $newContent = str_replace($fieldsDb->tag, $tag,  $newContent);
                        }
                    }
                }

                if (strpos($newContent, '#publish#') !== false) {
                            $newContent = str_replace('#publish#', date_format(date_create($pages->publish), 'd/m/Y'),  $newContent);
                }

                $finalHtml = $finalHtml . $newContent;

            };
            $contentPage = $finalHtml;
            
        }

        if($request->products){
            $idLanguageContent = $request->idLanguage;
            $idUser = '';
            $finalHtml = '';
            $tags = SelenaViewsBL::getTagByType('Items');
            $contentCode = Content::where('code', 'Items')->first();
            $contentLanguage = ContentLanguage::where('content_id', $contentCode->id)->where('language_id', $idLanguageContent)->first();
            $tags = SelenaViewsBL::getTagByType('Items');

            foreach (DB::select(
                'SELECT *
                FROM items
                INNER JOIN items_languages ON items.id = items_languages.item_id
                INNER JOIN categories_items ON categories_items.item_id = items.id
                AND items_languages.language_id = :idLanguage
                AND items.online = true
                AND (items_languages.description ilike \'%' . $request->searchGeneral . '%\' OR items.internal_code ilike \'%' . $request->searchGeneral . '%\')
                order by items.internal_code, categories_items.order asc limit 12',
                ['idLanguage' => $idLanguageContent]
            ) as $items){
                $newContent = $contentLanguage->content;
                foreach ($tags as $fieldsDb){
                    if(isset($fieldsDb->tag)){
                        if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                            $cleanTag = str_replace('#', '', $fieldsDb->tag);
                            if(is_numeric($items->{$cleanTag}) && $fieldsDb->tag != '#internal_code#' && $fieldsDb->tag != '#producer_id#'){ 
                                // Calcola prezzo (regola listino/prezzo dedicato a utente)
                                if($fieldsDb->tag == '#price#'){
                                    //prelievo campo nelle impostazioni per metodologia di visualizzazione, le opzioni sono NESSUNO, BARRATO, DEDICATO
                                    $settingDisplayPrice = SettingBL::getByCode(SettingEnum::DisplayPrice);
                                    if($idUser == null || $idUser == ''){
                                        $userId = '';
                                    }else{
                                        $userId = $idUser;
                                    }
                                    //creazione funzione per prelievo prezzo scontato, nelle regole del carrello

                                    switch ($settingDisplayPrice) {
                                        case "NESSUNO":
                                            //se NESSUNO stampo il prezzo in items
                                            $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                            break;
                                        case "BARRATO":
                                            //se BARRATO aggiungo l'html <strike> al prezzo normale e appendo il prezzo calcolato
                                            $priceCalculated = CartBL::getPriceCalculated($items->item_id, $userId);
                                            if($priceCalculated != ""){
                                                $tag = '<strike>' . str_replace('.',',',substr($items->{$cleanTag}, 0, -2)) . ' € </strike><br> ' . str_replace('.','',number_format($priceCalculated,2,",","."));
                                            }else{
                                                $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                            }
                                            break;
                                        case "DEDICATO":
                                            //se DEDICATO stampo il prezzo calcolato
                                            $priceCalculated = CartBL::getPriceCalculated($items->item_id, $userId);
                                            if($priceCalculated != ""){
                                                $tag = str_replace('.',',',substr($priceCalculated, 0, -1));
                                            }else{
                                                $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                            }
                                            break;
                                        default:
                                            $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                    }
                                }else{
                                    $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                }
                            }else{
                                $tag = $items->{$cleanTag};
                            }
                            if($fieldsDb->tag == '#item_id#'){
                                $newContent = str_replace($fieldsDb->tag , $items->item_id ,  $newContent);
                            }else{
                                $newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                            }
                            if (strpos($newContent, '#producer#') !== false) {
                                if(!is_null($items->producer_id) || $items->producer_id !=''){
                                    $producerItem = Producer::where('id', $items->producer_id)->get()->first();
                                    if(isset($producerItem)){
                                        $newContent = str_replace('#producer#' , $producerItem->business_name ,  $newContent);
                                    }else{
                                        $newContent = str_replace('#producer#' , '-',  $newContent);
                                    }
                                }else{
                                    $newContent = str_replace('#producer#' , '-',  $newContent);
                                }
                            }
                        }
                    }
                    
                }                                    
                $finalHtml = $finalHtml . $newContent;
            };

            $contentPage = $contentPage . $finalHtml;
        }

        if($contentPage == ''){
            $contentPage = MessageBL::getByCode('CONTENT_NOT_FOUND', $request->idLanguage);  
        }
        
        $content = $contentPage;


        return $content;
    }

    




}

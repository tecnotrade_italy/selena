<?php

namespace App\BusinessLogic;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoFast;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Fast;
use App\ItemGroupTechnicalSpecification;
use App\User;
use App\UsersDatas;
use Intervention\Image\Facades\Image;
use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;


class FastBL
{
 #region GET
 /**
     * insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoFast)
    {

        DB::beginTransaction();

        try {

            if(isset($DtoFast->FastUrgenze)) { 
            foreach ($DtoFast->FastUrgenze as $FastUrgenze) {
                $newFastUrgenze= new Fast();

                 if (isset($FastUrgenze['users_id'])) {
                    $newFastUrgenze->user_id = $FastUrgenze['users_id'];
                    }
                if (isset($FastUrgenze['object'])) {
                    $newFastUrgenze->object = $FastUrgenze['object'];
                    }
                if (isset($FastUrgenze['date'])) {
                    $newFastUrgenze->date = $FastUrgenze['date'];
                    }
                if (isset($FastUrgenze['description'])) {
                    $newFastUrgenze->description = $FastUrgenze['description'];
                    }
                $newFastUrgenze->save();
            }
  }   
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return '';
    }

 /**
     * Get UserData
     * 
     * @return DtoFast
     */
    public static function getUserData($sUserId)
    {
         $result = collect();
          
        //$fasturgencies = Fast::where('user_id', $sUserId)->get();

   
        foreach (Fast::orderBy('id', 'DESC')->get() as $fastUrgencies) {
        $DtoFast = new DtoFast();
        $DtoFast->id = $fastUrgencies->id;
        $DtoFast->object = $fastUrgencies->object;
        $DtoFast->date = $fastUrgencies->date;
        $DtoFast->description = $fastUrgencies->description;
         if (isset($fastUrgencies->user_id)) {
        $DtoFast->users_id  = $fastUrgencies->user_id;
        $DtoFast->description_user_id = UsersDatas::where('user_id', $fastUrgencies->user_id)->first()->business_name;
            }
       // $DtoFast->Aggfasturgencies->push($DtoFast);
        $result->push($DtoFast);
   }   
    
        return $result;
    }
 

  public static function updateFast(Request $request)
    { 
       
        DB::beginTransaction();
        try {

                if (!$request['0']['isNew']) {
                $FastOnDb = Fast::where('id', $request['0']['id'])->first();
                $FastOnDb->id = $request['0']['id'];    
                $FastOnDb->date = $request['0']['date'];
                $FastOnDb->object = $request['0']['object'];
                $FastOnDb->description = $request['0']['description'];
                $FastOnDb->user_id = $request['0']['users_id'];
                $FastOnDb->save();
                }else {
                    $Fast = new Fast();
                    if (isset($request['0']['date'])) {
                            $Fast->date = $request['0']['date'];
                            }
                    if (isset($request['0']['object'])) {
                            $Fast->object = $request['0']['object'];
                            }
                    if (isset($request['0']['description'])) {
                            $Fast->description = $request['0']['description'];
                            }
                   if (isset($request['0']['users_id'])) {
                        $Fast->user_id = $request['0']['users_id'];
                    }
                    $Fast->save();  

            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }   

     /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */

    public static function delete(int $id, int $idLanguage)
    {
        $Fast = Fast::find($id);

        if (is_null($Fast)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($Fast)) {
            DB::beginTransaction();
            try {
                Fast::where('id', $id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }  
        } 
    }


 } 

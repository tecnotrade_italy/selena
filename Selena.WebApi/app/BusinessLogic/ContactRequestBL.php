<?php

namespace App\BusinessLogic;

use App\Contact;
use App\ContactGroupNewsletter;
use App\ContactRequest;
use App\DtoModel\DtoContactRequest;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\GroupNewsletter;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Mail\ContactRequestMail;
use App\Mail\ContactRequestMailFeedback;
use App\Mail\ContactRequestMailWorkForUs;
use App\Mail\DynamicMail;
use App\Message;
use App\MessageLanguage;
use App\Reservations;
use App\Setting;
use App\Language;
use App\User;
use App\LogsContact;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ContactRequestBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request dtoContactRequest
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validataData(Request $dtoContactRequest, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($dtoContactRequest->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (ContactRequest::find($dtoContactRequest->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

            if (is_null($dtoContactRequest->description)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
    }

    /**
     * Convert to dto
     * 
     * @param ContactRequest contactRequest
     * 
     * @return DtoContactRequest
     */
    private static function _convertToDto(ContactRequest $contactRequest)
    {
        $contact = ContactBL::getById($contactRequest->contact_id);

        $dtoContactRequest = new DtoContactRequest();
        $dtoContactRequest->id = $contactRequest->id;
        $dtoContactRequest->name = $contact->name;
        $dtoContactRequest->surname = $contact->surname;
        $dtoContactRequest->email = $contact->email;

        if (isset($contact->phoneNumber)) {
            $dtoContactRequest->phoneNumber = $contact->phoneNumber;
        }

        if (isset($contactRequest->email_to_send)) {
            $dtoContactRequest->emailToSend = $contactRequest->email_to_send;
        }

        $dtoContactRequest->date = Carbon::parse($contactRequest->created_at)->format(HttpHelper::getLanguageDateTimeFormat());
        $dtoContactRequest->description = $contactRequest->description;
        return $dtoContactRequest;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get all
     * 
     * @return DtoContactRequest
     */
    public static function getAll()
    {
        return ContactRequest::all()->map(function ($contactRequest) {
            return static::_convertToDto($contactRequest);
        });
    }

    /**
     * Get by id
     * 
     * @param int id
     * 
     * @return DtoContactRequest
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(ContactRequest::find($id));
    }

    #endregion GET

    public static function insertLandingeBookPcb(Request $DtoContactRequest)
    {

        if (Contact::where('email', $DtoContactRequest->email)->count() > 0) {
            throw new Exception(DictionaryBL::getTranslate($DtoContactRequest->idLanguage, DictionariesCodesEnum::EmailAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoContactRequest->email)) {
            throw new Exception(DictionaryBL::getTranslate($DtoContactRequest->idLanguage, DictionariesCodesEnum::EmailNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoContactRequest->name)) {
            throw new Exception(DictionaryBL::getTranslate($DtoContactRequest->idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoContactRequest->surname)) {
            throw new Exception(DictionaryBL::getTranslate($DtoContactRequest->idLanguage, DictionariesCodesEnum::SurnameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoContactRequest->business_name)) {
            throw new Exception(DictionaryBL::getTranslate($DtoContactRequest->idLanguage, DictionariesCodesEnum::BusinessNameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if ($DtoContactRequest->processing_of_personal_data == false) {
            throw new Exception(DictionaryBL::getTranslate($DtoContactRequest->idLanguage, DictionariesCodesEnum::CheckProcessingOfPersonalDataNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        DB::beginTransaction();
        try {
            $contact = new Contact();
            $contact->name = $DtoContactRequest->name;
            $contact->surname = $DtoContactRequest->surname;
            $contact->email = $DtoContactRequest->email;
            $contact->business_name = $DtoContactRequest->business_name;
            $contact->role = $DtoContactRequest->role;
            $contact->send_newsletter = true;
            $contact->created_id = UserBL::getContactRequest();
            $contact->save();

            $contactRequest = new ContactRequest();
            $contactRequest->contact_id = $contact->id; 
            $contactRequest->description = ''; 
            $contactRequest->processing_of_personal_data = $DtoContactRequest->processing_of_personal_data;
            $DtoContactRequest->link_paper = static::getUrl(); 
            //'/storage/files/ebook-pcb-cistelaier.pdf';
            $contactRequest->save();
            
            $ContactGroupNewsletter = new ContactGroupNewsletter();
            $ContactGroupNewsletter->contact_id = $contact->id;   
                $Languages = Language::where('id', $DtoContactRequest->idLanguage)->first();
            if ($Languages->description ==  'Italiano'){
                $ContactGroupNewsletter->group_newsletter_id = GroupNewsletter::where('description', 'PaperItaliano')->first()->id; 
            }else{
                $ContactGroupNewsletter->group_newsletter_id = GroupNewsletter::where('description', 'PaperInglese')->first()->id; 
            }
                $ContactGroupNewsletter->save();
              
               

            if ($Languages->description ==  'Italiano'){

                /* INVIO EMAIL UTENTE IN FASE DI REGISTRAZIONE PER SCARICARE IL PAPER CISTELAIER */
                $emailFrom = SettingBL::getContactsEmail();
                $textMail= MessageBL::getByCode('EMAIL_DOWNLOAD_PAPER_TEXT', $DtoContactRequest->idLanguage);       
                $textObject= MessageBL::getByCode('EMAIL_DOWNLOAD_PAPER_OBJECT', $DtoContactRequest->idLanguage);
                $textMail =str_replace ('#email#', $DtoContactRequest->email, $textMail);  
                $textMail =str_replace ('#name#', $DtoContactRequest->name, $textMail);
                $textMail =str_replace ('#surname#', $DtoContactRequest->surname, $textMail);
                $textMail =str_replace ('#business_name#', $DtoContactRequest->business_name, $textMail);
                $textMail =str_replace ('#link_paper#', $DtoContactRequest->link_paper, $textMail);
                $textMail =str_replace ('#id_contact#', $contact->id, $textMail);
                Mail::to($DtoContactRequest->email)->send(new DynamicMail($textObject,$textMail));   
                /* END INVIO EMAIL UTENTE IN FASE DI REGISTRAZIONE PER SCARICARE IL PAPER CISTELAIER */

            } else { 

                  /* INVIO EMAIL UTENTE IN FASE DI REGISTRAZIONE PER SCARICARE IL PAPER CISTELAIER */
                  $emailFrom = SettingBL::getContactsEmail();
                  $textMail= MessageBL::getByCode('EMAIL_DOWNLOAD_PAPER_TEXT_ENG', $DtoContactRequest->idLanguage);       
                  $textObject= MessageBL::getByCode('EMAIL_DOWNLOAD_PAPER_OBJECT_ENG', $DtoContactRequest->idLanguage);
                  $textMail =str_replace ('#email#', $DtoContactRequest->email, $textMail);  
                  $textMail =str_replace ('#name#', $DtoContactRequest->name, $textMail);
                  $textMail =str_replace ('#surname#', $DtoContactRequest->surname, $textMail);
                  $textMail =str_replace ('#business_name#', $DtoContactRequest->business_name, $textMail);
                  $textMail =str_replace ('#link_paper#', $DtoContactRequest->link_paper, $textMail);
                  $textMail =str_replace ('#id_contact#', $contact->id, $textMail);
                  Mail::to($DtoContactRequest->email)->send(new DynamicMail($textObject,$textMail));   
                  /* END INVIO EMAIL UTENTE IN FASE DI REGISTRAZIONE PER SCARICARE IL PAPER CISTELAIER */

            }  


               
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

    }
    #region INSERT

    public static function insertLogsContacts(Request $request)
    {
            DB::beginTransaction();
                try {
                    $LogsContact = new LogsContact();
                    $LogsContact->created_id = $request->idUser;
                    $LogsContact->id_contact = $request->idContact;
                    $LogsContact->url_file = $request->url_file;
                    $LogsContact->ip = $request->ip();
                        $mytime = Carbon::now()->format('Y-m-d H:i:s');
                    $LogsContact->date = $mytime;
                    $LogsContact->save();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }

            return $LogsContact->url_file;
    }

    



    private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"];
    }


    /**
     * Insert
     * 
     * @param Request dtoContactRequest
     * 
     * @return int
     */
    public static function insert(Request $DtoContactRequest)
    {

        $idLanguage = LanguageBL::getDefault()->id;

        //static::_validataData($DtoContactRequest, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $idContact = ContactBL::insert($DtoContactRequest, $idLanguage);
            $contactRequest = new ContactRequest();
            $contactRequest->contact_id = $idContact;
            $contactRequest->description = $DtoContactRequest->description;
            $contactRequest->object = $DtoContactRequest->object;


            if ($DtoContactRequest->checkprivacy_cookie_policy == true) {
                $contactRequest->checkprivacy_cookie_policy = true;
            }else{
                $contactRequest->checkprivacy_cookie_policy = false; 
            }
            if ($DtoContactRequest->processing_of_personal_data == true) {
                $contactRequest->processing_of_personal_data = true;
            }else{
                $contactRequest->processing_of_personal_data = false; 
            }

          /*  if (isset($DtoContactRequest->checkprivacy_cookie_policy)) {
                $contactRequest->checkprivacy_cookie_policy = true;
            }else{
                $contactRequest->checkprivacy_cookie_policy = false; 
            }

            if (isset($DtoContactRequest->processing_of_personal_data)) {
                $contactRequest->processing_of_personal_data = true;
            }else{
                $contactRequest->processing_of_personal_data = false; 
            }*/


           /* $contactRequest->checkprivacy_cookie_policy = $DtoContactRequest->checkprivacy_cookie_policy;
            $contactRequest->processing_of_personal_data = $DtoContactRequest->processing_of_personal_data;*/

            if (isset($DtoContactRequest->emailToSend)) {
                $contactRequest->email_to_send = $DtoContactRequest->emailToSend;
            }
             if (isset($DtoContactRequest->city)) {
                $contactRequest->city = $DtoContactRequest->city;
            }else{
                $contactRequest->city = '';  
            }
            if (isset($DtoContactRequest->address)) {
                $contactRequest->address = $DtoContactRequest->address;
            }else{
                $contactRequest->address = '';  
            }

            if (isset($DtoContactRequest->countryofresidence)){
                $contactRequest->countryofresidence = $DtoContactRequest->countryofresidence;
            }else{
                $contactRequest->countryofresidence = '';
            }
                $contactRequest->save();

            
            $ContactGroupNewsletter = new ContactGroupNewsletter();
            $ContactGroupNewsletter->contact_id = $idContact;   
            $ContactGroupNewsletter->group_newsletter_id = GroupNewsletter::where('description', 'SubscribedToTheNewsletter')->first()->id; 
            $ContactGroupNewsletter->save();

            if ($DtoContactRequest->newsletter_info == true) {
                $ContactGroupNewsletter = new ContactGroupNewsletter();
                $ContactGroupNewsletter->contact_id = $idContact;   
                $ContactGroupNewsletter->group_newsletter_id = GroupNewsletter::where('description', 'SubscribedToTheNewsletterInfo')->first()->id; 
                $ContactGroupNewsletter->save();
            }

            if ($DtoContactRequest->newsletter_mailing == true) {
                $ContactGroupNewsletter = new ContactGroupNewsletter();
                $ContactGroupNewsletter->contact_id = $idContact;   
                $ContactGroupNewsletter->group_newsletter_id = GroupNewsletter::where('description', 'SubscribedToTheNewsletter')->first()->id; 
                $ContactGroupNewsletter->save();
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }


        if (isset($DtoContactRequest->phoneNumber)) {
            $phoneNumber = $DtoContactRequest->phoneNumber;
        }else{
            $phoneNumber = ""; 
        }
         if (isset($DtoContactRequest->surname)) {
            $surname = $DtoContactRequest->surname;
        }else{
            $surname = ""; 
        }

        if (isset($DtoContactRequest->businessName)) {
            $businessName = $DtoContactRequest->businessName;
        }else{
            $businessName = ""; 
        }

        if (isset($DtoContactRequest->province)) {
            $province = $DtoContactRequest->province;
        }else{
            $province = ""; 
        }

        if (isset($DtoContactRequest->vatNumber)) {
            $vatNumber = $DtoContactRequest->vatNumber;
        }else{
            $vatNumber = ""; 
        }

        if (isset($DtoContactRequest->postalCode)) {
            $postalCode = $DtoContactRequest->postalCode;
        }else{
            $postalCode = ""; 
        }
        if (isset($DtoContactRequest->object)) {
            $object = $DtoContactRequest->object;
        }else{
            $object = ""; 
        }
         if (isset($DtoContactRequest->city)) {
            $city = $DtoContactRequest->city;
        }else{
            $city = ""; 
        }
        if (isset($DtoContactRequest->address)) {
            $address = $DtoContactRequest->address;
        }else{
            $address = ""; 
        }
    
        if ($DtoContactRequest->processing_of_personal_data == 1){
            $processing_of_personal_data = ' Il richiedente ha Prestato il consenso al trattamento dei dati ai fini dell\'invio di comunicazioni commerciali';
        }else{
            $processing_of_personal_data = false; 
        }

        
        if (isset($DtoContactRequest->emailToSend)) {
                Mail::to($DtoContactRequest->emailToSend)->send(new ContactRequestMail($DtoContactRequest->name, $surname, $DtoContactRequest->email, $DtoContactRequest->description, $phoneNumber, $businessName, $province, $vatNumber, $postalCode, $processing_of_personal_data,$city,$address));
        } else {
                Mail::to(SettingBL::getContactsEmail())->send(new ContactRequestMail($DtoContactRequest->name, $surname, $DtoContactRequest->email, $DtoContactRequest->description, $phoneNumber, $businessName, $province, $vatNumber, $postalCode, $processing_of_personal_data,$city,$address));
        }

                 Mail::to($DtoContactRequest->email)->send(new ContactRequestMailFeedback($DtoContactRequest->name, $surname, $DtoContactRequest->email, $DtoContactRequest->description, $phoneNumber, $businessName, $province, $vatNumber, $postalCode));

        return $contactRequest->id;
    }

  /**
     * Insert
     * 
     * @param Request Dtolisttour
     * 
     * @return int
     */
    public static function insertTourAvailable(Request $request)
    
    {
           DB::beginTransaction();

        try {      
                $listtour = new Reservations();
              
                $listtour->date = $request->date;
                $listtour->adult_nr = $request->adult_nr;
                $listtour->kids_nr = $request->kids_nr;
                $listtour->babies_nr = $request->babies_nr;
                $listtour->total_amount = $request->total_amount; 
                $listtour->course_id = $request->course_id; 
                $listtour->name = $request->name; 
                $listtour->surname = $request->surname; 
                $listtour->email = $request->email; 
                $listtour->telephone = $request->telephone; 
                $listtour->notes = $request->notes; 
                $listtour->save();


                $emailFrom = SettingBL::getContactsEmail();
                $textMail= MessageBL::getByCode('CONFIRM_RESERVATIONS_TEXT', $request->idLanguage);       
                $textObject= MessageBL::getByCode('CONFIRM_RESERVATION_OBJECT', $request->idLanguage );
                $textMail =str_replace ('#date#', $request->date, $textMail);  
                $textMail =str_replace ('#adult_nr#', $request->adult_nr, $textMail);
                $textMail =str_replace ('#kids_nr#', $request->kids_nr, $textMail);
                $textMail =str_replace ('#babies_nr#', $request->babies_nr, $textMail);
                $textMail =str_replace ('#total_amount#', $request->total_amount, $textMail);
                $textMail =str_replace ('#name#', $request->name, $textMail);
                $textMail =str_replace ('#surname#', $request->surname, $textMail);
                $textMail =str_replace ('#email#', $request->email, $textMail);
                $textMail =str_replace ('#telephone#', $request->telephone, $textMail);
                $textMail =str_replace ('#notes#', $request->notes, $textMail);
                $textMail =str_replace ('#course_id#', $request->course_id, $textMail);
                Mail::to($request->email)->send(new DynamicMail($textObject,$textMail));   


                $textMail= MessageBL::getByCode('CONFIRM_RESERVATIONS_INFORMATION_TEXT', $request->idLanguage);       
                $textObject= MessageBL::getByCode('CONFIRM_RESERVATIONS_INFORMATION_OBJECT', $request->idLanguage);
                $textMail =str_replace ('#date#', $request->date, $textMail);  
                $textObject =str_replace ('#course_id#', $request->course_id, $textObject);  
                $textMail =str_replace ('#adult_nr#', $request->adult_nr, $textMail);
                $textMail =str_replace ('#kids_nr#', $request->kids_nr, $textMail);
                $textMail =str_replace ('#babies_nr#', $request->babies_nr, $textMail);
                $textMail =str_replace ('#total_amount#', $request->total_amount, $textMail);
                $textMail =str_replace ('#name#', $request->name, $textMail);
                $textMail =str_replace ('#surname#', $request->surname, $textMail);
                $textMail =str_replace ('#email#', $request->email, $textMail);
                $textMail =str_replace ('#notes#', $request->notes, $textMail);
                $textMail =str_replace ('#telephone#', $request->telephone, $textMail);
                $textMail =str_replace ('#course_id#', $request->course_id, $textMail);

                $mailinformation='EmailInformationList';
                $mailsendRiepilogo = Setting::where('code', $mailinformation)->first();
                Mail::to($mailsendRiepilogo->value)->send(new DynamicMail($textObject,$textMail)); 


            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

   
    /**
     * Insert
     * 
     * @param Request dtoContactWorkForUs
     * 
     * @return int
     */
    public static function insertWorkForUs(Request $dtoContactWorkForUs)
    {
        LogHelper::debug($dtoContactWorkForUs);
        //$idLanguage = LanguageBL::getDefault()->id;

        //static::_validataData($dtoContactWorkForUs, $idLanguage, DbOperationsTypesEnum::INSERT);
        $path = $dtoContactWorkForUs->file('file')->getRealPath();
        $as = $dtoContactWorkForUs->file('file')->getClientOriginalName();
        $mime = $dtoContactWorkForUs->file('file')->getClientMimeType();

    if(isset($dtoContactWorkForUs->name)) {  
            $dtoContactWorkForUs->name;
    }else{
        $dtoContactWorkForUs->name = '';
    } 
    if(isset($dtoContactWorkForUs->name)) {  
        $dtoContactWorkForUs->name;
    }else{
        $dtoContactWorkForUs->name = '';
    } 
    if(isset($dtoContactWorkForUs->email)) {  
        $dtoContactWorkForUs->email;
    }else{
        $dtoContactWorkForUs->email = '';
    } 
    if(isset($dtoContactWorkForUs->phoneNumber)) {  
        $dtoContactWorkForUs->phoneNumber;
    }else{
        $dtoContactWorkForUs->phoneNumber = '';
    } 
    if(isset($dtoContactWorkForUs->gender)) {  
        $dtoContactWorkForUs->gender;
    }else{
        $dtoContactWorkForUs->gender = '';
    } 
    if(isset($dtoContactWorkForUs->date)) {  
        $dtoContactWorkForUs->date;
    }else{
        $dtoContactWorkForUs->date = '';
    }
    if(isset($dtoContactWorkForUs->position)) {  
        $dtoContactWorkForUs->position;
    }else{
        $dtoContactWorkForUs->position = '';
    }
    if(isset($dtoContactWorkForUs->nation)) {  
        $dtoContactWorkForUs->nation;
    }else{
        $dtoContactWorkForUs->nation = '';
    }
    if(isset($dtoContactWorkForUs->city)) {  
        $dtoContactWorkForUs->city;
    }else{
        $dtoContactWorkForUs->city = '';
    }
    if(isset($dtoContactWorkForUs->info)) {  
        $dtoContactWorkForUs->info;
    }else{
        $dtoContactWorkForUs->info = '';
    }
    if(isset($dtoContactWorkForUs->actualCompany)) {  
        $dtoContactWorkForUs->actualCompany;
    }else{
        $dtoContactWorkForUs->actualCompany = '';
    }
     if(isset($dtoContactWorkForUs->actualCompany)) {  
        $dtoContactWorkForUs->actualCompany;
    }else{
        $dtoContactWorkForUs->actualCompany = '';
    }
    if(isset($dtoContactWorkForUs->cityofborn)) {  
        $dtoContactWorkForUs->cityofborn;
    }else{
        $dtoContactWorkForUs->cityofborn = '';
    }
    if(isset($dtoContactWorkForUs->address)) {  
        $dtoContactWorkForUs->address;
    }else{
        $dtoContactWorkForUs->address = '';
    }
    if(isset($dtoContactWorkForUs->cap)) {  
        $dtoContactWorkForUs->cap;
    }else{
        $dtoContactWorkForUs->cap = '';
    }
    if(isset($dtoContactWorkForUs->province)) {  
        $dtoContactWorkForUs->province;
    }else{
        $dtoContactWorkForUs->province = '';
    }
    if(isset($dtoContactWorkForUs->countryofresidence)) {  
        $dtoContactWorkForUs->countryofresidence;
    }else{
        $dtoContactWorkForUs->countryofresidence = '';
    }
    if(isset($dtoContactWorkForUs->regionofdomicily)) {  
        $dtoContactWorkForUs->regionofdomicily;
    }else{
        $dtoContactWorkForUs->regionofdomicily = '';
    }
    if(isset($dtoContactWorkForUs->cityofresidence)) {  
        $dtoContactWorkForUs->cityofresidence;
    }else{
        $dtoContactWorkForUs->cityofresidence = '';
    }
    if(isset($dtoContactWorkForUs->fiscalcode)) {  
        $dtoContactWorkForUs->fiscalcode;
    }else{
        $dtoContactWorkForUs->fiscalcode = '';
    }
    if(isset($dtoContactWorkForUs->website)) {  
        $dtoContactWorkForUs->website;
    }else{
        $dtoContactWorkForUs->website = '';
    }
    if(isset($dtoContactWorkForUs->license)) {  
        $dtoContactWorkForUs->license;
    }else{
        $dtoContactWorkForUs->license = '';
    }
    if(isset($dtoContactWorkForUs->meansoftransport)) {  
        $dtoContactWorkForUs->meansoftransport;
    }else{
        $dtoContactWorkForUs->meansoftransport = '';
    }
    if(isset($dtoContactWorkForUs->countryofbirth)) {  
        $dtoContactWorkForUs->countryofbirth;
    }else{
        $dtoContactWorkForUs->countryofbirth = '';
    }

               

        Mail::to(SettingBL::getContactsEmail())->send(new ContactRequestMailWorkForUs(
        $dtoContactWorkForUs->name,
        $dtoContactWorkForUs->surname, 
        $dtoContactWorkForUs->email, 
        $dtoContactWorkForUs->phoneNumber, 
        $dtoContactWorkForUs->gender, 
        $dtoContactWorkForUs->date, 
        $dtoContactWorkForUs->position, 
        $dtoContactWorkForUs->nation, 
        $dtoContactWorkForUs->city, 
        $dtoContactWorkForUs->info, 
        $dtoContactWorkForUs->actualCompany, 
        $path, $as, $mime, 
        $dtoContactWorkForUs->cityofborn, 
        $dtoContactWorkForUs->address, 
        $dtoContactWorkForUs->cap, 
        $dtoContactWorkForUs->province,
        $dtoContactWorkForUs->countryofresidence,
        $dtoContactWorkForUs->regionofdomicily,
        $dtoContactWorkForUs->cityofresidence,
        $dtoContactWorkForUs->fiscalcode,
        $dtoContactWorkForUs->website,
        $dtoContactWorkForUs->license,
        $dtoContactWorkForUs->meansoftransport,
        $dtoContactWorkForUs->countryofbirth
  ));
  
                $textMail= MessageBL::getByCode('CONFIRM_EMAIL_RECEIVED_CV_TEXT');
                $textObject= MessageBL::getByCode('CONFIRM_EMAIL_RECEIVED_CV_OBJECT');

                $textObject =str_replace ('#email#', $dtoContactWorkForUs->email, $textObject); 
                $textMail =str_replace ('#email#', $dtoContactWorkForUs->email, $textMail); 
                Mail::to($dtoContactWorkForUs->email)->send(new DynamicMail($textObject,$textMail));
 
  
    }
    #endregion INSERT

/**
     * Insert
     * 
     * @param Request request
     * 
     * @return int
     */
    public static function insertRequestLanguage(Request $request)
    {
      /*  if (Contact::where('email', $request->email)->count() > 0) {
            throw new Exception(DictionaryBL::getTranslate($request->idLanguage, DictionariesCodesEnum::EmailAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
        }*/

        if (is_null($request->name)) {
            throw new Exception(DictionaryBL::getTranslate($request->idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($request->surname)) {
            throw new Exception(DictionaryBL::getTranslate($request->idLanguage, DictionariesCodesEnum::SurnameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->email)) {
            throw new Exception(DictionaryBL::getTranslate($request->idLanguage, DictionariesCodesEnum::EmailNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($request->idLanguage, DictionariesCodesEnum::DescriptionNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
       
        if (is_null($request->phoneNumber)) {
            throw new Exception(DictionaryBL::getTranslate($request->idLanguage, DictionariesCodesEnum::PhoneNotValued), HttpResultsCodesEnum::InvalidPayload);
        }


        DB::beginTransaction();

        try {  
            $contact = new Contact();
            $contact->name = $request->name;
            $contact->surname = $request->surname;
            $contact->email = $request->email;

            if (isset($request->businessName)) {
                $contact->business_name = $request->businessName;
            }
    
            if (isset($request->phoneNumber)) {
                $contact->phone_number = $request->phoneNumber;
            }else{
                $contact->phone_number = ''; 
            }
    
            if (isset($request->faxNumber)) {
                $contact->fax_number = $request->faxNumber;
            }else{
                $contact->fax_number = ''; 
            }
            
            if (isset($request->province)) {
                $contact->province = $request->province;
            }
    
            if (isset($request->postalCode)) {
                $contact->postal_code = $request->postalCode;
            }
    
            if (isset($request->vatNumber)) {
                $contact->vat_number = $request->vatNumber;
            }
    
            if (isset($request->sendNewsletter)) {
                $contact->send_newsletter = $request->sendNewsletter;
            }
    
            if (isset($request->errorNewsletter)) {
                $contact->error_newsletter = $request->errorNewsletter;
            }
            
            $contact->send_newsletter = true;
            $contact->created_id = UserBL::getContactRequest();
            $contact->save();
   
            $contactRequest = new ContactRequest();
            $contactRequest->contact_id = $contact->id;
            $contactRequest->description = $request->description;
            $contactRequest->object = $request->object;
            $contactRequest->save();

            $ContactGroupNewsletter = new ContactGroupNewsletter();
            $ContactGroupNewsletter->contact_id = $contact->id;   
            $ContactGroupNewsletter->group_newsletter_id = GroupNewsletter::where('description', 'SubscribedToTheNewsletter')->first()->id; 
            $ContactGroupNewsletter->save();

            if ($request->newsletter_info == true) {
                $ContactGroupNewsletter = new ContactGroupNewsletter();
                $ContactGroupNewsletter->contact_id = $contact->id;
                $ContactGroupNewsletter->group_newsletter_id = GroupNewsletter::where('description', 'SubscribedToTheNewsletterInfo')->first()->id; 
                $ContactGroupNewsletter->save();
            }

            if ($request->newsletter_mailing == true) {
                $ContactGroupNewsletter = new ContactGroupNewsletter();
                $ContactGroupNewsletter->contact_id = $contact->id;
                $ContactGroupNewsletter->group_newsletter_id = GroupNewsletter::where('description', 'SubscribedToTheNewsletter')->first()->id; 
                $ContactGroupNewsletter->save();
            }


                //EMAIL-CONTATTO 
                $emailFrom = SettingBL::getContactsEmail();
                $textMail= MessageBL::getByCode('CONFIRM_REQUEST_CONTACT_TEXT', $request->idLanguage);       
                $textObject= MessageBL::getByCode('CONFIRM_REQUEST_CONTACT_OBJECT', $request->idLanguage);
                $textMail =str_replace ('#name#', $request->name, $textMail);  
                $textMail =str_replace ('#surname#', $request->surname, $textMail);
                $textMail =str_replace ('#email#', $request->email, $textMail);
                $textMail =str_replace ('#description#', $request->description, $textMail);
                $textMail =str_replace ('#phoneNumber#', $request->phoneNumber, $textMail);
                $textMail =str_replace ('#object#', $request->object, $textMail);
                Mail::to($request->email)->send(new DynamicMail($textObject,$textMail));  

                //EMAIL DI NOTIFICA AL PROPRIETARIO DEL SITO:
                $emailFrom = SettingBL::getContactsEmail();
                $textMail= MessageBL::getByCode('CONFIRM_REQUEST_INFORMATION_CONTACT_TEXT', $request->idLanguage);       
                $textObject= MessageBL::getByCode('CONFIRM_REQUEST_INFORMATION_CONTACT_OBJECT', $request->idLanguage );
                $textMail =str_replace ('#name#', $request->name, $textMail);  
                $textMail =str_replace ('#surname#', $request->surname, $textMail);
                $textMail =str_replace ('#email#', $request->email, $textMail);
                $textMail =str_replace ('#description#', $request->description, $textMail);
                $textMail =str_replace ('#phoneNumber#', $request->phoneNumber, $textMail);
                $textMail =str_replace ('#object#', $request->object, $textMail);
                $textObject =str_replace ('#name#', $request->name, $textObject);
                $textObject =str_replace ('#surname#', $request->surname, $textObject);
                $textObject =str_replace ('#phoneNumber#', $request->phoneNumber, $textObject);
                $textObject =str_replace ('#email#', $request->email, $textObject);
                //Mail::to($request->email)->send(new DynamicMail($textObject,$textMail));  
                $mailinformation='ContactsEmail';
                $mailsendRiepilogo = Setting::where('code', $mailinformation)->first();
                Mail::to($mailsendRiepilogo->value)->send(new DynamicMail($textObject,$textMail)); 

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

}

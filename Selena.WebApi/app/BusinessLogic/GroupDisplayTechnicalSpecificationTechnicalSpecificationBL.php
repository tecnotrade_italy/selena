<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoLanguageTechnicalSpecification;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\GroupDisplayTechnicalSpecification;
use App\GroupDisplayTechnicalSpecificationTechnicalSpecification;
use App\Helpers\LogHelper;
use App\TechincalSpecification;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;

class GroupDisplayTechnicalSpecificationTechnicalSpecificationBL
{
    #region PRIVATE

    /**
     * Validate data before insert
     * 
     * @param int idGroupTechnicalSpecification
     * @param int idTechnicalSpecification
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    private static function _validateDataInsert(int $idGroupDisplayTechnicalSpecification, int $idTechnicalSpecification, int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(GroupDisplayTechnicalSpecification::find($idGroupDisplayTechnicalSpecification))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupTechnicalSpecificationNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(TechincalSpecification::find($idTechnicalSpecification))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TechincalSpecificationNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    /**
     * Validate data before update
     * 
     * @param int id
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    private static function _validateDataUpdate(int $id, int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(GroupDisplayTechnicalSpecificationTechnicalSpecification::find($id))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupDisplayTechincalSpecificationTechincalSpecificationNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by group technical specification and language
     * 
     * @param int $idGroupDisplayTechnicalSpecification
     * @param int $idLanguage
     * 
     * @return DtoLanguageTechnicalSpecification
     */
    public static function getInlucedInGroup(int $idGroupDisplayTechnicalSpecification, int $idLanguage)
    {
        $result = collect();

        foreach (GroupDisplayTechnicalSpecificationTechnicalSpecification::where('group_display_technical_specification_id', $idGroupDisplayTechnicalSpecification)->get() as $groupDisplayTechnicalSpecificationTechnicalSpecification) {
            $dtoLanguageTechnicalSpecification = new DtoLanguageTechnicalSpecification();
            $dtoLanguageTechnicalSpecification->id = $groupDisplayTechnicalSpecificationTechnicalSpecification->id;
            $dtoLanguageTechnicalSpecification->description = LanguageTechnicalSpecificationBL::getByIdTechnicalSpecificationAndLanguage($groupDisplayTechnicalSpecificationTechnicalSpecification->technical_specification_id, $idLanguage);
            $dtoLanguageTechnicalSpecification->order = $groupDisplayTechnicalSpecificationTechnicalSpecification->order;
            $result->push($dtoLanguageTechnicalSpecification);
        }

        return $result;
    }

    /**
     * Get excluded by group technical specification and language
     * 
     * @param int $idGroupDisplayTechnicalSpecification
     * @param int $idLanguage
     * 
     * @return DtoLanguageTechnicalSpecification
     */
    public static function getExlucedInGroup(int $idGroupDisplayTechnicalSpecification, int $idLanguage)
    {
        $result = collect();

        foreach (DB::select('   SELECT *
                                FROM technicals_specifications
                                WHERE deleted_at IS NULL
                                AND id NOT IN (
                                                SELECT technical_specification_id
                                                FROM groups_display_technical_specification_tech_spec
                                                WHERE group_display_technical_specification_id = ' . $idGroupDisplayTechnicalSpecification . ')')
            as $groupDisplayTechnicalSpecificationTechnicalSpecification) {
            $dtoLanguageTechnicalSpecification = new DtoLanguageTechnicalSpecification();
            $dtoLanguageTechnicalSpecification->id = $groupDisplayTechnicalSpecificationTechnicalSpecification->id;
            $dtoLanguageTechnicalSpecification->description = LanguageTechnicalSpecificationBL::getByIdTechnicalSpecificationAndLanguage($groupDisplayTechnicalSpecificationTechnicalSpecification->id, $idLanguage);
            $result->push($dtoLanguageTechnicalSpecification);
        }

        return $result;
    }

    /**
     * Get by technical specification and language
     * 
     * @param int $idTechnicalSpecification
     * @param int $idLanguage
     * 
     * @return DtoLanguageTechnicalSpecification
     */
    public static function getInlucedInTechnicalSpecification(int $idTechnicalSpecification, int $idLanguage)
    {
        $result = collect();

        foreach (GroupDisplayTechnicalSpecificationTechnicalSpecification::where('technical_specification_id', $idTechnicalSpecification)->get() as $groupDisplayTechnicalSpecificationTechnicalSpecification) {
            $dtoLanguageTechnicalSpecification = new DtoLanguageTechnicalSpecification();
            $dtoLanguageTechnicalSpecification->id = $groupDisplayTechnicalSpecificationTechnicalSpecification->id;
            $dtoLanguageTechnicalSpecification->description = GroupDisplayTechnicalSpecificationLanguageBL::getByGroupDisplayTechnicalSpecificationAndLanguage($groupDisplayTechnicalSpecificationTechnicalSpecification->group_display_technical_specification_id, $idLanguage);
            $result->push($dtoLanguageTechnicalSpecification);
        }

        return $result;
    }

    /**
     * Get excluded by technical specification and language
     * 
     * @param int $idTechnicalSpecification
     * @param int $idLanguage
     * 
     * @return DtoLanguageTechnicalSpecification
     */
    public static function getExlucedInTechnicalSpecification(int $idTechnicalSpecification, int $idLanguage)
    {
        $result = collect();

        foreach (DB::select('   SELECT *
                                FROM groups_display_technicals_specifications
                                WHERE id NOT IN (
                                                SELECT group_display_technical_specification_id
                                                FROM groups_display_technical_specification_tech_spec
                                                WHERE technical_specification_id = ' . $idTechnicalSpecification . ')')
            as $groupDisplayTechnicalSpecificationTechnicalSpecification) {
            $dtoLanguageTechnicalSpecification = new DtoLanguageTechnicalSpecification();
            $dtoLanguageTechnicalSpecification->id = $groupDisplayTechnicalSpecificationTechnicalSpecification->id;
            $dtoLanguageTechnicalSpecification->description = GroupDisplayTechnicalSpecificationLanguageBL::getByGroupDisplayTechnicalSpecificationAndLanguage($groupDisplayTechnicalSpecificationTechnicalSpecification->id, $idLanguage);
            $result->push($dtoLanguageTechnicalSpecification);
        }

        return $result;
    }

    #endregion GET

    #region INSERT

    /**
     * Add technical specification
     * 
     * @param int idGroupDisplayTechnicalSpecification
     * @param int idTechnicalSpecification
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int id
     */
    public static function insert(int $idGroupDisplayTechnicalSpecification, int $idTechnicalSpecification, int $idFunctionality, int $idUser, int $idLanguage)
    {
        static::_validateDataInsert($idGroupDisplayTechnicalSpecification, $idTechnicalSpecification, $idFunctionality, $idUser, $idLanguage);

        $groupDisplayTechnicalSpecificationTechnicalSpecification = GroupDisplayTechnicalSpecificationTechnicalSpecification::where('group_display_technical_specification_id', $idGroupDisplayTechnicalSpecification)
            ->where('technical_specification_id', $idTechnicalSpecification)->first();

        if (is_null($groupDisplayTechnicalSpecificationTechnicalSpecification)) {
            $order = GroupDisplayTechnicalSpecificationTechnicalSpecification::where('group_display_technical_specification_id', $idGroupDisplayTechnicalSpecification)->max('order');
            if (is_null($order)) {
                $order = 1;
            } else {
                $order = $order + 1;
            }
            $groupDisplayTechnicalSpecificationTechnicalSpecification = new GroupDisplayTechnicalSpecificationTechnicalSpecification();
            $groupDisplayTechnicalSpecificationTechnicalSpecification->group_display_technical_specification_id = $idGroupDisplayTechnicalSpecification;
            $groupDisplayTechnicalSpecificationTechnicalSpecification->technical_specification_id = $idTechnicalSpecification;
            $groupDisplayTechnicalSpecificationTechnicalSpecification->order = $order;
            $groupDisplayTechnicalSpecificationTechnicalSpecification->save();
        }

        return $groupDisplayTechnicalSpecificationTechnicalSpecification->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Add technical specification
     * 
     * @param int id
     * @param int order
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int id
     */
    public static function update(int $id, int $order, int $idFunctionality, int $idUser, int $idLanguage)
    {
        static::_validateDataUpdate($id, $idFunctionality, $idUser, $idLanguage);

        $groupDisplayTechnicalSpecificationTechnicalSpecification = GroupDisplayTechnicalSpecificationTechnicalSpecification::find($id);
        $groupDisplayTechnicalSpecificationTechnicalSpecification->order = $order;
        $groupDisplayTechnicalSpecificationTechnicalSpecification->save();
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int $id
     */
    public static function delete(int $id, int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(GroupDisplayTechnicalSpecificationTechnicalSpecification::find($id))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupDisplayTechincalSpecificationTechincalSpecificationNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        GroupDisplayTechnicalSpecificationTechnicalSpecification::destroy($id);
    }

    /**
     * Delete by technical specification
     * 
     * @param int idTechnicalSpecification
     */
    public static function deleteByTechnicalSpecification($idTechnicalSpecification)
    {
        GroupDisplayTechnicalSpecificationTechnicalSpecification::where('technical_specification_id', $idTechnicalSpecification)->delete();
    }

    /**
     * Delete by grouptechnical specification
     * 
     * @param int idGroupDisplayTechnicalSpecification
     */
    public static function deleteByGroupTechnicalSpecification($idGroupDisplayTechnicalSpecification)
    {
        GroupDisplayTechnicalSpecificationTechnicalSpecification::where('group_display_technical_specification_id', $idGroupDisplayTechnicalSpecification)->delete();
    }

    #endregion DELETE
}

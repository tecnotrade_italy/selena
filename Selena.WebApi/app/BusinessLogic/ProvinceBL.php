<?php

namespace App\BusinessLogic;

use App\Contact;
use App\DtoModel\DtoProvince;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\LanguageProvince;
use App\LanguageNation;
use App\Nation;
use App\Province;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProvinceBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Province::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->nation_id)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PostalCodeNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->abbreviation)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SurnameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    /**
     * Conver to model
     * 
     * @param Request request
     * 
     * @return Contact
     */
    private static function _convertToModel(Request $request)
    {
        $province = new Province();

        if (isset($request->id)) {
            $province->id = $request->id;
        }

        if (isset($request->nation_id)) {
            $province->nation_id = $request->nation_id;
        }

        if (isset($request->abbreviation)) {
            $province->abbreviation = $request->abbreviation;
        }

        if (isset($request->description)) {
            $province->description = $request->description;
        }

        return $province;
    }

    /**
     * Convert to dto
     * 
     * @param Province province
     * @param int idLanguage
     * 
     * @return DtoProvince
     */
    private static function _convertToDto(Province $province)
    {
        $dtoProvince = new DtoProvince();
        $dtoProvince->id = $province->id;
        $dtoProvince->nation_id = $province->nation_id;
        $dtoProvince->abbreviation = $province->abbreviation;
        $dtoProvince->description = LanguageProvince::where('province_id', $province->id)->first()->description;

        $dtoProvince->nation = LanguageNation::where('nation_id', $province->nation_id)->first()->description;
        return $dtoProvince;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get
     *
     * @param String $search
     * @return LanguageProvince
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return LanguageProvince::select('province_id as id', 'description')->get();
        } else {
            return LanguageProvince::where('description', 'ilike','%'. $search . '%')->select('province_id as id', 'description')->get();
        }
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoProvince
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (LanguageProvince::where('language_id',$idLanguage)->get() as $provinceAll) {
            $dtoProvince = new DtoProvince();
            $province = Province::where('id', $provinceAll->province_id)->first();
            $dtoProvince->id = $province->id;
            $dtoProvince->abbreviation = $province->abbreviation;
            $dtoProvince->description = $provinceAll->description;
            $dtoProvince->nation_id = $province->nation_id;

            $descriptionNation = LanguageNation::where('nation_id', $province->nation_id)->first();
            $dtoProvince->nation = $descriptionNation->description;

            $result->push($dtoProvince); 
        }
        return $result;
    }

    /**
     * Get by id
     * 
     * @param int id
     * 
     * @return DtoProvince
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Province::find($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);
        DB::beginTransaction();

        try {
            $province = new Province();
            $province->abbreviation = $request->abbreviation;
            $province->nation_id = $request->nation_id;
            $province->created_id = $request->$idUser;
            $province->save();

            $languageProvince = new LanguageProvince();
            $languageProvince->language_id = $idLanguage;
            $languageProvince->province_id = $province->id;
            $languageProvince->description = $request->description;
            $languageProvince->created_id = $request->$idUser;
            $languageProvince->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $province->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoProvince
     * @param int idLanguage
     */
    public static function update(Request $dtoProvince, int $idLanguage)
    {
        static::_validateData($dtoProvince, $idLanguage, DbOperationsTypesEnum::UPDATE);
        
        $provinceOnDb = Province::find($dtoProvince->id);
        $languageProvinceOnDb = LanguageProvince::where('province_id', $dtoProvince->id)->first();
        //$descriptionNation = LanguageNation::where('nation_id', $dtoProvince->nation_id)->first();

        DB::beginTransaction();

        try {
            $provinceOnDb->nation_id = $dtoProvince->nation_id;
            $provinceOnDb->nation_id = $dtoProvince->nation_id;
            $provinceOnDb->abbreviation = $dtoProvince->abbreviation;
            $provinceOnDb->save();

            $languageProvinceOnDb->language_id = $idLanguage;
            $languageProvinceOnDb->province_id = $dtoProvince->id;
            $languageProvinceOnDb->description = $dtoProvince->description;
            $languageProvinceOnDb->save();
        
            DB::commit();

        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $languageProvince = LanguageProvince::where('province_id', $id)->first();

        if (is_null($languageProvince)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $languageProvince->delete();

        $province = Province::find($id);

        if (is_null($province)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $province->delete();
    }

    #endregion DELETE
}

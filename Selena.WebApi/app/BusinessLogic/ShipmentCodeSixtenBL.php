<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoSelect2;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\Helpers\LogHelper;
use App\ShipmentCodeSixten;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class ShipmentCodeSixtenBL
{
    #region PRIVATE

    /**
     * Check function enabled
     * 
     * @param int idFunctionlity
     * @param int idUser
     * @param int idLanguage
     */
    private static function _checkFunctionEnabled(int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (is_null($idFunctionality)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Functionality::find($idFunctionality))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get select
     *
     * @param string $search
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoSelect2
     */
    public static function getSelect(string $search, int $idFunctionality, int $idUser, int $idLanguage)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        return ShipmentCodeSixten::where('description', 'ilike', $search . '%')->select('id', 'description')->get();
    }

    /**
     * Get select
     *
     * @param int $idCustomer
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoSelect2
     */
    public static function getByCustomerSixten(int $idCustomer, int $idFunctionality, int $idUser, int $idLanguage)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $shipmentCodeSixten = DB::select('SELECT shipments_codes_sixtens.id, shipments_codes_sixtens.description
                                        FROM shipments_codes_sixtens
                                        INNER JOIN customers_sixten ON shipments_codes_sixtens.id = customers_sixten.shipment_code_sixten_id
                                        WHERE customers_sixten.customer_id = :idCustomer', ['idCustomer' => $idCustomer]);

        $result = new DtoSelect2();

        if (!is_null($shipmentCodeSixten)) {
            $result->id = $shipmentCodeSixten[0]->id;
            $result->description = $shipmentCodeSixten[0]->description;
        }

        return $result;
    }

    #endregion GET
}

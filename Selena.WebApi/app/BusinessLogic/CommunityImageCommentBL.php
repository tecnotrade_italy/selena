<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoCommunityImageComment;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\CommunityImageComment;
use App\CommunityAction;

use App\BusinessLogic\CommunityActionBL;
use Exception;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;

class CommunityImageCommentBL
{
    #region PRIVATE

    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request dtoCommunityImageComment
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationsTypesEnum)
    {

       

    }

    /**
     * Convert to dto
     * 
     * @param CommunityImageComment CommunityImageComment
     * @return DtoCommunityImageComment
     */
    private static function _convertToDto($CommunityImageComment)
    {
        $dtoCommunityImageComment = new DtoCommunityImageComment();

        if (isset($CommunityImageComment->id)) {
            $dtoCommunityImageComment->id = $CommunityImageComment->id;
        }

        if (isset($CommunityImageComment->user_id)) {
            $dtoCommunityImageComment->user_id = $CommunityImageComment->user_id;
        }

        if (isset($CommunityImageComment->image_id)) {
            $dtoCommunityImageComment->image_id = $CommunityImageComment->image_id;
        }

        if (isset($CommunityImageComment->comment_datetime)) {
            $dtoCommunityImageComment->comment_datetime = $CommunityImageComment->comment_datetime;
        }

        if (isset($CommunityImageComment->comment)) {
            $dtoCommunityImageComment->comment = $CommunityImageComment->comment;
        }

        if (isset($CommunityImageComment->comment_id_response)) {
            $dtoCommunityImageComment->comment_id_response = $CommunityImageComment->comment_id_response;
        }

        if (isset($CommunityImageComment->invisible)) {
            $dtoCommunityImageComment->invisible = $CommunityImageComment->invisible;
        }

        if (isset($CommunityImageComment->comment_admin_moderation)) {
            $dtoCommunityImageComment->comment_admin_moderation = $CommunityImageComment->comment_admin_moderation;
        }


        return $dtoCommunityImageComment;
    }


    /**
     * Convert to VatType
     * 
     * @param dtoCommunityImageComment dtoCommunityImageComment
     * 
     * @return CommunityImageComment
     */
    private static function _convertToModel($dtoCommunityImageComment)
    {
        $CommunityImageComment = new CommunityImageComment();

        if (isset($dtoCommunityImageComment->id)) {
            $CommunityImageComment->id = $dtoCommunityImageComment->id;
        }

        if (isset($dtoCommunityImageComment->user_id)) {
            $CommunityImageComment->user_id = $dtoCommunityImageComment->user_id;
        }

        if (isset($dtoCommunityImageComment->image_id)) {
            $CommunityImageComment->image_id = $dtoCommunityImageComment->image_id;
        }

        if (isset($dtoCommunityImageComment->comment_datetime)) {
            $CommunityImageComment->comment_datetime = $dtoCommunityImageComment->comment_datetime;
        }

        if (isset($dtoCommunityImageComment->comment)) {
            $CommunityImageComment->comment = $dtoCommunityImageComment->comment;
        }

        if (isset($dtoCommunityImageComment->comment_id_response)) {
            $CommunityImageComment->comment_id_response = $dtoCommunityImageComment->comment_id_response;
        }

        if (isset($dtoCommunityImageComment->invisible)) {
            $CommunityImageComment->invisible = $dtoCommunityImageComment->invisible;
        }

        if (isset($dtoCommunityImageComment->comment_admin_moderation)) {
            $CommunityImageComment->comment_admin_moderation = $dtoCommunityImageComment->comment_admin_moderation;
        }


        return $CommunityImageComment;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCommunityction
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(CommunityImageComment::find($id));
    }

    /*
     * Get all
     * 
     * @return DtoCommunityImageComment
     */
    public static function getAll()
    {

        $result = collect(); 

        foreach (CommunityImageComment::orderBy('id','DESC')->get() as $communityImageComment) {

            $DtoCommunityImageComment = new DtoCommunityImageComment();

            $DtoCommunityImageComment->id = $communityImageComment->id;
            $DtoCommunityImageComment->user_id = $communityImageComment->user_id;
            $DtoCommunityImageComment->image_id = $communityImageComment->image_id;
            $DtoCommunityImageComment->comment_datetime = $communityImageComment->comment_datetime;
            $DtoCommunityImageComment->comment = $communityImageComment->comment;
            $DtoCommunityImageComment->comment_id_response = $communityImageComment->comment_id_response;
            $DtoCommunityImageComment->invisible = $communityImageComment->invisible;
            $DtoCommunityImageComment->comment_admin_moderation = $communityImageComment->comment_admin_moderation;

            $result->push($DtoCommunityImageComment); 
        }
        return $result;

    }
    /**
     * getHtmlSubCommenti
     *
     * @param int $idPhoto
     * @param int $idResponseComment
     * @param string $url
     */
    private static function getHtmlSubCommenti($idPhoto, $idComment){
        $html = "";
        $htmlApp = "";
        $htmlTemp = "";
        
        $htmlApp = ContentBL::getByCode('Community_Image_Comment');

        foreach (DB::select(
            'SELECT cic.id, comment_datetime, comment, concat(users_datas."name", \' \', users_datas.surname) as user_name, comment_id_response,
            users_datas.img as user_img, users_datas.link as user_link
            from community_images_comments cic 
            inner join users_datas on users_datas.user_id = cic.user_id 
            where image_id =:image_id AND comment_id_response =  ' . $idComment . ' order by comment_datetime desc'
            , ['image_id' => $idPhoto]
            
        ) as $comments){
            $htmlTemp = $htmlApp->content;
            $htmlTemp = str_replace('#id#', $comments->id,  $htmlTemp); 
            $htmlTemp = str_replace('#comment_datetime#', date_format(date_create($comments->comment_datetime), 'd/m/Y H:i'),  $htmlTemp); 
            $htmlTemp = str_replace('#comment#', $comments->comment,  $htmlTemp); 
            $htmlTemp = str_replace('#user_name#', $comments->user_name,  $htmlTemp); 
            $htmlTemp = str_replace('#user_link#', $comments->user_link,  $htmlTemp); 

            if(!is_null($comments->user_img)){
                $htmlTemp = str_replace('#user_img#', $comments->user_img,  $htmlTemp); 
            }else{
                $htmlTemp = str_replace('#user_img#', '/storage/images/default-user-icon.jpeg',  $htmlTemp); 
            }
            
            $htmlTemp = str_replace('#user_link#', $comments->user_link,  $htmlTemp); 
            $htmlTemp = str_replace('#image_id#', $idPhoto, $htmlTemp);

            $subComment = CommunityImageComment::where('image_id', $idPhoto)->where('comment_id_response', $comments->id)->get()->first();

            if(isset($subComment)){
                $htmlSubCommento = static::getHtmlSubCommenti($idPhoto, $comments->id);
                $htmlTemp = str_replace('#image_comments#', $htmlSubCommento, $htmlTemp);
            }else{
                $htmlTemp = str_replace('#image_comments#', '', $htmlTemp);
            }
           
            $html = $html . $htmlTemp;
           
        };

        return $html;
    }

    public static function getHtmlCommenti($idPhoto)
    {
        
        $html = "";
        $htmlTemp = "";
        $htmlApp = "";
        
        $htmlApp = ContentBL::getByCode('Community_Image_Comment');

        foreach (DB::select(
            'SELECT cic.id, comment_datetime, comment, concat(users_datas."name", \' \', users_datas.surname) as user_name, comment_id_response,
            users_datas.img as user_img, users_datas.link as user_link
            from community_images_comments cic 
            inner join users_datas on users_datas.user_id = cic.user_id 
            where image_id =:image_id AND comment_id_response is null
            order by comment_datetime desc'
            , ['image_id' => $idPhoto]
            
        ) as $comments){
            $htmlTemp = $htmlApp->content;
            $htmlTemp = str_replace('#id#', $comments->id,  $htmlTemp); 
            $htmlTemp = str_replace('#comment_datetime#', date_format(date_create($comments->comment_datetime), 'd/m/Y H:i'),  $htmlTemp); 
            $htmlTemp = str_replace('#comment#', $comments->comment,  $htmlTemp); 
            $htmlTemp = str_replace('#user_name#', $comments->user_name,  $htmlTemp); 
            $htmlTemp = str_replace('#user_link#', $comments->user_link,  $htmlTemp); 
            if(!is_null($comments->user_img)){
                $htmlTemp = str_replace('#user_img#', $comments->user_img,  $htmlTemp); 
            }else{
                $htmlTemp = str_replace('#user_img#', '/storage/images/default-user-icon.jpeg',  $htmlTemp); 
            }
            $htmlTemp = str_replace('#user_link#', $comments->user_link,  $htmlTemp); 
            $htmlTemp = str_replace('#image_id#', $idPhoto, $htmlTemp);

            $subComment = CommunityImageComment::where('image_id', $idPhoto)->where('comment_id_response', $comments->id)->get()->first();

            if(isset($subComment)){
                $htmlSubCommento = static::getHtmlSubCommenti($idPhoto, $comments->id);
                $htmlTemp = str_replace('#image_comments#', $htmlSubCommento, $htmlTemp);
            }else{
                $htmlTemp = str_replace('#image_comments#', '', $htmlTemp);
            }
           
            $html = $html . $htmlTemp;
        };

        return $html;
    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoCommunityImageComment
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request)
    {
        $return = 0;
        
        DB::beginTransaction();

        try {
            if(isset($request->idImage) && isset($request->idAction) && isset($request->userId)){
                //add row in community favorite image
                $communityComment = new CommunityImageComment();
                $communityComment->comment = $request->comment;
                $communityComment->comment_datetime = date("Y-m-d H:i:s");
                $communityComment->user_id = $request->userId;
                $communityComment->image_id = $request->idImage;
                $communityComment->created_id = $request->userId;
                if($request->responsId != "null"){
                    $communityComment->comment_id_response = $request->responsId;
                }
                $communityComment->save();

                //add row in community action by image_id and user_id and action_type_id 
                $idCommunityAction = CommunityActionBL::addAction($request->userId, $request->idImage, $request->idAction);

                //call function to recalculate rating of images by add the value of community_type_action
                if(isset($idCommunityAction)){
                    $actualRate = CommunityActionBL::addOrRemoveRateByActionId($idCommunityAction, true);
                    CommunityActionBL::sendNotificationByActionId($idCommunityAction, $communityComment->id);
                    $return = CommunityImageCommentBL::getHtmlCommenti($request->idImage);
                }
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $return;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoCommunityImageComment
     * @param int idLanguage
     */
    public static function update(Request $DtoCommunityImageComment)
    {
        static::_validateData($DtoCommunityImageComment, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $CommunityImageComment = CommunityImageComment::find($DtoCommunityImageComment->id);
        DBHelper::updateAndSave(static::_convertToModel($DtoCommunityImageComment), $CommunityImageComment);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id)
    {
        $CommunityImageComment = CommunityImageComment::find($id);

        if (is_null($CommunityImageComment)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $CommunityImageComment->delete();
    }

    #endregion DELETE
}

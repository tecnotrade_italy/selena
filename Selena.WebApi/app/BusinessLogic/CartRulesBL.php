<?php

namespace App\BusinessLogic;

use App\Carrier;
use App\DtoModel\DtoCartRules;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\CartRules;
use App\CartRulesCarriers;
use App\CartRulesCategories;
use App\CartRulesSpecificItems;
use App\CartRulesGroup;
use App\CartRulesItems;
use App\CartRulesUsers;
use App\CategoryLanguage;
use App\CartRulesProducers;
use App\CartRulesSuppliers;
use App\Producer;
use App\Supplier;
use App\DtoModel\DtoCarrier;
use App\DtoModel\DtoCartRulesCarrier;
use App\DtoModel\DtoCartRulesCategories;
use App\DtoModel\DtoCartRulesGroup;
use App\DtoModel\DtoCartRulesItems;
use App\DtoModel\DtoCartRulesUser;
use App\DtoModel\DtoCategoryLanguages;
use App\DtoModel\DtoCartRulesProducers;
use App\DtoModel\DtoItemLanguages;
use App\DtoModel\DtoRole;
use App\DtoModel\DtoUser;
use App\Helpers\LogHelper;
use App\ItemLanguage;
use App\Role;
use Illuminate\Support\Facades\DB;
use App\UsersDatas;
use App\Voucher;
use Exception;
use Illuminate\Http\Request;
use LogHeader;

class CartRulesBL
{
    #region PRIVATE

    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {

        if (is_null($request->name)|| $request->name=='') {
            throw new Exception("Il nome della regola è obbligatorio");
        }

        if (is_null($request->voucher_code)==false && (is_null($request->voucher_type_id)|| $request->voucher_type_id=='')) {
            throw new Exception("Il tipo di voucher è obbligatorio");
        }

        if (is_null($request->availability)==false) {
            if ($request->availability<0){
                throw new Exception("La disponibilità non può essere minore di ZERO");
            }
        }
        
        if (is_null($request->enabled_from)==false && is_null($request->enabled_to)==false) {
            if ($request->enabled_from>$request->enabled_to){
                throw new Exception("La data di fine validità non può essere minore di quella iniziale");
            }
        }
        
        if (is_null($request->minimum_quantity)==false) {
            if ($request->minimum_quantity<0){
                throw new Exception("La quantità minima non può essere minore di ZERO");
            }
        }

        if (is_null($request->maximum_quantity)==false) {
            if ($request->minimum_quantity<0){
                throw new Exception("La quantità massima non può essere minore di ZERO");
            }
        }

        if (is_null($request->minimum_quantity)==false && is_null($request->maximum_quantity)==false) {
            if ($request->minimum_quantity>$request->maximum_quantity){
                throw new Exception("La quantità massima non può essere minore di quella minima");
            }
        }

        if (is_null($request->minimum_order_amount)==false) {
            if ($request->minimum_order_amount<0){
                throw new Exception("L'importo minimo dell'ordine non può essere minore di ZERO");
            }
        }
        
        if ($request->discount_type==1||$request->discount_type==2) {
            if (is_null($request->discount_value)|| $request->discount_value==''|| $request->discount_value<=0){
                throw new Exception("Il valore dello sconto è errato");
            }
        }
        //se è uno sconto percentuale verifico che sia da zero a cento
        if ($request->discount_type==1) {
            if ($request->discount_value>100){
                throw new Exception("La percentuale dello sconto non può essere maggiore del 100%");
            }
        }

        //non è possibile avere clienti specifici e gruppi specifici....o uno o l'altro
        if ($request->apply_to_all_customers == 0 && $request->apply_to_all_groups == 0){
            throw new Exception("Lo sconto può esser fatto a clienti specifici o a gruppi specifici, non ad entrambi.");
        }

        //non è possibile avere articoli specifici e categorie articolo specifiche....o uno o l'altro
        if ($request->apply_to_all_items == 0 && $request->apply_to_all_categories_items == 0){
            throw new Exception("Lo sconto può esser fatto ad articoli specifici o a categorie specifiche, non ad entrambi.");
        }


        if ($request->free_shipping == 'True' && $request->discount_type<>0){
            throw new Exception("Non è possibile avere spese di spedizione gratuite e anche lo sconto.");
        }

        switch ($dbOperationType) {
            case DbOperationsTypesEnum::INSERT:

              /*  if (Voucher::where('code', $request->voucher_code)->count() > 0) {
                    throw new Exception("Codice voucher già esistente!");
                }*/

                break;

            case DbOperationsTypesEnum::UPDATE:
  
                break;
        }


  
    }

    private static function _convertToDto($cartRules)
    {
        $dtoCartRules = new DtoCartRules();

        if (isset($cartRules->id)) {
            $dtoCartRules->id = $cartRules->id;
        }

        if (isset($cartRules->name)) {
            $dtoCartRules->name = $cartRules->name;
        }

        if (isset($cartRules->description)) 
            $dtoCartRules->description = $cartRules->description;
        else
            $dtoCartRules->description = null;
        
        if (isset($cartRules->voucher_code)) 
            $dtoCartRules->voucher_code = $cartRules->voucher_code;
        else
            $dtoCartRules->voucher_code = null;
        
        if (isset($cartRules->voucher_type_id)) 
            $dtoCartRules->voucher_type_id = $cartRules->voucher_type_id;
        else
            $dtoCartRules->voucher_type_id = null;
        
        if (isset($cartRules->availability)) 
            $dtoCartRules->availability = $cartRules->availability;
        else
            $dtoCartRules->availability = null;

        if (isset($cartRules->availability_residual)) 
            $dtoCartRules->availability_residual = $cartRules->availability_residual;
        else
            $dtoCartRules->availability_residual = null;
        
        if (isset($cartRules->availability_per_user)) 
            $dtoCartRules->availability_per_user = $cartRules->availability_per_user;
        else
            $dtoCartRules->availability_per_user = null;
        
        if (isset($cartRules->priority)) 
            $dtoCartRules->priority = $cartRules->priority;
        else
            $dtoCartRules->priority = null;

        if (isset($cartRules->exclude_other_rules)) 
            $dtoCartRules->exclude_other_rules = $cartRules->exclude_other_rules;
        else
            $dtoCartRules->exclude_other_rules = null;
        
        if (isset($cartRules->online)) 
            $dtoCartRules->online = $cartRules->online;
        else
            $dtoCartRules->online = null;
        
        if (isset($cartRules->enabled_from)) 
            $dtoCartRules->enabled_from = $cartRules->enabled_from;
        else
            $dtoCartRules->enabled_from = null;
       
        if (isset($cartRules->enabled_to)) 
            $dtoCartRules->enabled_to = $cartRules->enabled_to;
        else
            $dtoCartRules->enabled_to = null;

        if (isset($cartRules->apply_to_all_customers)) 
            $dtoCartRules->apply_to_all_customers = $cartRules->apply_to_all_customers;
        else
            $dtoCartRules->apply_to_all_customers = null;
        
        if (isset($cartRules->apply_to_all_groups)) 
            $dtoCartRules->apply_to_all_groups = $cartRules->apply_to_all_groups;
        else
            $dtoCartRules->apply_to_all_groups = null;
        
        if (isset($cartRules->apply_to_all_carriers)) 
            $dtoCartRules->apply_to_all_carriers = $cartRules->apply_to_all_carriers;
        else
            $dtoCartRules->apply_to_all_carriers = null;
       
        if (isset($cartRules->apply_to_all_items)) 
            $dtoCartRules->apply_to_all_items = $cartRules->apply_to_all_items;
        else
            $dtoCartRules->apply_to_all_items = null;
       
        if (isset($cartRules->apply_to_all_categories_items)) 
            $dtoCartRules->apply_to_all_categories_items = $cartRules->apply_to_all_categories_items;
        else
            $dtoCartRules->apply_to_all_categories_items = null;
        
        if (isset($cartRules->apply_to_all_producers)) 
            $dtoCartRules->apply_to_all_producers = $cartRules->apply_to_all_producers;
        else
            $dtoCartRules->apply_to_all_producers = null;

        if (isset($cartRules->apply_to_all_suppliers)) 
            $dtoCartRules->apply_to_all_suppliers = $cartRules->apply_to_all_suppliers;
        else
            $dtoCartRules->apply_to_all_suppliers = null;
        
        if (isset($cartRules->minimum_quantity)) 
            $dtoCartRules->minimum_quantity = $cartRules->minimum_quantity;
        else
            $dtoCartRules->minimum_quantity = null;
        
        if (isset($cartRules->maximum_quantity)) 
            $dtoCartRules->maximum_quantity = $cartRules->maximum_quantity;
        else
            $dtoCartRules->maximum_quantity = null;

        if (isset($cartRules->minimum_order_amount)) 
            $dtoCartRules->minimum_order_amount = $cartRules->minimum_order_amount;
        else
            $dtoCartRules->minimum_order_amount = null;
        
        if (isset($cartRules->taxes_excluded)) 
            $dtoCartRules->taxes_excluded = $cartRules->taxes_excluded;
        else
            $dtoCartRules->taxes_excluded = null;
        
        if (isset($cartRules->shipping_excluded)) 
            $dtoCartRules->shipping_excluded = $cartRules->shipping_excluded;
        else
            $dtoCartRules->shipping_excluded = null;
        
        if (isset($cartRules->free_shipping)) 
            $dtoCartRules->free_shipping = $cartRules->free_shipping;
        else
            $dtoCartRules->free_shipping = null;
        
        if (isset($cartRules->discount_type)) 
            $dtoCartRules->discount_type = $cartRules->discount_type;
        else
            $dtoCartRules->discount_type = null;
        
        $dtoCartRules->discount_value = 0;
        if (isset($cartRules->discount_value)) 
            $dtoCartRules->discount_value = $cartRules->discount_value;
            
        if (isset($cartRules->apply_discount_to)) 
            $dtoCartRules->apply_discount_to = $cartRules->apply_discount_to;
        else
            $dtoCartRules->apply_discount_to = null;
        
        if (isset($cartRules->add_to_cart_automatically)) 
            $dtoCartRules->add_to_cart_automatically = $cartRules->add_to_cart_automatically;
        else
            $dtoCartRules->add_to_cart_automatically = null;

        return $dtoCartRules;
    }


    private static function _convertToModel($dtoCartRules)
    {
        $cartRules = new CartRules();

        if (isset($dtoCartRules->id)) {
            $cartRules->id = $dtoCartRules->id;
        }

        if (isset($dtoCartRules->name)) {
            $cartRules->name = $dtoCartRules->name;
        }

        if (isset($dtoCartRules->description)) {
            $cartRules->description = $dtoCartRules->description;
        }else
            $cartRules->description = null;

        if (isset($dtoCartRules->voucher_code)) {
            $cartRules->voucher_code = $dtoCartRules->voucher_code;
        } else
            $cartRules->voucher_code = null;
        
        if (isset($dtoCartRules->voucher_type_id)) {
            $cartRules->voucher_type_id = $dtoCartRules->voucher_type_id;
        } else
            $cartRules->voucher_type_id = null;

        if (isset($dtoCartRules->availability)) {
            $cartRules->availability = $dtoCartRules->availability;
        } else
            $cartRules->availability = null;

        if (isset($dtoCartRules->availability_residual)) {
            $cartRules->availability_residual = $dtoCartRules->availability_residual;
        } else
            $cartRules->availability_residual = null;

        if (isset($dtoCartRules->availability_per_user)) {
            $cartRules->availability_per_user = $dtoCartRules->availability_per_user;
        } else
            $cartRules->availability_per_user = null;
        
        if (isset($dtoCartRules->priority)) {
            $cartRules->priority = $dtoCartRules->priority;
        } else
            $cartRules->priority = null;

        if (isset($dtoCartRules->exclude_other_rules)) {
            $cartRules->exclude_other_rules = $dtoCartRules->exclude_other_rules;
        } else
            $cartRules->exclude_other_rules = null;

        if (isset($dtoCartRules->online)) {
            $cartRules->online = $dtoCartRules->online;
        } else
            $cartRules->online = null;

        if (isset($dtoCartRules->enabled_from)) {
            $cartRules->enabled_from = $dtoCartRules->enabled_from;
        } else
            $cartRules->enabled_from = null;
        
        if (isset($dtoCartRules->enabled_to)) {
            $cartRules->enabled_to = $dtoCartRules->enabled_to;
        } else
            $cartRules->enabled_to = null;

        if (isset($dtoCartRules->apply_to_all_customers)) {
            $cartRules->apply_to_all_customers = $dtoCartRules->apply_to_all_customers;
        } else
            $cartRules->apply_to_all_customers = 0;

        if (isset($dtoCartRules->apply_to_all_groups)) {
            $cartRules->apply_to_all_groups = $dtoCartRules->apply_to_all_groups;
        } else
            $cartRules->apply_to_all_groups = 0;

        if (isset($dtoCartRules->apply_to_all_carriers)) {
            $cartRules->apply_to_all_carriers = $dtoCartRules->apply_to_all_carriers;
        } else
            $cartRules->apply_to_all_carriers = 0;
        
        if (isset($dtoCartRules->apply_to_all_items)) {
            $cartRules->apply_to_all_items = $dtoCartRules->apply_to_all_items;
        } else
            $cartRules->apply_to_all_items = 0;
        
        if (isset($dtoCartRules->apply_to_all_producers)) 
            $cartRules->apply_to_all_producers = $dtoCartRules->apply_to_all_producers;
        else
            $cartRules->apply_to_all_producers = 0;

        if (isset($dtoCartRules->apply_to_all_suppliers)) 
            $cartRules->apply_to_all_suppliers = $dtoCartRules->apply_to_all_suppliers;
        else
            $cartRules->apply_to_all_suppliers = 0;

        if (isset($dtoCartRules->apply_to_all_categories_items)) {
            $cartRules->apply_to_all_categories_items = $dtoCartRules->apply_to_all_categories_items;
        } else
            $cartRules->apply_to_all_categories_items = 0 ;

        if (isset($dtoCartRules->minimum_quantity)) {
            $cartRules->minimum_quantity = $dtoCartRules->minimum_quantity;
        } else
            $cartRules->minimum_quantity =  null;

        if (isset($dtoCartRules->maximum_quantity)) {
            $cartRules->maximum_quantity = $dtoCartRules->maximum_quantity;
        } else
            $cartRules->maximum_quantity = null;
        
        if (isset($dtoCartRules->minimum_order_amount)) {
            $cartRules->minimum_order_amount = $dtoCartRules->minimum_order_amount;
        } else
            $cartRules->minimum_order_amount = null;

        if (isset($dtoCartRules->taxes_excluded)) {
            $cartRules->taxes_excluded = $dtoCartRules->taxes_excluded;
        } else
            $cartRules->taxes_excluded = null;

        if (isset($dtoCartRules->shipping_excluded)) {
            $cartRules->shipping_excluded = $dtoCartRules->shipping_excluded;
        } else
            $cartRules->shipping_excluded = null;

        if (isset($dtoCartRules->free_shipping)) {
            $cartRules->free_shipping = $dtoCartRules->free_shipping;
        } else
            $cartRules->free_shipping = null;   
        
        if (isset($dtoCartRules->discount_type)) {
            $cartRules->discount_type = $dtoCartRules->discount_type;
        } else
            $cartRules->discount_type = 0;

        if (isset($dtoCartRules->discount_value)) {
            $cartRules->discount_value = $dtoCartRules->discount_value;
        }else
            $cartRules->discount_value = 0;

        if ($cartRules->free_shipping=='True')
            $cartRules->discount_value = 0;

        if (isset($dtoCartRules->apply_discount_to)) {
            $cartRules->apply_discount_to = $dtoCartRules->apply_discount_to;
        } else
            $cartRules->apply_discount_to = 0;    

        if (isset($dtoCartRules->add_to_cart_automatically)) {
            $cartRules->add_to_cart_automatically = $dtoCartRules->add_to_cart_automatically;
        } else
            $cartRules->add_to_cart_automatically = null;

        return $cartRules;
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(CartRules::find($id));
    }

    /**
     * Get all
     * 
     * @return DtoCartRules
     */
    public static function getAll()
    {
        return CartRules::all()->map(function ($cartRules) {
            return static::_convertToDto($cartRules);
        });
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request dtoCartRules
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $dtoCartRules, int $idLanguage)
    {
        static::_validateData($dtoCartRules, $idLanguage, DbOperationsTypesEnum::INSERT);
        $cartRules = static::_convertToModel($dtoCartRules);
        $cartRules->save();
        return $cartRules->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoCartRules
     * @param int idLanguage
     */
    public static function update(Request $dtoCartRules, int $idLanguage)
    {
        static::_validateData($dtoCartRules, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $cartRules = CartRules::find($dtoCartRules->id);
        DBHelper::updateAndSave(static::_convertToModel($dtoCartRules), $cartRules);
    }

    #endregion UPDATE


/**
     * getCarriersCartRolesSearch
     * 
     * 
     * @param request request
     */
    public static function getCarriersCartRolesSearch(request $request){
        $result = collect();
        $strWhere = ""; 
       
        $CarrierSearch = $request->carriersearch;
        if ($CarrierSearch != "") {
            $strWhere = $strWhere . ' AND c.name ilike \'%' . $CarrierSearch . '%\'';
        }
    foreach (DB::select(
        'SELECT c.id, c.name
                FROM carriers c
                WHERE c.id NOT IN (SELECT carrier_id FROM carts_rules_carriers WHERE cart_rule_id = :id)'.$strWhere,
            ['id' => $request->idCartRole]
            ) as $Carrier) {
                 $DtoCarrier = new DtoCarrier();
                 $DtoCarrier->id = $Carrier->id;
                 $DtoCarrier->name = $Carrier->name;   
                 $result->push($DtoCarrier);
        }
        return $result;
    }   


/**
     * getCarrierSearchNameCartRuleCarriers
     * 
     * 
     * @param request request
     */
    public static function getCarrierSearchNameCartRuleCarriers(request $request)
    {
        $result = collect();
        $strWhere = ""; 

        $CarrierSearchCartRule = $request->CarrierSearchCartRule;
        if ($CarrierSearchCartRule != "") {
            $strWhere = $strWhere . ' AND d.name ilike \'%' . $CarrierSearchCartRule . '%\'';
        }  

        $DtoCarrier = new DtoCarrier();
        foreach (DB::select(
           'SELECT c.id, c.cart_rule_id, c.carrier_id, d.name
           FROM carts_rules_carriers c
           inner join carriers as d on c.carrier_id = d.id   
            WHERE c.cart_rule_id = :idCartCartRole'.$strWhere,
            ['idCartCartRole' =>  $request->idCartRole]
            ) as $CartRulesCarriers) {
            $DtoCartRulesCarriers = new DtoCartRulesCarrier();
            $DtoCartRulesCarriers->id = $CartRulesCarriers->id;
            $DtoCartRulesCarriers->cart_rule_id = $CartRulesCarriers->cart_rule_id;
            $DtoCartRulesCarriers->carrier_id = $CartRulesCarriers->carrier_id;
            $DtoCartRulesCarriers->NameCarrier= Carrier::where('id', $CartRulesCarriers->carrier_id)->first()->name;
            $DtoCarrier->selectCarriersExist->push($DtoCartRulesCarriers);
        }  
        $result->push($DtoCarrier);

        return $result; 
}

/**
     * getGroupCartRolesSearch
     * 
     * 
     * @param request request
     */
    public static function getGroupCartRolesSearch(request $request){
        $result = collect();
        $strWhere = ""; 
       
        $GroupSearch = $request->groupsearch;
        if ($GroupSearch != "") {
            $strWhere = $strWhere . ' AND r.name ilike \'%' . $GroupSearch . '%\'';
        }
    foreach (DB::select(
        'SELECT r.id, r.name
                FROM roles r
                WHERE r.id NOT IN (SELECT group_id FROM carts_rules_groups WHERE cart_rule_id = :id)'.$strWhere,
            ['id' => $request->idCartRole]
            ) as $Roles) {
                 $DtoRole = new DtoRole();
                 $DtoRole->id = $Roles->id;
                 $DtoRole->name = $Roles->name;   
                $result->push($DtoRole);
        }
        return $result;
    }   


/**
     * getGroupSearchNameCartRuleGroup
     * 
     * 
     * @param request request
     */
    public static function getGroupSearchNameCartRuleGroup(request $request)
    {
        $result = collect();
        $strWhere = ""; 

        $GroupSearchCartRule = $request->GroupSearchCartRule;
        if ($GroupSearchCartRule != "") {
            $strWhere = $strWhere . ' AND r.name ilike \'%' . $GroupSearchCartRule . '%\'';
        }  

        $DtoRole = new DtoRole();
        foreach (DB::select(
           'SELECT c.id, c.cart_rule_id, c.group_id, r.name
           FROM carts_rules_groups c
           inner join roles as r on c.group_id = r.id   
            WHERE c.cart_rule_id = :idCartCartRole'.$strWhere,
            ['idCartCartRole' =>  $request->idCartRole]
            ) as $CartRulesGroup) {
            $DtoCartRulesGroup = new DtoCartRulesGroup();
            $DtoCartRulesGroup->id = $CartRulesGroup->id;
            $DtoCartRulesGroup->cart_rule_id = $CartRulesGroup->cart_rule_id;
            $DtoCartRulesGroup->group_id = $CartRulesGroup->group_id;
            $DtoCartRulesGroup->NameGroup= Role::where('id', $CartRulesGroup->group_id)->first()->name;
            $DtoRole->selectGroupExist->push($DtoCartRulesGroup);
      }  
        $result->push($DtoRole);

        return $result; 
    }


/**
     * getUserCartRolesSearch
     * 
     * 
     * @param request request
     */
    public static function getUserCartRolesSearch(request $request)
    {
        $result = collect();
        $strWhere = "";

        $UserSearch = $request->search;

         if ($UserSearch != "") {
            $strWhere = $strWhere . ' AND c.business_name ilike \'%' . $UserSearch . '%\'';
        }

        foreach (DB::select(
            '   SELECT c.user_id, c.business_name
                FROM users_datas c
                WHERE c.user_id NOT IN (SELECT user_id FROM carts_rules_users WHERE cart_rule_id = :id)'.$strWhere,     
            ['id' => $request->idCartRole]
        ) as $UsersDatas) {

            $DtoUser = new DtoUser();
            $DtoUser->user_id = $UsersDatas->user_id;
            $DtoUser->business_name = $UsersDatas->business_name;
            $result->push($DtoUser);
        }
        return $result;
    } 


    /**
     * getUserCartProducersSearch
     * 
     * 
     * @param request request
     */
    public static function getUserCartProducersSearch(request $request)
    {
        $result = collect();
        $strWhere = "";

        $UserSearch = $request->search;

         if ($UserSearch != "") {
            $strWhere = $strWhere . ' AND c.business_name ilike \'%' . $UserSearch . '%\'';
        }

        foreach (DB::select(
            '   SELECT c.prdocuer_id, c.business_name
                FROM producer c
                WHERE c.prdocuer_id NOT IN (SELECT user_id FROM carts_rules_producers WHERE cart_rule_id = :id)'.$strWhere,     
            ['id' => $request->idCartRole]
        ) as $UsersDatas) {

            $DtoUser = new DtoUser();
            $DtoUser->user_id = $UsersDatas->prdocuer_id;
            $DtoUser->business_name = $UsersDatas->business_name;
            $result->push($DtoUser);
        }
        return $result;
    } 

/**
     * getUserSearchBusinessNameCartRuleUser
     * 
     * 
     * @param request request
     */
    public static function getUserSearchBusinessNameCartRuleUser(request $request)
    {
        $result = collect();
        $strWhere = ""; 
        $UserSearchBusinessNameCartRuleUser = $request->UserSearchBusinessNameCartRuleUser; 

       if ($UserSearchBusinessNameCartRuleUser != "") {
            $strWhere = $strWhere . ' AND c.business_name ilike \'%' . $UserSearchBusinessNameCartRuleUser . '%\'';
        }  
        $DtoUser = new DtoUser();
          foreach (DB::select(
              'SELECT s.id, s.cart_rule_id, c.user_id, c.business_name  
              FROM carts_rules_users s 
              inner join users_datas as c on c.user_id = s.user_id
              WHERE s.cart_rule_id = :idCartCartRole'.$strWhere,
               ['idCartCartRole' =>  $request->idCartRole]
        ) as $CartRulesUsers) {
        $DtoCartRulesUser = new DtoCartRulesUser();
        $DtoCartRulesUser->id = $CartRulesUsers->id;
        $DtoCartRulesUser->user_id = $CartRulesUsers->user_id;
        $DtoCartRulesUser->cart_rule_id = $CartRulesUsers->cart_rule_id;
        $DtoCartRulesUser->User= UsersDatas::where('user_id', $CartRulesUsers->user_id)->first()->business_name;
        $DtoUser->selectUserExist->push($DtoCartRulesUser);
      }  
        $result->push($DtoUser);

        return $result; 
    }
 


    /**
     * getGroupSelectedAndNot
     * 
     * 
     * @param int id
     */
    public static function getGroupSelectedAndNot(int $id)
    {
          $result = collect();
           foreach (DB::select(
        'SELECT r.id, r.name
                FROM roles r
                WHERE r.id NOT IN (SELECT group_id FROM carts_rules_groups WHERE cart_rule_id = :id)',
            ['id' => $id]
            ) as $Roles) {
                 $DtoRole = new DtoRole();
                 $DtoRole->id = $Roles->id;
                 $DtoRole->name = $Roles->name;                

                  foreach (CartRulesGroup::where('cart_rule_id', $id)->get() as $CartRulesGroup) {
        $DtoCartRulesGroup = new DtoCartRulesGroup();
        $DtoCartRulesGroup->id = $CartRulesGroup->id;
        $DtoCartRulesGroup->cart_rule_id = $CartRulesGroup->cart_rule_id;
        $DtoCartRulesGroup->group_id = $CartRulesGroup->group_id;
        $DtoCartRulesGroup->NameGroup= Role::where('id', $CartRulesGroup->group_id)->first()->name;
        $DtoRole->selectGroupExist->push($DtoCartRulesGroup);
            }   
        $result->push($DtoRole);
        }
        return $result;
    } 
    #endregion getGroupSelectedAndNot

   /**
     * getUserSelectedAndNot
     * 
     * 
     * @param int id
     */
    public static function getItemsSelectedAndNot(int $id)
    {
       $result = collect();
        foreach (DB::select(
            '   SELECT i.id, i.item_id, i.description
                FROM items_languages i
                WHERE i.item_id NOT IN (SELECT item_id FROM carts_rules_items WHERE cart_rule_id = :id)',
            ['id' => $id]
        ) as $ItemsLanguages) {
            $DtoItemLanguages = new DtoItemLanguages();
            $DtoItemLanguages->item_id = $ItemsLanguages->item_id;
            $DtoItemLanguages->description = $ItemsLanguages->description;

        foreach (CartRulesItems::where('cart_rule_id', $id)->get() as $CartRulesItems) {
                $DtoCartRulesItems = new DtoCartRulesItems();
                $DtoCartRulesItems->id = $CartRulesItems->id;
                $DtoCartRulesItems->cart_rule_id = $CartRulesItems->cart_rule_id;
                $DtoCartRulesItems->item_id = $CartRulesItems->item_id;
                $DtoCartRulesItems->NameItem= ItemLanguage::where('item_id', $CartRulesItems->item_id)->first()->description;
                $DtoItemLanguages->selectItemExist->push($DtoCartRulesItems);
            } 
        $result->push($DtoItemLanguages);
        }
        return $result;
    }
    #endregion getItemsSelectedAndNot

 /**
     * getCategoriesSelectedAndNot
     * 
     * 
     * @param int id
     */
    public static function getCategoriesSelectedAndNot(int $id)
    {
       $result = collect();
        foreach (DB::select(
            '   SELECT c.id, c.category_id, c.description
                FROM categories_languages c
                WHERE c.category_id NOT IN (SELECT category_item_id FROM carts_rules_categories_items WHERE cart_rule_id = :id)',
            ['id' => $id]
        ) as $CategoriesLanguages) {
            $DtoCategoryLanguages = new DtoCategoryLanguages();
            $DtoCategoryLanguages->category_id = $CategoriesLanguages->category_id;
            $DtoCategoryLanguages->description = $CategoriesLanguages->description;

        foreach (CartRulesCategories::where('cart_rule_id', $id)->get() as $CartRulesCategories) {
                $DtoCartRulesCategories = new DtoCartRulesCategories();
                $DtoCartRulesCategories->id = $CartRulesCategories->id;
                $DtoCartRulesCategories->cart_rule_id = $CartRulesCategories->cart_rule_id;
                $DtoCartRulesCategories->category_item_id = $CartRulesCategories->category_item_id;
                $DtoCartRulesCategories->DescriptionCategories= CategoryLanguage::where('category_id', $CartRulesCategories->category_item_id)->first()->description;
                $DtoCategoryLanguages->selectCategoriesExist->push($DtoCartRulesCategories);
           } 
        $result->push($DtoCategoryLanguages);
        }
        return $result;
    }
    #endregion getCategoriesSelectedAndNot

    /**
     * getProducersSelectedAndNot
     * 
     * 
     * @param int id
     */
    public static function getProducersSelectedAndNot(int $id)
    {
       $result = collect();
        foreach (DB::select(
            '   SELECT c.id, c.business_name
                FROM producer c
                WHERE c.id NOT IN (SELECT producer_id FROM carts_rules_producers WHERE cart_rule_id = :id)',
            ['id' => $id]
        ) as $CategoriesLanguages) {
            $DtoCategoryLanguages = new DtoCategoryLanguages();
            $DtoCategoryLanguages->producer_id = $CategoriesLanguages->id;
            $DtoCategoryLanguages->description = $CategoriesLanguages->business_name;

        foreach (CartRulesProducers::where('cart_rule_id', $id)->get() as $CartRulesCategories) {
                $DtoCartRulesCategories = new DtoCartRulesProducers();
                $DtoCartRulesCategories->id = $CartRulesCategories->id;
                $DtoCartRulesCategories->cart_rule_id = $CartRulesCategories->cart_rule_id;
                $DtoCartRulesCategories->producer_id = $CartRulesCategories->producer_id;
                $DtoCartRulesCategories->DescriptionProducers= Producer::where('id', $CartRulesCategories->producer_id)->first()->business_name;
                $DtoCategoryLanguages->selectProducersExist->push($DtoCartRulesCategories);
           } 
        $result->push($DtoCategoryLanguages);
        }
        return $result;
    }
    #endregion getProducersSelectedAndNot

    /**
     * getSuppliersSelectedAndNot
     * 
     * 
     * @param int id
     */
    public static function getSuppliersSelectedAndNot(int $id)
    {
       $result = collect();
        foreach (DB::select(
            '   SELECT c.id, c.business_name
                FROM suppliers c
                WHERE c.id NOT IN (SELECT supplier_id FROM carts_rules_suppliers WHERE cart_rule_id = :id)',
            ['id' => $id]
        ) as $CategoriesLanguages) {
            $DtoCategoryLanguages = new DtoCategoryLanguages();
            $DtoCategoryLanguages->supplier_id = $CategoriesLanguages->id;
            $DtoCategoryLanguages->description = $CategoriesLanguages->business_name;

        foreach (CartRulesSuppliers::where('cart_rule_id', $id)->get() as $CartRulesCategories) {
                $DtoCartRulesCategories = new DtoCartRulesProducers();
                $DtoCartRulesCategories->id = $CartRulesCategories->id;
                $DtoCartRulesCategories->cart_rule_id = $CartRulesCategories->cart_rule_id;
                $DtoCartRulesCategories->supplier_id = $CartRulesCategories->supplier_id;
                $DtoCartRulesCategories->DescriptionSuppliers= Supplier::where('id', $CartRulesCategories->supplier_id)->first()->business_name;
                $DtoCategoryLanguages->selectSuppliersExist->push($DtoCartRulesCategories);
           } 
        $result->push($DtoCategoryLanguages);
        }
        return $result;
    }
    #endregion getSuppliersSelectedAndNot

    /**
     * getSpecificProductsSelectedAndNot
     * 
     * 
     * @param int id
     */
    public static function getSpecificProductsSelectedAndNot(int $id)
    {
       $result = collect();
        foreach (DB::select(
            '   SELECT c.id, c.item_id, c.description
                FROM items_languages c
                WHERE c.item_id NOT IN (SELECT item_id FROM cart_rules_specific_items WHERE carts_rules_id = :id)',
            ['id' => $id]
        ) as $CategoriesLanguages) {
            $DtoCategoryLanguages = new DtoCategoryLanguages();
            $DtoCategoryLanguages->item_id = $CategoriesLanguages->item_id;
            $DtoCategoryLanguages->description = $CategoriesLanguages->description;

        foreach (CartRulesSpecificItems::where('carts_rules_id', $id)->get() as $CartRulesCategories) {
                $DtoCartRulesCategories = new DtoCartRulesCategories();
                $DtoCartRulesCategories->id = $CartRulesCategories->id;
                $DtoCartRulesCategories->cart_rule_id = $CartRulesCategories->cart_rule_id;
                $DtoCartRulesCategories->item_id = $CartRulesCategories->item_id;
                $DtoCartRulesCategories->quantity = $CartRulesCategories->quantity;
                $DtoCartRulesCategories->discount_value = $CartRulesCategories->discount_value;
                $DtoCartRulesCategories->discount_percentage = $CartRulesCategories->discount_percentage;
                $DtoCartRulesCategories->add_to_cart_automatically = $CartRulesCategories->add_to_cart_automatically;
                $DtoCartRulesCategories->DescriptionCategories= ItemLanguage::where('item_id', $CartRulesCategories->item_id)->first()->description;
                $DtoCategoryLanguages->selectItemExist->push($DtoCartRulesCategories);
           } 
        $result->push($DtoCategoryLanguages);
        }
        return $result;
    }
    #endregion getSpecificProductsSelectedAndNot

/**
     * getSearchDescriptionCartRuleCategories
     * 
     * 
     * @param request request
     */
    public static function getSearchDescriptionCartRuleCategories(request $request)
    {
        $result = collect();
        $strWhere = ""; 
        $CategoriesSearchCartRule = $request->CategoriesSearchCartRule; 

       if ($CategoriesSearchCartRule != "") {
            $strWhere = $strWhere . ' AND i.description ilike \'%' . $CategoriesSearchCartRule . '%\'';
        }  
       $DtoCategoryLanguages = new DtoCategoryLanguages();
          foreach (DB::select(
              'SELECT s.id, s.cart_rule_id, s.category_item_id, i.description  
              FROM carts_rules_categories_items s 
              inner join categories_languages as i on s.category_item_id = i.category_id
              WHERE s.cart_rule_id = :idCartCartRole'.$strWhere,
               ['idCartCartRole' =>  $request->idCartRole]
        ) as $CartRulesCategories) {
                $DtoCartRulesCategories = new DtoCartRulesCategories();
                $DtoCartRulesCategories->id = $CartRulesCategories->id;
                $DtoCartRulesCategories->cart_rule_id = $CartRulesCategories->cart_rule_id;
                $DtoCartRulesCategories->category_item_id = $CartRulesCategories->category_item_id;
                $DtoCartRulesCategories->DescriptionCategories= CategoryLanguage::where('category_id', $CartRulesCategories->category_item_id)->first()->description;
                $DtoCategoryLanguages->selectCategoriesExist->push($DtoCartRulesCategories);
      }  
        $result->push($DtoCategoryLanguages);

        return $result; 
    }

    /**
     * getSearchDescriptionCartRuleProducers
     * 
     * 
     * @param request request
     */
    public static function getSearchDescriptionCartRuleProducers(request $request)
    {
        $result = collect();
        $strWhere = ""; 
        $CategoriesSearchCartRule = $request->ProducersSearchCartRule; 

       if ($CategoriesSearchCartRule != "") {
            $strWhere = $strWhere . ' AND i.business_name ilike \'%' . $CategoriesSearchCartRule . '%\'';
        }  
       $DtoCategoryLanguages = new DtoCategoryLanguages();
          foreach (DB::select(
              'SELECT s.id, s.cart_rule_id, s.producer_id, i.business_name  
              FROM carts_rules_producers s 
              inner join producer as i on s.producer_id = i.id
              WHERE s.cart_rule_id = :idCartCartRole'.$strWhere,
               ['idCartCartRole' =>  $request->idCartRole]
        ) as $CartRulesCategories) {
                $DtoCartRulesCategories = new DtoCartRulesCategories();
                $DtoCartRulesCategories->id = $CartRulesCategories->id;
                $DtoCartRulesCategories->cart_rule_id = $CartRulesCategories->cart_rule_id;
                $DtoCartRulesCategories->producer_id = $CartRulesCategories->producer_id;
                $DtoCartRulesCategories->DescriptionProducers= Producers::where('id', $CartRulesCategories->producer_id)->first()->business_name;
                $DtoCategoryLanguages->selectProducersExist->push($DtoCartRulesCategories);
      }  
        $result->push($DtoCategoryLanguages);

        return $result; 
    }


    /**
     * getSearchDescriptionCartRuleSuppliers
     * 
     * 
     * @param request request
     */
    public static function getSearchDescriptionCartRuleSuppliers(request $request)
    {
        $result = collect();
        $strWhere = ""; 
        $CategoriesSearchCartRule = $request->SuppliersSearchCartRule; 

       if ($CategoriesSearchCartRule != "") {
            $strWhere = $strWhere . ' AND i.business_name ilike \'%' . $CategoriesSearchCartRule . '%\'';
        }  
       $DtoCategoryLanguages = new DtoCategoryLanguages();
          foreach (DB::select(
              'SELECT s.id, s.cart_rule_id, s.supplier_id, i.business_name  
              FROM carts_rules_suppliers s 
              inner join suppliers as i on s.supplier_id = i.id
              WHERE s.cart_rule_id = :idCartCartRole'.$strWhere,
               ['idCartCartRole' =>  $request->idCartRole]
        ) as $CartRulesCategories) {
                $DtoCartRulesCategories = new DtoCartRulesCategories();
                $DtoCartRulesCategories->id = $CartRulesCategories->id;
                $DtoCartRulesCategories->cart_rule_id = $CartRulesCategories->cart_rule_id;
                $DtoCartRulesCategories->supplier_id = $CartRulesCategories->supplier_id;
                $DtoCartRulesCategories->DescriptionProducers= Supplier::where('id', $CartRulesCategories->supplier_id)->first()->business_name;
                $DtoCategoryLanguages->selectProducersExist->push($DtoCartRulesCategories);
      }  
        $result->push($DtoCategoryLanguages);

        return $result; 
    }

    
    

    /**
     * getSearchSpecificProductsSelectedCartRule
     * 
     * 
     * @param request request
     */
    public static function getSearchSpecificProductsSelectedCartRule(request $request)
    {
        $result = collect();
        $strWhere = ""; 
        $SpecificProductsSearchCartRule = $request->SpecificProductsSearchCartRule; 

       if ($SpecificProductsSearchCartRule != "") {
            $strWhere = $strWhere . ' AND i.description ilike \'%' . $SpecificProductsSearchCartRule . '%\'';
        }  
       $DtoCategoryLanguages = new DtoCategoryLanguages();
          foreach (DB::select(
              'SELECT s.id, s.carts_rules_id, s.item_id, i.description, s.quantity, s.discount_value, s.discount_percentage
              FROM cart_rules_specific_items s 
              inner join items_languages as i on s.item_id = i.item_id
              WHERE s.carts_rules_id = :idCartCartRole and i.language_id=1'. $strWhere,
               ['idCartCartRole' =>  $request->idCartRole]
        ) as $CartRulesCategories) {
                $DtoCartRulesCategories = new DtoCartRulesCategories();
                $DtoCartRulesCategories->id = $CartRulesCategories->id;
                $DtoCartRulesCategories->cart_rule_id = $CartRulesCategories->carts_rules_id;
                $DtoCartRulesCategories->item_id = $CartRulesCategories->item_id;
                $DtoCartRulesCategories->quantity = $CartRulesCategories->quantity;
                $DtoCartRulesCategories->discount_value = $CartRulesCategories->discount_value;
                $DtoCartRulesCategories->discount_percentage = $CartRulesCategories->discount_percentage;
                $DtoCartRulesCategories->DescriptionCategories= ItemLanguage::where('item_id', $CartRulesCategories->item_id)->first()->description;
                $DtoCategoryLanguages->selectItemExist->push($DtoCartRulesCategories);
      }  
        $result->push($DtoCategoryLanguages);

        return $result; 
    }


    /**
     * getCategoriesCartRolesSearch
     * 
     * 
     * @param request request
     */
    public static function getCategoriesCartRolesSearch(request $request)
    {
        $result = collect();
        $strWhere = "";

        $CategoriesSearch = $request->CategoriesSearch;

         if ($CategoriesSearch != "") {
            $strWhere = $strWhere . ' AND c.description ilike \'%' . $CategoriesSearch . '%\'';
        }

        foreach (DB::select(
            '   SELECT c.category_id, c.description
                FROM categories_languages c
                WHERE c.category_id NOT IN (SELECT category_item_id FROM carts_rules_categories_items WHERE cart_rule_id = :id)'.$strWhere,     
            ['id' => $request->idCartRole]
            ) as $CategoriesLanguages) {
            $DtoCategoryLanguages = new DtoCategoryLanguages();
            $DtoCategoryLanguages->category_id = $CategoriesLanguages->category_id;
            $DtoCategoryLanguages->description = $CategoriesLanguages->description;
            $result->push($DtoCategoryLanguages);
        }
        return $result;
    }

    /**
     * getProducersCartRolesSearch
     * 
     * 
     * @param request request
     */
    public static function getProducersCartRolesSearch(request $request)
    {
        $result = collect();
        $strWhere = "";

        $CategoriesSearch = $request->ProducersSearch;

         if ($CategoriesSearch != "") {
            $strWhere = $strWhere . ' AND c.business_name ilike \'%' . $CategoriesSearch . '%\'';
        }

        foreach (DB::select(
            '   SELECT c.id, c.business_name
                FROM producer c
                WHERE c.id NOT IN (SELECT producer_id FROM carts_rules_producers WHERE cart_rule_id = :id)'.$strWhere,     
            ['id' => $request->idCartRole]
            ) as $CategoriesLanguages) {
            $DtoCategoryLanguages = new DtoCategoryLanguages();
            $DtoCategoryLanguages->producer_id = $CategoriesLanguages->id;
            $DtoCategoryLanguages->description = $CategoriesLanguages->business_name;
            $result->push($DtoCategoryLanguages);
        }
        return $result;
    }

    /**
     * getSuppliersCartRolesSearch
     * 
     * 
     * @param request request
     */
    public static function getSuppliersCartRolesSearch(request $request)
    {
        $result = collect();
        $strWhere = "";

        $CategoriesSearch = $request->SuppliersSearch;

         if ($CategoriesSearch != "") {
            $strWhere = $strWhere . ' AND c.business_name ilike \'%' . $CategoriesSearch . '%\'';
        }

        foreach (DB::select(
            '   SELECT c.id, c.business_name
                FROM suppliers c
                WHERE c.id NOT IN (SELECT supplier_id FROM carts_rules_suppliers WHERE cart_rule_id = :id)'.$strWhere,     
            ['id' => $request->idCartRole]
            ) as $CategoriesLanguages) {
            $DtoCategoryLanguages = new DtoCategoryLanguages();
            $DtoCategoryLanguages->supplier_id = $CategoriesLanguages->id;
            $DtoCategoryLanguages->description = $CategoriesLanguages->business_name;
            $result->push($DtoCategoryLanguages);
        }
        return $result;
    }

    
    /**
     * getSpecificProductCartRolesSearch
     * 
     * 
     * @param request request
     */
    public static function getSpecificProductCartRolesSearch(request $request)
    {
        $result = collect();
        $strWhere = "";

        $SpecificProductSearch = $request->SpecificProductSearch;

         if ($SpecificProductSearch != "") {
            $strWhere = $strWhere . ' AND c.description ilike \'%' . $SpecificProductSearch . '%\'';
        }

        foreach (DB::select(
            '   SELECT c.id, c.item_id, c.description
                FROM items_languages c
                WHERE c.item_id NOT IN (SELECT item_id FROM cart_rules_specific_items WHERE carts_rules_id = :id)'.$strWhere,     
            ['id' => $request->idCartRole]
            ) as $CategoriesLanguages) {
            $DtoCategoryLanguages = new DtoCategoryLanguages();
            $DtoCategoryLanguages->item_id = $CategoriesLanguages->item_id;
            $DtoCategoryLanguages->description = $CategoriesLanguages->description;
            $result->push($DtoCategoryLanguages);
        }
        return $result;
    }

/**
     * getUserCartRolesSearch
     * 
     * 
     * @param request request
     */
    public static function getItemsCartRolesSearch(request $request)
    {
        $result = collect();
        $strWhere = "";

        $itemSearch = $request->itemSearch;

         if ($itemSearch != "") {
            $strWhere = $strWhere . ' AND i.description ilike \'%' . $itemSearch . '%\'';
        }

        foreach (DB::select(
            '   SELECT i.item_id, i.description
                FROM items_languages i
                WHERE i.item_id NOT IN (SELECT item_id FROM carts_rules_items WHERE cart_rule_id = :id)'.$strWhere,     
            ['id' => $request->idCartRole]
        ) as $ItemsLanguages) {
            $DtoItemLanguages = new DtoItemLanguages();
            $DtoItemLanguages->item_id = $ItemsLanguages->item_id;
            $DtoItemLanguages->description = $ItemsLanguages->description;
            $result->push($DtoItemLanguages);
        }
        return $result;
    } 



/**
     * getItemsSearchDescriptionCartRuleItems
     * 
     * 
     * @param request request
     */
    public static function getItemsSearchDescriptionCartRuleItems(request $request)
    {
        $result = collect();
        $strWhere = ""; 
        $ItemSearchCartRule = $request->ItemSearchCartRule; 

       if ($ItemSearchCartRule != "") {
            $strWhere = $strWhere . ' AND i.description ilike \'%' . $ItemSearchCartRule . '%\'';
        }  
        $DtoItemLanguages = new DtoItemLanguages();
          foreach (DB::select(
              'SELECT s.id, s.cart_rule_id, s.item_id, i.description  
              FROM carts_rules_items s 
              inner join items_languages as i on s.item_id = i.item_id
              WHERE s.cart_rule_id = :idCartCartRole'.$strWhere,
               ['idCartCartRole' =>  $request->idCartRole]
        ) as $CartRulesItems) {
                $DtoCartRulesItems = new DtoCartRulesItems();
                $DtoCartRulesItems->id = $CartRulesItems->id;
                $DtoCartRulesItems->cart_rule_id = $CartRulesItems->cart_rule_id;
                $DtoCartRulesItems->item_id = $CartRulesItems->item_id;
                $DtoCartRulesItems->NameItem= ItemLanguage::where('item_id', $CartRulesItems->item_id)->first()->description;
                $DtoItemLanguages->selectItemExist->push($DtoCartRulesItems);
      }  
        $result->push($DtoItemLanguages);

        return $result; 
    }
  /**
     * insertItemCartRole
     * 
      * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertItemCartRole(Request $request, int $idUser, int $idLanguage)
    {
         DB::beginTransaction();
          try {
          $CartRulesItems = new CartRulesItems();
          $CartRulesItems->item_id = $request->item_id;
          $CartRulesItems->cart_rule_id = $request->idCartRole;
          $CartRulesItems->save();  

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $CartRulesItems->id;
    }
    #endregion insertItemCartRole

 /**
     * deleteCartRoleItem
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function deleteCartRoleItem(int $id, int $idLanguage)
    {
        $CartRulesItems = CartRulesItems::find($id);
        if (is_null($CartRulesItems)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (!is_null($CartRulesItems)) {
            DB::beginTransaction();
            try {
                CartRulesItems::where('id', $id)->delete();
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }
    #endregion deleteCartRoleItem

 /**
     * getCarriersSelectedAndNot
     * 
     * 
     * @param int id
     */
    public static function getCarriersSelectedAndNot(int $id)
    {
            $result = collect();
            foreach (DB::select(            
            'SELECT c.id, c.name
                FROM carriers c
                WHERE c.id NOT IN (SELECT carrier_id FROM carts_rules_carriers WHERE cart_rule_id = :id)',
            ['id' => $id]
            ) as $Carriers) {  
                 $DtoCarrier = new DtoCarrier();
                 $DtoCarrier->id = $Carriers->id;
                 $DtoCarrier->name = $Carriers->name;     
                
        foreach (CartRulesCarriers::where('cart_rule_id', $id)->get() as $CartRulesCarriers) {
        $DtoCartRulesCarriers= new DtoCartRulesCarrier();
        $DtoCartRulesCarriers->id = $CartRulesCarriers->id;
        $DtoCartRulesCarriers->cart_rule_id = $CartRulesCarriers->cart_rule_id;
        $DtoCartRulesCarriers->carrier_id = $CartRulesCarriers->carrier_id;
        $DtoCartRulesCarriers->NameCarrier= Carrier::where('id', $CartRulesCarriers->carrier_id)->first()->name;
        $DtoCarrier->selectCarriersExist->push($DtoCartRulesCarriers);
        }  
            $result->push($DtoCarrier);
        }
            return $result;
    }

    /**
     * getUserSelectedAndNot
     * 
     * 
     * @param int id
     */
    public static function getUserSelectedAndNot(int $id)
    {
       $result = collect();
        foreach (DB::select(
            '   SELECT c.user_id, c.business_name
                FROM users_datas c
                WHERE c.user_id NOT IN (SELECT user_id FROM carts_rules_users WHERE cart_rule_id = :id)',
            ['id' => $id]
        ) as $UsersDatas) {
            $DtoUser = new DtoUser();
            $DtoUser->user_id = $UsersDatas->user_id;
            $DtoUser->business_name = $UsersDatas->business_name;
          

            //select utenti gia inseriti from table cart_rule_users 
        foreach (CartRulesUsers::where('cart_rule_id', $id)->get() as $CartRulesUsers) {
        $DtoCartRulesUser = new DtoCartRulesUser();
        $DtoCartRulesUser->id = $CartRulesUsers->id;
        $DtoCartRulesUser->user_id = $CartRulesUsers->user_id;
        $DtoCartRulesUser->cart_rule_id = $CartRulesUsers->cart_rule_id;
        $DtoCartRulesUser->User= UsersDatas::where('user_id', $CartRulesUsers->user_id)->first()->business_name;
        $DtoUser->selectUserExist->push($DtoCartRulesUser);
        }  
            $result->push($DtoUser);
        }   
            return $result;
    }
    #endregion getUserSelectedAndNot


/**
     * insertCategoriesCartRoleCategories
     * 
      * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertCategoriesCartRoleCategories(Request $request, int $idUser, int $idLanguage)
    {
         DB::beginTransaction();
          try {
          $CartRulesCategories = new CartRulesCategories();
          $CartRulesCategories->category_item_id = $request->category_id;
          $CartRulesCategories->cart_rule_id = $request->idCartRole;
          $CartRulesCategories->save();  

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $CartRulesCategories->id;
    }
    #endregion insertCategoriesCartRoleCategories


    /**
     * insertProducersCartRoleProducers
     * 
      * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertProducersCartRoleProducers(Request $request, int $idUser, int $idLanguage)
    {
         DB::beginTransaction();
          try {
          $CartRulesCategories = new CartRulesProducers();
          $CartRulesCategories->producer_id = $request->producer_id;
          $CartRulesCategories->cart_rule_id = $request->idCartRole;
          $CartRulesCategories->save();  

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $CartRulesCategories->id;
    }
    #endregion insertProducersCartRoleProducers

    /**
     * insertSuppliersCartRoleSuppliers
     * 
      * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertSuppliersCartRoleSuppliers(Request $request, int $idUser, int $idLanguage)
    {
         DB::beginTransaction();
          try {
          $CartRulesCategories = new CartRulesSuppliers();
          $CartRulesCategories->supplier_id = $request->supplier_id;
          $CartRulesCategories->cart_rule_id = $request->idCartRole;
          $CartRulesCategories->save();  

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $CartRulesCategories->id;
    }
    #endregion insertSuppliersCartRoleSuppliers
    
    

    /**
     * insertSpecificItemsCartRole
     * 
      * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertSpecificItemsCartRole(Request $request, int $idUser, int $idLanguage)
    {
         DB::beginTransaction();
          try {
          $CartRulesCategories = new CartRulesSpecificItems();
          $CartRulesCategories->item_id = $request->item_id;
          $CartRulesCategories->carts_rules_id = $request->idCartRole;
          $CartRulesCategories->quantity = 1;
          $CartRulesCategories->save();  

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $CartRulesCategories->id;
    }
    #endregion insertSpecificItemsCartRole

    /**
     * updateSpecificProductsValue
     * 
      * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function updateSpecificProductsValue(Request $request, int $idUser, int $idLanguage)
    {   
        $CartRulesSpecificItems = CartRulesSpecificItems::find($request->id);
        if (is_null($CartRulesSpecificItems)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (!is_null($CartRulesSpecificItems)) {
            DB::beginTransaction();
            try {

            switch ($request->field) {
                case "quantity":
                    $CartRulesSpecificItems->quantity = $request->value;
                    break;
                case "discount_percentage":
                    $CartRulesSpecificItems->discount_percentage = $request->value;
                    break;
                case "discount_value":
                    $CartRulesSpecificItems->discount_value = $request->value;
                    break;
                case "add_to_cart_automatically":
                    $CartRulesSpecificItems->add_to_cart_automatically = !$CartRulesSpecificItems->add_to_cart_automatically;
                    break;
            }

            $CartRulesSpecificItems->save();  

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
        return $CartRulesSpecificItems->id;
    }
    #endregion updateSpecificProductsValue

  /**
     * insertUserCartRoleUser
     * 
      * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertUserCartRoleUser(Request $request, int $idUser, int $idLanguage)
    {
         DB::beginTransaction();
          try {
          $CartRulesUsers = new CartRulesUsers();
          $CartRulesUsers->user_id = $request->user_id;
          $CartRulesUsers->cart_rule_id = $request->idCartRole;
          $CartRulesUsers->save();  
          
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $CartRulesUsers->id;
    }
    #endregion insertUserCartRoleUser


 /**
     * insertCartRoleGroup
     * 
      * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertCartRoleGroup(Request $request, int $idUser, int $idLanguage)
    {
        DB::beginTransaction();
          try {
          $CartRulesGroup = new CartRulesGroup();
          $CartRulesGroup->group_id = $request->id;
          $CartRulesGroup->cart_rule_id = $request->idCartRole;
          $CartRulesGroup->save();  
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $CartRulesGroup->id;
    }

    #endregion insertCartRoleGroup


/**
     * insertCarriersCartRole
     * 
      * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertCarriersCartRole(Request $request, int $idUser, int $idLanguage)
    {
        DB::beginTransaction();
          try {
          $CartRulesCarriers = new CartRulesCarriers();
          $CartRulesCarriers->carrier_id = $request->idCarrier;
          $CartRulesCarriers->cart_rule_id = $request->idCartRole;
          $CartRulesCarriers->save();  
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $CartRulesCarriers->id;
    }
    #endregion insertCarriersCartRole


    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $cartRules = CartRules::find($id);

        if (is_null($cartRules)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $cartRules->delete();
    }

    #endregion DELETE

 /**
     * deleteUserCartRoleUser
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function deleteUserCartRoleUser(int $id, int $idLanguage)
    {
        $CartRulesUsers = CartRulesUsers::find($id);
        if (is_null($CartRulesUsers)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (!is_null($CartRulesUsers)) {
            DB::beginTransaction();
            try {
                CartRulesUsers::where('id', $id)->delete();
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }
     #endregion deleteCategoriesCartRoleCategories

      /**
     * deleteUserCartRoleUser
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function deleteCategoriesCartRoleCategories(int $id, int $idLanguage)
    {
        $CartRulesCategories = CartRulesCategories::find($id);
        if (is_null($CartRulesCategories)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (!is_null($CartRulesCategories)) {
            DB::beginTransaction();
            try {
                CartRulesCategories::where('id', $id)->delete();
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }
     #endregion deleteCategoriesCartRoleCategories


     #endregion deleteProducersCartRoleProducers

      /**
     * deleteProducersCartRoleProducers
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function deleteProducersCartRoleProducers(int $id, int $idLanguage)
    {
        $CartRulesCategories = CartRulesProducers::find($id);
        if (is_null($CartRulesCategories)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (!is_null($CartRulesCategories)) {
            DB::beginTransaction();
            try {
                CartRulesProducers::where('id', $id)->delete();
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }
     #endregion deleteProducersCartRoleProducers


      /**
     * deleteSuppliersCartRoleSuppliers
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function deleteSuppliersCartRoleSuppliers(int $id, int $idLanguage)
    {
        $CartRulesCategories = CartRulesSuppliers::find($id);
        if (is_null($CartRulesCategories)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (!is_null($CartRulesCategories)) {
            DB::beginTransaction();
            try {
                CartRulesSuppliers::where('id', $id)->delete();
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }
     #endregion deleteSuppliersCartRoleSuppliers

     
     


      /**
     * deleteSpecificProductsCartRole
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function deleteSpecificProductsCartRole(int $id, int $idLanguage)
    {
        $CartRulesCategories = CartRulesSpecificItems::find($id);
        if (is_null($CartRulesCategories)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (!is_null($CartRulesCategories)) {
            DB::beginTransaction();
            try {
                CartRulesSpecificItems::where('id', $id)->delete();
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }
     #endregion deleteSpecificProductsCartRole
     
 /**
     * deleteCartRoleCarriers
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function deleteCartRoleCarriers(int $id, int $idLanguage)
    {
        $CartRulesCarriers = CartRulesCarriers::find($id);
        if (is_null($CartRulesCarriers)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (!is_null($CartRulesCarriers)) {
            DB::beginTransaction();
            try {
                CartRulesCarriers::where('id', $id)->delete();
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }
     #endregion deleteCartRoleCarriers

 /**
     * deleteCartRoleGroup
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function deleteCartRoleGroup(int $id, int $idLanguage)
    {
        $CartRulesGroup = CartRulesGroup::find($id);
        if (is_null($CartRulesGroup)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (!is_null($CartRulesGroup)) {
            DB::beginTransaction();
            try {
                CartRulesGroup::where('id', $id)->delete();
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }
     #endregion deleteCartRoleGroup
}

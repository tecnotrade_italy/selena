<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoQuotes;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Quotes;
use App\QuotesDetails;
use App\Cart;
use App\CartDetail;
use App\Content;
use App\ContentLanguage;
use App\DocumentStatusLanguage;
use App\Payment;
use App\Province;

use App\User;
use App\UsersDatas;
use App\UsersAddresses;

use App\CarriagePaidLanguage;
use App\Carrier;
use App\LanguagePaymentType;
use App\VatType;
use App\UnitMeasure;
use App\Item;
use App\ItemLanguage;
use App\Supports;



use Carbon\Carbon;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuotesBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Optional::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    #endregion PRIVATE

    #region GET

    
    /**
     * Get all
     * 
     * @return DtoQuotes
     */
    public static function getByUserId($id, $idLanguage)
    {        
        $salesOrder = Quotes::where('user_id', $id)->get();
        $finalHtml = "";
        $idContent = Content::where('code', 'ListQuotes')->get()->first();
        $contentLanguage = ContentLanguage::where('content_id', $idContent->id)->where('language_id', $idLanguage)->get()->first();

        $tags = SelenaViewsBL::getTagByType('quotes');
        foreach (DB::select(
            'SELECT *
            FROM quotes 
            WHERE user_id = ' . $id .' ORDER BY quotes.created_at DESC'
            ) as $salesorders){
            $newContent = $contentLanguage->content;  
            foreach ($tags as $fieldsDb){
                if(isset($fieldsDb->tag)){ 

                    if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                        if(is_numeric($salesorders->{$cleanTag}) && strpos($salesorders->{$cleanTag},'.')){
                            $tag = str_replace('.',',',substr($salesorders->{$cleanTag}, 0, -2));
                        }else{
                            $tag = $salesorders->{$cleanTag};
                        }
                        $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                               
                    }
                }
            }
            if (strpos($newContent, '#order-state#') !== false) {
                $descriptionStates = DocumentStatusLanguage::where('document_status_id', $salesorders->order_state_id)->first();   
                if (isset($descriptionStates)){
                    $newContent = str_replace('#order-state#', $descriptionStates->description,  $newContent); 
                }else{
                    $newContent = str_replace('#order-state#', '',  $newContent);           
                }
            }

            if (strpos($newContent, '#description_payment#') !== false) {
                $descriptionStates = Payment::where('id', $salesorders->payment_id)->first();   
                if (isset($descriptionStates)){
                    $newContent = str_replace('#description_payment#', $descriptionStates->payment_status,  $newContent); 
                }else{
                    $newContent = str_replace('#description_payment#', '',  $newContent);           
                }
            }


            if (strpos($newContent, '#date_order_formatter#') !== false) {
                $dateorderformatter = Quotes::where('id', $salesorders->id)->get()->first()->created_at;
                $dateorderformatters = $dateorderformatter;
                if (isset($dateorderformatters)){     
                    $newContent = str_replace('#date_order_formatter#',$dateorderformatters,  $newContent); 
                }else{
                    $newContent = str_replace('#date_order_formatter#', '',  $newContent);           
                }
            }

            $finalHtml = $finalHtml . $newContent;
          
        }; 

                       
        return $finalHtml;
    }



    
    /**
     * get By quotes id
     *
     * @param int $id
     */
    public static function getByQuotesId(int $id, $idLanguage){
        $salesOrder = Quotes::where('id', $id)->get()->first();
        $finalHtml = "";
        $idContent = Content::where('code', 'DetailQuotes')->get()->first();
         
        $contentLanguage = ContentLanguage::where('content_id', $idContent->id)->where('language_id', $idLanguage)->get()->first();    
         
        #user detail
        if (strpos($contentLanguage->content, '#order_user_data#') !== false) {
            $idContentUser = Content::where('code', 'DetailQuotesUserData')->get()->first();
            $contentLanguageUser = ContentLanguage::where('content_id', $idContentUser->id)->where('language_id', $idLanguage)->get()->first();    
            $tagsUserDatas = SelenaViewsBL::getTagByType('UserDetail');
            
            $user = UsersDatas::where('user_id', $salesOrder->user_id)->first();  
            $finalHtmlUserDatas= "";

            if (isset($user)){
                foreach (DB::select( 'SELECT * FROM users_datas WHERE user_id = ' . $salesOrder->user_id) as $salesordersUsersDetail){
                    $newContentUserDatas = $contentLanguageUser->content;  
                    foreach ($tagsUserDatas as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                              
                            if (strpos($contentLanguageUser->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($salesordersUsersDetail->{$cleanTag}) && strpos($salesordersUsersDetail->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($salesordersUsersDetail->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $salesordersUsersDetail->{$cleanTag};
                                }
                                $newContentUserDatas = str_replace($fieldsDb->tag , $tag,  $newContentUserDatas);                                                                               
                            }
                        }
                    }

                    if (strpos($newContentUserDatas, '#provinces#') !== false) {
                        $descriptionProvince = Province::where('id', $salesordersUsersDetail->province_id)->first();   
                        if (isset($descriptionProvince)){
                            $newContentUserDatas = str_replace('#provinces#', $descriptionProvince->abbreviation,  $newContentUserDatas); 
                        }else{
                            $newContentUserDatas = str_replace('#provinces#', '',  $newContentUserDatas);           
                        }
                    }
                    if (strpos($newContentUserDatas, '#email#') !== false) {
                        $Email = User::where('id', $salesordersUsersDetail->user_id)->first();   
                        if (isset($Email)){
                            $newContentUserDatas = str_replace('#email#', $Email->email,  $newContentUserDatas); 
                        }else{
                            $newContentUserDatas = str_replace('#email#', '',  $newContentUserDatas);           
                        }
                    }
                
                    $finalHtmlUserDatas = $finalHtmlUserDatas . $newContentUserDatas;
                }; 
            }
            $finalHtml = str_replace('#order_user_data#', $finalHtmlUserDatas,  $contentLanguage->content); 
        }

        //#order-goods-shipment-data# 
        if (strpos($contentLanguage->content, '#order-goods-shipment-data#') !== false) {
            $idContentUser = Content::where('code', 'DetailGoodShipmentData')->get()->first();
            $contentLanguageUser = ContentLanguage::where('content_id', $idContentUser->id)->where('language_id', $idLanguage)->get()->first();    
            $tagsUserDatas = SelenaViewsBL::getTagByType('DetailShipmentData');
            $user = UsersAddresses::where('id', $salesOrder->user_address_goods_id)->first();   
            $finalHtmlUserDatas= "";
            if (isset($user)){
                foreach (DB::select('SELECT * FROM users_addresses WHERE id = ' . $salesOrder->user_address_goods_id) as $salesordersUsersDetail){
                    $newContentUserDatas = $contentLanguageUser->content;  
                    foreach ($tagsUserDatas as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                              
                            if (strpos($contentLanguageUser->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($salesordersUsersDetail->{$cleanTag}) && strpos($salesordersUsersDetail->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($salesordersUsersDetail->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $salesordersUsersDetail->{$cleanTag};
                                }
                                $newContentUserDatas = str_replace($fieldsDb->tag , $tag,  $newContentUserDatas);                                                                               
                            }
                        }
                    }

                    if (strpos($newContentUserDatas, '#province#') !== false) {
                        $descriptionProvince = Province::where('id', $salesordersUsersDetail->province_id)->first();   
                        if (isset($descriptionProvince)){
                            $newContentUserDatas = str_replace('#province#', $descriptionProvince->abbreviation,  $newContentUserDatas); 
                        }else{
                            $newContentUserDatas = str_replace('#province#', '',  $newContentUserDatas);           
                        }
                    }
                    $finalHtmlUserDatas = $finalHtmlUserDatas . $newContentUserDatas;
                }; 
            }
            $finalHtml = str_replace('#order-goods-shipment-data#' , $finalHtmlUserDatas,  $finalHtml); 
        }

        #order-data-shipping-documents#
        if (strpos($contentLanguage->content, '#order-data-shipping-documents#') !== false) {
            $idContentShippingDocument= Content::where('code', 'OrderDataShippingDocuments')->get()->first();
            $contentLanguageUser = ContentLanguage::where('content_id', $idContentShippingDocument->id)->where('language_id', $idLanguage)->get()->first();    
            $tagsUserDatas = SelenaViewsBL::getTagByType('OrderDataShippingDocuments');
            $OrderDataShippingDocuments = UsersAddresses::where('id', $salesOrder->user_address_documents_id)->first();   
            $finalHtmlUserDatas= "";
            if (isset($OrderDataShippingDocuments)){
                foreach (DB::select( 'SELECT * FROM users_addresses WHERE id = ' . $salesOrder->user_address_documents_id) as $salesOrderDataShippingDocuments){
                    $newContentDataShippingDocuments = $contentLanguageUser->content;  
                    foreach ($tagsUserDatas as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                              
                            if (strpos($contentLanguageUser->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($salesOrderDataShippingDocuments->{$cleanTag}) && strpos($salesOrderDataShippingDocuments->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($salesOrderDataShippingDocuments->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $salesOrderDataShippingDocuments->{$cleanTag};
                                }
                                $newContentDataShippingDocuments = str_replace($fieldsDb->tag , $tag,  $newContentDataShippingDocuments);                                                                               
                            }
                        }
                    }

                    if (strpos($newContentDataShippingDocuments, '#province#') !== false) {
                        $descriptionProvince = Province::where('id', $salesOrderDataShippingDocuments->province_id)->first();   
                        if (isset($descriptionProvince)){
                            $newContentDataShippingDocuments = str_replace('#province#', $descriptionProvince->abbreviation,  $newContentDataShippingDocuments); 
                        }else{
                            $newContentDataShippingDocuments = str_replace('#province#', '',  $newContentDataShippingDocuments);           
                        }
                    }
                    $finalHtmlUserDatas = $finalHtmlUserDatas . $newContentDataShippingDocuments;
                }; 
            }
            $finalHtml = str_replace('#order-data-shipping-documents#' , $finalHtmlUserDatas,  $finalHtml); 
        }

        #summary-description-document#
        if (strpos($contentLanguage->content, '#summary-description-document#') !== false) {
            $SummaryDescriptionDocument= Content::where('code', 'SummaryDescriptionDocument')->get()->first();
            $contentLanguageUser = ContentLanguage::where('content_id', $SummaryDescriptionDocument->id)->where('language_id', $idLanguage)->get()->first();    
            $tagsSummaryDescriptionDocument = SelenaViewsBL::getTagByType('SummaryDescriptionDocument');
            $OrderSummaryDescDocument = Quotes::where('id', $salesOrder->id)->first();   
            $finalHtmlOrderSummaryDescDocuments= "";
            if (isset($OrderSummaryDescDocument)){

                foreach (DB::select('SELECT * FROM sales_orders WHERE id = ' . $salesOrder->id) as $OrderSummaryDescDocuments){
                    $newContentOrderSummaryDocuments = $contentLanguageUser->content;  
                    foreach ($tagsSummaryDescriptionDocument as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                              
                            if (strpos($contentLanguageUser->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($OrderSummaryDescDocuments->{$cleanTag}) && strpos($OrderSummaryDescDocuments->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($OrderSummaryDescDocuments->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $OrderSummaryDescDocuments->{$cleanTag};
                                }
                                $newContentOrderSummaryDocuments = str_replace($fieldsDb->tag , $tag,  $newContentOrderSummaryDocuments);                                                                               
                            }
                        }
                    }
                    
                    if (strpos($newContentOrderSummaryDocuments, '#carriage_description#') !== false) {
                        $descriptionCarriageLanguageDescription = CarriagePaidLanguage::where('carriage_paid_to_id', $salesOrder->carriage_paid_to_id)->first();
                        if (isset($descriptionCarriageLanguageDescription)){
                            $newContentOrderSummaryDocuments = str_replace('#carriage_description#', $descriptionCarriageLanguageDescription->description,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#carriage_description#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }

                    if (strpos($newContentOrderSummaryDocuments, '#carrier_description#') !== false) {
                        $descriptionCarrierDescription = Carrier::where('id', $OrderSummaryDescDocuments->carrier_id)->first();
                        if (isset($descriptionCarrierDescription)){
                            $newContentOrderSummaryDocuments = str_replace('#carrier_description#', $descriptionCarrierDescription->name,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#carrier_description#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }

                    if (strpos($newContentOrderSummaryDocuments, '#payment_description#') !== false) {
                        $PaymentDescription = LanguagePaymentType::where('payment_type_id', $OrderSummaryDescDocuments->payment_type_id)->first();
                        if (isset($PaymentDescription)){
                            $newContentOrderSummaryDocuments = str_replace('#payment_description#', $PaymentDescription->description,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#payment_description#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }

                    if (strpos($newContentOrderSummaryDocuments, '#date_order_formatter#') !== false) {
                        $dateorderformatter = Quotes::where('id', $salesOrder->id)->get()->first()->created_at;
                        $dateorderformatters = $dateorderformatter;
                        if (isset($dateorderformatters)){     
                            $newContentOrderSummaryDocuments = str_replace('#date_order_formatter#',$dateorderformatters,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#date_order_formatter#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }


                    $finalHtmlOrderSummaryDescDocuments = $finalHtmlOrderSummaryDescDocuments . $newContentOrderSummaryDocuments;
                }; 
            }
            $finalHtml = str_replace('#summary-description-document#' , $finalHtmlOrderSummaryDescDocuments,  $finalHtml); 
        }


        #detail-row-order#
        if (strpos($contentLanguage->content, '#detail-row-order#') !== false) {
            $SummaryDescriptionDocument= Content::where('code', 'DetailRowOrder')->get()->first();
            $contentLanguageUser = ContentLanguage::where('content_id', $SummaryDescriptionDocument->id)->where('language_id', $idLanguage)->get()->first();    
            $tagsSummaryDescriptionDocument = SelenaViewsBL::getTagByType('DetailRowOrder');
            $OrderSummaryDescDocument = Quotes::where('id', $salesOrder->id)->first();   
            $finalHtmlOrderSummaryDescDocuments= "";
            if (isset($OrderSummaryDescDocument)){

                foreach (DB::select('SELECT * FROM quotes_details WHERE quotes_id = ' . $salesOrder->id) as $OrderSummaryDescDocuments){
                    $newContentOrderSummaryDocuments = $contentLanguageUser->content;  
                    foreach ($tagsSummaryDescriptionDocument as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                              
                            if (strpos($contentLanguageUser->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($OrderSummaryDescDocuments->{$cleanTag}) && strpos($OrderSummaryDescDocuments->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($OrderSummaryDescDocuments->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $OrderSummaryDescDocuments->{$cleanTag};
                                }
                                $newContentOrderSummaryDocuments = str_replace($fieldsDb->tag , $tag,  $newContentOrderSummaryDocuments);                                                                               
                            }
                        }
                    }
          
                    if (strpos($newContentOrderSummaryDocuments, '#vat_type_description#') !== false) {
                        $DescriptionVatType = VatType::where('id', $OrderSummaryDescDocuments->vat_type_id)->first();
                        if (isset($DescriptionVatType)){
                            $newContentOrderSummaryDocuments = str_replace('#vat_type_description#', $DescriptionVatType->rate,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#vat_type_description#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }
                    if (strpos($newContentOrderSummaryDocuments, '#unit_of_measure_description#') !== false) {
                        $UnitMeasureDescription = UnitMeasure::where('id', $OrderSummaryDescDocuments->unit_of_measure_id)->first();
                        if (isset($UnitMeasureDescription)){
                            $newContentOrderSummaryDocuments = str_replace('#unit_of_measure_description#', $UnitMeasureDescription->code,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#unit_of_measure_description#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }
                    if (strpos($newContentOrderSummaryDocuments, '#items_description#') !== false) {
                        $ItemsDescriptionCode = Item::where('id', $OrderSummaryDescDocuments->item_id)->first();
                        if (isset($ItemsDescriptionCode)){
                            $newContentOrderSummaryDocuments = str_replace('#items_description#', $ItemsDescriptionCode->internal_code,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#items_description#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }

                    if (strpos($newContentOrderSummaryDocuments, '#description_language_item#') !== false) {
                        $ItemsDescriptionLanguage = ItemLanguage::where('item_id', $OrderSummaryDescDocuments->item_id)->first();
                        if (isset($ItemsDescriptionLanguage)){
                            $newContentOrderSummaryDocuments = str_replace('#description_language_item#', $ItemsDescriptionLanguage->description,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#description_language_item#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }

                    if (strpos($newContentOrderSummaryDocuments, '#shipment_amount#') !== false) {
                        $shipment_amount = Quotes::where('shipment_amount', $salesOrder->shipment_amount)->first();
                        if (isset($shipment_amount)){
                            $shipment_amount->shipment_amount = str_replace('.',',',substr($shipment_amount->shipment_amount, 0, -2));
                            $newContentOrderSummaryDocuments = str_replace('#shipment_amount#', $shipment_amount->shipment_amount,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#shipment_amount#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }

                    if (strpos($newContentOrderSummaryDocuments, '#support_id_description#') !== false) {
                        $DescriptionSupport = Supports::where('id', $OrderSummaryDescDocuments->support_id)->first();
                        if (isset($DescriptionSupport)){
                            $newContentOrderSummaryDocuments = str_replace('#support_id_description#', $DescriptionSupport->description,  $newContentOrderSummaryDocuments); 
                        }else{
                            $newContentOrderSummaryDocuments = str_replace('#support_id_description#', '',  $newContentOrderSummaryDocuments);           
                        }
                    }

                    $finalHtmlOrderSummaryDescDocuments = $finalHtmlOrderSummaryDescDocuments . $newContentOrderSummaryDocuments;
                }; 
            }
            $finalHtml = str_replace('#detail-row-order#' , $finalHtmlOrderSummaryDescDocuments,  $finalHtml); 
        }



        #order-amounts-summary#
        if (strpos($contentLanguage->content, '#order-amounts-summary#') !== false) {
            $SummaryAmountOrder= Content::where('code', 'SummaryAmountOrder')->get()->first();
            $contentLanguageSummaryAmountOrder = ContentLanguage::where('content_id', $SummaryAmountOrder->id)->where('language_id', $idLanguage)->get()->first();    
            $tagsSummaryAmountOrder = SelenaViewsBL::getTagByType('SummaryAmountOrder');
            $orderSummaryAmountOrder = Quotes::where('id', $salesOrder->id)->first();   
            $finalHtmlSummaryAmountOrder= "";
            if (isset($orderSummaryAmountOrder)){
                foreach (DB::select('SELECT * FROM quotes WHERE id = ' . $salesOrder->id) as $orderSummaryAmountOrders){
                    $newContentSummaryAmountOrder = $contentLanguageSummaryAmountOrder->content;  
                    foreach ($tagsSummaryAmountOrder as $fieldsDb){
                        if(isset($fieldsDb->tag)){                                                              
                            if (strpos($contentLanguageSummaryAmountOrder->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($orderSummaryAmountOrders->{$cleanTag}) && strpos($orderSummaryAmountOrders->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($orderSummaryAmountOrders->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $orderSummaryAmountOrders->{$cleanTag};
                                }
                                $newContentSummaryAmountOrder = str_replace($fieldsDb->tag , $tag,  $newContentSummaryAmountOrder);                                                                               
                            }
                        }
                    }

                    $finalHtmlSummaryAmountOrder = $finalHtmlSummaryAmountOrder . $newContentSummaryAmountOrder;
                }; 
            }
            $finalHtml = str_replace('#order-amounts-summary#' , $finalHtmlSummaryAmountOrder,  $finalHtml); 
        }
          
        return $finalHtml;
    }

    
    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * 
     * @return int
     */
    public static function insertByCartId(Request $request)
    {
        //static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);
        $msgReturn = "";
        
        DB::beginTransaction();

        try {
            if($request->cart_id != '' && !is_null($request->cart_id)){
                $cart = Cart::where('id', $request->cart_id)->get()->first();
                if(isset($cart)){
                    $quotes = new Quotes();
                    $quotes->user_id = $cart->user_id;
                    $quotes->session_token = $cart->session_token;
                    $quotes->description = $request->description;
                    $quotes->currency_id = $cart->currency_id;
                    $quotes->net_amount = $cart->net_amount;
                    $quotes->vat_amount = $cart->vat_amount;
                    $quotes->shipment_amount = $cart->shipment_amount;
                    $quotes->payment_cost = $cart->payment_cost;
                    $quotes->total_amount = $cart->total_amount;
                    $quotes->service_amount = $cart->service_amount;
                    $quotes->discount_percentage = $cart->discount_percentage;
                    $quotes->discount_value = $cart->discount_value;
                    $quotes->note = $cart->note;
                    $quotes->user_address_goods_id = $cart->user_address_goods_id;
                    $quotes->user_address_documents_id = $cart->user_address_documents_id;
                    $quotes->carrier_id = $cart->carrier_id;
                    $quotes->payment_type_id = $cart->payment_type_id;
                    $quotes->carrier_description = $cart->carrier_description;
                    $quotes->carriage_id = $cart->carriage_id;
                    $quotes->salesman_id = $cart->salesman_id;
                    $quotes->save();

                    $cartDetail = CartDetail::where('cart_id', $request->cart_id)->get();
                    if(isset($cartDetail)){
                        foreach($cartDetail as $cartDetails){
                            $quotesDetails = new QuotesDetails();
                            $quotesDetails->quotes_id = $quotes->id;
                            $quotesDetails->nr_row = $cartDetails->nr_row;
                            $quotesDetails->item_id = $cartDetails->item_id;
                            $quotesDetails->description = $cartDetails->description;
                            $quotesDetails->unit_of_measure_id = $cartDetails->unit_of_measure_id;
                            $quotesDetails->unit_price = $cartDetails->unit_price;
                            $quotesDetails->quantity = $cartDetails->quantity;
                            $quotesDetails->net_amount = $cartDetails->net_amount;
                            $quotesDetails->vat_amount = $cartDetails->vat_amount;
                            $quotesDetails->discount_percentage = $cartDetails->discount_percentage;
                            $quotesDetails->discount_value = $cartDetails->discount_value;
                            $quotesDetails->vat_type_id = $cartDetails->vat_type_id;
                            $quotesDetails->total_amount = $cartDetails->total_amount;
                            $quotesDetails->note = $cartDetails->note;
                            $quotesDetails->width = $cartDetails->width;
                            $quotesDetails->depth = $cartDetails->depth;
                            $quotesDetails->height = $cartDetails->height;
                            $quotesDetails->support_id = $cartDetails->support_id;
                            $quotesDetails->save();
                        }
                    }
                }
            }

            $msgReturn = "Preventivo salvato correttamente!";

            DB::commit();
        } catch (Exception $ex) {

            DB::rollback();
            
            throw $ex;
        }

        return $msgReturn;
    }

    /**
     * transformIntoCart
     * 
     * @param Request request
     * @param int idUser
     * 
     * @return int
     */
    public static function transformIntoCart(Request $request)
    {
        //static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);
        $msgReturn = "";
        
        DB::beginTransaction();

        try {
            if($request->id != '' && !is_null($request->id)){

                $cartOnDb = Cart::where('id', $request->cart_id)->get()->first();
                if(!isset($cartOnDb)){
                    $cartOnDb = new Cart();
                }

                $cartDetailOnDB = CartDetail::where('cart_id', $request->cart_id);
                $cartDetailOnDB->delete();

                $cart = Quotes::where('id', $request->id)->get()->first();
                if(isset($cart)){
                   
                    $cartOnDb->user_id = $cart->user_id;
                    $cartOnDb->session_token = $cart->session_token;
                    $cartOnDb->currency_id = $cart->currency_id;
                    $cartOnDb->net_amount = $cart->net_amount;
                    $cartOnDb->vat_amount = $cart->vat_amount;
                    $cartOnDb->shipment_amount = $cart->shipment_amount;
                    $cartOnDb->payment_cost = $cart->payment_cost;
                    $cartOnDb->total_amount = $cart->total_amount;
                    $cartOnDb->service_amount = $cart->service_amount;
                    $cartOnDb->discount_percentage = $cart->discount_percentage;
                    $cartOnDb->discount_value = $cart->discount_value;
                    $cartOnDb->note = $cart->note;
                    $cartOnDb->user_address_goods_id = $cart->user_address_goods_id;
                    $cartOnDb->user_address_documents_id = $cart->user_address_documents_id;
                    $cartOnDb->carrier_id = $cart->carrier_id;
                    $cartOnDb->payment_type_id = $cart->payment_type_id;
                    $cartOnDb->carrier_description = $cart->carrier_description;
                    $cartOnDb->carriage_id = $cart->carriage_id;
                    $cartOnDb->salesman_id = $cart->salesman_id;
                    $cartOnDb->save();

                    $cartDetail = QuotesDetails::where('quotes_id', $request->id)->get();
                    if(isset($cartDetail)){
                        foreach($cartDetail as $cartDetails){
                            $quotesDetails = new CartDetail();
                            $quotesDetails->cart_id = $cartOnDb->id;
                            $quotesDetails->nr_row = $cartDetails->nr_row;
                            $quotesDetails->item_id = $cartDetails->item_id;
                            $quotesDetails->description = $cartDetails->description;
                            $quotesDetails->unit_of_measure_id = $cartDetails->unit_of_measure_id;
                            $quotesDetails->unit_price = $cartDetails->unit_price;
                            $quotesDetails->quantity = $cartDetails->quantity;
                            $quotesDetails->net_amount = $cartDetails->net_amount;
                            $quotesDetails->vat_amount = $cartDetails->vat_amount;
                            $quotesDetails->discount_percentage = $cartDetails->discount_percentage;
                            $quotesDetails->discount_value = $cartDetails->discount_value;
                            $quotesDetails->vat_type_id = $cartDetails->vat_type_id;
                            $quotesDetails->total_amount = $cartDetails->total_amount;
                            $quotesDetails->note = $cartDetails->note;
                            $quotesDetails->width = $cartDetails->width;
                            $quotesDetails->depth = $cartDetails->depth;
                            $quotesDetails->height = $cartDetails->height;
                            $quotesDetails->support_id = $cartDetails->support_id;
                            $quotesDetails->save();
                        }
                    }
                }
            }

            $msgReturn = "Preventivo inserito nel carrello correttamente!";

            DB::commit();
        } catch (Exception $ex) {

            DB::rollback();
            
            throw $ex;
        }

        return $msgReturn;
    }


    

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoOptional
     * @param int idLanguage
     */
    public static function update(Request $dtoOptional, int $idLanguage)
    {
        /*static::_validateData($dtoOptional, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $optionalOnDb = Optional::find($dtoOptional->id);
        
        DB::beginTransaction();

        try {
            $optionalOnDb->order = $dtoOptional->order;
            $optionalOnDb->save();
            
            $optionalLanguageOnDb = OptionalLanguages::where('optional_id', $optionalOnDb->id)->where('language_id', $idLanguage)->get()->first();
            $optionalLanguageOnDb->description = $dtoOptional->description;
            $optionalLanguageOnDb->save();

            $optionalTypologyOnDb = OptionalTypology::where('optional_id', $optionalOnDb->id)->get()->first();
            $optionalTypologyOnDb->type_optional_id = $dtoOptional->typeOptionalId;
            $optionalTypologyOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }*/
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $optional = Quotes::find($id);
        
        if (is_null($optional)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            
            $optionalLanguages = QuotesDetails::where('quotes_id', $id);
            $optionalLanguages->delete();

            $optional->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    #endregion DELETE
}

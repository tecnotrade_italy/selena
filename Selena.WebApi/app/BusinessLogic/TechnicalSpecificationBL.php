<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoIncludedExcluded;
use App\TechincalSpecification;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\DtoModel\DtoTechnicalSpecification;
use App\Functionality;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;

class TechnicalSpecificationBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request $dtoTechnicalSpecification
     * @param int $idLanguage
     * @param DbOperationsTypesEnum $dbOperationType
     */
    private static function _validateData(Request $dtoTechnicalSpecification, int $idLanguage, $dbOperationType)
    {
        if (is_null($dtoTechnicalSpecification->idFunctionality)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Functionality::find($dtoTechnicalSpecification->idFunctionality))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (count($dtoTechnicalSpecification->languageTechnicalSpecification) == 0) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TechnicalSpecificationNotEmpty), HttpResultsCodesEnum::InvalidPayload);
        }

        $defaultLanguageFound = false;

        foreach ($dtoTechnicalSpecification->languageTechnicalSpecification as $dtoLanguageTechnicalSpecification) {
            if ($dtoLanguageTechnicalSpecification['idLanguage'] == LanguageBL::getDefault()->id) {
                $defaultLanguageFound = true;
                if (is_null($dtoLanguageTechnicalSpecification['description'])) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DefaultLanguageRequired), HttpResultsCodesEnum::InvalidPayload);
                }
            }
        }

        if (!$defaultLanguageFound) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DefaultLanguageRequired), HttpResultsCodesEnum::InvalidPayload);
        }

        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($dtoTechnicalSpecification->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            } else {
                if (is_null(TechincalSpecification::find($dtoTechnicalSpecification->id))) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TechincalSpecificationNotFound), HttpResultsCodesEnum::InvalidPayload);
                }
            }
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get all for table
     * 
     * @return DtoTable
     */
    public static function getDtoTable()
    {
        $result = collect();

        foreach (TechincalSpecification::all() as $technicalSpecification) {
            $dtoTechnicalSpecification = new DtoTechnicalSpecification();
            $dtoTechnicalSpecification = LanguageTechnicalSpecificationBL::getDtoTable($technicalSpecification->id);
            $dtoTechnicalSpecification['id'] = $technicalSpecification->id;
            $result->push($dtoTechnicalSpecification);
        }

        return $result;
    }

    /**
     * Get all group display technical specification included and excluded
     * 
     * @param int $id
     * @param int $idLanguage
     * 
     * @return DtoIncludedExcluded
     */
    public static function getGroupDisplayTechnicalSpecification(int $id, int $idLanguage)
    {
        $dtoIncludedExcluded = new DtoIncludedExcluded();
        $dtoIncludedExcluded->included = GroupDisplayTechnicalSpecificationTechnicalSpecificationBL::getInlucedInTechnicalSpecification($id, $idLanguage);
        $dtoIncludedExcluded->excluded = GroupDisplayTechnicalSpecificationTechnicalSpecificationBL::getExlucedInTechnicalSpecification($id, $idLanguage);
        return $dtoIncludedExcluded;
    }

    /**
     * Get all group technical specification included and excluded
     * 
     * @param int $id
     * @param int $idLanguage
     * 
     * @return DtoIncludedExcluded
     */
    public static function getGroupTechnicalSpecification(int $id, int $idLanguage)
    {
        $dtoIncludedExcluded = new DtoIncludedExcluded();
        $dtoIncludedExcluded->included = GroupTechnicalSpecificationTechnicalSpecificationBL::getInlucedInTechnicalSpecification($id, $idLanguage);
        $dtoIncludedExcluded->excluded = GroupTechnicalSpecificationTechnicalSpecificationBL::getExlucedInTechnicalSpecification($id, $idLanguage);
        return $dtoIncludedExcluded;
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request $request
     * @param int $idUser
     * @param int $idLanguage
     * 
     * @return int id
     */
    public static function insert(Request $dtoTechnicalSpecification, int $idUser, int $idLanguage)
    {
        if (!FieldUserRoleBL::checkFunctionalityEnabled($dtoTechnicalSpecification->idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        static::_validateData($dtoTechnicalSpecification, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $technicalSpecification = new TechincalSpecification();
            $technicalSpecification->save();

            foreach ($dtoTechnicalSpecification->languageTechnicalSpecification as $dtoLanguageTechnicalSpecification) {
                LanguageTechnicalSpecificationBL::insertRequest($technicalSpecification->id, $dtoLanguageTechnicalSpecification, $idLanguage);
            }

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $technicalSpecification->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request $request
     * @param int $idUser
     * @param int $idLanguage
     */
    public static function update(Request $dtoTechnicalSpecification, int $idUser, int $idLanguage)
    {
        if (!FieldUserRoleBL::checkFunctionalityEnabled($dtoTechnicalSpecification->idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        static::_validateData($dtoTechnicalSpecification, $idLanguage, DbOperationsTypesEnum::UPDATE);

        DB::beginTransaction();

        try {
            foreach ($dtoTechnicalSpecification->languageTechnicalSpecification as $dtoLanguageTechnicalSpecification) {
                LanguageTechnicalSpecificationBL::updateRequest($dtoTechnicalSpecification->id, $dtoLanguageTechnicalSpecification, $idLanguage);
            }

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int $id
     * @param int $idFunctionality
     * @param int $idUser
     * @param int $idLanguage
     */
    public static function delete(int $id, int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (is_null(FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(TechincalSpecification::find($id))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TechincalSpecificationNotFound), HttpResponseException::InvalidPayload);
        }

        try {
            GroupDisplayTechnicalSpecificationTechnicalSpecificationBL::deleteByTechnicalSpecification($id);
            GroupTechnicalSpecificationTechnicalSpecificationBL::deleteByTechnicalSpecification($id);
            LanguageTechnicalSpecificationBL::deleteByTechnicalSpecification($id);
            TechincalSpecification::destroy($id);

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion DELETE
}

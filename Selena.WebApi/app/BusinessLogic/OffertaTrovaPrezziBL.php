<?php

namespace App\BusinessLogic;

use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use Exception;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use App\DtoModel\DtoOffertaTrovaPrezzi;

class OffertaTrovaPrezziBL
{

    public static function getOfferte($partnerId, $stringaRicerca, $idCategoria)
    {
        $offerte = collect(); 

        $curl = curl_init();

        $stringaRicerca = str_replace(' ', '_', $stringaRicerca);

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://quickshop.shoppydoo.it/' . $partnerId . '/' . $stringaRicerca . '.aspx?categoryId=' . $idCategoria . '&sort=price&format=json',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $offerteTrovaprezzi = json_decode($response, true);

        if($response != ''){
            if(isset($offerteTrovaprezzi['offers'])){
                for($i = 0; $i < count($offerteTrovaprezzi['offers']); $i++){
                    $dtoOffertaTrovaPrezzi = new DtoOffertaTrovaPrezzi();

                    $dtoOffertaTrovaPrezzi->id = $offerteTrovaprezzi['offers'][$i]['id'];
                    
                    if(isset($offerteTrovaprezzi['offers'][$i]['name']))
                        $dtoOffertaTrovaPrezzi->name = $offerteTrovaprezzi['offers'][$i]['name'];
                    else
                        $dtoOffertaTrovaPrezzi->name = '';
                    
                    if(isset($offerteTrovaprezzi['offers'][$i]['merchantName']))
                        $dtoOffertaTrovaPrezzi->merchantName = $offerteTrovaprezzi['offers'][$i]['merchantName'];
                    else
                        $dtoOffertaTrovaPrezzi->merchantName = '';
                
                    if(isset($offerteTrovaprezzi['offers'][$i]['merchant']))
                        $dtoOffertaTrovaPrezzi->merchant = $offerteTrovaprezzi['offers'][$i]['merchant'];
                    else
                        $dtoOffertaTrovaPrezzi->merchant = '';
                
                    if(isset($offerteTrovaprezzi['offers'][$i]['currencyCode']))
                        $dtoOffertaTrovaPrezzi->currencyCode = $offerteTrovaprezzi['offers'][$i]['currencyCode'];
                    else
                        $dtoOffertaTrovaPrezzi->currencyCode = '';     

                    if(isset($offerteTrovaprezzi['offers'][$i]['price']))
                        $dtoOffertaTrovaPrezzi->price = $offerteTrovaprezzi['offers'][$i]['price'];
                    else
                        $dtoOffertaTrovaPrezzi->price = ''; 
                    
                    if(isset($offerteTrovaprezzi['offers'][$i]['listingPrice']))
                        $dtoOffertaTrovaPrezzi->listingPrice = $offerteTrovaprezzi['offers'][$i]['listingPrice'];
                    else
                        $dtoOffertaTrovaPrezzi->listingPrice = '';      

                    if(isset($offerteTrovaprezzi['offers'][$i]['deliveryCost']))
                        $dtoOffertaTrovaPrezzi->deliveryCost = $offerteTrovaprezzi['offers'][$i]['deliveryCost'];
                    else
                        $dtoOffertaTrovaPrezzi->deliveryCost = '';
        
                    if(isset($offerteTrovaprezzi['offers'][$i]['availability']))
                        $dtoOffertaTrovaPrezzi->availability = $offerteTrovaprezzi['offers'][$i]['availability'];
                    else
                        $dtoOffertaTrovaPrezzi->availability = '';

                    if(isset($offerteTrovaprezzi['offers'][$i]['availabilityDescr']))
                        $dtoOffertaTrovaPrezzi->availabilityDescr = $offerteTrovaprezzi['offers'][$i]['availabilityDescr'];
                    else
                        $dtoOffertaTrovaPrezzi->availabilityDescr = '';
                    
                    if(isset($offerteTrovaprezzi['offers'][$i]['url']))
                        $dtoOffertaTrovaPrezzi->url = $offerteTrovaprezzi['offers'][$i]['url'];
                    else
                        $dtoOffertaTrovaPrezzi->url = '';       
                        
                    if(isset($offerteTrovaprezzi['offers'][$i]['description']))
                        $dtoOffertaTrovaPrezzi->description = $offerteTrovaprezzi['offers'][$i]['description'];
                    else
                        $dtoOffertaTrovaPrezzi->description = '';  
                    
                    if(isset($offerteTrovaprezzi['offers'][$i]['smallImage']))
                        $dtoOffertaTrovaPrezzi->smallImage = $offerteTrovaprezzi['offers'][$i]['smallImage'];
                    else
                        $dtoOffertaTrovaPrezzi->smallImage = '';                 
                    
                    if(isset($offerteTrovaprezzi['offers'][$i]['bigImage']))
                        $dtoOffertaTrovaPrezzi->bigImage = $offerteTrovaprezzi['offers'][$i]['bigImage'];
                    else
                        $dtoOffertaTrovaPrezzi->bigImage = '';                  
            
                    if(isset($offerteTrovaprezzi['offers'][$i]['merchantLogo']))
                        $dtoOffertaTrovaPrezzi->merchantLogo = $offerteTrovaprezzi['offers'][$i]['merchantLogo'];
                    else
                        $dtoOffertaTrovaPrezzi->merchantLogo = '';   

                    if(isset($offerteTrovaprezzi['offers'][$i]['categoryId']))
                        $dtoOffertaTrovaPrezzi->categoryId = $offerteTrovaprezzi['offers'][$i]['categoryId'];
                    else
                        $dtoOffertaTrovaPrezzi->categoryId = ''; 
                        
                    if(isset($offerteTrovaprezzi['offers'][$i]['categoryName']))
                        $dtoOffertaTrovaPrezzi->categoryName = $offerteTrovaprezzi['offers'][$i]['categoryName'];
                    else
                        $dtoOffertaTrovaPrezzi->categoryName = '';          
            
                    if(isset($offerteTrovaprezzi['offers'][$i]['merchantRating']))
                        $dtoOffertaTrovaPrezzi->merchantRating = $offerteTrovaprezzi['offers'][$i]['merchantRating'];
                    else
                        $dtoOffertaTrovaPrezzi->merchantRating = ''; 
                    

                    $offerte->push($dtoOffertaTrovaPrezzi); 
                }
            }
        }

        return $offerte;
    }

    public static function migliorOfferta($partnerId, $stringaRicerca, $idCategoria)
    {
        $dtoOffertaTrovaPrezzi = new DtoOffertaTrovaPrezzi();

        $curl = curl_init();

        $stringaRicerca = str_replace(' ', '_', $stringaRicerca);

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://quickshop.shoppydoo.it/' . $partnerId . '/' . $stringaRicerca . '.aspx?categoryId=' . $idCategoria . '&sort=price&format=json',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $offerteTrovaprezzi = json_decode($response, true);
        if(isset($offerteTrovaprezzi['offers'])){
            if($response != '' && count($offerteTrovaprezzi['offers'])>0){
                $dtoOffertaTrovaPrezzi->id = $offerteTrovaprezzi['offers'][0]['id'];
                if(isset($offerteTrovaprezzi['offers'][0]['name']))
                    $dtoOffertaTrovaPrezzi->name = $offerteTrovaprezzi['offers'][0]['name'];
                else
                    $dtoOffertaTrovaPrezzi->name = '';
                
                if(isset($offerteTrovaprezzi['offers'][0]['merchantName']))
                    $dtoOffertaTrovaPrezzi->merchantName = $offerteTrovaprezzi['offers'][0]['merchantName'];
                else
                    $dtoOffertaTrovaPrezzi->merchantName = '';
            
                if(isset($offerteTrovaprezzi['offers'][0]['merchant']))
                    $dtoOffertaTrovaPrezzi->merchant = $offerteTrovaprezzi['offers'][0]['merchant'];
                else
                    $dtoOffertaTrovaPrezzi->merchant = '';
            
                if(isset($offerteTrovaprezzi['offers'][0]['currencyCode']))
                    $dtoOffertaTrovaPrezzi->currencyCode = $offerteTrovaprezzi['offers'][0]['currencyCode'];
                else
                    $dtoOffertaTrovaPrezzi->currencyCode = '';     

                if(isset($offerteTrovaprezzi['offers'][0]['price']))
                    $dtoOffertaTrovaPrezzi->price = $offerteTrovaprezzi['offers'][0]['price'];
                else
                    $dtoOffertaTrovaPrezzi->price = ''; 
                
                if(isset($offerteTrovaprezzi['offers'][0]['listingPrice']))
                    $dtoOffertaTrovaPrezzi->listingPrice = $offerteTrovaprezzi['offers'][0]['listingPrice'];
                else
                    $dtoOffertaTrovaPrezzi->listingPrice = '';      

                if(isset($offerteTrovaprezzi['offers'][0]['deliveryCost']))
                    $dtoOffertaTrovaPrezzi->deliveryCost = $offerteTrovaprezzi['offers'][0]['deliveryCost'];
                else
                    $dtoOffertaTrovaPrezzi->deliveryCost = '';

                if(isset($offerteTrovaprezzi['offers'][0]['availability']))
                    $dtoOffertaTrovaPrezzi->availability = $offerteTrovaprezzi['offers'][0]['availability'];
                else
                    $dtoOffertaTrovaPrezzi->availability = '';

                if(isset($offerteTrovaprezzi['offers'][0]['availabilityDescr']))
                    $dtoOffertaTrovaPrezzi->availabilityDescr = $offerteTrovaprezzi['offers'][0]['availabilityDescr'];
                else
                    $dtoOffertaTrovaPrezzi->availabilityDescr = '';
                
                if(isset($offerteTrovaprezzi['offers'][0]['url']))
                    $dtoOffertaTrovaPrezzi->url = $offerteTrovaprezzi['offers'][0]['url'];
                else
                    $dtoOffertaTrovaPrezzi->url = '';       
                    
                if(isset($offerteTrovaprezzi['offers'][0]['description']))
                    $dtoOffertaTrovaPrezzi->description = $offerteTrovaprezzi['offers'][0]['description'];
                else
                    $dtoOffertaTrovaPrezzi->description = '';  
                
                if(isset($offerteTrovaprezzi['offers'][0]['smallImage']))
                    $dtoOffertaTrovaPrezzi->smallImage = $offerteTrovaprezzi['offers'][0]['smallImage'];
                else
                    $dtoOffertaTrovaPrezzi->smallImage = '';                 
                
                if(isset($offerteTrovaprezzi['offers'][0]['bigImage']))
                    $dtoOffertaTrovaPrezzi->bigImage = $offerteTrovaprezzi['offers'][0]['bigImage'];
                else
                    $dtoOffertaTrovaPrezzi->bigImage = '';                  
        
                if(isset($offerteTrovaprezzi['offers'][0]['merchantLogo']))
                    $dtoOffertaTrovaPrezzi->merchantLogo = $offerteTrovaprezzi['offers'][0]['merchantLogo'];
                else
                    $dtoOffertaTrovaPrezzi->merchantLogo = '';   

                if(isset($offerteTrovaprezzi['offers'][0]['categoryId']))
                    $dtoOffertaTrovaPrezzi->categoryId = $offerteTrovaprezzi['offers'][0]['categoryId'];
                else
                    $dtoOffertaTrovaPrezzi->categoryId = ''; 
                    
                if(isset($offerteTrovaprezzi['offers'][0]['categoryName']))
                    $dtoOffertaTrovaPrezzi->categoryName = $offerteTrovaprezzi['offers'][0]['categoryName'];
                else
                    $dtoOffertaTrovaPrezzi->categoryName = '';          
        
                if(isset($offerteTrovaprezzi['offers'][0]['merchantRating']))
                    $dtoOffertaTrovaPrezzi->merchantRating = $offerteTrovaprezzi['offers'][0]['merchantRating'];
                else
                    $dtoOffertaTrovaPrezzi->merchantRating = '';    
            }
        }

        return $dtoOffertaTrovaPrezzi;
    }


}

    
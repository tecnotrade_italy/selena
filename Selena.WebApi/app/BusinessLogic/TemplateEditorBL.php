<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoTemplateEditor;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\TemplateEditor;
use App\TemplateEditorGroup;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class TemplateEditorBL
{
    #region PRIVATE

    private static $dirImage = '../storage/app/public/images/TemplateEditor/Previews/';
    private static $mimeTypeAllowed = ['jpeg', 'jpg', 'png', 'gif'];

    /**
     * Validate data
     *
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationsTypesEnum
     */
    private static function _validateData(Request $request, int $idLanguage,  $dbOperationsTypesEnum)
    {
        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->img)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ImageNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($request->imgName)) {
            if (!in_array(\explode('/', \explode(':', substr($request->img, 0, strpos($request->img, ';')))[1])[1], static::$mimeTypeAllowed)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FileTypeNotAllowed), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Storage::disk('public')->exists('images/TemplateEditor/Previews/' . $request->imgName)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameFileAlreadyExists), HttpResultsCodesEnum::NotAcceptable);
            }
        }

        if (is_null($request->html)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::HtmlNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->idTemplateEditorGroup) || is_null(TemplateEditorGroupBL::get($request->idTemplateEditorGroup))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TemplateEditorGroupNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if ($dbOperationsTypesEnum == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (is_null(TemplateEditor::find($request->id))) {

                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TemplateEditorNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }
    }

    /**
     * Get url
     */
    private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/TemplateEditor/Previews/';
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get dto
     *
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     *
     * @return DtoTemplateEditorGroup
     */
    public static function getDto(int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $result = collect();

        foreach (TemplateEditor::all() as $templateEditor) {
            $dtoTemplateEditor = new DtoTemplateEditor();
            $dtoTemplateEditor->id = $templateEditor->id;
            $dtoTemplateEditor->description = $templateEditor->description;
            $dtoTemplateEditor->img = $templateEditor->img;
            $dtoTemplateEditor->html = $templateEditor->html;
            $dtoTemplateEditor->idTemplateEditorGroup = $templateEditor->template_editor_group_id;
            $dtoTemplateEditor->idTemplateEditorsGroup = TemplateEditorGroupBL::get($templateEditor->template_editor_group_id)->description;
            //$dtoTemplateEditor->idTemplateEditorGroup =  TemplateEditorGroupBL::get($templateEditor->template_editor_group_id)->description;
            $dtoTemplateEditor->templateeditorgroupDescription = TemplateEditorGroupBL::get($templateEditor->template_editor_group_id)->description;
            $dtoTemplateEditor->order = $templateEditor->order;
            $result->push($dtoTemplateEditor);
        }

        return $result;
    }


    /**
     * Get template by id
     *
     * @param int id
     * @param int idUser
     * @param int idLanguage
     *
     * @return DtoTemplateEditorGroup
     */
    public static function getTemplateById(int $id, int $idUser, int $idLanguage)
    {
        //FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);      
        if (is_null($id)) {
            return;
        } else {
            if ($id != 0) {
                return TemplateEditor::where('template_editor_group_id', $id)->select('id', 'description', 'img', 'html')->orderBy('order')->get();
            } else {
                return TemplateEditor::select('id', 'description', 'img', 'html')->orderBy('order')->get();
            }
        }
    }

    #endregion GET

    public static function getTemplateDefaultValue()
    {
       $TemplateEditorGroup = TemplateEditorGroup::where('description', 'Base')->get()->first();
       
       return TemplateEditor::where('template_editor_group_id', $TemplateEditorGroup->id)->select('id', 'description', 'img', 'html')->orderBy('order')->get();
    }

    public static function getTemplateDefaultValueSelect()
    {
        return TemplateEditorGroup::where('description', '=', 'Base')->select('id', 'description')->orderBy('order')->get();
    }


    #region INSERT

    /**
     * Insert
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($request->idFunctionality, $idUser, $idLanguage);
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);
        Image::make($request->img)->save(static::$dirImage . $request->imgName);

        $TemplateEditor = new TemplateEditor();
        $TemplateEditor->description = $request->description;
        $TemplateEditor->img = static::getUrl() . $request->imgName;
        $TemplateEditor->html = $request->html;
        $TemplateEditor->template_editor_group_id = $request->idTemplateEditorGroup;
        $TemplateEditor->order = $request->order;
        $TemplateEditor->save();

        return $TemplateEditor->id;
    }

    /**
     * Clone
     * 
     * @param int id
     * @param int idFunctionality
     * @param int idLanguage
     * 
     * @return int
     */
    public static function clone(int $id, int $idFunctionality, int $idUser, $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $templateEditorToClone = TemplateEditor::find($id);
        if (is_null($templateEditorToClone)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PageNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        DB::beginTransaction();

        try {
            //Image::make($templateEditorToClone->img)->save(static::$dirImage . $templateEditorToClone->imgName);

            $TemplateEditor = new TemplateEditor();
            $TemplateEditor->description = $templateEditorToClone->description . ' - Copy';
            $TemplateEditor->img = $templateEditorToClone->img;
            $TemplateEditor->html = $templateEditorToClone->html;
            $TemplateEditor->template_editor_group_id = $templateEditorToClone->template_editor_group_id;
            $TemplateEditor->order = $templateEditorToClone->order;
            $TemplateEditor->save();
            
            DB::commit();

            return $TemplateEditor->id;
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     */
    public static function update(Request $request, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($request->idFunctionality, $idUser, $idLanguage);
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::UPDATE);

        $TemplateEditor = TemplateEditor::find($request->id);
        $TemplateEditor->description = $request->description;
        if (!is_null($request->imgName)) {
            Image::make($request->img)->save(static::$dirImage . $request->imgName);
            $TemplateEditor->img = static::getUrl() . $request->imgName;
        }
        $TemplateEditor->html = $request->html;
        $TemplateEditor->template_editor_group_id = $request->idTemplateEditorGroup;
        $TemplateEditor->order = $request->order;
        $TemplateEditor->save();
    }

    /**
     * Update table
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     */
    public static function updateTable(Request $request, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($request->idFunctionality, $idUser, $idLanguage);

        if (is_null($request->idTemplateEditorGroup) || is_null(TemplateEditorGroupBL::get($request->idTemplateEditorGroup))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TemplateEditorGroupNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->id)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(TemplateEditor::find($request->id))) {

            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TemplateEditorNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $TemplateEditor = TemplateEditor::find($request->id);
        $TemplateEditor->description = $request->description;
        $TemplateEditor->template_editor_group_id = $request->idTemplateEditorGroup;
        $TemplateEditor->order = $request->order;
        $TemplateEditor->save();
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function delete(int $id, int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        if (is_null(TemplateEditor::find($id))) {

            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TemplateEditorNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $templateEditor = TemplateEditor::find($id);
        $templateEditor->delete();
    }

    #endregion DELETE
}

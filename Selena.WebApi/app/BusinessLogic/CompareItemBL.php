<?php

namespace App\BusinessLogic;

use App\CorrelationPeopleUser;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoPeople;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\User;
use App\People;
use App\PeopleUser;
use App\Role;
use App\RolesPeople;
use App\RoleUser;
use App\UsersDatas;
use App\Item;
use App\ItemLanguage;
use App\CategoryItem;
use App\CompareItem;
use App\DtoModel\DtoPageSitemap;
use App\BusinessLogic\OffertaTrovaPrezziBL;
use App\BusinessLogic\ItemBestPriceBL;

use App\DtoModel\DtoItem;

use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;


class CompareItemBL
{

     /**
     * Get Content for compare item to item
     * 
     * @return DtoPageCategory
     */
    public static function getHtml($url, $idUser, $idCart, $idLanguage){
        //analisi url per prelevare i codici articolo esempio: /compara/1-2-3-4
        //creare loop con split dei trattini (-)
        //ogni numero è riferito al campo id della tabella items
        //da qui utilizzare la query delle schede tecniche su ogni articolo e appendere l'html generato (con in testa l'immagine)
        $html = "";
        $title = "";
        $description = "";
        $keywords = "";
        $dtoPageItemRender = new DtoItem();

        if (strpos($url, '/confronta/') !== false) {
            $cleanUrl = str_replace('/confronta/', '', $url);
            $arrayIdItems = explode("-", $cleanUrl);
            $numberOfItem = count($arrayIdItems);
            if($numberOfItem>0){
                $numberOfColumns = 3;
                switch ($numberOfItem) {
                    case 1:
                        $numberOfColumns = 12;
                        break;
                    case 2:
                        $numberOfColumns = 6;
                        break;
                    case 3:
                        $numberOfColumns = 4;
                        break;
                    case 4:
                        $numberOfColumns = 3;
                        break;
                    case 5:
                        $numberOfColumns = 2;
                        break;
                    case 6:
                        $numberOfColumns = 2;
                        break;
                }

                //mi preparo una stringa con la lista dei codici separati da virgole....mi serve nella where SQL
                $sListaItem = '';
                if($numberOfItem==1) 
                    $sListaItem = '(' . $arrayIdItems[0] . ')';
                else
                {
                    $sListaItem = '(';
                    foreach ($arrayIdItems as $items) {
                        $sListaItem = $sListaItem . $items . ',';
                    }
                    $sListaItem = substr($sListaItem, 0, -1);
                    $sListaItem = $sListaItem . ')';
                }
                
                $html = $html . '   <div class="container"> ';

                /*************************************************/
                //IMMAGINI E NOME
                /*************************************************/
                $html = $html . '<div class="row scheda_foto"> ';

                foreach ($arrayIdItems as $items) {
                    $itemOnDb = Item::where('id', $items)->get()->first();
                    $itemLanguageOnDb = ItemLanguage::where('item_id', $items)->get()->first();
                    $categoryItemOnDb = CategoryItem::where('item_id', $items)->get()->first();
                    
                    if($description == ""){
                        $description = $itemLanguageOnDb->description;
                    }else{
                        $description = $description . ', ' . $itemLanguageOnDb->description;
                    }

                    if($title == ""){
                        $title = $itemLanguageOnDb->description;
                    }else{
                        $title = $title . ' vs ' . $itemLanguageOnDb->description;
                    }
                    
                    $html = $html . '   <div class="col-2 text-center">';
                    $html = $html . '       <img src="' . $itemOnDb->img  .'" alt="" class="fr-fic fr-dii"> ';
                    $html = $html . '       <h1><a href="' . $itemLanguageOnDb->link .'">' . $itemLanguageOnDb->description . '</a></h1>';
                    $html = $html . '       <div class="scheda_prezzo text-center">';
                    $html = $html . '       <div class="arrow-up"><br></div>';
                    
                    // TROVAPREZZI
                    if ($categoryItemOnDb->category_id>=11 && $categoryItemOnDb->category_id<=15)
                        $offerte = OffertaTrovaPrezziBL::getOfferte("cameranationit", $itemLanguageOnDb->description, "5");
                    else if ($categoryItemOnDb->category_id==16)
                        $offerte = OffertaTrovaPrezziBL::getOfferte("cameranationit", $itemLanguageOnDb->description, "20124");
                    else if ($categoryItemOnDb->category_id==17)
                        $offerte = OffertaTrovaPrezziBL::getOfferte("cameranationit", $itemLanguageOnDb->description, "7");
                    else
                        $offerte = OffertaTrovaPrezziBL::getOfferte("cameranationit", $itemLanguageOnDb->description, "0");
                        

                    if (count($offerte)>0){
                        //$rangePrezzo = $offerte[0]->price . ' € - ' . $offerte[count($offerte)-1]->price . ' €<br/>';
                        //$rangePrezzo = $rangePrezzo . 'Vedi tutte le ' . count($offerte) . ' offerte';
                        //$html = $html . '   <div class="col-12 col-md-3"><a target="_blank" rel="nofollow" href="'. $offerte[0]->url . '"><b>MIGLIOR PREZZO ' . $offerte[0]->price . ' € </b></a> <br/>' . $rangePrezzo . '</div>';
                        $html = $html . '       <a target="_blank" rel="nofollow" href="'. $offerte[0]->url . '"><h2>'. $offerte[0]->price .' €</h2><h4>Miglior Prezzo</h4></a>';
                    }else
                    {
                        $html = $html . '   <h4>Prezzo non disponibile</h4>';
                    }           
                    $html = $html . '       </div>';
                    $html = $html . '   </div>';
                    
                }

                $html = $html . '</div>';
            
                
                $s = "SELECT TUTTO.gruppo, TUTTO.specifica ";
                foreach ($arrayIdItems as $items) {
                    $s = $s . ", ITEM" . $items . ".value as specifica" . $items;
                }            
                $s = $s . " from ";
                $s = $s . " ( ";
                $s = $s . "     select TS.technical_specification_id, gtsl.description as gruppo, LTS.description as specifica, ";
                $s = $s . "    GG.order as OrdineGruppo, gtsts.order as OrdineSpecifica ";
                $s = $s . "     from items_technicals_specifications as TS ";
                $s = $s . "     inner join languages_technicals_specifications as LTS on LTS.technical_specification_id = TS.technical_specification_id and LTS.language_id = 1 ";
                $s = $s . "     left outer join groups_display_technical_specification_tech_spec gtsts on gtsts.technical_specification_id  = LTS.technical_specification_id  ";
                $s = $s . "     left outer join groups_display_technicals_specifications GG on GG.id = gtsts.group_display_technical_specification_id  ";
                $s = $s . "     left outer join groups_display_technicals_specifications_languages gtsl on gtsl.group_display_technical_specification_id = GG.id and gtsl.language_id = 1 ";
                $s = $s . "     where TS.language_id = 1 and item_id in " . $sListaItem . " group by TS.technical_specification_id, LTS.description, gtsl.description, GG.order, gtsts.order ";
                $s = $s . " ) as TUTTO ";
                foreach ($arrayIdItems as $items) {
                    $s = $s . " LEFT outer join ";
                    $s = $s . " ( ";
                    $s = $s . "     select technical_specification_id, value ";
                    $s = $s . "     from items_technicals_specifications ";
                    $s = $s . "     where language_id = 1 and item_id = " . $items;
                    $s = $s . " ) as ITEM" . $items . " on ITEM" . $items . ".technical_specification_id = TUTTO.technical_specification_id  ";
                } 
                $s = $s . " order by TUTTO.OrdineGruppo, TUTTO.OrdineSpecifica ";
                
                $oldGruppo = '';
                $i = 0;

                foreach (DB::select(DB::raw($s)) as $specificheTOTALI){
                    
                    if ($specificheTOTALI->gruppo!=$oldGruppo){
                        $oldGruppo = $specificheTOTALI->gruppo;
                        $html = $html . '<div class="row scheda_titolo">';
                        $html = $html . '   <div class="col"><h3>' . $specificheTOTALI->gruppo . '</h3></div>';
                        $html = $html . '</div> ';
                    }
                    
                    $html = $html . '<div class="row scheda_specifica">';
                    $html = $html . '   <div class="col-2"><p>' . $specificheTOTALI->specifica . '</p></div>';
                    foreach ($arrayIdItems as $items) {
                        $spec = "specifica" . $items;
                        $html = $html . '   <div class="col-2"><p>' . $specificheTOTALI->$spec . '</p></div>';
                    }
                    $html = $html . '</div> ';

                }

               
                //chiudo il div container generale
                $html = $html . '</div> ';
                

                $dtoPageItemRender->html = '<div class="row">' . $html . '</div>';
                $dtoPageItemRender->description = $title;
                $dtoPageItemRender->seo_title = $title;
                $dtoPageItemRender->seo_description = 'Confronta '. $description . ' e scopri tutte le differenze.';
                $dtoPageItemRender->seo_keyword = 'Confronta '. $description . ' e scopri tutte le differenze.';
                $dtoPageItemRender->share_title = $title;
                $dtoPageItemRender->share_description = 'Confronta '. $description . ' e scopri tutte le differenze.';
                $dtoPageItemRender->url_cover_image = '';

            }else{
                $dtoPageItemRender->html = 'Nessun Articolo presente per il confronto';
                $dtoPageItemRender->description = $title;
                $dtoPageItemRender->seo_title = $title;
                $dtoPageItemRender->seo_description = 'Confronta '. $description . ' e scopri tutte le differenze.';
                $dtoPageItemRender->seo_keyword = 'Confronta '. $description . ' e scopri tutte le differenze.';
                $dtoPageItemRender->share_title = $title;
                $dtoPageItemRender->share_description = 'Confronta '. $description . ' e scopri tutte le differenze.';
                $dtoPageItemRender->url_cover_image = '';
            }
        }else{
            $dtoPageItemRender->html = '';
            $dtoPageItemRender->description = '';
            $dtoPageItemRender->seo_title = '';
            $dtoPageItemRender->seo_description = '';
            $dtoPageItemRender->seo_keyword = '';
            $dtoPageItemRender->share_title = '';
            $dtoPageItemRender->share_description = '';
            $dtoPageItemRender->url_cover_image = '';
        }

        return $dtoPageItemRender;
    }



    public static function getPages(string $page_type, int $idLanguage, int $page)
    {
        //perchè il conteggio parte da 0
        $paginator = $page + 1;
        $totItems = DB::select('SELECT count(item_compare.id) as cont from item_compare');

        $strSqlPagination = "";
        if($totItems[0]->cont <= 50000){
            $strSqlPagination = "";
        }else{
            $limit = 50000;
            $offset = 50000 * ($paginator - 1);

            $strSqlPagination = " limit " . $limit . " offset " . $offset;
        }

        $result = collect();
        foreach (DB::select(
            "SELECT item_compare.title, item_compare.created_at, item_compare.link
            FROM item_compare
            ORDER BY item_compare.created_at desc " . $strSqlPagination
        ) as $PagesAll) {
            $dtoPageSitemap = new DtoPageSitemap();
            $dtoPageSitemap->url = $PagesAll->link;
            $dtoPageSitemap->title = $PagesAll->title;
            $dtoPageSitemap->created_at = $PagesAll->created_at;

            $result->push($dtoPageSitemap);
        }
        return $result;
    }
}
<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoScheduler;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\Helpers\HttpHelper;
use Carbon\Carbon;
use App\Scheduler;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SchedulerBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Scheduler::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoOptional
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (DB::table('scheduler')
                ->orderBy('scheduler.id')
                ->get()
        as $optionalAll) {
            
            $dtoOptional = new DtoScheduler();
            $dtoOptional->id = $optionalAll->id;
            $dtoOptional->type = $optionalAll->type;
            $dtoOptional->id_task_type = $optionalAll->id_task_type;
            $dtoOptional->user_id = $optionalAll->user_id;
            $dtoOptional->name = $optionalAll->name;
            $dtoOptional->description = $optionalAll->description;
            $dtoOptional->action = $optionalAll->action;
            $dtoOptional->arguments = $optionalAll->arguments;
            if(!is_null($optionalAll->start_time_action)){
                $dateStart = Carbon::parse($optionalAll->start_time_action)->format(HttpHelper::getLanguageDateTimeFormat());
                $dtoOptional->start_time_action = $dateStart;
            }
            
            $dtoOptional->schedule = $optionalAll->schedule;
            $dtoOptional->frequency = $optionalAll->frequency;
            $dtoOptional->repeat_after_hour = $optionalAll->repeat_after_hour;
            $dtoOptional->repeat_after_min = $optionalAll->repeat_after_min;
            $dtoOptional->repeat_times = $optionalAll->repeat_times;


            
            if(!is_null($optionalAll->last_run)){
                $lastRun = Carbon::parse($optionalAll->last_run)->format(HttpHelper::getLanguageDateTimeFormat());
                $dtoOptional->last_run = $lastRun;
            }
            if(!is_null($optionalAll->next_run)){
                $nextRun = Carbon::parse($optionalAll->next_run)->format(HttpHelper::getLanguageDateTimeFormat());
                $dtoOptional->next_run = $nextRun;
            }

            $dtoOptional->status = $optionalAll->status;
            $dtoOptional->notification = $optionalAll->notification;
            $dtoOptional->active = $optionalAll->active;
            $result->push($dtoOptional); 
        }
        
        return $result;
    }

    /**
     * Get select user pages
     *
     * @param String $search
     * @return User
     */
    public static function typeScheduler(string $search)
    {
        if ($search == "*") {
            return collect(DB::select(
                "SELECT * FROM (VALUES ('O', 'Once'), ('D', 'Daily'), ('W', 'Weekly'), ('M', 'Monthly'), ('A', 'Annually')) AS t (id, description);"
            ));
        } else {
            return collect(DB::select(
                "SELECT * FROM (VALUES ('O', 'Once'), ('D', 'Daily'), ('W', 'Weekly'), ('M', 'Monthly'), ('A', 'Annually')) AS t (id, description) where description like '%" . $search . "%'"
            ));
        }
    }

    /**
     * Get select user pages
     *
     * @param String $search
     * @return User
     */
    public static function repeatHourScheduler(string $search)
    {
        if ($search == "*") {
            return collect(DB::select(
                "SELECT * FROM (VALUES 
                (0, 'Nessuna'),
                (1, '1 ora'), 
                (2, '2 ore'), 
                (3, '3 ore'), 
                (4, '4 ore'), 
                (5, '5 ore'), 
                (6, '6 ore'), 
                (7, '7 ore'), 
                (8, '8 ore'), 
                (9, '9 ore'), 
                (10, '10 ore'), 
                (11, '11 ore'), 
                (12, '12 ore'), 
                (13, '13 ore'), 
                (14, '14 ore'), 
                (15, '15 ore'), 
                (16, '16 ore'), 
                (17, '17 ore'), 
                (18, '18 ore'), 
                (19, '19 ore'), 
                (20, '20 ore'), 
                (21, '21 ore'), 
                (22, '22 ore'), 
                (23, '23 ore'), 
                (24, '24 ore')) AS t (id, description);"
            ));
        } else {
            return collect(DB::select(
                "SELECT * FROM (VALUES 
                (0, 'Nessuna'),
                (1, '1 ora'), 
                (2, '2 ore'), 
                (3, '3 ore'), 
                (4, '4 ore'), 
                (5, '5 ore'), 
                (6, '6 ore'), 
                (7, '7 ore'), 
                (8, '8 ore'), 
                (9, '9 ore'), 
                (10, '10 ore'), 
                (11, '11 ore'), 
                (12, '12 ore'), 
                (13, '13 ore'), 
                (14, '14 ore'), 
                (15, '15 ore'), 
                (16, '16 ore'), 
                (17, '17 ore'), 
                (18, '18 ore'), 
                (19, '19 ore'), 
                (20, '20 ore'), 
                (21, '21 ore'), 
                (22, '22 ore'), 
                (23, '23 ore'), 
                (24, '24 ore')) AS t (id, description) where description like '%" . $search . "%'"
            ));
        }
    }

    /**
     * Get select user pages
     *
     * @param String $search
     * @return User
     */
    public static function repeatMinutesScheduler(string $search)
    {
        if ($search == "*") {
            return collect(DB::select(
                "SELECT * FROM (VALUES 
                (0, '0 min'), 
                (15, '15 min'), 
                (30, '30 min'), 
                (45, '45 min')) AS t (id, description);"
            ));
        } else {
            return collect(DB::select(
                "SELECT * FROM (VALUES 
                (0, '0 min'), 
                (15, '15 min'), 
                (30, '30 min'), 
                (45, '45 min')) AS t (id, description)where description like '%" . $search . "%'"
            ));
        }
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $dateStart = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $request->start_time_action)->format('Y-m-d H:i:s');
            
            $optional = new Scheduler();
            $optional->type = $request->type;
            $optional->user_id = $idUser;
            $optional->name = $request->name;
            $optional->description = $request->description;
            $optional->action = $request->action;
            $optional->arguments = $request->arguments;
            $optional->start_time_action = $dateStart;
            $optional->schedule = $request->schedule;
            $optional->frequency = $request->frequency;
            $optional->repeat_after_hour = $request->repeat_after_hour;
            $optional->repeat_after_min = $request->repeat_after_min;
            $optional->repeat_times = $request->repeat_times;
            $optional->repeat_left = $request->repeat_times;
            
            if(is_null($request->next_run) || !isset($request->next_run)){
                $optional->next_run = $dateStart;
            }else{
                $dateNext = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $request->next_run)->format('Y-m-d H:i:s');
                $optional->next_run = $dateNext;
            }
            
            if(!is_null($request->last_run) || isset($request->last_run)){
                $lastRun = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $request->last_run)->format('Y-m-d H:i:s');
                $optional->last_run = $lastRun;
            }

            $optional->status = $request->status;
            $optional->notification = $request->notification;
            $optional->active = $request->active;
            $optional->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        //return $optional->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoOptional
     * @param int idLanguage
     */
    public static function update(Request $dtoOptional, int $idLanguage)
    {
        static::_validateData($dtoOptional, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $optionalOnDb = Scheduler::find($dtoOptional->id);
        
        DB::beginTransaction();

        try {
            $dateStart = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $dtoOptional->start_time_action)->format('Y-m-d H:i:s');

            $optionalOnDb->type = $dtoOptional->type;
            $optionalOnDb->description = $dtoOptional->description;
            $optionalOnDb->name = $dtoOptional->name;
            $optionalOnDb->action = $dtoOptional->action;
            $optionalOnDb->arguments = $dtoOptional->arguments;
            $optionalOnDb->start_time_action = $dateStart;
            $optionalOnDb->schedule = $dtoOptional->schedule;
            $optionalOnDb->frequency = $dtoOptional->frequency;
            $optionalOnDb->repeat_after_hour = $dtoOptional->repeat_after_hour;
            $optionalOnDb->repeat_after_min = $dtoOptional->repeat_after_min;
            $optionalOnDb->repeat_times = $dtoOptional->repeat_times;

            if(is_null($dtoOptional->next_run) || !isset($dtoOptional->next_run)){
                $optionalOnDb->next_run = $dateStart;
            }else{
                $dateNext = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $dtoOptional->next_run)->format('Y-m-d H:i:s');
                $optionalOnDb->next_run = $dateNext;
            }

            if(!is_null($dtoOptional->last_run) || isset($dtoOptional->last_run)){
                $lastRun = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $dtoOptional->last_run)->format('Y-m-d H:i:s');
                $optionalOnDb->last_run = $lastRun;
            }
            $optionalOnDb->status = $dtoOptional->status;
            $optionalOnDb->notification = $dtoOptional->notification;
            $optionalOnDb->active = $dtoOptional->active;

            $optionalOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    /**
     * update Check Active
     * 
     * @param int id
     */
    public static function updateCheckActive(Request $request)
    {
        //static::_validateData($dtoOptional, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $optionalOnDb = Scheduler::find($request->id);
        
        DB::beginTransaction();

        try {
            $optionalOnDb->active = !$optionalOnDb->active;
            $optionalOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $request->id;
    }

    /**
     * update Check Notification
     * 
     * @param int id
     */
    public static function updateCheckNotification(Request $request)
    {
        //static::_validateData($dtoOptional, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $optionalOnDb = Scheduler::find($request->id);
        
        DB::beginTransaction();

        try {
            $optionalOnDb->notification = !$optionalOnDb->notification;
            $optionalOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $request->id;
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $optional = Scheduler::find($id);
        
        if (is_null($optional)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            
            $optional->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    #endregion DELETE
}

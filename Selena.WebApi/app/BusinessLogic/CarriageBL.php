<?php

namespace App\BusinessLogic;

use App\Carriage;
use App\CarriageLanguage;
use App\Cart;
use App\DtoModel\DtoCarriage;
use App\DtoModel\DtoSelect2;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Illuminate\Http\Request;

class CarriageBL
{
    #region PRIVATE

    /**
     * Check function enabled
     * 
     * @param int idFunctionlity
     * @param int idUser
     * @param int idLanguage
     */
    private static function _checkFunctionEnabled(int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (is_null($idFunctionality)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Functionality::find($idFunctionality))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by customer
     * 
     * @param int idCustomer
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoSelect2
     */
    public static function getByCustomer(int $idCustomer, int $idFunctionality, int $idUser, $idLanguage)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idFunctionality);

        $carriage = DB::select(
            'SELECT carriages.id, carriages_languages.description
                                    FROM carriages
                                    INNER JOIN carriages_languages ON carriages.id = carriages_languages.carriage_id
                                    INNER JOIN customers ON carriages.id = customers.carriage_id
                                    WHERE carriages_languages.language_id = :idLanguage
                                    AND customers.id = :idCustomer',
            ['idLanguage' => $idLanguage, 'idCustomer' => $idCustomer]
        );

        $result = new DtoSelect2();

        if (count($carriage)) {
            $result->id = $carriage[0]->id;
            $result->description = $carriage[0]->description;
        }

        return $result;
    }

    /**
     * Get select
     * 
     * @param string search
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoSelect2
     */
    public static function getSelect(string $search)
        {
        /*static::_checkFunctionEnabled($idFunctionality, $idUser, $idFunctionality);
        return CarriageLanguage::where('language_id', $idLanguage)->where('description', 'ILIKE', $search . '%')->select('carriage_id as id', 'description')->get();*/
        
       if ($search == "*") {
            return CarriageLanguage::select('carriage_id as id', 'description as description')->get();
        } else {
            return CarriageLanguage::where('description', 'ilike', $search . '%')->select('carriage_id as id', 'description as description')->get();
        }
    }


    #endregion GET
    
     public static function getAllCarriageByCartId($idLanguage, $cartId)
    {
        $result = collect();
        foreach (Carriage::orderBy('code','DESC')->get() as $CarriageAll) {
            $CarriageLanguage = CarriageLanguage::where('carriage_id', $CarriageAll->id)->where('language_id', $idLanguage)->get()->first();
            if(isset($CarriageLanguage)){
                $DtoCarriage = new DtoCarriage();
                $DtoCarriage->id = $CarriageAll->id;
                $DtoCarriage->code = $CarriageAll->code;
                $cart = Cart::where('id', $cartId)->get()->first();
                if(isset($cart)){
                    if($cart->carriage_id == $CarriageAll->id){
                        $DtoCarriage->selected = "selected";
                    }else{
                        $DtoCarriage->selected = "";
                    }
                }
                $DtoCarriage->description = $CarriageLanguage->description;
                $DtoCarriage->carrierdescription = $cart->carrier_description;
                $result->push($DtoCarriage); 
            }
        }
        return $result;
    }
    

  /**
     * Get by id
     * 
     * @param int id
     * @return DtoCarriage
     */
    public static function getById(int $id)
    {
         $CarriageLanguage = CarriageLanguage::where('carriage_id', $id)->first();
         $Carriage = Carriage::where('id', $id)->first();

            $DtoCarriage = new DtoCarriage();
            $DtoCarriage->id = $id;
            $DtoCarriage->code = $Carriage->code;
            $DtoCarriage->description = $CarriageLanguage->description;
            $DtoCarriage->language_id = $CarriageLanguage->language_id;
            $DtoCarriage->online = $CarriageLanguage->online;
            $DtoCarriage->order = $CarriageLanguage->order;    
            $DtoCarriage->add_transport_cost = $CarriageLanguage->add_transport_cost; 
            return $DtoCarriage;
    }



     /**
     * Get all
     * 
     * @return DtoCarriage
     */
    public static function getAll($idLanguage)
    {
        $result = collect(); 
        foreach (CarriageLanguage::where('language_id',$idLanguage)->get() as $CarriageLanguage) {
            $DtoCarriage = new DtoCarriage();
            $DtoCarriage->id = Carriage::where('id', $CarriageLanguage->carriage_id)->first()->id;
            $DtoCarriage->Code = Carriage::where('id', $CarriageLanguage->carriage_id)->first()->code;
            $DtoCarriage->Description = $CarriageLanguage->description;
            $DtoCarriage->Order = $CarriageLanguage->order;
            if ($CarriageLanguage->online == true){
                $DtoCarriage->Online = "Si";
            }else{
                $DtoCarriage->Online = "No";  
            }

            $result->push($DtoCarriage); 
        }
        return $result;
    }
    

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoCarriage
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoCarriage, int $idUser)
    {
        DB::beginTransaction();

        try {
            $Carriage = new Carriage();
            $Carriage->code = $DtoCarriage->code;
            $Carriage->created_id = $DtoCarriage->$idUser;
            $Carriage->save();

            $CarriageLanguage = new CarriageLanguage();
            $CarriageLanguage->language_id =$DtoCarriage->idLanguage;
            $CarriageLanguage->carriage_id = $Carriage->id;
            $CarriageLanguage->description = $DtoCarriage->description;
            $CarriageLanguage->created_id = $DtoCarriage->$idUser;
            $CarriageLanguage->order = $DtoCarriage->order;
            $CarriageLanguage->online = $DtoCarriage->online;
            $CarriageLanguage->add_transport_cost = $DtoCarriage->add_transport_cost;
            $CarriageLanguage->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $Carriage->id;
    }
    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoCarriage
     * @param int idLanguage
     */

    public static function update(Request $DtoCarriage)
    {
        $Carriage = Carriage::find($DtoCarriage->id);
        $CarriageLanguage = CarriageLanguage::where('carriage_id', $DtoCarriage->id, $DtoCarriage->idLanguage)->first();
        
        DB::beginTransaction();

        try {
            $Carriage->id = $DtoCarriage->id;
            $Carriage->code = $DtoCarriage->code;
            $Carriage->save();

            $CarriageLanguage->language_id = $DtoCarriage->idLanguage;
            $CarriageLanguage->carriage_id = $DtoCarriage->id;
            $CarriageLanguage->description = $DtoCarriage->description;
            $CarriageLanguage->order = $DtoCarriage->order;
            $CarriageLanguage->online = $DtoCarriage->online;
            $CarriageLanguage->add_transport_cost = $DtoCarriage->add_transport_cost;

            if (isset($DtoCarriage->Code)) {
                $Carriage->code = $DtoCarriage->Code;  
                $Carriage->save(); 
            }
            if (isset($DtoCarriage->Description)) {
                $Carriage->CarriageLanguage = $DtoCarriage->Description;
                $CarriageLanguage->save();    
            }
            if (isset($DtoCarriage->Order)) {
                $Carriage->order = $DtoCarriage->Order;
                $CarriageLanguage->save();    
            }
             if (isset($DtoCarriage->Online)) {
                $Carriage->online = $DtoCarriage->Online;
                $CarriageLanguage->save();    
            }
                $CarriageLanguage->save();


            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
    #endregion UPDATE

      public static function updateCheck(Request $DtoCarriage)
    {

        $Carriage = Carriage::find($DtoCarriage->id);
        $CarriageLanguage = CarriageLanguage::where('carriage_id', $DtoCarriage->id, $DtoCarriage->idLanguage)->first();
        
        DB::beginTransaction();

        try {
            $Carriage->id = $DtoCarriage->id;
            $Carriage->code = $DtoCarriage->Code;
            $Carriage->save();

            $CarriageLanguage->language_id = $DtoCarriage->idLanguage;
            $CarriageLanguage->carriage_id = $DtoCarriage->id;
            $CarriageLanguage->description = $DtoCarriage->Description;
            $CarriageLanguage->order = $DtoCarriage->Order;
            if ($DtoCarriage->Online == 'Si'){
                $CarriageLanguage->online = true;
            }else{
                $CarriageLanguage->online = false;
            }
            //$CarriageLanguage->online = $DtoCarriage->Online;
            $CarriageLanguage->save();


            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
    #endregion UPDATE

    

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */

    public static function delete(int $id)
    {
    
        $CarriageLanguage= CarriageLanguage::where('carriage_id', $id)->first();

        if (is_null($CarriageLanguage)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        $CarriageLanguage->delete();

        $Carriage = Carriage::find($id);

        if (is_null($Carriage)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        $Carriage->delete();
    }

    #endregion DELETE


}

<?php

namespace App\BusinessLogic;

use Illuminate\Support\Facades\DB;
use App\Helpers\ArrayHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\UsersAddresses;
use App\Cart;
use App\Content;
use App\ContentLanguage;
use App\LanguageNation;
use App\LanguageProvince;
use App\Provinces;

class UserAddressBL
{
    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * 
     * @return int
     */
    public static function insert($request){
        
        DB::beginTransaction();

        try {
            $userAddress = new UsersAddresses();
            $userAddress->user_id = $request->user_id;
            $userAddress->address = $request->address;
            $userAddress->province_id = $request->province_id;
            $userAddress->nation_id = $request->nation_id;
            $userAddress->region_id = $request->region_id;
            $userAddress->phone_number = $request->phone_number;
            $userAddress->business_name = $request->business_name;
            $userAddress->name = $request->name;
            $userAddress->surname = $request->surname;
            $userAddress->fiscal_code = $request->fiscal_code;
            $userAddress->postal_code = $request->postal_code;
            $userAddress->vat_number = $request->vat_number;
            $userAddress->locality = $request->locality;
           
            if(isset($request->defaultAddress)){
                if($request->defaultAddress == 'on'){
                   $userAddressOnDb = UsersAddresses::where('user_id', $request->user_id)->where('default_address', true)->get()->first();
                   if(isset($userAddressOnDb)){
                        $userAddressOnDb->default_address = false;
                        $userAddressOnDb->save();
                   }

                   $userAddress->default_address = true;
                }else{
                    $userAddress->default_address = false;
                }
            }else{
                $userAddress->default_address = false;
            }
           
            $userAddress->save();
            
            

            if(isset($request->activeAddress)){
                if($request->activeAddress == 'on'){
                    if(isset($request->cart_id)){
                        $cart = Cart::where('id', $request->cart_id)->get()->first();
                        $cart->user_address_goods_id = $userAddress->id;
                        $cart->user_address_documents_id = $userAddress->id;
                        $cart->save();

                        CartBL::applyRulesAndRefreshTotalAmount($request->cart_id);
                    }
                }
            }

        DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $userAddress->id;

    }


    /**
     * active address by user
     * 
     * @param Request request
     * 
     * @return int
     */
    public static function activeAddressByUser($request){
        $cart = Cart::where('id', $request->idCart)->get()->first();
        if(isset($cart)){
            $cart->user_address_goods_id = $request->idAddress;
            $cart->user_address_documents_id = $request->idAddress;
            $cart->save();

            CartBL::applyRulesAndRefreshTotalAmount($request->idCart);
        }
        return $cart->id;
    }


    /**
     * reset address by user
     * 
     * @param Request request
     * 
     * @return int
     */
    public static function resetAddressByCartId($request){
        $cart = Cart::where('id', $request->cart_id)->get()->first();
        if(isset($cart)){
            $cart->user_address_goods_id = null;
            $cart->user_address_documents_id = null;
            $cart->save();

            CartBL::applyRulesAndRefreshTotalAmount($request->cart_id);
        }
        return $cart->id;
    }
    

    #endregion INSERT

    #region GET
    /**
     * Get by id
     *
     * @param int $id
     */
    public static function getAllByUser(int $id)
    {
        $userAddress = UsersAddresses::where('user_id', $id)->where('online', true)->get();
        $finalHtml = "";
        $idContent = Content::where('code', 'ListUsersAddresses')->get()->first();
        $contentLanguage = ContentLanguage::where('content_id', $idContent->id)->where('language_id', HttpHelper::getLanguageId())->get()->first();
        
        $tags = SelenaViewsBL::getTagByType('CartUsersAddresses');

        foreach (DB::select(
            'SELECT *
            FROM users_addresses 
            WHERE online = true AND user_id = ' . $id
            ) as $userAddress){
            $newContent = $contentLanguage->content;  
            foreach ($tags as $fieldsDb){
                if(isset($fieldsDb->tag)){                                                              
                    if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                        if(is_numeric($userAddress->{$cleanTag}) && strpos($userAddress->{$cleanTag},'.')){
                            $tag = str_replace('.',',',substr($userAddress->{$cleanTag}, 0, -2));
                        }else{
                            $tag = $userAddress->{$cleanTag};
                        }
                        $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                               
                    }
                }
            }

              if (strpos($newContent, '#description_province#') !== false) {
                        $DescriptionProvince = Provinces::where('id', $userAddress->province_id)->first();
                        if (isset($DescriptionProvince)){
                        $newContent = str_replace('#description_province#', $DescriptionProvince->abbreviation,  $newContent); 
                        }else{
                            $newContent = str_replace('#description_province#', '',  $newContent);           
                        }
                    }
                    if (strpos($newContent, '#description_nation#') !== false) {
                        $DescriptionNation = LanguageNation::where('nation_id', $userAddress->nation_id)->first();
                        if (isset($DescriptionNation)){
                        $newContent = str_replace('#description_nation#', $DescriptionNation->description,  $newContent); 
                        }else{
                            $newContent = str_replace('#description_nation#', '',  $newContent);           
                        }
                    }

            $finalHtml = $finalHtml . $newContent;
        };
        
        return $finalHtml;

    }

    #endregion GET


    #region DELETE

    /**
     * Get by id
     *
     * @param int $id
     */
    public static function deleteAddressById(int $id)
    {
        $userAddress = UsersAddresses::where('id', $id)->get()->first();
        
        if(isset($userAddress)){
            $userAddress->online = false;
            $userAddress->save();
        }
        
        return $userAddress->id;

    }

    #endregion DELETE
}

<?php

namespace App\BusinessLogic;

use App\Helpers\LogHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ManageStyleBL
{
    #region GET

    /**
     * Get
     * 
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function get(int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
        return Storage::disk('public')->get('\css\webmin.css');
    }

    #endregion GET

    #region POST
    /**
     * Update
     * 
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function update(Request $request, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($request->idFunctionality, $idUser, $idLanguage);
        Storage::disk('public')->put('\css\webmin.css', $request->css);

      /*  $str= $request->css;
        $str = str_replace("\n", "", $str);
        $str = str_replace("      ", "", $str);
        $str = str_replace("	 ", "", $str);
        $str = str_replace("    ", "", $str);
        $str = str_replace("   ", "", $str);
        $str = str_replace("  ", "", $str);
        Storage::disk('public')->put('\css\web.css', $str);
        return;*/
       
       $url = 'https://www.toptal.com/developers/cssminifier/api/raw';
        $css = $request->css;
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_HTTPHEADER => ["Content-Type: application/x-www-form-urlencoded"],
            CURLOPT_POSTFIELDS => http_build_query([ "input" => $css ])
        ]);
        $minified = curl_exec($ch);  
        curl_close($ch);
        Storage::disk('public')->put('\css\web.css', $minified);
        return;

    }
    #endregion POST

   // webNewsletter.css
   /**
     * Get
     * 
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function getCssEmail(int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
        return Storage::disk('public')->get('\css\webCssEmail.css');
    }

    /**
     * Get
     * 
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function getCssHead(int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
        return Storage::disk('public')->get('\css\webminCssHead.css');
    }

    #endregion GET


    #region POST
    /**
     * Update
     * 
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function updateCssEmail(Request $request, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($request->idFunctionality, $idUser, $idLanguage);
        Storage::disk('public')->put('\css\webCssEmail.css', $request->cssEmail);
        return;
    }
    #endregion POST

     public static function updateSaveGlobal(Request $request, int $idUser, int $idLanguage)
    {
        Storage::disk('public')->put('\css\webCssEmail.css', $request->cssToSaveEmail);
        Storage::disk('public')->put('\css\webmin.css', $request->cssToSave);

        /*Storage::disk('public')->put('\css\web.css', $request->cssToSave);
        return;*/

          /*  $str= $request->cssToSave;
            $str = str_replace("\n", "", $str);
            $str = str_replace("      ", "", $str);
            $str = str_replace("	 ", "", $str);
            $str = str_replace("    ", "", $str);
            $str = str_replace("   ", "", $str);
            $str = str_replace("  ", "", $str);
            Storage::disk('public')->put('\css\web.css', $str);
            return;*/

            $url = 'https://www.toptal.com/developers/cssminifier/api/raw';
            $css = $request->cssToSave;
            $ch = curl_init();
            curl_setopt_array($ch, [
             CURLOPT_URL => $url,
             CURLOPT_RETURNTRANSFER => true,
             CURLOPT_POST => true,
             CURLOPT_HTTPHEADER => ["Content-Type: application/x-www-form-urlencoded"],
             CURLOPT_POSTFIELDS => http_build_query([ "input" => $css ])
         ]);
            $minified = curl_exec($ch);  
            curl_close($ch);
            Storage::disk('public')->put('\css\web.css', $minified);
            return;

    }

    public static function updateSaveHead(Request $request, int $idUser, int $idLanguage)
    {
        Storage::disk('public')->put('\css\webminCssHead.css', $request->cssToSave);

        /*Storage::disk('public')->put('\css\web.css', $request->cssToSave);
        return;*/

          /*  $str= $request->cssToSave;
            $str = str_replace("\n", "", $str);
            $str = str_replace("      ", "", $str);
            $str = str_replace("	 ", "", $str);
            $str = str_replace("    ", "", $str);
            $str = str_replace("   ", "", $str);
            $str = str_replace("  ", "", $str);
            Storage::disk('public')->put('\css\web.css', $str);
            return;*/

            $url = 'https://www.toptal.com/developers/cssminifier/api/raw';
            $css = $request->cssToSave;
            $ch = curl_init();
            curl_setopt_array($ch, [
             CURLOPT_URL => $url,
             CURLOPT_RETURNTRANSFER => true,
             CURLOPT_POST => true,
             CURLOPT_HTTPHEADER => ["Content-Type: application/x-www-form-urlencoded"],
             CURLOPT_POSTFIELDS => http_build_query([ "input" => $css ])
         ]);
            $minified = curl_exec($ch);  
            curl_close($ch);
            Storage::disk('public')->put('\css\webhead.css', $minified);
            return;

    }
}

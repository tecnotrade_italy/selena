<?php

namespace App\BusinessLogic;

use App\Message;
use App\MessageLanguage;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\UsersDatas;
use Exception;
use Illuminate\Support\Facades\DB;

class MessageLanguageBL
{

    public static function getByCodeAndLanguage(int $idMessage, int $idLanguageMessage, int $idFunctionality = null, int $idUser = null, int $idLanguage = null)
    {
        if (!is_null($idFunctionality)) {
            FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
        }

        $Message = '';
        $MessageLanguage = MessageLanguage::where('message_id', $idMessage)->where('language_id', $idLanguageMessage)->first();

        if (is_null($MessageLanguage)) {
            $language = LanguageBL::getDefault();
            if ($language->id != $idLanguageMessage) {
                $MessageLanguage = MessageLanguage::where('message_id', $idMessage)->where('language_id', $language->id)->first();
                if (!is_null($MessageLanguage)) {
                    $Message = $MessageLanguage->content;
                }
            }
        } else {
            $Message = $MessageLanguage->content;
        }
        return $Message;
    }


}
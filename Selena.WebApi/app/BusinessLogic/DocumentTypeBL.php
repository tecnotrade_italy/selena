<?php

namespace App\BusinessLogic;

use App\DocumentType;
use App\Helpers\LogHelper;

class DocumentTypeBL
{

    #region GET

    /**
     * Get id by code
     *
     * @param int code
     * 
     * @return id
     */
    public static function getIdByCode($code)
    {
        return DocumentType::where('code', $code)->first()->id;
    }

    #endregion GET
}

<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoFormBuilderType;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\FormBuilderType;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormBuilderTypeBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (FormBuilderType::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoOptional
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (DB::table('form_builder_type')
                ->select('form_builder_type.*')
                ->get()
        as $optionalAll) {
            
            $dtoOptional = new DtoFormBuilderType();
            $dtoOptional->id = $optionalAll->id;
            $dtoOptional->order = $optionalAll->order;
            $dtoOptional->description = $optionalAll->description;
                        
            $result->push($dtoOptional); 
        }
        
        return $result;
    }

    /**
     * Get select
     *
     * @param String $search
     * @return List
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return FormBuilderType::select('id', 'description')->get();
        } else {
            return FormBuilderType::where('description', 'ilike', $search . '%')->select('id', 'description')->get();
        }
    }
    

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $optional = new FormBuilderType();
            $optional->order = $request->order;
            $optional->description = $request->description;
            $optional->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        //return $optional->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoOptional
     * @param int idLanguage
     */
    public static function update(Request $dtoOptional, int $idLanguage)
    {
        static::_validateData($dtoOptional, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $optionalOnDb = FormBuilderType::find($dtoOptional->id);
        
        DB::beginTransaction();

        try {
            $optionalOnDb->order = $dtoOptional->order;
            $optionalOnDb->description = $dtoOptional->description;
            $optionalOnDb->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $optional = FormBuilderType::find($id);
        
        if (is_null($optional)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            
            $optional->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    #endregion DELETE
}

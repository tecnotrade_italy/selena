<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoLanguagePaymentType;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\PaymentType;
use App\UsersDatas;

use App\LanguagePaymentType;
use App\Cart;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentTypeBL
{
    #region PRIVATE


    #endregion PRIVATE

    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoProvince
     */
    public static function getAllByCartId($idLanguage, $cartId)
    {
        $cart = Cart::where('id', $cartId)->get()->first();
        $result = collect();

        if(isset($cart)){
            $usersDatas = UsersDatas::where('user_id', $cart->user_id)->get()->first();
            if(isset($usersDatas)){
                if(is_null($usersDatas->id_payments)){
                    foreach (PaymentType::where('online', true)->orderBy('order')->get() as $paymentTypeAll) {
                        $paymentTypeLanguage = LanguagePaymentType::where('payment_type_id', $paymentTypeAll->id)->where('language_id', $idLanguage)->get()->first();
                        if(isset($paymentTypeLanguage)){
                            $dtoLanguagePaymentType = new DtoLanguagePaymentType();
                            $dtoLanguagePaymentType->id = $paymentTypeAll->id;
                            $dtoLanguagePaymentType->code = $paymentTypeAll->code;
                            $cart = Cart::where('id', $cartId)->get()->first();
                            if(isset($cart)){
                                if($cart->payment_type_id == $paymentTypeAll->id){
                                    $dtoLanguagePaymentType->selected = "selected";
                                }else{
                                    $dtoLanguagePaymentType->selected = "";
                                }
                            }
                            $dtoLanguagePaymentType->description = $paymentTypeLanguage->description;
                            $result->push($dtoLanguagePaymentType); 
                        }
                    }
                }else{
                    foreach (PaymentType::where('online', true)->where('id', $usersDatas->id_payments)->orderBy('order')->get() as $paymentTypeAll) {
                        $paymentTypeLanguage = LanguagePaymentType::where('payment_type_id', $paymentTypeAll->id)->where('language_id', $idLanguage)->get()->first();
                        if(isset($paymentTypeLanguage)){
                            $dtoLanguagePaymentType = new DtoLanguagePaymentType();
                            $dtoLanguagePaymentType->id = $paymentTypeAll->id;
                            $dtoLanguagePaymentType->code = $paymentTypeAll->code;
                            $cart = Cart::where('id', $cartId)->get()->first();
                            if(isset($cart)){
                                //if($cart->payment_type_id == $paymentTypeAll->id){
                                    $dtoLanguagePaymentType->selected = "selected";
                                /*}else{
                                    $dtoLanguagePaymentType->selected = "";
                                }*/
                            }
                            $dtoLanguagePaymentType->description = $paymentTypeLanguage->description;
                            $result->push($dtoLanguagePaymentType); 
                        }
                    }
                }
            }
        }
        return $result;
    }

    public static function getSelect(string $search){
             /*   if ($search == "*") {
                    return LanguagePaymentType::select('payment_type_id', 'description as description')->get();
                } else {
                    return LanguagePaymentType::where('description', 'ilike' , $search . '%')->select('payment_type_id', 'description as description')->get();
                }*/

                if ($search == "*") {
                    return LanguagePaymentType::select('payment_type_id as id', 'description as description')->get();
                } else {
                    return LanguagePaymentType::where('description', 'ilike', $search . '%')->select('payment_type_id as id', 'description as description')->get();
                }

    }

    #endregion GET

}

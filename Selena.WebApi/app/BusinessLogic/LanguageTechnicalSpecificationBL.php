<?php

namespace App\BusinessLogic;

use App\LanguageTechnicalSpecification;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use Mockery\CountValidator\Exception;
use App\Language;
use App\Helpers\LogHelper;
use App\DtoModel\DtoLanguageTechnicalSpecification;
use App\TechincalSpecification;

class LanguageTechnicalSpecificationBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param int $idTechnicalSpecification
     * @param Request $dtoLanguageTechnicalSpecification
     * @param int $idLanguage
     */
    private static function _validateData($idTechnicalSpecification, $dtoLanguageTechnicalSpecification, int $idLanguage)
    {
        if (is_null($dtoLanguageTechnicalSpecification['idLanguage'])) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::LanguageNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Language::find($dtoLanguageTechnicalSpecification['idLanguage']))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::LanguageNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (!is_null($dtoLanguageTechnicalSpecification['description']) && static::checkExists($dtoLanguageTechnicalSpecification['description'], $dtoLanguageTechnicalSpecification['idLanguage'], $idTechnicalSpecification)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TechnicalSpecificationAlreadyExists), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by technical specification and language
     * 
     * @param int $idTechnicalSpecification
     * @param int $idLanguage
     * @param int $version
     * 
     * @return string
     */
    public static function getByIdTechnicalSpecificationAndLanguage(int $idTechnicalSpecification, int $idLanguage, int $version = null)
    {
        if (is_null($version)) {
            $version = LanguageTechnicalSpecification::where('technical_specification_id', $idTechnicalSpecification)->where('language_id', $idLanguage)->max('version');
        }

        $languageTechnicalSpecification = LanguageTechnicalSpecification::where('technical_specification_id', $idTechnicalSpecification)->where('language_id', $idLanguage)->where('version', $version)->first();


        if (is_null($languageTechnicalSpecification)) {
            LanguageTechnicalSpecification::where('technical_specification_id', $idTechnicalSpecification)->where('language_id', LanguageBL::getDefault()->id)->first();
        }

        if (is_null($languageTechnicalSpecification)) {
            return '';
        } else {
            return $languageTechnicalSpecification->description;
        }
    }

    /**
     * Get DtoTechnicalSpecificationLanguage
     * 
     * @param int $idTechnicalSpecification
     * 
     * @return DtoTechnicalSpecificationLanguage
     */
    public static function getDtoTable(int $idTechnicalSpecification)
    {
        $dtoTable = collect();

        foreach (LanguageTechnicalSpecification::where('technical_specification_id', $idTechnicalSpecification)->distinct('language_id')->get() as $languageTechnicalSpecificationDistinct) {
            $languageTechnicalSpecification = LanguageTechnicalSpecification::where('technical_specification_id', $idTechnicalSpecification)->where('language_id', $languageTechnicalSpecificationDistinct->language_id)->orderBy('version', 'desc')->first();
            $dtoTable[$languageTechnicalSpecification->language_id] = $languageTechnicalSpecification->description;
        }

        return $dtoTable;
    }

    /**
     * Check if exist
     * 
     * @param string description
     * @param int idLanguage
     * @param int idTechnicalSpecification
     * 
     * @return bool
     */
    public static function checkExists(string $description, int $idLanguage, int $idTechnicalSpecification = null)
    {
        $found = false;

        if (is_null($idTechnicalSpecification)) {
            foreach (TechincalSpecification::all() as $technicalSpecification) {
                $lastVersion = LanguageTechnicalSpecification::where('technical_specification_id', $technicalSpecification->id)->where('language_id', $idLanguage)->max('version');
                if (LanguageTechnicalSpecification::where('technical_specification_id', $technicalSpecification->id)->where('language_id', $idLanguage)->where('version', $lastVersion)->where('description', $description)->count() > 0) {
                    $found = true;
                    break;
                }
            }
        } else {
            foreach (TechincalSpecification::where('id', '!=', $idTechnicalSpecification)->get() as $technicalSpecification) {
                $lastVersion = LanguageTechnicalSpecification::where('technical_specification_id', $technicalSpecification->id)->where('language_id', $idLanguage)->max('version');
                if (LanguageTechnicalSpecification::where('technical_specification_id', $technicalSpecification->id)->where('language_id', $idLanguage)->where('version', $lastVersion)->where('description', $description)->count() > 0) {
                    $found = true;
                    break;
                }
            }
        }

        return $found;
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     * 
     * @param int $idTechnicalSpecification
     * @param Request $dtoLanguageTechnicalSpecification
     * @param int $idLanguage
     * 
     * @return id
     */
    public static function insertRequest(int $idTechnicalSpecification, $dtoLanguageTechnicalSpecification, int $idLanguage)
    {
        static::_validateData($idTechnicalSpecification, $dtoLanguageTechnicalSpecification, $idLanguage);

        $languageTechnicalSpecification = new LanguageTechnicalSpecification();
        $languageTechnicalSpecification->language_id = $dtoLanguageTechnicalSpecification['idLanguage'];
        $languageTechnicalSpecification->technical_specification_id = $idTechnicalSpecification;
        $languageTechnicalSpecification->description = $dtoLanguageTechnicalSpecification['description'];
        $languageTechnicalSpecification->version = 1;

        $languageTechnicalSpecification->save();

        $languageTechnicalSpecification->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update request
     * 
     * @param int $idTechnicalSpecification
     * @param Request $dtoLanguageTechnicalSpecification
     * @param int $idLanguage
     * 
     * @return id
     */
    public static function updateRequest(int $idTechnicalSpecification, $dtoLanguageTechnicalSpecification, int $idLanguage)
    {
        static::_validateData($idTechnicalSpecification, $dtoLanguageTechnicalSpecification, $idLanguage);

        $lastVersion = LanguageTechnicalSpecification::where('technical_specification_id', $idTechnicalSpecification)->where('language_id', $dtoLanguageTechnicalSpecification['idLanguage'])->max('version');
        $rowLastVersion = LanguageTechnicalSpecification::where('technical_specification_id', $idTechnicalSpecification)->where('language_id', $dtoLanguageTechnicalSpecification['idLanguage'])->where('version', $lastVersion)->first();

        if (!is_null($rowLastVersion) && is_null($dtoLanguageTechnicalSpecification['description'])) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TechnicalSpecificationNotEmpty), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (((!is_null($rowLastVersion) && $rowLastVersion->description != $dtoLanguageTechnicalSpecification['description']))
                || (is_null($rowLastVersion) && !is_null($dtoLanguageTechnicalSpecification['description']))
            ) {
                $languageTechnicalSpecification = new LanguageTechnicalSpecification();
                $languageTechnicalSpecification->language_id = $dtoLanguageTechnicalSpecification['idLanguage'];
                $languageTechnicalSpecification->technical_specification_id = $idTechnicalSpecification;
                $languageTechnicalSpecification->description = $dtoLanguageTechnicalSpecification['description'];
                $languageTechnicalSpecification->version = $lastVersion + 1;

                $languageTechnicalSpecification->save();

                $languageTechnicalSpecification->id;
            } else {
                if (!is_null($rowLastVersion)) {
                    $rowLastVersion->id;
                }
            }
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete by technical specification
     * 
     * @param int $idTechnicalSpecification
     */
    public static function deleteByTechnicalSpecification(int $idTechnicalSpecification)
    {
        LanguageTechnicalSpecification::where('technical_specification_id', $idTechnicalSpecification)->delete();
    }

    #endregion DELETE
}

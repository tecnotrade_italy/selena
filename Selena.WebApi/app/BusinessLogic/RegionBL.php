<?php

namespace App\BusinessLogic;

use App\Contact;
use App\DtoModel\DtoRegion;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Mail\ContactRequestMail;
use App\Region;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class RegionBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Region::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->order)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SurnameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

    }

    /**
     * Conver to model
     * 
     * @param Request request
     * 
     * @return Contact
     */
    private static function _convertToModel(Request $request)
    {
        $region = new Region();

        if (isset($request->id)) {
            $region->id = $request->id;
        }

        if (isset($request->order)) {
            $region->order = $request->order;
        }

        if (isset($request->description)) {
            $region->description = $request->description;
        }

        return $region;
    }

    /**
     * Convert to dto
     * 
     * @param Region region
     * 
     * @return DtoRegion
     */
    private static function _convertToDto(Region $region)
    {
        $dtoRegion = new DtoRegion();
        $dtoRegion->id = $region->id;
        $dtoRegion->order = $region->order;
        $dtoRegion->description = $region->description;
        return $dtoRegion;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get
     *
     * @param String $search
     * @return Region
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return Region::select('id', 'description')->get();
        } else {
            return Region::where('description', 'ilike','%'. $search . '%')->select('id', 'description')->get();
        }
    }

    /**
     * Get all
     * 
     * @return DtoRegion
     */
    public static function getAll()
    {
        $result = collect();
        foreach (Region::orderBy('order')->get() as $region) {
       
            $dtoRegion = new DtoRegion();
            $dtoRegion->id = $region->id;
            $dtoRegion->order = $region->order;
            $dtoRegion->description = $region->description;
            $result->push($dtoRegion); 

        }
        return $result;

    } 

    /**
     * Get by id 
     * 
     * @param int id
     * 
     * @return DtoRegion
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Region::find($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);
        DB::beginTransaction();

        try {
            $region = new Region();
            $region->order = $request->order;
            $region->description = $request->description;
            $region->created_id = $request->$idUser;
            $region->save();
           
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $region->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoRegion
     * @param int idLanguage
     */
    public static function update(Request $dtoRegion, int $idLanguage)
    {
        static::_validateData($dtoRegion, $idLanguage, DbOperationsTypesEnum::UPDATE);
        
        $regionOnDb = Region::find($dtoRegion->id);
        DB::beginTransaction();

        try {
            $regionOnDb->order = $dtoRegion->order;
            $regionOnDb->description = $dtoRegion->description;
            $regionOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE


    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {

        $region = Region::find($id);

        if (is_null($region)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $region->delete();
    }

    #endregion DELETE






    
}

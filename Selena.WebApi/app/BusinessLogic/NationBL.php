<?php

namespace App\BusinessLogic;

use App\Contact;
use App\DtoModel\DtoNation;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\LanguageNation;
use App\Mail\ContactRequestMail;
use App\Nation;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class NationBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Nation::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->cee)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->abbreviation)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SurnameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    /**
     * Conver to model
     * 
     * @param Request request
     * 
     * @return Contact
     */
    private static function _convertToModel(Request $request)
    {
        $nation = new Nation();

        if (isset($request->id)) {
            $nation->id = $request->id;
        }

        if (isset($request->cee)) {
            $nation->cee = $request->cee;
        }

        if (isset($request->abbreviation)) {
            $nation->abbreviation = $request->abbreviation;
        }

        if (isset($request->description)) {
            $nation->description = $request->description;
        }

        return $nation;
    }

    /**
     * Convert to dto
     * 
     * @param Nation nation
     * @param int idLanguage
     * 
     * @return DtoNation
     */
    private static function _convertToDto(Nation $nation)
    {
        $dtoNation = new DtoNation();
        $dtoNation->id = $nation->id;
        $dtoNation->cee = $nation->cee;
        $dtoNation->abbreviation = $nation->abbreviation;
        $dtoNation->description = LanguageNation::where('nation_id', $nation->id)->first()->description;
        return $dtoNation;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get
     *
     * @param String $search
     * @return LanguageNation
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return LanguageNation::select('nation_id as id', 'description')->get();
        } else {
            return LanguageNation::where('description', 'ilike', $search . '%')->select('nation_id as id', 'description')->get();
        }
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoNation
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (LanguageNation::where('language_id',$idLanguage)->get() as $nationAll) {
            $dtoNation = new DtoNation();
            $dtoNation->id = Nation::where('id', $nationAll->nation_id)->first()->id;
            $dtoNation->cee = Nation::where('id', $nationAll->nation_id)->first()->cee;
            $dtoNation->abbreviation = Nation::where('id', $nationAll->nation_id)->first()->abbreviation;
            $dtoNation->description = $nationAll->description;
            $result->push($dtoNation); 
        }
        return $result;
    }

    /**
     * Get by id
     * 
     * @param int id
     * 
     * @return DtoNation
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Nation::find($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);
        //$nationModel = static::_convertToModel($request);
        DB::beginTransaction();

        try {
            $nation = new Nation();
            $nation->cee = $request->cee;
            $nation->abbreviation = $request->abbreviation;
            $nation->created_id = $request->$idUser;
            $nation->save();

            $languageNation = new LanguageNation();
            $languageNation->language_id = $idLanguage;
            $languageNation->nation_id = $nation->id;
            $languageNation->description = $request->description;
            $languageNation->created_id = $request->$idUser;
            $languageNation->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $nation->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoNation
     * @param int idLanguage
     */
    public static function update(Request $dtoNation, int $idLanguage)
    {
        static::_validateData($dtoNation, $idLanguage, DbOperationsTypesEnum::UPDATE);
        
        $nationOnDb = Nation::find($dtoNation->id);
        $languageNationOnDb = LanguageNation::where('nation_id', $dtoNation->id)->first();
        
        DB::beginTransaction();

        try {
            $nationOnDb->cee = $dtoNation->cee;
            $nationOnDb->abbreviation = $dtoNation->abbreviation;
            $nationOnDb->save();

            $languageNationOnDb->language_id = $idLanguage;
            $languageNationOnDb->nation_id = $dtoNation->id;
            $languageNationOnDb->description = $dtoNation->description;
            $languageNationOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $languageNation = LanguageNation::where('nation_id', $id)->first();

        if (is_null($languageNation)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $languageNation->delete();

        $nation = Nation::find($id);

        if (is_null($nation)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $nation->delete();
    }

    #endregion DELETE
}

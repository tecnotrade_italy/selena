<?php

namespace App\BusinessLogic;

use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoBookingCalendar;
use App\DtoModel\DtoBookingAppointments;
use App\DtoModel\DtoBookingSettingsOrder;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\User;
use App\BookingAppointments;
use App\Connections;
use App\Places;
use App\Employees;
use App\BookingServices;
use App\BookingStates;
use App\BookingSettings;
use App\Message;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
//use Mockery\Exception;

class AppointmentsBL
{

     #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request DtoPlaces
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoBookingAppointments, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoBookingAppointments->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (BookingAppointments::find($DtoBookingAppointments->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }
    }

    /**
     * Convert to dto
     * 
     * @param BookingAppointments
     * 
     * @return DtoBookingAppointments
     */
    private static function _convertToDto($BookingAppointments)
    {
        $DtoBookingAppointments = new DtoBookingAppointments();

        if (isset($BookingAppointments->id)) {
            $DtoBookingAppointments->id = $BookingAppointments->id;
        }
        if (isset($BookingAppointments->connections_id)) {
            $DtoBookingAppointments->connections_id = $BookingAppointments->connections_id;
        }

        if (isset($BookingAppointments->connections_id)) {
            $Connections = Connections::where('id', $BookingAppointments->connections_id)->first();
            $DtoBookingAppointments->place_id = Places::where('id', $Connections->place_id)->first()->name;
            $DtoBookingAppointments->service_id = BookingServices::where('id', $Connections->service_id)->first()->name;
            $DtoBookingAppointments->employee_id = Employees::where('id', $Connections->employee_id)->first()->name;
            $DtoBookingAppointments->description =  $Connections->description;
        }else{
            $DtoBookingAppointments->place_id = '';
            $DtoBookingAppointments->service_id = '';
            $DtoBookingAppointments->employee_id = '';     
        }

        if (isset($BookingAppointments->user_id)) {
            $DtoBookingAppointments->user_id = User::where('id', $BookingAppointments->user_id)->first()->name;
        }else {
            $DtoBookingAppointments->user_id =  '';
        }
        if (isset($BookingAppointments->states_id)) {
            $DtoBookingAppointments->id_states = BookingStates::where('id', $BookingAppointments->states_id)->first()->description;
        }else{
            $DtoBookingAppointments->id_states = ''; 
        }
       
        if (isset($BookingAppointments->price)) {
            $DtoBookingAppointments->price = '€ ' . $BookingAppointments->price;
        }
        if (!is_null($BookingAppointments->start_time)) {
            $DtoBookingAppointments->start_time = 'Inizio: '. substr($BookingAppointments->start_time, 0, -3);
        }else{
            $DtoBookingAppointments->start_time = '';
        }
        if (!is_null($BookingAppointments->end_time)) {
            $DtoBookingAppointments->end_time = 'Fine: '. substr($BookingAppointments->end_time, 0, -3);
        }else{
            $DtoBookingAppointments->end_time = '';
        }

        if (!is_null($BookingAppointments->fixed_date)) {
            $DtoBookingAppointments->date_from = 'Data: '. Carbon::parse($BookingAppointments->fixed_date)->format(HttpHelper::getLanguageDateFormat());
        }else{
            $DtoBookingAppointments->date_from = 'Dal: '. Carbon::parse($BookingAppointments->date_from)->format(HttpHelper::getLanguageDateFormat());
        }       

        if (isset($BookingAppointments->date_to)) {
            $DtoBookingAppointments->date_to =  'Al: '. Carbon::parse($BookingAppointments->date_to)->format(HttpHelper::getLanguageDateFormat());
        }else{
            $DtoBookingAppointments->date_to = '';
        }
        if (isset($BookingAppointments->note)) {
            $DtoBookingAppointments->note =  'Note: '. $BookingAppointments->note;
        }else{
            $DtoBookingAppointments->note = '';
        }
        if (isset($BookingAppointments->nr_place)) {
            $DtoBookingAppointments->nr_place = $BookingAppointments->nr_place;
        }else{
            $DtoBookingAppointments->nr_place = '';
        }
        if (isset($BookingAppointments->reduced_price)) {
            $DtoBookingAppointments->reduced_price = $BookingAppointments->reduced_price;
        }else{
            $DtoBookingAppointments->reduced_price = '';
        }
        if (isset($BookingAppointments->total_price)) {
            $DtoBookingAppointments->total_price = '€ ' . $BookingAppointments->total_price;
        }else{
            $DtoBookingAppointments->total_price = '';
        }
        
        return $DtoBookingAppointments;   
    }

    #endregion PRIVATE

    #region GET
    /**
     * Get by id
     * 
     * @param int id
     * @return DtoPlaces
     */
    public static function getByIdEvent(int $id)
    {
        return static::_convertToDto(BookingAppointments::find($id));
    }

    /**
     * Get select
     *
     * @param String $search
     * @param int $idUser
     * 
     */
    public static function getSelect(string $search)
    {
                if ($search == "*") {
                    return BookingAppointments::select('id', 'name as description')->get();
                } else {
                    return BookingAppointments::where('name', 'ilike' , $search . '%')->select('id', 'name as description')->get();
          } 
    }



     /**
     * Get all
     * 
     * @return DtoBookingAppointments
     */
    public static function getAllAppointmentsSearch($DtoAppointments)
    { 
        $result = collect();

        $place_id = $DtoAppointments->place_id;
        $service_id = $DtoAppointments->service_id;
        $employee_id = $DtoAppointments->employee_id;
        $id_states = $DtoAppointments->id_states;

        if (!is_null($DtoAppointments->date_from)){
            $date_from = Carbon::createFromFormat(HttpHelper::getLanguageDateFormat(), $DtoAppointments->date_from)->format('Y/m/d');
        }else{
            $date_from = '';
        }

        if (!is_null($DtoAppointments->date_to)){
            $date_to = Carbon::createFromFormat(HttpHelper::getLanguageDateFormat(), $DtoAppointments->date_to)->format('Y/m/d');
        }else{
            $date_to = '';
        }

        $strWhere = "";
       /* if ($place_id != "") {
            $strWhere = $strWhere . ' OR bc.place_id = \'' . $place_id . '\'';
        }*/
        if ($service_id != "") {
            $strWhere = $strWhere . ' OR bc.service_id = \'' . $service_id . '\'';
        }
        if ($employee_id != "") {
            $strWhere = $strWhere . ' OR bc.employee_id = \'' . $employee_id . '\'';
        }
        if ($id_states != "") {
            $strWhere = $strWhere . ' OR ba.states_id = \'' . $id_states . '\'';
        }
        if ($date_from != "") {
            $strWhere = $strWhere . ' OR ba.date_from = \'' . $date_from . '\'';
        }
        if ($date_to != "") {
            $strWhere = $strWhere . ' OR ba.date_to = \'' . $date_to . '\'';
        }

        foreach (DB::select(
            'SELECT ba.id,ba.connections_id,bc.place_id,bc.service_id,bc.employee_id, ba.user_id,ba.states_id,
            ba.price,ba.start_time,ba.end_time,ba.date_from,ba.date_to
            from booking_appointments ba 
            inner join booking_connections bc on bc.id = ba.connections_id 
            WHERE bc.place_id = :place_id ' . $strWhere,
            [
                'place_id' => $place_id
            ]
        ) as $BookingAppointments) {

            $DtoBookingAppointments = new DtoBookingAppointments();
            
            if (isset($BookingAppointments->id)) {
                $DtoBookingAppointments->id = $BookingAppointments->id;
            }
            if (isset($BookingAppointments->connections_id)) {
                $DtoBookingAppointments->connections_id = $BookingAppointments->connections_id;
            }
    
            if (isset($BookingAppointments->connections_id)) {
                $Connections = Connections::where('id', $BookingAppointments->connections_id)->first();
                $DtoBookingAppointments->place_id = Places::where('id', $Connections->place_id)->first()->name;
                $DtoBookingAppointments->service_id = BookingServices::where('id', $Connections->service_id)->first()->name;
                $DtoBookingAppointments->employee_id = Employees::where('id', $Connections->employee_id)->first()->name;    
                $DtoBookingAppointments->description =  $Connections->description;        
            }else{
                $DtoBookingAppointments->place_id = '';
                $DtoBookingAppointments->service_id = '';
                $DtoBookingAppointments->employee_id = '';  
            }

            if (isset($BookingAppointments->user_id)) {
                $DtoBookingAppointments->user_id = User::where('id', $BookingAppointments->user_id)->first()->name;
            }else {
                $DtoBookingAppointments->user_id =  '';
            }
            if (isset($BookingAppointments->states_id)) {
                $DtoBookingAppointments->id_states = BookingStates::where('id', $BookingAppointments->states_id)->first()->description;
            }else {
                $DtoBookingAppointments->id_states = '';
            }
            if (isset($BookingAppointments->price)) {
                $DtoBookingAppointments->price = '€ ' . $BookingAppointments->price;
            }
            if (isset($BookingAppointments->start_time)) {
                $DtoBookingAppointments->start_time = substr($BookingAppointments->start_time, 0, -3);
            }
            if (isset($BookingAppointments->end_time)) {
                $DtoBookingAppointments->end_time = substr($BookingAppointments->end_time, 0, -3);
            }
            if (isset($BookingAppointments->fixed_date)) {
                $DtoBookingAppointments->fixed_date = Carbon::parse($BookingAppointments->fixed_date)->format(HttpHelper::getLanguageDateFormat());
            }
    
            if (isset($BookingAppointments->date_from)) {
                $DtoBookingAppointments->date_from = Carbon::parse($BookingAppointments->date_from)->format(HttpHelper::getLanguageDateFormat());
            }
            if (isset($BookingAppointments->date_to)) {
                $DtoBookingAppointments->date_to = Carbon::parse($BookingAppointments->date_to)->format(HttpHelper::getLanguageDateFormat());
            }
            if (isset($BookingAppointments->note)) {
                $DtoBookingAppointments->note = $BookingAppointments->note;
            }else{
                $DtoBookingAppointments->note = '';
            }
           
            
            $result->push($DtoBookingAppointments);
        }
            return $result; 
    }


    
     /**
     * getAllResultCalendar
     * 
     * @return DtoBookingAppointments
     */
    public static function getAllResultCalendar()
    {
        $result = collect();

        $DtoBookingCalendar = new DtoBookingCalendar();

        foreach (BookingAppointments::orderBy('id','DESC')->get() as $BookingAppointments) {    

            $DtoBookingAppointmentss = new DtoBookingAppointments();
            $DtoBookingAppointmentssWeek = new DtoBookingAppointments();
            $DtoBookingAppointmentssDay = new DtoBookingAppointments();

            foreach (BookingSettings::orderBy('id','DESC')->get() as $BookingSettings) {
                /* CONFIGURAZIONE MESE */
                if (isset($BookingAppointments->connections_id)) {
                    $Connections = Connections::where('id', $BookingAppointments->connections_id)->first();
                    $DtoBookingAppointmentss->place_id = Places::where('id', $Connections->place_id)->first()->name;
                    $DtoBookingAppointmentss->service_id = BookingServices::where('id', $Connections->service_id)->first()->name;
                    $DtoBookingAppointmentss->employee_id = Employees::where('id', $Connections->employee_id)->first()->name;
                }else{
                    $DtoBookingAppointmentss->place_id = '';
                    $DtoBookingAppointmentss->service_id = '';
                    $DtoBookingAppointmentss->employee_id = '';      
                }
                if (isset($BookingAppointments->states_id)) {
                    $DtoBookingAppointmentss->id_states = BookingStates::where('id', $BookingAppointments->states_id)->first()->description;
                }else{
                    $DtoBookingAppointmentss->id_states = ''; 
                }
                if (isset($BookingAppointments->user_id)) {
                    $DtoBookingAppointmentss->user_id = User::where('id', $BookingAppointments->user_id)->first()->name;
                }else {
                    $DtoBookingAppointmentss->user_id =  '';
                }
                if (isset($BookingAppointments->price)) {
                    $DtoBookingAppointmentss->price = '€ ' . $BookingAppointments->price;
                }else {
                    $DtoBookingAppointmentss->price =  '';
                }


                /* array mese ordinati */
                $arrayNotOrderMonth = array();
                $arrayOrderMonth = array();

                $DtoBookingSettingsOrderMonth = new DtoBookingSettingsOrder();
                $DtoBookingSettingsOrderMonth->field = 'order_user_month';
                $DtoBookingSettingsOrderMonth->order = $BookingSettings->order_user_month;
                $DtoBookingSettingsOrderMonth->fieldCheck = 'check_user';
                $DtoBookingSettingsOrderMonth->fieldTo = 'user_id';
                array_push($arrayOrderMonth,$DtoBookingSettingsOrderMonth);

                $DtoBookingSettingsOrderMonth = new DtoBookingSettingsOrder();
                $DtoBookingSettingsOrderMonth->field = 'order_service_month';
                $DtoBookingSettingsOrderMonth->order = $BookingSettings->order_service_month;
                $DtoBookingSettingsOrderMonth->fieldCheck = 'check_service';
                $DtoBookingSettingsOrderMonth->fieldTo = 'service_id';
                array_push($arrayOrderMonth,$DtoBookingSettingsOrderMonth);

                $DtoBookingSettingsOrderMonth = new DtoBookingSettingsOrder();
                $DtoBookingSettingsOrderMonth->field = 'order_employee_month';
                $DtoBookingSettingsOrderMonth->order = $BookingSettings->order_employee_month;
                $DtoBookingSettingsOrderMonth->fieldCheck = 'check_employee';
                $DtoBookingSettingsOrderMonth->fieldTo = 'employee_id';
                array_push($arrayOrderMonth,$DtoBookingSettingsOrderMonth);

                $DtoBookingSettingsOrderMonth = new DtoBookingSettingsOrder();
                $DtoBookingSettingsOrderMonth->field = 'order_place_month';
                $DtoBookingSettingsOrderMonth->order = $BookingSettings->order_place_month;
                $DtoBookingSettingsOrderMonth->fieldCheck = 'check_place';
                $DtoBookingSettingsOrderMonth->fieldTo = 'place_id';
                array_push($arrayOrderMonth,$DtoBookingSettingsOrderMonth);

                $DtoBookingSettingsOrderMonth = new DtoBookingSettingsOrder();
                $DtoBookingSettingsOrderMonth->field = 'order_states_month';
                $DtoBookingSettingsOrderMonth->order = $BookingSettings->order_states_month;
                $DtoBookingSettingsOrderMonth->fieldCheck = 'check_states';
                $DtoBookingSettingsOrderMonth->fieldTo = 'id_states';
                array_push($arrayOrderMonth,$DtoBookingSettingsOrderMonth);

                $DtoBookingSettingsOrderMonth = new DtoBookingSettingsOrder();
                $DtoBookingSettingsOrderMonth->field = 'order_price_month';
                $DtoBookingSettingsOrderMonth->order = $BookingSettings->order_price_month;
                $DtoBookingSettingsOrderMonth->fieldCheck = 'check_price';
                $DtoBookingSettingsOrderMonth->fieldTo = 'price';
                array_push($arrayOrderMonth,$DtoBookingSettingsOrderMonth);
                
                foreach ($arrayOrderMonth as $k => $d) {
                    $arrayNotOrderMonth[$k] = $d->order;
                }
                array_multisort($arrayNotOrderMonth, SORT_ASC, $arrayOrderMonth);


                $strFinaleTitle = "";

                foreach ($arrayOrderMonth as $k => $d) {
                    if($BookingSettings->{$d->fieldCheck}){
                        if($strFinaleTitle == ""){
                            $strFinaleTitle = $DtoBookingAppointmentss->{$d->fieldTo};
                        }else{
                            $strFinaleTitle = $strFinaleTitle . ' - ' . $DtoBookingAppointmentss->{$d->fieldTo};
                        }
                    }
                }
                /* array mese ordinati */
                $DtoBookingAppointmentss->title = $strFinaleTitle;

                $DtoBookingAppointmentss->id = $BookingAppointments->id;
        
                if (isset($BookingAppointments->fixed_date)) {
                    $DtoBookingAppointmentss->start = $BookingAppointments->fixed_date .' '. $BookingAppointments->start_time;
                    $DtoBookingAppointmentss->end = $BookingAppointments->fixed_date .' '. $BookingAppointments->end_time;    
                }else{
                    $DtoBookingAppointmentss->start = $BookingAppointments->date_from .' '. $BookingAppointments->start_time;
                    $DtoBookingAppointmentss->end = $BookingAppointments->date_to .' '. $BookingAppointments->end_time;    

                }

                /* END CONFIGURAZIONE MESE */


                /* CONFIGURAZIONE SETTIMANA */
                if (isset($BookingAppointments->connections_id)) {
                    $Connections = Connections::where('id', $BookingAppointments->connections_id)->first();
                    $DtoBookingAppointmentssWeek->place_id = Places::where('id', $Connections->place_id)->first()->name;
                    $DtoBookingAppointmentssWeek->service_id = BookingServices::where('id', $Connections->service_id)->first()->name;
                    $DtoBookingAppointmentssWeek->employee_id = Employees::where('id', $Connections->employee_id)->first()->name;
                }else{
                    $DtoBookingAppointmentssWeek->place_id = '';
                    $DtoBookingAppointmentssWeek->service_id = '';
                    $DtoBookingAppointmentssWeek->employee_id = '';      
                }
                if (isset($BookingAppointments->states_id)) {
                    $DtoBookingAppointmentssWeek->id_states = BookingStates::where('id', $BookingAppointments->states_id)->first()->description;
                }else{
                    $DtoBookingAppointmentssWeek->id_states = ''; 
                }
                if (isset($BookingAppointments->user_id)) {
                    $DtoBookingAppointmentssWeek->user_id = User::where('id', $BookingAppointments->user_id)->first()->name;
                }else {
                    $DtoBookingAppointmentssWeek->user_id =  '';
                }
                if (isset($BookingAppointments->price)) {
                    $DtoBookingAppointmentssWeek->price = '€ ' . $BookingAppointments->price;
                }else {
                    $DtoBookingAppointmentssWeek->price =  '';
                }

                /* array mese ordinati */
                $arrayNotOrderWeek = array();
                $arrayOrderWeek = array();

                $DtoBookingAppointmentssWeekOrderWeek = new DtoBookingSettingsOrder();
                $DtoBookingAppointmentssWeekOrderWeek->field = 'order_user_week';
                $DtoBookingAppointmentssWeekOrderWeek->order = $BookingSettings->order_user_week;
                $DtoBookingAppointmentssWeekOrderWeek->fieldCheck = 'check_user_week';
                $DtoBookingAppointmentssWeekOrderWeek->fieldTo = 'user_id';
                array_push($arrayOrderWeek,$DtoBookingAppointmentssWeekOrderWeek);

                $DtoBookingAppointmentssWeekOrderWeek = new DtoBookingSettingsOrder();
                $DtoBookingAppointmentssWeekOrderWeek->field = 'order_service_week';
                $DtoBookingAppointmentssWeekOrderWeek->order = $BookingSettings->order_service_week;
                $DtoBookingAppointmentssWeekOrderWeek->fieldCheck = 'check_service_week';
                $DtoBookingAppointmentssWeekOrderWeek->fieldTo = 'service_id';
                array_push($arrayOrderWeek,$DtoBookingAppointmentssWeekOrderWeek);

                $DtoBookingAppointmentssWeekOrderWeek = new DtoBookingSettingsOrder();
                $DtoBookingAppointmentssWeekOrderWeek->field = 'order_employee_week';
                $DtoBookingAppointmentssWeekOrderWeek->order = $BookingSettings->order_employee_week;
                $DtoBookingAppointmentssWeekOrderWeek->fieldCheck = 'check_employee_week';
                $DtoBookingAppointmentssWeekOrderWeek->fieldTo = 'employee_id';
                array_push($arrayOrderWeek,$DtoBookingAppointmentssWeekOrderWeek);

                $DtoBookingAppointmentssWeekOrderWeek = new DtoBookingSettingsOrder();
                $DtoBookingAppointmentssWeekOrderWeek->field = 'order_place_week';
                $DtoBookingAppointmentssWeekOrderWeek->order = $BookingSettings->order_place_week;
                $DtoBookingAppointmentssWeekOrderWeek->fieldCheck = 'check_place_week';
                $DtoBookingAppointmentssWeekOrderWeek->fieldTo = 'place_id';
                array_push($arrayOrderWeek,$DtoBookingAppointmentssWeekOrderWeek);

                $DtoBookingAppointmentssWeekOrderWeek = new DtoBookingSettingsOrder();
                $DtoBookingAppointmentssWeekOrderWeek->field = 'order_states_week';
                $DtoBookingAppointmentssWeekOrderWeek->order = $BookingSettings->order_states_week;
                $DtoBookingAppointmentssWeekOrderWeek->fieldCheck = 'check_states_week';
                $DtoBookingAppointmentssWeekOrderWeek->fieldTo = 'id_states';
                array_push($arrayOrderWeek,$DtoBookingAppointmentssWeekOrderWeek);

                $DtoBookingAppointmentssWeekOrderWeek = new DtoBookingSettingsOrder();
                $DtoBookingAppointmentssWeekOrderWeek->field = 'order_price_week';
                $DtoBookingAppointmentssWeekOrderWeek->order = $BookingSettings->order_price_week;
                $DtoBookingAppointmentssWeekOrderWeek->fieldCheck = 'check_price_week';
                $DtoBookingAppointmentssWeekOrderWeek->fieldTo = 'price';
                array_push($arrayOrderWeek,$DtoBookingAppointmentssWeekOrderWeek);

                foreach ($arrayOrderWeek as $k => $d) {
                    $arrayNotOrderWeek[$k] = $d->order;
                }
                array_multisort($arrayNotOrderWeek, SORT_ASC, $arrayOrderWeek);

                $strFinaleTitle = "";
                foreach ($arrayOrderWeek as $k => $d) {
                    if($BookingSettings->{$d->fieldCheck}){
                        if($strFinaleTitle == ""){
                            $strFinaleTitle = $DtoBookingAppointmentssWeek->{$d->fieldTo};
                        }else{
                            $strFinaleTitle = $strFinaleTitle . ' - ' . $DtoBookingAppointmentssWeek->{$d->fieldTo};
                        }
                    }
                }
                /* array mese ordinati */                   
                $DtoBookingAppointmentssWeek->title = $strFinaleTitle;

                $DtoBookingAppointmentssWeek->id = $BookingAppointments->id;
        
                if (isset($BookingAppointments->fixed_date)) {
                    $DtoBookingAppointmentssWeek->start = $BookingAppointments->fixed_date .' '. $BookingAppointments->start_time;
                    $DtoBookingAppointmentssWeek->end = $BookingAppointments->fixed_date .' '. $BookingAppointments->end_time;    
                }else{
                    $DtoBookingAppointmentssWeek->start = $BookingAppointments->date_from .' '. $BookingAppointments->start_time;
                    $DtoBookingAppointmentssWeek->end = $BookingAppointments->date_to .' '. $BookingAppointments->end_time;    

                }
                /* END CONFIGURAZIONE SETTIMANA */


                /* CONFIGURAZIONE GIORNO */
                if (isset($BookingAppointments->connections_id)) {
                    $Connections = Connections::where('id', $BookingAppointments->connections_id)->first();
                    $DtoBookingAppointmentssDay->place_id = Places::where('id', $Connections->place_id)->first()->name;
                    $DtoBookingAppointmentssDay->service_id = BookingServices::where('id', $Connections->service_id)->first()->name;
                    $DtoBookingAppointmentssDay->employee_id = Employees::where('id', $Connections->employee_id)->first()->name;
                }else{
                    $DtoBookingAppointmentssDay->place_id = '';
                    $DtoBookingAppointmentssDay->service_id = '';
                    $DtoBookingAppointmentssDay->employee_id = '';      
                }
                if (isset($BookingAppointments->states_id)) {
                    $DtoBookingAppointmentssDay->id_states = BookingStates::where('id', $BookingAppointments->states_id)->first()->description;
                }else{
                    $DtoBookingAppointmentssDay->id_states = ''; 
                }
                if (isset($BookingAppointments->user_id)) {
                    $DtoBookingAppointmentssDay->user_id = User::where('id', $BookingAppointments->user_id)->first()->name;
                }else {
                    $DtoBookingAppointmentssDay->user_id =  '';
                }
                if (isset($BookingAppointments->price)) {
                    $DtoBookingAppointmentssDay->price = '€ ' . $BookingAppointments->price;
                }else {
                    $DtoBookingAppointmentssDay->price =  '';
                }

                /* array mese ordinati */
                $arrayNotOrderDay = array();
                $arrayOrderDay = array();

                $DtoBookingAppointmentssDayOrderDay = new DtoBookingSettingsOrder();
                $DtoBookingAppointmentssDayOrderDay->field = 'order_user_day';
                $DtoBookingAppointmentssDayOrderDay->order = $BookingSettings->order_user_day;
                $DtoBookingAppointmentssDayOrderDay->fieldCheck = 'check_user_day';
                $DtoBookingAppointmentssDayOrderDay->fieldTo = 'user_id';
                array_push($arrayOrderDay,$DtoBookingAppointmentssDayOrderDay);

                $DtoBookingAppointmentssDayOrderDay = new DtoBookingSettingsOrder();
                $DtoBookingAppointmentssDayOrderDay->field = 'order_service_day';
                $DtoBookingAppointmentssDayOrderDay->order = $BookingSettings->order_service_day;
                $DtoBookingAppointmentssDayOrderDay->fieldCheck = 'check_service_day';
                $DtoBookingAppointmentssDayOrderDay->fieldTo = 'service_id';
                array_push($arrayOrderDay,$DtoBookingAppointmentssDayOrderDay);

                $DtoBookingAppointmentssDayOrderDay = new DtoBookingSettingsOrder();
                $DtoBookingAppointmentssDayOrderDay->field = 'order_employee_day';
                $DtoBookingAppointmentssDayOrderDay->order = $BookingSettings->order_employee_day;
                $DtoBookingAppointmentssDayOrderDay->fieldCheck = 'check_employee_day';
                $DtoBookingAppointmentssDayOrderDay->fieldTo = 'employee_id';
                array_push($arrayOrderDay,$DtoBookingAppointmentssDayOrderDay);

                $DtoBookingAppointmentssDayOrderDay = new DtoBookingSettingsOrder();
                $DtoBookingAppointmentssDayOrderDay->field = 'order_place_day';
                $DtoBookingAppointmentssDayOrderDay->order = $BookingSettings->order_place_day;
                $DtoBookingAppointmentssDayOrderDay->fieldCheck = 'check_place_day';
                $DtoBookingAppointmentssDayOrderDay->fieldTo = 'place_id';
                array_push($arrayOrderDay,$DtoBookingAppointmentssDayOrderDay);

                $DtoBookingAppointmentssDayOrderDay = new DtoBookingSettingsOrder();
                $DtoBookingAppointmentssDayOrderDay->field = 'order_states_day';
                $DtoBookingAppointmentssDayOrderDay->order = $BookingSettings->order_states_day;
                $DtoBookingAppointmentssDayOrderDay->fieldCheck = 'check_states_day';
                $DtoBookingAppointmentssDayOrderDay->fieldTo = 'id_states';
                array_push($arrayOrderDay,$DtoBookingAppointmentssDayOrderDay);

                $DtoBookingAppointmentssDayOrderDay = new DtoBookingSettingsOrder();
                $DtoBookingAppointmentssDayOrderDay->field = 'order_price_day';
                $DtoBookingAppointmentssDayOrderDay->order = $BookingSettings->order_price_day;
                $DtoBookingAppointmentssDayOrderDay->fieldCheck = 'check_price_day';
                $DtoBookingAppointmentssDayOrderDay->fieldTo = 'price';
                array_push($arrayOrderDay,$DtoBookingAppointmentssDayOrderDay);
                
                foreach ($arrayOrderDay as $k => $d) {
                    $arrayNotOrderDay[$k] = $d->order;
                }
                array_multisort($arrayNotOrderDay, SORT_ASC, $arrayOrderDay);

                $strFinaleTitle = "";

                foreach ($arrayOrderDay as $k => $d) {
                    if($BookingSettings->{$d->fieldCheck}){
                        if($strFinaleTitle == ""){
                            $strFinaleTitle = $DtoBookingAppointmentssDay->{$d->fieldTo};
                        }else{
                            $strFinaleTitle = $strFinaleTitle . ' - ' . $DtoBookingAppointmentssDay->{$d->fieldTo};
                        }
                    }
                }

                /* array mese ordinati */
                $DtoBookingAppointmentssDay->title = $strFinaleTitle;

                $DtoBookingAppointmentssDay->id = $BookingAppointments->id;
        
                if (isset($BookingAppointments->fixed_date)) {
                    $DtoBookingAppointmentssDay->start = $BookingAppointments->fixed_date .' '. $BookingAppointments->start_time;
                    $DtoBookingAppointmentssDay->end = $BookingAppointments->fixed_date .' '. $BookingAppointments->end_time;
                }else{
                    $DtoBookingAppointmentssDay->start = $BookingAppointments->date_from .' '. $BookingAppointments->start_time;
                    $DtoBookingAppointmentssDay->end = $BookingAppointments->date_to .' '. $BookingAppointments->end_time;    
                }
                /* END CONFIGURAZIONE GIORNO */
            }   

            $DtoBookingCalendar->BookingAppointmentssMonth->push($DtoBookingAppointmentss);      
            $DtoBookingCalendar->BookingAppointmentssWeek->push($DtoBookingAppointmentssWeek);      
            $DtoBookingCalendar->BookingAppointmentssDay->push($DtoBookingAppointmentssDay);      
            }
            
            $result->push($DtoBookingCalendar);
            return $result;
    }



    /**
     * Get all
     * 
     * @return DtoBookingAppointments
     */
    public static function getAll()
    {
        $result = collect();

        foreach (BookingAppointments::orderBy('id','DESC')->get() as $BookingAppointments) {    

                $DtoBookingAppointments = new DtoBookingAppointments();
            
                if (isset($BookingAppointments->id)) {
                    $DtoBookingAppointments->id = $BookingAppointments->id;
                }
                if (isset($BookingAppointments->connections_id)) {
                    $DtoBookingAppointments->connections_id = $BookingAppointments->connections_id;
                }

                if (isset($BookingAppointments->connections_id)) {
                    $Connections = Connections::where('id', $BookingAppointments->connections_id)->first();
                    $DtoBookingAppointments->place_id = Places::where('id', $Connections->place_id)->first()->name;
                    $DtoBookingAppointments->service_id = BookingServices::where('id', $Connections->service_id)->first()->name;
                    $DtoBookingAppointments->employee_id = Employees::where('id', $Connections->employee_id)->first()->name;
                    $DtoBookingAppointments->description =  $Connections->description;
                }else{
                    $DtoBookingAppointments->place_id = '';
                    $DtoBookingAppointments->service_id = '';
                    $DtoBookingAppointments->employee_id = '';     
                }

                if (isset($BookingAppointments->user_id)) {
                    $DtoBookingAppointments->user_id = User::where('id', $BookingAppointments->user_id)->first()->name;
                }else {
                    $DtoBookingAppointments->user_id =  '';
                }
                if (isset($BookingAppointments->states_id)) {
                    $DtoBookingAppointments->id_states = BookingStates::where('id', $BookingAppointments->states_id)->first()->description;
                }else{
                    $DtoBookingAppointments->id_states = ''; 
                }
               
                if (isset($BookingAppointments->total_price)) {
                    $DtoBookingAppointments->total_price = '€ ' . $BookingAppointments->total_price;
                }
                if (isset($BookingAppointments->start_time)) {
                    $DtoBookingAppointments->start_time = substr($BookingAppointments->start_time, 0, -3);
                }
                if (isset($BookingAppointments->end_time)) {
                    $DtoBookingAppointments->end_time = substr($BookingAppointments->end_time, 0, -3);
                }
                if (isset($BookingAppointments->fixed_date)) {
                    $DtoBookingAppointments->fixed_date = Carbon::parse($BookingAppointments->fixed_date)->format(HttpHelper::getLanguageDateFormat());
                }

                if (isset($BookingAppointments->date_from)) {
                    $DtoBookingAppointments->date_from = Carbon::parse($BookingAppointments->date_from)->format(HttpHelper::getLanguageDateFormat());
                }
                if (isset($BookingAppointments->date_to)) {
                    $DtoBookingAppointments->date_to = Carbon::parse($BookingAppointments->date_to)->format(HttpHelper::getLanguageDateFormat());
                }
                if (isset($BookingAppointments->note)) {
                    $DtoBookingAppointments->note = $BookingAppointments->note;
                }else{
                    $DtoBookingAppointments->note = '';
                }
                
                    $result->push($DtoBookingAppointments);
                }
                return $result;

    }
    #endregion GET

    #region INSERT
    /**
     * Insert
     * 
     * @param Request DtoBookingAppointments
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoBookingAppointments, int $idUser)
    {

       DB::beginTransaction();
        try {
            $BookingAppointments = new BookingAppointments();
            
        if (isset($DtoBookingAppointments->name)) {
            $BookingAppointments->name = $DtoBookingAppointments->name;
        } 
         if (isset($DtoBookingAppointments->description)) {
            $BookingAppointments->description = $DtoBookingAppointments->description;
        } 
         if (isset($DtoBookingAppointments->email)) {
            $BookingAppointments->email = $DtoBookingAppointments->email;
        } 
        if (isset($DtoBookingAppointments->telephone_number)) {
            $BookingAppointments->telephone_number = $DtoBookingAppointments->telephone_number;
        } 
        $BookingAppointments->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $BookingAppointments->id;
    }
    #endregion INSERT

    /**
     * Update
     * 
     * @param Request [DtoBookingAppointments]
     * @param int idLanguage
     */
    public static function update(Request $DtoBookingAppointments, int $idUser)
    {
       
        $EmployeesOnDb = BookingAppointments::find($DtoBookingAppointments->id);
      
        DB::beginTransaction();

        try {
            if (isset($DtoBookingAppointments->id)) {
                $EmployeesOnDb->id = $DtoBookingAppointments->id;
            } 
            if (isset($DtoBookingAppointments->name)) {
                $EmployeesOnDb->name = $DtoBookingAppointments->name;
            } 
             if (isset($DtoBookingAppointments->description)) {
                $EmployeesOnDb->description = $DtoBookingAppointments->description;
            } 
             if (isset($DtoBookingAppointments->email)) {
                $EmployeesOnDb->email = $DtoBookingAppointments->email;
            } 
             if (isset($DtoBookingAppointments->telephone_number)) {
                $EmployeesOnDb->telephone_number = $DtoBookingAppointments->telephone_number;
            } 
            
            $EmployeesOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return  $DtoBookingAppointments->id;
    }
    #endregion UPDATE


        /**
             * Clone
             * 
             * @param int id
             * @param int idFunctionality
             * @param int idLanguage
             * @param int idUser
             * 
             * @return int
             */
            public static function clone(int $id, int $idFunctionality, int $idUser, int $idLanguage)
            {
                $BookingAppointments = BookingAppointments::find($id);            
                DB::beginTransaction();

                try {
                    $newEmployees = new BookingAppointments();
                    $newEmployees->name = $BookingAppointments->name;
                    $newEmployees->description = $BookingAppointments->description . ' - Copy';
                    $newEmployees->email = $BookingAppointments->email;
                    $newEmployees->telephone_number = $BookingAppointments->telephone_number;
                    $newEmployees->save();
   
                    DB::commit();
                    return $newEmployees->id;
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
            }
        #endregion CLONE

            #region DELETE
                    /**
                     * Delete
                     * 
                     * @param int id
                     * @param int idLanguage
                     */
                    public static function delete(int $id, int $idLanguage)
                    {
                        $BookingAppointments = BookingAppointments::find($id);                   

                        if (is_null($BookingAppointments)) {
                            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PeopleRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
                        }
                        DB::beginTransaction();
                        try {
                            $BookingAppointments->delete();
                            
                            DB::commit();
                        } catch (Exception $ex) {
                            DB::rollback();
                            throw $ex;
                        }
                    }
                
                }
        #endregion DELETE
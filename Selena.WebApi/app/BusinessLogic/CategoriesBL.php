<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoCategories;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\LogHelper;
use App\CategoryLanguage;
use App\CategoryFather;
use App\CategoryItem;
use App\CategoryTypeLanguage;
use App\Category;
use App\Content;
use App\ContentLanguage;
use App\Item;
use App\ItemLanguage;
use App\Language;
use App\DtoModel\DtoCategoriesList;
use App\DtoModel\DtoCategoryItemDetail;
use App\DtoModel\DtoItemImages;
use App\DtoModel\DtoItemLanguages;
use App\Helpers\ArrayHelper;
use App\SelenaSqlViews;
//use App\CategoryType;
use Intervention\Image\Facades\Image;
use App\User;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Http\Request;
use App\DtoModel\DtoUrlList;
use App\DtoModel\DtoUrlListDetail;
use App\DtoModel\DtoPageSitemap;

class CategoriesBL
{
    public static $dirImage = '../storage/app/public/images/Categories/';
    private static $mimeTypeAllowed = ['jpeg', 'jpg', 'png', 'gif'];

    #region PRIVATE

    /**
     * Convert image size
     * 
     * @param int byte
     *  
     */
    private static function _convertImageSize(int $bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    /**
     * Validate data
     * 
     * @param Request CategoriesBL
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoCategories, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoCategories->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Category::find($DtoCategories->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($DtoCategories->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoCategories->code)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CodeNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoCategories->link)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::LinkNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }

        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/Categories/';
    }

    /**
     * Convert to dto
     * 
     * @param Categories postalCode
     * 
     * @return DtoCategories
     */
    private static function _convertToDto($Categories)
    {

        $DtoCategories = new DtoCategories();

        if (isset($Categories->id)) {
            $DtoCategories->id = $Categories->id;
        }

        if (isset($Categories->order)) {
            $DtoCategories->order = $Categories->order;
        }
        if (isset($Categories->description)) {
            $DtoCategories->description = $Categories->description;
        }

        if (isset($Categories->category_type_id)) {
            $DtoCategories->category_type_id = $Categories->category_type_id;
        }

        if (isset($Categories->category_father_id)) {
            $DtoCategories->category_father_id = $Categories->category_father_id;
        }

        if (isset($Categories->meta_tag_characteristic)) {
            $DtoCategories->characteristic = $Categories->meta_tag_characteristic;
        }

        if (isset($Categories->img)) {
            $DtoCategories->img = $Categories->img;
        }

        if (isset($Categories->meta_tag_title)) {
            $DtoCategories->meta_tag_title = $Categories->meta_tag_title;
        }


        if (isset($Categories->meta_tag_description)) {
            $DtoCategories->meta_tag_description = $Categories->meta_tag_description;
        }

        if (isset($Categories->keyword)) {
            $DtoCategories->meta_tag_keyword = $Categories->keyword;
        }

        if (isset($Categories->meta_tag_link)) {
            $DtoCategories->link = $Categories->meta_tag_link;
        }

        if (isset($Categories->code)) {
            $DtoCategories->code = $Categories->code;
        }

        if (isset($Categories->online)) {
            $DtoCategories->online = $Categories->online;
        }


        return  $DtoCategories;
    }


    /**
     * Convert to VatType
     * 
     * @param DtoCategories dtoVatType
     * 
     * @return Categories
     */
    private static function _convertToModel($DtoCategories)
    {
        $Categories = new Category();

        if (isset($DtoCategories->id)) {
            $Categories->id = $DtoCategories->id;
        }

        if (isset($DtoCategories->order)) {
            $Categories->order = $DtoCategories->order;
        }

        if (isset($DtoCategories->description)) {
            $Categories->description = $DtoCategories->description;
        }

        if (isset($DtoCategories->category_type_id)) {
            $Categories->category_type_id = $DtoCategories->category_type_id;
        }

        if (isset($DtoCategories->category_father_id)) {
            $Categories->category_father_id = $DtoCategories->category_father_id;
        }

        if (isset($DtoCategories->language_id)) {
            $Categories->language_id = $DtoCategories->language_id;
        }

        if (isset($DtoCategories->code)) {
            $Categories->code = $DtoCategories->code;
        }

        if (isset($DtoCategories->online)) {
            $Categories->online = $DtoCategories->online;
        }
        if (isset($DtoCategories->characteristic)) {
            $Categories->meta_tag_characteristic = $DtoCategories->characteristic;
        }
        if (isset($DtoCategories->link)) {
            $Categories->meta_tag_link = $DtoCategories->link;
        }

        if (isset($DtoCategories->meta_tag_title)) {
            $Categories->meta_tag_title = $DtoCategories->meta_tag_title;
        }
        if (isset($DtoCategories->meta_tag_description)) {
            $Categories->meta_tag_description = $DtoCategories->meta_tag_description;
        }

        if (isset($DtoCategories->meta_tag_keyword)) {
            $Categories->keyword = $DtoCategories->meta_tag_keyword;
        }

        if (isset($DtoCategories->img)) {
            $Categories->img = $DtoCategories->img;
        }

        return $Categories;
    }

    /**
     * Get sub categories
     *
     * @param $rows
     * @param int $idCategoryFather
     * @param string $url
     */
    private static function _getSubCategories($rows, $idCategoryFather, $url)
    {
        $dtoSubCategoriesLists = collect();

        foreach (ArrayHelper::toCollection($rows)->whereIn('category_father_id', $idCategoryFather)->sortBy('order') as $row) {
            $dtoSubCategoriesList = new DtoCategories();
            $dtoSubCategoriesList->id = $row->id;
            $dtoSubCategoriesList->description = $row->description;
            $dtoSubCategoriesList->code = $row->code;
            $dtoSubCategoriesList->link = $row->meta_tag_link;
            if ($url == $row->meta_tag_link) {
                $dtoSubCategoriesList->active = true;
            } else {
                $dtoSubCategoriesList->active = false;
            }
            $dtoSubCategoriesList->categoriesList = static::_getSubCategories($rows, $row->id, $url);
            $dtoSubCategoriesLists->push($dtoSubCategoriesList);
        }

        return $dtoSubCategoriesLists;
    }

    /**
     * Get sub categories
     *
     * @param int $idCategoryFather
     */
    private static function _getSubAllListCategories($idCategoryFather, $idItem)
    {
        $dtoSubCategoriesLists = collect();

        foreach (CategoryFather::where('category_father_id', $idCategoryFather)->orderBy('order')->get() as $categoryAll) {
            $dtoSubCategoriesList = new DtoCategories();
            $Categories = Category::where('id', $categoryAll->category_id)->first();
            $CategoriesLanguages = CategoryLanguage::where('category_id', $categoryAll->category_id)->first();

            $dtoSubCategoriesList->id = $categoryAll->category_id;
            $dtoSubCategoriesList->order = $Categories->order;
            $dtoSubCategoriesList->description = $CategoriesLanguages->description;

            $itemCategory = CategoryItem::where('item_id', $idItem)->where('category_id', $categoryAll->category_id)->get()->first();

            $dtoSubCategoriesList->checked = false;
            if (isset($itemCategory)) {
                $dtoSubCategoriesList->checked = true;
            }

            $dtoSubCategoriesList->categoriesList = static::_getSubAllListCategories($categoryAll->category_id, $idItem);

            $dtoSubCategoriesLists->push($dtoSubCategoriesList);
        }

        return $dtoSubCategoriesLists;
    }

    

    /**
     * Get sub categories
     *
     * @param int $idCategoryFather
     */
    private static function _getSubAllTableCategoriesFather($idCategoryFather, $result, $level)
    {
        $level = $level + 1;
        foreach (CategoryFather::where('category_father_id', $idCategoryFather)->orderBy('order')->get() as $categoryAll) {
            $DtoCategories = new DtoCategories();
            $Categories = Category::where('id', $categoryAll->category_id)->first();
            $CategoriesLanguages = CategoryLanguage::where('category_id', $categoryAll->category_id)->first();

            $DtoCategories->id = $categoryAll->category_id;
            $DtoCategories->category_father_id = $categoryAll->id;
            if(is_null($categoryAll->order)){
                $DtoCategories->order = 0;
            }else{
                $DtoCategories->order = $categoryAll->order;
            }
            $DtoCategories->level = $level;
            $DtoCategories->description = $CategoriesLanguages->description;
            $DtoCategories->code = $Categories->code;
            $DtoCategories->nuberOfItem = static::_getAllItemByCategory($categoryAll->category_id, 0);
            $DtoCategories->img = $Categories->img;
            $DtoCategories->online = $Categories->online;
            $DtoCategories->link = $CategoriesLanguages->meta_tag_link;
            $DtoCategories->urlTable = $CategoriesLanguages->meta_tag_link;
            $DtoCategories->meta_tag_title = $CategoriesLanguages->meta_tag_title;
            $DtoCategories->meta_tag_description = $CategoriesLanguages->meta_tag_description;
            $DtoCategories->meta_tag_keyword = $CategoriesLanguages->keyword;
            $DtoCategories->category_type_id = $Categories->category_type_id;
            $DtoCategories->characteristic = $CategoriesLanguages->meta_tag_characteristic;
            $DtoCategories->visible_from = $Categories->visible_from;
            $DtoCategories->visible_to = $Categories->visible_to;

            $result->push($DtoCategories);
            $result = static::_getSubAllTableCategoriesFather($categoryAll->category_id, $result, $level);
        }

        return $result;
    }
    
    /**
     * Get sub categories
     *
     * @param int $idCategoryFather
     */
    private static function _getListItemCategoriesId($idCategory){
        $dtoItemList = collect(); 
        foreach (CategoryItem::where('category_id', $idCategory)->orderBy('order')->get() as $itemAll) {

            $items = Item::where('id', $itemAll->item_id)->get()->first();
            if(isset($items)){
               $itemLanguages = ItemLanguage::where('item_id', $items->id)->first();
                if(isset($itemLanguages)){
                    $dtoDetailListCategories = new DtoCategoryItemDetail();
                    $dtoDetailListCategories->id = $itemAll->id;
                    $dtoDetailListCategories->item_id = $itemAll->item_id;

                    if(is_null($itemAll->order)){
                        $dtoDetailListCategories->order = 0;
                    }else{
                        $dtoDetailListCategories->order = $itemAll->order;
                    }

                    if(is_null($items->online)){
                        $dtoDetailListCategories->online = false;
                    }else{
                        $dtoDetailListCategories->online = $items->online;
                    }
                    $dtoDetailListCategories->code = $items->internal_code;
                    $dtoDetailListCategories->description = ItemLanguage::where('item_id', $items->id)->first()->description;
                            
                    $dtoItemList->push($dtoDetailListCategories);
                }
            }
        }

        return $dtoItemList;
    }


    /**
     * Get sub categories
     *
     * @param int $idCategoryFather
     */
    private static function _getSubAllListCategoriesFather($idCategoryFather, $idItem)
    {
        $dtoSubCategoriesLists = collect();

        foreach (CategoryFather::where('category_father_id', $idCategoryFather)->orderBy('order')->get() as $categoryAll) {
            $dtoSubCategoriesList = new DtoCategories();
            $Categories = Category::where('id', $categoryAll->category_id)->first();
            $CategoriesLanguages = CategoryLanguage::where('category_id', $categoryAll->category_id)->first();

            $dtoSubCategoriesList->id = $categoryAll->category_id;
            $dtoSubCategoriesList->category_father_id = $categoryAll->id;
            if(is_null($categoryAll->order)){
                $dtoSubCategoriesList->order = 0;
            }else{
                $dtoSubCategoriesList->order = $categoryAll->order;
            }
            $dtoSubCategoriesList->description = $CategoriesLanguages->description;

            if(is_null($Categories->online)){
                $dtoSubCategoriesList->online = false;
            }else{
                $dtoSubCategoriesList->online = $Categories->online;
            }

            $dtoSubCategoriesList->code = $Categories->code;

            if($idItem!=$categoryAll->category_id){
                $itemCategory = CategoryFather::where('category_id', $idItem)->where('category_father_id', $categoryAll->category_id)->get()->first();

                $dtoSubCategoriesList->checked = false;
                if (isset($itemCategory)) {
                    $dtoSubCategoriesList->checked = true;
                }
            }else{
                $dtoSubCategoriesList->checked = true;
            }
            $dtoSubCategoriesList->itemsList = static::_getListItemCategoriesId($categoryAll->category_id);
            $dtoSubCategoriesList->categoriesList = static::_getSubAllListCategoriesFather($categoryAll->category_id, $idItem);

            $dtoSubCategoriesLists->push($dtoSubCategoriesList);
        }

        return $dtoSubCategoriesLists;
    }

    /**
     * Get sub categories
     *
     * @param int $idCategoryFather
     */
    private static function _getBreadCrumbsListCategory($idCategory, $routeCategory)
    {
        $CategoryFather = CategoryFather::where('category_id', $idCategory)->whereNotNull('category_father_id')->get()->first();
        if(isset($CategoryFather)){
            if($routeCategory != ''){
                $routeCategory = CategoryLanguage::where('category_id', $CategoryFather->category_father_id)->get()->first()->description . ' > ' .  $routeCategory;
            }else{
                $routeCategory = CategoryLanguage::where('category_id', $CategoryFather->category_father_id)->get()->first()->description;
            }
            
            $routeCategory = static::_getBreadCrumbsListCategory($CategoryFather->category_father_id, $routeCategory);
        }else{
            if($routeCategory != ''){
                $routeCategory = 'Categoria principale > ' . $routeCategory;
            }else{
                $routeCategory = 'Categoria principale';
            }
        }
        
        return $routeCategory;
    }

    /**
     * Get sub categories
     *
     * @param int $idCategoryFather
     */
    private static function _getAllItemByCategory($idCategory, $contItem)
    {
        $itemCategory = CategoryItem::where('category_id', $idCategory)->get();
        
        if(isset($itemCategory)){
            $numberItemCategory = $itemCategory->count();
        }

        if($numberItemCategory == 0){
            $numberItemCategory = '';
        }
        /*
        foreach (CategoryFather::where('category_father_id', $idCategory)->orderBy('order')->get() as $CategoryFather) {
            if(isset($CategoryFather)){
                $numberItemCategory = static::_getAllItemByCategory($CategoryFather->category_id, $numberItemCategory);
            }
        }*/
             
        
        return $numberItemCategory;
    }
    
    


    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCategories
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Category::find($id));
    }

    /**
     * Get select
     *
     * @param String $search
     * @return CategoryLanguage
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return CategoryLanguage::select('categories_languages.id', 'categories_languages.description')->join('categories', 'categories.id', '=', 'categories_languages.category_id')->where('categories.online', 't')->orderBy('categories_languages.description','ASC')->get();
        } else {
            return CategoryLanguage::where('categories_languages.description', 'ilike', $search . '%')->join('categories', 'categories.id', '=', 'categories_languages.category_id')->where('categories.online', 't')->select('categories_languages.id', 'categories_languages.description')->orderBy('categories_languages.description','ASC')->get();
        }
    }

    /**
     * Get select
     *
     * @param String $search
     * @param int $idCatFather
     * @return CategoryLanguage
     */
    public static function getSelectCategoryItemAttachment(string $search)
    {
        if ($search == "*" || $search == '') {
            return collect(DB::select(
                "SELECT catA.id as id, 
                (
                    case when
                        (
                        select 
                        replace(replace(replace(rtrim(catB.description),'\"',''),',',''),'+','')
                        from 
                        categories_languages as catB
                        inner join categories_father on catB.category_id = categories_father.category_father_id
                        where categories_father.category_id = catA.category_id
                        limit 1
                        ) is null then ''
                        else
                        (
                        select 
                        replace(replace(replace(rtrim(catB.description),'\"',''),',',''),'+','')
                        from 
                        categories_languages as catB
                        inner join categories_father on catB.category_id = categories_father.category_father_id
                        where categories_father.category_id = catA.category_id
                        limit 1
                        ) || ' - '
                        end
                    
                     || replace(replace(replace(rtrim(catA.description),'\"',''),',',''),'+','')
                ) as description
                from categories_languages as catA
                inner join categories on catA.category_id = categories.id
                where online = true 
                and 
                    (
                        case when
                            (
                            select 
                            replace(replace(replace(rtrim(catB.description),'\"',''),',',''),'+','')
                            from 
                            categories_languages as catB
                            inner join categories_father on catB.category_id = categories_father.category_father_id
                            where categories_father.category_id = catA.category_id
                            limit 1
                            ) is null then ''
                            else
                            (
                            select 
                            replace(replace(replace(rtrim(catB.description),'\"',''),',',''),'+','')
                            from 
                            categories_languages as catB
                            inner join categories_father on catB.category_id = categories_father.category_father_id
                            inner join categories on catB.category_id = categories.id
                            where categories_father.category_id = catA.category_id
                            and categories.online = true
                            limit 1
                            ) || ' / '
                            end
                        
                        || replace(replace(replace(rtrim(catA.description),'\"',''),',',''),'+','')
                    ) is not null
                order by catA.id"
            ));
        } else {
            return collect(DB::select(
                "SELECT catA.id, 
                (
                    case when
                        (
                        select 
                        replace(replace(replace(rtrim(catB.description),'\"',''),',',''),'+','')
                        from 
                        categories_languages as catB
                        inner join categories_father on catB.category_id = categories_father.category_father_id
                        where categories_father.category_id = catA.category_id
                        limit 1
                        ) is null then ''
                        else
                        (
                        select 
                        replace(replace(replace(rtrim(catB.description),'\"',''),',',''),'+','')
                        from 
                        categories_languages as catB
                        inner join categories_father on catB.category_id = categories_father.category_father_id
                        where categories_father.category_id = catA.category_id
                        limit 1
                        ) || ' - '
                        end
                    
                     || replace(replace(replace(rtrim(catA.description),'\"',''),',',''),'+','')
                ) as description
                from categories_languages as catA
                inner join categories on catA.category_id = categories.id
                where online = true 
                and 
                    (
                        case when
                            (
                            select 
                            replace(replace(replace(rtrim(catB.description),'\"',''),',',''),'+','')
                            from 
                            categories_languages as catB
                            inner join categories_father on catB.category_id = categories_father.category_father_id
                            where categories_father.category_id = catA.category_id
                            limit 1
                            ) is null then ''
                            else
                            (
                            select 
                            replace(replace(replace(rtrim(catB.description),'\"',''),',',''),'+','')
                            from 
                            categories_languages as catB
                            inner join categories_father on catB.category_id = categories_father.category_father_id
                            inner join categories on catB.category_id = categories.id
                            where categories_father.category_id = catA.category_id
                            and categories.online = true
                            limit 1
                            ) || ' / '
                            end
                        
                        || replace(replace(replace(rtrim(catA.description),'\"',''),',',''),'+','')
                    ) is not null
                and categories_languages.description ilike '%" . $search . "%'
                order by catA.id"

            ));
        }
    }

    /**
     * Get select
     *
     * @param String $search
     * @param int $idCatFather
     * @return CategoryLanguage
     */
    public static function getSelectPrincipalCategories(string $search)
    {
        if ($search == "*" || $search == '') {
            return collect(DB::select(
                'SELECT categories_languages.id as id, categories_languages.description as description FROM categories_languages
                INNER JOIN categories ON categories.id = categories_languages.category_id
                INNER JOIN categories_father ON categories.id = categories_father.category_id
                WHERE categories_father.category_father_id IS NULL '
            ));
        } else {
            return collect(DB::select(
                'SELECT categories_languages.id as id, categories_languages.description as description FROM categories_languages
                INNER JOIN categories ON categories.id = categories_languages.category_id
                INNER JOIN categories_father ON categories.id = categories_father.category_id
                WHERE categories_father.category_father_id iS NULL AND categories_languages.description ilike \'' . $search . '%\''
            ));
        }
    }

    /**
     * Get select
     *
     * @param String $search
     * @param int $idCatFather
     * @return CategoryLanguage
     */
    public static function getSelectSubCategories(int $idCatFather, string $search)
    {
        if ($search == "*" || $search == '') {
            return collect(DB::select(
                'SELECT categories_languages.id as id, categories_languages.description as description FROM categories_languages
                INNER JOIN categories ON categories.id = categories_languages.category_id
                INNER JOIN categories_father ON categories.id = categories_father.category_id
                WHERE categories_father.category_father_id in (SELECT category_id FROM categories_languages WHERE id = :idCatFather) ',
                ['idCatFather' => $idCatFather]
            ));
        } else {
            return collect(DB::select(
                'SELECT categories_languages.id as id, categories_languages.description as description FROM categories_languages
                INNER JOIN categories ON categories.id = categories_languages.category_id
                INNER JOIN categories_father ON categories.id = categories_father.category_id
                WHERE categories_father.category_father_id in (SELECT category_id FROM categories_languages WHERE id = :idCatFather) AND categories_languages.description ilike \'' . $search . '%\'',
                ['idCatFather' => $idCatFather]
            ));
        }
    }

    /**
     * Get url list
     *
     * @param int idLanguage
     * @return DtoUrlList
     */
    public static function getUrlSitemap(int $idLanguage)
    {
        $dtoUrlLinks = collect();
        $idDefaultLanguage = LanguageBL::getDefault();

        foreach (Category::all() as $page) {
            $languageFound = false;

            $dtoUrlList = new DtoUrlList();
            $dtoUrlList->idPage = $page->id;

            foreach (CategoryLanguage::where('category_id', $page->id)->get() as $languageItem) {
                //if ($languageItem->language_id == $idLanguage || ($languageItem->language_id == $idDefaultLanguage && !$languageFound)) {
                    $dtoUrlList->title = $languageItem->description;
                //    $languageFound = true;
                //}

                $dtoUrlLinkDetail = new DtoUrlListDetail();
                $dtoUrlLinkDetail->id = $languageItem->id;
                $dtoUrlLinkDetail->idLanguage = $languageItem->language_id;
                $dtoUrlLinkDetail->url = $languageItem->meta_tag_link;
                $dtoUrlList->urlListDetail->push($dtoUrlLinkDetail);
            }

            $dtoUrlLinks->push($dtoUrlList);
        }
        return $dtoUrlLinks;
    }

    public static function getPages(string $page_type, int $idLanguage)
    {

        $result = collect();
        foreach (DB::select(
            "SELECT categories_languages.description, categories.updated_at, categories_languages.meta_tag_link
            FROM categories
            INNER JOIN categories_languages ON categories.id = categories_languages.category_id
            AND categories_languages.language_id = " . $idLanguage . " ORDER BY categories.updated_at desc"
        ) as $PagesAll) {
            $dtoPageSitemap = new DtoPageSitemap();
            $dtoPageSitemap->url = $PagesAll->meta_tag_link;
            $dtoPageSitemap->title = $PagesAll->description;
            $dtoPageSitemap->updated_at = $PagesAll->updated_at;

            $result->push($dtoPageSitemap);
        }
        return $result;
    }

    /**
     * Get all
     * 
     * @return DtoCategories
     */
    public static function getAll($idLanguage)

    {

       /* $result = collect();

        foreach (CategoryLanguage::where('language_id', $idLanguage)->get() as $categoryAll) {
            

            $DtoCategories = new DtoCategories();
            $Categories = Category::where('id', $categoryAll->category_id)->first();

            //$CategoriesLanguages = CategoryLanguage::where('category_id', $categoryAll->id)->first();

            $DtoCategories->id = $categoryAll->category_id;
            $DtoCategories->order = $Categories->order;
            $DtoCategories->description = $categoryAll->description;
            $DtoCategories->code = $Categories->code;
            $DtoCategories->img = $Categories->img;
            $DtoCategories->online = $Categories->online;
            $DtoCategories->link = $categoryAll->meta_tag_link;
            $DtoCategories->urlTable = $categoryAll->meta_tag_link;
            $DtoCategories->meta_tag_title = $categoryAll->meta_tag_title;
            $DtoCategories->meta_tag_description = $categoryAll->meta_tag_description;
            $DtoCategories->meta_tag_keyword = $categoryAll->keyword;
            $DtoCategories->category_type_id = $Categories->category_type_id;
            $DtoCategories->characteristic = $categoryAll->meta_tag_characteristic;
            $DtoCategories->visible_from = $Categories->visible_from;
            $DtoCategories->visible_to = $Categories->visible_to;

            if (isset($Categories->category_father_id)) {
                $IdCatPadre = Category::where('id', $Categories->category_father_id)->first()->id;

                if (isset($IdCatPadre)) {
                    $descrCatPadre = CategoryLanguage::where('category_id', $IdCatPadre)->first()->id;
                } else {
                    $descrCatPadre = "";
                }
            } else {
                $descrCatPadre = "";
            }

            $DtoCategories->category_father_id = $descrCatPadre;

            $DtoCategories->categoryFather = static::_getBreadCrumbsListCategory($categoryAll->category_id, '');


            $result->push($DtoCategories);
        }
        return $result;*/
        
        $result = collect();
        foreach (CategoryFather::whereNull('category_father_id')->orderBy('order')->get() as $categoryAll) {
            
            $DtoCategories = new DtoCategories();
            $Categories = Category::where('id', $categoryAll->category_id)->first();
            $CategoriesLanguages = CategoryLanguage::where('category_id', $categoryAll->category_id)->first();

            $DtoCategories->id = $categoryAll->category_id;
            $DtoCategories->category_father_id = $categoryAll->id;
            
            if(is_null($categoryAll->order)){
                $DtoCategories->order = 0;
            }else{
                $DtoCategories->order = $categoryAll->order;
            }
            $DtoCategories->level = 0;
            $DtoCategories->description = $CategoriesLanguages->description;
            //$DtoCategories->nuberOfItem = static::_getAllItemByCategory($categoryAll->category_id, 0);
            $DtoCategories->code = $Categories->code;
            $DtoCategories->img = $Categories->img;
            $DtoCategories->online = $Categories->online;
            $DtoCategories->link = $CategoriesLanguages->meta_tag_link;
            $DtoCategories->urlTable = $CategoriesLanguages->meta_tag_link;
            $DtoCategories->meta_tag_title = $CategoriesLanguages->meta_tag_title;
            $DtoCategories->meta_tag_description = $CategoriesLanguages->meta_tag_description;
            $DtoCategories->meta_tag_keyword = $CategoriesLanguages->keyword;
            $DtoCategories->category_type_id = $Categories->category_type_id;
            $DtoCategories->characteristic = $CategoriesLanguages->meta_tag_characteristic;
            $DtoCategories->visible_from = $Categories->visible_from;
            $DtoCategories->visible_to = $Categories->visible_to;


            $result->push($DtoCategories);
            
            $result = static::_getSubAllTableCategoriesFather($categoryAll->category_id, $result, 0);

        }
        
        return $result;
    }

    /**
     * Get All List Navigation
     * 
     * @return DtoCategories
     */
    public static function getAllListNavigation($idLanguage, $idItem)
    {
        $result = collect();

        foreach (CategoryFather::whereNull('category_father_id')->orderBy('order')->get() as $categoryAll) {

            $DtoCategories = new DtoCategories();
            $Categories = Category::where('id', $categoryAll->category_id)->first();
            $CategoriesLanguages = CategoryLanguage::where('category_id', $categoryAll->category_id)->first();

            $DtoCategories->id = $categoryAll->category_id;
            $DtoCategories->order = $Categories->order;
            $DtoCategories->description = $CategoriesLanguages->description;

            $itemCategory = CategoryItem::where('item_id', $idItem)->where('category_id', $categoryAll->category_id)->get()->first();

            $DtoCategories->checked = false;
            if (isset($itemCategory)) {
                $DtoCategories->checked = true;
            }

            $DtoCategories->categoriesList = static::_getSubAllListCategories($categoryAll->category_id, $idItem);
            $result->push($DtoCategories);
        }
        return $result;
    }

    /**
     * Get All List Navigation
     * 
     * @return DtoCategories
     */
    public static function getAllListNavigationFather($idLanguage, $idItem)
    {
        $result = collect();

        foreach (CategoryFather::whereNull('category_father_id')->orderBy('order')->get() as $categoryAll) {

            $DtoCategories = new DtoCategories();
            $Categories = Category::where('id', $categoryAll->category_id)->first();
            $CategoriesLanguages = CategoryLanguage::where('category_id', $categoryAll->category_id)->first();

            $DtoCategories->id = $categoryAll->category_id;
            $DtoCategories->category_father_id = $categoryAll->id;

            if(is_null($categoryAll->order)){
                $DtoCategories->order = 0;
            }else{
                $DtoCategories->order = $categoryAll->order;
            }

            if(is_null($Categories->online)){
                $DtoCategories->online = false;
            }else{
                $DtoCategories->online = $Categories->online;
            }

            $DtoCategories->code = $Categories->code;

            $DtoCategories->description = $CategoriesLanguages->description;

            if($idItem!=$categoryAll->category_id){
                $itemCategory = CategoryFather::where('category_id', $idItem)->where('category_father_id', $categoryAll->category_id)->get()->first();

                $DtoCategories->checked = false;
                if (isset($itemCategory)) {
                    $DtoCategories->checked = true;
                }
            }else{
                $DtoCategories->checked = true;
            }

            $DtoCategories->itemsList = static::_getListItemCategoriesId($categoryAll->category_id);
            $DtoCategories->categoriesList = static::_getSubAllListCategoriesFather($categoryAll->category_id, $idItem);
            $result->push($DtoCategories);
        }
        return $result;
    }

    /**
     * get All Detail List Categories
     * 
     * @return DtoCategories
     */
    public static function getAllDetailListCategories($idLanguage, $idItem)
    {
        $result = collect();

        foreach (CategoryItem::where('item_id', $idItem)->orderBy('created_at', 'desc')->get() as $categoryItem) {
            $dtoDetailListCategories = new DtoCategoryItemDetail();

            $dtoDetailListCategories->id = $categoryItem->category_id;
            $dtoDetailListCategories->order = $categoryItem->order;

            $CategoriesLanguages = CategoryLanguage::where('category_id', $categoryItem->category_id)->first();
            $dtoDetailListCategories->descriptionCategory = $CategoriesLanguages->description;

            $ItemLanguages = ItemLanguage::where('item_id', $idItem)->first();
            $dtoDetailListCategories->descriptionItem = $ItemLanguages->description;

            if (isset($categoryItem->order)) {
                $itemPrec = CategoryItem::where('category_id', $categoryItem->category_id)->where('order', '<', $categoryItem->order)->orderBy('order')->get()->first();
                if (isset($itemPrec)) {
                    $dtoDetailListCategories->descriptionPrecItem = ItemLanguage::where('item_id', $itemPrec->item_id)->first()->description;
                    $dtoDetailListCategories->orderPrecItem = $itemPrec->order;
                } else {
                    $dtoDetailListCategories->descriptionPrecItem = ' - Inizio lista - ';
                    $dtoDetailListCategories->orderPrecItem = $categoryItem->order;
                }
            } else {
                $dtoDetailListCategories->descriptionPrecItem = ' - Nessun ordinamento - ';
                $dtoDetailListCategories->orderPrecItem = '';
            }

            if (isset($categoryItem->order)) {
                $itemPost = CategoryItem::where('category_id', $categoryItem->category_id)->where('order', '>', $categoryItem->order)->orderBy('order')->get()->first();
                if (isset($itemPost)) {
                    $dtoDetailListCategories->descriptionPostItem = ItemLanguage::where('item_id', $itemPost->item_id)->first()->description;
                    $dtoDetailListCategories->orderPostItem = $itemPost->order;
                } else {
                    $dtoDetailListCategories->descriptionPostItem = ' - Fine lista - ';
                    $dtoDetailListCategories->orderPostItem = $categoryItem->order;
                }
            } else {
                $dtoDetailListCategories->descriptionPostItem = ' - Nessun ordinamento - ';
                $dtoDetailListCategories->orderPostItem = '';
            }

            $result->push($dtoDetailListCategories);
        }

        return $result;
    }

    /**
     * get All Detail List Categories
     * 
     * @return DtoCategories
     */
    public static function getAllDetailListCategoriesFather($idLanguage, $idCategories)
    {
        $result = collect();

        foreach (CategoryFather::where('category_id', $idCategories)->orderBy('created_at', 'desc')->get() as $categoryFather) {
            $dtoDetailListCategories = new DtoCategoryItemDetail();

            $dtoDetailListCategories->id = $categoryFather->category_father_id;
            $dtoDetailListCategories->order = $categoryFather->order;

            $CategoriesLanguages = CategoryLanguage::where('category_id', $categoryFather->category_father_id)->first();
            if(isset($CategoriesLanguages)){
                $dtoDetailListCategories->descriptionCategory = $CategoriesLanguages->description;
            }else{
                $dtoDetailListCategories->descriptionCategory = 'Categoria Principale';
            }

            $ItemLanguages = CategoryLanguage::where('category_id', $categoryFather->category_id)->first();
            $dtoDetailListCategories->descriptionItem = $ItemLanguages->description;

            if (isset($categoryFather->order)) {
                $itemPrec = CategoryFather::where('category_father_id', $categoryFather->category_father_id)->where('order', '<', $categoryFather->order)->orderBy('order')->get()->first();
                if (isset($itemPrec)) {
                    $dtoDetailListCategories->descriptionPrecItem = CategoryLanguage::where('category_id', $itemPrec->category_id)->first()->description;
                    $dtoDetailListCategories->orderPrecItem = $itemPrec->order;
                } else {
                    $dtoDetailListCategories->descriptionPrecItem = ' - Inizio lista - ';
                    $dtoDetailListCategories->orderPrecItem = $categoryFather->order;
                }
            } else {
                $dtoDetailListCategories->descriptionPrecItem = ' - Nessun ordinamento - ';
                $dtoDetailListCategories->orderPrecItem = '';
            }

            if (isset($categoryFather->order)) {
                $itemPost = CategoryFather::where('category_father_id', $categoryFather->category_father_id)->where('order', '>', $categoryFather->order)->orderBy('order')->get()->first();
                if (isset($itemPost)) {
                    $dtoDetailListCategories->descriptionPostItem = CategoryLanguage::where('category_id', $itemPost->category_id)->first()->description;
                    $dtoDetailListCategories->orderPostItem = $itemPost->order;
                } else {
                    $dtoDetailListCategories->descriptionPostItem = ' - Fine lista - ';
                    $dtoDetailListCategories->orderPostItem = $categoryFather->order;
                }
            } else {
                $dtoDetailListCategories->descriptionPostItem = ' - Nessun ordinamento - ';
                $dtoDetailListCategories->orderPostItem = '';
            }

            $result->push($dtoDetailListCategories);
        }

        return $result;
    }


    /**
     * Get all
     * 
     * @return DtoCategories
     */
    public static function getList($request)
    {

        $url = $request->url;
        $dtoCategoriesList = new DtoCategoriesList();

        $selenaSqlView = SelenaSqlViews::where('name_view', 'View_Categorie')->first();
        if (isset($selenaSqlView)) {
            $strQuery = "";
            if (isset($selenaSqlView->select)) {
                $strQuery = $strQuery . 'SELECT ' . $selenaSqlView->select;
                if (isset($selenaSqlView->from)) {
                    $strQuery = $strQuery . ' FROM ' . $selenaSqlView->from;

                    if (isset($selenaSqlView->where)) {
                        $strQuery = $strQuery . ' WHERE ' . $selenaSqlView->where;
                    }

                    if (isset($selenaSqlView->order_by)) {
                        $strQuery = $strQuery . ' ORDER BY ' . $selenaSqlView->order_by;
                    }

                    if (isset($selenaSqlView->group_by)) {
                        $strQuery = $strQuery . ' GROUP BY ' . $selenaSqlView->group_by;
                    }
                }
            }
        }

        $rows = DB::select($strQuery);

        foreach (ArrayHelper::toCollection($rows)->whereIn('category_father_id', '')->sortBy('order') as $row) {
            $dtoCategories = new DtoCategories();
            $dtoCategories->id = $row->id;
            $dtoCategories->description = $row->description;
            $dtoCategories->code = $row->code;
            $dtoCategories->link = $row->meta_tag_link;
            if ($url == $row->meta_tag_link) {
                $dtoCategories->active = true;
            } else {
                $dtoCategories->active = false;
            }
            $dtoCategories->categoriesList = static::_getSubCategories($rows, $row->id, $url);
            $dtoCategoriesList->categoriesList->push($dtoCategories);
        }

        return $dtoCategoriesList;
    }

    /**
     * Get all
     * 
     * @return DtoCategories
     */
    public static function getHtml($url, $idLanguage)
    {

        //
        $content = CategoryLanguage::where('meta_tag_link', $url)->join('categories', 'categories.id', '=', 'categories_languages.category_id')->where('categories.online', true)->where('categories_languages.language_id', $idLanguage)->first();
        if (isset($content)) {
            $idContent = Content::where('code', 'PageCategories')->first();
            $htmlContent = ContentLanguage::where('content_id', $idContent->id)->where('language_id', $idLanguage)->first();
            $dtoPageCategoryRender = new DtoCategories();

            if (strpos($htmlContent->content, '#titlecategories#') !== false) {
                $contentHtml = str_replace('#titlecategories#', $content->description,  $htmlContent->content);
            } else {
                $contentHtml = $htmlContent->content;
            }

            if (strpos($contentHtml, '#imgbannercategories#') !== false) {
                if(isset($content->image_banner)){
                    $contentHtml = str_replace('#imgbannercategories#', $content->image_banner,  $contentHtml);
                }
            } else {
                $contentHtml = $contentHtml;
            }

            if (strpos($contentHtml, '#meta_tag_characteristic#') !== false) {
                $contentHtml = str_replace('#meta_tag_characteristic#', $content->meta_tag_characteristic,  $contentHtml);
            }

            $dtoPageCategoryRender->html = $contentHtml;
            $dtoPageCategoryRender->description = $content->description;
            $dtoPageCategoryRender->meta_tag_description = $content->meta_tag_description;
            $dtoPageCategoryRender->meta_tag_keyword = $content->keyword;
            $dtoPageCategoryRender->meta_tag_title = $content->meta_tag_title;

            return $dtoPageCategoryRender;
        } else {
            return '';
        }
    }


    /**
     * Get Content for link
     * 
     * @return DtoItems
     */
    public static function getImages($id)
    {
        $categories = Category::find($id);
        if(!is_null($id)){
            if (is_null($id)) {
                throw new Exception(DictionaryBL::getTranslate(1, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
            
            if (is_null($categories)) {
                throw new Exception(DictionaryBL::getTranslate(1, DictionariesCodesEnum::ItemNotFound), HttpResultsCodesEnum::InvalidPayload);
            }

            $listImages = new DtoItemImages();

            $listImages->id = $id;
            $listImages->itemId = $id;
            $listImages->img = $categories->img;
            $listImages->imgBanner = $categories->image_banner;

            if ($categories->img != '') {
                $path_parts = pathinfo($categories->img);

                $listImages->imgName = $path_parts['basename'];

                $ch = curl_init($categories->img);
                curl_setopt($ch, CURLOPT_NOBODY, true);
                curl_exec($ch);
                $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                
                if ($code == 200) {
                    $size = getimagesize($categories->img);
                    $listImages->width = $size[0];
                    $listImages->height = $size[1];
                } else {
                    $listImages->width = 0;
                    $listImages->height = 0;
                }

                $weight = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
                curl_close($ch);
                $dimension = static::_convertImageSize($weight);
                $listImages->weight = $dimension;
            } else {
                $listImages->imgName = 'Nessuna immagine presente';
                $listImages->width = 0;
                $listImages->height = 0;
                $listImages->weight = 0;
            }

            if ($categories->image_banner != '') {
                $path_parts = pathinfo($categories->image_banner);

                $listImages->imgNameBanner = $path_parts['basename'];

                $ch = curl_init($categories->image_banner);
                curl_setopt($ch, CURLOPT_NOBODY, true);
                curl_exec($ch);
                $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                if ($code == 200) {
                    $size = getimagesize($categories->image_banner);
                    $listImages->widthBanner = $size[0];
                    $listImages->heightBanner = $size[1];
                } else {
                    $listImages->widthBanner = 0;
                    $listImages->heightBanner = 0;
                }

                $weight = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
                curl_close($ch);
                $dimension = static::_convertImageSize($weight);
                $listImages->weightBanner = $dimension;
            } else {
                $listImages->imgNameBanner = 'Nessuna immagine presente';
                $listImages->widthBanner = 0;
                $listImages->heightBanner = 0;
                $listImages->weightBanner = 0;
            }


            return $listImages;
        }else{
            return '';
        }
    }

    /**
     * Get all dto
     *
     * @param int id
     *
     * @return DtoItem
     */
    public static function getAllImagesInFolder($id)
    {
        $result = collect();
        $dir = static::$dirImage;
        $cont = 0;
        foreach (\File::files($dir) as $f) {

            $cont = $cont + 1;

            if (ends_with($f, ['.png', '.jpg', '.jpeg', '.gif'])) {
                $listImages = new DtoItemImages();
                $listImages->id = $cont;
                $listImages->idImgAgg = $id;
                $listImages->img = static::getUrl() . $f->getRelativePathname();
                $listImages->imgName = $f->getRelativePathname();
                $result->push($listImages);
            }
        }

        return $result;
    }

    /**
     * Get by categories Languages
     * 
     * @param int id
     * @return DtoItems
     */
    public static function getCategoriesLanguages(int $id)
    {
        $result = collect();
        $language_id = 1;

        $DtoItemLanguagesGen = new DtoItemLanguages();
        foreach (CategoryLanguage::where('category_id', $id)->where('language_id', '<>', $language_id)->get() as $languageItem) {
            $DtoItemLanguages = new DtoItemLanguages();
            $DtoItemLanguages->id = $languageItem->id;
            $DtoItemLanguages->idItem = $languageItem->item_id;
            $DtoItemLanguages->language_id = Language::where('id', $languageItem->language_id)->first()->description;
            $DtoItemLanguages->description = $languageItem->description;
            $DtoItemLanguages->meta_tag_characteristic = $languageItem->meta_tag_characteristic;
            $DtoItemLanguagesGen->selectItemExist->push($DtoItemLanguages);
        }
        $DtoItemLanguagesGen->totalLn = DB::table('languages')->count();
        $DtoItemLanguagesGen->totalLnCategoriesPresent = DB::table('categories_languages')->where('category_id', $id)->count();
        return $DtoItemLanguagesGen;
    }

    /**
     * getByTranslate
     * 
     * @return DtoUser
     */
    public static function getByTranslateDefault($id, $idItem, $idLanguage)
    {

        $result = collect();
        $itemDefaultLanguage = CategoryLanguage::where('category_id', $idItem)->where('language_id', '=', 1)->first();

        $DtoItemLanguage = new DtoItemLanguages();
        $DtoItemLanguage->id = $id;
        $DtoItemLanguage->idItem = $idItem;
        $DtoItemLanguage->language_id = $idLanguage;
        $DtoItemLanguage->link = $itemDefaultLanguage->meta_tag_link;
        $DtoItemLanguage->description = $itemDefaultLanguage->description;
        $DtoItemLanguage->meta_tag_characteristic = $itemDefaultLanguage->meta_tag_characteristic;
        $result->push($DtoItemLanguage);
        return $result;
    }

    /**
     * getByTranslate
     * 
     * @return DtoUser
     */
    public static function getByTranslateDefaultNew($idItem, $idLanguage)
    {

        $result = collect();
        $itemDefaultLanguage = CategoryLanguage::where('category_id', $idItem)->where('language_id', '=', 1)->first();

        $DtoItemLanguage = new DtoItemLanguages();
        $DtoItemLanguage->id = '';
        $DtoItemLanguage->idItem = $idItem;
        $DtoItemLanguage->language_id = $idLanguage;
        $DtoItemLanguage->link = $itemDefaultLanguage->meta_tag_link;
        $DtoItemLanguage->description = $itemDefaultLanguage->description;
        $DtoItemLanguage->meta_tag_characteristic = $itemDefaultLanguage->meta_tag_characteristic;
        $result->push($DtoItemLanguage);
        return $result;
    }

    /**
     * getByTranslate
     * 
     * @return DtoUser
     */
    public static function getByTranslate($id)
    {
        $result = collect();
        $itemLanguage = CategoryLanguage::where('id', $id)->first();

        $DtoItemLanguage = new DtoItemLanguages();
        $DtoItemLanguage->id = $id;
        $DtoItemLanguage->language_id = $itemLanguage->language_id;
        $DtoItemLanguage->idItem = $itemLanguage->item_id;
        $DtoItemLanguage->description = $itemLanguage->description;
        $DtoItemLanguage->meta_tag_characteristic = $itemLanguage->meta_tag_characteristic;
        $DtoItemLanguage->link = $itemLanguage->meta_tag_link;
        $result->push($DtoItemLanguage);
        return $result;
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoCategories
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoCategories, int $idUser)
    {
        if (strpos($DtoCategories->img, '/storage/app/public/images/Categories/') === false) {
            //Image::make($DtoCategories->img)->save(static::$dirImage . $DtoCategories->imgName);
        }

        DB::beginTransaction();

        try {

            $Categories = new Category();
            $Categories->order = $DtoCategories->order;
            $Categories->code = $DtoCategories->code;
            $Categories->img = static::getUrl() . $DtoCategories->imgName;
            $Categories->online = $DtoCategories->online;
            $Categories->category_type_id = $DtoCategories->category_type_id;

            if (isset($DtoCategories->visible_from)) {
                $Categories->visible_from = $DtoCategories->visible_from;
            }
                if (isset($DtoCategories->visible_to)) {
                $Categories->visible_to = $DtoCategories->visible_to;
            }
            if (isset($DtoCategories->category_father_id)) {
                $CategoryIdFather = CategoryLanguage::where('id', $DtoCategories->category_father_id)->first()->category_id;
                $Categories->category_father_id = $CategoryIdFather;
            }
            $Categories->created_id = $DtoCategories->$idUser;
            $Categories->save();

            $CategoriesLanguage = new CategoryLanguage();
            $CategoriesLanguage->language_id = $DtoCategories->idLanguage;
            $CategoriesLanguage->category_id = $Categories->id;
            $CategoriesLanguage->description = $DtoCategories->description;
            $CategoriesLanguage->meta_tag_characteristic = $DtoCategories->characteristic;
            $CategoriesLanguage->meta_tag_link = $DtoCategories->link;
            $CategoriesLanguage->meta_tag_description = $DtoCategories->meta_tag_description;
            $CategoriesLanguage->keyword = $DtoCategories->meta_tag_keyword;
            $CategoriesLanguage->meta_tag_title = $DtoCategories->meta_tag_title;

            $CategoriesLanguage->created_id = $DtoCategories->$idUser;
            $CategoriesLanguage->save();

            $CategoriesFather = new CategoryFather();
            $CategoriesFather->category_id = $Categories->id;
            if (isset($DtoCategories->category_father_id)) {
                $CategoryIdFather = CategoryLanguage::where('id', $DtoCategories->category_father_id)->first()->category_id;
                $CategoriesFather->category_father_id = $CategoryIdFather;
            }
            $CategoriesFather->order = $DtoCategories->order;
            $CategoriesFather->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $Categories->id;
    }


    /**
     * insert categories Father
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertCategoriesFather(Request $request, int $idUser, int $idLanguage)
    {
        if (is_null($request->idCategories)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }
        if($request->idCategoriesSelected != $request->idCategories){
            $categoriesFatherOnDb = CategoryFather::where('category_id', $request->idCategoriesSelected)->where('category_father_id', $request->idCategories)->get()->first();

            if(!isset($categoriesFatherOnDb)){
                $newCategoryFather = new CategoryFather();
                $newCategoryFather->category_id = $request->idCategoriesSelected;
                $newCategoryFather->category_father_id = $request->idCategories;
                $newCategoryFather->created_id = $idUser;
                $newCategoryFather->save();

                return $newCategoryFather->id;
            }else{
                return '';
            }
        }else{
            return '';
        }
    }


    /**
     * insert Category Item Without Category
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertCategoryItemWithoutCategory(Request $request, int $idUser, int $idLanguage)
    {
        $msg = '';
        if (is_null($request->codeCategory)) {
            $msg = 'Codice categoria inesistente';
        }else{
            $categoryOnDb = Category::where('code', $request->codeCategory)->get()->first();
            if (!isset($categoryOnDb)) {
                $msg = 'Codice categoria inesistente';
            }else{
                DB::beginTransaction();

                try {
                    foreach($request->items as $items){
                        $categoryItemNew = new CategoryItem();
                        $categoryItemNew->item_id = $items;
                        $categoryItemNew->category_id = $categoryOnDb->id;
                        $categoryItemNew->save();
                    }

                    $msg = '';
                    
                    DB::commit();
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
            }
        }

        return $msg;

    }

    public static function insertTranslate(Request $request)
    {
        $newlanguageItem = new CategoryLanguage();
        $newlanguageItem->category_id = $request->idCategories;
        $newlanguageItem->description = $request->description;
        $newlanguageItem->meta_tag_characteristic = $request->meta_tag_characteristic;
        $newlanguageItem->meta_tag_link = $request->link;
        $newlanguageItem->language_id = $request->language_id;
        $newlanguageItem->save();
    }
    
    #endregion INSERT

    /**
     * Clone
     * 
     * @param int id
     * @param int idFunctionality
     * @param int idLanguage
     * @param int idUser
     * 
     * @return int
     */
    public static function clone(int $id, int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $Categories = Category::find($id);

        $CategoriesLanguages = CategoryLanguage::where('category_id', $id)->first();

        if (is_null($Categories)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        DB::beginTransaction();

        try {
            $newCategories = new Category();
            $newCategories->order = $Categories->order;
            $newCategories->code = $Categories->code;
            $newCategories->img = $Categories->img;
            $newCategories->online = $Categories->online;

            $newCategories->category_type_id = $Categories->category_type_id;
            $newCategories->category_father_id = $Categories->category_father_id;
            $newCategories->save();

            $newCategoriesLanguage = new CategoryLanguage();
            $newCategoriesLanguage->category_id = $newCategories->id;
            $newCategoriesLanguage->language_id = $CategoriesLanguages->language_id;
            $newCategoriesLanguage->description = $CategoriesLanguages->description . ' - Copy';
            $newCategoriesLanguage->meta_tag_description = $CategoriesLanguages->meta_tag_description;
            $newCategoriesLanguage->meta_tag_link = $CategoriesLanguages->meta_tag_link;
            $newCategoriesLanguage->keyword = $CategoriesLanguages->keyword;
            $newCategoriesLanguage->meta_tag_characteristic = $CategoriesLanguages->meta_tag_characteristic;
            $newCategoriesLanguage->meta_tag_title = $CategoriesLanguages->meta_tag_title;
            $newCategoriesLanguage->save();

            DB::commit();

            return $newCategories->id;
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoCategories
     * @param int idLanguage
     */

    public static function update(Request $DtoCategories)
    {
        static::_validateData($DtoCategories, $DtoCategories->idLanguage, DbOperationsTypesEnum::UPDATE);

        $categoriesOnDb = Category::find($DtoCategories->id);
        $languageCategoriesOnDb = CategoryLanguage::where('category_id', $DtoCategories->id, $DtoCategories->idLanguage)->first();

        DB::beginTransaction();

        try {

            $categoriesOnDb->order = $DtoCategories->order;
            $categoriesOnDb->code = $DtoCategories->code;
            $categoriesOnDb->online = $DtoCategories->online;
            $categoriesOnDb->category_type_id = $DtoCategories->category_type_id;
            $categoriesOnDb->visible_from = $DtoCategories->visible_from;
            $categoriesOnDb->visible_to = $DtoCategories->visible_to;

            // if (is_null($DtoCategories->imgName)) {
            //  Image::make($DtoCategories->img)->save(static::$dirImage . $DtoCategories->imgName);
            //  $categoriesOnDb->img = static::getUrl() . $DtoCategories->imgName;
            //   }

            if (!is_null($DtoCategories->imgName)) {
                Image::make($DtoCategories->img)->save(static::$dirImage . $DtoCategories->imgName);
                $categoriesOnDb->img = static::getUrl() . $DtoCategories->imgName;
            }

            if (isset($DtoCategories->category_father_id)) {
                $CategoryIdFather = CategoryLanguage::where('id', $DtoCategories->category_father_id)->first()->category_id;
                $categoriesOnDb->category_father_id = $CategoryIdFather;
            }
            $categoriesOnDb->save();

            $languageCategoriesOnDb->language_id = $DtoCategories->idLanguage;
            $languageCategoriesOnDb->description = $DtoCategories->description;
            $languageCategoriesOnDb->meta_tag_characteristic = $DtoCategories->characteristic;
            $languageCategoriesOnDb->meta_tag_link = $DtoCategories->link;
            $languageCategoriesOnDb->meta_tag_description = $DtoCategories->meta_tag_description;
            $languageCategoriesOnDb->keyword = $DtoCategories->meta_tag_keyword;
            $languageCategoriesOnDb->meta_tag_title = $DtoCategories->meta_tag_title;
            $languageCategoriesOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    public static function updateTranslate(Request $request)
    {
        $itemTranslationonDb = CategoryLanguage::where('id', $request->id)->first();
        DB::beginTransaction();

        try {
            if (isset($itemTranslationonDb)) {
                if (isset($request->idCategories)) {
                    $itemTranslationonDb->category_id = $request->idCategories;
                }
                if (isset($request->description)) {
                    $itemTranslationonDb->description = $request->description;
                }
                if (isset($request->meta_tag_characteristic)) {
                    $itemTranslationonDb->meta_tag_characteristic = $request->meta_tag_characteristic;
                }
                if (isset($request->link)) {
                    $itemTranslationonDb->meta_tag_link = $request->link;
                }
                if (isset($request->language_id)) {
                    $itemTranslationonDb->language_id = $request->language_id;
                }
                $itemTranslationonDb->save();
            }
            /*else {
                $itemTranslationonDb = new ItemLanguage();
                $itemTranslationonDb->description = $request->description;
                $itemTranslationonDb->item_id = $request->idItem;
                $itemTranslationonDb->language_id = $request->language_id;
                $itemTranslationonDb->meta_tag_characteristic = $request->meta_tag_characteristic;
                $itemTranslationonDb->link = $request->link;
                $itemTranslationonDb->save();
            }*/


            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return  $request->id;
    }

    #endregion UPDATE
    /**
     * Update
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateOrderCategoryItem(Request $request)
    {

        $categoriesItemOnDb = CategoryItem::where('item_id', $request->idItem)->where('category_id', $request->idCategories)->first();

        DB::beginTransaction();

        try {
            $categoriesItemOnDb->order = $request->order;
            $categoriesItemOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
    /**
     * Update
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateOrderCategoryFather(Request $request)
    {
        $categoriesFatherOnDb = CategoryFather::where('category_id', $request->idCategories)->where('category_father_id', $request->idCategoriesFather)->first();

        DB::beginTransaction();

        try {
            $categoriesFatherOnDb->order = $request->order;
            $categoriesFatherOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    /**
     * Update
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateOrderCategoriesTable(Request $request)
    {
        $categoriesFatherOnDb = CategoryFather::where('id', $request->id)->first();

        DB::beginTransaction();

        try {
            $categoriesFatherOnDb->order = $request->order;
            $categoriesFatherOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    /**
     * update categories Item
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateOnlineCategories(Request $request)
    {
        $categoriesOnDb = Category::find($request->idCategories);

        if (isset($categoriesOnDb)) {
            if(is_null($categoriesOnDb->online)){
                $categoriesOnDb->online = true;
            }else{
                $categoriesOnDb->online = !$categoriesOnDb->online;
            }
            $categoriesOnDb->save();
        }
    }

    /**
     * update Image Name
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateImageName(Request $request)
    {
        $categoriesOnDb = Category::find($request->idItem);

        if (isset($categoriesOnDb)) {
            if ($request->imgBase64 == '') {
                if($request->principal){
                    $categoriesOnDb->img = static::getUrl() . $request->imgName;
                }else{
                    $categoriesOnDb->image_banner = static::getUrl() . $request->imgName;
                }
                $categoriesOnDb->save();
            } else {
                Image::make($request->imgBase64)->save(static::$dirImage . $request->imgName);
                if($request->principal){
                    $categoriesOnDb->img = static::getUrl() . $request->imgName;
                }else{
                    $categoriesOnDb->image_banner = static::getUrl() . $request->imgName;
                }
                $categoriesOnDb->save();
            }
        }
    }

    /**
     * update Image Name
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateCategoriesImages(Request $request)
    {
        $itemsPhotosOnDb = Category::find($request->idItemPhotos);

        if (isset($itemsPhotosOnDb)) {
            $itemsPhotosOnDb->img = static::getUrl() . $request->imgName;
            $itemsPhotosOnDb->save();
        }
    }

    #endregion UPDATE

    #region DELETE
    /**
     * Delete
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $Categories = Category::find($id);

        if (is_null($Categories)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($Categories)) {

            DB::beginTransaction();

            try {
                CategoryItem::where('category_id', $id)->delete();
                CategoryFather::where('category_id', $id)->delete();
                CategoriesLanguagesBL::deleteByIdCategories($id);
                Category::where('id', $id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }

    /**
     * delete Categories Father
     *
     * @param int $idCategories
     * @param int $idCategoriesFather
     * @param int $idLanguage
     */
    public static function deleteCategoriesFather(int $idCategories, int $idCategoriesFather, int $idLanguage)
    {
        if (is_null($idCategories)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }
        $CategoryFather = CategoryFather::where('category_id', $idCategoriesFather)->where('category_father_id', $idCategories);
        $CategoryFather->delete();
    }

    /**
     * delete getCancelTranslate
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function getCancelTranslate(int $id, int $idLanguage)
    {
        $itemLanguage = CategoryLanguage::find($id);


        if (is_null($itemLanguage)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($itemLanguage)) {

            DB::beginTransaction();

            try {

                CategoryLanguage::where('id', $id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }

    #endregion DELETE
}

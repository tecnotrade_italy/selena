<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoSalesOrderStateLanguage;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\LogHelper;
use App\DocumentStatusLanguage;
use App\DocumentStatus;
use App\DtoModel\DtoDocumentStatusLanguage;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DocumentStatusesBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request DtoSalesOrderStateLanguage
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, $dbOperationType)
    {

        if (is_null($request->code)) {
            throw new Exception("Codice obbligatorio");
        }

        if (is_null($request->description)) {
            throw new Exception("Descrizione obbligatoria");
        }
    }


    /**
     * Convert to dto
     * 
     * @param DocumentStatusLanguage
     * 
     * @return DtoDocumentStatusLanguage
     */
    private static function _convertToDto($SalesOrderStateLanguage)
    {
        $DtoDocumentStatusLanguage = new DtoDocumentStatusLanguage();

        if (isset($SalesOrderStateLanguage->id)) {
            $DtoSalesOrderStateLanguage->id = $SalesOrderStateLanguage->id;
        }

        if (isset($SalesOrderStateLanguage->language_id)) {
            $DtoSalesOrderStateLanguage->language_id = $SalesOrderStateLanguage->language_id;
        }

        if (isset($SalesOrderStateLanguage->description)) {
            $DtoSalesOrderStateLanguage->description = $SalesOrderStateLanguage->description;
        }
        return $DtoSalesOrderStateLanguage;
    }


    #endregion PRIVATE

    #region GET
      /**
     * Get select
     *
     * @param String $search
     * @param int $idUser
     * 
     */
    public static function getSelect(string $search)
    {
                if ($search == "*") {
                    return DocumentStatus::select('id', 'code as description')->get();
                } else {
                    return DocumentStatus::where('code', 'ilike' , $search . '%')->select('id', 'code as description')->get();
                } 
    }

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoSalesOrderStateLanguage
     */
    public static function getById(int $id)
    {
        //return static::_convertToDto(DocumentStatusLanguage::find($id));

        $DocumentStatusLanguage = DocumentStatusLanguage::where('id', $id)->first();
        $DocumentStatus = DocumentStatus::where('id', $DocumentStatusLanguage->document_status_id)->first();

        $DtoDocumentStatusLanguage = new DtoDocumentStatusLanguage();

         $DtoDocumentStatusLanguage->language_id = $DocumentStatusLanguage->language_id;
         $DtoDocumentStatusLanguage->description = $DocumentStatusLanguage->description;
         $DtoDocumentStatusLanguage->document_status_id = $DocumentStatusLanguage->document_status_id;
         $DtoDocumentStatusLanguage->code = $DocumentStatus->document_status_id;
         $DtoDocumentStatusLanguage->html = $DocumentStatus->html;
         return  $DtoDocumentStatusLanguage;
      
    }


    /**
     * Get all
     * 
     * @return DtoSalesOrderStateLanguage
     */
    public static function getAll($idLanguage)
    {
      
        $result = collect();
        foreach (DocumentStatusLanguage::where('language_id',$idLanguage)->get() as $DocumentStatusLanguageAll) {

        $DocumentStatus =  DocumentStatus::where('id', $DocumentStatusLanguageAll->document_status_id)->first();

            $DtoDocumentStatusLanguage = new DtoSalesOrderStateLanguage();
            $DtoDocumentStatusLanguage->id = $DocumentStatusLanguageAll->id;
            $DtoDocumentStatusLanguage->description = $DocumentStatusLanguageAll->description;
            $DtoDocumentStatusLanguage->language_id = $idLanguage;
            $DtoDocumentStatusLanguage->code = $DocumentStatus->code;
            $DtoDocumentStatusLanguage->html = $DocumentStatus->html;

            $result->push($DtoDocumentStatusLanguage);
    
        }
        
        return $result;
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request)
    {
    static::_validateData($request, $request->idLanguage, DbOperationsTypesEnum::INSERT);

        static::_validateData($request, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();
        try {

            $DocumentStatus = new DocumentStatus();

        if (isset($request->id)) {
            $DocumentStatus->id = $request->id;
            }
        if (isset($request->code)) {
            $DocumentStatus->code = $request->code;
            }
        if (isset($request->html)) {
            $DocumentStatus->html = $request->html;
            }  
            $DocumentStatus->delivered_document = 'false';
            $DocumentStatus->enabled = 'false';
            $DocumentStatus->closede = 'false';
            $DocumentStatus->annulled = 'false';
            $DocumentStatus->save();

            $DocumentStatusLanguage = new DocumentStatusLanguage();

        if (isset($DocumentStatus->id)) {
            $DocumentStatusLanguage->document_status_id = $DocumentStatus->id;
        }

        if (isset($request->description)) {
            $DocumentStatusLanguage->description = $request->description;
        }

        if (isset($request->idLanguage)) {
            $DocumentStatusLanguage->language_id = $request->idLanguage;
        }


            $DocumentStatusLanguage->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $DocumentStatusLanguage->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request request
     * @param int idLanguage
     */
    public static function update(Request $request)
    {
        static::_validateData($request, $request->idLanguage, DbOperationsTypesEnum::UPDATE);
        $DocumentStatusLanguage = DocumentStatusLanguage::find($request->id);
        $DocumentStatus = DocumentStatus::where('id', $DocumentStatusLanguage->document_status_id)->first();
       
        DB::beginTransaction();
        try {

         //   if (isset($request->id)) {
           //     $SalesOrderStateLanguages->id = $request->id;
            //}

            if (isset($request->description)) {
                $DocumentStatusLanguage->description = $request->description;
            }
            if (isset($request->idLanguage)) {
                $DocumentStatusLanguage->language_id = $request->idLanguage;
            }
            $DocumentStatusLanguage->save();

             if (isset($request->code)) {
                $DocumentStatus->code = $request->code;
            }
            if (isset($request->html)) {
                $DocumentStatus->html = $request->html;
            }

            $DocumentStatus->delivered_document = 'false';
            $DocumentStatus->enabled = 'false';
            $DocumentStatus->closede = 'false';
            $DocumentStatus->annulled = 'false';

            $DocumentStatus->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return  $request->id;
    }


    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id)
    {
        $DocumentStatusLanguage = DocumentStatusLanguage::find($id);
        $DocumentStatus = DocumentStatus::where('id', $DocumentStatusLanguage->document_status_id)->first();
     
        if (is_null($DocumentStatusLanguage)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $DocumentStatusLanguage->delete();
        $DocumentStatus->delete();

    }

    #endregion DELETE
}

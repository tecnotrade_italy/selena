<?php

namespace App\BusinessLogic;

use App\Enums\DictionariesCodesEnum;
use App\Functionality;
use Mockery\CountValidator\Exception;
use App\GroupDisplayTechnicalSpecificationLanguage;
use App\DtoModel\DtoGroupDisplayTechnicalSpecificationLanguage;
use App\Enums\HttpResultsCodesEnum;
use App\Language;

class GroupDisplayTechnicalSpecificationLanguageBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param int $idGroupDisplayTechnicalSpecification
     * @param Request $dtoGroupDisplayTechnicalSpecificationLanguage
     * @param int $idLanguage
     */
    private static function _validateData(int $idGroupDisplayTechnicalSpecification, $dtoGroupDisplayTechnicalSpecificationLanguage, int $idLanguage)
    {
        if (is_null($dtoGroupDisplayTechnicalSpecificationLanguage['idLanguage'])) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::LanguageNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Language::find($dtoGroupDisplayTechnicalSpecificationLanguage['idLanguage']))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::LanguageNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (!is_null($dtoGroupDisplayTechnicalSpecificationLanguage['description']) && static::checkExists($dtoGroupDisplayTechnicalSpecificationLanguage['description'], $dtoGroupDisplayTechnicalSpecificationLanguage['idLanguage'], $idGroupDisplayTechnicalSpecification)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupTechnicalSpecificationAlreadyExists), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($dtoGroupDisplayTechnicalSpecificationLanguage['description']) && $dtoGroupDisplayTechnicalSpecificationLanguage['idLanguage'] == LanguageBL::getDefault()->id) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DefaultLanguageRequired), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by group display technical specification and language
     * 
     * @param int idGroupDisplayTechnicalSpecification
     * @param int idLanguage
     * 
     * @return string
     */
    public static function getByGroupDisplayTechnicalSpecificationAndLanguage(int $idGroupDisplayTechnicalSpecification, int $idLanguage)
    {
        $groupDispalyTechnicalSpecificationLanguage = GroupDisplayTechnicalSpecificationLanguage::where('group_display_technical_specification_id', $idGroupDisplayTechnicalSpecification)->where('language_id', $idLanguage)->first();

        if (is_null($groupDispalyTechnicalSpecificationLanguage)) {
            $groupDispalyTechnicalSpecificationLanguage = GroupDisplayTechnicalSpecificationLanguage::where('group_display_technical_specification_id', $idGroupDisplayTechnicalSpecification)->where('language_id', LanguageBL::getDefault()->id)->first();
        }

        if (is_null($groupDispalyTechnicalSpecificationLanguage)) {
            return '';
        } else {
            return $groupDispalyTechnicalSpecificationLanguage->description;
        }
    }

    /**
     * Get dtoGroupDisplayTechnicalSpecificationLanguage
     * 
     * @param int $idGroupDisplayTechnicalSpecification
     * 
     * @return DtoGroupDisplayTechnicalSpecificationLanguage
     */
    public static function getDtoTable(int $idGroupDisplayTechnicalSpecification)
    {
        $dtoTable = collect();

        foreach (GroupDisplayTechnicalSpecificationLanguage::where('group_display_technical_specification_id', $idGroupDisplayTechnicalSpecification)->get() as $groupDisplayTechnicalSpecificationLanguage) {
            $dtoTable[$groupDisplayTechnicalSpecificationLanguage->language_id] = $groupDisplayTechnicalSpecificationLanguage->description;
        }

        return $dtoTable;
    }

    /**
     * Check if exist
     * 
     * @param string description
     * @param int idLanguage
     * @param int idGroupDisplayTechnicalSpecification
     * 
     * @return bool
     */
    public static function checkExists(string $description, int $idLanguage, int $idGroupDisplayTechnicalSpecification = null)
    {
        $found = false;

        if (is_null($idGroupDisplayTechnicalSpecification)) {
            if (GroupDisplayTechnicalSpecificationLanguage::where('language_id', $idLanguage)->where('description', $description)->count() > 0) {
                $found = true;
            }
        } else {
            if (GroupDisplayTechnicalSpecificationLanguage::where('group_display_technical_specification_id', '!=', $idGroupDisplayTechnicalSpecification)->where('language_id', $idLanguage)->where('description', $description)->count() > 0) {
                $found = true;
            }
        }

        return $found;
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     * 
     * @param int $idGroupDisplayTechnicalSpecification
     * @param Request $dtoGroupDisplayTechnicalSpecificationLanguage
     * @param int $idLanguage
     * 
     * @return id
     */
    public static function insertRequest(int $idGroupDisplayTechnicalSpecification, $dtoGroupDisplayTechnicalSpecificationLanguage, int $idLanguage)
    {
        static::_validateData($idGroupDisplayTechnicalSpecification, $dtoGroupDisplayTechnicalSpecificationLanguage, $idLanguage);

        $groupDisplayTechnicalSpecificationLanguage = new GroupDisplayTechnicalSpecificationLanguage();
        $groupDisplayTechnicalSpecificationLanguage->language_id = $dtoGroupDisplayTechnicalSpecificationLanguage['idLanguage'];
        $groupDisplayTechnicalSpecificationLanguage->group_display_technical_specification_id = $idGroupDisplayTechnicalSpecification;
        $groupDisplayTechnicalSpecificationLanguage->description = $dtoGroupDisplayTechnicalSpecificationLanguage['description'];

        $groupDisplayTechnicalSpecificationLanguage->save();

        $groupDisplayTechnicalSpecificationLanguage->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update request
     * 
     * @param int $idGroupDisplayTechnicalSpecification
     * @param Request $dtoGroupDisplayTechnicalSpecificationLanguage
     * @param int $idLanguage
     */
    public static function updateRequest($idGroupDisplayTechnicalSpecification, $dtoGroupDisplayTechnicalSpecificationLanguage, int $idLanguage)
    {
        static::_validateData($idGroupDisplayTechnicalSpecification, $dtoGroupDisplayTechnicalSpecificationLanguage, $idLanguage);

        $groupDisplayTechnicalSpecificationLanguage = GroupDisplayTechnicalSpecificationLanguage::where('group_display_technical_specification_id', $idGroupDisplayTechnicalSpecification)->where('language_id', $dtoGroupDisplayTechnicalSpecificationLanguage['idLanguage'])->first();

        if (!is_null($groupDisplayTechnicalSpecificationLanguage) && is_null($dtoGroupDisplayTechnicalSpecificationLanguage['description'])) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupTechnicalSpecificationNotEmpty), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (!is_null($dtoGroupDisplayTechnicalSpecificationLanguage['description'])) {
                if (!is_null($groupDisplayTechnicalSpecificationLanguage)) {
                    if ($groupDisplayTechnicalSpecificationLanguage->description != $dtoGroupDisplayTechnicalSpecificationLanguage['description']) {
                        $groupDisplayTechnicalSpecificationLanguage->description = $dtoGroupDisplayTechnicalSpecificationLanguage['description'];
                    }
                } else {
                    $groupDisplayTechnicalSpecificationLanguage = new GroupDisplayTechnicalSpecificationLanguage();
                    $groupDisplayTechnicalSpecificationLanguage->group_display_technical_specification_id = $idGroupDisplayTechnicalSpecification;
                    $groupDisplayTechnicalSpecificationLanguage->language_id = $dtoGroupDisplayTechnicalSpecificationLanguage['idLanguage'];
                    $groupDisplayTechnicalSpecificationLanguage->description = $dtoGroupDisplayTechnicalSpecificationLanguage['description'];
                }

                $groupDisplayTechnicalSpecificationLanguage->save();
            }
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete by group display technical specification
     * 
     * @param int $idGroupDisplayTechnicalSpecification
     */
    public static function deleteByGroupDisplayTechnicalSpecification(int $idGroupDisplayTechnicalSpecification)
    {
        GroupDisplayTechnicalSpecificationLanguage::where('group_display_technical_specification_id', $idGroupDisplayTechnicalSpecification)->delete();
    }

    #endregion DELETE
}

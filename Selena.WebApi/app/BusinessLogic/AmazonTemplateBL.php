<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoAmazonTemplate;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\AmazonTemplate;
use Exception;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;

class AmazonTemplateBL
{
    #region PRIVATE

    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request dtoAmazonTemplate
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, $dbOperationsTypesEnum)
    {


    }

    /**
     * Convert to dto
     * 
     * @param AmazonTemplate AmazonTemplate
     * @return DtoAmazonTemplate
     */
    private static function _convertToDto($AmazonTemplate)
    {
        $dtoAmazonTemplate = new DtoAmazonTemplate();

        if (isset($AmazonTemplate->id)) {
            $dtoAmazonTemplate->id = $AmazonTemplate->id;
        }

        if (isset($AmazonTemplate->code)) {
            $dtoAmazonTemplate->code = $AmazonTemplate->code;
        }

        if (isset($AmazonTemplate->description)) {
            $dtoAmazonTemplate->description = $AmazonTemplate->description;
        }

        if (isset($AmazonTemplate->shortcode_example)) {
            $dtoAmazonTemplate->shortcode_example = $AmazonTemplate->shortcode_example;
        }

        if (isset($AmazonTemplate->content)) {
            $dtoAmazonTemplate->content = $AmazonTemplate->content;
        }

        return $dtoAmazonTemplate;
    }


    /**
     * Convert to VatType
     * 
     * @param dtoAmazonTemplate dtoAmazonTemplate
     * 
     * @return AmazonTemplate
     */
    private static function _convertToModel($dtoAmazonTemplate)
    {
        $AmazonTemplate = new AmazonTemplate();

        if (isset($dtoAmazonTemplate->id)) {
            $AmazonTemplate->id = $dtoAmazonTemplate->id;
        }

        if (isset($dtoAmazonTemplate->code)) {
            $AmazonTemplate->code = $dtoAmazonTemplate->code;
        }

        if (isset($dtoAmazonTemplate->description)) {
            $AmazonTemplate->description = $dtoAmazonTemplate->description;
        }else
            $AdvertisingBanner->description = null;

        if (isset($dtoAmazonTemplate->shortcode_example)) {
            $AmazonTemplate->shortcode_example = $dtoAmazonTemplate->shortcode_example;
        }else
            $AdvertisingBanner->shortcode_example = null;

        if (isset($dtoAmazonTemplate->content)) {
            $AmazonTemplate->content = $dtoAmazonTemplate->content;
        }else
            $AdvertisingBanner->content = null;

        return $AmazonTemplate;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCommunityction
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(AmazonTemplate::find($id));
    }


    public static function getByCode(string $code)
    {
        if (is_null($code)) {
            LogHelper::error('Code of amazon template not found');
            return '';
        } else {
            $advertiseArea = AmazonTemplate::where('code', $code)->first();

            if (is_null($advertiseArea)) {
                LogHelper::error('Code of amazon template not found');
                return '';
            } else {
                return $advertiseArea;
            }
        }
    }

    /*
     * Get all
     * 
     * @return DtoAmazonTemplate
     */
    public static function getAll()
    {

        $result = collect(); 

        foreach (AmazonTemplate::orderBy('id','DESC')->get() as $amazonTemplate) {

            $DtoAmazonTemplate = new DtoAmazonTemplate();

            $DtoAmazonTemplate->id = $amazonTemplate->id;
            $DtoAmazonTemplate->code = $amazonTemplate->code;
            $DtoAmazonTemplate->description = $amazonTemplate->description;
            $DtoAmazonTemplate->shortcode_example = $amazonTemplate->shortcode_example;
            $DtoAmazonTemplate->content = $amazonTemplate->content;

            $result->push($DtoAmazonTemplate); 
        }
        return $result;

    }

    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return AmazonTemplate::select('id', 'description')->get();
        } else {
            return AmazonTemplate::where('description', 'like', $search . '%')->select('id', 'description')->get();
        }
    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoAmazonTemplate
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoAmazonTemplate)
    {
        
        static::_validateData($DtoAmazonTemplate, DbOperationsTypesEnum::INSERT);
        $AmazonTemplate = static::_convertToModel($DtoAmazonTemplate);

        $AmazonTemplate->save();

        return $AmazonTemplate->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoAmazonTemplate
     */
    public static function update(Request $DtoAmazonTemplate)
    {
        static::_validateData($DtoAmazonTemplate, DbOperationsTypesEnum::UPDATE);
        $AmazonTemplate = AmazonTemplate::find($DtoAmazonTemplate->id);
        DBHelper::updateAndSave(static::_convertToModel($DtoAmazonTemplate), $AmazonTemplate);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     */
    public static function delete(int $id)
    {
        $AmazonTemplate = AmazonTemplate::find($id);

        $AmazonTemplate->delete();
    }

    #endregion DELETE
}

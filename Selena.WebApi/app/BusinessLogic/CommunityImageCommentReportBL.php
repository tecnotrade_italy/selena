<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoCommunityImageCommentReport;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\CommunityImageCommentReport;
use Exception;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;

class CommunityImageCommentReportBL
{
    #region PRIVATE

    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request dtoCommunityImageCommentReport
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationsTypesEnum)
    {

       

    }

    /**
     * Convert to dto
     * 
     * @param CommunityImageCommentReport CommunityImageCommentReport
     * @return DtoCommunityImageCommentReport
     */
    private static function _convertToDto($CommunityImageCommentReport)
    {
        $dtoCommunityImageCommentReport = new DtoCommunityImageCommentReport();

        if (isset($CommunityImageCommentReport->id)) {
            $dtoCommunityImageCommentReport->id = $CommunityImageCommentReport->id;
        }

        if (isset($CommunityImageCommentReport->user_id)) {
            $dtoCommunityImageCommentReport->user_id = $CommunityImageCommentReport->user_id;
        }

        if (isset($CommunityImageCommentReport->comment_id)) {
            $dtoCommunityImageCommentReport->comment_id = $CommunityImageCommentReport->comment_id;
        }

        if (isset($CommunityImageCommentReport->report_message)) {
            $dtoCommunityImageCommentReport->report_message = $CommunityImageCommentReport->report_message;
        }

        return $dtoCommunityImageCommentReport;
    }


    /**
     * Convert to VatType
     * 
     * @param dtoCommunityImageCommentReport dtoCommunityImageCommentReport
     * 
     * @return CommunityImageCommentReport
     */
    private static function _convertToModel($dtoCommunityImageCommentReport)
    {
        $CommunityImageCommentReport = new CommunityImageCommentReport();

        if (isset($dtoCommunityImageCommentReport->id)) {
            $CommunityImageCommentReport->id = $dtoCommunityImageCommentReport->id;
        }
        
        if (isset($dtoCommunityImageCommentReport->user_id)) {
            $CommunityImageCommentReport->user_id = $dtoCommunityImageCommentReport->user_id;
        }

        if (isset($dtoCommunityImageCommentReport->comment_id)) {
            $CommunityImageCommentReport->comment_id = $dtoCommunityImageCommentReport->comment_id;
        }

        if (isset($dtoCommunityImageCommentReport->report_message)) {
            $CommunityImageCommentReport->report_message = $dtoCommunityImageCommentReport->report_message;
        }

        return $CommunityImageCommentReport;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCommunityction
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(CommunityImageCommentReport::find($id));
    }

    /*
     * Get all
     * 
     * @return DtoCommunityImageCommentReport
     */
    public static function getAll()
    {

        $result = collect(); 

        foreach (CommunityImageCommentReport::orderBy('id','DESC')->get() as $communityImageCommentReport) {

            $DtoCommunityImageCommentReport = new DtoCommunityImageCommentReport();

            $DtoCommunityImageCommentReport->id = $communityImageCommentReport->id;
            $DtoCommunityImageCommentReport->user_id = $communityImageCommentReport->user_id;
            $DtoCommunityImageCommentReport->comment_id = $communityImageCommentReport->comment_id;
            $DtoCommunityImageCommentReport->report_message = $communityImageCommentReport->report_message;
            
            $result->push($DtoCommunityImageCommentReport); 
        }
        return $result;

    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoCommunityImageCommentReport
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoCommunityImageCommentReport)
    {
        
        static::_validateData($DtoCommunityImageCommentReport, $idLanguage, DbOperationsTypesEnum::INSERT);
        $CommunityImageCommentReport = static::_convertToModel($DtoCommunityImageCommentReport);

        $CommunityImageCommentReport->save();

        return $CommunityImageCommentReport->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoCommunityImageCommentReport
     * @param int idLanguage
     */
    public static function update(Request $DtoCommunityImageCommentReport)
    {
        static::_validateData($DtoCommunityImageCommentReport, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $CommunityImageCommentReport = CommunityImageCommentReport::find($DtoCommunityImageCommentReport->id);
        DBHelper::updateAndSave(static::_convertToModel($DtoCommunityImageCommentReport), $CommunityImageCommentReport);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id)
    {
        $CommunityImageCommentReport = CommunityImageCommentReport::find($id);

        if (is_null($CommunityImageCommentReport)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $CommunityImageCommentReport->delete();
    }

    #endregion DELETE
}

<?php

namespace App\BusinessLogic;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoPaymentGr;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\PaymentGr;
use App\ItemGroupTechnicalSpecification;
use Intervention\Image\Facades\Image;
use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;


class PaymentGrBL
{
 #region GET

 /**
     * Get select
     *
     * @param String $search
     * @return PaymentGr
     */
    public static function selectpaymentGr(string $search)
    {
        if ($search == "*") {
            return PaymentGr::select('id', 'description as description')->get();
        } else {
            return PaymentGr::where('description', 'ilike', $search . '%')->select('id', 'description as description')->get();
        }
    }

 }
       
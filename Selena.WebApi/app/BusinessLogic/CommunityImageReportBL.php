<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoCommunityImageReport;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\CommunityImageReport;
use Exception;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;

class CommunityImageReportBL
{
    #region PRIVATE

    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request dtoCommunityImageReport
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationsTypesEnum)
    {

       

    }

    /**
     * Convert to dto
     * 
     * @param CommunityImageReport CommunityImageReport
     * @return DtoCommunityImageReport
     */
    private static function _convertToDto($CommunityImageReport)
    {
        $dtoCommunityImageReport = new DtoCommunityImageReport();

        if (isset($CommunityImageReport->id)) {
            $dtoCommunityImageReport->id = $CommunityImageReport->id;
        }

        if (isset($CommunityImageReport->image_report_type_id)) {
            $dtoCommunityImageReport->image_report_type_id = $CommunityImageReport->image_report_type_id;
        }

        if (isset($CommunityImageReport->user_id)) {
            $dtoCommunityImageReport->user_id = $CommunityImageReport->user_id;
        }

        if (isset($CommunityImageReport->image_id)) {
            $dtoCommunityImageReport->image_id = $CommunityImageReport->image_id;
        }

        if (isset($CommunityImageReport->report_message)) {
            $dtoCommunityImageReport->report_message = $CommunityImageReport->report_message;
        }

        return $dtoCommunityImageReport;
    }


    /**
     * Convert to VatType
     * 
     * @param dtoCommunityImageReport dtoCommunityImageReport
     * 
     * @return CommunityImageReport
     */
    private static function _convertToModel($dtoCommunityImageReport)
    {
        $CommunityImageReport = new CommunityImageReport();

        if (isset($dtoCommunityImageReport->id)) {
            $CommunityImageReport->id = $dtoCommunityImageReport->id;
        }

        if (isset($dtoCommunityImageReport->image_report_type_id)) {
            $CommunityImageReport->image_report_type_id = $dtoCommunityImageReport->image_report_type_id;
        }

        if (isset($dtoCommunityImageReport->user_id)) {
            $CommunityImageReport->user_id = $dtoCommunityImageReport->user_id;
        }

        if (isset($dtoCommunityImageReport->image_id)) {
            $CommunityImageReport->image_id = $dtoCommunityImageReport->image_id;
        }

        if (isset($dtoCommunityImageReport->report_message)) {
            $CommunityImageReport->report_message = $dtoCommunityImageReport->report_message;
        }

        return $CommunityImageReport;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCommunityction
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(CommunityImageReport::find($id));
    }

    /*
     * Get all
     * 
     * @return DtoCommunityImageReport
     */
    public static function getAll()
    {

        $result = collect(); 

        foreach (CommunityImageReport::orderBy('id','DESC')->get() as $communityImageReport) {

            $DtoCommunityImageReport = new DtoCommunityImageReport();

            $DtoCommunityImageReport->id = $communityImageReport->id;
            $DtoCommunityImageReport->image_report_type_id = $communityImageReport->image_report_type_id;
            $DtoCommunityImageReport->user_id = $communityImageReport->user_id;
            $DtoCommunityImageReport->image_id = $communityImageReport->image_id;
            $DtoCommunityImageReport->report_message = $communityImageReport->report_message;
            
            $result->push($DtoCommunityImageReport); 
        }
        return $result;

    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoCommunityImageReport
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoCommunityImageReport)
    {
        
        static::_validateData($DtoCommunityImageReport, $idLanguage, DbOperationsTypesEnum::INSERT);
        $CommunityImageReport = static::_convertToModel($DtoCommunityImageReport);

        $CommunityImageReport->save();

        return $CommunityImageReport->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoCommunityImageReport
     * @param int idLanguage
     */
    public static function update(Request $DtoCommunityImageReport)
    {
        static::_validateData($DtoCommunityImageReport, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $CommunityImageReport = CommunityImageReport::find($DtoCommunityImageReport->id);
        DBHelper::updateAndSave(static::_convertToModel($DtoCommunityImageReport), $CommunityImageReport);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id)
    {
        $CommunityImageReport = CommunityImageReport::find($id);

        if (is_null($CommunityImageReport)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $CommunityImageReport->delete();
    }

    #endregion DELETE
}

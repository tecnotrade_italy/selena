<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoTypeOptional;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\TypeOptional;
use App\TypeOptionalLanguages;
use App\ItemTypeOptional;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TypeOptionalBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (TypeOptional::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoTypeOptional
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (DB::table('type_optional')
                ->select('type_optional.*', 'type_optional_languages.description')
                ->join('type_optional_languages', 'type_optional.id', '=', 'type_optional_languages.type_optional_id')
                ->where('type_optional_languages.language_id',$idLanguage)
                ->orderBy('type_optional.order')
                ->get()
        as $typeOptionalAll) {
            
            $dtoTypeOptional = new DtoTypeOptional();
            $dtoTypeOptional->id = $typeOptionalAll->id;
            $dtoTypeOptional->order = $typeOptionalAll->order;
            $dtoTypeOptional->description = $typeOptionalAll->description;
            $dtoTypeOptional->required = $typeOptionalAll->required;
                        
            $result->push($dtoTypeOptional); 
        }
        return $result;
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * @param int idItem
     * 
     * @return DtoTypeOptional
     */
    public static function getAllTypeAndOptional($idLanguage, $idItem)
    {
        $result = collect();
        foreach (DB::table('type_optional')
                ->select('type_optional.*', 'type_optional_languages.description')
                ->join('type_optional_languages', 'type_optional.id', '=', 'type_optional_languages.type_optional_id')
                ->where('type_optional_languages.language_id', $idLanguage)
                ->orderBy('type_optional.order')
                ->get()
        as $typeOptionalAll) {
            
            $dtoTypeOptional = new DtoTypeOptional();
            $dtoTypeOptional->id = $typeOptionalAll->id;
            $dtoTypeOptional->order = $typeOptionalAll->order;
            $dtoTypeOptional->description = $typeOptionalAll->description;
            
            $itemTypeOptional = ItemTypeOptional::where('item_id', $idItem)->where('type_optional_id', $typeOptionalAll->id)->get()->first();

            if(isset($itemTypeOptional)){
                $dtoTypeOptional->required = true;
            }else{
                $dtoTypeOptional->required = false;
            }

            $dtoTypeOptional->optional->push(OptionalBL::getAllByType($typeOptionalAll->id, $idLanguage, $idItem));
                        
            $result->push($dtoTypeOptional); 
        }
        return $result;
    }


    /**
     * Get
     *
     * @param String $search
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return TypeOptionalLanguages::select('type_optional_id as id', 'description')->orderBy('description')->get();
        } else {
            return TypeOptionalLanguages::where('description', 'ilike','%'. $search . '%')->select('type_optional_id as id', 'description')->orderBy('description')->get();
        }
    }

    
    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $typeOptional = new TypeOptional();
            $typeOptional->order = $request->order;
            $typeOptional->required = $request->required;
            $typeOptional->created_id = $idUser;
            $typeOptional->save();
            
            $typeOptionalLanguages = new TypeOptionalLanguages();
            $typeOptionalLanguages->type_optional_id = $typeOptional->id;
            $typeOptionalLanguages->description = $request->description;
            $typeOptionalLanguages->language_id = $idLanguage;
            $typeOptionalLanguages->created_id = $idUser;
            $typeOptionalLanguages->save();

            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $typeOptional->id;
    }

    /**
     * insert Item Type Optional
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertItemTypeOptional(Request $request, int $idUser, int $idLanguage)
    {
        DB::beginTransaction();
        try {
            $itemOptional = new ItemTypeOptional();
            $itemOptional->item_id = $request->idItem;
            $itemOptional->type_optional_id = $request->idTypeOptional;
            $itemOptional->save(); 
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $itemOptional->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoTypeOptional
     * @param int idLanguage
     */
    public static function update(Request $dtoTypeOptional, int $idLanguage)
    {
        static::_validateData($dtoTypeOptional, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $typeOptionalOnDb = TypeOptional::find($dtoTypeOptional->id);
        
        DB::beginTransaction();

        try {
            $typeOptionalOnDb->order = $dtoTypeOptional->order;
            $typeOptionalOnDb->required = $dtoTypeOptional->required;
            $typeOptionalOnDb->save();

            $typeOptionalLanguageOnDb = TypeOptionalLanguages::where('type_optional_id', $typeOptionalOnDb->id)->where('language_id', $idLanguage)->get()->first();
            $typeOptionalLanguageOnDb->description = $dtoTypeOptional->description;
            $typeOptionalLanguageOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $typeOptional = TypeOptional::find($id);
        
        if (is_null($typeOptional)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            
            $typeOptionalLanguages = TypeOptionalLanguages::where('type_optional_id', $id);
            $typeOptionalLanguages->delete();

            $typeOptional->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        
    }

    /**
     * Delete item optional
     * 
     * @param int idOptional
     * @param int idItem
     * @param int idLanguage
     */
    public static function delItemTypeOptional(int $idTypeOptional, int $idItem, int $idLanguage)
    {
        $itemTypeOptional = ItemTypeOptional::where('type_optional_id', $idTypeOptional)->where('item_id', $idItem);
        
        if (is_null($itemTypeOptional)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            $itemTypeOptional->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

    }

    #endregion DELETE
}

<?php

namespace App\BusinessLogic;

use App\DocumentHead;
use App\DocumentHeadMdMicrodetectors;
use App\DocumentRow;
use App\DocumentRowMdMicrodetectors;
use App\DtoModel\DtoDocumentRow;
use App\Enums\DictionariesCodesEnum;
use App\Enums\DocumentTypeEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Enums\PickingStateEnum;
use App\Functionality;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class DocumentRowBL
{
    #region PRIVATE

    /**
     * Check function enabled
     * 
     * @param int idFunctionlity
     * @param int idUser
     * @param int idLanguage
     */
    private static function _checkFunctionEnabled(int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (is_null($idFunctionality)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Functionality::find($idFunctionality))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    /**
     * Convert row to dto
     * 
     * @param row documentRow
     * @param int idLanguage
     * 
     * @return DtoDocumentRow
     */
    private static function _convertToDto($documentRow, $idLanguage)
    {
        $dtoDocumentRow = new DtoDocumentRow();
        $dtoDocumentRow->id = $documentRow->id;
        $dtoDocumentRow->itemCode = $documentRow->internal_code;
        $dtoDocumentRow->itemDescription = DocumentRowLanguageBL::getByDocumentRowAndLanguage($documentRow->id, $idLanguage);
        $dtoDocumentRow->quantity = $documentRow->quantity;
        $dtoDocumentRow->pickedQuantity = $documentRow->picked_quantity;
        $dtoDocumentRow->documentCode = DocumentHead::find($documentRow->document_head_id)->code;

        return $dtoDocumentRow;
    }

    /**
     * Convert row to dto for Sixten
     * 
     * @param row documentRow
     * @param int idLanguage
     * 
     * @return DtoDocumentRow
     */
    private static function _convertToDtoSixten($documentRow, $idLanguage)
    {
        $dtoDocumentRow = new DtoDocumentRow();
        $dtoDocumentRow->id = $documentRow->id;
        $dtoDocumentRow->itemCode = $documentRow->internal_code;
        if ($documentRow->internal_code == '.') {
            $dtoDocumentRow->itemDescription = DocumentRowLanguageBL::getByDocumentRowAndLanguage($documentRow->id, $idLanguage);
        } else {
            $dtoDocumentRow->itemDescription = $documentRow->internal_code;
        }
        $dtoDocumentRow->quantity = $documentRow->quantity;
        $dtoDocumentRow->pickedQuantity = $documentRow->picked_quantity;

        if ($documentRow->picked_quantity == 0) {
            $dtoDocumentRow->pickingState = PickingStateEnum::None;
        } else {
            if ($documentRow->quantity == $documentRow->picked_quantity) {
                $dtoDocumentRow->pickingState = PickingStateEnum::All;
            } else {
                $dtoDocumentRow->pickingState = PickingStateEnum::Partial;
            }
        }

        $dtoDocumentRow->documentCode = DocumentHead::find($documentRow->document_head_id)->code;

        return $dtoDocumentRow;
    }

    /**
     * Convert row to dto for MdMicrodetectors
     * 
     * @param row documentRow
     * @param int idLanguage
     * 
     * @return DtoDocumentRow
     */
    private static function _convertToDtoMdMicrodetectors($documentRow, $idLanguage)
    {
        $dtoDocumentRow = new DtoDocumentRow();
        $dtoDocumentRow->id = $documentRow->id;
        $dtoDocumentRow->documentCode = $documentRow->document_head_id;
        $dtoDocumentRow->itemCode = $documentRow->codice_articolo;
        $dtoDocumentRow->quantity = $documentRow->quantity;
        $dtoDocumentRow->pickingState = $documentRow->check_spedizione;
        $dtoDocumentRow->cell = $documentRow->cell;

        //$dtoDocumentRow->documentCode = DocumentHeadMdMicrodetectors::find($documentRow->document_head_id)->document_head_id;

        return $dtoDocumentRow;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get sixten by id document head
     *
     * @param int idHead
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoDocumentRow
     */
    public static function getSixtenByHead(int $idHead, int $idFunctionality, int $idUser, int $idLanguage)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
        $result = collect();

        foreach (DocumentRow::where('document_head_id', $idHead)->get() as $documentRow) {
            $result->push(static::_convertToDtoSixten($documentRow, $idLanguage));
        }

        return $result;
    }

    /**
     * Get mdmicrodetectors by id document head
     *
     * @param string idHead
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoDocumentRow
     */
    public static function getMdMicrodetectorsByHead(string $idHead, int $idUser, int $idLanguage)
    {
        //static::_checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
        $result = collect();
        //$codShipping = DocumentHeadMdMicrodetectors::where('id', $idHead)->first();
        foreach (DocumentRowMdMicrodetectors::where('document_head_id', $idHead)->orderBy('check_spedizione')->get() as $documentRow) {
            $result->push(static::_convertToDtoMdMicrodetectors($documentRow, $idLanguage));
        }
        
        return $result;
    }

    /**
     * Get by id
     * 
     * @param string idErp
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoDocumentRow
     */
    public static function getByErp(string $idErp, int $idFunctionality, int $idUser, int $idLanguage)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
        return static::_convertToDto(DocumentRow::where('erp_id', $idErp)->first(), $idLanguage);
    }

    /**
     * Get number package for DDT
     * 
     * @param int idCustomer
     * @param int idMonth
     * @param int idFunctionality
     * 
     * @return \Illuminate\Http\Response
     */
    public static function getNumberPackageDDT(int $idCustomer, int $idMonth, int $idFunctionality, int $idUser, int $idLanguage)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $count = DB::select(
            'SELECT COUNT(DISTINCT(documents_rows.package_id)) as c
            FROM documents_heads
            INNER JOIN documents_rows ON documents_heads.id =  documents_rows.document_head_id
            WHERE documents_heads.customer_id = :idCustomer
            AND document_type_id = (SELECT id FROM documents_types WHERE code = :codeDocumentType)
            AND EXTRACT(MONTH FROM expected_shipping_date) = :idMonth;',
            ['idCustomer' => $idCustomer, 'codeDocumentType' => DocumentTypeEnum::BeforeDDT, 'idMonth' => $idMonth]
        );

        if (count($count)) {
            return $count[0]->c;
        } else {
            return 0;
        }
    }

    #endregion GET

    #region INSERT

    public static function insertBeforeDDT(Request $request, int $idUser, int $idLanguage)
    {
        static::_checkFunctionEnabled($request->idFunctionality, $idUser, $idLanguage);

        DB::beginTransaction();

        try {
            $documentRowOC = DB::select('SELECT documents_rows.*
                                    FROM documents_rows
                                    INNER JOIN documents_heads on documents_rows.document_head_id = documents_heads.id
                                    WHERE documents_heads.document_type_id = (SELECT id FROM documents_types WHERE code = :codeDocumentType)
                                    AND documents_rows.erp_id = :idErp', ['codeDocumentType' => DocumentTypeEnum::Order, 'idErp' => $request->idErp]);

            if (!count($documentRowOC)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DocumentRowNotExist), HttpResultsCodesEnum::InvalidPayload);
            }

            $beforeDDT = DocumentHeadBL::getIdBeforeDDTByErpRow($documentRowOC[0]->erp_id);

            if (count($beforeDDT)) {
                $idDocumentHeadBeforeDDT = $beforeDDT[0]->id;
            } else {
                $documentHeadOC = DocumentHeadBL::getById($documentRowOC[0]->document_head_id);

                $documentHeadDB = new DocumentHead();
                $documentHeadDB->customer_id = $documentHeadOC->customer_id;
                $documentHeadDB->document_type_id = DocumentTypeBL::getIdByCode(DocumentTypeEnum::BeforeDDT);
                $documentHeadDB->reference_code = $documentHeadOC->reference_code;
                $documentHeadDB->description = $documentHeadOC->description;
                $documentHeadDB->date = Carbon::now()->format(HttpHelper::getLanguageDateTimeFormat());
                $documentHeadDB->shipping_date = $documentHeadOC->shipping_date;
                $documentHeadDB->expected_shipping_date = $documentHeadOC->expected_shipping_date;
                $documentHeadDB->currency_id = $documentHeadOC->currency_id;
                $documentHeadDB->carriage_id = $documentHeadOC->carriage_id;
                $documentHeadDB->payment_type_id = $documentHeadOC->payment_type_id;
                $documentHeadDB->carrier_id = $documentHeadOC->carrier_id;
                $documentHeadDB->shipping_address = $documentHeadOC->shipping_address;
                $documentHeadDB->shipping_district_id = $documentHeadOC->shipping_district_id;
                $documentHeadDB->shipping_province_id = $documentHeadOC->shipping_province_id;
                $documentHeadDB->shipping_nation_id = $documentHeadOC->shipping_nation_id;
                // $documentHeadDB->code = $documentHeadOC->code;
                $documentHeadDB->save();

                $idDocumentHeadBeforeDDT = $documentHeadDB->id;
            }

            $documentRowBD = new DocumentRow();
            $documentRowBD->document_head_id = $idDocumentHeadBeforeDDT;
            $documentRowBD->document_row_type_id = $documentRowOC[0]->document_row_type_id;
            $documentRowBD->item_id = $documentRowOC[0]->item_id;
            $documentRowBD->unit_of_measure_id = $documentRowOC[0]->unit_of_measure_id;
            $documentRowBD->quantity = $request->quantity;
            $documentRowBD->unit_price = $documentRowOC[0]->unit_price;
            $documentRowBD->discount = $documentRowOC[0]->discount;
            $documentRowBD->vat_type_id = $documentRowOC[0]->vat_type_id;
            $documentRowBD->total_price = $documentRowOC[0]->total_price;
            $documentRowBD->sale_type_id = $documentRowOC[0]->sale_type_id;
            $documentRowBD->document_status_id = $documentRowOC[0]->document_status_id;
            $documentRowBD->expected_evaded_date = $documentRowOC[0]->expected_evaded_date;
            $documentRowBD->evaded_date = $documentRowOC[0]->evaded_date;
            $documentRowBD->order = $documentRowOC[0]->order;
            $documentRowBD->package_id = $request->idPackage;
            $documentRowBD->document_row_reference = DocumentRow::where('erp_id', $request->idErp)->first()->id;
            $documentRowBD->internal_code = ItemBL::get($documentRowOC[0]->item_id)->internal_code;

            if (is_null($documentRowBD->picked_quantity)) {
                $documentRowBD->picked_quantity = $request->quantity;
            } else {
                $documentRowBD->picked_quantity = $documentRowBD->picked_quantity + $request->quantity;
            }
            $documentRowBD->save();

            DocumentRowLanguageBL::insert($documentRowBD->id, $documentRowOC[0]->item_id, $idLanguage);

            $uDocumentRow = DocumentRow::find($documentRowOC[0]->id);
            $uDocumentRow->picked_quantity = $uDocumentRow->picked_quantity + $request->quantity;
            $uDocumentRow->save();

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update check article from shipping MdMicrodetectors
     *
     * @param Request $dtoData
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoDocumentRow
     */
    public static function updateMdMicrodetectorsShippingRow(Request $dtoData, int $idUser, int $idLanguage)
    {
        //|| $customerCode == "C057551"
        $customerCode = DocumentHeadMdMicrodetectors::where('document_head_id', $dtoData->idShipping)->first()->customer_id;
        if(isset($customerCode)){
            if($customerCode == "C057554" || $customerCode == "C059038" || $customerCode == "C057551" || $customerCode == "C057556" || $customerCode == "C059063" || $customerCode == "C057557" || $customerCode == "C059096" || $customerCode == "C047551"){
                $codArticleToCheck = substr($dtoData->articleToCheck, 0, 7);
            }else{
                $codArticleToCheck = $dtoData->articleToCheck;
            }
        }

        $codShipping = DocumentRowMdMicrodetectors::where('document_head_id', $dtoData->idShipping)->where('codice_articolo', $codArticleToCheck)->first();
        if (!is_null($codShipping)) {
            if($codShipping->check_spedizione){
                if($dtoData->cancelStatus){
                    $codShippingN = DocumentRowMdMicrodetectors::where('document_head_id', $dtoData->idShipping)->where('codice_articolo', $codArticleToCheck)->where('check_spedizione',true)->first();
                    if (!is_null($codShippingN)) {
                        $codShippingN->check_spedizione = false;
                        $codShippingN->save();
                        $msg = "1 - " . $codShippingN->quantity;
                    }else{
                        $codShipping->check_spedizione = false;
                        $codShipping->save();
                        $msg = "1 - " . $codShipping->quantity;
                    }
                    
                }else{
                    $codShippingN = DocumentRowMdMicrodetectors::where('document_head_id', $dtoData->idShipping)->where('codice_articolo', $codArticleToCheck)->where('check_spedizione',false)->first();
                    if (!is_null($codShippingN)) {
                        $codShippingN->check_spedizione = true;
                        $codShippingN->save();
                        $msg = "1 - " . $codShippingN->quantity;
                    }else{
                        $msg = "2 - ";
                    }
                }
            }else{
                $codShipping->check_spedizione = true;
                $codShipping->save();
                $msg = "1 - " . $codShipping->quantity;
            }
            return $msg;
        }else{
            $codShippingCliente = DocumentRowMdMicrodetectors::where('document_head_id', $dtoData->idShipping)->where('codice_articolo_cliente', $codArticleToCheck)->first();
            if (!is_null($codShippingCliente)) {
                if($codShippingCliente->check_spedizione){
                    if($dtoData->cancelStatus){
                        $codShippingN = DocumentRowMdMicrodetectors::where('document_head_id', $dtoData->idShipping)->where('codice_articolo_cliente', $codArticleToCheck)->where('check_spedizione',true)->first();
                        if (!is_null($codShippingN)) {
                            $codShippingN->check_spedizione = false;
                            $codShippingN->save();
                            $msg = "1 - " . $codShippingN->quantity;
                        }else{
                            $codShippingCliente->check_spedizione = false;
                            $codShippingCliente->save();
                            $msg = "1 - " . $codShippingCliente->quantity;
                        }
                        
                    }else{
                        $codShippingN = DocumentRowMdMicrodetectors::where('document_head_id', $dtoData->idShipping)->where('codice_articolo_cliente', $codArticleToCheck)->where('check_spedizione',false)->first();
                        if (!is_null($codShippingN)) {
                            $codShippingN->check_spedizione = true;
                            $codShippingN->save();
                            $msg = "1 - " . $codShippingN->quantity;
                        }else{
                            $msg = "2 - ";
                        }
                    }
                }else{
                    $codShippingCliente->check_spedizione = true;
                    $codShippingCliente->save();
                    $msg = "1 - " . $codShippingCliente->quantity;
                }
                return $msg;

            }else{
                $codShippingBarcode = DocumentRowMdMicrodetectors::where('document_head_id', $dtoData->idShipping)->where('barcode_spedizione', $codArticleToCheck)->first();
                if (!is_null($codShippingBarcode)) {
                    if($codShippingBarcode->check_spedizione){
                        if($dtoData->cancelStatus){
                            $codShippingN = DocumentRowMdMicrodetectors::where('document_head_id', $dtoData->idShipping)->where('barcode_spedizione', $codArticleToCheck)->where('check_spedizione',true)->first();
                            if (!is_null($codShippingN)) {
                                $codShippingN->check_spedizione = false;
                                $codShippingN->save();
                                $msg = "1 - " . $codShippingN->quantity;
                            }else{
                                $codShippingBarcode->check_spedizione = false;
                                $codShippingBarcode->save();
                                $msg = "1 - " . $codShippingBarcode->quantity;
                            }
                            
                        }else{
                            $codShippingN = DocumentRowMdMicrodetectors::where('document_head_id', $dtoData->idShipping)->where('barcode_spedizione', $codArticleToCheck)->where('check_spedizione',false)->first();
                            if (!is_null($codShippingN)) {
                                $codShippingN->check_spedizione = true;
                                $codShippingN->save();
                                $msg = "1 - " . $codShippingN->quantity;
                            }else{
                                $msg = "2 - ";
                            }
                        }
                    }else{
                        $codShippingBarcode->check_spedizione = true;
                        $codShippingBarcode->save();
                        $msg = "1 - " . $codShippingBarcode->quantity;
                    }
                    return $msg;
                }else{
                    return "0 - ";
                }
            }
        }
    }

    /**
     * Update reset all article from shipping MdMicrodetectors
     *
     * @param Request $dtoData
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoDocumentRow
     */
    public static function updateMdMicrodetectorsResetShippingRow(Request $dtoData, int $idUser, int $idLanguage)
    {
        foreach (DocumentRowMdMicrodetectors::where('document_head_id', $dtoData->id)->get() as $documentRow) {
            $documentRow->check_spedizione = false;
            $documentRow->save();
        }
        return true;

    }

    #endregion UPDATE
}

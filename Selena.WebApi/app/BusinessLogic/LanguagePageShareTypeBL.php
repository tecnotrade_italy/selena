<?php

namespace App\BusinessLogic;

use Illuminate\Support\Facades\DB;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use App\LanguagePageShareType;

class LanguagePageShareTypeBL
{
    #region GET

    /**
     * Get
     *
     * @param String $search
     * @return LanguagePageShareType
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return LanguagePageShareType::select('id', 'description')->all();
        } else {
            return LanguagePageShareType::where('description', 'ilike','%'. $search . '%')->select('id', 'description')->get();
        }
    }

    /**
     * Get by id
     *
     * @param int $id
     * @return Icon
     */
    public static function getById(int $id)
    {
        return LanguagePageShareType::find($id);
    }


    #endregion GET
}

<?php

namespace App\BusinessLogic;

use App\Content;
use App\ContentLanguage;
use App\DtoModel\DtoInterventions;
use App\DtoModel\DtoInterventionsInterventionId;
use App\DtoModel\DtoInterventionsPhotos;
use App\DtoModel\DtoNation;
use App\DtoModel\DtoSelenaViews;
use App\DtoModel\DtoTagTemplate;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\ExternalRepair;
use App\GrIntervention;
use App\GrInterventionPhoto;
use App\GrInterventionProcessing;
use App\GrQuotes;
use App\GrStates;
use App\Helpers\LogHelper;
use App\ItemGr;
use App\ItemLanguage;
use App\ModuleValidationGuarantee;
use App\User;
use App\SelenaSqlViews;
use App\StatesQuotesGr;
use App\UsersDatas;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchInterventionBL
{

  public static $dirImage = '../storage/app/public/images/Intervention/Foto3/GUASTI/';
  public static $dirImageIntervention = '../storage/app/public/images/Intervention/';
  public static $dirImageInterventionReportTest = '../storage/app/public/images/Intervention/Foto3/REPORT TEST/';
  


  private static function getUrlOpenFileImg()
  {
      if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
          $protocol = "https://";
      } else {
          $protocol = "http://";
      }
      return $protocol . $_SERVER["HTTP_HOST"];
  }


  /**
     * Search implemented
     * 
     * @param Request request
     * 
     */
    public static function searchImplemented(request $request){
        $result = collect();

        $content = '';
        $finalHtml = '';
        $strWhere = "";
        $idLanguageContent = $request->idLanguage; 
      
        $create_quote = $request->create_quote;       
        if ($create_quote == "SI") {
            $strWhere = ' AND i.create_quote = true';
        }
       if ($create_quote == "NO") {
          $strWhere = ' AND (i.create_quote = false OR i.create_quote is null)';
      }
        
       $code_gr = $request->code_gr; 
       if ($code_gr != "") {
            $strWhere = ' AND c.code_gr ilike \'%' . $code_gr . '%\'';
        } 
        $idIntervention = $request->idIntervention;
         if ($idIntervention != "") {
            $strWhere = $strWhere . ' AND i.id = ' . $idIntervention . '';
        }
         $code_intervention_gr = $request->code_intervention_gr;
         if ($code_intervention_gr != "") {
            $strWhere = $strWhere . ' AND i.code_intervention_gr = ' . $code_intervention_gr . '';
        }
        $nr_ddt = $request->nr_ddt; 
          if ($nr_ddt != "") {
            $strWhere = ' AND i.nr_ddt ilike \'%' . $nr_ddt . '%\'';
        } 
        $user = $request->user;
                if ($user != "") {
                    $strWhere = $strWhere . ' AND i.user_id = ' . $user . '';
        }
         $items = $request->items;
                if ($items != "") {
                    $strWhere = $strWhere . ' AND i.items_id = ' . $items . '';
        }
     
        foreach (DB::select(
            'SELECT i.id, i.code_intervention_gr, i.date_ddt, i.nr_ddt, i.user_id, i.description,i.imgmodule,
            i.defect,i.nr_ddt_gr,i.date_ddt_gr,i.create_quote,i.states_id,c.code_gr,
            i.ready,i.code_tracking, i.note_internal_gr,i.working_with_testing,i.working_without_testing,i.plant_type,i.trolley_type,i.series,i.voltage,i.rip_association,i.fileddt
            FROM gr_interventions as i
            left join gr_items as c on c.id = i.items_id
            WHERE i.language_id = :idLanguage ' . $strWhere . ' order by i.code_intervention_gr desc limit 100',
            ['idLanguage' => $idLanguageContent],
        ) as $Intervention) {

           // $CustomerDescription = User::where('id', $Intervention->user_id)->first();
           //$CustomerDescription = UsersDatas::where('user_id', $Intervention->user_id)->first();
            $StatesDescription = GrStates::where('id', $Intervention->states_id)->first();
            $Quotes = GrQuotes::where('id_intervention', $Intervention->id)->first();
           // $ItemId = ItemGr::where('id', $Intervention->items_id)->first();
            $idInterventionExternalRepair = ExternalRepair::where('id_intervention', $Intervention->id)->first();

       $dtoInterventions = new DtoInterventions();

      if (isset($Intervention->id)) {
        $dtoInterventions->id = $Intervention->id;
      }else{
        $dtoInterventions->id = '';
    } 
      if (isset($Intervention->date_ddt)) {
            $dtoInterventions->date_ddt = str_replace('00:00:00', '', $Intervention->date_ddt);
      }else{
            $dtoInterventions->date_ddt = '';
    }
      if (isset($Intervention->nr_ddt)) {
            $dtoInterventions->nr_ddt = $Intervention->nr_ddt;
      }else{
            $dtoInterventions->nr_ddt = '';
    }    

      $Gr_InterventionProcessing = GrInterventionProcessing::where('id_intervention', $Intervention->id)->first();
      if (isset($Gr_InterventionProcessing->id_intervention)) {
          $dtoInterventions->checkProcessingExist = true;
      }else{
          $dtoInterventions->checkProcessingExist = false;
      }

      if (isset($Quotes->id_states_quote)) {
              $dtoInterventions->id_states_quote = StatesQuotesGr::where('id', $Quotes->id_states_quote)->first()->description;
      }else{
              $dtoInterventions->id_states_quote = '';
    }
      if (isset($Intervention->user_id)) {
        $dtoInterventions->user_id = $Intervention->user_id;
      }else{
        $dtoInterventions->user_id = '';
    }  
      if (isset($Intervention->description)) {
        $dtoInterventions->description = $Intervention->description;
      }else{
        $dtoInterventions->description = '';
    }  
    if (isset($Intervention->code_intervention_gr)) {
        $dtoInterventions->code_intervention_gr = $Intervention->code_intervention_gr;  
    }else{
        $dtoInterventions->code_intervention_gr = '';
    }  
           
     /* if (isset($Intervention->imgmodule)) {
        $dtoInterventions->imgmodule = $Intervention->imgmodule;
      }else{
        $dtoInterventions->imgmodule = '';
    }    */
   
      if (file_exists(static::$dirImage . $Intervention->code_intervention_gr . '.pdf' )) {
        $dtoInterventions->imgmodule = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/GUASTI/' . $Intervention->code_intervention_gr . '.pdf';
      } else {
        if ($Intervention->imgmodule != '' && !is_null($Intervention->imgmodule)) {
                    $dtoInterventions->imgmodule = $Intervention->imgmodule;
                } else {
                    $dtoInterventions->imgmodule = '';
                }
      }

      if (isset($Intervention->defect)) {
        $dtoInterventions->defect = $Intervention->defect;
      }else{
        $dtoInterventions->defect = '';
    }  
    if (isset($Intervention->nr_ddt_gr)) {
        $dtoInterventions->nr_ddt_gr = $Intervention->nr_ddt_gr;
      }else{
        $dtoInterventions->nr_ddt_gr = '';
    }  
    if (isset($Intervention->date_ddt_gr)) {
        $dtoInterventions->date_ddt_gr = str_replace('00:00:00', '', $Intervention->date_ddt_gr);
      }else{
        $dtoInterventions->date_ddt_gr = '';
    }  
     $CreateQuote = "Si";
     $CreateQuoteNo = "No";

    if ($Intervention->create_quote == "true") {
        $dtoInterventions->create_quote = $CreateQuote;
    } else {
        $dtoInterventions->create_quote = $CreateQuoteNo;
    }
      
    if (isset($Intervention->states_id)) {
        $dtoInterventions->states_id = $Intervention->states_id;
      }else{
        $dtoInterventions->states_id = '';
    }  
    if (isset($Intervention->code_gr)) {
        $dtoInterventions->codeGr = $Intervention->code_gr;
      }else{
        $dtoInterventions->codeGr = '';
    } 

    if (isset($Intervention->ready)) {
          $dtoInterventions->ready = $Intervention->ready;
      }else {
          $dtoInterventions->ready = false;
    }

      if (isset($Intervention->fileddt)) {
        $dtoInterventions->fileddt = $Intervention->fileddt;
    } else {
        $dtoInterventions->fileddt = '';
    }

  /*  if (isset($CustomerDescription->business_name)) {
        $dtoInterventions->descriptionCustomer =  $CustomerDescription->business_name;
      }else{
        $dtoInterventions->descriptionCustomer = '';
    }*/

        if (isset($Intervention->user_id)) {
          $userdatas = UsersDatas::where('user_id', $Intervention->user_id)->first();
        if (isset($userdatas->business_name)){
            $dtoInterventions->descriptionCustomer = $userdatas->business_name;
        }else{
            $dtoInterventions->descriptionCustomer =  $userdatas->name . ' ' . $userdatas->surname; 
        }
      } else {
              $dtoInterventions->descriptionCustomer = '';
              $dtoInterventions->user_id = '';
      }

    if (isset($StatesDescription->description)) {
        $dtoInterventions->descriptionStates =  $StatesDescription->description;
      }else{
        $dtoInterventions->descriptionStates = '';
      }

    if (isset($Intervention->code_tracking)) {
                $dtoInterventions->code_tracking = $Intervention->code_tracking;
      } else {
                $dtoInterventions->code_tracking = '';
      }

        if (isset($Intervention->note_internal_gr)) {
              $dtoInterventions->note_internal_gr = $Intervention->note_internal_gr;
        } else {
              $dtoInterventions->note_internal_gr = '';
        }
          if (isset($Intervention->rip_association)) {
                $dtoInterventions->rip_association = '<br><label style="color:red">Riparazione Associata:</label>' . $Intervention->rip_association . '';  
            }else {
              $dtoInterventions->rip_association = '';
          }

      if (isset($idInterventionExternalRepair->id)) {
                $dtoInterventions->idInterventionExternalRepair = $idInterventionExternalRepair->id;
            }
             if (isset($idInterventionExternalRepair->date_return_product_from_the_supplier)) {
                $dtoInterventions->date_return_product_from_the_supplier = $idInterventionExternalRepair->date_return_product_from_the_supplier;
            }
            if (isset($idInterventionExternalRepair->ddt_supplier)) {
                $dtoInterventions->ddt_supplier = $idInterventionExternalRepair->ddt_supplier;
            }

            if (isset($Intervention->working_with_testing)) {
                $dtoInterventions->working_with_testing = $Intervention->working_with_testing;
              }else {
                $dtoInterventions->working_with_testing = false;
            }
             if (isset($Intervention->working_without_testing)) {
                $dtoInterventions->working_without_testing = $Intervention->working_without_testing;
             }else {
                $dtoInterventions->working_without_testing = false;
            }
             if (($Intervention->plant_type == '' || $Intervention->plant_type == null) && 
                ($Intervention->trolley_type == '' || $Intervention->trolley_type == null) && 
                ($Intervention->series == '' || $Intervention->series == null) && 
                ($Intervention->voltage == '' || $Intervention->voltage == null)) {
                    $dtoInterventions->checkModuleFault = false;
            }else{
                    $dtoInterventions->checkModuleFault = true;
            }

             $ModuleValidationGuarantee = ModuleValidationGuarantee::where('id_intervention', $Intervention->id)->first();
            if (isset($ModuleValidationGuarantee)) { 
               
                    $dtoInterventions->checkModuleValidationGuarantee = true; 
            }else{
                    $dtoInterventions->checkModuleValidationGuarantee = false;   
            }
        
            foreach (GrInterventionPhoto::where('intervention_id', $Intervention->id)->get() as $InterventionCaptures) {
              $DtoInterventionsPhotos = new DtoInterventionsPhotos();   
                if (file_exists(static::$dirImageIntervention . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.jpg' )) {
                    $DtoInterventionsPhotos->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.jpg';
                } else {
                  if (file_exists(static::$dirImageIntervention . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.JPG' )) {
                    $DtoInterventionsPhotos->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.JPG';
                  }else{
                      if ($InterventionCaptures->img != '' && !is_null($InterventionCaptures->img)) {
                        $pos = strpos($InterventionCaptures->img, $InterventionCaptures->intervention_id);
                        if($pos === false){
                          $DtoInterventionsPhotos->img = '';
                        }else{
                          $DtoInterventionsPhotos->img = $InterventionCaptures->img;
                        }
                    } else {
                        $DtoInterventionsPhotos->img = '';
                    }
                  }  
              }
                if (file_exists(static::$dirImageInterventionReportTest . $InterventionCaptures->code_intervention_gr . '.pdf' )) {
                  $DtoInterventionsPhotos->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/REPORT TEST/' . $InterventionCaptures->code_intervention_gr . '.pdf';
              } else {
                  if ($InterventionCaptures->img_rep_test != '' && !is_null($InterventionCaptures->img_rep_test)) {
                    // if ($InterventionCaptures->img_rep_test != '' && $InterventionCaptures->img_rep_test != null) {
                          $DtoInterventionsPhotos->img_rep_test = $InterventionCaptures->img_rep_test;
                      } else {
                          $DtoInterventionsPhotos->img_rep_test = '';
                      }
              }      
              $dtoInterventions->captures->push($DtoInterventionsPhotos);
          }

          $result->push($dtoInterventions);
        }  
        return $result;
      
        }
    }


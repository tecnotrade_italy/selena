<?php

namespace App\BusinessLogic;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoBusinessName;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\BusinessName;
use App\ItemGroupTechnicalSpecification;
use Intervention\Image\Facades\Image;
use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;


class BusinessNameBL
{
 #region GET

 /**
     * Get select
     *
     * @param String $search
     * @return Intervention
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return BusinessName::select('id', 'business_name as description')->get();
        } else {
            return BusinessName::where('business_name', 'ilike', $search . '%')->select('id', 'business_name as description')->get();
        }
    }


/**
     * getAllSupplierRegistrySearch
     * 
     * @param Request request
     * 
     */
    public static function getAllSupplierRegistrySearch(request $request){
        $result = collect();

        $content = '';
        $finalHtml = '';
        $strWhere = "";
        $business_name = $request->business_name;
           
        //$UserSearch = $request->business_name;
        //if ($UserSearch != "") {
          //  $strWhere = $strWhere . ' AND business_name.business_name = ' . $UserSearch . '';
       // }
      
        $BusinessNameSearch = $request->business_name;
        if ($BusinessNameSearch != "") {
          $strWhere = $strWhere . ' OR business_name_supplier.business_name ilike \'%' . $BusinessNameSearch . '%\'';
        }

        $TelephoneNumberSearch = $request->telephone_number;
        if ($TelephoneNumberSearch != "") {
          $strWhere = $strWhere . ' OR business_name_supplier.telephone_number ilike \'%' . $TelephoneNumberSearch . '%\'';
        }
        
         $MobilePhoneSearch = $request->mobile_phone;
        if ($MobilePhoneSearch != "") {
          $strWhere = $strWhere . ' OR business_name_supplier.mobile_phone ilike \'%' . $MobilePhoneSearch . '%\'';
        }

            foreach (DB::select(
                'SELECT business_name_supplier.*
                FROM business_name_supplier
                WHERE business_name_supplier.business_name = :business_name' . $strWhere,
                ['business_name' => $business_name],
            ) as $BusinessNameSupplier) {

            $DtoBusinessName = new DtoBusinessName();

             if (isset($BusinessNameSupplier->id)) {
                $DtoBusinessName->id = $BusinessNameSupplier->id;
            } else {
                $DtoBusinessName->id = '-';
            }
             if (isset($BusinessNameSupplier->business_name)) {
                $DtoBusinessName->business_name = $BusinessNameSupplier->business_name;
            } else {
                $DtoBusinessName->business_name = '-';
            }

             if (isset($BusinessNameSupplier->name)) {
                $DtoBusinessName->name = $BusinessNameSupplier->name;
            } else {
                $DtoBusinessName->name = '-';
            }
             if (isset($BusinessNameSupplier->surname)) {
                $DtoBusinessName->surname = $BusinessNameSupplier->surname;
            } else {
                $DtoBusinessName->surname = '-';
            }
             if (isset($BusinessNameSupplier->telephone_number)) {
                $DtoBusinessName->telephone_number = $BusinessNameSupplier->telephone_number;
            } else {
                $DtoBusinessName->telephone_number = '-';
            }
              if (isset($BusinessNameSupplier->mobile_phone)) {
                $DtoBusinessName->mobile_phone = $BusinessNameSupplier->mobile_phone;
            } else {
                $DtoBusinessName->mobile_phone = '-';
            }
              if (isset($BusinessNameSupplier->fax_number)) {
                $DtoBusinessName->fax_number = $BusinessNameSupplier->fax_number;
            } else {
                $DtoBusinessName->fax_number = '-';
            }
              if (isset($BusinessNameSupplier->email)) {
                $DtoBusinessName->email = $BusinessNameSupplier->email;
            } else {
                $DtoBusinessName->email = '-';
            }
    
      $result->push($DtoBusinessName);
    }
    return $result;
  }


/**
     * Get by getById
     * 
     * @param int id
     * @return DtoAccountVision
     */
        public static function getById(int $id)
    {

        $BusinessNameSupplier = BusinessName::where('id', $id)->first();
       
        $DtoBusinessName = new DtoBusinessName();

        if (isset($BusinessNameSupplier->id)) {
                $DtoBusinessName->id = $BusinessNameSupplier->id;
            } else {
                $DtoBusinessName->id = '-';
            }
           
            if (isset($BusinessNameSupplier->name)) {
                $DtoBusinessName->name = $BusinessNameSupplier->name;
            } else {
                $DtoBusinessName->name = '-';
            }
            if (isset($BusinessNameSupplier->surname)) {
                $DtoBusinessName->surname = $BusinessNameSupplier->surname;
            } else {
                $DtoBusinessName->surname = '-';
            }     

            if (isset($BusinessNameSupplier->business_name)) {
                $DtoBusinessName->business_name = $BusinessNameSupplier->business_name;
            } else {
                $DtoBusinessName->business_name = '-';
            }   
            if (isset($BusinessNameSupplier->address)) {
                $DtoBusinessName->address = $BusinessNameSupplier->address;
            } else {
                $DtoBusinessName->address = '-';
            } 

            if (isset($BusinessNameSupplier->code_postal)) {
                $DtoBusinessName->code_postal = $BusinessNameSupplier->code_postal;
            } else {
                $DtoBusinessName->code_postal = '-';
            } 
              if (isset($BusinessNameSupplier->province)) {
                $DtoBusinessName->province = $BusinessNameSupplier->province;
            } else {
                $DtoBusinessName->province = '-';
            } 
              if (isset($BusinessNameSupplier->country)) {
                $DtoBusinessName->country = $BusinessNameSupplier->country;
            } else {
                $DtoBusinessName->country = '-';
            } 

             if (isset($BusinessNameSupplier->telephone_number)) {
                $DtoBusinessName->telephone_number = $BusinessNameSupplier->telephone_number;
            } else {
                $DtoBusinessName->telephone_number = '-';
            }
              if (isset($BusinessNameSupplier->mobile_phone)) {
                $DtoBusinessName->mobile_phone = $BusinessNameSupplier->mobile_phone;
            } else {
                $DtoBusinessName->mobile_phone = '-';
            }
              if (isset($BusinessNameSupplier->fax_number)) {
                $DtoBusinessName->fax_number = $BusinessNameSupplier->fax_number;
            } else {
                $DtoBusinessName->fax_number = '-';
            }
              if (isset($BusinessNameSupplier->email)) {
                $DtoBusinessName->email = $BusinessNameSupplier->email;
            } else {
                $DtoBusinessName->email = '-';
            }

         return $DtoBusinessName;
}


/**
     * Get by getById
     * 
     * @param int id
     * @return DtoAccountVision
     */
        public static function getByIdUp(int $id)
    {

        $BusinessNameSupplier = BusinessName::where('id', $id)->first();
       
        $DtoBusinessName = new DtoBusinessName();

        if (isset($BusinessNameSupplier->id)) {
                $DtoBusinessName->id = $BusinessNameSupplier->id;
            } else {
                $DtoBusinessName->id = '-';
            }
           
            if (isset($BusinessNameSupplier->name)) {
                $DtoBusinessName->name = $BusinessNameSupplier->name;
            } else {
                $DtoBusinessName->name = '-';
            }
            if (isset($BusinessNameSupplier->surname)) {
                $DtoBusinessName->surname = $BusinessNameSupplier->surname;
            } else {
                $DtoBusinessName->surname = '-';
            }     

            if (isset($BusinessNameSupplier->business_name)) {
                $DtoBusinessName->business_name = $BusinessNameSupplier->business_name;
            } else {
                $DtoBusinessName->business_name = '-';
            }   
            if (isset($BusinessNameSupplier->address)) {
                $DtoBusinessName->address = $BusinessNameSupplier->address;
            } else {
                $DtoBusinessName->address = '-';
            } 

            if (isset($BusinessNameSupplier->code_postal)) {
                $DtoBusinessName->code_postal = $BusinessNameSupplier->code_postal;
            } else {
                $DtoBusinessName->code_postal = '-';
            } 
              if (isset($BusinessNameSupplier->province)) {
                $DtoBusinessName->province = $BusinessNameSupplier->province;
            } else {
                $DtoBusinessName->province = '-';
            } 
              if (isset($BusinessNameSupplier->country)) {
                $DtoBusinessName->country = $BusinessNameSupplier->country;
            } else {
                $DtoBusinessName->country = '-';
            } 

             if (isset($BusinessNameSupplier->telephone_number)) {
                $DtoBusinessName->telephone_number = $BusinessNameSupplier->telephone_number;
            } else {
                $DtoBusinessName->telephone_number = '-';
            }
              if (isset($BusinessNameSupplier->mobile_phone)) {
                $DtoBusinessName->mobile_phone = $BusinessNameSupplier->mobile_phone;
            } else {
                $DtoBusinessName->mobile_phone = '-';
            }
              if (isset($BusinessNameSupplier->fax_number)) {
                $DtoBusinessName->fax_number = $BusinessNameSupplier->fax_number;
            } else {
                $DtoBusinessName->fax_number = '-';
            }
              if (isset($BusinessNameSupplier->email)) {
                $DtoBusinessName->email = $BusinessNameSupplier->email;
            } else {
                $DtoBusinessName->email = '-';
            }

         return $DtoBusinessName;
}



 //updatedetailsupplierregistry
  public static function updatedetailsupplierregistry(Request $DtoBusinessName){

    $BusinessName = BusinessName::where('id', $DtoBusinessName->id)->first();

    DB::beginTransaction();
    try {
     
                $BusinessName->id = $DtoBusinessName->id;             
                $BusinessName->business_name = $DtoBusinessName->business_name;             
                $BusinessName->name = $DtoBusinessName->name; 
                $BusinessName->surname = $DtoBusinessName->surname;                             
                $BusinessName->address = $DtoBusinessName->address;
                $BusinessName->code_postal = $DtoBusinessName->code_postal;
                $BusinessName->country = $DtoBusinessName->country;
                $BusinessName->province = $DtoBusinessName->province; 
                $BusinessName->telephone_number = $DtoBusinessName->telephone_number;   
                $BusinessName->mobile_phone = $DtoBusinessName->mobile_phone;
                $BusinessName->fax_number = $DtoBusinessName->fax_number;
                $BusinessName->email = $DtoBusinessName->email;

                $BusinessName->save();
       
              
  DB::commit();
    } catch (Exception $ex) {
        DB::rollback();
        throw $ex;
    }
}
 #region updatedetailsupplierregistry

  /**
     * Insert
     * 
     * @param Request DtoBusinessName
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertsupplierregistry(Request $DtoBusinessName, int $idUser)
    {    
        DB::beginTransaction();
        try {
            
            $BusinessName = new BusinessName();
            
                $BusinessName->business_name = $DtoBusinessName->business_name;
                $BusinessName->name = $DtoBusinessName->name;
                $BusinessName->surname = $DtoBusinessName->surname;
                $BusinessName->address = $DtoBusinessName->address;
                $BusinessName->code_postal = $DtoBusinessName->code_postal;
                $BusinessName->country = $DtoBusinessName->country;
                $BusinessName->province = $DtoBusinessName->province; 
                $BusinessName->telephone_number = $DtoBusinessName->telephone_number;   
                $BusinessName->mobile_phone = $DtoBusinessName->mobile_phone;
                $BusinessName->fax_number = $DtoBusinessName->fax_number;
                $BusinessName->email = $DtoBusinessName->email;          
                $BusinessName->save();

        
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $BusinessName->id;
    }


 /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */

    public static function delete(int $id, int $idLanguage)
    {
        $BusinessName = BusinessName::find($id);

        if (is_null($BusinessName)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($BusinessName)) {
            DB::beginTransaction();
            try {
                BusinessName::where('id', $id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }  
        } 

    }



 }
       
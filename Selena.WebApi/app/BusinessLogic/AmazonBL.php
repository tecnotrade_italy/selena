<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoAmazonTemplate;
use App\DtoModel\DtoAmazonProduct;
use App\BusinessLogic\AmazonTemplateBL;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\AmazonTemplate;
use App\AmazonItem;
use Exception;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;
use Revolution\Amazon\ProductAdvertising\Facades\AmazonProduct;
use Revolution\Amazon\ProductAdvertisingAPI\v1\ApiException;
use Revolution\Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\api\DefaultApi;
use Revolution\Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\GetItemsRequest;
use Revolution\Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\GetItemsResource;
use Revolution\Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\PartnerType;
use Revolution\Amazon\ProductAdvertisingAPI\v1\com\amazon\paapi5\v1\ProductAdvertisingAPIClientException;

class AmazonBL
{
    public static function sniffaCodici()
    {
        foreach (DB::select(
            "select content from languages_pages 
            where content like '%[amazon box%'
            "
        ) as $items) {

            $items->content = str_replace('&quot;','"',$items->content);
            //mi memorizzo in un array tutte le posizioni dove ho un box
            $indices = [];
            
            for($pos = strpos($items->content,'[amazon'); $pos !== false; $pos = strpos($items->content, '[amazon', $pos + 1)) {
                //$indices.push($pos);
                array_push($indices, $pos);
            }
            $ind = 0;
            $lengthIndices = count($indices);
            for($ind = $lengthIndices - 1; $ind>=0;  $ind--){
                //se ho lo stesso shortcode riputo tante volte, dopo la prima non ne ho più e non devo più ciclare
                if (strpos($items->content, '[amazon') === false){
                    break;
                } 
    
                $inizio = $indices[$ind];
                $fine = strpos($items->content, ']', $inizio);
                
                $stringaAmazon = '';
                $stringaAmazon = substr($items->content, $inizio, $fine-$inizio+1);
                
                //se esco e stringaAmazon = "" significa che mi sono scordato di chiudere la parentesi quadra.
                //Lascio tutto così ed esco dal ciclo while...altrimenti va in loop infinito
                if ($stringaAmazon == ''){
                    break;
                }
     
                $stringaAmazon = str_replace(' ', '',  $stringaAmazon);
                $inizio = strpos($stringaAmazon, '"');
                $fine = strpos($stringaAmazon, '"', $inizio+1);
                $stringaAmazon = substr($stringaAmazon, $inizio+1, $fine-$inizio-1);
                
          
                //richiamo l'API amazon
                $arrayArticoli = explode(",", $stringaAmazon);
                foreach ($arrayArticoli as $iii) {
                    //inserisco l'asin nella tabella
                    $amazonItem = new AmazonItem();
                    $amazonItem->asin = $iii;
                    $amazonItem->created_id = 1;

                    $amazonItem->save();
                }

            }
            
        }
    }



    public static function syncroItemsViaAPI()
    {
        //vado a leggere tutti i codici ASIN dalla tabella Amazon_Items
        try {
            
            $i = 0;
            $listaCodici = "";
     
            foreach (DB::select(
                "SELECT asin
                FROM amazon_items
                "
            ) as $items) {

           

                //devo prendere dei gruppi con massimo 10 articoli

                $i++;
                $listaCodici = $listaCodici . $items->asin . ",";

                if ($i==10){
                    
                    $listaCodici = substr($listaCodici, 0, -1);
                    $listaCodici = str_replace(' ', '',  $listaCodici);
                    
                    //richiamo l'API amazon
                    $arrayArticoli = explode(",", $listaCodici);
                    $arrayArticoliAmazon = collect();
        
                    $arrayArticoliAmazon = static::getItems($arrayArticoli);       
                    if (!$arrayArticoliAmazon->isEmpty())
                    {
                        
                        foreach($arrayArticoliAmazon as $articolo){
      
                            $amazonItem = AmazonItem::where('asin', $articolo->asin)->get()->first();                            
                           
                            $amazonItem->title = $articolo->title;
                            $amazonItem->url = $articolo->url;
                            $amazonItem->informations = $articolo->informations;
                            $amazonItem->imageSmall = $articolo->imageSmall;
                            $amazonItem->imageMedium = $articolo->imageMedium;
                            $amazonItem->imageLarge = $articolo->imageLarge;
                            $amazonItem->buyingPrice = $articolo->buyingPrice;
                            $amazonItem->normalPrice = $articolo->normalPrice;
                            $amazonItem->discountPercentage = $articolo->discountPercentage;
                            $amazonItem->discountValue = $articolo->discountValue;
                            $amazonItem->prime = $articolo->prime;
                           
                            $amazonItem->save();
                        }
                    
                    }

                    $i=0;
                    $listaCodici = "";
                    sleep(3);
                }
            }
            
        
            if ($i<>0){
                $listaCodici = substr($listaCodici, 0, -1);
                $listaCodici = str_replace(' ', '',  $listaCodici);
                
                //richiamo l'API amazon
                $arrayArticoli = explode(",", $listaCodici);
                $arrayArticoliAmazon = collect();
    
                $arrayArticoliAmazon = static::getItems($arrayArticoli);       
                if (!$arrayArticoliAmazon->isEmpty())
                {
                    
                    foreach($arrayArticoliAmazon as $articolo){
    
                        $amazonItem = AmazonItem::where('asin', $articolo->asin)->get()->first();                            
                        
                        $amazonItem->title = $articolo->title;
                        $amazonItem->url = $articolo->url;
                        $amazonItem->informations = $articolo->informations;
                        $amazonItem->imageSmall = $articolo->imageSmall;
                        $amazonItem->imageMedium = $articolo->imageMedium;
                        $amazonItem->imageLarge = $articolo->imageLarge;
                        $amazonItem->buyingPrice = $articolo->buyingPrice;
                        $amazonItem->normalPrice = $articolo->normalPrice;
                        $amazonItem->discountPercentage = $articolo->discountPercentage;
                        $amazonItem->discountValue = $articolo->discountValue;
                        $amazonItem->prime = $articolo->prime;
                        
                        $amazonItem->save();
                    }
                
                }
            }
                

            
        } catch (\Throwable $ex) {
            LogHelper::error('' . $ex);
        }

    }

    public static function getHtml(string $shortcode)
    {
        
        //cerco il template giusto a seconda dello shortcode che ho
        if (strpos($shortcode, 'amazon box') !== false) {
            if (strpos($shortcode, 'vertical') !== false || strpos($shortcode, 'grid') !== false) {
                $template = AmazonTemplateBL::getByCode('vertical');
            }elseif (strpos($shortcode, 'list') !== false) {
                $template = AmazonTemplateBL::getByCode('list');
            }else{
                $template = AmazonTemplateBL::getByCode('horizontal');
            }
            
            //ora mi devo memorizzare i prodotti che sono tra i doppi apici dopo la scritta box
            $posInizio= strpos($shortcode, 'box="')+4;
            $posFine= strpos($shortcode, '"', $posInizio+1);
            $listaArticoli = substr($shortcode, $posInizio+1, $posFine-$posInizio-1);
            $listaArticoli = str_replace(' ', '',  $listaArticoli);
            
            $arrayArticoli = explode(",", $listaArticoli);
            $listaArticoli = '(\'' . str_replace(',', '\',\'',  $listaArticoli) . '\')';
            
            $html = "";
            
            if ($listaArticoli==''){
                return '';
            }else{
                
                //vado a sostituire gli hashtag con i dati letti su Amazon
                $htmlFinale = '<div class="container">';
                if (strpos($shortcode, 'vertical') !== false || strpos($shortcode, 'grid') !== false) 
                    $htmlFinale = $htmlFinale.'<div class="row amazon-vertical">';  
                    
                foreach (DB::select(
                    'SELECT * from amazon_items 
                    WHERE asin in ' .$listaArticoli
                )  as $articolo) {
                    $html = '';
                    if (strpos($shortcode, 'vertical') !== false || strpos($shortcode, 'grid') !== false) {
                       if (count($arrayArticoli)<=2)
                           $html = $html.'<div class="col-12 col-md-6 text-center">';
                       if (count($arrayArticoli)==3)
                           $html = $html.'<div class="col-12 col-md-4 text-center">';
                       if (count($arrayArticoli)==4)
                           $html = $html.'<div class="col-12 col-md-3 text-center">';
                       if (count($arrayArticoli)>=5)
                           $html = $html.'<div class="col-12 col-md-2 text-center">';
                   }
                    $html = $html.$template->content;        
                    
                    $html = str_replace('#amazonproductlink#', $articolo->url,  $html);
                    $html = str_replace('#amazonproductimagesmall#', $articolo->imageSmall,  $html);
                    $html = str_replace('#amazonproductimagemedium#', $articolo->imageMedium,  $html);
                    $html = str_replace('#amazonproductimagelarge#', $articolo->imageLarge,  $html);
                    $html = str_replace('#amazonproducttitle#', $articolo->title,  $html);
                    $html = str_replace('#amazonproductinfo#', $articolo->informations,  $html);
                    $html = str_replace('#amazonproductprice#', $articolo->buyingPrice,  $html);

                    $html = str_replace('#amazonproductpriceoriginal#', $articolo->normalPrice,  $html);
                    $html = str_replace('#amazonproductdiscountpercentage#', $articolo->discountPercentage,  $html);

                    if($articolo->discountValue!='')
                        $html = str_replace('#amazonproductdiscountvalue#', '<span class="discount-amount">'.$articolo->discountValue.'</span>',  $html);
                    else
                        $html = str_replace('#amazonproductdiscountvalue#', '',  $html);

                    if ($articolo->prime=='true')
                        $html = str_replace('#amazonproductprime#', '<img src="https://m.media-amazon.com/images/G/29/marketing/prime/prime_logo_detailpage._CB485945765_.png" style="width:150px;" class="fr-fic fr-dii">',  $html);
                    else
                        $html = str_replace('#amazonproductprime#', '',  $html);

                    //chiudo il div della colonna
                    if (strpos($shortcode, 'vertical') !== false || strpos($shortcode, 'grid') !== false) 
                        $html = $html.'</div>';

                    $htmlFinale = $htmlFinale.$html;
                }
                
                if (strpos($shortcode, 'vertical') !== false || strpos($shortcode, 'grid') !== false)
                    //chiudo il div "row amazon-vertical"
                   $htmlFinale = $htmlFinale.'</div>';


                $htmlFinale = $htmlFinale.'</div>';
                
                return $htmlFinale;
            }
        }else
        {
            return '';
        }
    }
    
/*
    public static function getHtml(string $shortcode)
    {
        
        //cerco il template giusto a seconda dello shortcode che ho
        if (strpos($shortcode, 'amazon box') !== false) {
            if (strpos($shortcode, 'vertical') !== false || strpos($shortcode, 'grid') !== false) {
                $template = AmazonTemplateBL::getByCode('vertical');
            }elseif (strpos($shortcode, 'list') !== false) {
                $template = AmazonTemplateBL::getByCode('list');
            }else{
                $template = AmazonTemplateBL::getByCode('horizontal');
            }
            //ora mi devo memorizzare i prodotti che sono tra i doppi apici dopo la scritta box
            $posInizio= strpos($shortcode, 'box="')+4;
            $posFine= strpos($shortcode, '"', $posInizio+1);
            $listaArticoli = substr($shortcode, $posInizio+1, $posFine-$posInizio-1);
            $listaArticoli = str_replace(' ', '',  $listaArticoli);
            
            $arrayArticoli = explode(",", $listaArticoli);
            $html = "";
            $arrayArticoliAmazon = collect();

            $arrayArticoliAmazon = static::getItems($arrayArticoli);

            if ($arrayArticoliAmazon->isEmpty()){
                return '';
            }else{
                //vado a sostituire gli hashtag con i dati letti su Amazon
                $htmlFinale = '<div class="container">';
                if (strpos($shortcode, 'vertical') !== false || strpos($shortcode, 'grid') !== false) 
                    $htmlFinale = $htmlFinale.'<div class="row amazon-vertical">';
                
                foreach($arrayArticoliAmazon as $articolo){
                    $html = '';
                    if (strpos($shortcode, 'vertical') !== false || strpos($shortcode, 'grid') !== false) {
                       if (count($arrayArticoli)<=2)
                           $html = $html.'<div class="col-12 col-md-6 text-center">';
                       if (count($arrayArticoli)==3)
                           $html = $html.'<div class="col-12 col-md-4 text-center">';
                       if (count($arrayArticoli)==4)
                           $html = $html.'<div class="col-12 col-md-3 text-center">';
                       if (count($arrayArticoli)>=5)
                           $html = $html.'<div class="col-12 col-md-2 text-center">';
                   }
                    $html = $html.$template->content;        
                    
                    $html = str_replace('#amazonproductlink#', $articolo->url,  $html);
                    $html = str_replace('#amazonproductimagesmall#', $articolo->imageSmall,  $html);
                    $html = str_replace('#amazonproductimagemedium#', $articolo->imageMedium,  $html);
                    $html = str_replace('#amazonproductimagelarge#', $articolo->imageLarge,  $html);
                    $html = str_replace('#amazonproducttitle#', $articolo->title,  $html);
                    $html = str_replace('#amazonproductinfo#', $articolo->informations,  $html);
                    $html = str_replace('#amazonproductprice#', $articolo->buyingPrice,  $html);

                    $html = str_replace('#amazonproductpriceoriginal#', $articolo->normalPrice,  $html);
                    $html = str_replace('#amazonproductdiscountpercentage#', $articolo->discountPercentage,  $html);

                    if($articolo->discountValue!='')
                        $html = str_replace('#amazonproductdiscountvalue#', '<span class="discount-amount">'.$articolo->discountValue.'</span>',  $html);
                    else
                        $html = str_replace('#amazonproductdiscountvalue#', '',  $html);

                    if ($articolo->prime=='true')
                        $html = str_replace('#amazonproductprime#', '<img src="https://m.media-amazon.com/images/G/29/marketing/prime/prime_logo_detailpage._CB485945765_.png" style="width:150px;" class="fr-fic fr-dii">',  $html);
                    else
                        $html = str_replace('#amazonproductprime#', '',  $html);

                    //chiudo il div della colonna
                    if (strpos($shortcode, 'vertical') !== false || strpos($shortcode, 'grid') !== false) 
                        $html = $html.'</div>';

                    $htmlFinale = $htmlFinale.$html;
                    
                }
                
                if (strpos($shortcode, 'vertical') !== false || strpos($shortcode, 'grid') !== false)
                    //chiudo il div "row amazon-vertical"
                   $htmlFinale = $htmlFinale.'</div>';


                $htmlFinale = $htmlFinale.'</div>';
               
                return $htmlFinale;
            }
        }elseif(strpos($shortcode, 'bestseller') !== false){
            $template = AmazonTemplateBL::getByCode('bestseller');

            $posInizio= strpos($shortcode, 'bestseller="')+11;
            $posFine= strpos($shortcode, '"', $posInizio+1);
            $stringaRicerca = substr($shortcode, $posInizio+1, $posFine-$posInizio-1);
            
            $nrItems='';
            if ( strpos($shortcode, 'items="')!=0){
                $posInizio= strpos($shortcode, 'items="')+6;
                $posFine= strpos($shortcode, '"', $posInizio+1);
                $nrItems = substr($shortcode, $posInizio+1, $posFine-$posInizio-1);
            }
            
            if ($nrItems=='')$nrItems= 10;

            $arrayArticoliAmazon = collect();
            $arrayArticoliAmazon = static::searchItems($stringaRicerca);
       
            if ($arrayArticoliAmazon->isEmpty()){
                return '';
            }else{
                //vado a sostituire gli hashtag con i dati letti su Amazon
                $htmlFinale = '<div class="container">
                <div class="row amazon-table-first-line">
                    <div class="col-12 col-md-1 text-center">  
                        <p>Preview</p>
                    </div>
                    <div class="col-12 col-md-8" style="padding-left:20px !important;">
                        <p>Descrizione</p>
                    </div>
                    <div class="col-12 col-md-3 text-center">
                        <p>Prezzo</p>
                    </div>
                   </div>';
                $i = 0;
                foreach($arrayArticoliAmazon as $articolo){
                    if ($nrItems==0)break;
                    $i++;
                    $html = $template->content;
                    $html = str_replace('#amazonproductposition#', $i,  $html);
                    $html = str_replace('#amazonproductlink#', $articolo->url,  $html);
                    $html = str_replace('#amazonproductimage#', $articolo->imageSmall,  $html);
                    $html = str_replace('#amazonproducttitle#', $articolo->title,  $html);
                    $html = str_replace('#amazonproductinfo#', $articolo->informations,  $html);
                    $html = str_replace('#amazonproductprice#', $articolo->buyingPrice,  $html);
                    $html = str_replace('#amazonproductinfo#', $articolo->informations,  $html);

                    $html = str_replace('#amazonproductpriceoriginal#', $articolo->normalPrice,  $html);
                    $html = str_replace('#amazonproductdiscountpercentage#', $articolo->discountPercentage,  $html);

                    if($articolo->discountValue!='')
                        $html = str_replace('#amazonproductdiscountvalue#', '<span class="discount-amount">'.$articolo->discountValue.'</span>',  $html);
                    else
                        $html = str_replace('#amazonproductdiscountvalue#', '',  $html);

                    if ($articolo->prime=='true')
                        $html = str_replace('#amazonproductprime#', '<img src="https://m.media-amazon.com/images/G/29/marketing/prime/prime_logo_detailpage._CB485945765_.png" style="width:80px;" class="fr-fic fr-dii">',  $html);
                    else
                        $html = str_replace('#amazonproductprime#', '',  $html);

                    $htmlFinale = $htmlFinale.$html;
                    if ($i==$nrItems)break;
                }

                $htmlFinale = $htmlFinale.'</div>';

                return $htmlFinale;
            }
        }
    }
*/

    private static function getItems($arrayArticoli)
    {
        $articoli = collect(); 
        $getItemsResponse = AmazonProduct::items($arrayArticoli);
        //$getItemsResponse = [];
        
        if (isset($getItemsResponse['ItemsResult'])) {
            
            if (isset($getItemsResponse['ItemsResult']['Items'])) {
                
                $responseList = [];
                foreach ($getItemsResponse['ItemsResult']['Items'] as $item) {
                    $responseList[$item['ASIN']] = $item;
                }
                
                foreach ($responseList as $item) {

                    if (isset($item)) {
                        $dtoAmazonProduct = new DtoAmazonProduct();
                        
                        if ($item['ASIN']) {
                            $dtoAmazonProduct->asin = $item['ASIN'];
                        }
                        if (isset($item['ItemInfo']) and isset($item['ItemInfo']['Title']) and isset($item['ItemInfo']['Title']['DisplayValue'])) {
                            $dtoAmazonProduct->title = $item['ItemInfo']['Title']['DisplayValue'];
                        }
                        if (isset($item['DetailPageURL'])) {                           
                            $dtoAmazonProduct->url = $item['DetailPageURL'];
                        }
                        if (isset($item['ItemInfo']) and isset($item['ItemInfo']['Features']) and isset($item['ItemInfo']['Features']['DisplayValues'])) {
                            $dtoAmazonProduct->informations = '<ul>';
                            foreach ($item['ItemInfo']['Features']['DisplayValues'] as $info) {
                                $dtoAmazonProduct->informations = $dtoAmazonProduct->informations.'<li>'.$info.'</li>';
                            }
                            $dtoAmazonProduct->informations = $dtoAmazonProduct->informations.'</ul>';
                        }
                        if (isset($item['Images']) and isset($item['Images']['Primary']) 
                            and isset($item['Images']['Primary']['Small']) and isset($item['Images']['Primary']['Small']['URL'])) {                          
                            $dtoAmazonProduct->imageSmall =$item['Images']['Primary']['Small']['URL'];
                        }
                        if (isset($item['Images']) and isset($item['Images']['Primary']) 
                            and isset($item['Images']['Primary']['Medium']) and isset($item['Images']['Primary']['Medium']['URL'])) {                          
                            $dtoAmazonProduct->imageMedium =$item['Images']['Primary']['Medium']['URL'];
                        }
                        if (isset($item['Images']) and isset($item['Images']['Primary']) 
                            and isset($item['Images']['Primary']['Large']) and isset($item['Images']['Primary']['Large']['URL'])) {                          
                            $dtoAmazonProduct->imageLarge =$item['Images']['Primary']['Large']['URL'];
                        }
                        if (isset($item['Offers']) and
                            isset($item['Offers']['Listings'])
                            and isset($item['Offers']['Listings'][0]['Price'])
                            and isset($item['Offers']['Listings'][0]['Price']['DisplayAmount'])) {
                                $dtoAmazonProduct->buyingPrice =$item['Offers']['Listings'][0]['Price']['DisplayAmount'];
                        }
                        if (isset($item['Offers']) and
                            isset($item['Offers']['Listings'])
                            and isset($item['Offers']['Listings'][0]['SavingBasis'])
                            and isset($item['Offers']['Listings'][0]['SavingBasis']['DisplayAmount'])) {
                                $dtoAmazonProduct->normalPrice = $item['Offers']['Listings'][0]['SavingBasis']['DisplayAmount'];
                        }
                        if (isset($item['Offers']) and
                            isset($item['Offers']['Listings'])
                            and isset($item['Offers']['Listings'][0]['Price'])
                            and isset($item['Offers']['Listings'][0]['Price']['Savings'])
                            and isset($item['Offers']['Listings'][0]['Price']['Savings']['Amount'])) {
                                $dtoAmazonProduct->discountValue ='-'.$item['Offers']['Listings'][0]['Price']['Savings']['Amount'].' €';
                                
                                if (isset($item['Offers']['Listings'][0]['Price']['Savings']['Percentage'])){
                                    $dtoAmazonProduct->discountPercentage =$item['Offers']['Listings'][0]['Price']['Savings']['Percentage'];
                                }
                        }

                        if (isset($item['Offers']) and
                            isset($item['Offers']['Listings'])
                            and isset($item['Offers']['Listings'][0]['DeliveryInfo'])
                            and isset($item['Offers']['Listings'][0]['DeliveryInfo']['IsPrimeEligible'])) {
                                
                                if($item['Offers']['Listings'][0]['DeliveryInfo']['IsPrimeEligible'] == '1') 
                                    $dtoAmazonProduct->prime = 'true';
                                else
                                    $dtoAmazonProduct->prime = 'false';
                        }

                        $articoli->push($dtoAmazonProduct); 

                    } else {
                    }
                }
            }
           

        } else{
        }

        return $articoli;
    }

    private static function searchItems($stringaDaCercare)
    {

        $articoli = collect(); 
        //$getItemsResponse = AmazonProduct::search('All', $stringaDaCercare , 1);
        $getItemsResponse = [];

        if (isset($getItemsResponse['SearchResult'])) {
            
            if (isset($getItemsResponse['SearchResult']['Items'])) {
                
                $responseList = [];
                foreach ($getItemsResponse['SearchResult']['Items'] as $item) {
                    $responseList[$item['ASIN']] = $item;
                }
                
                foreach ($responseList as $item) {

                    if (isset($item)) {
                        $dtoAmazonProduct = new DtoAmazonProduct();
                        
                        if ($item['ASIN']) {
                            $dtoAmazonProduct->asin = $item['ASIN'];
                        }
                        if (isset($item['ItemInfo']) and isset($item['ItemInfo']['Title']) and isset($item['ItemInfo']['Title']['DisplayValue'])) {
                            $dtoAmazonProduct->title = $item['ItemInfo']['Title']['DisplayValue'];
                        }
                        if (isset($item['DetailPageURL'])) {                          
                            $dtoAmazonProduct->url = $item['DetailPageURL'];
                        }
                        if (isset($item['ItemInfo']) and isset($item['ItemInfo']['Features']) and isset($item['ItemInfo']['Features']['DisplayValues'])) {
                            $dtoAmazonProduct->informations = '<ul>';
                            foreach ($item['ItemInfo']['Features']['DisplayValues'] as $info) {
                                $dtoAmazonProduct->informations = $dtoAmazonProduct->informations.'<li>'.$info.'</li>';
                            }
                            $dtoAmazonProduct->informations = $dtoAmazonProduct->informations.'</ul>';
                        }
                        if (isset($item['Images']) and isset($item['Images']['Primary']) 
                            and isset($item['Images']['Primary']['Small']) and isset($item['Images']['Primary']['Small']['URL'])) {                          
                            $dtoAmazonProduct->imageSmall =$item['Images']['Primary']['Small']['URL'];
                        }
                        if (isset($item['Images']) and isset($item['Images']['Primary']) 
                            and isset($item['Images']['Primary']['Medium']) and isset($item['Images']['Primary']['Medium']['URL'])) {                          
                            $dtoAmazonProduct->imageMedium =$item['Images']['Primary']['Medium']['URL'];
                        }
                        if (isset($item['Images']) and isset($item['Images']['Primary']) 
                            and isset($item['Images']['Primary']['Large']) and isset($item['Images']['Primary']['Large']['URL'])) {                           
                            $dtoAmazonProduct->imageLarge =$item['Images']['Primary']['Large']['URL'];
                        }
                        if (isset($item['Offers']) and
                            isset($item['Offers']['Listings'])
                            and isset($item['Offers']['Listings'][0]['Price'])
                            and isset($item['Offers']['Listings'][0]['Price']['DisplayAmount'])) {
                                $dtoAmazonProduct->buyingPrice =$item['Offers']['Listings'][0]['Price']['DisplayAmount'];
                        }
                        if (isset($item['Offers']) and
                            isset($item['Offers']['Listings'])
                            and isset($item['Offers']['Listings'][0]['SavingBasis'])
                            and isset($item['Offers']['Listings'][0]['SavingBasis']['DisplayAmount'])) {
                                $dtoAmazonProduct->normalPrice = $item['Offers']['Listings'][0]['SavingBasis']['DisplayAmount'];
                        }
                        if (isset($item['Offers']) and
                            isset($item['Offers']['Listings'])
                            and isset($item['Offers']['Listings'][0]['Price'])
                            and isset($item['Offers']['Listings'][0]['Price']['Savings'])
                            and isset($item['Offers']['Listings'][0]['Price']['Savings']['Amount'])) {
                                $dtoAmazonProduct->discountValue ='-'.$item['Offers']['Listings'][0]['Price']['Savings']['Amount'].' €';
                                
                                if (isset($item['Offers']['Listings'][0]['Price']['Savings']['Percentage'])){
                                    $dtoAmazonProduct->discountPercentage =$item['Offers']['Listings'][0]['Price']['Savings']['Percentage'];
                                }
                        }

                        if (isset($item['Offers']) and
                            isset($item['Offers']['Listings'])
                            and isset($item['Offers']['Listings'][0]['DeliveryInfo'])
                            and isset($item['Offers']['Listings'][0]['DeliveryInfo']['IsPrimeEligible'])) {
                                
                                if($item['Offers']['Listings'][0]['DeliveryInfo']['IsPrimeEligible'] == '1') 
                                    $dtoAmazonProduct->prime = 'true';
                                else
                                    $dtoAmazonProduct->prime = 'false';
                        }

                        $articoli->push($dtoAmazonProduct); 

                    } else {
                    }
                }
            }
           

        } else{
        }

        return $articoli;
    }




}

<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoVatType;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\VatType;
use Exception;
use Illuminate\Http\Request;

class VatTypeBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request dtoVatType
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $dtoVatType, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($dtoVatType->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (VatType::find($dtoVatType->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($dtoVatType->rate)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CompanyNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    /**
     * Convert to dto
     * 
     * @param VatType postalCode
     * 
     * @return DtoVatType
     */
    private static function _convertToDto($vatType)
    {
        $dtoVatType = new DtoVatType();

        if (isset($vatType->id)) {
            $dtoVatType->id = $vatType->id;
        }

        if (isset($vatType->rate)) {
            $dtoVatType->rate = $vatType->rate;
        }

        return $dtoVatType;
    }


    /**
     * Convert to VatType
     * 
     * @param DtoVatType dtoVatType
     * 
     * @return VatType
     */
    private static function _convertToModel($dtoVatType)
    {
        $vatType = new VatType();

        if (isset($dtoVatType->id)) {
            $vatType->id = $dtoVatType->id;
        }

        if (isset($dtoVatType->rate)) {
            $vatType->rate = $dtoVatType->rate;
        }

        return $vatType;
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoVatType
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(VatType::find($id));
    }

    /**
     * Get select
     *
     * @param String $search
     * @return VatType
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return VatType::select('id', 'rate as description')->get();
        } else {
            return VatType::where('rate', 'ilike', $search . '%')->select('id', 'rate as description')->get();
        }
    }

    /**
     * Get all
     * 
     * @return DtoVatType
     */
    public static function getAll()
    {
        return VatType::all()->map(function ($vatType) {
            return static::_convertToDto($vatType);
        });
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request dtoVatType
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $dtoVatType, int $idLanguage)
    {
        static::_validateData($dtoVatType, $idLanguage, DbOperationsTypesEnum::INSERT);
        $customer = static::_convertToModel($dtoVatType);
        $customer->save();
        return $customer->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoVatType
     * @param int idLanguage
     */
    public static function update(Request $dtoVatType, int $idLanguage)
    {
        static::_validateData($dtoVatType, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $customerOnDb = VatType::find($dtoVatType->id);
        DBHelper::updateAndSave(static::_convertToModel($dtoVatType), $customerOnDb);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $vatType = VatType::find($id);

        if (is_null($vatType)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $vatType->delete();
    }

    #endregion DELETE
}

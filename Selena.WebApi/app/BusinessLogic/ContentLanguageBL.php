<?php

namespace App\BusinessLogic;

use App\Brand;
use App\Category;
use App\CategoryLanguage;
use App\Content;
use App\ContentLanguage;
use App\AdvertisingArea;
use App\AdvertisingBanner;
use App\AdvertisingBannerReport;
use App\GroupTechnicalSpecificationLanguage;
use App\Cart;
use App\Item;
use App\LanguagePageCategory;
use App\PagePageCategory;
use App\LinkPageCategory;
use App\Enums\DictionariesCodesEnum;
use App\DtoModel\DtoPagination;
use App\DtoModel\DtoAdvertisingBanner;
use App\DtoModel\DtoContentLanguages;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\ItemLanguage;
use App\ItemPhotos;
use App\ItemFavoritesUser;
use App\SelenaSqlViews;
use App\BusinessLogic\SettingBL;
use App\BusinessLogic\ItemPriceBL;
use App\BusinessLogic\CartBL;
use App\BusinessLogic\CommunityImageBL;
use App\BusinessLogic\CommunityImageCommentBL;
use App\CartDetailOptional;
use App\UserPhoto;
use App\CommunityImage;
use App\UsersDatas;
use App\RoleUser;
use App\Role;
use App\Enums\SettingEnum;
use App\ItemGr;
use App\LanguageNation;
use App\LanguageProvince;
use App\OptionalLanguages;
use App\OptionalTypology;
use App\Provinces;
use App\Producer;
use App\Supports;
use App\Tipology;
use App\User;
use App\TypeOptionalLanguages;
use App\UserAddress;
use App\CategoryItem;
use App\ItemAttachmentLanguageTypeTable;
use App\ItemAttachmentType;
use App\ItemAttachmentLanguageFileTable;
use App\CategoriesFilterLanguages;

use Exception;


use Illuminate\Support\Facades\DB;

class ContentLanguageBL
{


    public static $dirImageFiles = '../storage/app/public/files/';

    #region PRIVATE
    

    /**
     * render sub menu
     * 
     * @param subMenu idItem
     */
    private static function _renderSubMenu($subMenu){
        $htmlSub = "";
        foreach($subMenu as $subMenuList){
            //controllare se l'if si scrive così - controllare se array subMenu è valorizzato
            
            if(count($subMenuList->subMenu)>0){
                $htmlSub = $htmlSub . '<li class="dropdown-submenu">';
                $htmlSub = $htmlSub . '<a class="nav-link dropdown-item dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" href="' . $subMenuList->url . '" onClick="window.location=\'' . $subMenuList->url . '\';" id="navbarDropdownMenuLink' . $subMenuList->id . '" role="button" aria-haspopup="true" aria-expanded="false">';
                $htmlSub = $htmlSub . '<span>' . $subMenuList->title . '</span></a>'; 
                //$htmlSub = $htmlSub . '<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink' . $subMenuList->id . '">';
                $htmlSub = $htmlSub . '<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink' . $subMenuList->id . '">';
                $htmlSub = $htmlSub . static::_renderSubMenu($subMenuList->subMenu);
                $htmlSub = $htmlSub . '</ul>';
                $htmlSub = $htmlSub . '</li>';
                //$htmlSub = $htmlSub . '</div>';
            }else{
                $htmlSub = $htmlSub . '<li>';
                $htmlSub = $htmlSub . '<a class="nav-link dropdown-item" href="' . $subMenuList->url . '">';
                $htmlSub = $htmlSub . '<span>' . $subMenuList->title . '</span>';
                $htmlSub = $htmlSub . '</a>';
                $htmlSub = $htmlSub . '</li>';
            }
        }

        return $htmlSub;
    }

    /**
     * render sub menu
     * 
     * @param subMenu idItem
     */
    private static function _renderSubBurgerMenu($subMenu){
        $htmlSub = "";
        foreach($subMenu as $subMenuList){

            //controllare se l'if si scrive così - controllare se array subMenu è valorizzato
            if(count($subMenuList->subMenu)>0){
                $htmlSub = $htmlSub . '<ul><a class="burger-menu-a" href="' . $subMenuList->url . '"><li class="burger-menu-li">' . $subMenuList->title . '</li></a>';
                $htmlSub = $htmlSub . static::_renderSubBurgerMenu($subMenuList->subMenu);
                $htmlSub = $htmlSub . '</ul>';
            }else{
                $htmlSub = $htmlSub . '<ul><a class="burger-menu-a" href="' . $subMenuList->url . '"><li class="burger-menu-li">' . $subMenuList->title . '</li></a></ul>';
            }
        }

        return $htmlSub;
    }

    

    /**
     * Replace item content img agg
     * 
     * @param int idItem
     * @param string codeContent
     * @param string elementReplaced
     * @param string content
     */
    private static function _itemContentImgAgg(int $idItem, string $codeContent, string $elementReplaced, string $contentPassed)
    {
        $ItemPhoto = ItemPhotos::where('item_id', $idItem);
        
        if(isset($ItemPhoto)){
            $idContent = Content::where('code', $codeContent)->first();
            if(isset($idContent)){
                $htmlContent = ContentLanguage::where('content_id', $idContent->id)->first()->content;
                if(isset($htmlContent)){
                    $finalHtml = '';
                    $newContent  = '';
                    foreach (DB::select(
                        'SELECT *
                        FROM itemsphotos
                        WHERE itemsphotos.item_id = :idItem',
                        ['idItem' => $idItem]
                    ) as $photoUser){
                        $newContent = $htmlContent;
                        if (strpos($newContent, '#img#') !== false) {
                            $newContent = str_replace('#img#', $photoUser->img,  $newContent);
                        }
                        $finalHtml = $finalHtml . $newContent;
                    }
                }
                return str_replace($elementReplaced , $finalHtml,  $contentPassed); 
            }else{
                return "";
            }  
        }else{
            return "";
        }
    }

    /**
     * Replace item content technical specifications
     * 
     * @param int idItem
     * @param string codeContent
     * @param string elementReplaced
     * @param string content
     */
    private static function _itemContentTechnicalSpecifications(int $idItem, string $codeContent, string $elementReplaced, string $contentPassed)
    {
        //controllo 
        //$ItemPhoto = ItemPhotos::where('item_id', $idItem);
        /*
        if(isset($ItemPhoto)){
            $idContent = Content::where('code', $codeContent)->first();
            if(isset($idContent)){
                $htmlContent = ContentLanguage::where('content_id', $idContent->id)->first()->content;
                if(isset($htmlContent)){
                    $finalHtml = '';
                    $newContent  = '';
                    foreach (DB::select(
                        'SELECT *
                        FROM itemsphotos
                        WHERE itemsphotos.item_id = :idItem',
                        ['idItem' => $idItem]
                    ) as $photoUser){
                        $newContent = $htmlContent;
                        if (strpos($newContent, '#img#') !== false) {
                            $newContent = str_replace('#img#', $photoUser->img,  $newContent);
                        }
                        $finalHtml = $finalHtml . $newContent;
                    }
                }
                return str_replace($elementReplaced , $finalHtml,  $contentPassed); 
            }else{
                return "";
            }  
        }else{
            return "";
        }
        */
    }

    

    /**
     * Replace user content img agg
     * 
     * @param int idUserItem
     * @param string codeContent
     * @param string elementReplaced
     * @param string content
     */
    private static function _userContentImgAgg(int $idUserItem,string $codeContent, string $elementReplaced, string $contentPassed)
    {
        $UserPhoto = UserPhoto::where('user_id', $idUserItem);
        
        if(isset($UserPhoto)){
            $idContent = Content::where('code', $codeContent)->first();
            if(isset($idContent)){
                $htmlContent = ContentLanguage::where('content_id', $idContent->id)->first()->content;
                if(isset($htmlContent)){
                    $finalHtml = '';
                    $newContent  = '';
                    foreach (DB::select(
                        'SELECT *
                        FROM users_photos
                        WHERE users_photos.user_id = :idUser',
                        ['idUser' => $idUserItem]
                    ) as $photoUser){
                        $newContent = $htmlContent;
                        if (strpos($newContent, '#img#') !== false) {
                            $newContent = str_replace('#img#', $photoUser->img,  $newContent);
                        }
                        $finalHtml = $finalHtml . $newContent;
                    }
                }
                return str_replace($elementReplaced , $finalHtml,  $contentPassed); 
            }else{
                return "";
            }  
        }else{
            return "";
        }
    }



 /**
     * Replace user content
     * 
     * @param int idUserItem
     * @param string codeContent
     * @param string elementReplaced
     * @param string content
     */
    private static function _userContentUserAddress(int $idUserItem, string $codeContent, string $elementReplaced, string $contentPassed)
 {
        if(isset($idUserItem)){
            $idContent = Content::where('code', $codeContent)->first();
            if(isset($idContent)){
                $htmlContent = ContentLanguage::where('content_id', $idContent->id)->first()->content;
                if(isset($htmlContent)){
                    $finalHtml = '';
                    $tags = SelenaViewsBL::getTagByType('DetailAdress');
                    foreach (DB::select(
                        'SELECT user_address.*
                        FROM user_address
                        WHERE user_address.user_id = :idUser',
                        ['idUser' => $idUserItem]
                    ) as $useradress){
                        $newContent = $htmlContent;
                        foreach ($tags as $fieldsDb){
                            if(isset($fieldsDb->tag)){
                                if (strpos($newContent, $fieldsDb->tag) !== false) {
                                    $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                    $newContent = str_replace($fieldsDb->tag , $useradress->{$cleanTag},  $newContent);
                                   
                                }
                            }
                        }
                        $finalHtml = $finalHtml . $newContent;
                    };
                    return str_replace($elementReplaced , $finalHtml,  $contentPassed);   
                }
            }
        }
    }

    #endregion PRIVATE



    /**
     * Replace user content
     * 
     * @param int idUserItem
     * @param string codeContent
     * @param string elementReplaced
     * @param string content
     */
    private static function _userContent(int $idUserItem, string $codeContent, string $elementReplaced, string $contentPassed)
    {
        if(isset($idUserItem)){
            $idContent = Content::where('code', $codeContent)->first();
            if(isset($idContent)){
                $htmlContent = ContentLanguage::where('content_id', $idContent->id)->first()->content;
                if(isset($htmlContent)){
                    $finalHtml = '';
                    $tags = SelenaViewsBL::getTagByType('UserDetail');
                    foreach (DB::select(
                        'SELECT *
                        FROM users_datas
                        WHERE user_id = :idUser',
                        ['idUser' => $idUserItem]
                    ) as $items){
                        $newContent = $htmlContent;
                        foreach ($tags as $fieldsDb){
                            if(isset($fieldsDb->tag)){
                                if (strpos($newContent, $fieldsDb->tag) !== false) {
                                    $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                    $newContent = str_replace($fieldsDb->tag , $items->{$cleanTag},  $newContent);
                                }
                            }
                        }
                        $finalHtml = $finalHtml . $newContent;
                    };
                    return str_replace($elementReplaced , $finalHtml,  $contentPassed);   
                }
            }
        }
    }

    #endregion PRIVATE

    #region PRIVATE

    /**
     * Replace user content
     * 
     * @param int idUserItem
     * @param string codeContent
     * @param string elementReplaced
     * @param string content
     */
    private static function _userContentItem(int $idUserItem, string $codeContent, string $elementReplaced, string $contentPassed)
    {
        if(isset($idUserItem)){
            $idContent = Content::where('code', $codeContent)->first();
            if(isset($idContent)){
                $htmlContent = ContentLanguage::where('content_id', $idContent->id)->first()->content;
                if(isset($htmlContent)){
                    $finalHtml = '';
                    $tags = SelenaViewsBL::getTagByType('ShopItemDetail');
                    foreach (DB::select(
                        'SELECT items.*, items_languages.*
                        FROM items
                        INNER JOIN items_languages ON items.id = items_languages.item_id
                        WHERE items.created_id = :idUser',
                        ['idUser' => $idUserItem]
                    ) as $itemdetail){
                        $newContent = $htmlContent;
                        foreach ($tags as $fieldsDb){
                            if(isset($fieldsDb->tag)){
                                if (strpos($newContent, $fieldsDb->tag) !== false) {
                                    $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                    if(is_numeric($itemdetail->{$cleanTag})){
                                        $tag = str_replace('.',',',substr($itemdetail->{$cleanTag}, 0, -2));
                                    }else{
                                        $tag = $itemdetail->{$cleanTag};
                                    }
                                    $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);
                                }
                            }
                        }
                        $finalHtml = $finalHtml . $newContent;
                    };
                    return str_replace($elementReplaced , $finalHtml,  $contentPassed);   
                }
            }
        }
    }

    /**
     * Replace user content
     * 
     * @param int idUserItem
     * @param string codeContent
     * @param string elementReplaced
     * @param string content
     */
    private static function _userShopItemCategoriesTab(int $idUserItem, string $codeContent, string $elementReplaced, string $contentPassed)
    {
        if(isset($idUserItem)){
            $idContent = Content::where('code', $codeContent)->first();
            if(isset($idContent)){
                $htmlContent = ContentLanguage::where('content_id', $idContent->id)->first()->content;
                
                //controllo per lo shortcode delle tab categories
                if(isset($htmlContent)){
                    $finalHtml = '';
                    $cont = 0;
                    //$contItem = 0;
                    //$tags = SelenaViewsBL::getTagByType('ShopItemDetail');
                    //$idContentItemDetail = Content::where('code', 'ShopItemDetail')->first();
                    //$htmlContentItemDetail = ContentLanguage::where('content_id', $idContentItemDetail->id)->first()->content;
                    if (strpos($htmlContent, '#categoriestab#') !== false) {
                        $htmlContentTabs = '<ul class="nav nav-tabs" id="myTab1">';
                        $htmlContentTabs = $htmlContentTabs . '<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#p' . $cont . '" id="tab-' . $cont . '" data-id-category="">Generale</a></li>';
                        foreach (DB::select(
                            'SELECT distinct categories_languages.description, categories_father.category_father_id from categories
                            inner join categories_father on categories.id = categories_father.category_id
                            inner join categories_languages on categories_languages.category_id = categories_father.category_father_id
                            inner join categories_items on categories_items.category_id = categories.id
                            inner join items on categories_items.item_id = items.id
                            where items.created_id = :idUserItem',
                            ['idUserItem' => $idUserItem]
                        ) as $categories){
                            $cont = $cont + 1;
                            if($cont == 1){
                                $htmlContentTabs = $htmlContentTabs . '<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#p' . $cont . '" id="tab-' . $cont . '" data-id-category="' . $categories->category_father_id . '">' . $categories->description . '</a></li>';
                            }else{
                                $htmlContentTabs = $htmlContentTabs . '<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#p' . $cont . '" id="tab-' . $cont . '" data-id-category="' . $categories->category_father_id . '">' . $categories->description . '</a></li>';
                            }
                            
                        };
                        $htmlContentTabs = $htmlContentTabs . '</ul>';
                            
                        $finalHtml = str_replace('#categoriestab#', $htmlContentTabs, $htmlContent);                        
                    }
                }
            }
        }
        return str_replace($elementReplaced, $finalHtml, $contentPassed);
    }

    #endregion PRIVATE


    #region GET

    /**
     * Get content
     *
     * @param Int idContent
     * @param Int idLanguageContent
     * @param Int idFunctionality
     * @param Int idUser
     * @param Int idLanguage
     * @param string $contentCode
     *
     * @return String
     */
    public static function getByCodeAndLanguage(int $idContent, int $idLanguageContent, int $idFunctionality = null, int $idUser = null, int $idLanguage = null)
    {
        if (!is_null($idFunctionality)) {
            FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
        }

        $content = '';
        $contentLanguage = ContentLanguage::where('content_id', $idContent)->where('language_id', $idLanguageContent)->first();
        $dtoContentLanguage = new DtoContentLanguages();
        if (is_null($contentLanguage)) {
            $language = LanguageBL::getDefault();
            if ($language->id != $idLanguageContent) {
                $contentLanguage = ContentLanguage::where('content_id', $idContent)->where('language_id', $language->id)->first();
                if (!is_null($contentLanguage)) {
                    $content = $contentLanguage->content;
                    $dtoContentLanguage->content = $contentLanguage->content;
                    $dtoContentLanguage->description = $contentLanguage->description;
                }
            }
        } else {
            $content = $contentLanguage->content;

            $dtoContentLanguage->content = $contentLanguage->content;
            $dtoContentLanguage->description = $contentLanguage->description;
        }
        //return $content;
        return $dtoContentLanguage;
    }

    /**
     * Get content
     *
     * @param Int idContent
     * @param Int idLanguage
     * @param Int idUser
     * @param string $contentCode
     *
     * @return String
     */
    public static function getByCodeUserAndLanguage(int $idContent, int $idLanguageContent, $idUser, $cartId, $url, $salesmanId)
    {
        $content = '';
        $contentLanguage = ContentLanguage::where('content_id', $idContent)->where('language_id', $idLanguageContent)->first();

        if (is_null($contentLanguage)) {
            $language = LanguageBL::getDefault();
            if ($language->id != $idLanguageContent) {
                $contentLanguage = ContentLanguage::where('content_id', $idContent)->where('language_id', $language->id)->first();
                if (!is_null($contentLanguage)) {
                    $content = $contentLanguage->content;
                }
            }
        } else {
            $content = $contentLanguage->content;
        }

        /* replace shortcode header */
        if (strpos($content, '#user#') !== false) {
            if(!is_null($idUser)){
                $tags = SelenaViewsBL::getTagByType('UserShortcodeHeaderLoggedIn');
                $idContentLoggedIn = Content::where('code', 'UserShortcodeHeaderLoggedIn')->first();
                $contentLanguageLoggedIn = ContentLanguage::where('content_id', $idContentLoggedIn->id)->where('language_id', $idLanguageContent)->first();

                $htmlItemTabs = "";

                foreach (DB::select(
                    'SELECT users_datas.*
                    from users_datas 
                    where users_datas.user_id = :idUser',
                    ['idUser' => $idUser]
                ) as $userdetail){
                    $newContent = $contentLanguageLoggedIn->content;
                    foreach ($tags as $fieldsDb){
                        if(isset($fieldsDb->tag)){
                            if (strpos($newContent, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($userdetail->{$cleanTag})){
                                    $tag = str_replace('.',',',substr($userdetail->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $userdetail->{$cleanTag};
                                }
                                $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);
                            }
                        }
                    }
                    $htmlItemTabs = $htmlItemTabs . $newContent;
                };
                $content = str_replace('#user#', $htmlItemTabs,  $content);
            }else{
                $idContentNotLogged = Content::where('code', 'UserShortcodeHeaderLoggedOut')->first();
                $contentLanguageNotLogged = ContentLanguage::where('content_id', $idContentNotLogged->id)->where('language_id', $idLanguageContent)->first();
                if(isset($contentLanguageNotLogged)){
                    $content = str_replace('#user#', $contentLanguageNotLogged->content,  $content);
                }else{
                    $content = '';
                }
                
            }
        }

        /* replace shortcode header salesman */
        if (strpos($content, '#salesman#') !== false) {
            if(!is_null($salesmanId) && $salesmanId != ''){
                $tags = SelenaViewsBL::getTagByType('SalesmanShortcodeHeaderLoggedIn');
                $idContentLoggedIn = Content::where('code', 'SalesmanShortcodeHeaderLoggedIn')->first();
                $contentLanguageLoggedIn = ContentLanguage::where('content_id', $idContentLoggedIn->id)->where('language_id', $idLanguageContent)->first();

                $htmlItemTabs = "";

                foreach (DB::select(
                    'SELECT salesman.*
                    from salesman 
                    where salesman.id = :idSalesman',
                    ['idSalesman' => $salesmanId]
                ) as $userdetail){
                    $newContent = $contentLanguageLoggedIn->content;
                    foreach ($tags as $fieldsDb){
                        if(isset($fieldsDb->tag)){
                            if (strpos($newContent, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($userdetail->{$cleanTag})){
                                    $tag = str_replace('.',',',substr($userdetail->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $userdetail->{$cleanTag};
                                }
                                $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);
                            }
                        }
                    }
                    $htmlItemTabs = $htmlItemTabs . $newContent;
                };
                $content = str_replace('#salesman#', $htmlItemTabs,  $content);
            }else{
                $idContentNotLogged = Content::where('code', 'SalesmanShortcodeHeaderLoggedOut')->first();
                $contentLanguageNotLogged = ContentLanguage::where('content_id', $idContentNotLogged->id)->where('language_id', $idLanguageContent)->first();
                if(isset($contentLanguageNotLogged)){
                    $content = str_replace('#salesman#', $contentLanguageNotLogged->content,  $content);
                }else{
                    $content = '';
                }
                
            }
        }

        if (strpos($content, '#menu#') !== false) {
            $listPage = PageBL::getMenuList($idLanguageContent, 'Page');
            $html = "";
            $htmlNew = "";
            
            if(isset($listPage)){
                foreach($listPage->menuList as $listPageMenu){
                    if($listPageMenu->online){
                        //controllare se l'if si scrive così - controllare se array subMenu è valorizzato
                        if(count($listPageMenu->subMenu)>0){
                            $html = $html . '<li class="nav-item dropdown">';
                            $html = $html . '<a class="nav-link dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" href="' . $listPageMenu->url . '" id="navbarDropdownMenuLink' . $listPageMenu->id . '" role="button" aria-haspopup="true" aria-expanded="false">';
                            $html = $html . '<span>' . $listPageMenu->title . '</span>';
                            $html = $html . '</a>';
                            //$html = $html . '<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink' . $listPageMenu->id . '">';
                            $html = $html . '<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink' . $listPageMenu->id . '">';
                            $html = $html . static::_renderSubMenu($listPageMenu->subMenu);
                            $html = $html . '</ul>';
                            //$html = $html .'</div>';
                            $html = $html . '</li>';
                        }else{
                            $html = $html . '<li class="nav-item">';
                            $html = $html . '<a class="nav-link" href="' . $listPageMenu->url . '">';
                            $html = $html . "<span>" . $listPageMenu->title . "</span>";
                            $html = $html . "</a>";
                            $html = $html . "</li>";
                        }
                    }
                }
            }
            if($html != ""){
                $htmlNew = '<nav class="navbar navbar-expand-lg navbar-light bg-light"><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button><div class="collapse navbar-collapse" id="navbarNav"><ul class="navbar-nav">';
                $htmlNew = $htmlNew . $html;
                $htmlNew = $htmlNew . "</ul></div></nav>";
            }

            $content = str_replace('#menu#', $htmlNew,  $content);
        }

        /* burger menu - visualizzato su mobile */
        if (strpos($content, '#burgermenu#') !== false) {
            $listPage = PageBL::getMenuList($idLanguageContent, 'Page');
            $html = "";
            $htmlNew = "";
            
            if(isset($listPage)){
                foreach($listPage->menuList as $listPageMenu){
                    if($listPageMenu->online){
                        //controllare se l'if si scrive così - controllare se array subMenu è valorizzato
                        if(count($listPageMenu->subMenu)>0){
                            $html = $html . '<a class="burger-menu-a" href="' . $listPageMenu->url . '"><li class="burger-menu-li">' . $listPageMenu->title . '</li></a>';
                            $html = $html . static::_renderSubBurgerMenu($listPageMenu->subMenu);
                        }else{
                            $html = $html . '<a class="burger-menu-a" href="' . $listPageMenu->url . '"><li class="burger-menu-li">' . $listPageMenu->title . '</li></a>';
                        }
                    }
                }
            }

            if($html != ""){
                $htmlNew = '<nav role="navigation"><div id="menuToggle"><input type="checkbox" /><span></span><span></span><span></span><ul id="menu">';
                $htmlNew = $htmlNew . $html;
                $htmlNew = $htmlNew . "</ul></div></nav>";

                $htmlNew = $htmlNew . '<style>#menuToggle{display: block;position: relative;top: 70px;left: 50px;z-index: 1;-webkit-user-select: none;user-select: none;}#menuToggle a{text-decoration: none;color: #232323;transition: color 0.3s ease;}#menuToggle a:hover{color: tomato;}#menuToggle input{display: block;width: 40px;height: 32px;position: absolute;top: -7px;left: -5px;cursor: pointer;opacity: 0;z-index: 2;-webkit-touch-callout: none;}#menuToggle span{display: block;width: 33px;height: 4px;margin-bottom: 5px;position: relative;background: #cdcdcd;border-radius: 3px;z-index: 1;transform-origin: 4px 0px;transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0),background 0.5s cubic-bezier(0.77,0.2,0.05,1.0),opacity 0.55s ease;}#menuToggle span:first-child{transform-origin: 0% 0%;}#menuToggle span:nth-last-child(2){transform-origin: 0% 100%;}#menuToggle input:checked ~ span{opacity: 1;transform: rotate(45deg) translate(-2px, -1px);background: #232323;}#menuToggle input:checked ~ span:nth-last-child(3){opacity: 0;transform: rotate(0deg) scale(0.2, 0.2);}#menuToggle input:checked ~ span:nth-last-child(2){transform: rotate(-45deg) translate(0, -1px);}#menu{position: absolute;width: 300px;margin: -100px 0 0 -50px;padding: 50px;padding-top: 125px;background: #ededed;list-style-type: none;-webkit-font-smoothing: antialiased;transform-origin: 0% 0%;transform: translate(-100%, 0);transition: transform 0.5s cubic-bezier(0.77,0.2,0.05,1.0);}#menu li{padding: 10px 0;font-size: 22px;}#menuToggle input:checked ~ ul{transform: none;}</style>';
            }

            $content = str_replace('#burgermenu#', $htmlNew,  $content);

        }
        /* end burger menu - visualizzato su mobile */

        /* banner advertising header */
        if (strpos($content, '#bannerheader#') !== false ) {
            $codeArea = 'header';
            $paginaVisualizzaa = $url;

            $bannerList = collect();
            $htmlFinal = '';

            //recupero l'area in base al codice
            $advertisingArea = AdvertisingArea::where('code', $codeArea)->where('active', 'true')->first();
            
            if (is_null($advertisingArea)) {
                $content = $content;
            } else {
                //cerco i banner attivi e con le date giuste
                
                foreach (DB::select(
                        'SELECT * 
                        from advertising_banners
                        where advertising_area_id = :area_id
                        and active = true
                        and weight > 0
                        and start_date <= now()
                        and (stop_date >= now() or stop_date is null)',
                    ['area_id' => $advertisingArea->id]
                    ) as $Banner) {
                    
                            $DtoAdvertisingBanner = new DtoAdvertisingBanner();
                            $DtoAdvertisingBanner->id = $Banner->id;
                            $DtoAdvertisingBanner->advertising_area_id = $Banner->advertising_area_id;
                            $DtoAdvertisingBanner->start_date = $Banner->start_date;
                            $DtoAdvertisingBanner->stop_date = $Banner->stop_date;
                            $DtoAdvertisingBanner->image_file = $Banner->image_file;
                            $DtoAdvertisingBanner->image_alt = $Banner->image_alt;
                            $DtoAdvertisingBanner->link_url = $Banner->link_url;
                            $DtoAdvertisingBanner->target_blank = $Banner->target_blank;
                            $DtoAdvertisingBanner->active = $Banner->active;
                            $DtoAdvertisingBanner->script_Js = $Banner->script_Js;
                            $DtoAdvertisingBanner->maximum_impressions = $Banner->maximum_impressions;
                            $DtoAdvertisingBanner->progressive_impressions = $Banner->progressive_impressions;
                            $DtoAdvertisingBanner->weight = $Banner->weight;
                            $DtoAdvertisingBanner->user_id = $Banner->user_id;
                            $DtoAdvertisingBanner->active_reports = $Banner->active_reports;
                            $DtoAdvertisingBanner->description = $Banner->description; 
                            
                            $bannerList->push($DtoAdvertisingBanner);
                }

                // ho un array con i banner disponibili per quell'area. Ora devo scegliere quale far
                // vedere in base al suo peso. Mi calcolo un numero CASUALE da 0 a 100
                if ($bannerList->count()==0){
                    
                    $content = $content;

                } else {
                    $pesoCasuale = rand(1, 100);
                    $pesoProvvisorio = 0;
                    //ora cerco il banner in base al peso casuale e al peso del banner
                    $trovato = False;
                    foreach($bannerList as $banner){
                        $pesoProvvisorio = $pesoProvvisorio+$banner->weight;
                        if ($pesoProvvisorio>=$pesoCasuale){
                            $trovato = True;

                            if ($banner->script_Js<>'') {
                                //semplicemente ritorno lo script JS (tipo adsense)
                                $htmlFinal = $banner->script_Js;
                            }else{
                                //restituiscol'immagine del banner
                                if (isset($banner->link_url)){

                                    $htmlFinal = '<a href="'.$banner->link_url;
                                    $htmlFinal = $htmlFinal.'"';
                                    if ($banner->target_blank = True)
                                        $htmlFinal = $htmlFinal.' target="_blank"';
                                    $htmlFinal = $htmlFinal.'>';
                                }

                                $htmlFinal = $htmlFinal.'<img border="0" alt="'.$banner->image_alt.'" src="'.$banner->image_file.'">';

                                if (isset($banner->link_url)){
                                    $htmlFinal = $htmlFinal.'</a>';
                                }
                            }

                            //aumento le impression progressive e, se c'è il tracking, memorizzo tutto
                            $bannerVisualizzato = AdvertisingBanner::where('id', $banner->id)->get()->first();                            
                            $bannerVisualizzato->progressive_impressions += 1;
                            if ($bannerVisualizzato->maximum_impressions>0 && $bannerVisualizzato->progressive_impressions>=$bannerVisualizzato->maximum_impressions)
                                $bannerVisualizzato->active = False;
                            $bannerVisualizzato->save();

                            //verifico se devo tenere il 
                            if ($bannerVisualizzato->active_reports){
                                $report = new AdvertisingBannerReport();
                                $report->advertising_banner_id = $banner->id;
                                $report->impression_date = now();
                                $report->page = $url;
                                $report->source_ip = '';
                                $report->save();
                            }
                            break;
                        }
                    }

                    if ($trovato = False)
                        $content = $content;
                    else
                        $content = str_replace('#bannerheader#', $htmlFinal,  $content);

                }
            }
            
        }
        /* end banner advertising header */

        /* banner advertising footer */
        if (strpos($content, '#bannerfooter#') !== false) {
            $codeArea = 'footer';
            $paginaVisualizzaa = $url;

            $bannerList = collect();
            $htmlFinal = '';

            //recupero l'area in base al codice
            $advertisingArea = AdvertisingArea::where('code', $codeArea)->where('active', 'true')->first();
            
            if (is_null($advertisingArea)) {
                $content = $content;
            } else {
                //cerco i banner attivi e con le date giuste
                
                foreach (DB::select(
                        'SELECT * 
                        from advertising_banners
                        where advertising_area_id = :area_id
                        and active = true
                        and weight > 0
                        and start_date <= now()
                        and (stop_date >= now() or stop_date is null)',
                    ['area_id' => $advertisingArea->id]
                    ) as $Banner) {
                    
                            $DtoAdvertisingBanner = new DtoAdvertisingBanner();
                            $DtoAdvertisingBanner->id = $Banner->id;
                            $DtoAdvertisingBanner->advertising_area_id = $Banner->advertising_area_id;
                            $DtoAdvertisingBanner->start_date = $Banner->start_date;
                            $DtoAdvertisingBanner->stop_date = $Banner->stop_date;
                            $DtoAdvertisingBanner->image_file = $Banner->image_file;
                            $DtoAdvertisingBanner->image_alt = $Banner->image_alt;
                            $DtoAdvertisingBanner->link_url = $Banner->link_url;
                            $DtoAdvertisingBanner->target_blank = $Banner->target_blank;
                            $DtoAdvertisingBanner->active = $Banner->active;
                            $DtoAdvertisingBanner->script_Js = $Banner->script_Js;
                            $DtoAdvertisingBanner->maximum_impressions = $Banner->maximum_impressions;
                            $DtoAdvertisingBanner->progressive_impressions = $Banner->progressive_impressions;
                            $DtoAdvertisingBanner->weight = $Banner->weight;
                            $DtoAdvertisingBanner->user_id = $Banner->user_id;
                            $DtoAdvertisingBanner->active_reports = $Banner->active_reports;
                            $DtoAdvertisingBanner->description = $Banner->description; 
                            
                            $bannerList->push($DtoAdvertisingBanner);
                }

                // ho un array con i banner disponibili per quell'area. Ora devo scegliere quale far
                // vedere in base al suo peso. Mi calcolo un numero CASUALE da 0 a 100
                if ($bannerList->count()==0){
                    
                    $content = $content;

                } else {
                    $pesoCasuale = rand(1, 100);
                    $pesoProvvisorio = 0;
                    //ora cerco il banner in base al peso casuale e al peso del banner
                    $trovato = False;
                    foreach($bannerList as $banner){
                        $pesoProvvisorio = $pesoProvvisorio+$banner->weight;
                        if ($pesoProvvisorio>=$pesoCasuale){
                            $trovato = True;

                            if ($banner->script_Js<>'') {
                                //semplicemente ritorno lo script JS (tipo adsense)
                                $htmlFinal = $banner->script_Js;
                            }else{
                                //restituiscol'immagine del banner
                                if (isset($banner->link_url)){

                                    $htmlFinal = '<a href="'.$banner->link_url;
                                    $htmlFinal = $htmlFinal.'"';
                                    if ($banner->target_blank = True)
                                        $htmlFinal = $htmlFinal.' target="_blank"';
                                    $htmlFinal = $htmlFinal.'>';
                                }

                                $htmlFinal = $htmlFinal.'<img border="0" alt="'.$banner->image_alt.'" src="'.$banner->image_file.'">';

                                if (isset($banner->link_url)){
                                    $htmlFinal = $htmlFinal.'</a>';
                                }
                            }

                            //aumento le impression progressive e, se c'è il tracking, memorizzo tutto
                            $bannerVisualizzato = AdvertisingBanner::where('id', $banner->id)->get()->first();                            
                            $bannerVisualizzato->progressive_impressions += 1;
                            if ($bannerVisualizzato->maximum_impressions>0 && $bannerVisualizzato->progressive_impressions>=$bannerVisualizzato->maximum_impressions)
                                $bannerVisualizzato->active = False;
                            $bannerVisualizzato->save();

                            //verifico se devo tenere il log
                            if ($bannerVisualizzato->active_reports){
                                $report = new AdvertisingBannerReport();
                                $report->advertising_banner_id = $banner->id;
                                $report->impression_date = now();
                                $report->page = $url;
                                $report->source_ip = '';
                                $report->save();
                            }
                            break;
                        }
                    }

                    if ($trovato = False)
                        $content = $content;
                    else
                        $content = str_replace('#bannerfooter#', $htmlFinal,  $content);

                }
            }
            
        }
        /* end banner advertising footer */

        if (strpos($content, '#totItem#') !== false) {
            $cont = DB::table('items')->where('online', true)->count();
            $content = str_replace('#totItem#', $cont,  $content);
        }


        if(!is_null($cartId)){
            if (strpos($content, '#totItemCart#') !== false) {
                $cont = DB::table('carts_details')->where('cart_id', $cartId)->count();
            
                $content = str_replace('#totItemCart#', $cont,  $content);
            }
        }else{
            $content = str_replace('#totItemCart#', '0',  $content);
        }
        /* end replace shortcode header */

        return $content;
    }



    /**
     * html item for user shop by filter and category id
     *
     * @param string $idCategory
     * @param string $idUserItem
     *
     * @return String
     */
    public static function UserShopByIdCategory(string $idCategory, string $idUserItem, string $description,  string $priceFrom,  string $priceTo,  string $condition, string $producer, string $pageSelected){
        $tags = SelenaViewsBL::getTagByType('ShopItemDetail');
        $idContentItemDetail = Content::where('code', 'ShopItemDetail')->first();
        $htmlContentItemDetail = ContentLanguage::where('content_id', $idContentItemDetail->id)->first()->content;

        $strWhere = "";
        $strWhereCategory = "";
        $itemsPerPage =  SettingBL::getByCode(SettingEnum::NumberOfItemsPerPage);
        $strSqlPagination = "";

        if($description!=""){
            $strWhere = $strWhere . ' AND items_languages.description ilike \'%' . $description . '%\''; 
        }

        if($priceFrom!=""){
            $strWhere = $strWhere . ' AND items.price >= ' . $priceFrom; 
        }

        if($priceTo!=""){
            $strWhere = $strWhere . ' AND items.price <= ' . $priceTo; 
        }

        if($condition!=""){
            $strWhere = $strWhere . ' AND items.new = \'' . $condition . '\''; 
        }

        if($producer!=""){
            $strWhere = $strWhere . ' AND items.producer_id = \'' . $producer . '\''; 
        }

        if($idCategory!=""){
            $strWhereCategory = $strWhereCategory . ' AND categories_languages.category_id = ' . $idCategory; 
        }

        $htmlItemTabs = "";

        
        if($idCategory!=""){
            foreach (DB::select(
                'SELECT distinct categories_languages.category_id from categories
                inner join categories_father on categories.id = categories_father.category_id
                inner join categories_languages on categories_languages.category_id = categories_father.category_father_id
                inner join categories_items on categories_items.category_id = categories.id
                inner join items on categories_items.item_id = items.id
                where items.online = true and items.created_id = :idUserItem ' . $strWhereCategory,
                ['idUserItem' => $idUserItem]
            ) as $categoriesFather){

                $totItems = DB::select(
                    'SELECT count(items.id) as cont
                    from items 
                    inner join categories_items on categories_items.item_id = items.id
                    inner join categories on categories.id = categories_items.category_id
                    inner join categories_father on categories.id = categories_father.category_id
                    inner join items_languages ON items.id = items_languages.item_id
                    where categories_father.category_father_id = :idCategoryFather
                    and items.online = true and items.created_id = :idUserItem ' . $strWhere,
                    ['idUserItem' => $idUserItem, 'idCategoryFather' => $categoriesFather->category_id]);
                
                if($totItems[0]->cont <= $itemsPerPage){
                    $strSqlPagination = "";
                }else{
                    $limit = $itemsPerPage;
                    $offset = $itemsPerPage * ($pageSelected - 1);

                    $strSqlPagination = " limit " . $limit . " offset " . $offset;
                }

                foreach (DB::select(
                    'SELECT items.*, items_languages.*
                    from items 
                    inner join categories_items on categories_items.item_id = items.id
                    inner join categories on categories.id = categories_items.category_id
                    inner join categories_father on categories.id = categories_father.category_id
                    inner join items_languages ON items.id = items_languages.item_id
                    where categories_father.category_father_id = :idCategoryFather
                    and items.online = true and items.created_id = :idUserItem ' . $strWhere . ' ' . $strSqlPagination,
                    ['idUserItem' => $idUserItem, 'idCategoryFather' => $categoriesFather->category_id]
                ) as $itemdetail){
                    $newContent = $htmlContentItemDetail;
                    foreach ($tags as $fieldsDb){
                        if(isset($fieldsDb->tag)){
                            if (strpos($newContent, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($itemdetail->{$cleanTag})){
                                    $tag = str_replace('.',',',substr($itemdetail->{$cleanTag}, 0, -2));
                                }else{
                                    $tag = $itemdetail->{$cleanTag};
                                }
                                $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);
                            }
                        }
                    }
                    $user = UsersDatas::where("user_id", $itemdetail->created_id)->get()->first();
                    if(isset($user)){
                        if (strpos($newContent, '#userInfo#') !== false) {
                            $newContent = str_replace('#userInfo#', $user->business_name,  $newContent);
                        }
                    }
                    $htmlItemTabs = $htmlItemTabs . $newContent;
                };
            };
        }else{
            $totItems = DB::select(
                'SELECT count(items.id) as cont
                from items
                inner join items_languages ON items.id = items_languages.item_id
                and items.online = true and items.created_id = :idUserItem ' . $strWhere,
                ['idUserItem' => $idUserItem]);
            
            if($totItems[0]->cont <= $itemsPerPage){
                $strSqlPagination = "";
            }else{
                $limit = $itemsPerPage;
                $offset = $itemsPerPage * ($pageSelected - 1);
                $strSqlPagination = " limit " . $limit . " offset " . $offset;
            }
            

            foreach (DB::select(
                'SELECT items.*, items_languages.*
                from items
                inner join items_languages ON items.id = items_languages.item_id
                and items.online = true and items.created_id = :idUserItem ' . $strWhere . ' ' . $strSqlPagination,
                ['idUserItem' => $idUserItem]
            ) as $itemdetail){
                $newContent = $htmlContentItemDetail;
                foreach ($tags as $fieldsDb){
                    if(isset($fieldsDb->tag)){
                        if (strpos($newContent, $fieldsDb->tag) !== false) {
                            $cleanTag = str_replace('#', '', $fieldsDb->tag);
                            if(is_numeric($itemdetail->{$cleanTag})){
                                $tag = str_replace('.',',',substr($itemdetail->{$cleanTag}, 0, -2));
                            }else{
                                $tag = $itemdetail->{$cleanTag};
                            }
                            $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);
                        }
                    }
                }
                $user = UsersDatas::where("user_id", $itemdetail->created_id)->get()->first();
                if(isset($user)){
                    if (strpos($newContent, '#userInfo#') !== false) {
                        $newContent = str_replace('#userInfo#', $user->business_name,  $newContent);
                    }
                }
                $htmlItemTabs = $htmlItemTabs . $newContent;
            };
        }
        $dataReturn = new DtoPagination();
        $dataReturn->pageSelected = $pageSelected;
        $dataReturn->totItems = $totItems[0]->cont;
        $dataReturn->itemsPerPage = $itemsPerPage;
        if($totItems[0]->cont <= $itemsPerPage){
            $dataReturn->paginationActive = false;
            $dataReturn->totalPages = "1";
        }else{
            $dataReturn->paginationActive = true;
            $dataReturn->totalPages = ceil($totItems[0]->cont / $itemsPerPage);
        }
        $dataReturn->html = $htmlItemTabs;
        
        return $dataReturn;
    }

   /**
     * 
     *
     * @param string 
     * @param string 
     *
     * @return String
     */
    public static function ItemCategoryByNotIdCategory(string $idCategory, string $priceFrom,  string $priceTo,  string $condition, string $producer, string $order, string $pageSelected, string $description){
   
        $tags = SelenaViewsBL::getTagByType('Items');
        $idContentItemDetail = Content::where('code', 'Items')->first();
        $htmlContentItemDetail = ContentLanguage::where('content_id', $idContentItemDetail->id)->first()->content;

        $strWhere = "";
        $strOrder = "";
        $strWhereCategory = "";
        $itemsPerPage =  SettingBL::getByCode(SettingEnum::NumberOfItemsPerPage);
        $strSqlPagination = "";
        

       /*  if($description!=""){
            $strWhere = $strWhere . ' AND items_languages.description ilike \'%' . $description . '%\'';  
        }   */

        if($priceFrom!=""){
            $strWhere = $strWhere . ' AND items.price >= ' . $priceFrom; 
        }

        if($priceTo!=""){
            $strWhere = $strWhere . ' AND items.price <= ' . $priceTo; 
        }

        if($condition!=""){
            $strWhere = $strWhere . ' AND items.new = \'' . $condition . '\''; 
        }

        if($producer!=""){
            $strWhere = $strWhere . ' AND items.producer_id = \'' . $producer . '\''; 
            $joinProducer = ' left join producer ON items.producer_id = producer.id ';
        }else{
            $joinProducer = '';
        }

        if($order!=""){
            $strOrder = " ORDER BY " . $order;
        }else{
            $strOrder = " order by items_languages.description ";
        }

        $htmlItemTabs = "";
                
        if($idCategory ==""){
            $totItems = DB::select(
                'SELECT count(items.id) as cont
                from items 
                inner join items_languages ON items.id = items_languages.item_id
                ' . $joinProducer . '
                where items_languages.description ilike \'%' . $description . '%\' ' . $strWhere);
            
            if($totItems[0]->cont <= $itemsPerPage){
                $strSqlPagination = "";
            }else{
                $limit = $itemsPerPage;
                $offset = $itemsPerPage * ($pageSelected - 1);

                $strSqlPagination = " limit " . $limit . " offset " . $offset;
            }

            foreach (DB::select(
                'SELECT items.*, items_languages.*
                from items 
                inner join items_languages ON items.id = items_languages.item_id
                ' . $joinProducer . '
                and items_languages.description ilike \'%' . $description . '%\' ' . $strWhere . $strOrder . $strSqlPagination,  
            ) as $itemdetail){
                $newContent = $htmlContentItemDetail;
                foreach ($tags as $fieldsDb){
                    if(isset($fieldsDb->tag)){
                        if (strpos($newContent, $fieldsDb->tag) !== false) {
                            $cleanTag = str_replace('#', '', $fieldsDb->tag);
                            if(is_numeric($itemdetail->{$cleanTag}) && $fieldsDb->tag != '#internal_code#'){
                                $tag = str_replace('.',',',substr($itemdetail->{$cleanTag}, 0, -2));
                            }else{
                                $tag = $itemdetail->{$cleanTag};
                            }
                            $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);
                        }
                    }
                }
                $user = UsersDatas::where("user_id", $itemdetail->created_id)->get()->first();
                if(isset($user)){
                    
                    if (strpos($newContent, '#userInfo#') !== false) {
                        $newContent = str_replace('#userInfo#', $user->business_name,  $newContent);
                    }
                }
                $htmlItemTabs = $htmlItemTabs . $newContent;
               
            };     
        }
    
        $dataReturn = new DtoPagination();
        $dataReturn->pageSelected = $pageSelected;
        $dataReturn->totItems = $totItems[0]->cont;
        $dataReturn->itemsPerPage = $itemsPerPage;
        if($totItems[0]->cont <= $itemsPerPage){
            $dataReturn->paginationActive = false;
            $dataReturn->totalPages = "1";
        }else{
            $dataReturn->paginationActive = true;
            $dataReturn->totalPages = ceil($totItems[0]->cont / $itemsPerPage);
        }
        $dataReturn->html = $htmlItemTabs;
        
        return $dataReturn;
    }



    /**
     * html item for categories pages by filter and category id
     *
     * @param string $idCategory
     * @param string $idUserItem
     *
     * @return String
     */
    public static function ItemCategoryByIdCategory(string $idCategory, string $priceFrom,  string $priceTo,  string $condition, string $producer, string $order, string $pageSelected, string $description){
        $tags = SelenaViewsBL::getTagByType('Items');
        $idContentItemDetail = Content::where('code', 'Items')->first();
        $htmlContentItemDetail = ContentLanguage::where('content_id', $idContentItemDetail->id)->first()->content;

        $strWhere = "";
        $strOrder = "";
        $strWhereCategory = "";
        $itemsPerPage =  SettingBL::getByCode(SettingEnum::NumberOfItemsPerPage);
        $strSqlPagination = "";
        

         if($description!=""){
            $strWhere = $strWhere . ' AND items_languages.description ilike \'%' . $description . '%\'';  
        }   

        if($priceFrom!=""){
            $strWhere = $strWhere . ' AND items.price >= ' . $priceFrom; 
        }

        if($priceTo!=""){
            $strWhere = $strWhere . ' AND items.price <= ' . $priceTo; 
        }

        if($condition!=""){
            $strWhere = $strWhere . ' AND items.new = \'' . $condition . '\''; 
        }

        if($producer!=""){
            $strWhere = $strWhere . ' AND items.producer_id = \'' . $producer . '\''; 
            $joinProducer = ' left join producer ON items.producer_id = producer.id ';
        }else{
            $joinProducer = '';
        }

        if($order!=""){
            $strOrder = " ORDER BY " . $order;
        }else{
            $strOrder = " order by items.internal_code desc, categories_items.order ";
        }

        $htmlItemTabs = "";
                
        if($idCategory!=""){
            $totItems = DB::select(
                'SELECT count(items.id) as cont
                from items 
                inner join categories_items on categories_items.item_id = items.id
                inner join categories on categories.id = categories_items.category_id
                inner join items_languages ON items.id = items_languages.item_id
                ' . $joinProducer . '
                and categories_items.category_id = :idCategory ' . $strWhere ,
                ['idCategory' => $idCategory]);
            
            if($totItems[0]->cont <= $itemsPerPage){
                $strSqlPagination = "";
            }else{
                $limit = $itemsPerPage;
                $offset = $itemsPerPage * ($pageSelected - 1);

                $strSqlPagination = " limit " . $limit . " offset " . $offset;
            }

            foreach (DB::select(
                'SELECT items.*, items_languages.*
                from items 
                inner join categories_items on categories_items.item_id = items.id
                inner join categories on categories.id = categories_items.category_id
                inner join items_languages ON items.id = items_languages.item_id
                ' . $joinProducer . '
                and categories_items.category_id = :idCategory ' . $strWhere . $strOrder . $strSqlPagination,
                ['idCategory' => $idCategory]
            ) as $itemdetail){
                $newContent = $htmlContentItemDetail;
                foreach ($tags as $fieldsDb){
                    if(isset($fieldsDb->tag)){
                        if (strpos($newContent, $fieldsDb->tag) !== false) {
                            $cleanTag = str_replace('#', '', $fieldsDb->tag);
                            if(is_numeric($itemdetail->{$cleanTag}) && $fieldsDb->tag != '#internal_code#'){
                                $tag = str_replace('.',',',substr($itemdetail->{$cleanTag}, 0, -2));
                            }else{
                                $tag = $itemdetail->{$cleanTag};
                            }
                            $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);
                        }
                    }
                }
                $user = UsersDatas::where("user_id", $itemdetail->created_id)->get()->first();
                if(isset($user)){
                    
                    if (strpos($newContent, '#userInfo#') !== false) {
                        $newContent = str_replace('#userInfo#', $user->business_name,  $newContent);
                    }
                }
                $htmlItemTabs = $htmlItemTabs . $newContent;
            };
            
        }
        
        $dataReturn = new DtoPagination();
        $dataReturn->pageSelected = $pageSelected;
        $dataReturn->totItems = $totItems[0]->cont;
        $dataReturn->itemsPerPage = $itemsPerPage;
        if($totItems[0]->cont <= $itemsPerPage){
            $dataReturn->paginationActive = false;
            $dataReturn->totalPages = "1";
        }else{
            $dataReturn->paginationActive = true;
            $dataReturn->totalPages = ceil($totItems[0]->cont / $itemsPerPage);
        }
        $dataReturn->html = $htmlItemTabs;
        
        return $dataReturn;
    }


     /**
     * 
     * @param int CartId
     * @param string codeContent
     * @param string elementReplaced
     * @param string content
     */
    private static function _cartDetailOptional(int $CartId,string $codeContent, string $elementReplaced, string $contentPassed)
    {
        $CartDetailOptionals = CartDetailOptional::where('cart_detail_id', $CartId);
        
        if(isset($CartDetailOptionals)){
            $idContent = Content::where('code', $codeContent)->first();
            if(isset($idContent)){
                $htmlContent = ContentLanguage::where('content_id', $idContent->id)->first()->content;
                if(isset($htmlContent)){
                    $finalHtml = '';
                    $newContent  = '';
                    foreach (DB::select(
                        'SELECT *
                        FROM cart_detail_optional
                        WHERE cart_detail_optional.cart_detail_id = :CartId',
                        ['CartId' => $CartId]
                    ) as $CartDetailOptional){
                        $newContent = $htmlContent;
                        if (strpos($newContent, '#description_id_optionalPrint#') !== false) {
                            $Description_optionalPrint = OptionalLanguages::where('optional_id', $CartDetailOptional->optional_id)->get()->first();
                            if (isset($Description_optionalPrint)){
                            $newContent = str_replace('#description_id_optionalPrint#', $Description_optionalPrint->description,  $newContent); 
                            }else{
                                $newContent = str_replace('#description_id_optionalPrint#', '',  $newContent);           
                            }
                        }

                        if (strpos($newContent, '#title_tipology_optional#') !== false) {
                            $Title_Tipology_Optional = OptionalTypology::where('optional_id', $CartDetailOptional->optional_id)->get()->first();
                            $Title_Tipology_OptionalDescription = TypeOptionalLanguages::where('type_optional_id', $Title_Tipology_Optional->type_optional_id)->get()->first();

                            if (isset($Title_Tipology_OptionalDescription)){
                            $newContent = str_replace('#title_tipology_optional#', $Title_Tipology_OptionalDescription->description,  $newContent); 
                            }else{
                                $newContent = str_replace('#title_tipology_optional#', '',  $newContent);           
                            }
                        }

                        $finalHtml = $finalHtml . $newContent;
                    }

                }
                return str_replace($elementReplaced , $finalHtml,  $contentPassed); 
            }else{
                return "";
            }  
        }else{
            return "";
        }
    }




    /**
     * Get content
     *
     * @param Int idContent
     * @param Int idLanguageContent
     * @param String url
     * @param Int idFunctionality
     * @param Int idUser
     * @param Int idLanguage
     * @param string $contentCode
     *
     * @return String
     */
    public static function getByCodeRender(int $idContent, int $idLanguageContent, String $url, int $cart_id = null, int $idFunctionality = null, int $idUser = null, int $idLanguage = null, $param = null)
    {
        if (!is_null($idFunctionality)) {
            FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
        }    
        $content = '';
        $contentLanguage = ContentLanguage::where('content_id', $idContent)->where('language_id', $idLanguageContent)->first();
        $contentCode = Content::where('id', $idContent)->first()->code;
        
        if (is_null($contentLanguage)) {
            $language = LanguageBL::getDefault();
            if ($language->id != $idLanguageContent) {
                $contentLanguage = ContentLanguage::where('content_id', $idContent)->where('language_id', $language->id)->first();
                if (!is_null($contentLanguage)) {
                    $content = $contentLanguage->content;
                }
            }
        } else {

            //estrazione filtri categoria
            if($contentCode == 'CategoriesFilter'){

                $newContent = $contentLanguage->content;
                $finalHtml = '';
                $whereParamFilters = "";

                if($param != ""){
                    if(isset($param)){
                        foreach($param as $key=>$data){
                            if($data!=''){
                                $whereParamFilters = $whereParamFilters . " and items_technicals_specifications.item_id in (select items_technicals_specifications.item_id from items_technicals_specifications inner join languages_technicals_specifications on languages_technicals_specifications.technical_specification_id = items_technicals_specifications.technical_specification_id where items_technicals_specifications.value = '" . $data . "' and languages_technicals_specifications.description = '" . $key . "')";
                            }
                        }
                    }
                }

                $currentCategory = CategoryLanguage::where('meta_tag_link', $url)->where('language_id', $idLanguageContent)->first();
                //controllo che esista la categoria all'url navigato
                if(isset($currentCategory)){
                    $categoryItemOnDb = CategoryItem::where('category_id', $currentCategory->category_id)->get()->first();
                    //controllo che esistano articoli legati alla categoria      
                    if(isset($categoryItemOnDb)){
                        $categoryOnDb = Category::where('id', $currentCategory->category_id)->get()->first();
                        if(isset($categoryOnDb)){
                            $internalCodeCategoryFather = substr($categoryOnDb->code, 0, 2);
                            $categoryFinal = Category::where('code', '=', $internalCodeCategoryFather)->get()->first();
                            if(isset($categoryFinal)){
                                $categoryFilter = CategoriesFilterLanguages::where('category_id', $categoryFinal->id)->where('language_id', $idLanguageContent)->get();
                                if(isset($categoryFilter)){
                                    foreach ($categoryFilter as $categoryFilters){

                                        $totItems = DB::select(
                                            'SELECT distinct(items_technicals_specifications.value) from items_technicals_specifications
                                            inner join languages_technicals_specifications on items_technicals_specifications.technical_specification_id = languages_technicals_specifications.technical_specification_id
                                            inner join categories_items on items_technicals_specifications.item_id = categories_items.item_id
                                            inner join categories on categories.id = categories_items.category_id
                                            where languages_technicals_specifications.language_id = ' . $idLanguageContent . '
                                            and items_technicals_specifications.language_id = ' . $idLanguageContent . '
                                            and languages_technicals_specifications.description = \'' . $categoryFilters->description . '\' ' . $whereParamFilters . '
                                            and categories.code like \'%' . $internalCodeCategoryFather . '%\' and categories.id=' . $currentCategory->category_id . ' ORDER BY items_technicals_specifications.value');
                                        
                                        if(isset($totItems)){
                                            if(count($totItems) > 0){
                                                if(count($totItems) == 1){
                                                    $newContent = $contentLanguage->content;
                                                    $newContent = str_replace('#description#', $categoryFilters->description,  $newContent);
                                                    $htmlSelect = "";
                                                    $valueDefault = "";
                                                    if($param != ""){
                                                        if(isset($param)){
                                                            if(array_key_exists($categoryFilters->description, $param)){
                                                                $valueDefault = $param[$categoryFilters->description];
                                                            }
                                                        }
                                                    }
                                                    
                                                    // controllo attivazione filtro come su bormac

                                                    $htmlSelect = $htmlSelect . "<select class='form-control' id='" . $categoryFilters->id . "' name='" . $categoryFilters->description . "'>";
                                                    $htmlSelect = $htmlSelect . "<option value=''>-- Select --</option>";
                                                    foreach (DB::select(
                                                        'SELECT distinct(items_technicals_specifications.value) from items_technicals_specifications
                                                        inner join languages_technicals_specifications on items_technicals_specifications.technical_specification_id = languages_technicals_specifications.technical_specification_id
                                                        inner join categories_items on items_technicals_specifications.item_id = categories_items.item_id
                                                        inner join categories on categories.id = categories_items.category_id
                                                        where languages_technicals_specifications.language_id = ' . $idLanguageContent . '
                                                        and items_technicals_specifications.language_id = ' . $idLanguageContent . '
                                                        and languages_technicals_specifications.description = \'' . $categoryFilters->description . '\' ' . $whereParamFilters . '
                                                        and categories.code like \'%' . $internalCodeCategoryFather . '%\' and categories.id=' . $currentCategory->category_id . ' ORDER BY items_technicals_specifications.value') as $categoriesInput){  
                                                        if($categoriesInput->value == $valueDefault){
                                                            $valueSelected = true;
                                                            $valueSelectedInput = $categoriesInput->value;
                                                            $htmlSelect = $htmlSelect . "<option selected value='" . $categoriesInput->value . "'>" . $categoriesInput->value . "</option>";
                                                        }else{
                                                            $valueSelected = false;
                                                            $htmlSelect = $htmlSelect . "<option value='" . $categoriesInput->value . "'>" . $categoriesInput->value . "</option>";
                                                        }
                                                    }
                                                    $htmlSelect = $htmlSelect . "</select>";

                                                    if($valueSelected){
                                                        $htmlSelect = '<input type="hidden" value="' . $valueSelectedInput . '" id="' . $categoryFilters->id . '" name="' . $categoryFilters->description . '">' . $valueSelectedInput . '<i class="fa fa-times-circle" data-chiave="' . $categoryFilters->id . '"></i>';
                                                    }

                                                    $newContent = str_replace('#input_filter#', $htmlSelect,  $newContent);

                                                    $finalHtml = $finalHtml . $newContent;
                                                }else{
                                                    $newContent = $contentLanguage->content;
                                                    $newContent = str_replace('#description#', $categoryFilters->description,  $newContent);
                                                    $htmlSelect = "";
                                                    $valueDefault = "";
                                                    if($param != ""){
                                                        if(isset($param)){
                                                            if(array_key_exists($categoryFilters->description, $param)){
                                                                $valueDefault = $param[$categoryFilters->description];
                                                            }
                                                        }
                                                    }

                                                    $htmlSelect = $htmlSelect . "<select class='form-control' id='" . $categoryFilters->id . "' name='" . $categoryFilters->description . "'>";
                                                    $htmlSelect = $htmlSelect . "<option value=''>-- Select --</option>";
                                                    foreach (DB::select(
                                                        'SELECT distinct(items_technicals_specifications.value) from items_technicals_specifications
                                                        inner join languages_technicals_specifications on items_technicals_specifications.technical_specification_id = languages_technicals_specifications.technical_specification_id
                                                        inner join categories_items on items_technicals_specifications.item_id = categories_items.item_id
                                                        inner join categories on categories.id = categories_items.category_id
                                                        where languages_technicals_specifications.language_id = ' . $idLanguageContent . '
                                                        and items_technicals_specifications.language_id = ' . $idLanguageContent . '
                                                        and languages_technicals_specifications.description = \'' . $categoryFilters->description . '\' ' . $whereParamFilters . '
                                                        and categories.code like \'%' . $internalCodeCategoryFather . '%\' and categories.id=' . $currentCategory->category_id . ' ORDER BY items_technicals_specifications.value') as $categoriesInput){  
                                                        if($categoriesInput->value == $valueDefault){
                                                            $htmlSelect = $htmlSelect . "<option selected value='" . $categoriesInput->value . "'>" . $categoriesInput->value . "</option>";
                                                        }else{
                                                            $htmlSelect = $htmlSelect . "<option value='" . $categoriesInput->value . "'>" . $categoriesInput->value . "</option>";
                                                        }
                                                    }
                                                    $htmlSelect = $htmlSelect . "</select>";

                                                    $newContent = str_replace('#input_filter#', $htmlSelect,  $newContent);

                                                    $finalHtml = $finalHtml . $newContent;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else{
                            $finalHtml = 'internal filter setting';
                        }
                    }
                }

                $content = $finalHtml;
            }



            if($contentCode == 'Categories'){
                $currentCategory = CategoryLanguage::where('meta_tag_link', $url)->first();
                if(isset($currentCategory)){
                    $categoryFather = $currentCategory->category_id;
                    $titleCategories = $currentCategory->description;
                    $strWhere = " AND categories_father.category_father_id = " . $categoryFather;
                }else{
                    $titleCategories = "";
                    $strWhere = " AND categories_father.category_father_id IS NULL";
                }
                $finalHtml = '';
                $tags = SelenaViewsBL::getTagByType('Categories');
                foreach (DB::select(
                    'SELECT *
                    FROM categories
                    INNER JOIN categories_languages ON categories.id = categories_languages.category_id
                    INNER JOIN categories_father ON categories_father.category_id = categories_languages.category_id
                    AND categories.online = true
                    AND categories_languages.language_id = :idLanguage ' . $strWhere . '  ORDER BY categories_father.order asc',
                    ['idLanguage' => $idLanguageContent]
                ) as $categories){
                    $newContent = $contentLanguage->content;
                    foreach ($tags as $fieldsDb){
                        if(isset($fieldsDb->tag)){
                            if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                $newContent = str_replace($fieldsDb->tag , $categories->{$cleanTag},  $newContent);
                            }
                        }
                    }
                    $finalHtml = $finalHtml . $newContent;
                };
                
                $content = $finalHtml;

            }

            if($contentCode == 'Cart_Detail_Single_Row'){                    
                
                $finalHtml = '';
                $tags = SelenaViewsBL::getTagByType('Cart_Detail_Single_Row');                    

                foreach (DB::select(
                    'SELECT carts_details.*, items.img, items.internal_code, items.price
                    FROM carts_details 
                    inner join items on items.id = carts_details.item_id
                    WHERE cart_id = :cart_id
                    ORDER BY Nr_Row', ['cart_id' => $cart_id] 
                ) as $cartdetail){
                    $newContent = $contentLanguage->content;                        
                    foreach ($tags as $fieldsDb){  
                        if(isset($fieldsDb->tag)){                                                                 
                            if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($cartdetail->{$cleanTag}) && strpos($cartdetail->{$cleanTag},'.')){
                                    
                                    if($cleanTag == 'discount_percentage'){
                                        $tag = str_replace('.',',', $cartdetail->{$cleanTag}) . ' %';
                                    }else{
                                        if($cleanTag == 'discount_value'){
                                            $tag = str_replace('.',',', $cartdetail->{$cleanTag}) . ' €';
                                        }else{
                                            $tag = str_replace('.',',',substr($cartdetail->{$cleanTag}, 0, -2));
                                        }
                                    }
                                }else{
                                    if($cleanTag == 'discount_percentage'){
                                        $tag = $cartdetail->{$cleanTag} . ' %';
                                    }else{
                                        if($cleanTag == 'discount_value'){
                                            $tag = $cartdetail->{$cleanTag} . ' €';
                                        }else{
                                            $tag = $cartdetail->{$cleanTag};
                                        }
                                    }
                                }
                                $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                               
                            }
                        }
                    }
                    if (strpos($newContent, '#support_id_description#') !== false) {
                        if (isset($cartdetail->support_id)){
                            $DescriptionSupport = Supports::where('id', $cartdetail->support_id)->first()->description;
                        }else{
                            $DescriptionSupport = ''; 
                        }

                        
                        if (isset($DescriptionSupport)){
                            $newContent = str_replace('#support_id_description#', $DescriptionSupport, $newContent); 
                        }else{
                            $newContent = str_replace('#support_id_description#', '',  $newContent);           
                        }
                    }

                    if (strpos($newContent, '#cart_detail_optional#') !== false) {
                        $newContent = static::_cartDetailOptional($cartdetail->id, 'CartDetailOptional', '#cart_detail_optional#', $newContent);  
                    }
                    
                    $newContent = str_replace('#img#' , $cartdetail->img,  $newContent);
                    $newContent = str_replace('#price#' , str_replace('.',',',substr($cartdetail->price, 0, -2)),  $newContent);
                    $newContent = str_replace('#internal_code#' , $cartdetail->internal_code,  $newContent);
                    
                    $finalHtml = $finalHtml . $newContent;
                };
                $content = $finalHtml; 

            }

            if($contentCode == 'CartUsersAddresses'){                    
                
                $finalHtml = '';
                $cart = Cart::where('id', $cart_id)->get()->first();
                if(isset($cart)){
                    $idAddress = $cart->user_address_goods_id;
                    
                    if(is_null($idAddress) || $idAddress == ''){
                        $tags = SelenaViewsBL::getTagByType('UsersDetail');


                        foreach (DB::select(
                            'SELECT *
                        FROM users_datas 
                        WHERE user_id = :id', 
                        ['id' => $cart->user_id]
                        ) as $cartdetail){
            
                            $newContent = $contentLanguage->content;                        
                            
                            foreach ($tags as $fieldsDb){
                                
                                if(isset($fieldsDb->tag)){                                                                 
                                    if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                        
                                        if(is_numeric($cartdetail->{$cleanTag}) && strpos($cartdetail->{$cleanTag},'.')){
                                            $tag = str_replace('.',',',substr($cartdetail->{$cleanTag}, 0, -2));
                                        }else{
                                            $tag = $cartdetail->{$cleanTag};
                                        }
                                        $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                           
                                    }
                                }
                            }

                               if (strpos($newContent, '#description_province#') !== false) {
                        $DescriptionProvince = Provinces::where('id', $cartdetail->province_id)->first();
                        if (isset($DescriptionProvince)){
                        $newContent = str_replace('#description_province#', $DescriptionProvince->abbreviation,  $newContent); 
                        }else{
                            $newContent = str_replace('#description_province#', '',  $newContent);           
                        }
                    }
                    if (strpos($newContent, '#description_nation#') !== false) {
                        $DescriptionNation = LanguageNation::where('nation_id', $cartdetail->nation_id)->first();
                        if (isset($DescriptionNation)){
                        $newContent = str_replace('#description_nation#', $DescriptionNation->description,  $newContent); 
                        }else{
                            $newContent = str_replace('#description_nation#', '',  $newContent);           
                        }
                    }

                            $newContent = str_replace('#locality#' , $cartdetail->country,  $newContent);
                            
                            $finalHtml = $finalHtml . $newContent;
                        };
                        
                        $content = $finalHtml;
                    }else{
                        $tags = SelenaViewsBL::getTagByType('CartUsersAddresses');

                        foreach (DB::select(
                            'SELECT *
                            FROM users_addresses 
                            WHERE id = ' . $idAddress
                            ) as $cartdetail){
                            $newContent = $contentLanguage->content;                        
                            
                            foreach ($tags as $fieldsDb){
                                
                                if(isset($fieldsDb->tag)){                                                                 
                                    if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                        if(is_numeric($cartdetail->{$cleanTag}) && strpos($cartdetail->{$cleanTag},'.')){
                                            $tag = str_replace('.',',',substr($cartdetail->{$cleanTag}, 0, -2));
                                        }else{
                                            $tag = $cartdetail->{$cleanTag};
                                        }
                                        $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                               
                                    }
                                }
                            }

                        if (strpos($newContent, '#description_province#') !== false) {
                        $DescriptionProvince = Provinces::where('id', $cartdetail->province_id)->first();
                        if (isset($DescriptionProvince)){
                        $newContent = str_replace('#description_province#', $DescriptionProvince->abbreviation,  $newContent); 
                        }else{
                            $newContent = str_replace('#description_province#', '',  $newContent);           
                        }
                    }
                    if (strpos($newContent, '#description_nation#') !== false) {
                        $DescriptionNation = LanguageNation::where('nation_id', $cartdetail->nation_id)->first();
                        if (isset($DescriptionNation)){
                        $newContent = str_replace('#description_nation#', $DescriptionNation->description,  $newContent); 
                        }else{
                            $newContent = str_replace('#description_nation#', '',  $newContent);           
                        }
                    }
                            
                            $finalHtml = $finalHtml . $newContent;
                        };
                        
                        $content = $finalHtml;


                    }
                }
            }

            if($contentCode == 'Cart_Summary'){                    
                
                $finalHtml = '';
                $tags = SelenaViewsBL::getTagByType('carts');                    
             
                foreach (DB::select(
                    'SELECT *
                    FROM carts 
                    WHERE id = :cart_id'
                    , ['cart_id' => $cart_id]
                    
                ) as $cartsummary){

                    $newContent = $contentLanguage->content;                        
                    
                    foreach ($tags as $fieldsDb){
                        
                        if(isset($fieldsDb->tag)){                                                                 
                            if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                
                                if(is_numeric($cartsummary->{$cleanTag})){
                                    if ($cleanTag=="id")
                                        $tag = (int)$cartsummary->{$cleanTag};
                                    else
                                        $tag = number_format($cartsummary->{$cleanTag}, 2); 
                                }else{
                                    $tag = $cartsummary->{$cleanTag};
                                    
                                }
                                
                                $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                               
                            }
                        }
                    }
                    
                    $finalHtml = $finalHtml . $newContent;
                };

                $content = $finalHtml;                  
            }
        
            if($contentCode == 'Cart_Widget'){                    
                $content =  $contentLanguage->content;    
            }

            if($contentCode == 'HeaderArticle'){
                $finalHtml = '';
                $tags = SelenaViewsBL::getTag('View_BlogNews');  
                
                if(strpos($url, '_preview') !== false){
                    $url = str_replace('_preview', '', $url);
                }else{
                    $url = $url;
                }
                
                foreach (DB::select(
                    'SELECT pages.id as pageid, pages.online, pages.visible_from, pages.page_type, 
                    languages_pages.title as titolo, languages_pages."content", languages_pages.url, 
                    languages_pages.seo_title as titoloseo, languages_pages.seo_description as descrizioneseo,
                    languages_pages.share_title as titoloshare, languages_pages.share_description as descrizioneshare,
                    languages_pages.url_cover_image,
                    users.name as Autore, languages_page_categories.description as categoria, pages.publish as publish
                    FROM pages
                    INNER JOIN languages_pages on pages.id = languages_pages.page_id and languages_pages.language_id = :idLanguage
                    left outer join users on pages.author_id = users.id 
                    left outer join languages_page_categories on languages_page_categories.page_category_id = pages.page_category_id  and languages_page_categories.language_id = :idLanguage
                    where languages_pages.url = :urlPagina'
                    , ['urlPagina' => $url, 'idLanguage' => $idLanguageContent]
                ) as $articleBlogNews){
                    
                    $newContent = $contentLanguage->content;                        
                    $htmlTypeCategory = '';
                    foreach ($tags as $fieldsDb){
                   
                        if(isset($fieldsDb->tag)){                                                                 
                            if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                if(is_numeric($articleBlogNews->{$cleanTag}) && strpos($articleBlogNews->{$cleanTag},'.')){
                                    $tag = str_replace('.',',',substr($articleBlogNews->{$cleanTag}, 0, -2));
                                }else{
                                    
                                    if($fieldsDb->tag == '#categoria#'){
                                        $pagePageCategory = PagePageCategory::where('page_id', $articleBlogNews->pageid)->get();
                                        if(isset($pagePageCategory)){
                                            foreach($pagePageCategory as $pageCategorys){
                                                $languagePageCategory = LanguagePageCategory::where('page_category_id', $pageCategorys->page_category_id)->get()->first();
                                                if(isset($languagePageCategory)){
                                                    $linkPageCategory = LinkPageCategory::where('page_category_id', $pageCategorys->page_category_id)->get()->first();
                                                    if(isset($linkPageCategory)){
                                                        $htmlTypeCategory = $htmlTypeCategory . '<label><a href="' . $linkPageCategory->url . '">' . $languagePageCategory->description . '</a></label>';
                                                    }else{
                                                        $htmlTypeCategory = $htmlTypeCategory . '<label>' . $languagePageCategory->description . '</label>';
                                                    }
                                                }
                                            }
                                        }else{
                                            $tag = $articleBlogNews->{$cleanTag};
                                        }
                                    }else{
                                        if($fieldsDb->tag == '#publish#'){
                                            $tag = date_format(date_create($articleBlogNews->{$cleanTag}), 'd/m/Y');
                                        }else{
                                            $tag = $articleBlogNews->{$cleanTag};
                                        }
                                        
                                    }
                                    
                                }
                                
                                if($fieldsDb->tag == '#categoria#'){
                                    $newContent = str_replace($fieldsDb->tag , $htmlTypeCategory,  $newContent);
                                }else{
                                    $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);
                                }  
                                
                                
                            }
                        }
                    }
                    $finalHtml = $finalHtml . $newContent;
                };

                $content = $finalHtml; 
                return $content;

            }

            //gestione articoli preferiti
            if($contentCode == 'FavoritesUserItems'){
                $finalHtml = '';
                $currentItemFavoritesUser = ItemFavoritesUser::where('user_id', $idUser)->get()->first();
                if(isset($currentItemFavoritesUser)){
                    $tags = SelenaViewsBL::getTagByType('Items');

                    foreach (DB::select(
                        'SELECT *
                        FROM items
                        INNER JOIN item_favorites_user ON items.id = item_favorites_user.item_id
                        INNER JOIN items_languages ON items.id = items_languages.item_id
                        AND items_languages.language_id = :idLanguage
                        AND item_favorites_user.user_id = :userId',
                        ['idLanguage' => $idLanguageContent, 'userId' => $idUser]
                    ) as $items){
                        $newContent = $contentLanguage->content;
                        foreach ($tags as $fieldsDb){
                            if(isset($fieldsDb->tag)){
                                if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                    $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                    if(is_numeric($items->{$cleanTag})){ 
                                        // Calcola prezzo (regola listino/prezzo dedicato a utente)
                                        $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                    }else{
                                        
                                        $tag = $items->{$cleanTag};
                                    }
                                    if($fieldsDb->tag == '#item_id#'){
                                        $newContent = str_replace($fieldsDb->tag , $items->item_id ,  $newContent);
                                    }else{
                                        $newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                                    }
                                                                        
                                    //$newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                                }
                            }
                            
                        }
                        $itemId = $items->item_id;
                        //$idUser variabile prelevata dalla funzione
                        if(is_null($idUser) || $idUser == ''){
                            if (strpos($newContent, '#classFavoritesItem#') !== false) {
                                $newContent = str_replace('#classFavoritesItem#', 'fa-heart-o',  $newContent);
                            }
                        }else{
                            if (strpos($newContent, '#classFavoritesItem#') !== false) {
                                
                                $favoriteItemUserOnDb = ItemFavoritesUser::where('user_id', $idUser)->where('item_id', $itemId)->get()->first();
                                if(isset($favoriteItemUserOnDb)){
                                    $newContent = str_replace('#classFavoritesItem#', 'fa-heart',  $newContent);
                                }else{
                                    $newContent = str_replace('#classFavoritesItem#', 'fa-heart-o',  $newContent);
                                }
                            }
                        }
                        $finalHtml = $finalHtml . $newContent;
                    };  
                    $content = $finalHtml;
                }else{
                    $content = "Nessun articolo presente!";
                }          
            }
        
            //gestione articoli
            if($contentCode == 'Items'){
                $whereParamFilters = "";

                if($param != ""){
                    if(isset($param)){
                        foreach($param as $key=>$data){
                            if($data!=''){
                                $whereParamFilters = $whereParamFilters . " and items.id in (select items_technicals_specifications.item_id from items_technicals_specifications inner join languages_technicals_specifications on languages_technicals_specifications.technical_specification_id = items_technicals_specifications.technical_specification_id where items_technicals_specifications.value = '" . $data . "' and languages_technicals_specifications.description = '" . $key . "')";
                            }
                        }
                    }
                }

                $currentCategory = CategoryLanguage::where('meta_tag_link', $url)->first();
                if(isset($currentCategory)){
                    $finalHtml = '';
                    $tags = SelenaViewsBL::getTagByType('Items');
                    $itemsPerPage =  SettingBL::getByCode(SettingEnum::NumberOfItemsPerPage);
                    $strSqlPagination = "";
                    $pageSelected = 1;

                    $totItems = DB::select(
                        'SELECT count(items.id) as cont
                        FROM items
                        INNER JOIN items_languages ON items.id = items_languages.item_id
                        INNER JOIN categories_items ON categories_items.item_id = items.id
                        AND items_languages.language_id = :idLanguage
                        AND items.online = true
                        ' . $whereParamFilters . '
                        AND categories_items.category_id = :idCategory',
                        ['idLanguage' => $idLanguageContent, 'idCategory' => $currentCategory->category_id]);
                    
                    if($totItems[0]->cont <= $itemsPerPage){
                        $strSqlPagination = "";
                    }else{
                        $limit = $itemsPerPage;
                        $offset = $itemsPerPage * ($pageSelected - 1);
        
                        $strSqlPagination = " limit " . $limit . " offset " . $offset;
                    }

                    foreach (DB::select(
                        'SELECT *
                        FROM items
                        INNER JOIN items_languages ON items.id = items_languages.item_id
                        INNER JOIN categories_items ON categories_items.item_id = items.id
                        AND items_languages.language_id = :idLanguage
                        AND items.online = true
                        ' . $whereParamFilters . '
                        AND categories_items.category_id = :idCategory order by items.internal_code desc, categories_items.order asc ' . $strSqlPagination,
                        ['idLanguage' => $idLanguageContent, 'idCategory' => $currentCategory->category_id]
                    ) as $items){
                        $newContent = $contentLanguage->content;
                        foreach ($tags as $fieldsDb){
                            if(isset($fieldsDb->tag)){
                                if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                    $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                    if(is_numeric($items->{$cleanTag}) && $fieldsDb->tag != '#internal_code#' && $fieldsDb->tag != '#producer_id#'){ 
                                        // Calcola prezzo (regola listino/prezzo dedicato a utente)
                                        if($fieldsDb->tag == '#price#'){
                                            //prelievo campo nelle impostazioni per metodologia di visualizzazione, le opzioni sono NESSUNO, BARRATO, DEDICATO
                                            $settingDisplayPrice = SettingBL::getByCode(SettingEnum::DisplayPrice);
                                            if($idUser == null || $idUser == ''){
                                                $userId = '';
                                            }else{
                                                $userId = $idUser;
                                            }
                                            //creazione funzione per prelievo prezzo scontato, nelle regole del carrello

                                            switch ($settingDisplayPrice) {
                                                case "NESSUNO":
                                                    //se NESSUNO stampo il prezzo in items
                                                    $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                                    break;
                                                case "BARRATO":
                                                    
                                                    //se BARRATO aggiungo l'html <strike> al prezzo normale e appendo il prezzo calcolato
                                                    $priceCalculated = CartBL::getPriceCalculated($items->item_id, $userId);
                                                    
                                                    if($priceCalculated != ""){                                                        
                                                        $tag = '<strike>' . str_replace('.',',',substr($items->{$cleanTag}, 0, -2)) . ' € </strike><br> ' . str_replace('.','',number_format($priceCalculated,2,",","."));
                                                    }else{
                                                        $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                                    }
                                                    break;
                                                case "DEDICATO":
                                                    //se DEDICATO stampo il prezzo calcolato
                                                    $priceCalculated = CartBL::getPriceCalculated($items->item_id, $userId);
                                                    if($priceCalculated != ""){
                                                        $tag = str_replace('.',',',substr($priceCalculated, 0, -1));
                                                    }else{
                                                        $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                                    }
                                                    break;
                                                default:
                                                    $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                            }
                                            //$tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                        }else{
                                            $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                        }
                                    }else{
                                        $tag = $items->{$cleanTag};
                                    }
                                    if($fieldsDb->tag == '#item_id#'){
                                        $newContent = str_replace($fieldsDb->tag , $items->item_id ,  $newContent);
                                    }else{
                                        $newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                                    }
                                    if (strpos($newContent, '#producer#') !== false) {
                                        if(!is_null($items->producer_id) || $items->producer_id !=''){
                                            $producerItem = Producer::where('id', $items->producer_id)->get()->first();
                                            if(isset($producerItem)){
                                                $newContent = str_replace('#producer#' , $producerItem->business_name ,  $newContent);
                                            }else{
                                                $newContent = str_replace('#producer#' , '-',  $newContent);
                                            }
                                        }else{
                                            $newContent = str_replace('#producer#' , '-',  $newContent);
                                        }
                                    }
                                                                        
                                    //$newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                                }
                            }
                            
                        }
                        $itemId = $items->item_id;
                        //$idUser variabile prelevata dalla funzione
                        if(is_null($idUser) || $idUser == ''){
                            if (strpos($newContent, '#classFavoritesItem#') !== false) {
                                $newContent = str_replace('#classFavoritesItem#', 'fa-heart-o',  $newContent);
                            }
                        }else{
                            if (strpos($newContent, '#classFavoritesItem#') !== false) {
                                
                                $favoriteItemUserOnDb = ItemFavoritesUser::where('user_id', $idUser)->where('item_id', $itemId)->get()->first();
                                if(isset($favoriteItemUserOnDb)){
                                    $newContent = str_replace('#classFavoritesItem#', 'fa-heart',  $newContent);
                                }else{
                                    $newContent = str_replace('#classFavoritesItem#', 'fa-heart-o',  $newContent);
                                }
                            }
                        }

                        //$idUser variabile risettata per estrazione utente che ha caricato l'articolo
                        $idUserItem = Item::where('id', $itemId)->get()->first();
                        if(isset($idUserItem)){
                            $user = UsersDatas::where("user_id", $idUserItem->created_id)->get()->first();
                            if(isset($user)){
                                if (strpos($newContent, '#userInfo#') !== false) {
                                    $newContent = str_replace('#userInfo#', $user->business_name,  $newContent);
                                }
                            }
                        }                      
                        $finalHtml = $finalHtml . $newContent;
                    };
                    $contentDto = new DtoPagination();
                    $contentDto->pageSelected = $pageSelected;
                    $contentDto->totItems = $totItems[0]->cont;
                    $contentDto->itemsPerPage = $itemsPerPage;

                    if($totItems[0]->cont <= $itemsPerPage){
                        $contentDto->paginationActive = false;
                        $contentDto->totalPages = "1";
                    }else{
                        $contentDto->paginationActive = true;
                        $contentDto->totalPages = ceil($totItems[0]->cont / $itemsPerPage);
                    }

                    $contentDto->html = $finalHtml;
                    $content = $contentDto;
                }else{
                    $contentDto = new DtoPagination();
                    $contentDto->html = '';
                    $content = $contentDto;
                }
            }

            /* GroupTechnicalsSpecifications */
            if($contentCode == 'GroupTechnicalsSpecifications'){
                $currentItem = ItemLanguage::where('link', $url)->first();
                if(isset($currentItem)){
                    $finalHtml = '';
                    foreach (DB::select(
                        'select distinct groups_technicals_specifications_technicals_specifications.group_technical_specification_id 
                        from groups_technicals_specifications_technicals_specifications
                        where groups_technicals_specifications_technicals_specifications.technical_specification_id in (
                        select languages_technicals_specifications.technical_specification_id
                        from languages_technicals_specifications
                        inner join items_technicals_specifications on items_technicals_specifications.technical_specification_id = languages_technicals_specifications.technical_specification_id
                        where items_technicals_specifications.item_id = :idItem AND items_technicals_specifications.language_id = :idLanguage)
                        order by groups_technicals_specifications_technicals_specifications.group_technical_specification_id',
                        ['idLanguage' => $idLanguageContent, 'idItem' => $currentItem->item_id]
                    ) as $groupTechnicalSpecification){
                        $newContent = $contentLanguage->content;
                        $groupTechnicalSpecificationLanguages = GroupTechnicalSpecificationLanguage::where('group_technical_specification_id', $groupTechnicalSpecification->group_technical_specification_id)->where('language_id', $idLanguageContent)->get()->first();
                        if(isset($groupTechnicalSpecificationLanguages)){
                            $newContent = str_replace('#description#' , $groupTechnicalSpecificationLanguages->description ,  $newContent);
                        }else{
                            $newContent = str_replace('#description#' , '' ,  $newContent);
                        }

                        if (strpos($newContent, '#item_group_technicals_pecifications#') !== false) {
                            $contentItemTechnicalSpecifications = Content::where('code', 'ItemGroupTechnicalsSpecifications')->first();
                            if(isset($contentItemTechnicalSpecifications)){
                                $contentItemTechnicalSpecificationsLanguage = ContentLanguage::where('content_id', $contentItemTechnicalSpecifications->id)->where('language_id', $idLanguageContent)->first();
                                if(isset($contentItemTechnicalSpecificationsLanguage)){
                                    $finalHtmlItemTechnicalSpecifications = "";
                                    foreach (DB::select(
                                        'select languages_technicals_specifications.description, items_technicals_specifications.value  
                                        from languages_technicals_specifications
                                        inner join items_technicals_specifications on items_technicals_specifications.technical_specification_id = languages_technicals_specifications.technical_specification_id
                                        inner join groups_technicals_specifications_technicals_specifications on groups_technicals_specifications_technicals_specifications.technical_specification_id = languages_technicals_specifications.technical_specification_id
                                        where items_technicals_specifications.item_id = :idItem AND items_technicals_specifications.language_id = :idLanguage
                                        and groups_technicals_specifications_technicals_specifications.group_technical_specification_id = :idGroupTechnicalSpecification',
                                        ['idLanguage' => $idLanguageContent, 'idItem' => $currentItem->item_id, 'idGroupTechnicalSpecification' => $groupTechnicalSpecification->group_technical_specification_id]
                                    ) as $itemGroupTechnicalSpecification){
                                        $newContentItemTechnicalSpecification = $contentItemTechnicalSpecificationsLanguage->content;
                                        $newContentItemTechnicalSpecification = str_replace('#description#' , $itemGroupTechnicalSpecification->description ,  $newContentItemTechnicalSpecification);
                                        $newContentItemTechnicalSpecification = str_replace('#value#' , $itemGroupTechnicalSpecification->value ,  $newContentItemTechnicalSpecification);

                                        $finalHtmlItemTechnicalSpecifications = $finalHtmlItemTechnicalSpecifications . $newContentItemTechnicalSpecification;
                                    }
                                }else{
                                    $newContent = str_replace('#item_group_technicals_pecifications#' , '' ,  $newContent);
                                }
                            }else{
                                $newContent = str_replace('#item_group_technicals_pecifications#' , '' ,  $newContent); 
                            }
                            $newContent = str_replace('#item_group_technicals_pecifications#' , $finalHtmlItemTechnicalSpecifications ,  $newContent); 
                            
                        }

                        $finalHtml = $finalHtml . $newContent;
                    };

                    $content = $finalHtml;
                }else{
                    $content = 'Nessuna specifica tecnica presente';
                }
                return $content;
            }
            /* end  GroupTechnicalsSpecifications*/

            /* ItemAttachment */
            if($contentCode == 'ItemAttachmentType'){
                $currentItem = ItemLanguage::where('link', $url)->first();
                if(isset($currentItem)){
                    $finalHtml = '';
                    $strSqlCategory = '';

                    $isSettingProtected = SettingBL::getByCode('ProtectFile');

                    if($isSettingProtected == 'True'){
                        $strUserLogged = ' AND item_attachment_type.protected = false ';

                        if(!is_null($idUser) && $idUser != ''){
                            $roleUser = DB::select('SELECT role_id from role_user where user_id = ' . $idUser . ' limit 1');
                            
                            $order = DB::select ('SELECT * from sales_orders where user_id = ' . $idUser . 'order by date_order desc limit 1' ); //' and (CURRENT_DATE - date_order::date) < 90 order by date_order desc limit 1');
                        
                           //if(isset($order[0])){
                             $strUserLogged = '';
                           // }
                    
                        }
                    }else{
                        $strUserLogged = '';
                    }


                    $categoryItemOnDb = CategoryItem::where('item_id', $currentItem->item_id)->get()->first();
                    if(isset($categoryItemOnDb)){
                        $strSqlCategory = "or
                        item_attachment_type.id in (
                            select item_attachment_type_id from item_attachment_file 
                            inner join item_attachment_language_file_category_table on item_attachment_language_file_category_table.item_attachment_file_id = item_attachment_file.id
                            inner join categories_items on categories_items.category_id = item_attachment_language_file_category_table.category_id
                            inner join items on items.id = categories_items.item_id
                            where categories_items.category_id = " . $categoryItemOnDb->category_id . $strUserLogged . "
                        )";
                    }

                    foreach (DB::select(
                        'select distinct item_attachment_type.id, item_attachment_type.order from 
                        item_attachment_type
                        inner join item_attachment_language_type_table on item_attachment_language_type_table.item_attachment_type_id = item_attachment_type.id
                        where item_attachment_father_type_id is null ' . $strUserLogged . $strUserLogged . '
                        and
                        (
                        item_attachment_type.id in (
                            select item_attachment_type_id from item_attachment_file 
                            inner join item_attachment_language_file_item_table on item_attachment_language_file_item_table.item_attachment_file_id = item_attachment_file.id
                            inner join items on items.id = item_attachment_language_file_item_table.item_id
                            inner join items_languages on items_languages.item_id = items.id
                            where items_languages.item_id = :idItem and items_languages.language_id = :idLanguage
                        )' . $strSqlCategory . ') order by item_attachment_type.order',
                        ['idLanguage' => $idLanguageContent, 'idItem' => $currentItem->item_id]
                    ) as $itemAttachamentType){
                    
                        $newContent = $contentLanguage->content;
                        $itemAttachmentLanguageType = ItemAttachmentLanguageTypeTable::where('item_attachment_type_id', $itemAttachamentType->id)->get()->first();
                        if(isset($itemAttachmentLanguageType)){
                            $newContent = str_replace('#description#' , $itemAttachmentLanguageType->description ,  $newContent);
                        }else{
                            $newContent = str_replace('#description#' , '' ,  $newContent);
                        }

                        $itemAttachmentType = ItemAttachmentType::where('id', $itemAttachamentType->id)->get()->first();
                        if(isset($itemAttachmentType)){
                            $newContent = str_replace('#code#' , $itemAttachmentType->description ,  $newContent);
                        }else{
                            $newContent = str_replace('#code#' , '' ,  $newContent);
                        }

                        if (strpos($newContent, '#item_attachment_file#') !== false) {
                            //ciclo file per ogni tipologia sopra $itemAttachamentType->id
                            $contentItemAttachmentFile = Content::where('code', 'ItemAttachmentFile')->first();
                            if(isset($contentItemAttachmentFile)){
                                $contentItemAttachmentFileLanguage = ContentLanguage::where('content_id', $contentItemAttachmentFile->id)->where('language_id', $idLanguageContent)->first();
                                if(isset($contentItemAttachmentFileLanguage)){
                                    $finalHtmlItemTechnicalSpecifications = "";
                                    $strSqlCategory = '';
                                    $categoryItemOnDb = CategoryItem::where('item_id', $currentItem->item_id)->get()->first();
                                    if(isset($categoryItemOnDb)){
                                        $strSqlCategory = "or
                                        item_attachment_file.id in (
                                            select distinct item_attachment_file.id from item_attachment_file 
                                            inner join item_attachment_language_file_category_table on item_attachment_language_file_category_table.item_attachment_file_id = item_attachment_file.id
                                            inner join categories_items on categories_items.category_id = item_attachment_language_file_category_table.category_id
                                            inner join items on items.id = categories_items.item_id
                                            where categories_items.category_id = " . $categoryItemOnDb->category_id . "
                                        )";
                                    }

                                    foreach (DB::select(
                                        'select distinct item_attachment_file.id from 
                                        item_attachment_file
                                        inner join item_attachment_type on item_attachment_type.id = item_attachment_file.item_attachment_type_id
                                        where item_attachment_type.id = ' . $itemAttachamentType->id . $strUserLogged . ' and  
                                        (
                                        item_attachment_file.id in (
                                            select item_attachment_file.id from item_attachment_file 
                                            inner join item_attachment_language_file_item_table on item_attachment_language_file_item_table.item_attachment_file_id = item_attachment_file.id
                                            inner join items on items.id = item_attachment_language_file_item_table.item_id
                                            inner join items_languages on items_languages.item_id = items.id
                                            where items_languages.item_id = :idItem and items_languages.language_id = :idLanguage
                                        )' . $strSqlCategory . ')',
                                        ['idLanguage' => $idLanguageContent, 'idItem' => $currentItem->item_id]
                                    ) as $itemAttachmentFile){
                                        $newitemAttachmentFile = $contentItemAttachmentFileLanguage->content;

                                        $itemAttachmentLanguageFile= ItemAttachmentLanguageFileTable::where('item_attachment_file_id', $itemAttachmentFile->id)->where('language_id', $idLanguageContent)->get()->first();
                                        if(isset($itemAttachmentLanguageFile)){

                                            if($itemAttachmentLanguageFile->url != '' && !is_null($itemAttachmentLanguageFile->url)){
                                                if (strpos($itemAttachmentLanguageFile->url, '/storage/files/') !== false) {
                                                    $fileSearch = str_replace('/storage/files/', static::$dirImageFiles,  $itemAttachmentLanguageFile->url);
                                                    if (file_exists($fileSearch)) {
                                                        $newitemAttachmentFile = str_replace('#description#' , $itemAttachmentLanguageFile->description ,  $newitemAttachmentFile);
                                                        $newitemAttachmentFile = str_replace('#url#' , $itemAttachmentLanguageFile->url ,  $newitemAttachmentFile);
                                                        $finalHtmlItemTechnicalSpecifications = $finalHtmlItemTechnicalSpecifications . $newitemAttachmentFile;
                                                    }
                                                }else{
                                                    $newitemAttachmentFile = str_replace('#description#' , $itemAttachmentLanguageFile->description ,  $newitemAttachmentFile);
                                                    $newitemAttachmentFile = str_replace('#url#' , $itemAttachmentLanguageFile->url ,  $newitemAttachmentFile);
                                                    $finalHtmlItemTechnicalSpecifications = $finalHtmlItemTechnicalSpecifications . $newitemAttachmentFile;
                                                }
                                            }
                                        }
                                    }
                                }else{
                                    $newContent = str_replace('#item_attachment_file#' , '' ,  $newContent);
                                }
                            }else{
                                $newContent = str_replace('#item_attachment_file#' , '' ,  $newContent); 
                            }

                            $newContent = str_replace('#item_attachment_file#' , $finalHtmlItemTechnicalSpecifications ,  $newContent); 
                        }

                        $finalHtml = $finalHtml . $newContent;
                    }                                        

                    $content = $finalHtml;
                }else{
                    $content = 'Nessuna file trovato';
                }
            }
            /* end ItemAttachment */

            //gestione shop detail
            if($contentCode == 'User_Profile_Page'){
                
                $currentItem = UsersDatas::where('link', $url)->first();
                
                $currentUserItem = $currentItem->user_id;
                $nameRoleDescr = "";

                if(isset($currentItem)){
                    $roleUser = RoleUser::where('user_id', $currentUserItem)->get()->first();
                    if(isset($roleUser)){
                        $nameRole = Role::where('id', $roleUser->role_id)->get()->first();
                        if(isset($nameRole)){
                            $nameRoleDescr = $nameRole->name;
                        }
                    }

                    $finalHtml = '';
                    $tags = SelenaViewsBL::getTagByType('UsersDetail');
                     
                    foreach (DB::select(
                        'SELECT users_datas.*, users.email
                        FROM users_datas
                        INNER JOIN users ON users.id = users_datas.user_id
                        AND users_datas.language_id = :idLanguage 
                        AND users_datas.user_id = :idUser',
                        ['idLanguage' => $idLanguageContent, 'idUser' => $currentItem->user_id]
                    ) as $usersdatas){
                        $newContent = $contentLanguage->content;
                        
                        foreach ($tags as $fieldsDb){
                            if(isset($fieldsDb->tag)){
                                if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                    $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                    if(is_numeric($usersdatas->{$cleanTag})){
                                        if (strpos($finalHtml, '#telephone_number#') !== false) {
                                            $tag = str_replace('.',',',substr($usersdatas->{$cleanTag}, 0, -2));
                                        }else{
                                            $tag = $usersdatas->{$cleanTag};
                                        }
                                    }else{
                                        if($fieldsDb->tag == '#img#'){
                                            if(is_null($usersdatas->{$cleanTag})){
                                                $tag = '/storage/images/default-user-icon.jpeg';
                                            }else{
                                                $tag = $usersdatas->{$cleanTag};
                                            }
                                        }else{
                                            if($fieldsDb->tag == '#img_cover#'){
                                                if(is_null($usersdatas->{$cleanTag})){
                                                    $tag = '/storage/images/default-cover.jpg';
                                                }else{
                                                    $tag = $usersdatas->{$cleanTag};
                                                }
                                            }else{
                                                $tag = $usersdatas->{$cleanTag};
                                            }
                                        }
                                    }          
                                    
                                    $newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                                }
                            }
                        }
                        $socialTMP = '<div class="user-social-icons">';
                        if (strpos($newContent, '#social#') !== false) {
                            if(isset($usersdatas->website))
                                $socialTMP = $socialTMP . '<a href="' . $usersdatas->website . '" target="_blank"><i class="fa fa-globe"></i></a>'; 
                            if(isset($usersdatas->facebook))
                                $socialTMP = $socialTMP . '<a href="' . $usersdatas->facebook . '" target="_blank"><i class="fa fa-facebook-square"></i></a>'; 
                            if(isset($usersdatas->twitter))
                                $socialTMP = $socialTMP . '<a href="' . $usersdatas->twitter . '" target="_blank"><i class="fa fa-twitter-square"></i></a>'; 
                            if(isset($usersdatas->instagram))
                                $socialTMP = $socialTMP . '<a href="' . $usersdatas->instagram . '" target="_blank"><i class="fa fa-instagram"></i></a>'; 
                            if(isset($usersdatas->linkedin))
                                $socialTMP = $socialTMP . '<a href="' . $usersdatas->linkedin . '" target="_blank"><i class="fa fa-linkedin-square"></i></a>'; 
                            if(isset($usersdatas->youtube))
                                $socialTMP = $socialTMP . '<a href="' . $usersdatas->youtube . '" target="_blank"><i class="fa fa-youtube-square"></i></a>'; 
                        }
                        $socialTMP = $socialTMP . '</div>';
                        $newContent = str_replace('#social#' , $socialTMP,  $newContent);

                        $finalHtml = $finalHtml . $newContent;  
                    };

                    if (strpos($finalHtml, '#useritem#') !== false) {
                        $finalHtml = static::_userContentItem($currentUserItem, 'ShopItemDetail', '#useritem#', $finalHtml);
                    }

                    if (strpos($finalHtml, '#usershopitem#') !== false) {
                        //controllo su utente
                        if($nameRoleDescr == 'liutaio'){
                            $finalHtml = str_replace('#usershopitem#' , ' ',  $newContent);
                        }else{
                            $finalHtml = static::_userShopItemCategoriesTab($currentUserItem, 'UserShop', '#usershopitem#', $finalHtml);
                        }
                    }

                    if (strpos($finalHtml, '#nrphoto#') !== false) {
                        foreach (DB::select(
                            'SELECT count(*) as totimg
                            from community_images
                            where user_id = :idUser',
                            ['idUser' => $currentItem->user_id]
                        ) as $TotImages) 
                        {
                            $finalHtml = str_replace('#nrphoto#' , $TotImages->totimg,  $finalHtml);
                        }
                    }

                    if (strpos($finalHtml, '#imgagguser#') !== false) {
                        $content = static::_userContentImgAgg($currentUserItem, 'ImgAggUser', '#imgagguser#', $finalHtml);
                    }else{
                        $content = $finalHtml;
                    }

                    return $content;
                }
            }

          
            if($contentCode == 'DetailShopUser'){
                $currentItem = UsersDatas::where('link', $url)->first();
                $currentUserItem = $currentItem->user_id;
                if(isset($currentItem)){
                    $finalHtml = '';
                    $tags = SelenaViewsBL::getTagByType('UserSala');
                    foreach (DB::select(
                        'SELECT *
                        FROM users_datas
                        INNER JOIN users ON users.id = users_datas.user_id
                        AND users_datas.language_id = :idLanguage 
                        AND users_datas.user_id = :idUser',
                        ['idLanguage' => $idLanguageContent, 'idUser' => $currentItem->user_id]
                    ) as $usersdatas){
                        $newContent = $contentLanguage->content;
                        foreach ($tags as $fieldsDb){
                            if(isset($fieldsDb->tag)){
                                if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                    $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                    if(is_numeric($usersdatas->{$cleanTag})){
                                        if (strpos($finalHtml, '#telephone_number#') !== false) {
                                            $tag = str_replace('.',',',substr($usersdatas->{$cleanTag}, 0, -2));
                                        }else{
                                            $tag = $usersdatas->{$cleanTag};
                                        }
                                    }else{
                                        $tag = $usersdatas->{$cleanTag};
                                    }          
                                    
                                    $newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                                }
                            }
                        }
                        $finalHtml = $finalHtml . $newContent;
                    };

                    if (strpos($finalHtml, '#useritem#') !== false) {
                        $finalHtml = static::_userContentItem($currentUserItem, 'ShopItemDetail', '#useritem#', $finalHtml);
                    }

                     if (strpos($finalHtml, '#user_address#') !== false) {
                        $finalHtml = static::_userContentUserAddress($currentUserItem, 'DetailAdress', '#user_address#', $finalHtml);
                    }

                    if (strpos($finalHtml, '#usershopitem#') !== false) {
                        $finalHtml = static::_userShopItemCategoriesTab($currentUserItem, 'UserShop', '#usershopitem#', $finalHtml);
                    }

                    if (strpos($finalHtml, '#email#') !== false) {
                        $UserEmail = User::where('id', $usersdatas->user_id)->first();
                        if (isset($UserEmail)){
                            $finalHtml = str_replace('#email#', $UserEmail->email,  $finalHtml); 
                        }else{
                            $finalHtml = str_replace('#email#', '',  $finalHtml);           
                        }
                    }

                    if (strpos($finalHtml, '#imgagguser#') !== false) {
                        $content = static::_userContentImgAgg($currentUserItem, 'ImgAggUser', '#imgagguser#', $finalHtml);
                    }else{
                        $content = $finalHtml;
                    }

                    return $content;
                }
            }



            if($contentCode == 'DetailGrItemsSearch'){
                $currentItem = ItemGr::where('link', $url)->first();
                if(isset($currentItem)){
                    $finalHtml = '';
                    $tags = SelenaViewsBL::getTagByType('GrSearchListItems');
                    foreach (DB::select(
                        'SELECT *
                        FROM gr_items
                        WHERE gr_items.language_id = :idLanguage 
                        AND gr_items.id = :id',
                        ['idLanguage' => $idLanguageContent, 'id' => $currentItem->id]
                    ) as $gritemsDetail){
                        $newContent = $contentLanguage->content;
                        foreach ($tags as $fieldsDb){
                            if(isset($fieldsDb->tag)){
                                if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                    $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                    if(is_numeric($gritemsDetail->{$cleanTag})){
                                        if (strpos($finalHtml, '#price_list#') !== false) {
                                            $tag = str_replace('.',',',substr($gritemsDetail->{$cleanTag}, 0, -2));
                                        }else{
                                            $tag = $gritemsDetail->{$cleanTag};
                                        }
                                    }else{
                                        $tag = $gritemsDetail->{$cleanTag};
                                    }          
                                    
                                    $newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                                }
                            }
                        }

                          if (strpos($newContent, '#tipology_description#') !== false) {
                        $DescriptionTipology = Tipology::where('id', $gritemsDetail->tipology)->first();
                        if (isset($DescriptionTipology)){
                        $newContent = str_replace('#tipology_description#', $DescriptionTipology->tipology,  $newContent); 
                        }else{
                            $newContent = str_replace('#tipology_description#', '',  $newContent);           
                        }
                    }
                      if (strpos($newContent, '#brand_description#') !== false) {
                        $DescriptionBrand = Brand::where('id', $gritemsDetail->brand)->first();
                        if (isset($DescriptionBrand)){
                        $newContent = str_replace('#brand_description#', $DescriptionBrand->brand,  $newContent); 
                        }else{
                            $newContent = str_replace('#brand_description#', '',  $newContent);           
                        }
                    }

                        $finalHtml = $finalHtml . $newContent;
                    };
                    $content = $finalHtml;                  
                    return $content;

                }
            }

            if($contentCode == 'Community_Image_Detail'){
                
                $currentPhoto = CommunityImage::where('link', $url)->first();
                
                if(isset($currentPhoto)){
                    $finalHtml = '';
                    $tags = SelenaViewsBL::getTag('community_images');
                    foreach (DB::select(
                        'SELECT community_images.*, 
                        COALESCE(users_datas.img, \'\') as user_img, users_datas.link as user_link, concat(users_datas."name", \' \', users_datas.surname) as user_name,
                        community_images_categories_languages.description as category_name,  community_images_categories_languages.url as category_url,
                        COALESCE(items.img, \'\') as camera_img, COALESCE(items_languages.description, \'\') as camera_name, COALESCE(items_languages.link, \'\') as camera_link,
                        COALESCE(items2.img, \'\') as lens_img, COALESCE(itemslan2.description, \'\') as lens_name, COALESCE(itemslan2.link, \'\') as lens_link,
                        COALESCE(items_languages.link_gallery, \'\') as camera_link_gallery,
                        COALESCE(itemslan2.link_gallery, \'\') as lens_link_gallery
                        from community_images
                        inner join users_datas on users_datas.user_id = community_images.user_id 
                        inner join community_images_categories_languages on community_images_categories_languages.id = community_images.image_category_id and community_images_categories_languages.language_id = 1
                        left outer join items on items.id = community_images."EXIF_camera_id" 
                        left outer join items_languages on items_languages.item_id = community_images."EXIF_camera_id" and items_languages.language_id = 1
                        left outer join items as items2 on items2.id = community_images."EXIF_lens_id" 
                        left outer join items_languages as itemslan2 on itemslan2.item_id = community_images."EXIF_lens_id" and itemslan2.language_id = 1
                        WHERE community_images.id = :idPhoto',
                        ['idPhoto' => $currentPhoto->id]
                    ) as $items){
                        $newContent = $contentLanguage->content;
                        foreach ($tags as $fieldsDb){
                            
                            if(isset($fieldsDb->tag)){
                                if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                    
                                    $cleanTag = str_replace('#', '', $fieldsDb->tag);

                                    if(is_numeric($items->{$cleanTag})){
                                        if($fieldsDb->tag == "#star_rating#"){
                                            if(is_null($items->{$cleanTag})){
                                                $tag = "0,0";
                                            }else{
                                                $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -3));
                                            }
                                        }else{
                                            $tag = $items->{$cleanTag};
                                        }
                                    }else{
                                        if($fieldsDb->tag == '#user_img#'){
                                            if(is_null($items->{$cleanTag}) || $items->{$cleanTag} == ''){
                                                $tag = '/storage/images/default-user-icon.jpeg';
                                            }else{
                                                if($fieldsDb->tag == '#upload_datetime#'){
                                                    $tag = date_format(date_create($items->{$cleanTag}), 'd/m/Y h:m');
                                                }else{
                                                    $tag = $items->{$cleanTag};
                                                }
                                                //$tag = $items->{$cleanTag};
                                            }
                                        }else{
                                            if($fieldsDb->tag == '#upload_datetime#'){
                                                $tag = date_format(date_create($items->{$cleanTag}), 'd/m/Y h:m');
                                            }else{
                                                $tag = $items->{$cleanTag};
                                            }
                                        }
                                    }

                                    if(is_null($items->{$cleanTag})){
                                        $tag = "";
                                    }else{
                                        if(is_bool($items->{$cleanTag})){
                                            if($items->{$cleanTag}){
                                                $tag = "si";
                                            }else{
                                                $tag = "no";
                                            }
                                        }
                                    }
                                    
                                    $newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                                     
                                }
                            }
                           
                        }
                        $finalHtml = $finalHtml . $newContent;
                    };

                    if (strpos($finalHtml, '#number_rate#') !== false) {
                        $rateNumber = 0;

                        $totRateNumber = DB::select("SELECT count(community_actions.id) AS cont
                            FROM community_actions
                            INNER JOIN community_actions_types ON community_actions.action_type_id = community_actions_types.id
                            INNER JOIN community_images ON community_actions.image_id = community_images.id
                            WHERE 
                            (community_actions_types.code = 'STAR1' 
                            OR community_actions_types.code = 'STAR2' 
                            OR community_actions_types.code = 'STAR3' 
                            OR community_actions_types.code = 'STAR4' 
                            OR community_actions_types.code = 'STAR5') AND community_images.link = '" . $url . "'");
                        
                        
                        $rateNumber = $totRateNumber[0]->cont;

                        $finalHtml = str_replace('#number_rate#', $rateNumber,  $finalHtml);  
                    }

                    if (strpos($finalHtml, '#image_comments#') !== false) {
                        $commentiImmagine = CommunityImageCommentBL::getHtmlCommenti($currentPhoto->id);
                        $content = str_replace('#image_comments#', $commentiImmagine,  $finalHtml);  
        
                    }else{
                        $content = $finalHtml;
                    }

                }else{
                    $content = '';
                }   
            }

            if($contentCode == 'PagePrices'){
                
                $currentItem = ItemLanguage::where('link_price', $url)->first();
                
                if(isset($currentItem)){
                    $finalHtml = '';
                    $tags = SelenaViewsBL::getTagByType('Items');
                    foreach (DB::select(
                        'SELECT *
                        FROM items
                        INNER JOIN items_languages ON items.id = items_languages.item_id
                        INNER JOIN categories_items ON items.id = categories_items.item_id
                        AND items_languages.language_id = :idLanguage
                        AND items_languages.item_id = :idItem',
                        ['idLanguage' => $idLanguageContent, 'idItem' => $currentItem->item_id]
                    ) as $items){
                        $newContent = $contentLanguage->content;
                        foreach ($tags as $fieldsDb){
                            if(isset($fieldsDb->tag)){
                                if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                    $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                    if(is_numeric($items->{$cleanTag})){
                                        $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                    }else{
                                        $tag = $items->{$cleanTag};
                                    }
                                    if(is_null($items->{$cleanTag})){
                                        $tag = "";
                                    }else{
                                        if(is_bool($items->{$cleanTag})){
                                            if($items->{$cleanTag}){
                                                $tag = "si";
                                            }else{
                                                $tag = "no";
                                            }
                                        }
                                    }
                                    
                                    $newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                                     
                                }
                            }
                           
                        }
                        $finalHtml = $finalHtml . $newContent;
                    };
                    
                    if (strpos($finalHtml, '#trovaprezzi#') !== false) {
                        $dettaglioOfferte = ItemPriceBL::getHtmlDettaglioOfferte($currentItem->item_id);
                        $content = str_replace('#trovaprezzi#', $dettaglioOfferte,  $finalHtml);  
        
                    }else{
                        $content = $finalHtml;
                    }

                }else{
                    $content = 'Nessun prezzo trovato';
                }   
            }

            if($contentCode == 'ItemGalleryPage'){
                
                $currentItem = ItemLanguage::where('link_gallery', $url)->first();

                if(isset($currentItem)){
                    
                    $categoryItemOnDb = CategoryItem::where('item_id', $currentItem->item_id)->get()->first();

                    $finalHtml = '';
                    $tags = SelenaViewsBL::getTagByType('Items');
                    foreach (DB::select(
                        'SELECT *
                        FROM items
                        INNER JOIN items_languages ON items.id = items_languages.item_id
                        INNER JOIN categories_items ON items.id = categories_items.item_id
                        AND items_languages.language_id = :idLanguage
                        AND items_languages.item_id = :idItem',
                        ['idLanguage' => $idLanguageContent, 'idItem' => $currentItem->item_id]
                    ) as $items){
                        $newContent = $contentLanguage->content;
                        foreach ($tags as $fieldsDb){
                            if(isset($fieldsDb->tag)){
                                if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                    $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                    if(is_numeric($items->{$cleanTag})){
                                        $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                    }else{
                                        $tag = $items->{$cleanTag};
                                    }
                                    if(is_null($items->{$cleanTag})){
                                        $tag = "";
                                    }else{
                                        if(is_bool($items->{$cleanTag})){
                                            if($items->{$cleanTag}){
                                                $tag = "si";
                                            }else{
                                                $tag = "no";
                                            }
                                        }
                                    }
                                    
                                    $newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                                     
                                }
                            }
                           
                        }
                        $finalHtml = $finalHtml . $newContent;
                    };

                    if (strpos($finalHtml, '#galleriafoto#') !== false) {
                        $strReplace = "";
                        if ($categoryItemOnDb->category_id>=11 && $categoryItemOnDb->category_id<=15)
                            $strReplace = CommunityImageBL::getGallery('popular', '', '', '', '', $currentItem->item_id, '');
                        else if ($categoryItemOnDb->category_id==16)
                            $strReplace = CommunityImageBL::getGallery('popular', '', '', '', '', '', $currentItem->item_id);

                        $content = str_replace('#galleriafoto#', $strReplace,  $finalHtml);  
        
                    }else{
                        $content = $finalHtml;
                    }

                }else{
                    $content = 'Nessun articolo trovato';
                }   
            }

            if($contentCode == 'PageItems'){
                $currentItem = ItemLanguage::where('link', $url)->first();
                $currentUserItem = $currentItem->created_id;
                $roleUser = RoleUser::where('user_id', $currentUserItem)->get()->first();
                $roleDescription = "";
                if(isset($roleUser)){
                    $roleDescription = Role::where('id', $roleUser->role_id)->first()->name;
                }
                
                $currentItemId = $currentItem->item_id;
                if(isset($currentItem)){
                    $finalHtml = '';
                    $tags = SelenaViewsBL::getTagByType('Items');
                    foreach (DB::select(
                        'SELECT distinct *
                        FROM items
                        INNER JOIN items_languages ON items.id = items_languages.item_id
                        INNER JOIN categories_items ON items.id = categories_items.item_id
                        AND items_languages.language_id = :idLanguage
                        AND items_languages.item_id = :idItem limit 1',
                        ['idLanguage' => $idLanguageContent, 'idItem' => $currentItem->item_id]
                    ) as $items){
                        $newContent = $contentLanguage->content;
                        foreach ($tags as $fieldsDb){
                            if(isset($fieldsDb->tag)){
                                if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                    $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                    if(is_numeric($items->{$cleanTag}) && $fieldsDb->tag != '#producer_id#'){
                                        if($fieldsDb->tag == '#price#'){
                                            //prelievo campo nelle impostazioni per metodologia di visualizzazione, le opzioni sono NESSUNO, BARRATO, DEDICATO
                                            $settingDisplayPrice = SettingBL::getByCode(SettingEnum::DisplayPrice);
                                            if($idUser == null || $idUser == ''){
                                                $userId = '';
                                            }else{
                                                $userId = $idUser;
                                            }
                                            //creazione funzione per prelievo prezzo scontato, nelle regole del carrello

                                            switch ($settingDisplayPrice) {
                                                case "NESSUNO":
                                                    //se NESSUNO stampo il prezzo in items
                                                    $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                                    break;
                                                case "BARRATO":
                                                    //se BARRATO aggiungo l'html <strike> al prezzo normale e appendo il prezzo calcolato
                                                    $priceCalculated = CartBL::getPriceCalculated($currentItemId, $userId);
                                                    
                                                    if($priceCalculated != ""){
                                                        $tag = '<strike>' . str_replace('.',',',substr($items->{$cleanTag}, 0, -2)) . '</strike><br>€ ' . str_replace('.','',number_format($priceCalculated,2,",","."));
                                                    }else{
                                                        $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                                    }
                                                    break;
                                                case "DEDICATO":
                                                    //se DEDICATO stampo il prezzo calcolato
                                                    $priceCalculated = CartBL::getPriceCalculated($currentItemId, $userId);
                                                    if($priceCalculated != ""){
                                                        $tag = str_replace('.',',',substr($priceCalculated, 0, -1));
                                                    }else{
                                                        $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                                    }
                                                    break;
                                                default:
                                                    $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                            }
                                            //$tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                        }else{
                                            if($fieldsDb->tag == '#internal_code#'){
                                                $tag = $items->{$cleanTag};
                                            }else{
                                                $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                            }
                                        }
                                    }else{
                                        $tag = $items->{$cleanTag};
                                    }
                                    if(is_null($items->{$cleanTag})){
                                        $tag = "";
                                    }else{
                                        if(is_bool($items->{$cleanTag})){
                                            if($items->{$cleanTag}){
                                                $tag = "si";
                                            }else{
                                                $tag = "no";
                                            }
                                        }
                                    }
                                    
                                    if($fieldsDb->tag == '#created_id#'){
                                        $newContent = str_replace($fieldsDb->tag , $currentUserItem ,  $newContent);
                                    }else{
                                        if($fieldsDb->tag == '#item_id#'){
                                            $newContent = str_replace($fieldsDb->tag , $currentItemId ,  $newContent);
                                        }else{
                                            $newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                                        }
                                    } 
                                    if (strpos($newContent, '#description_button#') !== false) {
                                        if($roleDescription == "private"){
                                            $newContent = str_replace('#description_button#' , 'Contatta il venditore' ,  $newContent);
                                        }else{
                                            $newContent = str_replace('#description_button#' , 'Contatta il negozio' ,  $newContent);
                                        }
                                    }
                                    if (strpos($newContent, '#producer#') !== false) {
                                        if(!is_null($items->producer_id) || $items->producer_id !=''){
                                            $producerItem = Producer::where('id', $items->producer_id)->get()->first();
                                            if(isset($producerItem)){
                                                $newContent = str_replace('#producer#' , $producerItem->business_name ,  $newContent);
                                            }else{
                                                $newContent = str_replace('#producer#' , '-',  $newContent);
                                            }
                                        }else{
                                            $newContent = str_replace('#producer#' , '-',  $newContent);
                                        }
                                    }
                                     
                                }
                            }
                            //$idUser variabile prelevata dalla funzione
                            if(is_null($idUser) || $idUser == ''){
                                if (strpos($newContent, '#classFavoritesItem#') !== false) {
                                    $newContent = str_replace('#classFavoritesItem#', 'fa-heart-o',  $newContent);
                                }
                            }else{
                                if (strpos($newContent, '#classFavoritesItem#') !== false) {
                                    
                                    $favoriteItemUserOnDb = ItemFavoritesUser::where('user_id', $idUser)->where('item_id', $items->item_id)->get()->first();
                                    if(isset($favoriteItemUserOnDb)){
                                        $newContent = str_replace('#classFavoritesItem#', 'fa-heart',  $newContent);
                                    }else{
                                        $newContent = str_replace('#classFavoritesItem#', 'fa-heart-o',  $newContent);
                                    }
                                }
                            }
                        }
                        $finalHtml = $finalHtml . $newContent;
                    };
                    if (strpos($finalHtml, '#userdetailitem#') !== false) {
                        $finalHtml = static::_userContent($currentUserItem, 'UserDetail', '#userdetailitem#', $finalHtml);
                    }

                    if (strpos($finalHtml, '#imgaggitem#') !== false) {
                        $content = static::_itemContentImgAgg($currentItemId, 'ImgAggItem', '#imgaggitem#', $finalHtml);
                    }else{
                        $content = $finalHtml;
                    }

                    /* estrazione schede tecniche dedicate all'articolo che si sta navigando */
                    /*if (strpos($finalHtml, '#technicalspecifications#') !== false) {
                        $content = static::_itemContentTechnicalSpecifications($currentItemId, 'TechnicalSpecifications', '#technicalspecifications#', $finalHtml);
                    }else{
                        $content = $finalHtml;
                    }*/
                    /* end estrazione schede tecniche dedicate all'articolo che si sta navigando */


                }else{
                    $content = 'Nessun articolo trovato';
                }   
            }            
        }
        
        return $content;

        
    }




    #endregion GET

    #region INSERT OR UPDATE

    /**
     * Insert or Upate
     *
     * @param Int idContent
     * @param Int idLanguage
     * @param String content
     * @param String description
     *
     * @return Int
     */
    public static function insertOrUpdate(int $idContent, int $idLanguage, string $content, string $description)
    {
        $contentLanguage = ContentLanguage::where('content_id', $idContent)->where('language_id', $idLanguage)->first();

        if (is_null($contentLanguage)) {
            $contentLanguage = new ContentLanguage();
            $contentLanguage->content_id = $idContent;
            $contentLanguage->language_id = $idLanguage;
        }

        $contentLanguage->content = $content;
        $contentLanguage->description = $description;
        $contentLanguage->save();

        return $contentLanguage->id;
    }

    #endregion INSERT OR UPDATE

    #region DELETE

    /**
     * Delete by content
     *
     * @param Int idContent
     */
    public static function deleteByContent(int $idContent)
    {
        ContentLanguage::where('content_id', $idContent)->delete();
    }

    /**
     * Delete language
     *
     * @param Int idContent
     * @param Int idLanguageContent
     * @param Int idFunctionality
     * @param Int idUser
     * @param Int idLanguage
     */
    public static function deleteLanguage(int $idContent, int $idLanguageContent, int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $contentLanguage = ContentLanguage::where('content_id', $idContent)->where('language_id', $idLanguageContent)->first();

        if (is_null($contentLanguage)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContentNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $contentLanguage->delete();
    }

    #endregion DELETE
}

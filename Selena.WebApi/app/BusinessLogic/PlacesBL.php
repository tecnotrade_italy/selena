<?php

namespace App\BusinessLogic;

use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoPlaces;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\LanguageNation;
use App\LanguageProvince;
use App\User;
use App\Places;
use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;


class PlacesBL
{

     #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request DtoPlaces
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoPlaces, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoPlaces->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Places::find($DtoPlaces->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }
        if (is_null($DtoPlaces->name)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoPlaces->address)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::AddressNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoPlaces->postal_code)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PostalCodeNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoPlaces->place)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PlaceNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoPlaces->province)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ProvinceNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoPlaces->nation_id)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NationNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    /**
     * Convert to dto
     * 
     * @param Places
     * 
     * @return DtoPlaces
     */
    private static function _convertToDto($Places)
    {
        $DtoPlaces = new DtoPlaces();

        if (isset($Places->id)) {
            $DtoPlaces->id = $Places->id;
        }
        if (isset($Places->name)) {
            $DtoPlaces->name = $Places->name;
        }
        if (isset($Places->address)) {
            $DtoPlaces->address = $Places->address;
        }
        if (isset($Places->postal_code)) {
            $DtoPlaces->postal_code = $Places->postal_code;
        }
        if (isset($Places->place)) {
            $DtoPlaces->place = $Places->place;
        }
        $DescriptionProvince = LanguageProvince::where('province_id', $Places->province)->first()->description;
        if (isset($DescriptionProvince)) {
            $DtoPlaces->province = $DescriptionProvince;
        }else{
           $DtoPlaces->province = '';  
        }
        $DescriptionNation = LanguageNation::where('nation_id', $Places->nation_id)->first()->description;
        if (isset($DescriptionNation)) {
            $DtoPlaces->nation_id = $DescriptionNation;
        }
        if (isset($Places->lat)) {
            $DtoPlaces->lat = $Places->lat;
        }
        if (isset($Places->lon)) {
            $DtoPlaces->lon = $Places->lon;
        }

        return $DtoPlaces;
    }

    #endregion PRIVATE

    #region GET
    /**
     * Get by id
     * 
     * @param int id
     * @return DtoPlaces
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Places::find($id));
    }

    /**
     * Get select
     *
     * @param String $search
     * @param int $idUser
     * @return People
     */
    public static function getSelect(string $search)
    {
                if ($search == "*") {
                    return Places::select('id', 'name as description')->get();
                } else {
                    return Places::where('name', 'ilike' , $search . '%')->select('id', 'name as description')->get();
          } 
    }

  
    /**
     * Get all
     * 
     * @return DtoPlaces
     */
    public static function getAll()
    {
        $result = collect();

        foreach (Places::orderBy('id','DESC')->get() as $Places) {    

                $DtoPlaces = new DtoPlaces();
            
                if (isset($Places->id)) {
                    $DtoPlaces->id = $Places->id;
                }
                if (isset($Places->name)) {
                    $DtoPlaces->name = $Places->name;
                }
                if (isset($Places->address)) {
                    $DtoPlaces->address = $Places->address;
                }
                if (isset($Places->postal_code)) {
                    $DtoPlaces->postal_code = $Places->postal_code;
                }
                if (isset($Places->place)) {
                    $DtoPlaces->place = $Places->place;
                }
                //tabella
                if (isset($Places->province)) {
                    $DtoPlaces->Province = LanguageProvince::where('province_id', $Places->province)->first()->description;
                }
                if (isset($Places->nation_id)) {
                    $DtoPlaces->Nation = LanguageNation::where('nation_id', $Places->nation_id)->first()->description;
                }
                //dettaglio
                if (isset($Places->province)) {
                    $DtoPlaces->province = $Places->province;
                }
                if (isset($Places->nation_id)) {
                     $DtoPlaces->nation_id = $Places->nation_id;
                }
                if (isset($Places->lat)) {
                    $DtoPlaces->lat = $Places->lat;
                }
                if (isset($Places->lon)) {
                    $DtoPlaces->lon = $Places->lon;
                }

                    $result->push($DtoPlaces);
                }

                return $result;

                }

    #endregion GET



    #region INSERT
    /**
     * Insert
     * 
     * @param Request DtoPlaces
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoPlaces, int $idUser)
    {
       DB::beginTransaction();
        try {
            $Places = new Places();
            
        if (isset($DtoPlaces->name)) {
            $Places ->name = $DtoPlaces->name;
        } 
        if (isset($DtoPlaces->address)) {
            $Places ->address = $DtoPlaces->address;
        } 
        if (isset($DtoPlaces->postal_code)) {
            $Places ->postal_code = $DtoPlaces->postal_code;
        } 
        if (isset($DtoPlaces->place)) {
            $Places ->place = $DtoPlaces->place;
        } 
        if (isset($DtoPlaces->province)) {
            $Places ->province = $DtoPlaces->province;
        } 
        if (isset($DtoPlaces->nation_id)) {
            $Places ->nation_id = $DtoPlaces->nation_id;
        } 
        if (isset($DtoPlaces->lat)) {
            $Places ->lat = $DtoPlaces->lat;
        } 
        if (isset($DtoPlaces->lon)) {
            $Places ->lon = $DtoPlaces->lon;
        }

        $Places->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $Places->id;
    }

    #endregion INSERT


 
    /**
     * Update
     * 
     * @param Request [DtoPlaces]
     * @param int idLanguage
     */
    public static function update(Request $DtoPlaces, int $idUser)
    {
       
        $PlacesOnDb = Places::find($DtoPlaces->id);
      
        DB::beginTransaction();

        try {
            if (isset($DtoPlaces->id)) {
                $PlacesOnDb->id = $DtoPlaces->id;
            } 
            if (isset($DtoPlaces->name)) {
                $PlacesOnDb->name = $DtoPlaces->name;
            } 
            if (isset($DtoPlaces->address)) {
                $PlacesOnDb->address = $DtoPlaces->address;
            } 
            if (isset($DtoPlaces->postal_code)) {
                $PlacesOnDb->postal_code = $DtoPlaces->postal_code;
            }
            if (isset($DtoPlaces->place)) {
                $PlacesOnDb->place = $DtoPlaces->place;
            }
            if (isset($DtoPlaces->province)) {
                $PlacesOnDb->province = $DtoPlaces->province;
            }
            if (isset($DtoPlaces->nation_id)) {
                $PlacesOnDb->nation_id = $DtoPlaces->nation_id;
            }
            if (isset($DtoPlaces->lat)) {
                $PlacesOnDb->lat = $DtoPlaces->lat;
            }
            if (isset($DtoPlaces->lon)) {
                $PlacesOnDb->lon = $DtoPlaces->lon;
            }
            $PlacesOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return  $DtoPlaces->id;
    }
    #endregion UPDATE

        /**
             * Clone
             * 
             * @param int id
             * @param int idFunctionality
             * @param int idLanguage
             * @param int idUser
             * 
             * @return int
             */
            public static function clone(int $id, int $idFunctionality, int $idUser, int $idLanguage)
            {
                //FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
                $Places = Places::find($id);
                              
                DB::beginTransaction();

                try {

                    $newPlaces = new Places();
                    $newPlaces->name = $Places->name . ' - Copy';
                    $newPlaces->address = $Places->address;
                    $newPlaces->postal_code = $Places->postal_code;
                    $newPlaces->place = $Places->place;
                    $newPlaces->province = $Places->province;
                    $newPlaces->nation_id = $Places->nation_id;
                    $newPlaces->lat = $Places->lat;
                    $newPlaces->lon = $Places->lon;
                    $newPlaces->save();

                    DB::commit();
                    return $newPlaces->id;
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
            }

        #endregion CLONE

            #region DELETE
                    /**
                     * Delete
                     * 
                     * @param int id
                     * @param int idLanguage
                     */
                    public static function delete(int $id, int $idLanguage)
                    {
                        $Places = Places::find($id);                   

                        if (is_null($Places)) {
                            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PeopleRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
                        }
                        DB::beginTransaction();

                        try {

                            $Places->delete();
                            
                            DB::commit();
                        } catch (Exception $ex) {
                            DB::rollback();
                            throw $ex;
                        }
                    }
                
                }
        #endregion DELETE
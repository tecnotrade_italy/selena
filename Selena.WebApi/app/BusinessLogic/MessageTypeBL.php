<?php

namespace App\BusinessLogic;

use Illuminate\Support\Facades\DB;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use App\MessageType;

class MessageTypeBL
{
    #region GET

    /**
     * Get
     *
     * @param String $search
     * @return Icon
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return MessageType::select('id', 'description')->get();
        } else {
            return MessageType::where('description', 'ilike','%'. $search . '%')->select('id', 'description')->get();
        }
    }

    /**
     * Get by id
     *
     * @param int $id
     * @return MessageType
     */
    public static function getById(int $id)
    {
        return MessageType::find($id);
    }


    #endregion GET
}

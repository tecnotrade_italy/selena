<?php

namespace App\BusinessLogic;

use App\CartRules;
use App\DtoModel\DtoVoucher;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Voucher;
use Carbon\Carbon;
use App\Helpers\HttpHelper;
use Exception;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;

class VoucherBL
{
    #region PRIVATE

    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request dtoVoucher
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationsTypesEnum)
    {

        if (is_null($request->voucher_code)|| $request->voucher_code=='') {
            throw new Exception("Il codice è obbligatorio");
        }

        if (is_null($request->voucher_type_id)|| $request->voucher_type_id=='') {
            throw new Exception("Il tipo di voucher è obbligatorio");
        }

        if (is_null($request->discount_value)==true) {
            throw new Exception("Lo sconto è obbligatorio");
        }

        /*if (is_null($request->discount_value)==false && is_null($request->minimum_order_amount)==false) {
            if ($request->discount_value > $request->minimum_order_amount){
                throw new Exception("Lo sconto non può essere maggiore dell'importo minimo");
            }
        }*/


        if (is_null($request->discount_value)==false) {
            if ($request->discount_value<0){
                throw new Exception("Lo sconto in valore non può essere minore di ZERO");
            }
        }

        if (is_null($request->availability)==false) {
            if ($request->availability<0){
                throw new Exception("La disponibilità non può essere minore di ZERO");
            }
        }


        switch ($dbOperationsTypesEnum) {
            case DbOperationsTypesEnum::INSERT:

                if (CartRules::where('voucher_code', $request->voucher_code)->count() > 0) {
                    throw new Exception("Codice voucher già esistente!");
                }

                break;

            case DbOperationsTypesEnum::UPDATE:
                /*
                if (is_null($request->id)) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
                } else {
                    if (is_null(User::find($request->id))) {
                        throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotFound), HttpResultsCodesEnum::InvalidPayload);
                    }
                }

                if (User::where('name', $request->username)->whereNotIn('id', [$request->id])->count() > 0) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UsernameAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }

                if (User::where('email', $request->email)->whereNotIn('id', [$request->id])->count() > 0) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }
                */
                break;
        }

    }

    /**
     * Convert to dto
     * 
     * @param Voucher Voucher
     * @return DtoVoucher
     */
    private static function _convertToDto($Voucher)
    {
        $dtoVoucher = new DtoVoucher();

        if (isset($Voucher->id)) {
            $dtoVoucher->id = $Voucher->id;
        }

        if (isset($Voucher->code)) {
            $dtoVoucher->code = $Voucher->code;
        }

        if (isset($Voucher->type_id)) {
            $dtoVoucher->type_id = $Voucher->type_id;
        }
        if (isset($Voucher->enabled_from)) {
            $dtoVoucher->enabled_from = $Voucher->enabled_from;
        }

        if (isset($Voucher->enabled_to)) {
            $dtoVoucher->enabled_to = $Voucher->enabled_to;
        }

        if (isset($Voucher->cumulative)) {
            $dtoVoucher->cumulative = $Voucher->cumulative;
        }

        if (isset($Voucher->minimum_amount)) {
            $dtoVoucher->minimum_amount = $Voucher->minimum_amount;
        }        

        if (isset($Voucher->discount_percentage)) {
            $dtoVoucher->discount_percentage = $Voucher->discount_percentage;
        }

        if (isset($Voucher->discount_value)) {
            $dtoVoucher->discount_value = $Voucher->discount_value;
        }

        if (isset($Voucher->online)) {
            $dtoVoucher->online = $Voucher->online;
        }

        if (isset($Voucher->note)) {
            $dtoVoucher->note = $Voucher->note;
        }   

        if (isset($Voucher->availability)) {
            $dtoVoucher->availability = $Voucher->availability;
        }  

        if (isset($Voucher->availability_residual)) {
            $dtoVoucher->availability_residual = $Voucher->availability_residual;
        }  
        
        return $dtoVoucher;
    }

    public static function getByCode(string $code)
    {
        if (is_null($code)) {
            LogHelper::error('Code of voucher not found');
            return '';
        } else {
            $voucher = Voucher::where('code', $code)->first();

            if (is_null($voucher)) {
                LogHelper::error('Voucher not found');
                return '';
            } else {
                return $voucher;
            }
        }
    }

    /**
     * Convert to VatType
     * 
     * @param DtoVoucher dtoVoucher
     * 
     * @return Voucher
     */
    private static function _convertToModel($dtoVoucher)
    {
        $Voucher = new CartRules();

        if (isset($dtoVoucher->id)) {
            $Voucher->id = $dtoVoucher->id;
        }

        if (isset($dtoVoucher->voucher_code)) {
            $Voucher->voucher_code = $dtoVoucher->voucher_code;
        }

        if (isset($dtoVoucher->description)) {
            $Voucher->description = $dtoVoucher->description;
        }

        if (isset($dtoVoucher->description)) {
            $Voucher->name = $dtoVoucher->description;
        }

        if (isset($dtoVoucher->voucher_type_id)) {
            $Voucher->voucher_type_id = $dtoVoucher->voucher_type_id;
        }

        if (isset($dtoVoucher->enabled_to)) {
            $Voucher->enabled_to = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $dtoVoucher->enabled_to)->format('Y/m/d H:i:s');
        } else
            $Voucher->enabled_to = null;

        if (isset($dtoVoucher->enabled_from)) {
            $Voucher->enabled_from = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $dtoVoucher->enabled_from)->format('Y/m/d H:i:s');
        } else
            $Voucher->enabled_from = null;

        if (isset($dtoVoucher->minimum_order_amount)) {
            $Voucher->minimum_order_amount = $dtoVoucher->minimum_order_amount;
        } else
            $Voucher->minimum_order_amount = null;

        if (isset($dtoVoucher->discount_value)) {
            $Voucher->discount_value = $dtoVoucher->discount_value;
        }else
            $Voucher->discount_value = null;

        if (isset($dtoVoucher->online)) {
            $Voucher->online = $dtoVoucher->online;
        }

        if (isset($dtoVoucher->availability)) {
            $Voucher->availability = $dtoVoucher->availability;
        }   else
            $Voucher->availability = null;

        if (isset($dtoVoucher->availability_residual)) {
            $Voucher->availability_residual = $dtoVoucher->availability_residual;
        }   

        return $Voucher;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoVoucher
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Voucher::find($id));
    }

    /**
     * Get all
     * 
     * @return DtoVoucher
     */
    public static function getAll(){
        $result = collect(); 

        foreach (CartRules::where('voucher_code', '<>', '')->orderBy('id','DESC')->get() as $voucher) {
            $DtoVoucher = new DtoVoucher();

            $DtoVoucher->id = $voucher->id;
            $DtoVoucher->description = $voucher->description;
            $DtoVoucher->voucher_code = $voucher->voucher_code;
            $DtoVoucher->voucher_type_id = $voucher->voucher_type_id;
            $DtoVoucher->enabled_to = $voucher->enabled_to;
            $DtoVoucher->enabled_from = $voucher->enabled_from;
            $DtoVoucher->minimum_order_amount = $voucher->minimum_order_amount;
            $DtoVoucher->discount_value = $voucher->discount_value;
            $DtoVoucher->online = $voucher->online;
            $DtoVoucher->availability = $voucher->availability;
            $DtoVoucher->availability_residual = $voucher->availability_residual;

            $result->push($DtoVoucher); 
        }
        return $result;

    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request dtoVoucher
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $dtoVoucher, int $idLanguage)
    {
        
        static::_validateData($dtoVoucher, $idLanguage, DbOperationsTypesEnum::INSERT);
        $Voucher = static::_convertToModel($dtoVoucher);

        //in inserimento metto la disponibilità residua uguale alla disponibilità totale....poi verrà pian piano scalata
        $Voucher->availability_residual = $Voucher->availability;
        $Voucher->save();

        return $Voucher->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoVoucher
     * @param int idLanguage
     */
    public static function update(Request $dtoVoucher, int $idLanguage)
    {
        static::_validateData($dtoVoucher, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $Voucher = CartRules::find($dtoVoucher->id);
        DBHelper::updateAndSave(static::_convertToModel($dtoVoucher), $Voucher);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $Voucher = CartRules::find($id);

        if (is_null($Voucher)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $Voucher->delete();
    }

    #endregion DELETE
}

<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoPostalCode;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\PostalCode;
use Exception;
use Illuminate\Http\Request;

class PostalCodeBL
{
    #region PRIVATE

    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request dtoPostalCode
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $dtoPostalCode, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($dtoPostalCode->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (PostalCode::find($dtoPostalCode->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($dtoPostalCode->code)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CompanyNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    /**
     * Convert to dto
     * 
     * @param PostalCode postalCode
     * 
     * @return DtoPostalCode
     */
    private static function _convertToDto($postalCode)
    {
        $dtoPostalCode = new DtoPostalCode();

        if (isset($postalCode->id)) {
            $dtoPostalCode->id = $postalCode->id;
        }

        if (isset($postalCode->code)) {
            $dtoPostalCode->code = $postalCode->code;
        }

        return $dtoPostalCode;
    }

    /**
     * Convert to VatType
     * 
     * @param DtoPostalCode dtoPostalCode
     * 
     * @return PostalCode
     */
    private static function _convertToModel($dtoPostalCode)
    {
        $postalCode = new PostalCode();

        if (isset($dtoPostalCode->id)) {
            $postalCode->id = $dtoPostalCode->id;
        }

        if (isset($dtoPostalCode->code)) {
            $postalCode->code = $dtoPostalCode->code;
        }

        return $postalCode;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoPostalCode
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(PostalCode::find($id));
    }

    /**
     * Get select
     *
     * @param String $search
     * @return PostalCode
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return PostalCode::select('id', 'code as description')->get();
        } else {
            return PostalCode::where('code', 'ilike', $search . '%')->select('id', 'code as description')->get();
        }
    }

    /**
     * Get all
     * 
     * @return DtoPostalCode
     */
    public static function getAll()
    {
        return PostalCode::all()->map(function ($postalCode) {
            return static::_convertToDto($postalCode);
        });
    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request dtoPostalCode
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $dtoPostalCode, int $idLanguage)
    {
        static::_validateData($dtoPostalCode, $idLanguage, DbOperationsTypesEnum::INSERT);
        $postalcode = static::_convertToModel($dtoPostalCode);
        $postalcode->save();
        return $postalcode->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoPostalCode
     * @param int idLanguage
     */
    public static function update(Request $dtoPostalCode, int $idLanguage)
    {
        static::_validateData($dtoPostalCode, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $customerOnDb = PostalCode::find($dtoPostalCode->id);
        DBHelper::updateAndSave(static::_convertToModel($dtoPostalCode), $customerOnDb);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $postalCode = PostalCode::find($id);

        if (is_null($postalCode)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $postalCode->delete();
    }

    #endregion DELETE
}

<?php
namespace App\BusinessLogic;

use App\DtoModel\DtoNation;
use App\DtoModel\DtoNewsletterGroupQueryEmail;
use App\DtoModel\DtoSelenaViews;
use App\DtoModel\DtoTagTemplate;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\SelenaSqlViews;
use App\Region;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QueryGroupNewsletterBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (SelenaSqlViews::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->nameView)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    
    /**
     * Convert to dto
     * 
     * @param Nation nation
     * @param int idLanguage
     * 
     * @return DtoNation
     */
    private static function _convertToDto(SelenaSqlViews $selenaviews)
    {
        $dtoSelenaViews = new DtoSelenaViews();
        $dtoSelenaViews->id = $selenaviews->id;
        $dtoSelenaViews->nameView = $selenaviews->name_view;
        $dtoSelenaViews->description = $selenaviews->description;
        $dtoSelenaViews->select = $selenaviews->select;
        $dtoSelenaViews->from = $selenaviews->from;
        $dtoSelenaViews->where = $selenaviews->where;
        $dtoSelenaViews->orderBy = $selenaviews->order_by;
        $dtoSelenaViews->groupBy = $selenaviews->group_by;
        $dtoSelenaViews->group = $selenaviews->group;

        return $dtoSelenaViews;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoNation
     */
    public static function getAll()
    {
        $result = collect();
        
        foreach (SelenaSqlViews::where('group', 'Newsletter')->get() as $selenaViewsAll) {
            
            $dtoSelenaViews = new DtoSelenaViews();
            $dtoSelenaViews->id = $selenaViewsAll->id;
            $dtoSelenaViews->nameView = $selenaViewsAll->name_view;
            $dtoSelenaViews->description = $selenaViewsAll->description;
            $dtoSelenaViews->select = $selenaViewsAll->select;
            $dtoSelenaViews->from = $selenaViewsAll->from;
            $dtoSelenaViews->where = $selenaViewsAll->where;
            $dtoSelenaViews->orderBy = $selenaViewsAll->order_by;
            $dtoSelenaViews->groupBy = $selenaViewsAll->group_by;
            $result->push($dtoSelenaViews); 
        }
        return $result;
    }

      /**
     * Get select
     *
     * @param String $search
     * @return Intervention
     */
    public static function getSelect(string $search)
    {    
        $NewsletterParameter= 'Newsletter'; 
     if ($search == "*" || $search=='') {
            return collect(DB::select(
                "SELECT distinct selena_sql_views.id as id,  selena_sql_views.name_view as description
                from selena_sql_views
                where selena_sql_views.group ilike '" . $NewsletterParameter . "%' AND selena_sql_views.name_view ilike '" . $search . "%'"));  
            }else{
                //return SelenaSqlViews::where('name_view', 'ilike','%'. $search . '%')->select('id', 'name_view')->get();
              return collect(DB::select(       
                "SELECT distinct selena_sql_views.id as id,  selena_sql_views.name_view as description
                from selena_sql_views
                where selena_sql_views.group ilike '" . $NewsletterParameter . "%' AND selena_sql_views.name_view ilike '" . $search . "%'"));
            }
    }



    
      public static function executequeryresult(request $request)
    {
      
         $QueryResultExecuteQuery = "";
            if (isset($request->select)) {
                $QueryResultExecuteQuery = $QueryResultExecuteQuery . 'SELECT ' . $request->select;
                if (isset($request->from)) {
                    $QueryResultExecuteQuery = $QueryResultExecuteQuery . ' FROM ' . $request->from;

                    if (isset($request->where)) {
                        $QueryResultExecuteQuery = $QueryResultExecuteQuery . ' WHERE ' . $request->where;
                    }

                    if (isset($request->order_by)) {
                        $QueryResultExecuteQuery = $QueryResultExecuteQuery . ' ORDER BY ' . $request->order_by;
                    }

                    if (isset($request->group_by)) {
                        $QueryResultExecuteQuery = $QueryResultExecuteQuery . ' GROUP BY ' . $request->group_by;
                    }
                }
            }
               
            $rowsResult = DB::select($QueryResultExecuteQuery);
            

              $result = collect();
            if (isset($rowsResult)) {
            foreach ($rowsResult as $RowsResult) {    
               $result->push($RowsResult);   
            } 
                return  $result;
            } else {
                return '';
            }
    }



    /**
     * Get by id
     * 
     * @param int id
     * 
     * @return DtoNation
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(SelenaSqlViews::find($id));
    }

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request)
    {
        //static::_validateData($request, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $selenaviews = new SelenaSqlViews();
            $selenaviews->name_view = $request->nameView;
            $selenaviews->description = $request->description;
            $selenaviews->select = $request->select;
            $selenaviews->from = $request->from;
            $selenaviews->where = $request->where;
            $selenaviews->order_by = $request->orderBy;
            $selenaviews->group_by = $request->groupBy;
            //$selenaviews->created_id = $request->$idUser;
            $selenaviews->group = 'Newsletter';
            $selenaviews->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $selenaviews->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoSelenaViews
     * @param int idLanguage
     */
    public static function update(Request $request, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $selenaViewsOnDb = SelenaSqlViews::find($request->id);
        
        DB::beginTransaction();

        try {
            $selenaViewsOnDb->name_view = $request->nameView;
            $selenaViewsOnDb->description = $request->description;
            $selenaViewsOnDb->select = $request->select;
            $selenaViewsOnDb->from = $request->from;
            $selenaViewsOnDb->where = $request->where;
            $selenaViewsOnDb->order_by = $request->orderBy;
            $selenaViewsOnDb->group_by = $request->groupBy;
            $selenaViewsOnDb->group = 'Newsletter';
            $selenaViewsOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {

        $selenaviews = SelenaSqlViews::find($id);

        if (is_null($selenaviews)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $selenaviews->delete();
    }
    #endregion DELETE
}

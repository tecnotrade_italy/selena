<?php

namespace App\BusinessLogic;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoInternalRepair;
use App\DtoModel\DtoInterventions;
use App\DtoModel\DtoInterventionsPhotos;
use App\DtoModel\DtoInterventionsProcessing;
use App\DtoModel\DtoModuleValidationGuarantee;
use App\DtoModel\DtoQuotes;
use App\DtoModel\DtoQuotesMessages;
use App\Enums\DbOperationsTypesEnum;
use App\ExternalRepair;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\GrIntervention;
use App\GrInterventionPhoto;
use App\GrInterventionProcessing;
use App\GrQuotes;
use App\GrStates;
use App\ItemGr;
use App\ItemGroupTechnicalSpecification;
use App\Mail\DynamicMail;
use App\ModuleValidationGuarantee;
use App\PaymentGr;
use App\People;
use App\QuotesMessages;
use App\StatesQuotesGr;
use App\User;
use App\UsersDatas;
use Intervention\Image\Facades\Image;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Mail;
use PhpOffice\PhpSpreadsheet\Writer\Pdf;

//use Mockery\Exception;


class InterventionCustomerBL
{

    public static $dirImageIntervention = '../storage/app/public/images/Intervention/';

    private static function getUrlOpenFileImg()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"];
    }

/**
     * getAllInterventionCustomerSearch
     * 
     * @param Request request
     * 
     */
    public static function getAllInterventionCustomerSearch(request $request){
        $result = collect();

        $content = '';
        $finalHtml = '';
        $strWhere = "";
        $SearchUserId = $request->idUser;
 
         $SearchId = $request->id;
         if ($SearchId != "") {
             $strWhere = $strWhere . ' AND gr_interventions.id = ' . $SearchId . '';
        }
        $NrDdtSearch = $request->nr_ddt;
        if ($NrDdtSearch != "") {
          $strWhere = $strWhere . ' AND gr_interventions.nr_ddt ilike \'%' . $NrDdtSearch . '%\'';
        }

        $PlantTypeSearch = $request->plant_type;
        if ($PlantTypeSearch != "") {
          $strWhere = $strWhere . ' AND gr_interventions.plant_type ilike \'%' . $PlantTypeSearch . '%\'';
        }
       
            foreach (DB::select(
                'SELECT gr_interventions.*
                FROM gr_interventions
                WHERE gr_interventions.user_id = :idUser ' . $strWhere,
                ['idUser' => $SearchUserId],
            ) as $InterventionCustomer) {

         $StatesDescription = GrStates::where('id', $InterventionCustomer->states_id)->first();
         $idInterventionQuotes = GrQuotes::where('id_intervention', $InterventionCustomer->id)->first();
         if (isset($idInterventionQuotes)) {
         $DescriptionGrStatesQuotes = StatesQuotesGr::where('id', $idInterventionQuotes->id_states_quote)->first();
         }
         $DtoInterventions = new DtoInterventions();        
            if (isset($InterventionCustomer->id)) {
                $DtoInterventions->id = $InterventionCustomer->id;
            }
            if (isset($InterventionCustomer->code_intervention_gr)) {
                $DtoInterventions->code_intervention_gr = $InterventionCustomer->code_intervention_gr;
            }
            
            if (isset($InterventionCustomer->date_ddt)) {
                $DtoInterventions->date_ddt = $InterventionCustomer->date_ddt;
            } else {
                $DtoInterventions->date_ddt = '-';
            }
             if (isset($InterventionCustomer->nr_ddt)) {
                $DtoInterventions->nr_ddt = $InterventionCustomer->nr_ddt;
            } else {
                $DtoInterventions->nr_ddt = '-';
            }
             if (isset($InterventionCustomer->description)) {
                $DtoInterventions->description = $InterventionCustomer->description;
            } else {
                $DtoInterventions->description = '-';
            }
              if (isset($InterventionCustomer->nr_ddt_gr)) {
                $DtoInterventions->nr_ddt_gr = $InterventionCustomer->nr_ddt_gr;
            } else {
                $DtoInterventions->nr_ddt_gr = '-';
            }
              if (isset($InterventionCustomer->date_ddt_gr)) {
                $DtoInterventions->date_ddt_gr = $InterventionCustomer->date_ddt_gr;
            } else {
                $DtoInterventions->date_ddt_gr = '-';
            }
            if (isset($InterventionCustomer->plant_type)) {
                $DtoInterventions->plant_type = $InterventionCustomer->plant_type;
            } else {
                $DtoInterventions->plant_type = '-';
            }
             if (isset($InterventionCustomer->defect)) {
                $DtoInterventions->defect = $InterventionCustomer->defect;
            } else {
                $DtoInterventions->defect = '-';
            }
        /*    if (isset($InterventionCustomer->imgmodule)) {
                $DtoInterventions->imgmodule = $InterventionCustomer->imgmodule;
            } else {
                $DtoInterventions->imgmodule = '-';
            }*/


          foreach (GrInterventionPhoto::where('intervention_id', $InterventionCustomer->id)->get() as $InterventionCaptures) {
            $DtoInterventionsPhotos = new DtoInterventionsPhotos();   
              if (file_exists(static::$dirImageIntervention . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.jpg' )) {
                  $DtoInterventionsPhotos->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.jpg';
              } else {
                if (file_exists(static::$dirImageIntervention . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.JPG' )) {
                  $DtoInterventionsPhotos->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.JPG';
                }else{
                    if ($InterventionCaptures->img != '' && !is_null($InterventionCaptures->img)) {
                      $pos = strpos($InterventionCaptures->img, $InterventionCaptures->intervention_id);
                      if($pos === false){
                        $DtoInterventionsPhotos->img = '';
                      }else{
                        $DtoInterventionsPhotos->img = $InterventionCaptures->img;
                      }
                  } else {
                      $DtoInterventionsPhotos->img = '';
                  }
                }  
            }
            
            $DtoInterventions->captures->push($DtoInterventionsPhotos);
        }


            if (isset($StatesDescription->description)) {
               // $DtoInterventions->states_id =  $StatesDescription->id;
                $DtoInterventions->states_description =  $StatesDescription->description;
            } else {
                $DtoInterventions->states_id = '-';
            }

        if (isset($idInterventionQuotes)) {
            if (isset($DescriptionGrStatesQuotes->description)) {
                //$DtoInterventions->id_states_quotes =  $idInterventionQuotes->id_states_quotes;
                $DtoInterventions->states_quotes_description =  $DescriptionGrStatesQuotes->description;
            } else {
                $DtoInterventions->states_quotes_description = '-';
            }
            }else{
          $DtoInterventions->states_quotes_description = '-';
           } 
            if (($InterventionCustomer->plant_type == '' || $InterventionCustomer->plant_type == null) ||
                ($InterventionCustomer->trolley_type == '' || $InterventionCustomer->trolley_type == null) || 
                ($InterventionCustomer->series == '' || $InterventionCustomer->series == null) || 
                ($InterventionCustomer->external_referent_id == '' || $InterventionCustomer->external_referent_id == null) || 
                ($InterventionCustomer->nr_ddt == '' || $InterventionCustomer->nr_ddt == null) || 
                ($InterventionCustomer->date_ddt == '' || $InterventionCustomer->date_ddt == null) || 
                ($InterventionCustomer->voltage == '' || $InterventionCustomer->voltage == null) || 
                ($InterventionCustomer->exit_notes == '' || $InterventionCustomer->exit_notes == null) ||
                ($InterventionCustomer->defect == '' || $InterventionCustomer->defect == null)) {
                $DtoInterventions->checkModuleFault = false;
            }else{
                    $DtoInterventions->checkModuleFault = true;
            }

                $ModuleValidationGuarantee = ModuleValidationGuarantee::where('id_intervention', $InterventionCustomer->id)->first();
                
                if (isset($ModuleValidationGuarantee)) { 
                
                        $DtoInterventions->checkModuleValidationGuarantee = true; 
                }else{
                        $DtoInterventions->checkModuleValidationGuarantee = false;   
                }

                $result->push($DtoInterventions);
        }
        return $result;
    }

  /**
     * getAllInterventionCustomer
     * 
     * @return DtoInterventions
     */
    public static function getAllInterventionCustomer($idUser)
    {
        $result = collect();

       foreach (GrIntervention::where('user_id', $idUser)->orderBy('code_intervention_gr', 'DESC')->get() as $InterventionCustomer) {

         $StatesDescription = GrStates::where('id', $InterventionCustomer->states_id)->first();
         $idInterventionQuotes = GrQuotes::where('id_intervention', $InterventionCustomer->id)->first();
         if (isset($idInterventionQuotes)) {
         $DescriptionGrStatesQuotes = StatesQuotesGr::where('id', $idInterventionQuotes->id_states_quote)->first();
         }

         $DtoInterventions = new DtoInterventions();        
            if (isset($InterventionCustomer->id)) {
                $DtoInterventions->id = $InterventionCustomer->id;
            }
            if (isset($InterventionCustomer->code_intervention_gr)) {
                $DtoInterventions->code_intervention_gr = $InterventionCustomer->code_intervention_gr;
            }
            if (isset($InterventionCustomer->date_ddt)) {
                $DtoInterventions->date_ddt = $InterventionCustomer->date_ddt;
            } else {
                $DtoInterventions->date_ddt = '-';
            }
             if (isset($InterventionCustomer->nr_ddt)) {
                $DtoInterventions->nr_ddt = $InterventionCustomer->nr_ddt;
            } else {
                $DtoInterventions->nr_ddt = '-';
            }
             if (isset($InterventionCustomer->description)) {
                $DtoInterventions->description = $InterventionCustomer->description;
            } else {
                $DtoInterventions->description = '-';
            }
              if (isset($InterventionCustomer->nr_ddt_gr)) {
                $DtoInterventions->nr_ddt_gr = $InterventionCustomer->nr_ddt_gr;
            } else {
                $DtoInterventions->nr_ddt_gr = '-';
            }
              if (isset($InterventionCustomer->date_ddt_gr)) {
                $DtoInterventions->date_ddt_gr = $InterventionCustomer->date_ddt_gr;
            } else {
                $DtoInterventions->date_ddt_gr = '-';
            }
            if (isset($InterventionCustomer->plant_type)) {
                $DtoInterventions->plant_type = $InterventionCustomer->plant_type;
            } else {
                $DtoInterventions->plant_type = '-';
            }
             if (isset($InterventionCustomer->defect)) {
                $DtoInterventions->defect = $InterventionCustomer->defect;
            } else {
                $DtoInterventions->defect = '-';
            }
           /* if (isset($InterventionCustomer->imgmodule)) {
                $DtoInterventions->imgmodule = $InterventionCustomer->imgmodule;
            } else {
                $DtoInterventions->imgmodule = '-';
            }*/


          foreach (GrInterventionPhoto::where('intervention_id', $InterventionCustomer->id)->get() as $InterventionCaptures) {
            $DtoInterventionsPhotos = new DtoInterventionsPhotos();   
              if (file_exists(static::$dirImageIntervention . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.jpg' )) {
                  $DtoInterventionsPhotos->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.jpg';
              } else {
                if (file_exists(static::$dirImageIntervention . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.JPG' )) {
                  $DtoInterventionsPhotos->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.JPG';
                }else{
                    if ($InterventionCaptures->img != '' && !is_null($InterventionCaptures->img)) {
                      $pos = strpos($InterventionCaptures->img, $InterventionCaptures->intervention_id);
                      if($pos === false){
                        $DtoInterventionsPhotos->img = '';
                      }else{
                        $DtoInterventionsPhotos->img = $InterventionCaptures->img;
                      }
                  } else {
                      $DtoInterventionsPhotos->img = '';
                  }
                }  
            }
            
            $DtoInterventions->captures->push($DtoInterventionsPhotos);
        }

            
            if (isset($StatesDescription->description)) {
               // $DtoInterventions->states_id =  $StatesDescription->id;
                $DtoInterventions->states_description =  $StatesDescription->description;
            } else {
                $DtoInterventions->states_id = '-';
            }

        if (isset($idInterventionQuotes)) {
            if (isset($DescriptionGrStatesQuotes->description)) {
                //$DtoInterventions->id_states_quotes =  $idInterventionQuotes->id_states_quotes;
                $DtoInterventions->states_quotes_description =  $DescriptionGrStatesQuotes->description;
            } else {
                $DtoInterventions->states_quotes_description = '-';
            }
            }else{
                $DtoInterventions->states_quotes_description = '-';
           } 

            if (isset($idInterventionQuotes)) { 
                    $DtoInterventions->checkQuotes = true; 
            }else{
                    $DtoInterventions->checkQuotes = false;   
            }

            if (($InterventionCustomer->plant_type == '' || $InterventionCustomer->plant_type == null) ||
                ($InterventionCustomer->trolley_type == '' || $InterventionCustomer->trolley_type == null) || 
                ($InterventionCustomer->series == '' || $InterventionCustomer->series == null) || 
                ($InterventionCustomer->external_referent_id == '' || $InterventionCustomer->external_referent_id == null) || 
                ($InterventionCustomer->nr_ddt == '' || $InterventionCustomer->nr_ddt == null) || 
                ($InterventionCustomer->date_ddt == '' || $InterventionCustomer->date_ddt == null) || 
                ($InterventionCustomer->voltage == '' || $InterventionCustomer->voltage == null) || 
                ($InterventionCustomer->exit_notes == '' || $InterventionCustomer->exit_notes == null) ||
                ($InterventionCustomer->defect == '' || $InterventionCustomer->defect == null)) {
                $DtoInterventions->checkModuleFault = false;
            }else{
                    $DtoInterventions->checkModuleFault = true;
            }

          /* if (($ModuleValidationGuarantee->plant_type == '' || $ModuleValidationGuarantee->plant_type == null) ||
                ($ModuleValidationGuarantee->trolley_type == '' || $ModuleValidationGuarantee->trolley_type == null) || 
                ($ModuleValidationGuarantee->date_mount == '' || $ModuleValidationGuarantee->date_mount == null) || 
                ($ModuleValidationGuarantee->id_intervention == '' || $ModuleValidationGuarantee->id_intervention == null) || 
                ($ModuleValidationGuarantee->voltage == '' || $ModuleValidationGuarantee->voltage == null) || 
                ($ModuleValidationGuarantee->insulation_value_tewards_machine_frame_positive == '' || $ModuleValidationGuarantee->insulation_value_tewards_machine_frame_positive == null) || 
                ($ModuleValidationGuarantee->insulation_value_tewards_machine_frame_negative == '' || $ModuleValidationGuarantee->insulation_value_tewards_machine_frame_negative == null)) {
                $DtoInterventions->checkModuleValidationGuarantee = false;
            }else{
                    $DtoInterventions->checkModuleValidationGuarantee = true;
            }*/
             $ModuleValidationGuarantee = ModuleValidationGuarantee::where('id_intervention', $InterventionCustomer->id)->first();
            if (isset($ModuleValidationGuarantee)) { 
               
                    $DtoInterventions->checkModuleValidationGuarantee = true; 
            }else{
                    $DtoInterventions->checkModuleValidationGuarantee = false;   
            }
        
                $result->push($DtoInterventions);
        }
        return $result;
    }
    #endregion getAllInterventionCustomer



 /**
     * Get by getByViewModuleCustomer
     * 
     * @param int id
     * @return DtoInterventions
     */
    public static function getByViewModuleCustomer(int $id)
    {
        $Intervention = GrIntervention::where('id', $id)->first();

         $DtoInterventions = new DtoInterventions();
  
            $DtoInterventions->id = $Intervention->id;
             if (isset($Intervention->description)) {
                $DtoInterventions->description = $Intervention->description;
            }
            if (isset($Intervention->defect)) {
                $DtoInterventions->defect = $Intervention->defect;
            }
            if (isset($Intervention->nr_ddt)) {
                $DtoInterventions->nr_ddt = $Intervention->nr_ddt;
            }
            if (isset($Intervention->idLanguage)) {
                $DtoInterventions->idLanguage = $Intervention->idLanguage;
            }
            if (isset($Intervention->date_ddt)) {
                $DtoInterventions->date_ddt = $Intervention->date_ddt;
            }
       
            if (isset($Intervention->create_quote)) {
                $DtoInterventions->create_quote = $Intervention->create_quote;
            }

            if (isset($Intervention->external_referent_id)) {
                $DtoInterventions->external_referent_id = $Intervention->external_referent_id;
                $DtoInterventions->DescriptionReferent = People::where('id', $Intervention->external_referent_id)->first()->name;
            }

             if (isset($Intervention->external_referent_id)) {
                $DtoInterventions->phone_number = People::where('id', $Intervention->external_referent_id)->first()->phone_number;
            }
               
             if (isset($Intervention->plant_type)) {
                $DtoInterventions->plant_type = $Intervention->plant_type;
            }
             if (isset($Intervention->trolley_type)) {
                $DtoInterventions->trolley_type = $Intervention->trolley_type;
            }
             if (isset($Intervention->series)) {
                $DtoInterventions->series = $Intervention->series;
            }
            if (isset($Intervention->voltage)) {
                $DtoInterventions->voltage = $Intervention->voltage;
            }
            if (isset($Intervention->exit_notes)) {
                $DtoInterventions->exit_notes = $Intervention->exit_notes;
            }
             if (isset($Intervention->additional_notes)) {
                $DtoInterventions->additional_notes = $Intervention->additional_notes;
            }
            
        return $DtoInterventions;
    }

 /**
     * Get by getByViewQuotesCustomer
     * 
     * @param int id
     * @return DtoInterventions
     */
    public static function getByViewQuotesCustomer(int $id)
    {
        $idIntervention = GrIntervention::where('id', $id)->first();
        $DtoQuotes = new DtoQuotes();

       // $DtoQuotes->id = $id;

        if (isset($idIntervention->external_referent_id)) {
            $DtoQuotes->Description_People = People::where('id', $idIntervention->external_referent_id)->first()->name;
        }
        if (isset($idIntervention->description)) {
            $DtoQuotes->description = $idIntervention->description;
        }
        if (isset($idIntervention->date_ddt)) {
            $DtoQuotes->date_ddt = $idIntervention->date_ddt;
        }
        if (isset($idIntervention->nr_ddt)) {
            $DtoQuotes->nr_ddt = $idIntervention->nr_ddt;
        }
            $DtoQuotes->email = User::where('id', $idIntervention->user_id)->first()->email;

            $UsersDatas = UsersDatas::where('user_id', $idIntervention->user_id)->first()->business_name;
            if (isset($UsersDatas)) {
                $DtoQuotes->id_customer = $UsersDatas;
            }else{
                $DtoQuotes->id_customer = ''; 
            }


        foreach (GrInterventionProcessing::where('id_intervention', $id)->get() as $InterventionProcessing) {
            $DtoInterventionProcessing = new DtoInterventionsProcessing();
            if (isset($InterventionProcessing->id_gr_items)) {
                $DtoInterventionProcessing->code_gr = ItemGr::where('id', $InterventionProcessing->id_gr_items)->first()->code_gr;
            }
            $DtoInterventionProcessing->description = $InterventionProcessing->description;
            $DtoQuotes->Articles->push($DtoInterventionProcessing);
        }

        $IdQuotes = GrQuotes::where('id_intervention', $id)->first();
        if (isset($IdQuotes->email)) {
            $DtoQuotes->email = $IdQuotes->email;
        }
        if (isset($IdQuotes->object)) {
            $DtoQuotes->object = $IdQuotes->object;
        }
        if (isset($IdQuotes->date_agg)) {
            $DtoQuotes->date_agg = $IdQuotes->date_agg;
        }

        if (isset($IdQuotes->note_before_the_quote)) {
            $DtoQuotes->note_before_the_quote = $IdQuotes->note_before_the_quote;
        }
        if (isset($IdQuotes->description_payment)) {
            $DtoQuotes->description_payment = $IdQuotes->description_payment;
            $DtoQuotes->Description_payment = PaymentGr::where('id', $IdQuotes->description_payment)->first()->description;
        }
        if (isset($IdQuotes->id_states_quote)) {
            $DtoQuotes->id_states_quote = $IdQuotes->id_states_quote;
            $DtoQuotes->States_Description = StatesQuotesGr::where('id', $IdQuotes->id_states_quote)->first()->description;
        }
        if (isset($IdQuotes->note)) {
            $DtoQuotes->note = $IdQuotes->note;
        }
        if (isset($IdQuotes->note_customer)) {
            $DtoQuotes->note_customer = $IdQuotes->note_customer;
        }

        if (isset($IdQuotes->id_intervention)) {
            $DtoQuotes->id = $IdQuotes->id_intervention;
        }

        //visualizzazione totale preventivo interfaccia utente InterventionCustomerDB:
         if (isset($IdQuotes->tot_quote)) {
            $DtoQuotes->tot_quote_customer = $IdQuotes->tot_quote;
        }
         if (isset($IdQuotes->discount)) {
            $DtoQuotes->discount = $IdQuotes->discount;
        }
        if (isset($IdQuotes->total_amount_with_discount)) {
            $DtoQuotes->total_amount_with_discount = $IdQuotes->total_amount_with_discount;
        }

        $messages = QuotesMessages::where('id_intervention', $IdQuotes->id_intervention)->get();
        if (isset($messages)) {
            foreach ($messages as $MessageDetail) {
                $DtoQuotesMessages= new DtoQuotesMessages();
                $DtoQuotesMessages->id = $MessageDetail->id;

                if (isset($MessageDetail->id_sender)) {  
                    $DtoQuotesMessages->id_sender_id = $MessageDetail->id_sender;
                    $DtoQuotesMessages->id_sender = User::where('id', $MessageDetail->id_sender)->first()->name;
                }else{
                    $DtoQuotesMessages->id_sender = '';  
                }
                if (isset($MessageDetail->id_receiver)) {  
                    $DtoQuotesMessages->id_receiver_id = $MessageDetail->id_receiver;
                    $DtoQuotesMessages->id_receiver =  User::where('id', $MessageDetail->id_receiver)->first()->name;
                }else{  
                    $DtoQuotesMessages->id_receiver = '';  
                }
                $DtoQuotesMessages->messages = $MessageDetail->message;
                $DtoQuotesMessages->id_intervention = $MessageDetail->id_intervention;
                $DtoQuotesMessages->created_at = date_format($MessageDetail->created_at, 'd/m/Y h:m:s');
                $DtoQuotes->Messages->push($DtoQuotesMessages);
         }
    }
    

        return $DtoQuotes;
    }


        public static function updateInterventionUpdateViewCustomerQuotes(Request $DtoQuotes)
    {
     
       $GrStatesQuotes = GrQuotes::where('id_intervention', $DtoQuotes->id_intervention)->first();
        DB::beginTransaction();
        try {

            $GrStatesQuotes->id_intervention = $DtoQuotes->id_intervention;
            $GrStatesQuotes->updated_id = $DtoQuotes->updated_id; 
            $GrStatesQuotes->id_states_quote = $DtoQuotes->id_states_quote;
            $GrStatesQuotes->note_customer = $DtoQuotes->note_customer;
            
            $GrStatesQuotes->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

        //getByViewModuleValidationGuarantee

     /**
     * Get by getByViewModuleValidationGuarantee
     * 
     * @param int id
     * @return DtoModuleValidationGuarantee
     */
    public static function getByViewModuleValidationGuarantee(int $id)
    {
        $ModuleValidationGuarantee = ModuleValidationGuarantee::where('id_intervention', $id)->first();
        $idIntervention = GrIntervention::where('id', $id)->first();

        $DtoModuleValidationGuarantee = new DtoModuleValidationGuarantee();

             if (isset($idIntervention->external_referent_id)) {
            $DtoModuleValidationGuarantee->external_referent_id = $idIntervention->external_referent_id;
            $DtoModuleValidationGuarantee->DescriptionReferent = People::where('id', $idIntervention->external_referent_id)->first()->name;
            }
            if (isset($ModuleValidationGuarantee->date_mount)) {
                $DtoModuleValidationGuarantee->date_mount = $ModuleValidationGuarantee->date_mount;
            }
            if (isset($ModuleValidationGuarantee->id_user)) {
                 $DtoModuleValidationGuarantee->id_user = UsersDatas::where('user_id', $ModuleValidationGuarantee->id_user)->first()->name;
            }
             if (isset($ModuleValidationGuarantee->id)) {
                $DtoModuleValidationGuarantee->id = $ModuleValidationGuarantee->id;
            }
            if (isset($idIntervention->id)) {
                $DtoModuleValidationGuarantee->id_intervention = $idIntervention->id;
            }
            if (isset($idIntervention->trolley_type)) {
                $DtoModuleValidationGuarantee->trolley_type = $idIntervention->trolley_type;
            }
             if (isset($idIntervention->plant_type)) {
                $DtoModuleValidationGuarantee->plant_type = $idIntervention->plant_type;
            }
             if (isset($idIntervention->voltage)) {
                $DtoModuleValidationGuarantee->voltage = $idIntervention->voltage;
            }
             if (isset($ModuleValidationGuarantee->traction)) {
                $DtoModuleValidationGuarantee->traction = $ModuleValidationGuarantee->traction;
            }
             if (isset($ModuleValidationGuarantee->lift)) {
                $DtoModuleValidationGuarantee->lift = $ModuleValidationGuarantee->lift;
            }
             if (isset($ModuleValidationGuarantee->insulation_value_tewards_machine_frame_positive)) {
                $DtoModuleValidationGuarantee->insulation_value_tewards_machine_frame_positive = $ModuleValidationGuarantee->insulation_value_tewards_machine_frame_positive;
            }
             if (isset($ModuleValidationGuarantee->insulation_value_tewards_machine_frame_negative)) {
                $DtoModuleValidationGuarantee->insulation_value_tewards_machine_frame_negative = $ModuleValidationGuarantee->insulation_value_tewards_machine_frame_negative;
            }
             if (isset($ModuleValidationGuarantee->single_motor_with_traction_motor_range)) {
                $DtoModuleValidationGuarantee->single_motor_with_traction_motor_range = $ModuleValidationGuarantee->single_motor_with_traction_motor_range;
            }
             if (isset($ModuleValidationGuarantee->single_motor_with_brush_traction_motor)) {
                $DtoModuleValidationGuarantee->single_motor_with_brush_traction_motor = $ModuleValidationGuarantee->single_motor_with_brush_traction_motor;
            }

            if (isset($ModuleValidationGuarantee->single_motor_u_v_w_traction_phase_motor)) {
                $DtoModuleValidationGuarantee->single_motor_u_v_w_traction_phase_motor = $ModuleValidationGuarantee->single_motor_u_v_w_traction_phase_motor;
            }
            if (isset($ModuleValidationGuarantee->single_motor_u_v_w_motor_lifting_phase)) {
                $DtoModuleValidationGuarantee->single_motor_u_v_w_motor_lifting_phase = $ModuleValidationGuarantee->single_motor_u_v_w_motor_lifting_phase;
            }
            if (isset($ModuleValidationGuarantee->single_motor_with_brush_traction_motor)) {
                $DtoModuleValidationGuarantee->single_motor_with_brush_traction_motor = $ModuleValidationGuarantee->single_motor_with_brush_traction_motor;
            }
            if (isset($ModuleValidationGuarantee->bi_engine_with_traction_motor_range)) {
                $DtoModuleValidationGuarantee->bi_engine_with_traction_motor_range = $ModuleValidationGuarantee->bi_engine_with_traction_motor_range;
            }
               if (isset($ModuleValidationGuarantee->bi_engine_u_v_w_traction_phase_motor)) {
                $DtoModuleValidationGuarantee->bi_engine_u_v_w_traction_phase_motor = $ModuleValidationGuarantee->bi_engine_u_v_w_traction_phase_motor;
            }
               if (isset($ModuleValidationGuarantee->bi_engine_u_v_w_motor_lifting_phase)) {
                $DtoModuleValidationGuarantee->bi_engine_u_v_w_motor_lifting_phase = $ModuleValidationGuarantee->bi_engine_u_v_w_motor_lifting_phase;
            }

               if (isset($ModuleValidationGuarantee->lift_with_field_brush_motor_raised)) {
                $DtoModuleValidationGuarantee->lift_with_field_brush_motor_raised = $ModuleValidationGuarantee->lift_with_field_brush_motor_raised;
            }

               if (isset($ModuleValidationGuarantee->lift_with_u_v_w_motor_lift_pharse)) {
                $DtoModuleValidationGuarantee->lift_with_u_v_w_motor_lift_pharse = $ModuleValidationGuarantee->lift_with_u_v_w_motor_lift_pharse;
            }
               if (isset($ModuleValidationGuarantee->battery_voltage)) {
                $DtoModuleValidationGuarantee->battery_voltage = $ModuleValidationGuarantee->battery_voltage;
            }
                if (isset($ModuleValidationGuarantee->traction_limit_current_consumption)) {
                $DtoModuleValidationGuarantee->traction_limit_current_consumption = $ModuleValidationGuarantee->traction_limit_current_consumption;
            }
                if (isset($ModuleValidationGuarantee->bi_engine_with_brush_traction_motor)) {
                $DtoModuleValidationGuarantee->bi_engine_with_brush_traction_motor = $ModuleValidationGuarantee->bi_engine_with_brush_traction_motor;
            }
              if (isset($ModuleValidationGuarantee->lift_limit_current_absorption)) {
                $DtoModuleValidationGuarantee->lift_limit_current_absorption = $ModuleValidationGuarantee->lift_limit_current_absorption;
            }
             if (isset($ModuleValidationGuarantee->note)) {
                $DtoModuleValidationGuarantee->note = $ModuleValidationGuarantee->note;
            }

        return $DtoModuleValidationGuarantee;
    }
   


public static function updateSendFaultModule(Request $request)
    {
        $ModuleFault = GrIntervention::where('id', $request->id)->first();

         DB::beginTransaction();
        try {

            if (isset($request->additional_notes)) {
                $ModuleFault->additional_notes = $request->additional_notes;
            }
            if (isset($request->nr_ddt)) {
                $ModuleFault->nr_ddt = $request->nr_ddt;
            }
            if (isset($request->date_ddt)) {
                $ModuleFault->date_ddt = $request->date_ddt;
            }
            if (isset($request->trolley_type)) {
                $ModuleFault->trolley_type = $request->trolley_type;
            }
             if (isset($request->series)) {
                $ModuleFault->series = $request->series;
            }
            if (isset($request->voltage)) {
                $ModuleFault->voltage = $request->voltage;
            }
            if (isset($request->plant_type)) {
                $ModuleFault->plant_type = $request->plant_type;
            }
            if (isset($request->defect)) {
                $ModuleFault->defect = $request->defect;
            }
            if (isset($request->exit_notes)) {
                $ModuleFault->exit_notes = $request->exit_notes;
            }
              if (isset($request->external_referent_id)) {
                $ModuleFault->external_referent_id = $request->external_referent_id;
            }

            $ModuleFault->save();   

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
  
    
    public static function insertMessageCustomerQuotes(Request $request)
    { 
        DB::beginTransaction();
 
         try {
             $QuotesMessages= new QuotesMessages();
           

            if (isset($request->id_sender)) {
                $QuotesMessages->id_sender = $request->id_sender;
            } 
            if (isset($request->id_intervention)) {
                $QuotesMessages->id_intervention = $request->id_intervention;
            } 
            if (isset($request->messages)) {
                $QuotesMessages->message = $request->messages;
            } 

            $grquotes= GrQuotes::where('id_intervention', $request->id_intervention)->first();
            if (isset($grquotes)) {
                $QuotesMessages->id_receiver = $grquotes->created_id; 
                if (isset($grquotes->code_intervention_gr)) {
                    $QuotesMessages->id_code_intervention_gr_librone = $grquotes->code_intervention_gr; 
                }   
            }
            $QuotesMessages->save();

            $user = User::where('id', $QuotesMessages->id_receiver)->first(); 
            $id_intervention = $request->id_intervention;
            $id_receiver = User::where('id',  $QuotesMessages->id_receiver)->first()->name;
            $id_sender =  User::where('id', $request->id_sender)->first()->name;
            $created_at = QuotesMessages::where('message', $request->messages)->first()->created_at;
            $message = $request->messages;
    
                $emailFrom = SettingBL::getContactsEmail();

                    $textMail= MessageBL::getByCode('ORDER_CONFIRM_MESSAGE_MAIL_TEXT');
                    $textObject= MessageBL::getByCode('ORDER_CONFIRM_MESSAGE_MAIL_OBJECT');
                    
                    $textObject =str_replace ('#id_sender#', $id_sender, $textObject); 
                    $textMail =str_replace ('#id_intervention#', $id_intervention, $textMail); 
                    $textMail =str_replace ('#id_receiver#', $id_receiver, $textMail); 
                    $textMail =str_replace ('#id_sender#', $id_sender, $textMail); 
                    $textMail =str_replace ('#created_at#', $created_at, $textMail); 
                    $textMail =str_replace ('#message#', $message, $textMail); 
    
                    Mail::to($user->email)->send(new DynamicMail($textObject,$textMail));
      
             DB::commit();
         } catch (Exception $ex) {
             DB::rollback();
             throw $ex;
         }
 
         return $QuotesMessages->id;
     }
 

  public static function updateModuleValidationGuarantee(Request $DtoModuleValidationGuarantee)
    {
     
        $ModuleValidationGuarantees = ModuleValidationGuarantee::where('id_intervention', $DtoModuleValidationGuarantee->id_intervention)->first();
        $Intervention = GrIntervention::where('id', $DtoModuleValidationGuarantee->id_intervention)->first();
        DB::beginTransaction();
        try {

             $ModuleValidationGuarantee = new ModuleValidationGuarantee();

        if (is_null($DtoModuleValidationGuarantee->id)) { 
            if (isset($DtoModuleValidationGuarantee->id_intervention)) {
                $ModuleValidationGuarantee->id_intervention = $DtoModuleValidationGuarantee->id_intervention;
            }
            if (isset($DtoModuleValidationGuarantee->id_user)) {
                $ModuleValidationGuarantee->id_user = $DtoModuleValidationGuarantee->id_user;
            }
             if (isset($DtoModuleValidationGuarantee->updated_id)) {
                $ModuleValidationGuarantee->updated_id = $DtoModuleValidationGuarantee->updated_id;
            }
            if (isset($DtoModuleValidationGuarantee->external_referent_id)) {
                $Intervention->external_referent_id = $DtoModuleValidationGuarantee->external_referent_id;
                $ModuleValidationGuarantee->external_referent_id = $DtoModuleValidationGuarantee->external_referent_id;
            }else{
                $Intervention->external_referent_id = NULL;
            }

              $ModuleValidationGuarantee->referent = NULL;

              if (isset($DtoModuleValidationGuarantee->created_at)) {
                $ModuleValidationGuarantee->created_at = $DtoModuleValidationGuarantee->created_at;
            }
             if (isset($DtoModuleValidationGuarantee->date_mount)) {
                $ModuleValidationGuarantee->date_mount = $DtoModuleValidationGuarantee->date_mount;
            }
              if (isset($DtoModuleValidationGuarantee->trolley_type)) {
                $ModuleValidationGuarantee->trolley_type = $DtoModuleValidationGuarantee->trolley_type;
            }
             if (isset($DtoModuleValidationGuarantee->plant_type)) {
                $ModuleValidationGuarantee->plant_type = $DtoModuleValidationGuarantee->plant_type;
            }
             if (isset($DtoModuleValidationGuarantee->voltage)) {
                $ModuleValidationGuarantee->voltage = $DtoModuleValidationGuarantee->voltage;
            }
             if (isset($DtoModuleValidationGuarantee->traction)) {
                $ModuleValidationGuarantee->traction = $DtoModuleValidationGuarantee->traction;
            }
              if (isset($DtoModuleValidationGuarantee->lift)) {
                $ModuleValidationGuarantee->lift = $DtoModuleValidationGuarantee->lift;
            }
             if (isset($DtoModuleValidationGuarantee->insulation_value_tewards_machine_frame_positive)) {
                $ModuleValidationGuarantee->insulation_value_tewards_machine_frame_positive = $DtoModuleValidationGuarantee->insulation_value_tewards_machine_frame_positive;
            }
              if (isset($DtoModuleValidationGuarantee->single_motor_with_traction_motor_range)) {
                $ModuleValidationGuarantee->single_motor_with_traction_motor_range = $DtoModuleValidationGuarantee->single_motor_with_traction_motor_range;
            }
              if (isset($DtoModuleValidationGuarantee->insulation_value_tewards_machine_frame_positive)) {
                $ModuleValidationGuarantee->insulation_value_tewards_machine_frame_negative = $DtoModuleValidationGuarantee->insulation_value_tewards_machine_frame_negative;
            }
              if (isset($DtoModuleValidationGuarantee->single_motor_with_brush_traction_motor)) {
                $ModuleValidationGuarantee->single_motor_with_brush_traction_motor = $DtoModuleValidationGuarantee->single_motor_with_brush_traction_motor;
            }
                if (isset($DtoModuleValidationGuarantee->single_motor_u_v_w_traction_phase_motor)) {
                $ModuleValidationGuarantee->single_motor_u_v_w_traction_phase_motor = $DtoModuleValidationGuarantee->single_motor_u_v_w_traction_phase_motor;
            }
                if (isset($DtoModuleValidationGuarantee->single_motor_u_v_w_motor_lifting_phase)) {
                $ModuleValidationGuarantee->single_motor_u_v_w_motor_lifting_phase = $DtoModuleValidationGuarantee->single_motor_u_v_w_motor_lifting_phase;
            }
              if (isset($DtoModuleValidationGuarantee->bi_engine_with_traction_motor_range)) {
                $ModuleValidationGuarantee->bi_engine_with_traction_motor_range = $DtoModuleValidationGuarantee->bi_engine_with_traction_motor_range;
            }
              if (isset($DtoModuleValidationGuarantee->bi_engine_with_brush_traction_motor)) {
                $ModuleValidationGuarantee->bi_engine_with_brush_traction_motor = $DtoModuleValidationGuarantee->bi_engine_with_brush_traction_motor;
            }
               if (isset($DtoModuleValidationGuarantee->bi_engine_u_v_w_traction_phase_motor)) {
                $ModuleValidationGuarantee->bi_engine_u_v_w_traction_phase_motor = $DtoModuleValidationGuarantee->bi_engine_u_v_w_traction_phase_motor;
            }
              if (isset($DtoModuleValidationGuarantee->bi_engine_u_v_w_motor_lifting_phase)) {
                $ModuleValidationGuarantee->bi_engine_u_v_w_motor_lifting_phase = $DtoModuleValidationGuarantee->bi_engine_u_v_w_motor_lifting_phase;
            }
                 if (isset($DtoModuleValidationGuarantee->lift_with_field_brush_motor_raised)) {
                $ModuleValidationGuarantee->lift_with_field_brush_motor_raised = $DtoModuleValidationGuarantee->lift_with_field_brush_motor_raised;
            }
                 if (isset($DtoModuleValidationGuarantee->lift_with_u_v_w_motor_lift_pharse)) {
                $ModuleValidationGuarantee->lift_with_u_v_w_motor_lift_pharse = $DtoModuleValidationGuarantee->lift_with_u_v_w_motor_lift_pharse;
            }
                    if (isset($DtoModuleValidationGuarantee->battery_voltage)) {
                $ModuleValidationGuarantee->battery_voltage = $DtoModuleValidationGuarantee->battery_voltage;
            }
                    if (isset($DtoModuleValidationGuarantee->traction_limit_current_consumption)) {
                $ModuleValidationGuarantee->traction_limit_current_consumption = $DtoModuleValidationGuarantee->traction_limit_current_consumption;
            }
                    if (isset($DtoModuleValidationGuarantee->lift_limit_current_absorption)) {
                $ModuleValidationGuarantee->lift_limit_current_absorption = $DtoModuleValidationGuarantee->lift_limit_current_absorption;
            }
            $ModuleValidationGuarantee->save();
}

                if (isset($DtoModuleValidationGuarantee->id)) { 
                $ModuleValidationGuarantees->note = $DtoModuleValidationGuarantee->note;
                $ModuleValidationGuarantees->save();
                } 
                
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

 } 


<?php

namespace App\BusinessLogic;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoSendSms;
use App\DtoModel\DtoListGroupSms;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\sendSms;
use App\ItemGroupTechnicalSpecification;
use App\RecipientSms;
use App\Sms;
use App\Contact;
use App\SelenaSqlViews;
use App\User;
use App\UsersDatas;
use Intervention\Image\Facades\Image;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
//use Mockery\Exception;

class sendSmsBL
{
#region GET

/**
     * getnumbertelephone
     * 
     * @param int id
     * @return DtoSms
     */
    public static function getnumbertelephone(int $id)
    {
        $UsersDatas = Contact::where('id', $id)->first();

        $DtoSendSms = new DtoSendSms();
        if(isset($UsersDatas)){
            if(isset($UsersDatas->phone_number)){
                $DtoSendSms->telephone_number_select_contact = $UsersDatas->phone_number;
            }else{
                throw new Exception('Numero non trovato', HttpResultsCodesEnum::InvalidPayload);
            }
            if(isset($UsersDatas->name)){
                $DtoSendSms->name = $UsersDatas->name;
            }else{
               $DtoSendSms->name = '-';
            }
            if(isset($UsersDatas->surname)){
                $DtoSendSms->surname = $UsersDatas->surname;
            }else{
              $DtoSendSms->surname = '-';   
            }
            if(isset($UsersDatas->business_name)){
                $DtoSendSms->business_name = $UsersDatas->business_name;
            }else{
                $DtoSendSms->business_name = '-';              
            }
             $DtoSendSms->visible = true;

            }else{
                throw new Exception('Contatto non presente', HttpResultsCodesEnum::InvalidPayload);
            }  
        return $DtoSendSms;  
    }


    public static function getListContactByUser(int $id)
    {
        $UsersDatas = UsersDatas::where('user_id', $id)->first();
        $DtoSendSms = new DtoSendSms();
        if(isset($UsersDatas)){
            if(isset($UsersDatas->telephone_number)){
                $DtoSendSms->telephone_number_user_select_contact = $UsersDatas->telephone_number;
            }else{
                throw new Exception('Numero non trovato', HttpResultsCodesEnum::InvalidPayload);
            }
            if(isset($UsersDatas->name)){
                $DtoSendSms->name = $UsersDatas->name;
            }else{
                $DtoSendSms->name = '-';
            }
            if(isset($UsersDatas->surname)){
                $DtoSendSms->surname = $UsersDatas->surname;
            }else{
                $DtoSendSms->surname = '-';
            }
            if(isset($UsersDatas->business_name)){
                $DtoSendSms->business_name = $UsersDatas->business_name;
            }else{
                $DtoSendSms->business_name = '-';
            }
             $DtoSendSms->visible = true;

        }else{
            throw new Exception('Contatto non presente', HttpResultsCodesEnum::InvalidPayload);
        }
        return $DtoSendSms;  
    }

    public static function getListTelephoneQueryGroup(int $id)
    {
         $TelephoneNumberQueryGroupSelenaSqlView = SelenaSqlViews::where('id', $id)->first();
        if (isset($TelephoneNumberQueryGroupSelenaSqlView)) {
            $QueryResult = "";
            if (isset($TelephoneNumberQueryGroupSelenaSqlView->select)) {
                $QueryResult = $QueryResult . 'SELECT ' . $TelephoneNumberQueryGroupSelenaSqlView->select;
                if (isset($TelephoneNumberQueryGroupSelenaSqlView->from)) {
                    $QueryResult = $QueryResult . ' FROM ' . $TelephoneNumberQueryGroupSelenaSqlView->from;

                    if (isset($TelephoneNumberQueryGroupSelenaSqlView->where)) {
                        $QueryResult = $QueryResult . ' WHERE ' . $TelephoneNumberQueryGroupSelenaSqlView->where;
                    }

                    if (isset($TelephoneNumberQueryGroupSelenaSqlView->order_by)) {
                        $QueryResult = $QueryResult . ' ORDER BY ' . $TelephoneNumberQueryGroupSelenaSqlView->order_by;
                    }

                    if (isset($TelephoneNumberQueryGroupSelenaSqlView->group_by)) {
                        $QueryResult = $QueryResult . ' GROUP BY ' . $TelephoneNumberQueryGroupSelenaSqlView->group_by;
                    }
                }
            }            
        }
                    $rowsResult = DB::select($QueryResult);
            

        $result = collect();
        if (isset($rowsResult)) {
          foreach ($rowsResult as $RowsResult) { 
              $DtoSendSms = new DtoListGroupSms();
              if (isset($RowsResult->telephone_number)) {
                    $DtoSendSms->telephone_number = $RowsResult->telephone_number;
              }else{
                if (isset($RowsResult->phone_number)) {
                    $DtoSendSms->telephone_number = $RowsResult->phone_number;  
              }else{
                    $DtoSendSms->telephone_number = '-';
              } 
            }
              if (isset($RowsResult->name)) {
                    $DtoSendSms->name = $RowsResult->name;  
              }else{
                    $DtoSendSms->name = '-';
              } 
              if (isset($RowsResult->surname)) {
                    $DtoSendSms->surname = $RowsResult->surname;  
              }else{
                    $DtoSendSms->surname = '-';
              } 
               if (isset($RowsResult->business_name)) {
                    $DtoSendSms->business_name = $RowsResult->business_name;  
              }else{
                    $DtoSendSms->business_name = '-';
              } 
               $DtoSendSms->visible = true;
        
              $result->push($DtoSendSms);    
        }

       return  $result;
        } else {
            return '';
        }    
    }

    /**
     * Get list contact by group id
     * 
     * @param int id
     * @return DtoSms
     */
    public static function getListContactByGroup(int $id)
    {
        $result = collect();
        
        foreach (DB::select(
            'SELECT c.phone_number, c.name, c.surname, c.business_name
            FROM contacts_groups_newsletters cgn
            INNER JOIN contacts c ON cgn.contact_id = c.id
            WHERE cgn.group_newsletter_id = :idContact',
            ['idContact' => $id]
        ) as $groupSms) {
            if(isset($groupSms)){
                $DtoSendSms = new DtoListGroupSms();

                 if(isset($groupSms->phone_number)){
                    $DtoSendSms->telephone_number = $groupSms->phone_number;
                }else{
                    $DtoSendSms->telephone_number = '-';
                }

                if(isset($groupSms->name)){
                    $DtoSendSms->name = $groupSms->name;
                }else{
                 $DtoSendSms->name = '-';
                }
                if(isset($groupSms->surname)){
                    $DtoSendSms->surname = $groupSms->surname;
                }else{
                 $DtoSendSms->surname = '-';
                }
                if(isset($groupSms->business_name)){
                    $DtoSendSms->business_name = $groupSms->business_name;
                }else{
                 $DtoSendSms->business_name = '-';
                }
                 $DtoSendSms->visible = true;

                $result->push($DtoSendSms);
            }
        }

        foreach (DB::select(
            'SELECT  c.mobile_number,c.name,c.surname, c.business_name
            FROM customers_groups_newsletters cgn
            INNER JOIN users u ON cgn.customer_id = u.id
            INNER JOIN users_datas c ON c.user_id = u.id
            WHERE cgn.group_newsletter_id = :idContact',
            ['idContact' => $id]
        ) as $groupNewsletter) {
            if(isset($groupNewsletter)){
                $DtoSendSms = new DtoListGroupSms();

                 if(isset($groupNewsletter->mobile_number)){
                    $DtoSendSms->telephone_number = $groupNewsletter->mobile_number;
                }else{
                 $DtoSendSms->telephone_number = '-';
                }
                  if(isset($groupNewsletter->name)){
                    $DtoSendSms->name = $groupNewsletter->name;
                }else{
                 $DtoSendSms->name = '-';
                }
                if(isset($groupNewsletter->surname)){
                    $DtoSendSms->surname = $groupNewsletter->surname;
                }else{
                 $DtoSendSms->surname = '-';
                }
                if(isset($groupNewsletter->business_name)){
                    $DtoSendSms->business_name = $groupNewsletter->business_name;
                }else{
                 $DtoSendSms->business_name = '-';
                }
                 $DtoSendSms->visible = true;
                 
                $result->push($DtoSendSms);
            }
        }
        return $result;  
    }


  /**
     * insert SMS
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertSms(Request $request)
    {
        DB::beginTransaction();

        try {
            $Sms = new Sms();
            
            if (isset($request->title)) {
                $Sms->title = $request->title;
            }
            if (isset($request->text_message)) {
                $Sms->text_message = $request->text_message;
            }
            
            if (isset($request->date_send_sms)) {
                $Sms->date_send_sms = $request->date_send_sms;
            }
            if(isset($request->order_id_skebby)){
                $Sms->order_id_skebby =  $request->order_id_skebby;
            }
            $Sms->send = false;
            $Sms->save();

            foreach ($request->res as $res) {
                $RecipientSms = new RecipientSms();
                $RecipientSms->id_sms = $Sms->id;
                
                if (isset($res) && !is_null($res)) {
                    $RecipientSms->telephone_number = $res['numero'];
                    $RecipientSms->name = $res['name'];
                    $RecipientSms->surname = $res['surname'];
                    $RecipientSms->business_name = $res['business_name'];
                }
                    
                $RecipientSms->save();
            }



            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $Sms->id;
    }


  /**
     * Get all
     * 
     * @return DtoItems
     */
    public static function getAll()
    {
        $result = collect();

        foreach (Sms::orderBy('date_send_sms', 'DESC')->get() as $Sms) {
            
           $RecipientSms= RecipientSms::where('id_sms',$Sms->id)->first();

           $DtoSendSms = new DtoSendSms();    
             if (isset($Sms->id)) {
                $DtoSendSms->id = $Sms->id;
            }
            if (isset($Sms->title)) {
                $DtoSendSms->title = $Sms->title;
            }
            if (isset($Sms->text_message)) {
                $DtoSendSms->text_message = $Sms->text_message;
            }
            //$DtoSendSms->send = $Sms->send;
            $DtoSendSms->order_id_skebby = $Sms->order_id_skebby;
            $DtoSendSms->date_send_sms = $Sms->date_send_sms;
            $DtoSendSms->telephone_number = RecipientSms::where('id_sms',$Sms->id)->get()->count();
            
           // $diff = strtotime(Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $Sms->date_send_sms)->format('m/d/Y H:i:s')) < strtotime(date('m/d/Y H:i:s'));
            if(isset($diff) && $diff == 1){
                $DtoSendSms->result_sms = 'Inviato';
                $DtoSendSms->send = true;
            }else{
                $DtoSendSms->send = false;
                $DtoSendSms->result_sms = 'In attesa di invio';
            }
            

            foreach (RecipientSms::where('id_sms',$Sms->id)->get() as $RecipientSms) {
                $DtoSendSms->id_sms = $RecipientSms->id_sms;              
            }
         
            $result->push($DtoSendSms);
        }

        return $result;
    }

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {

        $sms = Sms::find($id);

        if (is_null($sms)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        DB::beginTransaction();

        try {
            $numberSms = RecipientSms::where('id_sms', $id)->delete();

            $smsOrderId = $sms->order_id_skebby;

            $sms->delete();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $smsOrderId;
    }

    #endregion DELETE


}
       
<?php

namespace App\BusinessLogic;

use App\BookingAppointments;
use App\BookingConnectionAppointmentsCartDetail;
use App\DtoModel\DtoCart;
use App\DtoModel\DtoCartPage;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\LogHelper;
use App\Cart;
use App\CartRules;
use App\CartRulesCarriers;
use App\CartRulesItem;
use App\CartRulesProducers;
use App\CartRulesSuppliers;
use App\CartRulesUsers;
use App\CartRulesRoleUser;
use App\CartRulesCategoryItem;
use App\CartRulesApplied;
use App\CartRulesCarriersApplied;
use App\CartRulesItemApplied;
use App\CartRulesUsersApplied;
use App\CartRulesProducersApplied;
use App\CartRulesSuppliersApplied;
use App\CartRulesRoleUserApplied;
use App\CartRulesCategoryItemApplied;
use App\CartDetail;
use App\CartRulesSpecificItems;
use App\RoleUser;
use App\SalesOrder;
use App\SalesOrderDetail;
use App\Item;
use App\VatType;
use App\CategoryItem;
use App\Carrier;
use App\CarrierArea;
use App\CartDetailOptional;
use App\UsersAddresses;
use App\UsersDatas;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Enums\SettingEnum;
use App\Helpers\HttpHelper;
use App\Logs;
use App\SalesOrderDetailOptional;
use App\SalesOrderTokenLinkUser;
use App\DocumentStatus;
use DateTime;

class CartBL
{
    #region PRIVATE

    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request dtoCart
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $dtoCart, int $idLanguage, $dbOperationType)
    {
        
    }

    /**
     * Convert to dto
     * 
     * @param Cart cart
     * @return DtoCart
     */
    private static function _convertToDto($cart)
    {
        $dtoCart = new DtoCart();

        if (isset($cart->id)) {
            $dtoCart->id = $cart->id;
        }

        if (isset($cart->user_id)) {
            $dtoCart->user_id = $cart->user_id;
        }

        if (isset($cart->session_token)) {
            $dtoCart->session_token = $cart->session_token;
        }

        if (isset($cart->currency_id)) {
            $dtoCart->currency_id = $cart->currency_id;
        }

        if (isset($cart->net_amount)) {
            $dtoCart->net_amount = $cart->net_amount;
        }

        if (isset($cart->vat_amount)) {
            $dtoCart->vat_amount = $cart->vat_amount;
        }

        if (isset($cart->shipment_amount)) {
            $dtoCart->shipment_amount = $cart->shipment_amount;
        }        

        if (isset($cart->total_amount)) {
            $dtoCart->total_amount = $cart->total_amount;
        }

        if (isset($cart->discount_percentage)) {
            $dtoCart->discount_percentage = $cart->discount_percentage;
        }

        if (isset($cart->discount_value)) {
            $dtoCart->discount_value = $cart->discount_value;
        }

        if (isset($cart->note)) {
            $dtoCart->note = $cart->note;
        }      

        if (isset($cart->payment_cost)) {
            $dtoCart->payment_cost = $cart->payment_cost;
        }

        if (isset($cart->service_amount)) {
            $dtoCart->service_amount = $cart->service_amount;
        }

        if (isset($cart->user_address_goods_id)) {
            $dtoCart->user_address_goods_id = $cart->user_address_goods_id;
        }

        if (isset($cart->user_address_documents_id)) {
            $dtoCart->user_address_documents_id = $cart->user_address_documents_id;
        }

        return $dtoCart;
    }

    /**
     * Convert to VatType
     * 
     * @param DtoCart dtoCart
     * 
     * @return Cart
     */
    private static function _convertToModel($dtoCart)
    {
        $cart = new Cart();

        if (isset($dtoCart->id)) {
            $cart->id = $dtoCart->id;
        }

        if (isset($dtoCart->user_id)) {
            $cart->user_id = $dtoCart->user_id;
        }

        if (isset($dtoCart->session_token)) {
            $cart->session_token = $dtoCart->session_token;
        }

        if (isset($dtoCart->currency_id)) {
            $cart->currency_id = $dtoCart->currency_id;
        }

        if (isset($dtoCart->net_amount)) {
            $cart->net_amount = $dtoCart->net_amount;
        }       
        
        if (isset($dtoCart->vat_amount)) {
            $cart->vat_amount = $dtoCart->vat_amount;
        }

        if (isset($dtoCart->shipment_amount)) {
            $cart->shipment_amount = $dtoCart->shipment_amount;
        }

        if (isset($dtoCart->total_amount)) {
            $cart->total_amount = $dtoCart->total_amount;
        }

        if (isset($dtoCart->discount_percentage)) {
            $cart->discount_percentage = $dtoCart->discount_percentage;
        }          

        if (isset($dtoCart->discount_value)) {
            $cart->discount_value = $dtoCart->discount_value;
        }

        if (isset($dtoCart->note)) {
            $cart->note = $dtoCart->note;
        }         

        if (isset($dtoCart->payment_cost)) {
            $cart->payment_cost = $dtoCart->payment_cost;
        }     

        if (isset($dtoCart->service_amount)) {
            $cart->service_amount = $dtoCart->service_amount;
        }     

        if (isset($dtoCart->user_address_goods_id)) {
            $cart->user_address_goods_id = $dtoCart->user_address_goods_id;
        }     

        if (isset($dtoCart->user_address_documents_id)) {
            $cart->user_address_documents_id = $dtoCart->user_address_documents_id;
        } 

        return $cart;
    }

    /**
     * Convert to VatType
     * 
     * @param string discount
     * @param string price
     * 
     * @return discountCalculated
     */
    private static function _calculateDiscountValue($discount, $price){
        $discount_value = 0;
        $newPrice = $price;

        $pos = strpos($discount, '+');
        if ($pos !== false) {
            $everyDiscountValue = explode("+", $discount);
            foreach($everyDiscountValue as $everyDiscountValues) {
                $discount_value_new = (($newPrice  / 100) * $everyDiscountValues);
                $discount_value = $discount_value + $discount_value_new;
                $newPrice = $newPrice - $discount_value_new;
            }
        }else{
            $discount_value = ($price / 100) * $discount;
        }

        return $discount_value;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCart
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Cart::find($id));
    }

    public static function getByUserId(int $idUser)
    {
        return static::_convertToDto(Cart::where('user_id', $idUser)->first());        
    }

    /**
     * Get all
     * 
     * @return DtoCart
     */
    public static function getAll()
    {
        $cart = Cart::orderBy('created_at', 'desc')->get();

        $result = collect();
        
        foreach($cart as $cartAll){
            $dtoCart = new DtoCartPage();
            $dtoCart->id =  $cartAll->id;
            $dtoCart->code =  $cartAll->id;
            if(is_null($cartAll->user_id)){
                $dtoCart->userId =  'Carrello Anonimo';
            }else{
                $userCarts = UsersDatas::where('user_id', $cartAll->user_id)->first();    
                if(isset($userCarts)){
                    $dtoCart->userId = $userCarts->name . ' ' . $userCarts->surname; 
                }else{
                    $dtoCart->userId = 'Utente non presente';
                }
            }
            $dtoCart->dateOrder = $cartAll->created_at->format('d/m/Y H:m');
            $dtoCart->orderState =  '-';
            if(is_numeric($cartAll->net_amount) && strpos($cartAll->net_amount,'.')){
                $net_amount = str_replace('.',',',substr($cartAll->net_amount, 0, -2));
            }else{
                $net_amount = $cartAll->net_amount;
            }
            if(is_numeric($cartAll->total_amount) && strpos($cartAll->total_amount,'.')){
                $total_amount = str_replace('.',',',substr($cartAll->total_amount, 0, -2));
            }else{
                $total_amount = $cartAll->total_amount;
            }
            $dtoCart->netAmount =   '€ ' . $net_amount;
            $dtoCart->totalAmount =  '€ ' . $total_amount;
            $result->push($dtoCart);
        }

        return $result;
    }

    public static function getTotalAmount(int $id)
    {
        $cart = Cart::where('id', $id)->first();

        if (is_null($cart)) {
            return 0;
        } else {
            return $cart->total_amount;
        }
    }

    public static function getNrProductRows(int $id)
    {
        return DB::table('carts_details')->where('cart_id', $id)->sum('quantity');
    }

    public static function getNrRows(int $id)
    {
        return DB::table('carts_details')->where('cart_id', $id)->count();
    }

    

    public static function getNoteByCartId(int $id)
    {
        $cart = Cart::where('id', $id)->first();

        if (is_null($cart)) {
            return '';
        } else {
            return $cart->note;
        }
    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request dtoCart
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $dtoCart, int $idLanguage)
    {
        static::_validateData($dtoCart, $idLanguage, DbOperationsTypesEnum::INSERT);
        $cart = static::_convertToModel($dtoCart);
        $cart->save();
        return $cart->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoCart
     * @param int idLanguage
     */
    public static function update(Request $dtoCart, int $idLanguage)
    {
        static::_validateData($dtoCart, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $cart = Cart::find($dtoCart->id);
        DBHelper::updateAndSave(static::_convertToModel($dtoCart), $cart);
    }

    /**
     * updatePaymentType
     * 
     * @param Request dtoCart
     * @param int idLanguage
     */
    public static function updatePaymentType(Request $dtoCart)
    {
        $return = 0;
        $cart = Cart::find($dtoCart->cartId);
        if(isset($cart)){
            $cart->payment_type_id =  $dtoCart->payment_type_id;
            $cart->save();
            $return = $cart->id;
        }
        return $return;
    }

    /**
     * updateNote
     * 
     * @param Request dtoCart
     * @param int idLanguage
     */
    public static function updateNote(Request $dtoCart)
    {
        $return = 0;
        $cart = Cart::find($dtoCart->cartId);
        if(isset($cart)){
            $cart->note =  $dtoCart->note;
            $cart->save();
            $return = $cart->id;
        }
        return $return;
    }

    

     public static function updateCarriage(Request $dtoCart)
    {
         
        $return = 0;
        $cart = Cart::find($dtoCart->cartId);
        if(isset($cart)){
            $cart->carriage_id =  $dtoCart->carriage_id;

        if (!is_null($dtoCart->carrierdescription)){
            $cart->carrier_description =  $dtoCart->carrierdescription;   
        }else{
            $cart->carrier_description = "";
        }
            $cart->save();
            $return = $cart->id;
        }
        return $return;
    }

       public static function updateCarrier(Request $dtoCart)
    {
        LogHelper::debug($dtoCart);
        $return = 0;
        $cart = Cart::find($dtoCart->cartId);
        if(isset($cart)){
            $cart->carrier_id = $dtoCart->carrier_id;
            $cart->save();
            
            $return = $cart->id;
           // CartBL::applyRulesAndRefreshTotalAmount($return);
        }
       
        return $return;
    }


    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $cart = Cart::find($id);

        if (is_null($cart)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $cart->delete();
    }

    #endregion DELETE

    /************************************************************************/
    //    funzione per applicare regola automatica del carrello se articolo che sto aggiungendo fa parte di un articolo specifico nelle azioni di un'altra regola
    /************************************************************************/
    public static function applyRulesFromItemsPresentInActionTab(int $idCartDetail, int $userId = null){
        $cartDetail = CartDetail::where('id', $idCartDetail)->get()->first();
        if(isset($cartDetail)){
            $cartRulesOnDb = DB::select('SELECT carts_rules.* from carts_rules 
                inner join carts_rules_items on carts_rules_items.cart_rule_id = carts_rules.id 
                where online = true
                and (
                    (enabled_from is null and enabled_to is null)
                    or 
                    (enabled_from is null and enabled_to > now())
                    or 
                    (enabled_from < now() and enabled_to is null)
                    or 
                    (enabled_from < now() and enabled_to > now())
                )
                and (availability is null or (availability > 0 and availability_residual > 0))
                and apply_discount_to = 3
                and carts_rules_items.item_id = :item_id',
                ['item_id' => $cartDetail->item_id]
            );

            if(isset($cartRulesOnDb) && count($cartRulesOnDb)>0){
                //sto aggiungendo un articolo che contiente dei prodotti specifici da aggiungere o da applicare uno sconto nel carrello
                //controllo sulla tabella cart_rules_specific_items
                $cartRulesSpecific = CartRulesSpecificItems::where('carts_rules_id', $cartRulesOnDb[0]->id)->get();
                if(isset($cartRulesSpecific)){
                    foreach($cartRulesSpecific as $cartRulesSpecifics){
                        $item = Item::find($cartRulesSpecifics->item_id);
                        //controllo la presenza dell'articolo nel carrello
                        $cartDetailSpecificItemOnDB = CartDetail::where('cart_id', $cartDetail->cart_id)->where('item_id', $cartRulesSpecifics->item_id)->get()->first();
                        if(isset($cartDetailSpecificItemOnDB)){
                            //articolo già presente nel carrello, applicazione sconto solo alla quantità indicata prevista per la scontistica
                            
                            //sconto percentuale
                            if ($cartRulesOnDb[0]->discount_type == 1 && $cartRulesOnDb[0]->discount_value > 0){
                                //$discount_value = ($item->price / 100) * $cartRulesOnDb[0]->discount_value;
                                $discount_value = static::_calculateDiscountValue($cartRulesOnDb[0]->discount_value, $item->price);
                                $cartDetailSpecificItemOnDB->discount_percentage = $cartRulesOnDb[0]->discount_value;
                                $newPrice = $item->price - $discount_value;
                                $cartDetailSpecificItemOnDB->unit_price= $newPrice;
                            //Sconto in valore
                            }else if ($cartRulesOnDb[0]->discount_type == 2 && $cartRulesOnDb[0]->discount_value > 0){
                                $cartDetailSpecificItemOnDB->discount_value = $cartRulesOnDb[0]->discount_value;
                                $newPrice = $item->price - $cartDetailSpecificItemOnDB->discount_value;
                                $cartDetailSpecificItemOnDB->unit_price= $newPrice;
                            }else{
                                $newPrice = $item->price;
                                $cartDetailSpecificItemOnDB->unit_price= $newPrice;
                            }

                            //se l'articolo ha un iva, me la memorizzo e poi controllo se devo scorporarla al prezzo unitario o aggiungerla
                            if (isset($item->vat_type_id)) {
                                $cartDetailSpecificItemOnDB->vat_type_id = $item->vat_type_id;
                                
                                $vatTypeRate = VatType::where('id', $item->vat_type_id)->first()->rate;

                                if ($vatTypeRate != null && $vatTypeRate>0){
                                    $PriceIncludingVat = SettingBL::getByCode(SettingEnum::PriceIncludingVat);
                                    //verifico se devo scorporare l'iva o aggiungerla
                                    if ($PriceIncludingVat=='True'){
                                        $baseImponibile = (100*$newPrice*$cartDetailSpecificItemOnDB->quantity)/(100+$vatTypeRate);

                                        $cartDetailSpecificItemOnDB->net_amount = $baseImponibile;
                                        $cartDetailSpecificItemOnDB->vat_amount = ($newPrice*$cartDetailSpecificItemOnDB->quantity) - $baseImponibile;
                                        $cartDetailSpecificItemOnDB->total_amount = $newPrice*$cartDetailSpecificItemOnDB->quantity;
                                    }else{
                                        $cartDetailSpecificItemOnDB->net_amount = $newPrice*$cartDetailSpecificItemOnDB->quantity;
                                        $cartDetailSpecificItemOnDB->vat_amount = ($cartDetailSpecificItemOnDB->net_amount/100)*$vatTypeRate;
                                        $cartDetailSpecificItemOnDB->total_amount = $cartDetailSpecificItemOnDB->net_amount + $cartDetailSpecificItemOnDB->vat_amount;
                                    }
                                }
                            }else{
                                $cartDetailSpecificItemOnDB->vat_amount = 0;
                                $cartDetailSpecificItemOnDB->net_amount = $newPrice*$cartDetailSpecificItemOnDB->quantity;
                                $cartDetailSpecificItemOnDB->total_amount = $newPrice*$cartDetailSpecificItemOnDB->quantity;
                            }

                            $cartDetailSpecificItemOnDB->save();
                        }else{
                            //articolo non già presente nel carrello
                            //controllo sul flag aggiungi articolo in automatico al carrello
                            if($cartRulesSpecifics->add_to_cart_automatically){
                                //se true allora lo aggiungo con lo sconto da applicare nelle condizione delle regole del carrello
                                //se false non faccio niente in quanto rientro nella casistica in cui l'articolo non è nel carrello quindi non ha senso ricontrollare nella tabella cart_detail
                                $newCartDetail = new CartDetail();
                                $newCartDetail->cart_id = $cartDetail->cart_id;
                                $newCartDetail->nr_row = CartDetailBL::getMaxRow($cartDetail->cart_id);            
                                $newCartDetail->item_id = $item->id;
                                $newCartDetail->description = ItemLanguageBL::getByItemAndLanguage($item->id, 1);
                                //$newCartDetail->unit_price = $item->price;
                                $newCartDetail->quantity = $cartRulesSpecifics->quantity;

                                //sconto percentuale
                                if ($cartRulesOnDb[0]->discount_type == 1 && $cartRulesOnDb[0]->discount_value > 0){
                                    //$discount_value = ($item->price / 100) * $cartRulesOnDb[0]->discount_value;
                                    $discount_value = static::_calculateDiscountValue($cartRulesOnDb[0]->discount_value, $item->price);
                                    $newCartDetail->discount_percentage = $cartRulesOnDb[0]->discount_value;
                                    $newPrice = $item->price - $discount_value;
                                    $newCartDetail->unit_price= $newPrice;
                                //Sconto in valore
                                }else if ($cartRulesOnDb[0]->discount_type == 2 && $cartRulesOnDb[0]->discount_value > 0){
                                    $newCartDetail->discount_value = $cartRulesOnDb[0]->discount_value;
                                    $newPrice = $item->price - $newCartDetail->discount_value;
                                    $newCartDetail->unit_price= $newPrice;
                                }else{
                                    $newPrice = $item->price;
                                    $newCartDetail->unit_price= $newPrice;
                                }

                                //se l'articolo ha un iva, me la memorizzo e poi controllo se devo scorporarla al prezzo unitario o aggiungerla
                                if (isset($item->vat_type_id)) {
                                    $newCartDetail->vat_type_id = $item->vat_type_id;
                                    
                                    $vatTypeRate = VatType::where('id', $item->vat_type_id)->first()->rate;

                                    if ($vatTypeRate != null && $vatTypeRate>0){
                                        $PriceIncludingVat = SettingBL::getByCode(SettingEnum::PriceIncludingVat);
                                        //verifico se devo scorporare l'iva o aggiungerla
                                        if ($PriceIncludingVat=='True'){
                                            $baseImponibile = (100*$newPrice*$cartRulesSpecifics->quantity)/(100+$vatTypeRate);

                                            $newCartDetail->net_amount = $baseImponibile;
                                            $newCartDetail->vat_amount = ($newPrice*$cartRulesSpecifics->quantity) - $baseImponibile;
                                            $newCartDetail->total_amount = $newPrice*$cartRulesSpecifics->quantity;
                                        }else{
                                            $newCartDetail->net_amount = $newPrice*$cartRulesSpecifics->quantity;
                                            $newCartDetail->vat_amount = ($newCartDetail->net_amount/100)*$vatTypeRate;
                                            $newCartDetail->total_amount = $newCartDetail->net_amount + $newCartDetail->vat_amount;
                                        }
                                    }
                                }else{
                                    $newCartDetail->vat_amount = 0;
                                    $newCartDetail->net_amount = $newPrice*$cartRulesSpecifics->quantity;
                                    $newCartDetail->total_amount = $newPrice*$cartRulesSpecifics->quantity;
                                }

                                $newCartDetail->save();
                            
                            }
                        }
                    }
                }
            }else{
                //query di controllo articolo in tabella cart_rules_specifical_item, filtrando per apply_discount_to = 3 e apply_to_all_items = 0 e add_to_cart_automatically = 0
                $cartRulesSpecificItemOnDb = DB::select('SELECT carts_rules.* from carts_rules 
                    inner join cart_rules_specific_items on cart_rules_specific_items.carts_rules_id = carts_rules.id 
                    where online = true
                    and (
                            (enabled_from is null and enabled_to is null)
                            or 
                            (enabled_from is null and enabled_to > now())
                            or 
                            (enabled_from < now() and enabled_to is null)
                            or 
                            (enabled_from < now() and enabled_to > now())
                    )
                    and (availability is null or (availability > 0 and availability_residual > 0))
                    and apply_discount_to = 3
                    and cart_rules_specific_items.add_to_cart_automatically = false
                    and cart_rules_specific_items.item_id = :item_id',
                    ['item_id' => $cartDetail->item_id]
                );
                
                //se è presente la regola prelevo gli articoli nelle condizioni - tabella cart_rule_items - e controllo che siano presenti tutti nel carrello
                if(isset($cartRulesSpecificItemOnDb) && count($cartRulesSpecificItemOnDb)>0){
                    foreach($cartRulesSpecificItemOnDb as $cartRulesSpecificItemOnDbs){
                        $cartRuleItemOnDb = CartRulesItem::where('cart_rule_id', $cartRulesSpecificItemOnDbs->id)->get();
                        if(isset($cartRuleItemOnDb)){
                            $contCartRuleItemOnDb = DB::select("SELECT count(id) as cont from carts_rules_items where cart_rule_id =" . $cartRulesSpecificItemOnDbs->id);
                            $contCartRuleItemCartOnDb = DB::select("SELECT count(id) as cont from carts_details where cart_id = " . $cartDetail->cart_id . " and item_id in (select item_id from carts_rules_items where cart_rule_id = " . $cartRulesSpecificItemOnDbs->id . ")");
                            //se presenti applico lo sconto sull'articolo che sto aggiungendo al carrello
                            //altrimenti non faccio niente
                            if($contCartRuleItemOnDb[0]->cont == $contCartRuleItemCartOnDb[0]->cont){
                                $cartDetailSpecificItemOnDB = CartDetail::where('id', $cartDetail->id)->get()->first();
                                if(isset($cartDetailSpecificItemOnDB)){
                                    //articolo già presente nel carrello, applicazione sconto solo alla quantità indicata prevista per la scontistica
                                    $item = Item::find($cartDetail->item_id);
                                    //sconto percentuale
                                    if ($cartRulesSpecificItemOnDbs->discount_type == 1 && $cartRulesSpecificItemOnDbs->discount_value > 0){
                                        //$discount_value = ($item->price / 100) * $cartRulesSpecificItemOnDbs->discount_value;
                                        $discount_value = static::_calculateDiscountValue($cartRulesSpecificItemOnDbs->discount_value, $item->price);
                                        $cartDetailSpecificItemOnDB->discount_percentage = $cartRulesSpecificItemOnDbs->discount_value;
                                        $newPrice = $item->price - $discount_value;
                                        $cartDetailSpecificItemOnDB->unit_price= $newPrice;
                                    //Sconto in valore
                                    }else if ($cartRulesSpecificItemOnDbs->discount_type == 2 && $cartRulesSpecificItemOnDbs->discount_value > 0){
                                        $cartDetailSpecificItemOnDB->discount_value = $cartRulesSpecificItemOnDbs->discount_value;
                                        $newPrice = $item->price - $cartDetailSpecificItemOnDB->discount_value;
                                        $cartDetailSpecificItemOnDB->unit_price= $newPrice;
                                    }else{
                                        $newPrice = $item->price;
                                        $cartDetailSpecificItemOnDB->unit_price= $newPrice;
                                    }
        
                                    //se l'articolo ha un iva, me la memorizzo e poi controllo se devo scorporarla al prezzo unitario o aggiungerla
                                    if (isset($item->vat_type_id)) {
                                        $cartDetailSpecificItemOnDB->vat_type_id = $item->vat_type_id;
                                        
                                        $vatTypeRate = VatType::where('id', $item->vat_type_id)->first()->rate;
        
                                        if ($vatTypeRate != null && $vatTypeRate>0){
                                            $PriceIncludingVat = SettingBL::getByCode(SettingEnum::PriceIncludingVat);
                                            //verifico se devo scorporare l'iva o aggiungerla
                                            if ($PriceIncludingVat=='True'){
                                                $baseImponibile = (100*$newPrice*$cartDetailSpecificItemOnDB->quantity)/(100+$vatTypeRate);
        
                                                $cartDetailSpecificItemOnDB->net_amount = $baseImponibile;
                                                $cartDetailSpecificItemOnDB->vat_amount = ($newPrice*$cartDetailSpecificItemOnDB->quantity) - $baseImponibile;
                                                $cartDetailSpecificItemOnDB->total_amount = $newPrice*$cartDetailSpecificItemOnDB->quantity;
                                            }else{
                                                $cartDetailSpecificItemOnDB->net_amount = $newPrice*$cartDetailSpecificItemOnDB->quantity;
                                                $cartDetailSpecificItemOnDB->vat_amount = ($cartDetailSpecificItemOnDB->net_amount/100)*$vatTypeRate;
                                                $cartDetailSpecificItemOnDB->total_amount = $cartDetailSpecificItemOnDB->net_amount + $cartDetailSpecificItemOnDB->vat_amount;
                                            }
                                        }
                                    }
                                    $cartDetailSpecificItemOnDB->save();
                                }
                            }
                        }
                    }
                }else{
                    if(!is_null($userId)){
                        //query di controllo articolo in tabella cart_rules_users, filtrando per apply_discount_to = 4 
                        $cartRulesSpecificItemOnDb = DB::select('SELECT carts_rules.* from carts_rules 
                            inner join carts_rules_users on carts_rules_users.cart_rule_id = carts_rules.id 
                            where online = true
                            and (
                                (enabled_from is null and enabled_to is null)
                                or 
                                (enabled_from is null and enabled_to > now())
                                or 
                                (enabled_from < now() and enabled_to is null)
                                or 
                                (enabled_from < now() and enabled_to > now())
                            )
                            and (availability is null or (availability > 0 and availability_residual > 0))
                            and apply_discount_to = 4
                            and apply_to_all_customers = 0
                            and carts_rules_users.user_id = :userId
                            order by priority',
                            ['userId' => $userId]
                        );
                        $item = Item::find($cartDetail->item_id);
                        //se è presente la regola prelevo gli articoli nelle condizioni - tabella cart_rule_items - e controllo che siano presenti tutti nel carrello
                        if(isset($cartRulesSpecificItemOnDb) && count($cartRulesSpecificItemOnDb)>0){
                            foreach($cartRulesSpecificItemOnDb as $cartRulesSpecificItemOnDbs){
                                $cartRuleItemOnDb = CartRulesUsers::where('cart_rule_id', $cartRulesSpecificItemOnDbs->id)->get();
                                if(isset($cartRuleItemOnDb)){

                                    $proseguiUser = False;
                                    $prosegui = True;
                                    $trovato = False;
                                    $cartRuleItemOnDb = CartRulesItem::where('cart_rule_id', $cartRulesSpecificItemOnDbs->id)->get(); 
                                    $cartRuleProducerOnDb = CartRulesProducers::where('cart_rule_id', $cartRulesSpecificItemOnDbs->id)->get();
                                    $cartRuleSupplierOnDb = CartRulesSuppliers::where('cart_rule_id', $cartRulesSpecificItemOnDbs->id)->get();
                                    $cartRuleCategoryItemOnDb = CartRulesCategoryItem::where('cart_rule_id', $cartRulesSpecificItemOnDbs->id)->get(); 
                                    
                                    if ($cartRulesSpecificItemOnDbs->apply_to_all_customers == 0){
                                        if($userId!='' && !is_null($userId)){
                                            if (CartRulesUsers::where('user_id', $userId)->where('cart_rule_id', $cartRulesSpecificItemOnDbs->id)->count() <= 0) {
                                                $proseguiUser = False;
                                            }else{
                                                $proseguiUser = True;
                                            }
                                        }else{
                                            $proseguiUser = False;
                                        }
                                    }else{
                                        $proseguiUser = True;
                                    }

                                    /*************************************************/   
                                    //prima condizione: controllo se l'articolo è presente nelle regole
                                    /*************************************************/
                                    if ($cartRulesSpecificItemOnDbs->apply_to_all_items == 0){
                                        $trovato = False;
                                        foreach($cartRuleItemOnDb as $itemsCart){
                                            if ($idItem == $itemsCart->item_id){
                                                //l'ho trovato, esco dal ciclo for
                                                $trovato = True;
                                                break;
                                            }
                                        }
                                        if ($trovato == False)
                                            $prosegui = False;
                                    }

                                    /*************************************************/   
                                    //deconda condizione: controllo se la categoria dell'articolo è presente nelle regole
                                    /*************************************************/
                                    if ($cartRulesSpecificItemOnDbs->apply_to_all_categories_items  == 0){
                                        $trovato = False;
                                        foreach($cartRuleCategoryItemOnDb as $cartRulesCategories){
                                            foreach($categoryItemOnDb as $categories){
                                                if ($cartRulesCategories->category_item_id == $categories->category_id){
                                                    //l'ho trovato, esco dal ciclo for
                                                    $trovato = True;
                                                    break;
                                                }
                                            }
                                        }
                                        if ($trovato == False)
                                            $prosegui = False;
                                    }

                                    /*************************************************/   
                                    //terza condizione: controllo se la categoria dell'articolo è presente nelle regole
                                    /*************************************************/
                                    if ($cartRulesSpecificItemOnDbs->apply_to_all_producers == 0){
                                        $trovato = False;
                                        foreach($cartRuleProducerOnDb as $producerCart){
                                            if ($item->producer_id == $producerCart->producer_id){
                                                //l'ho trovato, esco dal ciclo for
                                                $trovato = True;
                                                break;
                                            }
                                        }
                                        if ($trovato == False)
                                            $prosegui = False;
                                    }

                                    /*************************************************/   
                                    //quarta condizione: controllo se il fornitore dell'articolo è presente nelle regole
                                    /*************************************************/
                                    if ($cartRulesSpecificItemOnDbs->apply_to_all_suppliers == 0){
                                        $trovato = False;
                                        foreach($cartRuleSupplierOnDb as $suppliersCart){
                                            if ($item->supplier_id == $suppliersCart->supplier_id){
                                                //l'ho trovato, esco dal ciclo for
                                                $trovato = True;
                                                break;
                                            }
                                        }
                                        if ($trovato == False)
                                            $prosegui = False;
                                    }

                                    if($prosegui){
                                    
                                        $cartDetailSpecificItemOnDB = CartDetail::where('id', $cartDetail->id)->get()->first();
                                        if(isset($cartDetailSpecificItemOnDB)){
                                            //articolo già presente nel carrello, applicazione sconto solo alla quantità indicata prevista per la scontistica
                                            
                                            //sconto percentuale
                                            if ($cartRulesSpecificItemOnDbs->discount_type == 1 && $cartRulesSpecificItemOnDbs->discount_value > 0){
                                                //implementazione calcolo sconto multiplo, esempio 50+10+2 ecc..
                                                
                                                $discount_value = static::_calculateDiscountValue($cartRulesSpecificItemOnDbs->discount_value, $item->price);
                                                
                                                $cartDetailSpecificItemOnDB->discount_percentage = $cartRulesSpecificItemOnDbs->discount_value;
                                                $newPrice = $item->price - $discount_value;
                                                $cartDetailSpecificItemOnDB->unit_price= $newPrice;
                                            //Sconto in valore
                                            }else if ($cartRulesSpecificItemOnDbs->discount_type == 2 && $cartRulesSpecificItemOnDbs->discount_value > 0){
                                                $cartDetailSpecificItemOnDB->discount_value = $cartRulesSpecificItemOnDbs->discount_value;
                                                $newPrice = $item->price - $cartDetailSpecificItemOnDB->discount_value;
                                                $cartDetailSpecificItemOnDB->unit_price= $newPrice;
                                            }else{
                                                $newPrice = $item->price;
                                                $cartDetailSpecificItemOnDB->unit_price= $newPrice;
                                            }
                
                                            //se l'articolo ha un iva, me la memorizzo e poi controllo se devo scorporarla al prezzo unitario o aggiungerla
                                            if (isset($item->vat_type_id)) {
                                                $cartDetailSpecificItemOnDB->vat_type_id = $item->vat_type_id;
                                                
                                                $vatTypeRate = VatType::where('id', $item->vat_type_id)->first()->rate;
                
                                                if ($vatTypeRate != null && $vatTypeRate>0){
                                                    $PriceIncludingVat = SettingBL::getByCode(SettingEnum::PriceIncludingVat);
                                                    //verifico se devo scorporare l'iva o aggiungerla
                                                    if ($PriceIncludingVat=='True'){
                                                        $baseImponibile = (100*$newPrice*$cartDetailSpecificItemOnDB->quantity)/(100+$vatTypeRate);
                
                                                        $cartDetailSpecificItemOnDB->net_amount = $baseImponibile;
                                                        $cartDetailSpecificItemOnDB->vat_amount = ($newPrice*$cartDetailSpecificItemOnDB->quantity) - $baseImponibile;
                                                        $cartDetailSpecificItemOnDB->total_amount = $newPrice*$cartDetailSpecificItemOnDB->quantity;
                                                    }else{
                                                        $cartDetailSpecificItemOnDB->net_amount = $newPrice*$cartDetailSpecificItemOnDB->quantity;
                                                        $cartDetailSpecificItemOnDB->vat_amount = ($cartDetailSpecificItemOnDB->net_amount/100)*$vatTypeRate;
                                                        $cartDetailSpecificItemOnDB->total_amount = $cartDetailSpecificItemOnDB->net_amount + $cartDetailSpecificItemOnDB->vat_amount;
                                                    }
                                                }else{
                                                    $cartDetailSpecificItemOnDB->net_amount = $newPrice*$cartDetailSpecificItemOnDB->quantity;
                                                    $cartDetailSpecificItemOnDB->vat_amount = 0;
                                                    $cartDetailSpecificItemOnDB->total_amount = $cartDetailSpecificItemOnDB->net_amount + $cartDetailSpecificItemOnDB->vat_amount;
                                                }
                                            }else{
                                                $cartDetailSpecificItemOnDB->net_amount = $newPrice*$cartDetailSpecificItemOnDB->quantity;
                                                $cartDetailSpecificItemOnDB->vat_amount = 0;
                                                $cartDetailSpecificItemOnDB->total_amount = $cartDetailSpecificItemOnDB->net_amount + $cartDetailSpecificItemOnDB->vat_amount;
                                            }
                                            $cartDetailSpecificItemOnDB->save();
                                        }
                                        if($cartRulesSpecificItemOnDbs->exclude_other_rules){
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    /************************************************************************/
    //    funzione per ricalcolare il totale del carrello
    /************************************************************************/
    public static function applyRulesAndRefreshTotalAmount(int $id){
        $hoTrovatoUnaRegolaCheEscludeLeAltre = False;
        $cartOnDb = Cart::find($id);

        /*****************************************************************************/
        /* CANCELLO TUTTE LE REGOLE GIA' APPLICATE, DEVO RICALCOLARLE PER FORZA      */
        /*****************************************************************************/
        DB::delete(
            'DELETE FROM carts_rules_carriers_applied WHERE cart_rule_id IN
                (SELECT id FROM carts_rules_applied WHERE cart_id_temporary = :cart_id_temporary)',
            ['cart_id_temporary' => $id]
        );
        DB::delete(
            'DELETE FROM carts_rules_items_applied WHERE cart_rule_id IN
                (SELECT id FROM carts_rules_applied WHERE cart_id_temporary = :cart_id_temporary)',
            ['cart_id_temporary' => $id]
        );     
        DB::delete(
            'DELETE FROM carts_rules_categories_items_applied WHERE cart_rule_id IN
                (SELECT id FROM carts_rules_applied WHERE cart_id_temporary = :cart_id_temporary)',
            ['cart_id_temporary' => $id]
        ); 
        DB::delete(
            'DELETE FROM carts_rules_users_applied WHERE cart_rule_id IN
                (SELECT id FROM carts_rules_applied WHERE cart_id_temporary = :cart_id_temporary)',
            ['cart_id_temporary' => $id]
        );      
        DB::delete(
            'DELETE FROM carts_rules_role_user_applied WHERE cart_rule_id IN
                (SELECT id FROM carts_rules_applied WHERE cart_id_temporary = :cart_id_temporary)',
            ['cart_id_temporary' => $id]
        ); 
        DB::delete(
            'DELETE FROM carts_rules_producers_applied WHERE cart_rule_id IN
                (SELECT id FROM carts_rules_applied WHERE cart_id_temporary = :cart_id_temporary)',
            ['cart_id_temporary' => $id]
        );
        DB::delete(
            'DELETE FROM carts_rules_suppliers_applied WHERE cart_rule_id IN
                (SELECT id FROM carts_rules_applied WHERE cart_id_temporary = :cart_id_temporary)',
            ['cart_id_temporary' => $id]
        );
        DB::delete(
            'DELETE FROM carts_rules_applied WHERE cart_id_temporary = :cart_id_temporary',
            ['cart_id_temporary' => $id]
        ); 
        

        /*****************************************************************************/
        /* DEVO APPLICARE LE EVENTUALI REGOLE DEL CARRELLO                           */
        /* COME PRIMA COSA MI SCORRO TUTTE LE REGOLE DA APPLICARE ALLE SINGOLE RIGHE */
        /*****************************************************************************/
        foreach (DB::select(' SELECT ID, exclude_other_rules from carts_rules
                            where online = True
                            and (
                                (enabled_from is null and enabled_to is null)
                                or 
                                (enabled_from is null and enabled_to > now())
                                or 
                                (enabled_from < now() and enabled_to is null)
                                or 
                                (enabled_from < now() and enabled_to > now())
                            )
                            and (
                                (availability is null or (availability > 0 and availability_residual > 0))
                            )
                            and (apply_to_all_items = 0 or apply_to_all_categories_items = 0 or apply_to_all_producers = 0
                            or CASE WHEN minimum_quantity IS NULL THEN 0 ELSE minimum_quantity end>0
                            or CASE WHEN maximum_quantity IS NULL THEN 0 ELSE maximum_quantity end>0
                            )
                            and (apply_discount_to = 1 or apply_discount_to = 1)
                            order by priority')
        as $regoleDaApplicareAlleRighe) {
            if(!is_null($cartOnDb->user_id)){
                if ($hoTrovatoUnaRegolaCheEscludeLeAltre == False){
                    static::elaborateCartRule($id, $regoleDaApplicareAlleRighe->id);
                    if($regoleDaApplicareAlleRighe->exclude_other_rules==1){
                        $hoTrovatoUnaRegolaCheEscludeLeAltre = True; 
                    }
                }
            }
        }

        /************************************************/
        /* ORA RICALCOLO I TOTALI DEL CARRELLO */
        /************************************************/
         
        $cartDetailOnDb = CartDetail::where('cart_id', $id)->get(); 

        $totalNetAmount=0;
        $totalVatAmount=0;
        $totalAmount=0;
        $weight = 0;
        foreach($cartDetailOnDb as $cartDetail){
            $weightArt = 0;
            $item = Item::where('id', $cartDetail->item_id)->get()->first();
            if(isset($item)){
                if(!is_null($item->weight)){
                    $weight +=  $item->weight * $cartDetail->quantity;
                }else{
                    $weight += 0;
                }
            }else{
                $weight += 0;
            }

            $totalAmount += $cartDetail->total_amount;
            $totalVatAmount += $cartDetail->vat_amount;
            $totalNetAmount += $cartDetail->net_amount;
            
        }

        /************************************************/
        /* ORA CALCOLO LE SPESE DI SPEDIZIONE */
        /************************************************/

        //valorizzo con il prezzo di spedizine di default
        $totalShipmentAmount = floatval(SettingBL::getByCode('DefaultShippingPrice'));
        $idCarrier = null;
        if(is_null($cartOnDb->user_address_goods_id) || $cartOnDb->user_address_goods_id == ''){
            if(is_null($cartOnDb->user_id)){
                //valorizzo con il prezzo di spedizine di default
                $totalShipmentAmount = floatval(SettingBL::getByCode('DefaultShippingPrice'));
            }else{
                //calcolo spese di spedizione sulla sede di default di registrazione cliente
                $userDatas = UsersDatas::where('user_id', $cartOnDb->user_id)->get()->first();
                if(isset($userDatas)){
                    $idRegion  = $userDatas->region_id;
                    $idNation  = $userDatas->nation_id;
                    $idProvince  = $userDatas->province_id;
                    //controllo sul nation id -> per nazione
                    $carrierAreaNation = CarrierArea::where('nation_id', $userDatas->nation_id)->where('from', '<=', $weight)->where('to', '>', $weight)->get()->first();
                    if(isset($carrierAreaNation)){
                        $idCarrier = $carrierAreaNation->carrier_id;
                        $totalShipmentAmount = $carrierAreaNation->price;
                    }else{
                        //controllo sul region_id id -> per regione
                        $carrierAreaRegion = CarrierArea::where('region_id', $userDatas->region_id)->where('from', '<=', $weight)->where('to', '>', $weight)->get()->first();
                        if(isset($carrierAreaRegion)){
                            $idCarrier = $carrierAreaRegion->carrier_id;
                            $totalShipmentAmount = $carrierAreaRegion->price;
                        }else{
                            //controllo sul province id -> per provincia
                            $carrierAreaProvince = CarrierArea::where('province_id', $userDatas->province_id)->where('from', '<=', $weight)->where('to', '>', $weight)->get()->first();
                            if(isset($carrierAreaProvince)){
                                $idCarrier = $carrierAreaProvince->carrier_id;
                                $totalShipmentAmount = $carrierAreaProvince->price;
                            }else{
                                //valorizzo con il prezzo di spedizine di default
                                $totalShipmentAmount = floatval(SettingBL::getByCode('DefaultShippingPrice'));
                            }
                        }
                    }
                }else{
                    //valorizzo con il prezzo di spedizine di default
                    $totalShipmentAmount = floatval(SettingBL::getByCode('DefaultShippingPrice'));
                }
            }
        }else{
            //calcolo spesa di spedizione tramite la sede selezionata sul carrello
            $userAddresses = UsersAddresses::where('id', $cartOnDb->user_address_goods_id)->get()->first();
            if(isset($userAddresses)){
                $idRegion  = $userAddresses->region_id;
                $idNation  = $userAddresses->nation_id;
                $idProvince  = $userAddresses->province_id;
                //controllo sul nation id -> per nazione
                $carrierAreaNation = CarrierArea::where('nation_id', $userAddresses->nation_id)->where('from', '<=', $weight)->where('to', '>', $weight)->get()->first();
                if(isset($carrierAreaNation)){
                    $idCarrier = $carrierAreaNation->carrier_id;
                    $totalShipmentAmount = $carrierAreaNation->price;
                }else{
                    //controllo sul region_id id -> per regione
                    $carrierAreaRegion = CarrierArea::where('region_id', $userAddresses->region_id)->where('from', '<=', $weight)->where('to', '>', $weight)->get()->first();
                    if(isset($carrierAreaRegion)){
                        $idCarrier = $carrierAreaRegion->carrier_id;
                        $totalShipmentAmount = $carrierAreaRegion->price;
                    }else{
                        //controllo sul province id -> per provincia
                        $carrierAreaProvince = CarrierArea::where('province_id', $userAddresses->province_id)->where('from', '<=', $weight)->where('to', '>', $weight)->get()->first();
                        if(isset($carrierAreaProvince)){
                            $idCarrier = $carrierAreaProvince->carrier_id;
                            $totalShipmentAmount = $carrierAreaProvince->price;
                        }else{
                            //valorizzo con il prezzo di spedizine di default
                            $totalShipmentAmount = floatval(SettingBL::getByCode('DefaultShippingPrice'));
                        }
                    }
                }
            }else{
                //valorizzo con il prezzo di spedizine di default
                $totalShipmentAmount = floatval(SettingBL::getByCode('DefaultShippingPrice'));
            }
        }

        //valorizzazione id corriere a seconda di quanto trovato in precedenza;
        $cartOnDb->carrier_id = $idCarrier;

        $cartOnDb->total_amount = $totalAmount + $totalShipmentAmount + $cartOnDb->service_amount + $cartOnDb->payment_cost;
        $cartOnDb->shipment_amount = $totalShipmentAmount;
        $cartOnDb->net_amount = $totalNetAmount;
        $cartOnDb->vat_amount = $totalVatAmount;
        $cartOnDb->discount_percentage = 0;
        $cartOnDb->discount_value = 0;
        $cartOnDb->save();

        /**********************************************************/
        /* ORA MI SCORRO TUTTE LE REGOLE DA APPLICARE AL CARRELLO */
        /**********************************************************/
        if (!$hoTrovatoUnaRegolaCheEscludeLeAltre){
            foreach (DB::select(' SELECT ID, exclude_other_rules from carts_rules
                                where online = True
                                and (
                                    (enabled_from is null and enabled_to is null)
                                    or 
                                    (enabled_from is null and enabled_to > now())
                                    or 
                                    (enabled_from < now() and enabled_to is null)
                                    or 
                                    (enabled_from < now() and enabled_to > now())
                                )
                                and (
                                    (availability is null or (availability > 0 and availability_residual > 0))
                                )
                                and apply_to_all_items = 1 
                                and apply_to_all_categories_items = 1
                                and apply_to_all_producers = 1
                                and apply_discount_to <> 4
                                and CASE WHEN minimum_quantity IS NULL THEN 0 ELSE minimum_quantity end = 0
                                and CASE WHEN maximum_quantity IS NULL THEN 0 ELSE maximum_quantity end = 0
                                order by priority')
            as $regoleDaApplicareAlCarrelloGlobale) {
                if(!is_null($cartOnDb->user_id)){
                    if ($hoTrovatoUnaRegolaCheEscludeLeAltre == False){
                        static::elaborateCartRule ($id, $regoleDaApplicareAlCarrelloGlobale->id);
                        if($regoleDaApplicareAlCarrelloGlobale->exclude_other_rules==1){
                            $hoTrovatoUnaRegolaCheEscludeLeAltre = True; 
                        }
                    }
                }
            }
        }

        /**********************************************************/
        /* ORA MI SCORRO TUTTE LE REGOLE DA APPLICARE AL CARRELLO CHE DIPENDONO DALLE RIGHE */
        /**********************************************************/
        if (!$hoTrovatoUnaRegolaCheEscludeLeAltre){
            foreach (DB::select(' SELECT ID, exclude_other_rules from carts_rules
                                where online = True
                                and (
                                    (enabled_from is null and enabled_to is null)
                                    or 
                                    (enabled_from is null and enabled_to > now())
                                    or 
                                    (enabled_from < now() and enabled_to is null)
                                    or 
                                    (enabled_from < now() and enabled_to > now())
                                )
                                and (
                                    (availability is null or (availability > 0 and availability_residual > 0))
                                )
                                and (apply_to_all_items = 0 or apply_to_all_categories_items = 0 or apply_to_all_producers = 0)
                                and (apply_discount_to = 0 or apply_discount_to = 4)
                                and CASE WHEN minimum_quantity IS NULL THEN 0 ELSE minimum_quantity end = 0
                                and CASE WHEN maximum_quantity IS NULL THEN 0 ELSE maximum_quantity end = 0
                                order by priority')
            as $regoleDaApplicareAlCarrelloGlobale) {
                if(!is_null($cartOnDb->user_id)){
                    if ($hoTrovatoUnaRegolaCheEscludeLeAltre == False){
                        static::elaborateCartRule ($id, $regoleDaApplicareAlCarrelloGlobale->id);
                        if($regoleDaApplicareAlCarrelloGlobale->exclude_other_rules==1){
                            $hoTrovatoUnaRegolaCheEscludeLeAltre = True; 
                        }
                    }
                }
            }
        }
    }

    /**********************************************************/
    /* FUNZIONE CHE ELABORA E APPLICA UNA REGOLA DEL CARRELLO */
    /**********************************************************/
    public static function elaborateCartRule(int $idCart, int $idCartRule){
        try {

            $trovato = False;
            $prosegui = True;
            $proseguiUser = False;
            $cartOnDb = Cart::find($idCart);
            $cartRuleOnDb = CartRules::find($idCartRule);

            //se la regola del carrello va applicata ad articoli specifici (o categorie) significa che è
            // una regola da applicare alle singole righe del carrello
            if ($cartRuleOnDb->apply_to_all_items == 0 || $cartRuleOnDb->apply_to_all_categories_items == 0 || $cartRuleOnDb->apply_to_all_producers == 0 || $cartRuleOnDb->minimum_quantity > 0 || $cartRuleOnDb->maximum_quantity > 0){

                //se id utente presente nella regola controllare che l'utente del carrello sia tra uno degli utenti a cui applicare la regola altrimenti niente
                $cartRuleUserOnDb = CartRulesUsers::where('cart_rule_id', $idCartRule)->get();

                if ($cartRuleOnDb->apply_to_all_customers == 0){
                    if (CartRulesUsers::where('user_id', $cartOnDb->user_id)->where('cart_rule_id', $idCartRule)->count() <= 0) {
                        $proseguiUser = False;
                    }else{
                        $proseguiUser = True;
                    }
                }else{
                    $proseguiUser = True;
                }

                if($proseguiUser){
                    $cartRuleItemOnDb = CartRulesItem::where('cart_rule_id', $idCartRule)->get(); 
                    $cartRuleProducerOnDb = CartRulesProducers::where('cart_rule_id', $idCartRule)->get(); 
                    $cartRuleSupplierOnDb = CartRulesSuppliers::where('cart_rule_id', $idCartRule)->get();
                    $cartRuleCategoryItemOnDb = CartRulesCategoryItem::where('cart_rule_id', $idCartRule)->get(); 
                    $cartDetailOnDb = CartDetail::where('cart_id', $idCart)->get(); 
                    
                    //mi scorro tutte le righe del carrello e verifico se c'è un articolo (o una categoria) interessata
                    foreach($cartDetailOnDb as $cartDetail){

                        $prosegui = True;
                        
                        //mi memorizzo tutte le categorie dell'articolo della riga ordine
                        $categoryItemOnDb = CategoryItem::where('item_id', $cartDetail->item_id)->get(); 

                        //mi memorizzo tutte le categorie dell'articolo della riga ordine
                        $itemOnDb = Item::find($cartDetail->item_id); 

                        /*************************************************/   
                        //prima condizione: controllo la quantità minima
                        /*************************************************/
                        if (!isset($cartRuleOnDb->minimum_quantity)) 
                            $cartRuleOnDb->minimum_quantity = 0;
                        if (!isset($cartRuleOnDb->maximum_quantity)) 
                            $cartRuleOnDb->maximum_quantity = 999999999;
                        if ($cartDetail->quantity<$cartRuleOnDb->minimum_quantity || $cartDetail->quantity>$cartRuleOnDb->maximum_quantity)
                            $prosegui = False;

                        /*************************************************/   
                        //seconda condizione: controllo se l'articolo è presente nelle regole
                        /*************************************************/
                        if ($cartRuleOnDb->apply_to_all_items == 0){
                            $trovato = False;
                            foreach($cartRuleItemOnDb as $items){
                                if ($cartDetail->item_id == $items->item_id){
                                    //l'ho trovato, esco dal ciclo for
                                    $trovato = True;
                                    break;
                                }
                            }
                            if ($trovato == False)
                                $prosegui = False;
                        }

                        /*************************************************/   
                        //terza condizione: controllo se la categoria dell'articolo è presente nelle regole
                        /*************************************************/
                        if ($cartRuleOnDb->apply_to_all_categories_items == 0){
                            $trovato = False;
                            foreach($cartRuleCategoryItemOnDb as $cartRulesCategories){
                                foreach($categoryItemOnDb as $categories){
                                    if ($cartRulesCategories->category_item_id == $categories->category_id){
                                        //l'ho trovato, esco dal ciclo for
                                        $trovato = True;
                                    break;
                                    }
                                }
                            }
                            if ($trovato == False)
                                $prosegui = False;
                        }

                        /*************************************************/   
                        //quarta condizione: controllo se il produttore dell'articolo è presente nelle regole
                        /*************************************************/
                        if ($cartRuleOnDb->apply_to_all_producers == 0){
                            $trovato = False;
                            foreach($cartRuleProducerOnDb as $producers){
                                if ($itemOnDb->producer_id == $producers->producer_id){
                                    //l'ho trovato, esco dal ciclo for
                                    $trovato = True;
                                    break;
                                }
                            }
                            if ($trovato == False)
                                $prosegui = False;
                        }

                        /*************************************************/   
                        //quinta condizione: controllo se il fornitore dell'articolo è presente nelle regole
                        /*************************************************/
                        if ($cartRuleOnDb->apply_to_all_suppliers == 0){
                            $trovato = False;
                            foreach($cartRuleSupplierOnDb as $suppliers){
                                if ($itemOnDb->supplier_id == $suppliers->supplier_id){
                                    //l'ho trovato, esco dal ciclo for
                                    $trovato = True;
                                    break;
                                }
                            }
                            if ($trovato == False)
                                $prosegui = False;
                        }

                        //se ho rispettato tutte le condizioni, applico lo sconto
                        if ($prosegui==True){
                            //se lo sconto da applicare è sui prodotti selezionati
                            if($cartRuleOnDb->apply_discount_to == 1){

                                //creazione controllo per applicazione sconto a prodotti su categoria o prodotti specifici o all'ordine totale

                                $item = Item::find($cartDetail->item_id);
                                
                                //sconto percentuale
                                if ($cartRuleOnDb->discount_type == 1 && $cartRuleOnDb->discount_value > 0){

                                    $discount_value = static::_calculateDiscountValue($cartRuleOnDb->discount_value, $item->price);
                                    //$discount_value = ($item->price / 100) * $cartRuleOnDb->discount_value;
                                    $cartDetail->discount_percentage = $cartRuleOnDb->discount_value;
                                    $newPrice = $item->price - $discount_value;
                                    $cartDetail->unit_price= $newPrice;
                                //Sconto in valore
                                }else if ($cartRuleOnDb->discount_type == 2 && $cartRuleOnDb->discount_value > 0){
                                    $cartDetail->discount_value = $cartRuleOnDb->discount_value;
                                    $newPrice = $item->price - $cartDetail->discount_value;
                                    $cartDetail->unit_price= $newPrice;
                                }else{
                                    $newPrice = $item->price;
                                    $cartDetail->unit_price= $newPrice;
                                }
                                
                                
                                if (isset($item->vat_type_id)) {
                                    $cartDetail->vat_type_id = $item->vat_type_id;
                                    
                                    $vatTypeRate = VatType::where('id', $item->vat_type_id)->first()->rate;
                    
                                    if ($vatTypeRate != null && $vatTypeRate>0){
                                        $PriceIncludingVat = SettingBL::getByCode(SettingEnum::PriceIncludingVat);
                                        //verifico se devo scorporare l'iva o aggiungerla
                                        if ($PriceIncludingVat=='True'){
                                            $baseImponibile = (100*$newPrice*$cartDetail->quantity)/(100+$vatTypeRate);
                    
                                            $cartDetail->net_amount = $baseImponibile;
                                            $cartDetail->vat_amount = ($newPrice*$cartDetail->quantity) - $baseImponibile;
                                            $cartDetail->total_amount = $newPrice*$cartDetail->quantity;
                                        }else{
                                            $cartDetail->net_amount = $newPrice*$cartDetail->quantity;
                                            $cartDetail->vat_amount = ($cartDetail->net_amount/100)*$vatTypeRate;
                                            $cartDetail->total_amount = $cartDetail->net_amount + $cartDetail->vat_amount;
                                        }
                                    }
                                }else{
                                    $cartDetail->vat_amount = 0;
                                    $cartDetail->net_amount = $newPrice*$cartDetail->quantity;
                                    $cartDetail->total_amount = $newPrice*$cartDetail->quantity;
                                }
                                    
                                $cartDetail->save();

                                /************************************************/
                                //mi storicizzo la regola che ho applicato
                                /************************************************/
                                static::saveCartRuleApplied ($idCartRule, $idCart);

                            }else{
                                //controllo per applicazione sconto sul carrello
                                if($cartRuleOnDb->apply_discount_to == 0){
                                    $cartOnDb = Cart::find($idCart); 
                                    
                                    if ($cartRuleOnDb->free_shipping){ 
                                        $cartOnDb->total_amount = $cartOnDb->total_amount - $cartOnDb->shipment_amount;
                                        $cartOnDb->shipment_amount = 0;
                                        $cartOnDb->save();
                                    }

                                    switch ($cartRuleOnDb->discount_type) {
                                        case 0:
                                            //non devo fare nulla
                                            break;
                                        case 1:
                                            //sconto in percentuale
                                            $value = $cartRuleOnDb->discount_value;
                                            $discountValue = ($cartOnDb->total_amount/100)*$value;
                                            $cartOnDb->total_amount = $cartOnDb->total_amount - $discountValue;
                                            $cartOnDb->discount_value = $discountValue;
                                            $cartOnDb->discount_percentage = $cartRuleOnDb->discount_value;
                                            $cartOnDb->save();
                                            break;
                                        case 2:
                                            //Sconto in valore
                                            $discount_value = $cartRuleOnDb->discount_value;
                                            $cartOnDb->total_amount = $cartOnDb->total_amount - $cartRuleOnDb->discount_value;
                                            $cartOnDb->discount_value = $cartRuleOnDb->discount_value;                            
                                            $cartOnDb->discount_percentage = 0;
                                            $cartOnDb->save();
                                            break;
                                    }

                                    static::saveCartRuleApplied ($idCartRule, $idCart);
                                }else{
                                    //controllo per applicazione sconto a prodotti specifici
                                    if($cartRuleOnDb->apply_discount_to == 3){
                                        //controllo se il bit aggiungi automaticamente al carrello è true
                                        //se true ciclo gli articoli nel carrello per vedere se sono già presenti
                                        //se sono già presenti 
                                    }
                                }
                            }
                        }
                    }
                }
            } else{
                //regola da applicare al carrello globale
                $cartOnDb = Cart::find($idCart); 
                $prosegui = True;
                
                
                /*************************************************/   
                //prima condizione: controllo l'importo minimo
                /*************************************************/ 
                $importoDaVerificare = $cartOnDb->total_amount;
                if ($cartRuleOnDb->taxes_excluded==True)
                    $importoDaVerificare = $importoDaVerificare - $cartOnDb->vat_amount;
                if ($cartRuleOnDb->shipping_excluded==True)
                    $importoDaVerificare = $importoDaVerificare - $cartOnDb->shipment_amount;

                if (!isset($cartRuleOnDb->minimum_order_amount)) {
                    $cartRuleOnDb->minimum_order_amount = 0;
                }

                if ($cartRuleOnDb->minimum_order_amount>$importoDaVerificare)
                    $prosegui = False;
 
                /*************************************************/    
                //seconda condizione: verifico l'utente
                /*************************************************/
                if ($cartRuleOnDb->apply_to_all_customers == 0){
                    if (CartRulesUsers::where('user_id', $cartOnDb->user_id)->where('cart_rule_id', $cartRuleOnDb->id)->count() <= 0) {
                         $prosegui = False;
                    }
                }
                
                /*************************************************/   
                //terza condizione: verifico il gruppo utente
                 /*************************************************/   
                if ($cartRuleOnDb->apply_to_all_groups == 0){
                    $gruppoTrovato = False;
                    $allUserRoles = RoleUser::where('user_id', $cartOnDb->user_id)->get(); 
                    foreach($allUserRoles as $row){
                        if (CartRulesRoleUser::where('role_user_id', $row->role_id)->where('cart_rule_id', $cartRuleOnDb->id)->count() > 0) {
                            $gruppoTrovato = True;
                            break;
                       }
                    }
                    if ($gruppoTrovato == False)
                        $prosegui = False;
                }

                /*************************************************/   
                //quarta condizione: verifico il vettore
                /*************************************************/                  
                if ($cartRuleOnDb->apply_to_all_carriers == 0){
                    if (CartRulesCarriers::where('carrier_id', $cartOnDb->carrier_id)->where('cart_rule_id', $cartRuleOnDb->id)->count() <= 0) {
                        $prosegui = False;
                   }
                }

                
                //Se ho passato tutte le condizioni, applico lo sconto
                if ($prosegui==True){
                    //se ho le spese di spedizione gratuite le tolgo dalla testata
                    if ($cartRuleOnDb->free_shipping){ 
                        $cartOnDb->total_amount = $cartOnDb->total_amount - $cartOnDb->shipment_amount;
                        $cartOnDb->shipment_amount = 0;
                        $cartOnDb->save();
                    }

                    switch ($cartRuleOnDb->discount_type) {
                        case 0:
                            //non devo fare nulla
                            break;
                        case 1:
                            //sconto in percentuale
                            $value = $cartRuleOnDb->discount_value;
                            $discountValue = ($cartOnDb->total_amount/100)*$value;
                            $cartOnDb->total_amount = $cartOnDb->total_amount - $discountValue;
                            $cartOnDb->discount_value = $discountValue;
                            $cartOnDb->discount_percentage = $cartRuleOnDb->discount_value;
                            $cartOnDb->save();
                            break;
                        case 2:
                            //Sconto in valore
                            $discount_value = $cartRuleOnDb->discount_value;
                            $cartOnDb->total_amount = $cartOnDb->total_amount - $cartRuleOnDb->discount_value;
                            $cartOnDb->discount_value = $cartRuleOnDb->discount_value;                            
                            $cartOnDb->discount_percentage = 0;
                            $cartOnDb->save();
                            break;
                    }
                    /************************************************/
                    //mi storicizzo la regola che ho applicato
                    /************************************************/
                    static::saveCartRuleApplied ($idCartRule, $idCart);
                }

                
            }
        } catch (Exception $ex) {
            throw $ex;
        } 
        
    }

    public static function saveCartRuleApplied(int $idRuleApplied, int $idCart){
        try{
            
            $cartRule = CartRules::where('id', $idRuleApplied)->get()->first();
            $cartRuleCarrierOnDb = CartRulesCarriers::where('cart_rule_id', $idRuleApplied)->get(); 
            $cartRuleCategoryItemOnDb = CartRulesCategoryItem::where('cart_rule_id', $idRuleApplied)->get(); 
            $cartRuleItemOnDb = CartRulesItem::where('cart_rule_id', $idRuleApplied)->get(); 
            $cartRuleRoleUserOnDb = CartRulesRoleUser::where('cart_rule_id', $idRuleApplied)->get(); 
            $cartRuleUserOnDb = CartRulesUsers::where('cart_rule_id', $idRuleApplied)->get(); 
            $cartRuleProducersOnDb = CartRulesProducers::where('cart_rule_id', $idRuleApplied)->get(); 
            $cartRuleSupplierOnDb = CartRulesSuppliers::where('cart_rule_id', $idRuleApplied)->get();

            $newCartRule = new CartRulesApplied();
            $newCartRule->cart_rule_id = $idRuleApplied;
            $newCartRule->cart_id_temporary = $idCart;
            $newCartRule->name = $cartRule->name;
            $newCartRule->description = $cartRule->description;
            $newCartRule->voucher_code = $cartRule->voucher_code;
            $newCartRule->voucher_type_id = $cartRule->voucher_type_id;
            $newCartRule->availability = $cartRule->availability;
            $newCartRule->availability_residual = $cartRule->availability_residual;
            $newCartRule->availability_per_user = $cartRule->availability_per_user;
            $newCartRule->priority = $cartRule->priority;
            $newCartRule->exclude_other_rules = $cartRule->exclude_other_rules;
            $newCartRule->online = $cartRule->online;
            $newCartRule->enabled_from = $cartRule->enabled_from;
            $newCartRule->enabled_to = $cartRule->enabled_to;
            $newCartRule->apply_to_all_customers = $cartRule->apply_to_all_customers ;
            $newCartRule->apply_to_all_groups =  $cartRule->apply_to_all_groups ;
            $newCartRule->apply_to_all_carriers = $cartRule->apply_to_all_carriers ;
            $newCartRule->apply_to_all_items = $cartRule->apply_to_all_items ;
            $newCartRule->apply_to_all_categories_items = $cartRule->apply_to_all_categories_items ;
            $newCartRule->apply_to_all_producers = $cartRule->apply_to_all_producers;
            $newCartRule->apply_to_all_suppliers = $cartRule->apply_to_all_suppliers;
            $newCartRule->minimum_quantity = $cartRule->minimum_quantity ;
            $newCartRule->maximum_quantity = $cartRule->maximum_quantity ;
            $newCartRule->minimum_order_amount = $cartRule->minimum_order_amount ;
            $newCartRule->taxes_excluded = $cartRule->taxes_excluded ;
            $newCartRule->shipping_excluded = $cartRule->shipping_excluded ; 
            $newCartRule->free_shipping = $cartRule->free_shipping ; 
            $newCartRule->discount_type = $cartRule->discount_type ;
            $newCartRule->discount_value = $cartRule->discount_value ;
            $newCartRule->apply_discount_to = $cartRule->apply_discount_to ;
            $newCartRule->add_to_cart_automatically = $cartRule->add_to_cart_automatically ;
            $newCartRule->save();

            foreach($cartRuleCarrierOnDb as $row){
                $newCartRuleCarrier = New CartRulesCarriersApplied();
                $newCartRuleCarrier->cart_rule_id = $newCartRule->id;
                $newCartRuleCarrier->carrier_id = $row->carrier_id;
                $newCartRuleCarrier->save();
            }
            foreach($cartRuleCategoryItemOnDb as $row){
                $newCartRuleCategoryItem = New CartRulesCategoryItemApplied();
                $newCartRuleCategoryItem->cart_rule_id = $newCartRule->id;
                $newCartRuleCategoryItem->category_item_id = $row->category_item_id;
                $newCartRuleCategoryItem->save();
            }
            foreach($cartRuleItemOnDb as $row){
                $newCartRulesItems = New CartRulesItemApplied();
                $newCartRulesItems->cart_rule_id = $newCartRule->id;
                $newCartRulesItems->item_id = $row->item_id;
                $newCartRulesItems->save();
            }
            foreach($cartRuleRoleUserOnDb as $row){
                $newCartRuleRoleUser = New CartRulesRoleUserApplied();
                $newCartRuleRoleUser->cart_rule_id = $newCartRule->id;
                $newCartRuleRoleUser->role_user_id = $row->role_user_id;
                $newCartRuleRoleUser->save();
            }
            foreach($cartRuleUserOnDb as $row){
                $newCartRuleUser = New CartRulesUsersApplied();
                $newCartRuleUser->cart_rule_id = $newCartRule->id;
                $newCartRuleUser->user_id = $row->user_id;
                $newCartRuleUser->save();
            }

            foreach($cartRuleProducersOnDb as $row){
                $newCartRuleUser = New CartRulesProducersApplied();
                $newCartRuleUser->cart_rule_id = $newCartRule->id;
                $newCartRuleUser->producer_id = $row->producer_id;
                $newCartRuleUser->save();
            }
            foreach($cartRuleSupplierOnDb as $row){
                $newCartRuleUser = New CartRulesSuppliersApplied();
                $newCartRuleUser->cart_rule_id = $newCartRule->id;
                $newCartRuleUser->supplier_id = $row->supplier_id;
                $newCartRuleUser->save();
            }
        } catch (Exception $ex) {
            throw $ex;
        } 
    }

    public static function transformCartIntoSalesOrder(int $idCart, int $idPayment){
        
        try {

            //mi memorizzo il record del carrello e tutte le sue righe
            $cart = Cart::where('id', $idCart)->get()->first();
            $cartDetails = CartDetailBL::getAllByCart($idCart);

            /***********************************************************/
            //inserisco la testata dell'ordine
            /***********************************************************/
            $newSalesOrder = new SalesOrder();            
            //$mytime->toDateTimeString(); 
           // $mytime = Carbon::createFromFormat('Y/m/d H:i:s', $mytime)->toDateTimeString();
           /* $dt = new DateTime();
            $dt->format('Y-m-d H:i:s');*/
           // $newSalesOrder->date_order = Carbon::now()->format('Y/m/d H:i:s');  
            // DateTime::createFromFormat('Y-m-d', $newSalesOrder->date_order);  
            //Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
            //Carbon::parse($dateorderformatter)->format('d/m/Y H:i:s');
            //date('Y-m-d H:i:s', strtotime($newSalesOrder->date_order));
            //Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $mytime)->format('Y/m/d H:i:s');
            $mytime = Carbon::now()->format('Y-m-d H:i:s');
            $newSalesOrder->date_order = $mytime;
       
            //$newSalesOrder->order_state_id = 01234    qua ci andrà di default il codice stato "inserito"
            $newSalesOrder->user_id = $cart->user_id;
            $newSalesOrder->salesman_id = $cart->salesman_id;
            $newSalesOrder->currency_id = $cart->currency_id;
            $newSalesOrder->payment_type_id = $cart->payment_type_id;
            if($idPayment > 0){
                $newSalesOrder->payment_id = $idPayment;
            }
            $newSalesOrder->net_amount = $cart->net_amount;
            $newSalesOrder->vat_amount = $cart->vat_amount;
            $newSalesOrder->shipment_amount = $cart->shipment_amount;
            $newSalesOrder->payment_cost = $cart->payment_cost;
            $newSalesOrder->service_amount = $cart->service_amount;
            $newSalesOrder->discount_percentage = $cart->discount_percentage;
            $newSalesOrder->discount_value = $cart->discount_value;
            $newSalesOrder->total_amount = $cart->total_amount;
            $newSalesOrder->note = $cart->note;
            $newSalesOrder->user_address_goods_id = $cart->user_address_goods_id;
            $newSalesOrder->user_address_documents_id = $cart->user_address_documents_id;
            $newSalesOrder->carrier_id = $cart->carrier_id;

            $INVIATO = 'INVIATO'; 
            $documentstatuses = DocumentStatus::where('code', $INVIATO)->first();
            if (isset ($documentstatuses)) {
                $newSalesOrder->order_state_id = $documentstatuses->id;
            } else {
                $newSalesOrder->order_state_id = NULL;
            }

            //$newSalesOrder->$carriage_paid_to_id;
            //$newSalesOrder->$fulfillment_date;
            //$newSalesOrder->$to_be_updated;
            //$newSalesOrder->$updated;
            $newSalesOrder->save();   
            
            $logs = new Logs();
            $logs->user_id = $cart->user_id;
           // $logs->ip = $cart->ip(); 
            $logs->action = 'PURCHASEORDER';
            $logs->description = 'Ordine pagato';
            $logs->order_id = $newSalesOrder->id;
            $logs->save();

            $BookingConnectionAppointmentsCartDetail = BookingConnectionAppointmentsCartDetail::where('cart_id', $idCart)->get();
            if (isset($BookingConnectionAppointmentsCartDetail)) {
                foreach ($BookingConnectionAppointmentsCartDetail as $res) {
                        $BookingAppointments = new BookingAppointments();

                        $BookingAppointments->connections_id = $res->connections_id;
                        $BookingAppointments->user_id = $cart->user_id;
                        $BookingAppointments->start_time = $res->start_time;
                        $BookingAppointments->price = $res->price;
                        $BookingAppointments->nr_place = $res->nr_place;
                        $BookingAppointments->total_price = $res->total_price;
                        $BookingAppointments->reduced_price = $res->reduced_price;
                        $BookingAppointments->end_time = $res->end_time;
                        $BookingAppointments->date_from = $res->date_from;
                        $BookingAppointments->date_to= $res->date_from;
                        $BookingAppointments->save();
                }
                BookingConnectionAppointmentsCartDetail::where('cart_id', $idCart)->delete();
            }else{
                $BookingConnectionAppointmentsCartDetail = '';
            }
          
            /***********************************************************/
            //inserisco tutte le righe dell'ordine
            /***********************************************************/
            foreach($cartDetails as $row){
                $newSalesOrderDetail = new SalesOrderDetail();
                $newSalesOrderDetail->order_id = $newSalesOrder->id;
                $newSalesOrderDetail->row_number = $row->nr_row;
                //$newSalesOrderDetail->sales_order_row_type_id = 1234;
                //$newSalesOrderDetail->order_detail_state_id = 1234;
                $newSalesOrderDetail->item_id = $row->item_id;
                $newSalesOrderDetail->description = $row->description;
                $newSalesOrderDetail->unit_of_measure_id = $row->unit_of_measure_id;
                $newSalesOrderDetail->unit_price = $row->unit_price;
                $newSalesOrderDetail->quantity = $row->quantity;
                $newSalesOrderDetail->net_amount = $row->net_amount;
                $newSalesOrderDetail->vat_amount = $row->vat_amount;
                $newSalesOrderDetail->discount_percentage = $row->discount_percentage;
                $newSalesOrderDetail->discount_value = $row->discount_value;
                $newSalesOrderDetail->vat_type_id = $row->vat_type_id;
                $newSalesOrderDetail->total_amount = $row->total_amount;
                $newSalesOrderDetail->note = $row->note;
                if(isset($row->width)){
                    $newSalesOrderDetail->width = $row->width; 
                }
                if(isset($row->depth)){
                    $newSalesOrderDetail->depth = $row->depth;
                }    
                if(isset($row->height)){
                    $newSalesOrderDetail->height = $row->height;
                }
                if(isset($row->support_id)){
                    $newSalesOrderDetail->support_id = $row->support_id;
                }
                $newSalesOrderDetail->save();

                $itemsdigital = Item::where('id', $newSalesOrderDetail->item_id)->get()->first();
                if (isset($itemsdigital)){
                    if (($itemsdigital->digital == true)){
                    $newSalesOrderTokenLinkUser = new SalesOrderTokenLinkUser();
                    $newSalesOrderTokenLinkUser->id_user = $newSalesOrder->user_id;
                    $newSalesOrderTokenLinkUser->token_link = str_random(32);
                    $newSalesOrderTokenLinkUser->sales_order_detail_id = $newSalesOrderDetail->id;
                    $newSalesOrderTokenLinkUser->sales_order_id = $newSalesOrderDetail->order_id;
                    $newSalesOrderTokenLinkUser->save(); 
          
                    }
                }
                
                $cartDetailOptional = CartDetailOptional::where('cart_detail_id', $row->id)->get();
                if (isset($cartDetailOptional)){
                    foreach ($cartDetailOptional as $cartDetailOptionals){
                        $newSalesOrderDetailOptional = new SalesOrderDetailOptional();
                        $newSalesOrderDetailOptional->sales_order_detail_id = $newSalesOrderDetail->id;
                        $newSalesOrderDetailOptional->optional_id = $cartDetailOptionals->optional_id;
                        $newSalesOrderDetailOptional->save();
                        CartDetailOptional::where('cart_detail_id', $row->id)->delete();
                    }
                }   
            }

            /***********************************************************/
            //trasformo tutte le regole applicate al carrello in ordine
            /***********************************************************/
            foreach (CartRulesApplied::where('cart_id_temporary', $idCart)->get() as $cartRulesAll) {
                $cartRulesAll->cart_id_temporary = null;  //tolgo l'id del carrello perchè non mi serve più
                $cartRulesAll->sales_order_id = $newSalesOrder->id;  //metto l'id del nuovo ordine appena creato
                $cartRulesAll->save();
            }

            /***********************************************************/
            //cancello tutte le righe del carrello
            /***********************************************************/

            CartDetail::where('cart_id', $idCart)->delete();
          
            /***********************************************************/
            //pulisco la testata del carrello
            /***********************************************************/
            //$cart->session_token = null;
            //$cart->currency_id = null;
            $cart->net_amount = 0;
            $cart->vat_amount = 0;
            $cart->shipment_amount = 0;
            $cart->total_amount = 0;
            $cart->discount_percentage = 0;
            $cart->discount_value = 0;
            $cart->note = '';
            $cart->payment_cost = 0;
            $cart->service_amount = 0;
            $cart->user_address_goods_id = null;
            $cart->user_address_documents_id = null;
            $cart->save();

            return $newSalesOrder->id;

        } catch (Exception $ex) {
            throw $ex;
        } 
    }

    /*****************************************************************************/
    /* FUNZIONE CHE ELABORA E RESTITUISCE EVENTUALE PREZZO PER ARTICOLO / UTENTE */
    /*****************************************************************************/
    public static function getPriceCalculated(int $idItem, string $idUser){
        $item = Item::find($idItem);
        $return = "";
        $categoryItemOnDb = CategoryItem::where('item_id', $idItem)->get();
        $strWhereUser ="";
        

        if($idUser!='' && !is_null($idUser)){
            $strWhereUser = ' or carts_rules_users.user_id = ' . $idUser;
        }


        foreach (DB::select(' SELECT distinct carts_rules.id, carts_rules.exclude_other_rules, carts_rules.discount_type, carts_rules.discount_value, carts_rules.apply_to_all_categories_items, carts_rules.apply_to_all_suppliers, carts_rules.apply_to_all_customers, carts_rules.apply_to_all_items, carts_rules.apply_to_all_producers, priority from carts_rules
                            left join carts_rules_items on carts_rules_items.cart_rule_id = carts_rules.id
                            left join carts_rules_users on carts_rules_users.cart_rule_id = carts_rules.id
                            left join carts_rules_producers on carts_rules_producers.cart_rule_id = carts_rules.id
                            left join carts_rules_categories_items on carts_rules_categories_items.cart_rule_id = carts_rules.id
                            where online = True
                            and (
                                (enabled_from is null and enabled_to is null)
                                or 
                                (enabled_from is null and enabled_to > now())
                                or 
                                (enabled_from < now() and enabled_to is null)
                                or 
                                (enabled_from < now() and enabled_to > now())
                            )
                            and (
                                (availability is null or (availability > 0 and availability_residual > 0))
                            )
                            and (apply_to_all_items = 0 or apply_to_all_categories_items = 0 or apply_to_all_customers = 0 or apply_to_all_producers = 0)
                            and (carts_rules_items.item_id = ' . $idItem . $strWhereUser . ' or carts_rules_categories_items.category_item_id in (SELECT category_id from categories_items where item_id = ' . $idItem . ') or carts_rules_producers.producer_id in (SELECT producer_id from items where id = ' . $idItem . '))
                            order by priority')
        as $regoleDaApplicareAlleRighe) {
                $proseguiUser = False;
                $prosegui = True;
                $trovato = False;
            //if($idUser!='' && !is_null($idUser)){
                $cartRuleItemOnDb = CartRulesItem::where('cart_rule_id', $regoleDaApplicareAlleRighe->id)->get(); 
                $cartRuleProducerOnDb = CartRulesProducers::where('cart_rule_id', $regoleDaApplicareAlleRighe->id)->get();
                $cartRuleSupplierOnDb = CartRulesSuppliers::where('cart_rule_id', $regoleDaApplicareAlleRighe->id)->get();
                $cartRuleCategoryItemOnDb = CartRulesCategoryItem::where('cart_rule_id', $regoleDaApplicareAlleRighe->id)->get(); 
                
                if ($regoleDaApplicareAlleRighe->apply_to_all_customers == 0){
                    if($idUser!='' && !is_null($idUser)){
                        if (CartRulesUsers::where('user_id', $idUser)->where('cart_rule_id', $regoleDaApplicareAlleRighe->id)->count() <= 0) {
                            $proseguiUser = False;
                        }else{
                            $proseguiUser = True;
                        }
                    }else{
                        $proseguiUser = False;
                    }
                }else{
                    $proseguiUser = True;
                }

                if($proseguiUser){
                    /*************************************************/   
                    //prima condizione: controllo se l'articolo è presente nelle regole
                    /*************************************************/
                    if ($regoleDaApplicareAlleRighe->apply_to_all_items == 0){
                        $trovato = False;
                        foreach($cartRuleItemOnDb as $itemsCart){
                            if ($idItem == $itemsCart->item_id){
                                //l'ho trovato, esco dal ciclo for
                                $trovato = True;
                                break;
                            }
                        }
                        if ($trovato == False)
                            $prosegui = False;
                    }

                    /*************************************************/   
                    //deconda condizione: controllo se la categoria dell'articolo è presente nelle regole
                    /*************************************************/
                    if ($regoleDaApplicareAlleRighe->apply_to_all_categories_items  == 0){
                        $trovato = False;
                        foreach($cartRuleCategoryItemOnDb as $cartRulesCategories){
                            foreach($categoryItemOnDb as $categories){
                                if ($cartRulesCategories->category_item_id == $categories->category_id){
                                    //l'ho trovato, esco dal ciclo for
                                    $trovato = True;
                                    break;
                                }
                            }
                        }
                        if ($trovato == False)
                            $prosegui = False;
                    }

                    /*************************************************/   
                    //terza condizione: controllo se il produttore dell'articolo è presente nelle regole
                    /*************************************************/
                    if ($regoleDaApplicareAlleRighe->apply_to_all_producers == 0){
                        $trovato = False;
                        foreach($cartRuleProducerOnDb as $producerCart){
                            if ($item->producer_id == $producerCart->producer_id){
                                //l'ho trovato, esco dal ciclo for
                                $trovato = True;
                                break;
                            }
                        }
                        if ($trovato == False)
                            $prosegui = False;
                    }

                    /*************************************************/   
                    //quarta condizione: controllo se il fornitore dell'articolo è presente nelle regole
                    /*************************************************/
                    if ($regoleDaApplicareAlleRighe->apply_to_all_suppliers == 0){
                        $trovato = False;
                        foreach($cartRuleSupplierOnDb as $supplierCart){
                            if ($item->supplier_id == $supplierCart->supplier_id){
                                //l'ho trovato, esco dal ciclo for
                                $trovato = True;
                                break;
                            }
                        }
                        if ($trovato == False)
                            $prosegui = False;
                    }

                    //se ho rispettato tutte le condizioni, applico lo sconto
                    if ($prosegui){
                        //sconto percentuale
                        if ($regoleDaApplicareAlleRighe->discount_type == 1 && $regoleDaApplicareAlleRighe->discount_value > 0){

                            $discount_value = static::_calculateDiscountValue($regoleDaApplicareAlleRighe->discount_value, $item->price);
                            //$discount_value = ($item->price / 100) * $regoleDaApplicareAlleRighe->discount_value;
                            $newPrice = $item->price - $discount_value;
                        //Sconto in valore
                        }else if ($regoleDaApplicareAlleRighe->discount_type == 2 && $regoleDaApplicareAlleRighe->discount_value > 0){
                            $newPrice = $item->price - $regoleDaApplicareAlleRighe->discount_value;
                        }else{
                            $newPrice = "";
                        }

                        $return = $newPrice;

                        if($regoleDaApplicareAlleRighe->exclude_other_rules){
                            break;
                        }
                    }
                }
            //}
        }   
        return $return;
    }
}

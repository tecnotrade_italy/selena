<?php

namespace App\BusinessLogic;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoTipology;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Plant;
use App\ItemGroupTechnicalSpecification;
use Intervention\Image\Facades\Image;
use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;


class PlantBL
{
 #region GET

 /**
     * Get select
     *
     * @param String $search
     * @return Intervention
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return Plant::select('id', 'description_plant as description')->get();
        } else {
            return Plant::where('description_plant', 'ilike', $search . '%')->select('id', 'description_plant as description')->get();
        }
    }

 }

 
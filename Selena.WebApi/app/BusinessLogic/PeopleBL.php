<?php

namespace App\BusinessLogic;

use App\CorrelationPeopleUser;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoPeople;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\User;
use App\People;
use App\PeopleUser;
use App\Role;
use App\RolesPeople;
use App\RoleUser;
use App\UsersDatas;
use App\Content;
use App\ContentLanguage;
use App\CartRulesUsers;
use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;


class PeopleBL
{

     #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request DtoRole
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoPeople, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoPeople->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (People::find($DtoPeople->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($DtoPeople->name)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoPeople->surname)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SurnameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoPeople->email)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoPeople->phone_number)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TelephoneNumberNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoPeople->user_id)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

    }

//    getByIdReferent

 /**
     * getAllReferent
     * 
     * @param int id
     * @return DtoUser
     */
    public static function getAllReferent(int $id, int $idFunctionality)
    {
        $result = collect();
        foreach (PeopleUser::where('user_id', $id)->get() as $PeopleUser) {
            $People=People::where('id',$PeopleUser->people_id)->first();
            
            if (isset($People)) {

                $DtoPeople = new DtoPeople();
                //----- table --------//
                if (isset($People->id)) {
                    $DtoPeople->id = $People->id;
                }

                if (isset($People->name)) {
                    $DtoPeople->names = $People->name;
                }
        
                if (isset($People->surname)) {
                    $DtoPeople->surnames = $People->surname;
                }
                if (isset($People->phone_number)) {
                    $DtoPeople->phone_numbers = $People->phone_number;
                } 
                if (isset($People->email)) {
                    $DtoPeople->email = $People->email;
                } 
                if (isset($People->email)) {
                    $DtoPeople->email = $People->email;
                }
                if (isset($People->email)) {
                    $DtoPeople->username = $People->email;
                } 
                    //-------- detail --------//
                if (isset($People->name)) {
                    $DtoPeople->name_referent = $People->name;
                }
                if (isset($People->surname)) {
                    $DtoPeople->surname_referent = $People->surname;
                }
                if (isset($People->phone_number)) {
                    $DtoPeople->phone_number_referent = $People->phone_number;
                }
                if (isset($People->email)) {
                    $DtoPeople->email_referent = $People->email;
                }    
                if (isset($People->online)) {
                    $DtoPeople->online = $People->online;
                }
                if (isset($People->confirmed)) {
                    $DtoPeople->confirmed_referent = $People->confirmed;
                }

                if (isset($PeopleUser->user_id)) {
                    $DtoPeople->user_id = $PeopleUser->user_id;
                }


                $CorrelationPeopleUser = CorrelationPeopleUser::where('id_people', $People->id)->first();
                if (isset($CorrelationPeopleUser)){
                    $User = User::where('id', $CorrelationPeopleUser->id_user)->first();
             

                    if (isset($User->id_table_user)) {
                        $DtoPeople->id_table_user = $User->id;
                    }
                    if (isset($User->name)) {
                        $DtoPeople->username_referent = $User->name;
                    }
                    if (isset($User->password)) {
                        $DtoPeople->password_referent = $User->password;
                    }
                    if (isset($User->confirmed)) {
                        $DtoPeople->confirmed_referent = $User->confirmed;
                    }

                }else{
                  
                    if (isset($User->id_table_user)) {
                        $DtoPeople->id_table_user = '-';
                    }
                    if (isset($User->name)) {
                        $DtoPeople->username_referent = '-';
                    }
                    if (isset($User->password)) {
                        $DtoPeople->password_referent = '-';
                    } 
                }

                $result->push($DtoPeople);
               
            }
        }
        return  $result;
    }

    /**
     * getByIdReferent
     * 
     * @param int id
     * @return DtoUser
     */
    public static function getByIdReferent(int $id)
    {        
      
        $People= People::where('id', $id)->first();
        $RolePeople=RolesPeople::where('people_id', $People->id)->first();
        $RoleUser=PeopleUser::where('people_id', $People->id)->first();

            $DtoPeople = new DtoPeople();

                if (isset($People->id)) {
                    $DtoPeople->id = $People->id;
                }
                if (isset($People->name)) {
                    $DtoPeople->names = $People->name;
                }
        
                if (isset($People->surname)) {
                    $DtoPeople->surnames = $People->surname;
                }
        
                if (isset($People->email)) {
                    $DtoPeople->emails = $People->email;
                }

                if (isset($People->phone_number)) {
                    $DtoPeople->phone_numbers = $People->phone_number;
                } 


                if (isset($RolePeople->roles_id)) {
                    $DtoPeople->roles_id =  $RolePeople->roles_id;
                    $DtoPeople->descriptionRoleId = Role::where('id',$RolePeople->roles_id)->first()->name;
                }
                
                if (isset($RoleUser->user_id)) {
                    $DtoPeople->user_id =  $RoleUser->user_id;
                }

                return  $DtoPeople;
              
            }
   
            

/**
     * getPeopleUser
     * 
     * @param int id
     * @return DtoUser
     */
    public static function getPeopleUserWeb(int $id)
    {
        $result = collect();

        foreach (PeopleUser::where('user_id', $id)->get() as $PeopleUserWeb) {
            $People=People::where('id',$PeopleUserWeb->people_id)->first();
            $RolePeople=RolesPeople::where('people_id',$PeopleUserWeb->people_id)->first();
            //$UserPeople=PeopleUser::where('people_id',)->first();
            
            if (isset($People)) {
                $DtoPeople = new DtoPeople();
            
                //if (isset($People->id)) {
                    $DtoPeople->id = $People->id;
               // }
        
                if (isset($People->name)) {
                    $DtoPeople->name = $People->name;
                }else{
                    $DtoPeople->name = '-';
                }
        
                if (isset($People->surname)) {
                    $DtoPeople->surname = $People->surname;
                }else{
                    $DtoPeople->surname = '-';
                }
        
                if (isset($People->phone_number)) {
                    $DtoPeople->phone_number = $People->phone_number;
                }else{
                    $DtoPeople->phone_number = '-';
                }
                if (isset($People->email)) {
                    $DtoPeople->email = $People->email;
                }else{
                    $DtoPeople->email = '-';
                }

                if (isset($RolePeople->roles_id)) {
                    $descriptionRole = Role::where('id',$RolePeople->roles_id)->first()->name;
                    $DtoPeople->roles_id = $descriptionRole;
                }else{
                    $DtoPeople->roles_id = '-';
                }
                if (isset($PeopleUserWeb->user_id)) {
                    $DtoPeople->user_id = $PeopleUserWeb->user_id;
                }

                $result->push($DtoPeople);
            }
        }
        return  $result;
    }


    /**
     * Get People from user securpoint 
     * 
     * @param int id
     * @return DtoUser
     */
    public static function getAllUteByUserSecurPoint(int $id)
    {
        $userOnDb = User::find($id);

        $finalHtml = "";
        $idContent = Content::where('code', 'ListUsersPeople')->get()->first();
        $contentLanguage = ContentLanguage::where('content_id', $idContent->id)->where('language_id', HttpHelper::getLanguageId())->get()->first();
        
        $tags = SelenaViewsBL::getTagByType('UserList');

        if(isset($userOnDb)){
            if(!is_null($userOnDb->code) && $userOnDb->code != ''){
                $userPeopleOnDb = User::where('code', 'ilike', '%' . $userOnDb->code . '\_%')->orderBy('code', 'DESC')->get();
                if(isset($userPeopleOnDb)){
                    foreach (
                        DB::select(
                            'SELECT users_datas.*, users.id
                            FROM users 
                            inner join users_datas on users.id = users_datas.user_id
                            where users.code ilike \'%' . $userOnDb->code . '\_%\' order by users.code desc'
                        ) as $userPeopleOnDbs) {
                        $newContent = $contentLanguage->content;  
                        foreach ($tags as $fieldsDb){
                            if(isset($fieldsDb->tag)){                                                              
                                if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                    $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                    if(is_numeric($userPeopleOnDbs->{$cleanTag}) && strpos($userPeopleOnDbs->{$cleanTag},'.')){
                                        $tag = str_replace('.',',',substr($userPeopleOnDbs->{$cleanTag}, 0, -2));
                                    }else{
                                        $tag = $userPeopleOnDbs->{$cleanTag};
                                    }
                                    $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                               
                                }
                            }
                        }

                        if (strpos($newContent, '#email#') !== false) {
                            $usersOnDbPeople = User::where('id', $userPeopleOnDbs->id)->first();
                            if (isset($usersOnDbPeople)){
                                $newContent = str_replace('#email#', $usersOnDbPeople->email,  $newContent); 
                            }else{
                                $newContent = str_replace('#email#', '',  $newContent);           
                            }
                        }

                        $finalHtml = $finalHtml . $newContent;
                    }
                }
            }
        }
   
    
        return $finalHtml;
    }

     /**
     * Get People from user securpoint 
     * 
     * @param int id
     * @return DtoUser
     */
    public static function insertUteByUserSecurPoint(request $request)
    {

        DB::beginTransaction();
        try {

            

            $userOnDb = User::find($request->user_id);
            $userDatasOnDB = UsersDatas::where('user_id', $request->user_id)->get()->first();
            $htmlReturn = '';

            if (is_null($request->password)) {
                throw new Exception(DictionaryBL::getTranslate($userDatasOnDB->idLanguage, DictionariesCodesEnum::PasswordNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (User::where('email', $request->email)->count() > 0) {
                throw new Exception(DictionaryBL::getTranslate($userDatasOnDB->idLanguage, DictionariesCodesEnum::EmailAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
            }

            //servirà poi per l'aggiunta di un utente
            if(isset($userOnDb)){
                $userPeopleOnDb = User::where('code', 'ilike', '%' . $userOnDb->code . '\_%')->orderBy('code', 'DESC')->limit(1)->get()->first();
                if(isset($userPeopleOnDb)){
                    $pos = strpos($userPeopleOnDb->code, '_');
                    if ($pos !== false) {
                        $userExplode = explode('_', $userPeopleOnDb->code);
                        $progressivo = intval($userExplode[1]) + 1;
                        $code = $userOnDb->code . '_' . $progressivo;
                    }else{
                        $code = $userOnDb->code . '_2';
                    }
                }else{
                    $code = $userOnDb->code . '_2';
                }
            }

            $user = new User();
            $user->name = $request->email;
            $user->password = bcrypt($request->password);
            $user->email = $request->email;
            $user->code = $code;
            $user->language_id = $userOnDb->language_id;
            $user->online = true;
            $user->confirmed = true;
            $user->processed = false;
            $user->save();

            $UsersData = new UsersDatas();
            $UsersData->user_id = $user->id;
            $UsersData->language_id = $userOnDb->language_id;
            $UsersData->code = $code;
            if (isset($request->name)) {    
                $UsersData->name = $request->name . ' ' . $request->surname;
            }else{
                $UsersData->name = '-';
            } 
            if (isset($request->surname)) {
                $UsersData->surname = $request->surname;
            }else{
                $UsersData->surname = '-';
            } 

            if (isset($request->phone_number)) {
                $UsersData->telephone_number = $request->phone_number;
            } 

            $UsersData->country = $userDatasOnDB->country;
            $UsersData->address = $userDatasOnDB->address;
            $UsersData->postal_code = $userDatasOnDB->postal_code;
            $UsersData->province = $userDatasOnDB->province;
            $UsersData->id_payments = $userDatasOnDB->id_payments;
            $UsersData->id_payment = $userDatasOnDB->id_payment;
            $UsersData->business_name = $userDatasOnDB->business_name;
            $UsersData->vat_number = $userDatasOnDB->vat_number;
            $UsersData->fiscal_code = $userDatasOnDB->fiscal_code;
            $UsersData->fax_number = $userDatasOnDB->fax_number;

            $UsersData->save();

            $cartRulesUsersOnDB = CartRulesUsers::where('user_id', $userOnDb->id)->get();
            if(isset($cartRulesUsersOnDB)){
                foreach ($cartRulesUsersOnDB as $cartRulesUsersOnDBs){
                    $newCartRulesUsers = new CartRulesUsers();
                    $newCartRulesUsers->user_id = $user->id;
                    $newCartRulesUsers->cart_rule_id = $cartRulesUsersOnDBs->cart_rule_id;
                    $newCartRulesUsers->save();   
                }
            }
            
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $user->id;

        //servirà poi per l'aggiunta di un utente
    }

    /**
     * getPeopleUser
     * 
     * @param int id
     * @return DtoUser
     */
    public static function getPeopleUser(int $id)
    {
        $result = collect();

        foreach (PeopleUser::where('user_id', $id)->get() as $PeopleUser) {
            $People=People::where('id',$PeopleUser->people_id)->first();
            $RolePeople=RolesPeople::where('people_id',$PeopleUser->people_id)->first();
            //$UserPeople=PeopleUser::where('people_id',)->first();
            
            if (isset($People)) {
                $DtoPeople = new DtoPeople();
            
                if (isset($People->id)) {
                    $DtoPeople->id = $People->id;
                }
        
                if (isset($People->name)) {
                    $DtoPeople->name = $People->name;
                }else{
                    $DtoPeople->name = '-';
                }
        
                if (isset($People->surname)) {
                    $DtoPeople->surname = $People->surname;
                }else{
                    $DtoPeople->surname = '-';
                }
        
                if (isset($People->phone_number)) {
                    $DtoPeople->phone_number = $People->phone_number;
                }else{
                    $DtoPeople->phone_number = '-';
                }
                if (isset($People->email)) {
                    $DtoPeople->email = $People->email;
                }else{
                    $DtoPeople->email = '-';
                }

                if (isset($RolePeople->roles_id)) {
                    $descriptionRole = Role::where('id',$RolePeople->roles_id)->first()->name;
                    $DtoPeople->roles_id = $descriptionRole;
                }else{
                    $DtoPeople->roles_id = '-';
                }
                if (isset($PeopleUser->user_id)) {
                    $DtoPeople->user_id = $PeopleUser->user_id;
                }

                $result->push($DtoPeople);
            }
        }
        return  $result;
    }




    /**
     * Convert to dto
     * 
     * @param People
     * 
     * @return DtoPeople
     */
    private static function _convertToDto($People)
    {
        $DtoPeople = new DtoPeople();
        if (isset($People->id)) {
            $DtoPeople->id = $People->id;
        }
        if (isset($People->name)) {
            $DtoPeople->name = $People->name;
        }
        if (isset($People->surname)) {
            $DtoPeople->surname = $People->surname;
        }
        if (isset($People->email)) {
            $DtoPeople->email = $People->email;
        }
        if (isset($People->phone_number)) {
            $DtoPeople->phone_number = $People->phone_number;
        }
        if (isset($People->user_id)) {
            $DtoPeople->user_id = $People->user_id;
        }

        if (isset($People->name)) {
            $DtoPeople->names = $People->name;
        }

        if (isset($People->surname)) {
            $DtoPeople->surnames = $People->surname;
        }

        if (isset($People->phone_number)) {
            $DtoPeople->phone_numbers = $People->phone_number;
        } 

        if (isset($People->name)) {
            $DtoPeople->name_referent = $People->name;
        }

        if (isset($People->surname)) {
            $DtoPeople->surname_referent = $People->surname;
        }

        if (isset($People->phone_number)) {
            $DtoPeople->phone_number_referent = $People->phone_number;
        }
        if (isset($People->email)) {
            $DtoPeople->email_referent = $People->email;
        }

        return $DtoPeople;
    }


    /**
     * Convert to VatType
     * 
     * @param DtoPeople DtoPeople
     * 
     * @return People
     */
    private static function _convertToModel($DtoPeople)
    {
        $People = new People();

        if (isset($DtoPeople->id)) {
            $People->id = $DtoPeople->id;
        }

        if (isset($DtoPeople->name)) {
            $People->name = $DtoPeople->name;
        }

        if (isset($DtoPeople->surname)) {
            $People->surname = $DtoPeople->surname;
        }
        if (isset($DtoPeople->email)) {
            $People->email = $DtoPeople->email;
        }
        if (isset($DtoPeople->phone_number)) {
            $People->phone_number = $DtoPeople->phone_number;
        }
        if (isset($DtoPeople->user_id)) {
            $People->user_id = $DtoPeople->user_id;
        }

      return $People;
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoPeople
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(People::find($id));
    }

    /**
     * Get select
     *
     * @param String $search
     * @param int $idUser
     * @return People
     */
    public static function getSelect(string $search)
    {
        /*$user = User::where('id', $idUser)->first();
        
        if(isset($user)){
            if($user->name = 'admin'){*/
                if ($search == "*") {
                    return People::select('id', 'name as description','surname as people_surname')->where('type_referent', 'like', 'Interno')->get();
                } else {
                    return People::where('name', 'ilike' , $search . '%')->where('type_referent', 'like', 'Interno')->select('id', 'name as description','surname as people_surname')->get();
                }
            /*}else{
                if ($search == "*") {
                   return DB::table('people')
                               ->join('user_people', 'users_id', '=', $idUser)
                               ->select('id', 'name as description')
                               ->get();
                    People::where()->select('id', 'name as description')->get();
                } else {
                    return DB::table('people')
                               ->join('user_people', 'users_id', '=', $idUser)
                               ->where('name', 'like' , $search . '%')
                               ->select('id', 'name as description')
                               ->get();
                    return People::where('name', 'like' , $search . '%')->select('id', 'name as description')->get();
                }
            }  */
       // }
    }

  /**
     * Get getSelectExternalReferent
     *
     * @param String $search
     * @param int $idUser
     * @return People
     */
    public static function getSelectExternalReferent(string $search)
    {
        /*$user = User::where('id', $idUser)->first();
        
        if(isset($user)){
            if($user->name = 'admin'){*/
                if ($search == "*") {
                    return People::select('id', 'name as description')->where('type_referent', 'like', 'Esterno')->get();
                } else {
                    return People::where('name', 'ilike' , $search . '%')->where('type_referent', 'like', 'Esterno')->select('id', 'name as description')->get();
                }
    }



    /**
     * Get all
     * 
     * @return DtoPeople
     */
    public static function getAll()
    {
        $result = collect();

        foreach (People::orderBy('id','DESC')->get() as $People) {    
            $PeopleId = PeopleUser::where('people_id', $People->id)->first();

                $DtoPeople = new DtoPeople();
            
                if (isset($People->id)) {
                    $DtoPeople->id = $People->id;
                }
                
                if (isset($People->name)) {
                    $DtoPeople->name = $People->name;
                }
                if (isset($People->surname)) {
                    $DtoPeople->surname = $People->surname;
                }

                if (isset($People->email)) {
                    $DtoPeople->email = $People->email;
                }

                if (isset($People->phone_number)) {
                    $DtoPeople->phone_number = $People->phone_number;
                }
             
                
                if (isset($PeopleId->user_id)) {
                $DtoPeople->user_id = $PeopleId->user_id;
                }

                if (isset($People->name)) {
                    $DtoPeople->name = $People->name;
                }

                if (isset($People->surname)) {
                    $DtoPeople->surname = $People->surname;
                }

                if (isset($People->email)) {
                    $DtoPeople->email = $People->email;
                }

                if (isset($People->phone_number)) {
                    $DtoPeople->phone_number = $People->phone_number;
                }
              
                    $result->push($DtoPeople);
                }

                return $result;

                }

    #endregion GET



    #region INSERT
    /**
     * Insert
     * 
     * @param Request DtoPeople
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertNoAuth(Request $DtoPeople, int $idUser)
    {
        DB::beginTransaction();
        try {
            $People = new People();
            if (isset($DtoPeople->name)) {
                $People->name = $DtoPeople->name;
            } 
            if (isset($DtoPeople->surname)) {
                $People->surname = $DtoPeople->surname;
            } 
            if (isset($DtoPeople->email)) {
                $People->email = $DtoPeople->email;
            } 
            if (isset($DtoPeople->phone_number)) {
                $People->phone_number = $DtoPeople->phone_number;
            }

            if ($DtoPeople->ref == 'Esterno'){
                $People->type_referent = 'Esterno';
            }

            if ($DtoPeople->ref == 'Interno'){
                $People->type_referent = 'Interno';
            }
            $People->save();

            $PeopleUser = new PeopleUser();
            $PeopleUser->user_id = $DtoPeople->user_id;        
            if (isset($People->id)) {
                $PeopleUser->people_id = $People->id;
            }
            if (isset($People->$idUser)) {
                $PeopleUser->created_id = $People->$idUser;
            } 
            $PeopleUser->save();
            
            $RolesPeople = new RolesPeople(); 
            $RolesPeople->people_id = $People->id;
            $RolesPeople->roles_id = $DtoPeople->roles_id;
         if (isset($People->$idUser)) {
            $RolesPeople ->created_id = $People->$idUser;
            } 
         if (isset($People->$idUser)) {
             $RolesPeople ->created_at = $People->$idUser;
         } 
             $RolesPeople->save();

            $user = new User();
            $user->name = $DtoPeople->email;
            $user->password = bcrypt($DtoPeople->password);
            $user->email = $DtoPeople->email;
            $user->language_id = $DtoPeople->idLanguage;
            $user->online = 'true';
            $user->confirmed = 'true';
            $user->save();

             $UsersData = new UsersDatas();
             $UsersData->user_id = $user->id;
             $UsersData->language_id = $DtoPeople->idLanguage;
            if (isset($DtoPeople->name)) {    
                $UsersData->name = $DtoPeople->name;
            }else{
              $UsersData->name = '-';
            }  
            if (isset($DtoPeople->surname)) {
                $UsersData->surname = $DtoPeople->surname;
            }else{
              $UsersData->surname = '-';
            } 
            if (isset($DtoPeople->phone_number)) {
                $UsersData->telephone_number = $DtoPeople->phone_number;
            } 
            $UsersData->save(); 

            $userRole = new RoleUser();
            $userRole->role_id = $DtoPeople->roles_id;
            $userRole->user_id = $user->id;
            $userRole->user_type = 'App\User';
            $userRole->save();
            
            $CorrelationPeopleUser = new CorrelationPeopleUser();
            $CorrelationPeopleUser->id_user = $user->id;
            $CorrelationPeopleUser->id_people = $People->id;  
            $CorrelationPeopleUser->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $People->id;
    }


    #region insertReferentWeb
    /**
     * Insert
     * 
     * @param Request DtoPeople
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertReferentWeb(Request $DtoPeople, int $idUser)
    {
        DB::beginTransaction();

        try {
            $People = new People();
            
        if (isset($DtoPeople->name)) {
                $People->name = $DtoPeople->name;
            }  

        if (isset($DtoPeople->surname)) {
                $People->surname = $DtoPeople->surname;
            } 

        if (isset($DtoPeople->email)) {
                $People->email = $DtoPeople->email;
            } 

            if (isset($DtoPeople->phone_number)) {
                $People->phone_number = $DtoPeople->phone_number;
            } 
             
            if (isset($idUser)) {
                $People->created_id = $idUser;
            }  
            if ($DtoPeople->ref == 'Esterno'){
                $People->type_referent = 'Esterno';
            }

            if ($DtoPeople->ref == 'Interno'){
                $People->type_referent = 'Interno';
            }

            $People->save();

            $PeopleUser = new PeopleUser();
            $PeopleUser->user_id = $DtoPeople->user_id;        
            if (isset($People->id)) {
                $PeopleUser->people_id = $People->id;
            }
            if (isset($idUser)) {
                $PeopleUser->created_id = $idUser;
            } 
            $PeopleUser->save();
 
             $RolesPeople = new RolesPeople();   
              if (isset($People->id)) {   
             $RolesPeople->people_id = $People->id;
            }
            if (isset($DtoPeople->roles_id)) {   
             $RolesPeople->roles_id = $DtoPeople->roles_id;
            }        
            if (isset($idUser)) {
            $RolesPeople ->created_id = $idUser;
            } 
            if (isset($idUser)) {
             $RolesPeople ->created_at = $idUser;
            } 
            $RolesPeople->save();

            $user = new User();
            $user->name = $DtoPeople->email;
            $user->password = bcrypt($DtoPeople->password);
            $user->email = $DtoPeople->email;
            $user->language_id = $DtoPeople->idLanguage;
            $user->online = 'true';
            $user->confirmed = 'true';
            $user->save();

             $UsersData = new UsersDatas();
             $UsersData->user_id = $user->id;
             $UsersData->language_id = $DtoPeople->idLanguage;
            if (isset($DtoPeople->name)) {    
                $UsersData->name = $DtoPeople->name;
            }else{
              $UsersData->name = '-';
            }  
            if (isset($DtoPeople->surname)) {
                $UsersData->surname = $DtoPeople->surname;
            }else{
              $UsersData->surname = '-';
            } 
            if (isset($DtoPeople->phone_number)) {
                $UsersData->telephone_number = $DtoPeople->phone_number;
            } 
            $UsersData->save(); 

            $userRole = new RoleUser();
            $userRole->role_id = $DtoPeople->roles_id;
            $userRole->user_id = $user->id;
            $userRole->user_type = 'App\User';
            $userRole->save();
            
            $CorrelationPeopleUser = new CorrelationPeopleUser();
            $CorrelationPeopleUser->id_user = $user->id;
            $CorrelationPeopleUser->id_people = $People->id;  
            $CorrelationPeopleUser->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $People->id;
    }




    #region INSERT
    /**
     * Insert
     * 
     * @param Request DtoPeople
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoPeople, int $idUser)
    {

       DB::beginTransaction();

        try {
            $People = new People();
            
        if (isset($DtoPeople->name)) {
                $People->name = $DtoPeople->name;
            } 
            
        if (isset($DtoPeople->surname)) {
                $People->surname = $DtoPeople->surname;
            } 
            
        if (isset($DtoPeople->email)) {
                $People->email = $DtoPeople->email;
            } 
            
            if (isset($DtoPeople->phone_number)) {
                $People->phone_number = $DtoPeople->phone_number;
            } 
             
            if (isset($DtoPeople->idUser)) {
                $People->created_id = $DtoPeople->idUser;
            }  

            if (isset($DtoPeople->name_referent)) {
                $People->name = $DtoPeople->name_referent;
            } 
            
            if (isset($DtoPeople->surname_referent)) {
                $People->surname = $DtoPeople->surname_referent;
            } 
            
            if (isset($DtoPeople->email_referent)) {
                $People->email = $DtoPeople->email_referent;
            } 
            
            if (isset($DtoPeople->phone_number_referent)) {
                $People->phone_number = $DtoPeople->phone_number_referent;
            } 
            if (isset($DtoPeople->online)) {
                $People->online = $DtoPeople->online;
            }
            if (isset($DtoPeople->confirmed_referent)) {
                $People->confirmed = $DtoPeople->confirmed_referent;
            }
            //$People->type_referent='Esterno';
            $People->save();

            $PeopleUser = new PeopleUser();
            $PeopleUser->user_id = $DtoPeople->user_id;        
            if (isset($People->id)) {
                $PeopleUser->people_id = $People->id;
            }
            if (isset($People->$idUser)) {
                $PeopleUser->created_id = $People->$idUser;
            } 
            $PeopleUser->save();
 
             $RolesPeople = new RolesPeople();       
             $RolesPeople->people_id = $People->id;
             $RolesPeople->roles_id = $DtoPeople->roles_id;

            if (isset($People->$idUser)) {
            $RolesPeople ->created_id = $People->$idUser;
            } 
            if (isset($People->$idUser)) {
             $RolesPeople ->created_at = $People->$idUser;
            } 
            $RolesPeople->save();

                    $user = new User();
                    if (isset($DtoPeople->username_referent)) {
                        $user->name = $DtoPeople->username_referent;
                    }
                    if (isset($DtoPeople->password_referent)) {
                        $user->password = bcrypt($DtoPeople->password_referent);
                    }

                    if (isset($DtoPeople->email_referent)) {
                        $user->email = $DtoPeople->email_referent;
                    }
                    if (isset($DtoPeople->idLanguage)) {
                        $user->language_id = $DtoPeople->idLanguage;
                    }
                    if (isset($DtoPeople->online)) {
                        $user->online = $DtoPeople->online;
                    }
                    if (isset($DtoPeople->confirmed_referent)) {
                        $user->confirmed = $DtoPeople->confirmed_referent;
                    }
                    $user->save();

                    if (isset($user->id)) {
                         $CorrelationPeopleUser = new CorrelationPeopleUser();
                         $CorrelationPeopleUser->id_user = $user->id;
                         $CorrelationPeopleUser->id_people = $People->id;
                    }else{
                        $CorrelationPeopleUser->id_user = null;
                        $CorrelationPeopleUser->id_people = null;
                    }

                    $CorrelationPeopleUser->save();

           
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $People->id;
    }

    #endregion INSERT

    #region  updateReferent

/**
     * Update
     * 
     * @param Request DtoPeople
     * @param int idLanguage
     */
    public static function updateReferent(Request $DtoPeople)
    {
        $PeopleOndb= People::find($DtoPeople->id);
        $PeopleUserOnDb=PeopleUser::where('people_id', $PeopleOndb->id)->first();
        $RolePeople=RolesPeople::where('people_id',  $PeopleOndb->id)->first();

        DB::beginTransaction();
   
        try {

            $PeopleOndb->id = $DtoPeople->id;
            $PeopleOndb->name = $DtoPeople->name;
            $PeopleOndb->surname = $DtoPeople->surname;
            $PeopleOndb->email = $DtoPeople->email;
            $PeopleOndb->phone_number = $DtoPeople->phone_number;
            //$PeopleOndb->type_referent='Interno';
            $PeopleOndb->save();
            
            $RolePeople->roles_id = $DtoPeople->roles_id;
            $RolePeople->people_id = $DtoPeople->id;
            $RolePeople->save();
                   
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }


     #endregion UPDATE

/**
     * Update
     * 
     * @param Request DtoPeople
     * @param int idLanguage
     */
    public static function updateReferentWeb(Request $DtoPeople)
    {
        
        $PeopleOndb= People::find($DtoPeople->id);
        $PeopleUserOnDb=PeopleUser::where('people_id', $DtoPeople->id)->first();
        $RolePeople=RolesPeople::where('people_id',  $DtoPeople->id)->first();

        DB::beginTransaction();
   
        try {
            $PeopleOndb->id = $DtoPeople->id;
            $PeopleOndb->name = $DtoPeople->name;
            $PeopleOndb->surname = $DtoPeople->surname;
            $PeopleOndb->email = $DtoPeople->email;
            $PeopleOndb->phone_number = $DtoPeople->phone_number;
            $PeopleOndb->type_referent='Esterno';
            $PeopleOndb->save();      
            $RolePeople->roles_id = $DtoPeople->roles_id;
            $RolePeople->people_id = $DtoPeople->id;
            $RolePeople->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }




 
    /**
     * Update
     * 
     * @param Request DtoPeople
     * @param int idLanguage
     */
    public static function update(Request $DtoPeople, int $idUser)
    {

        $PeopleOnDb = People::find($DtoPeople->id);
        $PeopleUserOnDb = PeopleUser::where('people_id', $PeopleOnDb->id)->first();
         
        DB::beginTransaction();
        try {
            if (isset($DtoPeople->id)) {
                $PeopleOnDb->id = $DtoPeople->id;
            } 
            if (isset($DtoPeople->name)) {
                $PeopleOnDb->name = $DtoPeople->name;
            } 
            if (isset($DtoPeople->surname)) {
                $PeopleOnDb->surname = $DtoPeople->surname;
            } 
            if (isset($DtoPeople->email)) {
                $PeopleOnDb->email = $DtoPeople->email;
            } 
            if (isset($DtoPeople->phone_number)) {
                $PeopleOnDb->phone_number = $DtoPeople->phone_number;
            }
            if (isset($DtoPeople->$idUser)) {
                $PeopleOnDb->created_id = $DtoPeople->$idUser;
            }
            if (isset($DtoPeople->name_referent)) {
                $PeopleOnDb->name = $DtoPeople->name_referent;
            } 
            if (isset($DtoPeople->surname_referent)) {
                $PeopleOnDb->surname = $DtoPeople->surname_referent;
            } 
            if (isset($DtoPeople->email_referent)) {
                $PeopleOnDb->email = $DtoPeople->email_referent;
            } 
            if (isset($DtoPeople->phone_number_referent)) {
                $PeopleOnDb->phone_number = $DtoPeople->phone_number_referent;
            }
            if (isset($DtoPeople->online)) {
                $PeopleOnDb->online = $DtoPeople->online;
            }
            if (isset($DtoPeople->confirmed_referent)) {
                $PeopleOnDb->confirmed = $DtoPeople->confirmed_referent;
            }
            $PeopleOnDb->save();

            if (isset($DtoPeople->user_id)) {
                $PeopleUserOnDb->user_id = $DtoPeople->user_id;
            }
            if (isset($DtoPeople->id)) {
                $PeopleUserOnDb->people_id = $DtoPeople->id;
            }
            if (isset($DtoPeople->$idUser)) {
                $PeopleUserOnDb->created_id = $DtoPeople->$idUser;
            }
            $PeopleUserOnDb->save();

            $CorrelationPeopleUser = CorrelationPeopleUser::where('id_people', $DtoPeople->id)->first();
            if (!isset($CorrelationPeopleUser->id)) {
                    $user = new User();
                    $user->name = $DtoPeople->username_referent;
                    $user->password = bcrypt($DtoPeople->password_referent);
                    $user->email = $DtoPeople->email_referent;
                    $user->language_id = $DtoPeople->idLanguage;
                    $user->online = $DtoPeople->online;
                    $user->confirmed = $DtoPeople->confirmed_referent;
                    $user->save();

                    $CorrelationPeopleUser = new CorrelationPeopleUser();
                    $CorrelationPeopleUser->id_people = $DtoPeople->id;
                    $CorrelationPeopleUser->id_user = $user->id;
                    $CorrelationPeopleUser->save();
            }else{
                if (isset($CorrelationPeopleUser->id)) {
                    $UserOnDb = User::where('id', $CorrelationPeopleUser->id_user)->first();
                    $UserOnDb->name = $DtoPeople->username_referent;
                    $UserOnDb->password = bcrypt($DtoPeople->password_referent);
                    $UserOnDb->email = $DtoPeople->email_referent;
                    $UserOnDb->language_id = $DtoPeople->idLanguage;
                    $UserOnDb->online = $DtoPeople->online;
                    $UserOnDb->confirmed = $DtoPeople->confirmed_referent;
                    $UserOnDb->save();
        
                    $CorrelationPeopleUser->id_people = $DtoPeople->id;
                    $CorrelationPeopleUser->id_user = $UserOnDb->id;
                    $CorrelationPeopleUser->save();
                }      
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return  $DtoPeople->id;

    }
     #endregion UPDATE

        /**
             * Clone
             * 
             * @param int id
             * @param int idFunctionality
             * @param int idLanguage
             * @param int idUser
             * 
             * @return int
             */
            public static function clone(int $id, int $idFunctionality, int $idUser, int $idLanguage)
            {
                FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

                $People = People::find($id);
                $PeopleUser = PeopleUser::where('people_id',$id)->first();
                
                DB::beginTransaction();

                try {

                    $newPeople = new People();
                    $newPeople->name = $People->name . ' - Copy';
                    $newPeople->surname = $People->surname . ' - Copy';
                    $newPeople->email = $People->email ;
                    $newPeople->phone_number = $People->phone_number;
                    $newPeople->created_id = $People->$idUser; 
                    $newPeople->save();


                    $newPeopleUser = new PeopleUser();
                    $newPeopleUser->people_id = $newPeople->id;
                    $newPeopleUser->user_id = $PeopleUser->user_id;
                    $newPeopleUser->created_id = $PeopleUser->$idUser;
                    $newPeopleUser->save();

                    DB::commit();

                    return $newPeople->id;
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
            }

        #endregion CLONE

            #region DELETE
                    /**
                     * Delete
                     * 
                     * @param int id
                     * @param int idLanguage
                     */
                    public static function delete(int $id, int $idLanguage)
                    {
                        $People = People::find($id);
                        $PeopleUser = PeopleUser::where('people_id', $People->id);
                        $RolesPeople =RolesPeople::where('people_id', $People->id);
                        $CorrelationPeopleUser = CorrelationPeopleUser::where('id_people', $People->id);

                        if (is_null($People)) {
                            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PeopleRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
                        }
                        DB::beginTransaction();

                        try {

                            $RolesPeople->delete();
                            $PeopleUser->delete();
                            $CorrelationPeopleUser->delete();
                            $People->delete();

                            
                            DB::commit();

                        
                        } catch (Exception $ex) {
                            DB::rollback();
                            throw $ex;
                        }
                    }
                
                }
        #endregion DELETE
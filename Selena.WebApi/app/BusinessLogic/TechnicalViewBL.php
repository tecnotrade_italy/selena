<?php

namespace App\BusinessLogic;

use App\CorrelationPeopleUser;
use App\DtoModel\DtoInterventions;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\GrQuotes;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use App\Helpers\HttpHelper;
use App\People;
use App\Setting;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

class TechnicalViewBL
{
    
/**
     * getAllProcessingTechnical
     * 
     * @return DtoInterventionProcessing
     * @return DtoIntervention
     */
    public static function getAllProcessingTechnical($idUser)
    {
        $CorrelationPeopleUser= CorrelationPeopleUser::where('id_user', $idUser)->first();
        $result = collect();
            foreach (DB::select(
            "select distinct c.id, c.to_call, c.escape_from, c.search_product,c.working,c.user_id,
            c.referent, c.date_ddt_gr, c.nr_ddt_gr, c.defect,c.ready, c.imgmodule,c.working_without_testing,
            c.working_with_testing
            FROM gr_intervention_processing b
            INNER JOIN gr_interventions as c on c.id = b.id_intervention
            where c.id not in (select id_intervention from gr_quotes where id_intervention=c.id)
            and c.referent='$CorrelationPeopleUser->id_people'
            limit 100"

            ) as $InterventionAll) {    

            $CustomerDescription = User::where('id', $InterventionAll->user_id)->first();
            //$InterventionProcessing = GrInterventionProcessing::where('id_intervention', $InterventionAll->id)->first();

            $DtoInterventions = new DtoInterventions();

            if (isset($InterventionAll->id)) {
                $DtoInterventions->idIntervention = $InterventionAll->id;
                $DtoInterventions->id = $InterventionAll->id;
            }

            if (isset($InterventionAll->to_call)) {
                $DtoInterventions->to_call = $InterventionAll->to_call;
            }
            if (isset($InterventionAll->escape_from)) {
                $DtoInterventions->escape_from = $InterventionAll->escape_from;
            }
            if (isset($InterventionAll->search_product)) {
                $DtoInterventions->search_product = $InterventionAll->search_product;
            }

            if (isset($InterventionAll->working)) {
                $DtoInterventions->working = $InterventionAll->working;
            }

            if (isset($CustomerDescription->name)) {
                $DtoInterventions->descriptionCustomer =  $CustomerDescription->name;
            } else {
                $DtoInterventions->descriptionCustomer = '';
            }
             if (isset($InterventionAll->nr_ddt_gr)) {
                $DtoInterventions->nr_ddt_gr =  $InterventionAll->nr_ddt_gr;
            } else {
                $DtoInterventions->nr_ddt_gr = '';
            }
            if (isset($InterventionAll->date_ddt_gr)) {
                $DtoInterventions->date_ddt_gr =  $InterventionAll->date_ddt_gr;
            } else {
                $DtoInterventions->date_ddt_gr = '';
            }

            if (isset($InterventionAll->defect)) {
                $DtoInterventions->defect = $InterventionAll->defect;
            } else {
                $DtoInterventions->defect = '';
            }
             if (isset($InterventionAll->ready)) {
                 $DtoInterventions->ready = $InterventionAll->ready;
                }else {
                $DtoInterventions->ready = false;
            }
            if (isset($InterventionAll->working_without_testing)) {
                 $DtoInterventions->working_without_testing = $InterventionAll->working_without_testing;
                }else {
                $DtoInterventions->working_without_testing = false;
            }
            if (isset($InterventionAll->working_with_testing)) {
                 $DtoInterventions->working_with_testing = $InterventionAll->working_with_testing;
                }else {
                $DtoInterventions->working_with_testing = false;
            }

            if (isset($InterventionAll->imgmodule)) {
                $DtoInterventions->imgmodule = $InterventionAll->imgmodule;
            } else {
                $DtoInterventions->imgmodule = '';
            }
            if (isset($InterventionAll->referent)) {
                $DtoInterventions->referent =
                    People::where('id', $InterventionAll->referent)->first()->name;
            } else {
                $DtoInterventions->referent = '';
            }
            $result->push($DtoInterventions);
        }
        return $result;
    }



    /**
     * getAllProcessingView
     * 
     * @return DtoInterventionProcessing
     * @return DtoIntervention
     */
    public static function getAllQuotesTechnical($idUser)
    {
        $result = collect();
          $CorrelationPeopleUser= CorrelationPeopleUser::where('id_user', $idUser)->first(); 
         foreach (DB::select(
            "SELECT i.id, i.imgmodule, i.user_id, i.description,c.id_states_quote,
            i.nr_ddt_gr,i.date_ddt_gr, i.ready, i.referent
            FROM gr_interventions as i
            inner join gr_quotes as c on c.id_intervention = i.id
            WHERE (i.date_ddt_gr  = ''
            OR i.date_ddt_gr is null)
            AND (i.nr_ddt_gr is null
            OR i.nr_ddt_gr  = '')
            AND i.referent='$CorrelationPeopleUser->id_people'
            AND c.id_states_quote <>1
            limit 100" 
        ) as $InterventionAll) {
            $CustomerDescription = User::where('id', $InterventionAll->user_id)->first();
            $InterventionQuotes = GrQuotes::where('id_intervention', $InterventionAll->id)->first();

            $DtoInterventions = new DtoInterventions();

            if (isset($InterventionAll->imgmodule)) {
                $DtoInterventions->imgmodule = $InterventionAll->imgmodule;
            } else {
                $DtoInterventions->imgmodule = '';
            }

              if (isset($InterventionAll->ready)) {
                 $DtoInterventions->ready = $InterventionAll->ready;
                }else {
                $DtoInterventions->ready = false;
            }
            if (isset($InterventionAll->id)) {
                $DtoInterventions->idIntervention = $InterventionAll->id;
                $DtoInterventions->id = $InterventionAll->id;
            }
             if (isset($InterventionQuotes->id_states_quote)) {
                $DtoInterventions->id_states_quote = $InterventionQuotes->id_states_quote;
            }
            if (isset($InterventionQuotes->updated_at)) {
                $DtoInterventions->updated_at = date_format($InterventionQuotes->updated_at, 'd/m/Y');
            }
            
            if (isset($CustomerDescription->name)) {
                $DtoInterventions->descriptionCustomer = $CustomerDescription->name;
            } else {
                $DtoInterventions->descriptionCustomer = '';
            }

            if (isset($InterventionAll->description)) {
                $DtoInterventions->description = $InterventionAll->description;
            }else {
                $DtoInterventions->description = '';
            }
            if (isset($InterventionAll->date_ddt_gr)) {
                $DtoInterventions->date_ddt_gr = $InterventionAll->date_ddt_gr;
            }else {
                $DtoInterventions->date_ddt_gr = '';
            }
            if (isset($InterventionAll->nr_ddt_gr)) {
                $DtoInterventions->nr_ddt_gr = $InterventionAll->nr_ddt_gr;
            }else {
                $DtoInterventions->nr_ddt_gr = '';
            }
            if (isset($InterventionAll->working)) {
                $DtoInterventions->working = $InterventionAll->working;
            }

            $result->push($DtoInterventions);
        }
        return $result;
    }



}



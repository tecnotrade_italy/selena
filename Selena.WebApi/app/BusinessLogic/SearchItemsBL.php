<?php

namespace App\BusinessLogic;

use App\BusinessName;
use App\Tipology;
use App\Brand;
use App\Content;
use App\ContentLanguage;
use App\DtoModel\DtoNation;
use App\DtoModel\DtoSelenaViews;
use App\DtoModel\DtoTagTemplate;
use App\DtoModel\DtoItemGr;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\ItemGr;
use App\SelenaSqlViews;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchItemsBL
{

  /**
     * Search implemented
     * 
     * @param Request request
     * 
     */
    public static function searchImplemented(request $request){
        $result = collect();

        $content = '';
        $finalHtml = '';
        $strWhere = "";
        $idLanguageContent = $request->idLanguage;
        
        foreach($request->request as $key => $value){
            if($request->{$key} != ""){
                if($key != 'idLanguage'){
                    $operator = ' = ';
                    $val = $request->{$key};
                    $keyNew = $key;
                        if($key == 'tipologyes'){
                                $keyNew = 'gr_items.tipology';
                                $operator = ' = ';
                                $val = $request->{$key};
                        }else{
                            if($key == 'note'){
                                    $keyNew = 'gr_items.note';
                                    $operator = ' ilike ';
                                    $val = '\'%' . $request->{$key} . '%\'';
                            }else{
                            if($key == 'original_code'){
                                    $keyNew = 'gr_items.original_code';
                                    $operator = ' ilike ';
                                    $val = '\'%' . $request->{$key} . '%\'';
                                }else{
                                    if($key == 'code_product'){
                                        $keyNew = 'gr_items.code_product';
                                        $operator = ' ilike ';
                                        $val = '\'%' . $request->{$key} . '%\'';
                                    }else{
                                        if($key == 'code_gr'){
                                            $keyNew = 'gr_items.code_gr';
                                            $operator = ' ilike ';
                                            $val = '\'' . $request->{$key} . '\'';
                                        }else{
                                            if($key == 'update_date'){
                                                $keyNew = 'gr_items.update_date';
                                                $operator = ' ilike ';
                                                $val = '\'%' . $request->{$key} . '%\'';
                                            }else{
                                                if($key == 'usual_supplier'){
                                                    $keyNew = 'gr_items.usual_supplier';
                                                    $operator = ' ilike ';
                                                    $val = '\'%' . $request->{$key} . '%\'';
                                                }else{
                                                    if($key == 'brands'){
                                                        $keyNew = 'gr_items.brand';
                                                        $operator = ' = ';
                                                        $val = $request->{$key};
                                                    }else{
                                                        if($key == 'business_name_suppliers'){
                                                            $keyNew = 'gr_items.business_name_supplier';
                                                            $operator = ' = ';
                                                            $val = $request->{$key};
                                                        }else{
                                                            if($key == 'code_supplier'){
                                                                $keyNew = 'gr_items.code_supplier';
                                                                $operator = ' ilike ';
                                                                $val = '\'%' . $request->{$key} . '%\'';
                                                            }else{
                                                                if($key == 'price_purchase'){
                                                                    $keyNew = 'gr_items.price_purchase';
                                                                    $operator = ' = ';
                                                                    $val = $request->{$key};
                                                                }else{
                                                                    if($key == 'price_list'){
                                                                        $keyNew = 'gr_items.price_list';
                                                                        $operator = ' = ';
                                                                        $val = $request->{$key};
                                                                    }else{
                                                                        if($key == 'repair_price'){
                                                                            $keyNew = 'gr_items.repair_price';
                                                                            $operator = ' = ';
                                                                            $val = $request->{$key};                                                                                                                                     
                                                                    }else{
                                                                        if($key == 'plant'){
                                                                            $keyNew = 'gr_items.plant';
                                                                            $operator = ' ilike ';
                                                                            $val = '\'%' . $request->{$key} . '%\'';
                                                                    }else{
                                                                        $operator = ' = ';
                                                                        $val = $request->{$key};
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }       
                if($request->{$key} == "on"){
                        $keyNew = $key;
                        $val = "true";
                }
                        $strWhere = $strWhere . ' AND ' . $keyNew . $operator . $val;
            }
        }
     }
            foreach (DB::select(
                'SELECT gr_items.*
                FROM gr_items
                WHERE gr_items.language_id = :idLanguage ' . $strWhere,
                ['idLanguage' => $idLanguageContent],
            ) as $itemGr) {

                $Brand = Brand::where('id', $itemGr->brand)->first();
                $BusinessName = BusinessName::where('id', $itemGr->business_name_supplier)->first();
                $Tipology = Tipology::where('id', $itemGr->tipology)->first();

            $DtoItemGr = new DtoItemGr();

            if (isset($itemGr->id)) {
                $DtoItemGr->id = $itemGr->id;
            }else{
                $DtoItemGr->id = '';
            } 
            if (isset($Tipology->tipology)) {
                $DtoItemGr->tipology =  $Tipology->tipology;
            }else{
                $DtoItemGr->tipology = '';
            }

            if (isset($itemGr->note)) {
                $DtoItemGr->note = $itemGr->note;
            }else{
                $DtoItemGr->note = '';
            } 

            if (isset($itemGr->img)) {
                $DtoItemGr->img = $itemGr->img;
            }else{
                $DtoItemGr->img = '';
            } 
             if (isset($itemGr->imageitems)) {
                $DtoItemGr->imageitems = $itemGr->imageitems;
            }else{
                $DtoItemGr->imageitems = '';
            } 
            if (isset($itemGr->original_code)) {
                $DtoItemGr->original_code = $itemGr->original_code;
            }else{
                $DtoItemGr->original_code = '';
            } 
            if (isset($itemGr->code_product)) {
                $DtoItemGr->code_product = $itemGr->code_product;
            }else{
                $DtoItemGr->code_product = '';
            } 
            if (isset($itemGr->code_gr)) {
                $DtoItemGr->code_gr = $itemGr->code_gr;
            }else{
                $DtoItemGr->code_gr = '';
            } 
            if (isset($itemGr->plant)) {
                $DtoItemGr->plant = $itemGr->plant;
            }else{
                $DtoItemGr->plant = '';
            } 
            if (isset($Brand->brand)) {
                $DtoItemGr->brand = $Brand->brand;
            }else{
                $DtoItemGr->brand = '';
            } 
            if (isset($itemGr->code_supplier)) {
                $DtoItemGr->code_supplier = $itemGr->code_supplier;
            }else{
                $DtoItemGr->code_supplier = '';
            } 
            if (isset($itemGr->price_purchase)) {
                $DtoItemGr->price_purchase = '€ ' . $itemGr->price_purchase;
            }else{
                $DtoItemGr->price_purchase = '';
            } 
            if (isset($itemGr->price_list)) {
                $DtoItemGr->price_list = '€ ' . $itemGr->price_list;
            }else{
                $DtoItemGr->price_list = '';
            } 

            if (isset($itemGr->repair_price)) {
                $DtoItemGr->repair_price = '€ ' . $itemGr->repair_price;
            }else{
                $DtoItemGr->repair_price = '';
            } 
  
            if (isset($itemGr->price_new)) {
                $DtoItemGr->price_new = '€ ' . $itemGr->price_new;
            }else{
                $DtoItemGr->price_new = '';
            } 
            if (isset($itemGr->update_date)) {
                $DtoItemGr->update_date = str_replace('00:00:00', '', $itemGr->update_date);
            }else{
                $DtoItemGr->update_date = '';
            } 
            if (isset($itemGr->usual_supplier)) {
                $DtoItemGr->usual_supplier = $itemGr->usual_supplier;
            }else{
                $DtoItemGr->usual_supplier = '';
            } 
            if (isset($itemGr->pr_rip_conc)) {
                $DtoItemGr->pr_rip_conc = $itemGr->pr_rip_conc;
            }else{
                $DtoItemGr->pr_rip_conc = '';
            } 
            if (isset($BusinessName)) {
                $DtoItemGr->business_name_supplier = $BusinessName->business_name;
            }else{
                $DtoItemGr->business_name_supplier = '';
            } 

      $result->push($DtoItemGr);
    }
    return $result;
  }
}


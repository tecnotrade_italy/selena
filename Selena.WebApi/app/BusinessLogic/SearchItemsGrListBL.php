<?php

namespace App\BusinessLogic;

use App\BusinessName;
use App\Tipology;
use App\Brand;
use App\Content;
use App\ContentLanguage;
use App\DtoModel\DtoNation;
use App\DtoModel\DtoSelenaViews;
use App\DtoModel\DtoTagTemplate;
use App\DtoModel\DtoItemGr;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\ItemGr;
use App\SelenaSqlViews;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchItemsGrListBL
{

  /**
     * Search implemented
     * 
     * @param Request request
     * 
     */
    public static function searchImplementedItemsGr(request $request){
        $result = collect();
        $strWhere = "";
        $idLanguageContent = $request->idLanguage;
         
        $TipologySearch = $request->tipology;
        if ($TipologySearch != "") {
           // $strWhere = $strWhere . ' AND gr_items.tipology ilike \'%' . $TipologySearch . '%\'';
            $strWhere = $strWhere . ' AND gr_items.tipology = ' . $TipologySearch . '';
        }
        $BrandSearch = $request->brand;
        if ($BrandSearch != "") {
           // $strWhere = $strWhere . ' AND gr_items.brand ilike \'%' . $BrandSearch . '%\'';
            $strWhere = $strWhere . ' AND gr_items.brand = ' . $BrandSearch . '';
        }   
        $CodeGrSearch = $request->code_gr;
        if ($CodeGrSearch != "") {
         $strWhere = $strWhere . ' AND (gr_items.code_gr ilike \'%' . $CodeGrSearch . '%\'
                                   OR gr_items.code_product ilike \'%' . $CodeGrSearch . '%\'
                                   OR gr_items.original_code ilike \'%' . $CodeGrSearch . '%\')';
        }

         $usual_supplier = "SI";
        // ---- end search for brand and tipology ---- //
            foreach (DB::select(
                'SELECT gr_items.*
                FROM gr_items
                WHERE gr_items.language_id = :idLanguage
                AND gr_items.usual_supplier like \'%' . $usual_supplier . '%\'' . $strWhere,
                ['idLanguage' => $idLanguageContent],
            ) as $itemGr) {

          // if ($request->brand == $itemGr->brand || $request->tipology == $itemGr->tipology) {
                $Brand = Brand::where('id', $itemGr->brand)->first();
                $BusinessName = BusinessName::where('id', $itemGr->business_name_supplier)->first();
                $Tipology = Tipology::where('id', $itemGr->tipology)->first();

            $DtoItemGr = new DtoItemGr();

            if (isset($itemGr->id)) {
                $DtoItemGr->id = $itemGr->id;
            }else{
                $DtoItemGr->id = '-';
            } 
            if (isset($Tipology->tipology)) {
                $DtoItemGr->tipology =  $Tipology->tipology;
            }else{
                $DtoItemGr->tipology = '-';
            }
            if (isset($itemGr->img)) {
                $DtoItemGr->img = $itemGr->img;
            }else{
                $DtoItemGr->img = '-';
            } 
            //if (isset($itemGr->original_code)) {
            //    $DtoItemGr->original_code = $itemGr->original_code;
           // }else{
             //   $DtoItemGr->original_code = '-';
           // } 
            //if (isset($itemGr->code_product)) {
              //  $DtoItemGr->code_product = $itemGr->code_product;
          //  }else{
              //  $DtoItemGr->code_product = '-';
          //  } 
            //if (isset($itemGr->code_gr)) {
             //   $DtoItemGr->code_gr = $itemGr->code_gr;
            //}else{
            //    $DtoItemGr->code_gr = '-';
           // } 
            if (isset($itemGr->plant)) {
                $DtoItemGr->plant = $itemGr->plant;
            }else{
                $DtoItemGr->plant = '-';
            } 
            if (isset($Brand->brand)) {
                $DtoItemGr->brand = $Brand->brand;
            }else{
                $DtoItemGr->brand = '-';
            } 
             if (isset($itemGr->imageitems)) {
                $DtoItemGr->imageitems = $itemGr->imageitems;
            }else{
                $DtoItemGr->imageitems = '-';
            }

            if (isset($request->code_gr)) {
                if (strpos($itemGr->code_product, $request->code_gr) !== false) {
                    $DtoItemGr->code_product = $itemGr->code_product;
                    $DtoItemGr->original_code = '-';
                    $DtoItemGr->code_gr = $itemGr->code_gr;
                }else{
                    if (strpos($itemGr->original_code, $request->code_gr) !== false) {
                        $DtoItemGr->code_product = '-';
                        $DtoItemGr->original_code = $itemGr->original_code;
                        $DtoItemGr->code_gr = $itemGr->code_gr;
                    }else{
                        if (strpos($itemGr->code_gr, $request->code_gr) !== false) {
                            $DtoItemGr->code_product = '-';
                            $DtoItemGr->original_code = '-';
                            $DtoItemGr->code_gr = $itemGr->code_gr;
                        }else{
                            $DtoItemGr->code_product = '-'; 
                            $DtoItemGr->original_code = '-';
                            $DtoItemGr->code_gr = '-';
                        }
                    }
                }
            }else{
                //$DtoItemGr->code_product = $itemGr->code_product; 
                //$DtoItemGr->original_code = $itemGr->original_code;
                $DtoItemGr->code_gr = $itemGr->code_gr; 
                $DtoItemGr->original_code = '-'; 
                $DtoItemGr->code_product =  '-';   
            }
            $result->push($DtoItemGr);
        }

          return $result;
  }
     

 /**
     * Search implemented
     * 
     * @param Request request
     * 
     */
    public static function searchImplementedItemsGlobal(Request $request)
    {

        $content = '';
        $finalHtml = '';
        $strWhere = "";
        $idLanguageContent = $request->idLanguage; 
        $contentCode = Content::where('code', 'GrSearchListItems')->first();
        $contentLanguage = ContentLanguage::where('content_id', $contentCode->id)->where('language_id', $idLanguageContent)->first();
        
       
        $Request = $request->code_gr;
        if ($Request != "") {
         $strWhere = $strWhere . ' AND (gr_items.code_gr ilike \'%' . $Request . '%\'
                                   OR gr_items.code_product ilike \'%' . $Request . '%\'
                                   OR gr_items.original_code ilike \'%' . $Request . '%\')';
  }


        $tags = SelenaViewsBL::getTagByType('GrSearchListItems');
        foreach (DB::select(
            'SELECT gr_items.*
            FROM gr_items
            WHERE gr_items.language_id = :idLanguage ' . $strWhere,
            ['idLanguage' => $idLanguageContent]
        ) as $gritems){
            $newContent = $contentLanguage->content;
            foreach ($tags as $fieldsDb){      
                if(isset($fieldsDb->tag)){
                    if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                        if(is_numeric($gritems->{$cleanTag})){
                            $tag = str_replace('.',',',substr($gritems->{$cleanTag}, 0, -2));
                        }else{
                            $tag = $gritems->{$cleanTag};
                        }
                        $newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                    }
                }
            }
              if (strpos($newContent, '#tipology_description#') !== false) {
                        $DescriptionTipology = Tipology::where('id', $gritems->tipology)->first();
                        if (isset($DescriptionTipology)){
                        $newContent = str_replace('#tipology_description#', $DescriptionTipology->tipology,  $newContent); 
                        }else{
                            $newContent = str_replace('#tipology_description#', '',  $newContent);           
                        }
                    }
                      if (strpos($newContent, '#brand_description#') !== false) {
                        $DescriptionBrand = Brand::where('id', $gritems->brand)->first();
                        if (isset($DescriptionBrand)){
                        $newContent = str_replace('#brand_description#', $DescriptionBrand->brand,  $newContent); 
                        }else{
                            $newContent = str_replace('#brand_description#', '',  $newContent);           
                        }
                    }
            $finalHtml = $finalHtml . $newContent;
        };
        $content = $finalHtml;
        if($content == ""){
            $content = '<div class="col-12 div-no-info"><h1>Nessuna informazione trovata</h1></div>';
        }
        return $content;
}

    /**
     * Get Content for link DetailGrItemsSearch
     * 
     * @return DtoUser
     */
    public static function getHtml($url){
        $content = ItemGr::where('link', $url)->first();
              if (isset($content)) {

            $idContent = Content::where('code', 'DetailGrItemsSearch')->first();
            $languageContent = ContentLanguage::where('content_id', $idContent->id)->first();
            $contentHtml = ContentLanguageBL::getByCodeRender($idContent->id, $languageContent->language_id, $url);
            $dtoPageUserRender = new DtoItemGr();
            $dtoPageUserRender->html = $contentHtml;
            //$dtoPageUserRender->name = $content->name;
            $dtoPageUserRender->code_gr = $content->code_gr;
            $dtoPageUserRender->original_code = $content->original_code;
            $dtoPageUserRender->code_product = $content->code_product;
            $dtoPageUserRender->brand = $content->brand;
            $dtoPageUserRender->tipology = $content->tipology;
            $dtoPageUserRender->plant = $content->plant;
            $dtoPageUserRender->note = $content->note;

            return $dtoPageUserRender;
        } else {
            return '';
        }
    }



   /**
     * Get by id
     * 
     * @param int id
     * @return DtoItemGr
     */
    public static function getResultDetail(int $id)
    {
        return static::_convertToDto(ItemGr::find($id));
    }


 /**
     * Get select
     * Convert to dto
     * 
     * @param ItemGr
     * 
     * @return DtoItemGr
     */
    private static function _convertToDto($ItemGr)
    {
        $DtoItemGr = new ItemGr();

        if (isset($ItemGr->id)) {
            $DtoItemGr->id = $ItemGr->id;
        }
        if (isset($ItemGr->tipology)) {
            $DtoItemGr->tipology = $ItemGr->tipology;
            $DtoItemGr->Tipology = Tipology::where('id',$ItemGr->tipology)->first()->tipology;  
        }
        if (isset($ItemGr->note)) {
            $DtoItemGr->note = $ItemGr->note;
        }
        if (isset($ItemGr->img)) {
            $DtoItemGr->imgName = $ItemGr->img;
        }
        if (isset($ItemGr->original_code)) {
            $DtoItemGr->original_code = $ItemGr->original_code;
        }
        if (isset($ItemGr->code_product)) {
            $DtoItemGr->code_product = $ItemGr->code_product;
        }
        if (isset($ItemGr->code_gr)) {
            $DtoItemGr->code_gr = $ItemGr->code_gr;
        }
        if (isset($ItemGr->plant)) {
            $DtoItemGr->plant = $ItemGr->plant;
        }
         if (isset($ItemGr->imageitems)) {
            $DtoItemGr->imageitems = $ItemGr->imageitems;
        }

        if (isset($ItemGr->brand)) {
            $DtoItemGr->brand = $ItemGr->brand;
            $DtoItemGr->Brand = Brand::where('id',$ItemGr->brand)->first()->brand;  
        }
        
        return $DtoItemGr;
    }













}


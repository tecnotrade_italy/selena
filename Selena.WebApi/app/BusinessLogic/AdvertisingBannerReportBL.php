<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoAdvertisingBannerReport;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\AdvertisingBannerReport;
use Exception;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;

class AdvertisingBannerReportBL
{
    #region PRIVATE

    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request dtoAdvertisingBannerReport
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationsTypesEnum)
    {

       

    }

    /**
     * Convert to dto
     * 
     * @param AdvertisingBannerReport AdvertisingBannerReport
     * @return DtoAdvertisingBannerReport
     */
    private static function _convertToDto($AdvertisingBannerReport)
    {
        $dtoAdvertisingBannerReport = new DtoAdvertisingBannerReport();

        if (isset($AdvertisingBannerReport->id)) {
            $dtoAdvertisingBannerReport->id = $AdvertisingBannerReport->id;
        }

        if (isset($AdvertisingBannerReport->advertising_banner_id)) {
            $dtoAdvertisingBannerReport->advertising_banner_id = $AdvertisingBannerReport->advertising_banner_id;
        }

        if (isset($AdvertisingBannerReport->impression_date)) {
            $dtoAdvertisingBannerReport->impression_date = $AdvertisingBannerReport->impression_date;
        }

        if (isset($AdvertisingBannerReport->page)) {
            $dtoAdvertisingBannerReport->page = $AdvertisingBannerReport->page;
        }

        if (isset($AdvertisingBannerReport->source_ip)) {
            $dtoAdvertisingBannerReport->source_ip = $AdvertisingBannerReport->source_ip;
        }

        return $dtoAdvertisingBannerReport;
    }


    /**
     * Convert to VatType
     * 
     * @param dtoAdvertisingBannerReport dtoAdvertisingBannerReport
     * 
     * @return AdvertisingBannerReport
     */
    private static function _convertToModel($dtoAdvertisingBannerReport)
    {
        $AdvertisingBannerReport = new AdvertisingBannerReport();

        if (isset($dtoAdvertisingBannerReport->id)) {
            $AdvertisingBannerReport->id = $dtoAdvertisingBannerReport->id;
        }

        if (isset($dtoAdvertisingBannerReport->advertising_banner_id)) {
            $AdvertisingBannerReport->advertising_banner_id = $dtoAdvertisingBannerReport->advertising_banner_id;
        }

        if (isset($dtoAdvertisingBannerReport->impression_date)) {
            $AdvertisingBannerReport->impression_date = $dtoAdvertisingBannerReport->impression_date;
        }

        if (isset($dtoAdvertisingBannerReport->page)) {
            $AdvertisingBannerReport->page = $dtoAdvertisingBannerReport->page;
        }

        if (isset($dtoAdvertisingBannerReport->source_ip)) {
            $AdvertisingBannerReport->source_ip = $dtoAdvertisingBannerReport->source_ip;
        }

        return $AdvertisingBannerReport;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCommunityction
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(AdvertisingBannerReport::find($id));
    }


    /*
     * Get all
     * 
     * @return DtoAdvertisingBannerReport
     */
    public static function getAll()
    {

        $result = collect(); 

        foreach (AdvertisingBannerReport::orderBy('id','DESC')->get() as $advertisingBannerReport) {

            $DtoAdvertisingBannerReport = new DtoAdvertisingBannerReport();

            $DtoAdvertisingBannerReport->id = $advertisingBannerReport->id;
            $DtoAdvertisingBannerReport->advertising_banner_id = $advertisingBannerReport->advertising_banner_id;
            $DtoAdvertisingBannerReport->impression_date = $advertisingBannerReport->impression_date;
            $DtoAdvertisingBannerReport->page = $advertisingBannerReport->page;
            $DtoAdvertisingBannerReport->source_ip = $advertisingBannerReport->source_ip;

            $result->push($DtoAdvertisingBannerReport); 
        }
        return $result;

    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoAdvertisingBannerReport
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoAdvertisingBannerReport)
    {
        
        static::_validateData($DtoAdvertisingBannerReport, $idLanguage, DbOperationsTypesEnum::INSERT);
        $AdvertisingBannerReport = static::_convertToModel($DtoAdvertisingBannerReport);

        $AdvertisingBannerReport->save();

        return $AdvertisingBannerReport->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoAdvertisingBannerReport
     * @param int idLanguage
     */
    public static function update(Request $DtoAdvertisingBannerReport)
    {
        static::_validateData($DtoAdvertisingBannerReport, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $AdvertisingBannerReport = AdvertisingBannerReport::find($DtoAdvertisingBannerReport->id);
        DBHelper::updateAndSave(static::_convertToModel($DtoAdvertisingBannerReport), $AdvertisingBannerReport);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id)
    {
        $AdvertisingBannerReport = AdvertisingBannerReport::find($id);

        if (is_null($AdvertisingBannerReport)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $AdvertisingBannerReport->delete();
    }

    #endregion DELETE
}

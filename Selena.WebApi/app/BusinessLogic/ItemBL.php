<?php

namespace App\BusinessLogic;

use App\Cart;
use App\CartDetail;
use App\CartDetailOptional;
use App\Category;
use App\CategoryItem;
use App\CategoryFather;
use App\CategoryLanguage;
use App\Content;
use App\ContentLanguage;
use App\ItemTypeOptional;
use App\ItemSupport;
use App\Supports;
use App\SupportsPrices;
use App\ItemAttachmentLanguageFileTable;
use App\ItemGroupTechnicalSpecification;
use App\ItemAttachmentLanguageFileItemTable;
use App\ItemTechnicalSpecification;
use App\Negotiation;
use App\NegotiationsDetail;
use App\DtoModel\DtoCategories;
use App\DtoModel\DtoImage;
use App\DtoModel\DtoItem;
use App\DtoModel\DtoItemAttachments;
use App\DtoModel\DtoItemGroupTechnicalSpecification;
use App\DtoModel\DtoItemLanguages;
use App\DtoModel\DtoItemPhotos;
use App\DtoModel\DtoItemImages;
use App\DtoModel\DtoItems;
use App\DtoModel\DtoItemsRelated;
use App\DtoModel\DtoItemSupport;
use App\DtoModel\DtoCategoryItemDetail;

use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Enums\SettingEnum;

use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use App\Item;
use App\ItemLanguage;
use App\ItemPhotos;
use App\ItemFavoritesUser;
use App\Producer;
use App\UsersDatas;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use App\DtoModel\DtoUrlList;
use App\DtoModel\DtoUrlListDetail;
use App\DtoModel\DtoPageSitemap;
use App\DtoModel\DtoPriceList;
use App\ItemAttachments;
use App\ItemOptional;

use App\Language;
use App\PriceList;
use App\PriceListItems;
use App\RelatedItems;
use App\SalesOrderDetail;
use App\SalesOrderTokenLinkUser;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Filesystem;

use Barryvdh\DomPDF\Facade as PDF;
use League\Flysystem\Adapter\Local as Adapter;
use Spatie\PdfToImage\Pdf as PdfToImage;

class ItemBL
{

    public static $dirImage = '../storage/app/public/images/Items/';
    public static $dirImageAttachment = '../storage/app/public/images/Attachments/';
    public static $dirImageDigital = '../storage/app/public/images/ItemsDigital/';
    public static $dirImagePdfModule = '../storage/app/public/images/Items/pdf/';
    public static $dirImageSvgModule = '../storage/app/public/images/Items/svg/';
    private static $mimeTypeAllowed = ['jpeg', 'jpg', 'png', 'gif'];
    #region PRIVATE

    /**
     * Convert image size
     * 
     * @param int byte
     *  
     */
    private static function _convertImageSize(int $bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    /**
     * Validate data
     * 
     * @param Request ItemsBL
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoItems, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoItems->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Item::find($DtoItems->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($DtoItems->internal_code)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CodeNotFound), HttpResultsCodesEnum::InvalidPayload);
        }


        //if (is_null($DtoItems->enabled_from)) {
        //   throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoryTipeIdNotValued), HttpResultsCodesEnum::InvalidPayload);
        //  }

        //  if (is_null($DtoItems->enabled_to)) {
        //    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoryFatherIdNotValued), HttpResultsCodesEnum::InvalidPayload);
        // }

        if ($dbOperationType != DbOperationsTypesEnum::UPDATE) {
            //if (strpos($DtoItems->img, '/storage/app/public/images/Items/') === false) {
            if (strpos($DtoItems->img, '/storage/images/Items/') === false) {
                if (!in_array(\explode('/', \explode(':', substr($DtoItems->img, 0, strpos($DtoItems->img, ';')))[1])[1], static::$mimeTypeAllowed)) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FileTypeNotAllowed), HttpResultsCodesEnum::InvalidPayload);
                }
            }
        }
    }

    private static function getUrlAttachments()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/Attachments/';
    }


    private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/Items/';
        //return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/Items/';
    }

    private static function getUrlDigital()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/ItemsDigital/';
    }

    /**
     * Convert to dto
     * 
     * @param Items postalCode
     * 
     * @return DtoItems
     */
    private static function _convertToDto($Items)
    {
        $DtoItems = new DtoItems();

        if (isset($Items->id)) {
            $DtoItems->id = $Items->id;
        }

        if (isset($Items->internal_code)) {
            $DtoItems->internal_code = $Items->internal_code;
        }
        if (isset($Items->unit_of_measure_id)) {
            $DtoItems->unit_of_measure_id = $Items->unit_of_measure_id;
        }

        if (isset($Items->price)) {
            $DtoItems->price = str_replace('.', ',', substr($Items->price, 0, -2));
        }
        if (isset($Items->vat_type_id)) {
            $DtoItems->vat_type_id = $Items->vat_type_id;
        }

        if (isset($Items->height)) {
            $DtoItems->height = $Items->height;
        }

        if (isset($Items->width)) {
            $DtoItems->width = $Items->width;
        }

        if (isset($Items->depth)) {
            $DtoItems->depth = $Items->depth;
        }


        if (isset($Items->volume)) {
            $DtoItems->volume = $Items->volume;
        }

        if (isset($Items->weight)) {
            $DtoItems->weight = $Items->weight;
        }

        if (isset($Items->enabled_from)) {
            $DtoItems->enabled_from = $Items->enabled_from;
        }

        if (isset($Items->enabled_to)) {
            $DtoItems->enabled_to = $Items->enabled_to;
        }

        if (isset($Items->img)) {
            $DtoItems->img = $Items->img;
        }

        if (isset($Items->meta_tag_link)) {
            $DtoItems->meta_tag_link = $Items->meta_tag_link;
        }

        if (isset($Items->meta_tag_title)) {
            $DtoItems->meta_tag_title = $Items->meta_tag_title;
        }
        if (isset($Items->meta_tag_description)) {
            $DtoItems->meta_tag_description = $Items->meta_tag_description;
        }

        if (isset($Items->meta_tag_keyword)) {
            $DtoItems->meta_tag_keyword = $Items->meta_tag_keyword;
        }
        if (isset($Items->qta_min)) {
            $DtoItems->qta_min = $Items->qta_min;
        }
        if (isset($Items->shipment_request)) {
            $DtoItems->shipment_request = $Items->shipment_request;
        }
        if (isset($Items->availability)) {
            $DtoItems->availability = $Items->availability;
        }
        if (isset($Items->commited_customer)) {
            $DtoItems->commited_customer = $Items->commited_customer;
        }
        if (isset($Items->ordered_supplier)) {
            $DtoItems->ordered_supplier = $Items->ordered_supplier;
        }

        if (isset($Items->stock)) {
            $DtoItems->stock = $Items->stock;
        }
        if (isset($Items->producer_id)) {
            $DtoItems->producer_id = $Items->producer_id;
        }

        if (isset($Items->online)) {
            $DtoItems->online = $Items->online;
        }
        if (isset($Items->sell_if_not_available)) {
            $DtoItems->sell_if_not_available = $Items->sell_if_not_available;
        }
        if (isset($Items->pieces_for_pack)) {
            $DtoItems->pieces_for_pack = $Items->pieces_for_pack;
        }
        if (isset($Items->unit_of_measure_id_packaging)) {
            $DtoItems->unit_of_measure_id_packaging = $Items->unit_of_measure_id_packaging;
        }
        if (isset($Items->qta_max)) {
            $DtoItems->qta_max = $Items->qta_max;
        }
        if (isset($Items->digital)) {
            $DtoItems->digital = $Items->digital;
        }
        if (isset($Items->img_digital)) {
            $DtoItems->img_digital = $Items->img_digital;
        }

        if (isset($Items->img_attachment)) {
            $DtoItems->img_attachment = $Items->img_attachment;
        }

        if (isset($Items->limited)) {
            $DtoItems->limited = $Items->limited;
        }
        if (isset($Items->day_of_validity)) {
            $DtoItems->day_of_validity = $Items->day_of_validity;
        }
        if (isset($Items->announcements)) {
            $DtoItems->announcements = $Items->announcements;
        }
        if (isset($Items->announcements_from)) {
            $DtoItems->announcements_from = $Items->announcements_from;
        }
        if (isset($Items->announcements_to)) {
            $DtoItems->announcements_to = $Items->announcements_to;
        }
        if (isset($Items->price)) {
            $DtoItems->price = $Items->price;
        }
        if (isset($Items->price)) {
            $DtoItems->base_price = $Items->price;
        }

        $PriceList = PriceListItems::where('item_id', $DtoItems->id)->get();
        if (isset($PriceList)) {
            foreach ($PriceList as $price) {
                $DtoItems = new DtoItems();
                $DtoItems->id = $price->item_id;
                $DtoItems->pricelist_id = $price->pricelist_id;
                $DtoItems->pricelist = $price->pricelist;
                $DtoItems->pricelist->push($DtoItems);
            }
        }
        return  $DtoItems;
    }

    /**
     * Convert to VatType
     * 
     * @param query
     * 
     * @return html
     */
    private static function _queryShortcodeItemHomePage($query, $idLanguage, $idUser)
    {
        $itemShortcode = DB::select($query);
        if(isset($itemShortcode) && count($itemShortcode)>0){
            $finalHtml = '';
            $tags = SelenaViewsBL::getTagByType('Items');
            $content = Content::where('code', 'Items')->first();
            $contentLanguage = ContentLanguage::where('content_id', $content->id)->where('language_id', $idLanguage)->first();
            
            foreach ($itemShortcode as $items) {
                $newContent = $contentLanguage->content;
                foreach ($tags as $fieldsDb){
                    if(isset($fieldsDb->tag)){
                        if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                            $cleanTag = str_replace('#', '', $fieldsDb->tag);
                            if(is_numeric($items->{$cleanTag}) && $fieldsDb->tag != '#internal_code#'){ 
                                // Calcola prezzo (regola listino/prezzo dedicato a utente)
                                if($fieldsDb->tag == '#price#'){
                                    //prelievo campo nelle impostazioni per metodologia di visualizzazione, le opzioni sono NESSUNO, BARRATO, DEDICATO
                                    $settingDisplayPrice = SettingBL::getByCode(SettingEnum::DisplayPrice);
                                    if($idUser == null || $idUser == ''){
                                        $userId = '';
                                    }else{
                                        $userId = $idUser;
                                    }
                                    //creazione funzione per prelievo prezzo scontato, nelle regole del carrello

                                    switch ($settingDisplayPrice) {
                                        case "NESSUNO":
                                            //se NESSUNO stampo il prezzo in items
                                            $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                            break;
                                        case "BARRATO":
                                            //se BARRATO aggiungo l'html <strike> al prezzo normale e appendo il prezzo calcolato
                                            $priceCalculated = CartBL::getPriceCalculated($items->item_id, $userId);
                                            if($priceCalculated != ""){
                                                $tag = '<strike>' . str_replace('.',',',substr($items->{$cleanTag}, 0, -2)) . ' € </strike><br> ' . str_replace('.',',', $priceCalculated);
                                            }else{
                                                $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                            }
                                            break;
                                        case "DEDICATO":
                                            //se DEDICATO stampo il prezzo calcolato
                                            $priceCalculated = CartBL::getPriceCalculated($items->item_id, $userId);
                                            if($priceCalculated != ""){
                                                $tag = str_replace('.',',', $priceCalculated);
                                            }else{
                                                $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                            }
                                            break;
                                        default:
                                            $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                    }
                                }else{
                                    $tag = str_replace('.',',',substr($items->{$cleanTag}, 0, -2));
                                }
                            }else{
                                $tag = $items->{$cleanTag};
                            }
                            if($fieldsDb->tag == '#item_id#'){
                                $newContent = str_replace($fieldsDb->tag , $items->item_id ,  $newContent);
                            }else{
                                $newContent = str_replace($fieldsDb->tag , $tag ,  $newContent);
                            }
                            if (strpos($newContent, '#producer#') !== false) {
                                if(!is_null($items->producer_id) || $items->producer_id !=''){
                                    $producerItem = Producer::where('id', $items->producer_id)->get()->first();
                                    if(isset($producerItem)){
                                        $newContent = str_replace('#producer#' , $producerItem->business_name ,  $newContent);
                                    }else{
                                        $newContent = str_replace('#producer#' , '-',  $newContent);
                                    }
                                }else{
                                    $newContent = str_replace('#producer#' , '-',  $newContent);
                                }
                            }
                        }
                    } 
                }
                $finalHtml = $finalHtml . $newContent;
            }
            $content = $finalHtml;
        }else{
            $content = "<h3 class='title-no-foto'>Nessuna articolo presente</h3>";
        }   

        return $content;
    }


    /**
     * Convert to VatType
     * 
     * @param DtoItems
     * 
     * @return Items
     */
    private static function _convertToModel($DtoItems)
    {
        $Items = new Item();

        if (isset($DtoItems->id)) {
            $Items->id = $DtoItems->id;
        }

        if (isset($DtoItems->internal_code)) {
            $Items->internal_code = $DtoItems->internal_code;
        }

        if (isset($DtoItems->unit_of_measure_id)) {
            $Items->unit_of_measure_id = $DtoItems->unit_of_measure_id;
        }

        if (isset($DtoItems->price)) {
            $Items->price = $DtoItems->price;
        }

        if (isset($DtoItems->vat_type_id)) {
            $Items->vat_type_id = $DtoItems->vat_type_id;
        }

        if (isset($DtoItems->height)) {
            $Items->height = $DtoItems->height;
        }

        if (isset($DtoItems->width)) {
            $Items->width = $DtoItems->width;
        }

        if (isset($DtoItems->depth)) {
            $Items->depth = $DtoItems->depth;
        }

        if (isset($DtoItems->volume)) {
            $Items->volume = $DtoItems->volume;
        }
        if (isset($DtoItems->weight)) {
            $Items->weight = $DtoItems->weight;
        }
        if (isset($DtoItems->enabled_from)) {
            $Items->enabled_from = $DtoItems->enabled_from;
        }
        if (isset($DtoItems->enabled_to)) {
            $Items->enabled_to = $DtoItems->enabled_to;
        }
        if (isset($DtoItems->img)) {
            $Items->img = $DtoItems->img;
        }
        if (isset($DtoItems->meta_tag_link)) {
            $Items->meta_tag_link = $DtoItems->meta_tag_link;
        }

        if (isset($DtoItems->meta_tag_title)) {
            $Items->meta_tag_title = $DtoItems->meta_tag_title;
        }
        if (isset($DtoItems->meta_tag_description)) {
            $Items->meta_tag_description = $DtoItems->meta_tag_description;
        }
        if (isset($DtoItems->meta_tag_keyword)) {
            $Items->meta_tag_keyword = $DtoItems->meta_tag_keyword;
        }

        if (isset($DtoItems->online)) {
            $Items->online = $DtoItems->online;
        }
        if (isset($DtoItems->qta_min)) {
            $Items->qta_min = $DtoItems->qta_min;
        }
        if (isset($DtoItems->shipment_request)) {
            $Items->shipment_request = $DtoItems->shipment_request;
        }
        if (isset($DtoItems->availability)) {
            $Items->availability = $DtoItems->availability;
        }

        if (isset($DtoItems->commited_customer)) {
            $Items->commited_customer = $DtoItems->commited_customer;
        }
        if (isset($DtoItems->ordered_supplier)) {
            $Items->ordered_supplier = $DtoItems->ordered_supplier;
        }
        if (isset($DtoItems->stock)) {
            $Items->stock = $DtoItems->stock;
        }

        if (isset($DtoItems->producer_id)) {
            $Items->producer_id = $DtoItems->producer_id;
        }

        return $Items;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get
     *
     * @param int id
     *
     * @return Item
     */
    public static function get(int $id)
    {
        return Item::find($id);
    }

    /**
     * Get by ItemLanguages
     * 
     * @param int id
     * @return DtoItems
     */
    public static function getItemLanguages(int $id)
    {
        $result = collect();
        $language_id = 1;
        $DtoItemLanguagesGen = new DtoItemLanguages();
        foreach (ItemLanguage::where('item_id', $id)->where('language_id', '<>', $language_id)->get() as $languageItem) {
            $DtoItemLanguages = new DtoItemLanguages();
            $DtoItemLanguages->id = $languageItem->id;
            $DtoItemLanguages->idItem = $languageItem->item_id;
            $DtoItemLanguages->language_id = Language::where('id', $languageItem->language_id)->first()->description;
            $DtoItemLanguages->description = $languageItem->description;
            $DtoItemLanguages->meta_tag_characteristic = $languageItem->meta_tag_characteristic;
            $DtoItemLanguagesGen->selectItemExist->push($DtoItemLanguages);
        }
        $DtoItemLanguagesGen->totalLn = DB::table('languages')->count();
        $DtoItemLanguagesGen->totalLnItemsPresent = DB::table('items_languages')->where('item_id', $id)->count();
        return $DtoItemLanguagesGen;
    }

    /**
     * Get by ItemLanguages
     * 
     * @param Request request
     * @return result
     */
    public static function getAllItemsWithoutCategory(Request $request)
    {   
        $strWhere = '';

        if($request->search != ''){
            $strWhere = " AND (items.internal_code ilike '%" . $request->search . "%' or items_languages.description ilike '%" . $request->search . "%' or producer.business_name ilike '%" . $request->search . "%')";
        }

        $result = collect();
        foreach (DB::select("SELECT items.id, items.online, items.internal_code, items.producer_id from items inner join items_languages on items.id = items_languages.item_id left join producer on producer.id = items.producer_id
        where items.id not in (select item_id from categories_items) " . $strWhere) as $itemAll) {
            
            $dtoDetailListCategories = new DtoCategoryItemDetail();
            $dtoDetailListCategories->id = $itemAll->id;
            if(is_null($itemAll->online)){
                $dtoDetailListCategories->online = false;
            }else{
                $dtoDetailListCategories->online = $itemAll->online;
            }
            $dtoDetailListCategories->code = $itemAll->internal_code;
            $dtoDetailListCategories->description = ItemLanguage::where('item_id', $itemAll->id)->first()->description;

            if (isset($itemAll->producer_id)){
               $Produc = Producer::where('id', $itemAll->producer_id)->first()->business_name;

            if (isset($Produc)){
                $dtoDetailListCategories->codeProducer = '( ' . $Produc . ')';
            }
            }else{
                $dtoDetailListCategories->codeProducer = '';
            }
       
            $result->push($dtoDetailListCategories);
        }

        return $result;
    }

    
    /**
     * Get all dto
     *
     * @param int id
     * @param int idLanguage
     *
     * @return DtoItemf
     */
    public static function getAllDto(int $idLanguage)
    {
        $result = collect();

        foreach (Item::orderBy('internal_code')->get() as $item) {
            $dtoItem = new DtoItem();
            $dtoItem->id = $item->id;
            $dtoItem->internalCode = $item->internal_code;
            $dtoItem->description = ItemLanguageBL::getByItemAndLanguage($item->id, $idLanguage);
            $result->push($dtoItem);
        }

        return $result;
    }

    /**
     * Get all dto
     *
     * @param int id
     *
     * @return DtoItem
     */
    public static function getAllImagesInFolder($id)
    {
        $result = collect();
        $dir = static::$dirImage;
        $cont = 0;
        foreach (\File::files($dir) as $f) {

            $cont = $cont + 1;

            if (ends_with($f, ['.png', '.jpg', '.jpeg', '.gif'])) {
                $listImages = new DtoItemImages();
                $listImages->id = $cont;
                $listImages->idImgAgg = $id;
                $listImages->img = static::getUrl() . $f->getRelativePathname();
                $listImages->imgName = $f->getRelativePathname();
                $result->push($listImages);
            }
        }

        return $result;
    }

    /**
     * Get url list
     *
     * @param int idLanguage
     * @return DtoUrlList
     */
    public static function getUrlSitemap(int $idLanguage)
    {
        $dtoUrlLinks = collect();
        $idDefaultLanguage = LanguageBL::getDefault();

        foreach (Item::all() as $page) {
            $languageFound = false;

            $dtoUrlList = new DtoUrlList();
            $dtoUrlList->idPage = $page->id;

            foreach (ItemLanguage::where('item_id', $page->id)->get() as $languageItem) {
                if ($languageItem->language_id == $idLanguage || ($languageItem->language_id == $idDefaultLanguage && !$languageFound)) {
                    $dtoUrlList->title = $languageItem->description;
                    $languageFound = true;
                }

                $dtoUrlLinkDetail = new DtoUrlListDetail();
                $dtoUrlLinkDetail->id = $languageItem->id;
                $dtoUrlLinkDetail->idLanguage = $languageItem->language_id;
                $dtoUrlLinkDetail->url = $languageItem->link;
                $dtoUrlList->urlListDetail->push($dtoUrlLinkDetail);
            }

            $dtoUrlLinks->push($dtoUrlList);
        }

        return $dtoUrlLinks;
    }

    public static function getUrlSitemapPrices(int $idLanguage)
    {
        $dtoUrlLinks = collect();
        $idDefaultLanguage = LanguageBL::getDefault();

        foreach (Item::all() as $page) {
            $languageFound = false;

            $dtoUrlList = new DtoUrlList();
            $dtoUrlList->idPage = $page->id;

            foreach (ItemLanguage::where('item_id', $page->id)->get() as $languageItem) {
                if ($languageItem->language_id == $idLanguage || ($languageItem->language_id == $idDefaultLanguage && !$languageFound)) {
                    $dtoUrlList->title = $languageItem->meta_tag_title_price;
                    $languageFound = true;
                }

                $dtoUrlLinkDetail = new DtoUrlListDetail();
                $dtoUrlLinkDetail->id = $languageItem->id;
                $dtoUrlLinkDetail->idLanguage = $languageItem->language_id;
                $dtoUrlLinkDetail->url = $languageItem->link_price;
                $dtoUrlList->urlListDetail->push($dtoUrlLinkDetail);
            }

            $dtoUrlLinks->push($dtoUrlList);
        }

        return $dtoUrlLinks;
    }

    public static function getUrlSitemapPhoto(int $idLanguage)
    {
        $dtoUrlLinks = collect();
        $idDefaultLanguage = LanguageBL::getDefault();

        foreach (Item::all() as $page) {
            $languageFound = false;

            $dtoUrlList = new DtoUrlList();
            $dtoUrlList->idPage = $page->id;

            foreach (ItemLanguage::where('item_id', $page->id)->get() as $languageItem) {
                if ($languageItem->language_id == $idLanguage || ($languageItem->language_id == $idDefaultLanguage && !$languageFound)) {
                    $dtoUrlList->title = $languageItem->meta_tag_title_gallery;
                    $languageFound = true;
                }

                $dtoUrlLinkDetail = new DtoUrlListDetail();
                $dtoUrlLinkDetail->id = $languageItem->id;
                $dtoUrlLinkDetail->idLanguage = $languageItem->language_id;
                $dtoUrlLinkDetail->url = $languageItem->link_gallery;
                $dtoUrlList->urlListDetail->push($dtoUrlLinkDetail);
            }

            $dtoUrlLinks->push($dtoUrlList);
        }

        return $dtoUrlLinks;
    }

    public static function getPages(string $page_type, int $idLanguage)
    {

        $result = collect();
        foreach (DB::select(
            "SELECT items_languages.description, items.updated_at, items_languages.link
            FROM items
            INNER JOIN items_languages ON items.id = items_languages.item_id
            AND items_languages.language_id = " . $idLanguage . " ORDER BY items.updated_at desc"
        ) as $PagesAll) {
            $dtoPageSitemap = new DtoPageSitemap();
            $dtoPageSitemap->url = $PagesAll->link;
            $dtoPageSitemap->title = $PagesAll->description;
            $dtoPageSitemap->updated_at = $PagesAll->updated_at;

            $result->push($dtoPageSitemap);
        }
        return $result;
    }

    public static function getPagesPrices(int $idLanguage)
    {

        $result = collect();
        foreach (DB::select(
            "SELECT items_languages.meta_tag_title_price, items.updated_at, items_languages.link_price
            FROM items
            INNER JOIN items_languages ON items.id = items_languages.item_id
            AND items_languages.language_id = " . $idLanguage . " ORDER BY items.updated_at desc"
        ) as $PagesAll) {
            $dtoPageSitemap = new DtoPageSitemap();
            $dtoPageSitemap->url = $PagesAll->link_price;
            $dtoPageSitemap->title = $PagesAll->meta_tag_title_price;
            $dtoPageSitemap->updated_at = $PagesAll->updated_at;

            $result->push($dtoPageSitemap);
        }
        return $result;
    }

    public static function getPagesPhoto(int $idLanguage)
    {

        $result = collect();
        foreach (DB::select(
            "SELECT items_languages.meta_tag_title_gallery, items.updated_at, items_languages.link_gallery
            FROM items
            INNER JOIN items_languages ON items.id = items_languages.item_id
            AND items_languages.language_id = " . $idLanguage . " ORDER BY items.updated_at desc"
        ) as $PagesAll) {
            $dtoPageSitemap = new DtoPageSitemap();
            $dtoPageSitemap->url = $PagesAll->link_gallery;
            $dtoPageSitemap->title = $PagesAll->meta_tag_title_gallery;
            $dtoPageSitemap->updated_at = $PagesAll->updated_at;

            $result->push($dtoPageSitemap);
        }
        return $result;
    }

    /**
     * Get dto
     *
     * @param int id
     * @param int idLanguage
     *
     * @return DtoItem
     */
    public static function getDto(int $id, int $idLanguage)
    {
        $dtoItem = new DtoItem();
        $dtoItem->id = $id;
        $item = Item::find($id);

        if (!is_null($item)) {
            $dtoItem->internalCode = $item->internal_code;
            $dtoItem->price = $item->price;
            $dtoItem->description = ItemLanguageBL::getByItemAndLanguage($id, $idLanguage);
        }

        return $dtoItem;
    }

    /**
     * Get with group technical specification
     * 
     * @param int idLanguage
     * @return DtoItemGroupTechnicalSpecification
     */
    public static function getWithGroupTechnicalSpecification(int $idLanguage)
    {
        $result = collect();

        foreach (DB::select(
            'SELECT items.id, items.internal_code, COALESCE(il.description, ild.description) AS item_description,
                                    COALESCE(gtsl.description, gtsld.description) AS group_description, items.complete_technical_specification
                FROM items
                LEFT JOIN items_languages il ON items.id = il.item_id AND il.language_id = ?
                LEFT JOIN items_languages ild ON items.id = ild.item_id
                                                    AND ild.language_id = (SELECT id FROM languages WHERE "default" = true)
                LEFT JOIN items_groups_technicals_specifications ON items.id = items_groups_technicals_specifications.item_id
                LEFT JOIN groups_technicals_specifications_languages gtsl
                    ON items_groups_technicals_specifications.group_technical_specification_id = gtsl.group_technical_specification_id
                    AND gtsl.language_id = ?
                LEFT JOIN groups_technicals_specifications_languages gtsld
                    ON items_groups_technicals_specifications.group_technical_specification_id = gtsld.group_technical_specification_id
                    AND gtsld.language_id = (SELECT id FROM languages WHERE "default" = true)',
            [$idLanguage, $idLanguage]
        ) as $itemGroupTechnicalSpecification) {
            $dtoItemGroupTechnicalSpecification = new DtoItemGroupTechnicalSpecification();
            $dtoItemGroupTechnicalSpecification->id = $itemGroupTechnicalSpecification->id;
            $dtoItemGroupTechnicalSpecification->idItem = $itemGroupTechnicalSpecification->id;
            $dtoItemGroupTechnicalSpecification->descriptionItem = $itemGroupTechnicalSpecification->item_description;
            $dtoItemGroupTechnicalSpecification->internalCodeItem = $itemGroupTechnicalSpecification->internal_code;
            $dtoItemGroupTechnicalSpecification->descriptionGroupTechnicalSpecification = $itemGroupTechnicalSpecification->group_description;
            $dtoItemGroupTechnicalSpecification->completeTechnicalSpecification = $itemGroupTechnicalSpecification->complete_technical_specification;
            $result->push($dtoItemGroupTechnicalSpecification);
        }

        return $result;
    }



    /**
     * Get graphic item
     *
     * @param int idLanguage
     *
     */
    public static function getGraphic()
    {
        $rows = DB::select("select count(id) as number from items GROUP BY created_at");
        $array1 = array();
        foreach (ArrayHelper::toCollection($rows) as $row) {
            array_push($array1, $row->number);
        }

        return $array1;
    }

    /**
     * Get number of item
     * 
     * @return int
     */
    public static function count()
    {
        return Item::count();
    }

    /**
     * Get Content for link
     * 
     * @return DtoItems
     */
    public static function getHtml($url, $idUser, $idLanguage)
    {
        $content = ItemLanguage::where('link', $url)->first();
        
        if (isset($content)) {
            $contentItem = Item::where('id', $content->item_id)->first();
            if (is_null($idUser) || $idUser == '') {
                $idUserNew = null;
            }else{
                $idUserNew = $idUser;
            }
            $idContent = Content::where('code', 'PageItems')->first();
            $languageContent = ContentLanguage::where('content_id', $idContent->id)->first();
            $contentHtml = ContentLanguageBL::getByCodeRender($idContent->id, $idLanguage, $url, null, null, $idUserNew);
            $dtoPageItemRender = new DtoItem();
            $dtoPageItemRender->html = $contentHtml;
            $dtoPageItemRender->description = $content->description;
            $dtoPageItemRender->meta_tag_description = $content->meta_tag_description;
            $dtoPageItemRender->meta_tag_keyword = $content->meta_tag_keyword;
            $dtoPageItemRender->meta_tag_title = $content->meta_tag_title;
            
            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
                $protocol = "https://";
            } else {
                $protocol = "http://";
            }

            if (strpos($contentItem->img, "https://") !== false && strpos($contentItem->img, "http://") !== false) {
                $dtoPageItemRender->url_cover_image = $contentItem->img;
            }else{
                $dtoPageItemRender->url_cover_image = $protocol . $_SERVER["HTTP_HOST"] . $contentItem->img;
            }

            return $dtoPageItemRender;
        } else {
            return '';
        }
    }

    /**
     * Get Content for link
     * 
     * @return DtoItems
     */
    public static function getImages($id)
    {
        $item = Item::find($id);
        if(!is_null($id)){
            if (is_null($id)) {
                throw new Exception(DictionaryBL::getTranslate(1, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (is_null($item)) {
                throw new Exception(DictionaryBL::getTranslate(1, DictionariesCodesEnum::ItemNotFound), HttpResultsCodesEnum::InvalidPayload);
            }

            $listImages = new DtoItemImages();

            $listImages->id = $id;
            $listImages->itemId = $id;
            $listImages->img = $item->img;

            if ($item->img != '') {
                $path_parts = pathinfo($item->img);

                $listImages->imgName = $path_parts['basename'];
                $ch = curl_init($item->img);
                curl_setopt($ch, CURLOPT_NOBODY, true);
                curl_exec($ch);
                $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                
                if ($code == 200) {
                    $size = getimagesize($item->img);
                    $listImages->width = $size[0];
                    $listImages->height = $size[1];
                } else {
                    $listImages->width = 0;
                    $listImages->height = 0;
                }

                $weight = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
                curl_close($ch);
                $dimension = static::_convertImageSize($weight);
                $listImages->weight = $dimension;
            } else {
                $listImages->imgName = 'Nessuna immagine presente';
                $listImages->width = 0;
                $listImages->height = 0;
                $listImages->weight = 0;
            }

            /* PHOTO AGGIUNTIVE */
            $itemPhotos = ItemPhotos::where('item_id', $id)->orderBy('order')->get();
            if (isset($itemPhotos)) {
                foreach ($itemPhotos as $itemPhoto) {
                    $dtoItemPhotos = new DtoItemImages();
                    $dtoItemPhotos->id = $itemPhoto->id;
                    $dtoItemPhotos->itemId = $id;
                    $dtoItemPhotos->img = $itemPhoto->img;
                    $dtoItemPhotos->order = $itemPhoto->order;
                    $path_partsImgAgg = pathinfo($itemPhoto->img);
                    $dtoItemPhotos->imgName = $path_partsImgAgg['basename'];
                    $chImgAgg = curl_init($itemPhoto->img);
                    curl_setopt($chImgAgg, CURLOPT_NOBODY, true);
                    curl_exec($chImgAgg);
                    $code = curl_getinfo($chImgAgg, CURLINFO_HTTP_CODE);

                    if ($code == 200) {
                        $sizeAgg = getimagesize($itemPhoto->img);
                        $dtoItemPhotos->width = $sizeAgg[0];
                        $dtoItemPhotos->height = $sizeAgg[1];
                    } else {
                        $dtoItemPhotos->width = 0;
                        $dtoItemPhotos->height = 0;
                    }

                    $weightImgAgg = curl_getinfo($chImgAgg, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
                    curl_close($chImgAgg);

                    $dimensionImgAgg = static::_convertImageSize($weightImgAgg);

                    $dtoItemPhotos->weight = $dimensionImgAgg;

                    $listImages->imgAgg->push($dtoItemPhotos);
                }
            }
            /* END PHOTO AGGIUNTIVE */

            return $listImages;
        }else{
            return '';
        }
    }

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoItems
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Item::find($id));
    }

    /**
     * item by user shop 
     * 
     * @return data
     */
    public static function getItemsUserShop(request $request)
    {
        $currentItem = UsersDatas::where('link', $request->url)->first();
        if (!isset($currentItem)) {
            $idUserItem = 44;
        } else {
            $idUserItem = $currentItem->user_id;
        }
        if (is_null($request->description)) {
            $description = '';
        } else {
            $description = $request->description;
        }
        if (is_null($request->priceFrom)) {
            $priceFrom = '';
        } else {
            $priceFrom = $request->priceFrom;
        }
        if (is_null($request->priceTo)) {
            $priceTo = '';
        } else {
            $priceTo = $request->priceTo;
        }
        if (is_null($request->condition)) {
            $condition = '';
        } else {
            $condition = $request->condition;
        }
        if (is_null($request->producer)) {
            $producer = '';
        } else {
            $producer = $request->producer;
        }
        if (is_null($request->idCategory)) {
            $idCategory = '';
        } else {
            $idCategory = $request->idCategory;
        }

        if (is_null($request->pageSelected)) {
            $pageSelected = '1';
        } else {
            $pageSelected = $request->pageSelected;
        }

        return ContentLanguageBL::UserShopByIdCategory($idCategory, $idUserItem, $description, $priceFrom, $priceTo, $condition, $producer, $pageSelected);
    }

    /**
     * item by user shop 
     * 
     * @return data
     */
    public static function getItemsCategoriesFilter(request $request)
    {

    if (($request['url'] == '/prodotti')) {        
            $idCat = '';
        if (is_null($request['priceFrom'])) {
            $priceFrom = '';
        } else {
            $priceFrom = $request['priceFrom'];
        }
        if (is_null($request['priceTo'])) {
            $priceTo = '';
        } else {
            $priceTo = $request['priceTo'];
        }
        if (is_null($request['condition'])) {
            $condition = '';
        } else {
            $condition = $request['condition'];
        }
        if (is_null($request['producer'])) {
            $producer = '';
        } else {
            $producer = $request['producer'];
        }

        if (is_null($request['order'])) {
            $order = '';
        } else {
            $order = $request['order'];
        }

        if (is_null($request['pageSelected'])) {
            $pageSelected = '1';
        } else {
            $pageSelected = $request['pageSelected'];
        }
        if (is_null($request['description'])) {
            $description = '';
        } else {
            $description = $request['description'];
        }
    } else {   
        $idCategories = CategoryLanguage::where('meta_tag_link', $request['url'])->get()->first()->category_id;
        if (!isset($idCategories)) {
            $idCat = '';
        } else {
            $idCat = $idCategories;
        }

        if (is_null($request['priceFrom'])) {
            $priceFrom = '';
        } else {
            $priceFrom = $request['priceFrom'];
        }
        if (is_null($request['priceTo'])) {
            $priceTo = '';
        } else {
            $priceTo = $request['priceTo'];
        }
        if (is_null($request['condition'])) {
            $condition = '';
        } else {
            $condition = $request['condition'];
        }
        if (is_null($request['producer'])) {
            $producer = '';
        } else {
            $producer = $request['producer'];
        }

        if (is_null($request['order'])) {
            $order = '';
        } else {
            $order = $request['order'];
        }

        if (is_null($request['pageSelected'])) {
            $pageSelected = '1';
        } else {
            $pageSelected = $request['pageSelected'];
        }
         if (is_null($request['description'])) {
            $description = '';
        } else {
            $description = $request['description'];
        }
    }
         if ($idCat == ''){
            return ContentLanguageBL::ItemCategoryByNotIdCategory($idCat, $priceFrom, $priceTo, $condition, $producer, $order, $pageSelected, $description);
         }else{
            return ContentLanguageBL::ItemCategoryByIdCategory($idCat, $priceFrom, $priceTo, $condition, $producer, $order, $pageSelected, $description);
         }

}



      /**
     * downloadfile 
     * 
     * @return data
     */
    public static function downloadfile(request $request)
    {
        $result = collect();

        $SalesOrderTokenLinkUser = SalesOrderTokenLinkUser::where('sales_order_id', $request->order)->get()->first();
        $SalesOrderDetailOrder = SalesOrderDetail::where('id', $SalesOrderTokenLinkUser->sales_order_detail_id)->get()->first();
        $Items = Item::where('id', $SalesOrderDetailOrder->item_id)->get()->first();
        $DtoItems = new DtoItems();
        $mytime = Carbon::now()->format('Y-m-d H:i:s');
        $mydatenow = (strtotime(date($mytime)));

        $calcolodate= (strtotime(date($SalesOrderTokenLinkUser->created_at). " . + ". $Items->day_of_validity ." day"));
        
        if (($request->order == $SalesOrderTokenLinkUser->sales_order_id) && ($request->token == $SalesOrderTokenLinkUser->token_link) && ($request->user == $SalesOrderTokenLinkUser->id_user) && ($request->items == $SalesOrderDetailOrder->item_id) && ($calcolodate >= $mydatenow)){
            $DtoItems->img_digital = $Items->img_digital;
        }else{
          $DtoItems->img_digital = 'Link non valido';  
        
        }      
            $result->push($DtoItems);   
            return  $result; 
           
    }     




    /**
     * Get select
     *
     * @param String $search
     * @return ItemLanguage
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return ItemLanguage::select('id', 'description')->get();
        } else {
            return ItemLanguage::where('description', 'ilike', $search . '%')->select('id', 'description')->get();
        }
    }

    /**
     * Get list Items
     * 
     *  */

    public static function getListItems($sUserId)
    {

        $result = collect();

        foreach (Item::where('created_id', $sUserId)->get() as $items) {
            $DtoItems = new DtoItems();

            $producer = Producer::where('id', $items->producer_id)->first();
            $itemslanguages = ItemLanguage::where('item_id', $items->id)->where('created_id', $sUserId)->first();
            if (isset($itemslanguages)) {
                $DtoItems->id = $items->id;
                $DtoItems->img = $items->img;

                if (isset($producer->business_name)) {
                    $DtoItems->producer_id = $producer->business_name;
                }

                $DtoItems->price = str_replace('.', ',', substr($items->price, 0, -2));
                $DtoItems->description = $itemslanguages->description;

                $result->push($DtoItems);
            }
        }

        return  $result;
    }


    /**
     * getByTranslate
     * 
     * @return DtoUser
     */
    public static function getByTranslateDefault($id, $idItem, $idLanguage)
    {

        $result = collect();
        $itemDefaultLanguage = ItemLanguage::where('item_id', $idItem)->where('language_id', '=', 1)->first();

        $DtoItemLanguage = new DtoItemLanguages();
        $DtoItemLanguage->id = $id;
        $DtoItemLanguage->idItem = $idItem;
        $DtoItemLanguage->language_id = $idLanguage;
        $DtoItemLanguage->link = $itemDefaultLanguage->link;
        $DtoItemLanguage->description = $itemDefaultLanguage->description;
        $DtoItemLanguage->meta_tag_characteristic = $itemDefaultLanguage->meta_tag_characteristic;
        $result->push($DtoItemLanguage);
        return $result;
    }

    /**
     * getByTranslate
     * 
     * @return DtoUser
     */
    public static function getByTranslateDefaultNew($idItem, $idLanguage)
    {

        $result = collect();
        $itemDefaultLanguage = ItemLanguage::where('item_id', $idItem)->where('language_id', '=', 1)->first();

        $DtoItemLanguage = new DtoItemLanguages();
        $DtoItemLanguage->id = '';
        $DtoItemLanguage->idItem = $idItem;
        $DtoItemLanguage->language_id = $idLanguage;
        $DtoItemLanguage->link = $itemDefaultLanguage->link;
        $DtoItemLanguage->description = $itemDefaultLanguage->description;
        $DtoItemLanguage->meta_tag_characteristic = $itemDefaultLanguage->meta_tag_characteristic;
        $result->push($DtoItemLanguage);
        return $result;
    }

    /**
     * getByTranslate
     * 
     * @return DtoUser
     */
    public static function getByTranslate($id)
    {
        $result = collect();
        $itemLanguage = ItemLanguage::where('id', $id)->first();

        $DtoItemLanguage = new DtoItemLanguages();
        $DtoItemLanguage->id = $id;
        $DtoItemLanguage->language_id = $itemLanguage->language_id;
        $DtoItemLanguage->idItem = $itemLanguage->item_id;
        $DtoItemLanguage->description = $itemLanguage->description;
        $DtoItemLanguage->meta_tag_characteristic = $itemLanguage->meta_tag_characteristic;
        $DtoItemLanguage->link = $itemLanguage->link;
        $result->push($DtoItemLanguage);
        return $result;
    }


    /**
     * Get all
     * 
     * @return DtoUser
     */
    public static function getItemById($id)
    {

        $item = Item::where('id', $id)->first();
        $itemLanguage = ItemLanguage::where('item_id', $id)->first();
        $categoryItem = CategoryItem::where('item_id', $id)->first();

        if (isset($itemLanguage)) {

            $DtoItems = new DtoItems();

            $DtoItems->id = $id;
            $DtoItems->price = str_replace('.', ',', substr($item->price, 0, -2));
            $DtoItems->availability = $item->availability;

            if (isset($item->producer_id)) {
                $DtoItems->producer_id = $item->producer_id;
                $DtoItems->descriptionProducer = Producer::where('id', $item->producer_id)->first()->business_name;
            }

            if (isset($categoryItem->category_id)) {
                $DtoItems->category_id = $categoryItem->category_id;

                $category = Category::where('id', $categoryItem->category_id)->first();

                $categoryFather = CategoryFather::where('category_id', $categoryItem->category_id)->first();

                $categoryLanguages = CategoryLanguage::where('category_id', $categoryItem->category_id)->first();
                $categoryLanguages = CategoryLanguage::where('category_id', $categoryItem->category_id)->first();

                $DtoItems->categoryDescription = $categoryLanguages->description;

                if (isset($category)) {
                    $DtoItems->category_father_id = $categoryFather->category_father_id;
                    $categoryFatherLanguages = CategoryLanguage::where('category_id', $categoryFather->category_father_id)->first();
                    $DtoItems->categoryFatherDescription = $categoryFatherLanguages->description;
                }
            }
            $DtoItems->img = $item->img;
            $DtoItems->new = $item->new;
            $DtoItems->free_shipment = $item->free_shipment;
            $DtoItems->available = $item->available;
            $DtoItems->external_link = $item->external_link;
            $DtoItems->description = $itemLanguage->description;
            $DtoItems->meta_tag_characteristic = $itemLanguage->meta_tag_characteristic;

            /* PHOTO AGGIUNTIVE */
            $itemPhotos = ItemPhotos::where('item_id', $id)->get();
            if (isset($itemPhotos)) {
                foreach ($itemPhotos as $itemPhoto) {
                    $dtoItemPhotos = new DtoItemPhotos();
                    $dtoItemPhotos->id = $itemPhoto->id;
                    $dtoItemPhotos->img = $itemPhoto->img;
                    $DtoItems->imgAgg->push($dtoItemPhotos);
                }
            }
            /* END PHOTO AGGIUNTIVE */

            return  $DtoItems;
        } else {
            return '';
        }
    }

    /**
     * Get all
     * 
     * @return DtoUser
     */
    public static function getByImgAgg($id)
    {
        $result = collect();

        /* IMMAGINI ALLEGATE */
        $itemAttachment = ItemAttachments::where('item_id', $id)->get();

        if (isset($itemAttachment)) {
            foreach ($itemAttachment as $itemAttachments) {
                $DtoItemAttachments = new DtoItemAttachments();
                $DtoItemAttachments->id = $itemAttachments->id;
                $DtoItemAttachments->item_id = $itemAttachments->item_id;
                $DtoItemAttachments->img_attachment = $itemAttachments->img_attachment;
                $result->push($DtoItemAttachments);
            }
        }
        /* IMMAGINI ALLEGATE */
        return  $result;
        //return  $DtoItems;
    }

    /**
     * Get all
     * 
     * @return DtoUser
     */
    public static function getDocumentInformationById($id)
    {
        $item = Item::where('id', $id)->first();
        $dtoItemFiles = new DtoItemFiles();
        if (isset($item)) {
            
            $dtoItemFiles->id = $item->id;
            $dtoItemFiles->img = $item->img;

            $itemAttachmentType = ItemAttachmentLanguageTypeTable::where('language_id', 1)->orderBy('item_attachment_type_id')->get();

            foreach ($itemAttachmentType as $itemAttachmentTypes) {
                $dtoItemFilesType = new DtoItemFilesType();
                $dtoItemFilesType->id = $itemAttachmentTypes->item_attachment_type_id;
                $dtoItemFilesType->description = $itemAttachmentTypes->description;

                $queryitemAttachmentFile = "SELECT item_attachment_language_file_table.* from item_attachment_file inner join item_attachment_language_file_item_table on item_attachment_file.id = item_attachment_language_file_item_table.item_attachment_file_id inner join item_attachment_language_file_table on item_attachment_language_file_table.item_attachment_file_id = item_attachment_file.id where item_attachment_type_id = " . $itemAttachmentTypes->item_attachment_type_id . " and item_id = " .  $id;
                $itemAttachmentFiles = DB::select($queryitemAttachmentFile);
                $dtoItemFilesType->count = count($itemAttachmentFiles);

                foreach ($itemAttachmentFiles as $itemAttachmentFile) {
                    $dtoItemFilesUrl = new DtoItemFilesUrl();
                    $dtoItemFilesUrl->id = $itemAttachmentFile->item_attachment_file_id;
                    $dtoItemFilesUrl->description = $itemAttachmentFile->description;
                    $dtoItemFilesUrl->url = $itemAttachmentFile->url;
                    $dtoItemFilesType->itemAttachmentFile->push($dtoItemFilesUrl);
                }
                 
                $dtoItemFiles->itemAttachmentType->push($dtoItemFilesType);
            }
        }
        return $dtoItemFiles;
    }

    /**
     * Get ByPriceList
     * 
     * @return DtoItem
     */
    public static function getByPriceList($id)
    {
        $result = collect();
        $PriceList = PriceListItems::where('item_id', $id)->get();

        if (isset($PriceList)) {
            foreach ($PriceList as $price) {
                $DtoPriceList = new DtoPriceList();

                $DtoPriceList->id = $price->item_id;
                if (isset($price->pricelist_id)) {
                    $DtoPriceList->pricelist_id = $price->pricelist_id;
                }
                if (isset($price->price)) {
                    $DtoPriceList->pricelist = $price->price;
                } else {
                    $DtoPriceList->pricelist = '';
                }
                //if (isset($price->pricelist_id)) {
                //$DtoPriceList->pricelistDescription = PriceList::where('id', $DtoPriceList->pricelist_id)->first()->code_list;
                $pricelist =  PriceList::where('id', $DtoPriceList->pricelist_id)->first();
                if (isset($pricelist)) {
                    $DtoPriceList->pricelistDescription = $pricelist->code_list;
                } else {
                    $DtoPriceList->pricelistDescription = '';
                }
                //}
                //$DtoItems->pricelist->push($DtoPriceList);
                $result->push($DtoPriceList);
            }
        }
        return $result;
    }

    /**
     * getAllRelatedSearch
     * 
     * @return DtoItems
     */
    public static function getAllRelatedSearch($request)
    {
        $result = collect();

        $descriptionsearc = $request->input('search.0.descriptionsearc');
        $category_idsearc = $request->input('search.0.category_idsearc');
        $internal_codesearc = $request->input('search.0.internal_codesearc');

        $idLanguageContent = $request->idLanguage;
        $strWhere = "";
        if ($internal_codesearc != "") {
            $strWhere = $strWhere . ' AND items.internal_code ilike \'%' . $internal_codesearc . '%\'';
        }
        if ($descriptionsearc != "") {
            $strWhere = $strWhere . ' AND items_languages.description ilike \'%' . $descriptionsearc . '%\'';
        }

        if ($category_idsearc != "") {
            $strWhere = $strWhere . ' AND categories_items.category_id = \'' . $category_idsearc . '\'';
        }

        foreach (DB::select(
            '   SELECT   items.internal_code,
                items.id,
                items_languages.description,
                categories_languages.category_id
        FROM items
        INNER JOIN items_languages ON items_languages.item_id = items.id
        INNER JOIN categories_items ON  categories_items.item_id = items.id
        INNER JOIN categories_languages ON  categories_items.category_id = categories_languages.category_id
        WHERE items_languages.language_id = :idLanguage ' . $strWhere,
            [
                'idLanguage' => $idLanguageContent
            ]
        ) as $ResultItem) {

            $DtoItemsRelated = new DtoItemsRelated();

            if (isset($ResultItem->id)) {
                $DtoItemsRelated->id = $ResultItem->id;
            }
            if (isset($ResultItem->internal_code)) {
                $DtoItemsRelated->internal_code = $ResultItem->internal_code;
            } else {
                $DtoItemsRelated->internal_code = '';
            }

            $DtoItemsRelated->descriptionArticle = $ResultItem->description;
            $DtoItemsRelated->descriptionCategories = CategoryLanguage::where('category_id', $ResultItem->category_id)->first()->description;

            $checkRelated = RelatedItems::where('item_related_id', $ResultItem->id)->get()->first();
            if (isset($checkRelated)) {
                $DtoItemsRelated->check_related = true;
            } else {
                $DtoItemsRelated->check_related = false;
            }
            $result->push($DtoItemsRelated);
        }

        return $result;
    }


          /**
     * 
     * @param int $idCategories
     * @param int $idItem
     * @param int $idLanguage
     */
    public static function delRelated(int $idItems, int $rowIdSelected, int $idLanguage)
    {
        if (is_null($idItems)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, 'Articolo Non Trovato'), HttpResultsCodesEnum::InvalidPayload);
        }
        $RoleUser = RelatedItems::where('item_related_id', $idItems)->where('item_id', $rowIdSelected);
        $RoleUser->delete();
    }


    /**
     * Get all
     * 
     * @return DtoItems
     */
    public static function getAllRelated($id, $idLanguage)
    {
        $result = collect();

        foreach (ItemLanguage::where('language_id', $idLanguage)->where('item_id', '=', $id)->get() as $ItemAll) {

            $Items = Item::where('id', $ItemAll->item_id)->first();
            $IdCategoryItem = CategoryItem::where('item_id', $Items->id)->first();

            if (isset($IdCategoryItem)) {
                $categoryId = CategoryLanguage::where('category_id', $IdCategoryItem->category_id)->first()->id;
                $descriptionCategory = CategoryLanguage::where('category_id', $IdCategoryItem->category_id)->first()->description;
            } else {
                $descriptionCategory = "";
                $categoryId = "";
            }
            $DtoItemsRelated = new DtoItemsRelated();

            if (isset($ItemAll->item_id)) {
                $DtoItemsRelated->id = $ItemAll->item_id;
            }
            if (isset($Items->internal_code)) {
                $DtoItemsRelated->internal_code = $Items->internal_code;
            }
            if (isset($ItemAll->description)) {
                $DtoItemsRelated->descriptionArticle = $ItemAll->description;
            }
            if (isset($descriptionCategory)) {
                $DtoItemsRelated->descriptionCategories = $descriptionCategory;
            }

            $RelatedItems = RelatedItems::where('item_id', $id)->where('item_related_id', $ItemAll->item_id)->get()->first();

            if (isset($RelatedItems)) {
                $DtoItemsRelated->check_related = true;
            } else {
                $DtoItemsRelated->check_related = false;
            }
            $result->push($DtoItemsRelated);
        }
        return $result;
    }

    public static function getByFile($id)
    {

        $result = collect();
        $dir = static::$dirImageAttachment;
        $cont = 0;

        foreach (\File::files($dir) as $f) {
            $cont = $cont + 1;
            if (ends_with($f, ['.png', '.jpg', '.jpeg', '.gif'])) {
                $listImages = new DtoImage();
                $listImages->id = $cont;
                $listImages->idImgAgg = $id;
                $listImages->img = static::getUrlAttachments() . $f->getRelativePathname();
                $listImages->imgName = $f->getRelativePathname();
                $result->push($listImages);
            }
        }
        return $result;
    }




    public static function getByFileNoId($request)
    {

        $result = collect();
        $dir = static::$dirImageAttachment;
        $cont = 0;

        foreach (\File::files($dir) as $f) {
            $cont = $cont + 1;
            if (ends_with($f, ['.png', '.jpg', '.jpeg', '.gif'])) {
                $listImages = new DtoImage();
                $listImages->id = $cont;
                $listImages->idImgAgg = '';
                $listImages->img = static::getUrlAttachments() . $f->getRelativePathname();
                $listImages->imgName = $f->getRelativePathname();
                $result->push($listImages);
            }
        }
        return $result;
    }


    /**
     * update Image Name
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateImages(Request $request)
    {
        $itemAttachmentOnDb = ItemAttachments::find($request->idImgAgg);


        if (isset($itemAttachmentOnDb)) {
            $itemAttachmentOnDb->img_attachment = static::getUrlAttachments() . $request->imgName;
            $itemAttachmentOnDb->save();
        } else {
            $itemAttachment = new ItemAttachments();
            $itemAttachment->item_id = $request->idItem;
            $itemAttachment->img_attachment = static::getUrlAttachments() . $request->imgName;
            $itemAttachment->save();
        }
    }

    /**
     * add Favorites Item
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function addFavoritesItem(Request $request){
        //controllo che l'utente sia presente
        if (is_null($request->user_id)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        $favoriteItemUserOnDb = ItemFavoritesUser::where('user_id', $request->user_id)->where('item_id', $request->idItem)->get()->first();
        if(isset($favoriteItemUserOnDb)){
            $favoriteItemUserOnDb->delete();
            $varReturn = 0;
            //se presente elimino la riga poichè ho cliccato su un articolo che era già tra i miei preferiti
        }else{
            //se non presente aggiungo la riga poichè ho cliccato un articolo non presente tra i miei preferiti
            $favoriteItemUser = new ItemFavoritesUser();
            $favoriteItemUser->item_id = $request->idItem;
            $favoriteItemUser->user_id = $request->user_id;
            $favoriteItemUser->save();
            $varReturn = 1;
        }
        return $varReturn;
    }

    /**
     * move photo from a folder to another
     * 
     * @param Request request
     */

    public static function movePhoto(){
        $result = '';
        foreach (Item::where('online', true)->get() as $itemAll) {
            if(!is_null($itemAll->img)){
                $strImagePathTo = str_replace('/storage/', '../storage/app/public/', $itemAll->img);
                $strImagePathFrom = str_replace('/storage/images/', '../storage/app/public/IMMAGINI/', $itemAll->img);
                
                //se l'immagine esiste nel percorso di partenza
                if(file_exists($strImagePathFrom)) {
                    //se l'immagine non esiste già nel percorso di destinazione
                    if(!file_exists($strImagePathTo)) {
                        \File::move($strImagePathFrom, $strImagePathTo);
                        $result = 'ok';
                    }
                }
            }
        }

        return $result;
    }

    /**
     * move file from a folder to another
     * 
     * @param Request request
     */

    public static function moveFile(){
        $result = '';
        foreach (ItemAttachmentLanguageFileTable::where('description', '<>', '')->get() as $itemAll) {
            if(!is_null($itemAll->url)){
                $strImagePathTo = str_replace('/storage/', '../storage/app/public/', $itemAll->url);
                $strImagePathFrom = str_replace('/storage/files/', '../storage/app/public/ALLEGATI/', $itemAll->url);
                
                //se l'immagine esiste nel percorso di partenza
                if(file_exists($strImagePathFrom)) {
                    //se l'immagine non esiste già nel percorso di destinazione
                    if(!file_exists($strImagePathTo)) {
                        \File::move($strImagePathFrom, $strImagePathTo);
                        $result = 'ok';
                    }
                }
            }
        }

        return $result;
    }

    public static function getItemShortcode($type, $idCategoryItem, $idProducerItem, $limit, $orderBy, $idLanguage, $idUser){
        $content = '';
        $strWhere = '';
        $strOrderBy = '';
        $strLimit = '';

        if($idLanguage != ''){
            $strWhere = $strWhere . ' AND items_languages.language_id = ' . $idLanguage;
        }

        if ($idCategoryItem != "") {
            $strWhere = $strWhere . ' AND categories_items.category_id = ' . $idCategoryItem;
        }

        if ($idProducerItem != "") {
            $strWhere = $strWhere . ' AND items.producer_id = ' . $idProducerItem;
        }

        if ($orderBy != "") {
            $strOrderBy = ' ORDER BY ' . $orderBy;
        }else{
            $strOrderBy = ' ORDER BY RANDOM() ';
        }

        if ($limit != "" && $limit != "0") {
            $strLimit = $strLimit . ' LIMIT ' . $limit;
        }

        switch (strtolower($type)) { 
            /************************************************************/
            // ARTICOLI IN PROMOZIONE
            /************************************************************/
            case 'promotion':

                //TO DO - query su tabella delle promozioni
                $content = static::_queryShortcodeItemHomePage('SELECT * FROM items INNER JOIN items_languages ON items_languages.item_id = items.id INNER JOIN categories_items ON categories_items.item_id = items.id WHERE items.promotion = \'True\'AND  items.online = \'True\'' . $strWhere . $strOrderBy . $strLimit, $idLanguage, $idUser);
                break;
            
            /************************************************************/
            // ARTICOLI NUOVI
            /************************************************************/
            case 'new':
                $content = static::_queryShortcodeItemHomePage('SELECT distinct items.*, items_languages.* as items_languages FROM items INNER JOIN items_languages ON items_languages.item_id = items.id INNER JOIN categories_items ON categories_items.item_id = items.id WHERE items.announcements = \'True\' AND items.online = \'True\'' . $strWhere . $strLimit, $idLanguage, $idUser);
                break;

            /************************************************************/
            // ARTICOLI PIù VENDUTI
            /************************************************************/
            case 'bestseller':
                $whereItemBestSeller = "";
                $queryPreBestSeller = "SELECT sales_orders_details.item_id, count(sales_orders_details.item_id) as cont from sales_orders_details inner join items on items.id = sales_orders_details.item_id inner join items_languages on items.id = items_languages.item_id inner join categories_items on categories_items.item_id = sales_orders_details.item_id where items.online = 'True' " . $strWhere . " group by sales_orders_details.item_id order by cont desc " . $strLimit; 
                
                $itemBestSeller = DB::select($queryPreBestSeller);

                if(isset($itemBestSeller) && count($itemBestSeller)>0){
                    foreach ($itemBestSeller as $itemsBestSell) {
                        if($whereItemBestSeller == ''){
                            $whereItemBestSeller = ' items.id=' . $itemsBestSell->item_id;
                        }else{
                            $whereItemBestSeller = $whereItemBestSeller . ' OR items.id=' . $itemsBestSell->item_id;
                        }
                    } 
                    
                    if($whereItemBestSeller != ''){
                        $whereItemBestSeller = ' AND ( ' . $whereItemBestSeller . ' )';
                    }  

                    $content = static::_queryShortcodeItemHomePage('SELECT distinct items.*, items_languages.* as items_languages FROM items INNER JOIN items_languages ON items_languages.item_id = items.id INNER JOIN categories_items ON categories_items.item_id = items.id WHERE items.online = \'True\'' . $strWhere . $whereItemBestSeller, $idLanguage, $idUser);    
                }else{
                    $content = static::_queryShortcodeItemHomePage('SELECT * FROM items INNER JOIN items_languages ON items_languages.item_id = items.id INNER JOIN categories_items ON categories_items.item_id = items.id WHERE items.online = \'True\'' . $strWhere . $strOrderBy . $strLimit, $idLanguage, $idUser);
                }

                break;

            /************************************************************/
            // ARTICOLI ULTIMI VISUALIZZATI
            /************************************************************/
            case 'lastseen':
                $content = static::_queryShortcodeItemHomePage('SELECT distinct items.*, items_languages.* as items_languages FROM items INNER JOIN items_languages ON items_languages.item_id = items.id INNER JOIN categories_items ON categories_items.item_id = items.id WHERE items.announcements = \'True\' AND items.online = \'True\'' . $strWhere . $strLimit, $idLanguage, $idUser);
                break;

            /************************************************************/
            // ARTICOLI RANDOM
            /************************************************************/
            case 'random':
                $content = static::_queryShortcodeItemHomePage('SELECT * FROM items INNER JOIN items_languages ON items_languages.item_id = items.id INNER JOIN categories_items ON categories_items.item_id = items.id WHERE items.online = \'True\'' . $strWhere . $strOrderBy . $strLimit, $idLanguage, $idUser);
                break;

            default:
                $content = static::_queryShortcodeItemHomePage('SELECT * FROM items INNER JOIN items_languages ON items_languages.item_id = items.id INNER JOIN categories_items ON categories_items.item_id = items.id WHERE items.online = \'True\'' . $strWhere . $strOrderBy . $strLimit, $idLanguage, $idUser);
                break;
        }    
        return $content;
    }

    /**
     * Get all
     * 
     * @return DtoItems
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (ItemLanguage::where('language_id', $idLanguage)->get() as $ItemAll) {

            $DtoItems = new DtoItems();
            $Items = Item::where('id', $ItemAll->item_id)->first();
            //$IdCategoryItem = CategoryItem::where('item_id', $Items->id)->first();
            $PriceListItems = PriceListItems::where('item_id', $ItemAll->item_id)->first();
            //$ItemsAttachment = ItemAttachments::where('item_id', $ItemAll->item_id)->first();


            /*if (isset($IdCategoryItem)) {
                $categoryId = CategoryLanguage::where('category_id', $IdCategoryItem->category_id)->first()->id;
                $descriptionCategory = CategoryLanguage::where('category_id', $IdCategoryItem->category_id)->first()->description;
            } else {*/
                $descriptionCategory = "";
                $categoryId = "";
            /*}*/

            if (isset($ItemAll->item_id)) {
                $DtoItems->id = $ItemAll->item_id;
            }
            // if (isset($ItemAll->language_id)) {
            //    $DtoItems->idLanguage = $ItemAll->language_id;
            // }
             if (isset($ItemAll->link)) {
                $DtoItems->Url = $ItemAll->link;
            }

            if (isset($Items->internal_code)) {
                $DtoItems->internal_code = $Items->internal_code;
            }

            if (isset($Items->image_code_association)) {
                $DtoItems->image_code_association = $Items->image_code_association;
            }
            if (isset($Items->online)) {
                $DtoItems->online = $Items->online;
            }else {
                $DtoItems->online=false;
                }

            if (isset($ItemAll->description)) {
                $DtoItems->descriptionArticle = $ItemAll->description;
            }
            if (isset($ItemAll->description)) {
                $DtoItems->description = $ItemAll->description;
            }

            if (isset($Items->producer_id)) {
                $DtoItems->producer_id = $Items->producer_id;
            }

            if (isset($Items->unit_of_measure_id)) {
                $DtoItems->unit_of_measure_id = $Items->unit_of_measure_id;
            }

             if (isset($Items->price)) {
                $DtoItems->base_price = $Items->price;
            }

            if (isset($Items->vat_type_id)) {
                $DtoItems->vat_type_id = $Items->vat_type_id;
            }

            if (isset($Items->height)) {
                $DtoItems->height = $Items->height;
            }

            if (isset($Items->width)) {
                $DtoItems->width = $Items->width;
            }

            if (isset($Items->depth)) {
                $DtoItems->depth = $Items->depth;
            }
            if (isset($Items->volume)) {
                $DtoItems->volume = $Items->volume;
            }
            if (isset($Items->weight)) {
                $DtoItems->weight = $Items->weight;
            }

            if (isset($Items->enabled_from)) {

                $DtoItems->enabled_from = $Items->enabled_from;
            }

            if (isset($Items->enabled_to)) {
                $DtoItems->enabled_to = $Items->enabled_to;
            }

            if (isset($descriptionCategory)) {
                $DtoItems->descriptionCategories = $descriptionCategory;
            }

            if (isset($categoryId)) {
                $DtoItems->category_id = $categoryId;
            }

            if (isset($Items->img_attachment)) {
                $DtoItems->img_attachment = $Items->img_attachment;
            }

            if (isset($Items->img)) {
                $DtoItems->img = $Items->img;
            }
            if (isset($ItemAll->link)) {
                $DtoItems->meta_tag_link = $ItemAll->link;
            }
            if (isset($ItemAll->meta_tag_title)) {
                $DtoItems->meta_tag_title = $ItemAll->meta_tag_title;
            }
            if (isset($ItemAll->meta_tag_description)) {
                $DtoItems->meta_tag_description = $ItemAll->meta_tag_description;
            }
            if (isset($ItemAll->meta_tag_characteristic)) {
                $DtoItems->meta_tag_characteristic = $ItemAll->meta_tag_characteristic;
            }
            if (isset($ItemAll->meta_tag_keyword)) {
                $DtoItems->meta_tag_keyword = $ItemAll->meta_tag_keyword;
            }
            if (isset($Items->qta_min)) {
                $DtoItems->qta_min = $Items->qta_min;
            }

            if (isset($Items->shipment_request)) {
                $DtoItems->shipment_request = $Items->shipment_request;
            }
            if (isset($Items->available)) {
                $DtoItems->available = $Items->available;
            }

            if (isset($Items->commited_customer)) {
                $DtoItems->commited_customer = $Items->commited_customer;
            }
            if (isset($Items->ordered_supplier)) {
                $DtoItems->ordered_supplier = $Items->ordered_supplier;
            }
            if (isset($Items->stock)) {
                $DtoItems->stock = $Items->stock;
            }
            if (isset($Items->producer_id)) {
                $DtoItems->producer_id = $Items->producer_id;
            }

            if (isset($Items->sell_if_not_available)) {
                $DtoItems->sell_if_not_available = $Items->sell_if_not_available;
            }
            if (isset($Items->pieces_for_pack)) {
                $DtoItems->pieces_for_pack = $Items->pieces_for_pack;
            }
            if (isset($Items->unit_of_measure_id_packaging)) {
                $DtoItems->unit_of_measure_id_packaging = $Items->unit_of_measure_id_packaging;
            }
            if (isset($Items->qta_max)) {
                $DtoItems->qta_max = $Items->qta_max;
            }
            if (isset($Items->digital)) {
                $DtoItems->digital = $Items->digital;
            }
            if (isset($Items->img_digital)) {
                $DtoItems->img_digital = $Items->img_digital;
            }

            if (isset($Items->limited)) {
                $DtoItems->limited = $Items->limited;
            }

            if (isset($Items->delivery_time_if_available)) {
                $DtoItems->delivery_time_if_available = $Items->delivery_time_if_available;
            }
            if (isset($Items->delivery_time_if_out_stock)) {
                $DtoItems->delivery_time_if_out_stock = $Items->delivery_time_if_out_stock;
            }
            if (isset($Items->additional_delivery_costs)) {
                $DtoItems->additional_delivery_costs = $Items->additional_delivery_costs;
            }

            if (isset($Items->day_of_validity)) {
                $DtoItems->day_of_validity = $Items->day_of_validity;
            }
            if (isset($PriceListItems->pricelist_id)) {
                $DtoItems->pricelist_id = $PriceListItems->pricelist_id;
                $DtoItems->pricelistDescription = PriceList::where('id', $DtoItems->pricelist_id)->first()->code_list;
            }
            if (isset($PriceListItems->price)) {
                $DtoItems->pricelist = $PriceListItems->price;
            }

            if (isset($Items->announcements)) {
                if(is_null($Items->announcements)){
                    $DtoItems->announcements = false;
                }else{
                    $DtoItems->announcements = $Items->announcements;
                } 
            }
            if (isset($Items->promotion)) {
                if(is_null($Items->promotion)){
                    $DtoItems->promotion = false;
                }else{
                    $DtoItems->promotion = $Items->promotion;
                } 
            }

            if (isset($Items->announcements_from)) {
                $DtoItems->announcements_from = $Items->announcements_from;
            }
            if (isset($Items->announcements_to)) {
                $DtoItems->announcements_to = $Items->announcements_to;
            }
            if (isset($Items->price)) {
                $DtoItems->price = str_replace('.',',',substr($Items->price, 0, -2));
            }

            
            $support = Supports::all();
            if(isset($support)){
                foreach ($support as $supports) {
                    $ItemsSupport = ItemSupport::where('item_id', $ItemAll->item_id)->where('supports_id', $supports->id)->get()->first();

                    $DtoItemsSupport = new DtoItemSupport();
                    $DtoItemsSupport->id = $supports->id;
                    $DtoItemsSupport->description = $supports->description;

                    if(isset($ItemsSupport)){
                        $DtoItemsSupport->checked = true;
                    }else{
                        $DtoItemsSupport->checked = false;
                    }

                    $DtoItems->supports->push($DtoItemsSupport);
                }
            }
            
            $result->push($DtoItems);
        }

        return $result;
    }

    #endregion GET

    #region UPDATE

    /**
     * Set completed
     * 
     * @param Request request
     */
    public static function complete(Request $request, int $idUser, int $idLanguage)
    {
        if (is_null($request->idItem)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        $item = Item::find($request->idItem);

        if (is_null($item)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ItemNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $item->complete_technical_specification = $request->complete;

        $item->save();
    }

    #endregion UPDATE



    #region INSERT

    /**
     * insert Related
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertRelated(Request $request, int $idUser)
    {
      
        DB::beginTransaction();

        try {
                $newRelatedItem = new RelatedItems();
                $newRelatedItem->item_related_id = $request->idItems;
                $newRelatedItem->item_id = $request->rowIdSelected;
                $newRelatedItem->save();

         /*   RelatedItems::where('item_id', $request->itemId)->delete();
            foreach ($request->itemRelated as $itemRelated) {
                $newRelatedItem = new RelatedItems();
                $newRelatedItem->item_related_id = $itemRelated['item_id_related'];
                $newRelatedItem->item_id = $request->itemId;

                $newRelatedItem->save();
            }*/

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $newRelatedItem->id;
    }


    /**
     * Insert
     * 
     * @param Request DtoItems
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoItems, int $idUser)
    {
        //static::_validateData($DtoCategories, $DtoCategories->idLanguage, DbOperationsTypesEnum::INSERT);
        //$Categories = static::_convertToModel($DtoCategories);
        //$Categories->save();

        DB::beginTransaction();

        try {

            $Items = new Item();

            if (isset($DtoItems->internal_code)) {
                $Items->internal_code = $DtoItems->internal_code;
            }

            if (isset($DtoItems->unit_of_measure_id)) {
                $Items->unit_of_measure_id = $DtoItems->unit_of_measure_id;
            }

            if (isset($DtoItems->producer_id)) {
                $Items->producer_id = $DtoItems->producer_id;
            }

            if (isset($DtoItems->vat_type_id)) {
                $Items->vat_type_id = $DtoItems->vat_type_id;
            }

            if (isset($DtoItems->base_price)) {
                $Items->price = $DtoItems->base_price;
            }

            if (isset($DtoItems->height)) {
                $Items->height = $DtoItems->height;
            }

            if (isset($DtoItems->width)) {
                $Items->width = $DtoItems->width;
            }
            if (isset($DtoItems->depth)) {
                $Items->depth = $DtoItems->depth;
            }
            if (isset($DtoItems->volume)) {
                $Items->volume = $DtoItems->volume;
            }

            if (isset($DtoItems->weight)) {
                $Items->weight = $DtoItems->weight;
            }

            if (isset($DtoItems->enabled_from)) {
                $Items->enabled_from = $DtoItems->enabled_from;
            }

            if (isset($DtoItems->online)) {
                $Items->online = $DtoItems->online;
            }
            if (isset($DtoItems->new)) {
                $Items->new = $DtoItems->new;
            }
            if (isset($DtoItems->available)) {
                $Items->available = $DtoItems->available;
            }

            if (isset($DtoItems->external_link)) {
                $Items->external_link = $DtoItems->external_link;
            }


            if (isset($DtoItems->enabled_to)) {
                $Items->enabled_to = $DtoItems->enabled_to;
            }

            if (isset($DtoItems->created_id)) {
                $Items->created_id = $DtoItems->created_id;
            }
            if (isset($DtoItems->img)) {
                if (strpos($DtoItems->img, '/storage/app/public/images/Items/') === false) {
                    Image::make($DtoItems->img)->save(static::$dirImage . $DtoItems->created_id . $DtoItems->imgName);
                }
            }

            if (isset($DtoItems->img)) {
                $Items->img = static::getUrl() . $DtoItems->created_id . $DtoItems->imgName;
            }
            if (isset($DtoItems->qta_min)) {
                $Items->qta_min = $DtoItems->qta_min;
            }

            if (isset($DtoItems->shipment_request)) {
                $Items->shipment_request = $DtoItems->shipment_request;
            }

            if (isset($DtoItems->free_shipment)) {
                $Items->free_shipment = $DtoItems->free_shipment;
            }

            if (isset($DtoItems->availability)) {
                $Items->availability = $DtoItems->availability;
            }
            if (isset($DtoItems->commited_customer)) {
                $Items->commited_customer = $DtoItems->commited_customer;
            }
            if (isset($DtoItems->ordered_supplier)) {
                $Items->ordered_supplier = $DtoItems->ordered_supplier;
            }

            if (isset($DtoItems->stock)) {
                $Items->stock = $DtoItems->stock;
            }

            if (isset($DtoItems->shipping_price)) {
                $Items->shipping_price = $DtoItems->shipping_price;
            }

            if (isset($DtoItems->producer_id)) {
                $Items->producer_id = $DtoItems->producer_id;
            }

            if (isset($DtoItems->sell_if_not_available)) {
                $Items->sell_if_not_available = $DtoItems->sell_if_not_available;
            }

            if (isset($DtoItems->pieces_for_pack)) {
                $Items->pieces_for_pack = $DtoItems->pieces_for_pack;
            }
            if (isset($DtoItems->unit_of_measure_id_packaging)) {
                $Items->unit_of_measure_id_packaging = $DtoItems->unit_of_measure_id_packaging;
            }
            if (isset($DtoItems->qta_max)) {
                $Items->qta_max = $DtoItems->qta_max;
            }
            if (isset($DtoItems->digital)) {
                $Items->digital = $DtoItems->digital;
            }

            if(isset($DtoItems->img_digital)){
                if (strpos($DtoItems->img_digital, '/storage/app/public/images/ItemsDigital/') === false) {
                    Image::make($DtoItems->img_digital)->save(static::$dirImageDigital . $DtoItems->created_id . $DtoItems->imgName);
                }
            }
            if(isset($DtoItems->img_attachment)){
                if (strpos($DtoItems->img_attachment, '/storage/app/public/images/Attachments/') === false) {
                    Image::make($DtoItems->img_attachment)->save(static::$dirImageAttachment . $DtoItems->created_id . $DtoItems->imgName);
                }
            }

            if (isset($DtoItems->img_digital)) {
                $Items->img_digital = static::getUrlDigital() . $DtoItems->created_id . $DtoItems->imgName;
            }

            if (isset($DtoItems->limited)) {
                $Items->limited = $DtoItems->limited;
            }
            if (isset($DtoItems->day_of_validity)) {
                $Items->day_of_validity = $DtoItems->day_of_validity;
            }
            if (isset($DtoItems->delivery_time_if_available)) {
                $Items->delivery_time_if_available = $DtoItems->delivery_time_if_available;
            }
            if (isset($DtoItems->delivery_time_if_out_stock)) {
                $Items->delivery_time_if_out_stock = $DtoItems->delivery_time_if_out_stock;
            }
            if (isset($DtoItems->additional_delivery_costs)) {
                $Items->additional_delivery_costs = $DtoItems->additional_delivery_costs;
            }
            if (isset($DtoItems->announcements)) {
                $Items->announcements = $DtoItems->announcements;
            }
            if (isset($DtoItems->announcements_from)) {
                $Items->announcements_from = $DtoItems->announcements_from;
            }
            if (isset($DtoItems->announcements_to)) {
                $Items->announcements_to = $DtoItems->announcements_to;
            }
            $Items->save();

            $ItemsLanguage = new ItemLanguage();

            if (isset($DtoItems->idLanguage)) {
                $ItemsLanguage->language_id = $DtoItems->idLanguage;
            }
            if (isset($Items->id)) {
                $ItemsLanguage->item_id = $Items->id;
            }
            if (isset($DtoItems->description)) {
                $ItemsLanguage->description = $DtoItems->description;
            }

            if (isset($DtoItems->meta_tag_link)) {
                $ItemsLanguage->link = $DtoItems->meta_tag_link;
            }

            if (isset($DtoItems->meta_tag_title)) {
                $ItemsLanguage->meta_tag_title = $DtoItems->meta_tag_title;
            }

            if (isset($DtoItems->meta_tag_characteristic)) {
                $ItemsLanguage->meta_tag_characteristic = $DtoItems->meta_tag_characteristic;
            }

            if (isset($DtoItems->meta_tag_description)) {
                $ItemsLanguage->meta_tag_description = $DtoItems->meta_tag_description;
            }

            if (isset($DtoItems->meta_tag_keyword)) {
                $ItemsLanguage->meta_tag_keyword = $DtoItems->meta_tag_keyword;
            }
            $ItemsLanguage->save();

            foreach ($DtoItems->pricelist as $PriceList) {
                $ItemPriceList = new PriceListItems();
                $ItemPriceList->item_id = $Items->id;

                if (isset($PriceList['pricelist_id'])) {
                    $ItemPriceList->pricelist_id = $PriceList['pricelist_id'];
                }
                if (isset($PriceList['pricelist'])) {
                    $ItemPriceList->price = $PriceList['pricelist'];
                }
                $ItemPriceList->save();
            }

            if (isset($DtoItems->imgAgg)) {
                foreach ($DtoItems->imgAgg as $imgAgg) {
                    if (!is_null($imgAgg['imgName']) && !is_null($imgAgg['imgBase64'])) {
                        if (strpos($DtoItems->imgBase64, '/storage/app/public/images/Attachments/') === false) {
                            Image::make($imgAgg['imgBase64'])->save(static::$dirImageAttachment . $DtoItems->id . $imgAgg['imgName']);
                            $itemAttachment = new ItemAttachments();
                            $itemAttachment->item_id = $DtoItems->id;
                            if (isset($imgAgg['imgName'])) {
                                $itemAttachment->img_attachment = static::getUrlAttachments() . $imgAgg['imgName'];
                            }
                            $itemAttachment->save();
                        }
                    }
                }
            }

            /* END INSERIMENTO USER PHOTO AGGIUNTIVE */

            /*   $CategoriesItems = new CategoryItem();

            if (isset($DtoItems->category_id)) {
                $CategoryId = CategoryLanguage::where('id', $DtoItems->category_id)->first()->category_id;
            } else {
                $CategoryId = "";
            }

            $CategoriesItems->category_id = $CategoryId;

            if (isset($Items->id)) {
                $CategoriesItems->item_id = $Items->id;
            }


            if (isset($idUser)) {
                $CategoriesItems->created_id = $idUser;
            }

            $CategoriesItems->save();*/


            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $Items->id;
    }




    /**
     * insert Item Categories
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertItemCategories(Request $request, int $idUser, int $idLanguage)
    {
        if (is_null($request->idCategories)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }


        $newCategoryItems = new CategoryItem();
        $newCategoryItems->item_id = $request->idItem;
        $newCategoryItems->category_id = $request->idCategories;
        $newCategoryItems->created_id = $idUser;
        $newCategoryItems->save();

        return $newCategoryItems->id;
    }


    /**
     * insert Item Images
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertItemImages(Request $request, int $idUser, int $idLanguage)
    {
        if (is_null($request->idItem)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (strpos($request->imgBase64, '/storage/app/public/images/Items/') === false) {
            Image::make($request->imgBase64)->save(static::$dirImage . $request->item_id . $request->imgName);
            $newItemsPhotos = new ItemPhotos();
            $newItemsPhotos->item_id = $request->idItem;
            $newItemsPhotos->img = static::getUrl() . $request->item_id . $request->imgName;
            $newItemsPhotos->save();
        }

        return $newItemsPhotos->id;
    }


    /**
     * get replace shortcode optional
     * 
     * @param Request $request
     * 
     * @return string
     */
    public static function getOptional(Request $request){
        /*
        <!-- CONTAINER BODY ITEM INFORMATION - PRINT TYPE -->
            <div class="cardInformationItem">
                <div class="row">
                    <div class="col-sm-12 col-md-3">
                        <h3 class="subTitleInformationItem">Opzioni di stampa</h3>
                    </div>
                    <div class="col-sm-12 col-md-3 text-left">
                        <input type="radio" name="printType" value="1">
                        <label for="printType">Stampa a colori</label>
                    </div>
                    <div class="col-sm-12 col-md-6 text-left">
                        <input type="radio" name="printType" value="2">
                        <label for="printType">Senza stampa, bianche</label>
                    </div>
                </div>
            </div>
        <!-- END CONTAINER BODY ITEM INFORMATION - PRINT TYPE -->
        */

        // controllo ed estrazione ordinata delle tipologia optional
        // loop tipologie optional ed estrazione optional con html renderizzato correttamente
        // controllo sul primo campo con inserimento checked di default

        $html = "";
        $oldGruppo = '';
        $nameInput = '';
        $htmlend = '';
        $cheched = true;
        if($request->url != ''){
            $itemLanguage = ItemLanguage::where('link', $request->url)->first();
            if(isset($itemLanguage)){

                /*
                query da ciclare per costruzione html

                select optional_languages.optional_id, type_optional_languages.description as descriptionTipology ,optional_languages.description as descriptionOptional from type_optional
                inner join type_optional_languages on type_optional.id = type_optional_languages.type_optional_id
                inner join optional_typology on type_optional.id = optional_typology.type_optional_id
                inner join optional_languages on optional_typology.optional_id = optional_languages.optional_id
                inner join item_optional on optional_languages.optional_id = item_optional.optional_id

                where item_optional.item_id = 3
                order by type_optional.order
                */

                $itemOptional = ItemOptional::where('item_id', $itemLanguage->item_id)->get();
                if(isset($itemOptional)){
                    //creazione testata html per ogni optional
                    foreach (DB::select(
                    "SELECT optional_languages.optional_id, type_optional_languages.description as descriptiontipology, optional_languages.description as descriptionoptional, optional_languages.img from type_optional
                    inner join type_optional_languages on type_optional.id = type_optional_languages.type_optional_id
                    inner join optional_typology on type_optional.id = optional_typology.type_optional_id
                    inner join optional_languages on optional_typology.optional_id = optional_languages.optional_id
                    inner join optional on optional_languages.optional_id = optional.id
                    inner join item_optional on optional_languages.optional_id = item_optional.optional_id
                    where item_optional.item_id = " . $itemLanguage->item_id . " order by type_optional.order, optional.order"
                    ) as $optional){

                        if ($html != '' && $oldGruppo != '' && $optional->descriptiontipology != $oldGruppo){
                            $html = $html . '</div>';
                            $html = $html . '</div>';
                        }

                        if ($optional->descriptiontipology != $oldGruppo){
                            $cheched = true;
                            $oldGruppo = $optional->descriptiontipology;
                            $html = $html . '<div class="cardInformationItem">';
                            $html = $html . '<div class="row">';
                            $html = $html . '<div class="col-sm-12 col-md-12">';
                            $html = $html . '<h3 class="subTitleInformationItem">' . $optional->descriptiontipology . '</h3>';
                            $html = $html . '</div>';

                            
                        }

                        switch ($optional->descriptiontipology) {
                            case "Opzioni di stampa":
                                $nameInput = 'printType';
                                break;
                            case "Plastificazione":
                                $nameInput = 'printPlastification';
                                break;
                            case "Priorità":
                                $nameInput = 'shipment';
                                break;
                            case "Fronte e Retro":
                                $nameInput = 'side';
                                break;
                            case "Orientamento":
                                $nameInput = 'direction';
                                break;
                            case "Pannelli di stampa":
                                $nameInput = 'printPanel';
                                break;
                        }

                        $html = $html . '<div class="col-sm-12 col-md-4 text-left">';
                        if($cheched != ''){
                            $cheched = false;
                            $html = $html . '&nbsp;<input type="radio" checked name="' . $nameInput . '" value="' . $optional->optional_id . '">';
                        }else{
                            $html = $html . '&nbsp;<input type="radio" name="' . $nameInput . '" value="' . $optional->optional_id . '">';
                        }
                        if(!is_null($optional->img)){
                            $html = $html . '<img src="' . $optional->img . '" class="imgOptional"><br>';
                        }
                        $html = $html . '&nbsp;<label for="' . $nameInput . '"> ' . $optional->descriptionoptional . '</label>';
                        $html = $html . '</div>';

                        
                    }
                    
                    $html = $html . '</div>';
                    $html = $html . '</div>';

                    $htmlReturn = $html;

                }else{
                    $htmlReturn = '';
                }
            }else{
                $htmlReturn = '';
            }
        }else{
            $htmlReturn = '';
        }

        return $htmlReturn;
    }


    /**
     * get replace shortcode supports
     * 
     * @param Request $request
     * 
     * @return string
     */
    public static function getSupports(Request $request){
        $html = "";
        $cont = 0;
        if($request->url != ''){
            $itemLanguage = ItemLanguage::where('link', $request->url)->first();
            if(isset($itemLanguage)){
                $itemSupports = ItemSupport::where('item_id', $itemLanguage->item_id)->get();
                if(isset($itemSupports)){

                    //creazione testata html per ogni optional
                    foreach ($itemSupports as $supports) {
                        $itemSupportsDescription = Supports::where('id', $supports->supports_id)->get()->first();

                        if(isset($itemSupportsDescription)){
                            $cont = $cont + 1;
                            $html = $html . '<div class="col-sm-12 col-md-4 text-left">';
                            if($cont = 1){
                                $html = $html . '<input type="radio" name="printMaterial" checked value="' . $supports->supports_id . '">';
                            }else{
                                $html = $html . '<input type="radio" name="printMaterial" value="' . $supports->supports_id . '">';
                            }
                            $html = $html . '&nbsp;<label for="printMaterial"> ' . $itemSupportsDescription->description . '</label>';
                            $html = $html . '</div>';
                        }
                    }

                    if($html != ''){
                        $htmlReturn = '<div class="cardInformationItem"><div class="row"><div class="col-sm-12 col-md-12"><h3 class="subTitleInformationItem">Seleziona materiale</h3></div>' . $html . '</div></div>';
                    }else{
                        $htmlReturn = '';
                    }
                }else{
                    $htmlReturn = '';
                }
            }else{
                $htmlReturn = '';
            }
        }else{
            $htmlReturn = '';
        }

        return $htmlReturn;
    }

    /**
     * getPriceItemSupport
     * 
     * @param Request request
     * 
     * @return int
     */
    public static function getPriceItemSupport(Request $request)
    {
       
        /* calcolo prezzo  secondo il seguente Json passato webapi
        $request = {
            "url": "/harley-benton-hbo-850-sb-modello-ovation-18-16",
            "A": 3,
            "B": 3,
            "H": 6,
            "PZ": 50,
            "supportId": 1,
            "optional": [
                {
                    "idType": 1,
                    "idOptional": 8
                },
                {
                    "idType": 2,
                    "idOptional": 7
                }
            ]
        }
        */
        $przBaseSupporto = 0;
        $przBaseOptional = 0;
        $przFinal = 0;

        if($request->url != ''){
            $itemLanguage = ItemLanguage::where('link', $request->url)->first();
            if(isset($itemLanguage)){
                $idItem = $itemLanguage->item_id;
                if($request->supportId>0){
                    $support = $request->supportId;
                }

                if ($request->B > 0){
                    $base = ($request->A * 2) + ($request->B * 2);
                    $altezza = ($request->H) + ($request->B * 2);
                    //$area = $base * $altezza;
                    $area = $base * $altezza * $request->PZ;
                }else{
                    $base = $request->A;
                    $altezza = $request->H;
                    $area = $base * $altezza * $request->PZ;
                }
                
                $supportPrices = SupportsPrices::where('supports_id', $support)->where('from', '<=', $area)->where('to', '>', $area)->get()->first();
                if(isset($supportPrices)){
                    //calcolo prezzo in base al supporto, scaglione prezzo area impostato nella sezione supporti-prezzi
                    $przBaseSupporto = $supportPrices->price;
                }

                foreach ($request->optional as $optional) {
                    $itemOptional = ItemOptional::where('item_id', $idItem)->where('optional_id', $optional['idOptional'])->get()->first();
                    if(isset($itemOptional)){
                        $variationType = $itemOptional->variation_type;
                        $variationPrice = $itemOptional->variation_price;
                        //controllo se la variazione del prezzo è in euro o in %
                        if($variationType == '€'){
                            //controllo se le variabili per il supporto sono valorizzate, altrimenti il valore viene solo aggiunto senza il calcolo
                            if(!is_null($itemOptional->support_variation_value) && !is_null($itemOptional->support_prameter) && !is_null($itemOptional->um_support_variation_id)){
                                $supportValueEach = $itemOptional->support_variation_value;
                                switch ($itemOptional->support_prameter) {
                                    case "Area":
                                        $supportPriceArea = $area / $supportValueEach;
                                        $przBaseOptional = $przBaseOptional + ($supportPriceArea * $variationPrice);
                                        break;
                                        
                                    case "Lunghezza":
                                        $supportPriceArea = $request->A / $supportValueEach;
                                        $przBaseOptional = $przBaseOptional + ($supportPriceArea * $variationPrice);
                                        break;
                                
                                    case "Altezza":
                                        $supportPriceArea = $request->H / $supportValueEach;
                                        $przBaseOptional = $przBaseOptional + ($supportPriceArea * $variationPrice);
                                        break;  
                                        
                                    case "Profondita":
                                        $supportPriceArea = $request->B / $supportValueEach;
                                        $przBaseOptional = $przBaseOptional + ($supportPriceArea * $variationPrice);
                                        break;
                                }
                            }else{
                                $przBaseOptional = $przBaseOptional + $variationPrice;
                            }
                        }else{
                            if(!is_null($itemOptional->support_variation_value) && !is_null($itemOptional->support_prameter) && !is_null($itemOptional->um_support_variation_id)){
                               // cosa si fa in questo caso??
                               // non si può aumentare il valore in % se no ogni volta viene aggiunto sempre di più tenedo conto del valore precedente
                            }else{
                                $variationPercentage = ($przBaseOptional * $variationPrice) / 100;
                                $przBaseOptional = $przBaseOptional + $variationPercentage;
                            }
                        }
                    }
                }
                if ($request->B > 0){
                    //$przFinal = ($przBaseOptional + $przBaseSupporto) * $request->PZ;
                    $przFinal = $przBaseOptional + $przBaseSupporto;
                }else{
                    $przFinal = $przBaseOptional + $przBaseSupporto;
                }
            }
        }

        if (strpos($przFinal, '.') === false) {
            $przFinal = $przFinal + ".00";
            $przFinal = str_replace('.', ',', $przFinal);

        }else{
            $przFinal = round($przFinal, 2);
            $przFinal = str_replace('.', ',', $przFinal);
        }

        
        
        // controllo per non eseguire il codice se si stanno configurando deegli articoli diversi dalle scatole
        if($request->url == '/scatola-astuccio-lineare'){
            //IMPORTANTE !!!! CONTROLLO SULL'ESISTENZA DEL FILE COSI DA NON EFFETTUARE LA CHIAMATA PIù VOLTE INUTILMENTE
            if (file_exists(static::$dirImagePdfModule . 'result/' . str_replace('.', '-', $request->A) . str_replace('.', '-', $request->B) . str_replace('.', '-', $request->H) . '.pdf')) {
                
                $filePdf = "/storage/images/Items/pdf/result/" . str_replace('.', '-', $request->A) . str_replace('.', '-', $request->B) . str_replace('.', '-', $request->H) . ".pdf";
                $DtoItems = new DtoItems();
                $DtoItems->priceFinal = $przFinal;
                //inserire percorso pdf generale, generato dall'unione tra la prima pagina di pdf testata insieme al tracciato pdf generato dalle dimensioni
                $DtoItems->filePdf = $filePdf;


            }else{
                /* generate image dieline (fustella da scaricare) -- https://easypackmaker.com/generator/api */
                
                //per i parametri impostati il token sarà sempre questo 
                //48ef054a1459d5f4e82d81ff48e2a5a1976af82c448c80fd2402dc6a9afe16ea
                // se cambia il tipo di scatola andrà rigenerato facendo la conversione della stringa: TIPOLOGIADICARTA1001A_NmEJ36xC5zt7NWfilippo95
                
                /*$curl = curl_init();
                curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://easypackmaker.com/generator/api',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "UserName":"filippo95",
                    "Token":"48ef054a1459d5f4e82d81ff48e2a5a1976af82c448c80fd2402dc6a9afe16ea",
                    "OrderId":"1001",
                    "ModelName":"A20_20_03_01",
                    "ModelParams":
                    {
                        "L":"' . $request->A . '",
                        "W":"' . $request->B . '",
                        "D":"' . $request->H . '",
                        "Th":"0.15",
                        "Units":"mm"
                    },
                    "ModelOptions":
                    {
                        "Lock" : true,
                        "DimensionType":"Crease",
                        "KnifeInfo":false,
                        "GlueZone":true
                    }
                }',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'Cookie: PHPSESSID=bnhigjdmm5vnuokmi3nlj5fi7i'
                ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);

                $json = json_decode($response, true);

                if($json['ErrorCode'] == 0){
                    //making pdf file with the dieline of the box
                    $bin = base64_decode($json['Model'], true);
                    file_put_contents(static::$dirImagePdfModule . str_replace('.', '-', $request->A) . str_replace('.', '-', $request->B) . str_replace('.', '-', $request->H) . '.pdf', $bin);

                    
                    if (!file_exists(static::$dirImagePdfModule .'result/'. str_replace('.', '-', $request->A) . str_replace('.', '-', $request->B) . str_replace('.', '-', $request->H) . '.pdf')) {
                        if (!file_exists(static::$dirImagePdfModule .'Images/'. str_replace('.', '-', $request->A) . str_replace('.', '-', $request->B) . str_replace('.', '-', $request->H) . '.jpg')) {
                            
                            //predisposizione variabili pdf per generare l'immagine
                            $pdfImageFile2Path = $_SERVER['DOCUMENT_ROOT'] . "/storage/images/Items/pdf/". str_replace('.', '-', $request->A) . str_replace('.', '-', $request->B) . str_replace('.', '-', $request->H) . '.pdf';
                            //nuovo percorso in cui sarà salvata l'immagine del pdf
                            $pdfImage2Path = $_SERVER['DOCUMENT_ROOT'] . "/storage/images/Items/pdf/Images/" . str_replace('.', '-', $request->A) . str_replace('.', '-', $request->B) . str_replace('.', '-', $request->H) . '.jpg';
        
                            //converione pdf in immagine e salvataggio all'interno della cartella pdf 
                            $pdfImage = new PdfToImage($pdfImageFile2Path);
                            $pdfImage->saveImage($pdfImage2Path);
                        }
                        
                        $dtoPdfExportArray = collect();
                        $dtoPdfExport = new DtoItems();
                        $dtoPdfExport->A = $request->A;
                        $dtoPdfExport->B = $request->B;
                        $dtoPdfExport->H = $request->H;

                        $width = ($request->A * 2) + ($request->B * 2);
                        $percentageWidh = ($width * 17)/100;
                        $finalWidth = $width + $percentageWidh;
                        
                        $height = ($request->H) + ($request->B * 2);
                        $ptConversionWidth = ($width + 19)*3.5;
                        $ptConversionHeight = ($height + 19)*5;

                        $dtoPdfExport->width = $width + 19;
                        $dtoPdfExport->height = $request->H / 2;
                        //$dtoPdfExport->imgPdf = '/storage/images/Items/pdf/Images/' . str_replace('.', '-', $request->A) . str_replace('.', '-', $request->B) . str_replace('.', '-', $request->H) . '.jpg';
                        $dtoPdfExport->imgPdf = 'data:image/svg+xml;base64,' . $json['Preview'];
                        //$dtoPdfExport->imgPdf = '/storage/images/Items/svg/' . str_replace('.', '-', $request->A) . str_replace('.', '-', $request->B) . str_replace('.', '-', $request->H) . '.svg';
                        $dtoPdfExportArray->push($dtoPdfExport);
                        
                        view()->share('itemPdfDimension', $dtoPdfExport);
                        view()->share('dtoPdfExportArray', $dtoPdfExportArray);  
        
                        $pdfFileDownload = PDF::loadView('pdfviewlineaprint', $dtoPdfExportArray);
                        //$pdfFileDownload->setPaper('a3', 'landscape')->save($_SERVER['DOCUMENT_ROOT'] . '/storage/images/Items/pdf/result/' . str_replace('.', '-', $request->A) . str_replace('.', '-', $request->B) . str_replace('.', '-', $request->H) . '.pdf');
                        $pdfFileDownload->setPaper(array(0,0,$ptConversionWidth, $ptConversionHeight))->save($_SERVER['DOCUMENT_ROOT'] . '/storage/images/Items/pdf/result/' . str_replace('.', '-', $request->A) . str_replace('.', '-', $request->B) . str_replace('.', '-', $request->H) . '.pdf');
        
                        $filePdf = "/storage/images/Items/pdf/result/" . str_replace('.', '-', $request->A) . str_replace('.', '-', $request->B) . str_replace('.', '-', $request->H) . ".pdf";
        
                    }else{
                        $filePdf = "/storage/images/Items/pdf/result/" . str_replace('.', '-', $request->A) . str_replace('.', '-', $request->B) . str_replace('.', '-', $request->H) . ".pdf";
                    }
        
                    $DtoItems = new DtoItems();
                    $DtoItems->priceFinal = $przFinal;
                    //inserire percorso pdf generale, generato dall'unione tra la prima pagina di pdf testata insieme al tracciato pdf generato dalle dimensioni
                    $DtoItems->filePdf = "/storage/images/Items/pdf/result/" . str_replace('.', '-', $request->A) . str_replace('.', '-', $request->B) . str_replace('.', '-', $request->H) . ".pdf";
                      
                }else{
                    $DtoItems = new DtoItems();
                    $DtoItems->priceFinal = $przFinal;
                    $DtoItems->filePdf = '';
                    
                }*/

                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://templatemaker.pythonanywhere.com/?UNITS=mm&L=' . $request->A . '&W=' . $request->B . '&H=' . $request->H . '&MARGIN=10&PAGEPRESET=Auto&PAGEWIDTH=480&PAGEHEIGHT=330&RESOLUTION=300&REQUEST=PDF&THUMB=0&TUCK=15&GLUE=15&GLUEANGLE=80&T=0,2&R=5&REGISTRATION=none&MARK=6&BLEED=2&OVERCUT=0&DASH=0&GAP=0&FILMIN=0&NESTING=auto&COLS=1&ROWS=1&COLSPACE=5&ROWSPACE=5&MODEL=cardbox&CUSTOMER=c82468626',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                ));

                $response = curl_exec($curl);

                $pdfImageFile2Path = static::$dirImagePdfModule . 'result/' . str_replace('.', '-', $request->A) . str_replace('.', '-', $request->B) . str_replace('.', '-', $request->H) . '.pdf';

                file_put_contents($pdfImageFile2Path, $response);

                curl_close($curl);

                $DtoItems = new DtoItems();
                $DtoItems->priceFinal = $przFinal;
                $DtoItems->filePdf = "/storage/images/Items/pdf/result/" . str_replace('.', '-', $request->A) . str_replace('.', '-', $request->B) . str_replace('.', '-', $request->H) . ".pdf";
                
                /* end generate image dieline (fustella da scaricare) */ 
            }
        }else{
            $DtoItems = new DtoItems();
            $DtoItems->priceFinal = $przFinal;
            $DtoItems->filePdf = '';
        }
        

        return $DtoItems;       
    }


       /**
     * getPriceItemSupportAndAddParams
     * 
     * @param Request request
     * 
     * @return int
     */
    public static function getPriceItemSupportAndAddParams(Request $request)
    {
        $przBaseSupporto = 0;
        $przBaseOptional = 0;
        $przFinal = 0;

        if($request->url != ''){
            $itemLanguage = ItemLanguage::where('link', $request->url)->first();
            if(isset($itemLanguage)){
                $idItem = $itemLanguage->item_id;
                if($request->supportId>0){
                    $support = $request->supportId;
                }

                if ($request->B > 0){
                    $base = ($request->A * 2) + ($request->B * 2);
                    $altezza = ($request->H) + ($request->B * 2);
                }else{
                    $base = $request->A;
                    $altezza = $request->H;
                }
                
                $area = $base * $altezza * $request->PZ;
                $supportPrices = SupportsPrices::where('supports_id', $support)->where('from', '<=', $area)->where('to', '>', $area)->get()->first();
                if(isset($supportPrices)){
                    //calcolo prezzo in base al supporto, scaglione prezzo area impostato nella sezione supporti-prezzi
                    $przBaseSupporto = $supportPrices->price;
                }

                foreach ($request->optional as $optional) {
                    $itemOptional = ItemOptional::where('item_id', $idItem)->where('optional_id', $optional['idOptional'])->get()->first();
                    if(isset($itemOptional)){
                        $variationType = $itemOptional->variation_type;
                        $variationPrice = $itemOptional->variation_price;
                        //controllo se la variazione del prezzo è in euro o in %
                        if($variationType =='€'){
                            //controllo se le variabili per il supporto sono valorizzate, altrimenti il valore viene solo aggiunto senza il calcolo
                            if(!is_null($itemOptional->support_variation_value) && !is_null($itemOptional->support_prameter) && !is_null($itemOptional->um_support_variation_id)){
                                $supportValueEach = $itemOptional->support_variation_value;
                                switch ($itemOptional->support_prameter) {
                                    case "Area":
                                        $supportPriceArea = $area / $supportValueEach;
                                        $przBaseOptional = $przBaseOptional + ($supportPriceArea * $variationPrice);
                                        break;
                                        
                                    case "Lunghezza":
                                        $supportPriceArea = $request->A / $supportValueEach;
                                        $przBaseOptional = $przBaseOptional + ($supportPriceArea * $variationPrice);
                                        break;
                                
                                    case "Altezza":
                                        $supportPriceArea = $request->H / $supportValueEach;
                                        $przBaseOptional = $przBaseOptional + ($supportPriceArea * $variationPrice);
                                        break;  
                                        
                                    case "Profondita":
                                        $supportPriceArea = $request->B / $supportValueEach;
                                        $przBaseOptional = $przBaseOptional + ($supportPriceArea * $variationPrice);
                                        break;
                                }
                            }else{
                                $przBaseOptional = $przBaseOptional + $variationPrice;
                            }
                        }else{
                            if(!is_null($itemOptional->support_variation_value) && !is_null($itemOptional->support_prameter) && !is_null($itemOptional->um_support_variation_id)){
                               //cosa si fa in questo caso??
                               // non si può aumentare il valore in % se no ogni volta viene aggiunto sempre di più tenedo conto del valore precedente
                            }else{
                                $variationPercentage = ($przBaseOptional * $variationPrice) / 100;
                                $przBaseOptional = $przBaseOptional + $variationPercentage;
                            }
                        }
                    }
                }
                $przFinal = ($przBaseOptional + $przBaseSupporto) ;
            }
        }
        if (strpos($przFinal, '.') === false) {
            $przFinal = $przFinal + ".00";
            $przFinal = str_replace('.', ',', $przFinal);

        }else{
            $przFinal = round($przFinal, 2);
            $przFinal = str_replace('.', ',', $przFinal);
        }


         DB::beginTransaction();
        try {
            $itemLanguage = ItemLanguage::where('link', $request->url)->get()->first();
            $idItem = $itemLanguage->item_id;
            $cartDetailOnDb = CartDetail::where('cart_id', $request->idCart)->where('item_id', $idItem)->get()->first();
            foreach ($request->optional as $optional) {
                $CartDetailOptional = new CartDetailOptional();
                $CartDetailOptional->cart_detail_id = $cartDetailOnDb->id;
                $CartDetailOptional->optional_id = $optional['idOptional'];
                $CartDetailOptional->save();
            }

            $CartDetail = CartDetail::where('id', $cartDetailOnDb->id)->get()->first();
            $CartDetail->width = $request->A;
            $CartDetail->depth = $request->B;
            $CartDetail->height = $request->H;
            $CartDetail->quantity = $request->PZ;
            $CartDetail->support_id = $request->supportId;
            $CartDetail->net_amount = str_replace(',', '.', $przFinal);
            $CartDetail->vat_amount = (str_replace(',', '.', $przFinal)*22/100);
            $CartDetail->total_amount = (str_replace(',', '.', $przFinal) + $CartDetail->vat_amount);
            //$CartDetail->total_amount = str_replace(',', '.', $przFinal);
            $CartDetail->save();

                $sum_net_amount = DB::table('carts_details')
                ->select('cart_id', DB::raw('SUM(net_amount) as net_amount'))
                ->where('carts_details.cart_id', '=', $CartDetail->cart_id)
                ->groupBy('cart_id')
                ->get();

                $sum_total_amount = DB::table('carts_details')
                ->select('cart_id', DB::raw('SUM(total_amount) as total_amount'))
                ->where('carts_details.cart_id', '=', $CartDetail->cart_id)
                ->groupBy('cart_id')
                ->get();

                $sum_vat_amount = DB::table('carts_details')
                ->select('cart_id', DB::raw('SUM(vat_amount) as vat_amount'))
                ->where('carts_details.cart_id', '=', $CartDetail->cart_id)
                ->groupBy('cart_id')
                ->get();

               


            $Cart = Cart:: where('id', $CartDetail->cart_id)->first();
            $Cart->shipment_amount = 0;
            $Cart->net_amount = $sum_net_amount[0]->net_amount;
            //str_replace('.',',',substr($sum_net_amount[0]->net_amount, 0, -2));  
            $Cart->vat_amount = $sum_vat_amount[0]->vat_amount;
            //str_replace('.',',',substr($sum_vat_amount[0]->vat_amount, 0, -2));
            $Cart->total_amount = $sum_total_amount[0]->total_amount;
            //str_replace('.',',',substr($sum_total_amount[0]->total_amount, 0, -2));
            //$Cart->total_amount = str_replace(',', '.', $przFinal);

            $Cart->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $CartDetailOptional->cart_detail_id;
     
    }


    /**
     * Clone
     * 
     * @param int id
     * @param int idFunctionality
     * @param int idLanguage
     * @param int idUser
     * 
     * @return int
     */
    public static function clone(int $id, int $idFunctionality,  $idUser, int $idLanguage)
    {
        
        //FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $Items = Item::find($id);
        $ItemLanguage = ItemLanguage::where('item_id', $id)->first();
        $CategoryItem = CategoryItem::where('item_id', $id)->first();

        if (is_null($Items)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        DB::beginTransaction();

        try {
            #region Items
            $newItems = new Item();
            $newItems->internal_code = $Items->internal_code;
            $newItems->online = $Items->online;
            $newItems->unit_of_measure_id = $Items->unit_of_measure_id;
            $newItems->producer_id = $Items->producer_id;
            $newItems->price = $Items->price;
            $newItems->vat_type_id = $Items->vat_type_id;
            $newItems->height = $Items->height;
            $newItems->width = $Items->width;
            $newItems->depth = $Items->depth;
            $newItems->volume = $Items->volume;
            $newItems->weight = $Items->weight;
            $newItems->enabled_from = $Items->enabled_from;
            $newItems->enabled_to = $Items->enabled_to;
            $newItems->img = $Items->img;
            $newItems->qta_min = $Items->qta_min;
            $newItems->shipment_request = $Items->shipment_request;
            $newItems->availability = $Items->availability;
            $newItems->commited_customer = $Items->commited_customer;
            $newItems->ordered_supplier = $Items->ordered_supplier;
            $newItems->stock = $Items->stock;
            $newItems->sell_if_not_available = $Items->sell_if_not_available;
            $newItems->pieces_for_pack = $Items->pieces_for_pack;
            $newItems->unit_of_measure_id_packaging = $Items->unit_of_measure_id_packaging;
            $newItems->qta_max = $Items->qta_max;
            $newItems->digital = $Items->digital;
            $newItems->img_digital = $Items->img_digital;
            $newItems->limited = $Items->limited;
            $newItems->day_of_validity = $Items->day_of_validity;
            $newItems->announcements = $Items->announcements;
            $newItems->announcements_from = $Items->announcements_from;
            $newItems->announcements_to = $Items->announcements_to;

            $newItems->save();

            $newItemLanguage = new ItemLanguage();
            $newItemLanguage->language_id = $ItemLanguage->language_id;
            $newItemLanguage->item_id = $newItems->id;
            $newItemLanguage->description = $ItemLanguage->description . ' - Copy';
            $newItemLanguage->link = $ItemLanguage->link. ' - Copy';
            $newItemLanguage->meta_tag_characteristic = $ItemLanguage->meta_tag_characteristic;
            $newItemLanguage->meta_tag_title = $ItemLanguage->meta_tag_title . ' - Copy';
            $newItemLanguage->meta_tag_description = $ItemLanguage->meta_tag_description;
            $newItemLanguage->meta_tag_keyword = $ItemLanguage->meta_tag_keyword;
            $newItemLanguage->save();
            if(isset($CategoryItem)){
                $newCategoriesItems = new CategoryItem();
                $newCategoriesItems->category_id = $CategoryItem->category_id;
                $newCategoriesItems->item_id = $newItems->id;
                $newCategoriesItems->created_id = $idUser;
                $newCategoriesItems->save();
            }

            #endregion Items

            DB::commit();

            return $newItems->id;
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion INSERT
    public static function insertTranslate(Request $request)
    {
        $newlanguageItem = new ItemLanguage();
        $newlanguageItem->item_id = $request->idItem;
        $newlanguageItem->description = $request->description;
        $newlanguageItem->meta_tag_characteristic = $request->meta_tag_characteristic;
        $newlanguageItem->link = $request->link;
        $newlanguageItem->language_id = $request->language_id;
        $newlanguageItem->save();
    }

    #region UPDATE TRANSLATE

    public static function updateTranslate(Request $request)
    {
        $itemTranslationonDb = ItemLanguage::where('id', $request->id)->first();
        DB::beginTransaction();

        try {
            if (isset($itemTranslationonDb)) {
                if (isset($request->idItem)) {
                    $itemTranslationonDb->item_id = $request->idItem;
                }
                if (isset($request->description)) {
                    $itemTranslationonDb->description = $request->description;
                }
                if (isset($request->meta_tag_characteristic)) {
                    $itemTranslationonDb->meta_tag_characteristic = $request->meta_tag_characteristic;
                }
                if (isset($request->link)) {
                    $itemTranslationonDb->link = $request->link;
                }
                if (isset($request->language_id)) {
                    $itemTranslationonDb->language_id = $request->language_id;
                }
                $itemTranslationonDb->save();
            }
            /*else {
                $itemTranslationonDb = new ItemLanguage();
                $itemTranslationonDb->description = $request->description;
                $itemTranslationonDb->item_id = $request->idItem;
                $itemTranslationonDb->language_id = $request->language_id;
                $itemTranslationonDb->meta_tag_characteristic = $request->meta_tag_characteristic;
                $itemTranslationonDb->link = $request->link;
                $itemTranslationonDb->save();
            }*/


            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return  $request->id;
    }

    #region UPDATE NO AUTH
    public static function updateNoAuth(Request $request, int $idUser)
    {

        $itemsOnDb = Item::find($request->id);
        $languageItemsOnDb = ItemLanguage::where('item_id', $request->id)->first();

        DB::beginTransaction();

        try {
            if (isset($request->price)) {
                $itemsOnDb->price = $request->price;
            }

            if (isset($request->available)) {
                $itemsOnDb->available = $request->available;
            }

            if (isset($request->new)) {
                $itemsOnDb->new = $request->new;
            }

            if (isset($request->external_link)) {
                $itemsOnDb->external_link = $request->external_link;
            }

            if (isset($request->free_shipment)) {
                $itemsOnDb->free_shipment = $request->free_shipment;
            }

            if (isset($request->producer_id)) {
                $itemsOnDb->producer_id = $request->producer_id;
            }

            if (!is_null($request->imgName)) {
                Image::make($request->img)->save(static::$dirImage . $request->user_id . $request->imgName);
                $itemsOnDb->img = static::getUrl() . $request->user_id . $request->imgName;
            }


            if (isset($request->$idUser)) {
                $itemsOnDb->updated_id = $request->$idUser;
            }

            $itemsOnDb->save();

            if (isset($request->id)) {
                $languageItemsOnDb->item_id = $request->id;
            }

            if (isset($request->description)) {
                $languageItemsOnDb->description = $request->description;
            }

            if (isset($request->meta_tag_characteristic)) {
                $languageItemsOnDb->meta_tag_characteristic = $request->meta_tag_characteristic;
            }

            if (isset($request->$idUser)) {
                $languageItemsOnDb->updated_id = $request->$idUser;
            }

            $languageItemsOnDb->language_id = $request->idLanguage;

            $languageItemsOnDb->save();

            $categoryItem = CategoryItem::where('item_id', $request->id)->first();

            if (isset($categoryItem)) {
                $categoryItem->delete();
                $newCategoryItem = new CategoryItem();
                $newCategoryItem->category_id = $request->category_id;
                $newCategoryItem->item_id = $request->id;
                $newCategoryItem->save();
            }


            /* UPDATE IMG AGGIUNTIVE */
            $itemPhotos = ItemPhotos::where('item_id', $request->id)->get();
            if (isset($itemPhotos)) {
                ItemPhotos::where('item_id', $request->id)->delete();
            }

            if (isset($request->imgAgg)) {
                foreach ($request->imgAgg as $itemImgAgg) {
                    for ($i = 0; $i < 10; $i++) {
                        if (array_key_exists('imgName' . $i, $itemImgAgg)) {
                            if (!is_null($itemImgAgg['imgName' . $i]) && !is_null($itemImgAgg['imgBase64' . $i])) {
                                if (strpos($request->imgBase64, '/storage/app/public/images/Items/') === false) {
                                    Image::make($itemImgAgg['imgBase64' . $i])->save(static::$dirImage . $request->id . $i . $itemImgAgg['imgName' . $i]);
                                    $ItemPhoto = new ItemPhotos();
                                    $ItemPhoto->item_id = $request->id;
                                    $ItemPhoto->img = static::getUrl() . $request->id . $i . $itemImgAgg['imgName' . $i];
                                    $ItemPhoto->save();
                                }
                            } else {
                                if (!is_null($itemImgAgg['imgName' . $i]) && is_null($itemImgAgg['imgBase64' . $i])) {
                                    $ItemPhoto = new ItemPhotos();
                                    $ItemPhoto->item_id = $request->id;
                                    $ItemPhoto->img = $itemImgAgg['imgName' . $i];
                                    $ItemPhoto->save();
                                }
                            }
                        }
                    }
                }
            }
            /* END UPDATE IMG AGGIUNTIVE */


            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return  $request->id;
    }

    #region UPDATE

    /**
     * update Online Item
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateOnlineItem(Request $request)
    {
        $itemsOnDb = Item::find($request->idItem);

        if (isset($itemsOnDb)) {
            $itemsOnDb->online = !$itemsOnDb->online;
            $itemsOnDb->save();
        } else {
             $itemsOnDb->online = true;
             $itemsOnDb->save();
        }
    }



  /**
     * updateOnlineItemCheck
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateOnlineItemCheck(Request $request)
    {
        $itemsOnDb = Item::find($request->id);

        if (isset($itemsOnDb)) {
            $itemsOnDb->online = !$itemsOnDb->online;
            $itemsOnDb->save();
        } else {
             $itemsOnDb->online = true;
             $itemsOnDb->save();
            }
    }


     /**
     * update announcements ItemCheck
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateAnnouncementsItemCheck(Request $request)
    {
        $itemsOnDb = Item::find($request->id);

        if (isset($itemsOnDb)) {
            $itemsOnDb->announcements = !$itemsOnDb->announcements;
            $itemsOnDb->save();
        } else {
             $itemsOnDb->announcements = true;
             $itemsOnDb->save();
            }
    }

    /**
     * update Promotion ItemCheck
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updatePromotionItemCheck(Request $request)
    {
        $itemsOnDb = Item::find($request->id);

        if (isset($itemsOnDb)) {
            $itemsOnDb->promotion = !$itemsOnDb->promotion;
            $itemsOnDb->save();
        } else {
             $itemsOnDb->promotion = true;
             $itemsOnDb->save();
            }
    }


    /**
     * update Image Name
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateImageName(Request $request)
    {
        $itemsOnDb = Item::find($request->idItem);

        if (isset($itemsOnDb)) {
            if ($request->imgBase64 == '') {
                $itemsOnDb->img = $request->imgName;
                $itemsOnDb->save();
            } else {
                Image::make($request->imgBase64)->save(static::$dirImage . $request->imgName);
                $itemsOnDb->img = static::getUrl() . $request->imgName;
                $itemsOnDb->save();
            }
        }
    }

    /**
     * update Order Categories Item Table
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateOrderCategoriesItemTable(Request $request)
    {
        $CategoryItemOnDb = CategoryItem::where('item_id', $request->id)->where('category_id', $request->category_id)->first();

        if (isset($CategoryItemOnDb)) {
            $CategoryItemOnDb->order = $request->order;
            $CategoryItemOnDb->save();
        }
    }

    


    /**
     * update Image Name
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateItemImages(Request $request)
    {
        $itemsPhotosOnDb = ItemPhotos::find($request->idItemPhotos);

        if (isset($itemsPhotosOnDb)) {
            $itemsPhotosOnDb->img = static::getUrl() . $request->imgName;
            $itemsPhotosOnDb->save();
        }
    }


    /**
     * update Order Imame Photos
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateOrderImamePhotos(Request $request)
    {
        $itemsPhotosOnDb = ItemPhotos::find($request->id);

        if (isset($itemsPhotosOnDb)) {
            $itemsPhotosOnDb->order = $request->order;
            $itemsPhotosOnDb->save();
        }
    }

    /**
     * update Item Check Association
     * 
     * @param Request request
     * @param int idLanguage
     */

    public static function updateItemCheckAssociation(Request $request)
    {
        $items = Item::find($request->idItem);

        if (isset($items)) {
            $items->image_code_association = $request->checkAssociationCodeImage;
            $items->save();
        }
    }


    /**
     * Update
     * 
     * @param Request DtoCategories
     * @param int idLanguage
     */

    public static function update(Request $DtoItems)
    {

        static::_validateData($DtoItems, $DtoItems->idLanguage, DbOperationsTypesEnum::UPDATE);

        $itemsOnDb = Item::find($DtoItems->id);
        $languageItemsOnDb = ItemLanguage::where('item_id', $DtoItems->id)->first();
        $CategoryItemOnDb = CategoryItem::where('item_id', $DtoItems->id)->first();

        DB::beginTransaction();
        try {
            $itemsOnDb->internal_code = $DtoItems->internal_code;
            $itemsOnDb->unit_of_measure_id = $DtoItems->unit_of_measure_id;
            $itemsOnDb->online = $DtoItems->online;
            $itemsOnDb->producer_id = $DtoItems->producer_id;
            $itemsOnDb->vat_type_id = $DtoItems->vat_type_id;
            if($DtoItems->base_price!= ''){
                $itemsOnDb->price = str_replace(',', '.',  $DtoItems->base_price);
            }else{
                $itemsOnDb->price = 0;
            }
            $itemsOnDb->height = $DtoItems->height;
            $itemsOnDb->width = $DtoItems->width;
            $itemsOnDb->depth = $DtoItems->depth;
            $itemsOnDb->volume = $DtoItems->volume;
            $itemsOnDb->weight = $DtoItems->weight;
            $itemsOnDb->announcements = $DtoItems->announcements;
            $itemsOnDb->announcements_from = $DtoItems->announcements_from;
            $itemsOnDb->announcements_to = $DtoItems->announcements_to;

            if (!is_null($DtoItems->imgName)) {
                Image::make($DtoItems->img)->save(static::$dirImage . $DtoItems->imgName);
                $itemsOnDb->img = static::getUrl() . $DtoItems->imgName;
            }

            if (!is_null($DtoItems->imgName)) {
                Image::make($DtoItems->img_attachment)->save(static::$dirImageAttachment . $DtoItems->imgName);
                $itemsOnDb->img_attachment = static::getUrlAttachments() . $DtoItems->imgName;
            }

            if (!is_null($DtoItems->enabled_from)) {
                $itemsOnDb->enabled_from = $DtoItems->enabled_from;
            }
            if (!is_null($DtoItems->enabled_to)) {
                $itemsOnDb->enabled_to = $DtoItems->enabled_to;
            }

            $itemsOnDb->enabled_from = $DtoItems->enabled_from;
            $itemsOnDb->enabled_to = $DtoItems->enabled_to;
            $itemsOnDb->qta_min = $DtoItems->qta_min;
            $itemsOnDb->shipment_request = $DtoItems->shipment_request;
            $itemsOnDb->available = $DtoItems->available;
            $itemsOnDb->commited_customer = $DtoItems->commited_customer;
            $itemsOnDb->ordered_supplier = $DtoItems->ordered_supplier;
            $itemsOnDb->stock = $DtoItems->stock;
            $itemsOnDb->producer_id = $DtoItems->producer_id;
            $itemsOnDb->sell_if_not_available = $DtoItems->sell_if_not_available;
            $itemsOnDb->pieces_for_pack = $DtoItems->pieces_for_pack;
            $itemsOnDb->unit_of_measure_id_packaging = $DtoItems->unit_of_measure_id_packaging;
            $itemsOnDb->qta_max = $DtoItems->qta_max;
            $itemsOnDb->digital = $DtoItems->digital;
            $itemsOnDb->limited = $DtoItems->limited;
            $itemsOnDb->day_of_validity = $DtoItems->day_of_validity;

         /*if (!is_null($DtoItems->imgName)) {
                Image::make($DtoItems->img_digital)->save(static::$dirImageDigital . $DtoItems->imgName);
                $itemsOnDb->img_digital = static::getUrlDigital() . $DtoItems->imgName;
            }*/

              if (!is_null($DtoItems->imgName)) {
                $data = base64_decode(str_replace('data:application/pdf;base64,', '', $DtoItems->img_digital));
                file_put_contents(static::getUrlDigital() . $DtoItems->imgName, $data);
                }

            $itemsOnDb->delivery_time_if_available = $DtoItems->delivery_time_if_available;
            $itemsOnDb->delivery_time_if_out_stock = $DtoItems->delivery_time_if_out_stock;
            $itemsOnDb->additional_delivery_costs = $DtoItems->additional_delivery_costs;
            $itemsOnDb->save();

            $languageItemsOnDb->language_id = $DtoItems->idLanguage;
            $languageItemsOnDb->description = $DtoItems->description;
            $languageItemsOnDb->meta_tag_characteristic = $DtoItems->meta_tag_characteristic;
            $languageItemsOnDb->link = $DtoItems->meta_tag_link;
            $languageItemsOnDb->meta_tag_description = $DtoItems->meta_tag_description;
            $languageItemsOnDb->meta_tag_title = $DtoItems->meta_tag_title;
            $languageItemsOnDb->meta_tag_keyword = $DtoItems->meta_tag_keyword;
            $languageItemsOnDb->save();

            $IdPriceList = PriceListItems::where('item_id', $DtoItems->id)->get();
            if (isset($IdPriceList)) {
                PriceListItems::where('item_id', $DtoItems->id)->delete();
            }

            foreach ($DtoItems->pricelist as $PriceList) {
              //  if (!is_null($PriceList['pricelist']) && !is_null($PriceList['pricelist_id']) && $PriceList['pricelist_id'] != 'null' && $PriceList['pricelist'] != 'null') {
                    $RowPriceList = new PriceListItems();
                    $RowPriceList->item_id = $DtoItems->id;
                    if (isset($PriceList['pricelist_id'])) {
                        $RowPriceList->pricelist_id = $PriceList['pricelist_id'];
                    }
                    if (isset($PriceList['pricelist'])) {
                        $RowPriceList->price = $PriceList['pricelist'];
                    }
                    $RowPriceList->save();
             //   }
            }

            /* UPDATE IMG Allegati 
            $itemAttachment = ItemAttachments::where('item_id', $DtoItems->id)->get();
            if (isset($itemAttachment)) {
                ItemAttachments::where('item_id', $DtoItems->id)->delete();
            }*/

            
            if (isset($DtoItems->imgAgg)) {
                foreach ($DtoItems->imgAgg as $imgAgg) {
                    
                    if (!is_null($imgAgg['imgName']) && !is_null($imgAgg['imgBase64'])) {
                        if (strpos($DtoItems->imgBase64, '/storage/app/public/images/Attachments/') === false) {
                            Image::make($imgAgg['imgBase64'])->save(static::$dirImageAttachment . $DtoItems->id . $imgAgg['imgName']);
                            $itemAttachment = new ItemAttachments();
                            $itemAttachment->item_id = $DtoItems->id;
                            // $itemAttachment->img_attachment = static::getUrlAttachments() . $imgAgg['imgName'];
                            if (isset($imgAgg['imgName'])) {
                                $itemAttachment->img_attachment = static::getUrlAttachments() . $DtoItems->id . $imgAgg['imgName'];
                            }
                            $itemAttachment->save();
                        }
                    }

                    if (!is_null($imgAgg['imgName']) && is_null($imgAgg['imgBase64'])) {
                        $itemAttachment = new ItemAttachments();
                        $itemAttachment->item_id = $DtoItems->id;
                        if (isset($imgAgg['imgName'])) {
                            $itemAttachment->img_attachment = static::getUrlAttachments() . $DtoItems->id . $imgAgg['imgName'];
                        }
                        $itemAttachment->save();
                    }
                }
            }

            /*   if (isset($DtoItems->category_id)) {
            
            if(isset($DtoItems->category_id)){
                $CategoryId = CategoryLanguage::where('id', $DtoItems->category_id)->first()->category_id;
            } else {
                $CategoryId = "";
            }

            $CategoryItemOnDb->category_id = $CategoryId;
            $CategoryItemOnDb->save();*/


            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

     /**
     * Update
     * 
     * @param Request DtoCategories
     * @param int idLanguage
     */

    public static function updateImageAggFile(Request $DtoItems)
    {
        DB::beginTransaction();
        try {
              if (isset($DtoItems->imgAgg)) {
                foreach ($DtoItems->imgAgg as $imgAgg) {
                    if (isset($imgAgg['base64'])) {
                        if (!is_null($imgAgg['name']) && !is_null($imgAgg['base64'])) {
                            if (strpos($DtoItems->base64, '/storage/app/public/images/Attachments/') === false) {
                                Image::make($imgAgg['base64'])->save(static::$dirImageAttachment . $DtoItems->idItem . $imgAgg['name']);
                                $itemAttachment = new ItemAttachments();
                                $itemAttachment->item_id = $DtoItems->idItem;
                                if (isset($imgAgg['name'])) {
                                    $itemAttachment->img_attachment = static::getUrlAttachments() . $DtoItems->idItem . $imgAgg['name'];
                                }
                                $itemAttachment->save();
                            }
                        }
                    }
                }
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }



    #region DELETE Remove File Db 
    /**
     * Delete
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function RemoveFileDb(int $id, int $idLanguage)
    {
        $ItemAttachments = ItemAttachments::where('id', $id);

        if (is_null($ItemAttachments)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($ItemAttachments)) {

            DB::beginTransaction();

            try {

                ItemAttachments::where('id', $id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }

    #endregion DELETE removeFile ON DB 


    #region DELETE Remove File  
    /**
     * Delete
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function removeFile(int $id, int $idLanguage)
    {
        $dir = static::$dirImageAttachment;
        $filesystem = new Filesystem(new Adapter($dir));

        $ItemAttachments = ItemAttachments::where('id', $id)->first();
        $ImgNameAttachment = str_replace(static::getUrlAttachments(), '', $ItemAttachments->img_attachment);

        if (is_null($ItemAttachments)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($ItemAttachments)) {
            $filesystem->delete($ImgNameAttachment);
            ItemAttachments::where('id', $id)->delete();
        }
    }






    #endregion DELETE removeFile

    #region DELETE
    /**
     * Delete
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $Items = Item::find($id);


        if (is_null($Items)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($Items)) {

            DB::beginTransaction();

            try {

                ItemLanguageBL::deleteByIdCategoriesItems($id);
                ItemLanguageBL::deleteByItemLanguage($id);
                ItemPhotos::where('item_id', $id)->delete();
                $CategoryItem = CategoryItem::where('item_id', $id);
                $CategoryItem->delete();
                ItemAttachments::where('id', $id)->delete();
                ItemGroupTechnicalSpecification::where('item_id', $id)->delete();
                ItemAttachmentLanguageFileItemTable::where('item_id', $id)->delete();
                ItemTechnicalSpecification::where('item_id', $id)->delete();
                ItemTypeOptional::where('item_id', $id)->delete();
                PriceListItems::where('item_id', $id)->delete();
                $negotiation = Negotiation::where('item_id', $id)->get();
                if(isset($negotiation)){
                    foreach($negotiation as $negotiations){
                        NegotiationsDetail::where('negotiation_id', $negotiations->id)->delete();
                        Negotiation::where('id', $negotiations->id)->delete();
                    }
                }
                Item::where('id', $id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }

    /**
     * delete Items Photos
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function deleteItemsPhotos(int $id, int $idLanguage)
    {
        $itemPhotos = ItemPhotos::find($id);


        if (is_null($itemPhotos)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($itemPhotos)) {

            DB::beginTransaction();

            try {

                ItemPhotos::where('id', $id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }
    /**
     * delete getCancelTranslate
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function getCancelTranslate(int $id, int $idLanguage)
    {
        $itemLanguage = ItemLanguage::find($id);


        if (is_null($itemLanguage)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($itemLanguage)) {

            DB::beginTransaction();

            try {

                ItemLanguage::where('id', $id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }


    /**
     * delete Item Categories
     *
     * @param int $idCategories
     * @param int $idItem
     * @param int $idLanguage
     */
    public static function deleteItemCategories(int $idCategories, int $idItem, int $idLanguage)
    {
        if (is_null($idCategories)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }
        $CategoryItem = CategoryItem::where('item_id', $idItem)->where('category_id', $idCategories);
        $CategoryItem->delete();
    }

    #endregion DELETE
}

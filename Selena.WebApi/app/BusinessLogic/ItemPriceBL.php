<?php

namespace App\BusinessLogic;

use App\CorrelationPeopleUser;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoPeople;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\User;
use App\People;
use App\PeopleUser;
use App\Role;
use App\RolesPeople;
use App\RoleUser;
use App\UsersDatas;
use App\Item;
use App\ItemLanguage;
use App\CategoryItem;
use App\DtoModel\DtoPageSitemap;
use App\BusinessLogic\OffertaTrovaPrezziBL;
use App\Content;
use App\ContentLanguage;
use App\DtoModel\DtoItem;

use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;


class ItemPriceBL
{
    


    public static function getHtml($url, $idUser)
    {
        if (strpos($url, '/prezzo/') !== false) {
            $content = ItemLanguage::where('link_price', $url)->first();
            if (isset($content)) {
                $itemOnDb = Item::where('id', $content->item_id)->get()->first();

                if (is_null($idUser) || $idUser == '') {
                    $idUserNew = null;
                }else{
                    $idUserNew = $idUser;
                }
                $idContent = Content::where('code', 'PagePrices')->first();
                $languageContent = ContentLanguage::where('content_id', $idContent->id)->first();
                $contentHtml = ContentLanguageBL::getByCodeRender($idContent->id, $languageContent->language_id, $url, null, null, $idUserNew);

                $dtoPageItemRender = new DtoItem();
                $dtoPageItemRender->html = $contentHtml;
                $dtoPageItemRender->description = $content->meta_tag_title_price;
                $dtoPageItemRender->meta_tag_description = $content->meta_tag_description_price;
                $dtoPageItemRender->meta_tag_keyword = $content->meta_tag_keyword_price;
                $dtoPageItemRender->meta_tag_title = $content->meta_tag_title_price;
                
                if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
                    $protocol = "https://";
                } else {
                    $protocol = "http://";
                }

                if (strpos($itemOnDb->img, "https://") !== false && strpos($itemOnDb->img, "http://") !== false) {
                    $dtoPageItemRender->url_cover_image = $itemOnDb->img;
                }else{
                    $dtoPageItemRender->url_cover_image = $protocol . $_SERVER["HTTP_HOST"] . $itemOnDb->img;
                }
                
                return $dtoPageItemRender;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    public static function getHtmlDettaglioOfferte($idItem)
    {
        
        $html = "";
        $htmlTemp = "";
        $htmlApp = "";
        $itemLanguageOnDb = ItemLanguage::where('item_id', $idItem)->first();

        if (isset($itemLanguageOnDb)) {
            
            $categoryItemOnDb = CategoryItem::where('item_id', $itemLanguageOnDb->item_id)->get()->first();

            if ($categoryItemOnDb->category_id>=11 && $categoryItemOnDb->category_id<=15)
                $offerte = OffertaTrovaPrezziBL::getOfferte("cameranationit", $itemLanguageOnDb->description, "5");
            else if ($categoryItemOnDb->category_id==16)
                $offerte = OffertaTrovaPrezziBL::getOfferte("cameranationit", $itemLanguageOnDb->description, "20124");
            else if ($categoryItemOnDb->category_id==17)
                $offerte = OffertaTrovaPrezziBL::getOfferte("cameranationit", $itemLanguageOnDb->description, "7");
            else
                $offerte = OffertaTrovaPrezziBL::getOfferte("cameranationit", $itemLanguageOnDb->description, "0");

            if (count($offerte)>0){

                //mi prendo il content della riga singola del prezzo
                $htmlApp = ContentBL::getByCode('Price_Detail_Single_Row');
                
                for($i = 0; $i < count($offerte); $i++){
                    $htmlTemp = $htmlApp->content;
                    $htmlTemp = str_replace('#url#', $offerte[$i]->url,  $htmlTemp);  
                    $htmlTemp = str_replace('#name#', $offerte[$i]->name,  $htmlTemp); 
                    $htmlTemp = str_replace('#merchantName#', $offerte[$i]->merchantName,  $htmlTemp); 
                    $htmlTemp = str_replace('#merchant#', $offerte[$i]->merchant,  $htmlTemp); 
                    $htmlTemp = str_replace('#currencyCode#', $offerte[$i]->currencyCode,  $htmlTemp); 
                    $htmlTemp = str_replace('#price#', $offerte[$i]->price,  $htmlTemp); 
                    $htmlTemp = str_replace('#listingPrice#', $offerte[$i]->listingPrice,  $htmlTemp); 
                    $htmlTemp = str_replace('#deliveryCost#', $offerte[$i]->deliveryCost,  $htmlTemp); 

                    $htmlTemp = str_replace('#availability#', $offerte[$i]->availability,  $htmlTemp); 
                    $htmlTemp = str_replace('#availabilityDescr#', $offerte[$i]->availabilityDescr,  $htmlTemp); 
                    $htmlTemp = str_replace('#url#', $offerte[$i]->url,  $htmlTemp); 
                    $htmlTemp = str_replace('#description#', $offerte[$i]->description,  $htmlTemp); 
                    $htmlTemp = str_replace('#smallImage#', $offerte[$i]->smallImage,  $htmlTemp); 
                    $htmlTemp = str_replace('#bigImage#', $offerte[$i]->bigImage,  $htmlTemp); 
                    $htmlTemp = str_replace('#merchantLogo#', $offerte[$i]->merchantLogo,  $htmlTemp); 
                    $htmlTemp = str_replace('#categoryId#', $offerte[$i]->categoryId,  $htmlTemp); 
                    $htmlTemp = str_replace('#categoryName#', $offerte[$i]->categoryName,  $htmlTemp); 
                    $htmlTemp = str_replace('#merchantRating#', $offerte[$i]->merchantRating,  $htmlTemp); 

                    $html = $html . $htmlTemp;
                }

            }else
            {
                $html = $html . '   <h4>Prezzo non disponibile</h4>';
            } 
        }
        
        return $html;
    }

}
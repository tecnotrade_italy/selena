<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoAdvertisingArea;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\AdvertisingArea;
use App\AdvertisingBanner;
use App\AdvertisingBannerReport;
use Exception;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoAdvertisingBanner;

class AdvertisingAreaBL
{
    #region PRIVATE

    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request dtoAdvertisingArea
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, $dbOperationsTypesEnum)
    {

        if (is_null($request->code)|| $request->code=='') {
            throw new Exception("Il codice dell'area è obbligatorio");
        }

    }

    /**
     * Convert to dto
     * 
     * @param AdvertisingArea AdvertisingArea
     * @return DtoAdvertisingArea
     */
    private static function _convertToDto($AdvertisingArea)
    {
        $dtoAdvertisingArea = new DtoAdvertisingArea();

        if (isset($AdvertisingArea->id)) {
            $dtoAdvertisingArea->id = $AdvertisingArea->id;
        }

        if (isset($AdvertisingArea->code)) {
            $dtoAdvertisingArea->code = $AdvertisingArea->code;
        }

        if (isset($AdvertisingArea->description)) {
            $dtoAdvertisingArea->description = $AdvertisingArea->description;
        }

        if (isset($AdvertisingArea->width_pixels)) {
            $dtoAdvertisingArea->width_pixels = $AdvertisingArea->width_pixels;
        }

        if (isset($AdvertisingArea->height_pixels)) {
            $dtoAdvertisingArea->height_pixels = $AdvertisingArea->height_pixels;
        }

        if (isset($AdvertisingArea->active)) {
            $dtoAdvertisingArea->active = $AdvertisingArea->active;
        }


        return $dtoAdvertisingArea;
    }


    /**
     * Convert to VatType
     * 
     * @param dtoAdvertisingArea dtoAdvertisingArea
     * 
     * @return AdvertisingArea
     */
    private static function _convertToModel($dtoAdvertisingArea)
    {
        $AdvertisingArea = new AdvertisingArea();

        if (isset($dtoAdvertisingArea->id)) {
            $AdvertisingArea->id = $dtoAdvertisingArea->id;
        }

        if (isset($dtoAdvertisingArea->code)) {
            $AdvertisingArea->code = $dtoAdvertisingArea->code;
        }

        if (isset($dtoAdvertisingArea->description)) {
            $AdvertisingArea->description = $dtoAdvertisingArea->description;
        }else
            $AdvertisingBanner->description = null;

        if (isset($dtoAdvertisingArea->width_pixels)) {
            $AdvertisingArea->width_pixels = $dtoAdvertisingArea->width_pixels;
        }else
            $AdvertisingBanner->width_pixels = null;

        if (isset($dtoAdvertisingArea->height_pixels)) {
            $AdvertisingArea->height_pixels = $dtoAdvertisingArea->height_pixels;
        }else
            $AdvertisingBanner->height_pixels = null;

        if (isset($dtoAdvertisingArea->active)) {
            $AdvertisingArea->active = $dtoAdvertisingArea->active;
        }

        return $AdvertisingArea;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCommunityction
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(AdvertisingArea::find($id));
    }


    public static function getByCode(string $code)
    {
        if (is_null($code)) {
            LogHelper::error('Code of advertising area not found');
            return '';
        } else {
            $advertiseArea = AdvertisingArea::where('code', $code)->first();

            if (is_null($advertiseArea)) {
                LogHelper::error('Code of advertising area not found');
                return '';
            } else {
                return $advertiseArea;
            }
        }
    }

    public static function getHtmlBanner(Request $request){
        try {
            $codeArea = $request->code;
            $paginaVisualizzaa = $request->url;

            $bannerList = collect();
            $htmlFinal = '';

            //recupero l'area in base al codice
            $advertisingArea = AdvertisingArea::where('code', $codeArea)->where('active', 'true')->first();
            
            if (is_null($advertisingArea)) {
                return '';
            } else {
                //cerco i banner attivi e con le date giuste
                
                foreach (DB::select(
                        'SELECT * 
                        from advertising_banners
                        where advertising_area_id = :area_id
                        and active = true
                        and weight > 0
                        and start_date <= now()
                        and (stop_date >= now() or stop_date is null)',
                    ['area_id' => $advertisingArea->id]
                    ) as $Banner) {
                    
                            $DtoAdvertisingBanner = new DtoAdvertisingBanner();
                            $DtoAdvertisingBanner->id = $Banner->id;
                            $DtoAdvertisingBanner->advertising_area_id = $Banner->advertising_area_id;
                            $DtoAdvertisingBanner->start_date = $Banner->start_date;
                            $DtoAdvertisingBanner->stop_date = $Banner->stop_date;
                            $DtoAdvertisingBanner->image_file = $Banner->image_file;
                            $DtoAdvertisingBanner->image_alt = $Banner->image_alt;
                            $DtoAdvertisingBanner->link_url = $Banner->link_url;
                            $DtoAdvertisingBanner->target_blank = $Banner->target_blank;
                            $DtoAdvertisingBanner->active = $Banner->active;
                            $DtoAdvertisingBanner->script_Js = $Banner->script_Js;
                            $DtoAdvertisingBanner->maximum_impressions = $Banner->maximum_impressions;
                            $DtoAdvertisingBanner->progressive_impressions = $Banner->progressive_impressions;
                            $DtoAdvertisingBanner->weight = $Banner->weight;
                            $DtoAdvertisingBanner->user_id = $Banner->user_id;
                            $DtoAdvertisingBanner->active_reports = $Banner->active_reports;
                            $DtoAdvertisingBanner->description = $Banner->description; 
                            
                            $bannerList->push($DtoAdvertisingBanner);
                }

                // ho un array con i banner disponibili per quell'area. Ora devo scegliere quale far
                // vedere in base al suo peso. Mi calcolo un numero CASUALE da 0 a 100
                if ($bannerList->count()==0){
                    
                    return '';

                } else {
                    $pesoCasuale = rand(1, 100);
                    $pesoProvvisorio = 0;
                    //ora cerco il banner in base al peso casuale e al peso del banner
                    $trovato = False;
                    foreach($bannerList as $banner){
                        $pesoProvvisorio = $pesoProvvisorio+$banner->weight;
                        if ($pesoProvvisorio>=$pesoCasuale){
                            $trovato = True;

                            if ($banner->script_Js<>'') {
                                //semplicemente ritorno lo script JS (tipo adsense)
                                $htmlFinal = $banner->script_Js;
                            }else{
                                //restituiscol'immagine del banner
                                if (isset($banner->link_url)){

                                    $htmlFinal = '<a href="'.$banner->link_url;
                                    $htmlFinal = $htmlFinal.'"';
                                    if ($banner->target_blank = True)
                                        $htmlFinal = $htmlFinal.' target="_blank"';
                                    $htmlFinal = $htmlFinal.'>';
                                }

                                $htmlFinal = $htmlFinal.'<img border="0" alt="'.$banner->image_alt.'" src="'.$banner->image_file.'">';

                                if (isset($banner->link_url)){
                                    $htmlFinal = $htmlFinal.'</a>';
                                }
                            }

                            //aumento le impression progressive e, se c'è il tracking, memorizzo tutto
                            $bannerVisualizzato = AdvertisingBanner::where('id', $banner->id)->get()->first();                            
                            $bannerVisualizzato->progressive_impressions += 1;
                            if ($bannerVisualizzato->maximum_impressions>0 && $bannerVisualizzato->progressive_impressions>=$bannerVisualizzato->maximum_impressions)
                                $bannerVisualizzato->active = False;
                            $bannerVisualizzato->save();

                            //verifico se devo tenere il log
                            if ($bannerVisualizzato->active_reports){
                                $report = new AdvertisingBannerReport();
                                $report->advertising_banner_id = $banner->id;
                                $report->impression_date = now();
                                $report->page = $request->url;
                                $report->source_ip = '';
                                $report->save();
                            }
                            break;
                        }
                    }

                    if ($trovato = False)
                        return '';
                    else
                    
                        return $htmlFinal;

                }
            }
            


        } catch (Exception $ex) {
            throw $ex;
        } 
    }


    /*
     * Get all
     * 
     * @return DtoAdvertisingArea
     */
    public static function getAll()
    {

        $result = collect(); 

        foreach (AdvertisingArea::orderBy('id','DESC')->get() as $advertisingArea) {

            $DtoAdvertisingArea = new DtoAdvertisingArea();

            $DtoAdvertisingArea->id = $advertisingArea->id;
            $DtoAdvertisingArea->code = $advertisingArea->code;
            $DtoAdvertisingArea->description = $advertisingArea->description;
            $DtoAdvertisingArea->width_pixels = $advertisingArea->width_pixels;
            $DtoAdvertisingArea->height_pixels = $advertisingArea->height_pixels;
            $DtoAdvertisingArea->active = $advertisingArea->active;

            $result->push($DtoAdvertisingArea); 
        }
        return $result;

    }

    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return AdvertisingArea::select('id', 'description')->get();
        } else {
            return AdvertisingArea::where('description', 'like', $search . '%')->select('id', 'description')->get();
        }
    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoAdvertisingArea
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoAdvertisingArea)
    {
        
        static::_validateData($DtoAdvertisingArea, DbOperationsTypesEnum::INSERT);
        $AdvertisingArea = static::_convertToModel($DtoAdvertisingArea);

        $AdvertisingArea->save();

        return $AdvertisingArea->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoAdvertisingArea
     */
    public static function update(Request $DtoAdvertisingArea)
    {
        static::_validateData($DtoAdvertisingArea, DbOperationsTypesEnum::UPDATE);
        $AdvertisingArea = AdvertisingArea::find($DtoAdvertisingArea->id);
        DBHelper::updateAndSave(static::_convertToModel($DtoAdvertisingArea), $AdvertisingArea);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     */
    public static function delete(int $id)
    {
        $AdvertisingArea = AdvertisingArea::find($id);

        $AdvertisingArea->delete();
    }

    #endregion DELETE
}

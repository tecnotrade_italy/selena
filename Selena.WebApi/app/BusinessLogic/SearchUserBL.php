<?php

namespace App\BusinessLogic;

use App\Content;
use App\ContentLanguage;
use App\DtoModel\DtoNation;
use App\DtoModel\DtoSelenaViews;
use App\DtoModel\DtoTagTemplate;
use App\DtoModel\DtoUser;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\User;
use App\SelenaSqlViews;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchUserBL
{

  /**
     * Search implemented
     * 
     * @param Request request
     * 
     */
    public static function searchImplemented(request $request){
        $result = collect();
        $content = '';
        $finalHtml = '';
        $strWhere = "";
        $idLanguageContent = $request->idLanguage;

         $name = $request->name; 
       if ($name != "") {
            $strWhere = ' AND ud.name ilike \'%' . $name . '%\'';
        }
           $business_name = $request->business_name; 
       if ($business_name != "") {
            $strWhere = ' AND ud.business_name ilike \'%' . $business_name . '%\'';
        }
           $telephone_number = $request->telephone_number; 
       if ($telephone_number != "") {
            $strWhere = ' AND ud.telephone_number ilike \'%' . $telephone_number . '%\'';
        }
           $email = $request->email; 
       if ($email != "") {
            $strWhere = ' AND u.email ilike \'%' . $email . '%\'';
        }
          $country = $request->country; 
       if ($country != "") {
            $strWhere = ' AND ud.country ilike \'%' . $country . '%\'';
        }

          $province = $request->province; 
       if ($province != "") {
            $strWhere = ' AND ud.province ilike \'%' . $province . '%\'';
        }
    

        foreach (DB::select(
            'SELECT 
            u.id,
            s.subscriber,
            s.expiration_date,
            s.subscription_date,
            ud.vat_number,
            ud.business_name,
            ud.name,
            ud.telephone_number,
            ud.mobile_number,
            ud.fax_number,
            u.email
            FROM users_datas as ud
            INNER JOIN users as u ON u.id = ud.user_id
            LEFT JOIN subscriber as s ON s.user_id = ud.user_id
            WHERE ud.language_id = :idLanguage ' . $strWhere,
            ['idLanguage' => $idLanguageContent],
        ) as $user) {
 
       $dtousersdatas = new DtoUser();
       $dtousersdatas->id = $user->id;

        if (isset($user->business_name)) {
            $dtousersdatas->business_name = $user->business_name;
        } else {
            $dtousersdatas->business_name = '';
       }
        if (isset($user->name)) {
            $dtousersdatas->name = $user->name;
        } else {
            $dtousersdatas->name = '';
       }
       if (isset($user->vat_number)) {
            $dtousersdatas->vat_number = $user->vat_number;
        } else {
            $dtousersdatas->vat_number = '';
       }

        if (isset($user->telephone_number)) {
            $dtousersdatas->telephone_number = $user->telephone_number;
        } else {
            $dtousersdatas->telephone_number = '';
       }
       if (isset($user->mobile_number)) {
            $dtousersdatas->mobile_number = $user->mobile_number;
        } else {
            $dtousersdatas->mobile_number = '';
       }
        if (isset($user->fax_number)) {
            $dtousersdatas->fax_number = $user->fax_number;
        } else {
            $dtousersdatas->fax_number = '';
       }
         if (isset($user->email)) {
            $dtousersdatas->email = $user->email;
        } else {
            $dtousersdatas->email = '';
       }
         if (isset($user->subscriber)) {
            $dtousersdatas->subscriber = $user->subscriber;
        } else {
            $dtousersdatas->subscriber = '';
       }
        if (isset($user->expiration_date)) {
            $dtousersdatas->expiration_date = $user->expiration_date;
        } else {
            $dtousersdatas->expiration_date = '';
       }
        if (isset($user->subscription_date)) {
            $dtousersdatas->subscription_date = $user->subscription_date;
        } else {
            $dtousersdatas->subscription_date = '';
       }
       //$dtousersdatas->business_name = $user->business_name;
      // $dtousersdatas->name = $user->name;
       //$dtousersdatas->telephone_number = $user->telephone_number;
      // $dtousersdatas->mobile_number = $user->mobile_number;
       //$dtousersdatas->fax_number = $user->fax_number;
       //$dtousersdatas->email = $user->email;
      // $dtousersdatas->subscriber = $user->subscriber;
       //$dtousersdatas->expiration_date = $user->expiration_date;
       //$dtousersdatas->subscription_date = $user->subscription_date;

      $result->push($dtousersdatas);
    }
    return $result;

    }
    
    /**
     * SearchViewPrivates
     * 
     * @param Request request
     * 
     */
    public static function SearchViewPrivates(request $request){
        $result = collect();

        foreach (DB::select(
            'SELECT 
            u.id,
            s.subscriber,
            s.expiration_date,
            s.subscription_date,
            ud.vat_number,
            ud.business_name,
            ud.name,
            ud.telephone_number,
            ud.mobile_number,
            ud.fax_number,
            u.email
            FROM users_datas as ud
            INNER JOIN users as u ON u.id = ud.user_id
            LEFT JOIN subscriber as s ON s.user_id = ud.user_id
            WHERE ud.privates = true'
        ) as $user) {
 
       $dtousersdatas = new DtoUser();
       $dtousersdatas->id = $user->id;

        if (isset($user->business_name)) {
            $dtousersdatas->business_name = $user->business_name;
        } else {
            $dtousersdatas->business_name = '';
       }
        if (isset($user->name)) {
            $dtousersdatas->name = $user->name;
        } else {
            $dtousersdatas->name = '';
       }
       if (isset($user->vat_number)) {
            $dtousersdatas->vat_number = $user->vat_number;
        } else {
            $dtousersdatas->vat_number = '';
       }

        if (isset($user->telephone_number)) {
            $dtousersdatas->telephone_number = $user->telephone_number;
        } else {
            $dtousersdatas->telephone_number = '';
       }
       if (isset($user->mobile_number)) {
            $dtousersdatas->mobile_number = $user->mobile_number;
        } else {
            $dtousersdatas->mobile_number = '';
       }
        if (isset($user->fax_number)) {
            $dtousersdatas->fax_number = $user->fax_number;
        } else {
            $dtousersdatas->fax_number = '';
       }
         if (isset($user->email)) {
            $dtousersdatas->email = $user->email;
        } else {
            $dtousersdatas->email = '';
       }
         if (isset($user->subscriber)) {
            $dtousersdatas->subscriber = $user->subscriber;
        } else {
            $dtousersdatas->subscriber = '';
       }
        if (isset($user->expiration_date)) {
            $dtousersdatas->expiration_date = $user->expiration_date;
        } else {
            $dtousersdatas->expiration_date = '';
       }
        if (isset($user->subscription_date)) {
            $dtousersdatas->subscription_date = $user->subscription_date;
        } else {
            $dtousersdatas->subscription_date = '';
       }

      $result->push($dtousersdatas);
    }
    return $result;

    }


     /**
     * SearchViewPrivates
     * 
     * @param Request request
     * 
     */
    public static function SearchViewFalseSubscriber(request $request){
        $result = collect();

        foreach (DB::select(
            'SELECT 
            u.id,
            s.subscriber,
            s.expiration_date,
            s.subscription_date,
            ud.vat_number,
            ud.business_name,
            ud.name,
            ud.telephone_number,
            ud.mobile_number,
            ud.fax_number,
            u.email
            FROM users_datas as ud
            INNER JOIN users as u ON u.id = ud.user_id
            LEFT JOIN subscriber as s ON s.user_id = ud.user_id
            WHERE s.subscriber = false'
        ) as $user) {
 
       $dtousersdatas = new DtoUser();
       $dtousersdatas->id = $user->id;

        if (isset($user->business_name)) {
            $dtousersdatas->business_name = $user->business_name;
        } else {
            $dtousersdatas->business_name = '';
       }
        if (isset($user->name)) {
            $dtousersdatas->name = $user->name;
        } else {
            $dtousersdatas->name = '';
       }
       if (isset($user->vat_number)) {
            $dtousersdatas->vat_number = $user->vat_number;
        } else {
            $dtousersdatas->vat_number = '';
       }

        if (isset($user->telephone_number)) {
            $dtousersdatas->telephone_number = $user->telephone_number;
        } else {
            $dtousersdatas->telephone_number = '';
       }
       if (isset($user->mobile_number)) {
            $dtousersdatas->mobile_number = $user->mobile_number;
        } else {
            $dtousersdatas->mobile_number = '';
       }
        if (isset($user->fax_number)) {
            $dtousersdatas->fax_number = $user->fax_number;
        } else {
            $dtousersdatas->fax_number = '';
       }
         if (isset($user->email)) {
            $dtousersdatas->email = $user->email;
        } else {
            $dtousersdatas->email = '';
       }
         if (isset($user->subscriber)) {
            $dtousersdatas->subscriber = $user->subscriber;
        } else {
            $dtousersdatas->subscriber = '';
       }
        if (isset($user->expiration_date)) {
            $dtousersdatas->expiration_date = $user->expiration_date;
        } else {
            $dtousersdatas->expiration_date = '';
       }
        if (isset($user->subscription_date)) {
            $dtousersdatas->subscription_date = $user->subscription_date;
        } else {
            $dtousersdatas->subscription_date = '';
       }

      $result->push($dtousersdatas);
    }
    return $result;

    }

     /**
     * SearchViewTrueSubscriber
     * 
     * @param Request request
     * 
     */
    public static function SearchViewTrueSubscriber(request $request){
        $result = collect();

        foreach (DB::select(
            'SELECT 
            u.id,
            s.subscriber,
            s.expiration_date,
            s.subscription_date,
            ud.vat_number,
            ud.business_name,
            ud.name,
            ud.telephone_number,
            ud.mobile_number,
            ud.fax_number,
            u.email
            FROM users_datas as ud
            INNER JOIN users as u ON u.id = ud.user_id
            LEFT JOIN subscriber as s ON s.user_id = ud.user_id
            WHERE s.subscriber = true'
        ) as $user) {
 
       $dtousersdatas = new DtoUser();
       $dtousersdatas->id = $user->id;

        if (isset($user->business_name)) {
            $dtousersdatas->business_name = $user->business_name;
        } else {
            $dtousersdatas->business_name = '';
       }
        if (isset($user->name)) {
            $dtousersdatas->name = $user->name;
        } else {
            $dtousersdatas->name = '';
       }
       if (isset($user->vat_number)) {
            $dtousersdatas->vat_number = $user->vat_number;
        } else {
            $dtousersdatas->vat_number = '';
       }

        if (isset($user->telephone_number)) {
            $dtousersdatas->telephone_number = $user->telephone_number;
        } else {
            $dtousersdatas->telephone_number = '';
       }
       if (isset($user->mobile_number)) {
            $dtousersdatas->mobile_number = $user->mobile_number;
        } else {
            $dtousersdatas->mobile_number = '';
       }
        if (isset($user->fax_number)) {
            $dtousersdatas->fax_number = $user->fax_number;
        } else {
            $dtousersdatas->fax_number = '';
       }
         if (isset($user->email)) {
            $dtousersdatas->email = $user->email;
        } else {
            $dtousersdatas->email = '';
       }
         if (isset($user->subscriber)) {
            $dtousersdatas->subscriber = $user->subscriber;
        } else {
            $dtousersdatas->subscriber = '';
       }
        if (isset($user->expiration_date)) {
            $dtousersdatas->expiration_date = $user->expiration_date;
        } else {
            $dtousersdatas->expiration_date = '';
       }
        if (isset($user->subscription_date)) {
            $dtousersdatas->subscription_date = $user->subscription_date;
        } else {
            $dtousersdatas->subscription_date = '';
       }

      $result->push($dtousersdatas);
    }
    return $result;

    }

}
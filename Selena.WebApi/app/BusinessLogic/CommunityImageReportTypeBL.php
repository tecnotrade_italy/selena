<?php
namespace App\BusinessLogic;

use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\LogHelper;
use App\DtoModel\DtoCommunityImageReportType;
use App\CommunityImageReportTypeLanguage;
use App\CommunityImageReportType;
use App\User;
use Illuminate\Support\Facades\DB;



use Exception;
use Illuminate\Http\Request;

class CommunityImageReportTypeBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request CommunityImageReportTypeBL
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoCommunityImageReportType, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoCommunityImageReportType->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (CommunityImageReportType::find($DtoCommunityImageReportType->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($DtoCommunityImageReportType->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CompanyNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    /**
     * Convert to dto
     * 
     * @param CommunityImageReportType postalCode
     * 
     * @return DtoCommunityImageReportType
     */
    private static function _convertToDto($CommunityImageReportType)
    {
        $DtoCommunityImageReportType = new DtoCommunityImageReportType();
        
        if (isset($CommunityImageReportType->id)) {
            $DtoCommunityImageReportType->id = $CommunityImageReportType->id;
        }

        if (isset($CommunityImageReportType->description)) {
            $DtoCommunityImageReportType->description = $CommunityImageReportType->description;
        }

        return  $DtoCommunityImageReportType;
    }


    /**
     * Convert to VatType
     * 
     * @param DtoCommunityImageReportType dtoVatType
     * 
     * @return CommunityImageReportType
     */
    private static function _convertToModel($DtoCommunityImageReportType)
    {
        $CommunityImageReportType = new CommunityImageReportType();

        if (isset($DtoCommunityImageReportType->id)) {
            $CommunityImageReportType->id = $DtoCommunityImageReportType->id;
        }


        if (isset($DtoCommunityImageReportType->description)) {
            $CommunityImageReportType->description = $DtoCommunityImageReportType->description;
        }

        if (isset($DtoCommunityImageReportType->language_id)) {
            $CommunityImageReportType->language_id = $DtoCommunityImageReportType->language_id;
        }

        return $CommunityImageReportType;
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCommunityImageReportType
     */

    public static function getById(int $id)
    {
        return static::_convertToDto(CommunityImageReportType::find($id));
    }

 
    /**
     * Get select
     *
     * @param String $search
     * @return CommunityImageReportTypeLanguage
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return CommunityImageReportTypeLanguage::select('id', 'description')->get();
        } else {
            return CommunityImageReportTypeLanguage::where('description', 'like', $search . '%')->select('id', 'description')->get();
        }
    }


    /**
     * Get all
     * 
     * @return DtoCommunityImageReportType
     */
    public static function getAll($idLanguage)
    {
        $result = collect(); 
        foreach (CommunityImageReportTypeLanguage::where('language_id',$idLanguage)->get() as $categoryAll) {
            $DtoCommunityImageReportType = new DtoCommunityImageReportType();
            $DtoCommunityImageReportType->id = CommunityImageReportType::where('id', $categoryAll->image_report_type_id)->first()->id;
            $DtoCommunityImageReportType->description = $categoryAll->description;
            $result->push($DtoCommunityImageReportType); 
        }
        return $result;
    }
    

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoCommunityImageReportType
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoCommunityImageReportType, int $idLanguage, int $idUser)
    {
        
        static::_validateData($DtoCommunityImageReportType, $idLanguage, DbOperationsTypesEnum::INSERT);
        DB::beginTransaction();

        try {
            $CommunityImageReportType = new CommunityImageReportType();
            $CommunityImageReportType->created_id = $idUser;
            $CommunityImageReportType->save();

            $CommunityImageReportTypeLanguage = new CommunityImageReportTypeLanguage();
            $CommunityImageReportTypeLanguage->language_id =$idLanguage;
            $CommunityImageReportTypeLanguage->image_report_type_id = $CommunityImageReportType->id;
            $CommunityImageReportTypeLanguage->description = $DtoCommunityImageReportType->description;
            $CommunityImageReportTypeLanguage->created_id = $idUser;
            $CommunityImageReportTypeLanguage->save();

            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $CommunityImageReportType->id;
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoCommunityImageReportType
     * @param int idLanguage
     */
    //::debug($DtoCategory->toArray());

    public static function update(Request $DtoCommunityImageReportType, int $idLanguage)

    {
        static::_validateData($DtoCommunityImageReportType, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $customerOnDb = CommunityImageReportType::find($DtoCommunityImageReportType->id);

        $languageNationOnDb = CommunityImageReportTypeLanguage::where('image_report_type_id', $DtoCommunityImageReportType->id, $DtoCommunityImageReportType->idLanguage)->first();
        
        DB::beginTransaction();

        try {

            //$languageNationOnDb->language_id = $idLanguage;
            $languageNationOnDb->language_id = $idLanguage;
            $languageNationOnDb->image_report_type_id = $DtoCommunityImageReportType->id;
            $languageNationOnDb->description = $DtoCommunityImageReportType->description;
            $languageNationOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
    
  #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */

    public static function delete(int $id, int $idLanguage)
    {
        $CommunityImageReportTypeLanguage= CommunityImageReportTypeLanguage::where('image_report_type_id', $id)->first();

        if (is_null($CommunityImageReportTypeLanguage)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $CommunityImageReportTypeLanguage->delete();

        $CommunityImageReportType = CommunityImageReportType::find($id);

        if (is_null($CommunityImageReportType)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $CommunityImageReportType->delete();
    }

    #endregion DELETE
}
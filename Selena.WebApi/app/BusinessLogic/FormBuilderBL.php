<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoFormBuilder;
use App\DtoModel\DtoFormBuilderField;
use App\DtoModel\DtoValueFormBuilderField;
use App\DtoModel\DtoValueFatherFormBuilderField;

use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\FormBuilder;
use App\FormBuilderField;
use App\FormBuilderFieldType;
use App\ValueFormBuilderField;
use App\ValueFatherFormBuilderField;

use App\Contact;
use App\ContactFormBuilder;
use App\Content;
use App\ContentLanguage;
use App\Enums\SettingEnum;
use Intervention\Image\Facades\Image;


use App\Mail\DynamicMail;
use Illuminate\Support\Facades\Mail;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormBuilderBL
{
    public static $dirImage = '../storage/app/public/images/FormBuilder/';

    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (FormBuilder::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return '/storage/images/FormBuilder/';
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoFormBuilder
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (DB::table('form_builder')->get() as $optionalAll) {
            $dtoOptional = new DtoFormBuilder();
            $dtoOptional->id = $optionalAll->id;
            $dtoOptional->description = $optionalAll->description;
            $dtoOptional->object = $optionalAll->object;
            $dtoOptional->email_destination = $optionalAll->email_destination;
            $dtoOptional->feedback_message = $optionalAll->feedback_message;
            $dtoOptional->message_company = $optionalAll->message_company;
            $dtoOptional->message_customer = $optionalAll->message_customer;
            $dtoOptional->feedback_redirect = $optionalAll->feedback_redirect;
            $dtoOptional->feedback_url_redirect = $optionalAll->feedback_url_redirect;

            $result->push($dtoOptional); 
        }
        
        return $result;
    }

    /**
     * get All Request by form
     * 
     * 
     * @return DtoFormBuilder
     */
    public static function getAllRequest()
    {
        $result = collect();

        $queryFormBuilderFieldType = 'SELECT distinct contact_form_builder.contact_id, contact_form_builder.form_builder_id, contact_form_builder.created_at, form_builder.description from contact_form_builder inner join form_builder on contact_form_builder.form_builder_id = form_builder.id order by created_at desc';
        $formBuilderFieldTypeQuery = DB::select($queryFormBuilderFieldType);
       
        foreach ($formBuilderFieldTypeQuery as $optionalAll) {
            $dtoOptional = new DtoFormBuilder();
            $dtoOptional->id = $optionalAll->contact_id;
            $dtoOptional->description = $optionalAll->description;
            $dtoOptional->date = $optionalAll->created_at;


            $resultList = collect();
            $queryFormBuilderFieldTypeList = 'SELECT * FROM contact_form_builder WHERE contact_id = ' . $optionalAll->contact_id . ' and form_builder_id = ' . $optionalAll->form_builder_id . ' order by created_at desc';
            $formBuilderFieldTypeQueryList = DB::select($queryFormBuilderFieldTypeList);
            foreach ($formBuilderFieldTypeQueryList as $optionalAllList) {
                if($optionalAllList->field != 'id-form-builder'){
                    $dtoOptionalList = new DtoFormBuilder();
                    $dtoOptionalList->id = $optionalAllList->id;
                    $dtoOptionalList->field = $optionalAllList->field;
                    $dtoOptionalList->value = $optionalAllList->value;
                    $resultList->push($dtoOptionalList);
                }
            }
            $dtoOptional->listField = $resultList;

            $result->push($dtoOptional); 
        }
        
        return $result;
    }

    public static function getFormShortcode($idForm, $idLanguage, $idUser, $param){

        $content = '';

        $queryFormBuilderFieldType = 'SELECT distinct form_builder_type.id, form_builder_type.description, form_builder_type."order"  from form_builder_type 
        inner join form_builder_field on form_builder_field.form_builder_type_id = form_builder_type.id
        where form_builder_field.form_builder_id = ' . $idForm . ' order by form_builder_type."order"';
        $formBuilderFieldTypeQuery = DB::select($queryFormBuilderFieldType);

        if(isset($formBuilderFieldTypeQuery)){
            $idContent = Content::where('code', 'FormBuilderFieldType')->first();
            $htmlContent = ContentLanguage::where('content_id', $idContent->id)->first()->content;
               
            //settaggio variabile nascasta per email di invio form
            $strEmailHidden = '';
            $emailHiddenValue = SettingBL::getByCode(SettingEnum::ContactsEmail);
            $queryEmail = "SELECT * FROM form_builder_field WHERE form_builder_id=" . $idForm . " AND form_builder_field.type='input-email'";

            $queryEmailVerify = DB::select($queryEmail);
            if(!isset($queryEmailVerify)){
                $strEmailHidden = '<input type="hidden" data-description="Email hidden" id="email" name="email" value="' . $emailHiddenValue . '">';
            }

            $finalHtml = '';
            $newContent  = '';
            foreach ($formBuilderFieldTypeQuery as $formBuilderFieldTypeQuerys) {
                $content = '';
                $newContent = $htmlContent;

                $query = 'SELECT * FROM form_builder_field WHERE form_builder_type_id=' . $formBuilderFieldTypeQuerys->id . ' AND form_builder_id = ' . $idForm . ' ORDER BY "order"';
                $formShortcode = DB::select($query);


                if(isset($formShortcode)){
                    $newContent = str_replace('#description#', $formBuilderFieldTypeQuerys->description,  $newContent);
                    
                    foreach ($formShortcode as $formBuilder) {
                        
                        $typeValue = FormBuilderFieldType::where('id', $formBuilder->type)->get()->first()->value;
                        
                        $content = $content . '<div class="col-12 col-md-' . $formBuilder->width . '">';
                        
                        
                        $stringRequired = '';
                        if($formBuilder->required){
                            $stringRequired = 'required="required"';
                        }
                        
                        $stringPlaceholder = "";
                        if(!is_null($formBuilder->placeholder) && $formBuilder->placeholder!=''){
                            $stringPlaceholder = ' placeholder="' . $formBuilder->placeholder . '"';
                        }
                        
                        
                        switch ($typeValue) {
                            case "submit":
                                $content = $content . '<a id="buttonFormBuilder" name="' . $formBuilder->name . '" class="' . $formBuilder->css . '">' . $formBuilder->description . '</a>'; 
                                break;

                            case "input":

                                $valueDefault = $formBuilder->value;
                                if($param != ""){
                                    if(isset($param)){
                                        if(array_key_exists($formBuilder->name, $param)){
                                            $valueDefault = $param[$formBuilder->name];
                                        }
                                    }
                                }

                                $content = $content . '<label for="' . $formBuilder->name . '">' . $formBuilder->label . '</label>';
                                if($formBuilder->description != '' && !is_null($formBuilder->description)){
                                    $content = $content . '<p>' . $formBuilder->description . '</p>';
                                }

                                $content = $content . '<input ' . $stringPlaceholder  . ' data-description="' . $formBuilder->label . '" id="' . $formBuilder->name . '" name="' . $formBuilder->name . '" class="' . $formBuilder->css . '" ' . $stringRequired . ' value="' . $valueDefault . '">'; 
                                break; 
                            case "input-hidden":
                                $valueDefault = $formBuilder->value;
                                if($param != ""){
                                    if(isset($param)){
                                        if(array_key_exists($formBuilder->name, $param)){
                                            $valueDefault = $param[$formBuilder->name];
                                        }
                                    }
                                }
                                $content = $content . '<input type="hidden" data-description="' . $formBuilder->label . '" id="' . $formBuilder->name . '" name="' . $formBuilder->name . '" value="' . $formBuilder->value . '" class="' . $formBuilder->css . '" value="' . $valueDefault . '">'; 
                                break; 
                            case "input-number":
                                $valueDefault = $formBuilder->value;
                                if($param != ""){
                                    if(isset($param)){
                                        if(array_key_exists($formBuilder->name, $param)){
                                            $valueDefault = $param[$formBuilder->name];
                                        }
                                    }
                                }
                                $content = $content . '<label for="' . $formBuilder->name . '">' . $formBuilder->label . '</label>';
                                if($formBuilder->description != '' && !is_null($formBuilder->description)){
                                    $content = $content . '<p>' . $formBuilder->description . '</p>';
                                }
                                $content = $content . '<input ' . $stringPlaceholder  . ' data-description="' . $formBuilder->label . '" type="number" id="' . $formBuilder->name . '" name="' . $formBuilder->name . '" class="' . $formBuilder->css . '" ' . $stringRequired . ' value="' . $valueDefault . '">'; 
                                break;
                            case "input-email":
                                $valueDefault = $formBuilder->value;
                                if($param != ""){
                                    if(isset($param)){
                                        if(array_key_exists($formBuilder->name, $param)){
                                            $valueDefault = $param[$formBuilder->name];
                                        }
                                    }
                                }
                                $content = $content . '<label for="' . $formBuilder->name . '">' . $formBuilder->label . '</label>';
                                if($formBuilder->description != '' && !is_null($formBuilder->description)){
                                    $content = $content . '<p>' . $formBuilder->description . '</p>';
                                }
                                $content = $content . '<input ' . $stringPlaceholder  . ' data-description="' . $formBuilder->label . '" type="email" id="' . $formBuilder->name . '" name="' . $formBuilder->name . '" class="' . $formBuilder->css . '" ' . $stringRequired . ' value="' . $valueDefault . '">'; 
                                break;
                            case "textarea":
                                $valueDefault = $formBuilder->value;
                                if($param != ""){
                                    if(isset($param)){
                                        if(array_key_exists($formBuilder->name, $param)){
                                            $valueDefault = $param[$formBuilder->name];
                                        }
                                    }
                                }
                                $content = $content . '<label for="' . $formBuilder->name . '">' . $formBuilder->label . '</label>';
                                if($formBuilder->description != '' && !is_null($formBuilder->description)){
                                    $content = $content . '<p>' . $formBuilder->description . '</p>';
                                }
                                $content = $content . '<textarea ' . $stringPlaceholder  . ' data-description="' . $formBuilder->label . '" id="' . $formBuilder->name . '" name="' . $formBuilder->name . '" class="' . $formBuilder->css . '" ' . $stringRequired . '>' . $valueDefault . '</textarea>'; 
                                break; 
                            case "select":

                                $valueDefault = '';
                                if($param != ""){
                                    if(isset($param)){
                                        if(array_key_exists($formBuilder->name, $param)){
                                            $valueDefault = $param[$formBuilder->name];
                                        }
                                    }
                                }

                                $description = "";
                                $img = "";
                                $idFatherValueField = "";
                                $content = $content . '<div class="container-select-form-builder">';
                                $content = $content . '<div>';
                                $content = $content . '<label for="' . $formBuilder->name . '">' . $formBuilder->label . '</label>';
                                $content = $content . '<select data-description="' . $formBuilder->label . '" id="' . $formBuilder->name . '" name="' . $formBuilder->name . '" class="' . $formBuilder->css . '" ' . $stringRequired . '>';
                                
                                
                                $valueCheckedDefault = ValueFormBuilderField::where('form_builder_field_id', $formBuilder->id)->where('default', true)->orderBy('order')->get()->first();
                                if($valueDefault == ''){
                                    if(isset($valueCheckedDefault)){
                                        $valueDefault = $valueCheckedDefault->value;
                                    }else{
                                        $content = $content . '<option value="">-</option>';
                                    }
                                }else{
                                    if(!isset($valueCheckedDefault)){
                                        $content = $content . '<option value="">-</option>';
                                    }
                                }

                                $valueFormBuilderField = ValueFormBuilderField::where('form_builder_field_id', $formBuilder->id)->orderBy('id')->get();
                                if(isset($valueFormBuilderField)){
                                    foreach ($valueFormBuilderField as $valueFormBuilderFields) {
                                       
                                        if($valueFormBuilderFields->value != '' && !is_null($valueFormBuilderFields->value)){
                                            $checked = "";
                                            if($valueDefault == $valueFormBuilderFields->value){
                                                $idFatherValueField = $valueFormBuilderFields->id;
                                                $checked = "selected";

                                                if($valueFormBuilderFields->description != '' && !is_null($valueFormBuilderFields->description)){
                                                    $description = $valueFormBuilderFields->description ;
                                                }
                                                if($valueFormBuilderFields->img != '' && !is_null($valueFormBuilderFields->description)){
                                                    $img = '<img src="' . $valueFormBuilderFields->img . '" class="img-form-builder-select">';
                                                }
                                            }
                                            $content = $content . '<option ' . $checked . ' value="' . $valueFormBuilderFields->value . '">' . $valueFormBuilderFields->value . '</option>';
                                        }
                                    }
                                }
                                $content = $content . '</select>'; 
                                $content = $content . '<div class="container-info-select">';
                                if($description!= ''){
                                    $content =  $content . '<div class="container-description-select"><p>' . $description . '</p></div>';
                                }
                                if($img!= ''){
                                    $content =  $content . '<div class="container-img-select">' . $img . '</div>';
                                }
                                $content = $content . '</div>';
                                $content = $content . '</div>';
                                if($idFatherValueField != ''){
                                    $valueDefaultPlus = '';
                                    if($param != ""){
                                        if(isset($param)){
                                            if(array_key_exists($formBuilder->name. 'plus', $param)){
                                                $valueDefaultPlus = $param[$formBuilder->name. 'plus'];
                                            }
                                        }
                                    }

                                    $valueFatherFormBuilderField = ValueFatherFormBuilderField::where('value_form_builder_field_id', $idFatherValueField)->where('value', '<>', '')->orderBy('id')->get();
                                    if(isset($valueFatherFormBuilderField) && count($valueFatherFormBuilderField)>0){
                                        
                                        $valueFatherCheckedDefault = ValueFatherFormBuilderField::where('value_form_builder_field_id', $idFatherValueField)->where('default', true)->orderBy('order')->get()->first();
                                        
                                        $content = $content . '<div>';
                                        $content = $content . '<label>&nbsp;</label>';
                                        $content = $content . '<select data-description="' . $formBuilder->label . 'plus" id="' . $formBuilder->name . 'plus" name="' . $formBuilder->name . 'plus" class="' . $formBuilder->css . '">';
                                        
                                        if($valueDefaultPlus == ''){
                                            if(isset($valueFatherCheckedDefault)){
                                                $valueDefaultPlus = $valueFatherCheckedDefault->value;
                                            }else{
                                                $content = $content . '<option value="">-</option>';
                                            }
                                        }else{
                                            if(!isset($valueFatherCheckedDefault)){
                                                $content = $content . '<option value="">-</option>';
                                            }
                                        }

                                        foreach ($valueFatherFormBuilderField as $valueFatherFormBuilderFields) {
                                       
                                            if($valueFatherFormBuilderFields->value != '' && !is_null($valueFatherFormBuilderFields->value)){
                                                $checked = "";
                                                if($valueDefaultPlus == $valueFatherFormBuilderFields->value){
                                                    $checked = "selected";
    
                                                    if($valueFormBuilderFields->description != '' && !is_null($valueFatherFormBuilderFields->description)){
                                                        $description = $valueFatherFormBuilderFields->description ;
                                                    }
                                                    
                                                }
                                                $content = $content . '<option ' . $checked . ' value="' . $valueFatherFormBuilderFields->value . '">' . $valueFatherFormBuilderFields->value . '</option>';
                                            }
                                        }
                                        $content = $content . '</select>';
                                        $content = $content . '</div>';
                                    }
                                }
                                $content = $content . '</div>';

                                break;
                            case "checkbox":
                                $valueDefault = $formBuilder->value;
                                if($param != ""){
                                    if(isset($param)){
                                        if(array_key_exists($formBuilder->name, $param)){
                                            $valueDefault = $param[$formBuilder->name];
                                        }
                                    }
                                }

                                $description = "";
                                $img = "";
                                $content = $content . '<div class="content-form-builder-radiobutton">'; 
                                $content = $content . '<label for="' . $formBuilder->name . '">' . $formBuilder->label . '</label><br>';
                                if($formBuilder->description != '' && !is_null($formBuilder->description)){
                                    $content = $content . '<p>' . $formBuilder->description . '</p>';
                                }

                                $valueCheckedDefault = ValueFormBuilderField::where('form_builder_field_id', $formBuilder->id)->where('default', true)->orderBy('order')->get()->first();
                                if($valueDefault == ''){
                                    if(isset($valueCheckedDefault)){
                                        $valueDefault = $valueCheckedDefault->value;
                                    }else{
                                        $content = $content . '<option value="">-</option>';
                                    }
                                }else{
                                    if(!isset($valueCheckedDefault)){
                                        $content = $content . '<option value="">-</option>';
                                    }
                                }

                                $valueFormBuilderField = ValueFormBuilderField::where('form_builder_field_id', $formBuilder->id)->orderBy('id')->get();
                                if(isset($valueFormBuilderField)){
                                    foreach ($valueFormBuilderField as $valueFormBuilderFields) {
                                        if($valueFormBuilderFields->value != '' && !is_null($valueFormBuilderFields->value)){

                                            $checked = "";
                                            if($valueDefault == $valueFormBuilderFields->value){
                                                $checked = "checked";
                                                if($valueFormBuilderFields->description != '' && !is_null($valueFormBuilderFields->description)){
                                                    $description = $valueFormBuilderFields->description ;
                                                }
                                            }



                                            $content = $content . '<input ' . $checked . ' data-description="' . $formBuilder->label . '" type="checkbox" id="' . $valueFormBuilderFields->name . '" name="' . $formBuilder->name . '" class="' . $formBuilder->css . '" ' . $stringRequired . ' value="' . $valueFormBuilderFields->value . '">';
                                            if(!is_null($valueFormBuilderFields->img) && $valueFormBuilderFields->img != ''){
                                                $content = $content . '<img src="' . $valueFormBuilderFields->img . '" class="img-form-builder-checkbox">';
                                            }
                                            $content = $content . '<label for="' . $formBuilder->name . '">' . $valueFormBuilderFields->value . '</label>';
                                        }
                                    }
                                }
                                $content = $content . '</div>';
                                if($description!= ''){
                                    $content =  $content . '<div class="container-description-checkbox"><p>' . $description . '</p></div>';
                                }
                                break;
                            case "radiobutton":
                                $valueDefault = $formBuilder->value;
                                if($param != ""){
                                    if(isset($param)){
                                        if(array_key_exists($formBuilder->name, $param)){
                                            $valueDefault = $param[$formBuilder->name];
                                        }
                                    }
                                }
                                $description = "";
                                $img = "";

                                $content = $content . '<div class="content-form-builder-radiobutton">'; 
                                $content = $content . '<label for="' . $formBuilder->name . '">' . $formBuilder->label . '</label><br>';
                                if($formBuilder->description != '' && !is_null($formBuilder->description)){
                                    $content = $content . '<p class="description-subtitle-form-field">' . $formBuilder->description . '</p>';
                                }

                                $valueCheckedDefault = ValueFormBuilderField::where('form_builder_field_id', $formBuilder->id)->where('default', true)->orderBy('order')->get()->first();
                                if($valueDefault == ''){
                                    if(isset($valueCheckedDefault)){
                                        $valueDefault = $valueCheckedDefault->value;
                                    }else{
                                        $content = $content . '<option value="">-</option>';
                                    }
                                }else{
                                    if(!isset($valueCheckedDefault)){
                                        $content = $content . '<option value="">-</option>';
                                    }
                                }

                                $valueFormBuilderField = ValueFormBuilderField::where('form_builder_field_id', $formBuilder->id)->orderBy('id')->get();
                                if(isset($valueFormBuilderField)){
                                    foreach ($valueFormBuilderField as $valueFormBuilderFields) {
                                        if($valueFormBuilderFields->value != '' && !is_null($valueFormBuilderFields->value)){
                                            $checked = "";
                                            if($valueDefault == $valueFormBuilderFields->value){
                                                $checked = "checked";
                                                if($valueFormBuilderFields->description != '' && !is_null($valueFormBuilderFields->description)){
                                                    $description = $valueFormBuilderFields->description ;
                                                }
                                            }

                                            $content = $content . '<input ' . $checked . ' data-description="' . $formBuilder->label . '" type="radio" id="' . $valueFormBuilderFields->value . '" name="' . $formBuilder->name . '" class="' . $formBuilder->css . '" ' . $stringRequired . ' value="' . $valueFormBuilderFields->value . '">';
                                            if(!is_null($valueFormBuilderFields->img) && $valueFormBuilderFields->img != ''){
                                                $content = $content . '<img src="' . $valueFormBuilderFields->img . '" class="img-form-builder-radiobutton">';
                                            }
                                            $content = $content . '<label for="html">' . $valueFormBuilderFields->value . '</label>';
                                        }
                                    }
                                }
                                $content = $content . '</div>';
                                if($description!= ''){
                                    $content =  $content . '<div class="container-description-checkbox"><p>' . $description . '</p></div>';
                                }

                                break;
                            default:
                                $valueDefault = $formBuilder->value;
                                if($param != ""){
                                    if(isset($param)){
                                        if(array_key_exists($formBuilder->name, $param)){
                                            $valueDefault = $param[$formBuilder->name];
                                        }
                                    }
                                }
                                $content = $content . '<label for="' . $formBuilder->name . '">' . $formBuilder->label . '</label>';
                                $content = $content . '<input ' . $stringPlaceholder  . ' data-description="' . $formBuilder->label . '" id="' . $formBuilder->name . '" name="' . $formBuilder->name . '" class="' . $formBuilder->css . '" ' . $stringRequired . '>';  
                        }

                        $content = $content . '</div>';
                    }
                    $newContent = str_replace('#form#', '<div class="row">' . $content .'</div>',  $newContent);
                }
                $finalHtml = $finalHtml . $newContent;
            }
            
        }

        return '<form id="formBuilder" class="formBuilder" ><input data-description="Id Form Builder" type="hidden" id="id-form-builder" name="id-form-builder" value="' . $idForm . '">' . $strEmailHidden . $finalHtml . '</form>';
    }

    /**
     * Get all by id
     * 
     * @param int idLanguage
     * 
     * @return DtoFormBuilder
     */
    public static function getAllById($idLanguage, $id)
    {
        $result = collect();
        foreach (DB::table('form_builder_field')->where('form_builder_id', $id)->get() as $optionalAll) {
            $dtoOptional = new DtoFormBuilderField();
            $dtoOptional->id = $optionalAll->id;
            $dtoOptional->form_builder_id = $optionalAll->form_builder_id;
            $dtoOptional->description = $optionalAll->description;

            $type = FormBuilderFieldType::where('id', $optionalAll->type)->get()->first();
            if(isset($type)){
                $dtoOptional->typeDescription = $type->value;
            }

            $dtoOptional->type = $optionalAll->type;
            $dtoOptional->label = $optionalAll->label;
            $dtoOptional->value = $optionalAll->value;
            $dtoOptional->width = $optionalAll->width;
            $dtoOptional->order = $optionalAll->order;
            $dtoOptional->css = $optionalAll->css;
            $dtoOptional->required = $optionalAll->required;
            $dtoOptional->placeholder = $optionalAll->placeholder;
            $dtoOptional->name = $optionalAll->name;
            $dtoOptional->form_builder_type_id = $optionalAll->form_builder_type_id;

            if($type == '6' || $type == '7' || $type == '8'){
                $dtoOptional->showValueFormBuilderField = true;
            }else{
                $dtoOptional->showValueFormBuilderField = false;
            }

            $valueFormBuilderField = ValueFormBuilderField::where('form_builder_field_id', $optionalAll->id)->where('form_builder_type_id', $optionalAll->type)->orderBy('order')->get(); 
            if(isset($valueFormBuilderField)){ 
                foreach($valueFormBuilderField as $valueFormBuilderFields){
                    
                    $dtoValueFormBuilderField = new DtoValueFormBuilderField();
                    $dtoValueFormBuilderField->id = $valueFormBuilderFields->id;
                    $dtoValueFormBuilderField->value = $valueFormBuilderFields->value;
                    $dtoValueFormBuilderField->old = true;
                    $dtoValueFormBuilderField->base64 = '';
                    $dtoValueFormBuilderField->img = $valueFormBuilderFields->img;
                    $dtoValueFormBuilderField->description = $valueFormBuilderFields->description;

                    if(is_null($valueFormBuilderFields->default) || $valueFormBuilderFields->default == ''){
                        $dtoValueFormBuilderField->default = false;
                        $dtoValueFormBuilderField->checked = "";
                    }else{
                        $dtoValueFormBuilderField->default = true;
                        $dtoValueFormBuilderField->checked = "checked";
                    }
                    $dtoValueFormBuilderField->order = $valueFormBuilderFields->order;

                    if($type == '6'){
                        $dtoValueFormBuilderField->showFieldImage = true;
                    }else{
                        $dtoValueFormBuilderField->showFieldImage = true;
                    }

                    $valueFatherFormBuilderField = ValueFatherFormBuilderField::where('value_form_builder_field_id', $valueFormBuilderFields->id)->get();
                    if(isset($valueFatherFormBuilderField)){
                        foreach($valueFatherFormBuilderField as $valueFatherFormBuilderFields){
                            $DtoValueFatherFormBuilderField = new DtoValueFatherFormBuilderField();
                            $DtoValueFatherFormBuilderField->id = $valueFatherFormBuilderFields->id;
                            $DtoValueFatherFormBuilderField->value = $valueFatherFormBuilderFields->value;
                            $DtoValueFatherFormBuilderField->old = true;
                            $DtoValueFatherFormBuilderField->base64 = '';
                            $DtoValueFatherFormBuilderField->img = $valueFatherFormBuilderFields->img;
                            $DtoValueFatherFormBuilderField->description = $valueFatherFormBuilderFields->description;

                            if(is_null($valueFatherFormBuilderFields->default) || $valueFatherFormBuilderFields->default == ''){
                                $DtoValueFatherFormBuilderField->default = false;
                                $DtoValueFatherFormBuilderField->checked = "";
                            }else{
                                $DtoValueFatherFormBuilderField->default = true;
                                $DtoValueFatherFormBuilderField->checked = "checked";
                            }
                            $DtoValueFatherFormBuilderField->order = $valueFatherFormBuilderFields->order;

                            $dtoValueFormBuilderField->valueFatherFormBuilderField->push($DtoValueFatherFormBuilderField);
                        }
                    }

                    $dtoOptional->valueFormBuilderField->push($dtoValueFormBuilderField);
                }
            }
            $result->push($dtoOptional); 
        }
        
        return $result;
    }

    /**
     * Get select
     *
     * @param String $search
     * @return List
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return FormBuilderFieldType::select('id', 'text as description')->get();
        } else {
            return FormBuilderFieldType::where('text', 'ilike', $search . '%')->select('id', 'text as description')->get();
        }
    }
    
    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $optional = new FormBuilder();
            $optional->description = $request->description;
            $optional->object = $request->object;
            $optional->email_destination = $request->email_destination;
            $optional->feedback_message = $request->feedback_message;
            $optional->message_company = $request->message_company;
            $optional->message_customer = $request->message_customer;
            $optional->feedback_redirect = $request->feedback_redirect;
            $optional->feedback_url_redirect = $request->feedback_url_redirect;
            $optional->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        //return $optional->id;
    }

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertFormBuilderField(Request $request, int $idUser, int $idLanguage)
    {
        //static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $formBuilderField = new FormBuilderField();
            $formBuilderField->type = $request->type;
            $formBuilderField->label = $request->label;
            $formBuilderField->description = $request->description;
            $formBuilderField->form_builder_id = $request->form_builder_id;
            $formBuilderField->value = $request->value;
            $formBuilderField->width = $request->width;
            $formBuilderField->order = $request->order;
            $formBuilderField->css = $request->css;
            $formBuilderField->required = $request->required;
            $formBuilderField->form_builder_type_id = $request->form_builder_type_id;
            $formBuilderField->name = $request->name;
            $formBuilderField->placeholder = $request->placeholder;
            $formBuilderField->save();

            if(isset($request->valueFormBuilderField)){
                if(count($request->valueFormBuilderField) > 0){
                    foreach($request->valueFormBuilderField as $valueFormBuilderField){
                        $valueFormBuilderFieldDb = new ValueFormBuilderField();
                        $valueFormBuilderFieldDb->form_builder_field_id = $formBuilderField->id;
                        $valueFormBuilderFieldDb->form_builder_type_id = $request->type;
                        $valueFormBuilderFieldDb->value = $valueFormBuilderField['value'];
                        $valueFormBuilderFieldDb->img = $valueFormBuilderField['img'];
                        $valueFormBuilderFieldDb->description = $valueFormBuilderField['description'];
                        $valueFormBuilderFieldDb->default = $valueFormBuilderField['default'];

                        //controllo creazione immagini per form builder
                        if(isset($valueFormBuilderField['base64'])){
                            if($valueFormBuilderField['base64'] != '' && !is_null($valueFormBuilderField['base64'])){
                                Image::make($valueFormBuilderField['base64'])->save(static::$dirImage . $formBuilderField->id . $valueFormBuilderField['img']);
                                $valueFormBuilderFieldDb->img = static::getUrl() . $formBuilderField->id . $valueFormBuilderField['img'];
                            }else{
                                $valueFormBuilderFieldDb->img = $valueFormBuilderField['img'];
                            } 
                        }else{
                            $valueFormBuilderFieldDb->img = $valueFormBuilderField['img']; 
                        }

                        $valueFormBuilderFieldDb->order = $valueFormBuilderField['order'];                        
                        $valueFormBuilderFieldDb->save();
                    }
                }
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        //return $optional->id;
    }

    /**
     * insert Contact Form Builder
     * 
     * @param Request request
     * 
     * @return int
     */
    public static function insertContactFormBuilder(Request $request)
    {
        $emailContact = '';
        $nameContact = '';
        $surnameContact = '';
        $return = '';
        $idFormBuilder = '';

        if(isset($request)){
            foreach ($request[0] as $fieldFormBuilder) {
                foreach ($fieldFormBuilder as $key => $value) {
                    if($key == "email"){
                        $emailContact = $value;
                    }
                    if($key == "name"){
                        $nameContact = $value;
                    }
                    if($key == "surname"){
                        $surnameContact = $value;
                    }
                    if($key == "id-form-builder"){
                        $idFormBuilder = $value;
                    }
                }
            }
        }


        if($emailContact == '' || $idFormBuilder == ''){
            $dtoFromBuilder = new DtoFormBuilder();
            $dtoFromBuilder->feedback_message = 'Ops.. qualcosa è andato storto, riprovare più tardi! No id o email';
            $dtoFromBuilder->feedback_redirect = false;
            $dtoFromBuilder->feedback_url_redirect = '';
            $dtoFromBuilder->code = '500';
            $return = $dtoFromBuilder;
        }else{

            $fromBuilderOnDB = FormBuilder::find($idFormBuilder);
            if(isset($fromBuilderOnDB)){
                DB::beginTransaction();

                try {
                    $contact = new Contact();
                    $contact->name = $nameContact;
                    $contact->surname = $surnameContact;
                    $contact->email = $emailContact;
                    $contact->save();

                    if(isset($request)){
                        foreach ($request[0] as $fieldFormBuilder) {
                            if(isset($fieldFormBuilder['description'])){
                                $description = $fieldFormBuilder['description'];
                            }else{
                                $description = '';
                            }
                            foreach ($fieldFormBuilder as $key => $value) {
                                if($key != 'description' && $key != 'id-form-builder'){
                                    $contactFormBuilder = new ContactFormBuilder();
                                    $contactFormBuilder->contact_id = $contact->id;
                                    $contactFormBuilder->form_builder_id = $idFormBuilder;
                                    $contactFormBuilder->field = $key;
                                    $contactFormBuilder->value = $value;
                                    $contactFormBuilder->description = $description;
                                    $contactFormBuilder->save();
                                }
                            }
                        }
                    }

                    //send email
                    $textObject= $fromBuilderOnDB->object;

                    $textMail= MessageBL::getByCode('CONFIRM_REQUEST_FORM_BUILDER_TEXT', 1); 
                    $newContent  = '';
                    $finalHtml = '';
                    $textMailAzi = '';
                    $textMailCli = '';

                    if (strpos($textMail, '#form_builder_field#') !== false) {
                        $idContent = Content::where('code', 'FormBuilderField')->first();
                        $htmlContent = ContentLanguage::where('content_id', $idContent->id)->first();
                        $fromBuilderFielByContactOnDB = ContactFormBuilder::where('contact_id', $contact->id)->where('form_builder_id', $idFormBuilder)->get();
                        
                        if(isset($fromBuilderFielByContactOnDB)){
                            foreach ($fromBuilderFielByContactOnDB as $fieldContactFormBuilder) {
                                $newContent = $htmlContent->content;
                                $newContent = str_replace('#description#', $fieldContactFormBuilder->description,  $newContent);
                                $newContent = str_replace('#value#', $fieldContactFormBuilder->value,  $newContent);
                                $finalHtml = $finalHtml . $newContent;
                            }

                            $textMail = str_replace('#form_builder_field#', $finalHtml, $textMail);
                        }else{
                            $textMail = str_replace('#form_builder_field#', '', $textMail);
                        }
                    }   

                    if (strpos($textMail, '#feedback_message#') !== false) {
                        if($fromBuilderOnDB->message_company != '' && !is_null($fromBuilderOnDB->message_company)){
                            $textMailAzi = str_replace('#feedback_message#', $fromBuilderOnDB->message_company, $textMail);
                        }else{
                            $textMailAzi = $textMail;
                        }

                        if($fromBuilderOnDB->message_customer != '' && !is_null($fromBuilderOnDB->message_customer)){
                            $textMailCli = str_replace('#feedback_message#', $fromBuilderOnDB->message_customer, $textMail);
                        }else{
                            $textMailCli = $textMail;
                        }
                    }


                    Mail::to($fromBuilderOnDB->email_destination)->send(new DynamicMail($textObject, $textMailAzi)); 

                    if($emailContact !=''){
                        Mail::to($emailContact)->send(new DynamicMail($textObject, $textMailCli)); 
                    }

                    $dtoFromBuilder = new DtoFormBuilder();
                    $dtoFromBuilder->feedback_message = $fromBuilderOnDB->feedback_message;
                    $dtoFromBuilder->feedback_redirect = $fromBuilderOnDB->feedback_redirect;
                    $dtoFromBuilder->feedback_url_redirect = $fromBuilderOnDB->feedback_url_redirect;
                    $dtoFromBuilder->code = '200';

                    $return = $dtoFromBuilder;

                    DB::commit();
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
            }else{
                $dtoFromBuilder = new DtoFormBuilder();
                $dtoFromBuilder->feedback_message = 'Ops.. qualcosa è andato storto, riprovare più tardi! no id form';
                $dtoFromBuilder->feedback_redirect = false;
                $dtoFromBuilder->feedback_url_redirect = '';
                $dtoFromBuilder->code = '500';
                $return = $dtoFromBuilder;
            }
            
        }
        return $return;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoOptional
     * @param int idLanguage
     */
    public static function update(Request $dtoOptional, int $idLanguage)
    {
        static::_validateData($dtoOptional, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $optionalOnDb = FormBuilder::find($dtoOptional->id);
        
        DB::beginTransaction();

        try {
            $optionalOnDb->description = $dtoOptional->description;
            $optionalOnDb->object = $dtoOptional->object;
            $optionalOnDb->email_destination = $dtoOptional->email_destination;
            $optionalOnDb->feedback_message = $dtoOptional->feedback_message;
            $optionalOnDb->message_company = $dtoOptional->message_company;
            $optionalOnDb->message_customer = $dtoOptional->message_customer;
            $optionalOnDb->feedback_redirect = $dtoOptional->feedback_redirect;
            $optionalOnDb->feedback_url_redirect = $dtoOptional->feedback_url_redirect;
            $optionalOnDb->save();
           
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    /**
     * Update
     * 
     * @param Request dtoOptional
     * @param int idLanguage
     */
    public static function updateFormBuilderField(Request $dtoOptional, int $idLanguage)
    {
        //static::_validateData($dtoOptional, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $optionalOnDb = FormBuilderField::find($dtoOptional->id);
        
        DB::beginTransaction();

        try {
            $optionalOnDb->type = $dtoOptional->type;
            $optionalOnDb->label = $dtoOptional->label;
            $optionalOnDb->description = $dtoOptional->description;
            $optionalOnDb->value = $dtoOptional->value;
            $optionalOnDb->width = $dtoOptional->width;
            $optionalOnDb->order = $dtoOptional->order;
            $optionalOnDb->css = $dtoOptional->css;
            $optionalOnDb->required = $dtoOptional->required;
            $optionalOnDb->form_builder_type_id = $dtoOptional->form_builder_type_id;
            $optionalOnDb->name = $dtoOptional->name;
            $optionalOnDb->placeholder = $dtoOptional->placeholder;
            $optionalOnDb->save();
           
            $valueFormBuilderFieldOnDb = ValueFormBuilderField::where('form_builder_field_id', $optionalOnDb->id)->get();
            if(isset($valueFormBuilderFieldOnDb)){
                foreach($valueFormBuilderFieldOnDb as $valueFormBuilderFieldOnDbs){
                    $valueFatherFormBuilderFieldOnDbDelete = ValueFatherFormBuilderField::where('value_form_builder_field_id', $valueFormBuilderFieldOnDbs->id);
                    $valueFatherFormBuilderFieldOnDbDelete->delete();
                }
            }
            


            $ValueFormBuilderFieldOnDBDelete = ValueFormBuilderField::where('form_builder_field_id', $optionalOnDb->id);

            $ValueFormBuilderFieldOnDBDelete->delete();

            if(isset($dtoOptional->valueFormBuilderField)){
                if(count($dtoOptional->valueFormBuilderField) > 0){
                    foreach($dtoOptional->valueFormBuilderField as $valueFormBuilderField){

                        $valueFormBuilderFieldDb = new ValueFormBuilderField();
                        $valueFormBuilderFieldDb->form_builder_field_id = $optionalOnDb->id;
                        $valueFormBuilderFieldDb->form_builder_type_id = $dtoOptional->type;
                        $valueFormBuilderFieldDb->value = $valueFormBuilderField['value'];
                        $valueFormBuilderFieldDb->description = $valueFormBuilderField['description'];
                        $valueFormBuilderFieldDb->default = $valueFormBuilderField['default'];

                        //controllo creazione immagini per form builder
                        if(isset($valueFormBuilderField['base64'])){
                            if($valueFormBuilderField['base64'] != '' && !is_null($valueFormBuilderField['base64'])){
                                Image::make($valueFormBuilderField['base64'])->save(static::$dirImage . $optionalOnDb->id . $valueFormBuilderField['img']);
                                $valueFormBuilderFieldDb->img = static::getUrl() . $optionalOnDb->id . $valueFormBuilderField['img'];
                            }else{
                                $valueFormBuilderFieldDb->img = $valueFormBuilderField['img']; 
                            }
                        }else{
                            $valueFormBuilderFieldDb->img = $valueFormBuilderField['img']; 
                        }

                        $valueFormBuilderFieldDb->order = $valueFormBuilderField['order'];
                        $valueFormBuilderFieldDb->save();

                        if(isset($valueFormBuilderField['valueFatherFormBuilderField'])){
                            if(count($valueFormBuilderField['valueFatherFormBuilderField']) > 0){
                                foreach($valueFormBuilderField['valueFatherFormBuilderField'] as $valueFatherFormBuilderField){
                                    $valueFatherFormBuilderFieldNew = new ValueFatherFormBuilderField();
                                    $valueFatherFormBuilderFieldNew->value_form_builder_field_id = $valueFormBuilderFieldDb->id;
                                    $valueFatherFormBuilderFieldNew->value = $valueFatherFormBuilderField['value'];
                                    $valueFatherFormBuilderFieldNew->default = $valueFatherFormBuilderField['default'];
                                    if(isset($valueFatherFormBuilderField['description'])){
                                        $valueFatherFormBuilderFieldNew->description = $valueFatherFormBuilderField['description'];
                                    }
                                    //controllo creazione immagini per form builder
                                    if(isset($valueFatherFormBuilderField['base64'])){
                                        if($valueFatherFormBuilderField['base64'] != '' && !is_null($valueFatherFormBuilderField['base64'])){
                                            Image::make($valueFatherFormBuilderField['base64'])->save(static::$dirImage . $optionalOnDb->id . $valueFatherFormBuilderField['img']);
                                            $valueFatherFormBuilderFieldNew->img = static::getUrl() . $optionalOnDb->id . $valueFatherFormBuilderField['img'];
                                        }else{
                                            $valueFatherFormBuilderFieldNew->img = $valueFatherFormBuilderField['img']; 
                                        }
                                    }else{
                                        $valueFatherFormBuilderFieldNew->img = $valueFatherFormBuilderField['img']; 
                                    }

                                    $valueFatherFormBuilderFieldNew->order = $valueFatherFormBuilderField['order'];
                                    $valueFatherFormBuilderFieldNew->save();
                                }
                            }
                        }
                    }
                }
            }


            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }


    /**
     * Clone
     * 
     * @param int id
     * @param int idFunctionality
     * @param int idLanguage
     * 
     * @return int
     */
    public static function clone(int $id, int $idFunctionality, int $idUser, $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $formBuilderOnDB = FormBuilder::find($id);

        if (is_null($formBuilderOnDB)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PageNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        DB::beginTransaction();

        try {

            $formBuilderNew = new FormBuilder();
            $formBuilderNew->description = $formBuilderOnDB->description . ' - Copy';
            $formBuilderNew->object = $formBuilderOnDB->object;
            $formBuilderNew->email_destination = $formBuilderOnDB->email_destination;
            $formBuilderNew->feedback_message = $formBuilderOnDB->feedback_message;
            $formBuilderNew->message_company = $formBuilderOnDB->message_company;
            $formBuilderNew->message_customer = $formBuilderOnDB->message_customer;
            $formBuilderNew->feedback_redirect = $formBuilderOnDB->feedback_redirect;
            $formBuilderNew->feedback_url_redirect = $formBuilderOnDB->feedback_url_redirect;
            $formBuilderNew->save();


            $formBuilderFieldOnDB = FormBuilderField::where('form_builder_id', $id)->get();
            if(isset($formBuilderFieldOnDB)){
                foreach($formBuilderFieldOnDB as $formBuilderFieldOnDBs){
                    $formBuilderFieldNew = new FormBuilderField();
                    $formBuilderFieldNew->type = $formBuilderFieldOnDBs->type;
                    $formBuilderFieldNew->label = $formBuilderFieldOnDBs->label;
                    $formBuilderFieldNew->description = $formBuilderFieldOnDBs->description;
                    $formBuilderFieldNew->form_builder_id = $formBuilderNew->id;
                    $formBuilderFieldNew->value = $formBuilderFieldOnDBs->value;
                    $formBuilderFieldNew->width = $formBuilderFieldOnDBs->width;
                    $formBuilderFieldNew->order = $formBuilderFieldOnDBs->order;
                    $formBuilderFieldNew->css = $formBuilderFieldOnDBs->css;
                    $formBuilderFieldNew->placeholder = $formBuilderFieldOnDBs->placeholder;
                    $formBuilderFieldNew->required = $formBuilderFieldOnDBs->required;
                    $formBuilderFieldNew->form_builder_type_id = $formBuilderFieldOnDBs->form_builder_type_id;
                    $formBuilderFieldNew->name = $formBuilderFieldOnDBs->name;
                    $formBuilderFieldNew->save();

                    $valueFormBuilderFieldOnDB = ValueFormBuilderField::where('form_builder_field_id', $formBuilderFieldOnDBs->id)->get();
                    if(isset($valueFormBuilderFieldOnDB)){
                        foreach($valueFormBuilderFieldOnDB as $valueFormBuilderFieldOnDBs){
                            $valueFormBuilderFieldNew = new ValueFormBuilderField();
                            $valueFormBuilderFieldNew->form_builder_field_id = $formBuilderFieldNew->id;
                            $valueFormBuilderFieldNew->form_builder_type_id = $valueFormBuilderFieldOnDBs->form_builder_type_id;
                            $valueFormBuilderFieldNew->value = $valueFormBuilderFieldOnDBs->value;
                            $valueFormBuilderFieldNew->img = $valueFormBuilderFieldOnDBs->img;
                            $valueFormBuilderFieldNew->description = $valueFormBuilderFieldOnDBs->description;
                            $valueFormBuilderFieldNew->default = $valueFormBuilderFieldOnDBs->default;
                            $valueFormBuilderFieldNew->order = $valueFormBuilderFieldOnDBs->order;                       
                            $valueFormBuilderFieldNew->save();
                            

                            $valueFatherFormBuilderFieldOnDB = ValueFatherFormBuilderField::where('value_form_builder_field_id', $valueFormBuilderFieldOnDBs->id)->get();
                            if(isset($valueFatherFormBuilderFieldOnDB)){
                                foreach($valueFatherFormBuilderFieldOnDB as $valueFatherFormBuilderFieldOnDBs){
                                    $valueFatherFormBuilderFieldNew = new ValueFatherFormBuilderField();
                                    $valueFatherFormBuilderFieldNew->value_form_builder_field_id = $valueFormBuilderFieldNew->id;
                                    $valueFatherFormBuilderFieldNew->value = $valueFatherFormBuilderFieldOnDBs->value;
                                    $valueFatherFormBuilderFieldNew->default = $valueFatherFormBuilderFieldOnDBs->default;
                                    $valueFatherFormBuilderFieldNew->description = $valueFatherFormBuilderFieldOnDBs->description;
                                    $valueFatherFormBuilderFieldNew->img = $valueFatherFormBuilderFieldOnDBs->img; 
                                    $valueFatherFormBuilderFieldNew->order = $valueFatherFormBuilderFieldOnDBs->order;
                                    $valueFatherFormBuilderFieldNew->save();
                                }
                            }
                        }
                    }
                }
            }

            DB::commit();

            return $formBuilderNew->id;
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $optional = FormBuilder::find($id);
        
        if (is_null($optional)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {

            //TO DO Implementare delete sulla tabella ValueFormBuilderField
            $formBuilderFieldOnDb = FormBuilderField::where('form_builder_id', $id)->get();
            if(isset($formBuilderFieldOnDb)){
                foreach($formBuilderFieldOnDb as $formBuilderFieldOnDbs){
                    $valueFormBuilderField = ValueFormBuilderField::where('form_builder_field_id', $formBuilderFieldOnDbs->id);
                    $valueFormBuilderField->delete();
                }
            }

            $optionalLanguages = FormBuilderField::where('form_builder_id', $id);
            $optionalLanguages->delete();

            
            
            $optional->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function deleteFormBuilderField(int $id, int $idLanguage)
    {
        $optional = FormBuilderField::find($id);
        
        if (is_null($optional)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            
            $optionalLanguages = ValueFormBuilderField::where('form_builder_field_id', $id);
            $optionalLanguages->delete();

            $optional->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function deleteValueFormBuilderField(int $id, int $idLanguage)
    {
        $optional = ValueFormBuilderField::find($id);
        
        if (is_null($optional)) {
            throw new Exception('Valore non trovato', HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            
            $valueFatherFormBuilderFieldOnDbDelete = ValueFatherFormBuilderField::where('value_form_builder_field_id', $optional->id);
                    $valueFatherFormBuilderFieldOnDbDelete->delete();

            $optional->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    /**
     * del By Contact And Form Id
     * 
     * @param int id
     * @param int idForm
     */
    public static function delByContactAndFormId(int $id, int $idForm)
    {
        $optional = ContactFormBuilder::where('contact_id', $id)->where('form_builder_id', $idForm);
        
        if (is_null($optional)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            
            $optional->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function deleteValueFatherFormBuilderField(int $id, int $idLanguage)
    {
        $optional = ValueFatherFormBuilderField::find($id);
        
        if (is_null($optional)) {
            throw new Exception('Valore non trovato', HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            
            $optional->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    

    #endregion DELETE
}

<?php

namespace App\BusinessLogic;

use App\GroupTechnicalSpecificationTechnicalSpecification;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use Mockery\CountValidator\Exception;
use App\GroupTechnicalSpecification;
use App\TechincalSpecification;
use App\DtoModel\DtoLanguageTechnicalSpecification;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;

class GroupTechnicalSpecificationTechnicalSpecificationBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param int idGroupTechnicalSpecification
     * @param int idTechnicalSpecification
     * @param int idLanguage
     * @param int idUser
     * @param int idLanguage
     */
    private static function _validateData(int $idGroupTechnicalSpecification, int $idTechnicalSpecification, int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(GroupTechnicalSpecification::find($idGroupTechnicalSpecification))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupTechnicalSpecificationNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(TechincalSpecification::find($idTechnicalSpecification))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::TechincalSpecificationNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by group technical specification and language
     * 
     * @param int $idGroupTechnicalSpecification
     * @param int $idLanguage
     * 
     * @return DtoLanguageTechnicalSpecification
     */
    public static function getInlucedInGroup(int $idGroupTechnicalSpecification, int $idLanguage)
    {
        $result = collect();

        foreach (GroupTechnicalSpecificationTechnicalSpecification::where('group_technical_specification_id', $idGroupTechnicalSpecification)->get() as $groupTechnicalSpecificationTechnicalSpecification) {
            $dtoLanguageTechnicalSpecification = new DtoLanguageTechnicalSpecification();
            $dtoLanguageTechnicalSpecification->id = $groupTechnicalSpecificationTechnicalSpecification->id;
            $dtoLanguageTechnicalSpecification->description = LanguageTechnicalSpecificationBL::getByIdTechnicalSpecificationAndLanguage($groupTechnicalSpecificationTechnicalSpecification->technical_specification_id, $idLanguage);
            $result->push($dtoLanguageTechnicalSpecification);
        }

        return $result;
    }

    /**
     * Get excluded by group technical specification and language
     * 
     * @param int $idGroupTechnicalSpecification
     * @param int $idLanguage
     * 
     * @return DtoLanguageTechnicalSpecification
     */
    public static function getExlucedInGroup(int $idGroupTechnicalSpecification, int $idLanguage)
    {
        $result = collect();

        foreach (DB::select('   SELECT *
                                FROM technicals_specifications
                                WHERE deleted_at IS NULL
                                AND id NOT IN (
                                                SELECT technical_specification_id
                                                FROM groups_technicals_specifications_technicals_specifications
                                                WHERE group_technical_specification_id = ' . $idGroupTechnicalSpecification . ')')
            as $groupTechnicalSpecificationTechnicalSpecification) {
            $dtoLanguageTechnicalSpecification = new DtoLanguageTechnicalSpecification();
            $dtoLanguageTechnicalSpecification->id = $groupTechnicalSpecificationTechnicalSpecification->id;
            $dtoLanguageTechnicalSpecification->description = LanguageTechnicalSpecificationBL::getByIdTechnicalSpecificationAndLanguage($groupTechnicalSpecificationTechnicalSpecification->id, $idLanguage);
            $result->push($dtoLanguageTechnicalSpecification);
        }

        return $result;
    }

    /**
     * Get by technical specification and language
     * 
     * @param int $idTechnicalSpecification
     * @param int $idLanguage
     * 
     * @return DtoLanguageTechnicalSpecification
     */
    public static function getInlucedInTechnicalSpecification(int $idTechnicalSpecification, int $idLanguage)
    {
        $result = collect();

        foreach (GroupTechnicalSpecificationTechnicalSpecification::where('technical_specification_id', $idTechnicalSpecification)->get() as $groupTechnicalSpecificationTechnicalSpecification) {
            $dtoLanguageTechnicalSpecification = new DtoLanguageTechnicalSpecification();
            $dtoLanguageTechnicalSpecification->id = $groupTechnicalSpecificationTechnicalSpecification->id;
            $dtoLanguageTechnicalSpecification->description = GroupTechnicalSpecificationLanguageBL::getByGroupTechnicalSpecificationAndLanguage($groupTechnicalSpecificationTechnicalSpecification->group_technical_specification_id, $idLanguage);
            $result->push($dtoLanguageTechnicalSpecification);
        }

        return $result;
    }

    /**
     * Get excluded by technical specification and language
     * 
     * @param int $idTechnicalSpecification
     * @param int $idLanguage
     * 
     * @return DtoLanguageTechnicalSpecification
     */
    public static function getExlucedInTechnicalSpecification(int $idTechnicalSpecification, int $idLanguage)
    {
        $result = collect();

        foreach (DB::select('   SELECT *
                                FROM groups_technicals_specifications
                                WHERE id NOT IN (
                                                SELECT group_technical_specification_id
                                                FROM groups_technicals_specifications_technicals_specifications
                                                WHERE technical_specification_id = ' . $idTechnicalSpecification . ')')
            as $groupTechnicalSpecificationTechnicalSpecification) {
            $dtoLanguageTechnicalSpecification = new DtoLanguageTechnicalSpecification();
            $dtoLanguageTechnicalSpecification->id = $groupTechnicalSpecificationTechnicalSpecification->id;
            $dtoLanguageTechnicalSpecification->description = GroupTechnicalSpecificationLanguageBL::getByGroupTechnicalSpecificationAndLanguage($groupTechnicalSpecificationTechnicalSpecification->id, $idLanguage);
            $result->push($dtoLanguageTechnicalSpecification);
        }

        return $result;
    }

    #endregion GET

    #region INSERT

    /**
     * Add technical specification
     * 
     * @param int idGroupTechnicalSpecification
     * @param int idTechnicalSpecification
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int id
     */
    public static function insert(int $idGroupTechnicalSpecification, int $idTechnicalSpecification, int $idFunctionality, int $idUser, int $idLanguage)
    {
        static::_validateData($idGroupTechnicalSpecification, $idTechnicalSpecification, $idFunctionality, $idUser, $idLanguage);

        $groupTechnicalSpecificationTechnicalSpecification = GroupTechnicalSpecificationTechnicalSpecification::where('group_technical_specification_id', $idGroupTechnicalSpecification)->where('technical_specification_id', $idTechnicalSpecification)->first();

        if (is_null($groupTechnicalSpecificationTechnicalSpecification)) {
            $groupTechnicalSpecificationTechnicalSpecification = new GroupTechnicalSpecificationTechnicalSpecification();
            $groupTechnicalSpecificationTechnicalSpecification->group_technical_specification_id = $idGroupTechnicalSpecification;
            $groupTechnicalSpecificationTechnicalSpecification->technical_specification_id = $idTechnicalSpecification;
            $groupTechnicalSpecificationTechnicalSpecification->save();
        }

        return $groupTechnicalSpecificationTechnicalSpecification->id;
    }

    /**
     * Clone
     * 
     * @param int oldIdGroupTechnicalSpecification
     * @param int newIdGroupTechnicalSpecification
     */
    public static function clone(int $oldIdGroupTechnicalSpecification, int $newIdGroupTechnicalSpecification)
    {
        foreach (GroupTechnicalSpecificationTechnicalSpecification::where('group_technical_specification_id', $oldIdGroupTechnicalSpecification)->get() as $groupTechnicalSpecificationTechnicalSpecification) {
            $groupTechnicalSpecificationTechnicalSpecificationToInsert = new GroupTechnicalSpecificationTechnicalSpecification();
            $groupTechnicalSpecificationTechnicalSpecificationToInsert->group_technical_specification_id = $newIdGroupTechnicalSpecification;
            $groupTechnicalSpecificationTechnicalSpecificationToInsert->technical_specification_id = $groupTechnicalSpecificationTechnicalSpecification->technical_specification_id;
            $groupTechnicalSpecificationTechnicalSpecificationToInsert->save();
        }
    }

    #endregion INSERT

    #region DELETE

    /**
     * Delete
     * 
     * @param int $id
     */
    public static function delete(int $id, int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(GroupTechnicalSpecificationTechnicalSpecification::find($id))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupTechincalSpecificationTechincalSpecificationNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        GroupTechnicalSpecificationTechnicalSpecification::destroy($id);
    }

    /**
     * Delete by technical specification
     * 
     * @param int idTechnicalSpecification
     */
    public static function deleteByTechnicalSpecification($idTechnicalSpecification)
    {
        GroupTechnicalSpecificationTechnicalSpecification::where('technical_specification_id', $idTechnicalSpecification)->delete();
    }

    /**
     * Delete by grouptechnical specification
     * 
     * @param int idgroupTechnicalSpecification
     */
    public static function deleteByGroupTechnicalSpecification($idGroupTechnicalSpecification)
    {
        GroupTechnicalSpecificationTechnicalSpecification::where('group_technical_specification_id', $idGroupTechnicalSpecification)->delete();
    }

    #endregion DELETE
}

<?php

namespace App\BusinessLogic;

use App\GroupTechnicalSpecificationLanguage;
use App\DtoModel\DtoGroupTechnicalSpecificationLanguage;
use Mockery\CountValidator\Exception;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\Language;

class GroupTechnicalSpecificationLanguageBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param int $idGroupTechnicalSpecification
     * @param Request $dtoGroupTechnicalSpecificationLanguage
     * @param int $idLanguage
     */
    private static function _validateData($idGroupTechnicalSpecification, $dtoGroupTechnicalSpecificationLanguage, int $idLanguage)
    {
        if (is_null($dtoGroupTechnicalSpecificationLanguage['idLanguage'])) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::LanguageNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Language::find($dtoGroupTechnicalSpecificationLanguage['idLanguage']))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::LanguageNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (!is_null($dtoGroupTechnicalSpecificationLanguage['description']) && static::checkExists($dtoGroupTechnicalSpecificationLanguage['description'], $dtoGroupTechnicalSpecificationLanguage['idLanguage'], $idGroupTechnicalSpecification)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupTechnicalSpecificationAlreadyExists), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($dtoGroupTechnicalSpecificationLanguage['description']) && $dtoGroupTechnicalSpecificationLanguage['idLanguage'] == LanguageBL::getDefault()->id) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DefaultLanguageRequired), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by group technical specification and language
     * 
     * @param int $idGroupTechnicalSpecification
     * @param int $idLanguage
     * 
     * @return string
     */
    public static function getByGroupTechnicalSpecificationAndLanguage(int $idGroupTechnicalSpecification, int $idLanguage)
    {
        $groupTechnicalSpecificationLanguage = GroupTechnicalSpecificationLanguage::where('group_technical_specification_id', $idGroupTechnicalSpecification)->where('language_id', $idLanguage)->first();

        if (is_null($groupTechnicalSpecificationLanguage)) {
            $groupTechnicalSpecificationLanguage = GroupTechnicalSpecificationLanguage::where('group_technical_specification_id', $idGroupTechnicalSpecification)->where('language_id', LanguageBL::getDefault()->id)->first();
        }

        if (is_null($groupTechnicalSpecificationLanguage)) {
            return '';
        } else {
            return $groupTechnicalSpecificationLanguage->description;
        }
    }

    /**
     * Get DtoGroupTechnicalSpecificationLanguage
     * 
     * @param int $idGroupTechnicalSpecification
     * 
     * @return DtoGroupTechnicalSpecificationLanguage
     */
    public static function getDtoTable(int $idGroupTechnicalSpecification)
    {
        $dtoTable = collect();

        foreach (GroupTechnicalSpecificationLanguage::where('group_technical_specification_id', $idGroupTechnicalSpecification)->get() as $languageGroupTechnicalSpecificationDistinct) {
            $dtoTable[$languageGroupTechnicalSpecificationDistinct->language_id] = $languageGroupTechnicalSpecificationDistinct->description;
        }

        return $dtoTable;
    }

    /**
     * Check if exist
     * 
     * @param string description
     * @param int idLanguage
     * @param int idGroupTechnicalSpecification
     * 
     * @return bool
     */
    public static function checkExists(string $description, int $idLanguage, int $idGroupTechnicalSpecification = null)
    {
        $found = false;

        if (is_null($idGroupTechnicalSpecification)) {
            if (GroupTechnicalSpecificationLanguage::where('language_id', $idLanguage)->where('description', $description)->count() > 0) {
                $found = true;
            }
        } else {
            if (GroupTechnicalSpecificationLanguage::where('group_technical_specification_id', '!=', $idGroupTechnicalSpecification)->where('language_id', $idLanguage)->where('description', $description)->count() > 0) {
                $found = true;
            }
        }

        return $found;
    }

    #endregion GET

    #region INSERT

    /**
     * Insert request
     * 
     * @param int $idGroupTechnicalSpecification
     * @param Request $dtoGroupTechnicalSpecificationLanguage
     * @param int $idLanguage
     * 
     * @return id
     */
    public static function insertRequest($idGroupTechnicalSpecification, $dtoGroupTechnicalSpecificationLanguage, int $idLanguage)
    {
        static::_validateData($idGroupTechnicalSpecification, $dtoGroupTechnicalSpecificationLanguage, $idLanguage);

        $groupTechnicalSpecificationLanguage = new GroupTechnicalSpecificationLanguage();
        $groupTechnicalSpecificationLanguage->language_id = $dtoGroupTechnicalSpecificationLanguage['idLanguage'];
        $groupTechnicalSpecificationLanguage->group_technical_specification_id = $idGroupTechnicalSpecification;
        $groupTechnicalSpecificationLanguage->description = $dtoGroupTechnicalSpecificationLanguage['description'];

        $groupTechnicalSpecificationLanguage->save();

        $groupTechnicalSpecificationLanguage->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update request
     * 
     * @param int $idGroupTechnicalSpecification
     * @param Request $dtoGroupTechnicalSpecificationLanguage
     * @param int $idLanguage
     */
    public static function updateRequest(int $idGroupTechnicalSpecification, $dtoGroupTechnicalSpecificationLanguage, int $idLanguage)
    {
        static::_validateData($idGroupTechnicalSpecification, $dtoGroupTechnicalSpecificationLanguage, $idLanguage);

        $groupTechnicalSpecificationLanguage = GroupTechnicalSpecificationLanguage::where('group_technical_specification_id', $idGroupTechnicalSpecification)->where('language_id', $dtoGroupTechnicalSpecificationLanguage['idLanguage'])->first();

        if (!is_null($groupTechnicalSpecificationLanguage) && is_null($dtoGroupTechnicalSpecificationLanguage['description'])) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupTechnicalSpecificationNotEmpty), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (!is_null($dtoGroupTechnicalSpecificationLanguage['description'])) {
                if (!is_null($groupTechnicalSpecificationLanguage)) {
                    if ($groupTechnicalSpecificationLanguage->description != $dtoGroupTechnicalSpecificationLanguage['description']) {
                        $groupTechnicalSpecificationLanguage->description = $dtoGroupTechnicalSpecificationLanguage['description'];
                    }
                } else {
                    $groupTechnicalSpecificationLanguage = new GroupTechnicalSpecificationLanguage();
                    $groupTechnicalSpecificationLanguage->group_technical_specification_id = $idGroupTechnicalSpecification;
                    $groupTechnicalSpecificationLanguage->language_id = $dtoGroupTechnicalSpecificationLanguage['idLanguage'];
                    $groupTechnicalSpecificationLanguage->description = $dtoGroupTechnicalSpecificationLanguage['description'];
                }

                $groupTechnicalSpecificationLanguage->save();
            }
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete by group technical specification
     * 
     * @param int $idGroupTechnicalSpecification
     */
    public static function deleteByGroupTechnicalSpecification(int $idGroupTechnicalSpecification)
    {
        GroupTechnicalSpecificationLanguage::where('group_technical_specification_id', $idGroupTechnicalSpecification)->delete();
    }

    #endregion DELETE
}

<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoMenuAdmin;
use Illuminate\Support\Facades\DB;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use App\Mail\ContactRequestMail;
use App\MenuAdmin;
use App\MenuAdminUserRole;
use App\DtoModel\DtoIncludedExcluded;

class MenuAdminBL
{
    #region PRIVATE

        /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (MenuAdmin::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->code)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }

        /**
     * Conver to model
     * 
     * @param Request request
     * 
     * @return Contact
     */
    private static function _convertToModel(Request $request)
    {
        $MenuAdmin = new MenuAdmin();

        if (isset($request->id)) {
            $MenuAdmin->id = $request->id;
        }

        if (isset($request->code)) {
            $MenuAdmin->code = $request->code;
        }

        if (isset($request->icon_id)) {
            $MenuAdmin->icon_id = $request->icon_id;
        }

        if (isset($request->url)) {
            $MenuAdmin->url = $request->url;
        }

        if (isset($request->description)) {
            $MenuAdmin->description = $request->description;
        }

        return $MenuAdmin;
    }


    private static function _convertToDto(MenuAdmin $MenuAdmin)
    {
        $dtoMenuAdmin = new DtoMenuAdmin();
        $dtoMenuAdmin->id = $MenuAdmin->id;
        $dtoMenuAdmin->code = $MenuAdmin->code;
        if (isset($dtoMenuAdmin->icon_id)) {
            $dtoMenuAdmin->icon_id = $MenuAdmin->icon_id; 
        }
        if (isset($dtoMenuAdmin->functionality_id)) {
            $dtoMenuAdmin->functionality_id = $MenuAdmin->functionality_id; 
        }
        if (isset($dtoMenuAdmin->url)) {
            $dtoMenuAdmin->url = $MenuAdmin->url; 
        }
        $dtoMenuAdmin->description = MenuAdminLanguage::where('menu_admin_id', $MenuAdmin->id)->first()->description;
        return $dtoMenuAdmin;
    }

    public static function getAllCustom($idLanguage)
    {
        $result = collect();

        foreach (MenuAdmin::where('custom',1) as $MenuAdminAll) {
            $dtoMenuAdmin = new DtoMenuAdmin();
            $dtoMenuAdmin->id = $MenuAdminAll->id;
            $dtoMenuAdmin->code = $MenuAdminAll->code;
            $dtoMenuAdmin->description = MenuAdminLanguage::where('language_id', $idLanguage)->where('menu_admin_id', $MenuAdminAll->id)->first()->code;
            
            $result->push($dtoMenuAdmin); 
        }

        return $result;
    }

    public static function getByCode(string $code)
    {
        if (is_null($code)) {
            LogHelper::error('Code of menu not found');
            return '';
        } else {
            $menuadmin = MenuAdmin::where('code', $code)->first();

            if (is_null($menuadmin)) {
                LogHelper::error('Content not found');
                return '';
            } else {
                
                $idLanguage = LanguageBL::getDefault()->id;
                return MenuAdminLanguageBL::getByCodeAndLanguage($menuadmin->id, $idLanguage);
            }
        }
    }

    /**
     * Return dtoMenuAdmin
     *
     * @param stdClass rows
     * @return DtoMenuAdmin
     */
    private static function _getDto($rows)
    {
        $dtoMenusAdmins = collect();
        $collectionRows = ArrayHelper::toCollection($rows);

        foreach ($collectionRows->whereIn('idmenufather', '') as $row) {
            $dtoMenuAdmin = new DtoMenuAdmin;
            $dtoMenuAdmin->id = $row->id;

            if (isset($row->idfunctionality)) {
                $dtoMenuAdmin->idFunctionality = $row->idfunctionality;
            }

            if (isset($row->label)) {
                $dtoMenuAdmin->label = $row->label;
            }

            if (isset($row->cssicon)) {
                $dtoMenuAdmin->cssIcon = $row->cssicon;
            }

            if (isset($row->url)) {
                $dtoMenuAdmin->url = $row->url;
            }

            if ($collectionRows->where('idmenufather', $row->id)->count()) {
                foreach ($collectionRows->where('idmenufather', $row->id) as $children) {
                    $dtoMenuAdminChildren = new DtoMenuAdmin;
                    $dtoMenuAdminChildren->id = $children->id;

                    if (isset($children->idfunctionality)) {
                        $dtoMenuAdminChildren->idFunctionality = $children->idfunctionality;
                    }

                    if (isset($children->label)) {
                        $dtoMenuAdminChildren->label = $children->label;
                    }

                    if (isset($children->cssicon)) {
                        $dtoMenuAdminChildren->cssIcon = $children->cssicon;
                    }

                    if (isset($children->url)) {
                        $dtoMenuAdminChildren->url = $children->url;
                    }

                    if ($collectionRows->where('idmenufather', $children->id)->count()) {
                        foreach ($collectionRows->where('idmenufather', $children->id) as $childrenChildren) {
                            $dtoMenuAdminChildrenChildren = new DtoMenuAdmin;
                            $dtoMenuAdminChildrenChildren->id = $childrenChildren->id;

                            if (isset($childrenChildren->idfunctionality)) {
                                $dtoMenuAdminChildrenChildren->idFunctionality = $childrenChildren->idfunctionality;
                            }

                            if (isset($childrenChildren->label)) {
                                $dtoMenuAdminChildrenChildren->label = $childrenChildren->label;
                            }

                            if (isset($childrenChildren->cssicon)) {
                                $dtoMenuAdminChildrenChildren->cssIcon = $childrenChildren->cssicon;
                            }

                            if (isset($childrenChildren->url)) {
                                $dtoMenuAdminChildrenChildren->url = $childrenChildren->url;
                            }

                            $dtoMenuAdminChildren->nextLevel->push($dtoMenuAdminChildrenChildren);
                        }
                    }

                    $dtoMenuAdmin->nextLevel->push($dtoMenuAdminChildren);
                }
            }

            $dtoMenusAdmins->push($dtoMenuAdmin);
        }

        return $dtoMenusAdmins;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Return dto menu admin
     *
     * @param int idUser
     * @param int idLanguage
     * @return DtoMenuAdmin
     */
    public static function getDto($idUser, $idLanguage)
    {
        $idRole = DB::select('  SELECT CASE
                                WHEN (  SELECT 1 FROM menu_admins_users_roles WHERE user_id = ' . $idUser . ' ) IS NOT NULL THEN 0
                                ELSE (  SELECT role_id
                                        FROM menu_admins_users_roles
                                        WHERE role_id IN ( SELECT role_id FROM role_user WHERE user_id = ' . $idUser . ' )
                                        LIMIT 1)
                                END AS role')[0]->role;

        if (is_null($idRole)) {
            // No configuration found for this user
            return;
        } else {
            $query = "  SELECT *
                        FROM (
                            SELECT  mna.id,
                                    COALESCE( maur.description, COALESCE( ( SELECT lma.description
                                                                            FROM languages_menu_admins lma
                                                                            WHERE mna.id = lma.menu_admin_id
                                                                            AND lma.language_id = 1)
                                                                        , ( SELECT lma.description
                                                                            FROM languages_menu_admins lma
                                                                            WHERE mna.id = lma.menu_admin_id
                                                                            AND lma.language_id = ( SELECT id
                                                                                                    FROM languages WHERE languages.default = true))))
                                    AS label,
                                    COALESCE(imur.css, (select imn.css from icons imn where mna.icon_id = imn.id)) AS cssIcon,
                                    mna.url AS url, maur.menu_admin_father_id AS idMenuFather, maur.order AS order,
                                    mna.functionality_id AS idFunctionality
                            FROM menu_admins mna
                            LEFT JOIN menu_admins_users_roles maur ON mna.id = maur.menu_admin_id
                                AND {0}
                            LEFT JOIN icons imur ON maur.icon_id = imur.ID
                            WHERE (maur.enabled = true AND {0} )
                            OR mna.id IN (  SELECT imaur.menu_admin_father_id
                                            FROM menu_admins_users_roles imaur
        		                            where imaur.enabled = true
                                            AND {1} ) 
                        ) M ORDER BY M.order";

            if ($idRole == 0) {
                // User enabled
                $query = str_replace('{0}', 'maur.user_id = ' . $idUser, $query);
                $query = str_replace('{1}', 'imaur.user_id = ' . $idUser, $query);
            } else {
                // Role enabled
                $query = str_replace('{0}', 'maur.role_id = ' . $idRole, $query);
                $query = str_replace('{1}', 'imaur.role_id = ' . $idRole, $query);
            }
           
            return static::_getDto(DB::select($query));
        }
    }

    public static function getByRole(int $idRole)
    {
        $result = new DtoIncludedExcluded();
        $result->included = MenuAdminBL::getMenuEnabledByRole($idRole);
        $result->excluded = MenuAdminBL::getMenuDisabledByRole($idRole);
        return $result;
    }

    public static function getMenuEnabledByRole(int $idRole)
    {
        $result = collect();

        //questa per menù abilitati.....per quelli disabilitati mettere where = 0        
        
        foreach (DB::select('SELECT menu_admins."id", 
            case when COALESCE(MAURLAN2.description,\'\') = \'\' then MAURLAN.description 
            else MAURLAN2.description || \' - \' || MAURLAN.description
            end as descrizione
            from menu_admins
            left outer join languages_menu_admins MAURLAN on MAURLAN.menu_admin_id = menu_admins."id" and MAURLAN.language_id = 1
            left outer join menu_admins_users_roles as MAUR on MAUR.menu_admin_id = menu_admins."id" 
                and MAUR.role_id = :idRole and MAUR.enabled = \'t\'
            left outer join menu_admins_users_roles as FATH on FATH.id = MAUR.menu_admin_father_id
            left outer join languages_menu_admins MAURLAN2 on MAURLAN2.menu_admin_id = FATH."id" and MAURLAN2.language_id = 1
            where COALESCE(MAUR.id,0)<> 0
            order by COALESCE(MAUR.menu_admin_father_id,0), MAUR."order"', ['idRole' => $idRole]) as $menuAdmin) {
            $dtoMenuAdmin = new DtoMenuAdmin();
            $dtoMenuAdmin->id = $menuAdmin->id;
            $dtoMenuAdmin->description = $menuAdmin->descrizione;

            $result->push($dtoMenuAdmin);
        }

        return $result;
    }

    public static function getMenuDisabledByRole(int $idRole)
    {
        $result = collect();

        //questa per menù disabilitati.....per quelli abilitati mettere where <> 0        
        
        foreach (DB::select('SELECT menu_admins."id", 
            case when COALESCE(MAURLAN2.description,\'\') = \'\' then MAURLAN.description 
            else MAURLAN2.description || \' - \' || MAURLAN.description
            end as descrizione
            from menu_admins
            left outer join languages_menu_admins MAURLAN on MAURLAN.menu_admin_id = menu_admins."id" and MAURLAN.language_id = 1
            left outer join menu_admins_users_roles as MAUR on MAUR.menu_admin_id = menu_admins."id" 
                and MAUR.role_id = :idRole and MAUR.enabled = \'t\'
            left outer join menu_admins_users_roles as FATH on FATH.id = MAUR.menu_admin_father_id
            left outer join languages_menu_admins MAURLAN2 on MAURLAN2.menu_admin_id = FATH."id" and MAURLAN2.language_id = 1
            where COALESCE(MAUR.id,0)= 0
            order by COALESCE(MAUR.menu_admin_father_id,0), MAUR."order"', ['idRole' => $idRole]) as $menuAdmin) {
            $dtoMenuAdmin = new DtoMenuAdmin();
            $dtoMenuAdmin->id = $menuAdmin->id;
            $dtoMenuAdmin->description = $menuAdmin->descrizione;
            $result->push($dtoMenuAdmin);
        }

        return $result;
    }    

    public static function disable(int $menu_admin_id, int $role_id)
    {
        
        foreach (MenuAdminUserRole::where('menu_admin_id', $menu_admin_id)->where('role_id', $role_id)->get() as $menuRow) {
            $menuRow->enabled = "f";
            $menuRow->save();
            $menuTrovato = true;
        }

        return true;
    }

    public static function enable(int $menu_admin_id, int $role_id)
    {
        $menuTrovato = false;

        //verifico...se c'è già il record lo aggiorno, altrimenti lo devo inserire
        foreach (MenuAdminUserRole::where('menu_admin_id', $menu_admin_id)->where('role_id', $role_id)->get() as $menuRow) {
            $menuRow->enabled = "t";
            $menuRow->save();
            $menuTrovato = true;
        }

        if ($menuTrovato == false){

            //mi memorizzo il record dell'admin perchè tanto lo devo sempre avere...mi serve per i campi ordinamento e menù padre
            $menuAmministratore = MenuAdminUserRole::where('menu_admin_id', $menu_admin_id)->where('role_id', "1")->first();

            
            //ora inserisco il nuovo menù
            $newMenuAdminUserRole = new MenuAdminUserRole();
            $newMenuAdminUserRole->menu_admin_id = $menu_admin_id;
            $newMenuAdminUserRole->role_id = $role_id;
            $newMenuAdminUserRole->enabled = "t";
            $newMenuAdminUserRole->order = $menuAmministratore->order;
            $newMenuAdminUserRole->menu_admin_father_id = $menuAmministratore->menu_admin_father_id;
            //$newMenuAdminUserRole->created_id = $menuAmministratore->$created_id;
            $newMenuAdminUserRole->save();
        }


        return true;
    }

    #endregion GET
}

<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoVoucherType;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\VoucherType;
use Exception;
use Illuminate\Http\Request;

class VoucherTypeBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request dtoVoucherType
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $dtoVoucherType, $dbOperationType)
    {

        if (is_null($dtoVoucherType->description)) {
            throw new Exception("Descrizione obbligatoria");
        }
    }


    /**
     * Convert to dto
     * 
     * @param VoucherType postalCode
     * 
     * @return DtoVoucherType
     */
    private static function _convertToDto($VoucherType)
    {
        $dtoVoucherType = new DtoVoucherType();

        if (isset($VoucherType->id)) {
            $dtoVoucherType->id = $VoucherType->id;
        }

        if (isset($VoucherType->language_id)) {
            $dtoVoucherType->language_id = $VoucherType->language_id;
        }

        if (isset($VoucherType->description)) {
            $dtoVoucherType->description = $VoucherType->description;
        }

        return $dtoVoucherType;
    }


    /**
     * Convert to VoucherType
     * 
     * @param DtoVoucherType dtoVoucherType
     * 
     * @return VoucherType
     */
    private static function _convertToModel($dtoVoucherType)
    {
        $VoucherType = new VoucherType();

        if (isset($dtoVoucherType->id)) {
            $VoucherType->id = $dtoVoucherType->id;
        }

        if (isset($dtoVoucherType->description)) {
            $VoucherType->description = $dtoVoucherType->description;
        }

        if (isset($dtoVoucherType->language_id)) {
            $VoucherType->language_id = $dtoVoucherType->language_id;
        }

        return $VoucherType;
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoVoucherType
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(VoucherType::find($id));
    }

    /**
     * Get select
     *
     * @param String $search
     * @return VoucherType
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return VoucherType::select('id', 'description')->get();
        } else {
            return VoucherType::where('description', 'like', $search . '%')->select('id', 'description')->get();
        }
    }

    /**
     * Get all
     * 
     * @return DtoVoucherType
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (VoucherType::where('language_id',$idLanguage)->get() as $VoucherTypeAll) {

            $DtoVoucherType = new DtoVoucherType();
            $DtoVoucherType->id = $VoucherTypeAll->id;
            $DtoVoucherType->description = $VoucherTypeAll->description;

            $result->push($DtoVoucherType); 
    
        }
        
        return $result;
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request dtoVoucherType
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $dtoVoucherType)
    {
        static::_validateData($dtoVoucherType, DbOperationsTypesEnum::INSERT);
        $voucherType = static::_convertToModel($dtoVoucherType);
        $voucherType->save();
        return $voucherType->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoVoucherType
     * @param int idLanguage
     */
    public static function update(Request $dtoVoucherType)
    {
        static::_validateData($dtoVoucherType, $dtoVoucherType->idLanguage, DbOperationsTypesEnum::UPDATE);
        $voucherType = VoucherType::find($dtoVoucherType->id);
        DBHelper::updateAndSave(static::_convertToModel($dtoVoucherType), $voucherType);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id)
    {
        $VoucherType = VoucherType::find($id);

        if (is_null($VoucherType)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $VoucherType->delete();
    }

    #endregion DELETE
}

<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoMessage;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\MessageLanguage;
use App\Mail\ContactRequestMail;
use App\Message;
use App\User;
use App\Logs;
use App\CorrelationPeopleUser;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class LogsBL
{
    #region PRIVATE

    #endregion PRIVATE

    #region GET

    #endregion GET

    #region INSERT
    /**
     * Insert log
     * 
     * @param int idUser
     * @param string ip
     * @param string action
     * @param string description
     * 
     * @return int
     */
    public static function insert(int $idUser, string $ip, string $action, string $description)
    {
        if(isset($idUser)){
            $user = User::find($idUser);
            if(isset($user)){
                $logs = new Logs();

                $logs->user_id =  $idUser;

                $CorrelationPeopleUser = CorrelationPeopleUser::where('id_user', $idUser)->first();
                if(isset($CorrelationPeopleUser)) {  
                    $logs->people_id = $CorrelationPeopleUser->id_people;
                }else{
                    $logs->people_id = null;
                }    
                $logs->ip =  $ip;
                $logs->action =  $action;
                $logs->description =  $description;
                $logs->save();
            }
        }
    }
    #endregion INSERT

    #region UPDATE

    #endregion UPDATE

    #region DELETE

    #endregion DELETE
}

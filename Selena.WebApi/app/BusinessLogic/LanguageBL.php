<?php

namespace App\BusinessLogic;

use App\Language;
use App\LanguagePage;
use App\CategoryLanguage;
use App\ItemLanguage;
use App\Page;
use App\Enums\InputTypesEnum;
use App\DtoModel\DtoConfiguration;
use App\DtoModel\DtoPageConfiguration;
use App\DtoModel\DtoTabConfiguration;
use App\DtoModel\DtoTableConfiguration;
use App\Helpers\LogHelper;
use Exception;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;

class LanguageBL
{
    #region GET

    /**
     * Return a default language
     *
     * @return App\Language
     */
    public static function getDefault()
    {
        return Language::where('default', '=', true)->first();
    }

    /**
     * Return a language by id
     *
     * @param int id
     * @return App\Language
     */
    public static function get($id)
    {
        return Language::where('id', '=', $id)->first();
    }

    /**
     * Return a language for select
     *
     * @param string search
     * @return collection.App\Language
     */
    public static function getSelect($search = null)
    {
        if ($search == "*") {
            return Language::all();
        } else {
            return Language::where('description', 'ILIKE', $search . '%')->get();
        }
    }

    /**
     * Return a language for selectlanguagestanslate (ItemBL Popup)
     *
     * @param string search
     * @return collection.App\Language
     */
    public static function selectlanguagestranslate($search)
    {

        if ($search == "*" || $search == '') {
            return collect(DB::select(
                "SELECT languages.id,languages.description
                FROM languages 
                where languages.id not in (SELECT items_languages.language_id from items_languages where items_languages.item_id = 20)"
            ));
        }
    }

    /**
     * Return a language for selectlanguagestanslate (ItemBL Popup)
     *
     * @param string search
     * @return collection.App\Language
     */
    public static function selectlanguagescategoriestranslate($search, $idCat)
    {
        if ($search == "*" || $search == '') {
            return collect(DB::select(
                "SELECT languages.id,languages.description
                FROM languages 
                where languages.id not in (SELECT categories_languages.language_id from categories_languages where categories_languages.category_id =" . $idCat . ")"
            ));
        }
    }


    /**
     * Return a language for selectlanguagestanslate (ItemBL Popup)
     *
     * @param string search
     * @return collection.App\Language
     */
    public static function selectlanguagesitemstranslate($search, $idItem)
    {

        if ($search == "*" || $search == '') {
            return collect(DB::select(
                "SELECT languages.id,languages.description
                FROM languages 
                where languages.id not in (SELECT items_languages.language_id from items_languages where items_languages.item_id =" . $idItem . ")"
            ));
        }
    }
    

    /**
     * Return all language
     *
     * @return collection.App\Language
     */
    public static function getAll()
    {
        return Language::all();
    }

    /**
     * Get configuration
     * 
     * @return DtoConfiguration
     */
    public static function getConfiguration()
    {
        $dtoConfiguration = new DtoConfiguration();

        $defaultLanguage = static::getDefault();

        #region TAB

        $dtoTabConfiguration = new DtoTabConfiguration();
        $dtoTabConfiguration->code = 'language';
        $dtoTabConfiguration->label = 'Language';
        $dtoConfiguration->tabsConfigurations->push($dtoTabConfiguration);

        #endregion TAB

        #region DETAIL

        $posY = 1;

        $dtoPageConfiguration = new DtoPageConfiguration();
        $dtoPageConfiguration->label = $defaultLanguage->description;
        $dtoPageConfiguration->field = $defaultLanguage->id;
        $dtoPageConfiguration->inputType = InputTypesEnum::getKey(InputTypesEnum::Input);
        $dtoPageConfiguration->posX = 1;
        $dtoPageConfiguration->posY = $posY;
        $dtoPageConfiguration->colspan = 4;
        $dtoPageConfiguration->onChange = 'onChangeDetail';
        $dtoPageConfiguration->required = true;
        $dtoTabConfiguration->pageConfigurations->push($dtoPageConfiguration);

        $posY = $posY + 1;

        #endregion DETAIL

        #region TABLE

        $dtoBootstrapTable = new DtoTableConfiguration();
        $dtoBootstrapTable->inputType = InputTypesEnum::getKey(InputTypesEnum::TableCheckbox);
        $dtoConfiguration->tablesConfigurations->push($dtoBootstrapTable);

        $dtoBootstrapTable = new DtoTableConfiguration();
        $dtoBootstrapTable->inputType = InputTypesEnum::getKey(InputTypesEnum::CommandFormatter);
        $dtoConfiguration->tablesConfigurations->push($dtoBootstrapTable);

        $dtoBootstrapTable = new DtoTableConfiguration();
        $dtoBootstrapTable->field = $defaultLanguage->id;
        $dtoBootstrapTable->filterType = InputTypesEnum::getKey(InputTypesEnum::Input);
        $dtoBootstrapTable->inputType = InputTypesEnum::getKey(InputTypesEnum::Input);
        $dtoBootstrapTable->title = $defaultLanguage->description;
        $dtoBootstrapTable->onChange = 'tableHelpers.onChange';
        $dtoBootstrapTable->required = true;
        $dtoConfiguration->tablesConfigurations->push($dtoBootstrapTable);

        #endregion TABLE

        foreach (Language::where('default', false)->orderBy('description')->get() as $language) {

            #region DETAIL

            $dtoPageConfiguration = new DtoPageConfiguration();
            $dtoPageConfiguration->label = $language->description;
            $dtoPageConfiguration->field = $language->id;
            $dtoPageConfiguration->inputType = InputTypesEnum::getKey(InputTypesEnum::Input);
            $dtoPageConfiguration->posX = 1;
            $dtoPageConfiguration->posY = $posY;
            $dtoPageConfiguration->colspan = 4;
            $dtoPageConfiguration->onChange = 'onChangeDetail';
            $dtoTabConfiguration->pageConfigurations->push($dtoPageConfiguration);

            $posY = $posY + 1;

            #endregion DETAIL

            #region TABLE

            $dtoBootstrapTable = new DtoTableConfiguration();
            $dtoBootstrapTable->field = $language->id;
            $dtoBootstrapTable->filterType = InputTypesEnum::getKey(InputTypesEnum::Input);
            $dtoBootstrapTable->inputType = InputTypesEnum::getKey(InputTypesEnum::Input);
            $dtoBootstrapTable->title = $language->description;
            $dtoBootstrapTable->onChange = 'tableHelpers.onChange';
            $dtoConfiguration->tablesConfigurations->push($dtoBootstrapTable);

            #endregion TABLE
        }

        return $dtoConfiguration;
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        //static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $language = new Language();
            $language->description = $request->description;
            $language->default = $request->default;
            $language->created_id = $idUser;
            $language->date_format = $request->date_format;
            $language->date_time_format = $request->date_time_format;
            $language->date_format_client = $request->date_format_client;
            $language->date_time_format_client = $request->date_time_format_client;
            $language->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $language->id;
    }

    /**
     * get Url Translate
     * 
     * @param Request request
     * 
     * @return string
     */
    public static function getUrlTranslate(Request $request)
    {
        //static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);
        if($request->url == '' || $request->idLanguage == ''){
            $returnvar = '/';
        }else{
            $languagePage = LanguagePage::where('url', $request->url)->get()->first();
            if(isset($languagePage)){
                $languagePageTranslate = LanguagePage::where('page_id', $languagePage->page_id)->where('language_id', $request->idLanguage)->get()->first();
                if(isset($languagePageTranslate)){
                    $returnvar = $languagePageTranslate->url;
                }else{
                    $returnvar = '/';
                }
            }else{
                $languagePage = CategoryLanguage::where('meta_tag_link', $request->url)->get()->first();
                if(isset($languagePage)){
                    $languagePageTranslate = CategoryLanguage::where('category_id', $languagePage->category_id)->where('language_id', $request->idLanguage)->get()->first();
                    if(isset($languagePageTranslate)){
                        $returnvar = $languagePageTranslate->meta_tag_link;
                    }else{
                        $returnvar = '/';
                    }
                }else{
                    $languagePageItem = ItemLanguage::where('link', $request->url)->get()->first();
                    if(isset($languagePageItem)){
                        $languagePageTranslateItem = ItemLanguage::where('item_id', $languagePageItem->item_id)->where('language_id', $request->idLanguage)->get()->first();
                        if(isset($languagePageTranslateItem)){
                            $returnvar = $languagePageTranslateItem->link;
                        }else{
                            $returnvar = '';
                        }
                    }else{
                        $returnvar = '';
                    }
                }
            }
        }
        return $returnvar;
    }
    

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request request
     * @param int idLanguage
     */
    public static function update(Request $request, int $idLanguage)
    {
        //static::_validateData($request, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $languagesOnDb = Language::find($request->id);
        
        DB::beginTransaction();

        try {
            $languagesOnDb->description = $request->description;
            $languagesOnDb->default = $request->default;
            $languagesOnDb->date_format = $request->date_format;
            $languagesOnDb->date_time_format = $request->date_time_format;
            $languagesOnDb->date_format_client = $request->date_format_client;
            $languagesOnDb->date_time_format_client = $request->date_time_format_client;

            $languagesOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE
}

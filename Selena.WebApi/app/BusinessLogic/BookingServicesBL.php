<?php

namespace App\BusinessLogic;

use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoBookingServices;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\LanguageNation;
use App\LanguageProvince;
use App\User;
use App\BookingServices;
use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;

class BookingServicesBL
{

     #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request DtoBookingServices
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoBookingServices, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoBookingServices->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (BookingServices::find($DtoBookingServices->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }
        if (is_null($DtoBookingServices->name)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoBookingServices->duration)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DurationNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoBookingServices->slot_step)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SlotStepNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoBookingServices->block_first)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::BlockFirstNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoBookingServices->block_after)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::BlockAfterNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoBookingServices->price)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PriceNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    /**
     * Convert to dto
     * 
     * @param BookingServices
     * 
     * @return DtoBookingServices
     */
    private static function _convertToDto($BookingServices)
    {
        $DtoBookingServices = new DtoBookingServices();

        if (isset($BookingServices->id)) {
            $DtoBookingServices->id = $BookingServices->id;
        }
        if (isset($BookingServices->name)) {
            $DtoBookingServices->name = $BookingServices->name;
        }
        if (isset($BookingServices->duration)) {
            $DtoBookingServices->duration = $BookingServices->duration;
        }
        if (isset($BookingServices->slot_step)) {
            $DtoBookingServices->slot_step = $BookingServices->slot_step;
        }
        if (isset($BookingServices->block_first)) {
            $DtoBookingServices->block_first = $BookingServices->block_first;
        }
        if (isset($BookingServices->block_after)) {
            $DtoBookingServices->block_after = $BookingServices->block_after;
        }
        if (isset($BookingServices->price)) {
            $DtoBookingServices->price = $BookingServices->price;
        }
        if (isset($BookingServices->reduced_price)) {
            $DtoBookingServices->reduced_price = $BookingServices->reduced_price;
        }
        if (isset($BookingServices->advance)) {
            $DtoBookingServices->advance = $BookingServices->advance;
        }
         if (isset($BookingServices->activate_purchase)) {
            $DtoBookingServices->active_purchase = $BookingServices->activate_purchase;
        }
        if (isset($BookingServices->send_confirmation_end_only_upon_payment)) {
            $DtoBookingServices->send_confirmation_end_only_upon_payment = $BookingServices->send_confirmation_end_only_upon_payment;
        }
        if (isset($BookingServices->text_gdpr)) {
            $DtoBookingServices->text_gdpr = $BookingServices->text_gdpr;
        }
        if (isset($BookingServices->acceptance_of_mandatory_conditions)) {
            $DtoBookingServices->acceptance_of_mandatory_conditions = $BookingServices->acceptance_of_mandatory_conditions;
        }
        
        return $DtoBookingServices;
       
    }

    #endregion PRIVATE

    #region GET
    /**
     * Get by id
     * 
     * @param int id
     * @return DtoPlaces
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(BookingServices::find($id));
    }

    /**
     * Get select
     *
     * @param String $search
     * @param int $idUser
     * @return People
     */
    public static function getSelect(string $search)
    {
                if ($search == "*") {
                    return BookingServices::select('id', 'name as description')->get();
                } else {
                    return BookingServices::where('name', 'ilike' , $search . '%')->select('id', 'name as description')->get();
          } 
    }

  
    /**
     * Get all
     * 
     * @return DtoPlaces
     */
    public static function getAll()
    {
        $result = collect();

        foreach (BookingServices::orderBy('id','DESC')->get() as $BookingServices) {    

        $DtoBookingServices = new DtoBookingServices();

        if (isset($BookingServices->id)) {
            $DtoBookingServices->id = $BookingServices->id;
        }
        if (isset($BookingServices->name)) {
            $DtoBookingServices->name = $BookingServices->name;
        }else{
            $DtoBookingServices->name = '-';
        }
        if (isset($BookingServices->duration)) {
            $DtoBookingServices->duration = $BookingServices->duration;
        }else{
            $DtoBookingServices->duration = '-';
        }
        if (isset($BookingServices->slot_step)) {
            $DtoBookingServices->slot_step = $BookingServices->slot_step;
        }else{
            $DtoBookingServices->slot_step = '-';
        }
        if (isset($BookingServices->block_first)) {
            $DtoBookingServices->block_first = $BookingServices->block_first;
        }else{
            $DtoBookingServices->block_first = '-';
        }
        if (isset($BookingServices->block_after)) {
            $DtoBookingServices->block_after = $BookingServices->block_after;
        }else{
            $DtoBookingServices->block_after = '-';
        }
        if (isset($BookingServices->price)) {
            $DtoBookingServices->price = '€ ' . $BookingServices->price;
        }else{
            $DtoBookingServices->price = '-';
        }
        if (isset($BookingServices->reduced_price)) {
            $DtoBookingServices->reduced_price = '€ ' . $BookingServices->reduced_price;
        }else{
            $DtoBookingServices->reduced_price = '-';
        }
        if (isset($BookingServices->advance)) {
            $DtoBookingServices->advance = '€ ' . $BookingServices->advance;
        }else{
            $DtoBookingServices->advance = '-';
        }
         if (isset($BookingServices->activate_purchase)) {
            $DtoBookingServices->active_purchase = $BookingServices->activate_purchase;
        }
        if (isset($BookingServices->send_confirmation_end_only_upon_payment)) {
            $DtoBookingServices->send_confirmation_end_only_upon_payment = $BookingServices->send_confirmation_end_only_upon_payment;
        }
        if (isset($BookingServices->text_gdpr)) {
            $DtoBookingServices->text_gdpr = $BookingServices->text_gdpr;
        }else{
            $DtoBookingServices->text_gdpr = '-';
        }
        if (isset($BookingServices->acceptance_of_mandatory_conditions)) {
            $DtoBookingServices->acceptance_of_mandatory_conditions = $BookingServices->acceptance_of_mandatory_conditions;
        }
        
        $result->push($DtoBookingServices);
    }
        return $result;
    }

    #endregion GET

    #region INSERT
    /**
     * Insert
     * 
     * @param Request DtoBookingServices
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoBookingServices, int $idUser)
    {
       DB::beginTransaction();
        try {
            $BookingServices = new BookingServices();
            
        if (isset($DtoBookingServices->name)) {
            $BookingServices->name = $DtoBookingServices->name;
        } 
        if (isset($DtoBookingServices->duration)) {
            $BookingServices->duration = $DtoBookingServices->duration;
        } 
        if (isset($DtoBookingServices->slot_step)) {
            $BookingServices->slot_step = $DtoBookingServices->slot_step;
        } 
        if (isset($DtoBookingServices->block_first)) {
            $BookingServices->block_first = $DtoBookingServices->block_first;
        } 
        if (isset($DtoBookingServices->block_after)) {
            $BookingServices->block_after = $DtoBookingServices->block_after;
        } 
        if (isset($DtoBookingServices->price)) {
            $BookingServices->price = $DtoBookingServices->price;
        } else {
            $BookingServices->price = 0;
        }
        if (isset($DtoBookingServices->reduced_price)) {
            $BookingServices->reduced_price = $DtoBookingServices->reduced_price;
        } else {
            $BookingServices->reduced_price = 0;
        }
        if (isset($DtoBookingServices->advance)) {
            $BookingServices->advance = $DtoBookingServices->advance;
        } else {
            $BookingServices->advance = 0;
        }
        if (isset($DtoBookingServices->active_purchase)) {
            $BookingServices->activate_purchase = $DtoBookingServices->active_purchase;
        } 
        if (isset($DtoBookingServices->send_confirmation_end_only_upon_payment)) {
            $BookingServices->send_confirmation_end_only_upon_payment = $DtoBookingServices->send_confirmation_end_only_upon_payment;
        } 
        if (isset($DtoBookingServices->text_gdpr)) {
            $BookingServices->text_gdpr = $DtoBookingServices->text_gdpr;
        }
        if (isset($DtoBookingServices->acceptance_of_mandatory_conditions)) {
            $BookingServices->acceptance_of_mandatory_conditions = $DtoBookingServices->acceptance_of_mandatory_conditions;
        }
        $BookingServices->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $BookingServices->id;
    }

    #endregion INSERT


 
    /**
     * Update
     * 
     * @param Request [DtoBookingServices]
     * @param int idLanguage
     */
    public static function update(Request $DtoBookingServices, int $idUser)
    {
       
        $BookingServicesOnDb = BookingServices::find($DtoBookingServices->id);
      
        DB::beginTransaction();

        try {
            if (isset($DtoBookingServices->id)) {
                $BookingServicesOnDb->id = $DtoBookingServices->id;
            } 
            if (isset($DtoBookingServices->name)) {
                $BookingServicesOnDb->name = $DtoBookingServices->name;
            }else {
                $BookingServicesOnDb->name = '';
            }
             if (isset($DtoBookingServices->duration)) {
                $BookingServicesOnDb->duration = $DtoBookingServices->duration;
            }else {
                $BookingServicesOnDb->duration = NULL;
            }
            if (isset($DtoBookingServices->slot_step)) {
                $BookingServicesOnDb->slot_step = $DtoBookingServices->slot_step;
            }else {
                $BookingServicesOnDb->slot_step = NULL;
            }
            if (isset($DtoBookingServices->block_first)) {
                $BookingServicesOnDb->block_first = $DtoBookingServices->block_first;
            }else {
                $BookingServicesOnDb->block_first = NULL;
            }
            if (isset($DtoBookingServices->block_after)) {
                $BookingServicesOnDb->block_after = $DtoBookingServices->block_after;
            } else {
                $BookingServicesOnDb->block_after = NULL;
            }
            if (isset($DtoBookingServices->price)) {
                $BookingServicesOnDb->price = $DtoBookingServices->price;
            }else {
                $BookingServicesOnDb->price = 0;
            }
            if (isset($DtoBookingServices->reduced_price)) {
                $BookingServicesOnDb->reduced_price = $DtoBookingServices->reduced_price;
            }else {
                $BookingServicesOnDb->reduced_price = 0;
            }
             if (isset($DtoBookingServices->advance)) {
                $BookingServicesOnDb->advance = $DtoBookingServices->advance;
            }else {
                $BookingServicesOnDb->advance = 0;
            }
            if (isset($DtoBookingServices->active_purchase)) {
                $BookingServicesOnDb->activate_purchase = $DtoBookingServices->active_purchase;
            }
            if (isset($DtoBookingServices->send_confirmation_end_only_upon_payment)) {
                $BookingServicesOnDb->send_confirmation_end_only_upon_payment = $DtoBookingServices->send_confirmation_end_only_upon_payment;
            }
            if (isset($DtoBookingServices->text_gdpr)) {
                $BookingServicesOnDb->text_gdpr = $DtoBookingServices->text_gdpr;
            }else {
                $BookingServicesOnDb->text_gdpr = NULL;
            }
            if (isset($DtoBookingServices->acceptance_of_mandatory_conditions)) {
                $BookingServicesOnDb->acceptance_of_mandatory_conditions = $DtoBookingServices->acceptance_of_mandatory_conditions;
            }

            $BookingServicesOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return  $DtoBookingServices->id;
    }
    #endregion UPDATE

        /**
             * Clone
             * 
             * @param int id
             * @param int idFunctionality
             * @param int idLanguage
             * @param int idUser
             * 
             * @return int
             */
            public static function clone(int $id, int $idFunctionality, int $idUser, int $idLanguage)
            {
                //FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
                $BookingServices = BookingServices::find($id);
                              
                DB::beginTransaction();

                try {

                    $newBookingServices = new BookingServices();
                    $newBookingServices->name = $BookingServices->name . ' - Copy';
                    $newBookingServices->duration = $BookingServices->duration;
                    $newBookingServices->slot_step = $BookingServices->slot_step;
                    $newBookingServices->block_first = $BookingServices->block_first;    
                    $newBookingServices->block_after = $BookingServices->block_after;        
                    $newBookingServices->price = $BookingServices->price; 
                    $newBookingServices->reduced_price = $BookingServices->reduced_price; 
                    $newBookingServices->advance = $BookingServices->advance;
                    $newBookingServices->activate_purchase = $BookingServices->activate_purchase;
                    $newBookingServices->send_confirmation_end_only_upon_payment = $BookingServices->send_confirmation_end_only_upon_payment;
                    $newBookingServices->text_gdpr = $BookingServices->text_gdpr;
                    $newBookingServices->acceptance_of_mandatory_conditions = $BookingServices->acceptance_of_mandatory_conditions;

                    $newBookingServices->save();

                    DB::commit();
                    return $newBookingServices->id;
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
            }

        #endregion CLONE

            #region DELETE
                    /**
                     * Delete
                     * 
                     * @param int id
                     * @param int idLanguage
                     */
                    public static function delete(int $id, int $idLanguage)
                    {
                        $BookingServices = BookingServices::find($id);                   

                        if (is_null($BookingServices)) {
                            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PeopleRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
                        }
                        DB::beginTransaction();
                        try {
                            $BookingServices->delete();
                            
                            DB::commit();
                        } catch (Exception $ex) {
                            DB::rollback();
                            throw $ex;
                        }
                    }
                
                }
        #endregion DELETE
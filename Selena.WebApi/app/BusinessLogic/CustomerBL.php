<?php

namespace App\BusinessLogic;

use App\Customer;
use App\DtoModel\DtoCustomer;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\DocumentTypeEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class CustomerBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request dtoCustomer
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $dtoCustomer, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($dtoCustomer->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Customer::find($dtoCustomer->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($dtoCustomer->company)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CompanyNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    /**
     * Convert to dto
     * 
     * @param Customer customer
     * 
     * @return DtoCustomer
     */
    private static function _convertToDto($customer)
    {
        $dtoCustomer = new DtoCustomer();

        if (isset($customer->id)) {
            $dtoCustomer->id = $customer->id;
        }

        if (isset($customer->company)) {
            $dtoCustomer->company = $customer->company;
        }

        if (isset($customer->business_name)) {
            $dtoCustomer->businessName = $customer->business_name;
        }

        if (isset($customer->name)) {
            $dtoCustomer->name = $customer->name;
        }

        if (isset($customer->surname)) {
            $dtoCustomer->surname = $customer->surname;
        }

        if (isset($customer->vat_number)) {
            $dtoCustomer->vatNumber = $customer->vat_number;
        }

        if (isset($customer->fiscal_code)) {
            $dtoCustomer->fiscalCode = $customer->fiscal_code;
        }

        if (isset($customer->address)) {
            $dtoCustomer->address = $customer->address;
        }

        if (isset($customer->postal_code_id)) {
            $dtoCustomer->idPostalCode = $customer->postal_code_id;
            $dtoCustomer->postalcodeDescription = PostalCodeBL::getById($customer->postal_code_id)->code;
        }

        if (isset($customer->telephone_number)) {
            $dtoCustomer->telephoneNumber = $customer->telephone_number;
        }

        if (isset($customer->fax_number)) {
            $dtoCustomer->faxNumber = $customer->fax_number;
        }

        if (isset($customer->email)) {
            $dtoCustomer->email = $customer->email;
        }

        if (isset($customer->website)) {
            $dtoCustomer->website = $customer->website;
        }

        if (isset($customer->language_id)) {
            $dtoCustomer->idLanguage = $customer->language_id;
            $dtoCustomer->languageDescription = LanguageBL::get($customer->language_id)->description;
        }

        if (isset($customer->vat_type_id)) {
            $dtoCustomer->idVatType = $customer->vat_type_id;
            $dtoCustomer->vattypeDescription = VatTypeBL::getById($customer->vat_type_id)->rate;
        }

        if (isset($customer->enable_from)) {
            $dtoCustomer->enableFrom = $customer->enable_from;
        }

        if (isset($customer->enable_to)) {
            $dtoCustomer->enableTo = $customer->enable_to;
        }

        if (isset($customer->send_newsletter)) {
            $dtoCustomer->sendNewsletter = $customer->send_newsletter;
        }

        if (isset($customer->error_newsletter)) {
            $dtoCustomer->errorNewsletter = $customer->error_newsletter;
        }

        return $dtoCustomer;
    }

    /**
     * Convert to Customer
     * 
     * @param DtoCustomer dtoCustomer
     * 
     * @return Customer
     */
    private static function _convertToModel($dtoCustomer)
    {
        $customer = new Customer();

        if (isset($dtoCustomer->id)) {
            $customer->id = $dtoCustomer->id;
        }

        if (isset($dtoCustomer->company)) {
            $customer->company = $dtoCustomer->company;
        }

        if (isset($dtoCustomer->businessName)) {
            $customer->business_name = $dtoCustomer->businessName;
        }

        if (isset($dtoCustomer->name)) {
            $customer->name = $dtoCustomer->name;
        }

        if (isset($dtoCustomer->surname)) {
            $customer->surname = $dtoCustomer->surname;
        }

        if (isset($dtoCustomer->vatNumber)) {
            $customer->vat_number = $dtoCustomer->vatNumber;
        }

        if (isset($dtoCustomer->fiscalCode)) {
            $customer->fiscal_code = $dtoCustomer->fiscalCode;
        }

        if (isset($dtoCustomer->address)) {
            $customer->address = $dtoCustomer->address;
        }

        if (isset($dtoCustomer->idPostalCode)) {
            $customer->postal_code_id = $dtoCustomer->idPostalCode;
        }

        if (isset($dtoCustomer->telephoneNumber)) {
            $customer->telephone_number = $dtoCustomer->telephoneNumber;
        }

        if (isset($dtoCustomer->faxNumber)) {
            $customer->fax_number = $dtoCustomer->faxNumber;
        }

        if (isset($dtoCustomer->email)) {
            $customer->email = $dtoCustomer->email;
        }

        if (isset($dtoCustomer->website)) {
            $customer->website = $dtoCustomer->website;
        }

        if (isset($dtoCustomer->idLanguage)) {
            $customer->language_id = $dtoCustomer->idLanguage;
        }

        if (isset($dtoCustomer->idVatType)) {
            $customer->vat_type_id = $dtoCustomer->idVatType;
        }

        if (isset($dtoCustomer->enableFrom)) {
            $customer->enable_from = $dtoCustomer->enableFrom;
        }

        if (isset($dtoCustomer->enableTo)) {
            $customer->enable_to = $dtoCustomer->enableTo;
        }

        if (isset($dtoCustomer->sendNewsletter)) {
            $customer->send_newsletter = $dtoCustomer->sendNewsletter;
        }

        if (isset($dtoCustomer->errorNewsletter)) {
            $customer->error_newsletter = $dtoCustomer->errorNewsletter;
        }

        return $customer;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get business name
     * 
     * @param int id
     * 
     * @return description
     */
    public static function getBusinessName(int $id)
    {
        return Customer::find($id)->business_name;
    }

    /**
     * Get before ddt sixten
     * 
     * @param string search
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return Customer
     */
    public static function getSelectBeforeDDTSixten(string $search, int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        return collect(DB::select(
            'SELECT id, erp_id as description
            FROM customers
            WHERE id IN (SELECT documents_heads.customer_id
                FROM documents_heads
                INNER JOIN documents_rows ON documents_heads.id = documents_rows.document_head_id
                WHERE documents_heads.document_type_id IN (SELECT id FROM documents_types WHERE code = :codeDocumentType)
                AND documents_rows.evaded_date IS NULL
            )
            AND UPPER(erp_id) LIKE UPPER(:erp_id)',
            ['codeDocumentType' => DocumentTypeEnum::BeforeDDT, 'erp_id' => $search . '%']
        ));
    }
  
  /**
     * Get select
     *
     * @param String $search
     * @return Customer
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return Customer::select('id', 'business_name as description')->get();
        } else {
            return Customer::where('business_name', 'ilike', $search . '%')->select('id', 'business_name as description')->get();
        }
    }




    /**
     * Get all
     * 
     * @return DtoContact
     */
    public static function getAll()
    {
        return Customer::all()->map(function ($contact) {
            return static::_convertToDto($contact);
        });
    }

    /**
     * Get by id
     * 
     * @param int id
     * 
     * @return DtoContact
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Customer::find($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request dtoCustomer
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $dtoCustomer, int $idLanguage)
    {
        static::_validateData($dtoCustomer, $idLanguage, DbOperationsTypesEnum::INSERT);
        $customer = static::_convertToModel($dtoCustomer);
        $customer->save();
        return $customer->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Set error on newsletter
     * 
     * @param string email
     */
    public static function setNewsletterError(string $email)
    {
        $return = 0;
        $customer = Customer::where('email', $email)->first();
        if(isset($customer)){
            $customer->error_newsletter = true;
            $customer->save();
            $return =  $customer->id;
        }
        return $return;
    }

    /**
     * Update
     * 
     * @param Request dtoContact
     * @param int idLanguage
     */
    public static function update(Request $dtoCustomer, int $idLanguage)
    {
        static::_validateData($dtoCustomer, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $customerOnDb = Customer::find($dtoCustomer->id);
        DBHelper::updateAndSave(static::_convertToModel($dtoCustomer), $customerOnDb);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $customer = Customer::find($id);

        if (is_null($customer)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $customer->delete();
    }

    #endregion DELETE
}

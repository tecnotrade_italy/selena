<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoOptional;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\ItemOptional;
use App\Optional;
use App\UnitMeasure;

use App\OptionalLanguages;
use App\OptionalTypology;
use App\ItemSupport;
use App\TypeOptionalLanguages;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OptionalBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Optional::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    #endregion PRIVATE

    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoOptional
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (DB::table('optional')
                ->select('optional.*', 'optional_languages.description')
                ->join('optional_languages', 'optional.id', '=', 'optional_languages.optional_id')
                ->where('optional_languages.language_id', $idLanguage)
                ->orderBy('optional.order')
                ->get()
        as $optionalAll) {
            
            $dtoOptional = new DtoOptional();
            $dtoOptional->id = $optionalAll->id;
            $dtoOptional->order = $optionalAll->order;
            $dtoOptional->description = $optionalAll->description;

            $optionTypology = OptionalTypology::where('optional_id', $optionalAll->id)->get()->first();
            if(isset($optionTypology)){
                $typeOptional = TypeOptionalLanguages::where('type_optional_id', $optionTypology->type_optional_id)->get()->first();
                $dtoOptional->typeOptional = $typeOptional->description;
                $dtoOptional->typeOptionalId = $typeOptional->id;
            }else{
                $dtoOptional->typeOptional = '';
                $dtoOptional->typeOptionalId = '';
            }
                        
            $result->push($dtoOptional); 
        }
        
        return $result;
    }


    /**
     * Get all by Type Optional
     * 
     * @param int idType
     * @param int idLanguage
     * @param int idItem
     * 
     * @return DtoOptional
     */
    public static function getAllByType($idType, $idLanguage, $idItem)
    {
        $result = collect();
        foreach (DB::table('optional')
                ->select('optional.*', 'optional_languages.description')
                ->join('optional_languages', 'optional.id', '=', 'optional_languages.optional_id')
                ->join('optional_typology', 'optional.id', '=', 'optional_typology.optional_id')
                ->where('optional_languages.language_id', $idLanguage)
                ->where('optional_typology.type_optional_id', $idType)
                ->orderBy('optional.order')
                ->get()
        as $optionalAll) {
            
            $dtoOptional = new DtoOptional();
            $dtoOptional->id = $optionalAll->id;
            $dtoOptional->order = $optionalAll->order;
            $dtoOptional->description = $optionalAll->description;
            
            /* controllo tabella item_optional per inserire dati di variazione */
            $itemOptional = ItemOptional::where('item_id', $idItem)->where('optional_id', $optionalAll->id)->get()->first();
            if(isset($itemOptional)){
                $dtoOptional->selected = true;
                $dtoOptional->availability = $itemOptional->availability;
                $dtoOptional->variationType = $itemOptional->variation_type;
                $dtoOptional->variationPrice = $itemOptional->variation_price;
                $dtoOptional->umSupportVariationId = $itemOptional->um_support_variation_id;
                $codeUm = UnitMeasure::where('id',  $itemOptional->um_support_variation_id)->get()->first();
                if(isset($codeUm)){
                    $dtoOptional->umSupportVariationDescription = $codeUm->code;
                }else{
                    $dtoOptional->umSupportVariationDescription = 'Um';
                }
                
                $dtoOptional->supportParameter = $itemOptional->support_prameter;
                $dtoOptional->supportVariationValue = $itemOptional->support_variation_value;

                $itemSupport = ItemSupport::where('item_id', $idItem)->get()->first();
                if(isset($itemSupport)){
                    $dtoOptional->checkItemSupport = true;
                }else{
                    $dtoOptional->checkItemSupport = false;
                }

            }else{
                $dtoOptional->selected = false;
            }
            /* end controllo tabella item_optional per inserire dati di variazione */

            $result->push($dtoOptional); 
        }
        
        return $result;
    }
    

    
    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $optional = new Optional();
            $optional->order = $request->order;
            $optional->save();
            
            $optionalLanguages = new OptionalLanguages();
            $optionalLanguages->optional_id = $optional->id;
            $optionalLanguages->description = $request->description;
            $optionalLanguages->language_id = $idLanguage;
            $optionalLanguages->created_id = $idUser;
            $optionalLanguages->save();

            $optionalTypology = new OptionalTypology();
            $optionalTypology->optional_id = $optional->id;
            $optionalTypology->type_optional_id = $request->typeOptionalId;
            $optionalTypology->created_id = $idUser;
            $optionalTypology->save();

            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        //return $optional->id;
    }


    /**
     * insert Item Optional
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertItemOptional(Request $request, int $idUser, int $idLanguage)
    {
        DB::beginTransaction();
        try {
            
            $itemOptionOnDb = ItemOptional::where('item_id', $request->item_id);
            if(isset($itemOptionOnDb)){
                $itemOptionOnDb->delete();
            }

            foreach($request->optional as $itemOptional){
                $itemOptionalNew = new ItemOptional();
                $itemOptionalNew->item_id = $itemOptional['item_id'];
                $itemOptionalNew->optional_id = $itemOptional['optional_id']; 
                $itemOptionalNew->availability = $itemOptional['availability'];
                if(isset($itemOptional['variation_type'])){
                    $itemOptionalNew->variation_type = $itemOptional['variation_type'];
                }else{
                    $itemOptionalNew->variation_type = '€';
                }
                if(isset($itemOptional['variation_price'])){
                    $itemOptionalNew->variation_price = $itemOptional['variation_price'];
                }
                if(isset($itemOptional['umSupportVariationId'])){
                    if(!is_null($itemOptional['umSupportVariationId']) && $itemOptional['umSupportVariationId']!=''){
                        $itemOptionalNew->um_support_variation_id = $itemOptional['umSupportVariationId'];
                    }
                }
                if(isset($itemOptional['supportParameter'])){
                    if(!is_null($itemOptional['supportParameter']) && $itemOptional['supportParameter']!=''){
                        $itemOptionalNew->support_prameter = $itemOptional['supportParameter'];
                    }
                }
                if(isset($itemOptional['supportVariationValue'])){
                    if(!is_null($itemOptional['supportVariationValue']) && $itemOptional['supportVariationValue']!=''){
                        $itemOptionalNew->support_variation_value = $itemOptional['supportVariationValue'];
                    }
                }
                
                $itemOptionalNew->save();
            }
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

       return '';
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoOptional
     * @param int idLanguage
     */
    public static function update(Request $dtoOptional, int $idLanguage)
    {
        static::_validateData($dtoOptional, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $optionalOnDb = Optional::find($dtoOptional->id);
        
        DB::beginTransaction();

        try {
            $optionalOnDb->order = $dtoOptional->order;
            $optionalOnDb->save();
            
            $optionalLanguageOnDb = OptionalLanguages::where('optional_id', $optionalOnDb->id)->where('language_id', $idLanguage)->get()->first();
            $optionalLanguageOnDb->description = $dtoOptional->description;
            $optionalLanguageOnDb->save();

            $optionalTypologyOnDb = OptionalTypology::where('optional_id', $optionalOnDb->id)->get()->first();
            $optionalTypologyOnDb->type_optional_id = $dtoOptional->typeOptionalId;
            $optionalTypologyOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $optional = Optional::find($id);
        
        if (is_null($optional)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            
            $optionalLanguages = OptionalLanguages::where('optional_id', $id);
            $optionalLanguages->delete();

            $optionalTypology = OptionalTypology::where('optional_id', $id);
            $optionalTypology->delete();

            $optional->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    /**
     * Delete item optional
     * 
     * @param int idOptional
     * @param int idItem
     * @param int idLanguage
     */
    public static function delItemOptional(int $idOptional, int $idItem, int $idLanguage)
    {
        $itemOptional = ItemOptional::where('optional_id', $idOptional)->where('item_id', $idItem);
        
        if (is_null($itemOptional)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            $itemOptional->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

    }

    #endregion DELETE
}

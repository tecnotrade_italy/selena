<?php

namespace App\BusinessLogic;

use App\Carriage;
use App\CarriageLanguage;
use App\Carrier;
use App\GrDealer;
use App\GrUserDifferentDestination;
use App\CarrierLanguage;
use App\Cart;
use App\CartRules;
use App\CartRulesUsers;
use App\Cities;
use App\Content;
use App\ContentLanguage;
use App\CommunityImage;
use App\CommunityFavoriteImage;
use App\CommunityAction;
use App\CommunityActionType;
use App\PriceList;
use App\DtoModel\DtoAddress;
use App\DtoModel\DtoUser;
use App\DtoModel\DtoLogsDetail;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use App\Language;
use App\PostalCode;
use App\Role;
use App\RoleUser;
use App\People;
use App\PeopleUser;
use App\RolesPeople;
use App\UsersDatas;
use App\UserRescuePassword;
use App\Mail\UserRescuePasswordMail;
use App\DtoModel\DtoPeople;
use App\DtoModel\DtoUserPhoto;
use App\Subscriber;
use App\CorrelationPeopleUser;
use App\DtoModel\DtoCartRules;
use App\UserPhoto;
use App\VatType;
use App\UsersAddresses;
use App\PaymentGr;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Mockery\CountValidator\Exception;
use App\DtoModel\DtoUrlList;
use App\DtoModel\DtoCommunityAction;
use App\DtoModel\DtoUrlListDetail;
use App\DtoModel\DtoPageSitemap;
use App\Mail\ConfirmRegistrationMail;
use App\Mail\DynamicMail;
use App\Message;
use App\MessageLanguage;
use App\DtoModel\DtoGrDifferentDestination;
use App\DtoModel\DtoUserAddress;
use App\DtoModel\DtoSalesOrder;
use App\SalesOrder;
use App\DocumentStatusLanguage;
use App\DtoModel\DtoLogs;
use App\UserAddress;
use App\Logs;
use App\LanguageNation;
use App\LanguagePaymentType;
use App\LanguageProvince;
use App\Newsletter;
use App\Setting;
use App\PaymentType;
use App\Province;
use App\Provinces;
use App\Region;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class UserBL
{

    public static $dirImage = '../storage/app/public/images/User/';
    public static $dirImagesGeneral = '../storage/app/public/images/';
    private static $mimeTypeAllowed = ['jpeg', 'jpg', 'png', 'gif'];

    #region PRIVATE

    private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/api/storage/images/User/';
    }

    private static function getUrlGeneral()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/api/storage/images/';
    }

    /**
     * Validate data
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * @param DbOperationTypeEnum dbOperationsTypesEnum
     */
    private static function _validateData(Request $request, int $idUser, int $idLanguage, $dbOperationsTypesEnum)
    {

        $configurations = FieldUserRoleBL::checkFunctionalityEnabled($request->idFunctionality, $idUser);

        if (is_null($configurations)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->username)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UsernameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailNotValid), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->idLanguage)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::LanguageNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Language::find($request->idLanguage))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::LanguageNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        switch ($dbOperationsTypesEnum) {
            case DbOperationsTypesEnum::INSERT:

                if (User::where('name', $request->username)->count() > 0) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UsernameAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }

                if (is_null($request->password)) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PasswordNotValued), HttpResultsCodesEnum::InvalidPayload);
                }

                if (User::where('email', $request->email)->count() > 0) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }
                break;

               

            case DbOperationsTypesEnum::UPDATE:
                if (is_null($request->id)) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
                } else {
                    if (is_null(User::find($request->id))) {
                        throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotFound), HttpResultsCodesEnum::InvalidPayload);
                    }
                }

                if (User::where('name', $request->username)->whereNotIn('id', [$request->id])->count() > 0) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UsernameAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }

                if (User::where('email', $request->email)->whereNotIn('id', [$request->id])->count() > 0) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }
                break;
        }
    }

    /**
     * Validate data
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * @param DbOperationTypeEnum dbOperationsTypesEnum
     */
    private static function _validateDataNoAuth(Request $request, int $idLanguage, $dbOperationsTypesEnum)
    {
        switch ($dbOperationsTypesEnum) {
            case DbOperationsTypesEnum::INSERT:
               if (User::where('name', $request->email)->count() > 0) {
                   throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UsernameAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }

                //  if (is_null($request->password)) {
                //   throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PasswordNotValued), HttpResultsCodesEnum::InvalidPayload);
                // }

                if (User::where('email', $request->email)->count() > 0) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }
                if ($request['password'] != $request['password_confirmation']) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PasswordWrong), HttpResultsCodesEnum::InvalidPayload);
                }
     
                break;

            case DbOperationsTypesEnum::UPDATE:
                if (is_null($request->id)) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
                } else {
                    if (is_null(User::find($request->id))) {
                        throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotFound), HttpResultsCodesEnum::InvalidPayload);
                    }
                }

                if (User::where('name', $request->username)->whereNotIn('id', [$request->id])->count() > 0) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UsernameAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }

                if (User::where('email', $request->email)->whereNotIn('id', [$request->id])->count() > 0) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }

                // if (User::find($request->password != $request['password_confirmation'])){
                // throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PasswordWrong), HttpResultsCodesEnum::InvalidPayload);      
                // }

                // if ($request['password'] != $request['password_confirmation']){
                //    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PasswordWrong), HttpResultsCodesEnum::InvalidPayload);      
                // }

                break;
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get url list
     *
     * @param int idLanguage
     * @return DtoUrlList
     */
    public static function getUrlSitemap(int $idLanguage)
    {
        $dtoUrlLinks = collect();
        $idDefaultLanguage = LanguageBL::getDefault();

        foreach (User::all() as $page) {
            $languageFound = false;

            $dtoUrlList = new DtoUrlList();
            $dtoUrlList->idPage = $page->id;

            foreach (UsersDatas::where('user_id', $page->id)->get() as $languageItem) {
                $dtoUrlList->title = $languageItem->business_name;
                $languageFound = true;

                $dtoUrlLinkDetail = new DtoUrlListDetail();
                $dtoUrlLinkDetail->id = $languageItem->id;
                $dtoUrlLinkDetail->url = $languageItem->link;
                $dtoUrlList->urlListDetail->push($dtoUrlLinkDetail);
            }

            $dtoUrlLinks->push($dtoUrlList);
        }

        return $dtoUrlLinks;
    }

    public static function getPages(string $page_type, int $idLanguage)
    {

        $result = collect();
        foreach (DB::select(
            "SELECT users_datas.business_name, users.updated_at, users_datas.link
            FROM users
            INNER JOIN users_datas ON users.id = users_datas.user_id
            ORDER BY users.updated_at desc"
        ) as $PagesAll) {
            $dtoPageSitemap = new DtoPageSitemap();
            $dtoPageSitemap->url = $PagesAll->link;
            $dtoPageSitemap->title = $PagesAll->business_name;
            $dtoPageSitemap->updated_at = $PagesAll->updated_at;

            $result->push($dtoPageSitemap);
        }
        return $result;
    }

    /**
     * Get select
     *
     * @param String $search
     * @return User
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return UsersDatas::select('user_id as id', 'business_name as description')->all();
        } else {
            return UsersDatas::where('business_name', 'ilike', $search . '%')->select('user_id as id', 'business_name as description')->get();
        }
    }

    /**
     * Get select
     *
     * @param String $search
     * @return User
     */
    public static function getSelectProv(string $search)
    {
        if ($search == "*") {

            return collect(DB::select(
                "SELECT distinct UPPER(province) as id, UPPER(province) as description
                from users_datas ORDER BY description"
            ));
            return User::select('id', 'name as description')->all();
        } else {
            return collect(DB::select(
                "SELECT distinct UPPER(province) as id, UPPER(province) as description
                from users_datas where province ilike '" . $search . "%' ORDER BY description"
            ));
        }
    }

    /**
     * Get select
     *
     * @param String $search
     * @return User
     */
    public static function getSelectIntervention(string $search)
    {
        if ($search == "*") {
            return collect(DB::select(
                "SELECT distinct 
                where categories_items.category_id = :idCategories AND p1.business_name ilike '" . $search . "%' " . $strWhere .  ' ORDER BY cont DESC',
                ['idCategories' => $idCategories]
            ));
            return UsersDatas::select('user_id', 'business_name as description')->all();
        } else {
            return UsersDatas::where('business_name', 'ilike', $search . '%')->select('user_id', 'business_name as description')->get();
        }
    }

    /**
     * Get select user pages
     *
     * @param String $search
     * @return User
     */
    public static function getSelectUserPages(string $search)
    {
        if ($search == "*") {
            return User::select('id', 'name as description')->all();
        } else {
            return User::where('name', 'ilike', $search . '%')->select('id', 'name as description')->get();
        }
    }

    /**
     * Get graphic
     *
     * @param int $idLanguage
     */
    public static function getGraphic()
    {
        $rows = DB::select("select count(id) as number from users GROUP BY created_at");
        $array1 = array();
        foreach (ArrayHelper::toCollection($rows) as $row) {
            array_push($array1, $row->number);
        }

        return $array1;
    }

    /**
     * Get by id
     *
     * @param int $id
     * @return User
     */
    public static function getById(int $id)
    {
        return User::find($id);
    }


    /**
     * Get by id
     *
     * @param int $id
     * @return User
     */
    public static function getCommunityData(int $id)
    {
        $user = User::where('id', $id)->first();
        $userdata = UsersDatas::where('user_id', $id)->first();
        $dtoUser = "";
        if (isset($userdata)) {
            $dtoUser = new DtoUser();
            $dtoUser->community_send_notification_comments = $userdata->community_send_notification_comments;
            $dtoUser->community_send_notification_favorites = $userdata->community_send_notification_favorites;
            $dtoUser->community_send_notification_star_rates = $userdata->community_send_notification_star_rates;
            $dtoUser->facebook = $userdata->facebook;
            $dtoUser->twitter = $userdata->twitter;
            $dtoUser->instagram = $userdata->instagram;
            $dtoUser->youtube = $userdata->youtube;
            $dtoUser->linkedin = $userdata->linkedin;
            $dtoUser->website = $userdata->website;
        }
        return  $dtoUser;
    }

    public static function getDetailUsers(Int $id){
 

       $GrUserDifferentDestination = GrUserDifferentDestination::where('user_id', $id)->first();
       $GrUser = User::where('id', $id)->first();
       $GrUserDatas = UsersDatas::where('user_id', $id)->first();
       $UsersAddresses = UsersAddresses::where('user_id', $id)->first();

       $DtoGrDifferentDestination = new DtoGrDifferentDestination();

       $DtoGrDifferentDestination->user_id = $id;
   
       if (isset($GrUserDatas->name)) {
            $DtoGrDifferentDestination->Name = $GrUserDatas->name;
        }else {
            $DtoGrDifferentDestination->Name = '';     
        }
        if (isset($GrUserDatas->surname)) {
            $DtoGrDifferentDestination->Surname = $GrUserDatas->surname;
        }else {
            $DtoGrDifferentDestination->Surname = '';     
        }
        if (isset($GrUserDatas->business_name)) {
            $DtoGrDifferentDestination->Business_name = $GrUserDatas->business_name;
        }else {
            $DtoGrDifferentDestination->Business_name = '';     
        }
        if (isset($GrUserDatas->vat_number)) {
            $DtoGrDifferentDestination->Vat_number = $GrUserDatas->vat_number;
        }else {
            $DtoGrDifferentDestination->Vat_number = '';     
        }
        if (isset($GrUserDatas->fiscal_code)) {
            $DtoGrDifferentDestination->Fiscal_code = $GrUserDatas->fiscal_code;
        }else {
            $DtoGrDifferentDestination->Fiscal_code = '';     
        }
        if (isset($GrUserDatas->address)) {
            $DtoGrDifferentDestination->Address = $GrUserDatas->address;
        }else {
            $DtoGrDifferentDestination->Address = '';     
        }
        if (isset($GrUserDatas->postal_code)) {
            $DtoGrDifferentDestination->Code_postal = $GrUserDatas->postal_code;
        }else {
            $DtoGrDifferentDestination->Code_postal = '';     
        }
        if (isset($GrUserDatas->country)) {
            $DtoGrDifferentDestination->Country = $GrUserDatas->country;
        }else {
            $DtoGrDifferentDestination->Country = '';     
        }

        if (isset($GrUserDatas->province)) {
            $DtoGrDifferentDestination->Province = $GrUserDatas->province;
        }else {
            $DtoGrDifferentDestination->Province = '';     
        }
        if (isset($GrUserDatas->telephone_number)) {
            $DtoGrDifferentDestination->Telephone_number = $GrUserDatas->telephone_number;
        }else {
            $DtoGrDifferentDestination->Telephone_number = '';     
        }

        if (isset($GrUserDatas->mobile_number)) {
            $DtoGrDifferentDestination->Mobile_number = $GrUserDatas->mobile_number;
        }else {
            $DtoGrDifferentDestination->Mobile_number = '';     
        }
        if (isset($GrUser->email)) {
            $DtoGrDifferentDestination->Email = $GrUser->email;
        }else {
            $DtoGrDifferentDestination->Email = '';     
        }

        if (isset($UsersAddresses->id)) {
            $DtoGrDifferentDestination->id_useraddress = $UsersAddresses->id;
        }else {
            $DtoGrDifferentDestination->id_useraddress = '';     
        }

         if (isset($UsersAddresses->online)) {
            $DtoGrDifferentDestination->online = $UsersAddresses->online;
         }
         if (isset($UsersAddresses->address)) {
            $DtoGrDifferentDestination->address = $UsersAddresses->address;
         }else {
            $DtoGrDifferentDestination->address = '';     
        }
         if (isset($UsersAddresses->postal_code)) {
            $DtoGrDifferentDestination->postal_code = $UsersAddresses->postal_code;
         }else {
            $DtoGrDifferentDestination->postal_code = '';     
        }
         if (isset($UsersAddresses->locality)) {
            $DtoGrDifferentDestination->locality = $UsersAddresses->locality;
         }else {
            $DtoGrDifferentDestination->locality = '';     
        }
         if (isset($UsersAddresses->province_id)) {
            $DtoGrDifferentDestination->province_id = $UsersAddresses->province_id;
         }else {
            $DtoGrDifferentDestination->province_id = null;     
        }
         if (isset($UsersAddresses->nation_id)) {
            $DtoGrDifferentDestination->nation_id = $UsersAddresses->nation_id;
         }
         if (isset($UsersAddresses->phone_number)) {
            $DtoGrDifferentDestination->phone_number = $UsersAddresses->phone_number;
         }else {
            $DtoGrDifferentDestination->phone_number = '';     
        }
         if (isset($UsersAddresses->business_name)) {
            $DtoGrDifferentDestination->business_name = $UsersAddresses->business_name;
         }else {
            $DtoGrDifferentDestination->business_name = '';     
        }
         if (isset($UsersAddresses->name)) {
            $DtoGrDifferentDestination->name = $UsersAddresses->name;
         }else {
            $DtoGrDifferentDestination->name = '';     
        }
          if (isset($UsersAddresses->surname)) {
            $DtoGrDifferentDestination->surname = $UsersAddresses->surname;
         }else {
            $DtoGrDifferentDestination->surname = '';     
        }
         if (isset($UsersAddresses->region_id)) {
            $DtoGrDifferentDestination->region_id = $UsersAddresses->region_id;
         }      

         return  $DtoGrDifferentDestination;
    }

  /**
     * getDifferentDestination
     *
     * @param int $id
     * @return User
     */
    public static function getDifferentDestination(Int $id){

        $GrUserDifferentDestination = GrUserDifferentDestination::where('user_id', $id)->first();
        $DtoGrDifferentDestination = new DtoGrDifferentDestination();

        if (isset($GrUserDifferentDestination->id)) {
            $DtoGrDifferentDestination->id = $GrUserDifferentDestination->id;
        }else {
            $DtoGrDifferentDestination->id = '';     
        }
       
        if (isset($GrUserDifferentDestination->user_id)) {
            $DtoGrDifferentDestination->user_id = $id;
        }else {
            $DtoGrDifferentDestination->user_id = '';     
        }

        if (isset($GrUserDifferentDestination->business_name_plus)) {
            $DtoGrDifferentDestination->business_name_plus = $GrUserDifferentDestination->business_name_plus;
        }else {
            $DtoGrDifferentDestination->business_name_plus = '';     
        }

        if (isset($GrUserDifferentDestination->address)) {
            $DtoGrDifferentDestination->address = $GrUserDifferentDestination->address;
        }else {
            $DtoGrDifferentDestination->address = '';     
        }

        if (isset($GrUserDifferentDestination->codepostal)) {
            $DtoGrDifferentDestination->codepostal = $GrUserDifferentDestination->codepostal;
        }else {
            $DtoGrDifferentDestination->codepostal = '';     
        }

        if (isset($GrUserDifferentDestination->province)) {
            $DtoGrDifferentDestination->province = $GrUserDifferentDestination->province;
        }else {
            $DtoGrDifferentDestination->province = '';     
        }

        if (isset($GrUserDifferentDestination->country)) {
            $DtoGrDifferentDestination->country = $GrUserDifferentDestination->country;
        }else {
            $DtoGrDifferentDestination->country = '';     
        }
        if (isset($GrUserDifferentDestination->telephone)) {
            $DtoGrDifferentDestination->telephone = $GrUserDifferentDestination->telephone;
        }else {
            $DtoGrDifferentDestination->telephone = '';     
        }

        return  $DtoGrDifferentDestination;
    }


     public static function getbyIdAddress(int $id)
    {
       $UsersAddresses = UsersAddresses::where('id', $id)->first();
       $DtoUserAddress = new DtoUserAddress();

        if (isset($UsersAddresses->id)) {
           $DtoUserAddress->id_useraddress = $UsersAddresses->id;
        }
        if (isset($UsersAddresses->description)) {
           $DtoUserAddress->description = $UsersAddresses->description;
        }
        if (isset($UsersAddresses->online)) {
           $DtoUserAddress->online = $UsersAddresses->online;
        }
        if (isset($UsersAddresses->address)) {
           $DtoUserAddress->address = $UsersAddresses->address;
        }
        if (isset($UsersAddresses->postal_code)) {
           $DtoUserAddress->postal_code = $UsersAddresses->postal_code;
        }
        if (isset($UsersAddresses->locality)) {
           $DtoUserAddress->locality = $UsersAddresses->locality;
        }
        if (isset($UsersAddresses->province_id)) {
           $DtoUserAddress->province_id = $UsersAddresses->province_id;
        }
        return  $DtoUserAddress;
       
    }
        

    
    public static function getAllUserSales(int $id)
    {
        $result = collect();
        foreach (CartRulesUsers::where('user_id', $id)->get() as $CartRuleUsers) {
            if (isset($CartRuleUsers)) {
                $CartRules = CartRules::where('id', $CartRuleUsers->cart_rule_id)->first();
              
                $DtoCartRules = new DtoCartRules();

                if (isset($CartRules->id)) {
                    $DtoCartRules->id = $CartRules->id;
                }
                if (isset($CartRules->name)) {
                    $DtoCartRules->object = $CartRules->name;
                }
                if (isset($CartRules->description)) {
                    $DtoCartRules->description = $CartRules->description;
                }
                if (isset($CartRules->enabled_from)) {
                    $DtoCartRules->enabled_from = $CartRules->enabled_from;
                }
                if (isset($CartRules->enabled_to)) {
                    $DtoCartRules->enabled_to = $CartRules->enabled_to;
                }
                    $result->push($DtoCartRules); 
            }
        }
        return  $result;
    }

    public static function getAllUserHistorical(int $id)
    {
        $result = collect();
        foreach (SalesOrder::where('user_id', $id)->get() as $SalesOrder) {
          
    
                $DtoSalesOrder = new DtoSalesOrder();

                $DtoSalesOrder->code = $SalesOrder->id;  

                if (isset($SalesOrder->date_order)){
                    $DtoSalesOrder->date_order = $SalesOrder->date_order;   
                }else{
                    $DtoSalesOrder->date_order ='-';  
                }
                if (isset($SalesOrder->note)){
                    $DtoSalesOrder->note = $SalesOrder->note;  
                }else{
                    $DtoSalesOrder->note ='-';  
                }
                if (isset($SalesOrder->payment_type_id)){
                    $LanguagePaymentType = LanguagePaymentType::where('payment_type_id', $SalesOrder->payment_type_id)->first()->description;
                    $DtoSalesOrder->payment_type_id =$LanguagePaymentType;
                }else{
                    $DtoSalesOrder->payment_type_id ='-';  
                }

                if (isset($SalesOrder->order_state_id)){
                    $DtoSalesOrder->order_state_id = DocumentStatusLanguage::where('document_status_id', $SalesOrder->order_state_id)->first()->description;                   
                }else{
                    $DtoSalesOrder->order_state_id = '-';   
                }

                if (isset($SalesOrder->net_amount)){
                    $DtoSalesOrder->net_amount = '€ ' . str_replace('.',',',substr($SalesOrder->net_amount, 0, -2));
                    ;  
                }else{
                    $DtoSalesOrder->net_amount ='-';  
                }

                if (isset($SalesOrder->total_amount)){
                    $DtoSalesOrder->total_amount = '€ ' . str_replace('.',',',substr($SalesOrder->total_amount, 0, -2));
                     
                }else{
                    $DtoSalesOrder->total_amount ='-';  
                }
               
                    $result->push($DtoSalesOrder); 
            }
        
        return  $result;
    }

    

    public static function getAllDetailValueSalesUser(int $id)
    {
        $result = collect();

        $UsersDatas = UsersDatas::where('user_id', $id)->first();
        $DtoUsersDatas = new DtoUser();
        if (isset($UsersDatas)) {

            if (isset($UsersDatas->user_id)) {
                $DtoUsersDatas->user_id = $UsersDatas->user_id;
            }
            if (isset($UsersDatas->id_list)) {
                $DtoUsersDatas->id_list = $UsersDatas->id_list;
                $DtoUsersDatas->DescriptionList = PriceList::where('id', $UsersDatas->id_list)->first()->description;   
             }else{
                $DtoUsersDatas->id_list = '';
                $DtoUsersDatas->DescriptionList = '';
             }
             if (isset($UsersDatas->vat_type_id)) {
                $DtoUsersDatas->vat_type_id = $UsersDatas->vat_type_id;
                $DtoUsersDatas->descriptionVatType = VatType::where('id', $UsersDatas->vat_type_id)->first()->rate;
             }else{
                $DtoUsersDatas->vat_type_id = '';
                $DtoUsersDatas->descriptionVatType = '';
             }
             if (isset($UsersDatas->id_payment)) {
                $DtoUsersDatas->id_payment = $UsersDatas->id_payment;
                $DtoUsersDatas->DescriptionPayments = LanguagePaymentType::where('payment_type_id', $UsersDatas->id_payment)->first()->description;
             }else{
                $DtoUsersDatas->id_payment = '';
                $DtoUsersDatas->DescriptionPayments = '';
             }
             if (isset($UsersDatas->id_carriage)) {
                $DtoUsersDatas->carriage_id = $UsersDatas->id_carriage;
                $DtoUsersDatas->DescriptionCarriage = CarriageLanguage::where('carriage_id', $UsersDatas->id_carriage)->first()->description;
             }else{
                $DtoUsersDatas->carriage_id = '';
                $DtoUsersDatas->DescriptionCarriage = '';
             }
             if (isset($UsersDatas->carrier_id)) {
                $DtoUsersDatas->carrier_id = $UsersDatas->carrier_id;
                $DtoUsersDatas->DescriptionCarrier = CarrierLanguage::where('carrier_id', $UsersDatas->carrier_id)->first()->description;
             }else{
                $DtoUsersDatas->carrier_id = '';
                $DtoUsersDatas->DescriptionCarrier = '';
             }
                $DtoUsersDatas->payment_fixed = $UsersDatas->payment_fixed;
                $DtoUsersDatas->vat_exempt = $UsersDatas->vat_exempt; 
            }
                $result->push($DtoUsersDatas);

        return  $result;
    }


    

/**
     * getAllReferent
     * 
     * @param int id
     * @return DtoUser
     */
    public static function getAllAddress(int $id)
    {
        $result = collect();

        foreach (UsersAddresses::where('user_id', $id)->get() as $UsersAddresses) {
            
            if (isset($UsersAddresses)) {
                $DtoUserAddress = new DtoUserAddress();
                if (isset($UsersAddresses->id)) {
                    $DtoUserAddress->code = $UsersAddresses->id;
                }
                if (isset($UsersAddresses->address)) {
                    $DtoUserAddress->address = $UsersAddresses->address;
                }
                if (isset($UsersAddresses->default_address)) {
                    $DtoUserAddress->DefaultAddress = $UsersAddresses->default_address;
                }
                if (isset($UsersAddresses->online)) {
                    $DtoUserAddress->Online = $UsersAddresses->online;
                }
                 if (isset($UsersAddresses->description)) {
                    $DtoUserAddress->Description = $UsersAddresses->description;
                }
                if (isset($UsersAddresses->postal_code)) {
                    $DtoUserAddress->postal_code = $UsersAddresses->postal_code;
                }
                 if (isset($UsersAddresses->province_id)) {
                    $DtoUserAddress->province_id = $UsersAddresses->province_id;
                }
                if (isset($UsersAddresses->locality)) {
                    $DtoUserAddress->locality = $UsersAddresses->locality;
                }
                 if (isset($UsersAddresses->description)) {
                    $DtoUserAddress->description = $UsersAddresses->description;
                }
                $result->push($DtoUserAddress);
            }
        }
        return  $result;
    }

    public static function updateCheckOnlineAddress(Request $request)
    {
      
        $UsersAddresses = UsersAddresses::where('id', $request->id)->where('user_id', $request->user_id)->first();
        DB::beginTransaction();

        try {
            if (isset($UsersAddresses->online)) {
                    $UsersAddresses->online = !$UsersAddresses->online;
            }
                $UsersAddresses->save();
        
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        return  $request->id_useraddress;
    }


    public static function updateCheckDetailDefaultAddress(Request $request)
    {
        $UsersAddresses = UsersAddresses::where('user_id', $request->user_id)->get();
        if (isset($UsersAddresses)) {
            foreach ($UsersAddresses as $UsersAddressess) {   
                    
                $UsersAddressess->default_address = false;
                $UsersAddressess->save();
            }
        }
            $UsersAddress = UsersAddresses::where('id', $request->id)->first();
                $UsersAddress->default_address = true;
                $UsersAddress->save();
        return  $request->id;
    }


    public static function updateCheckOnlineReferentDetail(Request $request)
    {
            $People = People::where('id', $request->id_people)->first();
            DB::beginTransaction();
    
            try {
                if (isset($People->online)) {
                        $People->online = !$People->online;
                }
                    $People->save();

            $CorrelationPeopleUser = CorrelationPeopleUser::where('id_people', $request->id_people)->first();
            if (isset($CorrelationPeopleUser->id_user)) {
                $UserOnDb = User::where('id', $CorrelationPeopleUser->id_user)->first(); 
                    if (isset($UserOnDb->online)) {
                        $UserOnDb->online = !$UserOnDb->online;
                    }
                    $UserOnDb->save();     
            }


            
                    DB::commit();
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
            return  $request->id_people;

    }
    

    public static function updateSalesUser(Request $request)
    {
       
        $UsersDatasondb= UsersDatas::where('user_id', $request->user_id)->first();

        DB::beginTransaction();
        try {
            if (!isset($UsersDatasondb->user_id)) { 
                $UsersDatas = new UsersDatas();
                $UsersDatas->user_id = $request->user_id;
                $UsersDatas->id_list = $request->id_list;
                $UsersDatas->carrier_id = $request->carrier_id;
                $UsersDatas->id_payment = $request->id_payment;
                $UsersDatas->id_carriage = $request->carriage_id;
                $UsersDatas->vat_type_id = $request->vat_type_id;
                $UsersDatas->vat_exempt = $request->vat_exempt;
                $UsersDatas->payment_fixed = $request->payment_fixed;
                $UsersDatas->save();
            }else{
                if (isset($request->id_list)) {
                    $UsersDatasondb->id_list = $request->id_list;
                }
                if (isset($request->carrier_id)) {
                    $UsersDatasondb->carrier_id = $request->carrier_id;
                }
                if (isset($request->id_payment)) {
                    $UsersDatasondb->id_payment = $request->id_payment;
                }
                if (isset($request->carriage_id)) {
                    $UsersDatasondb->id_carriage = $request->carriage_id;
                }
                if (isset($request->vat_type_id)) {
                    $UsersDatasondb->vat_type_id = $request->vat_type_id;
                }
                if (isset($request->vat_exempt)) {
                    $UsersDatasondb->vat_exempt = $request->vat_exempt;
                }
                if (isset($request->payment_fixed)) {
                    $UsersDatasondb->payment_fixed = $request->payment_fixed;
                }
                    $UsersDatasondb->save();

            }
            
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
             return  $request->user_id;
    }


    

    public static function updateUserAddress(Request $request)
    {
        $UsersAddresses = UsersAddresses::find($request->id_useraddress);

        DB::beginTransaction();
        try {
            if (isset($request->id_useraddress)) {
                    $UsersAddresses->id = $request->id_useraddress;
            }
            if (isset($request->address)) {
                    $UsersAddresses->address = $request->address;
            }
            if (isset($request->locality)) {
                    $UsersAddresses->locality = $request->locality;
            }
            if (isset($request->province_id)) {
                    $UsersAddresses->province_id = $request->province_id;
            }
            if (isset($request->postal_code)) {
                    $UsersAddresses->postal_code = $request->postal_code;
            }
            if (isset($request->online)) {
                    $UsersAddresses->online = $request->online;
            }
            if (isset($request->description)) {
                    $UsersAddresses->description = $request->description;
            }
                $UsersAddresses->save();
        
                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }

        return  $request->id_useraddress;
    }

            
             /**
     * insertUserAddress
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int id
     */
    public static function insertUserAddress(Request $request, int $idLanguage)
    {
        DB::beginTransaction();

        try {
          $UsersAddresses = new UsersAddresses();
          $UsersAddresses->user_id = $request->user_id;
          $UsersAddresses->province_id = $request->province_id;
          $UsersAddresses->locality = $request->locality;
          $UsersAddresses->description = $request->description;
          $UsersAddresses->address = $request->address;
          $UsersAddresses->postal_code = $request->postal_code;
          $UsersAddresses->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $UsersAddresses->id;
    }


    public static function insertLoginForGoogle(Request $request)
    {
    
        DB::beginTransaction();
      
        try {
        $redirect = false;
         if (isset($request->email)){
            $User = User::where('email', $request->email)->get()->first();
            if(isset($User)) {
                $idUtente = $User->id;
                   $redirect = false;                 
             }else{
                $Users = new User();
                $Users->email = $request->email;
                $Users->name = $request->name_and_surname;
                $Users->password = bcrypt($request->name_and_surname);
                $Users->created_id = $request->idUser;
                $Users->online = true;
                $Users->confirmed = true;
                $Users->language_id=1;
                $Users->save();

                $UsersDatas = new UsersDatas();
                $UsersDatas->user_id = $Users->id;
                $UsersDatas->name = $request->name;
                $UsersDatas->surname = $request->surname;
                $UsersDatas->id_google = $request->id_google;
                $UsersDatas->check_google= true;
                $redirect = Setting::where('code', 'Force Redirect On Login')->first()->value;
                $UsersDatas->created_id = $request->idUser;
                $UsersDatas->language_id = 1;
                $UsersDatas->save();

                $idUtente = $Users->id;
             }
         }
       
         $userondb = User::where('id', $idUtente)->get()->first();
         if(isset($userondb)){
             if ($userondb->online == 'true' && $userondb->confirmed == 'true'){
                 $tokenResult = $userondb->createToken('Personal Access Token');
                 $token = $tokenResult->token;
                 $roleUser = RoleUser::where('user_id', $userondb->id)->get()->first();
                 if(isset($roleUser)){
                     $roleDescription = Role::where('id', $roleUser->role_id)->first()->name;
                 }else{
                     $roleDescription = "";
                 }
                 $usersDatas = UsersDatas::where('user_id', $userondb->id)->get()->first();
                 if(isset($usersDatas)){
                     $userDescription = $usersDatas->business_name;
                 }
                 
                 if ($request->remember_me) {
                     $token->expires_at = Carbon::now()->addWeeks(1);
                 }
                 $token->save();
             }
             
             $cartUser = Cart::where('user_id', $userondb->id)->get()->first();
             $cartid = '';
             if(isset($cartUser)){
                 $cartUser->save();
                 $cartid = $cartUser->id;
             }else{
                 $cartNewUser = new Cart();
                 $cartNewUser->user_id = $userondb->id;
                 $cartNewUser->session_token = 'Bearer ' . $tokenResult->accessToken;
                 $cartNewUser->save();
                 $cartid = $cartNewUser->id;       
             }
                $token =  $tokenResult->accessToken; 
                $expires_at = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString();
                $username = $userondb->name;
                $role = $roleDescription;
                $idLanguage = $userondb->language_id;
                $id = $userondb->id;
                $cart_id = $cartid;
         }else{
            $token =  ''; 
            $expires_at = ''; 
            $username = ''; 
            $role = ''; 
            $idLanguage = ''; 
            $id = ''; 
            $cart_id = ''; 
         }
         
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
            return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_at' => $expires_at,
            'username' => $username,
            'role' => $role,
            'idLanguage' => $idLanguage,
            'id' => $id,
            'cart_id' => $cart_id,
            'user_description' => '',
            'redirect' => $redirect,
            'code' => '200'
        ]);
    }


    public static function insertLoginForFacebook(Request $request)
    {
       
        DB::beginTransaction();
      
        try {
        $redirect = false;
         if (isset($request->email)){
            $User = User::where('email', $request->email)->get()->first();
            if(isset($User)) {
                $idUtente = $User->id;
                   $redirect = false;
             }else{
                $Users = new User();
                $Users->email = $request->email;
                $Users->name = $request->name;
                $Users->password = bcrypt($request->name);
                $Users->created_id = $request->idUser;
                $Users->online = true;
                $Users->confirmed = true;
                $Users->language_id=1;
                $Users->save();

                $UsersDatas = new UsersDatas();
                $UsersDatas->user_id = $Users->id;
                $UsersDatas->name = $request->first_name;
                $UsersDatas->surname = $request->last_name;
                $UsersDatas->id_facebook = $request->id_facebook;
                $UsersDatas->check_facebook = true;
                $redirect = Setting::where('code', 'Force Redirect On Login')->first()->value;
                $UsersDatas->created_id = $request->idUser;
                $UsersDatas->language_id = 1;
                $UsersDatas->save();
                $idUtente = $Users->id;
               
             }
         }
         
         $userondb = User::where('id', $idUtente)->get()->first();
         if(isset($userondb)){
             if ($userondb->online == 'true' && $userondb->confirmed == 'true'){
                 $tokenResult = $userondb->createToken('Personal Access Token');
                 $token = $tokenResult->token;
                 $roleUser = RoleUser::where('user_id', $userondb->id)->get()->first();
                 if(isset($roleUser)){
                     $roleDescription = Role::where('id', $roleUser->role_id)->first()->name;
                 }else{
                     $roleDescription = "";
                 }
                 $usersDatas = UsersDatas::where('user_id', $userondb->id)->get()->first();
                 if(isset($usersDatas)){
                     $userDescription = $usersDatas->business_name;
                 }
                 
                 if ($request->remember_me) {
                     $token->expires_at = Carbon::now()->addWeeks(1);
                 }
                 $token->save();
             }
             
             $cartUser = Cart::where('user_id', $userondb->id)->get()->first();
             $cartid = '';
             if(isset($cartUser)){
                 $cartUser->save();
                 $cartid = $cartUser->id;
             }else{
                 $cartNewUser = new Cart();
                 $cartNewUser->user_id = $userondb->id;
                 $cartNewUser->session_token = 'Bearer ' . $tokenResult->accessToken;
                 $cartNewUser->save();
                 $cartid = $cartNewUser->id;       
             }

                $token =  $tokenResult->accessToken; 
                $expires_at = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString();
                $username = $userondb->name;
                $role = $roleDescription;
                $idLanguage = $userondb->language_id;
                $id = $userondb->id;
                $cart_id = $cartid;
              
            

           /*  return response()->json([
                 'access_token' => $tokenResult->accessToken,
                 'token_type' => 'Bearer',
                 'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
                 'username' => $userondb->name,
                 'role' => $roleDescription,
                 'idLanguage' => $userondb->language_id,
                 'id' => $userondb->id,
                 'cart_id' => $cartid,
                 'user_description' => $userDescription,
                 'redirect' => $redirect,
                 'code' => '200'
             ]);*/
         }else{
            $token =  ''; 
            $expires_at = ''; 
            $username = ''; 
            $role = ''; 
            $idLanguage = ''; 
            $id = ''; 
            $cart_id = ''; 
            /* return response()->json([
                 'access_token' => '',
                 'token_type' => 'Bearer',
                 'expires_at' => '',
                 'username' => '',
                 'role' => '',
                 'idLanguage' => '',
                 'id' => '',
                 'cart_id' => '',
                 'user_description' => '',
                 'redirect' => $redirect,
                 'code' => '200'
             ]);*/
         }
         
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
            return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_at' => $expires_at,
            'username' => $username,
            'role' => $role,
            'idLanguage' => $idLanguage,
            'id' => $id,
            'cart_id' => $cart_id,
            'user_description' => '',
            'redirect' => $redirect,
            'code' => '200'
        ]);
    }

    

  /*  public static function insertLoginForGoogle(Request $request)
    {

        DB::beginTransaction();
        try {
            $token =  ''; 
            $expires_at = ''; 
            $username = ''; 
            $role = ''; 
            $idLanguage = ''; 
            $id = ''; 
            $cart_id = ''; 
            $code = '404';
            $messageReturn = 'Utente non trovato';
            $accessToken = ''; 
            $expires_at = '';
            $roleDescription = "";
            //$idUtente = '';

            if (isset($request->email)){
                $User = User::where('email', $request->email)->get()->first();            
                if(isset($User)){
                    $userondb = User::where('id', $User->id)->get()->first();
                    if(isset($userondb)){
                        if ($userondb->online == 'true' && $userondb->confirmed == 'true'){
                            $tokenResult = $userondb->createToken('Personal Access Token');
                            $token = $tokenResult->token;
                            $accessToken = $tokenResult->accessToken;

                            $roleUser = RoleUser::where('user_id', $userondb->id)->get()->first();
                            if(isset($roleUser)){
                                $roleDescription = Role::where('id', $roleUser->role_id)->first()->name;
                            }

                            $usersDatas = UsersDatas::where('user_id', $userondb->id)->get()->first();
                            if(isset($usersDatas)){
                                $userDescription = $usersDatas->business_name;
                            }
                            
                            if ($request->remember_me) {
                                $token->expires_at = Carbon::now()->addWeeks(1);
                            }
                            $code = '200';

                            $token->save();

                            $expires_at = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString();

                            LogsBL::insert($User->id, $request->ip(), 'LOGIN', 'Login Google Effettuato');
                        }else{
                            if ($userondb->online == false && $userondb->confirmed == true){
                                $Message = Message::where('code', 'message_user_not_enabled')->first();  
                                $MessageLanguages = MessageLanguage::where('message_id', $Message->id)->first();  
                                $messageReturn = $MessageLanguages->content;      
                            }else{  
                                if ($userondb->online == true && $userondb->confirmed == false){
                                    $Message = Message::where('code', 'message_unconfirmed_account')->first();  
                                    $MessageLanguages = MessageLanguage::where('message_id', $Message->id)->first();
                                    $messageReturn = $MessageLanguages->content;       
                                }else{
                                    if ($userondb->online == false && $userondb->confirmed == false) {
                                        $Message = Message::where('code', 'message_unauthorized_user')->first();  
                                        $MessageLanguages = MessageLanguage::where('message_id', $Message->id)->first();
                                        $messageReturn = $MessageLanguages->content;  
                                    }
                                }  
                            }
                        }
                        
                        $cartUser = Cart::where('user_id', $userondb->id)->get()->first();
                        $cartid = '';
                        if(isset($cartUser)){
                            $cartUser->save();
                            $cartid = $cartUser->id;
                        }else{
                            $cartNewUser = new Cart();
                            $cartNewUser->user_id = $userondb->id;
                            $cartNewUser->session_token = 'Bearer ' . $accessToken;
                            $cartNewUser->save();
                            $cartid = $cartNewUser->id;       
                        }
                        $token =  $accessToken;
                        $username = $userondb->name;
                        $role = $roleDescription;
                        $idLanguage = $userondb->language_id;
                        $id = $userondb->id;
                        $cart_id = $cartid;
                    }else{
                        $token =  ''; 
                        $expires_at = ''; 
                        $username = ''; 
                        $role = ''; 
                        $idLanguage = ''; 
                        $id = ''; 
                        $cart_id = ''; 
                        $code = '404';
                        $messageReturn = 'Utente non trovato'; 
                    }
                } 
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_at' => $expires_at,
            'username' => $username,
            'role' => $role,
            'idLanguage' => $idLanguage,
            'id' => $id,
            'cart_id' => $cart_id,
            'user_description' => '',
            'redirect' => false,
            'code' => $code,
            'message' => $messageReturn
        ]);
    }




    public static function insertLoginForFacebook(Request $request)
    {
     
        DB::beginTransaction();
        try {
            $token =  ''; 
            $expires_at = ''; 
            $username = ''; 
            $role = ''; 
            $idLanguage = ''; 
            $id = ''; 
            $cart_id = ''; 
            $code = '404';
            $messageReturn = 'Utente non trovato';
            $accessToken = ''; 
            $expires_at = '';
            $roleDescription = "";
            //$idUtente = '';

            if (isset($request->email)){
                $User = User::where('email', $request->email)->get()->first();            
                if(isset($User)){
                    $userondb = User::where('id', $User->id)->get()->first();
                    if(isset($userondb)){
                        if ($userondb->online == 'true' && $userondb->confirmed == 'true'){
                            $tokenResult = $userondb->createToken('Personal Access Token');
                            $token = $tokenResult->token;
                            $accessToken = $tokenResult->accessToken;

                            $roleUser = RoleUser::where('user_id', $userondb->id)->get()->first();
                            if(isset($roleUser)){
                                $roleDescription = Role::where('id', $roleUser->role_id)->first()->name;
                            }

                            $usersDatas = UsersDatas::where('user_id', $userondb->id)->get()->first();
                            if(isset($usersDatas)){
                                $userDescription = $usersDatas->business_name;
                            }
                            
                            if ($request->remember_me) {
                                $token->expires_at = Carbon::now()->addWeeks(1);
                            }
                            $code = '200';

                            $token->save();

                            $expires_at = Carbon::parse($tokenResult->token->expires_at)->toDateTimeString();

                            LogsBL::insert($User->id, $request->ip(), 'LOGIN', 'Login Facebook Effettuato');
                        }else{
                            if ($userondb->online == false && $userondb->confirmed == true){
                                $Message = Message::where('code', 'message_user_not_enabled')->first();  
                                $MessageLanguages = MessageLanguage::where('message_id', $Message->id)->first();  
                                $messageReturn = $MessageLanguages->content;      
                            }else{  
                                if ($userondb->online == true && $userondb->confirmed == false){
                                    $Message = Message::where('code', 'message_unconfirmed_account')->first();  
                                    $MessageLanguages = MessageLanguage::where('message_id', $Message->id)->first();
                                    $messageReturn = $MessageLanguages->content;       
                                }else{
                                    if ($userondb->online == false && $userondb->confirmed == false) {
                                        $Message = Message::where('code', 'message_unauthorized_user')->first();  
                                        $MessageLanguages = MessageLanguage::where('message_id', $Message->id)->first();
                                        $messageReturn = $MessageLanguages->content;  
                                    }
                                }  
                            }
                        }
                        
                        $cartUser = Cart::where('user_id', $userondb->id)->get()->first();
                        $cartid = '';
                        if(isset($cartUser)){
                            $cartUser->save();
                            $cartid = $cartUser->id;
                        }else{
                            $cartNewUser = new Cart();
                            $cartNewUser->user_id = $userondb->id;
                            $cartNewUser->session_token = 'Bearer ' . $accessToken;
                            $cartNewUser->save();
                            $cartid = $cartNewUser->id;       
                        }
                        $token =  $accessToken;
                        $username = $userondb->name;
                        $role = $roleDescription;
                        $idLanguage = $userondb->language_id;
                        $id = $userondb->id;
                        $cart_id = $cartid;
                    }else{
                        $token =  ''; 
                        $expires_at = ''; 
                        $username = ''; 
                        $role = ''; 
                        $idLanguage = ''; 
                        $id = ''; 
                        $cart_id = ''; 
                        $code = '404';
                        $messageReturn = 'Utente non trovato'; 
                    }
                } 
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return response()->json([
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_at' => $expires_at,
            'username' => $username,
            'role' => $role,
            'idLanguage' => $idLanguage,
            'id' => $id,
            'cart_id' => $cart_id,
            'user_description' => '',
            'redirect' => false,
            'code' => $code,
            'message' => $messageReturn
        ]);
    }*/






    
    public static function insertSalesUser(Request $request, int $idLanguage)
    {

        DB::beginTransaction();
        try {
          $UsersDatas = new UsersDatas();
          $UsersDatas->user_id = $request->user_id;
          $UsersDatas->id_list = $request->id_list;
          $UsersDatas->carrier_id = $request->carrier_id;
          $UsersDatas->id_payment = $request->id_payment;
          $UsersDatas->id_carriage = $request->carriage_id;
          $UsersDatas->vat_type_id = $request->vat_type_id;
          $UsersDatas->vat_exempt = $request->vat_exempt;
          $UsersDatas->payment_fixed = $request->payment_fixed;
          $UsersDatas->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $UsersDatas->user_id;
    }


    /**
     * Get by id
     *
     * @param int $id
     * @return User
     */
    public static function getByIdResult(int $id)
    {
        $user = User::where('id', $id)->first();
        $userdata = UsersDatas::where('user_id', $id)->first();
        $Subscriber = Subscriber::where('user_id', $id)->first();

        if (isset($userdata)) {
            $iduser = User::where('id', $userdata->user_id)->first();

            $dtoUser = new DtoUser();
            $dtoUser->id = $id;
            $dtoUser->username = $user->name;
            $dtoUser->email = $user->email;
            $dtoUser->password = $user->password;
            $dtoUser->language_id = $user->language_id;
            $dtoUser->business_name = $userdata->business_name;
            $dtoUser->company = $userdata->company;
            $dtoUser->name = $userdata->name;
            $dtoUser->surname = $userdata->surname;
            $dtoUser->vat_number = $userdata->vat_number;
            $dtoUser->fiscal_code = $userdata->fiscal_code;
            $dtoUser->address = $userdata->address;
            $dtoUser->codepostal = $userdata->codepostal;
            $dtoUser->country = $userdata->country;
            $dtoUser->province = $userdata->province;
            $dtoUser->website = $userdata->website;
            $dtoUser->telephone_number = $userdata->telephone_number;
            $dtoUser->fax_number = $userdata->fax_number;
            $dtoUser->mobile_number = $userdata->mobile_number;
            $dtoUser->pec = $userdata->pec;
            $dtoUser->carrier_id = $userdata->carrier_id;
            $dtoUser->id_payments = $userdata->id_payments;
            $dtoUser->id_sdi = $userdata->id_sdi;
            $dtoUser->create_quote = $userdata->create_quote;
            $dtoUser->privates = $userdata->privates;
            $dtoUser->final = $userdata->final;
            $dtoUser->port = $userdata->port;

            if (isset($userdata->vat_type_id)) {
                $dtoUser->vat_type_id = $userdata->vat_type_id;
                $dtoUser->descriptionVatType = VatType::where('id', $userdata->vat_type_id)->first()->rate;
            }
             if (isset($userdata->id_dealer)) {
                $dtoUser->id_dealer = $userdata->id_dealer;
                $dtoUser->descriptionDealer = GrDealer::where('id', $userdata->id_dealer)->first()->business_name;
            }


            if (isset($userdata->carrier_id)) {
                $dtoUser->carrier_id = $userdata->carrier_id;
                $dtoUser->descriptionCarrier = Carrier::where('id', $userdata->carrier_id)->first()->code;
            }
              if (isset($userdata->id_payments)) {
                $dtoUser->id_payments = $userdata->id_payments;
                $dtoUser->descriptionPayments = PaymentGr::where('id', $userdata->id_payments)->first()->description;
            }

           // $dtoUser->id = $Subscriber->user_id;
           if (isset($Subscriber->subscriber)) {
                $dtoUser->subscriber = $Subscriber->subscriber;
            }
            if (isset($Subscriber->basic)) {
                $dtoUser->basic = $Subscriber->basic;
            }else{
                $dtoUser->basic = false;
            }
            if (isset($Subscriber->plus)) {
                    $dtoUser->plus = $Subscriber->plus;
            }else{
                    $dtoUser->plus = false;
            } 

            if (isset($Subscriber->subscription_date)) {
                $dtoUser->subscription_date = $Subscriber->subscription_date;
            }
            if (isset($Subscriber->expiration_date)) {
                $dtoUser->expiration_date = $Subscriber->expiration_date;
            }
            return  $dtoUser;
        } else {
            return '';
        }
    }


    /**
     * Get all
     * 
     * @return DtoUser
     */
    public static function getUserData($sUserId)
    {

        $user = User::where('id', $sUserId)->first();
        $userdata = UsersDatas::where('user_id', $sUserId)->first();
        $useraddress = UserAddress::where('user_id', $sUserId)->first();
        $nation = LanguageNation::where('nation_id',$userdata->nation_id)->first();
        $province = LanguageProvince::where('province_id',$userdata->province_id)->first();
        $region = Region::where('id',$userdata->region_id)->first();

        if (isset($userdata)) {
            $iduser = User::where('id', $userdata->user_id)->first();
 
            $dtoUser = new DtoUser();
            $dtoUser->id = $sUserId;
            $dtoUser->name = $userdata->name;
            $dtoUser->surname = $userdata->surname;
            $dtoUser->business_name = $userdata->business_name;
            $dtoUser->check_salesregistration = $userdata->check_salesregistration;
            $dtoUser->check_rehearsal_room = $userdata->check_rehearsal_room;
            $dtoUser->email = $user->email;
            $dtoUser->vat_number = $userdata->vat_number;
            $dtoUser->fiscal_code = $userdata->fiscal_code;
            $dtoUser->address = $userdata->address;
            $dtoUser->postal_code = $userdata->postal_code;
            $dtoUser->address = $userdata->address;
            $dtoUser->province = $userdata->province;
            $dtoUser->telephone_number = $userdata->telephone_number;
            $dtoUser->website = $userdata->website;
            $dtoUser->description = $userdata->description;
            $dtoUser->accepted_payments = $userdata->accepted_payments;
            $dtoUser->terms_of_sale = $userdata->terms_of_sale;
            $dtoUser->country = $userdata->country;
            $dtoUser->province = $userdata->province;
            $dtoUser->mobile_number = $userdata->mobile_number;
            $dtoUser->pec = $userdata->pec;
            $dtoUser->description = $userdata->description;
            $dtoUser->accepted_payments = $userdata->accepted_payments;
            $dtoUser->terms_of_sale = $userdata->terms_of_sale;
            $dtoUser->shop_time = $userdata->shop_time;
            $dtoUser->maps = $userdata->maps;
            $dtoUser->img = $userdata->img;
            $dtoUser->monday = $userdata->monday;
            $dtoUser->tuesday = $userdata->tuesday;
            $dtoUser->wednesday = $userdata->wednesday;
            $dtoUser->thursday = $userdata->thursday;
            $dtoUser->friday = $userdata->friday;
            $dtoUser->saturday = $userdata->saturday;
            $dtoUser->sunday = $userdata->sunday;

            /*if (isset($userdata->nation_id)) {
                $dtoUser->nation_id = $userdata->nation_id;
                $dtoUser->Nation_id = $nation->description;
               } */
            /*if (isset($userdata->province_id)) {
             //   $dtoUser->province_id = $userdata->province_id;
              //  $dtoUser->Province_id = $province->description;
               } */
                /* if (isset($userdata->region_id)) {
                $dtoUser->region_id = $userdata->region_id;
                $dtoUser->Region_id = $region->description;
               }   */

            if (isset($province)) {  
            $dtoUser->province_id = $province->province_id;
            $dtoUser->Province_id = $province->description;
            }else{

            $dtoUser->province_id = ''; 
            }     

            if (isset($nation)) {  
            $dtoUser->nation_id = $nation->nation_id;
            $dtoUser->Nation_id = $nation->description;
            }else{
            $dtoUser->nation_id = ''; 
            }   
        
            if (isset($region)) {  
            $dtoUser->region_id = $region->id;
            $dtoUser->Region_id = $region->description;
            }else{
            $dtoUser->region_id = ''; 
            }
          
       
            /* PHOTO AGGIUNTIVE */
            $userPhotos = UserPhoto::where('user_id', $sUserId)->get();
            if (isset($userPhotos)) {
                foreach ($userPhotos as $userPhoto) {
                    $dtoUserPhotos = new DtoUserPhoto();
                    $dtoUserPhotos->id = $userPhoto->id;
                    $dtoUserPhotos->img = $userPhoto->img;
                    $dtoUser->imgAgg->push($dtoUserPhotos);
                }
            }
            /* END PHOTO AGGIUNTIVE */

            /* MAPPE AGGIUNTIVE */
            $useraddress = UserAddress::where('user_id', $sUserId)->get();
            if (isset($useraddress)) {
                foreach ($useraddress as $userAddress) {
                    $DtoAddress = new DtoAddress();
                    $DtoAddress->id_map = $userAddress->id;
                    $DtoAddress->user_id = $userAddress->user_id;
                    $DtoAddress->maps_map = $userAddress->maps;
                    $DtoAddress->address_maps = $userAddress->address;
                    $DtoAddress->codepostal_maps = $userAddress->codepostal;
                    $DtoAddress->country_maps = $userAddress->country;
                    $DtoAddress->province_maps = $userAddress->province;   
                    $dtoUser->AggMaps->push($DtoAddress);
                }
            }
            /* END MAPPE AGGIUNTIVE */

            return  $dtoUser;
        } else {
            return '';
        }
    }

    /**
     * Get by username
     *
     * @param string $username
     * @return User
     */
    public static function getByUsername(string $username)
    {
        return User::where('name', $username)->first();
    }


    /**
     * Get Content for link User_Profile_Page
     * 
     * @return DtoUser
     */
    public static function getHtml($url){
        $content = UsersDatas::where('link', $url)->first();
        if (isset($content)) {
            $roleUser = RoleUser::where('user_id', $content->user_id)->get()->first();
            if(isset($roleUser)){
                $role = Role::where('id', $roleUser->role_id)->get()->first();
                if(isset($role)){
                    if($role->name == 'sala' || $role->name == 'scuolamusica'){
                        $idContent = Content::where('code', 'DetailShopUser')->first();
                    }else{
                        $idContent = Content::where('code', 'User_Profile_Page')->first();
                    }
                }else{
                    $idContent = Content::where('code', 'User_Profile_Page')->first();
                }
            }else{
                $idContent = Content::where('code', 'User_Profile_Page')->first();
            }
            
            $languageContent = ContentLanguage::where('content_id', $idContent->id)->first();
            
            $contentHtml = ContentLanguageBL::getByCodeRender($idContent->id, $languageContent->language_id, $url);
            $dtoPageUserRender = new DtoUser();
            $dtoPageUserRender->html = $contentHtml;

            $dtoPageUserRender->description = $content->name . ' ' . $content->surname;
            $dtoPageUserRender->meta_tag_description = $content->name . ' ' . $content->surname;
            $dtoPageUserRender->meta_tag_keyword = $content->name . ' ' . $content->surname;
            $dtoPageUserRender->meta_tag_title = $content->name . ' ' . $content->surname;
            
            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
                $protocol = "https://";
            } else {
                $protocol = "http://";
            }

            if (strpos($content->img, "https://") !== false) {
                $dtoPageUserRender->url_cover_image = $content->img;
            }else{
                $dtoPageUserRender->url_cover_image = $protocol . $_SERVER["HTTP_HOST"] . $content->img;
            }

            $dtoPageUserRender->name = $content->name;
            $dtoPageUserRender->surname = $content->surname;
            $dtoPageUserRender->address = $content->address;
            $dtoPageUserRender->country = $content->country;
            $dtoPageUserRender->province = $content->province;
            $dtoPageUserRender->business_name = $content->business_name;

            

            return $dtoPageUserRender;
        } else {
            return '';
        }
    }

   

    /**
     * Get all
     * 
     * @return DtoUser
     */
    public static function getAllUser($idLanguage)
    {
        $result = collect();
       
         /*foreach (DB::select(
          "select
            u.id,
            u.name,
            u.email,
            u.language_id
            from users u
            left join users_datas ud on u.id = ud.user_id
            left join correlation_peopleuser up on u.id = up.id_user 
            where language_id = $idLanguage and u.id not in
            (select id_user from correlation_peopleuser where id_user=u.id) limit 100"
            ) as $user) {*/
                foreach (DB::select(
                "select
                u.id,
                u.name,
                u.email,
                u.language_id
                from users u
                left join users_datas ud on u.id = ud.user_id
                left join correlation_peopleuser up on u.id = up.id_user 
                where u.language_id = 1 and u.id not in
                (select id_user from correlation_peopleuser where id_user=u.id)
                order by ud.business_name limit 20") as $user) {   


       // foreach (User::where('language_id', $idLanguage)->take(50)->get() as $user) {
            //foreach (User::all() as $user) {

            $dtoUser = new DtoUser();
            $UserId = UsersDatas::where('user_id', $user->id)->first();
           // $VatTypeCode = VatType::where('id', $UserId->vat_type_id)->first();
           // $PostaCode = PostalCode::where('id', $UserId->postal_code)->first();
            $Subscriber = Subscriber::where('user_id', $user->id)->first();

            $dtoUser->id = $user->id;

            $dtoUser->username = $user->name;
            $dtoUser->email = $user->email;
            //$dtoUser->password = $user->password;
            $dtoUser->idLanguage = $user->language_id;

            if (isset($UserId->company)) {
                $dtoUser->company = $UserId->company;
            }
            if (isset($UserId->business_name)) {
                $dtoUser->business_name = $UserId->business_name;
            } else {
                $dtoUser->business_name = '';
            }

            if (isset($UserId->name)) {
                $dtoUser->name = $UserId->name;
            } else {
                $dtoUser->name = '';
            }

            if (isset($UserId->surname)) {
                $dtoUser->surname = $UserId->surname;
            } else {
                $dtoUser->surname = '';
            }

            if (isset($UserId->vat_number)) {
                $dtoUser->vat_number = $UserId->vat_number;
            } else {
                $dtoUser->vat_number = '';
            }

            if (isset($UserId->fiscal_code)) {
                $dtoUser->fiscal_code = $UserId->fiscal_code;
            } else {
                $dtoUser->fiscal_code = '';
            }
            if (isset($UserId->address)) {
                $dtoUser->address = $UserId->address;
            } else {
                $dtoUser->address = '';
            }

            if (isset($UserId->codepostal)) {
                $dtoUser->codepostal = $UserId->codepostal;
            } else {
                $dtoUser->codepostal = '';
            }

            if (isset($UserId->telephone_number)) {
                $dtoUser->telephone_number = $UserId->telephone_number;
            } else {
                $dtoUser->telephone_number = '';
            }
            if (isset($UserId->fax_number)) {
                $dtoUser->fax_number = $UserId->fax_number;
            } else {
                $dtoUser->fax_number = '';
            }

            if (isset($UserId->website)) {
                $dtoUser->website = $UserId->website;
            } else {
                $dtoUser->website = '';
            }

            if (isset($UserId->country)) {
                $dtoUser->country = $UserId->country;
            } else {
                $dtoUser->country = '';
            }
            if (isset($UserId->province)) {
                $dtoUser->province = $UserId->province;
            } else {
                $dtoUser->province = '';
            }

            if (isset($UserId->mobile_number)) {
                $dtoUser->mobile_number = $UserId->mobile_number;
            } else {
                $dtoUser->mobile_number = '';
            }
            if (isset($UserId->pec)) {
                $dtoUser->pec = $UserId->pec;
            } else {
                $dtoUser->pec = '';
            }
            if (isset($VatTypeCode->rate)) {
                $dtoUser->vat_type_id = $VatTypeCode->rate;
            } else {
                $dtoUser->vat_type_id = '';
            }
            if (isset($Subscriber->subscriber)) {
                    $dtoUser->subscriber = $Subscriber->subscriber;
            } else {
                    $dtoUser->subscriber = '';
            }
            if (isset($Subscriber->basic)) {
                    $dtoUser->basic = $Subscriber->basic;
            } else {
                    $dtoUser->basic = '';
            }
             if (isset($Subscriber->plus)) {
                    $dtoUser->plus = $Subscriber->plus;
            }else {
                    $dtoUser->plus = '';
            } 

           /* $subscriberSi = "Si";
            $subscriberNo = "No";


            if ($Subscriber->subscriber == "true") {
               $dtoUser->subscriber = $subscriberSi;
            } else {
                $dtoUser->subscriber = $subscriberNo;
            } */

            if (isset($Subscriber->expiration_date)) {
                $dtoUser->expiration_date = $Subscriber->expiration_date;
            } else {
                $dtoUser->expiration_date = '';
            }
            if (isset($Subscriber->subscription_date)) {
                $dtoUser->subscription_date = $Subscriber->subscription_date;
            } else {
                $dtoUser->subscription_date = '';
            }

            $result->push($dtoUser);
        }
        return $result;
    }

/**
     * availableCheck
     *
     * @param int idFunctionality
     * @param int idUser
     *
     * @return DtoUser
     */

public static function availableCheck(int $id)
    {
        if (!is_null($id)) {
        $User= User::where('id', $id)->first();
        if (isset($User)){
        $User->confirmed ='true';
        $User->save();
        }
        }      
        return  $id;
    }


    /**
     * Get all data
     *
     * @param int idFunctionality
     * @param int idUser
     *
     * @return DtoUser
     */
    public static function getAll(int $idFunctionality, int $idUser, int $idLanguage)
    {
        $configurations = FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser);

        if (is_null($configurations)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        $result = collect();
        foreach (User::all() as $user) {

            $dtoUser = new DtoUser();
            $UserId = UsersDatas::where('user_id', $user->id)->first();
            $VatTypeCode = VatType::where('id', $user->vat_type_id)->first();
            $PostaCode = PostalCode::where('id', $user->postal_code)->first();
           
            $dtoUser->id = $user->id;
            $dtoUser->username = $user->name;
            $dtoUser->email = $user->email;
            $dtoUser->idLanguage = $user->language_id;
            $dtoUser->languageDescription = LanguageBL::get($user->language_id)->description;

            if (isset($UserId->user_id)) {
                $dtoUser->user_id = $UserId->user_id;
            }
            if (isset($UserId->company)) {
                $dtoUser->company = $UserId->company;
            }
            if (isset($user->confirmed)) {
                $dtoUser->confirmed = $user->confirmed;
            }   

            if (isset($user->send_newsletter)) {
                $dtoUser->send_newsletter = $user->send_newsletter;
            }    
            if (isset($user->online)) {
                $dtoUser->online = $user->online;
            }       
            if (isset($UserId->business_name)) {
                $dtoUser->business_name = $UserId->business_name;
            }
            if (isset($UserId->name)) {
                $dtoUser->name = $UserId->name;
            }
            if (isset($UserId->surname)) {
                $dtoUser->surname = $UserId->surname;
            }
            if (isset($UserId->vat_number)) {
                $dtoUser->vat_number = $UserId->vat_number;
            }
            if (isset($UserId->fiscal_code)) {
                $dtoUser->fiscal_code = $UserId->fiscal_code;
            }
            if (isset($UserId->postal_code)) {
                $dtoUser->postal_code = $UserId->postal_code;
            }
            if (isset($UserId->address)) {
                $dtoUser->address = $UserId->address;
            }
           /* if (isset($PostaCode->code)) {
                $dtoUser->postal_code = $PostaCode->code;
            }*/

            if (isset($UserId->telephone_number)) {
                $dtoUser->telephone_number = $UserId->telephone_number;
            }
            if (isset($UserId->fax_number)) {
                $dtoUser->fax_number = $UserId->fax_number;
            }

            if (isset($UserId->website)) {
                $dtoUser->website = $UserId->website;
            }

            if (isset($UserId->country)) {
                $dtoUser->country = $UserId->country;
            }
           /* if (isset($UserId->province)) {
                $dtoUser->province = $UserId->province;
            }*/
  
            if (isset($UserId->province_id)) {
                $dtoUser->province = $UserId->province_id;
                $dtoUser->provinceDescription = Province::where('id', $UserId->province_id)->first()->abbreviation;
             }else{
                 $dtoUser->provinceDescription = '';
            }

            if (isset($UserId->cities_id)) {
               $dtoUser->country = $UserId->cities_id;
               $dtoUser->countryDescription = Cities::where('id', $UserId->cities_id)->first()->description;
            }else{
                 $dtoUser->countryDescription = '';
            }
    
            if (isset($UserId->mobile_number)) {
                $dtoUser->mobile_number = $UserId->mobile_number;
            }
            if (isset($UserId->pec)) {
                $dtoUser->pec = $UserId->pec;
            }
            if (isset($UserId->description)) {
                $dtoUser->description = $UserId->description;
            }
            if (isset($UserId->accepted_payments)) {
                $dtoUser->accepted_payments = $UserId->accepted_payments;
            }

            if (isset($UserId->terms_of_sale)) {
                $dtoUser->terms_of_sale = $UserId->terms_of_sale;
            }

            if (isset($VatTypeCode->rate)) {
                $dtoUser->vat_type_id = $VatTypeCode->rate;
            }
            if (isset($UserId->created_at)) {
                $dtoUser->created_at = date_format($UserId->created_at, 'd/m/Y');
            }
            if (isset($UserId->address_ip)) {
                $dtoUser->address_ip = $UserId->address_ip;
            }

            $Log =Logs::where('user_id', $user->id)->get();
            if (isset($Log)) {
                foreach ($Log as $Logs) {
                    $dtoUsers = new dtoUser();
                    if (isset($Logs->created_at)) {
                        $dtoUsers->date_login = date_format($Logs->created_at, 'd/m/Y');
                    }
                    if (isset($Logs->ip)) {
                        $dtoUsers->ip = $Logs->ip;
                    }
                    if (isset($Logs->user_id)) {
                        $dtoUsers->username_log = User::where('id', $Logs->user_id)->first()->name;
                    }
                        $dtoUser->ResultLogs->push($dtoUsers); 
                }
            }

                foreach (PeopleUser::where('user_id', $user->id)->get() as $PeopleUser) {
                    if (isset($PeopleUser->people_id)) {
                        $CorrelationPeopleUser = CorrelationPeopleUser::where('id_people', $PeopleUser->people_id)->get();
                        foreach ($CorrelationPeopleUser as $CorrelationPeopleUsers) {  
                            if (isset($CorrelationPeopleUsers->id_people)) {
                                $Log =Logs::where('people_id', $CorrelationPeopleUsers->id_people)->get();
                                foreach ($Log as $Logs) {  
                                    $dtoUsers = new dtoUser();
                                    if (isset($Logs->created_at)) {
                                        $dtoUsers->date_login = date_format($Logs->created_at, 'd/m/Y');
                                    }
                                    if (isset($Logs->ip)) {
                                        $dtoUsers->ip = $Logs->ip;
                                    }
                                    if (isset($Logs->user_id)) {
                                        $dtoUsers->username_log = User::where('id', $Logs->user_id)->first()->name;
                                    }
                                        $dtoUser->ResultLogs->push($dtoUsers);    
                                }   
                            }                      
                        }
                    }                        
                } 
                $result->push($dtoUser);
        }
        return $result;  
    }


    public static function getAllDetailTimeLine(int $id)
    {
        $result = collect();
        $MonthNew = "";
        $MeseOld = "";
     
        foreach (DB::select(
                'select created_atall   
                from  (
                SELECT to_char(logs.created_at, \'DD Month YYYY\') as created_atall, logs.created_at
                FROM logs
                WHERE logs.user_id = :id
                order by logs.created_at desc
                ) as C',
                ['id' => $id]
        ) as $Logs) {
            $MonthNew = rtrim(ltrim($Logs->created_atall));

            if ($MonthNew != $MeseOld || $MeseOld == "") {
                $MeseOld = $MonthNew; 
                $DtoL = new DtoLogs();
                $DtoL->created_at = $Logs->created_atall;
            
                    foreach (DB::select(
                        'SELECT *
                        FROM logs
                        WHERE logs.user_id = :id
                        AND to_char(logs.created_at, \'DD Month YYYY\') = \'' . $Logs->created_atall . '\'
                        ORDER BY logs.created_at DESC',
                        ['id' => $id]
                ) as $LogResult) {
                        $DtoR = new DtoLogsDetail(); 

                        $DtoR->user_id = $LogResult->user_id;
                        $DtoR->ip = $LogResult->ip;

                            if ($LogResult->action == 'COMMENT') {
                                $DtoR->description = 'Commento:' . '"' . $LogResult->description . '"' . '(' . UsersDatas::where('user_id', $LogResult->user_id)->first()->name . ' ' . UsersDatas::where('user_id', $LogResult->user_id)->first()->surname . ')';      
                            }else{
                                if (isset($LogResult->newsletter_id)) {
                                    $DtoR->description = $LogResult->description . ' "' .  Newsletter::where('id', $LogResult->newsletter_id)->first()->object . '"';                            
                                    } else {
                                        $DtoR->description = $LogResult->description;
                                }    
                            }

                        $DtoR->action = $LogResult->action;
                        $DtoR->people_id = $LogResult->people_id;
                        $DtoR->created_at = date('G:i', strtotime($LogResult->created_at));
                        $DtoL->ResultLogsAll->push($DtoR); 

                            if (isset($LogResult->order_id)) {
                                $SalesOrderId = SalesOrder::where('id', $LogResult->order_id)->first()->id;
                                $SalesOrderTotalAmount = SalesOrder::where('id', $LogResult->order_id)->first();
                                $TotalAmount= str_replace('.',',',substr($SalesOrderTotalAmount->total_amount, 0, -2));
                                $DtoR->idOrdine = 'N. ' .$SalesOrderId . ' di € ' .  $TotalAmount;                            
                                }else{
                                    $DtoR->idOrdine = ''; 
                                } 

                            if (isset($LogResult->people_id)) {
                                    $DtoR->name = 'Persona: (' . People::where('id', $LogResult->people_id)->first()->name;
                                    $DtoR->surname = People::where('id', $LogResult->people_id)->first()->surname . ')';
                                }else{
                                    $DtoR->name = ''; 
                                    $DtoR->surname = ''; 
                                }

                            /*if (isset($LogResult->newsletter_id)) {
                                $DtoR->description = $LogResult->description . '"' .  Newsletter::where('id', $LogResult->newsletter_id)->first()->object . '"';                            
                                } else {
                                    $DtoR->description = $LogResult->description;
                            } */


                    }
                       /* visualizza nome e cognome utente che fa l accesso: 
                        if (isset($LogResult->user_id)) {
                            $DtoR->name = 'Utente (' . UsersDatas::where('user_id', $LogResult->user_id)->first()->name;
                        }else {
                            $DtoR->name = ''; 
                        }
                        if (isset($LogResult->user_id)) {
                            $DtoR->surname = UsersDatas::where('user_id', $LogResult->user_id)->first()->surname . ')';
                        }else {
                            $DtoR->surname = ''; 
                        }*/

                      

                        $result->push($DtoL);
            }
        }
        
        return $result; 
    }


    /**
     * Get contact request
     * 
     * @return int
     */
    public static function getContactRequest()
    {
        return User::where('name', 'newsletter')->first()->id;
    }

    /**
     * Get number of user
     * 
     * @return int
     */
    public static function count()
    {
        return User::count();
    }

    /**
     * Get id by name
     * 
     * @param string name
     * 
     * @return int
     */
    public static function getIdByName(string $name)
    {
        $user = User::where('name', $name)->first();

        if (is_null($user)) {
            return 0;
        } else {
            return $user->id;
        }
    }

    #endregion GET

    #region PUT

    /**
     * Insert
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int id
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {

        static::_validateData($request, $idUser, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {

            $user = new User();
            $user->name = $request->username;
            $user->password = bcrypt($request->password);
            $user->email = $request->email;
            $user->language_id = $request->idLanguage;
            $user->online = $request->online;
            $user->send_newsletter = $request->send_newsletter;
            $user->confirmed = $request->confirmed;
            $user->save();

            $UsersData = new UsersDatas();
            $UsersData->user_id = $user->id;
            $UsersData->business_name = $request->business_name;
            $UsersData->name = $request->name;
            $UsersData->surname = $request->surname;
            $UsersData->vat_number = $request->vat_number;
            $UsersData->fiscal_code = $request->fiscal_code;
            $UsersData->language_id = $request->idLanguage;
            $UsersData->address = $request->address;
            $UsersData->postal_code = $request->postal_code;
            $UsersData->telephone_number = $request->telephone_number;
            $UsersData->fax_number = $request->fax_number;
            $UsersData->website = $request->website;
            $UsersData->country = $request->country;
            $UsersData->cities_id = $request->country;
            $UsersData->vat_type_id = $request->vat_type_id;
            $UsersData->province = $request->province;
            $UsersData->mobile_number = $request->mobile_number;
            $UsersData->pec = $request->pec;
            $UsersData->description = $request->description;
            $UsersData->accepted_payments = $request->accepted_payments;
            $UsersData->terms_of_sale = $request->terms_of_sale;
            $UsersData->company = $request->company;
            $UsersData->province_id = $request->province;
        
            
         /*   if (isset($request->business_name)) {
                $UsersData->business_name = $request->business_name;
            }else{
              $UsersData->business_name = '-';
            }  
            if (isset($request->name)) {
                $UsersData->name = $request->name;
            }else{
              $UsersData->name = '-';
            }  
            if (isset($request->surname)) {
                $UsersData->surname = $request->surname;
            }else{
             $UsersData->surname = '-';
            }  
            if (isset($request->vat_number)) {
                $UsersData->vat_number = $request->vat_number;
            }else{
              $UsersData->vat_number = '-';
            }  
           if (isset($request->fiscal_code)) {
                $UsersData->fiscal_code = $request->fiscal_code;
            }else{
              $UsersData->fiscal_code = '-';
            }  
            $UsersData->language_id = $request->idLanguage;

            if (isset($request->address)) {
                $UsersData->address = $request->address;
            }else{
              $UsersData->address = '-';
            }  
            if (isset($request->postal_code)) {
                $UsersData->postal_code = $request->postal_code;
            }else{
              $UsersData->postal_code = '-';
            }  
            if (isset($request->telephone_number)) {
                $UsersData->telephone_number = $request->telephone_number;
            }else{
              $UsersData->telephone_number = '-';
            }  
            if (isset($request->fax_number)) {
                $UsersData->fax_number = $request->fax_number;
            }else{
              $UsersData->fax_number = '-';
            }  
            if (isset($request->website)) {
                $UsersData->website = $request->website;
            }else{
              $UsersData->website = '-';
            } 
   
            $UsersData->vat_type_id = $request->vat_type_id;
              if (isset($request->country)) {
                $UsersData->country = $request->country;
            }else{
              $UsersData->country = '-';
            } 
            if (isset($request->province)) {
                $UsersData->province = $request->province;
            }else{
              $UsersData->province = '-';
            } 
             if (isset($request->mobile_number)) {
                $UsersData->mobile_number = $request->mobile_number;
            }else{
              $UsersData->mobile_number = '-';
            } 
             if (isset($request->pec)) {
                $UsersData->pec = $request->pec;
            }else{
              $UsersData->pec = '-';
            } 
            if (isset($request->description)) {
                $UsersData->description = $request->description;
            }else{
              $UsersData->description = '-';
            } 
            if (isset($request->accepted_payments)) {
                $UsersData->accepted_payments = $request->accepted_payments;
            }else{
              $UsersData->accepted_payments = '-';
            } 
            if (isset($request->terms_of_sale)) {
                $UsersData->terms_of_sale = $request->terms_of_sale;
            }else{
              $UsersData->terms_of_sale = '-';
            } */
            $UsersData->save();

            $People = new People();

           if (isset($request->name)) {
                $People->name = $request->name;
           }else{
              $People->name = '';
            }
            if (isset($request->surname)) {
                $People->surname = $request->surname;
            }else{
              $People->surname = '';
            }

            if (isset($request->email)) {
                $People->email = $request->email;
            }else{
              $People->email = '';
            }
            if (isset($request->phone_number)) {
                $People->phone_number = $request->phone_number;
            }else{
              $People->phone_number = '';
            }
          // if (isset($request->idUser)) {
                $People->created_id = $request->idUser;
          //  }
            $People->save();


            $PeopleUser = new PeopleUser();
            $PeopleUser->user_id = $request->user_id;

           // if (isset($People->id)) {
                $PeopleUser->people_id = $People->id;
           // }
           // if (isset($People->$idUser)) {
                $PeopleUser->created_id = $People->$idUser;
           // }
            $PeopleUser->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $user->id;
    }


    public static function insertComment(Request $request)
    {
        DB::beginTransaction();
        try {
            $logs = new Logs();
            $logs->user_id =  $request->id_user;
            $logs->ip =  $request->ip(); 
            $logs->action = 'COMMENT';
            $logs->description =  $request->comment;
            $logs->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $logs->id;
    }
    
 
 /**
     * Validate data
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * @param DbOperationTypeEnum dbOperationsTypesEnum
     */
    private static function _validateUser(Request $request, int $idLanguage, $dbOperationsTypesEnum)
    {
        switch ($dbOperationsTypesEnum) {
            case DbOperationsTypesEnum::INSERT:
               // if (User::where('name', $request->username)->count() > 0) {
                  //  throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UsernameAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
              //  }
                if (User::where('email', $request->email)->count() > 0) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }
              /*  if (UsersDatas::where('vat_number', $request->vat_number)->count() > 0) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::VatNumberAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }
                if (UsersDatas::where('fiscal_code', $request->fiscal_code)->count() > 0) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FiscalCodeAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }*/
                break;

            case DbOperationsTypesEnum::UPDATE:
                if (is_null($request->id)) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
                } else {
                    if (is_null(User::find($request->id))) {
                        throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotFound), HttpResultsCodesEnum::InvalidPayload);
                    }
                }
                if (User::where('email', $request->email)->whereNotIn('id', [$request->id])->count() > 0) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }
                if (UsersDatas::where('vat_number', $request->vat_number)->whereNotIn('user_id', [$request->id])->count() > 0) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::VatNumberAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }
                  if (UsersDatas::where('fiscal_code', $request->fiscal_code)->whereNotIn('user_id', [$request->id])->count() > 0) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FiscalCodeAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }

                break;
        }
    }


    /**
     * insertUser
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int id
     */
    public static function insertUser(Request $request, int $idLanguage)
    {
        static::_validateUser($request, $idLanguage, DbOperationsTypesEnum::INSERT);
        DB::beginTransaction();

        try {
            $user = new User();

            if (!isset($request->username)){
                $user->name = $request->email;
            }else{
                $user->name = $request->username;
            }

            $user->password = bcrypt($request->password);
            $user->email = $request->email;
            $user->language_id = $request->idLanguage;
            $user->confirmed = true;
            $user->online = true;
            $user->save();

            LogsBL::insert($user->id, $request->ip(), 'SIGNUP', 'Registrazione');

            $UsersData = new UsersDatas();
            $UsersData->user_id = $user->id;
            $UsersData->company = $request->company;
            

            if (isset($request->name)){
                $UsersData->name = $request->name;
            }else {
                $UsersData->name = '';
            }
            if (isset($request->name)){
                $UsersData->surname = $request->surname;
            }else {
                $UsersData->surname = '';
            }
            if (isset($request->business_name)){
                $UsersData->business_name = $request->business_name;
            }else {
                $UsersData->business_name = '';
            }
          
            $UsersData->fiscal_code = $request->fiscal_code;    
            $UsersData->vat_number = $request->vat_number;
            $UsersData->language_id = $request->idLanguage;
            $UsersData->address = $request->address;
            $UsersData->postal_code = $request->postal_code;
            $UsersData->telephone_number = $request->telephone_number;
            $UsersData->fax_number = $request->fax_number;
            $UsersData->website = $request->website;
            $UsersData->vat_type_id = $request->vat_type_id;
            $UsersData->country = $request->country;
            $UsersData->province = $request->province;
            $UsersData->province_id = $request->province_id;
            $UsersData->nation_id = $request->nation_id;
            $UsersData->region_id = $request->region_id;
            $UsersData->mobile_number = $request->mobile_number;
            $UsersData->pec = $request->pec;
            $UsersData->description = $request->description;
            $UsersData->accepted_payments = $request->accepted_payments;
            $UsersData->terms_of_sale = $request->terms_of_sale;
            $UsersData->id_sdi = $request->id_sdi;
            $UsersData->carrier_id = $request->carrier_id;
            $UsersData->create_quote = $request->create_quote;
            $UsersData->privates = $request->privates;
            $UsersData->id_payments = $request->id_payments;
            $UsersData->port = $request->port;
            $UsersData->final = $request->final;
            $UsersData->id_dealer = $request->id_dealer;
            $UsersData->save();

            $Subscriber = new Subscriber();
            $Subscriber->user_id = $user->id;
            $Subscriber->subscriber = $request->subscriber;
            $Subscriber->basic = $request->basic;
            $Subscriber->plus = $request->plus;
            $Subscriber->subscription_date = $request->subscription_date;
            $Subscriber->expiration_date = $request->expiration_date;
            $Subscriber->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $user->id;
    }


    /**
     * Insert without Auth
     *
     * @param Request request
     * @param int idLanguage
     *
     * @return int id
     */

    public static function insertNoAuth(Request $request, int $idLanguage)
    {
        if ((User::where('name', $request->email)->count() > 0) && (UsersDatas::where('vat_number', $request->vat_number)->count() > 0)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailAndVatNumberAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
        }

          if (User::where('email', $request->email)->count() > 0) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }
                if ($request['password'] != $request['password_confirmation']) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PasswordWrong), HttpResultsCodesEnum::InvalidPayload);
                }
                
       // static::_validateDataNoAuth($request, $idLanguage, DbOperationsTypesEnum::INSERT);
        DB::beginTransaction();

        try {
            #region INSERT DATA 
            $user = new User();
            $user->name = $request->email;
            $user->password = bcrypt($request->password);
            $user->email = $request->email;

            $isOnline = Setting::where('code', 'DefaultUserOnline')->first();
            if(isset($isOnline)){
                if($isOnline->value == 'True'){
                    $user->online = 'true';
                }else{
                    $user->online = 'false';
                }
            }else{
                $user->online = 'true';
            }

            
            if (isset($request->idLanguage)) {
                $idLn = $request->idLanguage;
            } else {
                $idLn = $idLanguage;
            }
            $user->language_id = $idLn;
            $user->save();

            $userData = new UsersDatas();

            if (!is_null($request->imgName)) {
                if (strpos($request->imgBase64, '/api/storage/app/public/images/User/') === false) {
                    Image::make($request->imgBase64)->save(static::$dirImage . $user->id . $request->imgName);
                }
                $userData->img = static::getUrl() . $user->id . $request->imgName;
            }

            $userData->user_id = $user->id;
            if (isset($request->company)) {
                $userData->company = $request->company;
            }
            if (isset($request->business_name)) {
                $userData->business_name = $request->business_name;
            }
            if (isset($request->name)) {
                $userData->name = $request->name;
            }
            if (isset($request->surname)) {
                $userData->surname = $request->surname;
            }
            if (isset($request->vat_number)) {
                $userData->vat_number = $request->vat_number;
            }
            if (isset($request->fiscal_code)) {
                $userData->fiscal_code = $request->fiscal_code;
            }
            if (isset($request->address)) {
                $userData->address = $request->address;
            }
            if (isset($request->website)) {
                $userData->website = $request->website;
            }
            if (isset($request->postal_code)) {
                $userData->postal_code = $request->postal_code;
            }
            if (isset($request->telephone_number)) {
                $userData->telephone_number = $request->telephone_number;
            }
            if (isset($request->fax_number)) {
                $userData->fax_number = $request->fax_number;
            }
            
            if (isset($request->description)) {
                $userData->description = $request->description;
            }
            if (isset($request->accepted_payments)) {
                $userData->accepted_payments = $request->accepted_payments;
            }
            if (isset($request->shop_time)) {
                $userData->shop_time = $request->shop_time;
            }
            if (isset($request->maps)) {
                $userData->maps = $request->maps;
            }
            if (isset($request->terms_of_sale)) {
                $userData->terms_of_sale = $request->terms_of_sale;
            }
            if (isset($request->country)) {
                $userData->country = $request->country;
            }
            if (isset($request->province)) {
                $userData->province = $request->province;
            }
            if (isset($request->monday)) {
                $userData->monday = $request->monday;
            }
            if (isset($request->tuesday)) {
                $userData->tuesday = $request->tuesday;
            }   
            if (isset($request->wednesday)) {
                $userData->wednesday = $request->wednesday;
            }   
            if (isset($request->thursday)) {
                $userData->thursday = $request->thursday;
            }   
            if (isset($request->friday)) {
                $userData->friday = $request->friday;
            }   
            if (isset($request->saturday)) {
                $userData->saturday = $request->saturday;
            }   
            if (isset($request->sunday)) {
                $userData->sunday = $request->sunday;
            }
            if (isset($request->region_id)) {
                $userData->region_id = $request->region_id;
            }
            if (isset($request->province_id)) {
                $userData->province_id = $request->province_id;
            }
            if (isset($request->nation_id)) {
                $userData->nation_id = $request->nation_id;
            }
            if (isset($request->check_rehearsal_room)) {
                $userData->check_rehearsal_room = $request->check_rehearsal_room;
            }
            if (isset($request->check_salesregistration)) {
                $userData->check_salesregistration = $request->check_salesregistration;
            }

            if (isset($request->business_name)) {
                $userData->link = '/' . str_replace('\'', '-', str_replace(' ', '-', strtolower($request->business_name))) . '-' . $user->id;
            } else {
                $userData->link = '/' . str_replace('\'', '-', str_replace(' ', '-', strtolower($request->name))) . '-' . str_replace('\'', '-', str_replace(' ', '-', strtolower($request->surname))) . '-' . $user->id;
            }

            $userData->language_id = $idLn;
            if (isset($request->enable_from)) {
                $userData->enable_from = $request->enable_from;
            }
            if (isset($request->enable_to)) {
                $userData->enable_to = $request->enable_to;
            }
            if (isset($request->mobile_number)) {
                $userData->mobile_number = $request->mobile_number;
            }
            if (isset($request->pec)) {
                $userData->pec = $request->pec;
            }
            $userData->save();

            /* INSERIMENTO USER PHOTO AGGIUNTIVE */
            if (isset($request->imgAgg)) {
                foreach ($request->imgAgg as $userImgAgg) {
                    for ($i = 0; $i < 10; $i++) {
                        if (array_key_exists('imgName' . $i, $userImgAgg)) {
                            if (!is_null($userImgAgg['imgName' . $i]) && !is_null($userImgAgg['imgBase64' . $i])) {
                                if (strpos($request->imgBase64, '/api/storage/app/public/images/User/') === false) {
                                    Image::make($userImgAgg['imgBase64' . $i])->save(static::$dirImage . $user->id . $i . $userImgAgg['imgName' . $i]);
                                    $UserPhoto = new UserPhoto();
                                    $UserPhoto->user_id = $user->id;
                                    $UserPhoto->img = static::getUrl() . $user->id . $i . $userImgAgg['imgName' . $i];
                                    $UserPhoto->save();
                                }
                            }
                        }
                    }
                }
            }
            /* END INSERIMENTO USER PHOTO AGGIUNTIVE */

           if(isset($request->MapAgg)) { 
             foreach ($request->MapAgg as $MapAgg) {
                        
                    $UserAddress= new UserAddress();
                     $UserAddress->user_id = $user->id;

                    if (isset($MapAgg['address'])) {
                    $UserAddress->address = $MapAgg['address'];
                    }
                    if (isset($MapAgg['country'])) {
                    $UserAddress->country = $MapAgg['country'];
                    }
                    if (isset($MapAgg['maps'])) {
                    $UserAddress->maps = $MapAgg['maps'];
                    }
                    if (isset($MapAgg['province'])) {
                    $UserAddress->province = $MapAgg['province'];
                    }
                    if (isset($MapAgg['codepostal'])) {
                    $UserAddress->codepostal = $MapAgg['codepostal'];
                    }
                    $UserAddress->save();
                }
             }   

            if (isset($request->role)) {
                $idRole = Role::where('name', $request->role)->first();
                if (isset($idRole)) {
                    $userRole = new RoleUser();
                    $userRole->role_id = $idRole->id;
                    $userRole->user_id = $user->id;
                    $userRole->user_type = 'App\User';
                    $userRole->save();
                }
            }
             #endregion END INSERT DATA

             //REGION EMAIL CONFIRM REGISTRATION
            $emailFrom = SettingBL::getContactsEmail();
            $name = $user->name;
            $email = $user->email;
            $id = $user->id;
            //$textcode = Message::where('code', 'text_confirm_registration')->first();
            //$content = MessageLanguage::where('message_id', $textcode->id)->first();
            //$htmlcontent = $content->content;
           
            $url = "";   
            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
                $protocol = "https://";
            } else {
                $protocol = "http://";
            }
            $url = $protocol . $_SERVER["HTTP_HOST"];

            $textMailTag= MessageBL::getByCode('REGISTRATION_CONFIRM_MAIL_TEXT');
            $textObject= MessageBL::getByCode('REGISTRATION_CONFIRM_MAIL_OBJECT');
            $textMail =str_replace ('#name#', $name, $textMailTag);
            $textMail =str_replace ('#url#', $url, $textMail);
            $textMail =str_replace ('#email#', $email, $textMail);
            $textMail =str_replace ('#id#', $id, $textMail);
            Mail::to($user->email)->send(new DynamicMail($textObject,$textMail)); 

            $mailNewUser = SettingBL::getByCode('EmailNewUser');
            
            if($mailNewUser != null && $mailNewUser!= ''){
                Mail::to($mailNewUser)->send(new DynamicMail($textObject,$textMail));
            }
            
            #region SEND EMAIL 

            
           // Mail::to($user->email)->send(new ConfirmRegistrationMail($emailFrom, $name, $url,$email, $id, $htmlcontent));
            //$return = $user->id;
            #endregion SEND EMAIL

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
    #endregion PUT


    public static function ResetConfermationEmail(Request $request)
    {
  
        $user = User::where('email', $request->email)->get()->first();
        $return = "";
        
        if (isset($user)) {
            $emailFrom = SettingBL::getContactsEmail();
            $name = $user->name;
            $email = $user->email;
            $id = $user->id;

            $url = "";
            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
                $protocol = "https://";
            } else {
                $protocol = "http://";
            }
            $url = $protocol . $_SERVER["HTTP_HOST"];

           $textMailTag= MessageBL::getByCode('REGISTRATION_CONFIRM_MAIL_TEXT');
            $textObject= MessageBL::getByCode('REGISTRATION_CONFIRM_MAIL_OBJECT');
            $textMail =str_replace ('#name#', $name, $textMailTag);
            $textMail =str_replace ('#url#', $url, $textMail);
            $textMail =str_replace ('#email#', $email, $textMail);
            $textMail =str_replace ('#id#', $id, $textMail);
            Mail::to($user->email)->send(new DynamicMail($textObject,$textMail)); 

            $return = $user->id;
        }else{
            $return = "";
        }
        return  $return;
    
    }
    #endregion PUT


    #region POST
    /**
     * change password
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int id
     */
    public static function changePassword(Request $request, int $idUser)
    {
        $user = User::find($request->id);
        if (!is_null($request->password)) {
            $user->password = bcrypt($request->password);
        }
        $user->save();
        return  $request->id;
    }

    /**
     * change password
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int id
     */
    public static function changePasswordRescued(Request $request)
    {
        DB::beginTransaction();
        try {
            $returnVar = "";
            $userRescuedPassword = UserRescuePassword::where('id', $request->id)->where('avaiable', true)->first();
            if(isset($userRescuedPassword)){
                $user = User::where('id', $userRescuedPassword->user_id)->first();
                if(isset($user)){
                    if (!is_null($request->password)) {
                        $user->password = bcrypt($request->password);
                    }
                    $user->save();

                    $userRescuedPasswordOnDb = UserRescuePassword::where('id', $request->id)->get()->first();
                    $userRescuedPasswordOnDb->avaiable = false;
                    $userRescuedPasswordOnDb->save();

                    $returnVar = $userRescuedPasswordOnDb->id;

                }else{
                    $returnVar = "";
                }
            }else{
                $returnVar = "";
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return  $returnVar;
    }

    /**
     * change password
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int id
     */
    public static function rescuePassword(Request $request)
    {
        $user = User::where('email', $request->email)->get()->first();
        $returnVar = "";
        
        if (isset($user)) {
            //insert new table tracking request change password;
            $userRescuePassword = new UserRescuePassword();
            $userRescuePassword->user_id = $user->id;
            $userRescuePassword->ip = $request->ip();
            $userRescuePassword->avaiable = true;
            $userRescuePassword->save();

            //send email with link rescuepassword
            $name = $user->name;
            $url = "";
            if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
                $protocol = "https://";
            } else {
                $protocol = "http://";
            }
            $url = $protocol . $_SERVER["HTTP_HOST"];

            $testoMailConTag = MessageBL::getByCode('USER_RESCUE_PASSWORD_TEXT');
            $oggettoMail = MessageBL::getByCode('USER_RESCUE_PASSWORD_SUBJECT');
            $testoMail = str_replace('#url#' , $url,  $testoMailConTag);
            $testoMail = str_replace('#id#', $userRescuePassword->id,  $testoMail);
            $testoMail = str_replace('#name#', $name,  $testoMail);

            Mail::to($user->email)->send(new DynamicMail($oggettoMail, $testoMail));

            $returnVar = $user->id;
        }else{
            $returnVar = "";
        }
        return  $returnVar;
    }

    /**
     * Insert
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int id
     */
    public static function update(Request $request, int $idUser)
    {
   
        //static::_validateData($request, $idUser, $idLanguage, DbOperationsTypesEnum::UPDATE);

        $user = User::find($request->id);
        $UsersDatas = UsersDatas::where('user_id', $user->id)->first();

        DB::beginTransaction();
        try {

            if (isset($request->id)) {
                $user->id = $request->id;
            }

            if (isset($request->username)) {
                $user->name = $request->username;
            }

            if (!is_null($request->password)) {
                $user->password = bcrypt($request->password);
            }

            if (isset($request->email)) {
                $user->email = $request->email;
            }

            if (isset($request->idLanguage)) {
                $user->language_id = $request->idLanguage;
            }

            if (isset($request->confirmed)) {
                $user->confirmed = $request->confirmed;
            }
            if (isset($request->send_newsletter)) {
                $user->send_newsletter = $request->send_newsletter;
            }
            if (isset($request->online)) {
                $user->online = $request->online;
            }
            $user->save();

           // if (isset($request->id)) {
                $UsersDatas->user_id = $request->id;
           // }

            if (isset($request->name)) {
                $UsersDatas->name = $request->name;
            }

            if (isset($request->company)) {
                $UsersDatas->company = $request->company;
            }else{
                $UsersDatas->company = false;   
            }
            if (isset($request->business_name)) {
                $UsersDatas->business_name = $request->business_name;
            }
            if (isset($request->surname)) {
                $UsersDatas->surname = $request->surname;
            }
            if (isset($request->vat_number)) {
                $UsersDatas->vat_number = $request->vat_number;
            }
            if (isset($request->fiscal_code)) {
                $UsersDatas->fiscal_code = $request->fiscal_code;
            }
            if (isset($request->province)) {
                $UsersDatas->province_id = $request->province;
            }
            if (isset($request->address)) {
                $UsersDatas->address = $request->address;
            }
            if (isset($request->postal_code)) {
                $UsersDatas->postal_code = $request->postal_code;
            }
            if (isset($request->telephone_number)) {
                $UsersDatas->telephone_number = $request->telephone_number;
            }
            if (isset($request->fax_number)) {
                $UsersDatas->fax_number = $request->fax_number;
            }
            if (isset($request->website)) {
                $UsersDatas->website = $request->website;
            }
            if (isset($request->vat_type_id)) {
                $UsersDatas->vat_type_id = $request->vat_type_id;
            }

            if (isset($request->country)) {
                $UsersDatas->country = $request->country;
            }
              if (isset($request->country)) {
                $UsersDatas->cities_id = $request->country;
            }
            

            if (isset($request->province)) {
                $UsersDatas->province = $request->province;
            }
            if (isset($request->mobile_number)) {
                $UsersDatas->mobile_number = $request->mobile_number;
            }
            if (isset($request->pec)) {
                $UsersDatas->pec = $request->pec;
            }

            if (isset($request->description)) {
                $UsersDatas->description = $request->description;
            }

            if (isset($request->accepted_payments)) {
                $UsersDatas->accepted_payments = $request->accepted_payments;
            }

            if (isset($request->terms_of_sale)) {
                $UsersDatas->terms_of_sale = $request->terms_of_sale;
            }
            if (isset($request->shop_time)) {
                $UsersDatas->shop_time = $request->shop_time;
            }
            if (isset($request->maps)) {
                $UsersDatas->maps = $request->maps;
            }

            if (!is_null($request->imgName)) {
                Image::make($request->img)->save(static::$dirImage . $request->imgName);
                $UsersDatas->img = static::getUrl() . $request->imgName;
            }
            if (isset($request->$idUser)) {
                $UsersDatas->created_id = $request->$idUser;
            }
            $UsersDatas->language_id = $request->idLanguage;
            $UsersDatas->save();


            /* END INSERIMENTO USER PHOTO AGGIUNTIVE */

            /*   if(isset($request->imgAgg)){
                foreach ($request->imgAgg as $userImgAgg) {
                    $UserPhotos = UserPhoto::where('user_id', $user->id)->where('img', $userImgAgg)->first();
                    if(!isset($userImgAgg)){  
                        Image::make($userImgAgg['imgBase64' . $i])->save(static::$dirImage . $user->id . $i . $userImgAgg['imgName' . $i]);    
                        $UserPhoto = new UserPhoto();
                        $UserPhoto->user_id = $user->id;
                        $UserPhoto->img = static::getUrl() . $user->id . $i . $userImgAgg['imgName' . $i];
                        $UserPhoto->save();


                    }
                }
            }
*/
          DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return  $request->id;
    }


  /**
     * updateUser
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int id
     */
    public static function updateorcreateUsersAdresses(Request $request)
    {

        if (isset($request->id_useraddress)) {
                $UsersAddresses = UsersAddresses::where('id', $request->id_useraddress)->first();
        }

        DB::beginTransaction();
        try {

              if (!isset($UsersAddresses->id)) {

                $UsersAddresses = new UsersAddresses();

                $UsersAddresses->user_id = $request->user_id; 

                if (isset($request->business_name)){
                    $UsersAddresses->business_name = $request->business_name;
                }else{
                    $UsersAddresses->business_name = ''; 
                }
                if (isset($request->address)){
                    $UsersAddresses->address = $request->address;
                }else{
                    $UsersAddresses->address = ''; 
                }
                if (isset($request->postal_code)){
                    $UsersAddresses->postal_code = $request->postal_code;
                }else{
                    $UsersAddresses->postal_code = ''; 
                }
                if (isset($request->locality)){
                    $UsersAddresses->locality = $request->locality;
                }else{
                    $UsersAddresses->locality = ''; 
                }
                if (isset($request->province_id)){
                    $UsersAddresses->province_id = $request->province_id;
                }else{
                    $UsersAddresses->province_id = null; 
                }
                if (isset($request->phone_number)){
                    $UsersAddresses->phone_number = $request->phone_number;
                }else{
                    $UsersAddresses->phone_number = ''; 
                }
                if (isset($request->name)){
                    $UsersAddresses->name = $request->name;
                }else{
                    $UsersAddresses->name = ''; 
                }
                if (isset($request->surname)){
                    $UsersAddresses->surname = $request->surname;
                }else{
                    $UsersAddresses->surname = ''; 
                }
                if (isset($request->region_id)){
                    $UsersAddresses->region_id = $request->region_id;
                }else{
                    $UsersAddresses->region_id = null; 
                }
                $UsersAddresses->save();

            } else {

                if (isset($UsersAddresses->id)) {
                    $UsersAddresses->user_id = $request->user_id;
                    if (isset($request->business_name)){
                        $UsersAddresses->business_name = $request->business_name;
                    }else{
                        $UsersAddresses->business_name = ''; 
                    }
                    if (isset($request->address)){
                        $UsersAddresses->address = $request->address;
                    }else{
                        $UsersAddresses->address = ''; 
                    }
                    if (isset($request->postal_code)){
                        $UsersAddresses->postal_code = $request->postal_code;
                    }else{
                        $UsersAddresses->postal_code = ''; 
                    }
                    if (isset($request->locality)){
                        $UsersAddresses->locality = $request->locality;
                    }else{
                        $UsersAddresses->locality = ''; 
                    }
                    if (isset($request->province_id)){
                        $UsersAddresses->province_id = $request->province_id;
                    }else{
                        $UsersAddresses->province_id = null; 
                    }
                    if (isset($request->phone_number)){
                        $UsersAddresses->phone_number = $request->phone_number;
                    }else{
                        $UsersAddresses->phone_number = ''; 
                    }
                    if (isset($request->name)){
                        $UsersAddresses->name = $request->name;
                    }else{
                        $UsersAddresses->name = ''; 
                    }
                    if (isset($request->surname)){
                        $UsersAddresses->surname = $request->surname;
                    }else{
                        $UsersAddresses->surname = ''; 
                    }
                    if (isset($request->region_id)){
                        $UsersAddresses->region_id = $request->region_id;
                    }else{
                        $UsersAddresses->region_id = null; 
                    }
                     $UsersAddresses->save();   
                }

            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return  $request->id;
    }

    
    /**
     * updateUser
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int id
     */
    public static function updateorcreateDestinationBusinessName(Request $request)
    {
       $GrUserDifferentDestination = GrUserDifferentDestination::where('id', $request->id)->first();
       
        DB::beginTransaction();
        try {

              if (!isset($GrUserDifferentDestination->id)) {
                $RowGrUserDifferentDestination = new GrUserDifferentDestination();

                $RowGrUserDifferentDestination->user_id = $request->user_id; 
                if (isset($request->business_name_plus)){
                    $RowGrUserDifferentDestination->business_name_plus = $request->business_name_plus;
                }else{
                    $RowGrUserDifferentDestination->business_name_plus = ''; 
                }
                if (isset($request->address)){
                    $RowGrUserDifferentDestination->address = $request->address;
                }else{
                    $RowGrUserDifferentDestination->address = ''; 
                }
                if (isset($request->codepostal)){
                    $RowGrUserDifferentDestination->codepostal = $request->codepostal;                
                }else{
                    $RowGrUserDifferentDestination->codepostal = ''; 
                }
                if (isset($request->province)){
                    $RowGrUserDifferentDestination->province = $request->province;              
                }else{
                    $RowGrUserDifferentDestination->province = ''; 
                }
                if (isset($request->country)){
                    $RowGrUserDifferentDestination->country = $request->country;                
                }else{
                    $RowGrUserDifferentDestination->country = ''; 
                }
                if (isset($request->telephone)){
                    $RowGrUserDifferentDestination->telephone = $request->telephone;                
                }else{
                    $RowGrUserDifferentDestination->telephone = ''; 
                }
                
                $RowGrUserDifferentDestination->save();
            } else {
                if (isset($GrUserDifferentDestination->id)) {
                $GrUserDifferentDestination->user_id = $request->user_id;

                if (isset($request->business_name_plus)){
                    $GrUserDifferentDestination->business_name_plus = $request->business_name_plus;                
                }else{
                    $GrUserDifferentDestination->business_name_plus = ''; 
                }
                if (isset($request->address)){
                    $GrUserDifferentDestination->address = $request->address;
                }else{
                    $GrUserDifferentDestination->address = ''; 
                }
                if (isset($request->codepostal)){
                    $GrUserDifferentDestination->codepostal = $request->codepostal;
                }else{
                    $GrUserDifferentDestination->codepostal = ''; 
                }
                if (isset($request->province)){
                    $GrUserDifferentDestination->province = $request->province;
                }else{
                    $GrUserDifferentDestination->province = ''; 
                }
                if (isset($request->country)){
                    $GrUserDifferentDestination->country = $request->country;
                }else{
                    $GrUserDifferentDestination->country = ''; 
                }
                if (isset($request->telephone)){
                    $GrUserDifferentDestination->telephone = $request->telephone;
                }else{
                    $GrUserDifferentDestination->telephone = ''; 
                }

                $GrUserDifferentDestination->save();
                }
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return  $request->id;
    }

    /**
     * update Field By Id
     *
     * @param Request request
     *
     * @return int id
     */
    public static function updateFieldById(Request $request)
    {
        $userdata = UsersDatas::where('user_id', $request->userId)->first();
        $return = 0;
        if(isset($userdata)){
            $userdata->{$request->field} = $request->value;
            $userdata->save();
            $return = $userdata->id;
        }

        return  $return;
    }



    /**
     * updateVatNumber
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int id
     */
    public static function updateVatNumber(Request $request)
    {
        $UsersDatas = UsersDatas::where('user_id', $request->id)->first();
        DB::beginTransaction();
        try {
                $UsersDatas->user_id = $request->id;
                $UsersDatas->vat_number = $request->vat_number;
                $UsersDatas->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    /**
     * updateProfileImage
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int id
     */
    public static function updateProfileImage(Request $request)
    {
        $UsersDatas = UsersDatas::where('user_id', $request->userId)->first();
        DB::beginTransaction();
        try {
                if (!is_null($request->imgName)) {
                    Image::make($request->imgBase64)->save(static::$dirImagesGeneral . $request->userId . '-' . $request->imgName);
                    $UsersDatas->img = static::getUrlGeneral() . $request->userId . '-' . $request->imgName;
                }
                $UsersDatas->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    /**
     * uploadImageCoverProfile
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int id
     */
    public static function uploadImageCoverProfile(Request $request)
    {
        $UsersDatas = UsersDatas::where('user_id', $request->userId)->first();
        DB::beginTransaction();
        try {
                if (!is_null($request->imgName)) {
                    Image::make($request->imgBase64)->save(static::$dirImagesGeneral . $request->userId . '-' . $request->imgName);
                    $UsersDatas->img_cover = static::getUrlGeneral() . $request->userId . '-' . $request->imgName;
                }
                $UsersDatas->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }




    /**
     * updateUser
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int id
     */
    public static function updateUser(Request $request, int $idUser, int $idLanguage)
    {
        LogHelper::debug($request);
       // static::_validateUser($request, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $user = User::find($request->id);
        $UsersDatas = UsersDatas::where('user_id', $user->id)->first();

        DB::beginTransaction();
        try {

            if (isset($request->id)) {
                $user->id = $request->id;
            }

            if (isset($request->username)) {
                $user->name = $request->username;
            }else{
              $user->name = '';
            } 

            if (isset($request->email)) {
                $user->email = $request->email;
            }else{
              $user->email = '';
            } 

            if (!is_null($request->password)) {
                $user->password = bcrypt($request->password);
            }

            if (isset($request->idLanguage)) {
                $user->language_id = $request->idLanguage;
            }
            $user->save();

            if (isset($request->id)) {
                $UsersDatas->user_id = $request->id;
            }

            if (isset($request->name)) {
                $UsersDatas->name = $request->name;
            }else{
                $UsersDatas->name = '';
            } 

            //if (isset($request->company)) {
                //$UsersDatas->company = $request->company;
            //}

            if (isset($request->business_name)) {
                $UsersDatas->business_name = $request->business_name;
            }else{
              $UsersDatas->business_name = '';
            } 

            if (isset($request->surname)) {
                $UsersDatas->surname = $request->surname;
            }else{
              $UsersDatas->surname = '';
            } 
            
            if (isset($request->vat_number)) {
                $UsersDatas->vat_number = $request->vat_number;
            }else{
              $UsersDatas->vat_number = '';
            } 
            if (isset($request->fiscal_code)) {
                $UsersDatas->fiscal_code = $request->fiscal_code;
            }else{
              $UsersDatas->fiscal_code = '';
            } 

            if (isset($request->address)) {
                $UsersDatas->address = $request->address;
            }else{
              $UsersDatas->address = '';
            } 
            if (isset($request->codepostal)) {
                $UsersDatas->codepostal = $request->codepostal;
            }else{
              $UsersDatas->codepostal = '';
            } 
            if (isset($request->telephone_number)) {
                $UsersDatas->telephone_number = $request->telephone_number;
            }else{
              $UsersDatas->telephone_number = '';
            } 
            if (isset($request->fax_number)) {
                $UsersDatas->fax_number = $request->fax_number;
            }else{
              $UsersDatas->fax_number = '';
            } 
            if (isset($request->website)) {
                $UsersDatas->website = $request->website;
            }else{
              $UsersDatas->website = '';
            } 
            if (isset($request->vat_type_id)) {
                $UsersDatas->vat_type_id = $request->vat_type_id;
            }else{
              $UsersDatas->vat_type_id = NULL;
            } 

            if (isset($request->country)) {
                $UsersDatas->country = $request->country;
            }else{
              $UsersDatas->country = '';
            } 

            if (isset($request->province)) {
                $UsersDatas->province = $request->province;
            }else{
              $UsersDatas->province = '';
            } 
            if (isset($request->mobile_number)) {
                $UsersDatas->mobile_number = $request->mobile_number;
            }else{
              $UsersDatas->mobile_number = '';
            } 
            if (isset($request->pec)) {
                $UsersDatas->pec = $request->pec;
            }else{
              $UsersDatas->pec = '';
            } 
            if (isset($request->carrier_id)) {
                $UsersDatas->carrier_id = $request->carrier_id;
            }else{
              $UsersDatas->carrier_id = NULL;
            } 
            if (isset($request->id_payments)) {
                $UsersDatas->id_payments = $request->id_payments;
            }else{
              $UsersDatas->id_payments = NULL;
            } 

            if (isset($request->id_sdi)) {
                $UsersDatas->id_sdi = $request->id_sdi;
            }else{
              $UsersDatas->id_sdi = NULL;
            } 
            if (isset($request->create_quote)) {
                $UsersDatas->create_quote = $request->create_quote;
            }else{
              $UsersDatas->create_quote = NULL;
            } 
            if (isset($request->privates)) {
                $UsersDatas->privates = $request->privates;
            }else{
              $UsersDatas->privates = NULL;
            } 

            if (isset($request->$idUser)) {
                $UsersDatas->created_id = $request->$idUser;
            }
            if (isset($request->port)) {
                $UsersDatas->port = $request->port;
            }else{
              $UsersDatas->port = '';
            } 
            if (isset($request->final)) {
                $UsersDatas->final = $request->final;
            }else{
              $UsersDatas->final = NULL;
            } 
             if (isset($request->id_dealer)) {
                $UsersDatas->id_dealer = $request->id_dealer;
            }else{
              $UsersDatas->id_dealer = NULL;
            } 

            $UsersDatas->language_id = $request->idLanguage;
            $UsersDatas->save();

            $Subscriber = Subscriber::where('user_id', $request->id)->first();

            if (isset($Subscriber->user_id)) {
            $Subscriber->user_id = $request->id;
            $Subscriber->subscriber = $request->subscriber;
            $Subscriber->basic = $request->basic;
            $Subscriber->plus = $request->plus;
            $Subscriber->subscription_date = $request->subscription_date; 
            $Subscriber->expiration_date = $request->expiration_date; 
            $Subscriber->save();  
            }else{
            if (!isset($Subscriber->user_id)) {
               $rowSubscriber = new Subscriber; 
               $rowSubscriber->user_id=$request->id;
               $rowSubscriber->subscriber =$request->subscriber; 
               $rowSubscriber->basic = $request->basic;
               $rowSubscriber->plus = $request->plus;
               $rowSubscriber->subscription_date = $request->subscription_date;
               $rowSubscriber->expiration_date = $request->expiration_date; 
               $rowSubscriber->save(); 
            }
        }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return  $request->id;
    }

    /**
     * updateNoAuth
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int id
     */
    public static function updateNoAuth(Request $request, int $idUser)
    {

        $user = User::find($request->id);
        $UsersDatas = UsersDatas::where('user_id', $user->id)->first();

        DB::beginTransaction();
        try {

            if (isset($request->id)) {
                $user->id = $request->id;
            }

            if (isset($request->username)) {
                $user->name = $request->username;
            }

            if (!is_null($request->password)) {
                $user->password = bcrypt($request->password);
            }

            if (isset($request->email)) {
                $user->email = $request->email;
            }

            if (isset($request->idLanguage)) {
                $user->language_id = $request->idLanguage;
            }

            $user->save();

            if (isset($request->id)) {
                $UsersDatas->user_id = $request->id;
            }

            if (isset($request->name)) {
                $UsersDatas->name = $request->name;
            }

            if (isset($request->company)) {
                $UsersDatas->company = $request->company;
            }
            if (isset($request->business_name)) {
                $UsersDatas->business_name = $request->business_name;
            }

            if (isset($request->surname)) {
                $UsersDatas->surname = $request->surname;
            }
            if (isset($request->vat_number)) {
                $UsersDatas->vat_number = $request->vat_number;
            }
            if (isset($request->fiscal_code)) {
                $UsersDatas->fiscal_code = $request->fiscal_code;
            }
             if (isset($request->check_rehearsal_room)) {
                $UsersDatas->check_rehearsal_room = $request->check_rehearsal_room;
            }
             if (isset($request->check_salesregistration)) {
                $UsersDatas->check_salesregistration = $request->check_salesregistration;
            }
            if (isset($request->address)) {
                $UsersDatas->address = $request->address;
            }
            if (isset($request->postal_code)) {
                $UsersDatas->postal_code = $request->postal_code;
            }
            if (isset($request->telephone_number)) {
                $UsersDatas->telephone_number = $request->telephone_number;
            }
            if (isset($request->fax_number)) {
                $UsersDatas->fax_number = $request->fax_number;
            }
            if (isset($request->website)) {
                $UsersDatas->website = $request->website;
            }
            if (isset($request->vat_type_id)) {
                $UsersDatas->vat_type_id = $request->vat_type_id;
            }

            if (isset($request->country)) {
                $UsersDatas->country = $request->country;
            }

            if (isset($request->province)) {
                $UsersDatas->province = $request->province;
            }
            if (isset($request->mobile_number)) {
                $UsersDatas->mobile_number = $request->mobile_number;
            }
            if (isset($request->pec)) {
                $UsersDatas->pec = $request->pec;
            }

            if (isset($request->description)) {
                $UsersDatas->description = $request->description;
            }

            if (isset($request->accepted_payments)) {
                $UsersDatas->accepted_payments = $request->accepted_payments;
            }

            if (isset($request->terms_of_sale)) {
                $UsersDatas->terms_of_sale = $request->terms_of_sale;
            }
            if (isset($request->shop_time)) {
                $UsersDatas->shop_time = $request->shop_time;
            }
            if (isset($request->maps)) {
                $UsersDatas->maps = $request->maps;
            }

            if (!is_null($request->imgName)) {
                Image::make($request->img)->save(static::$dirImage . $request->imgName);
                $UsersDatas->img = static::getUrl() . $request->imgName;
            }

            if (isset($request->$idUser)) {
                $UsersDatas->created_id = $request->$idUser;
            }

            $UsersDatas->language_id = $request->idLanguage;


            if (isset($request->monday)) {
                $UsersDatas->monday = $request->monday;
            }
            if (isset($request->tuesday)) {
                $UsersDatas->tuesday = $request->tuesday;
            }
            if (isset($request->wednesday)) {
                $UsersDatas->wednesday = $request->wednesday;
            }
            if (isset($request->sunday)) {
                $UsersDatas->sunday = $request->sunday;
            }
            if (isset($request->thursday)) {
                $UsersDatas->thursday = $request->thursday;
            }
            if (isset($request->friday)) {
                $UsersDatas->friday = $request->friday;
            }
            if (isset($request->saturday)) {
                $UsersDatas->saturday = $request->saturday;
            }

            if (isset($request->province_id)) {
                $UsersDatas->province_id = $request->province_id;
            }
            if (isset($request->nation_id)) {
                $UsersDatas->nation_id = $request->nation_id;
            }
            if (isset($request->region_id)) {
                $UsersDatas->region_id = $request->region_id;
            }

            $UsersDatas->save();

            /* UPDATE IMG AGGIUNTIVE */

            $userPhotos = UserPhoto::where('user_id', $request->id)->get();
            if (isset($userPhotos)) {
                UserPhoto::where('user_id', $request->id)->delete();
            }

            if (isset($request->imgAgg)) {
                foreach ($request->imgAgg as $userImgAgg) {
                    for ($i = 0; $i < 10; $i++) {
                        if (array_key_exists('imgName' . $i, $userImgAgg)) {
                            if (!is_null($userImgAgg['imgName' . $i]) && !is_null($userImgAgg['imgBase64' . $i])) {
                                if (strpos($request->imgBase64, '/api/storage/app/public/images/User/') === false) {
                                    Image::make($userImgAgg['imgBase64' . $i])->save(static::$dirImage . $request->id . $i . $userImgAgg['imgName' . $i]);
                                    $UserPhoto = new UserPhoto();
                                    $UserPhoto->user_id = $request->id;
                                    $UserPhoto->img = static::getUrl() . $request->id . $i . $userImgAgg['imgName' . $i];
                                    $UserPhoto->save();
                                }
                            } else {
                                if (!is_null($userImgAgg['imgName' . $i]) && is_null($userImgAgg['imgBase64' . $i])) {
                                    $UserPhoto = new UserPhoto();
                                    $UserPhoto->user_id = $request->id;
                                    $UserPhoto->img = $userImgAgg['imgName' . $i];
                                    $UserPhoto->save();
                                }
                            }
                        }
                    }
                }
            }
            /* END UPDATE IMG AGGIUNTIVE */
 
            //update or insert maps users: 
     // $userAddressOnDb = UserAddress::where('id', $AggMaps['id_map'])->where('user_id', $request->id)->get()->first();
           /*     $useraddress = UserAddress::where('user_id', $request->id)->get();
                if (isset($useraddress)) {
                    UserAddress::where('user_id', $request->id)->delete();
                }*/
                      
                   
                if (isset($request->AggMaps)) {
                     $AggMapss = UserAddress::where('user_id', $request->id)->get();
                            if (isset($AggMapss)) {
                                UserAddress::where('user_id', $request->id)->delete();                            
                            }
                    foreach ($request->AggMaps as $AggMaps) { 
                      //  if(!isset($AggMaps['id_map'])){
                            $UserAddress = new UserAddress();  
                            $UserAddress->user_id = $idUser;
                            if (isset($AggMaps['maps_map'])) {
                            $UserAddress->maps = $AggMaps['maps_map'];
                            }
                            if (isset($AggMaps['address_maps'])) {
                            $UserAddress->address = $AggMaps['address_maps'];
                            }
                            if (isset($AggMaps['country_maps'])) {
                            $UserAddress->country = $AggMaps['country_maps'];
                            }
                            if (isset($AggMaps['province_maps'])) {
                            $UserAddress->province = $AggMaps['province_maps'];
                            }
                            if (isset($AggMaps['codepostal_maps'])) {
                            $UserAddress->codepostal = $AggMaps['codepostal_maps'];
                            }
                            $UserAddress->save();
                        //}else{
                           /* $userAddressOnDb = UserAddress::where('id', $AggMaps['id_map'])->where('user_id', $request->id)->get()->first();
                            if(isset($userAddressOnDb)){  
                                if (isset($AggMaps['maps_map'])) {
                                $userAddressOnDb->maps = $AggMaps['maps_map'];
                                }
                                if (isset($AggMaps['address_maps'])) {
                                $userAddressOnDb->address = $AggMaps['address_maps'];
                                }
                                if (isset($AggMaps['country_maps'])) {
                                $userAddressOnDb->country = $AggMaps['country_maps'];
                                }
                                if (isset($AggMaps['province_maps'])) {
                                $userAddressOnDb->province = $AggMaps['province_maps'];
                                }
                                if (isset($AggMaps['codepostal_maps'])) {
                                $userAddressOnDb->codepostal = $AggMaps['codepostal_maps'];
                                }
                                $userAddressOnDb->save();
                            } */
                       // }
                    }
                } 
            // if (isset($MapAgg['address'])) {
                 //   $UserAddress->address = $MapAgg['address'];
                 //   }
            //end update or insert maps users
  
                DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return  $request->id;
    }

    /**
     * Check exist username
     *
     * @param Request request
     *
     * @return bool
     */
    public static function checkExistUsername(Request $request, int $idUser, int $idLanguage)
    {
        $configurations = FieldUserRoleBL::checkFunctionalityEnabled($request->idFunctionality, $idUser);
        if (is_null($configurations)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        $result = false;

        if (is_null($request->id)) {
            if (User::where('name', $request->username)->count() > 0) {
                $result = true;
            }
        } else {
            if (User::where('name', $request->username)->whereNotIn('id', [$request->id])->count() > 0) {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Check group user
     *
     * @param Request request
     *
     * @return bool
     */
    public static function checkGroupUser(Request $request, int $idUser, int $idLanguage)
    {
        $result = false;
        if (!is_null($idUser)) {
            foreach (RoleUserBL::getRoleAllIncludedByUser($idUser) as $userRole) {
                if ($userRole->name == 'admin') {
                    foreach (RoleUserBL::getRoleAllIncludedByUser($idUser) as $userRole) {
                        if ($userRole->name == 'admin') {
                            $result = true;
                        }
                    }
                }
                return $result;
            }
        }
    }

    /**
     * Check exist email
     *
     * @param Request request
     *
     * @return bool
     */
    public static function checkExistEmail(Request $request, int $idUser, int $idLanguage)
    {
        $configurations = FieldUserRoleBL::checkFunctionalityEnabled($request->idFunctionality, $idUser);

        if (is_null($configurations)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        $result = false;

        if (is_null($request->id)) {
            if (User::where('email', $request->email)->count() > 0) {
                $result = true;
            }
        } else {
            if (User::where('email', $request->email)->whereNotIn('id', [$request->id])->count() > 0) {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Check if user logged is the same of the user page navigated
     *
     * @param Request request
     *
     * @return DtoCommunityAction
     */
    public static function checkUserLoggedToUserUrl(Request $request, int $idLanguage)
    {
        $dtoReturn = new DtoCommunityAction();
        $dtoReturn->favorites = false;
        $dtoReturn->sameUser = false;
        $dtoReturn->ratingPhoto = false;
        $dtoReturn->rating = 0;

        if(isset($request->userId)){
            if($request->userId > 0){
                if(isset($request->url)){
                    $userdata = UsersDatas::where('link', $request->url)->get()->first();
                    if(isset($userdata)){
                        if($userdata->user_id == $request->userId){
                            $dtoReturn->favorites = false;
                            $dtoReturn->sameUser = true;
                        }
                    }

                    $userCommunityImage = CommunityImage::where('link', $request->url)->get()->first();
                    if(isset($userCommunityImage)){
                        if($userCommunityImage->user_id == $request->userId){
                             $dtoReturn->sameUser = true;
                        }

                        $favoritesCommunityImageOnDB = CommunityFavoriteImage::where('user_id', $request->userId)->where('image_id', $userCommunityImage->id)->get()->first();
                        if(isset($favoritesCommunityImageOnDB)){
                            $dtoReturn->favorites = true;
                        }

                        $rateCommunityImageOnDB = DB::select("SELECT community_actions_types.code
                        FROM community_actions
                        INNER JOIN community_actions_types ON community_actions.action_type_id = community_actions_types.id
                        WHERE 
                        (community_actions_types.code = 'STAR1' 
                        OR community_actions_types.code = 'STAR2' 
                        OR community_actions_types.code = 'STAR3' 
                        OR community_actions_types.code = 'STAR4' 
                        OR community_actions_types.code = 'STAR5') AND community_actions.image_id = " . $userCommunityImage->id . " AND community_actions.user_id = " . $request->userId . " limit 1 ");

                        if(isset($rateCommunityImageOnDB) && !empty($rateCommunityImageOnDB)){
                            switch ($rateCommunityImageOnDB[0]->code) {
                                case "STAR1":
                                    $dtoReturn->ratingPhoto = true;
                                    $dtoReturn->rating = 1;
                                    break;
                                case "STAR2":
                                    $dtoReturn->ratingPhoto = true;
                                    $dtoReturn->rating = 2;
                                    break;
                                case "STAR3":
                                    $dtoReturn->ratingPhoto = true;
                                    $dtoReturn->rating = 3;
                                    break;
                                case "STAR4":
                                    $dtoReturn->ratingPhoto = true;
                                    $dtoReturn->rating = 4;
                                    break;
                                case "STAR5":
                                    $dtoReturn->ratingPhoto = true;
                                    $dtoReturn->rating = 5;
                                    break;
                            }
                        }

                    }
                }
            }
        }

        return $dtoReturn;
    }

    #endregion POST

    #region DELETE

    /**
     * Delete
     *
     * @param int id
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function delete(int $id, int $idFunctionality, int $idUser, $idLanguage)
    {
        $configurations = FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser);

        if (is_null($configurations)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        $user = User::find($id);
        $UsersData = UsersDatas::where('user_id', $user->id)->delete();
        $RowLogs = Logs::where('user_id', $id)->delete();


        if (is_null($user)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $user->delete();
    }

    #endregion DELETE




    /**
     * Delete
     *
     * @param int id
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     */
    public static function deleteRow(int $id, $idLanguage)
    {


        $user = User::find($id);
        $UsersData = UsersDatas::where('user_id', $user->id)->delete();
        $UserRole = RoleUser::where('user_id', $id)->delete();
        $RowLogs = Logs::where('user_id', $id)->delete();
        $GrUserDifferentDestination = GrUserDifferentDestination::where('user_id', $id)->delete();
        $CorrelationPeopleUser = CorrelationPeopleUser::where('id_user', $id)->delete();
        $Subscriber = Subscriber::where('user_id', $id)->delete();


        if (is_null($user)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $user->delete();
    }

    #endregion DELETE

    public static function deleteAddress(int $id, $idLanguage)
    {
        $usersAddresses = UsersAddresses::find($id);

        if (is_null($usersAddresses)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        $usersAddresses->delete();
    }
    

}

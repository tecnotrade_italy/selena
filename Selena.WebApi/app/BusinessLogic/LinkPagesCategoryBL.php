<?php

namespace App\BusinessLogic;

use App\Content;
use App\ContentLanguage;
use App\LinkPageCategory;
use App\LanguagePageCategory;
use App\PageCategory;
use App\PagePageCategory;
use App\PageCategoryFather;
use App\BusinessLogic\ContentBL;
use App\BusinessLogic\LanguageBL;
use App\DtoModel\DtoItem;
use App\DtoModel\DtoUrlList;
use App\DtoModel\DtoUrlListDetail;
use App\DtoModel\DtoPageSitemap;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use Illuminate\Support\Facades\DB;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;
use App\Enums\SettingEnum;

class LinkPagesCategoryBL
{

    public static function getPageCategoryIDByUrl($url){
        $linkPageCategory = LinkPageCategory::where('url', $url)->first();
        if(isset($linkPageCategory)){
            return $linkPageCategory->page_category_id;
        }else
            return 0;
    }


    /**
     * Get Content for link page category
     * 
     * @return DtoPageCategory
     */
    public static function getHtml($url){
        //controllo nella tabella link_page_category se l'url passato è presente
        $linkPageCategory = LinkPageCategory::where('url', $url)->first();
        if(isset($linkPageCategory)){
            //se presente prelevare tutte le informazioni presenti nella tabella pages_pages_category tramite pages_pages_category.page_category_id = link_page_category.page_category_id
            $pagesPagesCategory = PagePageCategory::where('page_category_id', $linkPageCategory->page_category_id)->get();
            if(isset($pagesPagesCategory)){

                $pagesPagesCategory = LanguagePageCategory::where('page_category_id', $linkPageCategory->page_category_id)->get()->first();
            
                $idContent = Content::where('code', 'BlogListCategory')->first();
                $idContentListArticle = Content::where('code', 'BlogListArticle')->first();
                if(isset($idContent)){
                    $strLimit = "";
                    $classColCategory = 'col-md-12';

                    $numberArticleBlogNewsHome = SettingBL::getByCode(SettingEnum::HomeBlogList);

                    if(is_null($numberArticleBlogNewsHome)){
                        $numberArticleBlogNewsHome = 3;
                    }

                    if($url == '/'){
                        $strLimit = " limit " . $numberArticleBlogNewsHome;
                    }
                    
                    $contentLanguage = ContentLanguage::where('content_id', $idContent->id)->first();
                    $contentLanguageListArticle = ContentLanguage::where('content_id', $idContentListArticle->id)->first();
                    
                    $finalHtml = '';
                    $finalHtmlBlogList = '';
                    $newContent = '';
                    $tags = SelenaViewsBL::getTagByType('BlogListArticle');    

                    $isLastCategory = PageCategoryFather::where('page_category_father_id', $linkPageCategory->page_category_id)->first();
                    if(isset($isLastCategory)){
                        foreach (DB::select(
                            "SELECT languages_page_categories.description AS title, languages_page_categories.url_cover_image, link_pages_category.url,pages_category_father.page_category_father_id
                            FROM languages_page_categories
                            INNER JOIN pages_category_father ON pages_category_father.page_category_id = languages_page_categories.page_category_id
                            INNER JOIN link_pages_category ON link_pages_category.page_category_id = pages_category_father.page_category_id
                            INNER JOIN page_categories ON page_categories.id = link_pages_category.page_category_id
                            WHERE page_categories.online = 't' and pages_category_father.page_category_father_id = $linkPageCategory->page_category_id
                            ORDER BY pages_category_father.order ASC"
                        ) as $listBlog){
                            $newContent = $contentLanguage->content;                        
                            
                            foreach ($tags as $fieldsDb){
                                if(isset($fieldsDb->tag)){                                                                 
                                    if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                        if(is_numeric($listBlog->{$cleanTag}) && strpos($listBlog->{$cleanTag},'.')){
                                            $tag = str_replace('.',',',substr($listBlog->{$cleanTag}, 0, -2));
                                        }else{
                                            $tag = $listBlog->{$cleanTag};
                                        }
                                        $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                               
                                    }
                                }
                            }

                            $titlenation = LanguagePageCategory::where("page_category_id", $listBlog->page_category_father_id)->get()->first();
                            if(isset($titlenation)){
                                if (strpos($newContent, '#titlenation#') !== false) {
                                    $newContent = str_replace('#titlenation#', $titlenation->description, $newContent);
                                }
                            }
                            $classColCategory = 'col-md-9';
                            
                            $finalHtml = $finalHtml . $newContent;
                        }
                    } else {
                        //implementazione query per prelievo filtrato delle informazioni a seconda dell'url della categoria nagivata
                        foreach (DB::select(
                            "SELECT languages_pages.*, pages.publish as publish
                            FROM pages 
                            INNER JOIN languages_pages on pages.id = languages_pages.page_id
                            INNER JOIN pages_pages_category on pages.id = pages_pages_category.page_id
                            INNER JOIN link_pages_category on link_pages_category.page_category_id = pages_pages_category.page_category_id     
                            WHERE pages.page_type = 'Blog' AND link_pages_category.url = '" . $url . "' 
                            AND pages.online = true
                            AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() )
                            AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() )
                            AND languages_pages.content IS NOT NULL order by pages.publish DESC " . $strLimit
                        ) as $listBlog){
                            $newContent = $contentLanguageListArticle->content;                        
                            foreach ($tags as $fieldsDb){
                                if(isset($fieldsDb->tag)){                                                                 
                                    if (strpos($contentLanguageListArticle->content, $fieldsDb->tag) !== false) {
                                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                        if(is_numeric($listBlog->{$cleanTag}) && strpos($listBlog->{$cleanTag},'.')){
                                            $tag = str_replace('.',',',substr($listBlog->{$cleanTag}, 0, -2));
                                        }else{
                                            $tag = $listBlog->{$cleanTag};
                                        }
                                        $newContent = str_replace($fieldsDb->tag , $tag,  $newContent);                                                                               
                                    }
                                }
                            }
                            if (strpos($newContent, '#publish#') !== false) {
                                $newContent = str_replace('#publish#', date_format(date_create($listBlog->publish), SettingBL::getByCode(SettingEnum::DateFormat)), $newContent);
                            }
                            
                            $classColCategory = 'col-md-12';
                                $newContent = str_replace('#titlenation#', '', $newContent);
                                $finalHtml = $finalHtml . $newContent;
                        }
                    }


                /* shortcode #list_category_article_page_blog# */
                    $idContent = Content::where('code', 'BlogArticlesCategoryList')->first();
                    $contentLanguageS = ContentLanguage::where('content_id', $idContent->id)->first();
                    $finalHtmlListCategoryArticle = '';
                    //$newContents = '';
                    $tags = SelenaViewsBL::getTagByType('BlogListArticle');  
                    $isLastCategory = PageCategoryFather::where('page_category_father_id', $linkPageCategory->page_category_id)->first();   
                    if(isset($isLastCategory)){
                        foreach (DB::select(
                            "SELECT languages_page_categories.description AS title, languages_page_categories.url_cover_image, link_pages_category.url
                            FROM languages_page_categories
                            INNER JOIN pages_category_father ON pages_category_father.page_category_id = languages_page_categories.page_category_id
                            INNER JOIN link_pages_category ON link_pages_category.page_category_id = pages_category_father.page_category_id
                            INNER JOIN page_categories ON page_categories.id = link_pages_category.page_category_id
                            WHERE page_categories.online = 't' AND pages_category_father.page_category_father_id = $linkPageCategory->page_category_id
                            ORDER BY pages_category_father.order ASC"
                        ) as $listBlog){
                            $newContents= $contentLanguageS->content;                        
                            foreach ($tags as $fieldsDb){
                                if(isset($fieldsDb->tag)){                                                                 
                                    if (strpos($contentLanguageS->content, $fieldsDb->tag) !== false) {
                                        $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                        if(is_numeric($listBlog->{$cleanTag}) && strpos($listBlog->{$cleanTag},'.')){
                                            $tag = str_replace('.',',',substr($listBlog->{$cleanTag}, 0, -2));
                                        }else{
                                            $tag = $listBlog->{$cleanTag};
                                        }
                                          if($fieldsDb->tag == '#publish#'){
                                            $tag = date_format(date_create($listBlog->{$cleanTag}), 'd/m/Y');
                                        }else{
                                            $tag = $listBlog->{$cleanTag};
                                        }

                                        $newContents = str_replace($fieldsDb->tag , $tag,  $newContents); 
                                    }
                                }
                            }
                                $finalHtmlListCategoryArticle = $finalHtmlListCategoryArticle . $newContents;
                        }
                    }
                /* end shortcode #list_category_article_page_blog#*/

                    $dtoPageItemRender = new DtoItem();
  
                    $idContentPageCategory = Content::where('code', 'BlogArticlesPage')->first();
                    if(isset($idContentPageCategory)){
                        $contentLanguagePageCategory = ContentLanguage::where('content_id', $idContentPageCategory->id)->first();
                        $contentPage = $contentLanguagePageCategory->content;
                        $breadCrumbs = '';
                        $breadCrumbs = ContentBL::getBreadCrumbsPages($url);
                        $contentPageNew = str_replace('#list_sub_category_blog#', $finalHtml,  $contentPage);

                        if(isset($isLastCategory)){
                            //implementazione query per prelievo filtrato delle informazioni a seconda dell'url della categoria nagivata
                            foreach (DB::select(
                                "SELECT languages_pages.* 
                                FROM pages 
                                INNER JOIN languages_pages on pages.id = languages_pages.page_id
                                INNER JOIN pages_pages_category on pages.id = pages_pages_category.page_id
                                INNER JOIN link_pages_category on link_pages_category.page_category_id = pages_pages_category.page_category_id     
                                WHERE pages.page_type = 'Blog' AND link_pages_category.url = '" . $url . "' 
                                AND pages.online = true
                                AND ( pages.visible_from IS NULL OR pages.visible_from <= NOW() )
                                AND ( pages.visible_end IS NULL OR pages.visible_end >= NOW() )
                                AND languages_pages.content IS NOT NULL order by pages.publish DESC " . $strLimit
                            ) as $listBlog){
                                $newContentBlogList = $contentLanguage->content;                        
                                
                                foreach ($tags as $fieldsDb){
                                    if(isset($fieldsDb->tag)){                                                                 
                                        if (strpos($contentLanguage->content, $fieldsDb->tag) !== false) {
                                            $cleanTag = str_replace('#', '', $fieldsDb->tag);
                                            if(is_numeric($listBlog->{$cleanTag}) && strpos($listBlog->{$cleanTag},'.')){
                                                $tag = str_replace('.',',',substr($listBlog->{$cleanTag}, 0, -2));
                                            }else{
                                                $tag = $listBlog->{$cleanTag};
                                            }
                                            $newContentBlogList = str_replace($fieldsDb->tag , $tag,  $newContentBlogList);                                                                               
                                        }
                                    }
                                }
                                $finalHtmlBlogList = $finalHtmlBlogList . $newContentBlogList;
                            }
                            $contentPageNew = str_replace('#list_article_blog#', $finalHtmlBlogList,  $contentPageNew);
                        }else{
                            $contentPageNew = str_replace('#list_article_blog#', '',  $contentPageNew);
                        }

                        $contentPageNew = str_replace('#description#', $pagesPagesCategory->description,  $contentPageNew);
                        $contentPageNew = str_replace('#breadcrumbs#', $breadCrumbs,  $contentPageNew);


                        if($finalHtmlListCategoryArticle != ''){
                            $idContentPageCategoryContentHtml = Content::where('code', 'ContentHtmlBlogArticlesCategoryList')->first();
                            if(isset($idContentPageCategoryContentHtml)){
                                $contentHtmlLanguagePageCategory = ContentLanguage::where('content_id', $idContentPageCategoryContentHtml->id)->first();
                                $contentPageHtml = $contentHtmlLanguagePageCategory->content;
                                $contentPageHtml = str_replace('#list_category_article_page_blog#', $finalHtmlListCategoryArticle, $contentPageHtml);
                                $contentPageNew = str_replace('#content_list_category_article_page_blog#', $contentPageHtml, $contentPageNew);
                            }else{
                                $contentPageNew = str_replace('#content_list_category_article_page_blog#', '', $contentPageNew);
                            }
                            $contentPageNew = str_replace('#class_list_category_blog#', $classColCategory, $contentPageNew);

                        }else{
                            $contentPageNew = str_replace('#content_list_category_article_page_blog#', '', $contentPageNew);
                        }
                        
                     
                        $dtoPageItemRender->html = $contentPageNew;
                    }else{
                        $dtoPageItemRender->html = '<div class="container" id="containerPageCategory"><div class="row">'. $finalHtml . '</div></div>';
                    }

                    if(isset($pagesPagesCategory)){
                        $dtoPageItemRender->description = $pagesPagesCategory->description;
                        $dtoPageItemRender->seo_title = $pagesPagesCategory->seo_title;
                        $dtoPageItemRender->seo_description = $pagesPagesCategory->seo_description;
                        $dtoPageItemRender->seo_keyword = $pagesPagesCategory->seo_keyword;
                        $dtoPageItemRender->share_title = $pagesPagesCategory->share_title;
                        $dtoPageItemRender->share_description = $pagesPagesCategory->share_description;
                        $dtoPageItemRender->url_cover_image = $pagesPagesCategory->url_cover_image;
                    }else{
                        $dtoPageItemRender->description = '';
                        $dtoPageItemRender->seo_title = '';
                        $dtoPageItemRender->seo_description = '';
                        $dtoPageItemRender->seo_keyword = '';
                        $dtoPageItemRender->share_title = '';
                        $dtoPageItemRender->share_description = '';
                        $dtoPageItemRender->url_cover_image = '';
                    }
                    
                    return $dtoPageItemRender;

                }else{
                    return '';
                }
            }else{
                return '';
            }
        }else{
            return '';
        }
    }



    /**
     * Get url list
     *
     * @param int idLanguage
     * @return DtoUrlList
     */
    public static function getUrlSitemap(int $idLanguage)
    {
        $dtoUrlLinks = collect();
        $idDefaultLanguage = 1;

        foreach (LinkPageCategory::all() as $page) {
            $pageCategories = PageCategory::where('id', $page->page_category_id)->where('online', true)->get()->first();
            if(isset($pageCategories)){
                $languageFound = false;

                $dtoUrlList = new DtoUrlList();
                $dtoUrlList->idPage = $page->id;

                foreach (LanguagePageCategory::where('page_category_id', $page->page_category_id)->get() as $languageItem) {
                    if ($languageItem->language_id == $idLanguage || ($languageItem->language_id == $idDefaultLanguage && !$languageFound)) {
                        $dtoUrlList->title = $languageItem->description;
                        $languageFound = true;
                    }

                    $dtoUrlLinkDetail = new DtoUrlListDetail();
                    $dtoUrlLinkDetail->id = $languageItem->id;
                    $dtoUrlLinkDetail->idLanguage = $languageItem->language_id;
                    $dtoUrlLinkDetail->url = $page->url;
                    $dtoUrlList->urlListDetail->push($dtoUrlLinkDetail);
                }

                $dtoUrlLinks->push($dtoUrlList);
            }
        }
        return $dtoUrlLinks;
    }

    public static function getPages(string $page_type, int $idLanguage)
    {

        $result = collect();
        foreach (DB::select(
            "SELECT languages_page_categories.description, languages_page_categories.updated_at, link_pages_category.url
            FROM link_pages_category
            INNER JOIN languages_page_categories ON languages_page_categories.page_category_id = link_pages_category.page_category_id
            INNER JOIN page_categories ON page_categories.id = link_pages_category.page_category_id
            AND languages_page_categories.language_id = " . $idLanguage . " and page_categories.online = true ORDER BY languages_page_categories.updated_at desc"
        ) as $PagesAll) {
            $dtoPageSitemap = new DtoPageSitemap();
            $dtoPageSitemap->url = $PagesAll->url;
            $dtoPageSitemap->title = $PagesAll->description;
            $dtoPageSitemap->updated_at = $PagesAll->updated_at;

            $result->push($dtoPageSitemap);
        }
        return $result;
    }
}

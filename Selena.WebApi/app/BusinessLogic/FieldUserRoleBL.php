<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoConfiguration;
use App\DtoModel\DtoPageConfiguration;
use App\DtoModel\DtoTabConfiguration;
use App\Enums\InputTypesEnum;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;

class FieldUserRoleBL
{
    #region PRIVATE

    /**
     * Return DtoConfiguration from dataRow
     *
     * @param stdClass rows
     * @return DtoConfiguration
     */
    private static function _getDtoConfiguration($rows)
    {
        foreach ($rows as $row) {
            if (isset($row->inputType)) {
                $row->inputType = InputTypesEnum::getKey($row->inputType);
            }
        }

        $arrayRows = ArrayHelper::toCollection($rows);

        $dtoConfigurations = new DtoConfiguration();

        // altrimenti spacco i vecchi metodi
        if ($arrayRows->groupBy('idtab')->count() == 1) {
            $dtoConfigurations->tablesConfigurations = static::_getDtoPageConfiguration($arrayRows->whereNotIn('tableorder', '')->sortBy('tableorder'), true);
        } else {
            foreach ($arrayRows->whereNotIn('idtab', '')->where('tableorder', '!=', NULL)->pluck('idtab')->unique()->flatten() as $idTab) {
                $dtoTabConfiguration = new DtoTabConfiguration;
                $dtoTabConfiguration->code = $arrayRows->where('idtab', $idTab)->pluck('tabcode')->first();
                $dtoTabConfiguration->label = $arrayRows->where('idtab', $idTab)->pluck('tabdescription')->first();
                $dtoTabConfiguration->tablesConfigurations = static::_getDtoPageConfiguration($arrayRows->where('idtab', $idTab)->where('tableorder', '!=', NULL)->sortBy('tableorder'), true);
                $dtoConfigurations->tablesConfigurations->push($dtoTabConfiguration);
            }
        }

        foreach ($arrayRows->whereNotIn('idtab', null)->where('posx', '!=', NULL)->where('posy', '!=', NULL)->pluck('idtab')->unique()->flatten()->sortBy('tableorder') as $idTab) {
            $dtoTabConfiguration = new DtoTabConfiguration;
            $dtoTabConfiguration->code = $arrayRows->where('idtab', $idTab)->pluck('tabcode')->first();
            $dtoTabConfiguration->label = $arrayRows->where('idtab', $idTab)->pluck('tabdescription')->first();
            $dtoTabConfiguration->pageConfigurations = static::_getDtoPageConfiguration($arrayRows->where('idtab', $idTab)->where('posx', '!=', NULL)->where('posy', '!=', NULL), false);
            $dtoConfigurations->tabsConfigurations->push($dtoTabConfiguration);
        }

        return $dtoConfigurations;
    }

    /**
     * Return DtoPageConfiguration from dataRow
     *
     * @param stdClass rows
     * @return collect(DtoPageConfiguration)
     */
    private static function _getDtoPageConfiguration($rows, $isTable)
    {
        $dtoPagesConfigurations = collect();

        foreach ($rows as $row) {

            $dtoPageConfiguration = new DtoPageConfiguration();
            $dtoPageConfiguration->visibleOnTable = $isTable;

            if (isset($row->label)) {
                $dtoPageConfiguration->label = $row->label;
                $dtoPageConfiguration->title = $row->label;
            }

            if (isset($row->field)) {
                $dtoPageConfiguration->field = $row->field;
            }

            if (isset($row->formatter)) {
                $dtoPageConfiguration->formatter = $row->formatter;
            }

            if (isset($row->required)) {
                $dtoPageConfiguration->required = $row->required;
            }

            if (isset($row->defaultvalue)) {
                $dtoPageConfiguration->defaultValue = $row->defaultvalue;
            }

            if (isset($row->arrayRadioButton)) {
                $dtoPageConfiguration->arrayRadioButton = $row->arrayRadioButton;
            }

            if (isset($row->service)) {
                $dtoPageConfiguration->service = $row->service;
            }

            if (isset($row->maxlenght)) {
                $dtoPageConfiguration->maxLenght = $row->maxlenght;
            }

            if (isset($row->inputtype)) {
                $dtoPageConfiguration->inputType = InputTypesEnum::getKey($row->inputtype);
                if ($row->inputtype != InputTypesEnum::TableCheckbox && $row->inputtype != InputTypesEnum::CommandFormatter) {
                    $dtoPageConfiguration->filterType = InputTypesEnum::getKey(InputTypesEnum::Input);
                }
            }

            if (isset($row->posy)) {
                $dtoPageConfiguration->posY = $row->posy;
            }

            if (isset($row->posx)) {
                $dtoPageConfiguration->posX = $row->posx;
            }

            if (isset($row->tableorder)) {
                $dtoPageConfiguration->tableOrder = $row->tableorder;
            }

            if (isset($row->class)) {
                $dtoPageConfiguration->class = $row->class;
            }

            if (isset($row->attribute)) {
                $dtoPageConfiguration->attribute = $row->attribute;
            }

            if (isset($row->colspan)) {
                $dtoPageConfiguration->colspan = $row->colspan;
            }

            if (isset($row->idtab)) {
                $dtoPageConfiguration->idtab = $row->idtab;
            }

            if (isset($row->tabcode)) {
                $dtoPageConfiguration->tabCode = $row->tabcode;
            }

            if (isset($row->tabdescription)) {
                $dtoPageConfiguration->tabDescription = $row->tabdescription;
            }

            if (isset($row->onchange)) {
                $dtoPageConfiguration->onChange = $row->onchange;
                $dtoPageConfiguration->onSave = $row->onchange;
            }

            if (isset($row->onkeyup)) {
                $dtoPageConfiguration->onKeyUp = $row->onkeyup;
                $dtoPageConfiguration->onSave = $row->onkeyup;
            }

            $dtoPagesConfigurations->push($dtoPageConfiguration);
        }

        return $dtoPagesConfigurations;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Return configuration
     *
     * @param int $idFunctionality
     * @param int $idUser
     * @param int $idLanguage
     * @return DtoConfiguration
     */
    public static function getConfiguration($idFunctionality, $idUser, $idLanguage)
    {

        $idRole = DB::select('SELECT CASE
                                WHEN (	SELECT 1
                                        FROM menu_admins_users_roles MAUR
                                        INNER JOIN menu_admins MNA ON MAUR.menu_admin_id = MNA.id
                                        INNER JOIN functionalities FUN ON MNA.functionality_id = FUN.id
                                        INNER JOIN tabs TAB ON FUN.id = TAB.functionality_id
                                        INNER JOIN fields FCN ON TAB.id = FCN.tab_id
                                        INNER JOIN fields_users_roles FCUR ON FCN.id = FCUR.field_id
                                        WHERE FUN.id = ' . $idFunctionality . '
                                        AND FCUR.user_id = ' . $idUser . '
                                        LIMIT 1) IS NOT NULL THEN 0
                                ELSE (	SELECT FCUR.role_id
                                        FROM menu_admins_users_roles MAUR
                                        INNER JOIN menu_admins MNA ON MAUR.menu_admin_id = MNA.id
                                        INNER JOIN functionalities FUN ON MNA.functionality_id = FUN.id
                                        INNER JOIN tabs TAB ON FUN.id = TAB.functionality_id
                                        INNER JOIN fields FCN ON TAB.id = FCN.tab_id
                                        INNER JOIN fields_users_roles FCUR ON FCN.id = FCUR.field_id
                                        WHERE FUN.id = ' . $idFunctionality . '
                                        AND FCUR.role_id IN ( SELECT role_id FROM role_user WHERE user_id = ' . $idUser . ' )
                                        LIMIT 1)
                                END AS role')[0]->role;

        if (is_null($idRole)) {
            // No configuration found for this user
            return;
        } else {
            $query = "  SELECT 	COALESCE(FCUR.description, FCD.description) AS label, FLD.field AS field,
                                COALESCE(FCUR.required, FLD.required) AS required,
                                COALESCE(FCUR.default_value, FLD.default_value) AS defaultValue, FLD.service AS service,
                                FLD.max_length AS maxLenght, FCUR.input_type AS inputType, FCUR.pos_y AS posY, FCUR.pos_x AS posX,
                                FCUR.table_order AS tableOrder, FCUR.class AS class, FCUR.attribute AS attribute,
                                FCUR.colspan AS colspan, TAB.id AS idTab, TAB.code AS tabCode, LTB.description AS tabDescription,
                                FLD.on_change AS onChange, FLD.on_keyup AS onKeyUp, FLD.formatter, FLD.\"arrayRadioButton\"
                        FROM fields_users_roles FCUR
                        INNER JOIN fields FLD ON FCUR.field_id = FLD.id
                        INNER JOIN tabs TAB ON FLD.tab_id = TAB.id
                        INNER JOIN languages_tabs LTB ON TAB.id = LTB.tab_id
                        INNER JOIN functionalities FUN ON TAB.functionality_id = FUN.id
                        INNER JOIN menu_admins MNA ON FUN.id = MNA.functionality_id
                        INNER JOIN menu_admins_users_roles MAUR ON MNA.id = MAUR.menu_admin_id AND (MAUR.user_id = FCUR.user_id OR MAUR.role_id = FCUR.role_id)
                        INNER JOIN fields_languages FCD ON FLD.id = FCD.field_id
                        WHERE FUN.id = " . $idFunctionality . "
                        AND FCUR.enabled = true
                        AND ( FCD.language_id = ( SELECT LAN.id FROM languages LAN WHERE LAN.default = true ) )
                        AND ( LTB.language_id = ( SELECT LAN.id FROM languages LAN WHERE LAN.default = true ) )";

            if ($idRole == 0) {
                // User enabled
                $query = $query . ' AND FCUR.user_id = ' . $idUser;
            } else {
                // Role enabled
                $query = $query . ' AND FCUR.role_id = ' . $idRole;
            }

            $query = $query . ' ORDER BY TAB.order, FCUR.pos_y, FCUR.pos_x';

            $rows = DB::select($query);

            return static::_getDtoConfiguration($rows);
        }
    }

    /**
     * Check functionality enabled
     * 
     * @param int $idFunctionality
     * @param int $idUser
     * 
     * @return bool
     */
    public static function checkFunctionalityEnabled(int $idFunctionality, int $idUser)
    {
        $idRole = DB::select('  SELECT CASE
                                WHEN (	SELECT 1
                                        FROM menu_admins_users_roles MAUR
                                        INNER JOIN menu_admins MNA ON MAUR.menu_admin_id = MNA.id
                                        INNER JOIN functionalities FUN ON MNA.functionality_id = FUN.id
                                        WHERE FUN.id = ' . $idFunctionality . '
                                        AND MAUR.user_id = ' . $idUser . '
                                        LIMIT 1) IS NOT NULL THEN 0
                                ELSE (	SELECT MAUR.role_id
                                        FROM menu_admins_users_roles MAUR
                                        INNER JOIN menu_admins MNA ON MAUR.menu_admin_id = MNA.id
                                        INNER JOIN functionalities FUN ON MNA.functionality_id = FUN.id
                                        WHERE FUN.id = ' . $idFunctionality . '
                                        AND MAUR.role_id IN ( SELECT role_id FROM role_user WHERE user_id = ' . $idUser . ' )
                                        LIMIT 1)
                                END AS role')[0]->role;

        if (is_null($idRole)) {
            return false;
        } else {
            return true;
        }
    }

    #endregion GET
}

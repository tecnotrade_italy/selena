<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoLanguagePageSetting;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\LanguagePage;
use Mockery\CountValidator\Exception;
use App\DtoModel\DtoUrlList;
use App\DtoModel\DtoUrlListDetail;
use Intervention\Image\Facades\Image;
use App\Helpers\LogHelper;
use App\Page;
use Illuminate\Http\Request;

class LanguagePageBL
{

        public static $dirImage = '../storage/app/public/images/';

    #region PRIVATE

    /**
     * Converto dto to model
     *
     * @param $dtoLanguagePageSetting
     * @param int idPage
     * @return LanguageMenu
     */
    private static function _convertFromDtoBlogNews($dtoLanguagePageSetting, int $idPage)
    {
        $languagePage = new LanguagePage();

        if (isset($dtoLanguagePageSetting['idLanguage'])) {
            $languagePage->language_id = $dtoLanguagePageSetting['idLanguage'];
        }

        $languagePage->page_id = $idPage;

        if (isset($dtoLanguagePageSetting['title'])) {
            $languagePage->title = $dtoLanguagePageSetting['title'];
        }

        if (isset($dtoLanguagePageSetting['content'])) {
            $languagePage->content = str_replace('</label>&nbsp;', '</label>', str_replace('</label> &nbsp;', '</label>', str_replace('&nbsp;&nbsp;', '', $dtoLanguagePageSetting['content'])));
            //$languagePage->content = $dtoLanguagePageSetting['content'];
        }
        
        if (isset($dtoLanguagePageSetting['url'])) {
            $languagePage->url = $dtoLanguagePageSetting['url'];
        }
      
        if (isset($dtoLanguagePageSetting['urlCanonical'])) {
            $languagePage->urlCanonical = $dtoLanguagePageSetting['urlCanonical'];
        }

        if (isset($dtoLanguagePageSetting['hideHeader'])) {
            $languagePage->hide_header = $dtoLanguagePageSetting['hideHeader'];
        } else {
            $languagePage->hide_header = false;
        }

        if (isset($dtoLanguagePageSetting['hideFooter'])) {
            $languagePage->hide_footer = $dtoLanguagePageSetting['hideFooter'];
        } else {
            $languagePage->hide_footer = false;
        }

        if (isset($dtoLanguagePageSetting['hideBreadcrumb'])) {
            $languagePage->hide_breadcrumb = $dtoLanguagePageSetting['hideBreadcrumb'];
        } else {
            $languagePage->hide_breadcrumb = false;
        }

        if (isset($dtoLanguagePageSetting['seoTitle'])) {
            $languagePage->seo_title = $dtoLanguagePageSetting['seoTitle'];
        }

        if (isset($dtoLanguagePageSetting['seoDescription'])) {
            $languagePage->seo_description = $dtoLanguagePageSetting['seoDescription'];
        }

        if (isset($dtoLanguagePageSetting['SeoKeyword'])) {
            $languagePage->seo_keyword = $dtoLanguagePageSetting['SeoKeyword'];
        }

        if (isset($dtoLanguagePageSetting['shareTitle'])) {
            $languagePage->share_title = $dtoLanguagePageSetting['shareTitle'];
        }

        if (isset($dtoLanguagePageSetting['shareDescription'])) {
            $languagePage->share_description = $dtoLanguagePageSetting['shareDescription'];
        }

        if  (is_null($dtoLanguagePageSetting['imgName'])){
             $languagePage->url_cover_image = $dtoLanguagePageSetting['urlCoverImage'];
        }else{
            Image::make($dtoLanguagePageSetting['urlCoverImage'])->save(static::$dirImage . $dtoLanguagePageSetting['imgName']);
            $languagePage->url_cover_image = static::getUrlImage() . $dtoLanguagePageSetting['imgName'];
        }

        if  (is_null($dtoLanguagePageSetting['imgNameShare'])){
            $languagePage->url_share_image = $dtoLanguagePageSetting['urlShareImage'];
        }else{
           Image::make($dtoLanguagePageSetting['urlShareImage'])->save(static::$dirImage . $dtoLanguagePageSetting['imgNameShare']);
           $languagePage->url_share_image = static::getUrlImage() . $dtoLanguagePageSetting['imgNameShare'];
        }
     
        return $languagePage;
    }

        private static function getUrlImage()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/';
    }


    /**
     * Converto dto to model
     *
     * @param DtoLanguagePageSetting $dtoLanguagePageSetting
     * @param int idPage
     * @return LanguageMenu
     */
    private static function _convertFromDto(array $dtoLanguagePageSetting, int $idPage)
    {
        $languagePage = new LanguagePage();

        if (isset($dtoLanguagePageSetting['idLanguage'])) {
            $languagePage->language_id = $dtoLanguagePageSetting['idLanguage'];
        }

        $languagePage->page_id = $idPage;

        if (isset($dtoLanguagePageSetting['title'])) {
            $languagePage->title = $dtoLanguagePageSetting['title'];
        }

        if (isset($dtoLanguagePageSetting['content'])) {
            $languagePage->content = $dtoLanguagePageSetting['content'];
        }

        if (isset($dtoLanguagePageSetting['url'])) {
            $languagePage->url = $dtoLanguagePageSetting['url'];
        }

        if (isset($dtoLanguagePageSetting['urlCanonical'])) {
            $languagePage->urlCanonical = $dtoLanguagePageSetting['urlCanonical'];
        }

        if (isset($dtoLanguagePageSetting['hideHeader'])) {
            $languagePage->hide_header = $dtoLanguagePageSetting['hideHeader'];
        } else {
            $languagePage->hide_header = false;
        }

        if (isset($dtoLanguagePageSetting['hideFooter'])) {
            $languagePage->hide_footer = $dtoLanguagePageSetting['hideFooter'];
        } else {
            $languagePage->hide_footer = false;
        }

        if (isset($dtoLanguagePageSetting['hideBreadcrumb'])) {
            $languagePage->hide_breadcrumb = $dtoLanguagePageSetting['hideBreadcrumb'];
        } else {
            $languagePage->hide_breadcrumb = false;
        }

        if (isset($dtoLanguagePageSetting['seoTitle'])) {
            $languagePage->seo_title = $dtoLanguagePageSetting['seoTitle'];
        }

        if (isset($dtoLanguagePageSetting['seoDescription'])) {
            $languagePage->seo_description = $dtoLanguagePageSetting['seoDescription'];
        }

        if (isset($dtoLanguagePageSetting['seoKeyword'])) {
            $languagePage->seo_keyword = $dtoLanguagePageSetting['seoKeyword'];
        }

        if (isset($dtoLanguagePageSetting['shareTitle'])) {
            $languagePage->share_title = $dtoLanguagePageSetting['shareTitle'];
        }

        if (isset($dtoLanguagePageSetting['shareDescription'])) {
            $languagePage->share_description = $dtoLanguagePageSetting['shareDescription'];
        }

        if (isset($dtoLanguagePageSetting['urlCoverImage'])) {
            $languagePage->url_cover_image = $dtoLanguagePageSetting['urlCoverImage'];
        }

        if (isset($dtoLanguagePageSetting['urlShareImage'])) {
            $languagePage->url_share_image = $dtoLanguagePageSetting['urlShareImage'];
        }
        
        return $languagePage;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Check exist url
     *
     * @param Request request
     * @return bool
     */
    public static function checkExistUrl(Request $request)
    {
        if (is_null($request->idPage) && is_null($request->idNewsletter)) {
            return LanguagePage::where('url', $request->url)->join('pages', 'pages.id', '=', 'languages_pages.page_id')->where('pages.page_type', '<>', 'Link')->count() + NewsletterBL::checkExistUrl($request->url);
        } else {
            if (is_null($request->idPage)) {
                return LanguagePage::where('url', $request->url)->join('pages', 'pages.id', '=', 'languages_pages.page_id')->where('pages.page_type', '<>', 'Link')->count() + NewsletterBL::checkExistUrl($request->url, $request->idNewsletter);
            } else {
                if (is_null($request->idNewsletter)) {
                    return LanguagePage::where('url', $request->url)->whereNotIn('page_id', [$request->idPage])->count() + NewsletterBL::checkExistUrl($request->url);
                }
            }
        }
    }

    #endregion GET

    #region INSERT

    /**
     * Clone by id Page
     * 
     * @param int idPage
     * @param int newIdPage
     * 
     * @return LanguagePage
     */
    public static function cloneByIdPage(int $idPage, int $newIdPage)
    {
        foreach (LanguagePage::where('page_id', $idPage)->get() as $languagePage) {
            $newLanguagePage = new LanguagePage();
            $newLanguagePage->language_id = $languagePage->language_id;
            $newLanguagePage->page_id = $newIdPage;
            $newLanguagePage->title = $languagePage->title . ' - Copy';
            $newLanguagePage->content = $languagePage->content;
            $newLanguagePage->url = $languagePage->url . '-Copy';
            $newLanguagePage->urlCanonical = $languagePage->urlCanonical . '-Copy';
            $newLanguagePage->hide_header = $languagePage->hide_header;
            $newLanguagePage->hide_footer = $languagePage->hide_footer;
            $newLanguagePage->hide_breadcrumb = $languagePage->hide_breadcrumb;
            $newLanguagePage->seo_title = $languagePage->seo_title;
            $newLanguagePage->seo_description = $languagePage->seo_description;
            $newLanguagePage->seo_keyword = $languagePage->seo_keyword;
            $newLanguagePage->share_title = $languagePage->share_title;
            $newLanguagePage->share_description = $languagePage->share_description;
            $newLanguagePage->url_cover_image = $languagePage->url_cover_image;
            $newLanguagePage->save();
        }
    }

    #endregion INSERT

    #region INSERT OR UPDATE

    /**
     * Insert or update dto
     *
     * @param string $dtoLanguagePageSetting
     * @param int $idPage
     * @param int $idLanguage
     * @return int
     */
    public static function insertOrUpdateDtoNewsBlog(array $dtoLanguagePageSetting, int $idPage, int $idLanguage)
    {
        $pageOnDB = Page::find($idPage);
        $languagePage = LanguagePage::where(['language_id' => $dtoLanguagePageSetting['idLanguage'], 'page_id' => $idPage]);
        $count = $languagePage->count();

        if ($count > 1) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::MoreLanguagePageForSameLanguagePageFound), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if ($count) {
                $languagePage = $languagePage->first();
                if ($pageOnDB->page_type == 'Blog' || $pageOnDB->page_type == 'News' || $pageOnDB->page_type == 'Page') {
                    DBHelper::updateAndSaveBlogNews(static::_convertFromDtoBlogNews($dtoLanguagePageSetting, $idPage), $languagePage);
                } else {
                    DBHelper::updateAndSave(static::_convertFromDto($dtoLanguagePageSetting, $idPage), $languagePage);
                }
            } else {
                $languagePage = static::_convertFromDto($dtoLanguagePageSetting, $idPage);
                $languagePage->save();
            }
        }

        return $languagePage->id;
    }

    /**
     * Insert or update dto
     *
     * @param DtoLanguagePageSetting $dtoLanguagePageSetting
     * @param int $idPage
     * @param int $idLanguage
     * @return int
     */
    public static function insertOrUpdateDto(DtoLanguagePageSetting $dtoLanguagePageSetting, int $idPage, int $idLanguage)
    {
        $languagePage = LanguagePage::where(['language_id' => $dtoLanguagePageSetting->idLanguage, 'page_id' => $idPage]);
        $count = $languagePage->count();

        if ($count > 1) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::MoreLanguagePageForSameLanguagePageFound), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if ($count) {
                $languagePage = $languagePage->first();
                DBHelper::updateAndSave(static::_convertFromDto($dtoLanguagePageSetting, $idPage), $languagePage);
            } else {
                $languagePage = static::_convertFromDto($dtoLanguagePageSetting, $idPage);
                $languagePage->save();
            }
        }

        return $languagePage->id;
    }

    /**
     * Insert or update url
     *
     * @param DtoUrlListDetail $dtoUrlListDetail
     * @param int $idPage
     * @param int $idLanguage
     *
     * @return id
     */
    public static function insertOrUpdateUrl(DtoUrlListDetail $dtoUrlListDetail, int $idPage, int $idLanguage)
    {
        $languagePage = LanguagePage::where(['language_id' => $dtoUrlListDetail->idLanguage, 'page_id' => $idPage]);
        $count = $languagePage->count();

        if ($count > 1) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::MoreLanguagePageForSameLanguagePageFound), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if ($count) {
                $languagePage = $languagePage->first();
                $languagePage->url = $dtoUrlListDetail->url;
                $languagePage->save();
            } else {
                $languagePage = new LanguagePage();
                $languagePage->language_id = $dtoUrlListDetail->idLanguage;
                $languagePage->page_id = $idPage;
                $languagePage->url = $dtoUrlListDetail->url;
                $languagePage->save();
            }
        }

        return $languagePage->id;
    }

    #endregion INSERT OR UPDATE

    #region DELETE

    /**
     * Delete by idPage
     *
     * @param int $idPage
     */
    public static function deleteByIdPage(int $idPage)
    {
        LanguagePage::where('page_id', $idPage)->delete();
    }

    /**
     * Delete by id
     *
     * @param int $id
     */
    public static function deleteById(int $id)
    {
        LanguagePage::find($id)->delete();
    }

    #endregion DELETE
}

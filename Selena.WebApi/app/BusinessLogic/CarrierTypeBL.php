<?php

namespace App\BusinessLogic;

use App\CarrierLanguage;
use App\Carrier;
use App\CarrierArea;
use App\Cart;
use App\DtoModel\DtoSelect2;
use App\DtoModel\DtoCarrier;
use App\DtoModel\DtoCarrierArea;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Mockery\Exception;
use App\Helpers\LogHelper;

class CarrierTypeBL
{
    public static function selectCarrierType(string $search)
    {
        if ($search == "*") {
            return CarrierLanguage::select('carrier_id as id', 'description as description')->get();
        } else {
            return CarrierLanguage::where('description', 'ilike', $search . '%')->select('carrier_id as id', 'description as description')->get();
        }
    }

}

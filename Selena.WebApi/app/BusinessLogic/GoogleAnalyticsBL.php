<?php

namespace App\BusinessLogic;

use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use Mockery\Exception;
use Analytics;
use Spatie\Analytics\Period;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\BusinessLogic\SettingBL;
use Illuminate\Http\Request;
use App\DtoModel\DtoChartJS;
use App\DtoModel\DtoChartJSDatasets;
use App\Enums\GoogleAnalyticsEnum;
use App\Enums\SettingEnum;
use App\Helpers\LogHelper;
use Google_Service_Exception;
use Spatie\Analytics\Analytics as AnalyticsAnalytics;

class GoogleAnalyticsBL
{
    #region PROTECTED

    /**
     * Get view id
     * 
     * @param int idLanguage
     */
    protected static function _GetActiveAccount(int $idLanguage)
    {
        $viewIDActive = SettingBL::getByCode(SettingEnum::GoogleAnalyticsViewId);

        $return = '';
        if (empty($viewIDActive)) {
            $return = '';
            //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ViewNotSet), HttpResultsCodesEnum::InvalidPayload);
        }else{
            $return = $viewIDActive;
        }

        return $return;
    }

    /**
     * Get minute and second
     * 
     * @param seconds
     */
    protected static function _SecondMinute($seconds)
    {
        $minutes = floor($seconds / 60);
        $secondsleft = $seconds % 60;

        if ($minutes < 10) {
            $minutes = "0" . $minutes;
        }

        if ($secondsleft < 10) {
            $secondsleft = "0" . $secondsleft;
        }

        return "$minutes:$secondsleft";
    }

    /**
     * Create period
     * 
     * @param Date startDate
     * @param Date endDate
     */
    protected static function _CreatePeriod($startDate, $endDate)
    {
        if (empty($startDate)) {
            $startDate = Carbon::now()->subDays(30);
        } else {
            $startDate = Carbon::parse($startDate);
        }

        if (empty($endDate)) {
            $endDate = Carbon::now();
        } else {
            $endDate = Carbon::parse($endDate);
        }

        return $period = Period::create($startDate, $endDate);
    }

    /**
     * Get google analytics
     * 
     * @param GoogleAnalyticsEnum googleAnalyticsEnum
     * @param int idLanguage
     * @param Request request
     */
    protected static function _Get($googleAnalyticsEnum, int $idLanguage, Request $request = null)
    {
        $viewId = self::_GetActiveAccount($idLanguage);
        if(empty($viewId)){
            return '';
        }else{

            sleep(1);
            Analytics::setViewId(self::_GetActiveAccount($idLanguage));

            if (isset($request)) {
                $period = self::_CreatePeriod($request->start, $request->end);
            }

            switch ($googleAnalyticsEnum) {
                case GoogleAnalyticsEnum::AvgSessionDuration:
                    return static::_SecondMinute(Analytics::performQuery($period, 'ga:avgSessionDuration')->totalsForAllResults['ga:avgSessionDuration']);

                case GoogleAnalyticsEnum::BounceRate:
                    return number_format((float) Analytics::performQuery($period, 'ga:bounceRate')->totalsForAllResults['ga:bounceRate'], 2);

                case GoogleAnalyticsEnum::DeviceSession:
                    #region DEVICE SESSION

                    $analyticsData = Analytics::performQuery($period, 'ga:sessions', ["dimensions" => "ga:deviceCategory"]);

                    $labels = [];

                    $resultAnalytic = [];

                    foreach ($analyticsData as $data) {

                        $labels[] = $data['0'];

                        $resultAnalytic[] = (int) $data['1'];
                    }

                    return static::_convertToChartJS(GoogleAnalyticsEnum::DeviceSession, $labels, $resultAnalytic, $idLanguage);

                    #endregion DEVICE SESSION

                case GoogleAnalyticsEnum::MostVisitedPages:
                    #region MOST VISITED PAGES

                    if (is_null($request->topType)) {
                        $topType = 10;
                    } else {
                        $topType = $request->topType;
                    }

                    $analyticsData = Analytics::performQuery($period, 'ga:pageviews', ["dimensions" => "ga:pagePath", "sort" => "-ga:pageviews","max-results" => $topType]);
                    $list = [];
                    foreach ($analyticsData as $data) {
                        $list[] = ['url' => $data[0], 'pageview' => $data[1]];
                    }
                    return $list;

                    #endregion MOST VISITED PAGES

                case GoogleAnalyticsEnum::PageViewPerSession:
                    return number_format((float) Analytics::performQuery($period, 'ga:pageviewsPerSession')->totalsForAllResults['ga:pageviewsPerSession'], 2);

                case GoogleAnalyticsEnum::PagesViews:
                    #region PAGES VIEWS

                    //$analyticsData = Analytics::fetchVisitorsAndPageViews($period);
                    $analyticsData = Analytics::performQuery($period, 'ga:pageviews', ["dimensions" => "ga:date"]);

                    $labels = [];

                    $resultAnalytic = [];

                    foreach ($analyticsData as $data) {

                        $labels[] = Carbon::createFromFormat('Ymd',$data[0])->format('d-m');

                        $resultAnalytic[] = (int) $data[1];
                    }

                    return static::_convertToChartJS(GoogleAnalyticsEnum::PagesViews, $labels, $resultAnalytic, $idLanguage);

                    #endregion PAGES VIEWS

                case GoogleAnalyticsEnum::SourceSession:
                    #region SOURCE SESSION

                    $analyticsData = Analytics::performQuery($period, 'ga:sessions', ["dimensions" => "ga:channelGrouping"]);

                    $labels = [];

                    $resultAnalytic = [];

                    foreach ($analyticsData as $data) {

                        $labels[] = $data['0'];

                        $resultAnalytic[] = (int) $data['1'];
                    }

                    return static::_convertToChartJS(GoogleAnalyticsEnum::SourceSession, $labels, $resultAnalytic, $idLanguage);

                    #endregion SOURCE SESSION
                
                case GoogleAnalyticsEnum::ReturningVisitors:
                    #region RETURNING VISITOR

                    $analyticsData = Analytics::performQuery($period, 'ga:users', ["dimensions" => "ga:userType"]);

                    $labels = [];

                    $resultAnalytic = [];

                    foreach ($analyticsData as $data) {

                        $labels[] = $data['0'];

                        $resultAnalytic[] = (int) $data['1'];
                    }

                    return static::_convertToChartJS(GoogleAnalyticsEnum::ReturningVisitors, $labels, $resultAnalytic, $idLanguage);

                    #endregion RETURNING VISITOR

                case GoogleAnalyticsEnum::TotalPageView:
                    return Analytics::performQuery($period, 'ga:pageviews')->totalsForAllResults['ga:pageviews'];

                case GoogleAnalyticsEnum::TotalSession:
                    return Analytics::performQuery($period, 'ga:sessions')->totalsForAllResults['ga:sessions'];

                case GoogleAnalyticsEnum::TotalUser:
                    return Analytics::performQuery($period, 'ga:users')->totalsForAllResults['ga:users'];

                case GoogleAnalyticsEnum::UserActiveRealTime:
                    return Analytics::getAnalyticsService()->data_realtime->get('ga:' . self::_GetActiveAccount($idLanguage), 'rt:activeVisitors')->totalsForAllResults['rt:activeVisitors'];

                case GoogleAnalyticsEnum::VisitorsViews:

                    #region VISITORS VIEWS

                    $analyticsData = Analytics::performQuery($period, 'ga:sessions', ["dimensions" => "ga:date"]);

                    $labels = [];

                    $resultAnalytic = [];

                    foreach ($analyticsData as $data) {
                        $labels[] = Carbon::createFromFormat('Ymd',$data[0])->format('d-m');

                        $resultAnalytic[] = (int) $data[1];
                    }

                    return static::_convertToChartJS(GoogleAnalyticsEnum::VisitorsViews, $labels, $resultAnalytic, $idLanguage);

                    #endregion VISITORS VIEWS
            }
        }
    }

    #endregion PROTECTED

    #region PRIVATE

    /**
     * Check if exceeded user rate limit
     * 
     * @param Google_Service_Exception gse
     */
    private static function _hasExceededUserRateLimit(Google_Service_Exception $gse)
    {
        return $gse->getCode() == 403 && $gse->getErrors()[0]['message'] == 'Quota Error: User Rate Limit Exceeded.';
    }

    /**
     * Convert to chartJS
     * 
     * @param GoogleAnalyticsEnum googleAnalyticsEnum
     * @param array labels
     * @param array data
     * @param int idLanguage
     * 
     * @return DtoChartJS
     */
    private static function _convertToChartJS($googleAnalyticsEnum, $labels, $data, int $idLanguage)
    {
        $result = new DtoChartJS();
        $result->labels = $labels;

        $datasets = new DtoChartJSDatasets();

        switch ($googleAnalyticsEnum) {
            case GoogleAnalyticsEnum::DeviceSession;
                #region DEVICE SESSION

                $datasets->backgroundColor = SettingBL::getByCode(SettingEnum::BackgroundColorChartDeviceSession);

                if (is_null($datasets->backgroundColor)) {
                    sleep(1);
                    //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::BackGroundDeviceSessionEmpty), HttpResultsCodesEnum::InvalidPayload);
                }

                #endregion DEVICE SESSION
                break;

            case GoogleAnalyticsEnum::PagesViews;
                #region PAGES VIEWS

                $datasets->backgroundColor = SettingBL::getByCode(SettingEnum::BackgroundColorChartPagesViews);

                if (is_null($datasets->backgroundColor)) {
                    sleep(1);
                    //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::BackGroundPagesViewsEmpty), HttpResultsCodesEnum::InvalidPayload);
                }

                $datasets->borderColor  = SettingBL::getByCode(SettingEnum::BorderColorChartPagesViews);


                if (is_null($datasets->borderColor)) {
                    sleep(1);
                    //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::BorderPagesViewsEmpty), HttpResultsCodesEnum::InvalidPayload);
                }

                #endregion PAGES VIEWS
                break;

            case GoogleAnalyticsEnum::SourceSession:
                #region SOURCE SESSION

                $datasets->backgroundColor = SettingBL::getByCode(SettingEnum::BackgroundColorChartSourceSession);

                if (is_null($datasets->backgroundColor)) {
                    sleep(1);
                    //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::BackGroundSourceSessionEmpty), HttpResultsCodesEnum::InvalidPayload);
                }

                #endregion SOURCE SESSION
                break;
            
            case GoogleAnalyticsEnum::ReturningVisitors:
                    #region SOURCE SESSION
    
                    $datasets->backgroundColor = SettingBL::getByCode(SettingEnum::BackgroundColorChartReturningVisitors);
    
                    if (is_null($datasets->backgroundColor)) {
                        sleep(1);
                        //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::BackGroundSourceSessionEmpty), HttpResultsCodesEnum::InvalidPayload);
                    }
    
                    #endregion SOURCE SESSION
                    break;

            case GoogleAnalyticsEnum::VisitorsViews;
                #region VISITORS VIEWS

                $datasets->backgroundColor = SettingBL::getByCode(SettingEnum::BackgroundColorChartVisitorsViews);

                if (is_null($datasets->backgroundColor)) {
                    sleep(1);
                    //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::BackGroundVisitorsViewsEmpty), HttpResultsCodesEnum::InvalidPayload);
                }

                $datasets->borderColor  = SettingBL::getByCode(SettingEnum::BorderColorChartVisitorsViews);


                if (is_null($datasets->borderColor)) {
                    sleep(1);
                    //throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::BorderVisitorsViewsEmpty), HttpResultsCodesEnum::InvalidPayload);
                }

                #endregion VISITORS VIEWS
                break;
        }

        $datasets->data = $data;

        $result->datasets = ($datasets);

        return $result;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get user active real time
     */
    public static function getUserActiveRealTime(int $idLanguage)
    {
        try {
            return static::_Get(GoogleAnalyticsEnum::UserActiveRealTime, $idLanguage);
        } catch (\Google_Service_Exception $gse) {
            if (static::_hasExceededUserRateLimit($gse)) {
                return static::_Get(GoogleAnalyticsEnum::UserActiveRealTime, $idLanguage);
            } else {
                throw $gse;
            }
        }
    }

    /**
     * Get total users
     * 
     * @param Request request
     * @param int idLanguage
     */
    public static function getTotalUsers(Request $request, int $idLanguage)
    {
        try {
            return static::_Get(GoogleAnalyticsEnum::TotalUser, $idLanguage, $request);
        } catch (\Google_Service_Exception $gse) {
            if (static::_hasExceededUserRateLimit($gse)) {
                return static::_Get(GoogleAnalyticsEnum::TotalUser, $idLanguage, $request);
            } else {
                throw $gse;
            }
        }
    }

    /**
     * Get total page view
     * 
     * @param Request request
     * @param int idLanguage
     */
    public static function getTotalPageView(Request $request, int $idLanguage)
    {
        try {
            return static::_Get(GoogleAnalyticsEnum::TotalPageView, $idLanguage, $request);
        } catch (\Google_Service_Exception $gse) {
            if (static::_hasExceededUserRateLimit($gse)) {
                return static::_Get(GoogleAnalyticsEnum::TotalPageView, $idLanguage, $request);
            } else {
                throw $gse;
            }
        }
    }

    /**
     * Get total session
     * 
     * @param Request request
     * @param int idLanguage
     */
    public static function getTotalSession(Request $request, int $idLanguage)
    {
        try {
            return static::_Get(GoogleAnalyticsEnum::TotalSession, $idLanguage, $request);
        } catch (\Google_Service_Exception $gse) {
            if (static::_hasExceededUserRateLimit($gse)) {
                return static::_Get(GoogleAnalyticsEnum::TotalSession, $idLanguage, $request);
            } else {
                throw $gse;
            }
        }
    }

    /**
     * Get page view per session
     * 
     * @param Request request
     * @param int idLanguage
     */
    public static function getPageViewPerSession(Request $request, int $idLanguage)
    {
        try {
            return static::_Get(GoogleAnalyticsEnum::PageViewPerSession, $idLanguage, $request);
        } catch (\Google_Service_Exception $gse) {
            if (static::_hasExceededUserRateLimit($gse)) {
                return static::_Get(GoogleAnalyticsEnum::PageViewPerSession, $idLanguage, $request);
            } else {
                throw $gse;
            }
        }
    }

    /**
     * Get avg session duration
     * 
     * @param Request request
     * @param int idLanguage
     */
    public static function getAvgSessionDuration(Request $request, int $idLanguage)
    {
        try {
            return static::_Get(GoogleAnalyticsEnum::AvgSessionDuration, $idLanguage, $request);
        } catch (\Google_Service_Exception $gse) {
            if (static::_hasExceededUserRateLimit($gse)) {
                return static::_Get(GoogleAnalyticsEnum::AvgSessionDuration, $idLanguage, $request);
            } else {
                throw $gse;
            }
        }
    }

    /**
     * Get bounce rate
     * 
     * @param Request request
     * @param int idLanguage
     */
    public static function getBounceRate(Request $request, int $idLanguage)
    {
        try {
            return static::_Get(GoogleAnalyticsEnum::BounceRate, $idLanguage, $request);
        } catch (\Google_Service_Exception $gse) {
            if (static::_hasExceededUserRateLimit($gse)) {
                return static::_Get(GoogleAnalyticsEnum::AvgSessionDuration, $idLanguage, $request);
            } else {
                throw $gse;
            }
        }
    }

    /**
     * Get visitors views
     * 
     * @param Request request
     * @param int idLanguage
     * 
     * @return DtoChartJS
     */
    public static function getVisitorsViews(Request $request, int $idLanguage)
    {
        try {
            return static::_Get(GoogleAnalyticsEnum::VisitorsViews, $idLanguage, $request);
        } catch (\Google_Service_Exception $gse) {
            if (static::_hasExceededUserRateLimit($gse)) {
                return static::_Get(GoogleAnalyticsEnum::VisitorsViews, $idLanguage, $request);
            } else {
                throw $gse;
            }
        }
    }

    /**
     * Get pages views
     * 
     * @param Request request
     * @param int idLanguage
     * 
     * @return DtoChartJS
     */
    public static function getPagesViews(Request $request, int $idLanguage)
    {
        try {
            return static::_Get(GoogleAnalyticsEnum::PagesViews, $idLanguage, $request);
        } catch (\Google_Service_Exception $gse) {
            if (static::_hasExceededUserRateLimit($gse)) {
                return static::_Get(GoogleAnalyticsEnum::PagesViews, $idLanguage, $request);
            } else {
                throw $gse;
            }
        }
    }

    /**
     * Get most visited pages
     * 
     * @param Request request
     * @param int idLanguage
     * 
     * @return DtoChartJS
     */
    public static function getMostVisitedPages(Request $request, int $idLanguage)
    {
        try {
            return static::_Get(GoogleAnalyticsEnum::MostVisitedPages, $idLanguage, $request);
        } catch (\Google_Service_Exception $gse) {
            if (static::_hasExceededUserRateLimit($gse)) {
                return static::_Get(GoogleAnalyticsEnum::MostVisitedPages, $idLanguage, $request);
            } else {
                throw $gse;
            }
        }
    }

    /**
     * Get device session
     * 
     * @param Request request
     * @param int idLanguage
     * 
     * @return DtoChartJS
     */
    public static function getDeviceSession(Request $request, int $idLanguage)
    {
        try {
            return static::_Get(GoogleAnalyticsEnum::DeviceSession, $idLanguage, $request);
        } catch (\Google_Service_Exception $gse) {
            if (static::_hasExceededUserRateLimit($gse)) {
                return static::_Get(GoogleAnalyticsEnum::DeviceSession, $idLanguage, $request);
            } else {
                throw $gse;
            }
        }
    }

    /**
     * Get source session
     * 
     * @param Request request
     * @param int idLanguage
     * 
     * @return DtoChartJS
     */
    public static function getSourceSession(Request $request, int $idLanguage)
    {
        try {
            return static::_Get(GoogleAnalyticsEnum::SourceSession, $idLanguage, $request);
        } catch (\Google_Service_Exception $gse) {
            if (static::_hasExceededUserRateLimit($gse)) {
                return static::_Get(GoogleAnalyticsEnum::SourceSession, $idLanguage, $request);
            } else {
                throw $gse;
            }
        }
    }

    /**
     * Get returning visitors
     * 
     * @param Request request
     * @param int idLanguage
     * 
     * @return DtoChartJS
     */
    public static function getReturningVisitors(Request $request, int $idLanguage)
    {
        try {
            return static::_Get(GoogleAnalyticsEnum::ReturningVisitors, $idLanguage, $request);
        } catch (\Google_Service_Exception $gse) {
            if (static::_hasExceededUserRateLimit($gse)) {
                return static::_Get(GoogleAnalyticsEnum::ReturningVisitors, $idLanguage, $request);
            } else {
                throw $gse;
            }
        }
    }

    #endregion GET

    #region UPDATE

    /**
     * Store json key
     * 
     * @param Request request
     */
    public static function storeJsonKey(Request $request)
    {
        Storage::disk('local')->put('analytics/service-account-credentials.json', json_encode($request->all(), JSON_PRETTY_PRINT));
    }

    #endregion UPDATE
}

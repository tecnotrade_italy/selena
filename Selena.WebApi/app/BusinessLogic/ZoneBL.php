<?php

namespace App\BusinessLogic;

use App\Cities;
use App\DtoModel\DtoZone;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\LanguageNation;
use App\LanguageProvince;
use App\Mail\ContactRequestMail;
use App\Nation;
use App\PostalCode;
use App\Region;
use App\User;
use App\Zones;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ZoneBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Zones::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->id_cities)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->id_postalcodes)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SurnameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($request->id_provinces)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SurnameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
         if (is_null($request->id_regions)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SurnameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($request->id_nations)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SurnameNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    /**
     * Conver to model
     * 
     * @param Request request
     * 
     * @return Zones
     */
    private static function _convertToModel(Request $request)
    {
        $zones = new Zones();

        if (isset($request->id)) {
            $zones->id = $request->id;
        }
        if (isset($request->id_cities)) {
            $zones->id_cities = $request->id_cities;
        }
        if (isset($request->id_postalcodes)) {
            $zones->id_postalcodes = $request->id_postalcodes;
        }
        if (isset($request->id_provinces)) {
            $zones->id_provinces = $request->id_provinces;
        }
        if (isset($request->id_regions)) {
            $zones->id_regions = $request->id_regions;
        }
        if (isset($request->id_nations)) {
            $zones->id_nations = $request->id_nations;
        }

        

        return $zones;
    }

    /**
     * Convert to dto
     * 
     * @param Zones region
     * 
     * @return DtoZone
     */
    private static function _convertToDto(Zones $zones)
    {
        $DtoZone = new DtoZone();
        $DtoZone->id = $zones->id;
        $DtoZone->id_cities = $zones->id_cities;
        $DtoZone->id_postalcodes = $zones->id_postalcodes;
        $DtoZone->id_provinces = $zones->id_provinces;
        $DtoZone->id_regions = $zones->id_regions;
        $DtoZone->id_nations = $zones->id_nations;
        return $DtoZone;
    }
    #endregion PRIVATE

    #region GET
    /**
     * Get
     *
     * @param String $search
     * @return Zones
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return Zones::select('id', 'description')->get();
        } else {
            return Zones::where('description', 'ilike','%'. $search . '%')->select('id', 'description')->get();
        }
    }

    /**
     * Get all
     * 
     * @return DtoZones
     */
    public static function getAll()
    {
        $result = collect();
        foreach (Zones::orderBy('id')->get() as $zones) {
       
            $DtoZone = new DtoZone();
            $DtoZone->id = $zones->id;
            $DtoZone->id_cities = $zones->id_cities;
            $DtoZone->id_postalcodes = $zones->id_postalcodes;
            $DtoZone->id_provinces = $zones->id_provinces;
            $DtoZone->id_regions = $zones->id_regions;
            $DtoZone->id_nations = $zones->id_nations;

            $DtoZone->IdCities = Cities::where('id', $zones->id_cities)->first()->description;
            $DtoZone->IdPostalcodes =PostalCode::where('id', $zones->id_postalcodes)->first()->code;
            $DtoZone->IdProvinces = LanguageProvince::where('province_id', $zones->id_provinces)->first()->description;
            $DtoZone->IdRegions = Region::where ('id', $zones->id_regions)->first()->description;
            $DtoZone->IdNations = LanguageNation::where('nation_id',$zones->id_nations)->first()->description;
            $result->push($DtoZone); 
        }
        return $result;
    } 

    /**
     * Get by id 
     * 
     * @param int id
     * 
     * @return DtoCities
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Zones::find($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);
        DB::beginTransaction();

        try {
            $zones = new Zones();
            $zones->id_cities = $request->id_cities;
            $zones->id_postalcodes = $request->id_postalcodes;
            $zones->id_provinces = $request->id_provinces;
            $zones->id_regions = $request->id_regions;
            $zones->id_nations = $request->id_nations;
            $zones->save();

   
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $zones->id;
    }
    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoZone
     * @param int idLanguage
     */
    public static function update(Request $DtoZone, int $idLanguage)
    {
        static::_validateData($DtoZone, $idLanguage, DbOperationsTypesEnum::UPDATE);
        
        $zonesOnDb = Zones::find($DtoZone->id);
        DB::beginTransaction();

        try {
            $zonesOnDb->id_cities = $DtoZone->id_cities;
            $zonesOnDb->id_postalcodes = $DtoZone->id_postalcodes;
            $zonesOnDb->id_provinces = $DtoZone->id_provinces;
            $zonesOnDb->id_regions = $DtoZone->id_regions;
            $zonesOnDb->id_nations = $DtoZone->id_nations;
            $zonesOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
    #endregion UPDATE


    #region DELETE
    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {

        $zones = Zones::find($id);

        if (is_null($zones)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $zones->delete();
    }
    #endregion DELETE






    
}

<?php

namespace App\BusinessLogic;

use App\BusinessName;
use App\Content;
use App\Tipology;
use App\ContentLanguage;
use App\DtoModel\DtoInterventions;
use App\DtoModel\DtoItemGroupTechnicalSpecification;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use App\Customer;
use App\DtoModel\DtoExternalRepair;
use App\DtoModel\DtoInterventionsPhotos;
use App\DtoModel\DtoInterventionsProcessing;
use App\DtoModel\DtoItemGr;
use App\DtoModel\DtoModuleValidationGuarantee;
use App\DtoModel\DtoPeople;
use App\DtoModel\DtoQuotes;
use App\DtoModel\DtoQuotesMessages;
use App\DtoModel\DtoUser;
use App\ExternalRepair;
use App\Item;
use App\GrStates;
use App\GrIntervention;
use App\GrInterventionPhoto;
use App\GrInterventionProcessing;
use App\GrQuotes;
use App\Helpers\HttpHelper;
use App\ItemGr;
use App\ItemLanguage;
use App\ModuleValidationGuarantee;
use App\PaymentGr;
use App\People;
use App\QuotesGrIntervention;
use App\SelenaSqlViews;
use App\Setting;
use App\StatesQuotesGr;
use App\User;
use App\UsersDatas;
use App\Fast;
use App\GrQuotesEmailSend;
use App\PeopleUser;
use App\QuotesMessages;
use App\Role;
use App\RolesPeople;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Http\Request;


class InterventionBL
{


    public static $dirImageddt = '../storage/app/public/images/DdtReceivedfromCustomer/';
    public static $dirImageFaultModule = '../storage/app/public/images/FaultModule/';
    public static $dirImage = '../storage/app/public/images/Items/';
    public static $dirImageIntervention = '../storage/app/public/images/Intervention/';
    private static $mimeTypeAllowed = ['jpeg', 'jpg', 'png', 'gif', 'pdf'];
    public static $dirImageGuasti = '../storage/app/public/images/Intervention/Foto3/GUASTI/';
    public static $dirImageInterventionReportTest = '../storage/app/public/images/Intervention/Foto3/REPORT TEST/';
    

    private static function getUrlOpenFileImg()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"];
    }
    

    #region PRIVATE
    /**
     * Validate data
     * 
     * @param Request ItemsBL
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoInterventions, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoInterventions->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
            // if (is_null($DtoInterventions->idLanguage)) {
            //   throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            //}


            if (GrIntervention::find($DtoInterventions->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        // if (is_null($DtoInterventions->idIntervention)) {
        //      throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        //  }
        // if (is_null($DtoInterventions->descriptionCustomer)) {
        //     throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        //    }

        /*if (is_null($DtoInterventions->code_product)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }*/

        //  if (is_null($DtoInterventions->customer_id)) {
        //     throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        // }

        /*if (is_null($DtoInterventions->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoryTipeIdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoInterventions->defect)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoryTipeIdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoInterventions->nr_ddt)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoryTipeIdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoInterventions->date_ddt)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoryTipeIdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoInterventions->referent)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoryTipeIdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoInterventions->plant_type)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoryTipeIdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoInterventions->trolley_type)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoryTipeIdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        if (is_null($DtoInterventions->series)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoryTipeIdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoInterventions->voltage)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoryTipeIdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }
        
        if (is_null($DtoInterventions->exit_notes)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoryTipeIdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null($DtoInterventions->states_id)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoryTipeIdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }*/

        if (!is_null($DtoInterventions->imgName)) {
            if (!in_array(\explode('/', \explode(':', substr($DtoInterventions->imgmodule, 0, strpos($DtoInterventions->imgmodule, ';')))[1])[1], static::$mimeTypeAllowed)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FileTypeNotAllowed), HttpResultsCodesEnum::InvalidPayload);
            }
        }
         if (!is_null($DtoInterventions->imgNames)) {
            if (!in_array(\explode('/', \explode(':', substr($DtoInterventions->fileddt, 0, strpos($DtoInterventions->fileddt, ';')))[1])[1], static::$mimeTypeAllowed)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FileTypeNotAllowed), HttpResultsCodesEnum::InvalidPayload);
            }
        }
        //  if ($dbOperationType != DbOperationsTypesEnum::UPDATE) {
        //  if (strpos($DtoInterventions->imgmodule, '/storage/app/public/images/Items/') === false) {
        //       if (!in_array(\explode('/', \explode(':', substr($DtoInterventions->imgmodule, 0, strpos($DtoInterventions->imgmodule, ';')))[1])[1], static::$mimeTypeAllowed)) {
        //      throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FileTypeNotAllowed), HttpResultsCodesEnum::InvalidPayload);
        //      }
        //    }
        //    }  
    }

    private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/Items/';
    }

    
        private static function getUrlModule()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/FaultModule/';
    }
         private static function getUrlDdt()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/DdtReceivedfromCustomer/';
    } 

    /**
     * Convert to dto
     * 
     * @param Intervention postalCode
     * 
     * @return DtoInterventions
     */
    private static function _convertToDto($Intervention)
    {

        $DtoInterventions = new DtoInterventions();

        if (isset($Intervention->id)) {
            $DtoInterventions->id = $Intervention->id;
        }
        if (isset($Intervention->code_intervention_gr)) {
            $DtoInterventions->code_intervention_gr = $Intervention->code_intervention_gr;
        }

        if (isset($Intervention->customers_id)) {
            $DtoInterventions->customer_id = $Intervention->customers_id;
        }

        if (isset($Intervention->code_product)) {
            $DtoInterventions->code_product = $Intervention->code_product;
        }
         if (isset($Intervention->rip_association)) {
            $DtoInterventions->rip_association = $Intervention->rip_association;
        }
        
        if (isset($Intervention->items_id)) {
            $DtoInterventions->items_id = $Intervention->items_id;
            $DtoInterventions->descriptionItem = ItemGr::where('id', $Intervention->items_id)->first()->code_gr;
        }
          if (isset($Intervention->items_id)) {
            $DtoInterventions->name_product = $Intervention->items_id;
            $DtoInterventions->descriptionNameProduct = ItemGr::where('id', $Intervention->items_id)->first()->name_product;
        }

        if (isset($Intervention->user_id)) {
            $DtoInterventions->user_id = $Intervention->user_id;
            $userdatas = UsersDatas::where('user_id', $Intervention->user_id)->first();
            if (isset($userdatas->business_name)){
                $DtoInterventions->descriptionUser = $userdatas->business_name;
            }else{
                $DtoInterventions->descriptionUser =  $userdatas->name . ' ' . $userdatas->surname; 
            }
        }

       /* if (isset($Intervention->user_id)) {
            $DtoInterventions->user_id = $Intervention->user_id;
            $DtoInterventions->descriptionUser = UsersDatas::where('user_id', $Intervention->user_id)->first()->business_name;
        }*/

        if (isset($Intervention->description)) {
            $DtoInterventions->description = $Intervention->description;
        }
         if (isset($Intervention->note_internal_gr)) {
            $DtoInterventions->note_internal_gr = $Intervention->note_internal_gr;
        }
        
        if (isset($Intervention->defect)) {
            $DtoInterventions->defect = $Intervention->defect;
        }
        if (isset($Intervention->nr_ddt)) {
            $DtoInterventions->nr_ddt = $Intervention->nr_ddt;
        }

        if (isset($Intervention->nr_ddt_gr)) {
            $DtoInterventions->nr_ddt_gr = $Intervention->nr_ddt_gr;
        }
        if (isset($Intervention->date_ddt_gr)) {
            $DtoInterventions->date_ddt_gr = $Intervention->date_ddt_gr;
        }

        if (isset($Intervention->date_ddt)) {
            $DtoInterventions->date_ddt = $Intervention->date_ddt;
        }
      /*  if (isset($Intervention->imgmodule)) {
            $DtoInterventions->imgName = $Intervention->imgmodule;
        }*/

        if (isset($Intervention->unrepairable)) {
            $DtoInterventions->unrepairable = $Intervention->unrepairable;
        }

        if (isset($Intervention->send_to_customer)) {
            $DtoInterventions->send_to_customer = $Intervention->send_to_customer;
        }
        if (isset($Intervention->code_tracking)) {
            $DtoInterventions->code_tracking = $Intervention->code_tracking;
        }
        
        if (isset($Intervention->create_quote)) {
            $DtoInterventions->create_quote = $Intervention->create_quote;
        }
        if (isset($Intervention->referent)) {
            $DtoInterventions->referent = $Intervention->referent;
            $DtoInterventions->descriptionPeople = People::where('id', $Intervention->referent)->first()->name;
        }

        if (isset($Intervention->external_referent_id)) {
              $DtoInterventions->external_referent_id_intervention = $Intervention->external_referent_id;
              $DtoInterventions->descriptionPeopleReferentExternal = People::where('id', $Intervention->external_referent_id)->first()->name;
        }   
  
        if (isset($Intervention->trolley_type)) {
            $DtoInterventions->trolley_type = $Intervention->trolley_type;
        }
         if (isset($Intervention->plant_type)) {
            $DtoInterventions->plant_type = $Intervention->plant_type;
        }
        if (isset($Intervention->series)) {
            $DtoInterventions->series = $Intervention->series;
        }
        if (isset($Intervention->voltage)) {
            $DtoInterventions->voltage = $Intervention->voltage;
        }
        if (isset($Intervention->exit_notes)) {
            $DtoInterventions->exit_notes = $Intervention->exit_notes;
        }

        if (isset($Intervention->states_id)) {
            $DtoInterventions->states_id = $Intervention->states_id;
            $DtoInterventions->descriptionStates = GrStates::where('id', $Intervention->states_id)->first()->description;
        }

        if (file_exists(static::$dirImageGuasti . $Intervention->code_intervention_gr . '.pdf' )) {
            $DtoInterventions->imgName = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/GUASTI/' . $Intervention->code_intervention_gr . '.pdf';
          } else {
            if ($Intervention->imgmodule != '' && !is_null($Intervention->imgmodule)) {
                        $DtoInterventions->imgName = $Intervention->imgmodule;
                    } else {
                        $DtoInterventions->imgName = '';
                    }
          }

      foreach (GrInterventionPhoto::where('intervention_id', $Intervention->id)->get() as $InterventionCaptures) {
            $DtoInterventionsPhotos = new DtoInterventionsPhotos();   
              if (file_exists(static::$dirImageIntervention . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.jpg' )) {
                  $DtoInterventionsPhotos->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.jpg';
              } else {
                if (file_exists(static::$dirImageIntervention . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.JPG' )) {
                  $DtoInterventionsPhotos->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.JPG';
                }else{
                    if ($InterventionCaptures->img != '' && !is_null($InterventionCaptures->img)) {
                      $pos = strpos($InterventionCaptures->img, $InterventionCaptures->intervention_id);
                      if($pos === false){
                        $DtoInterventionsPhotos->img = '';
                      }else{
                        $DtoInterventionsPhotos->img = $InterventionCaptures->img;
                      }
                  } else {
                      $DtoInterventionsPhotos->img = '';
                  }
                }  
            }
              if (file_exists(static::$dirImageInterventionReportTest . $InterventionCaptures->code_intervention_gr . '.pdf' )) {
                $DtoInterventionsPhotos->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/REPORT TEST/' . $InterventionCaptures->code_intervention_gr . '.pdf';
            } else {
                if ($InterventionCaptures->img_rep_test != '' && !is_null($InterventionCaptures->img_rep_test)) {
                        $DtoInterventionsPhotos->img_rep_test = $InterventionCaptures->img_rep_test;
                    } else {
                        $DtoInterventionsPhotos->img_rep_test = '';
                    }
            }      
            $DtoInterventions->captures->push($DtoInterventionsPhotos);
        }


       /* foreach (GrInterventionPhoto::where('intervention_id', $Intervention->id)->get() as $InterventionCaptures) {
            $DtoInterventionsPhotos = new DtoInterventionsPhotos();
         if (isset($InterventionCaptures->img)) {
                $DtoInterventionsPhotos->img = $InterventionCaptures->img;
            }
            $DtoInterventions->captures->push($DtoInterventionsPhotos);
        }*/

        return  $DtoInterventions;
    }
    /**
     * Convert to VatType
     * 
     * @param DtoInterventions
     * 
     * @return Intervention
     */
    private static function _convertToModel($DtoInterventions)
    {
        $Intervention = new GrIntervention();

        if (isset($DtoInterventions->id)) {
            $Intervention->id = $DtoInterventions->id;
        }

        if (isset($DtoInterventions->customer_id)) {
            $Intervention->customers_id = $DtoInterventions->customer_id;
        }

        if (isset($DtoInterventions->code_product)) {
            $Intervention->code_product = $DtoInterventions->code_product;
        }

        if (isset($DtoInterventions->description)) {
            $Intervention->description = $DtoInterventions->description;
        }

        if (isset($DtoInterventions->defect)) {
            $Intervention->defect = $DtoInterventions->defect;
        }

        if (isset($DtoInterventions->nr_ddt)) {
            $Intervention->nr_ddt = $DtoInterventions->nr_ddt;
        }

        if (isset($DtoInterventions->date_ddt)) {
            $Intervention->date_ddt = $DtoInterventions->date_ddt;
        }

        if (isset($DtoInterventions->imgmodule)) {
            $Intervention->imgmodule = $DtoInterventions->imgmodule;
        }

        if (isset($DtoInterventions->unrepairable)) {
            $Intervention->unrepairable = $DtoInterventions->unrepairable;
        }
        if (isset($DtoInterventions->send_to_customer)) {
            $Intervention->send_to_customer = $DtoInterventions->send_to_customer;
        }

        if (isset($DtoInterventions->create_quote)) {
            $Intervention->create_quote = $DtoInterventions->create_quote;
        }
        if (isset($DtoInterventions->referent)) {
            $Intervention->referent = $DtoInterventions->referent;
        }

        if (isset($DtoInterventions->trolley_type)) {
            $Intervention->trolley_type = $DtoInterventions->trolley_type;
        }
         if (isset($DtoInterventions->plant_type)) {
            $Intervention->plant_type = $DtoInterventions->plant_type;
        }
        if (isset($DtoInterventions->series)) {
            $Intervention->series = $DtoInterventions->series;
        }
        if (isset($DtoInterventions->voltage)) {
            $Intervention->voltage = $DtoInterventions->voltage;
        }

        if (isset($DtoInterventions->exit_notes)) {
            $Intervention->exit_notes = $DtoInterventions->exit_notes;
        }
        if (isset($DtoInterventions->states_id)) {
            $Intervention->states_id = $DtoInterventions->states_id;
        }

        return $Intervention;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get
     *
     * @param int id
     *
     * @return Intervention
     */
    public static function get(int $id)
    {
        return GrIntervention::find($id);
    }

    /**
     * Get all dto
     *
     * @param int id
     * @param int idLanguage
     *
     * @return DtoItem
     */
    public static function getAllDto(int $idLanguage)
    {
        $result = collect();

        foreach (Item::orderBy('internal_code')->get() as $item) {
            $dtoItem = new DtoItem();
            $dtoItem->id = $item->id;
            $dtoItem->internalCode = $item->internal_code;
            $dtoItem->description = ItemLanguageBL::getByItemAndLanguage($item->id, $idLanguage);
            $result->push($dtoItem);
        }

        return $result;
    }

    /**
     * Get dto
     *
     * @param int id
     * @param int idLanguage
     *
     * @return DtoItem
     */
    public static function getDto(int $id, int $idLanguage)
    {
        $dtoItem = new DtoItem();
        $dtoItem->id = $id;
        $item = Item::find($id);

        if (!is_null($item)) {
            $dtoItem->internalCode = $item->internal_code;
            $dtoItem->description = ItemLanguageBL::getByItemAndLanguage($id, $idLanguage);
        }

        return $dtoItem;
    }

   /**
     * Get by getBySelectReferent
     * 
     * @param int id
     * @return DtoInterventions
     */
    public static function getBySelectReferent(int $id)
    {
        $Intervention = GrIntervention::where('id', $id)->first();
        $idInterventionExternalRepair = ExternalRepair::where('id_intervention', $id)->first();
        $DtoInterventions = new DtoInterventions();

        if (isset($Intervention->id)) {
        $DtoInterventions->id = $Intervention->id;
        }

        if (isset($Intervention->referent)) {
        $DtoInterventions->id_people = $Intervention->referent;
        $DtoInterventions->Description_People = People::where('id', $Intervention->referent)->first()->name;
        }
         if (isset($Intervention->rip_association)) {
        $DtoInterventions->rip_association = $Intervention->rip_association;
        }
        
        return $DtoInterventions;
    }



/**
     * getDifferentDestination
     *
     * @param int $id
     * @return User
     */
    public static function getByInterventionFU(Int $id){

        $GrIntervention = GrIntervention::where('id', $id)->first();
        $FastU=Fast::where('object', $id)->first();

        $DtoInterventions = new DtoInterventions();

        if (isset($GrIntervention->id)) {
            $DtoInterventions->id = $GrIntervention->id;
        }else {
            $DtoInterventions->id = '';     
        }
       
        if (isset($GrIntervention->user_id)) {
            $DtoInterventions->user_id = $GrIntervention->user_id;
            $DtoInterventions->description_user_id = User::where('id', $GrIntervention->user_id)->first()->name;
        }else {
            $DtoInterventions->user_id = '';     
        }
            if (isset($GrIntervention->referent)) {
            $DtoInterventions->referent = People::where('id', $GrIntervention->referent)->first()->name;
        }else {
            $DtoInterventions->referent = '';     
        }

        if (isset($FastU->date)) {
            $DtoInterventions->date = $FastU->date;
        }else {
            $DtoInterventions->date = '';     
        }
       
         if (isset($FastU->description)) {
            $DtoInterventions->descriptionfast = $FastU->description;
        }else {
            $DtoInterventions->descriptionfast = '';     
        }
        return  $DtoInterventions;
    }

/**
     * updateUser
     *
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     *
     * @return int id
     */
    public static function updateInterventionFU(Request $request)
    {
        $Fast = Fast::where('object', $request->object)->first();
        DB::beginTransaction();
        try {

              if (!isset($Fast->object)) {
                $RowFast = new Fast();
                $RowFast->user_id = $request->user_id; 
                $RowFast->date = $request->date;
                $RowFast->object = $request->object;
                $RowFast->description = $request->description;
              
                $RowFast->save();
            } else {
                if (isset($Fast->object)) {
                $Fast->user_id = $request->user_id;
                $Fast->object = $request->object;
                $Fast->date = $request->date;
                $Fast->description = $request->description;
                $Fast->save();
                }
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return  $request->id;
    }


    /**
     * Get by id
     * 
     * @param int id
     * @return DtoInterventions
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(GrIntervention::find($id));
    }

    /**
     * Get by getByViewExternalRepair
     * 
     * @param int id
     * @return DtoInterventions
     */
    public static function getByViewExternalRepair(int $id)
    {
        $idInterventionExternalRepair = ExternalRepair::where('id_intervention', $id)->first();

        $Intervention = GrIntervention::where('id', $id)->first();

        $DtoExternalRepair = new DtoExternalRepair();

        $DtoExternalRepair->id_intervention = $id;

        if (isset($idInterventionExternalRepair->id)) {
            $DtoExternalRepair->id = $idInterventionExternalRepair->id;
        }else {
                $DtoExternalRepair->id = '';
            }
        if (isset($idInterventionExternalRepair->id_business_name_supplier)) {
            $DtoExternalRepair->id_business_name_supplier =  $idInterventionExternalRepair->id_business_name_supplier;
            $DtoExternalRepair->Description_business_name_supplier = BusinessName::where('id', $idInterventionExternalRepair->id_business_name_supplier)->first()->business_name;
        }else {
                $DtoExternalRepair->Description_business_name_supplier = '';
            }

        if (isset($Intervention->description)) {
            $DtoExternalRepair->description = $Intervention->description;
        }else {
                $DtoExternalRepair->description = '';
            }
            if (isset($Intervention->code_intervention_gr)) {
                $DtoExternalRepair->codice_intervento = $Intervention->code_intervention_gr;
            }else {
                $DtoExternalRepair->codice_intervento = '';
                }

                if (isset($Intervention->id_librone_rip_esterna)) {
                    $DtoExternalRepair->id_librone_rip_esterna = $Intervention->id_librone_rip_esterna;
                }else {
                    $DtoExternalRepair->id_librone_rip_esterna = '';
                }

        if (isset($Intervention->items_id)) {
            $DtoExternalRepair->code_gr = ItemGr::where('id', $Intervention->items_id)->first()->code_gr;
        }else {
                $DtoExternalRepair->code_gr = '';
            }

        if (isset($idInterventionExternalRepair->date)) {
            $DtoExternalRepair->date = $idInterventionExternalRepair->date;
        }else {
                $DtoExternalRepair->date = '';
            }
        if (isset($idInterventionExternalRepair->ddt_exit)) {
            $DtoExternalRepair->ddt_exit = $idInterventionExternalRepair->ddt_exit;
        }else {
            $DtoExternalRepair->ddt_exit = '';
            }

        if (isset($idInterventionExternalRepair->price)) {
            $DtoExternalRepair->price = $idInterventionExternalRepair->price;
        }else {
            $DtoExternalRepair->price = '';
        }


        if (isset($Intervention->imgmodule)) {
            $DtoExternalRepair->imgName = $Intervention->imgmodule;
        }
        if (isset($idInterventionExternalRepair->date_return_product_from_the_supplier)) {
                        $DtoExternalRepair->date_return_product_from_the_supplier = $idInterventionExternalRepair->date_return_product_from_the_supplier;
                }else {
                        $DtoExternalRepair->date_return_product_from_the_supplier = '';
                }

        if (isset($idInterventionExternalRepair->ddt_supplier)) {
            $DtoExternalRepair->ddt_supplier = $idInterventionExternalRepair->ddt_supplier;
        }else {
            $DtoExternalRepair->ddt_supplier = '';
        }
        if (isset($idInterventionExternalRepair->reference_supplier)) {
            $DtoExternalRepair->reference_supplier = $idInterventionExternalRepair->reference_supplier;
        }else {
            $DtoExternalRepair->reference_supplier = '';
        }
        if (isset($idInterventionExternalRepair->notes_from_repairman)) {
            $DtoExternalRepair->notes_from_repairman = $idInterventionExternalRepair->notes_from_repairman;
        }else {
            $DtoExternalRepair->notes_from_repairman = '';
        }

         $DtoInterventions = new DtoInterventions();
            if (isset($Intervention->user_id)) {
                $DtoInterventions->user_id = UsersDatas::where('user_id', $Intervention->user_id)->first()->business_name;
            }
             if (isset($Intervention->description)) {
                $DtoInterventions->description = $Intervention->description;
            }
            if (isset($Intervention->defect)) {
                $DtoInterventions->defect = $Intervention->defect;
            }
            if (isset($Intervention->nr_ddt)) {
                $DtoInterventions->nr_ddt = $Intervention->nr_ddt;
            }
            if (isset($Intervention->idLanguage)) {
                $DtoInterventions->idLanguage = $Intervention->idLanguage;
            }
            if (isset($Intervention->date_ddt)) {
                $DtoInterventions->date_ddt = $Intervention->date_ddt;
            }
       
            if (isset($Intervention->create_quote)) {
                $DtoInterventions->create_quote = $Intervention->create_quote;
            }
            if (isset($Intervention->external_referent_id)) {
                $DtoInterventions->external_referent_id = People::where('id', $Intervention->external_referent_id)->first()->name;
            }
             if (isset($Intervention->plant_type)) {
                $DtoInterventions->plant_type = $Intervention->plant_type;
            }
             if (isset($Intervention->trolley_type)) {
                $DtoInterventions->trolley_type = $Intervention->trolley_type;
            }
             if (isset($Intervention->series)) {
                $DtoInterventions->series = $Intervention->series;
            }
            if (isset($Intervention->voltage)) {
                $DtoInterventions->voltage = $Intervention->voltage;
            }
            if (isset($Intervention->exit_notes)) {
                $DtoInterventions->exit_notes = $Intervention->exit_notes;
            }
            $DtoExternalRepair->selectFaultModule->push($DtoInterventions);

        return $DtoExternalRepair;
    }


    /**
     * Get by getByViewQuotes
     * 
     * @param int id
     * @return DtoInterventions
     */
    public static function getByViewQuotes(int $id, $idUser)
    {


        $idIntervention = GrIntervention::where('id', $id)->first();
        $DtoQuotes = new DtoQuotes();

        $DtoQuotes->id = $id;

      /*  if (isset($idIntervention->user_id)) {
            $DtoQuotes->id_customer= UsersDatas::where('user_id', $idIntervention->user_id)->first()->business_name;
        }else{
            $DtoQuotes->id_customer = '';
        }*/

        if (isset($idIntervention->user_id)) {
            $User = UsersDatas::where('user_id', $idIntervention->user_id)->first();
        if (isset($User->business_name)){
            $DtoQuotes->id_customer = $User->business_name;
            $DtoQuotes->id_user_quotes = $idIntervention->user_id;
          }else{
                $DtoQuotes->id_customer =  $User->name . ' ' . $User->surname; 
                $DtoQuotes->id_user_quotes = $idIntervention->user_id;
          }
        } else {
                $DtoQuotes->id_customer = '';
        }

        if (isset($idIntervention->code_intervention_gr)) {
            $DtoQuotes->code_intervention_gr = $idIntervention->code_intervention_gr;
        }else{
            $DtoQuotes->code_intervention_gr = '';
        }
        
        if (isset ($idIntervention->referent)){
            $DtoQuotes->referent = People::where('id', $idIntervention->referent)->first()->name;
        }else{
            $DtoQuotes->referent = '';
        }

        if (isset ($idIntervention->description)){
            $DtoQuotes->description = $idIntervention->description;
        }else{
            $DtoQuotes->description = '';
        }

        if (isset ($idIntervention->date_ddt)){
            $DtoQuotes->date_ddt = $idIntervention->date_ddt;
        }else{
            $DtoQuotes->date_ddt = '';
        }

        if (isset ($idIntervention->nr_ddt)){
            $DtoQuotes->nr_ddt = $idIntervention->nr_ddt;
        }else{
            $DtoQuotes->nr_ddt = '';
        }

        if (isset ($idIntervention->user_id)){
            $DtoQuotes->email= User::where('id', $idIntervention->user_id)->first()->email;
        }else{
            $DtoQuotes->email = '';
        }

        if (isset ($idIntervention->tot_final)){
            $DtoQuotes->tot_quote = $idIntervention->tot_final;
        }else{
                $DtoQuotes->tot_quote ='';
        } 


        foreach (PeopleUser::where('user_id', $idIntervention->user_id)->get() as $PeopleUser) {
            if (isset($PeopleUser->people_id)) {
                $Roles= Role::where('name', 'Preventivi')->get()->first();
                $RoleUserPeople = RolesPeople::where('roles_id', $Roles->id)->where('people_id', $PeopleUser->people_id)->get();
                foreach ($RoleUserPeople as $RoleUserPeoples) {
                    $DtoPeople = new DtoPeople();
                    $DtoPeople->user_id = $PeopleUser->user_id;
                    $DtoPeople->people_id = $RoleUserPeoples->people_id;
                    $DtoPeople->EmailReferente = People::where('id', $RoleUserPeoples->people_id)->get()->first()->email;
                    $DtoQuotes->ReferentEmailSendPreventivi->push($DtoPeople);
                }
            }                        
        } 
 
        foreach (GrInterventionProcessing::where('id_intervention', $id)->get() as $InterventionProcessing) {  
            //$DtoQuotes->description = $InterventionProcessing->description;
           // if (isset ($InterventionProcessing->id_gr_items)){
               // $DtoQuotes->code_gr = ItemGr::where('id', $InterventionProcessing->id_gr_items)->first()->code_gr;
            //}  
            $DtoInterventionProcessing = new DtoInterventionsProcessing();
            if (isset ($InterventionProcessing->id_gr_items)){
                $DtoInterventionProcessing->code_gr = ItemGr::where('id', $InterventionProcessing->id_gr_items)->first()->code_gr;
            } 
            $DtoInterventionProcessing->description = $InterventionProcessing->description;
            $DtoInterventionProcessing->qta = $InterventionProcessing->qta;
            $DtoQuotes->Articles->push($DtoInterventionProcessing);
        }

            $IdQuotes = GrQuotes::where('id_intervention', $id)->first();
            //$DtoQuotes = new DtoQuotes();
            if (isset ($IdQuotes->email)){
                $DtoQuotes->email = $IdQuotes->email;
            }
            if (isset ($IdQuotes->object)){
                $DtoQuotes->object = $IdQuotes->object;
            }
         /*  if (isset($IdQuotes->created_at)) {
                $DtoQuotes->date_agg = date_format($IdQuotes->created_at, 'd/m/Y');
            }else{
                $DtoQuotes->date_agg = Carbon::now();
            }*/

       
            if (isset($IdQuotes->date_agg)) {
                $timestamp = strtotime($IdQuotes->date_agg);
                if ($timestamp === FALSE) {
                    $DtoQuotes->date_agg = $IdQuotes->date_agg;
                }else{
                    $DtoQuotes->date_agg = date('d/m/Y', $timestamp);
                }
            }else{
                $dt = new DateTime();
                $DtoQuotes->date_agg = $dt->format('d-m-Y'); 
            }
            
             if (isset($IdQuotes->discount)) {
            $DtoQuotes->discount = $IdQuotes->discount;
            }else{
                $DtoQuotes->discount ='';
             } 

            if (isset($IdQuotes->discount)&&($idIntervention->tot_final)) { 
            $DtoQuotes->total_amount_with_discount = $idIntervention->tot_final - $IdQuotes->discount;
            }else{
                $DtoQuotes->total_amount_with_discount ='';
             }  
          /*  if (isset ($IdQuotes->note_before_the_quote)){
                $DtoQuotes->note_before_the_quote = $IdQuotes->note_before_the_quote;
            }*/

            if (isset ($IdQuotes->note_before_the_quote)){
                $DtoQuotes->note_before_the_quote = $IdQuotes->note_before_the_quote;   
            }else{
                $DtoQuotes->note_before_the_quote =  "In riferimento al Vs DDT n. " . $idIntervention->nr_ddt . "  del " . $idIntervention->date_ddt .  " con la presente siamo ad inviarVi il seguente preventivo di riparazione relativo a: ";   
            }
    

           /* if (isset ($IdQuotes->description_payment)){
                $DtoQuotes->description_payment = $IdQuotes->description_payment;
                $DtoQuotes->Description_payment = PaymentGr::where('id', $IdQuotes->description_payment)->first()->description;
            }*/
            if (isset($IdQuotes->description_payment)){
                $DtoQuotes->description_payment = $IdQuotes->description_payment;
                $DtoQuotes->Description_payment = PaymentGr::where('id', $IdQuotes->description_payment)->first()->description;
            }else{      
                $PaymentsUsersDatas = UsersDatas::where('user_id', $idIntervention->user_id)->first();
                if (isset($PaymentsUsersDatas)){
                    $GrPayments= PaymentGr::where('id', $PaymentsUsersDatas->id_payments)->first();
                }
               
                if (isset($GrPayments)){
                    $DtoQuotes->description_payment = $PaymentsUsersDatas->id_payments;
                    $DtoQuotes->Description_payment = $GrPayments->description;
                }else{
                    $DtoQuotes->description_payment = '';
                    $DtoQuotes->Description_payment = '';
                }
            } 

            if (isset ($IdQuotes->id_states_quote)){
                $DtoQuotes->id_states_quote = $IdQuotes->id_states_quote;
                $DtoQuotes->States_Description = StatesQuotesGr::where('id', $IdQuotes->id_states_quote)->first()->description;
            }else{ 
                $DtoQuotes->id_states_quote = StatesQuotesGr::where('description', 'In Attesa')->first()->id;
                $DtoQuotes->States_Description = StatesQuotesGr::where('description', 'In Attesa')->first()->description;
            }

            if (isset ($IdQuotes->note)){
                $DtoQuotes->note = $IdQuotes->note;
            }
             if (isset ($IdQuotes->note_customer)){
                $DtoQuotes->note_customer = $IdQuotes->note_customer;
            }
            if (isset ($IdQuotes->id_intervention)){
                $DtoQuotes->id = $IdQuotes->id_intervention;
            }
            if (isset ($IdQuotes->updated_at)){
                $DtoQuotes->updated_at = date_format($IdQuotes->updated_at, 'd/m/Y');
            }else{
                $DtoQuotes->updated_at = '';
            }

            if (isset ($IdQuotes->description_and_items_agg)){
                $DtoQuotes->description_and_items_agg = $IdQuotes->description_and_items_agg;
            }
  
       //  $User = User::where('id', $IdQuotes->created_id)->first();
        if (isset($IdQuotes->created_id)) {
            $DtoQuotes->created_id = UsersDatas::where('user_id', $IdQuotes->created_id)->first()->name;
        }else{
            $DtoQuotes->created_id = UsersDatas::where('user_id', $idUser)->first()->name;
        } 
    
               if (isset ($IdQuotes->kind_attention)){
                $DtoQuotes->kind_attention = $IdQuotes->kind_attention;
            }else{
                $DtoQuotes->kind_attention ='';
             }  

             if (isset ($IdQuotes->id_intervention)){
                    $messages = QuotesMessages::where('id_intervention', $IdQuotes->id_intervention)->get();
             }
            
             if (isset($messages)) {
                 foreach ($messages as $MessageDetail) {
                     $DtoQuotesMessages= new DtoQuotesMessages();
                     $DtoQuotesMessages->id = $MessageDetail->id;
     
                     if (isset($MessageDetail->id_sender)) { 
                         $DtoQuotesMessages->id_sender_id = $MessageDetail->id_sender;
                         $DtoQuotesMessages->id_sender = User::where('id', $MessageDetail->id_sender)->first()->name;
                     }else{
                         $DtoQuotesMessages->id_sender = '';  
                     }
                     if (isset($MessageDetail->id_receiver)) {  
                        $DtoQuotesMessages->id_receiver_id = $MessageDetail->id_receiver;
                         $DtoQuotesMessages->id_receiver =  User::where('id', $MessageDetail->id_receiver)->first()->name;
                     }else{  
                         $DtoQuotesMessages->id_receiver = '';  
                     }
                     $DtoQuotesMessages->messages = $MessageDetail->message;
                     $DtoQuotesMessages->id_intervention = $MessageDetail->id_intervention;
                     $DtoQuotesMessages->created_at = date_format($MessageDetail->created_at, 'd/m/Y h:m:s');
                     $DtoQuotes->Messages->push($DtoQuotesMessages);
              }
         }
         

        return $DtoQuotes;
    }

    /**
     * Get by getByViewValue
     * 
     * @param int id
     * @return DtoInterventions
     */
    public static function getByViewValue(int $id)
    {
        $valueConfigHour = "value_config_hour";
        $consumables = "consumables";

        $idIntervention = GrIntervention::where('id', $id)->first();

        $DtoInterventions = new DtoInterventions();

        $DtoInterventions->id = $id;
        $DtoInterventions->date_ddt = $idIntervention->date_ddt;

      /*  $User=UsersDatas::where('user_id', $idIntervention->user_id)->first();
        if (isset($User)) {
            $DtoInterventions->user_id_view_processing = $User->business_name;
        }*/

        if (isset($idIntervention->user_id)) {
            $User = UsersDatas::where('user_id', $idIntervention->user_id)->first();
          if (isset($User->business_name)){
              $DtoInterventions->user_id_view_processing = $User->business_name;
          }else{
              $DtoInterventions->user_id_view_processing =  $User->name . ' ' . $User->surname; 
          }
        } else {
            $DtoInterventions->user_id_view_processing = '';
            $DtoInterventions->user_id = '';
        }

        if (isset($idIntervention->code_intervention_gr)) {
            $DtoInterventions->code_intervention_gr = $idIntervention->code_intervention_gr;
        }else{
            $DtoInterventions->code_intervention_gr = '';
        }
        
       /* if (isset($idIntervention->user_id)) {
            $DtoInterventions->user_id = UsersDatas::where('user_id', $idIntervention->user_id)->first()->business_name;
        }*/

        if (isset($idIntervention->items_id)) {
            $DtoInterventions->items_id = ItemGr::where('id', $idIntervention->items_id)->first()->code_gr;
        }

        $PeopleRef=People::where('id', $idIntervention->referent)->first();
        if (isset($PeopleRef)) {
            $DtoInterventions->referent = $PeopleRef->name;
        }else{
          $DtoInterventions->referent = '';  
        }

        $DtoInterventions->value_config_hour = Setting::where('code', $valueConfigHour)->first()->value;
        $DtoInterventions->consumables = Setting::where('code', $consumables)->first()->value;

        if (isset($idIntervention->imgmodule)) {
            $DtoInterventions->imgName = $idIntervention->imgmodule;
        }

  if (isset($idIntervention->working_with_testing)) {
            $DtoInterventions->working_with_testing = $idIntervention->working_with_testing;
        }
          if (isset($idIntervention->working_without_testing)) {
            $DtoInterventions->working_without_testing = $idIntervention->working_without_testing;
        }
        //$DtoInterventions->updated_at =date_format($idIntervention->updated_at, 'd/m/Y');
        $dateProcessing = GrInterventionProcessing::where('id_intervention', $id)->first();
        if (isset($dateProcessing->updated_at)) {
            $DtoInterventions->updated_at = date_format($dateProcessing->updated_at, 'd/m/Y H:i:s');
        }else{
            $DtoInterventions->updated_at = '';
        }
        if (isset($dateProcessing->created_at)) {
            $DtoInterventions->created_at = date_format($dateProcessing->created_at, 'd/m/Y H:i:s');
        }else{
            $DtoInterventions->created_at = '';
        }

        $DtoInterventions->description = $idIntervention->description;
        $DtoInterventions->defect = $idIntervention->defect;
        $DtoInterventions->unrepairable = $idIntervention->unrepairable;
        $DtoInterventions->flaw_detection = $idIntervention->flaw_detection;
        $DtoInterventions->done_works = $idIntervention->done_works;
        $DtoInterventions->to_call = $idIntervention->to_call;
        $DtoInterventions->create_quote = $idIntervention->create_quote;
        $DtoInterventions->escape_from = $idIntervention->escape_from;
        $DtoInterventions->working = $idIntervention->working;
        $DtoInterventions->search_product = $idIntervention->search_product;
         if ($idIntervention->hour == '' || $idIntervention->hour == null){
          $DtoInterventions->hour = 0;  
        } else { 
        $DtoInterventions->hour = $idIntervention->hour;
        }
        if ($idIntervention->tot_value_config == '' || $idIntervention->tot_value_config == null){
          $DtoInterventions->tot_value_config = 0;  
        } else { 
        $DtoInterventions->tot_value_config = $idIntervention->tot_value_config;
        }
        $DtoInterventions->tot_final = $idIntervention->tot_final;
        $DtoInterventions->total_calculated = $idIntervention->total_calculated;

        foreach (GrInterventionProcessing::where('id_intervention', $id)->get() as $InterventionProcessing) {
            $DtoInterventionProcessing = new DtoInterventionsProcessing();
            $DtoInterventionProcessing->description = $InterventionProcessing->description;
            $DtoInterventionProcessing->qta = $InterventionProcessing->qta;
            $DtoInterventionProcessing->n_u = $InterventionProcessing->n_u;
            $DtoInterventionProcessing->id_intervention = $InterventionProcessing->id_intervention;
            if (isset($InterventionProcessing->id_gr_items)) {
                $DtoInterventionProcessing->code_gr = $InterventionProcessing->id_gr_items;
                $DtoInterventionProcessing->description_codeGr = ItemGr::where('id', $InterventionProcessing->id_gr_items)->first()->code_gr;
            }
            //$DtoInterventionProcessing->id_gr_items = $InterventionProcessing->id_gr_items;
            $DtoInterventionProcessing->price_list = $InterventionProcessing->price_list;
            $DtoInterventionProcessing->tot = $InterventionProcessing->tot;
            $DtoInterventions->Articles->push($DtoInterventionProcessing);
        }
        return $DtoInterventions;
    }


 /**
     * Get by getByModuleGuarantee
     * 
     * @param int id
     * @return DtoModuleValidationGuarantee
     */
    public static function getByModuleGuarantee(int $id)
    {
        $ModuleValidationGuarantee = ModuleValidationGuarantee::where('id_intervention', $id)->first();
        $idIntervention = GrIntervention::where('id', $id)->first();

        $DtoModuleValidationGuarantee = new DtoModuleValidationGuarantee();

             if (isset($idIntervention->external_referent_id)) {
            //$DtoModuleValidationGuarantee->external_referent_id = $idIntervention->external_referent_id;
            $DtoModuleValidationGuarantee->external_referent_id = People::where('id', $idIntervention->external_referent_id)->first()->name;
            }
            if (isset($ModuleValidationGuarantee->date_mount)) {
                $DtoModuleValidationGuarantee->date_mount = $ModuleValidationGuarantee->date_mount;
            }
            if (isset($ModuleValidationGuarantee->id_user)) {
                 $DtoModuleValidationGuarantee->id_user = UsersDatas::where('user_id', $ModuleValidationGuarantee->id_user)->first()->name;
            }
             if (isset($ModuleValidationGuarantee->id)) {
                $DtoModuleValidationGuarantee->id = $ModuleValidationGuarantee->id;
            }
            if (isset($idIntervention->id)) {
                $DtoModuleValidationGuarantee->id_intervention = $idIntervention->id;
            }
            if (isset($idIntervention->trolley_type)) {
                $DtoModuleValidationGuarantee->trolley_type = $idIntervention->trolley_type;
            }
             if (isset($idIntervention->plant_type)) {
                $DtoModuleValidationGuarantee->plant_type = $idIntervention->plant_type;
            }
             if (isset($idIntervention->voltage)) {
                $DtoModuleValidationGuarantee->voltage = $idIntervention->voltage;
            }
             if (isset($ModuleValidationGuarantee->traction)) {
                $DtoModuleValidationGuarantee->traction = $ModuleValidationGuarantee->traction;
            }
             if (isset($ModuleValidationGuarantee->lift)) {
                $DtoModuleValidationGuarantee->lift = $ModuleValidationGuarantee->lift;
            }
             if (isset($ModuleValidationGuarantee->insulation_value_tewards_machine_frame_positive)) {
                $DtoModuleValidationGuarantee->insulation_value_tewards_machine_frame_positive = $ModuleValidationGuarantee->insulation_value_tewards_machine_frame_positive;
            }
             if (isset($ModuleValidationGuarantee->insulation_value_tewards_machine_frame_negative)) {
                $DtoModuleValidationGuarantee->insulation_value_tewards_machine_frame_negative = $ModuleValidationGuarantee->insulation_value_tewards_machine_frame_negative;
            }
             if (isset($ModuleValidationGuarantee->single_motor_with_traction_motor_range)) {
                $DtoModuleValidationGuarantee->single_motor_with_traction_motor_range = $ModuleValidationGuarantee->single_motor_with_traction_motor_range;
            }
             if (isset($ModuleValidationGuarantee->single_motor_with_brush_traction_motor)) {
                $DtoModuleValidationGuarantee->single_motor_with_brush_traction_motor = $ModuleValidationGuarantee->single_motor_with_brush_traction_motor;
            }

            if (isset($ModuleValidationGuarantee->single_motor_u_v_w_traction_phase_motor)) {
                $DtoModuleValidationGuarantee->single_motor_u_v_w_traction_phase_motor = $ModuleValidationGuarantee->single_motor_u_v_w_traction_phase_motor;
            }
            if (isset($ModuleValidationGuarantee->single_motor_u_v_w_motor_lifting_phase)) {
                $DtoModuleValidationGuarantee->single_motor_u_v_w_motor_lifting_phase = $ModuleValidationGuarantee->single_motor_u_v_w_motor_lifting_phase;
            }
            if (isset($ModuleValidationGuarantee->single_motor_with_brush_traction_motor)) {
                $DtoModuleValidationGuarantee->single_motor_with_brush_traction_motor = $ModuleValidationGuarantee->single_motor_with_brush_traction_motor;
            }
            if (isset($ModuleValidationGuarantee->bi_engine_with_traction_motor_range)) {
                $DtoModuleValidationGuarantee->bi_engine_with_traction_motor_range = $ModuleValidationGuarantee->bi_engine_with_traction_motor_range;
            }
               if (isset($ModuleValidationGuarantee->bi_engine_u_v_w_traction_phase_motor)) {
                $DtoModuleValidationGuarantee->bi_engine_u_v_w_traction_phase_motor = $ModuleValidationGuarantee->bi_engine_u_v_w_traction_phase_motor;
            }
               if (isset($ModuleValidationGuarantee->bi_engine_u_v_w_motor_lifting_phase)) {
                $DtoModuleValidationGuarantee->bi_engine_u_v_w_motor_lifting_phase = $ModuleValidationGuarantee->bi_engine_u_v_w_motor_lifting_phase;
            }

               if (isset($ModuleValidationGuarantee->lift_with_field_brush_motor_raised)) {
                $DtoModuleValidationGuarantee->lift_with_field_brush_motor_raised = $ModuleValidationGuarantee->lift_with_field_brush_motor_raised;
            }

               if (isset($ModuleValidationGuarantee->lift_with_u_v_w_motor_lift_pharse)) {
                $DtoModuleValidationGuarantee->lift_with_u_v_w_motor_lift_pharse = $ModuleValidationGuarantee->lift_with_u_v_w_motor_lift_pharse;
            }
               if (isset($ModuleValidationGuarantee->battery_voltage)) {
                $DtoModuleValidationGuarantee->battery_voltage = $ModuleValidationGuarantee->battery_voltage;
            }
                if (isset($ModuleValidationGuarantee->traction_limit_current_consumption)) {
                $DtoModuleValidationGuarantee->traction_limit_current_consumption = $ModuleValidationGuarantee->traction_limit_current_consumption;
            }
                if (isset($ModuleValidationGuarantee->bi_engine_with_brush_traction_motor)) {
                $DtoModuleValidationGuarantee->bi_engine_with_brush_traction_motor = $ModuleValidationGuarantee->bi_engine_with_brush_traction_motor;
            }

              if (isset($ModuleValidationGuarantee->lift_limit_current_absorption)) {
                $DtoModuleValidationGuarantee->lift_limit_current_absorption = $ModuleValidationGuarantee->lift_limit_current_absorption;
            }
             if (isset($ModuleValidationGuarantee->note)) {
                $DtoModuleValidationGuarantee->note = $ModuleValidationGuarantee->note;
            }
        return $DtoModuleValidationGuarantee;
    }


 /**
     * Get by getByFaultModule
     * 
     * @param int id
     * @return DtoInterventions
     */
    public static function getByFaultModule(int $id)
    {
        $Intervention = GrIntervention::where('id', $id)->first();

         $DtoInterventions = new DtoInterventions();
           

            $DtoInterventions->id = $Intervention->id;
             if (isset($Intervention->description)) {
                $DtoInterventions->description = $Intervention->description;
            }
            if (isset($Intervention->defect)) {
                $DtoInterventions->defect = $Intervention->defect;
            }
            if (isset($Intervention->nr_ddt)) {
                $DtoInterventions->nr_ddt = $Intervention->nr_ddt;
            }
            if (isset($Intervention->idLanguage)) {
                $DtoInterventions->idLanguage = $Intervention->idLanguage;
            }
            if (isset($Intervention->date_ddt)) {
                $DtoInterventions->date_ddt = $Intervention->date_ddt;
            }
       
            if (isset($Intervention->create_quote)) {
                $DtoInterventions->create_quote = $Intervention->create_quote;
            }
            //if (isset($Intervention->referent)) {
              //  $DtoInterventions->referent = People::where('id', $Intervention->referent)->first()->name;
           // }
             if (isset($Intervention->external_referent_id)) {
                $DtoInterventions->external_referent_id = People::where('id', $Intervention->external_referent_id)->first()->name;
            } 
            if (isset($Intervention->external_referent_id)) {
                $DtoInterventions->phone_number = People::where('id', $Intervention->external_referent_id)->first()->phone_number;
            }

            // if (isset($Intervention->referent)) {
              //  $DtoInterventions->phone_number = People::where('id', $Intervention->referent)->first()->phone_number;
           // }
               
             if (isset($Intervention->plant_type)) {
                $DtoInterventions->plant_type = $Intervention->plant_type;
            }
             if (isset($Intervention->trolley_type)) {
                $DtoInterventions->trolley_type = $Intervention->trolley_type;
            }
             if (isset($Intervention->series)) {
                $DtoInterventions->series = $Intervention->series;
            }
            if (isset($Intervention->voltage)) {
                $DtoInterventions->voltage = $Intervention->voltage;
            }
            if (isset($Intervention->exit_notes)) {
                $DtoInterventions->exit_notes = $Intervention->exit_notes;
            }
             if (isset($Intervention->additional_notes)) {
                $DtoInterventions->additional_notes = $Intervention->additional_notes;
            }
            
        return $DtoInterventions;
    }

     /**
     * getDescriptionAndPriceProcessingSheet
     * 
     * @param int user_id
     * @return DtoInterventions
     */
    public static function getDescriptionAndPriceProcessingSheet(int $id)
    {
        $codeProduct = ItemGr::where('id', $id)->first();
        $dtoItemGr = new DtoItemGr();
        
        $dtoItemGr->id = $id;

        if (isset($codeProduct->description)){
            $dtoItemGr->description = $codeProduct->description;
        }else{
            $dtoItemGr->description = '';
        }
        if (isset($codeProduct->note)){
            $dtoItemGr->note = $codeProduct->note;
        }else{
            $dtoItemGr->note = '';
        }
        if (isset($codeProduct->plant)){
            $dtoItemGr->plant = $codeProduct->plant;
        }else{
            $dtoItemGr->plant = '';
        }
        if (isset($codeProduct->tipology)){
            $dtoItemGr->tipology = Tipology::where('id',$codeProduct->tipology)->first()->description;
        }else{
            $dtoItemGr->tipology = '';
        }
        if (isset($codeProduct->plant)){
            $dtoItemGr->price_list = $codeProduct->price_list;
        }else{
            $dtoItemGr->price_list = '';
        }
        return  $dtoItemGr;
    }
    //end getDescriptionAndPriceProcessingSheet


 /**
     * getdescription
     * 
 * @param int id
     * @return DtoInterventions
     */
   public static function getdescription(int $id) {
    //$GrItems = ItemGr::where('id', $id)->first();

      foreach (DB::select(
            "SELECT i.description, i.tipology, i.plant, i.note
            FROM gr_items as i
            WHERE i.id = $id" 
        ) as $GrItems) {
    $DtoInterventions = new DtoInterventions();  

    if (isset($GrItems->description)){
        $DtoInterventions->description = $GrItems->description; 
    }else{
        $DtoInterventions->description = '';
    }
    if (isset($GrItems->note)){
        $DtoInterventions->note = $GrItems->note; 
    }else{
        $DtoInterventions->note = '';
    }
    if (isset($GrItems->plant)){
        $DtoInterventions->plant = $GrItems->plant;  
    }else{
        $DtoInterventions->plant = '';
    }
    if (isset($GrItems->tipology)){
        $DtoInterventions->tipology = Tipology::where('id',$GrItems->tipology)->first()->description;  
    }else{
        $DtoInterventions->tipology = '';
    }
    //$DtoInterventions->description = $GrItems->description; 
    //$DtoInterventions->note = $GrItems->note; 
    //$DtoInterventions->plant = $GrItems->plant; 
    //$DtoInterventions->tipology = Tipology::where('id',$GrItems->tipology)->first()->description;
    return $DtoInterventions; 
    }       
   
}

  /**
     * getnumbertelephone
     * 
     * @param int id
     * @return DtoInterventions
     */
    public static function getnumbertelephone(int $id)
    {
        $People = People::where('id', $id)->first();
        $DtoInterventions = new DtoInterventions();
        $DtoInterventions->phone_number = $People->phone_number; 
        return  $DtoInterventions;
    }
    //end getnumbertelephone


    /**
     * getByIdUser
     * 
     * @param int user_id
     * @return DtoInterventions
     */
    public static function getByIdUser(int $user_id)
    {
        $user = User::where('id', $user_id)->first();
        $userdata = UsersDatas::where('user_id', $user_id)->first();

        $dtoUser = new DtoUser();
        $dtoUser->id = $user_id;
        $dtoUser->name = $userdata->name;
        $dtoUser->surname = $userdata->surname;
        $dtoUser->business_name = $userdata->business_name;
        $dtoUser->username = $user->name;
        $dtoUser->email = $user->email;
        $dtoUser->vat_number = $userdata->vat_number;
        $dtoUser->fiscal_code = $userdata->fiscal_code;
        $dtoUser->address = $userdata->address;
        $dtoUser->codepostal = $userdata->codepostal;
        $dtoUser->address = $userdata->address;
        $dtoUser->telephone_number = $userdata->telephone_number;
        $dtoUser->website = $userdata->website;
        $dtoUser->country = $userdata->country;
        $dtoUser->province = $userdata->province;
        $dtoUser->mobile_number = $userdata->mobile_number;
        $dtoUser->fax_number = $userdata->fax_number;
        $dtoUser->pec = $userdata->pec;
        $dtoUser->vat_type_id = $userdata->vat_type_id;
        return  $dtoUser;
    }


    /**
     * Get select
     *
     * @param String $search
     * @return Intervention
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return GrIntervention::select('id', 'description')->get();
        } else {
            return GrIntervention::where('description', 'like', $search . '%')->select('id', 'description')->get();
        }
    }

    /**
     * Get select
     *
     * @param String $search
     * @return Intervention
     */
    public static function getLastId()
    {

        $LastId = GrIntervention::orderBy('id', 'DESC')->first();
        if (isset($LastId->id)) {
            return $LastId->id + 1;
        } else {
            return 1;
        }
    }

        /**
     * getLastIdLib
     *
     * @param String $search
     * @return Intervention
     */
    public static function getLastIdLib()
    {
        $LastId = GrIntervention::orderBy('code_intervention_gr', 'DESC')->first();
        if (isset($LastId->code_intervention_gr)) {
          
            return $LastId->code_intervention_gr + 1;
        } else {
            return 1;
        }
    }


    /**
     * Get all
     * 
     * @return DtoInterventions
     */
    public static function getAll($idLanguage)
    {
        $result = collect();

        foreach (GrIntervention::orderBy('id', 'ASC')->get() as $InterventionAll) {

            $DtoInterventions = new DtoInterventions();

            $CustomerId = $InterventionAll->customers_id;
            //$CustomerDescription = User::where('id', $InterventionAll->customers_id)->first()->name;
            $CustomerDescription = User::where('id', $InterventionAll->customers_id)->first();
            $StatesDescription = GrStates::where('id', $InterventionAll->states_id)->first();

            $ItemId = ItemLanguage::where('item_id', $InterventionAll->items_id)->first();

            //$DtoInterventions->code_product = $ItemId;

            //valori tabella 
            if (isset($InterventionAll->id)) {
                $DtoInterventions->idIntervention = $InterventionAll->id;
                $DtoInterventions->id = $InterventionAll->id;
            }

            if (isset($CustomerDescription->name)) {
                $DtoInterventions->descriptionCustomer = $CustomerDescription->name;
            }

            //end valori tabella 
            //valori dettaglio

            if (isset($ItemId->id)) {
                $DtoInterventions->code_product = $ItemId->id;
            }

            if (isset($CustomerId)) {
                $DtoInterventions->customer_id = $CustomerId;
            }

            if (isset($StatesDescription->id)) {
                $DtoInterventions->states_id =  $StatesDescription->id;
            }

            if (isset($InterventionAll->description)) {
                $DtoInterventions->description = $InterventionAll->description;
            }
             if (isset($InterventionAll->rip_association)) {
                $DtoInterventions->rip_association = $InterventionAll->rip_association;
            }
            
            if (isset($InterventionAll->defect)) {
                $DtoInterventions->defect = $InterventionAll->defect;
            }
            if (isset($InterventionAll->nr_ddt)) {
                $DtoInterventions->nr_ddt = $InterventionAll->nr_ddt;
            }
            if (isset($InterventionAll->date_ddt)) {
                $DtoInterventions->date_ddt = $InterventionAll->date_ddt;
            }
            if (isset($InterventionAll->nr_ddt_gr)) {
                $DtoInterventions->nr_ddt_gr = $InterventionAll->nr_ddt_gr;
            }
            if (isset($InterventionAll->date_ddt_gr)) {
                $DtoInterventions->date_ddt_gr = $InterventionAll->date_ddt_gr;
            }

            if (isset($InterventionAll->unrepairable)) {
                $DtoInterventions->unrepairable = $InterventionAll->unrepairable;
            }
            if (isset($InterventionAll->send_to_customer)) {
                $DtoInterventions->send_to_customer = $InterventionAll->send_to_customer;
            }
            if (isset($InterventionAll->create_quote)) {
                $DtoInterventions->create_quote = $InterventionAll->create_quote;
            }

            if (isset($InterventionAll->referent)) {
                $DtoInterventions->referent = $InterventionAll->referent;
            }
            if (isset($InterventionAll->plant_type)) {
                $DtoInterventions->plant_type = $InterventionAll->plant_type;
            }
            if (isset($InterventionAll->trolley_type)) {
                $DtoInterventions->trolley_type = $InterventionAll->trolley_type;
            }
            if (isset($InterventionAll->series)) {
                $DtoInterventions->series = $InterventionAll->series;
            }
            if (isset($InterventionAll->voltage)) {
                $DtoInterventions->voltage = $InterventionAll->voltage;
            }
            if (isset($InterventionAll->exit_notes)) {
                $DtoInterventions->exit_notes = $InterventionAll->exit_notes;
            }

            if (isset($InterventionAll->imgmodule)) {
                $DtoInterventions->imgmodule = $InterventionAll->imgmodule;
            }

            foreach (GrInterventionPhoto::where('intervention_id', $InterventionAll->id)->get() as $InterventionCaptures) {
                $DtoInterventionsPhotos = new DtoInterventionsPhotos();
                $DtoInterventionsPhotos->img = $InterventionCaptures->img;
                $DtoInterventions->captures->push($DtoInterventionsPhotos);
            }

            //end valori dettaglio 

            $result->push($DtoInterventions);
        }

        return $result;
    }

    #endregion GET

  

/**
     * getAllIntervention
     * 
     * @return DtoInterventions
     */
    public static function getAllRepairArrivedNumberMenuGlobal($idLanguage)
    {
            $result = collect();

            foreach (DB::select(
                "SELECT COUNT(*) as contanumeriid
                FROM gr_interventions as i
                WHERE (i.nr_ddt IS NOT NULL
                OR i.date_ddt IS NOT NULL
                OR i.referent IS NOT NULL
                OR i.series IS NOT NULL
                OR i.trolley_type IS NOT NULL
                OR i.plant_type IS NOT NULL
                OR i.defect IS NOT NULL
                OR i.voltage IS NOT NULL
                OR i.exit_notes IS NOT null)
                and 
                (i.date_ddt_gr is null 
                )and
                ( i.nr_ddt_gr is null
                or i.nr_ddt_gr = '')and
                (i.description is null
                or i.description = '')and 
                (i.repaired_arrived = false)"
                ) as $ContaNumeroIdtot) {
                
            $DtoInterventions = new DtoInterventions();

            if (isset($ContaNumeroIdtot->contanumeriid)) {
                $DtoInterventions->contanumeriid = $ContaNumeroIdtot->contanumeriid;
            } else {
                $DtoInterventions->contanumeriid = '';
            }

            $result->push($DtoInterventions);
        }
        return $result;
    }


  /**
     * getAllIntervention
     * 
     * @return DtoInterventions
     */
    public static function getAllRepairArriving($idLanguage)
    {
            $result = collect();

            foreach (DB::select(
                "SELECT i.id, i.nr_ddt, i.date_ddt, i.user_id, i.series, i.trolley_type, i.plant_type, i.defect, 
                i.voltage, i.exit_notes, i.date_ddt_gr, i.nr_ddt_gr,  i.description,i.referent,i.repaired_arrived
                FROM gr_interventions as i
                WHERE (i.nr_ddt IS NOT NULL
                OR i.date_ddt IS NOT NULL
                OR i.referent IS NOT NULL
                OR i.series IS NOT NULL
                OR i.trolley_type IS NOT NULL
                OR i.plant_type IS NOT NULL
                OR i.defect IS NOT NULL
                OR i.voltage IS NOT NULL
                OR i.exit_notes IS NOT null)
                and 
                (i.date_ddt_gr is null)and
                ( i.nr_ddt_gr is null)and
                (i.description is null)
                and (i.repaired_arrived = false)
                order by i.id
                limit 100"
                ) as $InterventionAll) {
                
            $DtoInterventions = new DtoInterventions();

             $DtoInterventions->id = $InterventionAll->id;
            if (isset($InterventionAll->nr_ddt)) {
                $DtoInterventions->nr_ddt = $InterventionAll->nr_ddt;
            } else {
                $DtoInterventions->nr_ddt = '';
            }

            if (isset($InterventionAll->date_ddt)) {
                $DtoInterventions->date_ddt = str_replace('00:00:00', '', $InterventionAll->date_ddt);
            } else {
                $DtoInterventions->date_ddt = '';
            }

            if (isset($InterventionAll->referent)) {
                $DtoInterventions->referent = $InterventionAll->referent;
            } else {
                $DtoInterventions->referent = '';
            }
            
            if (isset($InterventionAll->series)) {
                $DtoInterventions->series = $InterventionAll->series;
            } else {
                $DtoInterventions->series = '';
            }
              if (isset($InterventionAll->trolley_type)) {
                $DtoInterventions->trolley_type = $InterventionAll->trolley_type;
            } else {
                $DtoInterventions->trolley_type = '';
            }
              if (isset($InterventionAll->plant_type)) {
                $DtoInterventions->plant_type = $InterventionAll->plant_type;
            } else {
                $DtoInterventions->plant_type = '';
            }
            if (isset($InterventionAll->defect)) {
                $DtoInterventions->defect = $InterventionAll->defect;
            } else {
                $DtoInterventions->defect = '';
            }
               if (isset($InterventionAll->voltage)) {
                $DtoInterventions->voltage = $InterventionAll->voltage;
            } else {
                $DtoInterventions->voltage = '';
            }
               if (isset($InterventionAll->exit_notes)) {
                $DtoInterventions->exit_notes = $InterventionAll->exit_notes;
            } else {
                $DtoInterventions->exit_notes = '';
            }
           
              if (isset($InterventionAll->nr_ddt_gr)) {
                $DtoInterventions->nr_ddt_gr = $InterventionAll->nr_ddt_gr;
            } else {
                $DtoInterventions->nr_ddt_gr = '';
            }
              if (isset($InterventionAll->date_ddt_gr)) {
                $DtoInterventions->date_ddt_gr = str_replace('00:00:00', '', $InterventionAll->date_ddt_gr);
            } else {
                $DtoInterventions->date_ddt_gr = '';
            }
              if (isset($InterventionAll->user_id)) {
                  $userdatas = UsersDatas::where('user_id', $InterventionAll->user_id)->first();
                if (isset($userdatas->business_name)){
                    $DtoInterventions->user_id = $userdatas->business_name;
                }else{
                    $DtoInterventions->user_id =  $userdatas->name . ' ' . $userdatas->surname; 
                }

                //$DtoInterventions->user_id = UsersDatas::where('user_id', $InterventionAll->user_id)->first()->business_name;
            } else {
                $DtoInterventions->user_id = '';
            }
              if (isset($InterventionAll->description)) {
                $DtoInterventions->description = $InterventionAll->description;
            } else {
                $DtoInterventions->description = '';
            }
            if (isset($InterventionAll->repaired_arrived)) {
                $DtoInterventions->repaired_arrived = $InterventionAll->repaired_arrived;
            } else {
                $DtoInterventions->repaired_arrived = false;
            }
            
            
            $result->push($DtoInterventions);
        }
        return $result;
    }


/**
     * 
     * 
     * @param Request request
     * 
     */
    public static function SearchTrue(request $request){
        $result = collect();
        
        foreach (DB::select(
               'SELECT gr_interventions.*
                FROM gr_interventions
                WHERE gr_interventions.ready = true
                and gr_interventions.date_ddt_gr is null 
                and gr_interventions.nr_ddt_gr is null
                order by id desc
                limit 150'     
            ) as $InterventionAll) {

        $idInterventionExternalRepair = ExternalRepair::where('id_intervention', $InterventionAll->id)->first();
            $DtoInterventions = new DtoInterventions();

            $CustomerId = $InterventionAll->customers_id;
            //  $CustomerDescription = User::where('id', $InterventionAll->customers_id)->first()->name;
            //$CustomerDescription = User::where('id', $InterventionAll->user_id)->first();
            $CustomerDescription = UsersDatas::where('user_id', $InterventionAll->user_id)->first();

            $UserId = User::where('id', $InterventionAll->user_id)->first();
            $StatesDescription = GrStates::where('id', $InterventionAll->states_id)->first();
            $CodeGr = ItemGr::where('id', $InterventionAll->items_id)->first();
            $ItemId = ItemLanguage::where('item_id', $InterventionAll->items_id)->first();
            $Quotes = GrQuotes::where('id_intervention', $InterventionAll->id)->first();
            //$DtoInterventions->code_product = $ItemId;

            if (isset($Quotes->id_states_quote)) {
                $DtoInterventions->id_states_quote = StatesQuotesGr::where('id', $Quotes->id_states_quote)->first()->description;
            } else {
                $DtoInterventions->id_states_quote = '';
            }
            $Gr_InterventionProcessing = GrInterventionProcessing::where('id_intervention', $InterventionAll->id)->first();
            if (isset($Gr_InterventionProcessing->id_intervention)) {
                $DtoInterventions->checkProcessingExist = true;
            }else{
                $DtoInterventions->checkProcessingExist = false;
            }
            if (isset($idInterventionExternalRepair->id)) {
                $DtoInterventions->idInterventionExternalRepair = $idInterventionExternalRepair->id;
            }
            if (isset($idInterventionExternalRepair->date_return_product_from_the_supplier)) {
                $DtoInterventions->date_return_product_from_the_supplier = $idInterventionExternalRepair->date_return_product_from_the_supplier;
            }
            if (isset($InterventionAll->code_intervention_gr)) {
                $DtoInterventions->code_intervention_gr = $InterventionAll->code_intervention_gr;  
            }
            if (isset($InterventionAll->rip_association)) {
                $DtoInterventions->rip_association = '<br><label style="color:red">Riparazione Associata:</label>' . $InterventionAll->rip_association . '';    
            }   
           
            if (isset($idInterventionExternalRepair->ddt_supplier)) {
                $DtoInterventions->ddt_supplier = $idInterventionExternalRepair->ddt_supplier;
            }
            //valori tabella 
            if (isset($InterventionAll->id)) {
                $DtoInterventions->idIntervention = $InterventionAll->id;
                $DtoInterventions->id = $InterventionAll->id;
            }
            if (isset($StatesDescription->description)) {
                $DtoInterventions->descriptionStates =  $StatesDescription->description;
            } else {
                $DtoInterventions->descriptionStates = '';
            }
           /* if (isset($CustomerDescription->business_name)) {
                $DtoInterventions->descriptionCustomer =  $CustomerDescription->business_name;
            } else {
                $DtoInterventions->descriptionCustomer = '';
            }*/

            if (isset($InterventionAll->user_id)) {
                $userdatas = UsersDatas::where('user_id', $InterventionAll->user_id)->first();
              if (isset($userdatas->business_name)){
                  $DtoInterventions->descriptionCustomer = $userdatas->business_name;
              }else{
                  $DtoInterventions->descriptionCustomer =  $userdatas->name . ' ' . $userdatas->surname; 
              }
            } else {
                    $DtoInterventions->user_id = '';
            }

            if (isset($CodeGr->code_gr)) {
                $DtoInterventions->codeGr =  $CodeGr->code_gr;
            } else {
                $DtoInterventions->codeGr = '';
            }
            //end valori tabella 
            //valori dettaglio
            if (isset($CodeGr->id)) {
                $DtoInterventions->items_id = $CodeGr->id;
            } else {
                $DtoInterventions->items_id = '';
            }
            if (isset($ItemId->id)) {
                $DtoInterventions->code_product = $ItemId->id;
            } else {
                $DtoInterventions->code_product = '';
            }
            if (isset($UserId->id)) {
                $DtoInterventions->user_id = $UserId->id;
            } else {
                $DtoInterventions->user_id = '';
            }
            if (isset($StatesDescription->id)) {
                $DtoInterventions->states_id =  $StatesDescription->id;
            } else {
                $DtoInterventions->states_id = '';
            }

            if (isset($InterventionAll->description)) {
                $DtoInterventions->description = $InterventionAll->description;
            } else {
                $DtoInterventions->description = '';
            }
            if (isset($InterventionAll->defect)) {
                $DtoInterventions->defect = $InterventionAll->defect;
            } else {
                $DtoInterventions->defect = '';
            }
            if (isset($InterventionAll->nr_ddt)) {
                $DtoInterventions->nr_ddt = $InterventionAll->nr_ddt;
            } else {
                $DtoInterventions->nr_ddt = '';
            }
            if (isset($InterventionAll->date_ddt)) {
                $DtoInterventions->date_ddt = str_replace('00:00:00', '', $InterventionAll->date_ddt);
            } else {
                $DtoInterventions->date_ddt = '';
            }
            if (isset($InterventionAll->nr_ddt_gr)) {
                $DtoInterventions->nr_ddt_gr = $InterventionAll->nr_ddt_gr;
            } else {
                $DtoInterventions->nr_ddt_gr = '';
            }
              $DtoInterventions->date_ddt_gr = str_replace('00:00:00', '', $InterventionAll->date_ddt_gr);

            if (isset($InterventionAll->ready)) {
                 $DtoInterventions->ready = $InterventionAll->ready;
                }else {
                $DtoInterventions->ready = false;
            }
            if (isset($InterventionAll->unrepairable)) {
                $DtoInterventions->unrepairable = $InterventionAll->unrepairable;
            } else {
                $DtoInterventions->unrepairable = '';
            }

            if (isset($InterventionAll->fileddt)) {
                $DtoInterventions->fileddt = $InterventionAll->fileddt;
            } else {
                $DtoInterventions->fileddt = '';
            }

            
            if (isset($InterventionAll->send_to_customer)) {
                $DtoInterventions->send_to_customer = $InterventionAll->send_to_customer;
            } else {
                $DtoInterventions->send_to_customer = '';
            }
            $CreateQuote = "Si";
            $CreateQuoteNo = "No";

            if ($InterventionAll->create_quote == "true") {
                $DtoInterventions->create_quote = $CreateQuote;
            } else {
                $DtoInterventions->create_quote = $CreateQuoteNo;
            }
            if (isset($InterventionAll->referent)) {
                $DtoInterventions->referent = $InterventionAll->referent;
            } else {
                $DtoInterventions->referent = '';
            }
            if (isset($InterventionAll->plant_type)) {
                $DtoInterventions->plant_type = $InterventionAll->plant_type;
            } else {
                $DtoInterventions->plant_type = '';
            }
            if (isset($InterventionAll->trolley_type)) {
                $DtoInterventions->trolley_type = $InterventionAll->trolley_type;
            } else {
                $DtoInterventions->trolley_type = '';
            }
            if (isset($InterventionAll->series)) {
                $DtoInterventions->series = $InterventionAll->series;
            } else {
                $DtoInterventions->series = '';
            }
            if (isset($InterventionAll->voltage)) {
                $DtoInterventions->voltage = $InterventionAll->voltage;
            } else {
                $DtoInterventions->voltage = '';
            }
            if (isset($InterventionAll->exit_notes)) {
                $DtoInterventions->exit_notes = $InterventionAll->exit_notes;
            } else {
                $DtoInterventions->exit_notes = '';
            }
          /*  if (isset($InterventionAll->imgmodule)) {
                $DtoInterventions->imgmodule = $InterventionAll->imgmodule;
            } else {
                $DtoInterventions->imgmodule = '';
            }*/

             if (isset($InterventionAll->code_tracking)) {
                $DtoInterventions->code_tracking = $InterventionAll->code_tracking;
            } else {
                $DtoInterventions->code_tracking = '';
            }
            if (isset($InterventionAll->working_with_testing)) {
                $DtoInterventions->working_with_testing = $InterventionAll->working_with_testing;
              }else {
                $DtoInterventions->working_with_testing = false;
            }
             if (isset($InterventionAll->working_without_testing)) {
                $DtoInterventions->working_without_testing = $InterventionAll->working_without_testing;
             }else {
                $DtoInterventions->working_without_testing = false;
            }
             if (($InterventionAll->plant_type == '' || $InterventionAll->plant_type == null) && 
                ($InterventionAll->trolley_type == '' || $InterventionAll->trolley_type == null) && 
                ($InterventionAll->series == '' || $InterventionAll->series == null) && 
                ($InterventionAll->voltage == '' || $InterventionAll->voltage == null)) {
                    $DtoInterventions->checkModuleFault = false;
            }else{
                    $DtoInterventions->checkModuleFault = true;
            }

             $ModuleValidationGuarantee = ModuleValidationGuarantee::where('id_intervention', $InterventionAll->id)->first();
            if (isset($ModuleValidationGuarantee)) { 
               
                    $DtoInterventions->checkModuleValidationGuarantee = true; 
            }else{
                    $DtoInterventions->checkModuleValidationGuarantee = false;   
            }

              
            if (file_exists(static::$dirImageGuasti . $InterventionAll->code_intervention_gr . '.pdf' )) {
                $DtoInterventions->imgmodule = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/GUASTI/' . $InterventionAll->code_intervention_gr . '.pdf';
              } else {
                if ($InterventionAll->imgmodule != '' && !is_null($InterventionAll->imgmodule)) {
                            $DtoInterventions->imgmodule = $InterventionAll->imgmodule;
                        } else {
                            $DtoInterventions->imgmodule = '';
                        }
              }

          foreach (GrInterventionPhoto::where('intervention_id', $InterventionAll->id)->get() as $InterventionCaptures) {
                $DtoInterventionsPhotos = new DtoInterventionsPhotos();   
                  if (file_exists(static::$dirImageIntervention . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.jpg' )) {
                      $DtoInterventionsPhotos->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.jpg';
                  } else {
                    if (file_exists(static::$dirImageIntervention . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.JPG' )) {
                      $DtoInterventionsPhotos->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.JPG';
                    }else{
                        if ($InterventionCaptures->img != '' && !is_null($InterventionCaptures->img)) {
                          $pos = strpos($InterventionCaptures->img, $InterventionCaptures->intervention_id);
                          if($pos === false){
                            $DtoInterventionsPhotos->img = '';
                          }else{
                            $DtoInterventionsPhotos->img = $InterventionCaptures->img;
                          }
                      } else {
                          $DtoInterventionsPhotos->img = '';
                      }
                    }  
                }
                  if (file_exists(static::$dirImageInterventionReportTest . $InterventionCaptures->code_intervention_gr . '.pdf' )) {
                    $DtoInterventionsPhotos->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/REPORT TEST/' . $InterventionCaptures->code_intervention_gr . '.pdf';
                } else {
                    if ($InterventionCaptures->img_rep_test != '' && !is_null($InterventionCaptures->img_rep_test)) {
                      // if ($InterventionCaptures->img_rep_test != '' && $InterventionCaptures->img_rep_test != null) {
                            $DtoInterventionsPhotos->img_rep_test = $InterventionCaptures->img_rep_test;
                        } else {
                            $DtoInterventionsPhotos->img_rep_test = '';
                        }
                }      
                $DtoInterventions->captures->push($DtoInterventionsPhotos);
            }
           
         /* foreach (GrInterventionPhoto::where('intervention_id', $InterventionAll->id)->get() as $InterventionCaptures) {
                $DtoInterventionsPhotos = new DtoInterventionsPhotos();
                $DtoInterventionsPhotos->img = $InterventionCaptures->img;
                if (isset($InterventionCaptures->img_rep_test)) {
                    $DtoInterventionsPhotos->img_rep_test = $InterventionCaptures->img_rep_test;
                } else {
                    $DtoInterventionsPhotos->img_rep_test = '';
                }
                $DtoInterventions->captures->push($DtoInterventionsPhotos);
            }*/
            //end valori dettaglio 
            $result->push($DtoInterventions);
        }
        
        return $result;
  }

/**
     * SearchOpen
     * 
     * @param Request request
     * 
     */
    public static function SearchOpen(request $request){
        $result = collect();
        
        foreach (DB::select(
               'SELECT gr_interventions.*
                FROM gr_interventions
                WHERE gr_interventions.date_ddt_gr is null 
                and gr_interventions.nr_ddt_gr is null
                order by id desc
                limit 150'        
            ) as $InterventionAll) {

            $idInterventionExternalRepair = ExternalRepair::where('id_intervention', $InterventionAll->id)->first();
            $DtoInterventions = new DtoInterventions();

            $CustomerId = $InterventionAll->customers_id;
            //  $CustomerDescription = User::where('id', $InterventionAll->customers_id)->first()->name;
            $CustomerDescription = UsersDatas::where('user_id', $InterventionAll->user_id)->first();

          //  $CustomerDescription = User::where('id', $InterventionAll->user_id)->first();
            $UserId = User::where('id', $InterventionAll->user_id)->first();
            $StatesDescription = GrStates::where('id', $InterventionAll->states_id)->first();
            $CodeGr = ItemGr::where('id', $InterventionAll->items_id)->first();
            $ItemId = ItemLanguage::where('item_id', $InterventionAll->items_id)->first();
            $Quotes = GrQuotes::where('id_intervention', $InterventionAll->id)->first();
            //$DtoInterventions->code_product = $ItemId;

            if (isset($Quotes->id_states_quote)) {
                $DtoInterventions->id_states_quote = StatesQuotesGr::where('id', $Quotes->id_states_quote)->first()->description;
            } else {
                $DtoInterventions->id_states_quote = '';
            }
            if (isset($InterventionAll->code_intervention_gr)) {
                $DtoInterventions->code_intervention_gr = $InterventionAll->code_intervention_gr;  
            }
             if (isset($InterventionAll->rip_association)) {
                $DtoInterventions->rip_association = '<br><label style="color:red">Riparazione Associata:</label>' . $InterventionAll->rip_association . '';    
            }
            $Gr_InterventionProcessing = GrInterventionProcessing::where('id_intervention', $InterventionAll->id)->first();
            if (isset($Gr_InterventionProcessing->id_intervention)) {
                $DtoInterventions->checkProcessingExist = true;
            }else{
                $DtoInterventions->checkProcessingExist = false;
            }
           
           
            if (isset($idInterventionExternalRepair->id)) {
                $DtoInterventions->idInterventionExternalRepair = $idInterventionExternalRepair->id;
            }
            if (isset($idInterventionExternalRepair->date_return_product_from_the_supplier)) {
                $DtoInterventions->date_return_product_from_the_supplier = $idInterventionExternalRepair->date_return_product_from_the_supplier;
            }
            if (isset($idInterventionExternalRepair->ddt_supplier)) {
                $DtoInterventions->ddt_supplier = $idInterventionExternalRepair->ddt_supplier;
            }
            //valori tabella 
            if (isset($InterventionAll->id)) {
                $DtoInterventions->idIntervention = $InterventionAll->id;
                $DtoInterventions->id = $InterventionAll->id;
            }
            if (isset($StatesDescription->description)) {
                $DtoInterventions->descriptionStates =  $StatesDescription->description;
            } else {
                $DtoInterventions->descriptionStates = '';
            }
          /*  if (isset($CustomerDescription->business_name)) {
                $DtoInterventions->descriptionCustomer =  $CustomerDescription->business_name;
            } else {
                $DtoInterventions->descriptionCustomer = '';
            }*/

            if (isset($InterventionAll->user_id)) {
                $userdatas = UsersDatas::where('user_id', $InterventionAll->user_id)->first();
              if (isset($userdatas->business_name)){
                  $DtoInterventions->descriptionCustomer = $userdatas->business_name;
              }else{
                  $DtoInterventions->descriptionCustomer =  $userdatas->name . ' ' . $userdatas->surname; 
              }
            } else {
                    $DtoInterventions->user_id = '';
            }


            if (isset($CodeGr->code_gr)) {
                $DtoInterventions->codeGr =  $CodeGr->code_gr;
            } else {
                $DtoInterventions->codeGr = '';
            }
            //end valori tabella 
            //valori dettaglio
            if (isset($CodeGr->id)) {
                $DtoInterventions->items_id = $CodeGr->id;
            } else {
                $DtoInterventions->items_id = '';
            }
            if (isset($ItemId->id)) {
                $DtoInterventions->code_product = $ItemId->id;
            } else {
                $DtoInterventions->code_product = '';
            }
            if (isset($UserId->id)) {
                $DtoInterventions->user_id = $UserId->id;
            } else {
                $DtoInterventions->user_id = '';
            }
            if (isset($StatesDescription->id)) {
                $DtoInterventions->states_id =  $StatesDescription->id;
            } else {
                $DtoInterventions->states_id = '';
            }

            if (isset($InterventionAll->description)) {
                $DtoInterventions->description = $InterventionAll->description;
            } else {
                $DtoInterventions->description = '';
            }
            if (isset($InterventionAll->defect)) {
                $DtoInterventions->defect = $InterventionAll->defect;
            } else {
                $DtoInterventions->defect = '';
            }
            if (isset($InterventionAll->nr_ddt)) {
                $DtoInterventions->nr_ddt = $InterventionAll->nr_ddt;
            } else {
                $DtoInterventions->nr_ddt = '';
            }
            if (isset($InterventionAll->date_ddt)) {
                $DtoInterventions->date_ddt = str_replace('00:00:00', '', $InterventionAll->date_ddt);
            } else {
                $DtoInterventions->date_ddt = '';
            }
            if (isset($InterventionAll->nr_ddt_gr)) {
                $DtoInterventions->nr_ddt_gr = $InterventionAll->nr_ddt_gr;
            } else {
                $DtoInterventions->nr_ddt_gr = '';
            }
              $DtoInterventions->date_ddt_gr = str_replace('00:00:00', '', $InterventionAll->date_ddt_gr);

            if (isset($InterventionAll->ready)) {
                 $DtoInterventions->ready = $InterventionAll->ready;
                }else {
                $DtoInterventions->ready = false;
            }
            if (isset($InterventionAll->unrepairable)) {
                $DtoInterventions->unrepairable = $InterventionAll->unrepairable;
            } else {
                $DtoInterventions->unrepairable = '';
            }
            
            if (isset($InterventionAll->send_to_customer)) {
                $DtoInterventions->send_to_customer = $InterventionAll->send_to_customer;
            } else {
                $DtoInterventions->send_to_customer = '';
            }
            $CreateQuote = "Si";
            $CreateQuoteNo = "No";

            if ($InterventionAll->create_quote == "true") {
                $DtoInterventions->create_quote = $CreateQuote;
            } else {
                $DtoInterventions->create_quote = $CreateQuoteNo;
            }
            if (isset($InterventionAll->referent)) {
                $DtoInterventions->referent = $InterventionAll->referent;
            } else {
                $DtoInterventions->referent = '';
            }
            if (isset($InterventionAll->plant_type)) {
                $DtoInterventions->plant_type = $InterventionAll->plant_type;
            } else {
                $DtoInterventions->plant_type = '';
            }
            if (isset($InterventionAll->trolley_type)) {
                $DtoInterventions->trolley_type = $InterventionAll->trolley_type;
            } else {
                $DtoInterventions->trolley_type = '';
            }
            if (isset($InterventionAll->series)) {
                $DtoInterventions->series = $InterventionAll->series;
            } else {
                $DtoInterventions->series = '';
            }
            if (isset($InterventionAll->voltage)) {
                $DtoInterventions->voltage = $InterventionAll->voltage;
            } else {
                $DtoInterventions->voltage = '';
            }
            if (isset($InterventionAll->note_internal_gr)) {
                $DtoInterventions->note_internal_gr = $InterventionAll->note_internal_gr;
            } else {
                $DtoInterventions->note_internal_gr = '';
            }
           /* if (isset($InterventionAll->imgmodule)) {
                $DtoInterventions->imgmodule = $InterventionAll->imgmodule;
            } else {
                $DtoInterventions->imgmodule = '';
            }*/
             if (isset($InterventionAll->code_tracking)) {
                $DtoInterventions->code_tracking = $InterventionAll->code_tracking;
            } else {
                $DtoInterventions->code_tracking = '';
            }
            if (isset($InterventionAll->fileddt)) {
                $DtoInterventions->fileddt = $InterventionAll->fileddt;
            } else {
                $DtoInterventions->fileddt = '';
            }

           if (isset($InterventionAll->working_with_testing)) {
                $DtoInterventions->working_with_testing = $InterventionAll->working_with_testing;
              }else {
                $DtoInterventions->working_with_testing = false;
            }
             if (isset($InterventionAll->working_without_testing)) {
                $DtoInterventions->working_without_testing = $InterventionAll->working_without_testing;
             }else {
                $DtoInterventions->working_without_testing = false;
            }
             if (($InterventionAll->plant_type == '' || $InterventionAll->plant_type == null) && 
                ($InterventionAll->trolley_type == '' || $InterventionAll->trolley_type == null) && 
                ($InterventionAll->series == '' || $InterventionAll->series == null) && 
                ($InterventionAll->voltage == '' || $InterventionAll->voltage == null)) {
                    $DtoInterventions->checkModuleFault = false;
            }else{
                    $DtoInterventions->checkModuleFault = true;
            }

             $ModuleValidationGuarantee = ModuleValidationGuarantee::where('id_intervention', $InterventionAll->id)->first();
            if (isset($ModuleValidationGuarantee)) { 
               
                    $DtoInterventions->checkModuleValidationGuarantee = true; 
            }else{
                    $DtoInterventions->checkModuleValidationGuarantee = false;   
            }

              
            if (file_exists(static::$dirImageGuasti . $InterventionAll->code_intervention_gr . '.pdf' )) {
                $DtoInterventions->imgmodule = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/GUASTI/' . $InterventionAll->code_intervention_gr . '.pdf';
              } else {
                if ($InterventionAll->imgmodule != '' && !is_null($InterventionAll->imgmodule)) {
                            $DtoInterventions->imgmodule = $InterventionAll->imgmodule;
                        } else {
                            $DtoInterventions->imgmodule = '';
                        }
              }

          foreach (GrInterventionPhoto::where('intervention_id', $InterventionAll->id)->get() as $InterventionCaptures) {
                $DtoInterventionsPhotos = new DtoInterventionsPhotos();   
                  if (file_exists(static::$dirImageIntervention . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.jpg' )) {
                      $DtoInterventionsPhotos->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.jpg';
                  } else {
                    if (file_exists(static::$dirImageIntervention . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.JPG' )) {
                      $DtoInterventionsPhotos->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.JPG';
                    }else{
                        if ($InterventionCaptures->img != '' && !is_null($InterventionCaptures->img)) {
                          $pos = strpos($InterventionCaptures->img, $InterventionCaptures->intervention_id);
                          if($pos === false){
                            $DtoInterventionsPhotos->img = '';
                          }else{
                            $DtoInterventionsPhotos->img = $InterventionCaptures->img;
                          }
                      } else {
                          $DtoInterventionsPhotos->img = '';
                      }
                    }  
                }
                  if (file_exists(static::$dirImageInterventionReportTest . $InterventionCaptures->code_intervention_gr . '.pdf' )) {
                    $DtoInterventionsPhotos->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/REPORT TEST/' . $InterventionCaptures->code_intervention_gr . '.pdf';
                } else {
                    if ($InterventionCaptures->img_rep_test != '' && !is_null($InterventionCaptures->img_rep_test)) {
                      // if ($InterventionCaptures->img_rep_test != '' && $InterventionCaptures->img_rep_test != null) {
                            $DtoInterventionsPhotos->img_rep_test = $InterventionCaptures->img_rep_test;
                        } else {
                            $DtoInterventionsPhotos->img_rep_test = '';
                        }
                }      
                $DtoInterventions->captures->push($DtoInterventionsPhotos);
            }
           
          /*foreach (GrInterventionPhoto::where('intervention_id', $InterventionAll->id)->get() as $InterventionCaptures) {
                $DtoInterventionsPhotos = new DtoInterventionsPhotos();
                $DtoInterventionsPhotos->img = $InterventionCaptures->img;
                if (isset($InterventionCaptures->img_rep_test)) {
                    $DtoInterventionsPhotos->img_rep_test = $InterventionCaptures->img_rep_test;
                } else {
                    $DtoInterventionsPhotos->img_rep_test = '';
                }
                $DtoInterventions->captures->push($DtoInterventionsPhotos);
            }*/
            //end valori dettaglio 
            $result->push($DtoInterventions);
        }
        
        return $result;
  }

/**
     * SearchClose
     * 
     * @param Request request
     * 
     */
    public static function SearchClose(request $request){
        $result = collect();
        foreach (DB::select(
                'SELECT gr_interventions.*
                FROM gr_interventions
                WHERE gr_interventions.date_ddt_gr is not null 
                and gr_interventions.nr_ddt_gr is not null
                order by id desc
                limit 150'  
            ) as $InterventionAll) {

            $idInterventionExternalRepair = ExternalRepair::where('id_intervention', $InterventionAll->id)->first();
            $DtoInterventions = new DtoInterventions();

            $CustomerId = $InterventionAll->customers_id;
            //  $CustomerDescription = User::where('id', $InterventionAll->customers_id)->first()->name;
            //$CustomerDescription = User::where('id', $InterventionAll->user_id)->first();
            $CustomerDescription = UsersDatas::where('user_id', $InterventionAll->user_id)->first();

            $UserId = User::where('id', $InterventionAll->user_id)->first();
            $StatesDescription = GrStates::where('id', $InterventionAll->states_id)->first();
            $CodeGr = ItemGr::where('id', $InterventionAll->items_id)->first();
            $ItemId = ItemLanguage::where('item_id', $InterventionAll->items_id)->first();
            $Quotes = GrQuotes::where('id_intervention', $InterventionAll->id)->first();
            //$DtoInterventions->code_product = $ItemId;

            if (isset($Quotes->id_states_quote)) {
                $DtoInterventions->id_states_quote = StatesQuotesGr::where('id', $Quotes->id_states_quote)->first()->description;
            } else {
                $DtoInterventions->id_states_quote = '';
            }
            if (isset($idInterventionExternalRepair->id)) {
                $DtoInterventions->idInterventionExternalRepair = $idInterventionExternalRepair->id;
            }
            if (isset($idInterventionExternalRepair->date_return_product_from_the_supplier)) {
                $DtoInterventions->date_return_product_from_the_supplier = $idInterventionExternalRepair->date_return_product_from_the_supplier;
            }
            if (isset($idInterventionExternalRepair->ddt_supplier)) {
                $DtoInterventions->ddt_supplier = $idInterventionExternalRepair->ddt_supplier;
            }
            $Gr_InterventionProcessing = GrInterventionProcessing::where('id_intervention', $InterventionAll->id)->first();
            if (isset($Gr_InterventionProcessing->id_intervention)) {
                $DtoInterventions->checkProcessingExist = true;
            }else{
                $DtoInterventions->checkProcessingExist = false;
            }

            //valori tabella 
            if (isset($InterventionAll->id)) {
                $DtoInterventions->idIntervention = $InterventionAll->id;
                $DtoInterventions->id = $InterventionAll->id;
            }
            if (isset($InterventionAll->code_intervention_gr)) {
                $DtoInterventions->code_intervention_gr = $InterventionAll->code_intervention_gr;  
            }
             if (isset($InterventionAll->rip_association)) {
                $DtoInterventions->rip_association = '<br><label style="color:red">Riparazione Associata:</label>' . $InterventionAll->rip_association . '';    
            }

            if (isset($StatesDescription->description)) {
                $DtoInterventions->descriptionStates =  $StatesDescription->description;
            } else {
                $DtoInterventions->descriptionStates = '';
            }
          /*  if (isset($CustomerDescription->business_name)) {
                $DtoInterventions->descriptionCustomer =  $CustomerDescription->business_name;
            } else {
                $DtoInterventions->descriptionCustomer = '';
            }*/

            if (isset($InterventionAll->user_id)) {
                $userdatas = UsersDatas::where('user_id', $InterventionAll->user_id)->first();
              if (isset($userdatas->business_name)){
                  $DtoInterventions->descriptionCustomer = $userdatas->business_name;
              }else{
                  $DtoInterventions->descriptionCustomer =  $userdatas->name . ' ' . $userdatas->surname; 
                }
            } else {
                    $DtoInterventions->user_id = '';
            }

            if (isset($CodeGr->code_gr)) {
                $DtoInterventions->codeGr =  $CodeGr->code_gr;
            } else {
                $DtoInterventions->codeGr = '';
            }
            //end valori tabella 
            //valori dettaglio
            if (isset($CodeGr->id)) {
                $DtoInterventions->items_id = $CodeGr->id;
            } else {
                $DtoInterventions->items_id = '';
            }
            if (isset($ItemId->id)) {
                $DtoInterventions->code_product = $ItemId->id;
            } else {
                $DtoInterventions->code_product = '';
            }
            if (isset($UserId->id)) {
                $DtoInterventions->user_id = $UserId->id;
            } else {
                $DtoInterventions->user_id = '';
            }
            if (isset($StatesDescription->id)) {
                $DtoInterventions->states_id =  $StatesDescription->id;
            } else {
                $DtoInterventions->states_id = '';
            }

            if (isset($InterventionAll->description)) {
                $DtoInterventions->description = $InterventionAll->description;
            } else {
                $DtoInterventions->description = '';
            }
            if (isset($InterventionAll->defect)) {
                $DtoInterventions->defect = $InterventionAll->defect;
            } else {
                $DtoInterventions->defect = '';
            }
            if (isset($InterventionAll->nr_ddt)) {
                $DtoInterventions->nr_ddt = $InterventionAll->nr_ddt;
            } else {
                $DtoInterventions->nr_ddt = '';
            }
            if (isset($InterventionAll->date_ddt)) {
                $DtoInterventions->date_ddt = str_replace('00:00:00', '', $InterventionAll->date_ddt);
            } else {
                $DtoInterventions->date_ddt = '';
            }
            if (isset($InterventionAll->nr_ddt_gr)) {
                $DtoInterventions->nr_ddt_gr = $InterventionAll->nr_ddt_gr;
            } else {
                $DtoInterventions->nr_ddt_gr = '';
            }
              $DtoInterventions->date_ddt_gr = str_replace('00:00:00', '', $InterventionAll->date_ddt_gr);

            if (isset($InterventionAll->ready)) {
                 $DtoInterventions->ready = $InterventionAll->ready;
                }else {
                $DtoInterventions->ready = false;
            }
            if (isset($InterventionAll->unrepairable)) {
                $DtoInterventions->unrepairable = $InterventionAll->unrepairable;
            } else {
                $DtoInterventions->unrepairable = '';
            }
            
            if (isset($InterventionAll->send_to_customer)) {
                $DtoInterventions->send_to_customer = $InterventionAll->send_to_customer;
            } else {
                $DtoInterventions->send_to_customer = '';
            }
            $CreateQuote = "Si";
            $CreateQuoteNo = "No";

            if (isset($InterventionAll->fileddt)) {
                $DtoInterventions->fileddt = $InterventionAll->fileddt;
            } else {
                $DtoInterventions->fileddt = '';
            }


            if ($InterventionAll->create_quote == "true") {
                $DtoInterventions->create_quote = $CreateQuote;
            } else {
                $DtoInterventions->create_quote = $CreateQuoteNo;
            }
            if (isset($InterventionAll->referent)) {
                $DtoInterventions->referent = $InterventionAll->referent;
            } else {
                $DtoInterventions->referent = '';
            }
            if (isset($InterventionAll->plant_type)) {
                $DtoInterventions->plant_type = $InterventionAll->plant_type;
            } else {
                $DtoInterventions->plant_type = '';
            }
            if (isset($InterventionAll->trolley_type)) {
                $DtoInterventions->trolley_type = $InterventionAll->trolley_type;
            } else {
                $DtoInterventions->trolley_type = '';
            }
            if (isset($InterventionAll->series)) {
                $DtoInterventions->series = $InterventionAll->series;
            } else {
                $DtoInterventions->series = '';
            }
            if (isset($InterventionAll->voltage)) {
                $DtoInterventions->voltage = $InterventionAll->voltage;
            } else {
                $DtoInterventions->voltage = '';
            }
            if (isset($InterventionAll->note_internal_gr)) {
                $DtoInterventions->note_internal_gr = $InterventionAll->note_internal_gr;
            } else {
                $DtoInterventions->note_internal_gr = '';
            }
          /*  if (isset($InterventionAll->imgmodule)) {
                $DtoInterventions->imgmodule = $InterventionAll->imgmodule;
            } else {
                $DtoInterventions->imgmodule = '';
            }*/

             if (isset($InterventionAll->code_tracking)) {
                $DtoInterventions->code_tracking = $InterventionAll->code_tracking;
            } else {
                $DtoInterventions->code_tracking = '';
            }

              if (isset($InterventionAll->working_with_testing)) {
                $DtoInterventions->working_with_testing = $InterventionAll->working_with_testing;
              }else {
                $DtoInterventions->working_with_testing = false;
            }
             if (isset($InterventionAll->working_without_testing)) {
                $DtoInterventions->working_without_testing = $InterventionAll->working_without_testing;
             }else {
                $DtoInterventions->working_without_testing = false;
            }
             if (($InterventionAll->plant_type == '' || $InterventionAll->plant_type == null) && 
                ($InterventionAll->trolley_type == '' || $InterventionAll->trolley_type == null) && 
                ($InterventionAll->series == '' || $InterventionAll->series == null) && 
                ($InterventionAll->voltage == '' || $InterventionAll->voltage == null)) {
                    $DtoInterventions->checkModuleFault = false;
            }else{
                    $DtoInterventions->checkModuleFault = true;
            }

             $ModuleValidationGuarantee = ModuleValidationGuarantee::where('id_intervention', $InterventionAll->id)->first();
            if (isset($ModuleValidationGuarantee)) { 
               
                    $DtoInterventions->checkModuleValidationGuarantee = true; 
            }else{
                    $DtoInterventions->checkModuleValidationGuarantee = false;   
            }
              
            if (file_exists(static::$dirImageGuasti . $InterventionAll->code_intervention_gr . '.pdf' )) {
                $DtoInterventions->imgmodule = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/GUASTI/' . $InterventionAll->code_intervention_gr . '.pdf';
              } else {
                if ($InterventionAll->imgmodule != '' && !is_null($InterventionAll->imgmodule)) {
                            $DtoInterventions->imgmodule = $InterventionAll->imgmodule;
                        } else {
                            $DtoInterventions->imgmodule = '';
                        }
              }

          foreach (GrInterventionPhoto::where('intervention_id', $InterventionAll->id)->get() as $InterventionCaptures) {
                $DtoInterventionsPhotos = new DtoInterventionsPhotos();   
                  if (file_exists(static::$dirImageIntervention . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.jpg' )) {
                      $DtoInterventionsPhotos->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.jpg';
                  } else {
                    if (file_exists(static::$dirImageIntervention . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.JPG' )) {
                      $DtoInterventionsPhotos->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.JPG';
                    }else{
                        if ($InterventionCaptures->img != '' && !is_null($InterventionCaptures->img)) {
                          $pos = strpos($InterventionCaptures->img, $InterventionCaptures->intervention_id);
                          if($pos === false){
                            $DtoInterventionsPhotos->img = '';
                          }else{
                            $DtoInterventionsPhotos->img = $InterventionCaptures->img;
                          }
                      } else {
                          $DtoInterventionsPhotos->img = '';
                      }
                    }  
                }
                  if (file_exists(static::$dirImageInterventionReportTest . $InterventionCaptures->code_intervention_gr . '.pdf' )) {
                    $DtoInterventionsPhotos->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/REPORT TEST/' . $InterventionCaptures->code_intervention_gr . '.pdf';
                } else {
                    if ($InterventionCaptures->img_rep_test != '' && !is_null($InterventionCaptures->img_rep_test)) {
                      // if ($InterventionCaptures->img_rep_test != '' && $InterventionCaptures->img_rep_test != null) {
                            $DtoInterventionsPhotos->img_rep_test = $InterventionCaptures->img_rep_test;
                        } else {
                            $DtoInterventionsPhotos->img_rep_test = '';
                        }
                }      
                $DtoInterventions->captures->push($DtoInterventionsPhotos);
            }
           
        /*  foreach (GrInterventionPhoto::where('intervention_id', $InterventionAll->id)->get() as $InterventionCaptures) {
                $DtoInterventionsPhotos = new DtoInterventionsPhotos();
                $DtoInterventionsPhotos->img = $InterventionCaptures->img;
                if (isset($InterventionCaptures->img_rep_test)) {
                    $DtoInterventionsPhotos->img_rep_test = $InterventionCaptures->img_rep_test;
                } else {
                    $DtoInterventionsPhotos->img_rep_test = '';
                }
                $DtoInterventions->captures->push($DtoInterventionsPhotos);
            }*/
            //end valori dettaglio 
            $result->push($DtoInterventions);
        }
        
        return $result;
  }

    /**
     * getAllIntervention
     * 
     * 
     * @return DtoInterventions
     */
    public static function getAllIntervention($idLanguage)
    {
        $result = collect();
        $repair_arrived = true;

        foreach (GrIntervention::
        where('repaired_arrived', $repair_arrived)
        ->orwhereNotNull('date_ddt_gr')
        ->orwhereNotNull('nr_ddt_gr')
        ->orwhereNotNull('description')
        ->orwhereNotNull('items_id')
        ->orderBy('code_intervention_gr', 'desc')->take(20)->get() as $InterventionAll) {
            $idInterventionExternalRepair = ExternalRepair::where('id_intervention', $InterventionAll->id)->first();
            $DtoInterventions = new DtoInterventions();

            $CustomerId = $InterventionAll->customers_id;
            //  $CustomerDescription = User::where('id', $InterventionAll->customers_id)->first()->name;
            //$CustomerDescription = UsersDatas::where('user_id', $InterventionAll->user_id)->first();
            $UserId = User::where('id', $InterventionAll->user_id)->first();
            $StatesDescription = GrStates::where('id', $InterventionAll->states_id)->first();
            $CodeGr = ItemGr::where('id', $InterventionAll->items_id)->first();
            $ItemId = ItemLanguage::where('item_id', $InterventionAll->items_id)->first();
            $Quotes = GrQuotes::where('id_intervention', $InterventionAll->id)->first();
            //$DtoInterventions->code_product = $ItemId;

            if (isset($Quotes->id_states_quote)) {
                $DtoInterventions->id_states_quote = StatesQuotesGr::where('id', $Quotes->id_states_quote)->first()->description;
            } else {
                $DtoInterventions->id_states_quote = '';
            }

            $Gr_InterventionProcessing = GrInterventionProcessing::where('id_intervention', $InterventionAll->id)->first();
            if (isset($Gr_InterventionProcessing->id_intervention)) {
                $DtoInterventions->checkProcessingExist = true;
            }else{
                $DtoInterventions->checkProcessingExist = false;
            }

            if (isset($idInterventionExternalRepair->id)) {
                $DtoInterventions->idInterventionExternalRepair = $idInterventionExternalRepair->id;
            }
            if (isset($idInterventionExternalRepair->date_return_product_from_the_supplier)) {
                $DtoInterventions->date_return_product_from_the_supplier = $idInterventionExternalRepair->date_return_product_from_the_supplier;
            }
            if (isset($idInterventionExternalRepair->ddt_supplier)) {
                $DtoInterventions->ddt_supplier = $idInterventionExternalRepair->ddt_supplier;
            }
            //valori tabella 
            if (isset($InterventionAll->id)) {
                $DtoInterventions->idIntervention = $InterventionAll->id;
                $DtoInterventions->id = $InterventionAll->id;
            }
            if (isset($StatesDescription->description)) {
                $DtoInterventions->descriptionStates =  $StatesDescription->description;
            } else {
                $DtoInterventions->descriptionStates = '';
            }

            if (isset($InterventionAll->user_id)) {
                $userdatas = UsersDatas::where('user_id', $InterventionAll->user_id)->first();
              if (isset($userdatas->business_name)){
                  $DtoInterventions->descriptionCustomer = $userdatas->business_name;
              }else{
                  $DtoInterventions->descriptionCustomer =  $userdatas->name . ' ' . $userdatas->surname; 
              }
            } else {
                    $DtoInterventions->user_id = '';
                    $DtoInterventions->descriptionCustomer = '';
            }

           
           /* if (isset($CustomerDescription->business_name)) {
                $DtoInterventions->descriptionCustomer =  $CustomerDescription->business_name;
            } else {
                $DtoInterventions->descriptionCustomer = '';
            }*/
            if (isset($CodeGr->code_gr)) {
                $DtoInterventions->codeGr =  $CodeGr->code_gr;
            } else {
                $DtoInterventions->codeGr = '';
            }
            if (isset($InterventionAll->code_intervention_gr)) {
                $DtoInterventions->code_intervention_gr = $InterventionAll->code_intervention_gr;  
            }else {
                $DtoInterventions->code_intervention_gr = '';
            }
            if (isset($InterventionAll->rip_association)) {
                $DtoInterventions->rip_association = '<br><label style="color:red">Riparazione Associata:</label>' . $InterventionAll->rip_association . '';    
            }else {
                $DtoInterventions->rip_association = '';
            }
           

            //end valori tabella 
            //valori dettaglio
            if (isset($CodeGr->id)) {
                $DtoInterventions->items_id = $CodeGr->id;
            } else {
                $DtoInterventions->items_id = '';
            }
            if (isset($ItemId->id)) {
                $DtoInterventions->code_product = $ItemId->id;
            } else {
                $DtoInterventions->code_product = '';
            }
            if (isset($UserId->id)) {
                $DtoInterventions->user_id = $UserId->id;
            } else {
                $DtoInterventions->user_id = '';
            }

            if (isset($InterventionAll->fileddt)) {
                $DtoInterventions->fileddt = $InterventionAll->fileddt;
            } else {
                $DtoInterventions->fileddt = '';
            }
            
            //  if (isset($CustomerId)) {
            //   $DtoInterventions->customer_id = $CustomerId;
            // }else{
            //$DtoInterventions->customer_id = '-';
            //  }

            if (isset($StatesDescription->id)) {
                $DtoInterventions->states_id =  $StatesDescription->id;
            } else {
                $DtoInterventions->states_id = '';
            }

            if (isset($InterventionAll->description)) {
                $DtoInterventions->description = $InterventionAll->description;
            } else {
                $DtoInterventions->description = '';
            }
            if (isset($InterventionAll->defect)) {
                $DtoInterventions->defect = $InterventionAll->defect;
            } else {
                $DtoInterventions->defect = '';
            }
            if (isset($InterventionAll->nr_ddt)) {
                $DtoInterventions->nr_ddt = $InterventionAll->nr_ddt;
            } else {
                $DtoInterventions->nr_ddt = '';
            }
            if (isset($InterventionAll->date_ddt)) {
                $DtoInterventions->date_ddt = str_replace('00:00', '', $InterventionAll->date_ddt);
            } else {
                $DtoInterventions->date_ddt = '';
            }
            if (isset($InterventionAll->nr_ddt_gr)) {
                $DtoInterventions->nr_ddt_gr = $InterventionAll->nr_ddt_gr;
            } else {
                $DtoInterventions->nr_ddt_gr = '';
            }
            //if (isset($InterventionAll->date_ddt_gr) || $InterventionAll->date_ddt_gr != '') {
             if (isset($InterventionAll->date_ddt_gr)) {
              $DtoInterventions->date_ddt_gr = str_replace('00:00', '', $InterventionAll->date_ddt_gr);
           } else {
                $DtoInterventions->date_ddt_gr = '';
            }

            if (isset($InterventionAll->ready)) {
                 $DtoInterventions->ready = $InterventionAll->ready;
                }else {
                $DtoInterventions->ready = false;
            }

            if (isset($InterventionAll->unrepairable)) {
                $DtoInterventions->unrepairable = $InterventionAll->unrepairable;
            } else {
                $DtoInterventions->unrepairable = '';
            }
            
            if (isset($InterventionAll->send_to_customer)) {
                $DtoInterventions->send_to_customer = $InterventionAll->send_to_customer;
            } else {
                $DtoInterventions->send_to_customer = '';
            }

            if (($InterventionAll->plant_type == '' || $InterventionAll->plant_type == null) && 
                ($InterventionAll->trolley_type == '' || $InterventionAll->trolley_type == null) && 
                ($InterventionAll->series == '' || $InterventionAll->series == null) && 
                ($InterventionAll->voltage == '' || $InterventionAll->voltage == null)) {
                    $DtoInterventions->checkModuleFault = false;
            }else{
                    $DtoInterventions->checkModuleFault = true;
            }

             $ModuleValidationGuarantee = ModuleValidationGuarantee::where('id_intervention', $InterventionAll->id)->first();
             
            if (isset($ModuleValidationGuarantee)) { 
                    $DtoInterventions->checkModuleValidationGuarantee = true; 
            }else{
                    $DtoInterventions->checkModuleValidationGuarantee = false;   
            }

            $CreateQuote = "Si";
            $CreateQuoteNo = "No";

            if ($InterventionAll->create_quote == "true") {
                $DtoInterventions->create_quote = $CreateQuote;
            } else {
                $DtoInterventions->create_quote = $CreateQuoteNo;
            }
            if (isset($InterventionAll->referent)) {
                $DtoInterventions->referent = $InterventionAll->referent;
            } else {
                $DtoInterventions->referent = '';
            }
            if (isset($InterventionAll->plant_type)) {
                $DtoInterventions->plant_type = $InterventionAll->plant_type;
            } else {
                $DtoInterventions->plant_type = '';
            }
            if (isset($InterventionAll->trolley_type)) {
                $DtoInterventions->trolley_type = $InterventionAll->trolley_type;
            } else {
                $DtoInterventions->trolley_type = '';
            }
            if (isset($InterventionAll->series)) {
                $DtoInterventions->series = $InterventionAll->series;
            } else {
                $DtoInterventions->series = '';
            }
            if (isset($InterventionAll->voltage)) {
                $DtoInterventions->voltage = $InterventionAll->voltage;
            } else {
                $DtoInterventions->voltage = '';
            }
            if (isset($InterventionAll->note_internal_gr)) {
                $DtoInterventions->note_internal_gr = $InterventionAll->note_internal_gr;
            } else {
                $DtoInterventions->note_internal_gr = '';
            }
             if (isset($InterventionAll->code_tracking)) {
                $DtoInterventions->code_tracking = $InterventionAll->code_tracking;
            } else {
                $DtoInterventions->code_tracking = '';
            }

             if (isset($InterventionAll->working_with_testing)) {
                $DtoInterventions->working_with_testing = $InterventionAll->working_with_testing;
              }else {
                $DtoInterventions->working_with_testing = false;
            }
             if (isset($InterventionAll->working_without_testing)) {
                $DtoInterventions->working_without_testing = $InterventionAll->working_without_testing;
             }else {
                $DtoInterventions->working_without_testing = false;
            }

            if (file_exists(static::$dirImageGuasti . $InterventionAll->code_intervention_gr . '.pdf' )) {
                $DtoInterventions->imgmodule = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/GUASTI/' . $InterventionAll->code_intervention_gr . '.pdf';
              } else {
                if ($InterventionAll->imgmodule != '' && !is_null($InterventionAll->imgmodule)) {
                            $DtoInterventions->imgmodule = $InterventionAll->imgmodule;
                        } else {
                            $DtoInterventions->imgmodule = '';
                        }
              }

          foreach (GrInterventionPhoto::where('intervention_id', $InterventionAll->id)->get() as $InterventionCaptures) {
                $DtoInterventionsPhotos = new DtoInterventionsPhotos();   
                  if (file_exists(static::$dirImageIntervention . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.jpg' )) {
                      $DtoInterventionsPhotos->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.jpg';
                  } else {
                    if (file_exists(static::$dirImageIntervention . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.JPG' )) {
                      $DtoInterventionsPhotos->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $InterventionCaptures->img . $InterventionCaptures->code_intervention_gr . '.JPG';
                    }else{
                        if ($InterventionCaptures->img != '' && !is_null($InterventionCaptures->img)) {
                          $pos = strpos($InterventionCaptures->img, $InterventionCaptures->intervention_id);
                          if($pos === false){
                            $DtoInterventionsPhotos->img = '';
                          }else{
                            $DtoInterventionsPhotos->img = $InterventionCaptures->img;
                          }
                      } else {
                          $DtoInterventionsPhotos->img = '';
                      }
                    }  
                }
                  if (file_exists(static::$dirImageInterventionReportTest . $InterventionCaptures->code_intervention_gr . '.pdf' )) {
                    $DtoInterventionsPhotos->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/REPORT TEST/' . $InterventionCaptures->code_intervention_gr . '.pdf';
                } else {
                    if ($InterventionCaptures->img_rep_test != '' && !is_null($InterventionCaptures->img_rep_test)) {
                      // if ($InterventionCaptures->img_rep_test != '' && $InterventionCaptures->img_rep_test != null) {
                            $DtoInterventionsPhotos->img_rep_test = $InterventionCaptures->img_rep_test;
                        } else {
                            $DtoInterventionsPhotos->img_rep_test = '';
                        }
                }      
                $DtoInterventions->captures->push($DtoInterventionsPhotos);
            }
            //end valori dettaglio 
            $result->push($DtoInterventions);
        }
        return $result;
    }

        /* if(file_exists(static::$dirImage . $json['articoli'][$i]['codice'] . '.jpg')){
                    $filesystem->delete(static::getUrl() . $json['articoli'][$i]['codice'] . '.jpg');
                    Image::make($json['articoli'][$i]['immagini'][0])->save(static::$dirImage . $json['articoli'][$i]['codice'] . '.jpg');
                    $itemOnDb->img = static::getUrl() . $json['articoli'][$i]['codice'] . '.jpg';
                }else{
                    Image::make($json['articoli'][$i]['immagini'][0])->save(static::$dirImage . $json['articoli'][$i]['codice'] . '.jpg');
                    $itemOnDb->img = static::getUrl() . $json['articoli'][$i]['codice'] . '.jpg';
                }*/
    #endregion getAllIntervention

    #region UPDATE

    /**
     * Set completed
     * 
     * @param Request request
     */
    public static function complete(Request $request, int $idUser, int $idLanguage)
    {
        if (is_null($request->idItem)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
        }

        $item = Item::find($request->idItem);

        if (is_null($item)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ItemNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $item->complete_technical_specification = $request->complete;

        $item->save();
    }
    #endregion UPDATE

  #region insertModule
    /**
     * Insert
     * 
     * @param Request DtoInterventions
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insertModule(Request $DtoInterventions)
    {
        // if (strpos($DtoInterventions->imgmodule, '/storage/app/public/images/Items/') === false) {
        //  Image::make($DtoInterventions->imgmodule)->save(static::$dirImage . $DtoInterventions->imgName);    
        //  }
      //  Image::make($DtoInterventions->imgmodule)->save(static::$dirImageFaultModule . $DtoInterventions->imgName);
        //Image::make($DtoInterventions->fileddt)->save(static::$dirImageddt . $DtoInterventions->imgNames);
        DB::beginTransaction();
        try {
            $Intervention = new GrIntervention();
            if (isset($DtoInterventions->customer_id)) {
                $Intervention->customers_id = $DtoInterventions->customer_id;
            }
            if (isset($DtoInterventions->customer_id)) {
                $Intervention->user_id = $DtoInterventions->customer_id;
            }
            if (isset($DtoInterventions->description)) {
                $Intervention->description = $DtoInterventions->description;
            }
           if (isset($DtoInterventions->defect)) {
                $Intervention->defect = $DtoInterventions->defect;
            }
            if (isset($DtoInterventions->nr_ddt)) {
                $Intervention->nr_ddt = $DtoInterventions->nr_ddt;
            }
            if (isset($DtoInterventions->idLanguage)) {
                $Intervention->language_id = $DtoInterventions->idLanguage;
            }
            if (isset($DtoInterventions->date_ddt)) {
                $Intervention->date_ddt = $DtoInterventions->date_ddt;
            }
            if (isset($DtoInterventions->unrepairable)) {
                $Intervention->unrepairable = $DtoInterventions->unrepairable;
            }
            if (isset($DtoInterventions->send_to_customer)) {
                $Intervention->send_to_customer = $DtoInterventions->send_to_customer;
            }
            if (isset($DtoInterventions->create_quote)) {
                $Intervention->create_quote = $DtoInterventions->create_quote;
            }
            if (isset($DtoInterventions->external_referent_id)) {
                $Intervention->external_referent_id = $DtoInterventions->external_referent_id;
            }
            if (isset($DtoInterventions->plant_type)) {
                $Intervention->plant_type = $DtoInterventions->plant_type;
            }
            if (isset($DtoInterventions->trolley_type)) {
                $Intervention->trolley_type = $DtoInterventions->trolley_type;
            }
            if (isset($DtoInterventions->series)) {
                $Intervention->series = $DtoInterventions->series;
            }
        
            if (isset($DtoInterventions->voltage)) {
                $Intervention->voltage = $DtoInterventions->voltage;
            }
            if (isset($DtoInterventions->exit_notes)) {
                $Intervention->exit_notes = $DtoInterventions->exit_notes;
            }
           // $Intervention->imgmodule = static::getUrlModule() . $DtoInterventions->imgName;
         /*   if (!is_null($DtoInterventions->imgName)) {
        $data = base64_decode(str_replace('data:application/pdf;base64,', '', $DtoInterventions->imgmodule));
                file_put_contents(static::$dirImageFaultModule . $DtoInterventions->imgName, $data);
                }*/

            if (!is_null($DtoInterventions->imgNames)) {
                $Intervention->fileddt = static::getUrlDdt() . $DtoInterventions->imgNames;
                $data = base64_decode(str_replace('data:application/pdf;base64,', '', $DtoInterventions->fileddt));
                file_put_contents(static::$dirImageddt . $DtoInterventions->imgNames, $data);
            }else{
                    $DtoInterventions->fileddt ="";
            }
                $Intervention->repaired_arrived = false;
                $Intervention->states_id = 1; 
            $Intervention->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $Intervention->id;
    }


    #region INSERT
    /**
     * Insert
     * 
     * @param Request DtoInterventions
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoInterventions)
    {
        // if (strpos($DtoInterventions->imgmodule, '/storage/app/public/images/Items/') === false) {
        //  Image::make($DtoInterventions->imgmodule)->save(static::$dirImage . $DtoInterventions->imgName);    
        //  }

        DB::beginTransaction();
        try {
            $Intervention = new GrIntervention();
            if (isset($DtoInterventions->customer_id)) {
                $Intervention->customers_id = $DtoInterventions->customer_id;
            }
            if (isset($DtoInterventions->user_id)) {
                $Intervention->user_id = $DtoInterventions->user_id;
            }
            if (isset($DtoInterventions->states_id)) {
                $Intervention->states_id = $DtoInterventions->states_id;
            }
            if (isset($DtoInterventions->code_intervention_gr)) {
                $Intervention->code_intervention_gr = $DtoInterventions->code_intervention_gr;
            }          
            // if (isset($DtoInterventions->code_product)) {
            // $Intervention->items_id = ItemLanguage::where('id',$DtoInterventions->code_product)->first()->item_id; 
            // }
            // if (isset($DtoInterventions->items_id)) {
            // $Intervention->items_id = ItemLanguage::where('id',$DtoInterventions->items_id)->first()->item_id; 
            // }
            if (isset($DtoInterventions->items_id)) {
                $Intervention->items_id = $DtoInterventions->items_id;
            }
            if (isset($DtoInterventions->description)) {
                $Intervention->description = $DtoInterventions->description;
            }
           if (isset($DtoInterventions->defect)) {
                $Intervention->defect = $DtoInterventions->defect;
            }
            if (isset($DtoInterventions->nr_ddt)) {
                $Intervention->nr_ddt = $DtoInterventions->nr_ddt;
            }
            if (isset($DtoInterventions->nr_ddt_gr)) {
                $Intervention->nr_ddt_gr = $DtoInterventions->nr_ddt_gr;
            }
            if (isset($DtoInterventions->idLanguage)) {
                $Intervention->language_id = $DtoInterventions->idLanguage;
            }
            if (isset($DtoInterventions->date_ddt)) {
                $Intervention->date_ddt = $DtoInterventions->date_ddt;
            }
            if (isset($DtoInterventions->date_ddt_gr)) {
                $Intervention->date_ddt_gr = $DtoInterventions->date_ddt_gr;
            }
            if (isset($DtoInterventions->unrepairable)) {
                $Intervention->unrepairable = $DtoInterventions->unrepairable;
            }
            if (isset($DtoInterventions->send_to_customer)) {
                $Intervention->send_to_customer = $DtoInterventions->send_to_customer;
            }
            if (isset($DtoInterventions->create_quote)) {
                $Intervention->create_quote = $DtoInterventions->create_quote;
            }
            if (isset($DtoInterventions->referent)) {
                $Intervention->referent = $DtoInterventions->referent;
            }
            if (isset($DtoInterventions->plant_type)) {
                $Intervention->plant_type = $DtoInterventions->plant_type;
            }
            if (isset($DtoInterventions->trolley_type)) {
                $Intervention->trolley_type = $DtoInterventions->trolley_type;
            }
            if (isset($DtoInterventions->series)) {
                $Intervention->series = $DtoInterventions->series;
            }
            if (isset($DtoInterventions->voltage)) {
                $Intervention->voltage = $DtoInterventions->voltage;
            }
            if (isset($DtoInterventions->code_tracking)) {
                $Intervention->code_tracking = $DtoInterventions->code_tracking;
            }
            if (isset($DtoInterventions->rip_association)) {
                $Intervention->rip_association = $DtoInterventions->rip_association;
            }
            if (isset($DtoInterventions->note_internal_gr)) {
                $Intervention->note_internal_gr = $DtoInterventions->note_internal_gr;
            }  
            if (isset($DtoInterventions->external_referent_id_intervention)) {
                $Intervention->external_referent_id = $DtoInterventions->external_referent_id_intervention;
            } 
            if (isset($DtoInterventions->exit_notes)) {
                $Intervention->exit_notes = $DtoInterventions->exit_notes;
            }  
                $Intervention->preventivo_nuovo_creato = false;

            if (!is_null($DtoInterventions->imgName)) {
                $Intervention->imgmodule = static::getUrlModule() . $DtoInterventions->imgName;
                    $data = base64_decode(str_replace('data:application/pdf;base64,', '', $DtoInterventions->imgmodule));
                    file_put_contents(static::$dirImageFaultModule . $DtoInterventions->imgName, $data);
               // Image::make($DtoInterventions->imgmodule)->save(static::$dirImageFaultModule . $DtoInterventions->imgName);
              //  $interventionOnDb->imgmodule = static::getUrlModule() . $DtoInterventions->imgName;
            }
       // Image::make($DtoInterventions->imgmodule)->save(static::$dirImage . $DtoInterventions->imgName);
        //$Intervention->imgmodule = static::getUrlModule() . $DtoInterventions->imgName;
            //if (isset($DtoInterventions->imgName)) {
            //   Image::make($Intervention->imgmodule)->save(static::$dirImage . $DtoInterventions->imgName);
            // }
            //$Intervention->imgmodule = '/storage/images/Items/' . $Intervention->id . '_' . '.JPG';
            $Intervention->save();

            $cont = 0;
            if (isset($DtoInterventions->captures)) {
                foreach ($DtoInterventions->captures as $InterventionPhoto) {
                    $cont = $cont + 1;
                    Image::make($InterventionPhoto)->save(static::$dirImageIntervention . $Intervention->id . '_' . $cont . '.JPG');
                    $RowInterventionPhoto = new GrInterventionPhoto();
                    $RowInterventionPhoto->intervention_id = $Intervention->id;
                    $RowInterventionPhoto->img = '/storage/images/Intervention/' . $Intervention->id . '_' . $cont . '.JPG';
                    $RowInterventionPhoto->save();
                }
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $Intervention->id;
    }

    /**
     * Clone
     * 
     * @param int id
     * @param int idFunctionality
     * @param int idLanguage
     * @param int idUser
     * 
     * @return int
     */
    public static function clone(int $id, int $idFunctionality, int $idUser, int $idLanguage)
    {
        FunctionBL::checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        $Intervention = GrIntervention::find($id);
        $CustomerId = Customer::where('id', $Intervention->customers_id)->first();
        $GrInterventionsPhoto = GrInterventionPhoto::where('intervention_id', $id)->first();

        if (is_null($Intervention)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        DB::beginTransaction();

        try {

            $newInterventions = new GrIntervention();
            $newInterventions->customers_id =  $Intervention->customers_id;
            $newInterventions->items_id =  $Intervention->items_id;
            $newInterventions->states_id =  $Intervention->states_id;
            $newInterventions->description = $Intervention->description;
            $newInterventions->defect = $Intervention->defect;
            $newInterventions->nr_ddt = $Intervention->nr_ddt;
            $newInterventions->nr_ddt_gr = $Intervention->nr_ddt_gr;
            $newInterventions->language_id = $Intervention->language_id;
            $newInterventions->unrepairable = $Intervention->unrepairable;
            $newInterventions->send_to_customer = $Intervention->send_to_customer;
            $newInterventions->create_quote = $Intervention->create_quote;
            $newInterventions->date_ddt = $Intervention->date_ddt;
            $newInterventions->date_ddt_gr = $Intervention->date_ddt_gr;
            $newInterventions->referent = $Intervention->referent;
            $newInterventions->plant_type = $Intervention->plant_type;
            $newInterventions->trolley_type = $Intervention->trolley_type;
            $newInterventions->series = $Intervention->series;
            $newInterventions->voltage = $Intervention->voltage;
            $newInterventions->exit_notes = $Intervention->exit_notes;
            $newInterventions->imgmodule = $Intervention->imgmodule;

            $newInterventions->save();


            DB::commit();

            return $newInterventions->id;
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion INSERT

    #region UPDATE
    /**
     * Update
     * 
     * @param Request DtoCategories
     * @param int idLanguage
     */

    public static function update(Request $DtoInterventions)
    {
        static::_validateData($DtoInterventions, $DtoInterventions->idLanguage, DbOperationsTypesEnum::UPDATE);

        $interventionOnDb = GrIntervention::find($DtoInterventions->id);
        $interventionOnDbPhoto = GrInterventionPhoto::find($DtoInterventions->intervention_id);
        //$ItemsId = Item::where('id',  $interventionOnDb->items_id)->first()->id;
        //$customerId= User::where('id',$DtoInterventions->customer_id)->first();
        DB::beginTransaction();
        try {
            $interventionOnDb->language_id = $DtoInterventions->idLanguage;
            $interventionOnDb->customers_id = $DtoInterventions->customer_id;
            $interventionOnDb->states_id = $DtoInterventions->states_id;
            $interventionOnDb->items_id = ItemLanguage::where('id', $DtoInterventions->code_product)->first()->item_id;
            //$interventionOnDb->items_id = ItemLanguage::where('id',$DtoInterventions->items_id)->first()->item_id; 
            $interventionOnDb->description = $DtoInterventions->description;
            $interventionOnDb->defect = $DtoInterventions->defect;
            $interventionOnDb->nr_ddt = $DtoInterventions->nr_ddt;
            $interventionOnDb->nr_ddt_gr = $DtoInterventions->nr_ddt_gr;
            $interventionOnDb->date_ddt_gr = $DtoInterventions->date_ddt_gr;
            if (isset($DtoInterventions->date_ddt)) {
                $interventionOnDb->date_ddt = $DtoInterventions->date_ddt;
            }
            $interventionOnDb->referent = $DtoInterventions->referent;
            $interventionOnDb->plant_type = $DtoInterventions->plant_type;
            $interventionOnDb->trolley_type = $DtoInterventions->trolley_type;
            $interventionOnDb->series = $DtoInterventions->series;
            $interventionOnDb->voltage = $DtoInterventions->voltage;
            $interventionOnDb->exit_notes = $DtoInterventions->exit_notes;
            $interventionOnDb->unrepairable = $DtoInterventions->unrepairable;
            $interventionOnDb->send_to_customer = $DtoInterventions->send_to_customer;
            $interventionOnDb->create_quote = $DtoInterventions->create_quote;
          /*  if (!is_null($DtoInterventions->imgName)) {
                Image::make($DtoInterventions->imgmodule)->save(static::$dirImageFaultModule . $DtoInterventions->imgName);
                $interventionOnDb->imgmodule = static::getUrlModule() . $DtoInterventions->imgName;
            }*/
            if (!is_null($DtoInterventions->imgName)) {
                $data = base64_decode(str_replace('data:application/pdf;base64,', '', $DtoInterventions->imgmodule));
                file_put_contents(static::$dirImageFaultModule . $DtoInterventions->imgName, $data);

           // Image::make($DtoInterventions->imgmodule)->save(static::$dirImageFaultModule . $DtoInterventions->imgName);
          //  $interventionOnDb->imgmodule = static::getUrlModule() . $DtoInterventions->imgName;
        }

            //$interventionOnDb->id = $DtoInterventions->idIntervention;
            //$interventionOnDb->customer_id = $DtoInterventions->descriptionCustomer;
            $interventionOnDb->save();

            $cont = 0;
            if (isset($DtoInterventions->captures)) {
                foreach ($DtoInterventions->captures as $InterventionPhoto) {
                    $IdInterventionPhoto = GrInterventionPhoto::where('intervention_id', $interventionOnDb->id)->where('img', $InterventionPhoto)->first();
                    $cont = $cont + 1;
                    if (!isset($IdInterventionPhoto)) {
                        Image::make($InterventionPhoto)->save(static::$dirImageIntervention . $interventionOnDb->id . '_' . $cont . '.JPG');
                        $RowInterventionPhoto = new GrInterventionPhoto();
                        $RowInterventionPhoto->intervention_id = $interventionOnDb->id;
                        $RowInterventionPhoto->img = '/storage/images/Intervention/' . $interventionOnDb->id . '_' . $cont . '.JPG';
                        $RowInterventionPhoto->save();
                    }
                }
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    //Modifica check commessa NonPronta/Pronta
    
 public static function updateCheck(Request $DtoInterventions)
    {
        $interventionOnDb = GrIntervention::where('id', $DtoInterventions->id)->first();

        DB::beginTransaction();
        try {

            $interventionOnDb->id = $DtoInterventions->id;  
          
            //if (isset($interventionOnDb->ready)) {
            $interventionOnDb->ready = !$interventionOnDb->ready;
            //}
            $interventionOnDb->save();

        DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
 
    
   #endregion UPDATE
  
 public static function updateCheckRepairArrived(Request $DtoInterventions)
    {
        $interventionOnDb = GrIntervention::where('id', $DtoInterventions->id)->first();

        DB::beginTransaction();
        try {

            $interventionOnDb->id = $DtoInterventions->id;  
          
            //if (isset($interventionOnDb->ready)) {
            $interventionOnDb->repaired_arrived = !$interventionOnDb->repaired_arrived;
            //}
            $interventionOnDb->save();

        DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
   #endregion UPDATE


    //Modifica popup tecnico
    
 public static function updateSelectReferent(Request $DtoInterventions)
    {
        $interventionOnDb = GrIntervention::where('id', $DtoInterventions->id)->first();

        DB::beginTransaction();
        try {

            $interventionOnDb->id = $DtoInterventions->id;  
          
            $interventionOnDb->referent = $DtoInterventions->id_people;
            $interventionOnDb->save();

        DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

 
    //Modifica Riparazione Esterna
    public static function updateExternalRepair(Request $DtoExternalRepair)
    {
        $IdExternalRepair = ExternalRepair::where('id_intervention', $DtoExternalRepair->id_intervention)->first();
        $Intervention = GrIntervention::where('id', $DtoExternalRepair->id_intervention)->first();
        DB::beginTransaction();
        try {
            if (!isset($IdExternalRepair->id_intervention)) {
                $RowExternalRepair = new ExternalRepair();
                $RowExternalRepair->id_intervention = $DtoExternalRepair->id_intervention; 
                $RowExternalRepair->id_librone_rip_esterna = $DtoExternalRepair->id_librone_rip_esterna; 
                $RowExternalRepair->date = $DtoExternalRepair->date;
                $RowExternalRepair->ddt_exit = $DtoExternalRepair->ddt_exit;
                $RowExternalRepair->numero = $DtoExternalRepair->codice_intervento;
                $RowExternalRepair->id_business_name_supplier = $DtoExternalRepair->id_business_name_supplier;
                $RowExternalRepair->date_return_product_from_the_supplier = $DtoExternalRepair->date_return_product_from_the_supplier;
                $RowExternalRepair->ddt_supplier = $DtoExternalRepair->ddt_supplier;
                $RowExternalRepair->reference_supplier = $DtoExternalRepair->reference_supplier;
                $RowExternalRepair->notes_from_repairman = $DtoExternalRepair->notes_from_repairman;
                $RowExternalRepair->price = $DtoExternalRepair->price;
                $RowExternalRepair->save();
            } else {
                if (isset($IdExternalRepair->id)) {
                    $IdExternalRepair->id_intervention = $DtoExternalRepair->id_intervention;
                    $IdExternalRepair->date = $DtoExternalRepair->date;
                    $IdExternalRepair->numero = $DtoExternalRepair->codice_intervento;
                    $IdExternalRepair->ddt_exit = $DtoExternalRepair->ddt_exit;
                    $IdExternalRepair->id_business_name_supplier = $DtoExternalRepair->id_business_name_supplier;
                    $IdExternalRepair->id_business_name_supplier = $DtoExternalRepair->id_business_name_supplier;
                    $IdExternalRepair->date_return_product_from_the_supplier = $DtoExternalRepair->date_return_product_from_the_supplier;
                    $IdExternalRepair->ddt_supplier = $DtoExternalRepair->ddt_supplier;
                    $IdExternalRepair->reference_supplier = $DtoExternalRepair->reference_supplier;
                    $IdExternalRepair->notes_from_repairman = $DtoExternalRepair->notes_from_repairman;
                    $IdExternalRepair->price = $DtoExternalRepair->price;
                    $IdExternalRepair->save();
                    
                    if (isset($IdExternalRepair->ddt_supplier)) {
                        $Intervention->states_id = 1;    
                        $Intervention->ready = true; 
                     }
                    if (isset($IdExternalRepair->date_return_product_from_the_supplier)) {
                        $Intervention->states_id = 1; 
                        $Intervention->ready = true;
                     }
                        $Intervention->save();
                }
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    //updateInterventionUpdateViewQuotes
    public static function updateInterventionUpdateViewQuotes(Request $DtoQuotes) {



        $IdQuotes = GrQuotes::where('id_intervention', $DtoQuotes->id)->first();

        DB::beginTransaction();
        try {
            if (!isset($IdQuotes->id_intervention)) {
            
                $RowQuotes = new GrQuotes();
                $RowQuotes->id_intervention = $DtoQuotes->id;
                $RowQuotes->email = $DtoQuotes->email;
                $RowQuotes->description_payment = $DtoQuotes->description_payment; 
                $RowQuotes->code_intervention_gr = $DtoQuotes->code_intervention_gr;
                $RowQuotes->id_states_quote = $DtoQuotes->id_states_quote; 
                $RowQuotes->note = $DtoQuotes->note; 
                $RowQuotes->note_before_the_quote = $DtoQuotes->note_before_the_quote;
                $RowQuotes->object = $DtoQuotes->object;
                $RowQuotes->tot_quote = $DtoQuotes->tot_quote;
                $RowQuotes->date_agg = $DtoQuotes->date_agg;
                $RowQuotes->total_amount_with_discount = $DtoQuotes->total_amount_with_discount;
                $RowQuotes->discount = $DtoQuotes->discount;
                $RowQuotes->kind_attention = $DtoQuotes->kind_attention;
                $RowQuotes->description_and_items_agg = $DtoQuotes->description_and_items_agg;
                $RowQuotes->created_id = $DtoQuotes->updated_id;
                $RowQuotes->save();

                $GrIntervention = GrIntervention::where('id', $DtoQuotes->id)->get()->first();
                if (isset($GrIntervention)){
                    $GrIntervention->preventivo_nuovo_creato = true;
                    $GrIntervention->save();
                }

                $IdGrQuotesEmailSend = GrQuotesEmailSend::where('id_quotes', $DtoQuotes->id)->get();
                if (isset($IdGrQuotesEmailSend)) {
                    GrQuotesEmailSend::where('id_quotes', $DtoQuotes->id)->delete();
                }
                    foreach ($DtoQuotes->AggOptions as $res) {
                        if (!is_null($res['people_id'])){
                                $RowQuotesEmailSend = new GrQuotesEmailSend();
                                $RowQuotesEmailSend->email = $res['emailreferente'];
                                $RowQuotesEmailSend->id_people = $res['people_id'];
                                $RowQuotesEmailSend->id_user = $res['user_id'];
                                $RowQuotesEmailSend->id_quotes = $DtoQuotes->id;
                                $RowQuotesEmailSend->save();
                        }
                    }
            } else {
                if (isset($IdQuotes->id)) {
                $IdQuotes->id_intervention = $DtoQuotes->id;             
                $IdQuotes->email = $DtoQuotes->email;             
                $IdQuotes->note = $DtoQuotes->note; 
                $IdQuotes->note_customer = $DtoQuotes->note_customer;  
                $IdQuotes->code_intervention_gr = $DtoQuotes->code_intervention_gr;                           
                $IdQuotes->id_states_quote = $DtoQuotes->id_states_quote;
                $IdQuotes->description_payment = $DtoQuotes->description_payment;
                $IdQuotes->note_before_the_quote = $DtoQuotes->note_before_the_quote;
                $IdQuotes->object = $DtoQuotes->object; 
                $IdQuotes->tot_quote = $DtoQuotes->tot_quote; 
                $IdQuotes->total_amount_with_discount = $DtoQuotes->total_amount_with_discount;
                $IdQuotes->discount = $DtoQuotes->discount;
                $IdQuotes->date_agg = $DtoQuotes->date_agg;
                $IdQuotes->kind_attention = $DtoQuotes->kind_attention;
                $IdQuotes->description_and_items_agg = $DtoQuotes->description_and_items_agg;
                $IdQuotes->created_id = $DtoQuotes->updated_id;
                $IdQuotes->updated_id = $DtoQuotes->updated_id;
                $IdQuotes->save();

                $IdGrQuotesEmailSend = GrQuotesEmailSend::where('id_quotes', $DtoQuotes->id)->get();
                if (isset($IdGrQuotesEmailSend)) {
                    GrQuotesEmailSend::where('id_quotes', $DtoQuotes->id)->delete();
                }
                    foreach ($DtoQuotes->AggOptions as $res) {
                        if (!is_null($res['people_id'])){
                                $RowQuotesEmailSend = new GrQuotesEmailSend();
                                $RowQuotesEmailSend->email = $res['emailreferente'];
                                $RowQuotesEmailSend->id_people = $res['people_id'];
                                $RowQuotesEmailSend->id_user = $res['user_id'];
                                $RowQuotesEmailSend->id_quotes = $DtoQuotes->id;
                                $RowQuotesEmailSend->save();
                        }
                    }


                }
            }
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #region updateInterventionUpdateProcessingSheet
    /**
     * updateInterventionUpdateProcessingSheet
     * 
     * @param Request DtoInterventions
     * @param int idLanguage
     */

    public static function updateInterventionUpdateProcessingSheet(Request $DtoInterventions)
    {
        $interventionOnDb = GrIntervention::where('id', $DtoInterventions->id)->first();

        DB::beginTransaction();
        try {

            if (isset($DtoInterventions->id)) {
                $interventionOnDb->id = $DtoInterventions->id;
            }
            if (isset($DtoInterventions->idLanguage)) {
                $interventionOnDb->language_id = $DtoInterventions->idLanguage;
            }
            if (isset($DtoInterventions->updated_id)) {
                $interventionOnDb->updated_id = $DtoInterventions->updated_id;
            }
            if (isset($DtoInterventions->create_quote)) {
                $interventionOnDb->create_quote = $DtoInterventions->create_quote;
            }
            if (isset($DtoInterventions->code_intervention_gr)) {
                $interventionOnDb->code_intervention_gr = $DtoInterventions->code_intervention_gr;
            }else{
                $interventionOnDb->code_intervention_gr = '';
            }
            if (isset($DtoInterventions->to_call)) {
                $interventionOnDb->to_call = $DtoInterventions->to_call;
                $interventionOnDb->preventivo_nuovo_creato = false;
            }
            if (isset($DtoInterventions->escape_from)) {
                $interventionOnDb->escape_from = $DtoInterventions->escape_from;
                $interventionOnDb->preventivo_nuovo_creato = false;
            }
            if (isset($DtoInterventions->working)) {
                $interventionOnDb->working = $DtoInterventions->working;
            }
             if (isset($DtoInterventions->unrepairable)) {
                $interventionOnDb->unrepairable = $DtoInterventions->unrepairable;
                $interventionOnDb->preventivo_nuovo_creato = false;
            }

            if (isset($DtoInterventions->search_product)) {
                $interventionOnDb->search_product = $DtoInterventions->search_product;
            }
            if (isset($DtoInterventions->working_with_testing)) {       
                $interventionOnDb->working_with_testing = $DtoInterventions->working_with_testing;
                $interventionOnDb->preventivo_nuovo_creato = false;
            }
            if (isset($DtoInterventions->working_without_testing)) {
                $interventionOnDb->working_without_testing = $DtoInterventions->working_without_testing;
                $interventionOnDb->preventivo_nuovo_creato = false;
            }

            if (isset($DtoInterventions->flaw_detection)) {
                $interventionOnDb->flaw_detection = $DtoInterventions->flaw_detection;
            }
            if (isset($DtoInterventions->done_works)) {
                $interventionOnDb->done_works = $DtoInterventions->done_works;
            }
            if (isset($DtoInterventions->hour)) {
                $interventionOnDb->hour = $DtoInterventions->hour;
            }
            if (isset($DtoInterventions->value_config_hour)) {
                $interventionOnDb->value_config_hour = $DtoInterventions->value_config_hour;
            }
            if (isset($DtoInterventions->tot_value_config)) {
                $interventionOnDb->tot_value_config = $DtoInterventions->tot_value_config;
            }
            if (isset($DtoInterventions->consumables)) {
                $interventionOnDb->consumables = $DtoInterventions->consumables;
            }
            if (isset($DtoInterventions->tot_final)) {
                $interventionOnDb->tot_final = $DtoInterventions->tot_final;
            }
            if (isset($DtoInterventions->total_calculated)) {
                $interventionOnDb->total_calculated = $DtoInterventions->total_calculated;
            }

            $interventionOnDb->save();

            $IdInterventionProcessing = GrInterventionProcessing::where('id_intervention', $DtoInterventions->id)->get();
            if (isset($IdInterventionProcessing)) {
                GrInterventionProcessing::where('id_intervention', $DtoInterventions->id)->delete();
            }
            //$Rowgrprocessing = new GrInterventionProcessing();
            //if(isset($DtoInterventions->date_aggs)) {
            // $Rowgrprocessing->date_aggs = $DtoInterventions->date_aggs;             
            // }
            // $Rowgrprocessing->save();
            foreach ($DtoInterventions->Articles as $InterventionArticles) {
                //$IdInterventionArticles =  GrInterventionProcessing::where('id_intervention', $DtoInterventions->id)->first();

                //if(isset($InterventionArticles->description)){ 
                $RowInterventionArticles = new GrInterventionProcessing();

                if (isset($DtoInterventions->id)) {
                    $RowInterventionArticles->id_intervention = $DtoInterventions->id;
                }
                if (isset($InterventionArticles['description'])) {
                    $RowInterventionArticles->description = $InterventionArticles['description'];
                }else{
                   $RowInterventionArticles->description =''; 
                }
                if (isset($InterventionArticles['qta'])) {
                    $RowInterventionArticles->qta = $InterventionArticles['qta'];
                }else{
                   $RowInterventionArticles->qta =''; 
                }

                if(isset($InterventionArticles['n_u'])) {
                $RowInterventionArticles->n_u = $InterventionArticles['n_u'];             
                }

                if (isset($InterventionArticles['code_gr']) && $InterventionArticles['code_gr'] != 'null' ) {
                    $RowInterventionArticles->id_gr_items = $InterventionArticles['code_gr'];
                }else{
                   $RowInterventionArticles->id_gr_items = null; 
                }
                
                 
                if (isset($InterventionArticles['price_list'])) {
                    $RowInterventionArticles->price_list = $InterventionArticles['price_list'];
                }else{
                   $RowInterventionArticles->price_list = ''; 
                }
                if (isset($InterventionArticles['tot'])) {
                    $RowInterventionArticles->tot = $InterventionArticles['tot'];
                }else{
                   $RowInterventionArticles->tot = ''; 
                }
                if (isset($DtoInterventions->date_aggs)) {
                    $RowInterventionArticles->date_aggs = $DtoInterventions->date_aggs;
                }
                $RowInterventionArticles->code_intervention_gr = $DtoInterventions->code_intervention_gr;

                $RowInterventionArticles->save();
                //}
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }
    #region updateInterventionUpdateProcessingSheet


    /**
     * Update
     * 
     * @param Request DtoInterventions
     * @param int idLanguage
     */

    public static function updateIntervention(Request $DtoInterventions)
    {
        static::_validateData($DtoInterventions, $DtoInterventions->idLanguage, DbOperationsTypesEnum::UPDATE);
        $interventionOnDb = GrIntervention::find($DtoInterventions->id);
        $interventionOnDbPhoto = GrInterventionPhoto::find($DtoInterventions->intervention_id);
        //$ItemsId = ItemGr::where('id',  $interventionOnDb->items_id)->first()->id;
        //$ItemsId = Item::where('id',  $interventionOnDb->items_id)->first()->id;
        //$customerId= User::where('id',$DtoInterventions->customer_id)->first();
        DB::beginTransaction();
        try {
            $interventionOnDb->language_id = $DtoInterventions->idLanguage;
            $interventionOnDb->customers_id = $DtoInterventions->customer_id;

            if ($DtoInterventions->user_id != '' && !is_null($DtoInterventions->user_id)) {
                $interventionOnDb->user_id = $DtoInterventions->user_id;
            }

            $interventionOnDb->states_id = $DtoInterventions->states_id;
            //$interventionOnDb->items_id = ItemLanguage::where('id',$DtoInterventions->code_product)->first()->item_id; 
            //$interventionOnDb->items_id = ItemLanguage::where('id',$DtoInterventions->items_id)->first()->item_id; 
              if (isset($DtoInterventions->items_id)) {
                $interventionOnDb->items_id = $DtoInterventions->items_id;
                }else {
                    $DtoInterventions->items_id = '';
                }
            /*    if (isset($DtoInterventions->name_product)) {
                $interventionOnDb->items_id = $DtoInterventions->name_product;
                }else {
                    $DtoInterventions->items_id = '-';
                }*/
               
            //$interventionOnDb->items_id = $DtoInterventions->items_id;
            //$interventionOnDb->items_id = $DtoInterventions->name_product;
            $interventionOnDb->description = $DtoInterventions->description;
            $interventionOnDb->code_intervention_gr = $DtoInterventions->code_intervention_gr;
            $interventionOnDb->defect = $DtoInterventions->defect;
            $interventionOnDb->nr_ddt = $DtoInterventions->nr_ddt;
            $interventionOnDb->nr_ddt_gr = $DtoInterventions->nr_ddt_gr;
            $interventionOnDb->rip_association = $DtoInterventions->rip_association;
            $interventionOnDb->note_internal_gr = $DtoInterventions->note_internal_gr;    
            
           // if (isset($DtoInterventions['date_ddt_gr'])) {
              //  $interventionOnDb->date_ddt_gr = $DtoInterventions['date_ddt_gr'];
           // }else{
             //   $interventionOnDb->date_ddt_gr =$DtoInterventions['date_ddt_gr'];
            // }    
               // if (isset($DtoInterventions['date_ddt_gr'])) {
                    $interventionOnDb->date_ddt_gr = $DtoInterventions['date_ddt_gr'];
               // }

            // if (is_null($DtoInterventions['date_ddt_gr'])) {
              // $interventionOnDb->date_ddt_gr = $DtoInterventions['date_ddt_gr'];
           // } 

            if (isset($DtoInterventions->date_ddt)) {
                $interventionOnDb->date_ddt = $DtoInterventions->date_ddt;
            }
            $interventionOnDb->referent = $DtoInterventions->referent;
            $interventionOnDb->trolley_type = $DtoInterventions->trolley_type;
            $interventionOnDb->plant_type = $DtoInterventions->plant_type;
            $interventionOnDb->series = $DtoInterventions->series;
            $interventionOnDb->voltage = $DtoInterventions->voltage;
            $interventionOnDb->exit_notes = $DtoInterventions->exit_notes;
            //$interventionOnDb->unrepairable = $DtoInterventions->unrepairable;
            $interventionOnDb->send_to_customer = $DtoInterventions->send_to_customer;

            if ($DtoInterventions->create_quote == true){
                $interventionOnDb->create_quote = $DtoInterventions->create_quote;
            }else{
                $interventionOnDb->create_quote = false;
            }
          
            $interventionOnDb->code_tracking = $DtoInterventions->code_tracking;
            $interventionOnDb->external_referent_id = $DtoInterventions->external_referent_id_intervention;
                if (($interventionOnDb->nr_ddt_gr != null|| $interventionOnDb->nr_ddt_gr != "")) {
                    $interventionOnDb->states_id = 2;    
                }else{
                    $interventionOnDb->states_id = 1;  
                }
                if (($interventionOnDb->date_ddt_gr != null|| $interventionOnDb->date_ddt_gr != "")) {
                    $interventionOnDb->states_id = 2;    
                }else{
                    $interventionOnDb->states_id = 1;  
                }

              /*  if (!is_null($DtoInterventions->imgName)) {
                $data = base64_decode(str_replace('data:application/pdf;base64,', '', $DtoInterventions->imgmodule));
                file_put_contents(static::$dirImageFaultModule . $DtoInterventions->imgName, $data);
               // Image::make($DtoInterventions->imgmodule)->save(static::$dirImageFaultModule . $DtoInterventions->imgName);
              //  $interventionOnDb->imgmodule = static::getUrlModule() . $DtoInterventions->imgName;
                }*/
                if (!is_null($DtoInterventions->imgName)) {
                    $interventionOnDb->imgmodule = static::getUrlModule() . $DtoInterventions->imgName;
                    $data = base64_decode(str_replace('data:application/pdf;base64,', '', $DtoInterventions->imgmodule));
                    file_put_contents(static::$dirImageFaultModule . $DtoInterventions->imgName, $data);
                }
            //$interventionOnDb->id = $DtoInterventions->idIntervention;
            //$interventionOnDb->customer_id = $DtoInterventions->descriptionCustomer;
            $interventionOnDb->save();

              if (isset($DtoInterventions->rip_association)) {
                  $Intervention = GrIntervention::where('id', $DtoInterventions->rip_association)->first();  
                  if (isset($Intervention)) {
                    $Intervention->referent = $DtoInterventions->referent; 
                    $Intervention->save();
                  }
                    
                }
                
            $cont = 0;
            if (isset($DtoInterventions->captures)) {
                foreach ($DtoInterventions->captures as $InterventionPhoto) {
                    $IdInterventionPhoto = GrInterventionPhoto::where('intervention_id', $interventionOnDb->id)->where('img', $InterventionPhoto)->first();
                    $cont = $cont + 1;
                    if (!isset($IdInterventionPhoto)) {
                        Image::make($InterventionPhoto)->save(static::$dirImageIntervention . $interventionOnDb->id . '_' . $cont . '.JPG');
                        $RowInterventionPhoto = new GrInterventionPhoto();
                        $RowInterventionPhoto->intervention_id = $interventionOnDb->id;
                        $RowInterventionPhoto->img = '/storage/images/Intervention/' . $interventionOnDb->id . '_' . $cont . '.JPG';
                        $RowInterventionPhoto->save();
                    }
                }
            }

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion updateIntervention

    #region updateUser

    /**
     * updateUser
     * 
     * @param Request DtoUser
     * @param int idLanguage
     */

    public static function updateUser(Request $DtoUser)
    {

        //$userOnDb= User::find($DtoUser->id);
        $userOnDb = User::where('id', $DtoUser->id)->first();
        $userdataOnDb = UsersDatas::where('user_id', $DtoUser->id)->first();

        DB::beginTransaction();
        try {

            $userOnDb->id = $DtoUser->id;
            $userOnDb->name = $DtoUser->username;
            $userOnDb->email = $DtoUser->email;
            $userOnDb->save();

            $userdataOnDb->language_id = $DtoUser->idLanguage;
            $userdataOnDb->user_id = $DtoUser->id;
            $userdataOnDb->name = $DtoUser->name;
            $userdataOnDb->surname = $DtoUser->surname;
            $userdataOnDb->business_name = $DtoUser->business_name;
            $userdataOnDb->vat_number = $DtoUser->vat_number;
            $userdataOnDb->fiscal_code = $DtoUser->fiscal_code;
            $userdataOnDb->address = $DtoUser->address;
            $userdataOnDb->codepostal = $DtoUser->codepostal;
            $userdataOnDb->telephone_number = $DtoUser->telephone_number;
            $userdataOnDb->website = $DtoUser->website;
            $userdataOnDb->country = $DtoUser->country;
            $userdataOnDb->province = $DtoUser->province;
            $userdataOnDb->mobile_number = $DtoUser->mobile_number;
            $userdataOnDb->fax_number = $DtoUser->fax_number;
            $userdataOnDb->pec = $DtoUser->pec;
            $userdataOnDb->vat_type_id = $DtoUser->vat_type_id;
            $userdataOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion updateUser


    #region DELETE
    /**
     * Delete
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $Intervention = GrIntervention::find($id);
        $IdInterventionPhoto = GrInterventionPhoto::where('intervention_id', $Intervention->id);

        if (is_null($Intervention)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!is_null($Intervention)) {

            DB::beginTransaction();

            try {

                GrIntervention::where('id', $id)->delete();
                GrInterventionPhoto::where('intervention_id', $Intervention->id)->delete();

                DB::commit();
            } catch (Exception $ex) {
                DB::rollback();
                throw $ex;
            }
        }
    }

    #endregion DELETE
}

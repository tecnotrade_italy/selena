<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoListKeywords;
use App\DtoModel\DtoPageSitemap;


use App\DtoModel\DtoTypologyKeywords;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;

use App\ListKeywords;
use App\TypologyKeywords;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ListKeywordsBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (ListKeywords::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->description)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DescriptionNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }


    #endregion PRIVATE

    #region GET

    public static function getPages(string $page_type, int $idLanguage)
    {

        $result = collect();
        foreach (DB::select(
            "SELECT list_keywords.url, list_keywords.updated_at
            FROM list_keywords
            ORDER BY list_keywords.updated_at desc"
        ) as $PagesAll) {
            $dtoPageSitemap = new DtoPageSitemap();
            $dtoPageSitemap->url = $PagesAll->url;
            $dtoPageSitemap->updated_at = $PagesAll->updated_at;

            $result->push($dtoPageSitemap);
        }
        return $result;
    }

    
    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoOptional
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (DB::table('list_keywords')
                ->select('*')
                ->orderBy('list_keywords.id')
                ->get()
        as $listKeywordsAll) {
            $dtoListKeywords = new DtoListKeywords();
            $dtoListKeywords->id = $listKeywordsAll->id;
            $dtoListKeywords->typology_keywords_id = $listKeywordsAll->typology_keywords_id;
            $dtoListKeywords->description = $listKeywordsAll->description;
            $dtoListKeywords->url = $listKeywordsAll->url;
            $dtoListKeywords->title = $listKeywordsAll->title;
            $dtoListKeywords->keywords = $listKeywordsAll->keywords;
            $dtoListKeywords->online = $listKeywordsAll->online;
            $result->push($dtoListKeywords); 
        }
        
        return $result;
    }

    /** 
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoOptional
     */
    public static function getHtml($url, $idUser, $idLanguage)
    {
        $listKeywords = ListKeywords::where('url', $url)->where('online', true)->first();
        
        if (isset($listKeywords)) {
            $typologyKeyword = TypologyKeywords::where('id', $listKeywords->typology_keywords_id)->first();
            if(isset($typologyKeyword)){
                $dtoPageItemRender = new DtoTypologyKeywords();

                $strHtmlReplace = str_replace('#description#', $listKeywords->description, $typologyKeyword->html);
                $dtoPageItemRender->html = $strHtmlReplace;
                $dtoPageItemRender->description = $listKeywords->description;
                $dtoPageItemRender->meta_tag_description = $listKeywords->description;
                $dtoPageItemRender->meta_tag_title = $listKeywords->title;
                $dtoPageItemRender->meta_tag_keyword = $listKeywords->keywords;
                
                return $dtoPageItemRender;
            }            
        } else {
            return '';
        }
    }
    
    
    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $listKeywords = new ListKeywords();
            $listKeywords->typology_keywords_id = $request->typology_keywords_id;
            $listKeywords->description = $request->description;
            $listKeywords->url = $request->url;
            $listKeywords->title = $request->title;
            $listKeywords->keywords = $request->keywords;
            $listKeywords->online = $request->online;
            $listKeywords->save();
                        
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $listKeywords->id;
    }


    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoOptional
     * @param int idLanguage
     */
    public static function update(Request $request, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $listKeywordsOnDb = ListKeywords::find($request->id);
        
        DB::beginTransaction();

        try {
            $listKeywordsOnDb->typology_keywords_id = $request->typology_keywords_id;
            $listKeywordsOnDb->description = $request->description;
            $listKeywordsOnDb->url = $request->url;
            $listKeywordsOnDb->title = $request->title;
            $listKeywordsOnDb->keywords = $request->keywords;
            $listKeywordsOnDb->online = $request->online;
            $listKeywordsOnDb->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $listKeywords = ListKeywords::find($id);
        
        if (is_null($listKeywords)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            
            $listKeywords->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    
    #endregion DELETE
}

<?php
namespace App\BusinessLogic;

use App\DtoModel\DtoImageResize;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\ImageResize;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ImageResizeBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (ImageResize::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }
    }

    
    /**
     * Convert to dto
     * 
     * @param Nation nation
     * @param int idLanguage
     * 
     * @return DtoNation
     */
    private static function _convertToDto(SelenaSqlViews $selenaviews)
    {
        $dtoSelenaViews = new DtoImageResize();
        $dtoSelenaViews->id = $selenaviews->id;
        $dtoSelenaViews->container = $selenaviews->container;
        $dtoSelenaViews->width = $selenaviews->width;
        $dtoSelenaViews->online = $selenaviews->online;
        return $dtoSelenaViews;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoNation
     */
    public static function getAll()
    {
        $result = collect();
        foreach (ImageResize::all() as $selenaViewsAll) {
            
            $dtoSelenaViews = new DtoImageResize();
            $dtoSelenaViews->id = $selenaViewsAll->id;
            $dtoSelenaViews->container = $selenaViewsAll->container;
            $dtoSelenaViews->width = $selenaViewsAll->width;
            $dtoSelenaViews->online = $selenaViewsAll->online;
            $result->push($dtoSelenaViews); 
        }
        return $result;
    }

    /**
     * Get by id
     * 
     * @param int id
     * 
     * @return DtoNation
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(ImageResize::find($id));
    }

    
    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $selenaviews = new ImageResize();
            $selenaviews->container = $request->container;
            $selenaviews->width = $request->width;
            $selenaviews->online = $request->online;
            $selenaviews->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $selenaviews->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoSelenaViews
     * @param int idLanguage
     */
    public static function update(Request $dtoSelenaViews, int $idLanguage)
    {
        static::_validateData($dtoSelenaViews, $idLanguage, DbOperationsTypesEnum::UPDATE);
               
        $selenaViewsOnDb = ImageResize::find($dtoSelenaViews->id);
        
        DB::beginTransaction();

        try {
            $selenaViewsOnDb->container = $dtoSelenaViews->container;
            $selenaViewsOnDb->width = $dtoSelenaViews->width;
            $selenaViewsOnDb->online = $dtoSelenaViews->online;
            $selenaViewsOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {

        $selenaviews = ImageResize::find($id);

        if (is_null($selenaviews)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $selenaviews->delete();
    }

    #endregion DELETE
}

<?php

namespace App\BusinessLogic;


use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\LogHelper;
use App\User;
use App\UsersDatas;
use App\Item;
use App\ItemLanguage;
use App\Category;
use App\CategoryItem;
use App\GroupNewsletter;
use App\CustomerGroupNewsletter;
use Intervention\Image\Facades\Image;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local as Adapter;

use Exception;

class SpecialFormaggiBL
{
    public static $dirImage = '../storage/app/public/images/Items/';

    #region PRIVATE

    private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/Items/';
        //return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/Items/';
    }
    #endregion PRIVATE
    
    #region GET

    /**
     * Get all
     * 
     * 
     * @return dtoSupports
     */
    public static function importItems()
    {
        //API per autenticazione (POST con username e password comunicati dal cliente ) --> return json con token
        //{"username" : "tecnotrade", "password": "45TY*78B790e?"}


        ini_set('memory_limit', '-1');

        $curlAuth = curl_init();

        curl_setopt_array($curlAuth, array(
            CURLOPT_URL => 'https://catalogo.specialformaggi.it/services/export/login',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "username" : "tecnotrade",
                "password": "45TY*78B790e?"
            }',
            CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
            ),
        ));
        
        $responseAuth = curl_exec($curlAuth);
        curl_close($curlAuth);

        if($responseAuth == ''){
            return 'Autenticazione fallita';
        }else{
            //API per estrazione articoli dopo la risposta dell'autenticazione
            $curlItems = curl_init();

            curl_setopt_array($curlItems, array(
                CURLOPT_URL => 'https://catalogo.specialformaggi.it/services/export/articoli',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "token": "' . $responseAuth . '"
                }',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            ));

            $responseItems = curl_exec($curlItems);

            curl_close($curlItems);
            $json = json_decode($responseItems, true);

            if($responseItems == ''){
                return 'Sessione scaduta';
            }else{
                //loop della risposta (Array??) interimento articolo se non già presente nella tabella items ed inserimento nella tabella category_items con l'ordinamento che viene trovato secondo il loop
                $filesystem = new Filesystem(new Adapter(static::$dirImage));
                for($i = 0; $i < count($json['articoli']); $i++){
                    DB::beginTransaction();

                    try {
                        $itemOnDb = Item::where('internal_code', $json['articoli'][$i]['codice'])->get()->first();
                        if(isset($itemOnDb)){
                            //update items
                            $itemOnDb->online = $json['articoli'][$i]['esportabile'];
                            $itemOnDb->price = $json['articoli'][$i]['prezzo'];

                            //gestione update image
                            if(count($json['articoli'][$i]['immagini'])>0){
                                //verificare se vengono aggiornate le immagini anche già caricate

                                /*if(file_exists(static::$dirImage . $json['articoli'][$i]['codice'] . '.jpg')){
                                    $filesystem->delete(static::getUrl() . $json['articoli'][$i]['codice'] . '.jpg');
                                    Image::make($json['articoli'][$i]['immagini'][0])->save(static::$dirImage . $json['articoli'][$i]['codice'] . '.jpg');
                                    $itemOnDb->img = static::getUrl() . $json['articoli'][$i]['codice'] . '.jpg';
                                }else{
                                    Image::make($json['articoli'][$i]['immagini'][0])->save(static::$dirImage . $json['articoli'][$i]['codice'] . '.jpg');
                                    $itemOnDb->img = static::getUrl() . $json['articoli'][$i]['codice'] . '.jpg';
                                }*/

                                if($itemOnDb->img = 'https://specialformaggi.it/storage/images/LOGO_SF_1.jpg'){
                                    Image::make($json['articoli'][$i]['immagini'][0])->save(static::$dirImage . $json['articoli'][$i]['codice'] . '.jpg');
                                    $itemOnDb->img = static::getUrl() . $json['articoli'][$i]['codice'] . '.jpg';
                                }
                            //}else{
                                //$itemOnDb->img = 'https://specialformaggi.it/storage/images/LOGO_SF_1.jpg';
                            }

                            $itemOnDb->save();

                            $itemsLanguageOnDb = ItemLanguage::where('item_id', $itemOnDb->id)->where('language_id', 1)->get()->first();
                            //update items languages
                            if(isset($itemsLanguageOnDb)){
                                $itemsLanguageOnDb->description = $json['articoli'][$i]['descrizione'];
                                $itemsLanguageOnDb->meta_tag_title = $json['articoli'][$i]['descrizione'];
                                $itemsLanguageOnDb->meta_tag_description = $json['articoli'][$i]['descrizione'];
                                $itemsLanguageOnDb->meta_tag_characteristic = $json['articoli'][$i]['descrizioneLunga'];
                                $itemsLanguageOnDb->meta_tag_keyword = $json['articoli'][$i]['descrizione'];
                                $itemsLanguageOnDb->save();
                            }
                        }else{
                            //insert tabella items
                            $items = new Item();
                            $items->internal_code = $json['articoli'][$i]['codice'];
                            $items->online = $json['articoli'][$i]['esportabile'];
                            $items->price = $json['articoli'][$i]['prezzo'];
                            if(count($json['articoli'][$i]['immagini'])>0){
                                //make image salvataggio percorso con codice .jpg
                                // all'interno del loop convertire il base 64 in immagine solo se l'articolo non è presente nella tabella (quindi nuovo)
                                Image::make($json['articoli'][$i]['immagini'][0])->save(static::$dirImage . $json['articoli'][$i]['codice'] . '.jpg');
                                $items->img = static::getUrl() . $json['articoli'][$i]['codice'] . '.jpg';
                            }else{
                                $items->img = 'https://specialformaggi.it/storage/images/LOGO_SF_1.jpg';
                            }

                            $items->save();

                            //insert tabella items languages
                            $itemsLanguage = new ItemLanguage();
                            $itemsLanguage->language_id = 1;
                            $itemsLanguage->item_id = $items->id;
                            $itemsLanguage->description = $json['articoli'][$i]['descrizione'];
                            $itemsLanguage->meta_tag_title = $json['articoli'][$i]['descrizione'];
                            $itemsLanguage->meta_tag_description = $json['articoli'][$i]['descrizione'];
                            $itemsLanguage->meta_tag_characteristic = $json['articoli'][$i]['descrizioneLunga'];
                            $itemsLanguage->meta_tag_keyword = $json['articoli'][$i]['descrizione'];
                            $itemsLanguage->link = '/' . str_replace(')', '', str_replace('(', '', str_replace('.', '-', str_replace(',', '-', str_replace('/', '-', str_replace(' ', '-', strtolower($json['articoli'][$i]['descrizione'])))))));
                            
                            $itemsLanguage->save();

                            //associazione categoria articolo
                            $categoriesOnDb = Category::where('code', '=', $json['articoli'][$i]['classe'])->get()->first();
                            if(isset($categoriesOnDb)){
                                $categoriesItemOnDb = CategoryItem::where('category_id', $categoriesOnDb->id)->where('item_id', $items->id)->get()->first();
                                if(!isset($categoriesItemOnDb)){
                                    $categoryItem = new CategoryItem();
                                    $categoryItem->item_id = $items->id;
                                    $categoryItem->category_id = $categoriesOnDb->id;
                                    $categoryItem->save();
                                }
                            }
                        }
                        
                        DB::commit();
                    } catch (Exception $ex) {
                        DB::rollback();
                        throw $ex;
                    }
                    //return risultati da stampare a schermo per verifica
                }
                //return $responseItems;
            }
        }
        //return $jsonAuth;
    }

    /**
     * Get all users from external api
     * 
     * 
     * @return dtoSupports
     */
    public static function importUsers()
    {
        //API per autenticazione (POST con username e password comunicati dal cliente ) --> return json con token
        //{"username" : "tecnotrade", "password": "45TY*78B790e?"}

        $curlAuth = curl_init();

        curl_setopt_array($curlAuth, array(
            CURLOPT_URL => 'https://catalogo.specialformaggi.it/services/export/login',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "username" : "tecnotrade",
                "password": "45TY*78B790e?"
            }',
            CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
            ),
        ));
        
        $responseAuth = curl_exec($curlAuth);
        curl_close($curlAuth);

        if($responseAuth == ''){
            return 'Autenticazione fallita';
        }else{
            //API per estrazione articoli dopo la risposta dell'autenticazione
            $curlUsers = curl_init();

            curl_setopt_array($curlUsers, array(
                CURLOPT_URL => 'https://catalogo.specialformaggi.it/services/export/clienti',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "token": "' . $responseAuth . '"
                }',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            ));

            $responseUsers = curl_exec($curlUsers);

            curl_close($curlUsers);
            $json = json_decode($responseUsers, true);

            if($responseUsers == ''){
                return 'Sessione scaduta';
            }else{
                for($i = 0; $i < count($json['clienti']); $i++){
                    DB::beginTransaction();

                    try {
                        $usersDatasOnDb = UsersDatas::where('code', $json['clienti'][$i]['codice'])->get()->first();
                        if(isset($usersDatasOnDb)){
                            $usersDatasOnDb->code = $json['clienti'][$i]['codice'];

                            if($json['clienti'][$i]['nome'] == ''){
                                $usersDatasOnDb->name = $json['clienti'][$i]['nome'];
                            }else{
                                $usersDatasOnDb->name = $json['clienti'][$i]['nome'];
                            }
                            
                            $usersDatasOnDb->surname = $json['clienti'][$i]['cognome'];

                            if($json['clienti'][$i]['cellulare'] != ''){
                                $usersDatasOnDb->mobile_number = str_replace('/', '', $json['clienti'][$i]['cellulare']);
                            }

                            if($json['clienti'][$i]['telefono'] != ''){
                                $usersDatasOnDb->telephone_number = str_replace('/', '', $json['clienti'][$i]['telefono']);
                            }

                            $usersDatasOnDb->business_name = $json['clienti'][$i]['ragioneSociale'];
                            
                            $usersDatasOnDb->save();

                            $usersOnDb = User::where('id', $usersDatasOnDb->user_id)->get()->first();

                            if($json['clienti'][$i]['email2'] == ''){
                                $usersOnDb->email = $json['clienti'][$i]['email2'];
                            }else{
                                $usersDatasOnDbByEmail = User::where('email', $json['clienti'][$i]['email2'])->get()->first();
                                if(isset($usersDatasOnDbByEmail)){
                                    $usersOnDb->email = $json['clienti'][$i]['email2'] .  $usersDatasOnDbByEmail->id;
                                }else{
                                    $usersOnDb->email = $json['clienti'][$i]['email2'];
                                }
                                
                            }

                            /* CHECK GRUPPO SMS */

                            //gruppo Agente
                            if($json['clienti'][$i]['agente']>0){
                                $groupNewsletterDb = GroupNewsletter::where('code', 'AGE-' . $json['clienti'][$i]['agente'])->get()->first();
                                if(isset($groupNewsletterDb)){
                                    $userGroupOnDb = CustomerGroupNewsletter::where('customer_id', $usersDatasOnDb->user_id)->where('group_newsletter_id', $groupNewsletterDb->id)->get()->first();
                                    if(!isset($userGroupOnDb)){
                                        $CustomerGroupNewsletter = new CustomerGroupNewsletter();
                                        $CustomerGroupNewsletter->customer_id = $usersDatasOnDb->user_id;
                                        $CustomerGroupNewsletter->group_newsletter_id = $groupNewsletterDb->id;
                                        $CustomerGroupNewsletter->save();
                                    }
                                }
                            }

                            //gruppo zona commerciale
                            if($json['clienti'][$i]['zona']>0){
                                $groupNewsletterDb = GroupNewsletter::where('code', 'ZC-' . $json['clienti'][$i]['zona'])->get()->first();
                                if(isset($groupNewsletterDb)){
                                    $userGroupOnDb = CustomerGroupNewsletter::where('customer_id', $usersDatasOnDb->user_id)->where('group_newsletter_id', $groupNewsletterDb->id)->get()->first();
                                    if(!isset($userGroupOnDb)){
                                        $CustomerGroupNewsletter = new CustomerGroupNewsletter();
                                        $CustomerGroupNewsletter->customer_id = $usersDatasOnDb->user_id;
                                        $CustomerGroupNewsletter->group_newsletter_id = $groupNewsletterDb->id;
                                        $CustomerGroupNewsletter->save();
                                    }
                                }
                            }

                            //gruppo canale di vendita 
                            if($json['clienti'][$i]['canale']>0){
                                $groupNewsletterDb = GroupNewsletter::where('code', 'CV-' . $json['clienti'][$i]['canale'])->get()->first();
                                if(isset($groupNewsletterDb)){
                                    $userGroupOnDb = CustomerGroupNewsletter::where('customer_id', $usersDatasOnDb->user_id)->where('group_newsletter_id', $groupNewsletterDb->id)->get()->first();
                                    if(!isset($userGroupOnDb)){
                                        $CustomerGroupNewsletter = new CustomerGroupNewsletter();
                                        $CustomerGroupNewsletter->customer_id = $usersDatasOnDb->user_id;
                                        $CustomerGroupNewsletter->group_newsletter_id = $groupNewsletterDb->id;
                                        $CustomerGroupNewsletter->save();
                                    }
                                }
                            }

                            //gruppo categoria di clienti
                            if($json['clienti'][$i]['categoria']>0){
                                $groupNewsletterDb = GroupNewsletter::where('code', 'CC-' . $json['clienti'][$i]['categoria'])->get()->first();
                                if(isset($groupNewsletterDb)){
                                    $userGroupOnDb = CustomerGroupNewsletter::where('customer_id', $usersDatasOnDb->user_id)->where('group_newsletter_id', $groupNewsletterDb->id)->get()->first();
                                    if(!isset($userGroupOnDb)){
                                        $CustomerGroupNewsletter = new CustomerGroupNewsletter();
                                        $CustomerGroupNewsletter->customer_id = $usersDatasOnDb->user_id;
                                        $CustomerGroupNewsletter->group_newsletter_id = $groupNewsletterDb->id;
                                        $CustomerGroupNewsletter->save();
                                    }
                                }
                            }

                            /* END CHECK GRUPPO SMS */

                        }else{
                            // utente già presente in db, aggiornamento dati
                            
                            if($json['clienti'][$i]['ragioneSociale'] != ''){

                                $users = new User();
                                $users->name = $json['clienti'][$i]['codice'];

                                $users->password = bcrypt('specialformaggi');
                                
                                if($json['clienti'][$i]['email2'] == ''){
                                    $users->email = $json['clienti'][$i]['email2'];
                                }else{
                                    $usersDatasOnDbByEmail = User::where('email', $json['clienti'][$i]['email2'])->get()->first();
                                    if(isset($usersDatasOnDbByEmail)){
                                        $users->email = $json['clienti'][$i]['email2'] .  $usersDatasOnDbByEmail->id;
                                    }else{
                                        $users->email = $json['clienti'][$i]['email2'];
                                    }
                                    
                                }
                                $users->created_id = '1';
                                $users->language_id = '1';
                                $users->online = true;
                                $users->save();

                                $usersDatas = new UsersDatas();
                                $usersDatas->user_id = $users->id;
                                $usersDatas->code = $json['clienti'][$i]['codice'];
                                $usersDatas->business_name = $json['clienti'][$i]['ragioneSociale'];

                                $usersDatas->created_id = '1';

                                if($json['clienti'][$i]['nome'] == ''){
                                    $usersDatas->name = $json['clienti'][$i]['nome'];
                                }else{
                                    $usersDatas->name = $json['clienti'][$i]['nome'];
                                }

                                $usersDatas->surname = $json['clienti'][$i]['cognome'];

                                if($json['clienti'][$i]['cellulare'] != ''){
                                    $usersDatas->mobile_number = str_replace('/', '', $json['clienti'][$i]['cellulare']);
                                }

                                if($json['clienti'][$i]['telefono'] != ''){
                                    $usersDatas->telephone_number = str_replace('/', '', $json['clienti'][$i]['telefono']);
                                }

                                $usersDatas->save();

                                /* CHECK GRUPPO SMS */

                                //gruppo Agente
                                if($json['clienti'][$i]['agente']>0){
                                    $groupNewsletterDb = GroupNewsletter::where('code', 'AGE-' . $json['clienti'][$i]['agente'])->get()->first();
                                    if(isset($groupNewsletterDb)){
                                        $userGroupOnDb = CustomerGroupNewsletter::where('customer_id', $users->id)->where('group_newsletter_id', $groupNewsletterDb->id)->get()->first();
                                        if(!isset($userGroupOnDb)){
                                            $CustomerGroupNewsletter = new CustomerGroupNewsletter();
                                            $CustomerGroupNewsletter->customer_id = $users->id;
                                            $CustomerGroupNewsletter->group_newsletter_id = $groupNewsletterDb->id;
                                            $CustomerGroupNewsletter->save();
                                        }
                                    }
                                }

                                //gruppo zona commerciale
                                if($json['clienti'][$i]['zona']>0){
                                    $groupNewsletterDb = GroupNewsletter::where('code', 'ZC-' . $json['clienti'][$i]['zona'])->get()->first();
                                    if(isset($groupNewsletterDb)){
                                        $userGroupOnDb = CustomerGroupNewsletter::where('customer_id', $users->id)->where('group_newsletter_id', $groupNewsletterDb->id)->get()->first();
                                        if(!isset($userGroupOnDb)){
                                            $CustomerGroupNewsletter = new CustomerGroupNewsletter();
                                            $CustomerGroupNewsletter->customer_id = $users->id;
                                            $CustomerGroupNewsletter->group_newsletter_id = $groupNewsletterDb->id;
                                            $CustomerGroupNewsletter->save();
                                        }
                                    }
                                }

                                //gruppo canale di vendita 
                                if($json['clienti'][$i]['canale']>0){
                                    $groupNewsletterDb = GroupNewsletter::where('code', 'CV-' . $json['clienti'][$i]['canale'])->get()->first();
                                    if(isset($groupNewsletterDb)){
                                        $userGroupOnDb = CustomerGroupNewsletter::where('customer_id', $users->id)->where('group_newsletter_id', $groupNewsletterDb->id)->get()->first();
                                        if(!isset($userGroupOnDb)){
                                            $CustomerGroupNewsletter = new CustomerGroupNewsletter();
                                            $CustomerGroupNewsletter->customer_id = $users->id;
                                            $CustomerGroupNewsletter->group_newsletter_id = $groupNewsletterDb->id;
                                            $CustomerGroupNewsletter->save();
                                        }
                                    }
                                }

                                //gruppo categoria di clienti
                                if($json['clienti'][$i]['categoria']>0){
                                    $groupNewsletterDb = GroupNewsletter::where('code', 'CC-' . $json['clienti'][$i]['categoria'])->get()->first();
                                    if(isset($groupNewsletterDb)){
                                        $userGroupOnDb = CustomerGroupNewsletter::where('customer_id', $users->id)->where('group_newsletter_id', $groupNewsletterDb->id)->get()->first();
                                        if(!isset($userGroupOnDb)){
                                            $CustomerGroupNewsletter = new CustomerGroupNewsletter();
                                            $CustomerGroupNewsletter->customer_id = $users->id;
                                            $CustomerGroupNewsletter->group_newsletter_id = $groupNewsletterDb->id;
                                            $CustomerGroupNewsletter->save();
                                        }
                                    }
                                }

                                /* END CHECK GRUPPO SMS */
                                
                            }
                        }
                        
                        DB::commit();
                    } catch (Exception $ex) {
                        DB::rollback();
                        throw $ex;
                    }
                    //return risultati da stampare a schermo per verifica
                }
                //return $responseUsers;
            }
        }
        //return $jsonAuth;
    }


    /**
     * Get Group Agenti from external api
     * 
     * 
     * @return dtoSupports
     */
    public static function importGroupAgenti()
    {
        //API per autenticazione (POST con username e password comunicati dal cliente ) --> return json con token
        //{"username" : "tecnotrade", "password": "45TY*78B790e?"}

        $curlAuth = curl_init();

        curl_setopt_array($curlAuth, array(
            CURLOPT_URL => 'https://catalogo.specialformaggi.it/services/export/login',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "username" : "tecnotrade",
                "password": "45TY*78B790e?"
            }',
            CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
            ),
        ));
        
        $responseAuth = curl_exec($curlAuth);
        curl_close($curlAuth);

        if($responseAuth == ''){
            return 'Autenticazione fallita';
        }else{
            //API per estrazione articoli dopo la risposta dell'autenticazione
            $curlGroupAgente = curl_init();

            curl_setopt_array($curlGroupAgente, array(
                CURLOPT_URL => 'https://catalogo.specialformaggi.it/services/export/agenti',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "token": "' . $responseAuth . '"
                }',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            ));

            $responseGroupAgente = curl_exec($curlGroupAgente);

            curl_close($curlGroupAgente);
            $json = json_decode($responseGroupAgente, true);

            if($responseGroupAgente == ''){
                return 'Sessione scaduta';
            }else{
                for($i = 0; $i < count($json['agenti']); $i++){
                    DB::beginTransaction();

                    try {
                        $groupNewsletterDb = GroupNewsletter::where('code', 'AGE-' . $json['agenti'][$i]['id'])->get()->first();
                        if(isset($groupNewsletterDb)){
                            //aggiornamento dato??
                        }else{
                            // gruppo non presente aggiunta
                            $groupNewsletter = new GroupNewsletter();
                            $groupNewsletter->code = 'AGE-' . $json['agenti'][$i]['id'];
                            $groupNewsletter->description = 'Agente - ' . $json['agenti'][$i]['nome'];
                            $groupNewsletter->save();
                        }
                        
                        DB::commit();
                    } catch (Exception $ex) {
                        DB::rollback();
                        throw $ex;
                    }
                    //return risultati da stampare a schermo per verifica
                }
                //return $responseUsers;
            }
        }
        //return $jsonAuth;
    }

    /**
     * Get Canali Vendita from external api
     * 
     * 
     * @return dtoSupports
     */
    public static function importCanaliVendita()
    {
        //API per autenticazione (POST con username e password comunicati dal cliente ) --> return json con token
        //{"username" : "tecnotrade", "password": "45TY*78B790e?"}

        $curlAuth = curl_init();

        curl_setopt_array($curlAuth, array(
            CURLOPT_URL => 'https://catalogo.specialformaggi.it/services/export/login',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "username" : "tecnotrade",
                "password": "45TY*78B790e?"
            }',
            CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
            ),
        ));
        
        $responseAuth = curl_exec($curlAuth);
        curl_close($curlAuth);

        if($responseAuth == ''){
            return 'Autenticazione fallita';
        }else{
            //API per estrazione articoli dopo la risposta dell'autenticazione
            $curlCanaliVendita = curl_init();

            curl_setopt_array($curlCanaliVendita, array(
                CURLOPT_URL => 'https://catalogo.specialformaggi.it/services/export/canalivendita',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "token": "' . $responseAuth . '"
                }',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            ));

            $responseCanaliVendita = curl_exec($curlCanaliVendita);

            curl_close($curlCanaliVendita);
            $json = json_decode($responseCanaliVendita, true);

            if($responseCanaliVendita == ''){
                return 'Sessione scaduta';
            }else{
                for($i = 0; $i < count($json['canali']); $i++){
                    DB::beginTransaction();

                    try {
                        $groupNewsletterDb = GroupNewsletter::where('code', 'CV-' . $json['canali'][$i]['id'])->get()->first();
                        if(isset($groupNewsletterDb)){
                            //aggiornamento dato??
                        }else{
                            // gruppo non presente aggiunta
                            $groupNewsletter = new GroupNewsletter();
                            $groupNewsletter->code = 'CV-' . $json['canali'][$i]['id'];
                            $groupNewsletter->description = 'Canale vendita - ' . $json['canali'][$i]['descrizione'];
                            $groupNewsletter->save();
                        }
                        
                        DB::commit();
                    } catch (Exception $ex) {
                        DB::rollback();
                        throw $ex;
                    }
                    //return risultati da stampare a schermo per verifica
                }
                //return $responseUsers;
            }
        }
        //return $jsonAuth;
    }

    /**
     * Get Zone Commerciali from external api
     * 
     * 
     * @return dtoSupports
     */
    public static function importZoneCommerciali()
    {
        //API per autenticazione (POST con username e password comunicati dal cliente ) --> return json con token
        //{"username" : "tecnotrade", "password": "45TY*78B790e?"}

        $curlAuth = curl_init();

        curl_setopt_array($curlAuth, array(
            CURLOPT_URL => 'https://catalogo.specialformaggi.it/services/export/login',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "username" : "tecnotrade",
                "password": "45TY*78B790e?"
            }',
            CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
            ),
        ));
        
        $responseAuth = curl_exec($curlAuth);
        curl_close($curlAuth);

        if($responseAuth == ''){
            return 'Autenticazione fallita';
        }else{
            //API per estrazione articoli dopo la risposta dell'autenticazione
            $curlZoneCommerciali = curl_init();

            curl_setopt_array($curlZoneCommerciali, array(
                CURLOPT_URL => 'https://catalogo.specialformaggi.it/services/export/zonecommerciali',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "token": "' . $responseAuth . '"
                }',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            ));

            $responseZoneCommerciali= curl_exec($curlZoneCommerciali);

            curl_close($curlZoneCommerciali);
            $json = json_decode($responseZoneCommerciali, true);

            if($responseZoneCommerciali == ''){
                return 'Sessione scaduta';
            }else{
                for($i = 0; $i < count($json['zone']); $i++){
                    DB::beginTransaction();

                    try {
                        $groupNewsletterDb = GroupNewsletter::where('code', 'ZC-' . $json['zone'][$i]['id'])->get()->first();
                        if(isset($groupNewsletterDb)){
                            //aggiornamento dato??
                        }else{
                            // gruppo non presente aggiunta
                            $groupNewsletter = new GroupNewsletter();
                            $groupNewsletter->code = 'ZC-' . $json['zone'][$i]['id'];
                            $groupNewsletter->description = 'Zona Commerciale - ' . $json['zone'][$i]['descrizione'];
                            $groupNewsletter->save();
                        }
                        
                        DB::commit();
                    } catch (Exception $ex) {
                        DB::rollback();
                        throw $ex;
                    }
                    //return risultati da stampare a schermo per verifica
                }
                //return $responseUsers;
            }
        }
        //return $jsonAuth;
    }

    /**
     * Get Categorie Cliente from external api
     * 
     * 
     * @return dtoSupports
     */
    public static function importCategorieCliente()
    {
        //API per autenticazione (POST con username e password comunicati dal cliente ) --> return json con token
        //{"username" : "tecnotrade", "password": "45TY*78B790e?"}

        $curlAuth = curl_init();

        curl_setopt_array($curlAuth, array(
            CURLOPT_URL => 'https://catalogo.specialformaggi.it/services/export/login',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>'{
                "username" : "tecnotrade",
                "password": "45TY*78B790e?"
            }',
            CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
            ),
        ));
        
        $responseAuth = curl_exec($curlAuth);
        curl_close($curlAuth);

        if($responseAuth == ''){
            return 'Autenticazione fallita';
        }else{
            //API per estrazione articoli dopo la risposta dell'autenticazione
            $curlCategorieClienti = curl_init();

            curl_setopt_array($curlCategorieClienti, array(
                CURLOPT_URL => 'https://catalogo.specialformaggi.it/services/export/categorieclienti',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "token": "' . $responseAuth . '"
                }',
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json'
                ),
            ));

            $responseCategorieClienti = curl_exec($curlCategorieClienti);

            curl_close($curlCategorieClienti);
            $json = json_decode($responseCategorieClienti, true);

            if($responseCategorieClienti == ''){
                return 'Sessione scaduta';
            }else{
                for($i = 0; $i < count($json['categorie']); $i++){
                    DB::beginTransaction();

                    try {
                        $groupNewsletterDb = GroupNewsletter::where('code', 'CC-' . $json['categorie'][$i]['id'])->get()->first();
                        if(isset($groupNewsletterDb)){
                            //aggiornamento dato??
                        }else{
                            // gruppo non presente aggiunta
                            $groupNewsletter = new GroupNewsletter();
                            $groupNewsletter->code = 'CC-' . $json['categorie'][$i]['id'];
                            $groupNewsletter->description = 'Categoria Cliente - ' . $json['categorie'][$i]['descrizione'];
                            $groupNewsletter->save();
                        }
                        
                        DB::commit();
                    } catch (Exception $ex) {
                        DB::rollback();
                        throw $ex;
                    }
                    //return risultati da stampare a schermo per verifica
                }
                //return $responseUsers;
            }
        }
        //return $jsonAuth;
    }

    

    #endregion GET

   
}

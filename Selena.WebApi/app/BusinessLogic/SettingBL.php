<?php

namespace App\BusinessLogic;

use App\Setting;
use Symfony\Component\HttpFoundation\Request;
use App\Enums\SettingEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Enums\DbOperationsTypesEnum;
use Intervention\Image\Facades\Image;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use Exception;
use Illuminate\Support\Carbon;

class SettingBL
{
    public static $dirImage = '../storage/app/public/images/';

    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request $request
     * @param int $idLanguage
     * @param DbOperationsTypesEnum $dbOperationsTypesEnum
     */
    private static function _validate(Request $request, int $idLanguage, $dbOperationsTypesEnum)
    {
        if (is_null($request->code)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CodeNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        if (!SettingEnum::hasKey($request->code)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CodeNotEncoded), HttpResultsCodesEnum::InvalidPayload);
        }

        if ($dbOperationsTypesEnum == DbOperationsTypesEnum::INSERT) {
            if (Setting::where('code', $request->code)->count() > 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CodeAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
            }
        } else {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            $setting = Setting::find($request->id);
            if ($setting->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SettingNotFound), HttpResultsCodesEnum::InvalidPayload);
            }

            if ($setting->code != $request->code && Setting::where('code', $request->code)->count() > 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CodeAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($request->value)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ValueNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get all
     *
     * @return Setting
     */
    public static function getAll()
    {
        return Setting::all();
    }

    /**
     * Get all
     *
     * @return SettingEnum
     */
    public static function getCodes()
    {
        return SettingEnum::toArray();
    }

    /**
     * Get last sync
     *
     * @return DateTime
     */
    public static function getLastSync()
    {
        $setting = Setting::where('code', SettingEnum::LastDateSync);
        if ($setting->count() > 0) {
            return Carbon::parse($setting->first()->value)->format(HttpHelper::getLanguageDateTimeFormat());
        } else {
            return null;
        }
    }


    /**
     * Get Render menu
     *
     * @return RenderMenu
     */
    public static function getRenderMenu()
    {
        $setting = Setting::where('code', SettingEnum::MenuRender);
        if ($setting->count() > 0) {
            return $setting->first()->value;
        } else {
            return null;
        }
    }


    /**
     * Get Render News
     *
     * @return RenderNews
     */
    public static function getRenderNews()
    {
        $setting = Setting::where('code', SettingEnum::NewsRender);
        if ($setting->count() > 0) {
            return $setting->first()->value;
        } else {
            return null;
        }
    }


    /**
     * Get Render Blog
     *
     * @return RenderBlog
     */
    public static function getRenderBlog()
    {
        $setting = Setting::where('code', SettingEnum::BlogRender);
        if ($setting->count() > 0) {
            return $setting->first()->value;
        } else {
            return null;
        }
    }

    /**
     * Get contacts email
     * 
     * @return string
     */
    public static function getContactsEmail()
    {
        $setting = Setting::where('code', SettingEnum::ContactsEmail);
        if ($setting->count() > 0) {
            return $setting->first()->value;
        } else {
            return null;
        }
    }

    /**
     * Get by code
     *
     * @param string code
     * 
     * @return string
     */
    public static function getByCode(string $code)
    {
        $setting = Setting::where('code', $code);
        if ($setting->count() > 0) {
            return $setting->first()->value;
        } else {
            return null;
        }
    }

    #endregion GET

    #region PUT

    /**
     * Insert
     *
     * @param Request $request
     * @param int $idUser
     * @param int $idLanguage
     *
     * @return int
     */
    public static function insert(Request $request, int $idUser, $idLanguage)
    {
        static::_validate($request, $idLanguage, DbOperationsTypesEnum::INSERT);
        $setting = new Setting();
        $setting->code = $request->code;
        $setting->value = $request->value;
        $setting->description = $request->description;
        $setting->save();
        return $setting->id;
    }

    #endregion PUT

    #region POST

    /**
     * Update
     *
     * @param Request $request
     * @param int $idUser
     * @param int $idLanguage
     *
     * @return int
     */
    public static function update(Request $request, int $idUser, $idLanguage)
    {
        static::_validate($request, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $setting = Setting::find($request->id);
        $setting->code = $request->code;
        $setting->value = $request->value;
        $setting->description = $request->description;
        $setting->save();
    }


    public static function updateImageNameFavicon(Request $request, int $idUser, $idLanguage)
    {
   

        $Settings = Setting::where('code', 'Image Favicon')->get()->first();

        if (isset($Settings)){
                Image::make($request->imgBase64)->save(static::$dirImage . 'favicon');
                $Settings->value = static::getUrl() . 'favicon';
                $Settings->description ='Favicon Configuration'; 
               /*$data = base64_decode(str_replace('data:application/pdf;base64,', '', $request->imgBase64));
                file_put_contents(static::$dirImage . $request->imgName, $data);*/
                $Settings->save(); 
        }else{
            $Settings = new Setting();
            Image::make($request->imgBase64)->save(static::$dirImage  . 'favicon');
            $Settings->code = 'Image Favicon';
            $Settings->value = static::getUrl() . 'favicon';
            $Settings->description ='Favicon Configuration'; 
            $Settings->save(); 
          /*  $data = base64_decode(str_replace('data:application/pdf;base64,', '', $request->imgBase64));
            file_put_contents(static::$dirImage . $request->imgName, $data);*/
        }
    }


    private static function getUrl()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/';

    }


    /**
     * Update settigs
     *
     * @param Request $request
     * @param int $idUser
     * @return int
     */
    public static function updateSettings(Request $request)
    {
        foreach ($request->datasetting as $requestArray) {
            $setting = Setting::find($requestArray['id']);
            if (is_null($setting)) {
                throw new Exception(DictionaryBL::getTranslate(1, DictionariesCodesEnum::PageNotExist), HttpResultsCodesEnum::InvalidPayload);
            } else {
                $setting->value = $requestArray['value'];
                $setting->save();
            }
        }
    }

    #endregion POST

    #region DELETE

    /**
     * Delete
     *
     * @param int $id
     * @param int $idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $setting = Setting::find($id);

        if (is_null($setting)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SettingNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $setting->delete();
    }

    #endregion DELETE
}

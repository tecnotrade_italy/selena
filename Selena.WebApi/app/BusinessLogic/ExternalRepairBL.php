<?php

namespace App\BusinessLogic;

use App\BusinessName;
use App\Content;
use App\ContentLanguage;
use App\DtoModel\DtoInterventions;
use App\DtoModel\DtoItemGroupTechnicalSpecification;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use App\Helpers\ArrayHelper;
use App\Helpers\LogHelper;
use App\Customer;
use App\DtoModel\DtoExternalRepair;
use App\DtoModel\DtoInterventionsPhotos;
use App\DtoModel\DtoInterventionsProcessing;
use App\DtoModel\DtoItemGr;
use App\DtoModel\DtoQuotes;
use App\DtoModel\DtoUser;
use App\ExternalRepair;
use App\Item;
use App\GrStates;
use App\GrIntervention;
use App\GrInterventionPhoto;
use App\GrInterventionProcessing;
use App\GrQuotes;
use App\Helpers\HttpHelper;
use App\ItemGr;
use App\ItemLanguage;
use App\PaymentGr;
use App\People;
use App\QuotesGrIntervention;
use App\SelenaSqlViews;
use App\Setting;
use App\StatesQuotesGr;
use App\User;
use App\UsersDatas;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;


class ExternalRepairBL
{

    public static $dirImage = '../storage/app/public/images/Intervention/Foto3/GUASTI/';
    public static $dirImageInterventionReForn = '../storage/app/public/images/Intervention/Foto3/RE_FORN/';
    public static $dirImageIntervention = '../storage/app/public/images/Intervention/';
    
    private static function getUrlOpenFileImg()
  {
      if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
          $protocol = "https://";
      } else {
          $protocol = "http://";
      }
      return $protocol . $_SERVER["HTTP_HOST"];
  }


 /**
     * getAllexternalRepairSearch
     * 
     * @return DtoInterventions
     */
    public static function getAllexternalRepairSearch(request $request)
    {
        $result = collect();
        $content = '';
        $finalHtml = '';
        $strWhere = "";
        $business_name = $request->business_name;


       /* $ItemsId = ItemGr::where('code_gr', $CodeGrSearch)->get()->first();
        //$Intervention = GrIntervention::where('items_id', $ItemsId)->get()->first();

         if ($ItemsId->id != "") {
          $strWhere = $strWhere . ' OR gr_interventions.items_id = ' . $ItemsId->id . '';
        }*/

        $CodeGrSearch = $request->code_gr;
        if ($CodeGrSearch != "") {
          $strWhere = $strWhere . ' OR gi.code_gr ilike \'%' . $CodeGrSearch . '%\'';
        }

         $IdInterventionSearch = $request->code_intervention_gr;
        if ($IdInterventionSearch != "") {
          $strWhere = $strWhere . ' OR e.numero = ' . $IdInterventionSearch . '';
        }
          $DateSearch = $request->date;
        if ($DateSearch != "") {
          $strWhere = $strWhere . ' OR e.date ilike \'%' . $DateSearch . '%\'';
        }
               
            foreach (DB::select(
                'SELECT e.*
                FROM external_repair as e
                left join gr_interventions as gr_i on gr_i.id = e.id_intervention
                left join gr_items as gi on gi.id = gr_i.items_id
                WHERE e.id_business_name_supplier = :business_name ' . $strWhere,
            ['business_name' => $business_name],
            ) as $ExternalRepair) {

          /*  foreach (DB::select(
                'SELECT external_repair.*
                FROM external_repair
                WHERE external_repair.id_business_name_supplier = :business_name ' . $strWhere,
                ['business_name' => $business_name],
            ) as $ExternalRepair) {*/

            $idIntervention = GrIntervention::where('id', $ExternalRepair->id_intervention)->first();

            $DtoExternalRepair = new DtoExternalRepair();

            if (isset($ExternalRepair->id)) {
                $DtoExternalRepair->id = $ExternalRepair->id;
            } else {
                $DtoExternalRepair->id = '';
            }
            if (isset($ExternalRepair->numero)) {
                $DtoExternalRepair->numero = $ExternalRepair->numero;
            } else {
                $DtoExternalRepair->numero = '';
            }

            if (isset($ExternalRepair->id_librone_rip_esterna)) {
                $DtoExternalRepair->id_librone_rip_esterna = $ExternalRepair->id_librone_rip_esterna;
            } else {
                $DtoExternalRepair->id_librone_rip_esterna = '';
            }

            $timestamp = strtotime($ExternalRepair->date);
        if (isset($ExternalRepair->date)) {
            if ($timestamp === FALSE) {
                $DtoExternalRepair->date = $ExternalRepair->date;
            }else{
                $DtoExternalRepair->date = date('d/m/Y', $timestamp);
            }
        }else{
            $DtoExternalRepair->date = '';
        }
           /* if (isset($ExternalRepair->date)) {
                $DtoExternalRepair->date = $ExternalRepair->date;
            } else {
                $DtoExternalRepair->date = '';
            }*/

            if (isset($ExternalRepair->ddt_exit)) {
                $DtoExternalRepair->ddt_exit = $ExternalRepair->ddt_exit;
            } else {
                $DtoExternalRepair->ddt_exit = '';
            }
            if (isset($ExternalRepair->id_business_name_supplier)) {
                $DtoExternalRepair->id_business_name_supplier = BusinessName::where('id', $ExternalRepair->id_business_name_supplier)->first()->business_name;
            } else {
                $DtoExternalRepair->id_business_name_supplier = '';
            }

            if (isset($idIntervention->description)) {
                $DtoExternalRepair->description = $idIntervention->description;
            } else {
                $DtoExternalRepair->description = '';
            }

       /*     if (isset($idIntervention->imgmodule)) {
                $DtoExternalRepair->imgmodule = $idIntervention->imgmodule;
            } else {
                $DtoExternalRepair->imgmodule = '';
            }*/

            if (isset($idIntervention)) {
                if (file_exists(static::$dirImage . $idIntervention->code_intervention_gr . '.pdf' )) {
                    $DtoExternalRepair->imgmodule = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/GUASTI/' . $idIntervention->code_intervention_gr . '.pdf';
                } else {
                    if ($idIntervention->imgmodule != '' && !is_null($idIntervention->imgmodule)) {
                        $DtoExternalRepair->imgmodule = $idIntervention->imgmodule;
                            } else {
                                $DtoExternalRepair->imgmodule = '';
                            }
                        }
            }    
        
          if (file_exists(static::$dirImageInterventionReForn . $ExternalRepair->id_librone_rip_esterna . '.pdf' )) {
                $DtoExternalRepair->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE_FORN/' . $ExternalRepair->id_librone_rip_esterna . '.pdf';
            } else {
                if ($ExternalRepair->img_rep_test != '' && $ExternalRepair->img_rep_test != null) {
                        $DtoExternalRepair->img_rep_test = $ExternalRepair->img_rep_test;
                    } else {
                        $DtoExternalRepair->img_rep_test = '';
                    }
            }                

        $GrInterventionImgPhoto = GrInterventionPhoto::where('intervention_id', $ExternalRepair->id_intervention)->first();
        if (isset($GrInterventionImgPhoto)) {
            if (file_exists(static::$dirImageIntervention . $GrInterventionImgPhoto->img . $GrInterventionImgPhoto->code_intervention_gr . '.jpg' )) {
                $DtoExternalRepair->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $GrInterventionImgPhoto->img . $GrInterventionImgPhoto->code_intervention_gr . '.jpg';
            } else {
            if (file_exists(static::$dirImageIntervention . $GrInterventionImgPhoto->img . $GrInterventionImgPhoto->code_intervention_gr . '.JPG' )) {
                $DtoExternalRepair->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $GrInterventionImgPhoto->img . $GrInterventionImgPhoto->code_intervention_gr . '.JPG';
            }else{
                if ($GrInterventionImgPhoto->img != '' && !is_null($GrInterventionImgPhoto->img)) {
                    $pos = strpos($GrInterventionImgPhoto->img, $GrInterventionImgPhoto->intervention_id);
                    if($pos === false){
                        $DtoExternalRepair->img = '';
                    }else{
                        $DtoExternalRepair->img = $GrInterventionImgPhoto->img;
                    }
                    
                } else {
                    $DtoExternalRepair->img = '';
                }
            }  
        }
    }

            if (isset($ExternalRepair->id_intervention)) {
                $DtoExternalRepair->id_intervention = $ExternalRepair->id_intervention;
            } else {
                $DtoExternalRepair->id_intervention = '';
            }
            if (isset($idIntervention->items_id)) {
               $DtoExternalRepair->code_gr = ItemGr::where('id', $idIntervention->items_id)->first()->code_gr; 
            } else {
                $DtoExternalRepair->code_gr = '';
            }

            $timestamp1 = strtotime($ExternalRepair->date_return_product_from_the_supplier);
            if (isset($ExternalRepair->date_return_product_from_the_supplier)) {
                if ($timestamp1 === FALSE) {
                    $DtoExternalRepair->date_return_product_from_the_supplier = $ExternalRepair->date_return_product_from_the_supplier;
                }else{
                    $DtoExternalRepair->date_return_product_from_the_supplier = date('d/m/Y', $timestamp1);
                }
            }else{
                $DtoExternalRepair->date_return_product_from_the_supplier = '';
            }

           /* if (isset($ExternalRepair->date_return_product_from_the_supplier)) {
                $DtoExternalRepair->date_return_product_from_the_supplier = $ExternalRepair->date_return_product_from_the_supplier;
            } else {
                $DtoExternalRepair->date_return_product_from_the_supplier = '';
            }*/

            if (isset($ExternalRepair->ddt_supplier)) {
                $DtoExternalRepair->ddt_supplier = $ExternalRepair->ddt_supplier;
            } else {
                $DtoExternalRepair->ddt_supplier = '';
            }
            if (isset($ExternalRepair->reference_supplier)) {
                $DtoExternalRepair->reference_supplier = $ExternalRepair->reference_supplier;
            } else {
                $DtoExternalRepair->reference_supplier = '';
            }
            if (isset($ExternalRepair->notes_from_repairman)) {
                $DtoExternalRepair->notes_from_repairman = $ExternalRepair->notes_from_repairman;
            } else {
                $DtoExternalRepair->notes_from_repairman = '';
            }
             if (isset($ExternalRepair->price)) {
                $DtoExternalRepair->price = $ExternalRepair->price;
            } else {
                $DtoExternalRepair->price = '';
            }

            $result->push($DtoExternalRepair);
        }
        return $result;
    }

 /**
     * getexternalRepairSearchViewClose
     * 
     * @return DtoInterventions
     */
    public static function getexternalRepairSearchViewClose(request $request)
    {
        $result = collect();
   
            foreach (DB::select(
               'SELECT external_repair.*
                FROM external_repair
                WHERE external_repair.date_return_product_from_the_supplier is not null 
                and external_repair.ddt_supplier is not null limit 100'        
            ) as $ExternalRepair) {

            $idIntervention = GrIntervention::where('id', $ExternalRepair->id_intervention)->first();
            $DtoExternalRepair = new DtoExternalRepair();

            if (isset($ExternalRepair->id)) {
                $DtoExternalRepair->id = $ExternalRepair->id;
            } else {
                $DtoExternalRepair->id = '';
            }
             if (isset($ExternalRepair->numero)) {
                $DtoExternalRepair->numero = $ExternalRepair->numero;
            } else {
                $DtoExternalRepair->numero = '';
            }
            if (isset($ExternalRepair->id_librone_rip_esterna)) {
                $DtoExternalRepair->id_librone_rip_esterna = $ExternalRepair->id_librone_rip_esterna;
            } else {
                $DtoExternalRepair->id_librone_rip_esterna = '';
            }

            $timestamp = strtotime($ExternalRepair->date);
        if (isset($ExternalRepair->date)) {
            if ($timestamp === FALSE) {
                $DtoExternalRepair->date = $ExternalRepair->date;
            }else{
                $DtoExternalRepair->date = date('d/m/Y', $timestamp);
            }
        }else{
            $DtoExternalRepair->date = '';
        }
           /* if (isset($ExternalRepair->date)) {
                $DtoExternalRepair->date = $ExternalRepair->date;
            } else {
                $DtoExternalRepair->date = '';
            }*/

            if (isset($ExternalRepair->ddt_exit)) {
                $DtoExternalRepair->ddt_exit = $ExternalRepair->ddt_exit;
            } else {
                $DtoExternalRepair->ddt_exit = '';
            }
            if (isset($ExternalRepair->id_business_name_supplier)) {
                $DtoExternalRepair->id_business_name_supplier = BusinessName::where('id', $ExternalRepair->id_business_name_supplier)->first()->business_name;
            } else {
                $DtoExternalRepair->id_business_name_supplier = '';
            }

            if (isset($idIntervention->description)) {
                $DtoExternalRepair->description = $idIntervention->description;
            } else {
                $DtoExternalRepair->description = '';
            }

       /*     if (isset($idIntervention->imgmodule)) {
                $DtoExternalRepair->imgmodule = $idIntervention->imgmodule;
            } else {
                $DtoExternalRepair->imgmodule = '';
            }*/

            if (isset($idIntervention)) {
                if (file_exists(static::$dirImage . $idIntervention->code_intervention_gr . '.pdf' )) {
                    $DtoExternalRepair->imgmodule = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/GUASTI/' . $idIntervention->code_intervention_gr . '.pdf';
                } else {
                    if ($idIntervention->imgmodule != '' && !is_null($idIntervention->imgmodule)) {
                        $DtoExternalRepair->imgmodule = $idIntervention->imgmodule;
                            } else {
                                $DtoExternalRepair->imgmodule = '';
                            }
                        }
            }    
        
          if (file_exists(static::$dirImageInterventionReForn . $ExternalRepair->id_librone_rip_esterna . '.pdf' )) {
                $DtoExternalRepair->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE_FORN/' . $ExternalRepair->id_librone_rip_esterna . '.pdf';
            } else {
                if ($ExternalRepair->img_rep_test != '' && $ExternalRepair->img_rep_test != null) {
                        $DtoExternalRepair->img_rep_test = $ExternalRepair->img_rep_test;
                    } else {
                        $DtoExternalRepair->img_rep_test = '';
                    }
            }                

        $GrInterventionImgPhoto = GrInterventionPhoto::where('intervention_id', $ExternalRepair->id_intervention)->first();
        if (isset($GrInterventionImgPhoto)) {
            if (file_exists(static::$dirImageIntervention . $GrInterventionImgPhoto->img . $GrInterventionImgPhoto->code_intervention_gr . '.jpg' )) {
                $DtoExternalRepair->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $GrInterventionImgPhoto->img . $GrInterventionImgPhoto->code_intervention_gr . '.jpg';
            } else {
            if (file_exists(static::$dirImageIntervention . $GrInterventionImgPhoto->img . $GrInterventionImgPhoto->code_intervention_gr . '.JPG' )) {
                $DtoExternalRepair->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $GrInterventionImgPhoto->img . $GrInterventionImgPhoto->code_intervention_gr . '.JPG';
            }else{
                if ($GrInterventionImgPhoto->img != '' && !is_null($GrInterventionImgPhoto->img)) {
                    $pos = strpos($GrInterventionImgPhoto->img, $GrInterventionImgPhoto->intervention_id);
                    if($pos === false){
                        $DtoExternalRepair->img = '';
                    }else{
                        $DtoExternalRepair->img = $GrInterventionImgPhoto->img;
                    }
                    
                } else {
                    $DtoExternalRepair->img = '';
                }
            }  
        }
    }

            if (isset($ExternalRepair->id_intervention)) {
                $DtoExternalRepair->id_intervention = $ExternalRepair->id_intervention;
            } else {
                $DtoExternalRepair->id_intervention = '';
            }
            if (isset($idIntervention->items_id)) {
               $DtoExternalRepair->code_gr = ItemGr::where('id', $idIntervention->items_id)->first()->code_gr; 
            } else {
                $DtoExternalRepair->code_gr = '';
            }

            $timestamp1 = strtotime($ExternalRepair->date_return_product_from_the_supplier);
            if (isset($ExternalRepair->date_return_product_from_the_supplier)) {
                if ($timestamp1 === FALSE) {
                    $DtoExternalRepair->date_return_product_from_the_supplier = $ExternalRepair->date_return_product_from_the_supplier;
                }else{
                    $DtoExternalRepair->date_return_product_from_the_supplier = date('d/m/Y', $timestamp1);
                }
            }else{
                $DtoExternalRepair->date_return_product_from_the_supplier = '';
            }

           /* if (isset($ExternalRepair->date_return_product_from_the_supplier)) {
                $DtoExternalRepair->date_return_product_from_the_supplier = $ExternalRepair->date_return_product_from_the_supplier;
            } else {
                $DtoExternalRepair->date_return_product_from_the_supplier = '';
            }*/

            if (isset($ExternalRepair->ddt_supplier)) {
                $DtoExternalRepair->ddt_supplier = $ExternalRepair->ddt_supplier;
            } else {
                $DtoExternalRepair->ddt_supplier = '';
            }
            if (isset($ExternalRepair->reference_supplier)) {
                $DtoExternalRepair->reference_supplier = $ExternalRepair->reference_supplier;
            } else {
                $DtoExternalRepair->reference_supplier = '';
            }
            if (isset($ExternalRepair->notes_from_repairman)) {
                $DtoExternalRepair->notes_from_repairman = $ExternalRepair->notes_from_repairman;
            } else {
                $DtoExternalRepair->notes_from_repairman = '';
            }
             if (isset($ExternalRepair->price)) {
                $DtoExternalRepair->price = $ExternalRepair->price;
            } else {
                $DtoExternalRepair->price = '';
            }

            $result->push($DtoExternalRepair);
        }
        return $result;
    }

 /**
     * getexternalRepairSearchViewOpen
     * 
     * @return DtoInterventions
     */
    public static function getexternalRepairSearchViewOpen(request $request)
    {

        $result = collect();

            foreach (DB::select(
               'SELECT external_repair.*
                FROM external_repair
                WHERE external_repair.date_return_product_from_the_supplier is null 
                and external_repair.ddt_supplier is null limit 100'        
            ) as $ExternalRepair) {

            $idIntervention = GrIntervention::where('id', $ExternalRepair->id_intervention)->first();

            $DtoExternalRepair = new DtoExternalRepair();

            if (isset($ExternalRepair->id)) {
                $DtoExternalRepair->id = $ExternalRepair->id;
            } else {
                $DtoExternalRepair->id = '';
            }
             if (isset($ExternalRepair->numero)) {
                $DtoExternalRepair->numero = $ExternalRepair->numero;
            } else {
                $DtoExternalRepair->numero = '';
            }
            if (isset($ExternalRepair->id_librone_rip_esterna)) {
                $DtoExternalRepair->id_librone_rip_esterna = $ExternalRepair->id_librone_rip_esterna;
            } else {
                $DtoExternalRepair->id_librone_rip_esterna = '';
            }

            $timestamp = strtotime($ExternalRepair->date);
        if (isset($ExternalRepair->date)) {
            if ($timestamp === FALSE) {
                $DtoExternalRepair->date = $ExternalRepair->date;
            }else{
                $DtoExternalRepair->date = date('d/m/Y', $timestamp);
            }
        }else{
            $DtoExternalRepair->date = '';
        }
           /* if (isset($ExternalRepair->date)) {
                $DtoExternalRepair->date = $ExternalRepair->date;
            } else {
                $DtoExternalRepair->date = '';
            }*/

            if (isset($ExternalRepair->ddt_exit)) {
                $DtoExternalRepair->ddt_exit = $ExternalRepair->ddt_exit;
            } else {
                $DtoExternalRepair->ddt_exit = '';
            }
            if (isset($ExternalRepair->id_business_name_supplier)) {
                $DtoExternalRepair->id_business_name_supplier = BusinessName::where('id', $ExternalRepair->id_business_name_supplier)->first()->business_name;
            } else {
                $DtoExternalRepair->id_business_name_supplier = '';
            }

            if (isset($idIntervention->description)) {
                $DtoExternalRepair->description = $idIntervention->description;
            } else {
                $DtoExternalRepair->description = '';
            }

       /*     if (isset($idIntervention->imgmodule)) {
                $DtoExternalRepair->imgmodule = $idIntervention->imgmodule;
            } else {
                $DtoExternalRepair->imgmodule = '';
            }*/

            if (isset($idIntervention)) {
                if (file_exists(static::$dirImage . $idIntervention->code_intervention_gr . '.pdf' )) {
                    $DtoExternalRepair->imgmodule = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/GUASTI/' . $idIntervention->code_intervention_gr . '.pdf';
                } else {
                    if ($idIntervention->imgmodule != '' && !is_null($idIntervention->imgmodule)) {
                        $DtoExternalRepair->imgmodule = $idIntervention->imgmodule;
                            } else {
                                $DtoExternalRepair->imgmodule = '';
                            }
                        }
            }    
        
          if (file_exists(static::$dirImageInterventionReForn . $ExternalRepair->id_librone_rip_esterna . '.pdf' )) {
                $DtoExternalRepair->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE_FORN/' . $ExternalRepair->id_librone_rip_esterna . '.pdf';
            } else {
                if ($ExternalRepair->img_rep_test != '' && $ExternalRepair->img_rep_test != null) {
                        $DtoExternalRepair->img_rep_test = $ExternalRepair->img_rep_test;
                    } else {
                        $DtoExternalRepair->img_rep_test = '';
                    }
            }                

        $GrInterventionImgPhoto = GrInterventionPhoto::where('intervention_id', $ExternalRepair->id_intervention)->first();
        if (isset($GrInterventionImgPhoto)) {
            if (file_exists(static::$dirImageIntervention . $GrInterventionImgPhoto->img . $GrInterventionImgPhoto->code_intervention_gr . '.jpg' )) {
                $DtoExternalRepair->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $GrInterventionImgPhoto->img . $GrInterventionImgPhoto->code_intervention_gr . '.jpg';
            } else {
            if (file_exists(static::$dirImageIntervention . $GrInterventionImgPhoto->img . $GrInterventionImgPhoto->code_intervention_gr . '.JPG' )) {
                $DtoExternalRepair->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $GrInterventionImgPhoto->img . $GrInterventionImgPhoto->code_intervention_gr . '.JPG';
            }else{
                if ($GrInterventionImgPhoto->img != '' && !is_null($GrInterventionImgPhoto->img)) {
                    $pos = strpos($GrInterventionImgPhoto->img, $GrInterventionImgPhoto->intervention_id);
                    if($pos === false){
                        $DtoExternalRepair->img = '';
                    }else{
                        $DtoExternalRepair->img = $GrInterventionImgPhoto->img;
                    }
                    
                } else {
                    $DtoExternalRepair->img = '';
                }
            }  
        }
    }

            if (isset($ExternalRepair->id_intervention)) {
                $DtoExternalRepair->id_intervention = $ExternalRepair->id_intervention;
            } else {
                $DtoExternalRepair->id_intervention = '';
            }
            if (isset($idIntervention->items_id)) {
               $DtoExternalRepair->code_gr = ItemGr::where('id', $idIntervention->items_id)->first()->code_gr; 
            } else {
                $DtoExternalRepair->code_gr = '';
            }

            $timestamp1 = strtotime($ExternalRepair->date_return_product_from_the_supplier);
            if (isset($ExternalRepair->date_return_product_from_the_supplier)) {
                if ($timestamp1 === FALSE) {
                    $DtoExternalRepair->date_return_product_from_the_supplier = $ExternalRepair->date_return_product_from_the_supplier;
                }else{
                    $DtoExternalRepair->date_return_product_from_the_supplier = date('d/m/Y', $timestamp1);
                }
            }else{
                $DtoExternalRepair->date_return_product_from_the_supplier = '';
            }

           /* if (isset($ExternalRepair->date_return_product_from_the_supplier)) {
                $DtoExternalRepair->date_return_product_from_the_supplier = $ExternalRepair->date_return_product_from_the_supplier;
            } else {
                $DtoExternalRepair->date_return_product_from_the_supplier = '';
            }*/

            if (isset($ExternalRepair->ddt_supplier)) {
                $DtoExternalRepair->ddt_supplier = $ExternalRepair->ddt_supplier;
            } else {
                $DtoExternalRepair->ddt_supplier = '';
            }
            if (isset($ExternalRepair->reference_supplier)) {
                $DtoExternalRepair->reference_supplier = $ExternalRepair->reference_supplier;
            } else {
                $DtoExternalRepair->reference_supplier = '';
            }
            if (isset($ExternalRepair->notes_from_repairman)) {
                $DtoExternalRepair->notes_from_repairman = $ExternalRepair->notes_from_repairman;
            } else {
                $DtoExternalRepair->notes_from_repairman = '';
            }
             if (isset($ExternalRepair->price)) {
                $DtoExternalRepair->price = $ExternalRepair->price;
            } else {
                $DtoExternalRepair->price = '';
            }

            $result->push($DtoExternalRepair);
        }
        return $result;
    }
    /**
     * getAllexternalRepair
     * 
     * @return DtoInterventions
     */
    public static function getAllexternalRepair($idLanguage)
    {
        $result = collect();

        foreach (ExternalRepair::orderBy('id_librone_rip_esterna', 'DESC')->take(150)->get() as $ExternalRepair) {
            $idIntervention = GrIntervention::where('id', $ExternalRepair->id_intervention)->first();

            $DtoExternalRepair = new DtoExternalRepair();

            if (isset($ExternalRepair->id)) {
                $DtoExternalRepair->id = $ExternalRepair->id;
            } else {
                $DtoExternalRepair->id = '';
            }
             if (isset($ExternalRepair->numero)) {
                $DtoExternalRepair->numero = $ExternalRepair->numero;
            } else {
                $DtoExternalRepair->numero = '';
            }

            if (isset($ExternalRepair->id_librone_rip_esterna)) {
                $DtoExternalRepair->id_librone_rip_esterna = $ExternalRepair->id_librone_rip_esterna;
            } else {
                $DtoExternalRepair->id_librone_rip_esterna = '';
            }

            $timestamp = strtotime($ExternalRepair->date);
        if (isset($ExternalRepair->date)) {
            if ($timestamp === FALSE) {
                $DtoExternalRepair->date = $ExternalRepair->date;
            }else{
                $DtoExternalRepair->date = date('d/m/Y', $timestamp);
            }
        }else{
            $DtoExternalRepair->date = '';
        }
           /* if (isset($ExternalRepair->date)) {
                $DtoExternalRepair->date = $ExternalRepair->date;
            } else {
                $DtoExternalRepair->date = '';
            }*/

            if (isset($ExternalRepair->ddt_exit)) {
                $DtoExternalRepair->ddt_exit = $ExternalRepair->ddt_exit;
            } else {
                $DtoExternalRepair->ddt_exit = '';
            }
            if (isset($ExternalRepair->id_business_name_supplier)) {
                $DtoExternalRepair->id_business_name_supplier = BusinessName::where('id', $ExternalRepair->id_business_name_supplier)->first()->business_name;
            } else {
                $DtoExternalRepair->id_business_name_supplier = '';
            }

            if (isset($idIntervention->description)) {
                $DtoExternalRepair->description = $idIntervention->description;
            } else {
                $DtoExternalRepair->description = '';
            }

       /*     if (isset($idIntervention->imgmodule)) {
                $DtoExternalRepair->imgmodule = $idIntervention->imgmodule;
            } else {
                $DtoExternalRepair->imgmodule = '';
            }*/

            if (isset($idIntervention)) {
                if (file_exists(static::$dirImage . $idIntervention->code_intervention_gr . '.pdf' )) {
                    $DtoExternalRepair->imgmodule = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/GUASTI/' . $idIntervention->code_intervention_gr . '.pdf';
                } else {
                    if ($idIntervention->imgmodule != '' && !is_null($idIntervention->imgmodule)) {
                        $DtoExternalRepair->imgmodule = $idIntervention->imgmodule;
                            } else {
                                $DtoExternalRepair->imgmodule = '';
                            }
                        }
            }    
        
          if (file_exists(static::$dirImageInterventionReForn . $ExternalRepair->id_librone_rip_esterna . '.pdf' )) {
                $DtoExternalRepair->img_rep_test = static::getUrlOpenFileImg() . '/storage/images/Intervention/Foto3/RE_FORN/' . $ExternalRepair->id_librone_rip_esterna . '.pdf';
            } else {
                if ($ExternalRepair->img_rep_test != '' && $ExternalRepair->img_rep_test != null) {
                        $DtoExternalRepair->img_rep_test = $ExternalRepair->img_rep_test;
                    } else {
                        $DtoExternalRepair->img_rep_test = '';
                    }
            }                

        $GrInterventionImgPhoto = GrInterventionPhoto::where('intervention_id', $ExternalRepair->id_intervention)->first();
        if (isset($GrInterventionImgPhoto)) {
            if (file_exists(static::$dirImageIntervention . $GrInterventionImgPhoto->img . $GrInterventionImgPhoto->code_intervention_gr . '.jpg' )) {
                $DtoExternalRepair->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $GrInterventionImgPhoto->img . $GrInterventionImgPhoto->code_intervention_gr . '.jpg';
            } else {
            if (file_exists(static::$dirImageIntervention . $GrInterventionImgPhoto->img . $GrInterventionImgPhoto->code_intervention_gr . '.JPG' )) {
                $DtoExternalRepair->img = static::getUrlOpenFileImg() . '/storage/images/Intervention/' . $GrInterventionImgPhoto->img . $GrInterventionImgPhoto->code_intervention_gr . '.JPG';
            }else{
                if ($GrInterventionImgPhoto->img != '' && !is_null($GrInterventionImgPhoto->img)) {
                    $pos = strpos($GrInterventionImgPhoto->img, $GrInterventionImgPhoto->intervention_id);
                    if($pos === false){
                        $DtoExternalRepair->img = '';
                    }else{
                        $DtoExternalRepair->img = $GrInterventionImgPhoto->img;
                    }
                    
                } else {
                    $DtoExternalRepair->img = '';
                }
            }  
        }
    }

            if (isset($ExternalRepair->id_intervention)) {
                $DtoExternalRepair->id_intervention = $ExternalRepair->id_intervention;
            } else {
                $DtoExternalRepair->id_intervention = '';
            }
            if (isset($idIntervention->items_id)) {
               $DtoExternalRepair->code_gr = ItemGr::where('id', $idIntervention->items_id)->first()->code_gr; 
            } else {
                $DtoExternalRepair->code_gr = '';
            }

            $timestamp1 = strtotime($ExternalRepair->date_return_product_from_the_supplier);
            if (isset($ExternalRepair->date_return_product_from_the_supplier)) {
                if ($timestamp1 === FALSE) {
                    $DtoExternalRepair->date_return_product_from_the_supplier = $ExternalRepair->date_return_product_from_the_supplier;
                }else{
                    $DtoExternalRepair->date_return_product_from_the_supplier = date('d/m/Y', $timestamp1);
                }
            }else{
                $DtoExternalRepair->date_return_product_from_the_supplier = '';
            }

           /* if (isset($ExternalRepair->date_return_product_from_the_supplier)) {
                $DtoExternalRepair->date_return_product_from_the_supplier = $ExternalRepair->date_return_product_from_the_supplier;
            } else {
                $DtoExternalRepair->date_return_product_from_the_supplier = '';
            }*/

            if (isset($ExternalRepair->ddt_supplier)) {
                $DtoExternalRepair->ddt_supplier = $ExternalRepair->ddt_supplier;
            } else {
                $DtoExternalRepair->ddt_supplier = '';
            }
            if (isset($ExternalRepair->reference_supplier)) {
                $DtoExternalRepair->reference_supplier = $ExternalRepair->reference_supplier;
            } else {
                $DtoExternalRepair->reference_supplier = '';
            }
            if (isset($ExternalRepair->notes_from_repairman)) {
                $DtoExternalRepair->notes_from_repairman = $ExternalRepair->notes_from_repairman;
            } else {
                $DtoExternalRepair->notes_from_repairman = '';
            }
             if (isset($ExternalRepair->price)) {
                $DtoExternalRepair->price = $ExternalRepair->price;
            } else {
                $DtoExternalRepair->price = '';
            }

            $result->push($DtoExternalRepair);
        }
        return $result;
    }

    
/**
     * Get by getbyFaultModule
     * 
     * @param int id
     * @return DtoInterventions
     */
        public static function getbyFaultModule(int $id)
    {
        $result = collect();
        $Intervention = GrIntervention::where('id', $id)->first();

            $DtoInterventions = new DtoInterventions();

             if (isset($Intervention->customers_id)) {
                $DtoInterventions->customer_id = $Intervention->customers_id;
            }

             if (isset($Intervention->description)) {
                $DtoInterventions->description = $Intervention->description;
            }
            if (isset($Intervention->defect)) {
                $DtoInterventions->defect = $Intervention->defect;
            }

            if (isset($Intervention->nr_ddt)) {
                $DtoInterventions->nr_ddt = $Intervention->nr_ddt;
            }
            if (isset($Intervention->idLanguage)) {
                $DtoInterventions->idLanguage = $Intervention->idLanguage;
            }
            if (isset($Intervention->date_ddt)) {
                $DtoInterventions->date_ddt = $Intervention->date_ddt;
            }
            if (isset($Intervention->unrepairable)) {
                $DtoInterventions->unrepairable = $Intervention->unrepairable;
            }
            if (isset($Intervention->create_quote)) {
                $DtoInterventions->create_quote = $Intervention->create_quote;
            }
            if (isset($Intervention->referent)) {
                $DtoInterventions->referent = $Intervention->referent;
            }
             if (isset($Intervention->plant_type)) {
                $DtoInterventions->plant_type = $Intervention->plant_type;
            }
             if (isset($Intervention->trolley_type)) {
                $DtoInterventions->trolley_type = $Intervention->trolley_type;
            }
             if (isset($Intervention->series)) {
                $DtoInterventions->series = $Intervention->series;
            }
            if (isset($Intervention->voltage)) {
                $DtoInterventions->voltage = $Intervention->voltage;
            }
            if (isset($Intervention->exit_notes)) {
                $DtoInterventions->exit_notes = $Intervention->exit_notes;
            }
            $result->push($DtoInterventions);
     
        return $result;
    }



/**
     * Get by getByRepair
     * 
     * @param int id
     * @return DtoExternalRepair
     */
        public static function getByRepair(int $id)
    {

        $ExternalRepair = ExternalRepair::where('id', $id)->first();
        $Intervention = GrIntervention::where('id', $ExternalRepair->id_intervention)->first();
        $DtoExternalRepair = new DtoExternalRepair();

        if (isset($ExternalRepair->id)) {
                $DtoExternalRepair->id = $ExternalRepair->id;
        }
        if (isset($ExternalRepair->id_intervention)) {
                $DtoExternalRepair->id_intervention = $ExternalRepair->id_intervention;
        }
        if (isset($ExternalRepair->id_librone_rip_esterna)) {
                $DtoExternalRepair->id_librone_rip_esterna = $ExternalRepair->id_librone_rip_esterna;
        }
            
        if (isset($ExternalRepair->id_business_name_supplier)) {
                $DtoExternalRepair->id_business_name_supplier =  $ExternalRepair->id_business_name_supplier;
                $DtoExternalRepair->Description_business_name_supplier = BusinessName::where('id', $ExternalRepair->id_business_name_supplier)->first()->business_name;
        }else {
                $DtoExternalRepair->Description_business_name_supplier = '';
        }

        if (isset($ExternalRepair->date)) {
                $DtoExternalRepair->date = $ExternalRepair->date;
        }else {
                $DtoExternalRepair->date = '';
        }
         if (isset($Intervention->items_id)) {
            $DtoExternalRepair->code_gr = ItemGr::where('id', $Intervention->items_id)->first()->code_gr;
        }else {
            $DtoExternalRepair->code_gr = '';
        }
          if (isset($Intervention->description)) {
                $DtoExternalRepair->description = $Intervention->description;
        }else {
                $DtoExternalRepair->description = '';
        }

        if (isset($ExternalRepair->ddt_exit)) {
                $DtoExternalRepair->ddt_exit = $ExternalRepair->ddt_exit;
        }else {
                $DtoExternalRepair->ddt_exit = '';
        }
         if (isset($Intervention->imgmodule)) {
            $DtoExternalRepair->imgName = $Intervention->imgmodule;
        }

        if (isset($ExternalRepair->date_return_product_from_the_supplier)) {
                $DtoExternalRepair->date_return_product_from_the_supplier = $ExternalRepair->date_return_product_from_the_supplier;
        }else {
                $DtoExternalRepair->date_return_product_from_the_supplier = '';
        }
        if (isset($ExternalRepair->ddt_supplier)) {
                $DtoExternalRepair->ddt_supplier = $ExternalRepair->ddt_supplier;
        }else {
                $DtoExternalRepair->ddt_supplier = '';
        }
        if (isset($ExternalRepair->reference_supplier)) {
                $DtoExternalRepair->reference_supplier = $ExternalRepair->reference_supplier;
        }else {
                $DtoExternalRepair->reference_supplier = '';
        }
        if (isset($ExternalRepair->notes_from_repairman)) {
                $DtoExternalRepair->notes_from_repairman = $ExternalRepair->notes_from_repairman;
        }else {
                $DtoExternalRepair->notes_from_repairman = '';
        }
          if (isset($ExternalRepair->price)) {
                $DtoExternalRepair->price = $ExternalRepair->price;
        }else {
                $DtoExternalRepair->price = '';
        }
      
       

        $DtoInterventions = new DtoInterventions();
            if (isset($Intervention->user_id)) {
                $DtoInterventions->user_id = UsersDatas::where('user_id', $Intervention->user_id)->first()->business_name;
            }
             if (isset($Intervention->description)) {
                $DtoInterventions->description = $Intervention->description;
            }
            if (isset($Intervention->defect)) {
                $DtoInterventions->defect = $Intervention->defect;
            }
            if (isset($Intervention->nr_ddt)) {
                $DtoInterventions->nr_ddt = $Intervention->nr_ddt;
            }
            if (isset($Intervention->idLanguage)) {
                $DtoInterventions->idLanguage = $Intervention->idLanguage;
            }
            if (isset($Intervention->date_ddt)) {
                $DtoInterventions->date_ddt = $Intervention->date_ddt;
            }
       
            if (isset($Intervention->create_quote)) {
                $DtoInterventions->create_quote = $Intervention->create_quote;
            }
            if (isset($Intervention->external_referent_id)) {
                $DtoInterventions->external_referent_id = People::where('id', $Intervention->external_referent_id)->first()->name;
            }
             if (isset($Intervention->plant_type)) {
                $DtoInterventions->plant_type = $Intervention->plant_type;
            }
             if (isset($Intervention->trolley_type)) {
                $DtoInterventions->trolley_type = $Intervention->trolley_type;
            }
             if (isset($Intervention->series)) {
                $DtoInterventions->series = $Intervention->series;
            }
            if (isset($Intervention->voltage)) {
                $DtoInterventions->voltage = $Intervention->voltage;
            }
            if (isset($Intervention->exit_notes)) {
                $DtoInterventions->exit_notes = $Intervention->exit_notes;
            }
            $DtoExternalRepair->selectFaultModule->push($DtoInterventions);

        return $DtoExternalRepair;
}

    #region DELETE
        /**
         * Delete
         *
         * @param int $id
         * @param int $idLanguage
         */
        public static function delete(int $id, int $idLanguage)
        {
            $ExternalRepair= ExternalRepair::find($id);
            if (is_null($ExternalRepair)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotExist), HttpResultsCodesEnum::InvalidPayload);
            }

            $Intervention = GrIntervention::where('id', $ExternalRepair->id_intervention)->first();
            if (isset($Intervention)) {
                $Intervention->ready = false;
                $Intervention->save();
            }

            if (!is_null($ExternalRepair)) {
                DB::beginTransaction();
                try {
                    ExternalRepair::where('id', $id)->delete();
                    DB::commit();
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
            }
        }
        #endregion DELETE

 //Modifica Riparazione Esterna
    public static function updateRepairExternal(Request $DtoExternalRepair)
    {
        $IdExternalRepair = ExternalRepair::where('id', $DtoExternalRepair->id)->first();
        $Intervention = GrIntervention::where('id', $DtoExternalRepair->id_intervention)->first();

        DB::beginTransaction();
        try {
           
                if (isset($IdExternalRepair->id)) {
                    $IdExternalRepair->id_intervention = $DtoExternalRepair->id_intervention;
                    $IdExternalRepair->id_librone_rip_esterna = $DtoExternalRepair->id_librone_rip_esterna;
                    $IdExternalRepair->date = $DtoExternalRepair->date;
                    $IdExternalRepair->ddt_exit = $DtoExternalRepair->ddt_exit;
                    $IdExternalRepair->id_business_name_supplier = $DtoExternalRepair->id_business_name_supplier;
                    $IdExternalRepair->price = $DtoExternalRepair->price;
                    $IdExternalRepair->date_return_product_from_the_supplier = $DtoExternalRepair->date_return_product_from_the_supplier;
                    $IdExternalRepair->ddt_supplier = $DtoExternalRepair->ddt_supplier;
                    $IdExternalRepair->reference_supplier = $DtoExternalRepair->reference_supplier;
                    $IdExternalRepair->notes_from_repairman = $DtoExternalRepair->notes_from_repairman;
                    $IdExternalRepair->save();

                    if (isset($IdExternalRepair->ddt_supplier)) {
                        $Intervention->states_id = 1;
                        $Intervention->ready = true;
                     }
                    if (isset($IdExternalRepair->date_return_product_from_the_supplier)) {
                        $Intervention->states_id = 1; 
                        $Intervention->ready = true; 
                     }
                      $Intervention->save();
                }
        
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

}

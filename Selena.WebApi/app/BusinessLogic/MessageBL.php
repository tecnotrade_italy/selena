<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoMessage;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\MessageLanguage;
use App\Mail\ContactRequestMail;
use App\Message;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class MessageBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
       // if (is_null($request->description)|| $request->description=='') {
        //    throw new Exception("La descrizione è obbligatoria");
       // }

        if (is_null($request->code)|| $request->code=='') {
            throw new Exception("Il codice è obbligatorio");
        }

        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Message::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }

       /* if ($dbOperationType == DbOperationsTypesEnum::INSERT) {
            if (Message::where('code', $request->code)->count() > 0) {
                throw new Exception("Codice già esistente!");
            }
        }*/
    }

    /**
     * Conver to model
     * 
     * @param Request request
     * 
     * @return Contact
     */
    private static function _convertToModel(Request $request)
    {
        $Message = new Message();

        if (isset($request->id)) {
            $Message->id = $request->id;
        }

        if (isset($request->code)) {
            $Message->code = $request->code;
        }

        if (isset($request->message_type_id)) {
            $Message->message_type_id = $request->message_type_id;
        }

        if (isset($request->content)) {
            $Message->content = $request->content;
        }

        if (isset($request->description)) {
            $Message->description = $request->description;
        }

        return $Message;
    }

    /**
     * Convert to dto
     * 
     * @param Message Message
     * @param int idLanguage
     * 
     * @return DtoMessage
     */
    private static function _convertToDto(Message $Message)
    {
        $dtoMessage = new DtoMessage();
        $dtoMessage->id = $Message->id;
        $dtoMessage->code = $Message->code;
        if (isset($Message->message_type_id)) {
            $dtoMessage->message_type_id = $Message->message_type_id;
            
        }
        $dtoMessage->content = MessageLanguage::where('Message_id', $Message->id)->first()->content;
        $dtoMessage->description = MessageLanguage::where('Message_id', $Message->id)->first()->description;

        return $dtoMessage;
    }

    #endregion PRIVATE

    /**
     * Get select
     *
     * @param String $search
     * @return List
     */
    public static function getSelect(string $search)
    {
        if ($search == "*") {
            return Message::select('id', 'code as description')->get();
        } else {
            return Message::where('code', 'ilike', $search . '%')->select('id', 'code as description')->get();
        }
    }



    #region GET

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoMessage
     */
    public static function getAll($idLanguage)
    {
        $result = collect();
        foreach (MessageLanguage::where('language_id',$idLanguage)->get() as $MessageAll) {

            $dtoMessage = new DtoMessage();
            $dtoMessage->id = Message::where('id', $MessageAll->message_id)->first()->id;
            $dtoMessage->code = Message::where('id', $MessageAll->message_id)->first()->code;
            $dtoMessage->message_type_id = Message::where('id', $MessageAll->message_id)->first()->message_type_id;
            $dtoMessage->content = $MessageAll->content;
            $dtoMessage->description = $MessageAll->description;
            $dtoMessage->idLanguage = $MessageAll->language_id;
            $result->push($dtoMessage); 

    
        }
        return $result;
    }

    public static function getByCode(string $code, int $idLanguage=null)
    {
        if (is_null($code)) {
            LogHelper::error('Code of message not found');
            return '';
        } else {
            $message = Message::where('code', $code)->first();
            if (is_null($message)) {
                LogHelper::error('Content not found');
                return '';
            } else {
                if (is_null($idLanguage)) {
                  $idLanguageFinal = LanguageBL::getDefault()->id;   
                }else{
                  $idLanguageFinal= $idLanguage;
                }
                return MessageLanguageBL::getByCodeAndLanguage($message->id, $idLanguageFinal);
            }
        }
    }

    /**
     * Get by id
     * 
     * @param int id
     * 
     * @return DtoMessage
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Message::find($id));
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $dtoMessages, int $idUser)
    {

        static::_validateData($dtoMessages, $dtoMessages->idLanguage, DbOperationsTypesEnum::INSERT);
        //$MessageModel = static::_convertToModel($request);
        DB::beginTransaction();

        try {
            $Message = new Message();
            $Message->code = $dtoMessages->code;
            $Message->message_type_id = $dtoMessages->message_type_id;
            $Message->created_id = $dtoMessages->$idUser;
            $Message->save();

            $languageMessage = new MessageLanguage();
            $languageMessage->language_id = $dtoMessages->idLanguage;
            $languageMessage->message_id = $Message->id;
            $languageMessage->content = $dtoMessages->content;
            $languageMessage->description = $dtoMessages->description;
            $languageMessage->created_id = $dtoMessages->$idUser;
            $languageMessage->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $Message->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request dtoMessage
     * @param int idLanguage
     */
    public static function update(Request $dtoMessage, int $idLanguage)
    {
        
        static::_validateData($dtoMessage, $idLanguage, DbOperationsTypesEnum::UPDATE);
        
        $MessageOnDb = Message::find($dtoMessage->id);
        $languageMessageOnDb = MessageLanguage::where('message_id', $dtoMessage->id)->first();
        
        DB::beginTransaction();

        try {
            $MessageOnDb->code = $dtoMessage->code;
            $MessageOnDb->message_type_id = $dtoMessage->message_type_id;
            $MessageOnDb->save();

            $languageMessageOnDb->language_id = $dtoMessage->idLanguage;
            $languageMessageOnDb->message_id = $dtoMessage->id;
            $languageMessageOnDb->content = $dtoMessage->content;
            $languageMessageOnDb->description = $dtoMessage->description;
            $languageMessageOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $languageMessage = MessageLanguage::where('message_id', $id)->first();

        if (is_null($languageMessage)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $languageMessage->delete();

        $Message = Message::find($id);

        if (is_null($Message)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $Message->delete();
    }

    #endregion DELETE
}

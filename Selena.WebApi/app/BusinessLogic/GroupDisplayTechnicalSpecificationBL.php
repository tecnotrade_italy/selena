<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoConfiguration;
use App\Enums\DictionariesCodesEnum;
use App\Functionality;
use App\Enums\DbOperationsTypesEnum;
use App\GroupDisplayTechnicalSpecification;
use App\DtoModel\DtoGroupDisplayTechnicalSpecification;
use App\DtoModel\DtoIncludedExcluded;
use App\DtoModel\DtoPageConfiguration;
use App\DtoModel\DtoTabConfiguration;
use App\Enums\HttpResultsCodesEnum;
use App\Enums\InputTypesEnum;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;
use LogHeader;
use Mockery\CountValidator\Exception;
use Symfony\Component\HttpFoundation\Request;

class GroupDisplayTechnicalSpecificationBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request $dtoGroupDisplayTechnicalSpecification
     * @param int $idLanguage
     * @param DbOperationsTypesEnum $dbOperationType
     */
    private static function _validateData(Request $dtoGroupDisplayTechnicalSpecification, int $idLanguage, $dbOperationType)
    {
        if (is_null($dtoGroupDisplayTechnicalSpecification->idFunctionality)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Functionality::find($dtoGroupDisplayTechnicalSpecification->idFunctionality))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (is_null($dtoGroupDisplayTechnicalSpecification->order)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::OrderNotEmpty), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (!is_numeric($dtoGroupDisplayTechnicalSpecification->order)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::OrderOnlyNumeric), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        $defaultLanguageFound = false;

        foreach ($dtoGroupDisplayTechnicalSpecification->groupDisplayTechnicalSpecificationLanguage as $dtoGroupDisplayTechnicalSpecificationLanguage) {
            if ($dtoGroupDisplayTechnicalSpecificationLanguage['idLanguage'] == LanguageBL::getDefault()->id) {
                $defaultLanguageFound = true;
                if (is_null($dtoGroupDisplayTechnicalSpecificationLanguage['description'])) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DefaultLanguageRequired), HttpResultsCodesEnum::InvalidPayload);
                }
            }
        }

        if (!$defaultLanguageFound) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DefaultLanguageRequired), HttpResultsCodesEnum::InvalidPayload);
        }

        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($dtoGroupDisplayTechnicalSpecification->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            } else {
                if (is_null(GroupDisplayTechnicalSpecification::find($dtoGroupDisplayTechnicalSpecification->id))) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupDisplayTechincalSpecificationNotFound), HttpResultsCodesEnum::InvalidPayload);
                }
            }
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get DtoGroupDisplayTechnicalSpecification
     * 
     * @return DtoGroupDisplayTechnicalSpecification
     */
    public static function getDtoTable()
    {
        $result = collect();

        foreach (GroupDisplayTechnicalSpecification::all() as $groupDisplayTechnicalSpecification) {
            $dtoGroupDisplayTechnicalSpecification = new DtoGroupDisplayTechnicalSpecification();
            $dtoGroupDisplayTechnicalSpecification = GroupDisplayTechnicalSpecificationLanguageBL::getDtoTable($groupDisplayTechnicalSpecification->id);
            $dtoGroupDisplayTechnicalSpecification['order'] = $groupDisplayTechnicalSpecification->order;
            $dtoGroupDisplayTechnicalSpecification['id'] = $groupDisplayTechnicalSpecification->id;
            $result->push($dtoGroupDisplayTechnicalSpecification);
        }

        return $result;
    }

    /**
     * Get all technical specification included and excluded
     * 
     * @param int $id
     * 
     * @return DtoIncludedExcluded
     */
    public static function getTechnicalSpecification(int $id, int $idLanguage)
    {
        $dtoIncludedExcluded = new DtoIncludedExcluded();
        $dtoIncludedExcluded->included = GroupDisplayTechnicalSpecificationTechnicalSpecificationBL::getInlucedInGroup($id, $idLanguage);
        $dtoIncludedExcluded->excluded = GroupDisplayTechnicalSpecificationTechnicalSpecificationBL::getExlucedInGroup($id, $idLanguage);
        return $dtoIncludedExcluded;
    }

    /**
     * Get by configuration
     * 
     * @param int idItem
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoConfiguration
     */
    public static function getConfiguration(int $idItem, int $idFunctionality, $idUser, int $idLanguage)
    {
        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        $dtoConfiguration = new DtoConfiguration();
        $lastTabDescription = '';

        foreach (DB::select('SELECT gdts.id AS group_display_technical_specification_id,
                                COALESCE(gdtsl.description, gdtsld.description) AS tab_description,
                                gdts."order" AS group_display_technical_specification_order,
                                ts.id AS technical_specification_id,
                                COALESCE(
                                    (   SELECT description
                                        FROM languages_technicals_specifications
                                        WHERE ts.id = technical_specification_id
                                        AND language_id = ?
                                        AND "version" = (SELECT MAX(version)
                                                            FROM languages_technicals_specifications
                                                            WHERE ts.id = technical_specification_id
                                                            AND language_id = ?)),
                                    (   SELECT description
                                        FROM languages_technicals_specifications
                                        WHERE ts.id = technical_specification_id
                                        AND language_id = ( SELECT id
                                                            FROM languages
                                                            WHERE "default" = true
                                                            LIMIT 1)
                                        AND "version" = (SELECT MAX(version)
                                                            FROM languages_technicals_specifications
                                                            WHERE ts.id = technical_specification_id
                                                            AND language_id = ( SELECT id
                                                                                FROM languages
                                                                                WHERE "default" = true
                                                                                LIMIT 1)))) AS field_description,
                                gdtsts."order" AS groups_display_technical_specification_tech_spec_order
                            FROM groups_display_technical_specification_tech_spec gdtsts
                            INNER JOIN groups_display_technicals_specifications gdts ON gdtsts.group_display_technical_specification_id = gdts.id
                            INNER JOIN technicals_specifications ts ON gdtsts.technical_specification_id = ts.id
                            INNER JOIN groups_technicals_specifications_technicals_specifications gtsts ON ts.id = gtsts.technical_specification_id
                            INNER JOIN items_groups_technicals_specifications igts ON gtsts.group_technical_specification_id = igts.group_technical_specification_id
                            LEFT JOIN groups_display_technicals_specifications_languages gdtsl ON gdts.id = gdtsl.group_display_technical_specification_id
                                                                                                    AND gdtsl.language_id = ?
                            LEFT JOIN groups_display_technicals_specifications_languages gdtsld ON gdts.id = gdtsld.group_display_technical_specification_id
                                                                                                    AND gdtsld.language_id = ( SELECT id
                                                                                                                                FROM languages
                                                                                                                                WHERE "default" = true
                                                                                                                                LIMIT 1)
                            WHERE igts.item_id = ?
                            ORDER BY gdts."order", field_description', [$idLanguage, $idLanguage, $idLanguage, $idItem]) as $row) {
            //ORDER BY gdts."order", gdtsts."order"
            if ($lastTabDescription != $row->tab_description) {
                $dtoTabConfiguration = new DtoTabConfiguration();
                $dtoTabConfiguration->label = $row->tab_description;
                $dtoTabConfiguration->code = $row->group_display_technical_specification_id;
                $dtoTabConfiguration->order = $row->group_display_technical_specification_order;
                $dtoConfiguration->tabsConfigurations->push($dtoTabConfiguration);
                $lastTabDescription = $row->tab_description;
            }
            $dtoPageConfiguration = new DtoPageConfiguration();
            $dtoPageConfiguration->label = $row->field_description;
            $dtoPageConfiguration->field = $row->technical_specification_id;
            $dtoPageConfiguration->inputType = InputTypesEnum::getKey(InputTypesEnum::Input);
            $dtoPageConfiguration->posX = 1;
            $dtoPageConfiguration->posY = $row->groups_display_technical_specification_tech_spec_order;
            $dtoTabConfiguration->pageConfigurations->push($dtoPageConfiguration);
        }

        return $dtoConfiguration;
    }



    /**
     * Get by configuration
     * 
     * @param int idItem
     * @param int version
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoConfiguration
     */
    public static function getVersionConfiguration(int $idItem, int $version, int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        $dtoConfiguration = new DtoConfiguration();
        $lastTabDescription = '';

        foreach (DB::select(
            'SELECT itsprl.group_display_description AS tab_description, its.technical_specification_id,
                    itsprl.field_description, itsprl."field_posx", itsprl."field_posy", itsprl.field_input_type
            FROM items_technicals_specifications its
            LEFT JOIN items_technicals_specifications_revisions itsprl ON its.id = itsprl.item_technical_specification_id
            WHERE its.item_id = ?
            AND its.version = ?
            AND its.language_id = ?
            ORDER BY itsprl.group_display_order, itsprl."field_posx", itsprl."field_posy"',
            [$idItem, $version, $idLanguage]
        ) as $row) {
            if ($lastTabDescription != $row->tab_description) {
                $dtoTabConfiguration = new DtoTabConfiguration();
                $dtoTabConfiguration->label = $row->tab_description;
                $dtoTabConfiguration->code = $row->tab_description;
                $dtoConfiguration->tabsConfigurations->push($dtoTabConfiguration);
                $lastTabDescription = $row->tab_description;
            }
            $dtoPageConfiguration = new DtoPageConfiguration();
            $dtoPageConfiguration->label = $row->field_description;
            $dtoPageConfiguration->field = $row->technical_specification_id;
            $dtoPageConfiguration->inputType = InputTypesEnum::getKey($row->field_input_type);
            $dtoPageConfiguration->isDisabled = true;
            $dtoPageConfiguration->posX = $row->field_posx;
            $dtoPageConfiguration->posY = $row->field_posy;
            $dtoTabConfiguration->pageConfigurations->push($dtoPageConfiguration);
        }

        return $dtoConfiguration;
    }

    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request $dtoGroupDisplayTechnicalSpecification
     * @param int $idUser
     * @param int $idLanguage
     * 
     * @return int id
     */
    public static function insert(Request $dtoGroupDisplayTechnicalSpecification, int $idUser, int $idLanguage)
    {
        if (!FieldUserRoleBL::checkFunctionalityEnabled($dtoGroupDisplayTechnicalSpecification->idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        static::_validateData($dtoGroupDisplayTechnicalSpecification, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $groupDisplayTechnicalSpecification = new GroupDisplayTechnicalSpecification();
            $groupDisplayTechnicalSpecification->order = $dtoGroupDisplayTechnicalSpecification->order;
            $groupDisplayTechnicalSpecification->save();

            foreach ($dtoGroupDisplayTechnicalSpecification->groupDisplayTechnicalSpecificationLanguage as $dtoGroupDisplayTechnicalSpecificationLanguage) {
                GroupDisplayTechnicalSpecificationLanguageBL::insertRequest($groupDisplayTechnicalSpecification->id, $dtoGroupDisplayTechnicalSpecificationLanguage, $idLanguage);
            }

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $groupDisplayTechnicalSpecification->id;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request $dtoGroupDisplayTechnicalSpecification
     * @param int $idUser
     * @param int $idLanguage
     */
    public static function update(Request $dtoGroupDisplayTechnicalSpecification, int $idUser, int $idLanguage)
    {
        if (!FieldUserRoleBL::checkFunctionalityEnabled($dtoGroupDisplayTechnicalSpecification->idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        static::_validateData($dtoGroupDisplayTechnicalSpecification, $idLanguage, DbOperationsTypesEnum::UPDATE);

        DB::beginTransaction();

        try {
            $groupDisplayTechnicalSpecification = GroupDisplayTechnicalSpecification::find($dtoGroupDisplayTechnicalSpecification->id);
            if ($groupDisplayTechnicalSpecification->order != $dtoGroupDisplayTechnicalSpecification->order) {
                $groupDisplayTechnicalSpecification->order = $dtoGroupDisplayTechnicalSpecification->order;
                $groupDisplayTechnicalSpecification->save();
            }

            foreach ($dtoGroupDisplayTechnicalSpecification->groupDisplayTechnicalSpecificationLanguage as $dtoGroupDisplayTechnicalSpecificationLanguage) {
                GroupDisplayTechnicalSpecificationLanguageBL::updateRequest($groupDisplayTechnicalSpecification->id, $dtoGroupDisplayTechnicalSpecificationLanguage, $idLanguage);
            }

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int $id
     */
    public static function delete(int $id, int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }

        if (is_null(GroupDisplayTechnicalSpecification::find($id))) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::GroupDisplayTechincalSpecificationNotFound), HttpResponseException::InvalidPayload);
        }

        DB::beginTransaction();

        try {
            GroupDisplayTechnicalSpecificationTechnicalSpecificationBL::deleteByGroupTechnicalSpecification($id);
            GroupDisplayTechnicalSpecificationLanguageBL::deleteByGroupDisplayTechnicalSpecification($id);
            GroupDisplayTechnicalSpecification::destroy($id);

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion DELETE
}

<?php

namespace App\BusinessLogic;
use App\DtoModel\DtoRedirect;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Redirect;
use App\Mail\ContactRequestMail;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class RedirectBL
{
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Redirect::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }
        }
            if (is_null($request->url_old)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::NameNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (is_null($request->url_new)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::SurnameNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (is_null($request->active)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::EmailNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
    }


    /**
     * Convert to dto
     * 
     * @param Redirect redirect
     * @param int idLanguage
     * 
     * @return DtoRedirect
     */
    private static function _convertToDto(Redirect $redirect)
    {
        $DtoRedirect = new DtoRedirect();
        $DtoRedirect->id = $redirect->id;
        $DtoRedirect->url_old = $redirect->url_old;
        $DtoRedirect->url_new = $redirect->url_new;
        $DtoRedirect->active = $redirect->active;
        return $DtoRedirect;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get
     *
     * @param String $search
     * @return Redirect
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return Redirect::select('id as id', 'url_old')->get();
        } else {
            return Redirect::where('url_old', 'ilike', $search . '%')->select('id as id', 'url_old')->get();
        }
    }

    /**
     * Get
     *
     * @param String $url
     * @param int $idUser
     * @param int $idLanguage
     * @return Redirect
     */
    public static function getHtml(String $url)
    {
        return Redirect::where('url_old', $url)->where('active', true)->get()->first();
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoRedirect
     */
      public static function getAll()
    {
        $result = collect();

        foreach (Redirect::orderBy('url_old')->get() as $Redirect) {
            $DtoRedirect = new DtoRedirect();
            $DtoRedirect->id = $Redirect->id;
            $DtoRedirect->url_old = $Redirect->url_old;
            $DtoRedirect->url_new = $Redirect->url_new;
            $DtoRedirect->active = $Redirect->active;
            $DtoRedirect->UrlOld = $Redirect->url_old;
            $DtoRedirect->UrlNew = $Redirect->url_new;
            $DtoRedirect->Active = $Redirect->active;
            $result->push($DtoRedirect); 
        }
        return $result;
    } 

    /**
     * Get by id
     * 
     * @param int id
     * 
     * @return DtoRedirect
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Redirect::find($id));
    }
    #endregion GET

    #region INSERT

    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        static::_validateData($request, $idLanguage, DbOperationsTypesEnum::INSERT);

        DB::beginTransaction();

        try {
            $redirect = new Redirect();
            $redirect->url_old = $request->url_old;
            $redirect->url_new = $request->url_new;
            $redirect->active = $request->active;
            $redirect->created_id = $request->$idUser;
            $redirect->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $redirect->id;
    }
    #endregion INSERT

    #region UPDATE
    /**
     * Update
     * 
     * @param Request DtoRedirect
     * @param int idLanguage
     */
    public static function update(Request $DtoRedirect, int $idLanguage)
    {
       static::_validateData($DtoRedirect, $idLanguage, DbOperationsTypesEnum::UPDATE);
        
        $RedirectOnDb = Redirect::find($DtoRedirect->id);
       
        DB::beginTransaction();
        try {

            $RedirectOnDb->url_old = $DtoRedirect->url_old;
            $RedirectOnDb->url_new = $DtoRedirect->url_new;
            $RedirectOnDb->active = $DtoRedirect->active;
            $RedirectOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE

        public static function insertRedirectCheck(Request $request)
        {
            $redirectOnDb = Redirect::find($request->IdRedirect);
    
            if (isset($redirectOnDb)) {
                $redirectOnDb->active = !$redirectOnDb->active;
                $redirectOnDb->save();
            } else {
                 $redirectOnDb->active = true;
                 $redirectOnDb->save();
                }
        }



    public static function updateTable(Request $DtoRedirect, int $idLanguage)
    {

       static::_validateData($DtoRedirect, $idLanguage, DbOperationsTypesEnum::UPDATE);
        
        $RedirectOnDb = Redirect::find($DtoRedirect->id);
       
        DB::beginTransaction();
        try {

            $RedirectOnDb->url_old = $DtoRedirect->UrlOld;
            $RedirectOnDb->url_new = $DtoRedirect->UrlNew;
            $RedirectOnDb->active =  $DtoRedirect->Active;
            $RedirectOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $Redirect = Redirect::find($id);
        if (is_null($Redirect)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UrlNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        $Redirect->delete();
    }

    #endregion DELETE
}

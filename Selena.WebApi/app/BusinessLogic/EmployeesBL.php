<?php

namespace App\BusinessLogic;

use Illuminate\Support\Facades\DB;
use App\DtoModel\DtoIncludedExcluded;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\DtoModel\DtoEmployees;
use App\Enums\DbOperationsTypesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\LanguageNation;
use App\LanguageProvince;
use App\User;
use App\Employees;
use App\Message;
use Exception;
use Illuminate\Http\Request;
//use Mockery\Exception;

class EmployeesBL
{

     #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request DtoPlaces
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $DtoEmployees, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($DtoEmployees->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

            if (Employees::find($DtoEmployees->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }
        }
    }

    /**
     * Convert to dto
     * 
     * @param Employees
     * 
     * @return DtoEmployees
     */
    private static function _convertToDto($Employees)
    {
        $DtoEmployees = new DtoEmployees();

        if (isset($Employees->id)) {
            $DtoEmployees->id = $Employees->id;
        }
         if (isset($Employees->name)) {
            $DtoEmployees->name = $Employees->name;
        }
         if (isset($Employees->description)) {
            $DtoEmployees->description = $Employees->description;
        }
        if (isset($Employees->email)) {
            $DtoEmployees->email = $Employees->email;
        }
        if (isset($Employees->telephone_number)) {
            $DtoEmployees->telephone_number = $Employees->telephone_number;
        }     
        return $DtoEmployees;   
    }

    #endregion PRIVATE

    #region GET
    /**
     * Get by id
     * 
     * @param int id
     * @return DtoPlaces
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(Employees::find($id));
    }

    /**
     * Get select
     *
     * @param String $search
     * @param int $idUser
     * 
     */
    public static function getSelect(string $search)
    {
                if ($search == "*") {
                    return Employees::select('id', 'name as description')->get();
                } else {
                    return Employees::where('name', 'ilike' , $search . '%')->select('id', 'name as description')->get();
          } 
    }


    /**
     * Get all
     * 
     * @return DtoPlaces
     */
    public static function getAll()
    {
        $result = collect();

        foreach (Employees::orderBy('id','DESC')->get() as $Employees) {    

                $DtoEmployees = new DtoEmployees();
            
                if (isset($Employees->id)) {
                    $DtoEmployees->id = $Employees->id;
                }
                if (isset($Employees->name)) {
                    $DtoEmployees->name = $Employees->name;
                }
                if (isset($Employees->description)) {
                    $DtoEmployees->description = $Employees->description;
                }
                if (isset($Employees->email)) {
                    $DtoEmployees->email = $Employees->email;
                }
                if (isset($Employees->telephone_number)) {
                    $DtoEmployees->telephone_number = $Employees->telephone_number;
                }
                    $result->push($DtoEmployees);
                }
                return $result;

    }
    #endregion GET

    #region INSERT
    /**
     * Insert
     * 
     * @param Request DtoEmployees
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoEmployees, int $idUser)
    {

       DB::beginTransaction();
        try {
            $Employees = new Employees();
            
        if (isset($DtoEmployees->name)) {
            $Employees->name = $DtoEmployees->name;
        } 
         if (isset($DtoEmployees->description)) {
            $Employees->description = $DtoEmployees->description;
        } 
         if (isset($DtoEmployees->email)) {
            $Employees->email = $DtoEmployees->email;
        } 
        if (isset($DtoEmployees->telephone_number)) {
            $Employees->telephone_number = $DtoEmployees->telephone_number;
        } 
        $Employees->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $Employees->id;
    }
    #endregion INSERT

    /**
     * Update
     * 
     * @param Request [DtoEmployees]
     * @param int idLanguage
     */
    public static function update(Request $DtoEmployees, int $idUser)
    {
       
        $EmployeesOnDb = Employees::find($DtoEmployees->id);
      
        DB::beginTransaction();

        try {
            if (isset($DtoEmployees->id)) {
                $EmployeesOnDb->id = $DtoEmployees->id;
            } 
            if (isset($DtoEmployees->name)) {
                $EmployeesOnDb->name = $DtoEmployees->name;
            } 
             if (isset($DtoEmployees->description)) {
                $EmployeesOnDb->description = $DtoEmployees->description;
            } 
             if (isset($DtoEmployees->email)) {
                $EmployeesOnDb->email = $DtoEmployees->email;
            } 
             if (isset($DtoEmployees->telephone_number)) {
                $EmployeesOnDb->telephone_number = $DtoEmployees->telephone_number;
            } 
            
            $EmployeesOnDb->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return  $DtoEmployees->id;
    }
    #endregion UPDATE


        /**
             * Clone
             * 
             * @param int id
             * @param int idFunctionality
             * @param int idLanguage
             * @param int idUser
             * 
             * @return int
             */
            public static function clone(int $id, int $idFunctionality, int $idUser, int $idLanguage)
            {
            
                $Employees = Employees::find($id);            
                DB::beginTransaction();

                try {

                    $newEmployees = new Employees();
                    $newEmployees->name = $Employees->name;
                    $newEmployees->description = $Employees->description . ' - Copy';
                    $newEmployees->email = $Employees->email;
                    $newEmployees->telephone_number = $Employees->telephone_number;
                    $newEmployees->save();
   
                    DB::commit();
                    return $newEmployees->id;
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }
            }
        #endregion CLONE

            #region DELETE
                    /**
                     * Delete
                     * 
                     * @param int id
                     * @param int idLanguage
                     */
                    public static function delete(int $id, int $idLanguage)
                    {
                        $Employees = Employees::find($id);                   

                        if (is_null($Employees)) {
                            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::PeopleRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
                        }
                        DB::beginTransaction();
                        try {
                            $Employees->delete();
                            
                            DB::commit();
                        } catch (Exception $ex) {
                            DB::rollback();
                            throw $ex;
                        }
                    }
                
                }
        #endregion DELETE
<?php

namespace App\BusinessLogic;

use App\DtoModel\DtoCommunityFavoriteImage;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\CommunityFavoriteImage;
use App\CommunityAction;

use App\BusinessLogic\CommunityActionBL;

use Exception;
use Illuminate\Http\Request;
use App\Helpers\LogHelper;
use Illuminate\Support\Facades\DB;

class CommunityFavoriteImageBL
{
    #region PRIVATE

    #region PRIVATE

    /**
     * Validate data
     *
     * @param Request dtoCommunityFavoriteImage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationsTypesEnum)
    {

       

    }

    /**
     * Convert to dto
     * 
     * @param CommunityFavoriteImage CommunityFavoriteImage
     * @return DtoCommunityFavoriteImage
     */
    private static function _convertToDto($CommunityFavoriteImage)
    {
        $dtoCommunityFavoriteImage = new DtoCommunityFavoriteImage();

        if (isset($CommunityFavoriteImage->id)) {
            $dtoCommunityFavoriteImage->id = $CommunityFavoriteImage->id;
        }

        if (isset($CommunityFavoriteImage->user_id)) {
            $dtoCommunityFavoriteImage->user_id = $CommunityFavoriteImage->user_id;
        }

        if (isset($CommunityFavoriteImage->image_id)) {
            $dtoCommunityFavoriteImage->image_id = $CommunityFavoriteImage->image_id;
        }

        if (isset($CommunityFavoriteImage->favorite_gallery_id)) {
            $dtoCommunityFavoriteImage->favorite_gallery_id = $CommunityFavoriteImage->favorite_gallery_id;
        }

        return $dtoCommunityFavoriteImage;
    }


    /**
     * Convert to VatType
     * 
     * @param dtoCommunityFavoriteImage dtoCommunityFavoriteImage
     * 
     * @return CommunityFavoriteImage
     */
    private static function _convertToModel($dtoCommunityFavoriteImage)
    {
        $CommunityFavoriteImage = new CommunityFavoriteImage();

        if (isset($dtoCommunityFavoriteImage->id)) {
            $CommunityFavoriteImage->id = $dtoCommunityFavoriteImage->id;
        }

        if (isset($dtoCommunityFavoriteImage->user_id)) {
            $CommunityFavoriteImage->user_id = $dtoCommunityFavoriteImage->user_id;
        }

        if (isset($dtoCommunityFavoriteImage->image_id)) {
            $CommunityFavoriteImage->image_id = $dtoCommunityFavoriteImage->image_id;
        }

        if (isset($dtoCommunityFavoriteImage->favorite_gallery_id)) {
            $CommunityFavoriteImage->favorite_gallery_id = $dtoCommunityFavoriteImage->favorite_gallery_id;
        }

        return $CommunityFavoriteImage;
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get by id
     * 
     * @param int id
     * @return DtoCommunityction
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(CommunityFavoriteImage::find($id));
    }

    /*
     * Get all
     * 
     * @return DtoCommunityFavoriteImage
     */
    public static function getAll()
    {

        $result = collect(); 

        foreach (CommunityFavoriteImage::orderBy('id','DESC')->get() as $communityFavoriteImage) {

            $DtoCommunityFavoriteImage = new DtoCommunityFavoriteImage();

            $DtoCommunityFavoriteImage->id = $communityFavoriteImage->id;
            $DtoCommunityFavoriteImage->favorite_gallery_id = $communityFavoriteImage->favorite_gallery_id;
            $DtoCommunityFavoriteImage->user_id = $communityFavoriteImage->user_id;
            $DtoCommunityFavoriteImage->image_id = $communityFavoriteImage->image_id;
            
            $result->push($DtoCommunityFavoriteImage); 
        }
        return $result;

    }

    #endregion GET


    #region INSERT

    /**
     * Insert
     * 
     * @param Request DtoCommunityFavoriteImage
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $DtoCommunityFavoriteImage)
    {
        
        static::_validateData($DtoCommunityFavoriteImage, $idLanguage, DbOperationsTypesEnum::INSERT);
        $CommunityFavoriteImage = static::_convertToModel($DtoCommunityFavoriteImage);

        $CommunityFavoriteImage->save();

        return $CommunityFavoriteImage->id;
    }

    /**
     * update or insert favorites images for community 
     * 
     * @param Request request
     * @param int idLanguage
     * 
     * @return int
     */
    public static function updateOrInsert(Request $request){
       
        $return = 0;
        DB::beginTransaction();

        try {
            if(isset($request->idImage) && isset($request->idAction) && isset($request->userId)){
                $communityActionOnDb = CommunityAction::where('image_id', $request->idImage)->where('user_id', $request->userId)->where('action_type_id', $request->idAction)->get()->first();

                if(!isset($communityActionOnDb)){
                    //add row in community favorite image
                    $communityFavoriteImage = new CommunityFavoriteImage();
                    $communityFavoriteImage->user_id = $request->userId;
                    $communityFavoriteImage->image_id = $request->idImage;
                    $communityFavoriteImage->created_id = $request->userId;
                    $communityFavoriteImage->save();

                    //add row in community action by image_id and user_id and action_type_id 
                    $idCommunityAction = CommunityActionBL::addAction($request->userId, $request->idImage, $request->idAction);

                    //call function to recalculate rating of images by add the value of community_type_action
                    if(isset($idCommunityAction)){
                        $actualRate = CommunityActionBL::addOrRemoveRateByActionId($idCommunityAction, true);
                        
                        // chiamata funzione per invio email se l'utente ha abilitato le notifiche nelle impostazioni
                        CommunityActionBL::sendNotificationByActionId($idCommunityAction, 0);
                        
                        $return = $actualRate;
                    }
                }else{
                    //call function to recalculate rating of images by remove the value of community_type_action
                    $actualRate = CommunityActionBL::addOrRemoveRateByActionId($communityActionOnDb->id, false);

                    //delete row in community action by image_id and user_id and action_type_id
                    CommunityActionBL::delete($communityActionOnDb->id);
                    
                    //delete row in community favorite image
                    $CommunityFavoriteImage = CommunityFavoriteImage::where('image_id', $request->idImage)->where('user_id', $request->userId)->get()->first();
                    if(isset($CommunityFavoriteImage)){
                        $CommunityFavoriteImage->delete();
                    }
                    
                    $return = $actualRate;
                }
            }
        DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }

        return $return;
    }

    #endregion INSERT

    #region UPDATE

    /**
     * Update
     * 
     * @param Request DtoCommunityFavoriteImage
     * @param int idLanguage
     */
    public static function update(Request $DtoCommunityFavoriteImage)
    {
        static::_validateData($DtoCommunityFavoriteImage, $idLanguage, DbOperationsTypesEnum::UPDATE);
        $CommunityFavoriteImage = CommunityFavoriteImage::find($DtoCommunityFavoriteImage->id);
        DBHelper::updateAndSave(static::_convertToModel($DtoCommunityFavoriteImage), $CommunityFavoriteImage);
    }

    #endregion UPDATE

    #region DELETE

    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id)
    {
        $CommunityFavoriteImage = CommunityFavoriteImage::find($id);

        if (is_null($CommunityFavoriteImage)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CustomerRequestNotFound), HttpResultsCodesEnum::InvalidPayload);
        }

        $CommunityFavoriteImage->delete();
    }

    #endregion DELETE
}

<?php

namespace App\BusinessLogic;

use App\Contact;
use App\DtoModel\DtoLanguagePageCategory;
use App\Enums\DbOperationsTypesEnum;
use App\Enums\DictionariesCodesEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Helpers\DBHelper;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\LanguagePageCategory;
use App\PageCategory;
use App\Language;
use App\LanguagePage;
use App\PageCategoryFather;
use App\PagePageCategory;

use App\LinkPageCategory;
use App\Mail\ContactRequestMail;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\Facades\Image;

class CategoriesPagesBL
{
    public static $dirImageAttachment = '../storage/app/public/images';
    #region PRIVATE

    /**
     * Validate data
     * 
     * @param Request request
     * @param int idLanguage
     * @param DbOperationsTypesEnum dbOperationType
     */
    private static function _validateData(Request $request, int $idLanguage, $dbOperationType)
    {
        if ($dbOperationType == DbOperationsTypesEnum::UPDATE) {
            if (is_null($request->id)) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::IdNotValued), HttpResultsCodesEnum::InvalidPayload);
            }

           /* if (LanguagePageCategory::find($request->id)->count() == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::ContactNotFound), HttpResultsCodesEnum::InvalidPayload);
            }*/
        }
    }


    /**
     * Convert to dto
     */
    private static function _convertToDto(LanguagePageCategory $LanguagePageCategory)
    {
        $DtoLanguagePageCategory = new DtoLanguagePageCategory();

        $DtoLanguagePageCategory->description = $LanguagePageCategory->description;
        $DtoLanguagePageCategory->language_id = $LanguagePageCategory->language_id;
        $DtoLanguagePageCategory->page_category_id = $LanguagePageCategory->page_category_id;
        $DtoLanguagePageCategory->seo_title = $LanguagePageCategory->seo_title;
        $DtoLanguagePageCategory->seo_keyword = $LanguagePageCategory->seo_keyword;
        $DtoLanguagePageCategory->seo_description = $LanguagePageCategory->seo_description;
        $DtoLanguagePageCategory->share_title = $LanguagePageCategory->share_title;
        $DtoLanguagePageCategory->share_description = $LanguagePageCategory->share_description;
        $DtoLanguagePageCategory->url_cover_image = $LanguagePageCategory->url_cover_image;
        $DtoLanguagePageCategory->order = $LanguagePageCategory->order;
        return $DtoLanguagePageCategory;
    }
    #endregion PRIVATE

    /**
     * Get
     *
     * @param String $search
     * @return LanguageNation
     */
    public static function getSelect(String $search)
    {
        if ($search == "*") {
            return LanguagePageCategory::select('page_category_id as id', 'description')->get();
        } else {
            return LanguagePageCategory::where('description', 'ilike', $search . '%')->select('page_category_id as id', 'description')->get();
        }
    }

    /**
     * Get all
     * 
     * @param int idLanguage
     * 
     * @return DtoNation
     */
    public static function getAll($idLanguage)
    {
        $result = collect();

        foreach (LanguagePageCategory::where('language_id',$idLanguage)->get() as $LanguagePageCategory) {

            $DtoLanguagePageCategory = new DtoLanguagePageCategory();
            
            $DtoLanguagePageCategory->description = $LanguagePageCategory->description;
            $DtoLanguagePageCategory->language_id = $LanguagePageCategory->language_id;

            $PagePageCategoryFather = PageCategoryFather::where('page_category_id', $LanguagePageCategory->page_category_id)->first();
            if (isset ($PagePageCategoryFather->page_category_father_id)){
                $DtoLanguagePageCategory->page_category_id = $PagePageCategoryFather->page_category_father_id;
                //LanguagePageCategory::where('page_category_id', $PagePageCategoryFather->page_category_father_id)->first()->description;
            }else{
                $DtoLanguagePageCategory->page_category_id = '';
            }

            $PageCategory = PageCategory::where('id', $LanguagePageCategory->page_category_id)->first();

            if (isset($PageCategory)){

                $DtoLanguagePageCategory->id = $PageCategory->id;
                $DtoLanguagePageCategory->online = $PageCategory->online;
            }else{
                $DtoLanguagePageCategory->id = '';
                $DtoLanguagePageCategory->online = false;
            }
           // $DtoLanguagePageCategory->page_category_id = $LanguagePageCategory->page_category_id;
            $DtoLanguagePageCategory->seo_title = $LanguagePageCategory->seo_title;
            $DtoLanguagePageCategory->seo_keyword = $LanguagePageCategory->seo_keyword;
            $DtoLanguagePageCategory->seo_description = $LanguagePageCategory->seo_description;
            $DtoLanguagePageCategory->share_title = $LanguagePageCategory->share_title;
            $DtoLanguagePageCategory->share_description = $LanguagePageCategory->share_description;
            $DtoLanguagePageCategory->url_cover_image = $LanguagePageCategory->url_cover_image;
            $DtoLanguagePageCategory->base64CoverImage = '';
            
            if (isset($PagePageCategoryFather)){
                $DtoLanguagePageCategory->order = $PagePageCategoryFather->order;
            }else{
                $DtoLanguagePageCategory->order = '';
            }

            $DtoLanguagePageCategory->Description = $LanguagePageCategory->description;

            $language = Language::where('id', '=', $LanguagePageCategory->language_id)->first();
            if(isset($language)){
                $DtoLanguagePageCategory->LanguageId = $language->description;
            }else{
                $DtoLanguagePageCategory->LanguageId = '';
            }

             if (isset($PagePageCategoryFather->page_category_father_id)){
                $DtoLanguagePageCategory->PageCategory_id =  LanguagePageCategory::where('page_category_id', $PagePageCategoryFather->page_category_father_id)->first()->description;
            }else{
                $DtoLanguagePageCategory->PageCategory_id = '';
            } 

            $LinkPageCategory = LinkPageCategory::where('page_category_id', $LanguagePageCategory->page_category_id)->first();
            if (isset ($LinkPageCategory)){
                $DtoLanguagePageCategory->LinkTable = $LinkPageCategory->url;
                $DtoLanguagePageCategory->url = $LinkPageCategory->url;
            }else{
                $DtoLanguagePageCategory->LinkTable = '';
                $DtoLanguagePageCategory->url = '';
            }

            $DtoLanguagePageCategory->SeoTitle = $LanguagePageCategory->seo_title;
            $DtoLanguagePageCategory->SeoKeyword = $LanguagePageCategory->seo_keyword;
            $DtoLanguagePageCategory->SeoDescription = $LanguagePageCategory->seo_description;
            $DtoLanguagePageCategory->ShareTitle = $LanguagePageCategory->share_title;
            $DtoLanguagePageCategory->ShareDescription = $LanguagePageCategory->share_description;
            $DtoLanguagePageCategory->UrlcoverImage = $LanguagePageCategory->url_cover_image;

            if (isset ($PagePageCategoryFather)){
                $DtoLanguagePageCategory->Order = $PagePageCategoryFather->order;
            }else{
                $DtoLanguagePageCategory->Order = '';
            }

           
            $result->push($DtoLanguagePageCategory); 

        }
        return $result;
    }

    /**
     * Get by id
     * 
     * @param int id
     * 
     * @return DtoNation
     */
    public static function getById(int $id)
    {
        return static::_convertToDto(LanguagePageCategory::find($id));
    }

    private static function getUrlImage()
    {
        if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
            $protocol = "https://";
        } else {
            $protocol = "http://";
        }
        return $protocol . $_SERVER["HTTP_HOST"] . '/storage/images/';
    }
    #endregion GET

    
    /**
     * Insert
     * 
     * @param Request request
     * @param int idUser
     * @param int idLanguage
     * 
     * @return int
     */
    public static function insert(Request $request, int $idUser, int $idLanguage)
    {
        DB::beginTransaction();
        $languagePageSetting = LanguagePage::where('url', $request->url)->join('pages', 'pages.id', '=', 'languages_pages.page_id')->where('pages.page_type', '<>', 'Link')->first();
        if (isset($languagePageSetting)){
            if ($request->url == $languagePageSetting->url){
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UrlAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        $LinkPageCategorys = LinkPageCategory::where('url', $request->url)->first();
        if (isset($LinkPageCategorys)){
            if ($request->url == $LinkPageCategorys->url){
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UrlAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }
 
        try {
            $PageCategory = new PageCategory();
            $PageCategory->created_id = $request->$idUser;
            $PageCategory->online = $request->online;
            $PageCategory->save();

            $LanguagePageCategory = new LanguagePageCategory();
            $LanguagePageCategory->description = $request->description;
            $LanguagePageCategory->language_id = $request->language_id;
            $LanguagePageCategory->page_category_id = $PageCategory->id;
            $LanguagePageCategory->seo_title = $request->seo_title;
            $LanguagePageCategory->seo_keyword = $request->seo_keyword;
            $LanguagePageCategory->seo_description = $request->seo_description;
            $LanguagePageCategory->share_title = $request->share_title;
            $LanguagePageCategory->share_description = $request->share_description;
            $LanguagePageCategory->url_cover_image = $request->url_cover_image;

            if($request->base64CoverImage != ''){
                $date = date("Y-m-d");
                $dateElements = explode('-', $date);
                $year = $dateElements[0];
                $month = $dateElements[1];

                Image::make($request->base64CoverImage)->save(static::$dirImageAttachment . '/' . $year . '/' . $month . '/'. $request->url_cover_image);
                
                $LanguagePageCategory->url_cover_image = '/storage/images/' . $year . '/' . $month . '/' . $request->url_cover_image;
            }else{
                $LanguagePageCategory->url_cover_image = $request->url_cover_image;
            }

            $LanguagePageCategory->created_id = $request->$idUser;
            $LanguagePageCategory->save();

            $LanguagePageCategoryFather = new PageCategoryFather();
            $LanguagePageCategoryFather->page_category_id = $PageCategory->id;
            $LanguagePageCategoryFather->page_category_father_id = $request->page_category_id;
            $LanguagePageCategoryFather->order = $request->order;
            $LanguagePageCategoryFather->created_id = $request->$idUser;
            $LanguagePageCategoryFather->save();

            
            $LinkPageCategory = new LinkPageCategory();
            $LinkPageCategory->page_category_id = $PageCategory->id;
            $LinkPageCategory->url = $request->url;
            $LinkPageCategory->created_id = $request->$idUser;
            $LinkPageCategory->save();
            
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
        return $LanguagePageCategoryFather->id;
    }

    #endregion INSERT


    /**
     * Update
     * 
     * @param Request dtoNation
     * @param int idLanguage
     */
    public static function update(Request $DtoLanguagePageCategory, int $idLanguage)
    {

        static::_validateData($DtoLanguagePageCategory, $idLanguage, DbOperationsTypesEnum::UPDATE);

        $LanguagePageCategory = LanguagePageCategory::where('page_category_id', $DtoLanguagePageCategory->id)->first();
        DB::beginTransaction();
        try {

            $PageCategory = PageCategory::where('id', $LanguagePageCategory->page_category_id)->first();
            $PageCategory->online = $DtoLanguagePageCategory->online;
            $PageCategory->save();

            $LanguagePageCategory->description = $DtoLanguagePageCategory->description;
            $LanguagePageCategory->language_id = $DtoLanguagePageCategory->language_id;
            $LanguagePageCategory->seo_title = $DtoLanguagePageCategory->seo_title;
            $LanguagePageCategory->seo_keyword = $DtoLanguagePageCategory->seo_keyword;
            $LanguagePageCategory->seo_description = $DtoLanguagePageCategory->seo_description;
            $LanguagePageCategory->share_title = $DtoLanguagePageCategory->share_title;
            $LanguagePageCategory->share_description = $DtoLanguagePageCategory->share_description;

            if($DtoLanguagePageCategory->base64CoverImage != ''){
                $date = date("Y-m-d");
                $dateElements = explode('-', $date);
                $year = $dateElements[0];
                $month = $dateElements[1];
                Image::make($DtoLanguagePageCategory->base64CoverImage)->save(static::$dirImageAttachment . '/' . $year . '/' . $month . '/'. $DtoLanguagePageCategory->url_cover_image);
                $LanguagePageCategory->url_cover_image = '/storage/images/' . $year . '/' . $month . '/' . $DtoLanguagePageCategory->url_cover_image;
            }else{
                $LanguagePageCategory->url_cover_image = $DtoLanguagePageCategory->url_cover_image;
            }

            $LanguagePageCategory->save();

            $LanguagePageCategoryFather = PageCategoryFather::where('page_category_id', $LanguagePageCategory->page_category_id)->first();
            if (isset($LanguagePageCategoryFather)){
                $LanguagePageCategoryFather->order = $DtoLanguagePageCategory->order;
                $LanguagePageCategoryFather->page_category_father_id = $DtoLanguagePageCategory->page_category_id;
                $LanguagePageCategoryFather->save();
            }

            $languagePageSetting = LanguagePage::where('url', $DtoLanguagePageCategory->url)->join('pages', 'pages.id', '=', 'languages_pages.page_id')->where('pages.page_type', '<>', 'Link')->first();
            if (isset($languagePageSetting)){
                if ($DtoLanguagePageCategory->url == $languagePageSetting->url){
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UrlAlreadyExist), HttpResultsCodesEnum::InvalidPayload);
                }
            }
            
            $LinkPageCategory = LinkPageCategory::where('page_category_id', $LanguagePageCategory->page_category_id)->first();
            if (isset($LinkPageCategory)){
                $LinkPageCategory->url = $DtoLanguagePageCategory->url;
            }
                $LinkPageCategory->save();
          
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }


    /**
     * Delete
     * 
     * @param int id
     * @param int idLanguage
     */
    public static function delete(int $id, int $idLanguage)
    {
        $pageCategory = PageCategory::find($id);
        
        if (is_null($pageCategory)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::CategoriesNotFound), HttpResultsCodesEnum::InvalidPayload);
        }
        
        DB::beginTransaction();

        try {
            
            $linkTypology = LinkPageCategory::where('page_category_id', $id)->get()->first();
            if(isset($linkTypology)){
                $linkTypologyDel = LinkPageCategory::where('page_category_id', $id);
                $linkTypologyDel->delete();
            }

            $pageCategoryFather = PageCategoryFather::where('page_category_id', $id)->get()->first();
            if(isset($pageCategoryFather)){
                $pageCategoryFatherDel = PageCategoryFather::where('page_category_id', $id);
                $pageCategoryFatherDel->delete();
            }

            $languagePageCategory = LanguagePageCategory::where('page_category_id', $id)->get()->first();
            if(isset($languagePageCategory)){
                $languagePageCategoryDel = LanguagePageCategory::where('page_category_id', $id);
                $languagePageCategoryDel->delete();
            }

            $pagePageCategory = PagePageCategory::where('page_category_id', $id)->get()->first();
            if(isset($pagePageCategory)){
                $pagePageCategoryDel = PagePageCategory::where('page_category_id', $id);
                $pagePageCategoryDel->delete();
            }

            $pageCategory->delete();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw $ex;
        } 
    }

    #endregion DELETE
}

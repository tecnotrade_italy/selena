<?php

namespace App\BusinessLogic;

use App\DocumentHead;
use App\DocumentHeadSixten;
use App\DocumentHeadMdMicrodetectors;
use App\DtoModel\DtoDocumentHead;
use App\Enums\DictionariesCodesEnum;
use App\Enums\DocumentTypeEnum;
use App\Enums\HttpResultsCodesEnum;
use App\Functionality;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use BenSampo\Enum\Enum;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DocumentHeadBL
{
    #region PRIVATE

    /**
     * Check function enabled
     * 
     * @param int idFunctionlity
     * @param int idUser
     * @param int idLanguage
     */
    private static function _checkFunctionEnabled(int $idFunctionality, int $idUser, int $idLanguage)
    {
        if (is_null($idFunctionality)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotValued), HttpResultsCodesEnum::InvalidPayload);
        } else {
            if (is_null(Functionality::find($idFunctionality))) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::FunctionNotExist), HttpResultsCodesEnum::InvalidPayload);
            }
        }

        if (!FieldUserRoleBL::checkFunctionalityEnabled($idFunctionality, $idUser)) {
            throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::UserNotEnabled), HttpResultsCodesEnum::InvalidPayload);
        }
    }

    #endregion PRIVATE

    #region GET

    /**
     * Get sixten confirmed
     * 
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoDocumentHeader
     */
    public static function getSixtenOrderConfirmed(int $idFunctionality, int $idUser, int $idLanguage)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
        $result = collect();

        foreach (DocumentHead::where('document_type_id', DocumentTypeBL::getIdByCode(DocumentTypeEnum::Order))->whereNull('shipping_date')->OrderBy('code', 'desc')->get() as $documentHead) {
            $dtoDocumentHead = new DtoDocumentHead();
            $dtoDocumentHead->id = $documentHead->id;
            $dtoDocumentHead->orderNumber = $documentHead->code;
            $dtoDocumentHead->customerDescription = CustomerBL::getBusinessName($documentHead->customer_id);
            if (!is_null($documentHead->expected_shipping_date)) {
                $dtoDocumentHead->shipmentMonth = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $documentHead->expected_shipping_date)->format('n');
            }
            $result->push($dtoDocumentHead);
        }

        return $result;
    }


    /**
     * Get MdMicrodetectors shipping list
     * 
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return DtoDocumentHeader
     */
    public static function getMdMicrodetectorsShippingList(int $idFunctionality, int $idUser, int $idLanguage)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);
        $result = collect();

        foreach (DocumentHeadMdMicrodetectors::get() as $documentHead) {
            $dtoDocumentHead = new DtoDocumentHead();
            $dtoDocumentHead->id = $documentHead->id;
            $dtoDocumentHead->orderNumber = $documentHead->document_head_id;
            $dtoDocumentHead->customerDescription = $documentHead->customer_description;
            $dtoDocumentHead->shippingDate = $documentHead->shipping_date;
            
            $result->push($dtoDocumentHead);
        }

        return $result;
    }

    /**
     * Get id first BeforeDDT by Ero
     * 
     * @param string idErp
     * 
     * @return id
     */
    public static function getIdBeforeDDTByErpRow(string $idErp)
    {
        return DB::select('SELECT id
                            FROM documents_heads
                            WHERE (customer_id, EXTRACT(YEAR FROM expected_shipping_date), EXTRACT(MONTH FROM expected_shipping_date)) in (
                                SELECT documents_heads.customer_id, EXTRACT(YEAR FROM expected_shipping_date) as year,
                                    EXTRACT(MONTH FROM expected_shipping_date) as month
                                FROM documents_heads
                                INNER JOIN documents_rows ON documents_heads.id = documents_rows.document_head_id
                                WHERE documents_rows.erp_id = :idErp
                            )
                            AND document_type_id = (SELECT id FROM documents_types WHERE code = :codeDocumentType)
                            ', ['idErp' => $idErp, 'codeDocumentType' => DocumentTypeEnum::BeforeDDT]);
    }

    /**
     * Get by id
     * 
     * @param int id
     * 
     * @return DocumentHead
     */
    public static function getById(int $id)
    {
        return DocumentHead::find($id);
    }

    /**
     * Get month of Before DDT Sixten
     * 
     * @param int idCustomer
     * @param int idFunctionality
     * @param int idUser
     * @param int idLanguage
     * 
     * @return Month
     */
    public static function getSelectMonthBeforeDDTSixten(int $idCustomer, int $idFunctionality, int $idUser, $idLanguage)
    {
        static::_checkFunctionEnabled($idFunctionality, $idUser, $idLanguage);

        return collect(DB::select(
            'SELECT EXTRACT(MONTH FROM expected_shipping_date) as id, 
                    CASE 
                        WHEN EXTRACT(MONTH FROM expected_shipping_date) = 1 THEN (SELECT description FROM dictionaries WHERE code = \'January\' AND language_id = :idLanguage)
                        WHEN EXTRACT(MONTH FROM expected_shipping_date) = 2 THEN (SELECT description FROM dictionaries WHERE code = \'February\' AND language_id = :idLanguage)
                        WHEN EXTRACT(MONTH FROM expected_shipping_date) = 3 THEN (SELECT description FROM dictionaries WHERE code = \'March\' AND language_id = :idLanguage)
                        WHEN EXTRACT(MONTH FROM expected_shipping_date) = 4 THEN (SELECT description FROM dictionaries WHERE code = \'April\' AND language_id = :idLanguage)
                        WHEN EXTRACT(MONTH FROM expected_shipping_date) = 5 THEN (SELECT description FROM dictionaries WHERE code = \'May\' AND language_id = :idLanguage)
                        WHEN EXTRACT(MONTH FROM expected_shipping_date) = 6 THEN (SELECT description FROM dictionaries WHERE code = \'June\' AND language_id = :idLanguage)
                        WHEN EXTRACT(MONTH FROM expected_shipping_date) = 7 THEN (SELECT description FROM dictionaries WHERE code = \'July\' AND language_id = :idLanguage)
                        WHEN EXTRACT(MONTH FROM expected_shipping_date) = 8 THEN (SELECT description FROM dictionaries WHERE code = \'August\' AND language_id = :idLanguage)
                        WHEN EXTRACT(MONTH FROM expected_shipping_date) = 9 THEN (SELECT description FROM dictionaries WHERE code = \'September\' AND language_id = :idLanguage)
                        WHEN EXTRACT(MONTH FROM expected_shipping_date) = 10 THEN (SELECT description FROM dictionaries WHERE code = \'October\' AND language_id = :idLanguage)
                        WHEN EXTRACT(MONTH FROM expected_shipping_date) = 11 THEN (SELECT description FROM dictionaries WHERE code = \'November\' AND language_id = :idLanguage)
                        WHEN EXTRACT(MONTH FROM expected_shipping_date) = 12 THEN (SELECT description FROM dictionaries WHERE code = \'December\' AND language_id = :idLanguage)
                    END as description
            FROM documents_heads
            WHERE document_type_id = (SELECT id FROM documents_types WHERE code = :codeDocumentType)
            AND customer_id = :idCustomer
            AND shipping_date IS NULL',
            ['idLanguage' => $idLanguage, 'codeDocumentType' => DocumentTypeEnum::BeforeDDT, 'idCustomer' => $idCustomer]
        ));
    }

    #endregion GET

    #region UPDATE

    /**
     * Confirm before DDT Sixten
     * 
     * @param Request $request
     * @param int idUser
     * @param int idLanguage
     */
    public static function confirmBeforeDDTSixten(Request $request, int $idUser, int $idLanguage)
    {
        static::_checkFunctionEnabled($request->idFunctionality, $idUser, $idLanguage);

        DB::beginTransaction();

        try {
            $findDocumentHead = DB::select(
                'SELECT id
                                    FROM documents_heads
                                    WHERE customer_id = :idCustomer
                                    AND EXTRACT(MONTH FROM expected_shipping_date) = :idMonth
                                    AND document_type_id = (SELECT id FROM documents_types WHERE code = :codeDocumentType)
                                    AND shipping_date IS NULL',
                ['idCustomer' => $request->idCustomer, 'idMonth' => $request->idMonth, 'codeDocumentType' => DocumentTypeEnum::BeforeDDT]
            );

            if (count($findDocumentHead) == 0) {
                throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::DocumentHeadNotExist), HttpResultsCodesEnum::InvalidPayload);
            } else {
                if (count($findDocumentHead) > 1) {
                    throw new Exception(DictionaryBL::getTranslate($idLanguage, DictionariesCodesEnum::MoreDocumentFound), HttpResultsCodesEnum::InvalidPayload);
                }
            }

            $documentHead = DocumentHead::find($findDocumentHead[0]->id);
            $documentHead->carriage_id = $request->idCarriage;
            $documentHead->carrier_id = $request->idCarrier;
            $documentHead->shipping_date = Carbon::now()->format(HttpHelper::getLanguageDateTimeFormat());
            $documentHead->save();

            $documentHeadSixten = new DocumentHeadSixten();
            $documentHeadSixten->document_head_id = $documentHead->id;
            $documentHeadSixten->packaging_type_sixten_id = $request->idPackagingTypeSixten;
            $documentHeadSixten->shipment_code_sixten_id = $request->idShipmentCodeSixten;
            $documentHeadSixten->weight = $request->weight;
            $documentHeadSixten->packagingNumber = $request->packagingNumber;
            $documentHeadSixten->save();

            DB::update('UPDATE documents_heads
                    SET shipping_date = now(), updated_id = :idUser, updated_at = now()
                    WHERE id IN (
                        SELECT document_head_id
                        FROM documents_rows 
                        WHERE documents_rows.id IN (
                            SELECT documents_rows.document_row_reference
                            FROM documents_heads
                            INNER JOIN documents_rows ON documents_heads.id = documents_rows.document_head_id
                            WHERE documents_heads.id = :idDocumentHead 
                        )
                    )', ['idUser' => $idUser, 'idDocumentHead' => $documentHead->id]);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            throw $ex;
        }
    }

    #endregion UPDATE
}

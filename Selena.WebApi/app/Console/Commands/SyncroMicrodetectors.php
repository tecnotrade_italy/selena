<?php

namespace App\Console\Commands;

use App\BusinessLogic\LanguageBL;
use App\DocumentHeadMdMicrodetectors;
use App\DocumentRowMdMicrodetectors;
use App\Enums\SettingEnum;
use App\Helpers\LogSyncroHelper;
use App\Setting;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;


use App\Mail\LogMail;
use Illuminate\Support\Facades\Mail;

class SyncroMicrodetectors extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'syncro:microdetectors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sincronizzazione Microdetectors';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $startDateSyncro = Carbon::Now()->format('Y-m-d_H-i');
            $startDateSyncroToSave = Carbon::Now()->format('Y-m-d H:i:s');
            $syncroTitle = 'GENERAL';
            $lastDateSync = Setting::where('code', SettingEnum::LastDateSync)->first()->value;
            $idUser = User::where('name', 'syncro')->first()->id;
            $idLanguage = LanguageBL::getDefault()->id;
            $hasError = false;


            $serverName = "GB-SRVMDSQL01\Navision";
            $connectionInfo = array( "Database"=>"MD_NAV_DB","UID"=>"sa", "PWD"=>"sapwd123");
            $conn = sqlsrv_connect($serverName, $connectionInfo);
            

            if( $conn ) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Connessione stabilita   ###');
            }else{
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Errore Connessione stabilita   ###');
            }

            LogSyncroHelper::startLog($startDateSyncro, $syncroTitle);

            #region IMPORTAZIONI

            #region IMPORTAZIONI DOCUMENTSHEAD CONSEGNE

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione DOCUMENTSHEAD CONSEGNE alle ' . Carbon::now() . '   ###');

            try {
                foreach (DB::connection('syncro')->select(
                    'SELECT top 1 D.No_ as Consegna,  C.Name as Cliente, C.[No_] as codCliente FROM [MD Microdetectors$Delivery] as D LEFT OUTER JOIN [MD Microdetectors$Customer] as C on C.No_ = D.[Customer No_]'
                ) as $documentHeadMd) {
                    try {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione DOCUMENTSHEAD CONSEGNE --> ' . $documentHeadMd->Consegna . '   ###');
                        /*$documentHeadSelena = DocumentHeadMdMicrodetectors::where('document_head_id', $documentHeadMd->Consegna)->first();
                        if (is_null($documentHeadSelena)) {
                            $documentHeadSelena = new DocumentHeadMdMicrodetectors();
                            $documentHeadSelena->document_head_id = $documentHeadMd->Consegna;
                            $documentHeadSelena->customer_id = $documentHeadMd->codCliente;
                            $documentHeadSelena->customer_description = $documentHeadMd->Cliente;
                            $documentHeadSelena->created_id = $idUser;
                            $documentHeadSelena->save();
                        }*/
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in salvataggio dei DOCUMENTSHEAD CONSEGNE  - No. ' . $documentHeadMd->Consegna , $documentHeadMd, $ex);
                        $hasError = true;
                    }
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei DOCUMENTSHEAD CONSEGNE', null, $ex);
                $hasError = true;
            }

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine importazione DOCUMENTSHEAD CONSEGNE alle ' . Carbon::now() . '   ###');

            #endregion IMPORTAZIONI DOCUMENTSHEAD CONSEGNE

            #region IMPORTAZIONI DOCUMENTSROWS CONSEGNE

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione DOCUMENTSROWS CONSEGNE alle ' . Carbon::now() . '   ###');
            
            try {
                foreach (DB::connection('syncro')->select(
                    "SELECT top 1  D.No_ as Consegna,  L.[Item No_] as CodArticolo, (CONVERT(int, L.[Qty_ to Ship])) as Quantita, L.Check_spedizione, isnull(CR.[Cross-Reference No_],'') as CodArticoloCliente, isnull(CR.[Box Barcode],'') as BarcodeConfezione, L.No_ as codicespedizione, L.[Line No_] as numerorigaspedizione FROM [MD Microdetectors$Delivery] as D LEFT OUTER JOIN [[MD Microdetectors$Warehouse Shipment Header] as H on H.[Delivery Document No_] = D.No_ LEFT OUTER JOIN [MD Microdetectors$Warehouse Shipment Line] as L on L.No_ = H.No_ WHERE H.[Reason Code] not in ('RF','RN','RR','RRF') ORDER BY L.No_, L.[Line No_] "
                ) as $documentRowMd) {
                    try {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione DOCUMENTSROWS CONSEGNE ART--> ' . $documentRowMd->CodArticolo . '   ###');
                        /*$documentRowSelena = DocumentRowMdMicrodetectors::where('codice_articolo', $documentRowMd->CodArticolo)->where('quantity', $documentRowMd->Quantita)->first();
                        if (is_null($documentRowSelena)) {
                            $documentRowSelena = new DocumentRowMdMicrodetectors();
                            $documentRowSelena->document_head_id = $documentRowMd->Consegna;
                            $documentRowSelena->codice_articolo = $documentRowMd->CodArticolo;
                            $documentRowSelena->codice_articolo_cliente = $documentRowMd->CodArticoloCliente;
                            $documentRowSelena->barcode_spedizione = $documentRowMd->BarcodeConfezione;
                            $documentRowSelena->quantity = $documentRowMd->Quantita;
                            $documentRowSelena->check_spedizione = false;
                            $documentRowSelena->created_id = $idUser;
                            $documentRowSelena->save();
                        }*/
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in salvataggio dei DOCUMENTSROWS CONSEGNE  - No. ' . $documentRowMd->Consegna , $documentRowMd, $ex);
                        $hasError = true;
                    }
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei DOCUMENTSROWS CONSEGNE', null, $ex);
                $hasError = true;
            }

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine importazione DOCUMENTSROWS CONSEGNE alle ' . Carbon::now() . '   ###');

            #endregion IMPORTAZIONI DOCUMENTSROWS CONSEGNE

            #endregion IMPORTAZIONI

            LogSyncroHelper::endLog($startDateSyncro, $syncroTitle);
        } catch (\Throwable $ex) {
            LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore', null, $ex);
            $hasError = true;
        }

        if ($hasError) {
            LogSyncroHelper::send($startDateSyncro, $syncroTitle);
        } else {
            $setting = Setting::where('code', SettingEnum::LastDateSync)->first();
            $setting->value = $startDateSyncroToSave;
            $setting->updated_id = $idUser;
            $setting->save();
        }

        $fileToDelete = storage_path() . '/logs/syncro_' . $syncroTitle . '_' . Carbon::createFromFormat('Y-m-d H:i:s', $startDateSyncroToSave)->subDays(3)->format('Y-m-d_H-i') . '.log';
        if (file_exists($fileToDelete)) {
            unlink($fileToDelete);
        }
    }
}

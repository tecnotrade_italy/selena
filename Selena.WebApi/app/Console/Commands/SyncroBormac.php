<?php

namespace App\Console\Commands;

use App\BusinessLogic\LanguageBL;
use App\Enums\SettingEnum;
use App\Helpers\LogSyncroHelper;
use App\Helpers\LogHelper;
use App\Item;
use App\ItemLanguage;
use App\ItemTechnicalSpecification;
use App\ItemTechnicalSpecificationRevision;
use App\Language;
use App\Setting;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;


use App\Mail\LogMail;
use Illuminate\Support\Facades\Mail;

class SyncroBormac extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'syncro:bormac';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sincronizzazione Bormac';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $startDateSyncro = Carbon::Now()->format('Y-m-d_H-i');
            $startDateSyncroToSave = Carbon::Now()->format('Y-m-d H:i:s');
            $syncroTitle = 'GENERAL';
            $lastDateSync = Setting::where('code', SettingEnum::LastDateSync)->first()->value;
            $idUser = User::where('name', 'syncro')->first()->id;
            $idLanguage = LanguageBL::getDefault()->id;
            $hasError = false;

            LogSyncroHelper::startLog($startDateSyncro, $syncroTitle);

            #region IMPORTAZIONI

            #region IMPORTAZIONI ARTICOLI

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione ITEMS alle ' . Carbon::now() . '   ###');

            try {
                foreach (DB::connection('syncro')->select(
                    'SELECT Item, Description FROM MA_Items WHERE TBCreated >= CONVERT(SMALLDATETIME, :tb_created, 101) OR TBModified = CONVERT(SMALLDATETIME, :tb_modified, 101)',
                    ['tb_created' => $lastDateSync, 'tb_modified' => $lastDateSync]
                ) as $itemFromErp) {
                    try {
                        $item = Item::where('internal_code', $itemFromErp->Item)->first();
                        if (is_null($item)) {
                            $item = new Item();
                            $item->internal_code = $itemFromErp->Item;
                            $item->created_id = $idUser;
                            $item->save();

                            $itemLanguage = new ItemLanguage();
                            $itemLanguage->item_id = $item->id;
                            $itemLanguage->language_id = $idLanguage;
                            $itemLanguage->description = $itemFromErp->Description;
                            $itemLanguage->created_id = $idUser;
                            $itemLanguage->save();
                        } else {
                            $itemLanguage = ItemLanguage::where('item_id', $item->id)->first();

                            if (is_null($itemLanguage)) {
                                $itemLanguage = new ItemLanguage();
                                $itemLanguage->item_id = $item->id;
                                $itemLanguage->language_id = $idLanguage;
                                $itemLanguage->description = $itemFromErp->Description;
                                $itemLanguage->created_id = $idUser;
                            } else {
                                $itemLanguage->description = $itemFromErp->Description;
                                $itemLanguage->updated_id = $idUser;
                            }

                            $itemLanguage->save();
                        }
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in salvataggio dei ITEMS', $itemFromErp, $ex);
                        $hasError = true;
                    }
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei ITEMS', null, $ex);
                $hasError = true;
            }

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine importazione ITEMS alle ' . Carbon::now() . '   ###');

            #endregion IMPORTAZIONI ARTICOLI

            #endregion IMPORTAZIONI



            #region ESPORTAZIONI WEB

            $lastSync = DB::connection('syncro_web')->select('SELECT Valore_CONF as valore FROM Configurazioni WHERE Nome_CONF = \'DataUltimaSyncSelena\'');

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio esportazione web SPECIFICHE TECNICHE alle ' . Carbon::now() . '   ###');

            try {
                foreach (DB::select(
                    'SELECT item_id, max("version") as "version", language_id
									FROM items_technicals_specifications
									WHERE created_at >= :lastSync
									AND updated_at >= :lastSync
									GROUP BY item_id, language_id',
                    ['lastSync' => $lastSync[0]->valore]
                ) as $itemVersionLanguage) {

                    try {
                        $codArticolo = Item::find($itemVersionLanguage->item_id)->internal_code;
                        $lingua = substr(strtoupper(Language::find($itemVersionLanguage->language_id)->description), 0, 3);

                        #region DELETE OLD DATA

                        DB::connection('syncro_web')->delete(
                            'DELETE FROM ArticoliSpecificheTecnicheRevisioni
                            WHERE CodArticoloSpecificaTecnica_ASPR IN (SELECT Id_AST
                                                                        FROM ArticoliSpecificheTecniche
																        WHERE CodArticolo_AST = :codArticolo
																        AND Versione_AST < :versione
																        AND Lingua_AST = :lingua)',
                            [
                                'codArticolo' => $codArticolo,
                                'versione' => $itemVersionLanguage->version,
                                'lingua' => $lingua
                            ]
                        );

                        DB::connection('syncro_web')->delete(
                            'DELETE FROM ArticoliSpecificheTecniche
																WHERE CodArticolo_AST = :codArticolo
																AND Versione_AST < :versione
																AND Lingua_AST = :lingua',
                            [
                                'codArticolo' => $codArticolo,
                                'versione' => $itemVersionLanguage->version,
                                'lingua' => $lingua
                            ]
                        );

                        #endregion DELETE OLD DATA

                        foreach (ItemTechnicalSpecification::where('item_id', $itemVersionLanguage->item_id)
                            ->where('version', $itemVersionLanguage->version)
                            ->where('language_id', $itemVersionLanguage->language_id)->get() as $itemTechnicalSpecification) {
                            #region SPECIFICHE TECNICHE

                            $existOnDB = DB::connection('syncro_web')->select(
                                'SELECT Id_AST
																				FROM ArticoliSpecificheTecniche
																				WHERE Id_AST = :idItemTechnicalSpecification',
                                ['idItemTechnicalSpecification' => $itemTechnicalSpecification->id]
                            );

                            if (count($existOnDB) == 0) {
                                DB::connection('syncro_web')->insert(
                                    'INSERT INTO ArticoliSpecificheTecniche VALUES (:id, :codArticolo, :specificaTecnica, :valore, :versione, :lingua)',
                                    [
                                        'id' => $itemTechnicalSpecification->id,
                                        'codArticolo' => $codArticolo,
                                        'specificaTecnica' => $itemTechnicalSpecification->technical_specification_id,
                                        'valore' => $itemTechnicalSpecification->value,
                                        'versione' => $itemTechnicalSpecification->version,
                                        'lingua' => $lingua
                                    ]
                                );
                            } else {
                                DB::connection('syncro_web')->update(
                                    'UPDATE ArticoliSpecificheTecniche 
									SET CodArticolo_AST = :codArticolo,
									CodSpecificaTecnica_AST = :specificaTecnica,
									Valore_AST = :valore,
									Versione_AST = :versione,
									Lingua_AST = :lingua
									WHERE Id_AST = :id',
                                    [
                                        'codArticolo' => $codArticolo,
                                        'specificaTecnica' => $itemTechnicalSpecification->technical_specification_id,
                                        'valore' => $itemTechnicalSpecification->value,
                                        'versione' => $itemTechnicalSpecification->version,
                                        'lingua' => $lingua,
                                        'id' => $itemTechnicalSpecification->id
                                    ]
                                );
                            }

                            #endregion SPECIFICHE TECNICHE

                            #region SPECIFICHE TECNICHE REVISIONI

                            foreach (ItemTechnicalSpecificationRevision::where('item_technical_specification_id', $itemTechnicalSpecification->id)->get()
                                as $itemTechnicalSpecificationRevision) {
                                $existOnDB = DB::connection('syncro_web')->select(
                                    'SELECT ID_ASPR FROM ArticoliSpecificheTecnicheRevisioni WHERE CodArticoloSpecificaTecnica_ASPR = :idItemTechnicalSpecification',
                                    ['idItemTechnicalSpecification' => $itemTechnicalSpecificationRevision->id]
                                );

                                if (count($existOnDB) == 0) {
                                    DB::connection('syncro_web')->insert(
                                        'INSERT INTO ArticoliSpecificheTecnicheRevisioni VALUES (:id, :codArticoloSpecificaTecnica, :descrizioneGruppoVisualizzazione, :ordineGruppoVisualizzazione, :descrizioneSpecificaTecnica, :posX, :posY, :tipoCampo)',
                                        [
                                            'id' => $itemTechnicalSpecificationRevision->id,
                                            'codArticoloSpecificaTecnica' => $itemTechnicalSpecificationRevision->item_technical_specification_id,
                                            'descrizioneGruppoVisualizzazione' => $itemTechnicalSpecificationRevision->group_display_description,
                                            'ordineGruppoVisualizzazione' => $itemTechnicalSpecificationRevision->group_display_order,
                                            'descrizioneSpecificaTecnica' => $itemTechnicalSpecificationRevision->field_description,
                                            'posX' => $itemTechnicalSpecificationRevision->field_posx,
                                            'posY' => $itemTechnicalSpecificationRevision->field_posy,
                                            'tipoCampo' => $itemTechnicalSpecificationRevision->field_input_type
                                        ]
                                    );
                                } else {
                                    DB::connection('syncro_web')->update(
                                        'UPDATE ArticoliSpecificheTecnicheRevisioni 
										SET CodArticoloSpecificaTecnica_ASPR = :codArticoloSpecificaTecnica,
										DescrizioneGruppoVisualizzazione_ASPR = :descrizioneGruppoVisualizzazione,
										OrdineGruppoVisualizzazione_ASPR = :ordineGruppoVisualizzazione,
										DescrizioneSpecificaTecnica_ASPR = :descrizioneSpecificaTecnica,
										PosX_ASPR = :posX,
										PosY_ASPR = :posY,
										TipoCampo_ASPR = :tipoCampo
										WHERE ID_ASPR = :id',
                                        [
                                            'codArticoloSpecificaTecnica' => $itemTechnicalSpecificationRevision->item_technical_specification_id,
                                            'descrizioneGruppoVisualizzazione' => $itemTechnicalSpecificationRevision->group_display_description,
                                            'ordineGruppoVisualizzazione' => $itemTechnicalSpecificationRevision->group_display_order,
                                            'descrizioneSpecificaTecnica' => $itemTechnicalSpecificationRevision->field_description,
                                            'posX' => $itemTechnicalSpecificationRevision->field_posx,
                                            'posY' => $itemTechnicalSpecificationRevision->field_posy,
                                            'tipoCampo' => $itemTechnicalSpecificationRevision->field_input_type,
                                            'id' => $itemTechnicalSpecificationRevision->id
                                        ]
                                    );
                                }
                            }

                            #endregion SPECIFICHE TECNICHE REVISIONI
                        }
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in salvataggio degli SPECIFICHE TECNICHE', $itemTechnicalSpecification, $ex);
                        $hasError = true;
                    }
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura degli SPECIFICHE TECNICHE', null, $ex);
                $hasError = true;
            }

            #endregion ESPORTAZIONI WEB

            LogSyncroHelper::endLog($startDateSyncro, $syncroTitle);
        } catch (\Throwable $ex) {
            LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore', null, $ex);
            $hasError = true;
        }

        if ($hasError) {
            LogSyncroHelper::send($startDateSyncro, $syncroTitle);
        } else {
            $setting = Setting::where('code', SettingEnum::LastDateSync)->first();
            $setting->value = $startDateSyncroToSave;
            $setting->updated_id = $idUser;
            $setting->save();

            DB::connection('syncro_web')->update('UPDATE Configurazioni SET Valore_CONF = ' . $startDateSyncroToSave . 'WHERE Nome_CONF = \'DataUltimaSyncSelena\'');
        }

        $fileToDelete = storage_path() . '/logs/syncro_' . $syncroTitle . '_' . Carbon::createFromFormat('Y-m-d H:i:s', $startDateSyncroToSave)->subDays(3)->format('Y-m-d_H-i') . '.log';
        if (file_exists($fileToDelete)) {
            unlink($fileToDelete);
        }
    }
}

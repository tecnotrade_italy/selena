<?php

namespace App\Console\Commands;

use App\BusinessLogic\DocumentRowLanguageBL;
use App\BusinessLogic\DocumentTypeBL;
use App\BusinessLogic\ItemLanguageBL;
use App\BusinessLogic\LanguageBL;
use App\Carriage;
use App\CarriageLanguage;
use App\Carrier;
use App\CarrierLanguage;
use App\Category;
use App\CategoryFather;
use App\CategoryItem;
use App\CategoryLanguage;
use App\Currency;
use App\CurrencyLanguage;
use App\Customer;
use App\CustomerSixten;
use App\DocumentHead;
use App\DocumentHeadSixten;
use App\DocumentRow;
use App\DocumentRowLanguage;
use App\DocumentRowType;
use App\DocumentType;
use App\Enums\DocumentRowTypeEnum;
use App\Enums\DocumentTypeEnum;
use App\Enums\SettingEnum;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Helpers\LogSyncroHelper;
use App\Item;
use App\ItemLanguage;
use App\LanguagePaymentType;
use App\LanguageUnitOfMeasure;
use App\PaymentType;
use App\Producer;
use App\Setting;
use App\ShipmentCodeSixten;
use App\UnitOfMeasure;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;

class Ftp_Transfer_File_Droad extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'syncro:ftpdroad';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sincronizzazione dati FTP Droad';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

    #section categorie migrazione dati da file di testo #
                $file_local = Storage::disk('ftp')->get('categorie.txt');
                $file_ftp = Storage::disk('public')->put('categorie.txt', $file_local);

                $file = fopen(storage_path("app/public/categorie.txt"), "r") or exit("Unable to open file!");

                while(!feof($file)) {
                $fileseparato = explode("|", (fgets($file)));

                    DB::beginTransaction();
                    try {
      
                    $CategoryLanguage = CategoryLanguage::where('description',  trim(ucfirst(strtolower($fileseparato[0]))))->get()->first();
                    if (!isset($CategoryLanguage)) {
                            $Categories = new Category();
                             $Categories->code = str_replace(' ', '_', $fileseparato[0]);
                             $Categories->online = true;
                             $Categories->save();
         
                             $CategoriesLanguages = new CategoryLanguage();
                             $CategoriesLanguages->category_id = $Categories->id;

                             if (isset($fileseparato[0])) {
                                $CategoriesLanguages->description = trim(ucfirst(strtolower($fileseparato[0])));
                                $variabileunita = str_replace(' ', '-', $fileseparato[0]);
                                $CategoriesLanguages->meta_tag_link = rtrim(strtolower('/' . $variabileunita));
                                $CategoriesLanguages->meta_tag_title = trim(ucfirst(strtolower($fileseparato[0])));
                                $CategoriesLanguages->meta_tag_description = trim(ucfirst(strtolower($fileseparato[0])));
                                $CategoriesLanguages->keyword = trim(ucfirst(strtolower($fileseparato[0])));
                                $CategoriesLanguages->language_id = 1;
                                $CategoriesLanguages->save();

                                $CategoryFather = new CategoryFather();
                                $CategoryFather->category_id = $CategoriesLanguages->category_id;
                                $CategoryFather->save();
                              }


                             

                             /*$CategoriesLanguages->description = trim(ucfirst(strtolower($fileseparato[0])));
                             $variabileunita = str_replace(' ', '_', $fileseparato[0]);
                             $CategoriesLanguages->meta_tag_link = rtrim(strtolower('/' . $variabileunita));
                             $CategoriesLanguages->meta_tag_title = trim(ucfirst(strtolower($fileseparato[0])));
                             $CategoriesLanguages->meta_tag_description = trim(ucfirst(strtolower($fileseparato[0])));
                             $CategoriesLanguages->keyword = trim(ucfirst(strtolower($fileseparato[0])));
                             $CategoriesLanguages->language_id = 1;
                             $CategoriesLanguages->save();*/
                    }
                    
                        if (isset($fileseparato[2])) {
                                $CategoryLanguage1 = CategoryLanguage::where('description',  trim(ucfirst(strtolower($fileseparato[2]))))->get()->first();

                            if (!isset($CategoryLanguage1)) {
                                        $Categories = new Category();

                                    if (isset($fileseparato[1])){
                                        $Categories->code = $fileseparato[1];
                                        $Categories->online = true;
                                        $Categories->save();
                                    }    

                                        $CategoriesLanguages = new CategoryLanguage();
                                        $CategoriesLanguages->category_id = $Categories->id;

                                    if (isset($fileseparato[2])){
                                        $CategoriesLanguages->description = trim(ucfirst(strtolower($fileseparato[2])));
                                        #$variabileunita = str_replace(' ', '-', $fileseparato[2]);
                                        $variabileunita =  str_replace([':', '\\', '/', '*',' ',',','<>','"','?','|','.','+','>','<','_','__','~','\\\\'], '-', $fileseparato[2]);
                                        $CategoriesLanguages->meta_tag_link = rtrim(strtolower('/' . $variabileunita));
                                        $CategoriesLanguages->meta_tag_title = trim(ucfirst(strtolower($fileseparato[2])));
                                        $CategoriesLanguages->meta_tag_description = trim(ucfirst(strtolower($fileseparato[2])));
                                        $CategoriesLanguages->keyword = trim(ucfirst(strtolower($fileseparato[2])));
                                        $CategoriesLanguages->language_id = 1;
                                        $CategoriesLanguages->save();
                                    }

                                        $categoryFatherdescr = CategoryLanguage::where('description',  trim(ucfirst(strtolower($fileseparato[0]))))->get()->first();

                                        $CategoryFather = new CategoryFather();
                                        $CategoryFather->category_id = $CategoriesLanguages->category_id;
                                        if (isset($categoryFatherdescr)){
                                            $CategoryFather->category_father_id = $categoryFatherdescr->category_id; 
                                        }else{
                                            $CategoryFather->category_father_id = '';
                                        }
                                            $CategoryFather->save();
                            }
                        }
                       DB::commit();
                    } catch (Exception $ex) {
                        DB::rollback();
                        throw $ex;
                    }
            }
            fclose($file);
            # end section categorie migrazione dati da file di testo #

            /* section insert anagrafica produttori  */
            $file_local = Storage::disk('ftp')->get('articoli.txt');
            $file_ftp = Storage::disk('public')->put('articoli.txt', $file_local);

            $fileproducer = fopen(storage_path("app/public/articoli.txt"), "r") or exit("Unable to open file!");

            while(!feof($fileproducer)) {
            $fileProducerDiviso = explode("|", (fgets($fileproducer)));

            DB::beginTransaction();
            try {

                if (isset($fileProducerDiviso[4])) {
                $ProducerExist = Producer::where('business_name',  $fileProducerDiviso[4])->get()->first();
      
                if (!isset($ProducerExist)) {
                $Producer = new Producer();

                if (isset($fileProducerDiviso[4])) {
                $Producer->code = $fileProducerDiviso[4];
                $Producer->business_name = $fileProducerDiviso[4];
                $Producer->online = true;
                $Producer->language_id = 1;
                $Producer->save();   
                }
            }
        }

                DB::commit();
              } catch (Exception $ex) {
                  DB::rollback();
                  throw $ex;
              }  
            }
            fclose($fileproducer);
            /* end section insert anagrafica produttori */

            /* section insert articoli  */
            $file_local = Storage::disk('ftp')->get('articoli.txt');
            $file_ftp = Storage::disk('public')->put('articoli.txt', $file_local);

            $fileitems = fopen(storage_path("app/public/articoli.txt"), "r") or exit("Unable to open file!");

            $cont = 0;
            while(!feof($fileitems)) {
                $fileItemsDiviso = explode("|", (fgets($fileitems)));     

            $cont = $cont +1;
            if($cont > 1){

                    DB::beginTransaction();
                    try {

                        $Items = Item::where('internal_code',  $fileItemsDiviso[0])->get()->first();
                    if (!isset($Items)){
                        $Item = new Item();

                        if (isset($fileItemsDiviso[0])){
                            $Item->internal_code = $fileItemsDiviso[0];
                        }
                        if (isset($fileItemsDiviso[8])){
                            $Item->available = $fileItemsDiviso[8];
                        }
                        $Item->online = true;

                        if (isset($fileItemsDiviso[4])){
                            $Producer = Producer::where('business_name', $fileItemsDiviso[4])->get()->first();
                            if (isset($Producer)){
                                $Item->producer_id = $Producer->id;
                            }else{
                                $Item->producer_id = '';
                            }
                        }
                           
                        if (isset($fileItemsDiviso[9])){
                            if ($fileItemsDiviso[9] != ''){
                                $Item->ordered_supplier = $fileItemsDiviso[9];
                            }else{
                                $Item->ordered_supplier = null;
                            }
                        }

                        if (isset($fileItemsDiviso[10])){
                            if ($fileItemsDiviso[10] == 1){
                                $Item->promotion = true;
                            }else{
                                $Item->promotion = false;
                            }
                        }
                        $Item->save();
    
                        $ItemsLanguages = ItemLanguage::where('item_id', $Item->id)->get()->first();
                        if (!isset($ItemsLanguages)){
                            $ItemLanguage = new ItemLanguage();
                            $ItemLanguage->item_id = $Item->id;
                            $ItemLanguage->language_id = 1;

                            if (isset($fileItemsDiviso[3])) {
                                $ItemLanguage->description = trim(ucfirst(strtolower($fileItemsDiviso[3])));
                            }else{
                                $ItemLanguage->description = '';
                            }   

                            if (isset($fileItemsDiviso[3])) {
                            //if (isset($fileItemsDiviso[3]) && $fileItemsDiviso[3] != '' && !is_null($fileItemsDiviso[3])){
                            #$VariabileLinkUnita = str_replace(' ', '-', $fileItemsDiviso[3]);
                            $VariabileLinkUnita =  str_replace([':', '\\', '/', '*',' ',',','<>','"','?','|','.','+','>','<','_','__','~','\\\\'], '-', $fileItemsDiviso[3]);
                            $ItemLanguage->link = rtrim(strtolower('/' . $VariabileLinkUnita));
                            $ItemLanguage->meta_tag_title = trim(ucfirst(strtolower($fileItemsDiviso[3])));
                            $ItemLanguage->meta_tag_description = trim(ucfirst(strtolower($fileItemsDiviso[3])));
                            $ItemLanguage->meta_tag_keyword = trim(ucfirst(strtolower($fileItemsDiviso[3])));
                            }
                            $ItemLanguage->save();
                        }
                        if (isset($fileItemsDiviso[6])){
                            $CategoryCode = Category::where('code', $fileItemsDiviso[6])->get()->first();
                            if (isset($CategoryCode)){
                                $CategoriesItem = CategoryItem::where('category_id', $CategoryCode->id)->where('item_id', $Item->id)->get()->first();
                                if (!isset($CategoriesItem)){
                                $CategoriesItems = new CategoryItem();
                                    if (isset($CategoryCode)){
                                        $CategoriesItems->category_id = $CategoryCode->id;
                                    }else{
                                        $CategoriesItems->category_id = '';
                                    }  
                                    $CategoriesItems->item_id = $ItemLanguage->item_id;
                                    $CategoriesItems->save();  
                                }
                            }
                        }
                    }    
                    DB::commit();
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }  
            }
        }
            fclose($fileitems);


    /* section update articoli inserimento immagini + caratteristiche */

            $file_local = Storage::disk('ftp')->get('descp.txt');
            $file_ftp = Storage::disk('public')->put('descp.txt', $file_local);

            $filedesc = fopen(storage_path("app/public/descp.txt"), "r") or exit("Unable to open file!");

            $cont = 0;
            while(!feof($filedesc)) {
            $fileDescr = explode("|", (fgets($filedesc)));

            $cont = $cont +1;
            if($cont > 1){
  
                    DB::beginTransaction();
                    try {

                        if (isset($fileDescr[0])) {
                            $ItemsOnDb = Item::where('internal_code', $fileDescr[0])->get()->first();
                            if (isset($ItemsOnDb)) {
                                if (isset($fileDescr[1])) {
                                $ItemsOnDb->img = $fileDescr[1]; 
                                } 
                                $ItemsOnDb->save();

                                $ItemsLanguage = ItemLanguage::where('item_id', $ItemsOnDb->id)->get()->first();
                                if (isset($ItemsLanguage)) {
                                    if (isset($fileDescr[3])) {
                                    $ItemsLanguage->meta_tag_characteristic = $fileDescr[3];
                                    }

                                    $ItemsLanguage->save();
                                }
                            }
                        }
                    DB::commit();
                  } catch (Exception $ex) {
                      DB::rollback();
                      throw $ex;
                  }  
                }
        }
            fclose($filedesc);
        /* end section update articoli inserimento immagini + caratteristiche */

        /*
            select
            ci.category_id,
            ci.item_id,
            cl.description as Categoria,
            i.internal_code as CodiceArticolo, 
            i.img as Immagine,
            i.ordered_supplier as OrdineInArrivo,
            p.business_name as Produttore,
            i.available as Disponibilita,
            i.weight as Peso,
            i.height as Altezza,
            i."depth" as Profondita,
            i.width as Larghezza,
            il.meta_tag_characteristic as SchedaProdotto
            from categories_items ci 
            left join items_languages il on il.item_id = ci.item_id 
            left join items i on i.id = il.item_id 
            left join producer as p on p.id = i.producer_id
            left join categories_languages cl on cl.category_id = ci.category_id 
            order by ci.item_id desc
        */

        /* section update immagini */
            $file_local = Storage::disk('ftp')->get('immagini.txt');
            $file_ftp = Storage::disk('public')->put('immagini.txt', $file_local);

            $fileimmagini = fopen(storage_path("app/public/immagini.txt"), "r") or exit("Unable to open file!");

            $cont = 0;
            while(!feof($fileimmagini)) {
            $fileImmag = explode("|", (fgets($fileimmagini)));

            $cont = $cont +1;
            if($cont > 1){

                    DB::beginTransaction();
                    try {
                    
                        if (isset($fileImmag[0])) {
                            $ItemsOnDb = Item::where('internal_code', $fileImmag[0])->get()->first();
                            if (isset($ItemsOnDb)) {
                                if (isset($fileImmag[1])) {
                                    $ItemsOnDb->img = $fileImmag[1];
                                }  
                                $ItemsOnDb->save();
                            }
                        }
                    DB::commit();
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }  
            }
        }
            fclose($fileimmagini);
        /* end section update immagini */

                /* section update articoli aggiungi peso */
                $file_local = Storage::disk('ftp')->get('peso.txt');
                $file_ftp = Storage::disk('public')->put('peso.txt', $file_local);

                $filepeso = fopen(storage_path("app/public/peso.txt"), "r") or exit("Unable to open file!");

                $cont = 0;
                while(!feof($filepeso)) {
                $filePeso= explode("|", (fgets($filepeso)));
    
                $cont = $cont +1;
                if($cont > 1){
    
                        DB::beginTransaction();
                        try {
                            if (isset($filePeso[0])) {
                                $ItemsOnDb = Item::where('internal_code', $filePeso[0])->get()->first();
                                if (isset($ItemsOnDb)) {
                                    if (isset($filePeso[1])) {  
                                        $ItemsOnDb->weight = $filePeso[1];  
                                    }
                                    $ItemsOnDb->save();
                                }
                            }
                        DB::commit();
                    } catch (Exception $ex) {
                        DB::rollback();
                        throw $ex;
                    }  
                    }
            }
                fclose($filepeso);
                /* end section update articoli aggiungi peso */

                 /* section update articoli disponibilità */
                $file_local = Storage::disk('ftp')->get('dispo.txt');
                $file_ftp = Storage::disk('public')->put('dispo.txt', $file_local);

                $filedispo = fopen(storage_path("app/public/dispo.txt"), "r") or exit("Unable to open file!");

                $cont = 0;
                while(!feof($filedispo)) {
                $fileDispo= explode("|", (fgets($filedispo)));

                $cont = $cont +1;
                if($cont > 1){

                        DB::beginTransaction();
                        try {
                            if (isset($fileDispo[0])) {
                                $ItemsOnDb = Item::where('internal_code', $fileDispo[0])->get()->first();
                                if (isset($ItemsOnDb)) {
                                    if (isset($fileDispo[1])) { 
                                        $ItemsOnDb->available = $fileDispo[1]; 
                                    } 
                                    $ItemsOnDb->save();
                                }
                            }
                        DB::commit();
                    } catch (Exception $ex) {
                        DB::rollback();
                        throw $ex;
                    }  
                }
            }
                fclose($filedispo);
                /* end section update articoli disponibilità */


                /* end section update articoli giorni dispobilità */
                $file_local = Storage::disk('ftp')->get('giorni_disponibilita.txt');
                $file_ftp = Storage::disk('public')->put('giorni_disponibilita.txt', $file_local);

                $filegiornidisponibilita = fopen(storage_path("app/public/giorni_disponibilita.txt"), "r") or exit("Unable to open file!");

                $cont = 0;
                while(!feof($filegiornidisponibilita)) {
                $fileGiorniDispo= explode("|", (fgets($filegiornidisponibilita)));
    
                $cont = $cont +1;
                if($cont > 1){
    
                        DB::beginTransaction();
                        try {
                            if (isset($fileGiorniDispo[0])) {
                                $ItemsOnDb = Item::where('internal_code', $fileGiorniDispo[0])->get()->first();
                                if (isset($ItemsOnDb)) {
                                    if (isset($fileGiorniDispo[1])) {
                                        $ItemsOnDb->day_available = $fileGiorniDispo[1];  
                                    }
                                    $ItemsOnDb->save();
                                }
                            }
                        DB::commit();
                    } catch (Exception $ex) {
                        DB::rollback();
                        throw $ex;
                    }  
                }
            }
                fclose($filegiornidisponibilita);
                /* end section update articoli giorni dispobilità */

                 /* end section update articoli dimensioni */
                 $file_local = Storage::disk('ftp')->get('dimensioni.txt');
                 $file_ftp = Storage::disk('public')->put('dimensioni.txt', $file_local);
 
                 $filedimensioni = fopen(storage_path("app/public/dimensioni.txt"), "r") or exit("Unable to open file!");
                     
                 $cont = 0;
                 while(!feof($filedimensioni)) {
                 $fileDimensioni= explode("|", (fgets($filedimensioni)));

                 $cont = $cont +1;
                 if($cont > 1){
     
                         DB::beginTransaction();
                         try {
                             if (isset($fileDimensioni[0])) {
                                 $ItemsOnDb = Item::where('internal_code', $fileDimensioni[0])->get()->first();
                                 if (isset($ItemsOnDb)) {
                                  //  if (trim($fileDimensioni[2]) != '' && $fileDimensioni[2] != null) {
                                        if (isset($fileDimensioni[2])) {
                                       // if(is_numeric($fileDimensioni[2])){
                                            $ItemsOnDb->width = $fileDimensioni[2]; //Larghezza
                                      // }
                                  //  }
                                }
                                   // if (trim($fileDimensioni[3]) != '' && $fileDimensioni[3] != null){
                                        if (isset($fileDimensioni[3])) {
                                      //  if(is_numeric($fileDimensioni[3])){  
                                            $ItemsOnDb->depth = $fileDimensioni[3]; //Profondita
                                        }
                                   // } 
                               // }  
                                
                                   // if (trim($fileDimensioni[4]) != '' && $fileDimensioni[4] != null){
                                        if (isset($fileDimensioni[4])) {
                                       // if(is_numeric($fileDimensioni[4])){
                                            $ItemsOnDb->height = $fileDimensioni[4]; //Altezza
                                       // }
                                    }
                               // }
                                     $ItemsOnDb->save();
                                 }
                             }
                         DB::commit();
                     } catch (Exception $ex) {
                         DB::rollback();
                         throw $ex;
                     }  
                 }
            }
                 fclose($filedimensioni);
                  /* end section update articoli dimensioni */


            /* section update prezzi articoli*/
            $file_local = Storage::disk('ftp')->get('/C211237/prezzi_no_sconto.txt');
            $file_ftp = Storage::disk('public')->put('prezzi_no_sconto.txt', $file_local);

            $fileprezzi= fopen(storage_path("app/public/prezzi_no_sconto.txt"), "r") or exit("Unable to open file!");

            $cont = 0;
            while(!feof($fileprezzi)) {
            $filePrezzi = explode("|", (fgets($fileprezzi)));
            $cont = $cont +1;
            if($cont > 1){
                    DB::beginTransaction();
                    try {
                        if (isset($filePrezzi[0])) {
  
                            $ItemsOnDb = Item::where('internal_code', $filePrezzi[0])->get()->first();
                            if (isset($ItemsOnDb)) {
                                if (isset($filePrezzi[1])) {
                                    $ItemsOnDb->cost = $filePrezzi[1];  
                                    $ItemsOnDb->price = $filePrezzi[1];  
                                }
                                $ItemsOnDb->save();
                            }
                        }
                    DB::commit();
                } catch (Exception $ex) {
                    DB::rollback();
                    throw $ex;
                }  
            }
            }
            fclose($fileprezzi);
            /* end section update prezzi articoli */

                /*
                $file_local = Storage::disk('ftp')->get('filtri.txt');
                $file_ftp = Storage::disk('public')->put('filtri.txt', $file_local);
                */
          

        
    }
}

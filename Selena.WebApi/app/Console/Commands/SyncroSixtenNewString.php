<?php

namespace App\Console\Commands;

use App\BusinessLogic\DocumentRowLanguageBL;
use App\BusinessLogic\DocumentTypeBL;
use App\BusinessLogic\ItemLanguageBL;
use App\BusinessLogic\LanguageBL;
use App\Carriage;
use App\CarriageLanguage;
use App\Carrier;
use App\CarrierLanguage;
use App\Currency;
use App\CurrencyLanguage;
use App\Customer;
use App\CustomerSixten;
use App\DocumentHead;
use App\DocumentHeadSixten;
use App\DocumentRow;
use App\DocumentRowLanguage;
use App\DocumentRowType;
use App\DocumentType;
use App\Enums\DocumentRowTypeEnum;
use App\Enums\DocumentTypeEnum;
use App\Enums\SettingEnum;
use App\Helpers\HttpHelper;
use App\Helpers\LogSyncroHelper;
use App\Helpers\LogHelper;
use App\Item;
use App\ItemLanguage;
use App\LanguagePaymentType;
use App\LanguageUnitOfMeasure;
use App\PaymentType;
use App\Setting;
use App\ShipmentCodeSixten;
use App\UnitOfMeasure;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class SyncroSixtenNewString extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'syncro:sixten';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sincronizzazione dati Sixten';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$startDateSyncro = Carbon::Now()->format('Y-m-d_H-i');
		$syncroTitle = 'GENERAL';
		LogSyncroHelper::startLog($startDateSyncro, $syncroTitle);
		LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione CARRIAGES alle  ###');
		$serverName = "SERVER-2016\ARCAEVO";
		$connectionInfo = array( "Database"=>"ADB_SIXTEN","UID"=>"sa", "PWD"=>"4jhG599C");
		$conn = sqlsrv_connect($serverName, $connectionInfo);
		

		if( $conn ) {
			 LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Connessione stabilita   ###');
		}else{
			 LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Errore Connessione stabilita   ###');
		}
		
        try {
            $startDateSyncro = Carbon::Now()->format('Y-m-d_H-i');
            $startDateSyncroToSave = Carbon::Now()->format('Y-m-d H:i:s');
            $syncroTitle = 'GENERAL';
            $lastDateSync = Setting::where('code', SettingEnum::LastDateSync)->first()->value;
            $idUser = User::where('name', 'syncro')->first()->id;
            $idLanguage = LanguageBL::getDefault()->id;
            $hasError = false;
            $idDocumentTypeOrder = DocumentTypeBL::getIdByCode(DocumentTypeEnum::Order);
            $idDocumentRowTypeItem = DocumentRowType::where('code', DocumentRowTypeEnum::Item)->first()->id;

            LogSyncroHelper::startLog($startDateSyncro, $syncroTitle);

            #region IMPORTAZIONE

            #region IMPORTAZIONE CARRIAGES

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione CARRIAGES alle ' . Carbon::now() . '   ###');

            try {
               /* foreach (DB::connection('syncro')->select('SELECT LTRIM(RTRIM(Cd_DOPorto)) AS code, LTRIM(RTRIM(Descrizione)) AS description
                                                            FROM xbtvw_sixtenlogis_DOPorto
                                                            WHERE TimeIns >= CONVERT(SMALLDATETIME, :timeIns, 101)
                                                            OR TimeUpd >= CONVERT(SMALLDATETIME, :timeUpd, 101)', [
                    'timeIns' => $lastDateSync, 'timeUpd' => $lastDateSync
                ])
                    as $carriageFromErp) {*/
                
				$carriageFromErp = sqlsrv_query($conn,'SELECT LTRIM(RTRIM(Cd_DOPorto)) AS code, LTRIM(RTRIM(Descrizione)) AS description FROM xbtvw_sixtenlogis_DOPorto WHERE TimeIns >= CONVERT(SMALLDATETIME, \'' . $lastDateSync . '\', 101) OR TimeUpd >= CONVERT(SMALLDATETIME,  \'' . $lastDateSync . '\', 101)');
				while( $row = sqlsrv_fetch_array( $carriageFromErp, SQLSRV_FETCH_ASSOC)) {
                    try {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio analisi CARRIAGES - ' . $row['code'] . '   ###');
                        $carriage = Carriage::where('code', $row['code'])->first();

                        if (is_null($carriage)) {
                            $carriage = new Carriage();
                            $carriage->code = $row['code'];
                            $carriage->created_id = $idUser;
                            $carriage->save();

                            $carriageLanguage = new CarriageLanguage();
                            $carriageLanguage->carriage_id = $carriage->id;
                            $carriageLanguage->language_id = $idLanguage;
                            $carriageLanguage->description = $row['description'];
                            $carriageLanguage->created_id = $idUser;
                            $carriageLanguage->save();
                        } else {
                            $carriageLanguage = CarriageLanguage::where('carriage_id', $carriage->id)->first();
                            if (is_null($carriageLanguage)) {
                                $carriageLanguage = new CarriageLanguage();
                                $carriageLanguage->carriage_id = $carriage->id;
                                $carriageLanguage->language_id = $idLanguage;
                                $carriageLanguage->description = $row['description'];
                                $carriageLanguage->created_id = $idUser;
                                $carriageLanguage->save();
                            } else {
                                $carriageLanguage->description = $row['description'];
                                $carriageLanguage->updated_id = $idUser;
                                $carriageLanguage->save();
                            }
                        }
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine analisi CARRIAGES - ' . $row['code'] . '   ###');
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in salvataggio dei CARRIAGES', $carriageFromErp, $ex);
                        $hasError = true;
                    }
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei CARRIAGES', null, $ex);
                $hasError = true;
            }

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine importazione CARRIAGES alle ' . Carbon::now() . '   ###');

            #endregion IMPORTAZIONE CARRIAGES

            #region IMPORTAZIONE CARRIERS

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione CARRIERS alle ' . Carbon::now() . '   ###');

            try {
                /*foreach (DB::connection('syncro')->select('SELECT LTRIM(RTRIM(Cd_DoVettore)) AS code, LTRIM(RTRIM(Descrizione)) AS description
                                                            FROM xbtvw_sixtenlogis_DOVettore
                                                            WHERE TimeIns >= CONVERT(SMALLDATETIME, :timeIns, 101)
                                                            OR TimeUpd >= CONVERT(SMALLDATETIME, :timeUpd, 101)', [
                    'timeIns' => $lastDateSync, 'timeUpd' => $lastDateSync
                ])
                    as $carrierFromErp) {*/
                $carrierFromErp = sqlsrv_query($conn,'SELECT LTRIM(RTRIM(Cd_DoVettore)) AS code, LTRIM(RTRIM(Descrizione)) AS description FROM xbtvw_sixtenlogis_DOVettore WHERE TimeIns >= CONVERT(SMALLDATETIME, \'' . $lastDateSync . '\', 101) OR TimeUpd >= CONVERT(SMALLDATETIME,  \'' . $lastDateSync . '\', 101)');
                while( $row = sqlsrv_fetch_array( $carrierFromErp, SQLSRV_FETCH_ASSOC)) {
                    try {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio analisi CARRIERS - ' . $row['code'] . '   ###');
                        $carrier = Carrier::where('code', $row['code'])->first();

                        if (is_null($carrier)) {
                            $carrier = new Carrier();
                            $carrier->code = $row['code'];
                            $carrier->created_id = $idUser;
                            $carrier->save();

                            $carrierLanguage = new CarrierLanguage();
                            $carrierLanguage->carrier_id = $carrier->id;
                            $carrierLanguage->language_id = $idLanguage;
                            $carrierLanguage->description = $row['description'];
                            $carrierLanguage->created_id = $idUser;
                            $carrierLanguage->save();
                        } else {
                            $carrierLanguage = CarrierLanguage::where('carrier_id', $carrier->id)->first();
                            if (is_null($carrierLanguage)) {
                                $carrierLanguage = new CarrierLanguage();
                                $carrierLanguage->carrier_id = $carrier->id;
                                $carrierLanguage->language_id = $idLanguage;
                                $carrierLanguage->description = $row['description'];
                                $carrierLanguage->created_id = $idUser;
                                $carrierLanguage->save();
                            } else {
                                $carrierLanguage->description = $row['description'];
                                $carrierLanguage->updated_id = $idUser;
                                $carrierLanguage->save();
                            }
                        }
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine analisi CARRIERS - ' . $row['code'] . '   ###');
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in salvataggio dei CARRIERS', $carrierFromErp, $ex);
                        $hasError = true;
                    }
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei CARRIERS', null, $ex);
                $hasError = true;
            }

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine importazione CARRIERS alle ' . Carbon::now() . '   ###');

            #endregion IMPORTAZIONE CARRIERS

            #region IMPORTAZIONE SHIPMENT CODE SIXTEN

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione SHIPMENT CODE SIXTEN alle ' . Carbon::now() . '   ###');
            try {
                /*foreach (DB::connection('syncro')->select('SELECT RTRIM(LTRIM(Cd_DOSped)) AS id_erp, RTRIM(LTRIM(Descrizione)) AS description
                                                            FROM xbtvw_sixtenlogis_DOSped
                                                            WHERE TimeIns >= CONVERT(SMALLDATETIME, :timeIns, 101)
                                                            OR TimeUpd >= CONVERT(SMALLDATETIME, :timeUpd, 101)', [
                    'timeIns' => $lastDateSync, 'timeUpd' => $lastDateSync
                ])
                    as $shipmentCodeSixtenFromErp) {*/
                $shipmentCodeSixtenFromErp = sqlsrv_query($conn,'SELECT RTRIM(LTRIM(Cd_DOSped)) AS id_erp, RTRIM(LTRIM(Descrizione)) AS description FROM xbtvw_sixtenlogis_DOSped WHERE TimeIns >= CONVERT(SMALLDATETIME, \'' . $lastDateSync . '\', 101) OR TimeUpd >= CONVERT(SMALLDATETIME,  \'' . $lastDateSync . '\', 101)');
                while( $row = sqlsrv_fetch_array( $shipmentCodeSixtenFromErp, SQLSRV_FETCH_ASSOC)) {
                    try {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio analisi SHIPMENT CODE SIXTEN - ' . $row['id_erp'] . '   ###');

                        $shipmentCodeSixten = ShipmentCodeSixten::where('erp_id', $row['id_erp'])->first();

                        if (is_null($shipmentCodeSixten)) {
                            $shipmentCodeSixten = new ShipmentCodeSixten();
                            $shipmentCodeSixten->created_id = $idUser;
                        } else {
                            $shipmentCodeSixten->updated_id = $idUser;
                        }

                        $shipmentCodeSixten->erp_id = $row['id_erp'];
                        $shipmentCodeSixten->description = $row['description'];
                        $shipmentCodeSixten->save();

                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine analisi SHIPMENT CODE SIXTEN - ' . $row['id_erp'] . '   ###');
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in salvataggio dei SHIPMENT CODE SIXTEN', $shipmentCodeSixtenFromErp, $ex);
                        $hasError = true;
                    }
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei SHIPMENT CODE SIXTEN', null, $ex);
                $hasError = true;
            }

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine importazione SHIPMENT CODE SIXTEN alle ' . Carbon::now() . '   ###');

            #endregion IMPORTAZIONE SHIPMENT CODE SIXTEN

            #region IMPORTAZIONE CURRENCIES

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione CURRENCIES alle ' . Carbon::now() . '   ###');

            try {
                /*foreach (DB::connection('syncro')->select('SELECT LTRIM(RTRIM(Cd_VL)) AS code, LTRIM(RTRIM(Simbolo)) AS symbol, LTRIM(RTRIM(Descrizione)) AS description
                                                            FROM xbtvw_sixtenlogis_VL
                                                            WHERE TimeIns >= CONVERT(SMALLDATETIME, :timeIns, 101)
                                                            OR TimeUpd >= CONVERT(SMALLDATETIME, :timeUpd, 101)', [
                    'timeIns' => $lastDateSync, 'timeUpd' => $lastDateSync
                ])
                    as $currencyFromErp) {*/
                $currencyFromErp = sqlsrv_query($conn,'SELECT LTRIM(RTRIM(Cd_VL)) AS code, LTRIM(RTRIM(Simbolo)) AS symbol, LTRIM(RTRIM(Descrizione)) AS description FROM xbtvw_sixtenlogis_VL WHERE TimeIns >= CONVERT(SMALLDATETIME, \'' . $lastDateSync . '\', 101) OR TimeUpd >= CONVERT(SMALLDATETIME,  \'' . $lastDateSync . '\', 101)');
                while( $row = sqlsrv_fetch_array( $currencyFromErp, SQLSRV_FETCH_ASSOC)) {
                    try {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio analisi CURRENCIES - ' . $row['code'] . '   ###');
                        $currency = Currency::where('code', $row['code'])->first();

                        if (is_null($currency)) {
                            $currency = new Currency();
                            $currency->code = $row['code'];
                            $currency->symbol = $row['symbol'];
                            $currency->created_id = $idUser;
                            $currency->save();

                            $currencyLanguage = new CurrencyLanguage();
                            $currencyLanguage->currency_id = $currency->id;
                            $currencyLanguage->language_id = $idLanguage;
                            $currencyLanguage->description = $row['description'];
                            $currencyLanguage->created_id = $idUser;
                            $currencyLanguage->save();
                        } else {
                            $currencyLanguage = CurrencyLanguage::where('currency_id', $currency->id)->first();
                            if (is_null($currencyLanguage)) {
                                $currencyLanguage = new CurrencyLanguage();
                                $currencyLanguage->currency_id = $currency->id;
                                $currencyLanguage->language_id = $idLanguage;
                                $currencyLanguage->description = $row['description'];
                                $currencyLanguage->created_id = $idUser;
                                $currencyLanguage->save();
                            } else {
                                $currencyLanguage->description = $row['description'];
                                $currencyLanguage->updated_id = $idUser;
                                $currencyLanguage->save();
                            }
                        }
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine analisi CURRENCIES - ' . $row['code'] . '   ###');
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in salvataggio dei CURRENCIES', $currencyFromErp, $ex);
                        $hasError = true;
                    }
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei CURRENCIES', null, $ex);
                $hasError = true;
            }

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine importazione CURRENCIES alle ' . Carbon::now() . '   ###');

            #endregion IMPORTAZIONE CURRENCIES

            #region IMPORTAZIONE PAYMENTS TYPES

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione PAYMENTS TYPES alle ' . Carbon::now() . '   ###');

            try {
                /*foreach (DB::connection('syncro')->select('SELECT LTRIM(RTRIM(Cd_PG)) AS code, LTRIM(RTRIM(Descrizione)) AS description
                                                            FROM xbtvw_sixtenlogis_PG
                                                            WHERE TimeIns >= CONVERT(SMALLDATETIME, :timeIns, 101)
                                                            OR TimeUpd >= CONVERT(SMALLDATETIME, :timeUpd, 101)', [
                    'timeIns' => $lastDateSync, 'timeUpd' => $lastDateSync
                ])
                    as $paymenTypeFromErp) {*/
                $paymenTypeFromErp = sqlsrv_query($conn,'SELECT LTRIM(RTRIM(Cd_PG)) AS code, LTRIM(RTRIM(Descrizione)) AS description FROM xbtvw_sixtenlogis_PG WHERE TimeIns >= CONVERT(SMALLDATETIME, \'' . $lastDateSync . '\', 101) OR TimeUpd >= CONVERT(SMALLDATETIME,  \'' . $lastDateSync . '\', 101)');
                while( $row = sqlsrv_fetch_array( $paymenTypeFromErp, SQLSRV_FETCH_ASSOC)) {
                    try {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio analisi PAYMENTS TYPES - ' . $row['code'] . '   ###');
                        $paymenType = PaymentType::where('code', $row['code'])->first();

                        if (is_null($paymenType)) {
                            $paymenType = new PaymentType();
                            $paymenType->code = $row['code'];
                            $paymenType->created_id = $idUser;
                            $paymenType->save();

                            $languagePaymentType = new LanguagePaymentType();
                            $languagePaymentType->language_id = $idLanguage;
                            $languagePaymentType->paymenType_id = $paymenType->id;
                            $languagePaymentType->description = $row['description'];
                            $languagePaymentType->created_id = $idUser;
                            $languagePaymentType->save();
                        } else {
                            $languagePaymentType = LanguagePaymentType::where('payment_type_id', $paymenType->id)->first();
                            if (is_null($languagePaymentType)) {
                                $languagePaymentType = new LanguagePaymentType();
                                $languagePaymentType->language_id = $idLanguage;
                                $languagePaymentType->payment_type_id = $paymenType->id;
                                $languagePaymentType->description = $row['description'];
                                $languagePaymentType->created_id = $idUser;
                                $languagePaymentType->save();
                            } else {
                                $languagePaymentType->description = $row['description'];
                                $languagePaymentType->updated_id = $idUser;
                                $languagePaymentType->save();
                            }
                        }
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine analisi PAYMENTS TYPES - ' . $row['code'] . '   ###');
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in salvataggio dei PAYMENTS TYPES', $paymenTypeFromErp, $ex);
                        $hasError = true;
                    }
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei PAYMENTS TYPES', null, $ex);
                $hasError = true;
            }

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine importazione PAYMENTS TYPES alle ' . Carbon::now() . '   ###');

            #endregion IMPORTAZIONE PAYMENTS TYPES

            #region IMPORTAZIONE UNITS OF MEASURES

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione UNITS OF MEASURES alle ' . Carbon::now() . '   ###');

            try {
                /*foreach (DB::connection('syncro')->select('SELECT LTRIM(RTRIM(Cd_ARMisura)) AS code, LTRIM(RTRIM(Descrizione)) AS description
                                                            FROM xbtvw_sixtenlogis_ARMisura
                                                            WHERE TimeIns >= CONVERT(SMALLDATETIME, :timeIns, 101)
                                                            OR TimeUpd >= CONVERT(SMALLDATETIME, :timeUpd, 101)', [
                    'timeIns' => $lastDateSync, 'timeUpd' => $lastDateSync
                ])
                    as $unitOfMeasureFromErp) {*/
                $unitOfMeasureFromErp = sqlsrv_query($conn,'SELECT LTRIM(RTRIM(Cd_ARMisura)) AS code, LTRIM(RTRIM(Descrizione)) AS description FROM xbtvw_sixtenlogis_ARMisura WHERE TimeIns >= CONVERT(SMALLDATETIME, \'' . $lastDateSync . '\', 101) OR TimeUpd >= CONVERT(SMALLDATETIME,  \'' . $lastDateSync . '\', 101)');
                while( $row = sqlsrv_fetch_array( $unitOfMeasureFromErp, SQLSRV_FETCH_ASSOC)) {
                    try {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio analisi UNITS OF MEASURES - ' . $row['code'] . '   ###');
                        $unitOfMeasure = UnitOfMeasure::where('code', $row['code'])->first();

                        if (is_null($unitOfMeasure)) {
                            $unitOfMeasure = new UnitOfMeasure();
                            $unitOfMeasure->code = $row['code'];
                            $unitOfMeasure->created_id = $idUser;
                            $unitOfMeasure->save();

                            $languageUnitOfMeasure = new LanguageUnitOfMeasure();
                            $languageUnitOfMeasure->language_id = $idLanguage;
                            $languageUnitOfMeasure->unit_of_measure_id = $unitOfMeasure->id;
                            $languageUnitOfMeasure->description = $row['description'];
                            $languageUnitOfMeasure->created_id = $idUser;
                            $languageUnitOfMeasure->save();
                        } else {
                            $languageUnitOfMeasure = LanguageUnitOfMeasure::where('unit_of_measure_id', $unitOfMeasure->id)->first();
                            if (is_null($languageUnitOfMeasure)) {
                                $languageUnitOfMeasure = new LanguageUnitOfMeasure();
                                $languageUnitOfMeasure->language_id = $idLanguage;
                                $languageUnitOfMeasure->unit_of_measure_id = $unitOfMeasure->id;
                                $languageUnitOfMeasure->description = $row['description'];
                                $languageUnitOfMeasure->created_id = $idUser;
                                $languageUnitOfMeasure->save();
                            } else {
                                $languageUnitOfMeasure->description = $row['description'];
                                $languageUnitOfMeasure->updated_id = $idUser;
                                $languageUnitOfMeasure->save();
                            }
                        }
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine analisi UNITS OF MEASURES - ' . $row['code'] . '   ###');
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in salvataggio dei UNITS OF MEASURES', $unitOfMeasureFromErp, $ex);
                        $hasError = true;
                    }
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei UNITS OF MEASURES', null, $ex);
                $hasError = true;
            }

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine importazione UNITS OF MEASURES alle ' . Carbon::now() . '   ###');

            #endregion IMPORTAZIONE UNITS OF MEASURES

            #region IMPORTAZIONE CUSTOMER

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione CUSTOMER alle ' . Carbon::now() . '   ###');

            try {
               /* foreach (DB::connection('syncro')->select('SELECT RTRIM(LTRIM(Descrizione)) AS business_name, RTRIM(LTRIM(PartitaIva)) AS vat_number
                                                                , RTRIM(LTRIM(CodiceFiscale)) AS fiscal_code, RTRIM(LTRIM(Indirizzo)) AS address
                                                                , RTRIM(LTRIM(Cd_CF)) AS id_erp, RTRIM(LTRIM(Cd_DOVettore)) AS carrier_code
                                                                , RTRIM(LTRIM(Cd_DOPorto)) AS carriage_code, RTRIM(LTRIM(Cd_DOSped)) AS shipment_code_sixten_code
                                                            FROM xbtvw_sixtenlogis_CFCli
                                                            WHERE TimeIns >= CONVERT(SMALLDATETIME, :timeIns, 101)
                                                            OR TimeUpd >= CONVERT(SMALLDATETIME, :timeUpd, 101)', [
                    'timeIns' => $lastDateSync, 'timeUpd' => $lastDateSync
                ])
                    as $customerFromErp) {*/
                $customerFromErp = sqlsrv_query($conn,'SELECT RTRIM(LTRIM(Descrizione)) AS business_name, RTRIM(LTRIM(PartitaIva)) AS vat_number, RTRIM(LTRIM(CodiceFiscale)) AS fiscal_code, RTRIM(LTRIM(Indirizzo)) AS address, RTRIM(LTRIM(Cd_CF)) AS id_erp, RTRIM(LTRIM(Cd_DOVettore)) AS carrier_code, RTRIM(LTRIM(Cd_DOPorto)) AS carriage_code, RTRIM(LTRIM(Cd_DOSped)) AS shipment_code_sixten_code FROM xbtvw_sixtenlogis_CFCli WHERE TimeIns >= CONVERT(SMALLDATETIME, \'' . $lastDateSync . '\', 101) OR TimeUpd >= CONVERT(SMALLDATETIME,  \'' . $lastDateSync . '\', 101)');
                while( $row = sqlsrv_fetch_array( $customerFromErp, SQLSRV_FETCH_ASSOC)) {
                    try {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio analisi CUSTOMER - ' . $row['id_erp'] . '   ###');
                        $customer = Customer::where('erp_id', $row['id_erp'])->first();

                        if (is_null($customer)) {
                            $customer = new Customer();
                            $customer->created_id = $idUser;
                        } else {
                            $customer->updated_id = $idUser;
                        }

                        $customer->company = true;
                        $customer->business_name = utf8_encode($row['business_name']);
                        $customer->vat_number = $row['vat_number'];
                        $customer->fiscal_code = $row['fiscal_code'];
                        $customer->address = utf8_encode($row['address']);
                        $customer->erp_id = $row['id_erp'];
                        $carrier = Carrier::where('code', $row['carrier_code'])->first();
                        if (!is_null($carrier)) {
                            $customer->carrier_id = $carrier->id;
                        }
                        $carriage = Carriage::where('code', $row['carriage_code'])->first();
                        if (!is_null($carriage)) {
                            $customer->carriage_id = $carriage->id;
                        }
                        $customer->language_id = $idLanguage;
                        $customer->save();

                        $shipmentCodeSixten = ShipmentCodeSixten::where('erp_id', $row['shipment_code_sixten_code'])->first();

                        if (!is_null($shipmentCodeSixten)) {
                            $customerSixten = CustomerSixten::where('customer_id', $customer->id)->first();

                            if (is_null($customerSixten)) {
                                $customerSixten = new CustomerSixten();
                                $customerSixten->created_id = $idUser;
                            } else {
                                $customerSixten->updated_id = $idUser;
                            }
                            $customerSixten->customer_id = $customer->id;
                            $customerSixten->shipment_code_sixten_id = $shipmentCodeSixten->id;
                            $customerSixten->save();
                        }
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine analisi CUSTOMER - ' . $row['id_erp'] . '   ###');
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in salvataggio dei CUSTOMER', $customerFromErp, $ex);
                        $hasError = true;
                    }
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei CUSTOMER', null, $ex);
                $hasError = true;
            }

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine importazione CUSTOMER alle ' . Carbon::now() . '   ###');

            #endregion IMPORTAZIONE CUSTOMER

            #region IMPORTAZIONE ITEMS

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione ITEMS alle ' . Carbon::now() . '   ###');

            try {
                /*foreach (DB::connection('syncro')->select('SELECT LTRIM(RTRIM(Cd_AR)) AS internal_code, LTRIM(RTRIM(Cd_ARMisura)) AS unit_of_measure_code
                                                                , Altezza AS height, Larghezza AS width, Lunghezza AS depth, PesoNetto AS weight
                                                                , LTRIM(RTRIM(Descrizione)) AS description
                                                            FROM xbtvw_sixtenlogis_AR
                                                            WHERE TimeIns >= CONVERT(SMALLDATETIME, :timeIns, 101)
                                                            OR TimeUpd >= CONVERT(SMALLDATETIME, :timeUpd, 101)', [
                    'timeIns' => $lastDateSync, 'timeUpd' => $lastDateSync
                ])
                    as $itemFromErp) {*/
                $itemFromErp = sqlsrv_query($conn,'SELECT LTRIM(RTRIM(Cd_AR)) AS internal_code, LTRIM(RTRIM(Cd_ARMisura)) AS unit_of_measure_code, Altezza AS height, Larghezza AS width, Lunghezza AS depth, PesoNetto AS weight, LTRIM(RTRIM(Descrizione)) AS description FROM xbtvw_sixtenlogis_AR WHERE TimeIns >= CONVERT(SMALLDATETIME, \'' . $lastDateSync . '\', 101) OR TimeUpd >= CONVERT(SMALLDATETIME,  \'' . $lastDateSync . '\', 101)');
                while( $row = sqlsrv_fetch_array( $itemFromErp, SQLSRV_FETCH_ASSOC)) {
                    try {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio analisi ITEMS - ' . utf8_encode($row['internal_code']) . '   ###');
                        $item = Item::where('internal_code', utf8_encode($row['internal_code']))->first();

                        if (is_null($item)) {
                            $item = new Item();
                            $item->internal_code = utf8_encode($row['internal_code']);
                            $unitOfMeasure = UnitOfMeasure::where('code', $row['unit_of_measure_code'])->first();
                            if (!is_null($unitOfMeasure)) {
                                $item->unit_of_measure_id = $unitOfMeasure->id;
                            }
                            $item->height = $row['height'];
                            $item->width = $row['width'];
                            $item->depth = $row['depth'];
                            $item->weight = $row['weight'];
                            $item->created_id = $idUser;
                            $item->save();

                            $itemLanguage = new ItemLanguage();
                            $itemLanguage->item_id = $item->id;
                            $itemLanguage->language_id = $idLanguage;
                            $itemLanguage->description = utf8_encode($row['description']);
                            $itemLanguage->created_id = $idUser;
                            $itemLanguage->save();
                        } else {
                            $itemLanguage = ItemLanguage::where('item_id', $item->id)->first();
                            if (is_null($itemLanguage)) {
                                $itemLanguage = new ItemLanguage();
                                $itemLanguage->item_id = $item->id;
                                $itemLanguage->language_id = $idLanguage;
                                $itemLanguage->description = utf8_encode($row['description']);
                                $itemLanguage->created_id = $idUser;
                                $itemLanguage->save();
                            } else {
                                $itemLanguage->description = utf8_encode($row['description']);
                                $itemLanguage->updated_id = $idUser;
                                $itemLanguage->save();
                            }
                        }
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine analisi ITEMS - ' . utf8_encode($row['internal_code']) . '   ###');
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in salvataggio dei ITEMS', $itemFromErp, $ex);
                        $hasError = true;
                    }
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei ITEMS', null, $ex);
                $hasError = true;
            }

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine importazione ITEMS alle ' . Carbon::now() . '   ###');

            #endregion IMPORTAZIONE ITEMS

            #region IMPORTAZIONE DOCUMENTS HEADS OC

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione DOCUMENTS HEADS OC alle ' . Carbon::now() . '   ###');

            try {
                /*foreach (DB::connection('syncro')->select('SELECT LTRIM(RTRIM(Cd_CF)) AS customer_code, LTRIM(RTRIM(NumeroDocRif)) AS reference_code, DataDoc as date
                                                                , DataConsegna AS expected_shipping_date, LTRIM(RTRIM(Cd_VL)) AS currency_code
                                                                , LTRIM(RTRIM(Cd_VL)) AS carriage_code, LTRIM(RTRIM(Cd_DOPorto)) AS payment_type_code
                                                                , LTRIM(RTRIM(Cd_DoVettore_1)) AS carrier_code, LTRIM(RTRIM(NumeroDoc)) AS code
                                                                , Id_DoTes as erp_id
                                                            FROM xbtvw_sixtenlogis_TesteOrdini
                                                            WHERE Cd_DO = \'OC\'
                                                            AND RigheEvadibili > 0
                                                            AND (TimeIns >= CONVERT(SMALLDATETIME, :timeIns, 101)
                                                            OR TimeUpd >= CONVERT(SMALLDATETIME, :timeUpd, 101))', [
                    'timeIns' => $lastDateSync, 'timeUpd' => $lastDateSync
                ])
                    as $documentHeadFromErp) {*/
                $documentHeadFromErp = sqlsrv_query($conn,'SELECT LTRIM(RTRIM(Cd_CF)) AS customer_code, LTRIM(RTRIM(NumeroDocRif)) AS reference_code, DataDoc as date, DataConsegna AS expected_shipping_date, LTRIM(RTRIM(Cd_VL)) AS currency_code, LTRIM(RTRIM(Cd_VL)) AS carriage_code, LTRIM(RTRIM(Cd_DOPorto)) AS payment_type_code, LTRIM(RTRIM(Cd_DoVettore_1)) AS carrier_code, LTRIM(RTRIM(NumeroDoc)) AS code, Id_DoTes as erp_id FROM xbtvw_sixtenlogis_TesteOrdini WHERE Cd_DO = \'OC\' AND RigheEvadibili > 0 AND TimeIns >= CONVERT(SMALLDATETIME, \'' . $lastDateSync . '\', 101) OR TimeUpd >= CONVERT(SMALLDATETIME,  \'' . $lastDateSync . '\', 101)');
                while( $row = sqlsrv_fetch_array( $documentHeadFromErp, SQLSRV_FETCH_ASSOC)) {
                    try {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio analisi DOCUMENTS HEADS OC - ' . $row['erp_id'] . '   ###');
                        $documentHead = DocumentHead::where('erp_id', $row['erp_id'])->first();

                        if (is_null($documentHead)) {
                            $documentHead = new DocumentHead();
                            $documentHead->created_id = $idUser;
                        } else {
                            $documentHead->updated_id = $idUser;
                        }
                        $documentHead->customer_id = Customer::where('erp_id', $row['customer_code'])->first()->id;

                        $documentHead->document_type_id = $idDocumentTypeOrder;
                        $documentHead->reference_code = $row['reference_code'];
                        if (!is_null($row['expected_shipping_date'])) {
                            //$documentHead->expected_shipping_date = Carbon::createFromFormat('Y-m-d H:i:s', $row['expected_shipping_date'])->format(HttpHelper::getLanguageDateTimeFormat());
                            $documentHead->expected_shipping_date = $row['expected_shipping_date']->format(HttpHelper::getLanguageDateTimeFormat());
                        }

                        $currency = Currency::where('code', $row['currency_code'])->first();
                        if (!is_null($currency)) {
                            $documentHead->currency_id = $currency->id;
                        }

                        $carriage = Carriage::where('code', $row['carriage_code'])->first();
                        if (!is_null($carriage)) {
                            $documentHead->carriage_id = $carriage->id;
                        }

                        $paymentType = PaymentType::where('code', $row['payment_type_code'])->first();
                        if (!is_null($carriage)) {
                            $documentHead->payment_type_id = $paymentType->id;
                        }

                        $carrier = Carrier::where('code', $row['carrier_code'])->first();
                        if (!is_null($carrier)) {
                            $documentHead->carrier_id = $carrier->id;
                        }

                        $documentHead->code = $row['code'];
                        $documentHead->erp_id = $row['erp_id'];
                        $documentHead->save();
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in salvataggio dei DOCUMENTS HEADS OC', $documentHeadFromErp, $ex);
                        $hasError = true;
                    }
                    LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine analisi DOCUMENTS HEADS OC - ' . $row['erp_id'] . '   ###');
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei DOCUMENTS HEADS OC', null, $ex);
                $hasError = true;
            }

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine importazione DOCUMENTS HEADS OC alle ' . Carbon::now() . '   ###');

            #endregion IMPORTAZIONE DOCUMENTS HEADS OC

            #region IMPORTAZIONE DOCUMENTS ROWS

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione DOCUMENTS ROWS alle ' . Carbon::now() . '   ###');

            try {
                /*foreach (DB::connection('syncro')->select('SELECT Id_DOTes AS document_head_id, LTRIM(RTRIM(Cd_AR)) AS item_code
                                                                , LTRIM(RTRIM(Cd_ARMisura)) AS unit_of_measure_code, Qta AS quantity, Id_DORig AS erp_id
                                                                , LTRIM(RTRIM(Descrizione)) AS item_description, QtaEvadibile AS avoidable_quantity
                                                                , PrezzoUnitarioV as unit_price, PrezzoTotaleV as total_price
                                                            FROM xbtvw_sixtenlogis_RigheOrdini
                                                            WHERE Cd_AR IS NOT NULL
                                                            AND Id_DOTes IN (SELECT Id_DOTes
                                                                            FROM xbtvw_sixtenlogis_TesteOrdini
                                                                            WHERE Cd_DO = \'OC\'
                                                                            AND RigheEvadibili > 0
                                                                            AND (TimeIns >= CONVERT(SMALLDATETIME, :timeIns, 101)
                                                                            OR TimeUpd >= CONVERT(SMALLDATETIME, :timeUpd, 101)))', [
                    'timeIns' => $lastDateSync, 'timeUpd' => $lastDateSync
                ])
                    as $documentRowFromErp) {*/
                $documentRowFromErp = sqlsrv_query($conn,'SELECT Id_DOTes AS document_head_id, LTRIM(RTRIM(Cd_AR)) AS item_code , LTRIM(RTRIM(Cd_ARMisura)) AS unit_of_measure_code, Qta AS quantity, Id_DORig AS erp_id , LTRIM(RTRIM(Descrizione)) AS item_description, QtaEvadibile AS avoidable_quantity , PrezzoUnitarioV as unit_price, PrezzoTotaleV as total_price FROM xbtvw_sixtenlogis_RigheOrdini WHERE Cd_AR IS NOT NULL AND Id_DOTes IN (SELECT Id_DOTes FROM xbtvw_sixtenlogis_TesteOrdini WHERE Cd_DO = \'OC\' AND RigheEvadibili > 0  AND (TimeIns >= CONVERT(SMALLDATETIME, \'' . $lastDateSync . '\', 101) OR TimeUpd >= CONVERT(SMALLDATETIME, \'' . $lastDateSync . '\', 101)))');
                while( $row = sqlsrv_fetch_array( $documentRowFromErp, SQLSRV_FETCH_ASSOC)) {
                    try {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio analisi DOCUMENTS ROWS - ' . $row['document_head_id'] . '   ###');
                        $documentRow = DocumentRow::where('erp_id', $row['erp_id'])->first();

                        if (is_null($documentRow)) {
                            $documentRow = new DocumentRow();
                            $documentRow->created_id = $idUser;
                        } else {
                            $documentRow->updated_id = $idUser;
                        }

                        $documentRow->document_head_id = DocumentHead::where('erp_id', $row['document_head_id'])->first()->id;
                        $documentRow->document_row_type_id = $idDocumentRowTypeItem;
                        $documentRow->item_id = Item::where('internal_code', utf8_encode($row['item_code']))->first()->id;
                        $documentRow->internal_code = utf8_encode($row['item_code']);
                        $documentRow->unit_price = $row['unit_price'];
                        $documentRow->total_price = $row['total_price'];

                        $unitOfMeasure = UnitOfMeasure::where('code', $row['unit_of_measure_code'])->first();
                        if (!is_null($unitOfMeasure)) {
                            $documentRow->unit_of_measure_id = $unitOfMeasure->id;
                        }

                        $documentRow->quantity = round($row['quantity']);
                        $documentRow->picked_quantity = round($row['quantity']) - round($row['avoidable_quantity']);
                        $documentRow->erp_id = $row['erp_id'];
                        $documentRow->save();

                        $documentRowLanguage = DocumentRowLanguage::where('document_row_id', $documentRow->id)->first();

                        if (is_null($documentRowLanguage)) {
                            $documentRowLanguage = new DocumentRowLanguage();
                            $documentRowLanguage->created_id = $idUser;
                        } else {
                            $documentRowLanguage->updated_id = $idUser;
                        }
                        $documentRowLanguage->document_row_id = $documentRow->id;
                        $documentRowLanguage->language_id = $idLanguage;
                        if ($row['item_code'] == '.') {
                            $documentRowLanguage->description = $row['item_description'];
                        } else {
                            $documentRowLanguage->description = ItemLanguageBL::getByItemAndLanguage($documentRow->item_id, $idLanguage);
                        }
                        $documentRowLanguage->save();
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in salvataggio dei DOCUMENTS ROWS', $documentRowFromErp, $ex);
                        $hasError = true;
                    }
                    LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine analisi DOCUMENTS ROWS - ' . $row['item_code'] . '   ###');
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei DOCUMENTS ROWS', null, $ex);
                $hasError = true;
            }

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine importazione DOCUMENTS ROWS alle ' . Carbon::now() . '   ###');

            #endregion IMPORTAZIONE DOCUMENTS ROWS

            #endregion IMPORTAZIONE

            #region ESPORTAZIONE

            #region DOCUMENT HEAD PB NOT SHIPPED

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione DOCUMENT HEAD PB NOT SHIPPED alle ' . Carbon::now() . '   ###');

            try {
                $idDocumentType = DocumentType::where('code', DocumentTypeEnum::BeforeDDT)->first()->id;

                foreach (DocumentHead::where('document_type_id', $idDocumentType)->whereNull('erp_id')->whereNotNull('expected_shipping_date')->get() as $documentHead) {
                    try {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio analisi importazione DOCUMENT HEAD PB NOT SHIPPED - ' . $documentHead->customer_id . '   ###');
                        $customer_code = Customer::find($documentHead->customer_id)->erp_id;

                        /*$documentHeadFromErp = DB::connection('syncro')->select(
                            'SELECT Id_DoTes as erp_id FROM xbtvw_sixtenlogis_TesteOrdini WHERE Cd_CF = :cd_cf AND DataConsegna = :data_consegna
                            AND Cd_DO = \'PB\'',
                            ['cd_cf' => $customer_code, 'data_consegna' => $documentHead->expected_shipping_date]
                        );*/

                        $documentHeadFromErp = sqlsrv_query($conn,'SELECT Id_DoTes as erp_id FROM xbtvw_sixtenlogis_TesteOrdini WHERE Cd_CF = \'' . $customer_code . '\' AND DataConsegna = \'' . $documentHead->expected_shipping_date . '\' AND Cd_DO = \'PB\'');
                        $row_count = sqlsrv_num_rows($documentHeadFromErp);
                        if ($row_count === false) {
                            //$resultInsertDH = DB::connection('syncro')->insert(
                            //    'INSERT INTO DOTes (Cd_DO, TipoDocumento, Cd_CF, CliFor, NumeroDoc, DataDoc, Cd_MGEsercizio, DataConsegna
                            //                    , EsAnno, UserIns, TimeIns)
                            //    values (\'PB\', \'B\', :cd_cf, \'C\'
                            //    , ISNULL((SELECT MAX(NumeroDoc) + 1 FROM DOTes WHERE Cd_DO = \'PB\' AND Cd_MGEsercizio = :cd_mg_esercizio_num_doc), 1)
                            //    , :data_doc, :cd_mg_esercizio, :data_consegna, :es_anno, (SELECT Id_UserInfo FROM UserInfo WHERE SUserSName = \'sa\')
                            //    , CONVERT(SMALLDATETIME, :time_ins, 101))',
                            //    [
                            //        'cd_cf' => $customer_code, 'cd_mg_esercizio_num_doc' => Carbon::now()->year, 'data_doc' => $documentHead->date,
                            //        'cd_mg_esercizio' => Carbon::now()->year, 'data_consegna' => $documentHead->expected_shipping_date,
                            //        'es_anno' => Carbon::now()->year, 'time_ins' => Carbon::now()
                            //    ]
                            //);

                            //$resultInsertDH = DB::connection('syncro')->insert(
                            //    'INSERT INTO xbt_sixtenlogis_UPDatePREbolle_Dotes (id_dotes, Colli, PesoNetto)
                            //    values (ISNULL((SELECT MAX(NumeroDoc) + 1 FROM xbtvw_sixtenlogis_TesteOrdini WHERE Cd_DO = \'PB\' AND Cd_MGEsercizio = :cd_mg_esercizio_num_doc), 1)
                            //    ,0
                            //    ,1)',
                            //    [
                            //        'cd_mg_esercizio_num_doc' => Carbon::now()->year
                            //    ]
                            //);
                            $resultInsertDH = sqlsrv_query($conn,'INSERT INTO xbt_sixtenlogis_UPDatePREbolle_Dotes (id_dotes, Colli, PesoNetto) values (ISNULL((SELECT MAX(NumeroDoc) + 1 FROM xbtvw_sixtenlogis_TesteOrdini WHERE Cd_DO = \'PB\' AND Cd_MGEsercizio = ' . Carbon::now()->year . '), 1),0,1); SELECT SCOPE_IDENTITY() AS id');

                            if ($resultInsertDH === true) {
                                //$newId = DB::connection('syncro')->getPdo()->lastInsertId();
                                sqlsrv_next_result($resultInsertDH);
                                sqlsrv_fetch($resultInsertDH);
                                $newId = sqlsrv_get_field($resultInsertDH, 0);
                                $documentHead->erp_id = $newId;
                                $documentHead->updated_id = $idUser;
                                $documentHead->save();
                            }
                        } else {
                            $objDocumentHead = sqlsrv_fetch_object($documentHeadFromErp);
                            $documentHead->erp_id = $objDocumentHead[0]->erp_id;
                            $documentHead->updated_id = $idUser;
                            $documentHead->save();
                        }
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei DOCUMENTS HEAD PB NOT SHIPPED', $documentHead, $ex);
                        $hasError = true;
                    }
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei DOCUMENTS HEAD PB NOT SHIPPED', null, $ex);
                $hasError = true;
            }

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine importazione DOCUMENT HEAD PB NOT SHIPPED alle ' . Carbon::now() . '   ###');

            #endregion DOCUMENT HEAD PB NOT SHIPPED

            #region DOCUMENT ROW PB NOT SHIPPED

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione DOCUMENT ROW PB NOT SHIPPED alle ' . Carbon::now() . '   ###');

            try {
                foreach (DocumentRow::whereNull('erp_id')->get() as $documentRow) {
                    try {
                        $documentHead = DocumentHead::find($documentRow->document_head_id);
                        $documentRowOC = DocumentRow::find($documentRow->document_row_reference);

                        /*$documentRowFromErp = DB::connection('syncro')->select(
                            'SELECT Id_DORig AS erp_id FROM xbtvw_sixtenlogis_RigheOrdini WHERE Id_DOTes = :id_dotes AND Cd_AR = :cd_ar AND Qta = :qta',
                            ['id_dotes' >= $documentHead->erp_id, 'cd_ar' => $documentRow->interna_code, 'qta' => $documentRow->quantity]
                        );*/
                        
                        $documentRowFromErp = sqlsrv_query($conn,'SELECT Id_DORig AS erp_id FROM xbtvw_sixtenlogis_RigheOrdini WHERE Id_DOTes = \'' . $documentHead->erp_id . '\' AND Cd_AR = \'' . utf8_encode($documentRow->internal_code) . '\' AND Qta = ' . $documentRow->quantity);
                        $row_count = sqlsrv_num_rows($documentRowFromErp);
                        if ($row_count === false) {
                        
                            $item = Item::find(utf8_encode($documentRow->item_id));
                            $unitOfMeasure = UnitOfMeasure::find($item->unit_of_measure_id);
                            $unitOfMeasureCode = 'NR';
                            if (!is_null($unitOfMeasure)) {
                                $unitOfMeasureCode = $unitOfMeasure->code;
                            }
                            $quantitaEvadibile = $documentRow->quantity;
                            if (!is_null($documentRow->picked_quantity)) {
                                $quantitaEvadibile = $documentRow->quantity - $documentRow->picked_quantity;
                            }

                            //$resultInsertDR = DB::connection('syncro')->insert(
                            //    'INSERT INTO DORig (Id_DOTes, Contabile, NumeroDoc, DataDoc, Cd_MGEsercizio, Cd_DO, TipoDocumento, Cd_CF, Cd_LS_C, Cd_VL, Cambio
                            //                        , Decimali, DecimaliPrzUn, Riga, Cd_MGCausale, Cd_MG_P, Cd_AR, Descrizione, Cd_ARMisura, Qta, QtaEvasa
                            //                        , Id_DORig_Evade, PrezzoUnitarioV, PrezzoTotaleV, Cd_Aliquota
                            //                        , UserIns, TimeIns)
                            //    SELECT Id_DOTes, Contabile, NumeroDoc, DataDoc, Cd_MGEsercizio, Cd_DO, TipoDocumento, Cd_CF, Cd_LS_C, Cd_VL, Cambio, Decimali
                            //                , DecimaliPrzUn, ISNULL((SELECT MAX(Riga) + 1 FROM DORig WHERE Id_DoTes = :id_dotes_riga), 1), Cd_MGCausale, \'00001\'
                            //                , :cd_ar, :descrizione, :cd_armisura, :qta, :qta_evasa, :id_dorig_evade, :prezzo_unitario_v, :prezzo_totale_v, 22
                            //                , (SELECT Id_UserInfo FROM UserInfo WHERE SUserSName = \'sa\'), CONVERT(SMALLDATETIME, :time_ins, 101)
                            //     FROM DOTes WHERE Id_DOTes = :id_dotes',
                            //    [
                            //        'id_dotes_riga'  => $documentHead->erp_id, 'id_dotes' => $documentHead->erp_id, 'cd_ar' => $documentRow->internal_code,
                            //        'descrizione' => DocumentRowLanguageBL::getByDocumentRowAndLanguage($documentRow->id, $idLanguage),
                            //        'cd_armisura' => $unitOfMeasureCode, 'qta' => $documentRow->quantity, 'qta_evasa' => $documentRow->picked_quantity,
                            //        'id_dorig_evade' => $documentRowOC->erp_id, 'prezzo_unitario_v' => $documentRowOC->unit_price, x
                            //        'prezzo_totale_v' => $documentRowOC->unit_price * $documentRow->quantity, 'time_ins' => Carbon::now()
                            //    ]
                            //);

                            /*$resultInsertDR = DB::connection('syncro')->insert(
                                'INSERT xbt_sixtenlogis_PREbolle_DORig (Riga, Cd_AR, Descrizione, CD_ARMisura, Qta, QtaEvadibile, Id_DORig_Evade)
                                            ISNULL((SELECT MAX(Riga) + 1 FROM xbtvw_sixtenlogis_RigheOrdini WHERE Id_DoTes = :id_dotes_riga), 1),
                                            , :cd_ar, :descrizione, :cd_armisura, :qta, :qta_evasa, :id_dorig_evade',
                                [
                                    'id_dotes_riga'  => $documentHead->erp_id, 'cd_ar' => $documentRow->internal_code,
                                    'descrizione' => DocumentRowLanguageBL::getByDocumentRowAndLanguage($documentRow->id, $idLanguage),
                                    'cd_armisura' => $unitOfMeasureCode, 'qta' => $documentRow->quantity, 'qta_evasa' => $documentRow->picked_quantity,
                                    'id_dorig_evade' => $documentRowOC->erp_id                                    
                                ]
                            );*/

                            $resultInsertDR = sqlsrv_query($conn,'INSERT INTO xbt_sixtenlogis_PREbolle_DORig (Riga, Cd_AR, Descrizione, CD_ARMisura, Qta, QtaEvadibile, Id_DORig_Evade) VALUES (ISNULL((SELECT MAX(Riga) + 1 FROM xbtvw_sixtenlogis_RigheOrdini WHERE Id_DoTes = \'' . $documentHead->erp_id . '\'), 1), \'' . utf8_encode($documentRow->internal_code) . '\', ' . DocumentRowLanguageBL::getByDocumentRowAndLanguage($documentRow->id, $idLanguage) . ', \'' . $unitOfMeasureCode . '\', ' . $documentRow->quantity . ', ' . $documentRow->picked_quantity . ', \'' . $documentRowOC->erp_id . '\'); SELECT SCOPE_IDENTITY() AS id');

                            if ($resultInsertDR === true) {
                                //$newId = DB::connection('syncro')->getPdo()->lastInsertId();
                                sqlsrv_next_result($resultInsertDR);
                                sqlsrv_fetch($resultInsertDR);
                                $newId = sqlsrv_get_field($resultInsertDR, 0);
                                $documentRow->erp_id = $newId;
                                $documentRow->updated_id = $idUser;
                                $documentRow->save();

                                //DB::connection('syncro')->update(
                                //    'UPDATE DORig SET QtaEvasa = QtaEvasa - :qta_evasa WHERE Id_DORig = :id_dorig',
                                //   ['qta_evasa' => $quantitaEvadibile, 'id_dorig' => $documentRowOC->erp_id]
                                //);
                                // DB::connection('syncro')->update(
                                //     'UPDATE DOTes SET Righe = (SELECT COUNT(*) FROM DORig WHERE Id_DOTes = :id_dotes_rig) WHERE Id_DOTes = :id_dotes',
                                //     ['id_dotes_rig' => $documentHead->erp_id, 'id_dotes' => $documentHead->erp_id]
                                // );
                            }
                        } else {
                            $objDocumentRow = sqlsrv_fetch_object($documentRowFromErp);
                            $documentRow->erp_id = $objDocumentRow[0]->erp_id;
                            $documentRow->updated_id = $idUser;
                            $documentRow->save();

                            //DB::connection('syncro')->update(
                            //    'UPDATE DORig SET QtaEvasa = QtaEvasa - :qta_evasa WHERE Id_DORig = :id_dorig',
                            //    ['qta_evasa' => $documentRowFromErp[0]->qta_evasa, 'id_dorig' => $documentRowOC->erp_id]
                            //);

                            // DB::connection('syncro')->update(
                            //     'UPDATE DOTes SET Righe = (SELECT COUNT(*) FROM DORig WHERE Id_DOTes = :id_dotes_rig)
                            //     WHERE Id_DOTes = :id_dotes',
                            //     ['id_dotes_rig' => $documentRowFromErp[0]->Id_DOTes, 'id_dotes' => $documentRowFromErp[0]->Id_DOTes]
                            // );
                        }
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei DOCUMENTS ROW PB NOT SHIPPED', $documentRowFromErp, $ex);
                        $hasError = true;
                    }
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei DOCUMENTS ROW PB NOT SHIPPED', null, $ex);
                $hasError = true;
            }

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine importazione DOCUMENT ROW PB NOT SHIPPED alle ' . Carbon::now() . '   ###');

            #endregion DOCUMENT ROW PB NOT SHIPPED

            #region DOCUMENT HEAD PB SHIPPED

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Inizio importazione DOCUMENT HEAD PB SHIPPED alle ' . Carbon::now() . '   ###');

            try {
                $idDocumentType = DocumentType::where('code', DocumentTypeEnum::BeforeDDT)->first()->id;

                foreach (DocumentHead::where('document_type_id', $idDocumentType)->whereNotNull('erp_id')->whereNotNull('shipping_date')->whereNull('code')->get() as $documentHead) {
                    try {
                        //$documentHeadFromErp = DB::connection('syncro')->select('SELECT * FROM xbt_sixtenlogis_UPDatePREbolle_Dotes WHERE id_dotes = :id_dotes', ['id_dotes' => $documentHead->erp_id]);

                        $querydocumentHeadFromErp = sqlsrv_query($conn,'SELECT * FROM xbt_sixtenlogis_UPDatePREbolle_Dotes WHERE id_dotes =\'' . $documentHead->erp_id . '\'');
                        $documentHeadFromErp = sqlsrv_fetch_object($querydocumentHeadFromErp);
                        if (is_null($documentHeadFromErp[0]->DataConsegna)) {
                            $carriage = Carriage::find($documentHead->carriage_id);
                            $carrier = Carrier::find($documentHead->carrier_id);
                            $paymentType = PaymentType::find($documentHead->payment_type_id);
                            $documentHeadSixten = DocumentHeadSixten::where('document_head_id', $documentHead->id)->first();
                            $carriageCode = '';
                            $carrierCode = '';
                            $paymentTypeCode = '';

                            if (!is_null($carriage)) {
                                $carriageCode = $carriage->code;
                            }

                            if (!is_null($carrier)) {
                                $carrierCode = $carrier->code;
                            }

                            if (!is_null($paymentType)) {
                                $paymentTypeCode = $paymentType->code;
                            }

                            /*$resultUpdateDH = DB::connection('syncro')->update(
                                'UPDATE xbt_sixtenlogis_UPDatePREbolle_Dotes SET Colli = :colli, PesoNetto = :peso_netto
                                WHERE id_dotes = :id_dotes',
                                [
                                    'colli' => $documentHeadSixten->packagingNumber,
                                    'peso_netto' => $documentHead->weight,
                                    'id_dotes' => $documentHead->erp_id
                                ]
                            );*/

                            $resultUpdateDH = sqlsrv_query($conn,'UPDATE xbt_sixtenlogis_UPDatePREbolle_Dotes SET Colli = ' . $documentHeadSixten->packagingNumber . ', PesoNetto = ' . $documentHead->weight . ' WHERE id_dotes = \'' . $documentHead->erp_id . '\'');

                            if ($resultUpdateDH === true) {
                                $documentHead->code = $documentHeadFromErp[0]->code;
                                $documentHead->updated_id = $idUser;
                                $documentHead->save();

                                //DB::connection('syncro')->update(
                                //    'UPDATE DORig SET DataConsegna = CONVERT(SMALLDATETIME, :data_consegna, 101)
                                //                    , UserUpd = (SELECT Id_UserInfo FROM UserInfo WHERE SUserSName = \'sa\')
                                //                    , TimeUpd = CONVERT(SMALLDATETIME, :time_upd, 101)
                                //    WHERE Id_DOTes = :id_do_test',
                                //    ['data_consegna' => $documentHead->shipping_date, 'time_upd' => Carbon::now(), 'id_do_test', $documentHead->erp_id]
                                //);
                            }
                        } else {
                            $documentHead->code = $documentHeadFromErp[0]->code;
                            $documentHead->updated_id = $idUser;
                            $documentHead->save();

                            //DB::connection('syncro')->update(
                            //    'UPDATE DORig SET DataConsegna = CONVERT(SMALLDATETIME, :data_consegna, 101) , UserUpd = (SELECT Id_UserInfo FROM UserInfo WHERE SUserSName = \'sa\')
                            //    , TimeUpd = CONVERT(SMALLDATETIME, :time_upd, 101)
                            //    WHERE Id_DOTes = :id_do_test',
                            //    ['data_consegna' => $documentHead->shipping_date, 'time_upd' => Carbon::now(), 'id_do_test', $documentHead->erp_id]
                            //);
                        }
                    } catch (\Throwable $ex) {
                        LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei DOCUMENTS HEAD PB SHIPPED', $documentHeadFromErp, $ex);
                        $hasError = true;
                    }
                }
            } catch (\Throwable $ex) {
                LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore in lettura dei DOCUMENTS HEAD PB SHIPPED', null, $ex);
                $hasError = true;
            }

            LogSyncroHelper::log($startDateSyncro, $syncroTitle, '###   Fine importazione DOCUMENT HEAD PB SHIPPED alle ' . Carbon::now() . '   ###');

            #endregion DOCUMENT HEAD PB SHIPPED

            #endregion ESPORTAZIONE

            LogSyncroHelper::endLog($startDateSyncro, $syncroTitle);
        } catch (\Throwable $ex) {
            LogSyncroHelper::log($startDateSyncro, $syncroTitle, 'Errore', null, $ex);
            $hasError = true;
        }

        if ($hasError) {
            LogSyncroHelper::send($startDateSyncro, $syncroTitle);
        } else {
            $setting = Setting::where('code', SettingEnum::LastDateSync)->first();
            $setting->value = $startDateSyncroToSave;
            $setting->updated_id = $idUser;
            $setting->save();
        }

        $fileToDelete = storage_path() . '/logs/syncro_' . $syncroTitle . '_' . Carbon::createFromFormat('Y-m-d H:i:s', $startDateSyncroToSave)->subDays(3)->format('Y-m-d_H-i') . '.log';
        if (file_exists($fileToDelete)) {
            unlink($fileToDelete);
        }
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\GeneratorCommand;

class MakeModelLogCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'make:modellog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new model log';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/command.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\ModelsLogs';
    }
}

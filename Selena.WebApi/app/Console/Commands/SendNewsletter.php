<?php

namespace App\Console\Commands;

use App\BusinessLogic\ContactBL;
use App\BusinessLogic\CustomerBL;
use App\Enums\FeedbackNewsletterEnum;
use App\FeedbackNewsletter;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Helpers\LogHelperNewsletter;

use App\Helpers\NewsletterHelper;
use App\Newsletter;
use App\NewsletterRecipient;
use App\Setting;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Console\StorageLinkCommand;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class SendNewsletter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:newsletter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Newsletter';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $idUser = User::where('name', 'newsletter')->first()->id;
            $emailSent = 0;

            $newsletter = Newsletter::where('schedule_date_time', '<=', Carbon::now())->where('draft', false)->where('start_send_date_time', NULL)->first();
            if (isset($newsletter)) {
                $newsletter->start_send_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
                //$newsletter->start_send_date_time = Carbon::now()->format('Y-m-d H:i:s');
                $newsletter->updated_id = $idUser;
                $newsletter->save();

                 $cssEmail = Storage::disk('public')->get('\css\webCssEmail.css'); 

                #region READ RECIPIENT TO SENT

                foreach (DB::select('SELECT nr.id, nr.name, nr.email
                                    FROM newsletters_recipients nr
                                    LEFT JOIN feedback_newsletters fn ON nr.id = fn.newsletter_recipient_id
                                    WHERE nr.newsletter_id = :idNewsletter
                                    AND fn.id IS NULL', ['idNewsletter' => $newsletter->id]) as $newslettersRecipients) {



                    $feedbackNewsletter = new FeedbackNewsletter();
                    $feedbackNewsletter->newsletter_recipient_id = $newslettersRecipients->id;

                    // Check if the email is valid
                    if (!filter_var($newslettersRecipients->email, FILTER_VALIDATE_EMAIL)) {
                        /*LogHelperNewsletter::error('Errore invio Newsletter ' . $newsletter->title . ' <' . $newslettersRecipients->email . '>\n\n Email non valida');*/
                        $feedbackNewsletter->code = FeedbackNewsletterEnum::Error;
                        $feedbackNewsletter->save();
                    } else {
                        // Every 50 email, wait 1 second
                     
                        $parameterNewsletterBlockDelay = 'NewsletterBlockDelay';  
                        $NewsletterBlockDelay = Setting::where('code', $parameterNewsletterBlockDelay)->first()->value;

                        $parameterNewsletterBlockItems = 'NewsletterBlockItems';  
                        $NewsletterBlockItems = Setting::where('code', $parameterNewsletterBlockItems)->first()->value;
                        
                        if ($emailSent == $NewsletterBlockItems) {
                            sleep($NewsletterBlockDelay);
                            $emailSent = 0;
                        }

                        if(is_null($newsletter->preview_object) || $newsletter->preview_object == ''){
                            $previewObject = '';
                        }else{
                            $previewObject = $newsletter->preview_object;
                        }

                        try {
                            NewsletterHelper::send(
                                $newsletter->sender_email,
                                $newslettersRecipients->email,
                                $newsletter->title,
                                '<p style="display:none;">' . $newsletter->preview_object .  '</p>' . $newsletter->html,
                                $newsletter->object,
                                $newsletter->sender,
                                $previewObject,
                                $cssEmail,
                                $newslettersRecipients->id
                            );
                       

                            $feedbackNewsletter->code = FeedbackNewsletterEnum::Sent;
                        } catch (\Throwable $ex) {
                            /*LogHelperNewsletter::error('Errore invio Newsletter ' . $newsletter->description . ' <' . $newslettersRecipients->email . '>\n\n ' . $ex);*/
                            
                            CustomerBL::setNewsletterError($newslettersRecipients->email);
                            ContactBL::setNewsletterError($newslettersRecipients->email);
                            $feedbackNewsletter->code = FeedbackNewsletterEnum::Error;
                        }
                       
                               // config('mail.username_newsletter'), 

                        $feedbackNewsletter->save();

                        $emailSent += 1;
                        
                    }
                }

                #endregion READ RECIPIENT TO SENT
                $newsletter->end_send_date_time = Carbon::parse(Carbon::now())->format(HttpHelper::getLanguageDateTimeFormat());
                $newsletter->updated_id = $idUser;
                $newsletter->save();
            }
        } catch (\Throwable $ex) {
           /*LogHelper::error('Errore invio Newsletter\n\n' . $ex);*/
        }
    }
}

            
       

<?php

namespace App\Console\Commands;

use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;

use App\Scheduler;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Console\StorageLinkCommand;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class SyncroScheduler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'syncro:scheduler';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Syncro scheduler';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();  
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $resultMsg = '';
        $scheduler = Scheduler::where('start_time_action', '<=', Carbon::now())->where('next_run', '<=', Carbon::now())->whereNotNull('next_run')->where('active', true)->get();
        if (isset($scheduler)) {
            foreach ($scheduler as $schedulerAll) {
                try {
                    if(is_null($schedulerAll->repeat_left) || $schedulerAll->repeat_left > 0){
                        $return = DB::statement($schedulerAll->action);

                        if($return == 1){
                            //logica di controllo tipologia di schedulazione e ripetizione per impostare la data in next_run
                            switch ($schedulerAll->schedule) {
                                case "O":
                                    // --- casistica ONCE, pulire campo di next_run in quanto deve essere eseguita solo una volta
                                    $schedulerAll->next_run = null;
                                    break;
                                case "D": 
                                    // --- casistica daily, inserire la prima data disponibile tra quelle presente negli orari di ripetizione
                                    
                                    //prelievo variabili di ogni ora e minuti da aggiungere alla data di next_run, così da autoaggiornarsi
                                    $repeatEveryHour = 0;
                                    $repeatEveryMinutes = 0;
                                    $startTimeAction = $schedulerAll->next_run;

                                    if(!is_null($schedulerAll->repeat_after_hour) && $schedulerAll->repeat_after_hour > 0){
                                        $repeatEveryHour = $schedulerAll->repeat_after_hour;
                                    }
                                    if(!is_null($schedulerAll->repeat_after_min) && $schedulerAll->repeat_after_min > 0){
                                        $repeatEveryMinutes = $schedulerAll->repeat_after_min;
                                    }
                                    
                                    //aggiunta parametri alla data
                                    $nextRunCalculation = date('Y-m-d H:i:s', strtotime('+' . $repeatEveryHour . ' hour +' . $repeatEveryMinutes . ' minutes', strtotime($startTimeAction)));
                                    
                                    //salvataggio della prossima schedulazione
                                    $schedulerAll->next_run = $nextRunCalculation;

                                    break;
                                case "W":
                                    // --- casistica weekly, inserire la prima data disponibile tra quelle presente nei giorni del campo frequency
                                    //0=sunday,1=monday...,6=saturday
                                    if(!is_null($schedulerAll->frequency) && $schedulerAll->frequency != ''){
                                        $dailyFrequency = $schedulerAll->frequency;
                                        $startTimeAction = $schedulerAll->next_run;
                                        $startTimeActionHour = date('H', strtotime($schedulerAll->next_run));
                                        $startTimeActionMinutes = date('i', strtotime($schedulerAll->next_run));
                                        $foundDay = '';
                                        //prelievo il giorno della settimana della data di next_run
                                        $dayOfWeekNext = date('w', strtotime($startTimeAction));

                                        //controllo se sono presenti più giorni della settimana
                                        $checkGiorniMultipli = strpos($dailyFrequency, ',');
                                        if ($checkGiorniMultipli !== false) {
                                            //controllare il giorno della settimana seguente, tramite l'array di giorni nel campo di frequenza
                                            $arrayOfFrequency = explode(",", $dailyFrequency);
                                            //ordino l'array in quanto non sappiamo se i giorni nel campo sono in ordine o sparsi
                                            asort($arrayOfFrequency);
                                            //attraverso il ciclo viene trovato il giorno della settimana successivo a quello della data di next_run 
                                            foreach($arrayOfFrequency as $arrayOfFrequencys) {
                                                if($arrayOfFrequencys != ''){
                                                    if($arrayOfFrequencys > $dayOfWeekNext){
                                                        $foundDay = $arrayOfFrequencys;
                                                        break;
                                                    }
                                                }
                                            }
                                            //se non viene trovato il giorno della settimana successivo diventa il primo in quanto deve riniziare da capo
                                            $dayOfWeekNext = $arrayOfFrequency[0];
                                            if($foundDay != ''){
                                                //saltrimenti il giorno viene settato con quanto trovato nell'array sopra
                                                $dayOfWeekNext = $foundDay;
                                            }
                                        }else{
                                            //impostare il giorno della settimana subito successivo con quanto presente nel campo di frequenza
                                            //converto il dato numerico con il giorno della settimana
                                            $dayOfWeekNext = $schedulerAll->frequency;
                                        }

                                        //casistica di conversione dato numerico in testuale per aggiornamento della data
                                        switch ($dayOfWeekNext) {
                                            case "0":
                                                $nextRunCalculation = date('Y-m-d H:i:s', strtotime("next sunday", strtotime($startTimeAction)));
                                            break;
                                            case "1":
                                                $nextRunCalculation = date('Y-m-d H:i:s', strtotime("next monday", strtotime($startTimeAction)));
                                            break;
                                            case "2":
                                                $nextRunCalculation = date('Y-m-d H:i:s', strtotime("next tuesday", strtotime($startTimeAction)));
                                            break;
                                            case "3":
                                                $nextRunCalculation = date('Y-m-d H:i:s', strtotime("next wednesday", strtotime($startTimeAction)));
                                            break;
                                            case "4":
                                                $nextRunCalculation = date('Y-m-d H:i:s', strtotime("next thursday", strtotime($startTimeAction)));
                                            break;
                                            case "5":
                                                $nextRunCalculation = date('Y-m-d H:i:s', strtotime("next friday", strtotime($startTimeAction)));
                                            break;
                                            case "6":
                                                $nextRunCalculation = date('Y-m-d H:i:s', strtotime("next saturday", strtotime($startTimeAction)));
                                            break;
                                        }
                                        $nextRunCalculation = date('Y-m-d H:i:s', strtotime('+' . $startTimeActionHour . ' hour +' . $startTimeActionMinutes . ' minutes', strtotime($nextRunCalculation)));
                                        $schedulerAll->next_run = $nextRunCalculation;

                                    }else{
                                        $resultMsg = 'Nessun giorno della settimana inserito!';
                                    }

                                    break;
                                case "M":
                                    // --- casistica monthly, inserire la prima data disponibile tra quelle presente nei campi di mese e giorni
                                    if(!is_null($schedulerAll->month) && $schedulerAll->month != ''){
                                        //settaggi variabili per la casistica
                                        $monthlyFrequency = $schedulerAll->month;
                                        $dayMonthlyFrequency = $schedulerAll->days_of_the_month;
                                        $startTimeAction = $schedulerAll->next_run;
                                        $startTimeActionHour = date('H', strtotime($schedulerAll->next_run));
                                        $startTimeActionMinutes = date('i', strtotime($schedulerAll->next_run));
                                        $foundDay = '';
                                        $foundMonth = '';

                                        //prelievo il giorno del mese della data di next_run
                                        $dayOfMonthNext = date('d', strtotime($startTimeAction));
                                        //prelievo il mese della data di next_run
                                        $monthNext = date('m', strtotime($startTimeAction));

                                        //controllo se sono presenti più giorni del mese impostati
                                        $checkGiorniMultipli = strpos($dayMonthlyFrequency, ',');
                                        if ($checkGiorniMultipli !== false) {
                                            $arrayOfDaysMonthFrequency = explode(",", $dayMonthlyFrequency);
                                            //ordino l'array in quanto non sappiamo se i giorni nel campo sono in ordine o sparsi
                                            asort($arrayOfDaysMonthFrequency);

                                            foreach($arrayOfDaysMonthFrequency as $arrayOfDaysMonthFrequencys) {
                                                if($arrayOfDaysMonthFrequencys != ''){
                                                    if($arrayOfDaysMonthFrequencys > $dayOfMonthNext){
                                                        $foundDay = $arrayOfDaysMonthFrequencys;
                                                        break;
                                                    }
                                                }
                                            }

                                            //setto il giorno con il valore nel primo campo dell'array così da avere già la casistica sulla data maggiore
                                            $dayOfMonthNext = $arrayOfDaysMonthFrequency[0];
                                            if($foundDay != ''){
                                                //saltrimenti il giorno viene settato con quanto trovato nell'array sopra
                                                $dayOfMonthNext = $foundDay;
                                            }

                                            $checkMesiMultipli = strpos($monthlyFrequency, ',');
                                            if ($checkMesiMultipli !== false) {
                                                $arrayOfMonthFrequency = explode(",", $monthlyFrequency);
                                                //ordino l'array in quanto non sappiamo se i giorni nel campo sono in ordine o sparsi
                                                asort($arrayOfMonthFrequency);

                                                foreach($arrayOfMonthFrequency as $arrayOfMonthFrequencys) {
                                                    if($arrayOfMonthFrequencys != ''){
                                                        if($arrayOfMonthFrequencys > $monthNext){
                                                            $foundMonth = $arrayOfMonthFrequencys;
                                                            break;
                                                        }
                                                    }
                                                }
                                                $monthNext = $arrayOfMonthFrequency[0];
                                                if($foundMonth != ''){
                                                    //saltrimenti il giorno viene settato con quanto trovato nell'array sopra
                                                    $monthNext = $foundMonth;
                                                }
                                                $monthNext = $monthNext;
                                            }else{
                                                $monthNext = $schedulerAll->month;
                                            }

                                        }else{
                                            $dayOfMonthNext = $schedulerAll->days_of_the_month;

                                            $checkMesiMultipli = strpos($monthlyFrequency, ',');
                                            if ($checkMesiMultipli !== false) {
                                                $arrayOfMonthFrequency = explode(",", $monthlyFrequency);
                                                //ordino l'array in quanto non sappiamo se i giorni nel campo sono in ordine o sparsi
                                                asort($arrayOfMonthFrequency);
                                                foreach($arrayOfMonthFrequency as $arrayOfMonthFrequencys) {
                                                    if($arrayOfMonthFrequencys != ''){
                                                        if($arrayOfMonthFrequencys > $monthNext){
                                                            $foundMonth = $arrayOfMonthFrequencys;
                                                            break;
                                                        }
                                                    }
                                                }
                                                $monthNext = $arrayOfMonthFrequency[0];
                                                if($foundMonth != ''){
                                                    //saltrimenti il giorno viene settato con quanto trovato nell'array sopra
                                                    $monthNext = $foundMonth;
                                                }
                                                $monthNext = $monthNext;
                                            }else{
                                                $monthNext = $schedulerAll->month;
                                            }
                                        }

                                        //codifica valori numerici con la stringa mensile
                                        switch ($monthNext) {
                                            case "1":
                                                $d = "Jan " . $dayOfMonthNext;
                                            break;
                                            case "2":
                                                $d = "Feb " . $dayOfMonthNext;
                                            break;
                                            case "3":
                                                $d = "Mar " . $dayOfMonthNext;
                                            break;
                                            case "4":
                                                $d = "Apr " . $dayOfMonthNext;
                                            break;
                                            case "5":
                                                $d = "May " . $dayOfMonthNext;
                                            break;
                                            case "6":
                                                $d = "Jun " . $dayOfMonthNext;
                                            break;
                                            case "7":
                                                $d = "Jul " . $dayOfMonthNext;
                                            break;
                                            case "8":
                                                $d = "Aug " . $dayOfMonthNext;
                                            break;
                                            case "9":
                                                $d = "Sep " . $dayOfMonthNext;
                                            break;
                                            case "10":
                                                $d = "Oct " . $dayOfMonthNext;
                                            break;
                                            case "11":
                                                $d = "Nov " . $dayOfMonthNext;
                                            break;
                                            case "12":
                                                $d = "Dec " . $dayOfMonthNext;
                                            break;
                                        }

                                        //controllo se la data che andrà impostata è più grande della data odierna così da sapere se aggiungere un anno oppure no
                                        $nextRunCalculation = date("Y-m-d H:i:s", (strtotime($d)>time())?strtotime($d):strtotime("$d +1 year"));
                                        //aggiunta delle ore e minuti così da mantenere lo stesso formato data
                                        $nextRunCalculation = date('Y-m-d H:i:s', strtotime('+' . $startTimeActionHour . ' hour +' . $startTimeActionMinutes . ' minutes', strtotime($nextRunCalculation)));
                                        $schedulerAll->next_run = $nextRunCalculation;
                                    }

                                    break;
                            }

                            $schedulerAll->status = 'Ok';
                        }else{
                            $schedulerAll->status = 'Error';
                        }

                        //controllo se il campo di ripetizione e valorizzato o maggiore di 0 così da eseguire correttamente la sottrazione
                        if(!is_null($schedulerAll->repeat_left) && $schedulerAll->repeat_left > 0){
                            $schedulerAll->repeat_left = $schedulerAll->repeat_left - 1;
                        }

                        $schedulerAll->last_run = Carbon::Now()->format('Y-m-d H:i:s');
                        $schedulerAll->save();
                    }else{
                        $resultMsg = 'Nessuna schedulazione programmata, per ripetizioni finite!';
                    }

                    LogHelper::debug($resultMsg);

                } catch (\Throwable $ex) {
                    LogHelper::debug($ex);
                    
                    //controllo se il campo di ripetizione e valorizzato o maggiore di 0 così da eseguire correttamente la sottrazione
                    if(!is_null($schedulerAll->repeat_left) && $schedulerAll->repeat_left > 0){
                        $schedulerAll->repeat_left = $schedulerAll->repeat_left - 1;
                    }
                    $schedulerAll->last_run = Carbon::Now()->format('Y-m-d H:i:s');
                    $schedulerAll->status = 'Error';
                    $schedulerAll->save();
                }
            }
        }else{
            LogHelper::debug('Nessuna schedulazione programmata!');
        } 
    }
}

            
       

<?php

namespace App\Console\Commands;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;
use App\Setting;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Console\StorageLinkCommand;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\BusinessLogic\AmazonBL;

class SyncroAmazonItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'syncro:amazonitems';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sincronizza prodotti Amazon';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            
            AmazonBL::syncroItemsViaAPI();
            //AmazonBL::sniffaCodici();

        } catch (\Throwable $ex) {
            LogHelper::error('' . $ex);
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;

class PaymentGr extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'gr_payments';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}

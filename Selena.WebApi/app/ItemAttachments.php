<?php

namespace App;

use App\Helpers\HttpHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ItemAttachments extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'items_attachments';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}

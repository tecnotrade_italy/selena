<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\LogHelper;
use App\Helpers\HttpHelper;

class Page extends Model
{
    use ObservantTrait;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['homepage'];

    #region DATE FORMAT FIELD

    /**
     * Get fixed_end with format
     */
    public function getFixedEndAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set fixed_end from client format date to server format
     */
    public function setFixedEndAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['fixed_end'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }else{
            $this->attributes['fixed_end'] = null;
        }
    }

    /**
     * Get visible_from with format
     */
    public function getVisibleFromAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set visible_from from client format date to server format
     */
    public function setVisibleFromAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['visible_from'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }else{
            $this->attributes['visible_from'] = null;
        }
    }

    #endregion DATE FORMAT FIELD
     /**
     * Get visible_end with format
     */
    public function getVisibleEndAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set enabled_to from client format date to server format
     */
    public function setVisibleEndAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['visible_end'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }else{
            $this->attributes['visible_end'] = null;
        }
    }

     #endregion DATE FORMAT FIELD
     /**
     * Get publish with format
     */
    public function getPublishAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set publish from client format date to server format
     */
    public function setPublishAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['publish'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }else{
            $this->attributes['publish'] = null;
        }
    }

}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoList
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * id
     */
    public $id;

    /**
     * code_list
     */
    public $code_list;


    /**
     * beginning_validity
     */
    public $beginning_validity;

    /**
     * end_validity
     */
    public $end_validity;

    /**
     * value_list
     */
    public $value_list;

    /**
     * descrizione
     */
    public $description;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoValueFormBuilderField
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->valueFatherFormBuilderField = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * value
     */
    public $value;

    /**
     * img
     */
    public $img;

    /**
     * order
     */
    public $order;

    /**
     * description
     */
    public $description;

    /**
     * base64
     */
    public $base64;

    /**
     * valueFatherFormBuilderField
     */
    public $valueFatherFormBuilderField;

     /**
     * default
     */
    public $default;
    


}

<?php

namespace App\DtoModel;
use App\Helpers\ModelHelper;

class DtoConnections
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        $this->listOptions = collect();
        $this->DetailAddDayHour = collect();
        
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }
    
    public $id;
    public $PlaceId;
    public $ServiceId;
    public $EmployeeId;
    public $NrPlace;
    public $ShowAvailabilityCalendar;
    public $FixedDate;
    public $DateFrom;
    public $StartTime;
    public $EndTime;
    public $place_id;
    public $service_id;
    public $employee_id;
    public $nr_place;
    public $show_availability_calendar;
    public $description;
    public $check_multiple;
    public $min_day;
    public $date;
    public $start_time_pm;
    public $end_time_pm;
}

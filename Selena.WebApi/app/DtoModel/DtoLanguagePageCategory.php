<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoLanguagePageCategory
{
    /**
     * Constructor
     */
    public function __construct()
    {}

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * id
     */
    public $id;

    /**
     * description
     */
    public $description;
    
    public $language_id;
    public $page_category_id;
    public $seo_title;
    public $seo_keyword;
    public $seo_description;
    public $share_title;
    public $share_description;
    public $url_cover_image;

    /**
     * active
     */
    public $active;


    

}

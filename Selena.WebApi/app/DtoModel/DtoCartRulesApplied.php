<?php

namespace App\DtoModel;

class DtoCartRulesApplied
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $name;
    public $description;
    public $voucher_code;
    public $voucher_type_id;
    public $availability;
    public $availability_residual;
    public $availability_per_user;
    public $priority;
    public $exclude_other_rules;
    public $online;
    public $enabled_from;
    public $enabled_to;
    public $apply_to_all_customers;
    public $apply_to_all_groups;
    public $apply_to_all_carriers;
    public $apply_to_all_items;
    public $apply_to_all_categories_items;
    public $minimum_quantity;
    public $maximum_quantity;
    public $minimum_order_amount;
    public $taxes_excluded;
    public $shipping_excluded;
    public $free_shipping;
    public $discount_type;
    public $discount_value;
    public $apply_discount_to;
    public $add_to_cart_automatically;
    
}

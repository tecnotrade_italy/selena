<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoMenuAdmin
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->nextLevel = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Id functionality
     */
    public $idFunctionality;

    /**
     * Label
     */
    public $label;

    /**
     * Css icon
     */
    public $cssIcon;

    /**
     * Url
     */
    public $url;

    /**
     * Next level
     */
    public $nextLevel;

    public $customMenu;
    public $description;
}
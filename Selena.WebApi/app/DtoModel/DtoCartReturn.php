<?php

namespace App\DtoModel;

class DtoCartReturn
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $cart_id;
    public $cart_detail_id;
    

}

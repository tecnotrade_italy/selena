<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoTableConfiguration
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->visibleOnTable = true;
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $label;

    public $field;

    public $formatter;

    public $required;

    public $defaultValue;

    public $service;

    public $maxLenght;

    public $inputType;

    public $posY;

    public $posX;

    public $tableOrder;

    public $class;

    public $attribute;

    public $colspan;

    public $idTab;

    public $tabCode;

    public $tabDescription;

    public $onChange;

    public $visibleOnTable;
}

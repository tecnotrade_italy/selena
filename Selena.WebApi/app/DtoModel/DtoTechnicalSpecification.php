<?php

namespace App\DtoModel;

class DtoTechnicalSpecification
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->languageTechnicalSpecification = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id functionality
     */
    public $idFunctionality;

    /**
     * Id
     */
    public $id;

    /**
     * Language technical specification
     */
    public $languageTechnicalSpecification;
}

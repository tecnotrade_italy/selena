<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoLogsDetail
{
    /**
     * Constructor
     */
    public function __construct()
    {
    
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    
    public $id;
    public $user_id;
    public $ip;
    public $action;
    public $description;
    public $people_id;
    public $created_at;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoItems
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->imgAgg = collect();
        $this->pricelist = collect();
        $this->related = collect();
        $this->imgAggAttachment = collect();
        $this->filesUpload = collect();
        $this->supports = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Internal code
     */
    public $internal_code;

    /**
     * Description
     */
    public $description;


    /**
     * Description
     */
    public $descriptionCategories;


    /**
     * unit_of_measure_id
     */
    public $unit_of_measure_id;
    /**
     * price
     */
    public $price;
    /**
     * vat_type_id
     */
    public $vat_type_id;
    /**
     * height
     */
    public $height;

    /**
     * width
     */
    public $width;
    /**
     * depth
     */
    public $depth;
    /**
     * volume
     */
    public $volume;
    /**
     * weight
     */
    public $weight;
    /**
     * enabled_from
     */
    public $enabled_from;
    /**
     * enabled_to
     */
    public $enabled_to;

    /**
     * updated_id
     */
    public $updated_id;

    /**
     * updated_id
     */
    public $created_id;

    /**
     * created_at
     */
    public $created_at;
    /**
     * update_at
     */
    public $updated_at;

    /**
     * category_id
     */
    public $category_id;

    /**
     * categoryDescription
     */
    public $categoryDescription;

    /**
     * category_father_id
     */
    public $category_father_id;

    /**
     * categoryFatherDescription
     */
    public $categoryFatherDescription;

    /**
     * qta_min
     */
    public $qta_min;

    /**
     * shipment_request
     */
    public $shipment_request;

    /**
     * availability
     */
    public $availability;

    /**
     * commited_customer
     */
    public $commited_customer;

    /**
     * ordered_supplier
     */
    public $ordered_supplier;

    /**
     * stock
     */
    public $stock;

    /**
     * producer_id
     */
    public $producer_id;

    /**
     * descriptionProducer
     */
    public $descriptionProducer;

    /**
     * meta_tag_characteristic
     */
    public $meta_tag_characteristic;

    /**
     * img
     */
    public $img;

    /**
     * new
     */
    public $new;

    /**
     * available
     */
    public $available;

    /**
     * free_shipment
     */
    public $free_shipment;

    /**
     * external_link
     */
    public $external_link;

    /**
     * imgAgg
     */
    public $imgAgg;

    /**
     * sell_if_not_available
     */
    public $sell_if_not_available;


    /**
     * pieces_for_pack
     */
    public $pieces_for_pack;

    /**
     * unit_of_measure_id_packaging
     */
    public $unit_of_measure_id_packaging;

    /**
     * qta_max
     */
    public $qta_max;

    /**
     * digital
     */
    public $digital;

    /**
     * img_digital
     */
    public $img_digital;

    /**
     * limited
     */
    public $limited;

    /**
     * day_of_validity
     */
    public $day_of_validity;

    /**
     * pricelistDescription
     */
    public $pricelistDescription;

    /**
     * item_id
     */
    public $item_id;
    /**
     * item_related_id
     */
    public $item_related_id;
    /**
     * delivery_time_if_available
     */
    public $delivery_time_if_available;
    /**
     * delivery_time_if_out_stock
     */
    public $delivery_time_if_out_stock;
    /**
     * additional_delivery_costs
     */
    public $additional_delivery_costs;

    public $img_attachment;

    public $announcements;

    public $announcements_from;

    public $announcements_to;
    public $Url;
    public $supports;
    public $updateds_at;
    public $imgName_digital;
    public $base64;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoGrDifferentDestination
{
    /**
     * Constructor
     */
    public function __construct()
    { 
       // $this->MapAgg = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

  
    public $id;
    public $user_id;
    public $business_name_plus;
    public $address;
    public $codepostal;
    public $province;
    public $country;

    
    public $business_name;
    public $postal_code;
    public $province_id;
    public $locality;
    public $phone_number;
    public $name;
    public $surname;
  
}

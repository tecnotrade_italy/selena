<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoGrItemsAgg
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;
    public $business_name_supplier;
    public $code_supplier;
    public $price_purchase;
    public $update_date;
    public $usual_supplier;
    public $id_gritems;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoProvince
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Abbreviazione
     */
    public $abbreviation;

    /**
     * Descrizione
     */
    public $description;

    /**
     * Nation_id
     */
    public $nation_id;

    /**
     * Nation
     */
    public $nation;
    

}

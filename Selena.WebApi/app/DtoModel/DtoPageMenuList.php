<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoPageMenuList
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subMenu = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Online
     */
    public $online;

    /**
     * Title
     */
    public $title;

    /**
     * Id menu
     */
    public $idMenu;

    /**
     * Id menu father
     */
    public $idMenuFather;

    /**
     * Order menu
     */
    public $orderMenu;

    /**
     * Css icon
     */
    public $cssIcon;

    /**
     * Menu type
     */
    public $menuType;

    /**
     * Sub menu
     */
    public $subMenu;

    /**
     * url
     */
    public $url;

    /**
     * pageType
     */
    public $pageType;

    public $cssStyle;

    public $seotitle;
    public $seodescription;

}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoJs
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * jsHead
     */
    public $jsHead;

    /**
     * jsBody
     */
    public $jsBody;
}

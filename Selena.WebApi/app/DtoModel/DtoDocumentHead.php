<?php

namespace App\DtoModel;

class DtoDocumentHead
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Order number
     */
    public $orderNumber;

    /**
     * Customer description
     */
    public $customerDescription;

    /**
     * Shipment month
     */
    public $shipmentMonth;
    
    /**
     * Shipment date
     */
    public $shippingDate;
}

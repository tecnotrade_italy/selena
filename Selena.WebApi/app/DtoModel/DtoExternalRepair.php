<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoExternalRepair
{
  /**
   * Constructor
   */
  public function __construct()
  {
      $this->selectFaultModule = collect();
  }

  /**
   * Convert dto to array
   *
   * @return array
   */
  public function toArray()
  {
    return ModelHelper::toArray($this);
  }

  /**
   * id
   */
  public $id;

  /**
   * id_intervention
   */
  public $id_intervention;

  /**
   * id_business_name_supplier
   */
  public $id_business_name_supplier;

  /**
   * description
   */
  public $description;
  /**
   * code_gr
   */
  public $code_gr;
  /**
   * date
   */
  public $date;
  /**
   * ddt_exit
   */
  public $ddt_exit;
  /**
   * nr
   */
  public $nr;

  /**
   * Description_business_name_supplier
   */
  public $Description_business_name_supplier;

  // CAMPI DI RIENTRO DA FORNITORE 
  /**
   * imgmodule
   */
  public $imgmodule;

  /**
   * date_return_product_from_the_supplier
   */
  public $date_return_product_from_the_supplier;

  /**
   * ddt_supplier
   */
  public $ddt_supplier;
  /**
   * reference_supplier
   */
  public $reference_supplier;

  /**
   * notes_from_repairman
   */
  public $notes_from_repairman;
  
  public $imgName;
  public $numero;
  public $codice_intervento;
  public $id_librone_rip_esterna;
  
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoSalesman
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->salesmanUsersIncluded = collect();
        $this->salesmanUsersExcluded = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $code;
    public $business_name;
    public $name;
    public $surname;
    public $email;
    public $phone_number;
    public $vat_number;
    public $fiscal_code;
    public $address;
    public $city;
    public $province;
    public $postal_code;
    public $username;
    public $password;
    public $can_create_new_customer;
    public $can_edit_price;
    public $can_edit_discount;
    public $can_edit_order;
    public $can_active_customer;
    public $view_all_customer;
    public $online;
    

}

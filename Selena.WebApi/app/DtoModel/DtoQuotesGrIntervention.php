<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoQuotesGrIntervention
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * id
     */
    public $id;
    
 	/**
     * id_intervention
     */
    public $id_intervention;
 
    /**
     * id_quotes
     */
    public $id_quotes;
}

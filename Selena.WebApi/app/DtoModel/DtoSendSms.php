<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoSendSms
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        //$this-> = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;
    public $text_message;
    public $send;
    public $date_send;
    public $telephone_number_select_contact;

    public $telephone_number;
    public $result_sms;
    public $date_send_sms;
    public $name;
    public $surname;
    public $business_name;
}

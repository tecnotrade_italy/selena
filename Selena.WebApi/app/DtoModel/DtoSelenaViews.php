<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoSelenaViews
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * nameView
     */
    public $nameView;

    /**
     * description
     */
    public $description;
    
    /**
     * select
     */
    public $select;

    /**
     * from
     */
    public $from;

    /**
     * where
     */
    public $where;

    /**
     * orderBy
     */
    public $orderBy;

    /**
     * groupBy
     */
    public $groupBy;

    /**
     * group
     */
    public $group;
    
}

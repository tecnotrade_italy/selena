<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoBookingSettings
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }
    
    public $id;
    public $nr_appointments;
    public $send_email_to_user;
    public $send_email_to_employee;       
    public $allow_cancellation;  
    public $acceptance_of_mandatory_conditions;
    public $text_gdpr;
    public $email_receives_notifications; 
    public $email_send_notifications;
    public $notification_object_send_to_administrator;
    public $notification_object_send_to_visitor;
    public $time_limit;
    public $booking_notification_id;
    public $cancellation_notification_id;
    public $confirmation_notification_id;
    public $admin_notification_id;
    public $DescriptionNotificationBooking;
    public $CancellationNotification;
    public $ConfirmationNotification;
    public $AdminNotification;
    public $check_user;
    public $check_service;
    public $check_employee;
    public $check_place;
    public $check_price;
    public $check_states;

    public $check_user_week;
    public $check_service_week;
    public $check_employee_week;
    public $check_place_week;
    public $check_price_week;
    public $check_states_week;

    public $check_user_day;
    public $check_service_day;
    public $check_employee_day;
    public $check_place_day;
    public $check_price_day;
    public $check_states_day;

    public $order_user_week;
    public $order_service_week;
    public $order_employee_week;
    public $order_place_week;
    public $order_price_week;
    public $order_states_week;

    public $order_user_day;
    public $order_service_day;
    public $order_employee_day;
    public $order_place_day;
    public $order_price_day;
    public $order_states_day;

    public $order_user_month;
    public $order_service_month;
    public $order_employee_month;
    public $order_place_month;
    public $order_price_month;
    public $order_states_month;

}

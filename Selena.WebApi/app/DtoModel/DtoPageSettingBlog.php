<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoPageSettingBlog
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->languagePageSetting = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Fixed
     */
    public $fixed;

    /**
     * Fixed end
     */
    public $fixedEnd;



    /**
     * Hide search engine
     */
    public $hideSearchEngine;

    /**
     * Home page
     */
    public $homePage;

    /**
     * Id
     */
    public $id;

    /**
     * Id author
     */
    public $idAuthor;

    /**
     * Id functionality
     */
    public $idFunctionality;

    /**
     * Id icon
     */
    public $idIcon;

    /**
     * Id page category
     */
    public $idPageCategory;

    /**
     * Id page share type
     */
    public $idPageShareType;

    /**
     * page category
     */
    public $pageCategory;

    /**
     * page share type
     */
    public $pageShareType;

    /**
     * Language page setting
     */
    public $languagePageSetting;


    /**
     * Publish
     */
    public $publish;

    /**
     * Visible end
     */
    public $visibleEnd;

    /**
     * Visible from
     */
    public $visibleFrom;

    /**
     * Page type
     */
    public $pageType;

    /**
     * protected
     */
    public $protected;
}

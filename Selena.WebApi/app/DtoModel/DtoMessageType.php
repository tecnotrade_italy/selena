<?php

namespace App\DtoMessageType;

use App\Helpers\ModelHelper;

class DtoMessageType
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * id
     */
    public $id;

    /**
     * rate
     */
    public $description;
}

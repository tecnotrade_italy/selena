<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoRedirect
{
    /**
     * Constructor
     */
    public function __construct()
    { 
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;
    public $url_old;
    public $url_new;
    public $active;

    public $UrlOld;
    public $UrlNew;
    public $Active;
}

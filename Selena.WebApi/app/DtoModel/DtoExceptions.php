<?php

namespace App\DtoModel;
use App\Helpers\ModelHelper;

class DtoExceptions
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }
    
    public $id;
    public $place_id;
    public $service_id;
    public $employee_id;
    public $tooltip;
    public $date_start;
    public $date_end;
}

<?php

namespace App\DtoModel;

class DtoCommunityContestImage
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $contest_id;
    public $image_id;
    
}
<?php

namespace App\DtoModel;

class DtoCartRulesItems
{
    /**
     * Constructor
     */
    public function __construct()
    { 

    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $cart_rule_id;
    public $item_id;
    public $NameItem;
    
 
}

<?php

namespace App\DtoModel;

class DtoTemplateEditorGroup
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Code
     */
    public $code;

    /**
     * Description
     */
    public $description;

    /**
     * Id template editor type
     */
    public $idTemplateEditorType;

    /**
     * Template editor type description
     */
    public $templateeditortypeDescription;

    /**
     * Order
     */
    public $order;
}

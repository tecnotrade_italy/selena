<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoImageResize
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * container
     */
    public $container;

    /**
     * width
     */
    public $width;
    
    /**
     * online
     */
    public $online;
    
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoNewsletter
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->listEmail = collect();
        $this->NewsletterGroupQueryEmail = collect(); 
        $this->ListEmailError = collect(); 
        $this->ListEmailOpen = collect();  
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Description
     */
    public $description;

    /**
     * Html
     */
    public $html;

    /**
     * Draft
     */
    public $draft;

    /**
     * Url
     */
    public $url;

    /**
     * Schedule date time
     */
    public $scheduleDateTime;

    /**
     * Start send date time
     */
    public $startSendDateTime;

    /**
     * End send date time
     */
    public $endSendDateTime;

    /**
     * Title
     */
    public $title;

     /**
     * schedule_date_time
     */
    public $schedule_date_time;

      /**
     * sender
     */
    public $sender;

         /**
     * sender_email
     */
    public $sender_email;

    /**
     * object
     */
    public $object;

    /**
     * preview_object
     */
    public $preview_object;

    public $sending_duration;

    public $duratainvio;

    public $nrblocchi;
    public $sommaorafinestimata;
    public $nrdestinatari;
    public $nr_recipients;

   
}

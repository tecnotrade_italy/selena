<?php

namespace App\DtoModel;

class DtoCartPage
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $code;
    public $userId;
    public $dateOrder;
    public $orderState;
    public $netAmount;
    public $totalAmount;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoListGroupSms
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        //$this-> = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }


    public $telephone_number;
}

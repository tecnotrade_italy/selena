<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoCategoryItemDetail
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * descriptionCategory
     */
    public $descriptionCategory;

    /**
     * descriptionItem
     */
    public $descriptionItem;

    /**
     * order
     */
    public $order;

    /**
     * descriptionPrecItem
     */
    public $descriptionPrecItem;

    /**
     * orderPrecItem
     */
    public $orderPrecItem;

    /**
     * descriptionPostItem
     */
    public $descriptionPostItem;

    /**
     * orderPostItem
     */
    public $orderPostItem;

    public $codeProducer;

}

<?php

namespace App\DtoModel;

class DtoTemplateEditor
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Description
     */
    public $description;

    /**
     * Img
     */
    public $img;

    /**
     * Html
     */
    public $html;

    /**
     * Id template editor group
     */
    public $idTemplateEditorGroup;

    /**
     * Template editor type group
     */
    public $templateeditorgroupDescription;

    /**
     * Order
     */
    public $order;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoLogs
{
    /**
     * Constructor
     */
    public function __construct()
    {
       
        $this->ResultLogsAll = collect();
        $this->ResultLogsAllOrder = collect();
        
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    
    public $id;
    public $user_id;
    public $ip;
    public $action;
    public $description;
    public $people_id;
    public $created_at;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoItemAttachmentGroupNewsletterFileTable
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * customergroup
     */
    public $customergroup;

    /**
     * code
     */
    public $code;
    
    /**
     * description
     */
    public $description;

    


}

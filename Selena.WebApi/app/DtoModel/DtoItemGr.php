<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoItemGr
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        $this->AggItems = collect();
        $this->AggExploded = collect();
        $this->AggProduct = collect();
        
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

   
    /**
     * id
     */
    public $id;

    /**
     * tipology
     */
    public $tipology;

    /**
     * note
     */
    public $note;

   /**
     * img_schemes
     */
    public $img;


   /**
     * img_schemes
     */
    public $imgName;

   /**
     * original_code
     */
    public $original_code;

     /**
     * code_product
     */
    public $code_product;
   
     /**
     * code_gr
     */
    public $code_gr;

     /**
     * plant
     */
    public $plant;

        /**
     * brand
     */
    public $brand;

        /**
     * business_name_supplier
     */
    public $business_name_supplier;

        /**
     * code_supplier
     */
    public $code_supplier;
    
         /**
     * price_purchase
     */
    public $price_purchase;
    
         /**
     * price_list
     */
    public $price_list;
    
         /**
     * repair_price
     */
    public $repair_price;
    
         /**
     * price_new
     */
    public $price_new;
    
    /**
     * update_date
     */
    public $update_date;

/**
     * usual_supplier
     */
    public $usual_supplier;

    /**
     * pr_rip_conc
     */
    public $pr_rip_conc;

     /**
     * Tipology
     */
    public $Tipology;

  /**
     * Brand
     */
    public $Brand;

    
  /**
     * BusinessName
     */
    public $BusinessName;

   /**
     * description
     */
    public $description; 

     /**
     * imageitems
     */
    public $imageitems;

    public $AggItems;
    public $totcolumn;

    public $tipologyDescription;

}

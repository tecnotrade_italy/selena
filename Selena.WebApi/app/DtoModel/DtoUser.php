<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoUser
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        $this->imgAgg = collect();
        $this->AggMaps = collect();
        $this->selectAll = collect();
        $this->selectUserExist = collect();
        $this->ResultLogs = collect();
        $this->ResultLogsReferent = collect();
        $this->resultUsers = collect();
        
        
  
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * id
     */
    public $id;

    /**
     * username
     */
    public $username;

    /**
     * email
     */
    public $email;

    /**
     * password
     */
    public $password;

    /**
     * idLanguage
     */
    public $idLanguage;

    /**
     * languageDescription
     */
    public $languageDescription;

  /**
     * company
     */
    public $company;

/**
     * business_name
     */
    public $business_name;

/**
     * name
     */
    public $name;

/**
     * surname
     */
    public $surname;

/**
     * vat_number
     */
    public $vat_number;

/**
     * fiscal_code
     */
    public $fiscal_code;

    /**
     * postal_code
     */
    public $postal_code;
    /**
     * address
     */
    public $address;

    /**
     * telephone_number
     */
    public $telephone_number;

    /**
     * fax_number
     */
    public $fax_number;

     /**
     * website
     */
    public $website;

   /**
     * vat_type_id
     */
    public $vat_type_id;

 /**
     * user_id
     */
    public $user_id;

 /**
     * country
     */
    public $country;

/**
     * province
     */
    public $province;

/**
     * mobile_number
     */
    public $mobile_number;

/**
     * pec
     */
    public $pec;


/**
     * codepostal
     */
    public $codepostal;

    /**
     * imgAgg
     */
    public $imgAgg;


    /**
     * tabella subscriber
     *  * subscriber
     */
    public $subscriber;

     /**
     * tabella subscriber
     *  * subscription_date
     */
    public $subscription_date;
 
     /**
     * tabella subscriber
     *  * expiration_date
     */
    public $expiration_date;
    public $monday;
    public $tuesday;
    public $wednesday;
    public $sunday;
    public $thursday;
    public $friday;
    public $saturday;
    public $nation_id;
    public $Nation_id;
    public $province_id;
    public $Province_id;
    public $region_id;
    public $Region_id;
    public $created_at;
  
    public $id_payments;
    public $id_sdi;
    public $create_quote;
    public $privates;
    public $descriptionPayments;
    public $descriptionCarrier;
    public $port;
    public $final;
    public $check_salesregistration;
    public $check_rehearsal_room;
    public $check;
    public $confirmed;
    public $send_newsletter;
    public $online;
    public $ip; 
    public $date_login;
    public $username_log; 
    public $id_payment;
    public $id_list;
    public $carrier_id;
    public $carriage_id;
    public $payment_fixed;
    public $vat_exempt;
}

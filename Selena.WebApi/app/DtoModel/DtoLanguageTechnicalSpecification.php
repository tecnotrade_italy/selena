<?php

namespace App\DtoModel;

class DtoLanguageTechnicalSpecification
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Description
     */
    public $description;

    /**
     * Order
     */
    public $order;
}

<?php

namespace App\DtoModel;

class DtoIncludedExcluded
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->included = collect();
        $this->excluded = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Included
     */
    public $included;

    /**
     * Included
     */
    public $excluded;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoItemAttachmentFileCategoryTable
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * category_id
     */
    public $category_id;

    /**
     * code
     */
    public $code;
    
    /**
     * description
     */
    public $description;

    


}

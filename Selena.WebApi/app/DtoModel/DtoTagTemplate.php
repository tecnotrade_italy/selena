<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoTagTemplate
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * tag
     */
    public $tag;

}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoConfiguration
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tabsConfigurations = collect();
        $this->tablesConfigurations = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * @param collect(App\DtoModel\DtoTabConfiguration)
     */
    public $tabsConfigurations;

    /**
     * @param collect(App\DtoModel\DtoTableConfiguration)
     */
    public $tablesConfigurations;
}

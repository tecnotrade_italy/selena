<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoPageSitemap
{
    /**
     * Constructor
     */
    public function __construct()
    {
        
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Fixed
     */

    public $url;
    public $title;
    public $updated_at;
  
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoBlogNewsData
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->category = array();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Url cover image
     */
    public $urlCoverImage;

    /**
     * Url share image
     */
    public $urlShareImage;

    /**
     * Title
     */
    public $title;

    /**
     * Url
     */
    public $url;

    /**
     * Text
     */
    public $text;

    /**
     * Category
     */
    public $category;

    /**
     * Date
     */
    public $date;

    public $dateString;

    /**
     * Author
     */
    public $author;

    public $content;
}

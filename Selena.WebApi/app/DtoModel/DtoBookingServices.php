<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoBookingServices
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }
   
    public $id;
    public $name;
    public $duration;
    public $slot_step;
    public $block_first;
    public $block_after;
    public $price;
    public $reduced_price;
    public $advance;
    public $active_purchase;
    public $send_confirmation_end_only_upon_payment;
    public $acceptance_of_mandatory_conditions;
    public $text_gdpr;
    
    
 }
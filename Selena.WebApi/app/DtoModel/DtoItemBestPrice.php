<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoItemBestPrice
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

   
    /**
     * id
     */
    public $id;

    /**
     * item_id
     */
    public $item_id;

    /**
     * img
     */
    public $price;

    public $date;
    
}

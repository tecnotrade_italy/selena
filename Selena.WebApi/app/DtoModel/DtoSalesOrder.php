<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoSalesOrder
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->UsersDatas = collect();
        $this->UserAddressesGoods = collect();
        $this->UserAddressesDocuments = collect();
        $this->SalesOrderDetail = collect(); 
        $this->SalesOrderDet = collect(); 
        $this->OrderMessages = collect(); 
        $this->CartRulesApplied = collect();
        $this->SalesOrderDetailOptional = collect();
        $this->SalesOrderDetailTitleTipology_Optional = collect();
        $this->DetailStatesOrder = collect();   
     }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $order_number;
    public $date_order;
    public $order_state_id;
    public $user_id;
    public $currency_id;
    public $payment_id;
    public $net_amount;
    public $vat_amount;
    public $shipment_amount;
    public $payment_cost;
    public $service_amount;
    public $discount_percentage;
    public $discount_value;
    public $total_amount;
    public $note;
    public $affiliate_id;
    public $carrier_id;
    public $user_address_goods_id;
    public $user_address_documents_id;
    public $carriage_paid_to_id;
    public $fulfillment_date;
    public $to_be_updated;
    public $updated;
    public $payment_type_id;
    public $code;
    public $number_order; 
    public $sum_total_amount;
    public $medium_amount;
    public $available;
    public $sum_total_weight;
    public $sum_quantity;
    public $tracking_shipment;
    public $width;
    public $height;
    public $depth;
    public $sales_order_detail_id;
    public $optional_id;
    public $title_tipology_optional;
    public $support_id;
    

}

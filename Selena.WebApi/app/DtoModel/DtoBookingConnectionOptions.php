<?php

namespace App\DtoModel;
use App\Helpers\ModelHelper;

class DtoBookingConnectionOptions
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }
    
    public $id;
    public $connections_id;
    public $options;
    public $cost;
    public $check_each;
    public $optional_id;
    public $show_button;

}

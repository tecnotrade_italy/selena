<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoAccountVisionPhotos
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        //$this-> = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;
    public $img;
    public $account_vision_id;
}

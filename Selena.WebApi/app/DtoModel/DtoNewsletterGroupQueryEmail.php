<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoNewsletterGroupQueryEmail
{
    /**
     * Constructor
     */
    public function __construct()
    {
         
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * email
     */
    public $email;
    public $select;
    public $from;
    public $where;
    public $order_by;
    public $group_by;


}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoPlant
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->menuList = collect();
        $this->pageList = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $from;
    public $to;
    public $description_plant;
    public $id_tipology_plant;
}

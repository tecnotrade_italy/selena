<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoStates
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * id
     */
    public $id;

 /**
     * descrizione
     */
public $description;
}

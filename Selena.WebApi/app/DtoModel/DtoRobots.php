<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoRobots
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * txtRobots
     */
    public $txtRobots;

}

<?php

namespace App\DtoModel;

class DtoChartJSDatasets
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->data = array();
        $this->backgroundColor = array();
        $this->borderColor = array();
        $this->borderWidth = 1;
        $this->fill = true;
        $this->showLine = true;
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Data
     */
    public $data;

    /**
     * Background color
     */
    public $backgroundColor;

    /**
     * Border color
     */
    public $borderColor;

    /**
     * Border width
     */
    public $borderWidth;

    /**
     * Fill
     */
    public $fill;

    /**
     * Show line
     */
    public $showLine;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoAdvertisingArea
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        $this->MapAgg = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;
    public $code;
    public $description;
    public $width_pixels;
    public $height_pixels;
    public $active;
}

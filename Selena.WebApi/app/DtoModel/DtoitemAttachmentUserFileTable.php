<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoItemAttachmentUserFileTable
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * userid
     */
    public $userid;

    /**
     * code
     */
    public $code;
    
    /**
     * description
     */
    public $description;

    


}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoQuotes
{
    /**
     * Constructor
     */
    public function __construct()
    { 
      
        $this->Articles = collect();
        $this->ReferentEmailSendPreventivi = collect();
        $this->Messages = collect();

    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;
/**
     * states_quote
     */
    public $id_states_quote;

/**
     * id_customer
     */
    public $id_customer; 
   /**
     * date
     */
    public $date_agg;  
    
    /**
     * referent
     */
    public $referent; 

    /**
     * object
     */
    public $object; 

       /**
     * note_before_the_quote
     */
    public $note_before_the_quote; 

 /**
     * code_gr
     */
    public $code_gr; 

    /**
     * description
     */
    public $description;  

  /**
     * description_payment
     */
    public $description_payment;  

  /**
     * totale
     */
    public $tot_quote;  

      /**
     * note
     */
    public $note;  

 /**
     * email
     */
    public $email;  

 /**
     * date_ddt
     */
    public $date_ddt;  

 /**
     * nr_ddt
     */
    public $nr_ddt;  

    /**
     * created_at
     */
    public $created_at;  
    public $updated_at;
    public $updated_id;
    public $created_id;
    

    
 /**
     * Description_payment
     */
    public $Description_payment;  
    
/**
     * States_Description
     */
    public $States_Description;  
    
    public $tot_quote_customer;
    public $note_customer;

    public $external_referent_id;
    public $Description_People;
    public $total_amount_with_discount;
    public $discount; 
    public $kind_attention;
    public $checksendmail;
    public $description_and_items_agg;

    }

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoCategories
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * id
     */
    public $id;

    /**
     * ordinamento
     */
    public $order;


 /**
     * tipologia 
     */
    public $categoryTypeDescription;


 /**
     * categoria padre
     */
    public $category_father_id;

 /**
     * descrizione
     */
    public $description;


/**
       * code
     */
    public $code;


/**
       * code
     */
    public $img;

/**
       * meta_tag_characteristic
     */
    public $characteristic;

    /**
       * meta_tag_link
     */
    public $link;


 /**
       * meta_tag_description
     */
    public $meta_tag_description;

     
/**
       * keyword
     */
    public $meta_tag_keyword;


/**
       * titolo
     */
    public $meta_tag_title;

    /**
       * active
     */
    public $active;

    /**
       * Online
     */
    public $online;

}
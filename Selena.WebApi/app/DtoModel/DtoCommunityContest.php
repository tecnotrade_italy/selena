<?php

namespace App\DtoModel;

class DtoCommunityContest
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $start_datetime;
    public $stop_datetime;
    public $image_id_winner;
    public $image_id_winner2;
    public $image_id_winner3;
    public $description;

}
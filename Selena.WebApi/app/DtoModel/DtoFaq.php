<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoFaq
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * shortcode
     */
    public $shortcode;

    /**
     * description
     */
    public $description;

    /**
    * where
    */
    public $where;

    /**
    * how
    */
    public $how;

    /**
    * what
    */
    public $what;


     /**
    * order
    */
    public $order;
    
}

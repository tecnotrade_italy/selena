<?php

namespace App\DtoModel;

class DtoCarrier
{
    /**
     * Constructor
     */
    public function __construct()
    { 
    $this->selectCarriersExist = collect();
   
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }
    /**
    * Id
    */
    public $id;

    /**
    * name
    */  
    public $name;
    
    /**
    * description
    */ 
    public $description;

    /**
    * img
    */ 
    public $img;

    /**
    * trackingUrl
    */ 
    public $tracking_url;

    /**
    * order
    */ 
    public $order;

    /**
    * vatTypeId
    */ 
    public $vat_type_id;

    /**
    * insurance
    */ 
    public $insurance;

    public $selected;

}

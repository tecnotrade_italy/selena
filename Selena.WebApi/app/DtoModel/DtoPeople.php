<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoPeople
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

   
    /**
     * id
     */
    public $id;


    /**
     * name
     */
    public $name;

    /**
     * surname
     */
    public $surname;


   /**
     * email
     */
    public $email;


   /**
     * email
     */
    public $emails;

     /**
     * phone_number
     */
    public $phone_number;
     /**
     * people_id
     */
    public $people_id;

     /**
     * user_id
     */
    public $user_id;

     /**
     * surnames
     */
    public $surnames;

        /**
     * phone_numbers
     */
    public $phone_numbers;

        /**
     * names
     */
    public $names;

        /**
     * roles_id
     */
    public $roles_id;
    
         /**
     * name_referent
     */
    public $name_referent;
    
         /**
     * surname_referent
     */
    public $surname_referent;
    
         /**
     * email_referent
     */
    public $email_referent;
    
         /**
     * phone_number_referent
     */
    public $phone_number_referent;
    
    /**
     * add_name
     */
    public $add_name;

/**
     * add_surname
     */
    public $add_surname;


    /**
     * add_email
     */
    public $add_email;


    /**
     * add_role_id
     */
    public $add_roles_id;


    /**
     * add_phone_number
     */
    public $add_phone_number;

    public $type_referent;
    public $online;
    public $confirmed_referent;
    public $id_table_user;
    public $username_referent;
    public $password_referent;


}

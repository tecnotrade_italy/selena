<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoSupports
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * desctiption
     */
    public $desctiption;

    /**
     * width
     */
    public $width;

    /**
    * height
    */
    public $height;

    /**
    * depth
    */
    public $depth;

    /**
    * area
    */
    public $area;

    /**
     * margins
     */
    public $margins;
    
   
}

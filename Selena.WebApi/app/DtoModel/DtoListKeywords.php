<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoListKeywords
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * typology_keywords_id
     */
    public $typology_keywords_id;

    /**
     * description
     */
    public $description;

    /**
     * url
     */
    public $url;
    
      /**
     * title
     */
    public $title;
    
    /**
     * keywords
     */
    public $keywords;

    /**
     * online
     */
    public $online;

}

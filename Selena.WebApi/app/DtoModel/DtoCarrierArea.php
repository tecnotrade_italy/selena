<?php

namespace App\DtoModel;

class DtoCarrierArea
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        $this->carrierAreaListPrice = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }
    
    /**
    * Id
    */
    public $id;

    /**
    * carrier_id
    */
    public $carrier_id;

    /**
    * nation_id
    */
    public $nation_id;

    /**
    * province_id
    */
    public $province_id;

    /**
    * region_id
    */
    public $region_id;

    /**
    * from
    */
    public $from;

    /**
    * to
    */
    public $to;

    /**
    * price
    */
    public $price;
    
    /**
    * zone
    */
    public $zone;

    /**
     * carrierAreaListPrice
     */
    public $carrierAreaListPrice;

    
    

}

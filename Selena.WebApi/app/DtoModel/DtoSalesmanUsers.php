<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoSalesmanUsers
{
    /**
     * Constructor
     */
    public function __construct()
    {
        
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $user_id;
    public $salesman_id;
    public $description;
    

}

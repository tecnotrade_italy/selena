<?php

namespace App\DtoModel;

class DtoCommunityFavoriteImage
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $user_id;
    public $image_id;
    public $favorite_gallery_id;
    
}
<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoAddress
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        $this->MapAgg = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;
    public $address;
    public $user_id;
    public $description;
    public $country;
    public $province;
    public $codepostal;
    public $id_map;
    public $maps_map;
    public $address_maps;
    public $country_maps;
    public $province_maps;
    public $codepostal_maps;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoLastActivity
{
    /**
     * Constructor
     */
    public function __construct()
    {}

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * data
     */
    public $data;

    /**
     * Object
     */
    public $oggetto;

    /**
     * Type
     */
    public $type;

    /**
     * Online
     */
    public $online;
}

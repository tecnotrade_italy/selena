<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoNegotiation
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * item
     */
    public $item;

    /**
     * img
     */
    public $img;

    /**
     * to read
     */
    public $toRead;

    /**
     * link
     */
    public $link;

    /**
     * user negotiatior
     */
    public $userNegotiatior;

    /**
     * data
     */
    public $data;
    
}

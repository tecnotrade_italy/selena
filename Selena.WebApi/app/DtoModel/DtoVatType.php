<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoVatType
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * id
     */
    public $id;

    /**
     * rate
     */
    public $rate;
}

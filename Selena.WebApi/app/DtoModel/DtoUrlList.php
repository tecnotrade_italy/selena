<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoUrlList
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->urlListDetail = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * idPage
     */
    public $idPage;

    /**
     * title
     */
    public $title;

    /**
     * urlListDetail
     */
    public $urlListDetail;
}

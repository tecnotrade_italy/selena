<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoSupportsPrices
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * from
     */
    public $from;

    /**
     * to
     */
    public $to;

    /**
    * price
    */
    public $price;

    /**
    * support_prices
    */
    public $supports_id;
   
}

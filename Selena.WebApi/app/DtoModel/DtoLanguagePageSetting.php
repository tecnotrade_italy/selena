<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoLanguagePageSetting
{
    /**
     * Constructor
     */
    public function __construct()
    {}

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Content
     */
    public $content;

    /**
     * Id language
     */
    public $idLanguage;

    /**
     * Seo description
     */
    public $seoDescription;

    /**
     * Seo keyword
     */
    public $seoKeyword;

    /**
     * Seo title
     */
    public $seoTitle;

    /**
     * Share description
     */
    public $shareDescription;

    /**
     * Share title
     */
    public $shareTitle;

    /**
     * Hide breadcrumbs
     */
    public $hideBreadcrumb;

    /**
     * Hide footer
     */
    public $hideFooter;

    /**
     * Hide menu mobile
     */
    public $hideMenuMobile;

    /**
     * Title
     */
    public $title;

    /**
     * Url
     */
    public $url;

    /**
     * Url cover image
     */
    public $urlCoverImage;

    /**
     * Url share image
     */
    public $urlShareImage;

    public $urlCanonical;
    public $imgName;
    public $imgNameShare;

}

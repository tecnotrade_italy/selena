<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoExploded
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    /**
     * description
     */
    public $description;

    /**
     * code_gr
     */
    public $code_gr;

    /**
     * supplier
     */
    public $supplier;

    /**
     * code_supplier
     */
    public $code_supplier;

    /**
     * qta
     */
    public $qta;

    /**
     * unit_price
     */
    public $unit_price;

    /**
     * total_price
     */
    public $total_price;

    /**
     * id_items
     */
    public $id_items;
}

<?php

namespace App\DtoModel;

class DtoSalesOrderDetail
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $order_id;
    public $row_number;
    public $sales_order_row_type_id;
    public $order_detail_state_id;
    public $item_id;
    public $description;
    public $unit_of_measure_id;
    public $unit_price;
    public $quantity;
    public $net_amount;
    public $vat_amount;
    public $discount_percentage;
    public $discount_value;
    public $vat_type_id;
    public $total_amount;
    public $note;
    public $fulfillment_date;
    public $shipped_quantity;
    public $html;
    
    public $sales_order_detail_id;
    public $optional_id;
    public $title_tipology_optional;
    public $support_id;
    public $file;
}
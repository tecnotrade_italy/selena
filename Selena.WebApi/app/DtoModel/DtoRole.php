<?php

namespace App\DtoModel;
use App\Helpers\ModelHelper;

class DtoRole
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        $this->selectGroupExist = collect();   
        $this->users = collect(); 
    
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * name
     */
    public $name;
    
    /**
     * display_name
     */
    public $display_name;

    /**
     * Description
     */
    public $description;
}

<?php

namespace App\DtoModel;

class DtoSalesOrderStateLanguage
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $description;    
    public $language_id;    
}
<?php

namespace App\DtoModel;

class DtoCommunityImageComment
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $user_id;
    public $image_id;
    public $comment_datetime;
    public $comment;
    public $comment_id_response;
    public $invisible;
    public $comment_admin_moderation;
    
}
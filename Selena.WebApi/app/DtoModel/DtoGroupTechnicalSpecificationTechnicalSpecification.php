<?php

namespace App\DtoModel;

class DtoGroupTechnicalSpecificationTechnicalSpecification
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id functionality
     */
    public $idFunctionality;

    /**
     * Id
     */
    public $id;

    /**
     * Id group technical specification
     */
    public $idGroupTechnicalSpecification;

    /**
     * Id technical specification
     */
    public $idTechnicalSpecification;

    /**
     * Description technical specification
     */
    public $descriptionTechnicalSpecification;
}

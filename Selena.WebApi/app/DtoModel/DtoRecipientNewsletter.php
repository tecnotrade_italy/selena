<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoRecipientNewsletter
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        //$this-> = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $newsletter_id;
    public $id;
    public $email;
    public $contact_id;
    public $customer_id;
    public $group_newsletter_id;  
    public $visible;
    public $group;
    public $checked;
}

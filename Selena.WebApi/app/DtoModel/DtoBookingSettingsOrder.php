<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoBookingSettingsOrder
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $field;
    public $order;
    public $fieldCheck;
    public $fieldTo;

}

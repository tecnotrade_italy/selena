<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoBookingAppointments
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->BookingAppointmentssWeek = collect();
        $this->BookingAppointmentssDay = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $connections_id;
    public $user_id;
    public $states_id;       
    public $price;  
    public $start_time;
    public $end_time;
    public $date_from;
    public $date_to;
    public $place_id;
    public $service_id;
    public $employee_id;
    public $fixed_date;
    public $id_states;
    public $title;
    public $date;
    public $description;
    public $note; 
    public $nr_place;
    public $reduced_price;
    public $total_price;

}

<?php

namespace App\DtoModel;

class DtoCommunityFavoriteImageGallery
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $user_id;
    public $name;
    public $description;
    
}
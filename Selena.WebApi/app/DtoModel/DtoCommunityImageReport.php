<?php

namespace App\DtoModel;

class DtoCommunityImageReport
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $image_report_type_id;
    public $user_id;
    public $image_id;
    public $report_message;
    
}
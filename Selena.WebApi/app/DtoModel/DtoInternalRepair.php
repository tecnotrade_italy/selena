<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoInternalRepair
{
  /**
   * Constructor
   */
  public function __construct()
  {
     $this->imagecaptures = collect();
     $this->InternalRepairResultImageCaptures = collect();
     $this->InternalRepairResult = collect();
  }

  /**
   * Convert dto to array
   *
   * @return array
   */
  public function toArray()
  {
    return ModelHelper::toArray($this);
  }

  /**
   * id
   */
  public $id;


  /**
   * supplier
   */
  public $supplier;
  /**
   * supplier
   */
  public $Supplier;

  /**
   * description
   */
  public $description;
  /**
   * code_gr
   */
  public $code_gr;
  /**
   * date
   */
  public $date;
  /**
   * ddt_exit
   */
  public $ddt_exit;

  /**
   * Description_business_name_supplier
   */
  public $Description_business_name_supplier;

  // CAMPI DI RIENTRO DA FORNITORE 
  /**
   * imgmodule
   */
  public $imgmodule;

  /**
   * date_return_product_from_the_supplier
   */
  public $date_return_product_from_the_supplier;

  /**
   * ddt_supplier
   */
  public $ddt_supplier;
  /**
   * reference_supplier
   */
  public $reference_supplier;

  /**
   * notes_from_repairman
   */
  public $notes_from_repairman;

  public $numero;
  
  public $nr_rip_interna; 

}
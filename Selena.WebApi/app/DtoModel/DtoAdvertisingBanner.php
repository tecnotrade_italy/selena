<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoAdvertisingBanner
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        $this->MapAgg = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;
    public $advertising_area_id;
    public $advertising_area;
    public $start_date;
    public $stop_date;
    public $image_file;
    public $image_alt;
    public $link_url;
    public $target_blank;
    public $active;
    public $script_Js;
    public $maximum_impressions;
    public $progressive_impressions;
    public $weight;
    public $user_id;
    public $active_reports;
    public $description;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoItemLanguages
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->selectItemExist = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * idItem
     */
    public $idItem;


    /**
     * language_id
     */
    public $language_id;

    /**
     * description 
     */
    public $description;

    /**
     * meta_tag_characteristic
     */
    public $meta_tag_characteristic;

    public $link;
    public $item_id;
}

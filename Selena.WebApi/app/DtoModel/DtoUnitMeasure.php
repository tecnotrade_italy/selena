<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoSalesOrderDetailUploadFile
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * id
     */
    public $id;

    /**
     * file
     */
    public $file;


 /**
     * sales_order_detail_id
     */
public $sales_order_detail_id;
}

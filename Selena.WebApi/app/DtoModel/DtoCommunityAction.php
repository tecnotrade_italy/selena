<?php

namespace App\DtoModel;

class DtoCommunityAction
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $action_type_id;
    public $user_id;
    public $image_id;

}
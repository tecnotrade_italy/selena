<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoBookingCalendar
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->BookingAppointmentssMonth = collect();
        $this->BookingAppointmentssWeek = collect();
        $this->BookingAppointmentssDay = collect();
 
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $BookingAppointmentssMonth;
    public $BookingAppointmentssWeek;
    public $BookingAppointmentssDay;

}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoFormBuilderField
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->valueFormBuilderField = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * type
     */
    public $type;

    /**
     * label
     */
    public $label;

    /**
     * description
     */
    public $description;

    /**
     * value
     */
    public $value;

    /**
     * width
     */
    public $width;
    
    /**
     * order
     */
    public $order;

     /**
     * valueFormBuilderField
     */
    public $valueFormBuilderField;
    
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoPlaces
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }
   
    /**
     * id
     */
    public $id;

    /**
     * name
     */
    public $name;

    /**
     * address
     */
    public $address;

   /**
     * postal_code
     */
    public $postal_code;

     /**
     * place
     */
    public $place;

     /**
     * province
     */
    public $province;

      /**
     * nation_id
     */
    public $nation_id;
    
    public $Province;
    public $Nation;
    public $lat;
    
    public $lon;
}

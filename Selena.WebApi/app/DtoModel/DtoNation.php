<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoNation
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Cee
     */
    public $cee;

    /**
     * Abbreviazione
     */
    public $abbreviation;
    
    /**
     * Descrizione
     */
    public $description;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoOffertaTrovaPrezzi
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        $this->MapAgg = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;
    public $name;
    public $merchantName;
    public $merchant;
    public $currencyCode;
    public $price;   //prezzo corrente dell'offerta
    public $listingPrice;   //prezzo di listino dell'oggetto. Se non disponibile, viene settato uguale a price
    public $deliveryCost;   //costo di spedizione
    public $availability;    //codice disponibilità  -1 informazione non fornita, 0 disponibile, 3 disp limitata
    public $availabilityDescr;   //Descrizione testuale della disponibilità
    public $url;   //url dell'offerta
    public $description;   //descrizione dell'offerta fornita dal merchant
    public $smallImage;   //immagine del prodotto 100x100
    public $bigImage;   //immagine del prodotto 300x300
    public $merchantLogo;   //logo del merchant
    public $categoryId;
    public $categoryName;
    public $merchantRating;   //voto medio del merchant nelle recensioni di trovaprezzi



}

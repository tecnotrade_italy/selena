<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoQuotesMessages
{
    /**
     * Constructor
     */
    public function __construct()
    { 
       // $this-> = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;
    public $id_sender;
    public $user_id;
    public $message;
    public $id_receiver;
    public $id_intervention;
    public $created_at;
    public $updated_at;
}

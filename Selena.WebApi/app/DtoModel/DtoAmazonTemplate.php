<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoAmazonTemplate
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        $this->MapAgg = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;
    public $code;
    public $description;
    public $shortcode_example;
    public $content;

}

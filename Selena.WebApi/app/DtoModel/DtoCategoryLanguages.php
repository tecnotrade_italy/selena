<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoCategoryLanguages
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->selectCategoriesExist = collect();
        $this->selectItemExist = collect();
        
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * id
     */
    public $id;

    /**
     * category_id
     */
    public $category_id;

 /**
     * descrizione
     */
    public $description;

}
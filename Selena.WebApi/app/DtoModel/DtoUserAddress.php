<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoUserAddress
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        $this->UserAddress = collect();
     
        
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * id
     */
    public $id;

    /**
     * user_id
     */
    public $user_id;

    /**
     * address
     */
    public $address;

    /**
     * locality
     */
    public $locality;

    /**
     * postal_code_id
     */
    public $postal_code_id;

    /**
     * province_id
     */
    public $province_id;

  /**
     * nation_id
     */
    public $nation_id;

/**
     * phone_number
     */
    public $phone_number;

/**
     * note
     */
    public $note;

/**
     * default_address
     */
    public $default_address;

/**
     * name
     */
    public $name;

/**
     * surname
     */
    public $surname;

    /**
     * postal_code
     */
    public $postal_code;
    /**
     * vat_number
     */
    public $vat_number;

    /**
     * region_id
     */
    public $region_id;

    /**
     * online
     */
    public $online;

    public $code;

    public $DefaultAddress;

    public $Online;

    public $id_useraddress;
    public $description;
}
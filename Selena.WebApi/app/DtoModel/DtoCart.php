<?php

namespace App\DtoModel;

class DtoCart
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $user_id;
    public $session_token;
    public $currency_id;
    public $net_amount;
    public $vat_amount;
    public $shipment_amount;
    public $total_amount;
    public $discount_percentage;
    public $discount_value;
    public $note;
    public $payment_cost;
    public $service_amount;
    public $user_address_goods_id;
    public $user_address_documents_id;
    public $carriage_id;

}

<?php

namespace App\DtoModel;
use App\Helpers\ModelHelper;

class DtoEmployees
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }
    
    public $id;
    public $name;
    public $description;
    public $email;
    public $telephone_number;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoFeedbackNewsletter
{
    /**
     * Constructor
     */
    public function __construct()
    {
       
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Code
     */
    public $code;

    /**
     * name
     */
    public $name;

    /**
     * Email
     */
    public $email;
    public $surname;
    public $business_name;
    public $newsletter_recipient_id;
    public $send_newsletter;
    public $created_at;
    public $updated_email;
    public $cont;
   
}

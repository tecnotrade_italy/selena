<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoBlogNews
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->data = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Title
     */
    public $title;

    /**
     * Data
     */
    public $data;
}

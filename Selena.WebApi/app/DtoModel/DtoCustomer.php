<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoCustomer
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Datasets
     */
    public $company;

    /**
     * Business name
     */
    public $businessName;

    /**
     * Name
     */
    public $name;

    /**
     * Surname
     */
    public $surname;

    /**
     * Vat number
     */
    public $vatNumber;

    /**
     * Fiscal code
     */
    public $fiscalCode;

    /**
     * Address
     */
    public $address;

    /**
     * Id postal code
     */
    public $idPostalCode;

    /**
     * Postal code
     */
    public $postalCodeDescription;

    /**
     * Telephone number
     */
    public $telephoneNumber;

    /**
     * Fax number
     */
    public $faxNumber;

    /**
     * Email
     */
    public $email;

    /**
     * Website
     */
    public $website;

    /**
     * Id language
     */
    public $idLanguage;

    /**
     * Language
     */
    public $languageDescription;

    /**
     * Id vat type
     */
    public $idVatType;

    /**
     * Vat type
     */
    public $vatTypeDescription;

    /**
     * Enable from
     */
    public $enableFrom;

    /**
     * Enable to
     */
    public $enableTo;

    /**
     * Send newsletter
     */
    public $sendNewsletter;

    /**
     * Error newsletter
     */
    public $errorNewsletter;
}

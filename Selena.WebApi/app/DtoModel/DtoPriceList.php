<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoPriceList
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * id
     */
    public $id;

    /**
     * pricelist_id
     */
    public $pricelist_id;

    /**
     * item_id
     */
    public $item_id;

    /**
     * price
     */
    public $price;

    public $pricelistDescription;
};

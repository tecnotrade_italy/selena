<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoModuleValidationGuarantee
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        //$this-> = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;
    public $id_user;
    public $referent;
    public $date_mount;
    public $id_intervention;
    public $trolley_type;
    public $plant_type;
    public $voltage;
    public $traction;
    public $lift;
    public $insulation_value_tewards_machine_frame_positive;
    public $insulation_value_tewards_machine_frame_negative;
    public $single_motor_with_traction_motor_range;
    public $single_motor_with_brush_traction_motor;
    public $single_motor_u_v_w_traction_phase_motor;
    public $single_motor_u_v_w_motor_lifting_phase;
    public $bi_engine_with_traction_motor_range;
    public $bi_engine_with_brush_traction_motor;
    public $bi_engine_u_v_w_traction_phase_motor;
    public $bi_engine_u_v_w_motor_lifting_phase;
    public $lift_with_field_brush_motor_raised;
    public $lift_with_u_v_w_motor_lift_pharse;
    public $battery_voltage;
    public $traction_limit_current_consumption;
    public $lift_limit_current_absorption;
    public $DescriptionReferent;
    public $external_referent_id;
 
}

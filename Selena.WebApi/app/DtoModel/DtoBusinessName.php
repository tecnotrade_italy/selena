<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoBusinessName
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

   
    /**
     * id
     */
    public $id;

    /**
     * business_name
     */
    public $business_name;
    
 	/**
     * address
     */
    public $address;
 
 	/**
     * country
     */
    public $country;

/**
     * province
     */
    public $province;
    public $name;
    public $surname;
    public $email;
    public $telephone_number;
    public $mobile_phone;
    public $fax_number;



}

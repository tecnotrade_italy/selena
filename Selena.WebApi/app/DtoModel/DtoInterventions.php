<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoInterventions
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->captures = collect();
        $this->Articles = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Internal code
     */
    public $customer_id;

    /**
     * items_id
     */
    public $items_id;

    /**
     * Internal code
     */
    public $code_product;

    /**
     * Description
     */
    public $description;


    /**
     * Description
     */
    public $defect;


    /**
     * unit_of_measure_id
     */
    public $nr_ddt;

    /**
     * unit_of_measure_id
     */
    public $nr_ddt_gr;

    /**
     * date_ddt
     */
    public $date_ddt;

    /**
     * date_ddt
     */
    public $date_ddt_gr;

    /**
     * vat_type_id
     */
    public $img;
    /**
     * height
     */
    public $unrepairable;

    /**
     * width
     */
    public $send_to_customer;
    /**
     * depth
     */
    public $create_quote;
    /**
     * volume
     */
    public $idIntervention;


    /**
     * descriptionCustomer
     */
    public $descriptionCustomer;



    /**
     * descriptionStates
     */
    public $descriptionStates;


    /**
     * weight
     */
    public $captures;

    /**
     * weight
     */
    public $referent;

    /**
     * weight
     */
    public $plant_type;

    /**
     * weight
     */
    public $trolley_type;

    /**
     * weight
     */
    public $series;

    /**
     * weight
     */
    public $voltage;

    /**
     * weight
     */
    public $exit_notes;

    /**
     * img_module
     */
    public $imgmodule;

    /**
     * user_id
     */
    public $user_id;

    /**
     * imgName
     */
    public $imgName;

    public $codeGr;

    public $updated_at;

    public $flaw_detection;

    public $done_works;

    public $to_call;

    public $escape_from;

    public $working;

    public $descriptionUser;

    public $hour;

    public $value_config_hour;

    public $tot_value_config;

    public $consumables;

    public $tot_final;

    public $total_calculated;

    public $Articles;

    public $date_aggs;

    public $States_Description;

    public $id_states_quote;

    public $idInterventionExternalRepair;

    public $code_tracking;
    public $search_product;
    public $ready;
    public $date_return_product_from_the_supplier;
    public $ddt_supplier;
    public $fileddt;
    public $phone_number;
    public $id_states_quotes;
    public $states_description;
    public $states_quotes_description;
    public $descriptionNameProduct;
    public $name_product;
    public $working_with_testing;
    public $working_without_testing; 

    public $id_people; 
    public $rip_association; 
    public $plant; 
    public $repaired_arrived;
    public $note_internal_gr;
    public $checkModuleFault;
    public $checkModuleValidationGuarantee;
    public $contanumeriid;
    public $external_referent_id;
    public $Description_People; 
    public $checkQuotes;
    public $descriptionfast;
    public $date;
    public $description_user_id;
    public $checksendmail;
    public $code_intervention_gr;
    public $descriptionPeopleReferentExternal;
    public $external_referent_id_intervention;
    public $checkProcessingExist;
    
}

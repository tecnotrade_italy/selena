<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoOptional
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * desctiption
     */
    public $desctiption;

    /**
     * availability
     */
    public $availability;

    /**
    * variationType
    */
    public $variationType;

    /**
    * variationPrice
    */
    public $variationPrice;

    /**
    * selected
    */
    public $selected;

    /**
     * order
     */
    public $order;
    
    /**
     * typeOptional used for table
     */
    public $typeOptional;

    /**
     * typeOptionalId used for select
     */
    public $typeOptionalId;

}

<?php

namespace App\DtoModel;

class DtoCarriage
{
    /**
     * Constructor
     */
    public function __construct()
    { 
    $this->selectCarriersExist = collect();
   
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }
    /**
    * Id
    */
    public $id;

    /**
    * code
    */  
    public $code;
    
    /**
    * description
    */ 
    public $description;

    /**
    * add_transport_cost
    */ 
    public $add_transport_cost;

    /**
    * order
    */ 
    public $order;

    /**
    * online
    */ 
    public $online;

    public $language_id;

    /**
    * records on table
    */ 
    public $Code;
    public $Description;
    public $Online;
    public $Order;
    public $selected;
    public $carrierdescription;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoRecipientSms
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        //$this-> = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id_sms;
    public $id;
    public $telephone_number;
    public $result_sms;
    public $date_send_sms;
    
}

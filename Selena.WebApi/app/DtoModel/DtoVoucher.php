<?php

namespace App\DtoModel;

class DtoVoucher
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $code;
    public $type_id;
    public $enabled_to;
    public $enabled_from;
    public $cumulative;
    public $minimum_amount;
    public $discount_percentage;
    public $discount_value;
    public $online;
    public $note;
    public $availability;
    public $availability_residual;

}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoProducer
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
            *  table
     * code
     */
    public $Code;
/**
      * id
     */
    public $id;


    /**
     * BusinessName
     */
    public $BusinessName;

    /**
     * Address
     */
    public $Address;


   /**
     * Resort
     */
    public $Resort;


     /**
     * Province
     */
    public $Province;
     /**
     * PostalCode
     */
    public $PostalCode;
     /**
     * Email
     */
    public $Email;


     /**
     * detail
     * code
     */
    public $code;

 /**
     * business_name
     */
    public $business_name;
     /**
     * address
     */
    public $address;
     /**
     * resort
     */
    public $resort;
     /**
     * province
     */
    public $province;
     /**
     * postal_code_id
     */
    public $postal_code_id;
     /**
     * telephone_number
     */
    public $telephone_number;

/**
     * email
     */
    public $email;

/**
     * online
     */
    public $online;

}

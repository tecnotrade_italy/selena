<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoContactGroupNewsletter
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Id contact
     */
    public $idContact;

    /**
     * Id group newsletter
     */
    public $idGroupNewsletter;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoFormBuilder
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->formField = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * description
     */
    public $description;

    public $formField;

}

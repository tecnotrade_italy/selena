<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoChartMulti
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->charts = collect();
        $this->chartsres = collect();
        $this->chartsrestime = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Count
     */
    public $count;

    /**
     * Charts
     */
    public $charts;

}

<?php

namespace App\DtoModel;

class DtoGroupDisplayTechnicalSpecification
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groupDisplayTechnicalSpecificationLanguage = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id functionality
     */
    public $idFunctionality;

    /**
     * Id
     */
    public $id;

    /**
     * Order
     */
    public $order;

    /**
     * Group display technical specification language
     */
    public $groupDisplayTechnicalSpecificationLanguage;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoItemImages
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        $this->imgAgg = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * itemId
     */
    public $itemId;

    /**
     * img
     */
    public $img;

    /**
     * imgName
     */
    public $imgName;

    /**
     * weight
     */
    public $weight;

    /**
     * height
     */
    public $height;

    /**
     * width
     */
    public $width;

    /**
     * order
     */
    public $order;


}

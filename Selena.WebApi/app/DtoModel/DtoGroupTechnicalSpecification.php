<?php

namespace App\DtoModel;

class DtoGroupTechnicalSpecification
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groupTechnicalSpecificationLanguage = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id functionality
     */
    public $idFunctionality;

    /**
     * Id
     */
    public $id;

    /**
     * Group technical specification language
     */
    public $groupTechnicalSpecificationLanguage;
}

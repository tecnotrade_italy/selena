<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoGroupNewsletter
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Description
     */
    public $description;

    /**
     * Name
     */
    public $name;

    /**
     * Surname
     */
    public $surname;

    /**
     * Business name
     */
    public $businessName;

    /**
     * Email
     */
    public $email;
    public $checkUserGroup;
}

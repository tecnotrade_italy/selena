<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoInterventionsInterventionId
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        $this->captures = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Internal code
     */
    public $customer_id;

/**
     * items_id
     */
    public $items_id;

/**
     * Internal code
     */
    public $code_product;

    /**
     * Description
     */
    public $description;


   /**
     * Description
     */
    public $defect;


     /**
     * unit_of_measure_id
     */
    public $nr_ddt;

     /**
     * unit_of_measure_id
     */
    public $nr_ddt_gr;
    
     /**
     * date_ddt
     */
    public $date_ddt;

    /**
     * date_ddt
     */
    public $date_ddt_gr;

     /**
     * vat_type_id
     */
    public $img;
     /**
     * height
     */
    public $unrepairable;

 /**
     * width
     */
    public $send_to_customer;
     /**
     * depth
     */
    public $create_quote;
     /**
     * volume
     */
    public $idIntervention;


     /**
     * descriptionCustomer
     */
    public $descriptionCustomer;
    


     /**
     * descriptionStates
     */
    public $descriptionStates;


     /**
     * weight
     */
    public $captures;

     /**
     * weight
     */
    public $referent;

     /**
     * weight
     */
    public $plant_type;

    /**
     * weight
     */
    public $trolley_type;

/**
     * weight
     */
    public $series;

/**
     * weight
     */
    public $voltage;

/**
     * weight
     */
    public $exit_notes;

    /**
     * img_module
     */
    public $imgmodule;

 /**
     * user_id
     */
    public $user_id;

      /**
     * imgName
     */
    public $imgName;

}

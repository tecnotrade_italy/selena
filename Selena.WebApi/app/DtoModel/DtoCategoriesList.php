<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoCategoriesList
{
  /**
   * Constructor
   */
  public function __construct()
  {
      $this->categoriesList = collect();
  }

  /**
   * Convert dto to array
   *
   * @return array
   */
  public function toArray()
  {
      return ModelHelper::toArray($this);
  }

  /**
   * Categories List
   */
  public $categoriesList;
}
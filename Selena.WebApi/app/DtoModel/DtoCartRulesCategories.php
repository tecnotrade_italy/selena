<?php

namespace App\DtoModel;

class DtoCartRulesCategories
{
    /**
     * Constructor
     */
    public function __construct()
    { 

    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $cart_rule_id;
    public $category_item_id;
    public $DescriptionCategories;
    
 
}

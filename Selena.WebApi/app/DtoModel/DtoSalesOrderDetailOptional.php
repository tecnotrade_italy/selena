<?php

namespace App\DtoModel;

class DtoSalesOrderDetailOptional
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $sales_order_detail_id;
    public $optional_id;
    public $title_tipology_optional;
}
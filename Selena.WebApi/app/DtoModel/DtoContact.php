<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoContact
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Name
     */
    public $name;

    /**
     * Surname
     */
    public $surname;

    /**
     * Business name
     */
    public $businessName;

    /**
     * Email
     */
    public $email;

    /**
     * Phone number
     */
    public $phoneNumber;

    /**
     * Send newsletter
     */
    public $sendNewsletter;

    /**
     * Error newsletter
     */
    public $errorNewsletter;

    public $result;
}

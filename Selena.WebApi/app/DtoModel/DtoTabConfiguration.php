<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoTabConfiguration
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pageConfigurations = collect();
        $this->tablesConfigurations = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * @param string
     */
    public $code;

    /**
     * @param string
     */
    public $label;

    /**
     * @param order
     */
    public $order;

    /**
     * @param collect(App\DtoModel\DtoPageConfiguration)
     */
    public $pageConfigurations;

    /**
     * @param collect(App\DtoModel\DtoTableConfiguration)
     */
    public $tablesConfigurations;
}

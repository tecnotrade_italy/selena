<?php

namespace App\DtoModel;

class DtoCommunityImage
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $user_id;
    public $users_id;
    public $image_title;
    public $image_category_id;
    public $gallery_id;
    public $image_description;
    public $location_latitude;
    public $location_longitude;
    public $keywords;
    public $susceptible_content;
    public $upload_datetime;
    public $star_rating;
    public $nr_views;
    public $rating;
    public $EXIF_camera;
    public $EXIF_camera_id;
    public $EXIF_lens;
    public $EXIF_lens_id;
    public $EXIF_iso;
    public $EXIF_shutter_speed;
    public $EXIF_aperture;
    public $EXIF_focal_lenght;
    public $editor_pick;
    public $editor_pick_date;
    public $online;

}
<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoItemAttachmentLanguageTypeTable
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->itemAttachmentLanguageType = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * desctiption
     */
    public $desctiption;

    /**
     * code
     */
    public $code;

    /**
    * order
    */
    public $order;

    /**
    * language_id
    */
    public $language_id;

    /**
    * item_attachment_father_type_id
    */
    public $item_attachment_father_type_id;


}

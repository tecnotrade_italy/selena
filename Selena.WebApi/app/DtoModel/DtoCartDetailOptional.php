<?php

namespace App\DtoModel;

class DtoCartDetailOptional
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $cart_detail_id;
    public $optional_id;
    
}


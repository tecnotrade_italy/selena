<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoItemAttachmentLanguageFileTable
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;
    
    /**
     * description
     */
    public $description;

    /**
     * file
     */
    public $file;

    /**
     * fileBase64
     */
    public $fileBase64;

    /**
     * url
     */
    public $url;

    /**
     * language_id
     */
    public $language_id;

    /**
     * language_description
     */
    public $language_description;


}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoItem
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Internal code
     */
    public $internalCode;

    /**
     * Description
     */
    public $description;


 /**
     * availability
     */
    public $availability;

     /**
     * price
     */
    public $price;

    public $vat_type_id;

}

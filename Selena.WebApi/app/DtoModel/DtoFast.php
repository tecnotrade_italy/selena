<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoFast
{
    /**
     * Constructor
     */
    public function __construct()
    {

        $this->Aggfasturgencies = collect();

     }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * date
     */
    public $date;

    /**
     * Description
     */
    public $description;

 /**
     * object
     */
    public $object;

     /**
     * user_id
     */
    public $user_id;

    public $description_user_id;

}

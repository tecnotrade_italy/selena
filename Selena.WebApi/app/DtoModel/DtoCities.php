<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoCities
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Descrizione
     */
    public $description;

    /**
     * Ordine
     */
    public $order;

    /**
     * Istat
     */
    public $istat;
    

}

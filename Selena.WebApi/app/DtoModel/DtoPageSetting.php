<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoPageSetting
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->languagePageSetting = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Fixed
     */
    public $fixed;

    /**
     * Fixed end
     */
    public $fixedEnd;

    /**
     * Hide header
     */
    public $hideHeader;

    /**
     * Hide menu
     */
    public $hideMenu;

    /**
     * Hide search engine
     */
    public $hideSearchEngine;

    /**
     * Home page
     */
    public $homePage;

    /**
     * Id
     */
    public $id;

    /**
     * Id author
     */
    public $idAuthor;

    /**
     * Id functionality
     */
    public $idFunctionality;

    /**
     * Id icon
     */
    public $idIcon;

    /**
     * Id page category
     */
    public $idPageCategory;

    /**
     * Id page share type
     */
    public $idPageShareType;

    /**
     * page category
     */
    public $pageCategory;

    /**
     * page share type
     */
    public $pageShareType;

    /**
     * Language page setting
     */
    public $languagePageSetting;

    /**
     * Online
     */
    public $online;

    /**
     * Publish
     */
    public $publish;

    /**
     * Visible end
     */
    public $visibleEnd;

    /**
     * Visible from
     */
    public $visibleFrom;

    /**
     * Page type
     */
    public $pageType;

    /**
     * js
     */
    public $js;

    /**
     * Enable js
     */
    public $enableJS;

    /**
     * protected
     */
    public $protected;

    public $userName;

    public $idUser;
    public $userIdAuthor;

}

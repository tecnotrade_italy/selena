<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoRequestContact
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Name
     */
    public $name;

    /**
     * Surame
     */
    public $surname;

    /**
     * Email
     */
    public $email;

    /**
     * Date
     */
    public $date;

    /**
     * Description
     */
    public $description;
}

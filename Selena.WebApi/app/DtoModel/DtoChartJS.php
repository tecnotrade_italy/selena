<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoChartJS
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->labels = collect();
        $this->datasets = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Labels
     */
    public $labels;

    /**
     * Datasets
     */
    public $datasets;
}

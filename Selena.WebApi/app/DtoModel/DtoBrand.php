<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoBrand
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

   
    /**
     * id
     */
    public $id;

    /**
     * brand
     */
    public $brand;
    
 	/**
     * description
     */
    public $description;
 

}

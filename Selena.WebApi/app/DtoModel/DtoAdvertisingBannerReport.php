<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoAdvertisingBannerReport
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        $this->MapAgg = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;
    public $advertising_banner_id;
    public $impression_date;
    public $page;
    public $source_ip;

}

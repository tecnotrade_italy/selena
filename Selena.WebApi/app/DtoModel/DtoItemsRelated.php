<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoItemsRelated
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * item_id
     */
    public $item_id;
    /**
     * item_related_id
     */
    public $item_related_id;

    public $internal_code;

    public $descriptionArticle;
    public $descriptionCategories;
}

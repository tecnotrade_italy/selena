<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoNegotiationDetail
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->msg = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * item
     */
    public $item;

    /**
     * description
     */
    public $description;

    /**
     * id user msg
     */
    public $idUserMsg;

    /**
     * id user negotiatior
     */
    public $idUserNegotiatior;

    /**
     * id user shopkeeper
     */
    public $idUserShopkeeper;

    /**
     * txt user negotiatior
     */
    public $txtUserNegotiatior;

    /**
     * txt user shopkeeper
     */
    public $txtUserShopkeeper;

    /**
     * msg
     */
    public $msg;

    /**
     * data
     */
    public $data;
    
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoItemAttachmentFileTable
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->translateAttachment = collect();
        $this->itemIncluded = collect();
        $this->categoryIncluded = collect();
        $this->groupNewsletter = collect();
        $this->userIncluded = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;
    
    /**
     * description
     */
    public $description;

    /**
     * attachment
     */
    public $attachment;
    
    /**
    * typeAttachmentId
    */
    public $typeAttachmentId;

    /**
    * typeAttachmentDescription
    */
    public $typeAttachmentDescription;



}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoUserPhoto
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

   
    /**
     * id
     */
    public $id;

    /**
     * user_id
     */
    public $user_id;

    /**
     * img
     */
    public $img;

}

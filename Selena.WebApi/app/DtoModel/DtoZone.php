<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoZone
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * id_cities
     */
    public $id_cities;
    public $IdCities;  
    /**
     * id_postalcodes
     */
    public $id_postalcodes;
    public $IdPostalcodes;
    /**
     * id_provinces
     */
    public $id_provinces;
    public $IdProvinces;
    /**
     * id_regions
     */
    public $id_regions;
    public $IdRegions;
    /**
     * id_nations
     */
    public $id_nations;
    public $IdNations;
}

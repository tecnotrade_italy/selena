<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoAmazonProduct
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        $this->MapAgg = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $asin;
    public $title;
    public $url;
    public $informations;
    public $imageSmall;
    public $imageMedium;
    public $imageLarge;
    public $buyingPrice;
    public $normalPrice;
    public $discountPercentage;
    public $discountValue;
    public $prime;

}

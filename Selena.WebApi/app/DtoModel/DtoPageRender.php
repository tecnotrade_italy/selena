<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoPageRender
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->blog = new DtoBlogNews();
        $this->news = new DtoBlogNews();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Content
     */
    public $content;

    /**
     * hideBreadcrumb
     */
    public $hideBreadcrumb;

    /**
     * Hide footer
     */
    public $hideFooter;

    /**
     * Hide header
     */
    public $hideHeader;

    /**
     * Share title
     */
    public $shareTitle;

    /**
     * Share Description
     */
    public $shareDescription;

    /**
     * Url cover image
     */
    public $urlCoverImage;

    /**
     * Url share image
     */
    public $urlShareImage;

    /**
     * Visible from
     */
    public $visibleFrom;

    /**
     * Visible end
     */
    public $visibleEnd;

     /**
     * Visible end
     */
    public $publishDate;

    /**
     * Hide search engine
     */
    public $hideSearchEngine;

    /**
     * Seo description
     */
    public $seoDescription;

    /**
     * Seo keyword
     */
    public $seoKeyword;
/**
     * Seo keyword
     */
    public $availability;

    
    /**
     * Seo title
     */
    public $seoTitle;

    /**
     * Title
     */
    public $title;

    /**
     * Blog
     */
    public $blog;

    /**
     * News
     */
    public $news;

    /**
     * js
     */
    public $js;

    /**
     * Enable js
     */
    public $enableJS;

 /**
     * name
     */
    public $name;

 /**
     * surname
     */
    public $surname;


/**
     * address
     */
    public $address;

/**
     * country
     */
    public $country;

/**
     * province
     */
    public $province;
}

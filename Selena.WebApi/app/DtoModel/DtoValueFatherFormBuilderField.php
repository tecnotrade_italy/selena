<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoValueFatherFormBuilderField
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * value
     */
    public $value;

    /**
     * img
     */
    public $img;

    /**
     * order
     */
    public $order;

    /**
     * description
     */
    public $description;

    /**
     * base64
     */
    public $base64;

     /**
     * default
     */
    public $default;

    


}

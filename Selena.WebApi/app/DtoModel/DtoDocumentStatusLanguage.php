<?php

namespace App\DtoModel;

class DtoDocumentStatusLanguage
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $description;    
    public $language_id;    
    public $code;
    public $html;
    public $document_status_id;
}
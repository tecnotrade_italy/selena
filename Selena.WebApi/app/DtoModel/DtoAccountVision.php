<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoAccountVision
{
    /**
     * Constructor
     */
    public function __construct()
    { 
        $this->imageaccountvision = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;
    public $date;
    public $ddt_gr;
    public $description;
    public $code_gr;
    public $note;
    public $user_id;
    public $date_return;
    public $ddt_return;
    public $note_return;
    public $soll;
    public $DescriptionUser_id;
    public $id_contovisione;
  
}

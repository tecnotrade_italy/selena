<?php

namespace App\DtoModel;

class DtoItemGroupTechnicalSpecification
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id functionality
     */
    public $idFunctionality;

    /**
     * Id
     */
    public $id;

    /**
     * Id item
     */
    public $idItem;

    /**
     * Description Item
     */
    public $descriptionItem;

    /**
     * Internal code Item
     */
    public $internalCodeItem;

    /**
     * Id group technical specification
     */
    public $idGroupTechnicalSpecification;

    /**
     * Description group technical specification
     */
    public $descriptionGroupTechnicalSpecification;

    /**
     * Complete technical specification
     */
    public $completeTechnicalSpecification;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoTypeOptional
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->optional = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * desctiption
     */
    public $desctiption;

    /**
     * order
     */
    public $order;
    
    /**
     * required
     */
    public $required;

    /**
     * optional
     */
    public $optional;

}

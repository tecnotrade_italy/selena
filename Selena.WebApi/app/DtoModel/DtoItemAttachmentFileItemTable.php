<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoItemAttachmentFileItemTable
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * item_id
     */
    public $item_id;

    /**
     * code
     */
    public $code;
    
    /**
     * description
     */
    public $description;

    


}

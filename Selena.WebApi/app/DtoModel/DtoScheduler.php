<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoScheduler
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * id
     */
    public $id;

    /**
     * type
     */
    public $type;

    /**
     * id_task_type
     */
    public $id_task_type;

    /**
    * user_id
    */
    public $user_id;

    /**
    * name
    */
    public $name;

    /**
    * description
    */
    public $description;


     /**
    * action
    */
    public $action;

     /**
    * arguments
    */
    public $arguments;

    /**
    * start_time_action
    */
    public $start_time_action;

     /**
    * schedule
    */
    public $schedule;

    /**
    * frequency
    */
    public $frequency;

    /**
    * repeat_after_hour
    */
    public $repeat_after_hour;

     /**
    * repeat_after_min
    */
    public $repeat_after_min;
     /**
    * repeat_times
    */
    public $repeat_times;


     /**
    * next_run
    */
    public $next_run;
     /**
    * last_run
    */
    public $last_run;
     /**
    * status
    */
    public $status;
     /**
    * notification
    */
    public $notification;
     /**
    * active
    */
    public $active;

    
}

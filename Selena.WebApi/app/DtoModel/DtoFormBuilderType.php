<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoFormBuilderType
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * description
     */
    public $description;

    /**
     * order
     */
    public $order;


}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoPageConfiguration
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Label
     */
    public $label;

    /**
     * Field
     */
    public $field;

    /**
     * Formatter
     */
    public $formatter;

    /**
     * Required
     */
    public $required;

    /**
     * Default value
     */
    public $defaultValue;

    /**
     * Service
     */
    public $service;

    /**
     * Max lenght
     */
    public $maxLenght;

    /**
     * Input type
     */
    public $inputType;

    /**
     * Pos y
     */
    public $posY;

    /**
     * Pos x
     */
    public $posX;

    /**
     * Table order
     */
    public $tableOrder;

    /**
     * Class
     */
    public $class;

    /**
     * Attribute
     */
    public $attribute;

    /**
     * Colspan
     */
    public $colspan;

    /**
     * Id tab
     */
    public $idTab;

    /**
     * Is disabled
     */
    public $isDisabled;

    /**
     * Tab code
     */
    public $tabCode;

    /**
     * Tab description
     */
    public $tabDescription;

    /**
     * Title
     */
    public $title;

    /**
     * On change
     */
    public $onChange;

    /**
     * Visible on table
     */
    public $visibleOnTable;
}

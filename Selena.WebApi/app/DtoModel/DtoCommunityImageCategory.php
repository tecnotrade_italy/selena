<?php

namespace App\DtoModel;

class DtoCommunityImageCategory
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $order;
    public $online;
    public $description;
    
}
<?php

namespace App\DtoModel;

use App\Helpers\LogHelper;
use App\Helpers\ModelHelper;

class DtoMenu
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->languagesMenus = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Id page
     */
    public $idPage;

    /**
     * New window
     */
    public $newWindow;

    /**
     * Id icon
     */
    public $idIcon;

    /**
     * Hide mobile
     */
    public $hideMobile;

    /**
     * Languages menus
     */
    public $languagesMenus;

    /**
     * Order
     */
    public $order;

    /**
     * Id functionality
     */
    public $idFunctionality;
}

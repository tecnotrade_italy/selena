<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoInterventionsProcessing
{
    /**
     * Constructor
     */
    public function __construct()
    { 
      $this->arrayresult = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
      
        return ModelHelper::toArray($this);
    }

   
    /**
     * id
     */
    public $id;

    /**
     * description
     */
    public $description;

    /**
     * qta
     */
    public $qta;

   /**
     * n_u
     */
    public $n_u;


   /**
     * id_intervention
     */
    public $id_intervention;

   /**
     * id_gr_items
     */
    public $id_gr_items;

     /**
     * price_unit
     */
    public $price_list;
 
     /**
     * tot
     */
    public $tot;

    /**
     * code_gr
     */
    public $code_gr;
 /**
     * description_codeGr
     */
    public $description_codeGr;
    
 /**
     * data_agg
     */
    public $date_agg;

}

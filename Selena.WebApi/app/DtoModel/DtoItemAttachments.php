<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoItemAttachments
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * img
     */
    public $img;

    public $img_attachment;
    /**
     * item_id
     */
    public $item_id;
}

<?php

namespace App\DtoModel;

class DtoCartDetail
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    public $id;
    public $cart_id;
    public $nr_row;
    public $item_id;
    public $description;
    public $unit_of_measure_id;
    public $unit_price;
    public $quantity;
    public $discount_percentage;
    public $discount_value;
    public $vat_type_id;
    public $total_amount;
    public $note;
    public $net_amount;
    public $vat_amount;
    

}

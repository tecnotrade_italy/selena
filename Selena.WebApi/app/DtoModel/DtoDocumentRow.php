<?php

namespace App\DtoModel;

class DtoDocumentRow
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Item code
     */
    public $itemCode;

    /**
     * Item description
     */
    public $itemDescription;

    /**
     * Quantity
     */
    public $quantity;

    /**
     * Picked quantity
     */
    public $pickedQuantity;

    /**
     * Picked state
     */
    public $pickingState;

    /**
     * Cell
     */
    public $cell;

    /**
     * Document code
     */
    public $documentCode;
}

<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoContactRequest
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    /**
     * Name
     */
    public $name;

    /**
     * Surname
     */
    public $surname;

    /**
     * Email
     */
    public $email;

    /**
     * Phone number
     */
    public $phoneNumber;

    /**
     * Date
     */
    public $date;

    /**
     * Description
     */
    public $description;

    /**
     * Email to send
     */
    
    public $emailToSend;
    public $object;
    public $checkprivacy_cookie_policy;
    public $processing_of_personal_data;
    public $city; 
    public $link_user;
    public $countryofresidence;
}

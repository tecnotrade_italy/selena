<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoListGenerator
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ResultCost = collect();
        $this->ListUsers = collect();
        $this->ListGroup = collect();
        $this->ListCategory = collect();
        $this->ListDestinationUsers = collect();
        $this->ListDestinationGroup = collect();
        $this->ListDestinationCategory = collect();
        $this->ListScheduler = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id
     */
    public $id;

    public $id_destination_list;  
    public $id_starting_list;
    public $percent;
    public $value;

    public $check_starting_list;
    public $check_starting_base_price;
    public $check_starting_purchase_cost;
    public $check_starting_group;

    public $check_starting_customer;
    public $check_starting_categories;

    public $check_noprice_change;
    public $check_noprice_change_percentage;
    public $check_price_change_value;
    public $check_destination_baseprice;
    public $check_destination_list;

    public $check_destination_users;
    public $check_destination_groups;
    public $check_destination_categories;

    public $type_of_price_change;
    public $starting_price_type;
    public $destination_type;

    public $populate_only_if_empty;
    public $round_to_decimals;
    public $save_rule;
    public $description_save_rule;
    public $schedule;
    public $days_of_the_week;
    public $days_of_the_month;
    public $month;
    public $repeat_after_hour;
    public $repeat_after_min;
    public $repeat_times;
    public $description_destination_list;
    public $id_destination_price_list;

}

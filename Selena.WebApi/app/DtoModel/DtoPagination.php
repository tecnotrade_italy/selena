<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoPagination
{
    /**
     * Constructor
     */
    public function __construct()
    { }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * pageSelected
     */
    public $pageSelected;

    /**
     * totItems
     */
    public $totItems;

    /**
     * Description
     */
    public $description;


 /**
     * itemsPerPage
     */
    public $itemsPerPage;

    /**
     * html
     */
    public $html;

    /**
     * paginationActive
     */
    public $paginationActive;
    

}

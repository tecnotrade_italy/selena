<?php

namespace App\DtoModel;
use App\Helpers\ModelHelper;

class DtoBookingConnectionAddDayHour
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }
    
    public $id;
    public $monday_agg;
    public $tuesday_agg;
    public $wednesday_agg;
    public $thursday_agg;
    public $friday_agg;
    public $saturday_agg;
    public $sunday_agg;
    public $start_time_am_agg;
    public $end_time_am_agg;
    public $start_time_pm_agg;
    public $end_time_pm_agg;
    public $show_availability_calendar_agg;
    public $nr_place_agg;

}




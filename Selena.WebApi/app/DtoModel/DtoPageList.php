<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoPageList
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->menuList = collect();
        $this->pageList = collect();
    }

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Menu list
     */
    public $menuList;

    /**
     * Page list
     */
    public $pageList;
}

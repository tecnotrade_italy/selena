<?php

namespace App\DtoModel;

use App\Helpers\ModelHelper;

class DtoLanguageMenu
{
    /**
     * Constructor
     */
    public function __construct()
    {}

    /**
     * Convert dto to array
     *
     * @return array
     */
    public function toArray()
    {
        return ModelHelper::toArray($this);
    }

    /**
     * Id language
     */
    public $idLanguage;

    /**
     * Title
     */
    public $title;

    /**
     * Url
     */
    public $url;
}

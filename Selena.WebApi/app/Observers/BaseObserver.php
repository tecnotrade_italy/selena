<?php

namespace App\Observers;

use App\User;
use Illuminate\Support\Facades\Auth;

class BaseObserver
{
    /**
     * Listen to the model creating event.
     *
     * @param App $model
     * @return void
     */
    public function creating($model)
    {
        if (Auth::check()) {
            $model->created_id = Auth::user()->id;
        } else {
            $model->created_id = User::where('name', 'newsletter')->first()->id;
        }
    }

    /**
     * Listen to the model updating event.
     *
     * @param App $model
     * @return void
     */
    public function updating($model)
    {
        if (Auth::check()) {
            $model->updated_id = Auth::user()->id;
        }
    }

    // /**
    //  * Listen to the model deleted event.
    //  *
    //  * @param App $model
    //  * @return void
    //  */
    // public function deleting($model)
    // {
    //     if (Auth::check()) {
    //         $model->updated_id = Auth::user()->id;
    //     }
    // }
}

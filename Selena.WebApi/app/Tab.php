<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tab extends Model
{
    use ObservantTrait;

    protected $table = 'tabs';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}

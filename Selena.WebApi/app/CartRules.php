<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\HttpHelper;

class CartRules extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'carts_rules';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];

    /**
     * Get enabled_to with format
     */
    public function getEnabledToAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Get enabled_from with format
     */
    public function getEnabledFromAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set enabled_from from client format date to server format
     */
   /* public function setEnabledFromAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['enabled_from'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('d/m/Y H:i:s');
        }else{
            $this->attributes['enabled_from'] = NULL;
        }
    }*/

    /**
     * Set enabled_to from client format date to server format
     */
    /*public function setEnabledToAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['enabled_to'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('d/m/Y H:i:s');
        }else{
            $this->attributes['enabled_to'] = NULL;
        }
    }*/
}
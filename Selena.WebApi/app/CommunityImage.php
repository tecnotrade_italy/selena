<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;


class CommunityImage extends Model
{
    use ObservantTrait;

    protected $table = 'community_images';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];


    public function getUploadDateTimeAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set enabled_from from client format date to server format
     */
    public function setUploadDateTimeAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['upload_datetime'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }else{
            $this->attributes['upload_datetime'] = null;
        }
    }



}

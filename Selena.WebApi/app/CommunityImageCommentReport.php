<?php

namespace App;

use App\Helpers\LogHelper;
use Illuminate\Database\Eloquent\Model;

class CommunityImageCommentReport extends Model
{
    use ObservantTrait;

    protected $table = 'community_images_comments_reports';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}
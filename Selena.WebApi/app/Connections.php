<?php

namespace App;

use App\Helpers\HttpHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Connections extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'booking_connections';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
      /**
     * Get start_send_date_time with format
     */
  /*  public function getFixedDateAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }*/

    /**
     * Set start_send_date_time from client format date to server format
     */
    public function setFixedDateAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['fixed_date'] = Carbon::createFromFormat(HttpHelper::getLanguageDateFormat(), $value)->format('Y/m/d');
        }else{
            $this->attributes['fixed_date'] = NULL;
        }
    }

       /**
     * Get start_send_date_time with format
     */
   /* public function getDateFromAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }*/

    /**
     * Set start_send_date_time from client format date to server format
     */
    public function setDateFromAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['date_from'] = Carbon::createFromFormat(HttpHelper::getLanguageDateFormat(), $value)->format('Y/m/d');
        }else{
            $this->attributes['date_from'] = NULL;
        }
    }

       /**
     * Get start_send_date_time with format
     */
 /*   public function getDateToAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }*/

    /**
     * Set start_send_date_time from client format date to server format
     */
    public function setDateToAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['date_to'] = Carbon::createFromFormat(HttpHelper::getLanguageDateFormat(), $value)->format('Y/m/d');
        }else{
            $this->attributes['date_to'] = NULL;
        }
    }












}

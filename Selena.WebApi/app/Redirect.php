<?php

namespace App;

use App\Helpers\LogHelper;
use Illuminate\Database\Eloquent\Model;

class Redirect extends Model
{
    use ObservantTrait;
    /**
     * Table
     *
     * @var table
     */
    protected $table = 'redirect';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}


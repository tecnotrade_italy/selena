<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemAttachmentLanguageTypeTable extends Model
{
    use ObservantTrait;
    /**
     * Table
     *
     * @var table
     */
    protected $table = 'item_attachment_language_type_table';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}

<?php

namespace App;

use App\Helpers\LogHelper;
use Illuminate\Database\Eloquent\Model;

class CommunityFavoriteImage extends Model
{
    use ObservantTrait;

    protected $table = 'community_favorite_images';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}
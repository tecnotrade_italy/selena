<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupDisplayTechnicalSpecificationTechnicalSpecification extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'groups_display_technical_specification_tech_spec';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}

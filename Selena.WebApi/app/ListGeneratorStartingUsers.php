<?php

namespace App;

use App\Helpers\HttpHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ListGeneratorStartingUsers extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'list_generator_starting_users';


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}

<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class DocumentRowTypeEnum extends Enum
{
    /**
     * Item
     */
    const Item = 'Item';
}

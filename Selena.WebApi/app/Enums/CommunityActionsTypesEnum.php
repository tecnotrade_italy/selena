<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class CommunityActionsTypesEnum extends Enum
{

    const STAR1 = 'STAR1';
    const STAR2 = 'STAR2';
    const STAR3 = 'STAR3';
    const STAR4 = 'STAR4';
    const STAR5 = 'STAR5';
    const REPORTIMAGE = 'REPORTIMAGE';
    const ADDIMAGETOFAVORITE = 'ADDIMAGETOFAVORITE';
    const REMOVEIMAGEFROMFAVORITE = 'REMOVEIMAGEFROMFAVORITE';
    const ADDCOMMENT = 'ADDCOMMENT';
    const REPORTCOMMENT = 'REPORTCOMMENT';
    const ADDTOEDITORPICK = 'ADDTOEDITORPICK';

}
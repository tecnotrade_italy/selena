<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class DocumentTypeEnum extends Enum
{
    /**
     * Offer
     */
    const Offer = 'Offer';

    /**
     * Order
     */
    const Order = 'Order';

    /**
     * BeforeDDT
     */
    const BeforeDDT = 'BeforeDDT';

    /**
     * DDT
     */
    const DDT = 'DDT';
}

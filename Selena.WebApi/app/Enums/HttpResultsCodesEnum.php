<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class HttpResultsCodesEnum extends Enum
{
            /**
             * OK
             */
            const OK = 200;
            
            #region CLIENT ERROR from 400 to 500

            #region DEFAULT CLIENT

            /**
             * Bad request
             */
            const BadRequest = 400;

            /**
             * Unauthorized
             */
            const Unauthorized = 401;

            /**
             *  Payment required
             */
            const PaymentRequired = 402;

            /**
             * Forbidden
             */
            const Forbidden = 403;

            /**
             * Not found
             */
            const NotFound = 404;

            /**
             * Method not allowed
             */
            const MethodNotAllowed = 405;

            /**
             * Not acceptable
             */
            const NotAcceptable = 406;

            /**
             * Proxy authentication required
             */
            const ProxyAuthenticationRequired = 407;

            /**
             * Request time out
             */
            const RequestTimeOut = 408;

            /**
             * Conflit
             */
            const Conflit = 409;

            /**
             * Gone
             */
            const Gone = 410;

            /**
             * Length required
             */
            const LengthRequired = 411;

            /**
             * Precondition failed
             */
            const PreconditionFailed = 412;

            /**
             * Request entity too large
             */
            const RequestEntityTooLarge = 413;

            /**
             * Request uri too long
             */
            const RequestUriTooLong = 414;

            /**
             * Unsupported media type
             */
            const UnsupportedMediaType = 415;

            /**
             * Range not satisfiable
             */
            const RangeNotSatisfiable = 416;

            /**
             * Expectation failed
             */
            const ExpectationFailed = 417;

            /**
             * Im a teapot
             */
            const ImATeapot = 418;

            /**
             * Enhance your calm
             */
            const EnhanceYourCalm = 420;

            /**
             * Misdirected request
             */
            const MisdirectedRequest = 421;

            /**
             * Unprocessable entity
             */
            const UnprocessableEntity = 422;

            /**
             * Locked
             */
            const Locked = 423;

            /**
             * Failed dependency
             */
            const FailedDependency = 424;

            /**
             * Reserved for WebDAV
             */
            const ReservedForWebDAV = 425;

            /**
             * Upgrade required
             */
            const UpgradeRequired = 426;

            /**
             * Precondition required
             */
            const PreconditionRequired = 428;

            /**
             * Too many requests
             */
            const TooManyRequests = 429;

            /**
             * Request header fields too large
             */
            const RequestHeaderFieldsTooLarge = 431;

            /**
             * No response
             */
            const NoResponse = 444;

            /**
             * Retry with
             */
            const RetryWith = 449;

            /**
             * Blocked by windows parental controls
             */
            const BlockedByWindowsParentalControls = 450;

            /**
             * Unavailable for legal reasons
             */
            const UnavailableForLegalReasons = 451;

            /**
             * Client close request
             */
            const ClientCloseRequest = 499;

            #endregion DEFAULT CLIENT

            #region CUSTOM CLIENT

            /**
             * No data found
             */
            const NoDataFound = 452;

            #endregion CUSTOM CLIENT

            #endregion CLIENT ERROR from 400 to 500

            #region SERVER ERROR from 500 to 600

            #region DEFAULT SERVER

            /**
             * Internal server error
             */
            const InternalServerError = 500;

            /**
             * Not implemented
             */
            const NotImplemented = 501;

            /**
             * Bad gateway
             */
            const BadGateway = 502;

            /**
             * Service unavailable
             */
            const ServiceUnavailable = 503;

            /**
             * Gateway timeout
             */
            const GatewayTimeout = 504;

            /**
             * HTTP version not supported
             */
            const HTTPVersionNotSupported = 505;

            /**
             * Variant also negotiates
             */
            const VariantAlsoNegotiates = 506;

            /**
             * Insufficient storage
             */
            const InsufficientStorage = 507;

            /**
             * Loop detected
             */
            const LoopDetected = 508;

            /**
             * Bandwidth limit exceeded
             */
            const BandwidthLimitExceeded = 509;

            /**
             * Not extended
             */
            const NotExtended = 510;

            /**
             * Network authentication required
             */
            const NetworkAuthenticationRequired = 511;

            /**
             * Network read timeout error
             */
            const NetworkReadTimeoutError = 598;

            /**
             * Network connect timeout error
             */
            const NetworkConnectTimeoutError = 599;

            #endregion DEFAULT SERVER

            #region CUSTOM SERVER

            /**
             * DB error
             */
            const DbError = 511;

            /**
             * Invalid pay lad
             */
            const InvalidPayload = 513;

            #endregion CUSTOM SERVER

            #endregion SERVER ERROR from 500 to 600


            const PasswordWrong = 900;


}
<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class DictionariesCodesEnum extends Enum
{
    /**
     * The user name or password is incorrect
     */
    const WrongUsernameOrPassword = "WrongUsernameOrPassword";

    /**
     * Author not exist
     */
    const AuthorNotExist = "AuthorNotExist";

    /**
     * Icon not exist
     */
    const IconNotExist = "IconNotExist";

    /**
     * Function not valued
     */
    const FunctionNotValued = "FunctionNotValued";

    /**
     * Function not exist
     */
    const FunctionNotExist = "FunctionNotExist";

    /**
     * Page category not exist
     */
    const PageCategoryNotExist = "PageCategoryNotExist";

    /**
     * Page share type not exist
     */
    const PageShareTypeNotExist = "PageShareTypeNotExist";

    /**
     * Language not valued
     */
    const LanguageNotValued = "LanguageNotValued";

    /**
     * Language not exist
     */
    const LanguageNotExist = "LanguageNotExist";

    /**
     * Page not valued
     */
    const PageNotValued = "PageNotValued";

    /**
     * Page not exist
     */
    const PageNotExist = "PageNotExist";

    /**
     * Url already exist
     */
    const UrlAlreadyExist = "UrlAlreadyExist";

    /**
     * Url not found
     */
    const UrlNotFound = "UrlNotFound";

    /**
     * Title not found
     */
    const TitleNotFound = "TitleNotFound";

    /**
     * Id not valued
     */
    const IdNotValued = "IdNotValued";

    /**
     * Menu not exist
     */
    const MenuNotExist = "MenuNotExist";

    /**
     * More languageMenu for same language menu found
     */
    const MoreLanguageMenuForSameLanguageMenuFound = "MoreLanguageMenuForSameLanguageMenuFound";

    /**
     * More language page for same language page found
     */
    const MoreLanguagePageForSameLanguagePageFound = "MoreLanguagePageForSameLanguagePageFound";

    /**
     * More menu for page found
     */
    const MoreMenuForPageFound = "MoreMenuForPageFound";

    /**
     * Code not found
     */
    const CodeNotFound = "CodeNotFound";

    /**
     * Code not encoded
     */
    const CodeNotEncoded = "CodeNotEncoded";

    /**
     * Code already exist
     */
    const CodeAlreadyExist = "CodeAlreadyExist";

    /**
     * Value not found
     */
    const ValueNotFound = "ValueNotFound";

    /**
     * Setting not found
     */
    const SettingNotFound = "SettingNotFound";

    /**
     * User not enabled
     */
    const UserNotEnabled = "UserNotEnabled";

    /**
     * Techincal specification not found
     */
    const TechincalSpecificationNotFound = "TechincalSpecificationNotFound";

    /**
     * Group technical specification not found
     */
    const GroupTechnicalSpecificationNotFound = "GroupTechnicalSpecificationNotFound";

    /**
     * Group display techincal specification not found
     */
    const GroupDisplayTechincalSpecificationNotFound = "GroupDisplayTechincalSpecificationNotFound";

    /**
     * Group techincal specification techincal specification not found
     */
    const GroupTechincalSpecificationTechincalSpecificationNotFound = "GroupTechincalSpecificationTechincalSpecificationNotFound";

    /**
     * Item not found
     */
    const ItemNotFound = "ItemNotFound";

    /**
     * Item technical specification not found
     */
    const ItemTechnicalSpecificationNotFound = "ItemTechnicalSpecificationNotFound";

    /**
     * Item group technical specification not found
     */
    const ItemGroupTechnicalSpecificationNotFound = "ItemGroupTechnicalSpecificationNotFound";

    /**
     * Technical specification already exists
     */
    const TechnicalSpecificationAlreadyExists = "TechnicalSpecificationAlreadyExists";

    /**
     * Technical specification not empty
     */
    const TechnicalSpecificationNotEmpty = "TechnicalSpecificationNotEmpty";

    /**
     * Group technical specification already exists
     */
    const GroupTechnicalSpecificationAlreadyExists = "GroupTechnicalSpecificationAlreadyExists";

    /**
     * Group technical specification not empty
     */
    const GroupTechnicalSpecificationNotEmpty = "GroupTechnicalSpecificationNotEmpty";

    /**
     * Order not empty
     */
    const OrderNotEmpty = "OrderNotEmpty";

    /**
     * Group display techincal specification techincal specification not found
     */
    const GroupDisplayTechincalSpecificationTechincalSpecificationNotFound = "GroupDisplayTechincalSpecificationTechincalSpecificationNotFound";

    /**
     * Order only numeric
     */
    const OrderOnlyNumeric = "OrderOnlyNumeric";

    /**
     * Default language required
     */
    const DefaultLanguageRequired = "DefaultLanguageRequired";

    /**
     * Last
     */
    const Last = "Last";

    /**
     * Group display technical specification not empty
     */
    const GroupDisplayTechnicalSpecificationNotEmpty = "GroupDisplayTechnicalSpecificationNotEmpty";

    /**
     * Email not valid
     */
    const EmailNotValid = "EmailNotValid";

    /**
     * Username not valued
     */
    const UsernameNotValued = "UsernameNotValued";

    /**
     * Password not valued
     */
    const PasswordNotValued = "PasswordNotValued";
    /**
     * Username already exist
     */
    const UsernameAlreadyExist = "UsernameAlreadyExist";

    /**
     * Email already exist
     */
    const EmailAlreadyExist = "EmailAlreadyExist";

    /**
     * User not found
     */
    const UserNotFound = "UserNotFound";

    /**
     * Document row not exist
     */
    const DocumentRowNotExist = "DocumentRowNotExist";

    /**
     * Document head not exist
     */
    const DocumentHeadNotExist = "DocumentHeadNotExist";

    /**
     * More document found
     */
    const MoreDocumentFound = "MoreDocumentFound";

    /**
     * User not empty
     */
    const UserNotEmpty = "UserNotEmpty";

    /**
     * Role not empty
     */
    const RoleNotEmpty = "RoleNotEmpty";

    /**
     * Role not found
     */
    const RoleNotFound = "RoleNotFound";

    /**
     * Content not decode
     */
    const ContentNotDecode = "ContentNotDecode";

    /**
     * Content not found
     */
    const ContentNotFound = "ContentNotFound";

    /**
     * Content mismatch
     */
    const ContentMismatch = "ContentMismatch";

    /**
     * Content already exists
     */
    const ContentAlreadyExists = "ContentAlreadyExists";

    /**
     * File type not allowed
     */
    const FileTypeNotAllowed = "FileTypeNotAllowed";

    /**
     * Name file already exists
     */
    const NameFileAlreadyExists = "NameFileAlreadyExists";

    /**
     * Can't deleted image
     */
    const CantDeletedImage = "CantDeletedImage";

    /**
     * Description not found
     */
    const DescriptionNotFound = "DescriptionNotFound";

    /**
     * Template editor type not found
     */
    const TemplateEditorTypeNotFound = "TemplateEditorTypeNotFound";

    /**
     * Template editor group not found
     */
    const TemplateEditorGroupNotFound = "TemplateEditorGroupNotFound";

    /** Template editor not found */
    const TemplateEditorNotFound = "TemplateEditorNotFound";

    /**
     * Image not found
     */
    const ImageNotFound = "ImageNotFound";

    /**
     * Html not found
     */
    const HtmlNotFound = "HtmlNotFound";

    /**
     * Name not valued
     */
    const NameNotValued = "NameNotValued";

    /**
     * Surname not valued
     */
    const SurnameNotValued = "SurnameNotValued";

    /**
     * Email not valued
     */
    const EmailNotValued = "EmailNotValued";

    /**
     * Request not valued
     */
    const RequestNotValued = "RequestNotValued";

    /**
     * Contact not found
     */
    const ContactNotFound = "ContactNotFound";

    /**
     * Group newsletter not found
     */
    const GroupNewsletterNotFound = "GroupNewsletterNotFound";

    /**
     * Description not valued
     */
    const DescriptionNotValued = "DescriptionNotValued";

     /**
     * BusinessNameNotValued not valued
     */
    const BusinessNameNotValued = "BusinessNameNotValued";

       /**
     * CheckProcessingOfPersonalDataNotValued not valued
     */
    const CheckProcessingOfPersonalDataNotValued = "CheckProcessingOfPersonalDataNotValued";

    /**
     * Contact group newsletter not found
     */
    const ContactGroupNewsletterNotFound = "ContactGroupNewsletterNotFound";

    /**
     * Contact not valued
     */
    const ContactNotValued = "ContactNotValued";

    /**
     * Contact request not found
     */
    const ContactRequestNotFound = "ContactRequestNotFound";

    /**
     * Contact already exists
     */
    const ContactAlreadyExists = "ContactAlreadyExists";

    /**
     * Company not valued
     */
    const CompanyNotValued = "CompanyNotValued";

    /**
     * Customer request not found
     */
    const CustomerRequestNotFound = "CustomerRequestNotFound";

    /**
     * Newsletter not found
     */
    const NewsletterNotFound = "NewsletterNotFound";

    /**
     * Newsletter not valued
     */
    const NewsletterNotValued = "NewsletterNotValued";

    /**
     * Customer not found
     */
    const CustomerNotFound = "CustomerNotFound";

    /**
     * Customer not valued
     */
    const CustomerNotValued = "CustomerNotValued";

    /**
     * GroupNewsletter not valued
     */
    const GroupNewsletterNotValued = "GroupNewsletterNotValued";

    /**
     * Newsletter recipient not found
     */
    const NewsletterRecipientNotFound = "NewsletterRecipientNotFound";

    /**
     * View not set
     */
    const ViewNotSet = "ViewNotSet";

    /**
     * Back ground visitors views empty
     */
    const BackGroundVisitorsViewsEmpty = "BackGroundVisitorsViewsEmpty";

    /**
     * Border empty visitors views
     */
    const BorderVisitorsViewsEmpty = "BorderVisitorsViewsEmpty";

    /**
     * Back ground pages views empty
     */
    const BackGroundPagesViewsEmpty = "BackGroundPagesViewsEmpty";

    /**
     * Border empty pages views
     */
    const BorderPagesViewsEmpty = "BorderPagesViewsEmpty";

    /**
     * Back ground device session empty
     */
    const BackGroundDeviceSessionEmpty = "BackGroundDeviceSessionEmpty";

    /**
     * Back ground source session empty
     */
    const BackGroundSourceSessionEmpty = "BackGroundSourceSessionEmpty";

    /**
     * Google analytics view not found
     */
    const GoogleAnalyticsViewNotFound = "GoogleAnalyticsViewNotFound";

    /**
     * Disiscrizione avvenuta correttamente
     */
    const UnsubscribeContactCompleted = "UnsubscribeContactCompleted";

    /**
     * Iscrizione avvenuta correttamente
     */
    const SubscribeContactCompleted = "SubscribeContactCompleted";

    /**
     * Categoria non esistente
     */
    const CategoriesNotExist = "CategoriesNotExist";

    /**
     * Caratteristica Categoria non esistente
     */
    const CharacteristicNotValued = "CharacteristicNotValued";
    
    /**
     * Link Categoria non trovato
     */
    const LinkNotValued = "LinkNotValued";

    /**
     * Metatag keywords Categoria non trovato
     */
    const MetaTagKeywordNotValued = "MetaTagKeywordNotValued";

    /**
     * Metatag title categoria non trovato
     */
    const MetaTagTTitleNotValued = "MetaTagTTitleNotValued";
    
    /**
     * Ordinamento categoria non trovato
     */
    const OrderNotValued = "OrderNotValued";

    /**
     * Tipo categoria non trovato
     */
    const CategoryTipeIdNotValued = "CategoryTipeIdNotValued";
    
    /**
     * Categoria padre non trovato
     */
    const CategoryFatherIdNotValued = "CategoryFatherIdNotValued";

  /**
     * AddressNotFound
     */
    const AddressNotFound = "AddressNotFound";

    /**
     * Resort not found
     */
    const ResortNotFound = "ResortNotFound";


    /**
     * Province not found
     */
    const ProvinceNotFound = "ProvinceNotFound";

    /**
     * PostalCodeNotFound not found
     */
    const PostalCodeNotFound = "PostalCodeNotFound";

    /**
     * TelephoneNumberNotFound not found
     */
    const TelephoneNumberNotFound = "TelephoneNumberNotFound";

    /**
     * EmailNotFound not found
     */
    const EmailNotFound = "EmailNotFound"; 

    /**
     * onlineNotFound not found
     */
    const onlineNotFound = "onlineNotFound"; 

    /**
     * PeopleRequestNotFound
     */

    const PeopleRequestNotFound ="PeopleRequestNotFound";

    /**
     * ContactNegotiationCompleted
     */

    const ContactNegotiationCompleted ="ContactNegotiationCompleted";
    /**
     * UserShopNotValued
     */

    const UserShopNotValued ="UserShopNotValued";
    /**
     * UserNegotiatiorNotValued
     */

    const UserNegotiatiorNotValued ="UserNegotiatiorNotValued";

    /**
     * MessageNotValued
     */

    const MessageNotValued ="MessageNotValued";

    const PasswordWrong ="PasswordWrong";
    
    const PasswordValue ="PasswordValue";

    const VatNumberAlreadyExist ="VatNumberAlreadyExist";

    const FiscalCodeAlreadyExist ="FiscalCodeAlreadyExist";

    const DurationNotValued = "DurationNotValued";
    const SlotStepNotValued = "SlotStepNotValued";
    const BlockAfterNotValued = "BlockAfterNotValued";
    const PriceNotValued = "PriceNotValued";
    const BlockFirstNotFound = "BlockFirstNotFound";
    const PlaceNotFound = "PlaceNotFound";
    const NationNotValued = "NationNotValued";
    const PhoneNotValued = "PhoneNotValued"; 
}

<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class MenusTypesEnum extends Enum
{
    /**
     * Page
     */
    const Page = 0;

    /**
     * Link
     */
    const Link = 1;

    /**
     * Folder
     */
    const Folder = 2;
}
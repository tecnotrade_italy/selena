<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class DbOperationsTypesEnum extends Enum
{
    /**
     * Insert
     */
    const INSERT = 'INSERT';

    /**
     * Update
     */
    const UPDATE = 'UPDATE';

    /**
     * Delete
     */
    const DELETE = 'DELETE';
}
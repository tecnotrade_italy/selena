<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class ContentEnum extends Enum
{
    /**
    * Header
    */
    const Header = 'Header';

    /**
    * Footer
    */
    const Footer = 'Footer';

    /**
    * Categories
    */
    const Categories = 'Categories';

    /**
    * Items
    */
    const Items = 'Items';

    /**
    *  PageCategories
    */
    const PageCategories = 'PageCategories';

    /**
    *  PageItems
    */
    const PageItems = 'PageItems';

    /**
    *  UserDetail
    */
    const UserDetail = 'UserDetail';

 /**
    *  UsersDetail
    */
    const UsersDetail = 'UsersDetail';

 /**
    *  ShopDetail
    */
    const ShopDetail = 'ShopDetail';

/**
    *  ShopItemDetail
    */
    const ShopItemDetail = 'ShopItemDetail';

    /**
    *  ImgAggUser
    */
    const ImgAgg = 'ImgAggUser';

    /**
    *  ImgAggItem
    */
    const ImgAggItem = 'ImgAggItem';

    /**
    *  UserShop
    */
    const UserShop = 'UserShop';
    
    /**
    *  UserShopCategoriesTabs
    */
    const UserShopCategoriesTabs = 'UserShopCategoriesTabs';

    /**
    *  UserShopContentTabs
    */
    const UserShopContentTabs = 'UserShopContentTabs';

        /**
    *  Cart_Detail
    */
    const Cart_Detail = 'Cart_Detail';

        /**
    *  Cart_Summary
    */
    const Cart_Summary = 'Cart_Summary';
    
     /**
    *  Cart_Detail_Single_Row
    */
    const Cart_Detail_Single_Row = 'Cart_Detail_Single_Row';

    /**
    *  Sales_Order_Detail_Single_Row
    */
    const Sales_Order_Detail_Single_Row = 'Sales_Order_Detail_Single_Row';
    
    
    /*
    * DetailShopUser
    */
    const DetailShopUser = 'DetailShopUser';


    /*
    * UserSala
    */

    const UserSala = 'UserSala';

 /*
    * DetailAdress
    */
const DetailAdress = 'DetailAdress';

    /*
    * CartUsersAddresses
    */
    const CartUsersAddresses = 'CartUsersAddresses';

    /*
    * ListUsersAddresses
    */
    const ListUsersAddresses = 'ListUsersAddresses';

    /*
    * FavoritesUserItems
    */
    const FavoritesUserItems = 'FavoritesUserItems';
   
      /*
    * ListSalesOrder
    */
    const ListSalesOrder = 'ListSalesOrder';
    
    /*
    * ListQuotes
    */
    const ListQuotes = 'ListQuotes';
    
   /*
    *     DetailSalesOrders
    */
    const DetailSalesOrders = 'DetailSalesOrders'; 

/*
    *     DetailSalesOrderUserData
    */
    const DetailSalesOrderUserData = 'DetailSalesOrderUserData'; 

/*
    *     DetailShipmentData
    */
    const DetailShipmentData = 'DetailShipmentData';    

/*
    *     DetailGoodShipmentData
    */
    const DetailGoodShipmentData = 'DetailGoodShipmentData'; 

/*
    *     OrderDataShippingDocuments
    */
    const OrderDataShippingDocuments = 'OrderDataShippingDocuments'; 

/*
    *     SummaryDescriptionDocument
    */
    const SummaryDescriptionDocument = 'SummaryDescriptionDocument'; 

/*
    *     DetailRowOrder
    */
    const DetailRowOrder = 'DetailRowOrder'; 
    
    /*
    * UserShortcodeHeaderLoggedIn
    */
    const UserShortcodeHeaderLoggedIn = 'UserShortcodeHeaderLoggedIn'; 

    /*
    * UserShortcodeHeaderLoggedOut
    */
    const UserShortcodeHeaderLoggedOut = 'UserShortcodeHeaderLoggedOut'; 

    /*
    * SalesmanShortcodeHeaderLoggedIn
    */
    const SalesmanShortcodeHeaderLoggedIn = 'SalesmanShortcodeHeaderLoggedIn'; 

    /*
    * SalesmanShortcodeHeaderLoggedOut
    */
    const SalesmanShortcodeHeaderLoggedOut = 'SalesmanShortcodeHeaderLoggedOut'; 

    /*
    * BlogListArticle
    */
    const BlogListArticle = 'BlogListArticle'; 

    /*
    * NewsListArticle
    */
    const NewsListArticle = 'NewsListArticle'; 
    
    /*
    *     SummaryAmountOrder
    */
    const SummaryAmountOrder = 'SummaryAmountOrder';
    
    /*
    *     DetailMailTableOrderSales
    */
    const DetailMailTableOrderSales = 'DetailMailTableOrderSales';
    
    /*
    *     GrSearchListItems
    */
    const GrSearchListItems = 'GrSearchListItems';

    /*
    *     DetailGrItemsSearch
    */
    const DetailGrItemsSearch = 'DetailGrItemsSearch';
    
    /*
    *     HeaderArticle
    */
    const HeaderArticle = 'HeaderArticle';
    
    /*
    *     Sidebar
    */
    const Sidebar = 'Sidebar';

    /*
    *     CartDetailOptional
    */
    const CartDetailOptional = 'CartDetailOptional';
    const SalesDetailOptional = 'SalesDetailOptional';
    const ArticlesList = 'ArticlesList';
    const GroupTechnicalsSpecifications = 'GroupTechnicalsSpecifications';
    const ItemGroupTechnicalsSpecifications = 'ItemGroupTechnicalsSpecifications';
    const SalesOrderDetailTokenUrl = 'SalesOrderDetailTokenUrl';
    const UploadFileDetail = 'UploadFileDetail';
    const UploadFileNew = 'UploadFileNew';
    const PagePrices = 'PagePrices';
    const Price_Detail_Single_Row = 'Price_Detail_Single_Row';
    const Community_Image_Detail = 'Community_Image_Detail';
    const Community_Image_Comment = 'Community_Image_Comment';
    const User_Profile_Page = 'User_Profile_Page';
    const ItemGalleryPage = 'ItemGalleryPage';
    const Community_Image_Comment_Homepage = 'Community_Image_Comment_Homepage';
    const BookingConnectionsDate ='BookingConnectionsDate';
    const BookingConnectionsResult ='BookingConnectionsResult'; 
    const BookingConnectionsResultSingolaData ='BookingConnectionsResultSingolaData'; 
    const DetailOptionBooking ='DetailOptionBooking'; 
    const ResultEmailGrQuotes ='ResultEmailGrQuotes';
    const BlogArticlesPage ='BlogArticlesPage';
    const ItemAttachmentType ='ItemAttachmentType';
    const ItemAttachmentFile ='ItemAttachmentFile';
    const BlogArticlesCategoryList = 'BlogArticlesCategoryList';
    const BlogListCategory= 'BlogListCategory';
    const ContentHtmlBlogArticlesCategoryList = 'ContentHtmlBlogArticlesCategoryList';
    const UserList = 'UserList';
    const ListAttachmentUser ='ListAttachmentUser';
    const FormBuilderField ='FormBuilderField';
    const FormBuilderFieldType ='FormBuilderFieldType';
    const CategoriesFilter ='CategoriesFilter';
    const ArticlesListAdd = 'ArticlesListAdd';
    
    

}

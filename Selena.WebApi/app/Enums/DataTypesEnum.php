<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class DataTypesEnum extends Enum
{
    /**
     * Number
     */
    const Number = 0;

    /**
     * String
     */
    const String = 1;

    /**
     * Date
     */
    const Date = 2;

    /**
     * Bool
     */
    const Bool = 3;
}

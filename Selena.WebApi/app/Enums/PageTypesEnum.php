<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class PageTypesEnum extends Enum
{
    /**
     * Page
     */
    const Page = "Page";

    /**
     * News
     */
    const News = "News";

    /**
     * Blog
     */
    const Blog = "Blog";

}

<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

class GoogleAnalyticsEnum extends Enum
{
    /**
     * Avg session duration
     */
    const AvgSessionDuration = 'AvgSessionDuration';

    /**
     * Bounce rate
     */
    const BounceRate = 'BounceRate';

    /**
     * Device session
     */
    const DeviceSession = 'DeviceSession';

    /**
     * Most visited pages
     */
    const MostVisitedPages = 'MostVisitedPages';

    /**
     * Page view per session
     */
    const PageViewPerSession = 'PageViewPerSession';

    /**
     * Pages views
     */
    const PagesViews = 'PagesViews';

    /**
     * Source session
     */
    const SourceSession = 'SourceSession';

    /**
     * Source session
     */
    const ReturningVisitors = 'ReturningVisitors';

    /**
     * Total page view
     */
    const TotalPageView = 'TotalPageView';

    /**
     * Total session
     */
    const TotalSession = 'TotalSession';

    /**
     * Total user
     */
    const TotalUser = 'TotalUser';

    /**
     * User active real time
     */
    const UserActiveRealTime = 'UserActiveRealTime';

    /**
     * Visitors ciews
     */
    const VisitorsViews = 'VisitorsViews';
}

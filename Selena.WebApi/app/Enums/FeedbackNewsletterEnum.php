<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

class FeedbackNewsletterEnum extends Enum
{
    /**
     * Error
     */
    const Error = 'ERROR';

    /**
     * Sent
     */
    const Sent = 'SENT';

    /**
     * Open
     */
    const Open = 'OPEN';
}

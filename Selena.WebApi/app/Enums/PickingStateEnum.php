<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class PickingStateEnum extends Enum
{
    /**
     * None
     */
    const None = 0;

    /**
     * Partial
     */
    const Partial = 1;

    /**
     * All
     */
    const All = 2;
}

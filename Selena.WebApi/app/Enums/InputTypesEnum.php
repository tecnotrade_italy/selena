<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class InputTypesEnum extends Enum
{
    /**
     * Input
     */
    const Input = 0;

    /**
     * TextArea
     */
    const TextArea = 1;

    /**
     * Select
     */
    const Select = 2;

    /**
     * DatePicker
     */
    const DatePicker = 3;

    /**
     * DateTimePicker
     */
    const DateTimePicker = 4;

    /**
     * CheckBox
     */
    const CheckBox = 5;

    /**
     * Radio
     */
    const Radio = 6;

    /**
     * Numeric
     */
    const Numeric = 7;

    /**
     * PhoneText
     */
    const PhoneText = 8;

    /**
     * EmailText
     */
    const EmailText = 9;

    /**
     * Popup
     */
    const Popup = 10;

    /**
     * Button
     */
    const Button = 11;

    /**
     * Hidden
     */
    const Hidden = 12;

    /**
     * Table checkbox
     */
    const TableCheckbox = 13;

    /**
     * Command formatter
     */
    const CommandFormatter = 14;

    /**
     * Label
     */
    const Label = 15;

    /**
     * Password
     */
    const Password = 16;

    /**
     * Editor
     */
    const Editor = 17;

    /**
     * Image
     */
    const Image = 18;


    /**
     * Input no render
     */
    const NoRender = 19;


    /**
     * Code editor
     */
    const CodeEditor = 20;
}

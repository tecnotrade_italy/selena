<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

class SettingEnum extends Enum
{
    /**
     * CopyContentDefaultLanguage
     */
    const CopyContentDefaultLanguage = "CopyContentDefaultLanguage";

    /**
     * CopySettingDefaultLanguage
     */
    const CopySettingDefaultLanguage = "CopySettingDefaultLanguage";

    /**
     * Last date sync
     */
    const LastDateSync = "LastDateSync";

    /**
     * Menu Render
     */
    const MenuRender = "MenuRender";

    /**
     * Home Blog List
     */
    const HomeBlogList = "HomeBlogList";

    /**
     * Home News List
     */
    const HomeNewsList = "HomeNewsList";

    /**
     * Blog List
     */
    const BlogList = "BlogList";

    /**
     * News List
     */
    const NewsList = "NewsList";

    /**
     * Blog Render
     */
    const BlogRender = "BlogRender";

    /**
     * News Render
     */
    const NewsRender = "NewsRender";

    /**
     * Contacts Email
     */
    const ContactsEmail = "ContactsEmail";

    /**
     * Google analytics viewId
     */
    const GoogleAnalyticsViewId = "GoogleAnalyticsViewId";

    /**
     * Background color chart visitors views
     */
    const BackgroundColorChartVisitorsViews = "BackgroundColorChartVisitorsViews";

    /**
     * Border color chart visitors views
     */
    const BorderColorChartVisitorsViews = "BorderColorChartVisitorsViews";

    /**
     * Background color chart pages views
     */
    const BackgroundColorChartPagesViews = "BackgroundColorChartPagesViews";

    /**
     * Border color chart pages views
     */
    const BorderColorChartPagesViews = "BorderColorChartPagesViews";

    /**
     * Background color chart device session
     */
    const BackgroundColorChartDeviceSession = "BackgroundColorChartDeviceSession";

    /**
     * Background color chart source session
     */
    const BackgroundColorChartSourceSession = "BackgroundColorChartSourceSession";

    /**
     * Background color chart returning visitors
     */
    const BackgroundColorChartReturningVisitors = "BackgroundColorChartReturningVisitors";

    /**
     * Google analytics trackId
     */
    const GoogleAnalyticsTrackId = "GoogleAnalyticsTrackId";

    /**
    * Facebook track Id
    */
    const FacebookTrackId = "FacebookTrackId";

    /**
    * Number of items per page
    */
    const NumberOfItemsPerPage = "NumberOfItemsPerPage";
    
    /**
    * Item price with or without VAT
    */
    const PriceIncludingVat = "PriceIncludingVat";

    const Sales_Order_Confirmation_Page = "Sales_Order_Confirmation_Page";

    const DefaultShipmentPrice = "DefaultShipmentPrice";


    const SidebarVisible = "SidebarVisible";

    const SidebarPosition = "SidebarPosition";

    const SidebarWidth = "SidebarWidth";

    const SidebarWidthSmall = "SidebarWidthSmall";
    
    const SidebarWidthMedium = "SidebarWidthMedium";

    const UsernameProviderMessaggi = "UsernameProviderMessaggi";

    const PasswordProviderMessaggi = "PasswordProviderMessaggi";

    const SenderProviderMessaggi = "SenderProviderMessaggi";

    const TokenAPITinyurl = "TokenAPITinyurl";

    const NewsletterBlockItems = "NewsletterBlockItems";

    const NewsletterBlockDelay = "NewsletterBlockDelay";

    const ClientIdPayPal = "ClientIdPayPal";

    const SecretPayPal = "SecretPayPal";
    
    const TestModePayPal = "TestModePayPal";

    const DisplayPrice = "DisplayPrice";
    
    const DateFormat = "DateFormat";

    const MailHost = "MailHost";
    const MailPort = "MailPort";
    const MailUsername = "MailUsername";
    const MailPassword = "MailPassword";
    const MailEncryption = "MailEncryption";
    const MailToErrorNewsletter = "MailToErrorNewsletter";
    const AppIdFacebook = "AppIdFacebook";
    const AppIdGoogle = "AppIdGoogle";

    const DefaultUserOnline = "DefaultUserOnline";
    const SendOrderEmailToSalesman = "SendOrderEmailToSalesman";
    const EmailOrder = "EmailOrder";
    const EmailNewUser = "EmailNewUser";
    const ProtectFile = "ProtectFile";

}

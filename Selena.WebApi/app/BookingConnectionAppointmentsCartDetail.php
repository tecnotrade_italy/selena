<?php

namespace App;

use App\Helpers\LogHelper;
use Carbon\Carbon;
use App\Helpers\HttpHelper;
use Illuminate\Database\Eloquent\Model;

class BookingConnectionAppointmentsCartDetail extends Model
{
    use ObservantTrait;

    protected $table = 'booking_connection_appointments_cart_detail';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];


}

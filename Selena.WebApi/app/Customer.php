<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;

class Customer extends Model
{
    use ObservantTrait;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];

    #region DATE FORMAT FIELD

    /**
     * Get enable_from with format
     */
    public function getEnableFromAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set enable_from from client format date to server format
     */
    public function setEnableFromAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['enable_from'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }
    }

    /**
     * Get enable_to with format
     */
    public function getEnableToAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }

    /**
     * Set enable_from from client format date to server format
     */
    public function setEnableToAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['enable_to'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }
    }

    #endregion DATE FORMAT FIELD
}

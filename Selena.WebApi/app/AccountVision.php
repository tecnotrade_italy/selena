<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;

class AccountVision extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'account_vision';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];


    /**
     * Get date_ddt with format
     */
    public function getUpdateDateAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateFormat());
        }
    }

    /**
     * Set date_ddt from client format date to server format
     */
    public function setUpdateDateAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['subscription_date'] = Carbon::createFromFormat(HttpHelper::getLanguageDateFormat(), $value)->format('m/d/Y');
            $this->attributes['expiration_date'] = Carbon::createFromFormat(HttpHelper::getLanguageDateFormat(), $value)->format('m/d/Y');
        }
    }
}

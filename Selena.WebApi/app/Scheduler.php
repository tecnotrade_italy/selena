<?php

namespace App;
use Carbon\Carbon;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;

use Illuminate\Database\Eloquent\Model;

class Scheduler extends Model
{
    use ObservantTrait;
    /**
     * Table
     *
     * @var table
     */
    protected $table = 'scheduler';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];



     /**
     * Get start_time_action with format
     */
    /*public function getStartTimeActionAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateTimeFormat());
        }
    }*/

    /**
     * Set start_time_action from client format date to server format
     */
    /*public function setStartTimeActionAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['start_time_action'] = Carbon::createFromFormat(HttpHelper::getLanguageDateTimeFormat(), $value)->format('Y/m/d H:i:s');
        }else{
            $this->attributes['start_time_action'] = null;
        }
    }*/
}

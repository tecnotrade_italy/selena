<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemGroupTechnicalSpecification extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'items_groups_technicals_specifications';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}

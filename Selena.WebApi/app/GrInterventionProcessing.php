<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Helpers\HttpHelper;
use App\Helpers\LogHelper;

class GrInterventionProcessing extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'gr_intervention_processing';



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at'
    ];

    /**
     * Get date_ddt with format
     */
    public function getDateDdtAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateFormat());
        }
    }

    /**
     * Set date_ddt from client format date to server format
     */
    public function setDateDdtAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['date_ddt'] = Carbon::createFromFormat(HttpHelper::getLanguageDateFormat(), $value)->format('m/d/Y');
        }
    }

    /**
     * Get date_ddt_gr with format
     */
    public function getDateDdtGrAttribute($value)
    {
        if (!is_null($value)) {
            return Carbon::parse($value)->format(HttpHelper::getLanguageDateFormat());
        }
    }

    /**
     * Set date_ddt_gr from client format date to server format
     */
    public function setDateDdtGrAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['date_ddt_gr'] = Carbon::createFromFormat(HttpHelper::getLanguageDateFormat(), $value)->format('m/d/Y');
        }
    }

    #endregion DATE FORMAT FIELD
}

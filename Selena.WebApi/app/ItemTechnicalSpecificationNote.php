<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemTechnicalSpecificationNote extends Model
{
    use ObservantTrait;

    /**
     * Table
     *
     * @var table
     */
    protected $table = 'items_technicals_specifications_notes';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_id', 'created_at', 'updated_id', 'updated_at',
    ];
}

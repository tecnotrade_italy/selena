/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["getByViewValue"] = getByViewValue;
/* harmony export (immutable) */ __webpack_exports__["getByViewQuotes"] = getByViewQuotes;
/* harmony export (immutable) */ __webpack_exports__["getByViewExternalRepair"] = getByViewExternalRepair;
/* harmony export (immutable) */ __webpack_exports__["getByIdUser"] = getByIdUser;
/* harmony export (immutable) */ __webpack_exports__["getDescriptionAndPriceProcessingSheet"] = getDescriptionAndPriceProcessingSheet;
/* harmony export (immutable) */ __webpack_exports__["getnumbertelephone"] = getnumbertelephone;
/* harmony export (immutable) */ __webpack_exports__["getLastId"] = getLastId;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["getAllIntervention"] = getAllIntervention;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getWithGroupTechnicalSpecification"] = getWithGroupTechnicalSpecification;
/* harmony export (immutable) */ __webpack_exports__["getGraphic"] = getGraphic;
/* harmony export (immutable) */ __webpack_exports__["count"] = count;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["insertModule"] = insertModule;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["updateIntervention"] = updateIntervention;
/* harmony export (immutable) */ __webpack_exports__["updateInterventionUpdateProcessingSheet"] = updateInterventionUpdateProcessingSheet;
/* harmony export (immutable) */ __webpack_exports__["updateInterventionUpdateViewQuotes"] = updateInterventionUpdateViewQuotes;
/* harmony export (immutable) */ __webpack_exports__["updateExternalRepair"] = updateExternalRepair;
/* harmony export (immutable) */ __webpack_exports__["updateCheck"] = updateCheck;
/* harmony export (immutable) */ __webpack_exports__["updateUser"] = updateUser;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["clone"] = clone;
/** *   InterventionsServices   ***/

/** *   GET   ***/

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/intervention/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/intervention/select?search=' + search, fnSuccess, fnError);
}
function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/intervention/' + id, fnSuccess, fnError);
}

function getByViewValue(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/intervention/getByViewValue/' + id, fnSuccess, fnError);
}
function getByViewQuotes(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/intervention/getByViewQuotes/' + id, fnSuccess, fnError);
}
function getByViewExternalRepair(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/intervention/getByViewExternalRepair/' + id, fnSuccess, fnError);
}

function getByIdUser(user_id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/intervention/getByIdUser/' + user_id, fnSuccess, fnError);
}

function getDescriptionAndPriceProcessingSheet(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/intervention/getDescriptionAndPriceProcessingSheet/' + id, fnSuccess, fnError);
}
function getnumbertelephone(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/intervention/getnumbertelephone/' + id, fnSuccess, fnError);
}
function getLastId(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/intervention/getLastId', fnSuccess, fnError);
}

function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/intervention/all/' + idLanguage, fnSuccess, fnError);
}

function getAllIntervention(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/intervention/getAllIntervention/' + idLanguage, fnSuccess, fnError);
}

function get(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/intervention', fnSuccess, fnError);
}

function getWithGroupTechnicalSpecification(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/intervention/withGroupTechnicalSpecification', fnSuccess, fnError);
}

function getGraphic(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/intervention/getGraphic', fnSuccess, fnError);
}

function count(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/intervention/count', fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(DtoInterventions, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/intervention', DtoInterventions, fnSuccess, fnError);
}

function insertModule(DtoInterventions, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/intervention/insertModule', DtoInterventions, fnSuccess, fnError);
}
/** *   END PUT   ***/

/** *   POST   ***/

function update(DtoInterventions, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/intervention', DtoInterventions, fnSuccess, fnError);
}

function updateIntervention(DtoInterventions, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/intervention/updateIntervention', DtoInterventions, fnSuccess, fnError);
}

function updateInterventionUpdateProcessingSheet(DtoInterventions, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/intervention/updateInterventionUpdateProcessingSheet', DtoInterventions, fnSuccess, fnError);
}

function updateInterventionUpdateViewQuotes(DtoQuotes, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/intervention/updateInterventionUpdateViewQuotes', DtoQuotes, fnSuccess, fnError);
}

function updateExternalRepair(DtoExternalRepair, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/intervention/updateExternalRepair', DtoExternalRepair, fnSuccess, fnError);
}
function updateCheck(DtoInterventions, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/intervention/updateCheck', DtoInterventions, fnSuccess, fnError);
}
/** *   END POST   ***/

function updateUser(DtoUser, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/intervention/updateUser', DtoUser, fnSuccess, fnError);
}

/** * DELETE ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/intervention/' + id, fnSuccess, fnError);
}

/** * DELETE ***/

/** *   clone ***/
function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/intervention/clone/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), null, fnSuccess, fnError);
}
/** *   END  clone ***/

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(2);


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

/***   Data   ***/
window.configData = __webpack_require__(3);
window.notificationData = __webpack_require__(4);
window.storageData = __webpack_require__(5);

/***   Helpers   ***/
window.calendarHelpers = __webpack_require__(6);
window.configurationHelpers = __webpack_require__(7);
window.contactHelpers = __webpack_require__(8);
window.dataHelpers = __webpack_require__(9);
window.dictionaryHelpers = __webpack_require__(10);
window.failServiceHelpers = __webpack_require__(11);
window.functionHelpers = __webpack_require__(12);
window.groupDisplayTechnicalSpecificationHelpers = __webpack_require__(13);
window.groupTechnicalSpecificationHelpers = __webpack_require__(14);
window.searchGrItemsHelpers = __webpack_require__(15);
window.interventionHelpers = __webpack_require__(16);
//window.processingviewHelpers = require("./helpers/processingview.js");
//window.quotesHelpers = require("./helpers/quotes.js");
//window.externalRepairHelpers = require("./helpers/externalRepair.js");
window.peopleHelpers = __webpack_require__(17);
window.adsHelpers = __webpack_require__(18);
window.languageHelpers = __webpack_require__(19);
window.loginHelpers = __webpack_require__(20);
window.menuHelpers = __webpack_require__(21);
window.negotiationHelpers = __webpack_require__(22);
window.notificationHelpers = __webpack_require__(23);
window.pageHelpers = __webpack_require__(24);
window.serviceHelpers = __webpack_require__(25);
window.searchHelpers = __webpack_require__(26);
window.spinnerHelpers = __webpack_require__(27);
window.tableHelpers = __webpack_require__(28);
window.technicalSpecificationHelpers = __webpack_require__(29);
window.userHelpers = __webpack_require__(30);
window.validationHelpers = __webpack_require__(31);
window.personaldataHelpers = __webpack_require__(32);
window.itemsHelpers = __webpack_require__(33);
window.searchShopHelpers = __webpack_require__(34);
window.statesHelpers = __webpack_require__(35);
window.rolesHelpers = __webpack_require__(36);
window.itemsGrHelpers = __webpack_require__(37);
window.searchLiutaioHelpers = __webpack_require__(38);
window.searchSalaRegistrazioneHelpers = __webpack_require__(39);
window.searchInterventionHelpers = __webpack_require__(40);
window.searchUserHelpers = __webpack_require__(41);
window.searchItemsHelpers = __webpack_require__(42);
window.fasturgenciesHelpers = __webpack_require__(43);
window.internalRepairHelpers = __webpack_require__(44);
window.interventioncustomerHelpers = __webpack_require__(45);
window.userAddressHelpers = __webpack_require__(46);
window.salesOrderHelpers = __webpack_require__(47);
//window.attachmentFileUserHelpers = require("./helpers/attachmentFileUser.js");
window.cartHelpers = __webpack_require__(48);
window.cartDetailHelpers = __webpack_require__(49);
window.searchDataSheetHelpers = __webpack_require__(50);
//window.downloadHelpers = require("./helpers/download.js");

/***   Services   ***/
window.adminConfigurationServices = __webpack_require__(51);
window.carriageServices = __webpack_require__(52);
window.carrierServices = __webpack_require__(53);
window.categoriesServices = __webpack_require__(54);
window.categoryTypeServices = __webpack_require__(55);
window.contactServices = __webpack_require__(56);
window.contactGroupNewsletterServices = __webpack_require__(57);
window.customerGroupNewsletterServices = __webpack_require__(58);
window.contactRequestServices = __webpack_require__(59);
window.contentServices = __webpack_require__(60);
window.customerServices = __webpack_require__(61);
window.dictionaryServices = __webpack_require__(62);
window.documentHeadServices = __webpack_require__(63);
window.documentRowServices = __webpack_require__(64);
window.feedbackNewsletterServices = __webpack_require__(65);
window.googleAnalyticsServices = __webpack_require__(66);
window.groupDisplayTechnicalSpecificationServices = __webpack_require__(67);
window.groupTechnicalSpecificationServices = __webpack_require__(68);
window.groupNewsletterServices = __webpack_require__(69);
window.iconServices = __webpack_require__(70);
window.itemServices = __webpack_require__(71);
window.itemGroupTechnicalSpecificationServices = __webpack_require__(72);
window.itemTechnicalSpecificationServices = __webpack_require__(73);
window.jsServices = __webpack_require__(74);
window.languageServices = __webpack_require__(75);
window.loginServices = __webpack_require__(76);
window.manageStyleServices = __webpack_require__(77);
window.menuServices = __webpack_require__(78);
window.nationServices = __webpack_require__(79);
window.negotiationServices = __webpack_require__(80);
window.newsletterServices = __webpack_require__(81);
window.newsletterRecipientServices = __webpack_require__(82);
window.packageServices = __webpack_require__(83);
window.packagingTypeSixtenServices = __webpack_require__(84);
window.pageCategoryServices = __webpack_require__(85);
window.pageServices = __webpack_require__(86);
window.pageShareTypeServices = __webpack_require__(87);
window.postalCodeServices = __webpack_require__(88);
window.roleServices = __webpack_require__(89);
window.roleUserServices = __webpack_require__(90);
window.settingServices = __webpack_require__(91);
window.shipmentCodeSixtenServices = __webpack_require__(92);
window.selenaViewsServices = __webpack_require__(93);
window.messageServices = __webpack_require__(94);
window.messageTypeServices = __webpack_require__(95);
window.searchServices = __webpack_require__(96);
window.technicalSpecificationServices = __webpack_require__(97);
window.templateEditorServices = __webpack_require__(98);
window.templateEditorGroupServices = __webpack_require__(99);
window.templateEditorTypeServices = __webpack_require__(100);
window.unitMeasuresServices = __webpack_require__(101);
window.userServices = __webpack_require__(102);
window.vatTypeServices = __webpack_require__(103);
window.InterventionsServices = __webpack_require__(0);
window.faultModulesServices = __webpack_require__(104);
window.statesServices = __webpack_require__(105);
window.producerServices = __webpack_require__(106);
window.rolesServices = __webpack_require__(107);
window.peopleServices = __webpack_require__(108);
window.searchShopServices = __webpack_require__(109);
window.itemGrServices = __webpack_require__(110);
window.searchLiutaioServices = __webpack_require__(111);
window.searchSalaRegistrazioneServices = __webpack_require__(112);
window.searchInterventionServices = __webpack_require__(113);
window.searchUserServices = __webpack_require__(114);
window.brandServices = __webpack_require__(115);
window.tipologyServices = __webpack_require__(116);
window.typeOptionalServices = __webpack_require__(117);
window.optionalServices = __webpack_require__(118);
window.businessnamesupplierServices = __webpack_require__(119);
window.searchItemsServices = __webpack_require__(120);
window.menuAdminCustomServices = __webpack_require__(121);
window.processingviewServices = __webpack_require__(122);
window.quotesServices = __webpack_require__(123);
window.listServices = __webpack_require__(124);
window.regionsServices = __webpack_require__(125);
window.provincesServices = __webpack_require__(126);
window.userAddressServices = __webpack_require__(127);
window.salesOrderServices = __webpack_require__(128);
//window.connectionsServices = require("./services/connections.js");
//window.itemAttachmentFileServices = require("./services/itemAttachmentFile.js");
window.cartDetailServices = __webpack_require__(129);
window.cartServices = __webpack_require__(130);
window.voucherServices = __webpack_require__(131);
window.voucherTypeServices = __webpack_require__(132);
window.cartRulesServices = __webpack_require__(133);
window.externalRepairServices = __webpack_require__(134);
window.fasturgenciesServices = __webpack_require__(135);
window.InternalRepairServices = __webpack_require__(136);
window.searchItemsGrServices = __webpack_require__(137);
window.interventionCustomerServices = __webpack_require__(138);
window.interventionServices = __webpack_require__(0);
//window.amazonServices = require("./services/amazon.js");
window.searchDataSheetServices = __webpack_require__(139);
//window.downloadServices = require("./services/download.js");

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "customer", function() { return customer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "wsRootServicesUrl", function() { return wsRootServicesUrl; });
/***   configData   ***/

/*** STATIC ***/

/***   DEV   ***/
var customer = "Selena";
var wsRootServicesUrl = "http://localhost";

//***   SELENA   ***/
// export const customer = "Selena";
// export const wsRootServicesUrl = "https://selena.tecnotrade.com/api";

/***   BORMAC   ***/
// export const customer = "Bormac";
// export const wsRootServicesUrl = "http://192.168.0.54:8080"; // Production

/***   SIXTEN   ***/
// export const customer = "Sixten";
// export const wsRootServicesUrl = "http://10.0.1.52:588"; //Sixten dev
// export const wsRootServicesUrl = "https://sede.tecnotrade.com:5543"; //Sixten release
// export const wsRootServicesUrl = "https://192.168.2.186:8443"; // Production

/*** END STATIC ***/

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["data"] = data;
/* harmony export (immutable) */ __webpack_exports__["put"] = put;
/* harmony export (immutable) */ __webpack_exports__["readAll"] = readAll;
/** *   notificationData   ***/

function data() {
  if (!functionHelpers.isValued(storageData.sNotificationData())) {} else {
    return storageData.sNotificationData();
  }
}

function put(type, message) {
  var _notification = this.data();
  if (!functionHelpers.isValued(_notification)) {
    _notification = [{ type: '', message: '', read: true }];
    _notification.splice(0, 1);
  }

  _notification.unshift({ type: type, message: message, read: false });
  storageData.setNotificationData(_notification);
}

function readAll() {
  var _notification = this.data();

  $.each(_notification, function (i, item) {
    item.read = true;
  });

  storageData.setNotificationData(_notification);
  setTimeout(function () {
    notificationHelpers.readNotification();
  }, 2000);
}

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["removeCredentialData"] = removeCredentialData;
/* harmony export (immutable) */ __webpack_exports__["sMenu"] = sMenu;
/* harmony export (immutable) */ __webpack_exports__["setMenu"] = setMenu;
/* harmony export (immutable) */ __webpack_exports__["sTokenKey"] = sTokenKey;
/* harmony export (immutable) */ __webpack_exports__["setTokenKey"] = setTokenKey;
/* harmony export (immutable) */ __webpack_exports__["sRefreshTokenKey"] = sRefreshTokenKey;
/* harmony export (immutable) */ __webpack_exports__["setRefreshTokenKey"] = setRefreshTokenKey;
/* harmony export (immutable) */ __webpack_exports__["sExpiresTokenKey"] = sExpiresTokenKey;
/* harmony export (immutable) */ __webpack_exports__["setExpiresTokenKey"] = setExpiresTokenKey;
/* harmony export (immutable) */ __webpack_exports__["sCredetial"] = sCredetial;
/* harmony export (immutable) */ __webpack_exports__["setCredetial"] = setCredetial;
/* harmony export (immutable) */ __webpack_exports__["removeCredetial"] = removeCredetial;
/* harmony export (immutable) */ __webpack_exports__["sIdLanguage"] = sIdLanguage;
/* harmony export (immutable) */ __webpack_exports__["setIdLanguage"] = setIdLanguage;
/* harmony export (immutable) */ __webpack_exports__["removeIdLanguage"] = removeIdLanguage;
/* harmony export (immutable) */ __webpack_exports__["sDescriptionLanguage"] = sDescriptionLanguage;
/* harmony export (immutable) */ __webpack_exports__["setDescriptionLanguage"] = setDescriptionLanguage;
/* harmony export (immutable) */ __webpack_exports__["sDictionary"] = sDictionary;
/* harmony export (immutable) */ __webpack_exports__["setDictionary"] = setDictionary;
/* harmony export (immutable) */ __webpack_exports__["sDatePickerFormat"] = sDatePickerFormat;
/* harmony export (immutable) */ __webpack_exports__["setDatePickerFormat"] = setDatePickerFormat;
/* harmony export (immutable) */ __webpack_exports__["sDateTimePickerFormat"] = sDateTimePickerFormat;
/* harmony export (immutable) */ __webpack_exports__["setDateTimePickerFormat"] = setDateTimePickerFormat;
/* harmony export (immutable) */ __webpack_exports__["sHoldUrl"] = sHoldUrl;
/* harmony export (immutable) */ __webpack_exports__["setHoldUrl"] = setHoldUrl;
/* harmony export (immutable) */ __webpack_exports__["removeHoldUrl"] = removeHoldUrl;
/* harmony export (immutable) */ __webpack_exports__["sNotificationData"] = sNotificationData;
/* harmony export (immutable) */ __webpack_exports__["setNotificationData"] = setNotificationData;
/* harmony export (immutable) */ __webpack_exports__["sRouteParameter"] = sRouteParameter;
/* harmony export (immutable) */ __webpack_exports__["setRouteParameter"] = setRouteParameter;
/* harmony export (immutable) */ __webpack_exports__["removeRouteParameter"] = removeRouteParameter;
/* harmony export (immutable) */ __webpack_exports__["sFilterParameter"] = sFilterParameter;
/* harmony export (immutable) */ __webpack_exports__["setFilterParameter"] = setFilterParameter;
/* harmony export (immutable) */ __webpack_exports__["removeFilterParameter"] = removeFilterParameter;
/* harmony export (immutable) */ __webpack_exports__["sSetting"] = sSetting;
/* harmony export (immutable) */ __webpack_exports__["sUserName"] = sUserName;
/* harmony export (immutable) */ __webpack_exports__["setUserName"] = setUserName;
/* harmony export (immutable) */ __webpack_exports__["setRole"] = setRole;
/* harmony export (immutable) */ __webpack_exports__["sRole"] = sRole;
/* harmony export (immutable) */ __webpack_exports__["sUserId"] = sUserId;
/* harmony export (immutable) */ __webpack_exports__["setUserId"] = setUserId;
/* harmony export (immutable) */ __webpack_exports__["sUserDescription"] = sUserDescription;
/* harmony export (immutable) */ __webpack_exports__["setUserDescription"] = setUserDescription;
/* harmony export (immutable) */ __webpack_exports__["sSalesmanId"] = sSalesmanId;
/* harmony export (immutable) */ __webpack_exports__["setSalesmanId"] = setSalesmanId;
/* harmony export (immutable) */ __webpack_exports__["sCartId"] = sCartId;
/* harmony export (immutable) */ __webpack_exports__["setCartId"] = setCartId;
/* harmony export (immutable) */ __webpack_exports__["setCookie"] = setCookie;
/* harmony export (immutable) */ __webpack_exports__["getCookie"] = getCookie;
/* harmony export (immutable) */ __webpack_exports__["delCookie"] = delCookie;
/** *   storageData   ***/

/** *   WINDOW STORAGE   ***/

function removeCredentialData() {
  window.localStorage.removeItem('sTokenKey');
  window.localStorage.removeItem('sRefreshTokenKey');
  window.localStorage.removeItem('sExpiresTokenKey');
  window.localStorage.removeItem('sCredetial');
  window.localStorage.removeItem('sUserName');
  window.localStorage.removeItem('sUserId');
  //window.localStorage.removeItem('sSalesmanId')
  window.localStorage.removeItem('sUserDescription');
  window.localStorage.removeItem('sNotificationData');
  window.localStorage.removeItem('sRouteParameter');
  window.localStorage.removeItem('sFilterParameter');
  window.localStorage.removeItem('sMenu');

  storageData.delCookie('sUserId');
  storageData.delCookie('sUserDescription');
  //storageData.delCookie('sSalesmanId');
  storageData.delCookie('sUserName');
  storageData.delCookie('sRole');
  storageData.delCookie('sCartId');
}

/** * CONFIGURATION ***/

function sMenu() {
  if (functionHelpers.isValued(window.localStorage.getItem('sMenu'))) {
    return JSON.parse(window.localStorage.getItem('sMenu'));
  }
}

function setMenu(value) {
  window.localStorage.setItem('sMenu', JSON.stringify(value));
}

/** * END CONFIGURATION ***/

/** * CREDENTIAL ***/

function sTokenKey() {
  return window.localStorage.getItem('sTokenKey');
}

function setTokenKey(value) {
  window.localStorage.setItem('sTokenKey', value);
}

function sRefreshTokenKey() {
  return window.localStorage.getItem('sRefreshTokenKey');
}

function setRefreshTokenKey(value) {
  window.localStorage.setItem('sRefreshTokenKey', value);
}

function sExpiresTokenKey() {
  return window.localStorage.getItem('sExpiresTokenKey');
}

function setExpiresTokenKey(value) {
  window.localStorage.setItem('sExpiresTokenKey', value);
}

function sCredetial() {
  return JSON.parse(window.localStorage.getItem('sCredetial'));
}

function setCredetial(value) {
  window.localStorage.setItem('sCredetial', JSON.stringify(value));
}

function removeCredetial() {
  window.localStorage.removeItem('sCredetial');
}

/** * END CREDENTIAL ***/

/** * LANGUAGE ***/

function sIdLanguage() {
  if (functionHelpers.isValued(window.localStorage.getItem('sIdLanguage'))) {
    return window.localStorage.getItem('sIdLanguage');
  } else {
    languageServices.getDefault(function (result) {
      window.localStorage.setItem('sIdLanguage', result.data.id);
      storageData.setDescriptionLanguage(result.data.description);
      storageData.setDatePickerFormat(result.data.date_format_client);
      storageData.setDateTimePickerFormat(result.data.date_time_format_client);
      dictionaryServices.setByLanguage(function (result) {
        storageData.setDictionary(result.data);
      });
      return result.data.id;
    });

    return 0;
  }
}

function setIdLanguage(id) {
  /*window.localStorage.setItem('sIdLanguage', id)
  storageData.setCookie('sIdLanguage', id , 365);*/

  languageServices.get(id, function (result) {
    storageData.setDescriptionLanguage(result.data.description);
    storageData.setDatePickerFormat(result.data.date_format_client);
    storageData.setDateTimePickerFormat(result.data.date_time_format_client);
    dictionaryServices.setByLanguage(function (result) {
      storageData.setDictionary(result.data);
    });
    return result.data.id;
  });
}

function removeIdLanguage() {
  window.localStorage.removeItem('sIdLanguage');
  window.localStorage.removeItem('sDictionary');

  storageData.delCookie('sIdLanguage');
}

function sDescriptionLanguage() {
  return window.localStorage.getItem('sDescriptionLanguage');
}

function setDescriptionLanguage(value) {
  window.localStorage.setItem('sDescriptionLanguage', value);
}

function sDictionary() {
  if (functionHelpers.isValued(window.localStorage.getItem('sDictionary'))) {
    return JSON.parse(window.localStorage.getItem('sDictionary'));
  } else {
    dictionaryServices.setByLanguage(function (result) {
      storageData.setDictionary(result.data);
    });
  }
}

function setDictionary(value) {
  window.localStorage.setItem('sDictionary', JSON.stringify(value));
}

function sDatePickerFormat() {
  return window.localStorage.getItem('sDatePickerFormat');
}

function setDatePickerFormat(value) {
  window.localStorage.setItem('sDatePickerFormat', value);
}

function sDateTimePickerFormat() {
  return window.localStorage.getItem('sDateTimePickerFormat');
}

function setDateTimePickerFormat(value) {
  window.localStorage.setItem('sDateTimePickerFormat', value);
}

/** * END LANGUAGE ***/

/** * PAGE ***/

function sHoldUrl() {
  return window.localStorage.getItem('sHoldUrl');
}

function setHoldUrl(value) {
  window.localStorage.setItem('sHoldUrl', value);
}

function removeHoldUrl() {
  window.localStorage.removeItem('sHoldUrl');
}

function sNotificationData() {
  if (functionHelpers.isValued(window.localStorage.getItem('sNotificationData'))) {
    return JSON.parse(window.localStorage.getItem('sNotificationData'));
  }
}

function setNotificationData(value) {
  window.localStorage.setItem('sNotificationData', JSON.stringify(value));
}

function sRouteParameter() {
  return window.localStorage.getItem('sRouteParameter');
}

function setRouteParameter(value) {
  window.localStorage.setItem('sRouteParameter', value);
}

function removeRouteParameter() {
  window.localStorage.removeItem('sRouteParameter');
}

function sFilterParameter(view) {
  if (functionHelpers.isValued(view)) {
    var filterParameter = JSON.parse(window.localStorage.getItem('sFilterParameter'));

    if (functionHelpers.isValued(filterParameter) && functionHelpers.isValued(filterParameter[view])) {
      return filterParameter[view];
    } else {}
  } else {}
}

function setFilterParameter(view, value) {
  if (functionHelpers.isValued(view)) {
    var filterParameter = JSON.parse(window.localStorage.getItem('sFilterParameter'));

    if (!functionHelpers.isValued(filterParameter)) {
      filterParameter = {};
    }

    filterParameter[view] = value;
    window.localStorage.setItem('sFilterParameter', JSON.stringify(filterParameter));
  }
}

function removeFilterParameter(view) {
  if (functionHelpers.isValued(view)) {
    var filterParameter = JSON.parse(window.localStorage.getItem('sFilterParameter'));

    if (functionHelpers.isValued(filterParameter) && functionHelpers.isValued(filterParameter[view])) {
      filterParameter = delete filterParameter[view];
      if (filterParameter.toString() == '[object Object]') {
        window.localStorage.setItem('sFilterParameter', JSON.stringify(filterParameter));
      } else {
        window.localStorage.removeItem('sFilterParameter');
      }
    }
  } else {
    window.localStorage.removeItem('sFilterParameter');
  }
}

/** * END PAGE ***/

/** * SETTING ***/

// settingServices

function sSetting() {
  if (functionHelpers.isValued(window.localStorage.getItem('sSetting'))) {
    return JSON.parse(window.localStorage.getItem('sSetting'));
  } else {
    settingService.getAll(function (result) {
      window.localStorage.setItem('sSetting', JSON.stringify(result));
      return JSON.parse(window.localStorage.getItem('sSetting'));
    });
  }
}

/** * END SETTING ***/

/** * USER ***/

function sUserName() {
  return window.localStorage.getItem('sUserName');
}

function setUserName(value) {
  window.localStorage.setItem('sUserName', value);
  storageData.setCookie('sUserName', value, 365);
}

function setRole(value) {
  window.localStorage.setItem('sRole', value);
  storageData.setCookie('sRole', value, 365);
}
function sRole(value) {
  return window.localStorage.getItem('sRole');
}

function sUserId(value) {
  return window.localStorage.getItem('sUserId');
}

function setUserId(value) {
  window.localStorage.setItem('sUserId', value);
  storageData.setCookie('sUserId', value, 365);
}

function sUserDescription(value) {
  return window.localStorage.getItem('sUserDescription');
}

function setUserDescription(value) {
  window.localStorage.setItem('sUserDescription', value);
  storageData.setCookie('sUserDescription', value, 365);
}

function sSalesmanId(value) {
  return window.localStorage.getItem('sSalesmanId');
}

function setSalesmanId(value) {
  window.localStorage.setItem('sSalesmanId', value);
  storageData.setCookie('sSalesmanId', value, 365);
}

function sCartId(value) {
  return window.localStorage.getItem('sCartId');
}
function setCartId(value) {
  window.localStorage.setItem('sCartId', value);
  storageData.setCookie('sCartId', value, 365);
}

/** * END USER ***/

/** * GESTIONE COOKIE  ***/

function setCookie(name, value, days) {
  var expires = "";
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1, c.length);
    }if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}

function delCookie(name) {
  document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

/** * END GESTIONE COOKIE ***/

/** * END WINDOW STORAGE ***/

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["selectMonth"] = selectMonth;
/** *   calendarHelpers   ***/

function selectMonth() {
  return [{
    id: 1,
    text: dictionaryHelpers.getDictionary('January')
  }, {
    id: 2,
    text: dictionaryHelpers.getDictionary('February')
  }, {
    id: 3,
    text: dictionaryHelpers.getDictionary('March')
  }, {
    id: 4,
    text: dictionaryHelpers.getDictionary('April')
  }, {
    id: 5,
    text: dictionaryHelpers.getDictionary('May')
  }, {
    id: 6,
    text: dictionaryHelpers.getDictionary('June')
  }, {
    id: 7,
    text: dictionaryHelpers.getDictionary('July')
  }, {
    id: 8,
    text: dictionaryHelpers.getDictionary('August')
  }, {
    id: 9,
    text: dictionaryHelpers.getDictionary('September')
  }, {
    id: 10,
    text: dictionaryHelpers.getDictionary('October')
  }, {
    id: 11,
    text: dictionaryHelpers.getDictionary('November')
  }, {
    id: 12,
    text: dictionaryHelpers.getDictionary('December')
  }];
}

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getValueToSave"] = getValueToSave;
/* harmony export (immutable) */ __webpack_exports__["getValueForSelect"] = getValueForSelect;
/** *   configurationHelpers   ***/

function getValueToSave(value, config) {
  switch (config) {
    case 'CheckBox':
      if (!functionHelpers.isValued(value)) {
        return false;
      } else {
        return value;
      }
    case 'Select':
      if (functionHelpers.isNumeric(value) && Number(value) > 0) {
        return Number(value);
      } else {
        return value;
      }
    default:
      return value;
  }
}

function getValueForSelect(el) {
  switch (el.service) {
    case 'icons':
      iconServices.getById(el.value, function (result) {
        if (functionHelpers.isValued(result.data)) {
          $(el.$el).append(new Option(result.data.description, result.data.id, false, false)).trigger('change');
        }
      });
      break;

    case 'languages':
      this.url = 'language';
      break;

    case 'pageShareType':
      pageShareTypeServices.getById(el.value, function (result) {
        if (functionHelpers.isValued(result.data)) {
          $(el.$el).append(new Option(result.data.description, result.data.id, false, false)).trigger('change');
        }
      });
      break;

    case 'pageCategory':
      pageCategoryServices.getById(el.value, function (result) {
        if (functionHelpers.isValued(result.data)) {
          $(el.$el).append(new Option(result.data.description, result.data.id, false, false)).trigger('change');
        }
      });
      break;

    case 'postalCode':
      postalCodeServices.getById(el.value, function (result) {
        if (functionHelpers.isValued(result.data)) {
          $(el.$el).append(new Option(result.data.description, result.data.id, false, false)).trigger('change');
        }
      });
      break;

    case 'templateEditorGroup':
      templateEditorGroupServices.getById(el.value, function (result) {
        if (functionHelpers.isValued(result.data)) {
          $(el.$el).append(new Option(result.data.description, result.data.id, false, false)).trigger('change');
        }
      });
      break;

    case 'templateEditorType':
      templateEditorTypeServices.getById(el.value, function (result) {
        if (functionHelpers.isValued(result.data)) {
          $(el.$el).append(new Option(result.data.description, result.data.id, false, false)).trigger('change');
        }
      });
      break;

    case 'users':
      userServices.getById(el.value, function (result) {
        if (functionHelpers.isValued(result.data)) {
          $(el.$el).append(new Option(result.data.name, result.data.id, false, false)).trigger('change');
        }
      });
      break;

    case 'vatType':
      vatTypeServices.getById(el.value, function (result) {
        if (functionHelpers.isValued(result.data)) {
          $(el.$el).append(new Option(result.data.name, result.data.id, false, false)).trigger('change');
        }
      });
      // this.url = 'vattype';
      break;

    default:
      this.url = '';
      break;
  }
}

/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["insertLogsContacts"] = insertLogsContacts;
/* harmony export (immutable) */ __webpack_exports__["insertLandingeBookPcb"] = insertLandingeBookPcb;
/* harmony export (immutable) */ __webpack_exports__["insertRequest"] = insertRequest;
/* harmony export (immutable) */ __webpack_exports__["insertRequestLanguage"] = insertRequestLanguage;
/* harmony export (immutable) */ __webpack_exports__["inserTourAvailable"] = inserTourAvailable;
/* harmony export (immutable) */ __webpack_exports__["insertRequestWorkForUs"] = insertRequestWorkForUs;
/* harmony export (immutable) */ __webpack_exports__["unsubscribeContact"] = unsubscribeContact;
/* harmony export (immutable) */ __webpack_exports__["unsubscribeNewsletter"] = unsubscribeNewsletter;
/* harmony export (immutable) */ __webpack_exports__["subscribeContact"] = subscribeContact;
/***   contactHelpers   ***/

function insertLogsContacts(id, url_file) {
  console.log(url_file);
  console.log(id);

  var data = {
    idUser: storageData.sUserId(),
    idContact: id,
    url_file: url_file
  };
  console.log(data);
  contactRequestServices.insertLogsContacts(data, function (result) {
    console.log(result);
  });
}

function insertLandingeBookPcb() {
  spinnerHelpers.show();
  var processing_of_personal_data = false;
  if ($("#contactRequestLandingeBookPcb #processing_of_personal_data").is(":checked")) {
    processing_of_personal_data = true;
  } else {
    processing_of_personal_data = false;
  }

  var data = {
    idLanguage: storageData.sIdLanguage(),
    name: $('#contactRequestLandingeBookPcb #name').val(),
    surname: $('#contactRequestLandingeBookPcb #surname').val(),
    email: $('#contactRequestLandingeBookPcb #email').val(),
    business_name: $('#contactRequestLandingeBookPcb #business_name').val(),
    role: $('#contactRequestLandingeBookPcb #role').val(),
    processing_of_personal_data: processing_of_personal_data
  };
  if (functionHelpers.isValued($('#contactRequestLandingeBookPcb #emailToSend')) && functionHelpers.isValued($('#contactRequestLandingeBookPcb #emailToSend').val())) {
    data.emailToSend = $('#contactRequestLandingeBookPcb #emailToSend').val();
  }
  contactRequestServices.insertLandingeBookPcb(data, function () {
    $('#contactRequestLandingeBookPcb #name').val();
    $('#contactRequestLandingeBookPcb #surname').val();
    $('#contactRequestLandingeBookPcb #email').val();
    $('#contactRequestLandingeBookPcb #business_name').val();
    $('#contactRequestLandingeBookPcb #role').val('');
    $('#contactRequestLandingeBookPcb #processing_of_personal_data').val('');
    notificationHelpers.success("Richiesta di contatto inviata con successo");
    spinnerHelpers.hide();
  });
}

function insertRequest() {
  spinnerHelpers.show();

  var checkprivacy_cookie_policy = false;
  if ($("#contactRequest #checkprivacy_cookie_policy").is(":checked")) {
    checkprivacy_cookie_policy = true;
  } else {
    checkprivacy_cookie_policy = false;
  }

  var processing_of_personal_data = false;
  if ($("#contactRequest #processing_of_personal_data").is(":checked")) {
    processing_of_personal_data = true;
  } else {
    processing_of_personal_data = false;
  }
  var newsletter_info = false;
  if ($("#contactRequest #newsletter_info").is(":checked")) {
    newsletter_info = true;
  } else {
    newsletter_info = false;
  }
  var newsletter_mailing = false;
  if ($("#contactRequest #newsletter_mailing").is(":checked")) {
    newsletter_mailing = true;
  } else {
    newsletter_mailing = false;
  }

  var data = {
    name: $('#contactRequest #name').val(),
    surname: $('#contactRequest #surname').val(),
    email: $('#contactRequest #email').val(),
    description: $('#contactRequest #description').val(),
    businessName: $('#contactRequest #businessName').val(),
    phoneNumber: $('#contactRequest #phoneNumber').val(),
    faxNumber: $('#contactRequest #faxNumber').val(),
    province: $('#contactRequest #province').val(),
    address: $('#contactRequest #address').val(),
    vatNumber: $('#contactRequest #vatNumber').val(),
    object: $('#contactRequest #object').val(),
    city: $('#contactRequest #city').val(),
    postalCode: $('#contactRequest #postalCode').val(),
    countryofresidence: $('#contactRequest #countryofresidence').val(),
    checkprivacy_cookie_policy: checkprivacy_cookie_policy,
    processing_of_personal_data: processing_of_personal_data,
    newsletter_info: newsletter_info,
    newsletter_mailing: newsletter_mailing
  };

  if (functionHelpers.isValued($('#contactRequest #emailToSend')) && functionHelpers.isValued($('#contactRequest #emailToSend').val())) {
    data.emailToSend = $('#contactRequest #emailToSend').val();
  }

  contactRequestServices.insert(data, function () {
    $('#contactRequest #object').val();
    $('#contactRequest #vatNumber').val();
    $('#contactRequest #businessName').val();
    $('#contactRequest #postalCode').val();
    $('#contactRequest #city').val();
    $('#contactRequest #address').val();
    $('#contactRequest #province').val();
    $('#contactRequest #faxNumber').val();
    $('#contactRequest #phoneNumber').val();
    $('#contactRequest #name').val('');
    $('#contactRequest #surname').val('');
    $('#contactRequest #email').val('');
    $('#contactRequest #description').val('');
    $('#contactRequest #telefono').val('');
    $('#contactRequest #checkprivacy_cookie_policy').val('');
    $('#contactRequest #processing_of_personal_data').val('');
    $('#contactRequest #countryofresidence').val('');
    notificationHelpers.success("Richiesta di contatto inviata con successo");
    spinnerHelpers.hide();
  });
}

function insertRequestLanguage() {
  spinnerHelpers.show();

  var checkprivacy_cookie_policy = false;
  if ($("#RequestContactLanguage #checkprivacy_cookie_policy").is(":checked")) {
    checkprivacy_cookie_policy = true;
  } else {
    checkprivacy_cookie_policy = false;
  }

  var processing_of_personal_data = false;
  if ($("#RequestContactLanguage #processing_of_personal_data").is(":checked")) {
    processing_of_personal_data = true;
  } else {
    processing_of_personal_data = false;
  }
  var newsletter_info = false;
  if ($("#RequestContactLanguage #newsletter_info").is(":checked")) {
    newsletter_info = true;
  } else {
    newsletter_info = false;
  }
  var newsletter_mailing = false;
  if ($("#RequestContactLanguage #newsletter_mailing").is(":checked")) {
    newsletter_mailing = true;
  } else {
    newsletter_mailing = false;
  }

  var data = {
    idLanguage: storageData.sIdLanguage(),
    name: $('#RequestContactLanguage #name').val(),
    surname: $('#RequestContactLanguage #surname').val(),
    email: $('#RequestContactLanguage #email').val(),
    description: $('#RequestContactLanguage #description').val(),
    businessName: $('#RequestContactLanguage #businessName').val(),
    phoneNumber: $('#RequestContactLanguage #phoneNumber').val(),
    faxNumber: $('#RequestContactLanguage #faxNumber').val(),
    province: $('#RequestContactLanguage #province').val(),
    vatNumber: $('#RequestContactLanguage #vatNumber').val(),
    object: $('#RequestContactLanguage #object').val(),
    city: $('#RequestContactLanguage #city').val(),
    postalCode: $('#RequestContactLanguage #postalCode').val(),
    countryofresidence: $('#RequestContactLanguage #countryofresidence').val(),
    checkprivacy_cookie_policy: checkprivacy_cookie_policy,
    processing_of_personal_data: processing_of_personal_data,
    newsletter_info: newsletter_info,
    newsletter_mailing: newsletter_mailing
  };

  contactRequestServices.insertRequestLanguage(data, function () {
    $('#RequestContactLanguage #object').val();
    $('#RequestContactLanguage #vatNumber').val();
    $('#RequestContactLanguage #businessName').val();
    $('#RequestContactLanguage #postalCode').val();
    $('#RequestContactLanguage #city').val();
    $('#RequestContactLanguage #province').val();
    $('#RequestContactLanguage #faxNumber').val();
    $('#RequestContactLanguage #phoneNumber').val();
    $('#RequestContactLanguage #name').val('');
    $('#RequestContactLanguage #surname').val('');
    $('#RequestContactLanguage #email').val('');
    $('#RequestContactLanguage #description').val('');
    $('#RequestContactLanguage #telefono').val('');
    $('#RequestContactLanguage #checkprivacy_cookie_policy').val('');
    $('#RequestContactLanguage #processing_of_personal_data').val('');
    $('#RequestContactLanguage #countryofresidence').val('');

    notificationHelpers.success(dictionaryHelpers.getDictionary("ContactRequestCompleted"));
    //window.location.href = '/contact-request';
    spinnerHelpers.hide();
  });
}

function inserTourAvailable() {
  spinnerHelpers.show();
  var data = {
    idLanguage: storageData.sIdLanguage(),
    date: $('#form_tour_availability #date').val(),
    course_id: $('#form_tour_availability #course_id').val(),
    adult_nr: $('#form_tour_availability #adult_nr').val(),
    kids_nr: $('#form_tour_availability #kids_nr').val(),
    babies_nr: $('#form_tour_availability #babies_nr').val(),
    total_amount: $('#form_tour_availability #total_amount').val(),
    name: $('#form_tour_availability #name').val(),
    surname: $('#form_tour_availability #surname').val(),
    email: $('#form_tour_availability #email').val(),
    telephone: $('#form_tour_availability #telephone').val(),
    notes: $('#form_tour_availability #notes').val()
  };

  /* if (functionHelpers.isValued($('#contactRequest #emailToSend')) && functionHelpers.isValued($('#contactRequest #emailToSend').val())) {
     data.emailToSend = $('#contactRequest #emailToSend').val();
   }*/

  contactRequestServices.insertTourAvailable(data, function () {
    $('#form_tour_availability #date').val();
    $('#form_tour_availability #course_id').val();
    $('#form_tour_availability #adult_nr').val();
    $('#form_tour_availability #kids_nr').val();
    $('#form_tour_availability #babies_nr').val();
    $('#form_tour_availability #total_amount').val('');
    $('#form_tour_availability #name').val('');
    $('#form_tour_availability #surname').val('');
    $('#form_tour_availability #email').val('');
    $('#form_tour_availability #telephone').val('');
    $('#form_tour_availability #notes').val('');
    notificationHelpers.success("Richiesta inviata con successo");
    spinnerHelpers.hide();
    $('#paypalform').submit();
  });
}

function insertRequestWorkForUs() {
  spinnerHelpers.show();
  var checkprivacy_cookie_policy = false;
  if ($("#workForUs #checkprivacy_cookie_policy").is(":checked")) {
    checkprivacy_cookie_policy = true;
  } else {
    checkprivacy_cookie_policy = false;
  }

  /*var data = {
    name: $('#workForUs #name').val(),
    surname: $('#workForUs #surname').val(),
    email: $('#workForUs #email').val(),
    phoneNumber: $('#workForUs #phone').val(),
    gender: $('#workForUs #gender').val(),
    date: $('#workForUs #date').val(),
    position: $('#workForUs #position').val(),
    nation: $('#workForUs #nation').val(),
    city: $('#workForUs #city').val(),
    info: $('#workForUs #info').val(),
    actualCompany: $('#workForUs #actualCompany').val(),
    file: $("#myFile").prop('files')[0]
  };*/
  var file_data = $('#myFile').prop('files')[0],
      name = $('#workForUs #name').val(),
      surname = $('#workForUs #surname').val(),
      email = $('#workForUs #email').val(),
      phoneNumber = $('#workForUs #phone').val(),
      gender = $('#workForUs #gender').val(),
      date = $('#workForUs #date').val(),
      position = $('#workForUs #position').val(),
      nation = $('#workForUs #nation').val(),
      city = $('#workForUs #city').val(),
      info = $('#workForUs #info').val(),
      cityofborn = $('#workForUs #cityofborn').val(),
      address = $('#workForUs #address').val(),
      cap = $('#workForUs #cap').val(),
      province = $('#workForUs #province').val(),
      countryofresidence = $('#workForUs #countryofresidence').val(),
      regionofdomicily = $('#workForUs #regionofdomicily').val(),
      cityofresidence = $('#workForUs #cityofresidence').val(),
      fiscalcode = $('#workForUs #fiscalcode').val(),
      website = $('#workForUs #website').val(),
      license = $('#workForUs #license').val(),
      meansoftransport = $('#workForUs #meansoftransport').val(),
      countryofbirth = $('#workForUs #countryofbirth').val(),
      actualCompany = $('#workForUs #actualCompany').val();
  checkprivacy_cookie_policy = checkprivacy_cookie_policy;

  var form_data = new FormData();
  form_data.append('file', file_data);
  form_data.append('name', name);
  form_data.append('surname', surname);
  form_data.append('email', email);
  form_data.append('phoneNumber', phoneNumber);
  form_data.append('gender', gender);
  form_data.append('date', date);
  form_data.append('position', position);
  form_data.append('nation', nation);
  form_data.append('city', city);
  form_data.append('info', info);
  form_data.append('cityofborn', cityofborn);
  form_data.append('address', address);
  form_data.append('cap', cap);
  form_data.append('province', province);
  form_data.append('actualCompany', actualCompany);
  form_data.append('checkprivacy_cookie_policy', checkprivacy_cookie_policy);
  form_data.append('countryofresidence', countryofresidence);
  form_data.append('regionofdomicily', regionofdomicily);
  form_data.append('cityofresidence', cityofresidence);
  form_data.append('fiscalcode', fiscalcode);
  form_data.append('website', website);
  form_data.append('license', license);
  form_data.append('meansoftransport', meansoftransport);
  form_data.append('countryofbirth', countryofbirth);

  $.ajax({
    url: configData.wsRootServicesUrl + "/api/v1/contactrequest/insertworkforus", // point to server-side PHP script
    data: form_data,
    type: 'POST',
    contentType: false, // The content type used when sending data to the server.
    cache: false, // To unable request pages to be cached
    processData: false,
    success: function success(data) {
      $('#workForUs #name').val('');
      $('#workForUs #surname').val('');
      $('#workForUs #email').val('');
      $('#workForUs #phone').val('');
      $('#workForUs #gender').val('');
      $('#workForUs #date').val('');
      $('#workForUs #position').val('');
      $('#workForUs #nation').val('');
      $('#workForUs #city').val('');
      $('#workForUs #info').val('');
      $('#workForUs #actualCompany').val('');
      $('#workForUs #checkprivacy_cookie_policy').val('');
      $('#workForUs #countryofresidence').val('');
      $('#workForUs #regionofdomicily').val('');
      $('#workForUs #cityofresidence').val('');
      $('#workForUs #fiscalcode').val('');
      $('#workForUs #website').val('');
      $('#workForUs #license').val('');
      $('#workForUs #meansoftransport').val('');
      $('#workForUs #countryofbirth').val('');
      $("#myFile").val('');
      notificationHelpers.success('Modulo lavora con noi spedito correttamente!');
      spinnerHelpers.hide();
      setTimeout(function () {
        window.location.href = "/";
      }, 1500);
    }
  });

  /*contactRequestServices.insertWorkForUs(form_data, function () {
    $('#workForUs #name').val('');
    $('#workForUs #surname').val('');
    $('#workForUs #email').val('');
    $('#workForUs #phone').val('');
    $('#workForUs #gender').val('');
    $('#workForUs #date').val('');
    $('#workForUs #position').val('');
    $('#workForUs #nation').val('');
    $('#workForUs #city').val('');
    $('#workForUs #info').val('');
    $('#workForUs #actualCompany').val('');
    $("#myFile").val('');
    notificationHelpers.success(dictionaryHelpers.getDictionary("ContactRequestCompleted"));
    spinnerHelpers.hide();
  });*/
}

function unsubscribeContact() {
  spinnerHelpers.show();

  var data = {
    email: $('#contactUnsubscribe #email').val()
  };

  contactServices.unsubscribe(data, function () {
    $('#contactUnsubscribe #email').val('');
    notificationHelpers.success(dictionaryHelpers.getDictionary("UnsubscribeContactCompleted"));
    spinnerHelpers.hide();
  });
}

function unsubscribeNewsletter() {
  spinnerHelpers.show();

  var data = {
    email: $('#unsubscribeNewsletter #email').val()
  };

  contactServices.unsubscribeNewsletter(data, function (response) {

    $('#unsubscribeNewsletter #email').val('');
    $("#div_html_newsletter").html("");
    var html = "";
    html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
    html += '<div class="col-12" style="text-align:center;">';
    html += '<h2 for="' + '" style="color: #3f8ec5;">Cancellazione Dalla Newsletter' + "</h2>";
    html += "</div>";
    html += '<div class="col-12" style="text-align:center;">';
    html += '<h3 for="">' + response.message + "</h3>";
    html += "</div>";
    html += "</div>";
    $("#div_html_newsletter").append(html);
    window.$("#unsubscribeNewsletterA").modal("show");
    //notificationHelpers.success(dictionaryHelpers.getDictionary("UnsubscribeContactCompleted"));
    spinnerHelpers.hide();
  });
}

function subscribeContact() {
  spinnerHelpers.show();

  var processing_of_personal_data = false;
  if ($("#contactSubscribe #processing_of_personal_data").is(":checked")) {
    processing_of_personal_data = true;
  } else {
    processing_of_personal_data = false;
  }

  console.log(data);
  var data = {
    email: $('#contactSubscribe #emailnewsletter').val(),
    processing_of_personal_data: processing_of_personal_data
  };

  contactServices.subscribe(data, function () {
    $('#contactSubscribe #emailnewsletter').val(''), $('#contactSubscribe #processing_of_personal_data').val('');
    var cname = "newsletterPopUp";
    var cvalue = "true";
    var exdays = 7;
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    window.$('.modal').modal('hide');

    notificationHelpers.success(dictionaryHelpers.getDictionary("SubscribeContactCompleted"));
    spinnerHelpers.hide();
  });
}

/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["unbind"] = unbind;
/** *   dataHelpers   ***/

function unbind(value) {
  return JSON.parse(JSON.stringify(value));
}

/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getDictionary"] = getDictionary;
/* harmony export (immutable) */ __webpack_exports__["_getDictionary"] = _getDictionary;
/** *   dictionaryHelpers   ***/

function getDictionary(property) {
  var value = _getDictionary(property);

  if (!functionHelpers.isValued(value)) {
    dictionaryServices.setByLanguage(function (result) {
      storageData.setDictionary(result.data);

      value = _getDictionary(property);
    });
  }

  return value;
}

function _getDictionary(property) {
  var propertySplit = property.split('.');
  var propertyLength = propertySplit.length;
  var value = void 0;

  $.each(propertySplit, function (i, item) {
    if (i == propertyLength - 1) {
      $.each(storageData.sDictionary(), function (x, dictionary) {
        if (functionHelpers.isValued(dictionary[item])) {
          value = dictionary[item];
          return true;
        }
      });
      return true;
    }
  });

  return value;
}

/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["fail"] = fail;
/** *   failService   ***/

function fail(response, fnError, id) {
  if (response.status == 401) {
    alert(401);
  }

  if (response.status == 400 && response.responseText == '{"error":"invalid_grant"}') {
    // || response.status == 401) {
    loginHelpers.disconnect();
  } else if (response.status == 0) {
    notificationHelpers.error(dictionaryHelpers.getDictionary('UnableToConnectToServer'));
  } else if (functionHelpers.isValued(response.responseText) && ($.parseJSON(response.responseText).message != undefined || $.parseJSON(response.responseText).error_description != undefined)) {
    if ($.parseJSON(response.responseText).message != undefined) {
      if ($.isFunction(fnError)) {
        if (functionHelpers.isValued(id)) {
          fnError($.parseJSON(response.responseText).message, id);
        } else {
          fnError($.parseJSON(response.responseText).message);
        }
      } else {
        notificationHelpers.error($.parseJSON(response.responseText).message);
      }
    } else if ($.isFunction(fnError)) {
      if (functionHelpers.isValued(id)) {
        fnError($.parseJSON(response.responseText).error_description, id);
      } else {
        fnError($.parseJSON(response.responseText).error_description);
      }
    } else {
      notificationHelpers.error($.parseJSON(response.responseText).error_description);
    }
  } else if ($.isFunction(fnError)) {
    fnError(response.responseText);
  } else {
    notificationHelpers.error(response.responseText);
  }
  spinnerHelpers.hide();
}

/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["isValued"] = isValued;
/* harmony export (immutable) */ __webpack_exports__["isFunction"] = isFunction;
/* harmony export (immutable) */ __webpack_exports__["clearNullOnJson"] = clearNullOnJson;
/* harmony export (immutable) */ __webpack_exports__["isNumeric"] = isNumeric;
/* harmony export (immutable) */ __webpack_exports__["replaceAll"] = replaceAll;
/* harmony export (immutable) */ __webpack_exports__["stringRemoveQuotes"] = stringRemoveQuotes;
/* harmony export (immutable) */ __webpack_exports__["dateFormatter"] = dateFormatter;
/* harmony export (immutable) */ __webpack_exports__["dateTimeFormatter"] = dateTimeFormatter;
/* harmony export (immutable) */ __webpack_exports__["dateFromITtoEN"] = dateFromITtoEN;
/* harmony export (immutable) */ __webpack_exports__["dateToJsonFormatter"] = dateToJsonFormatter;
/* harmony export (immutable) */ __webpack_exports__["scrollToTop"] = scrollToTop;
/* harmony export (immutable) */ __webpack_exports__["validateEmail"] = validateEmail;
/* harmony export (immutable) */ __webpack_exports__["formToJson"] = formToJson;
/* harmony export (immutable) */ __webpack_exports__["insertPaginator"] = insertPaginator;
/** *   functionHelpers   ***/

// Return if element is valued
function isValued(value) {
  if (value !== undefined && value !== '' && value !== null && value !== 'undefined' && typeof value !== 'undefined') {
    return true;
  } else {
    return false;
  }
}

// Return if element is function
function isFunction(value) {
  if ($.isFunction(value)) {
    return true;
  } else {
    return false;
  }
}

// Return json without null value
function clearNullOnJson(object) {
  $.each(object, function (i, value) {
    if (value == null) {
      delete object[i];
    }
  });

  return object;
}

// Return if element is numeric
function isNumeric(value) {
  return !isNaN(value);
}

// Replace all
function replaceAll(stringValue, stringSearch, stringReplace) {
  return stringValue.replace(new RegExp(stringSearch, 'g'), stringReplace);
}

// Remove quotes
function stringRemoveQuotes(value) {
  if (functionHelpers.isNumeric(value)) {
    return value;
  } else {
    return value.replace(new RegExp("#\\'#", 'g'), "'");
  }
}

// Date formatter
function dateFormatter(value) {
  if (functionHelpers.isValued(value)) {
    return DateFormat.format.date(value, storageData.sDatePickerFormat());
  }
}

// Date time formatter
function dateTimeFormatter(value) {
  if (functionHelpers.isValued(value)) {
    return DateFormat.format.date(value, storageData.sDateTimePickerFormat());
  }
}

// Date from IT to EN
function dateFromITtoEN(date) {
  if (functionHelpers.isValued(date)) {
    if (date.split(' ').length > 1) {
      var d = date.split(' ')[0].split('/');
      var t = date.split(' ')[1].split(':');
      var day = '' + d[0];
      var month = '' + d[1];
      var year = d[2];
      var hour = t[0];
      var minute = t[1];
      var second = t[2];

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return [year, month, day].join('-') + 'T' + [hour, minute, second].join(':');
    } else {
      var d = date.split('/');
      var day = '' + d[0];
      var month = '' + d[1];
      var year = d[2];

      if (month.length < 2) month = '0' + month;
      if (day.length < 2) day = '0' + day;

      return [year, month, day].join('-');
    }
  } else {}
}

// Date to JSON formatter
function dateToJsonFormatter(date) {
  if (functionHelpers.isValued(date)) {
    return functionHelpers.dateFromITtoEN(date);
  } else {
    return null;
  }
}

function scrollToTop(scrollDuration) {
  var scrollStep = -window.scrollY / (scrollDuration / 15);
  var scrollInterval = setInterval(function () {
    if (window.scrollY != 0) {
      window.scrollBy(0, scrollStep);
    } else clearInterval(scrollInterval);
  }, 15);
}

// Validate email address
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function formToJson(form) {
  var dataForm = $('#' + form).serializeArray();
  var data = {};
  $.map(dataForm, function (n, i) {
    data[n.name] = n.value;
  });
  return data;
}

function insertPaginator(form) {}

/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["onChangeTable"] = onChangeTable;
/** *   groupDisplayTechnicalSpecificationHelpers   ***/

function onChangeTable(index, value, id, field) {
  var idTable = $('#' + id).closest('table')[0].id;
  if (functionHelpers.isValued(value)) {
    groupDisplayTechnicalSpecificationServices.checkExists(value, field, $('#' + idTable).bootstrapTable('getData')[index].id, function (result) {
      if (result.data) {
        if (!$('#' + id).hasClass('error')) {
          $('#' + id).addClass('error');
        }
        $('#' + idTable).bootstrapTable('uncheck', index);
        notificationHelpers.error(dictionaryHelpers.getDictionary('GroupDisplayTechnicalSpecificationAlreadyExists'));
      } else if ($('#' + id).hasClass('error')) {
        $('#' + id).removeClass('error');
      }
    });
  } else if (field == storageData.sIdLanguage()) {
    if (!$('#' + id).hasClass('error')) {
      $('#' + id).addClass('error');
    }
    $('#' + idTable).bootstrapTable('uncheck', index);
    notificationHelpers.error(dictionaryHelpers.getDictionary('ThisFieldIsRequired'));
  } else if ($('#' + id).hasClass('error')) {
    $('#' + id).removeClass('error');
  }
}

/***/ }),
/* 14 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["onChangeTable"] = onChangeTable;
/** *   groupTechnicalSpecificationHelpers   ***/

function onChangeTable(index, value, id, field) {
  var idTable = $('#' + id).closest('table')[0].id;
  if (functionHelpers.isValued(value)) {
    groupTechnicalSpecificationServices.checkExists(value, field, $('#' + idTable).bootstrapTable('getData')[index].id, function (result) {
      if (result.data) {
        if (!$('#' + id).hasClass('error')) {
          $('#' + id).addClass('error');
        }
        $('#' + idTable).bootstrapTable('uncheck', index);
        notificationHelpers.error(dictionaryHelpers.getDictionary('GroupTechnicalSpecificationAlreadyExists'));
      } else if ($('#' + id).hasClass('error')) {
        $('#' + id).removeClass('error');
      }
    });
  } else if (field == storageData.sIdLanguage()) {
    if (!$('#' + id).hasClass('error')) {
      $('#' + id).addClass('error');
    }
    $('#' + idTable).bootstrapTable('uncheck', index);
    notificationHelpers.error(dictionaryHelpers.getDictionary('ThisFieldIsRequired'));
  } else if ($('#' + id).hasClass('error')) {
    $('#' + id).removeClass('error');
  }
}

/***/ }),
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["searchImplementedItemsGr"] = searchImplementedItemsGr;
/* harmony export (immutable) */ __webpack_exports__["getResultDetail"] = getResultDetail;
/* harmony export (immutable) */ __webpack_exports__["searchImplementedItemsGlobal"] = searchImplementedItemsGlobal;
/***   searchGrItemsHelpers   ***/
/***   POST   ***/

function searchImplementedItemsGr(id, brand, tipology, code_gr) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(id, brand, tipology, code_gr);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  searchItemsGrServices.searchImplementedItemsGr(data, function (response) {
    $("#table_list_searchitemsGrlist tbody").html('');
    if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_searchitemsGrlist").show();
      $("#table_list_searchitemsGrlist tbody").append(newList);
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        newList += "<tr>";
        newList += "<td><a href='/dettaglio-articolo?id=" + response.data[i].id + "'><img style='width: 150px;max-width: 150px;' src = '" + response.data[i].imageitems + "'></a></td> ";
        newList += "<td>" + response.data[i].tipology + "</td>";
        // newList += "<td>" + response.data[i].plant + "</td>";
        newList += "<td>" + response.data[i].original_code + "</td>";
        newList += "<td>" + response.data[i].brand + "</td>";
        // newList += "<td>" + response.data[i].code_product + "</td>";
        newList += "<td>" + response.data[i].code_gr + "</td>";
        newList += "<td><a href='" + response.data[i].img + "'><i class='fa fa-file-pdf' class='fr-fic fr-dii' style = 'color:#efb70d;'></i></a></td></div></div>";
        newList += "</td>";
        newList += "</tr>";
        $("#table_list_searchitemsGrlist").show();
        $("#table_list_searchitemsGrlist tbody").append(newList);
        //$("#frmSearchItems").hide();
      }
    }
    lightGallery(document.getElementById('lightgallery'));
  });

  spinnerHelpers.hide();
}

function getResultDetail(id) {
  searchItemsGrServices.getResultDetail(id, function (response) {
    var searchParam = "";
    jQuery.each(response.data, function (i, val) {

      if (val == true) {
        if (i == "id") {
          $("#frmViewDetailItems #" + i).html(val);
        } else {
          $('#frmViewDetailItems #' + i).attr('checked', 'checked');
        }
      } else {
        if (i == "imageitems") {
          if (val != "") {
            $("#frmViewDetailItems #imgPreview").attr('src', val);
            $("#frmViewDetailItems #imgName").attr('href', val);
            $("#frmViewDetailItems #imageitems").attr('src', val);
          } else {
            $("#frmViewDetailItems #imgName").remove();
          }
        } else {
          $("#frmViewDetailItems #" + i).val(val);
          $("#frmViewDetailItems #" + i).html(val);
        }
      }
    });
    $("#frmViewDetailItems").show();
  });
}

function searchImplementedItemsGlobal(idForm) {
  spinnerHelpers.show();
  var data = {
    code_gr: $("#frmSearchItemsGlobal #code_gr").val(),
    idLanguage: storageData.sIdLanguage()
  };

  //var data = functionHelpers.formToJson(idForm);
  //data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  searchItemsGrServices.searchImplementedItemsGlobal(data, function (response) {
    html += "<div class='container div-cont-search'>";
    html += "<div class='row'>";
    html += response.message;
    html += "</div>";
    html += "</div>";

    $('#result').html(html);
    spinnerHelpers.hide();
  });
}

/***   END POST   ***/

/***/ }),
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getLastId"] = getLastId;
/* harmony export (immutable) */ __webpack_exports__["getLastIdLib"] = getLastIdLib;
/* harmony export (immutable) */ __webpack_exports__["getdescription"] = getdescription;
/* harmony export (immutable) */ __webpack_exports__["getdescriptionforminsert"] = getdescriptionforminsert;
/* harmony export (immutable) */ __webpack_exports__["getnumbertelephone"] = getnumbertelephone;
/* harmony export (immutable) */ __webpack_exports__["insertModule"] = insertModule;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["updateCheckRepairArrived"] = updateCheckRepairArrived;
/* harmony export (immutable) */ __webpack_exports__["getAllRepairArriving"] = getAllRepairArriving;
/* harmony export (immutable) */ __webpack_exports__["ViewAll"] = ViewAll;
/* harmony export (immutable) */ __webpack_exports__["SearchTrue"] = SearchTrue;
/* harmony export (immutable) */ __webpack_exports__["SearchOpen"] = SearchOpen;
/* harmony export (immutable) */ __webpack_exports__["SearchClose"] = SearchClose;
/* harmony export (immutable) */ __webpack_exports__["getAllIntervention"] = getAllIntervention;
/* harmony export (immutable) */ __webpack_exports__["getByViewExternalRepair"] = getByViewExternalRepair;
/* harmony export (immutable) */ __webpack_exports__["getByInterventionFU"] = getByInterventionFU;
/* harmony export (immutable) */ __webpack_exports__["updateInterventionFU"] = updateInterventionFU;
/* harmony export (immutable) */ __webpack_exports__["getBySelectReferent"] = getBySelectReferent;
/* harmony export (immutable) */ __webpack_exports__["getByViewQuotes"] = getByViewQuotes;
/* harmony export (immutable) */ __webpack_exports__["getByViewValue"] = getByViewValue;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["getByFaultModule"] = getByFaultModule;
/* harmony export (immutable) */ __webpack_exports__["getByModuleGuarantee"] = getByModuleGuarantee;
/* harmony export (immutable) */ __webpack_exports__["getByIdUser"] = getByIdUser;
/* harmony export (immutable) */ __webpack_exports__["updateCheck"] = updateCheck;
/* harmony export (immutable) */ __webpack_exports__["updateSelectReferent"] = updateSelectReferent;
/* harmony export (immutable) */ __webpack_exports__["updateExternalRepair"] = updateExternalRepair;
/* harmony export (immutable) */ __webpack_exports__["updateInterventionUpdateViewQuotes"] = updateInterventionUpdateViewQuotes;
/* harmony export (immutable) */ __webpack_exports__["updateUser"] = updateUser;
/* harmony export (immutable) */ __webpack_exports__["updateInterventionUpdateProcessingSheet"] = updateInterventionUpdateProcessingSheet;
/* harmony export (immutable) */ __webpack_exports__["updateIntervention"] = updateIntervention;
/* harmony export (immutable) */ __webpack_exports__["deleteRow"] = deleteRow;
/* harmony export (immutable) */ __webpack_exports__["getDescriptionAndPriceProcessingSheet"] = getDescriptionAndPriceProcessingSheet;
/* harmony export (immutable) */ __webpack_exports__["searchImplemented"] = searchImplemented;
/* harmony export (immutable) */ __webpack_exports__["getAllRepairArrivedNumberMenuGlobal"] = getAllRepairArrivedNumberMenuGlobal;
/***   interventionHelpers   ***/
function getLastId() {
  InterventionsServices.getLastId(function (response) {
    $("#id").val(response.data);
  });
}

function getLastIdLib() {
  InterventionsServices.getLastIdLib(function (response) {
    $("#code_intervention_gr").val(response.data);
  });
}

function getdescription(id) {
  InterventionsServices.getdescription(id, function (response) {
    $("#frmUpdateIntervention #description").val(response.data.tipology + ' ' + response.data.note + ' ' + response.data.plant);
  });
}

function getdescriptionforminsert(id) {
  InterventionsServices.getdescription(id, function (response) {
    $("#frmAddIntervention #description").val(response.data.tipology + ' ' + response.data.note + ' ' + response.data.plant);
  });
}

function getnumbertelephone(id) {
  InterventionsServices.getnumbertelephone(id, function (response) {
    $("#intervention #phone_number").val(response.data.phone_number);
    $("#ViewFaultModule #phone_number").val(response.data.phone_number);
  });
}

function insertModule() {
  spinnerHelpers.show();

  var create_quote = false;
  if ($("#intervention #create_quote").is(":checked")) {
    create_quote = true;
  } else {
    create_quote = false;
  }
  var data = {
    customer_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    imgmodule: $("#intervention #imgBase64").val(),
    imgName: $("#intervention #imgName").val(),
    imgNames: $("#intervention #imgNames").val(),
    external_referent_id: $("#intervention #referent").val(),
    nr_ddt: $("#intervention #nr_ddt").val(),
    date_ddt: $("#intervention #date_ddt").val(),
    series: $("#intervention #series").val(),
    trolley_type: $("#intervention #trolley_type").val(),
    plant_type: $("#intervention #plant_type").val(),
    defect: $("#intervention #defect").val(),
    voltage: $("#intervention #voltage").val(),
    exit_notes: $("#intervention #exit_notes").val(),
    gridCheck: $("#intervention #gridCheck").val(),
    fileddt: $("#intervention #imgBase64").val(),
    create_quote: create_quote
  };
  //var file_data = $('#imgmodule').prop('files')[0];
  var file_data = $('#fileddt').prop('files')[0];

  InterventionsServices.insertModule(data, function () {
    $("#intervention #referent").val("");
    $("#intervention #imgBase64").val("");
    $("#intervention #nr_ddt").val("");
    $("#intervention #date_ddt").val("");
    $("#intervention #series").val("");
    $("#intervention #trolley_type").val("");
    $("#intervention #plant_type").val("");
    $("#intervention #defect").val("");
    $("#intervention #voltage").val("");
    $("#intervention #exit_notes").val("");
    $("#intervention #gridCheck").val("");
    $("#intervention #create_quote").val("");
    notificationHelpers.success("Inserimento avvenuto correttamente!");
    spinnerHelpers.hide();
    window.location.reload();
    window.location.href = 'https://grsrl.net/pannello-cliente';
  });
}

function insert() {
  spinnerHelpers.show();

  var unrepairable = false;
  if ($("#frmAddIntervention #unrepairable").is(":checked")) {
    unrepairable = true;
  } else {
    unrepairable = false;
  }

  var create_quote = false;
  if ($("#frmAddIntervention #create_quote").is(":checked")) {
    create_quote = true;
  } else {
    create_quote = false;
  }

  var send_to_customer = false;
  if ($("#frmAddIntervention #send_to_customer").is(":checked")) {
    send_to_customer = true;
  } else {
    send_to_customer = false;
  }

  var data = {
    customer_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    // ADD frmAddIntervention
    id: $("#frmAddIntervention #id").val(),
    imgmodule: $("#frmAddIntervention #imgBase64").val(),
    imgName: $("#frmAddIntervention #imgName").val(),
    referent: $("#frmAddIntervention #referent").val(),
    nr_ddt: $("#frmAddIntervention #nr_ddt").val(),
    date_ddt: $("#frmAddIntervention #date_ddt").val(),
    series: $("#frmAddIntervention #series").val(),
    trolley_type: $("#frmAddIntervention #trolley_type").val(),
    code_intervention_gr: $("#frmAddIntervention #code_intervention_gr").val(),
    plant_type: $("#frmAddIntervention #plant_type").val(),
    defect: $("#frmAddIntervention #defect").val(),
    voltage: $("#frmAddIntervention #voltage").val(),
    exit_notes: $("#frmAddIntervention #exit_notes").val(),
    gridCheck: $("#frmAddIntervention #gridCheck").val(),
    states_id: $("#frmAddIntervention #states_id").val(),
    nr_ddt_gr: $("#frmAddIntervention #nr_ddt_gr").val(),
    date_ddt_gr: $("#frmAddIntervention #date_ddt_gr").val(),
    note_internal_gr: $("#frmAddIntervention #note_internal_gr").val(),
    user_id: $("#frmAddIntervention #user_new_id").val(),
    items_id: $("#frmAddIntervention #items_id").val(),
    description: $("#frmAddIntervention #description").val(),
    code_tracking: $("#frmAddIntervention #code_tracking").val(),
    rip_association: $("#frmAddIntervention #rip_association").val(),
    external_referent_id_intervention: $("#frmAddIntervention #external_referent_id_intervention").val(),
    unrepairable: unrepairable,
    create_quote: create_quote,
    send_to_customer: send_to_customer
  };

  var file_data = $('#imgmodule').prop('files')[0];

  var arrayImgAgg = [];
  for (var i = 0; i < 4; i++) {
    if (functionHelpers.isValued($("#imgBase64" + i).val())) {
      arrayImgAgg.push($("#imgBase64" + i).val());
    }
  }
  data.captures = arrayImgAgg;

  InterventionsServices.insert(data, function () {
    //ADD frmAddIntervention
    $("#frmAddIntervention #id").val("");
    $("#frmAddIntervention #referent").val("");
    $("#frmAddIntervention #code_intervention_gr").val("");
    $("#frmAddIntervention #imgBase64").val("");
    $("#frmAddIntervention #nr_ddt").val("");
    $("#frmAddIntervention #date_ddt").val("");
    $("#frmAddIntervention #series").val("");
    $("#frmAddIntervention #trolley_type").val("");
    $("#frmAddIntervention #plant_type").val("");
    $("#frmAddIntervention #defect").val("");
    $("#frmAddIntervention #voltage").val("");
    $("#frmAddIntervention #exit_notes").val("");
    $("#frmAddIntervention #gridCheck").val("");
    $("#frmAddIntervention #states_id").val("");
    $("#frmAddIntervention #nr_ddt_gr").val("");
    $("#frmAddIntervention #date_ddt_gr").val("");
    $("#frmAddIntervention #user_new_id").val("");
    $('#frmAddIntervention #note_internal_gr').val('');
    $("#frmAddIntervention #items_id").val("");
    $("#frmAddIntervention #states_id").val("");
    $("#frmAddIntervention #description").val("");
    $("#frmAddIntervention #unrepairable").val("");
    $("#frmAddIntervention #send_to_customer").val("");
    $("#frmAddIntervention #create_quote").val("");
    $("#frmAddIntervention #code_tracking").val("");
    $("#frmAddIntervention #rip_association").val("");
    notificationHelpers.success("Inserimento avvenuto correttamente!");
    spinnerHelpers.hide();
    window.location.reload();
    window.location.href = 'https://grsrl.net/gestione-materiali';
  });
}

function updateCheckRepairArrived(id) {
  spinnerHelpers.show();
  var data = {
    id: id
  };
  InterventionsServices.updateCheckRepairArrived(data, function () {
    $("#table_list_repair_arriving #repaired_arrived").val("");
    notificationHelpers.success("Modifica avvenuta con successo");
    window.location.reload();
    spinnerHelpers.hide();
    $("#table_list_repair_arriving").show();
  });
}

function getAllRepairArriving() {
  spinnerHelpers.show();
  InterventionsServices.getAllRepairArriving(storageData.sIdLanguage(), function (response) {

    for (var i = 0; i < response.data.length; i++) {
      var newList = "";
      newList += "<tr>";
      newList += '<td><button class="btn btn-info" onclick="interventionHelpers.getById(' + response.data[i].id + ')"><i class="fa fa-pencil"/></button></td>';
      if (response.data[i].repaired_arrived == true) {
        newList += '<td><input type="checkbox"  id="form-control ready' + i + '" value="' + response.data[i].repaired_arrived + '" name="ready' + i + '" class="switch-input' + i + '" checked="true' + i + '" onclick="interventionHelpers.updateCheckRepairArrived(' + response.data[i].id + ')"></td>';
      } else {
        newList += '<td><input type="checkbox" id="form-control ready' + i + '" value="' + response.data[i].repaired_arrived + '" name="ready' + i + '" class="switch-input' + i + '" onclick="interventionHelpers.updateCheckRepairArrived(' + response.data[i].id + ')"></td>';
      }
      newList += "<td>" + response.data[i].id + "</td>";
      newList += "<td>" + response.data[i].user_id + "</td>";
      newList += "<td>" + response.data[i].date_ddt + "</td>";
      newList += "<td>" + response.data[i].nr_ddt + "</td>";
      newList += "<td>" + response.data[i].series + "</td>";
      newList += "<td>" + response.data[i].plant_type + "</td>";
      newList += "<td>" + response.data[i].trolley_type + "</td>";
      newList += "<td>" + response.data[i].defect + "</td>";
      newList += "<td>" + response.data[i].voltage + "</td>";
      newList += "<td>" + response.data[i].exit_notes + "</td>";
      newList += "<td>" + response.data[i].description + "</td>";
      newList += "<td>" + response.data[i].nr_ddt_gr + "</td>";
      newList += "<td>" + response.data[i].date_ddt_gr + "</td>";
      newList += "</tr>";
      $("#table_list_repair_arriving tbody").append(newList);
      $("#table_list_repair_arriving").show();
      $("#frmUpdateIntervention").hide();
    }
    lightGallery(document.getElementById('lightgallery'));
  });

  spinnerHelpers.hide();
}

function ViewAll() {
  spinnerHelpers.show();
  $(".loader").show();
  InterventionsServices.ViewAll(storageData.sIdLanguage(), function (response) {
    $("#table_list_intervention tbody").html('');
    if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_intervention tbody").append(newList);
      $("#table_list_intervention").show();
      $("#table_list_searchintervention").hide();
      $("#table_list_repair_arriving").hide();
      $("#frmUpdateIntervention").hide();
      $("#frmViewUser").hide();
      $("#frmViewProcessingSheet").hide();
      $("#frmViewQuotes").hide();
      $("#frmViewExternalRepair").hide();
      $("#frmViewSelectReferent").hide();
      $("#ModuleValidationGuarantee").hide();
      $("#FaultModule").hide();
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        if (response.data[i].ready) {
          if (response.data[i].nr_ddt_gr == null || response.data[i].nr_ddt_gr == "" || (response.data[i].date_ddt_gr == null || response.data[i].date_ddt_gr == "") && (response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable)) {

            newList += "<tr class='color-orange'>";
          } else {
            newList += "<tr class='color-white'>";
          }
        } else {
          if (response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable) {
            newList += "<tr class='color-orange'>";
          } else {
            newList += "<tr class='color-white'>";
          }
        }

        if (response.data[i].ready == true) {
          newList += '<td><input type="checkbox"  id="form-control ready' + i + '" value="' + response.data[i].ready + '" name="ready' + i + '" class="switch-input' + i + '" checked="true' + i + '" onclick="interventionHelpers.updateCheck(' + response.data[i].id + ')"></td>';
        } else {
          newList += '<td><input type="checkbox" id="form-control ready' + i + '" value="' + response.data[i].ready + '" name="ready' + i + '" class="switch-input' + i + '" onclick="interventionHelpers.updateCheck(' + response.data[i].id + ')"></td>';
        }

        newList += '<td><button class="btn btn-info" style="background-color: #AEC4F1;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Compila Fast Urgenza!" onclick="interventionHelpers.getByInterventionFU(' + response.data[i].id + ')"><i class="fa fa-bolt" style="color:#fff;"/></button></td>';

        if (response.data[i].checkProcessingExist == true) {
          newList += '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' + response.data[i].id + ')"><i class="fa fa-circle-o" style="color:#007fff!important"/></button></td>';
        } else {
          newList += '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' + response.data[i].id + ')"><i class="fa fa-pencil-square-o" style="color:#007fff!important"/></button></td>';
        }

        newList += "<td>";
        if (response.data[i].id_states_quote != 'Accettato' && response.data[i].id_states_quote != 'Rifiutato' && response.data[i].id_states_quote != 'In Attesa') {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:navi!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:navi!important;"/></button>';
        }
        if (response.data[i].id_states_quote == 'In Attesa') {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:#f5ea74!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:#f5ea74!important;"/></button>';
        }

        if (response.data[i].id_states_quote == 'Accettato') {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:green!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:green!important;"/></button>';
        }
        if (response.data[i].id_states_quote == 'Rifiutato') {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:red!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:red!important;"/></button>';
        }
        "</td>";

        /*newList +=
          '<td><button class="btn btn-link" onclick="interventionHelpers.getByViewQuotes(' +
          response.data[i].id +
          ')"><i class="fa fa-flag"/></button></td>';*/
        newList += "<td style='white-space:nowrap;'>" + response.data[i].id + "</td>";
        newList += "<td style='white-space:nowrap;'>" + response.data[i].code_intervention_gr + "</td>";
        newList += '<td><a  style="cursor:pointer;white-space: nowrap; color:#787878 !important;" data-toggle="tooltip" data-placement="top" title="Visualizza dettaglio Utente!" onclick="interventionHelpers.getByIdUser(' + response.data[i].user_id + ')">' + response.data[i].descriptionCustomer + '</a></td>';
        newList += "<td>" + response.data[i].date_ddt + "</td>";
        newList += "<td>" + response.data[i].nr_ddt + "</td>";

        //newList += "<td><a href='" + response.data[i].imgmodule + "'</a>Visualizza</td>";
        newList += "<td>" + response.data[i].description + response.data[i].rip_association + "</td>";
        //newList += "<td>" + response.data[i].defect + "</td>";
        newList += "<td>";
        for (var j = 0; j < response.data[i].captures.length; j++) {
          newList += '<div class="container"><div id="lightgallery" style="display:flex">';
          newList += "<a href='" + response.data[i].captures[j].img + "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        newList += "</td>";
        newList += "<td>";
        if (response.data[i].imgmodule != "" && response.data[i].imgmodule != '-') {
          newList += '<div class="container"><div id="lightgallery" style="display:flex">';
          newList += "<a href='" + response.data[i].imgmodule + "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a> " + "'</div></div>";
        }
        if (response.data[i].imgmodule == "") {
          newList += "<div>" + response.data[i].defect + "</div>";
        }

        //
        if (response.data[i].checkModuleFault == true) {
          newList += '<button class="btn btn-green" style="color:#007FFF!important;" data-toggle="tooltip" data-placement="top" title="Visualizza Modulo Guasti!"  onclick="interventionHelpers.getByFaultModule(' + response.data[i].id + ')"><i class="fa fa-file-pdf-o" style="color:#007FFF!important;"/></button>';
        }
        if (response.data[i].checkModuleFault == false) {
          newList += "<div>" + "</div>";
        }
        //
        "</td>";
        newList += "<td>" + response.data[i].codeGr + "</td>";
        newList += "<td>" + response.data[i].date_ddt_gr + "</td>";
        newList += "<td>" + response.data[i].nr_ddt_gr + "</td>";

        if (response.data[i].idInterventionExternalRepair == null) {
          newList += '<td><button class="btn btn-green" style="color:#d7e7fe !important;font-size:28px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-truck" style="color:#d7e7fe !important"/></button></td>';
        }
        if (response.data[i].idInterventionExternalRepair != null) {
          newList += '<td><button class="btn btn-green" style="color:#02784d !important;font-size:26px;margin-top: 21px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-truck" style="color:#02784d !important;"/></button>';
        }
        if (response.data[i].ddt_supplier != null && response.data[i].ddt_supplier != "") {
          newList += '<button class="btn btn-green" style="color:#02784d !important;font-size:27px;margin-top: -100px!important;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-reply" style="color:#02784d !important;>" aria-hidden="true"></i></button></td>';
        }
        newList += '<td><button class="btn btn-info" style="background-color: #ff337b;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Sezione gestione del tecnico" onclick="interventionHelpers.getBySelectReferent(' + response.data[i].id + ')"><i class="fa fa-circle-o" style="color:#fff;"/></button></td>';
        if (response.data[i].create_quote == "Si") {
          newList += "<td style='color:green;'>" + response.data[i].create_quote + "</td>";
        } else {
          newList += "<td style='color:red;'>" + response.data[i].create_quote + "</td>";
        }
        // newList += "<td>" + response.data[i].descriptionStates + "</td>";
        newList += "<td>" + response.data[i].code_tracking + "</td>";
        newList += "<td>" + response.data[i].note_internal_gr + "</td>";
        newList += "<td>";
        if (response.data[i].fileddt != "" && response.data[i].fileddt != '-') {
          newList += '<div class="container"><div id="lightgallery" style="display:flex">';
          newList += "<a href='" + response.data[i].fileddt + "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:red;float:right;'></i></a></div></div>";
        }
        if (response.data[i].fileddt == "") {
          newList += "<div>" + "</div>";
        }
        newList += "</td>";
        newList += '<td><button class="btn btn-info" onclick="interventionHelpers.getById(' + response.data[i].id + ')"><i class="fa fa-pencil"style="color:#fff"/></button></td>';

        newList += "<td>";
        if (response.data[i].checkModuleValidationGuarantee == true) {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Modulo Garanzie" style="color:#007FFF!important;" onclick="interventionHelpers.getByModuleGuarantee(' + response.data[i].id + ')"><i class="fa fa-external-link" style="color:#007FFF!important;"/></button>';
        }
        if (response.data[i].checkModuleValidationGuarantee == false) {
          newList += "<div>" + "</div>";
        }
        for (var j = 0; j < response.data[i].captures.length; j++) {
          newList += '<div class="container"><div id="lightgallery" style="display:flex">';
          newList += "<a href='" + response.data[i].captures[j].img_rep_test + "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        "</td>";
        newList += '<td><button class="btn btn-danger" onclick="interventionHelpers.deleteRow(' + response.data[i].id + ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
        newList += "</tr>";
        $("#table_list_intervention tbody").append(newList);
        $("#table_list_intervention").show();
        $("#table_list_searchintervention").hide();
        $("#table_list_repair_arriving").hide();
        $("#frmUpdateIntervention").hide();
        $("#frmViewUser").hide();
        $("#frmViewProcessingSheet").hide();
        $("#frmViewQuotes").hide();
        $("#frmViewExternalRepair").hide();
        $("#frmViewSelectReferent").hide();
        $("#ModuleValidationGuarantee").hide();
        $("#FaultModule").hide();
      }
    }
    lightGallery(document.getElementById("lightgallery"));
    $(".loader").hide();
  });

  spinnerHelpers.hide();
}

function SearchTrue(id) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(id);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  InterventionsServices.SearchTrue(data, function (response) {
    $("#table_list_intervention tbody").html('');

    if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_intervention tbody").append(newList);
      $("#table_list_intervention").show();
      $("#table_list_searchintervention").hide();
      $("#table_list_repair_arriving").hide();
      $("#frmUpdateIntervention").hide();
      $("#frmViewUser").hide();
      $("#frmViewProcessingSheet").hide();
      $("#frmViewQuotes").hide();
      $("#frmViewExternalRepair").hide();
      $("#frmViewSelectReferent").hide();
      $("#ModuleValidationGuarantee").hide();
      $("#FaultModule").hide();
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        if (response.data[i].ready) {
          if (response.data[i].nr_ddt_gr == null || response.data[i].nr_ddt_gr == "" || (response.data[i].date_ddt_gr == null || response.data[i].date_ddt_gr == "") && (response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable)) {

            newList += "<tr class='color-orange'>";
          } else {
            newList += "<tr class='color-white'>";
          }
        } else {
          if (response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable) {
            newList += "<tr class='color-orange'>";
          } else {
            newList += "<tr class='color-white'>";
          }
        }

        if (response.data[i].ready == true) {
          newList += '<td><input type="checkbox"  id="form-control ready' + i + '" value="' + response.data[i].ready + '" name="ready' + i + '" class="switch-input' + i + '" checked="true' + i + '" onclick="interventionHelpers.updateCheck(' + response.data[i].id + ')"></td>';
        } else {
          newList += '<td><input type="checkbox" id="form-control ready' + i + '" value="' + response.data[i].ready + '" name="ready' + i + '" class="switch-input' + i + '" onclick="interventionHelpers.updateCheck(' + response.data[i].id + ')"></td>';
        }

        newList += '<td><button class="btn btn-info" style="background-color: #AEC4F1;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Compila Fast Urgenza!" onclick="interventionHelpers.getByInterventionFU(' + response.data[i].id + ')"><i class="fa fa-bolt" style="color:#fff;"/></button></td>';

        if (response.data[i].checkProcessingExist == true) {
          newList += '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' + response.data[i].id + ')"><i class="fa fa-circle-o" style="color:#007fff!important"/></button></td>';
        } else {
          newList += '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' + response.data[i].id + ')"><i class="fa fa-pencil-square-o" style="color:#007fff!important"/></button></td>';
        }
        newList += "<td>";
        if (response.data[i].id_states_quote != 'Accettato' && response.data[i].id_states_quote != 'Rifiutato' && response.data[i].id_states_quote != 'In Attesa') {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:navi!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:navi!important;"/></button>';
        }
        if (response.data[i].id_states_quote == 'In Attesa') {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:#f5ea74!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:#f5ea74!important;"/></button>';
        }

        if (response.data[i].id_states_quote == 'Accettato') {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:green!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:green!important;"/></button>';
        }
        if (response.data[i].id_states_quote == 'Rifiutato') {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:red!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:red!important;"/></button>';
        }
        "</td>";

        /*newList +=
          '<td><button class="btn btn-link" onclick="interventionHelpers.getByViewQuotes(' +
          response.data[i].id +
          ')"><i class="fa fa-flag"/></button></td>';*/
        newList += "<td style='white-space:nowrap;'>" + response.data[i].id + "</td>";
        newList += "<td style='white-space:nowrap;'>" + response.data[i].code_intervention_gr + "</td>";
        newList += '<td><a  style="cursor:pointer;white-space: nowrap; color:#787878 !important;" data-toggle="tooltip" data-placement="top" title="Visualizza dettaglio Utente!" onclick="interventionHelpers.getByIdUser(' + response.data[i].user_id + ')">' + response.data[i].descriptionCustomer + '</a></td>';
        newList += "<td>" + response.data[i].date_ddt + "</td>";
        newList += "<td>" + response.data[i].nr_ddt + "</td>";

        //newList += "<td><a href='" + response.data[i].imgmodule + "'</a>Visualizza</td>";
        newList += "<td>" + response.data[i].description + response.data[i].rip_association + "</td>";
        //newList += "<td>" + response.data[i].defect + "</td>";
        newList += "<td>";
        for (var j = 0; j < response.data[i].captures.length; j++) {
          newList += '<div class="container"><div id="lightgallery" style="display:flex">';
          newList += "<a href='" + response.data[i].captures[j].img + "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        newList += "</td>";
        newList += "<td>";
        if (response.data[i].imgmodule != "" && response.data[i].imgmodule != '-') {
          newList += '<div class="container"><div id="lightgallery" style="display:flex">';
          newList += "<a href='" + response.data[i].imgmodule + "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a> " + "'</div></div>";
        }
        if (response.data[i].imgmodule == "") {
          newList += "<div>" + response.data[i].defect + "</div>";
        }
        //
        if (response.data[i].checkModuleFault == true) {
          newList += '<button class="btn btn-green" style="color:#007FFF!important;" data-toggle="tooltip" data-placement="top" title="Visualizza Modulo Guasti!"  onclick="interventionHelpers.getByFaultModule(' + response.data[i].id + ')"><i class="fa fa-file-pdf-o" style="color:#007FFF!important;"/></button>';
        }
        if (response.data[i].checkModuleFault == false) {
          newList += "<div>" + "</div>";
        }
        //
        "</td>";
        newList += "<td>" + response.data[i].codeGr + "</td>";
        newList += "<td>" + response.data[i].date_ddt_gr + "</td>";
        newList += "<td>" + response.data[i].nr_ddt_gr + "</td>";

        if (response.data[i].idInterventionExternalRepair == null) {
          newList += '<td><button class="btn btn-green" style="color:#d7e7fe !important;font-size:28px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-truck" style="color:#d7e7fe !important"/></button></td>';
        }
        if (response.data[i].idInterventionExternalRepair != null) {
          newList += '<td><button class="btn btn-green" style="color:#02784d !important;font-size:26px;margin-top: 21px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-truck" style="color:#02784d !important;"/></button>';
        }
        if (response.data[i].ddt_supplier != null && response.data[i].ddt_supplier != "") {
          newList += '<button class="btn btn-green" style="color:#02784d !important;font-size:27px;margin-top: -100px!important;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-reply" style="color:#02784d !important;>" aria-hidden="true"></i></button></td>';
        }
        newList += '<td><button class="btn btn-info" style="background-color: #ff337b;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Sezione gestione del tecnico" onclick="interventionHelpers.getBySelectReferent(' + response.data[i].id + ')"><i class="fa fa-circle-o" style="color:#fff;"/></button></td>';
        if (response.data[i].create_quote == "Si") {
          newList += "<td style='color:green;'>" + response.data[i].create_quote + "</td>";
        } else {
          newList += "<td style='color:red;'>" + response.data[i].create_quote + "</td>";
        }
        // newList += "<td>" + response.data[i].descriptionStates + "</td>";
        newList += "<td>" + response.data[i].code_tracking + "</td>";
        newList += "<td>" + response.data[i].note_internal_gr + "</td>";
        newList += "<td>";
        if (response.data[i].fileddt != "" && response.data[i].fileddt != '-') {
          newList += '<div class="container"><div id="lightgallery" style="display:flex">';
          newList += "<a href='" + response.data[i].fileddt + "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:red;float:right;'></i></a></div></div>";
        }
        if (response.data[i].fileddt == "") {
          newList += "<div>" + "</div>";
        }
        newList += "</td>";
        newList += '<td><button class="btn btn-info" onclick="interventionHelpers.getById(' + response.data[i].id + ')"><i class="fa fa-pencil"style="color:#fff"/></button></td>';

        newList += "<td>";
        if (response.data[i].checkModuleValidationGuarantee == true) {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Modulo Garanzie" style="color:#007FFF!important;" onclick="interventionHelpers.getByModuleGuarantee(' + response.data[i].id + ')"><i class="fa fa-external-link" style="color:#007FFF!important;"/></button>';
        }
        if (response.data[i].checkModuleValidationGuarantee == false) {
          newList += "<div>" + "</div>";
        }
        for (var j = 0; j < response.data[i].captures.length; j++) {
          newList += '<div class="container"><div id="lightgallery" style="display:flex">';
          newList += "<a href='" + response.data[i].captures[j].img_rep_test + "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        "</td>";

        newList += '<td><button class="btn btn-danger" onclick="interventionHelpers.deleteRow(' + response.data[i].id + ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
        newList += "</tr>";
        $("#table_list_intervention tbody").append(newList);
        $("#table_list_intervention").show();
        $("#table_list_searchintervention").hide();
        $("#table_list_repair_arriving").hide();
        $("#frmUpdateIntervention").hide();
        $("#frmViewUser").hide();
        $("#frmViewProcessingSheet").hide();
        $("#frmViewQuotes").hide();
        $("#frmViewExternalRepair").hide();
        $("#frmViewSelectReferent").hide();
        $("#ModuleValidationGuarantee").hide();
        $("#FaultModule").hide();
      }
    }
    lightGallery(document.getElementById("lightgallery"));
  });
  spinnerHelpers.hide();
}

function SearchOpen(id) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(id);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  InterventionsServices.SearchOpen(data, function (response) {
    $("#table_list_intervention tbody").html('');
    if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_intervention tbody").append(newList);
      $("#table_list_intervention").show();
      $("#table_list_searchintervention").hide();
      $("#table_list_repair_arriving").hide();
      $("#frmUpdateIntervention").hide();
      $("#frmViewUser").hide();
      $("#frmViewProcessingSheet").hide();
      $("#frmViewQuotes").hide();
      $("#frmViewExternalRepair").hide();
      $("#frmViewSelectReferent").hide();
      $("#ModuleValidationGuarantee").hide();
      $("#FaultModule").hide();
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        if (response.data[i].ready) {
          if (response.data[i].nr_ddt_gr == null || response.data[i].nr_ddt_gr == "" || (response.data[i].date_ddt_gr == null || response.data[i].date_ddt_gr == "") && (response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable)) {

            newList += "<tr class='color-orange'>";
          } else {
            newList += "<tr class='color-white'>";
          }
        } else {
          if (response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable) {
            newList += "<tr class='color-orange'>";
          } else {
            newList += "<tr class='color-white'>";
          }
        }

        if (response.data[i].ready == true) {
          newList += '<td><input type="checkbox"  id="form-control ready' + i + '" value="' + response.data[i].ready + '" name="ready' + i + '" class="switch-input' + i + '" checked="true' + i + '" onclick="interventionHelpers.updateCheck(' + response.data[i].id + ')"></td>';
        } else {
          newList += '<td><input type="checkbox" id="form-control ready' + i + '" value="' + response.data[i].ready + '" name="ready' + i + '" class="switch-input' + i + '" onclick="interventionHelpers.updateCheck(' + response.data[i].id + ')"></td>';
        }

        newList += '<td><button class="btn btn-info" style="background-color: #AEC4F1;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Compila Fast Urgenza!" onclick="interventionHelpers.getByInterventionFU(' + response.data[i].id + ')"><i class="fa fa-bolt" style="color:#fff;"/></button></td>';

        if (response.data[i].checkProcessingExist == true) {
          newList += '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' + response.data[i].id + ')"><i class="fa fa-circle-o" style="color:#007fff!important"/></button></td>';
        } else {
          newList += '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' + response.data[i].id + ')"><i class="fa fa-pencil-square-o" style="color:#007fff!important"/></button></td>';
        }
        newList += "<td>";
        if (response.data[i].id_states_quote != 'Accettato' && response.data[i].id_states_quote != 'Rifiutato' && response.data[i].id_states_quote != 'In Attesa') {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:navi!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:navi!important;"/></button>';
        }
        if (response.data[i].id_states_quote == 'In Attesa') {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:#f5ea74!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:#f5ea74!important;"/></button>';
        }

        if (response.data[i].id_states_quote == 'Accettato') {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:green!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:green!important;"/></button>';
        }
        if (response.data[i].id_states_quote == 'Rifiutato') {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:red!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:red!important;"/></button>';
        }
        "</td>";

        /*newList +=
          '<td><button class="btn btn-link" onclick="interventionHelpers.getByViewQuotes(' +
          response.data[i].id +
          ')"><i class="fa fa-flag"/></button></td>';*/
        newList += "<td style='white-space:nowrap;'>" + response.data[i].id + "</td>";
        newList += "<td style='white-space:nowrap;'>" + response.data[i].code_intervention_gr + "</td>";
        newList += '<td><a  style="cursor:pointer;white-space: nowrap; color:#787878 !important;" data-toggle="tooltip" data-placement="top" title="Visualizza dettaglio Utente!" onclick="interventionHelpers.getByIdUser(' + response.data[i].user_id + ')">' + response.data[i].descriptionCustomer + '</a></td>';
        newList += "<td>" + response.data[i].date_ddt + "</td>";
        newList += "<td>" + response.data[i].nr_ddt + "</td>";

        //newList += "<td><a href='" + response.data[i].imgmodule + "'</a>Visualizza</td>";
        newList += "<td>" + response.data[i].description + response.data[i].rip_association + "</td>";
        //newList += "<td>" + response.data[i].defect + "</td>";
        newList += "<td>";
        for (var j = 0; j < response.data[i].captures.length; j++) {
          newList += '<div class="container"><div id="lightgallery" style="display:flex">';
          newList += "<a href='" + response.data[i].captures[j].img + "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        newList += "</td>";
        newList += "<td>";
        if (response.data[i].imgmodule != "" && response.data[i].imgmodule != '-') {
          newList += '<div class="container"><div id="lightgallery" style="display:flex">';
          newList += "<a href='" + response.data[i].imgmodule + "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a> " + "'</div></div>";
        }
        if (response.data[i].imgmodule == "") {
          newList += "<div>" + response.data[i].defect + "</div>";
        }

        //
        if (response.data[i].checkModuleFault == true) {
          newList += '<button class="btn btn-green" style="color:#007FFF!important;" data-toggle="tooltip" data-placement="top" title="Visualizza Modulo Guasti!"  onclick="interventionHelpers.getByFaultModule(' + response.data[i].id + ')"><i class="fa fa-file-pdf-o" style="color:#007FFF!important;"/></button>';
        }
        if (response.data[i].checkModuleFault == false) {
          newList += "<div>" + "</div>";
        }
        //
        "</td>";
        newList += "<td>" + response.data[i].codeGr + "</td>";
        newList += "<td>" + response.data[i].date_ddt_gr + "</td>";
        newList += "<td>" + response.data[i].nr_ddt_gr + "</td>";

        if (response.data[i].idInterventionExternalRepair == null) {
          newList += '<td><button class="btn btn-green" style="color:#d7e7fe !important;font-size:28px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-truck" style="color:#d7e7fe !important"/></button></td>';
        }
        if (response.data[i].idInterventionExternalRepair != null) {
          newList += '<td><button class="btn btn-green" style="color:#02784d !important;font-size:26px;margin-top: 21px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-truck" style="color:#02784d !important;"/></button>';
        }
        if (response.data[i].ddt_supplier != null && response.data[i].ddt_supplier != "") {
          newList += '<button class="btn btn-green" style="color:#02784d !important;font-size:27px;margin-top: -100px!important;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-reply" style="color:#02784d !important;>" aria-hidden="true"></i></button></td>';
        }
        newList += '<td><button class="btn btn-info" style="background-color: #ff337b;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Sezione gestione del tecnico" onclick="interventionHelpers.getBySelectReferent(' + response.data[i].id + ')"><i class="fa fa-circle-o" style="color:#fff;"/></button></td>';
        if (response.data[i].create_quote == "Si") {
          newList += "<td style='color:green;'>" + response.data[i].create_quote + "</td>";
        } else {
          newList += "<td style='color:red;'>" + response.data[i].create_quote + "</td>";
        }
        // newList += "<td>" + response.data[i].descriptionStates + "</td>";
        newList += "<td>" + response.data[i].code_tracking + "</td>";
        newList += "<td>" + response.data[i].note_internal_gr + "</td>";
        newList += "<td>";
        if (response.data[i].fileddt != "" && response.data[i].fileddt != '-') {
          newList += '<div class="container"><div id="lightgallery" style="display:flex">';
          newList += "<a href='" + response.data[i].fileddt + "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:red;float:right;'></i></a></div></div>";
        }
        if (response.data[i].fileddt == "") {
          newList += "<div>" + "</div>";
        }
        newList += "</td>";
        newList += '<td><button class="btn btn-info" onclick="interventionHelpers.getById(' + response.data[i].id + ')"><i class="fa fa-pencil"style="color:#fff"/></button></td>';

        newList += "<td>";
        if (response.data[i].checkModuleValidationGuarantee == true) {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Modulo Garanzie" style="color:#007FFF!important;" onclick="interventionHelpers.getByModuleGuarantee(' + response.data[i].id + ')"><i class="fa fa-external-link" style="color:#007FFF!important;"/></button>';
        }
        if (response.data[i].checkModuleValidationGuarantee == false) {
          newList += "<div>" + "</div>";
        }
        for (var j = 0; j < response.data[i].captures.length; j++) {
          newList += '<div class="container"><div id="lightgallery" style="display:flex">';
          newList += "<a href='" + response.data[i].captures[j].img_rep_test + "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        "</td>";

        newList += '<td><button class="btn btn-danger" onclick="interventionHelpers.deleteRow(' + response.data[i].id + ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
        newList += "</tr>";
        $("#table_list_intervention tbody").append(newList);
        $("#table_list_intervention").show();
        $("#table_list_searchintervention").hide();
        $("#table_list_repair_arriving").hide();
        $("#frmUpdateIntervention").hide();
        $("#frmViewUser").hide();
        $("#frmViewProcessingSheet").hide();
        $("#frmViewQuotes").hide();
        $("#frmViewExternalRepair").hide();
        $("#frmViewSelectReferent").hide();
        $("#ModuleValidationGuarantee").hide();
        $("#FaultModule").hide();
      }
    }
    lightGallery(document.getElementById("lightgallery"));
  });

  spinnerHelpers.hide();
}

function SearchClose(id) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(id);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  InterventionsServices.SearchClose(data, function (response) {
    $("#table_list_intervention tbody").html('');
    if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_intervention tbody").append(newList);
      $("#table_list_intervention").show();
      $("#table_list_searchintervention").hide();
      $("#table_list_repair_arriving").hide();
      $("#frmUpdateIntervention").hide();
      $("#frmViewUser").hide();
      $("#frmViewProcessingSheet").hide();
      $("#frmViewQuotes").hide();
      $("#frmViewExternalRepair").hide();
      $("#frmViewSelectReferent").hide();
      $("#ModuleValidationGuarantee").hide();
      $("#FaultModule").hide();
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        if (response.data[i].ready) {
          if (response.data[i].nr_ddt_gr == null || response.data[i].nr_ddt_gr == "" || (response.data[i].date_ddt_gr == null || response.data[i].date_ddt_gr == "") && (response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable)) {

            newList += "<tr class='color-orange'>";
          } else {
            newList += "<tr class='color-white'>";
          }
        } else {
          if (response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable) {
            newList += "<tr class='color-orange'>";
          } else {
            newList += "<tr class='color-white'>";
          }
        }

        if (response.data[i].ready == true) {
          newList += '<td><input type="checkbox"  id="form-control ready' + i + '" value="' + response.data[i].ready + '" name="ready' + i + '" class="switch-input' + i + '" checked="true' + i + '" onclick="interventionHelpers.updateCheck(' + response.data[i].id + ')"></td>';
        } else {
          newList += '<td><input type="checkbox" id="form-control ready' + i + '" value="' + response.data[i].ready + '" name="ready' + i + '" class="switch-input' + i + '" onclick="interventionHelpers.updateCheck(' + response.data[i].id + ')"></td>';
        }

        newList += '<td><button class="btn btn-info" style="background-color: #AEC4F1;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Compila Fast Urgenza!" onclick="interventionHelpers.getByInterventionFU(' + response.data[i].id + ')"><i class="fa fa-bolt" style="color:#fff;"/></button></td>';

        if (response.data[i].checkProcessingExist == true) {
          newList += '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' + response.data[i].id + ')"><i class="fa fa-circle-o" style="color:#007fff!important"/></button></td>';
        } else {
          newList += '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' + response.data[i].id + ')"><i class="fa fa-pencil-square-o" style="color:#007fff!important"/></button></td>';
        }

        newList += "<td>";
        if (response.data[i].id_states_quote != 'Accettato' && response.data[i].id_states_quote != 'Rifiutato' && response.data[i].id_states_quote != 'In Attesa') {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:navi!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:navi!important;"/></button>';
        }
        if (response.data[i].id_states_quote == 'In Attesa') {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:#f5ea74!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:#f5ea74!important;"/></button>';
        }

        if (response.data[i].id_states_quote == 'Accettato') {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:green!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:green!important;"/></button>';
        }
        if (response.data[i].id_states_quote == 'Rifiutato') {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:red!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:red!important;"/></button>';
        }
        "</td>";

        /*newList +=
          '<td><button class="btn btn-link" onclick="interventionHelpers.getByViewQuotes(' +
          response.data[i].id +
          ')"><i class="fa fa-flag"/></button></td>';*/
        newList += "<td style='white-space:nowrap;'>" + response.data[i].id + "</td>";
        newList += "<td style='white-space:nowrap;'>" + response.data[i].code_intervention_gr + "</td>";
        newList += '<td><a  style="cursor:pointer;white-space: nowrap; color:#787878 !important;" data-toggle="tooltip" data-placement="top" title="Visualizza dettaglio Utente!" onclick="interventionHelpers.getByIdUser(' + response.data[i].user_id + ')">' + response.data[i].descriptionCustomer + '</a></td>';
        newList += "<td>" + response.data[i].date_ddt + "</td>";
        newList += "<td>" + response.data[i].nr_ddt + "</td>";

        //newList += "<td><a href='" + response.data[i].imgmodule + "'</a>Visualizza</td>";
        newList += "<td>" + response.data[i].description + response.data[i].rip_association + "</td>";
        //newList += "<td>" + response.data[i].defect + "</td>";
        newList += "<td>";
        for (var j = 0; j < response.data[i].captures.length; j++) {
          newList += '<div class="container"><div id="lightgallery" style="display:flex">';
          newList += "<a href='" + response.data[i].captures[j].img + "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        newList += "</td>";
        newList += "<td>";
        if (response.data[i].imgmodule != "" && response.data[i].imgmodule != '-') {
          newList += '<div class="container"><div id="lightgallery" style="display:flex">';
          newList += "<a href='" + response.data[i].imgmodule + "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a> " + "'</div></div>";
        }
        if (response.data[i].imgmodule == "") {
          newList += "<div>" + response.data[i].defect + "</div>";
        }

        //
        if (response.data[i].checkModuleFault == true) {
          newList += '<button class="btn btn-green" style="color:#007FFF!important;" data-toggle="tooltip" data-placement="top" title="Visualizza Modulo Guasti!"  onclick="interventionHelpers.getByFaultModule(' + response.data[i].id + ')"><i class="fa fa-file-pdf-o" style="color:#007FFF!important;"/></button>';
        }
        if (response.data[i].checkModuleFault == false) {
          newList += "<div>" + "</div>";
        }
        //
        "</td>";
        newList += "<td>" + response.data[i].codeGr + "</td>";
        newList += "<td>" + response.data[i].date_ddt_gr + "</td>";
        newList += "<td>" + response.data[i].nr_ddt_gr + "</td>";

        if (response.data[i].idInterventionExternalRepair == null) {
          newList += '<td><button class="btn btn-green" style="color:#d7e7fe !important;font-size:28px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-truck" style="color:#d7e7fe !important"/></button></td>';
        }
        if (response.data[i].idInterventionExternalRepair != null) {
          newList += '<td><button class="btn btn-green" style="color:#02784d !important;font-size:26px;margin-top: 21px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-truck" style="color:#02784d !important;"/></button>';
        }
        if (response.data[i].ddt_supplier != null && response.data[i].ddt_supplier != "") {
          newList += '<button class="btn btn-green" style="color:#02784d !important;font-size:27px;margin-top: -100px!important;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-reply" style="color:#02784d !important;>" aria-hidden="true"></i></button></td>';
        }
        newList += '<td><button class="btn btn-info" style="background-color: #ff337b;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Sezione gestione del tecnico" onclick="interventionHelpers.getBySelectReferent(' + response.data[i].id + ')"><i class="fa fa-circle-o" style="color:#fff;"/></button></td>';
        if (response.data[i].create_quote == "Si") {
          newList += "<td style='color:green;'>" + response.data[i].create_quote + "</td>";
        } else {
          newList += "<td style='color:red;'>" + response.data[i].create_quote + "</td>";
        }
        // newList += "<td>" + response.data[i].descriptionStates + "</td>";
        newList += "<td>" + response.data[i].code_tracking + "</td>";
        newList += "<td>" + response.data[i].note_internal_gr + "</td>";
        newList += "<td>";
        if (response.data[i].fileddt != "" && response.data[i].fileddt != '-') {
          newList += '<div class="container"><div id="lightgallery" style="display:flex">';
          newList += "<a href='" + response.data[i].fileddt + "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:red;float:right;'></i></a></div></div>";
        }
        if (response.data[i].fileddt == "") {
          newList += "<div>" + "</div>";
        }
        newList += "</td>";
        newList += '<td><button class="btn btn-info" onclick="interventionHelpers.getById(' + response.data[i].id + ')"><i class="fa fa-pencil"style="color:#fff"/></button></td>';

        newList += "<td>";
        if (response.data[i].checkModuleValidationGuarantee == true) {
          newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Modulo Garanzie" style="color:#007FFF!important;" onclick="interventionHelpers.getByModuleGuarantee(' + response.data[i].id + ')"><i class="fa fa-external-link" style="color:#007FFF!important;"/></button>';
        }
        if (response.data[i].checkModuleValidationGuarantee == false) {
          newList += "<div>" + "</div>";
        }
        for (var j = 0; j < response.data[i].captures.length; j++) {
          newList += '<div class="container"><div id="lightgallery" style="display:flex">';
          newList += "<a href='" + response.data[i].captures[j].img_rep_test + "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        "</td>";

        newList += '<td><button class="btn btn-danger" onclick="interventionHelpers.deleteRow(' + response.data[i].id + ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
        newList += "</tr>";
        $("#table_list_intervention tbody").append(newList);
        $("#table_list_intervention").show();
        $("#table_list_searchintervention").hide();
        $("#table_list_repair_arriving").hide();
        $("#frmUpdateIntervention").hide();
        $("#frmViewUser").hide();
        $("#frmViewProcessingSheet").hide();
        $("#frmViewQuotes").hide();
        $("#frmViewExternalRepair").hide();
        $("#frmViewSelectReferent").hide();
        $("#ModuleValidationGuarantee").hide();
        $("#FaultModule").hide();
      }
    }
    lightGallery(document.getElementById("lightgallery"));
  });

  spinnerHelpers.hide();
}

function getAllIntervention() {
  spinnerHelpers.show();
  InterventionsServices.getAllIntervention(storageData.sIdLanguage(), function (response) {
    for (var i = 0; i < response.data.length; i++) {
      var newList = "";
      if (response.data[i].ready) {
        if (response.data[i].nr_ddt_gr == null || response.data[i].nr_ddt_gr == "" || (response.data[i].date_ddt_gr == null || response.data[i].date_ddt_gr == "") && (response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable)) {
          newList += "<tr class='color-orange'>";
        } else {
          newList += "<tr class='color-white'>";
        }
      } else {
        if ((response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable) && (response.data[i].nr_ddt_gr == null || response.data[i].nr_ddt_gr == '' || response.data[i].date_ddt_gr == null || response.data[i].date_ddt_gr == '')) {
          newList += "<tr class='color-orange'>";
        } else {
          newList += "<tr class='color-white'>";
        }
      }
      if (response.data[i].ready == true) {
        newList += '<td><input type="checkbox"  id="form-control ready' + i + '" value="' + response.data[i].ready + '" name="ready' + i + '" class="switch-input' + i + '" checked="true' + i + '" onclick="interventionHelpers.updateCheck(' + response.data[i].id + ')"></td>';
      } else {
        newList += '<td><input type="checkbox" id="form-control ready' + i + '" value="' + response.data[i].ready + '" name="ready' + i + '" class="switch-input' + i + '" onclick="interventionHelpers.updateCheck(' + response.data[i].id + ')"></td>';
      }
      newList += '<td><button class="btn btn-info" style="background-color: #AEC4F1;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Compila Fast Urgenza!" onclick="interventionHelpers.getByInterventionFU(' + response.data[i].id + ')"><i class="fa fa-bolt" style="color:#fff;"/></button></td>';

      if (response.data[i].checkProcessingExist == true) {
        newList += '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' + response.data[i].id + ')"><i class="fa fa-circle-o" style="color:#007fff!important"/></button></td>';
      } else {
        newList += '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' + response.data[i].id + ')"><i class="fa fa-pencil-square-o" style="color:#007fff!important"/></button></td>';
      }

      newList += "<td>";
      if (response.data[i].id_states_quote != 'Accettato' && response.data[i].id_states_quote != 'Rifiutato' && response.data[i].id_states_quote != 'In Attesa') {
        newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:navi!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:navi!important;"/></button>';
      }
      if (response.data[i].id_states_quote == 'In Attesa') {
        newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:#f5ea74!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:#f5ea74!important;"/></button>';
      }

      if (response.data[i].id_states_quote == 'Accettato') {
        newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:green!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:green!important;"/></button>';
      }
      if (response.data[i].id_states_quote == 'Rifiutato') {
        newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:red!important;" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-flag" style="color:red!important;"/></button>';
      }
      "</td>";

      /*newList +=
        '<td><button class="btn btn-link" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag"/></button></td>';*/
      newList += "<td>" + response.data[i].id + "</td>";
      newList += "<td>" + response.data[i].code_intervention_gr + "</td>";
      newList += '<td><a  style="cursor:pointer;white-space: nowrap; color:#787878 !important;" data-toggle="tooltip" data-placement="top" title="Visualizza dettaglio Utente!" onclick="interventionHelpers.getByIdUser(' + response.data[i].user_id + ')">' + response.data[i].descriptionCustomer + '</a></td>';
      newList += "<td>" + response.data[i].date_ddt + "</td>";
      newList += "<td>" + response.data[i].nr_ddt + "</td>";

      //newList += "<td><a href='" + response.data[i].imgmodule + "'</a>Visualizza</td>";
      newList += "<td>" + response.data[i].description + response.data[i].rip_association + "</td>";
      //newList += "<td>" + response.data[i].defect + "</td>";
      newList += "<td>";
      for (var j = 0; j < response.data[i].captures.length; j++) {
        newList += '<div class="container"><div id="lightgallery" style="display:flex">';
        newList += "<a href='" + response.data[i].captures[j].img + "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
      }
      newList += "</td>";
      newList += "<td>";
      if (response.data[i].imgmodule != "" && response.data[i].imgmodule != '-') {
        newList += '<div class="container"><div id="lightgallery" style="display:flex">';
        /*  newList +=
              "<a href='" +
              response.data[i].imgmodule +
              "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a> " + response.data[i].defect + "'</div></div>";*/
        newList += "<a href='" + response.data[i].imgmodule + "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a> " + "'</div></div>";
      }
      if (response.data[i].imgmodule == "") {
        newList += "<div>" + response.data[i].defect + "</div>";
      }
      //
      if (response.data[i].checkModuleFault == true) {
        newList += '<button class="btn btn-green" style="color:#007FFF!important;" data-toggle="tooltip" data-placement="top" title="Visualizza Modulo Guasti!"  onclick="interventionHelpers.getByFaultModule(' + response.data[i].id + ')"><i class="fa fa-file-pdf-o" style="color:#007FFF!important;"/></button>';
      }
      if (response.data[i].checkModuleFault == false) {
        newList += "<div>" + "</div>";
      }
      //
      newList += "<td>" + response.data[i].codeGr + "</td>";
      newList += "<td>" + response.data[i].date_ddt_gr + "</td>";
      newList += "<td>" + response.data[i].nr_ddt_gr + "</td>";
      if (response.data[i].idInterventionExternalRepair == null) {
        newList += '<td><button class="btn btn-green" style="color:#d7e7fe !important;font-size:28px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-truck" style="color:#d7e7fe !important"/></button></td>';
      }
      if (response.data[i].idInterventionExternalRepair != null) {
        newList += '<td><button class="btn btn-green" style="color:#02784d !important;font-size:26px;margin-top: 21px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-truck" style="color:#02784d !important;"/></button>';
      }
      if (response.data[i].ddt_supplier != null && response.data[i].ddt_supplier != "") {
        newList += '<button class="btn btn-green" style="color:#02784d !important;font-size:27px;margin-top: -100px!important;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-reply" style="color:#02784d !important;>" aria-hidden="true"></i></button></td>';
      }
      newList += '<td><button class="btn btn-info" style="background-color: #ff337b;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Sezione gestione del tecnico" onclick="interventionHelpers.getBySelectReferent(' + response.data[i].id + ')"><i class="fa fa-circle-o" style="color:#fff;"/></button></td>';
      if (response.data[i].create_quote == "Si") {
        newList += "<td style='color:green;'>" + response.data[i].create_quote + "</td>";
      } else {
        newList += "<td style='color:red;'>" + response.data[i].create_quote + "</td>";
      }
      // newList += "<td>" + response.data[i].descriptionStates + "</td>";
      newList += "<td>" + response.data[i].code_tracking + "</td>";
      newList += "<td>" + response.data[i].note_internal_gr + "</td>";

      newList += "<td>";
      if (response.data[i].fileddt != "" && response.data[i].fileddt != '-') {
        newList += '<div class="container"><div id="lightgallery" style="display:flex">';
        newList += "<a href='" + response.data[i].fileddt + "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:red;float:right;'></i></a></div></div>";
      }
      if (response.data[i].fileddt == "") {
        newList += "<div>" + "</div>";
      }
      newList += "</td>";

      newList += '<td><button class="btn btn-info" onclick="interventionHelpers.getById(' + response.data[i].id + ')"><i class="fa fa-pencil"style="color:#fff"/></button></td>';

      newList += "<td>";
      if (response.data[i].checkModuleValidationGuarantee == true) {
        newList += '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Modulo Garanzie" style="color:#007FFF!important;" onclick="interventionHelpers.getByModuleGuarantee(' + response.data[i].id + ')"><i class="fa fa-external-link" style="color:#007FFF!important;"/></button>';
      }
      if (response.data[i].checkModuleValidationGuarantee == false) {
        newList += "<div>" + "</div>";
      }

      for (var j = 0; j < response.data[i].captures.length; j++) {
        newList += '<div class="container"><div id="lightgallery" style="display:flex">';
        newList += "<a href='" + response.data[i].captures[j].img_rep_test + "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
      }
      "</td>";
      newList += '<td><button class="btn btn-danger" onclick="interventionHelpers.deleteRow(' + response.data[i].id + ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
      newList += "</tr>";
      $("#table_list_intervention tbody").append(newList);

      $("#table_list_intervention").show();
      $("#table_list_searchintervention").hide();
      $("#table_list_repair_arriving").hide();
      $("#frmUpdateIntervention").hide();
      $("#frmViewUser").hide();
      $("#frmViewProcessingSheet").hide();
      $("#frmViewQuotes").hide();
      $("#frmViewExternalRepair").hide();
      $("#frmViewSelectReferent").hide();
      $("#ModuleValidationGuarantee").hide();
      $("#FaultModule").hide();
    }
    lightGallery(document.getElementById("lightgallery"));
  });
  spinnerHelpers.hide();
}
//   ------ region get by -------  //
function getByViewExternalRepair(id) {
  InterventionsServices.getByViewExternalRepair(id, function (response) {
    var searchParam = "";
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmViewExternalRepair #" + i).html(val);
        } else {
          $("#frmViewExternalRepair #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmViewExternalRepair #imgPreview").attr("src", val);
          } else {
            $("#frmViewExternalRepair #imgName").remove();
          }
        } else {
          $("#frmViewExternalRepair #" + i).val(val);
          $("#frmViewExternalRepair #" + i).html(val);
        }
      }
      var selectFaultModule = response.data.selectFaultModule[0];
      $("#div-cont-module").html("");
      var html = "";
      html += '<h3 ' + '">Anteprima Modulo Guasti ' + "</h3>&nbsp;";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-3">';
      html += '<label for="external_referent_id' + '">Referente ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="external_referent_id" id="external_referent_id" placeholder="Referente" readonly value="' + selectFaultModule.external_referent_id + '">';
      html += "</div>";
      html += '<div class="col-3">';
      html += '<label for="nr_ddt' + '">Numero DDT ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="nr_ddt" id="nr_ddt" placeholder="Numero DDT" readonly value="' + selectFaultModule.nr_ddt + '">';
      html += "</div>";
      html += '<div class="col-3">';
      html += '<label for="date_ddt' + '">Data DDT ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="referent" id="date_ddt" placeholder="Data DDT" readonly value="' + selectFaultModule.date_ddt + '">';
      html += "</div>";
      html += "</div>";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-3">';
      html += '<label for="trolley_type' + '">Modello Carrello ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="trolley_type" id="trolley_type" placeholder="Modello Carrello" readonly value="' + selectFaultModule.trolley_type + '">';
      html += "</div>";
      html += '<div class="col-3">';
      html += '<label for="series' + '">Matricola ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="series" id="series" placeholder="Matricola" readonly value="' + selectFaultModule.series + '">';
      html += "</div>";
      html += '<div class="col-3">';
      html += '<label for="voltage' + '">Voltaggio ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="voltage" id="voltage" placeholder="Voltaggio" readonly value="' + selectFaultModule.voltage + '">';
      html += "</div>";
      html += '<div class="col-3">';
      html += '<label for="plant_type' + '">Tipo Impianto ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="plant_type" id="plant_type" placeholder="Tipo Impianto" readonly value="' + selectFaultModule.plant_type + '">';
      html += "</div>";
      html += "</div>";

      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<label for="defect' + '">Difetto ' + "</label>&nbsp;";
      html += '<textarea class="form-control" id="defect" readonly>' + selectFaultModule.defect + '</textarea>';
      html += "</div>";
      html += '<div class="col-6">';
      html += '<label for="exit_notes' + '">Note ' + "</label>&nbsp;";
      html += '<textarea class="form-control" id="exit_notes" readonly>' + selectFaultModule.exit_notes + '</textarea>';
      html += "</div>";
      html += "</div>";
      $("#div-cont-module").append(html);

      var AggExternalRepair = response.data;
      console.log(response.data);
      $("#div-cont-agg-external-repair").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-4">';
      html += '<label for="id_intervention' + '">Numero Rip.Esterna ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="id_librone_rip_esterna" id="id_librone_rip_esterna" placeholder="Numero Riparazione Esterna" value="' + AggExternalRepair.id_librone_rip_esterna + '">';
      html += '<input type="hidden" class="form-control" name="id_intervention" id="id_intervention" placeholder="Numero Riparazione" value="' + AggExternalRepair.id_intervention + '">';
      html += "</div>";
      html += '<div class="col-4">';
      html += '<label for="date' + '">Data ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="date" id="date" placeholder="Data" value="' + AggExternalRepair.date + '">';
      html += "</div>";
      html += '<div class="col-4">';
      html += '<label for="ddt_exit' + '">DDT Uscita ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="ddt_exit" id="ddt_exit" placeholder="DDT Uscita" value="' + AggExternalRepair.ddt_exit + '">';
      html += "</div>";
      html += "</div>";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<label for="id_business_name_supplier' + '">Fornitore ' + "</label>&nbsp;";
      html += '<select class="form-control select-graphic custom-select id_business_name_supplier" id="id_business_name_supplier' + '" value="' + AggExternalRepair.id_business_name_supplier + '" name="id_business_name_supplier' + '"></select>';
      html += "</div>";
      html += "</div>";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-11">';
      html += '<label for="description' + '">Descrizione ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="description" id="description" placeholder="Descrizione" value="' + AggExternalRepair.description + '">';
      html += "</div>";
      html += "</div>";
      html += '<div class="row">';
      html += '<div class="col-6">';
      html += '<label for="code_gr' + '">Codice GR ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="code_gr" id="code_gr" placeholder="Codice GR" value="' + AggExternalRepair.code_gr + '">';
      html += "</div>";
      html += '<div class="col-6">';
      html += '<label for="price' + '">Prezzo ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="price" id="price" placeholder="Prezzo" value="' + AggExternalRepair.price + '">';
      html += '<input type="hidden" class="form-control" name="id" id="id" placeholder="id" value="' + AggExternalRepair.id + '">';
      html += "</div>";
      html += '<input type="hidden" class="form-control" name="codice_intervento" id="codice_intervento" placeholder="codice_intervento" value="' + AggExternalRepair.codice_intervento + '">';
      html += "</div>";
      html += "</div>";
      $("#div-cont-agg-external-repair").append(html);

      $('#div-cont-agg-external-repair #date').datetimepicker({
        format: 'DD/MM/YYYY'
      });

      var AggExternalRepair = response.data;
      $("#div-cont-upd-rientro-external-repair").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-4">';
      html += '<label for="date_return_product_from_the_supplier' + '">Data Entrata ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="date_return_product_from_the_supplier" id="date_return_product_from_the_supplier" placeholder="Data Entrata" value="' + AggExternalRepair.date_return_product_from_the_supplier + '">';
      html += "</div>";
      html += '<div class="col-4">';
      html += '<label for="ddt_supplier' + '">DDT Fornitore ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="ddt_supplier" id="ddt_supplier" placeholder="DDT Fornitore" value="' + AggExternalRepair.ddt_supplier + '">';
      html += "</div>";
      html += '<div class="col-4">';
      html += '<label for="reference_supplier' + '">Rif Fornitore ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="reference_supplier" id="reference_supplier" placeholder="Rif Fornitore" value="' + AggExternalRepair.reference_supplier + '">';
      html += "</div>";
      html += "</div>";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-12">';
      html += '<label for="notes_from_repairman' + '">Note Fornitore ' + "</label>&nbsp;";
      html += '<textarea class="form-control" id="notes_from_repairman" placeholder="Note Fornitore">' + AggExternalRepair.notes_from_repairman + '</textarea>';

      html += "</div>";
      html += "</div>";
      $("#div-cont-upd-rientro-external-repair").append(html);

      $('#div-cont-upd-rientro-external-repair #date_return_product_from_the_supplier').datetimepicker({
        format: 'DD/MM/YYYY'
      });

      var searchParam = "";
      var initials = [];
      initials.push({
        id: AggExternalRepair.id_business_name_supplier,
        text: AggExternalRepair.Description_business_name_supplier
      });
      $("#id_business_name_supplier").select2({
        data: initials,
        ajax: {
          url: configData.wsRootServicesUrl + "/api/v1/businessnamesupplier/select",
          data: function data(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function processResults(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var searchParam = "";
      $("#id_business_name_supplier").select2({
        ajax: {
          url: configData.wsRootServicesUrl + "/api/v1/businessnamesupplier/select",
          data: function data(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function processResults(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });
    });
    $("#frmViewExternalRepair").modal("show");
    $("#table_list_searchintervention").hide();
    $("#table_list_intervention").show();
    $("#frmSearchIntervention").hide();
    $("#frmViewProcessingSheet").hide();
    $("#frmViewQuotes").hide();
    $("#frmViewSelectReferent").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
  });
}

function getByInterventionFU(id) {
  InterventionsServices.getByInterventionFU(id, function (response) {
    var searchParam = "";
    if (!$.isEmptyObject(response.data)) {
      jQuery.each(response.data, function (i, val) {
        if (val == true) {
          if (i == "id") {
            $("#frmViewInterventionFU #" + i).html(val);
          } else {
            $("#frmViewInterventionFU #" + i).attr("checked", "checked");
          }
        } else {
          if (i == "imgName") {
            if (val != "") {
              $("#frmViewInterventionFU #imgPreview").attr("src", val);
            } else {
              $("#frmViewInterventionFU #imgName").remove();
            }
          } else {
            $("#frmViewInterventionFU #" + i).val(val);
            $("#frmViewInterventionFU #" + i).html(val);
          }
        }

        var data = new Date();
        var gg, mm, aaaa;
        gg = data.getDate() + "/";
        mm = data.getMonth() + 1 + "/";
        aaaa = data.getFullYear();

        var HH = data.getHours();
        var MM = data.getMinutes();
        var SS = data.getSeconds();
        parseInt(MM) < 10 ? MM = "0" + MM : null;
        parseInt(SS) < 10 ? SS = "0" + SS : null;
        var AggInterventionFU = response.data;

        $("#div-cont-agg-intervention-fu").html("");
        var html = "";
        html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
        html += '<div class="col-6">';
        html += '<label for="description_user_id' + '">Cliente ' + "</label>&nbsp;";
        html += '<input type="text" style="background-color:fff!important;" class="form-control" name="description_user_id" id="description_user_id" placeholder=" Cliente" value="' + AggInterventionFU.description_user_id + '" readonly>';
        html += '<input type="hidden" class="form-control" name="user_id" id="user_id" value="' + AggInterventionFU.user_id + '">';
        html += "</div>";

        html += '<div class="col-3">';
        html += '<label for="referent' + '">Tecnico ' + "</label>&nbsp;";
        html += '<input type="text" style="background-color:fff!important;" class="form-control" name="referent" id="referent" value="' + AggInterventionFU.referent + '" readonly>';
        html += "</div>";
        html += '<div class="col-3">';
        html += '<label for="id' + '">Numero Riparazione ' + "</label>&nbsp;";
        html += '<input type="text" style="background-color:fff!important;" class="form-control" name="id" id="id" value="' + AggInterventionFU.id + '" readonly>';
        html += "</div>";
        html += "</div>";

        html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
        html += '<div class="col-6">';
        html += '<label for="date' + '">Data ' + "</label>&nbsp;";

        html += '<input type="text" class="form-control" name="date" id="date" value="' + AggInterventionFU.date + '">';
        html += "</div>";
        html += '<div class="col-6">';
        html += '<label for="descriptionfast' + '">Descrizione ' + "</label>&nbsp;";
        html += '<input type="text" class="form-control" name="descriptionfast" id="descriptionfast" placeholder=" Descrizione" value="' + AggInterventionFU.descriptionfast + '">';
        html += "</div>";
        html += "</div>";
        $("#div-cont-agg-intervention-fu").append(html);

        $('#div-cont-agg-intervention-fu #date').datetimepicker({
          format: 'DD/MM/YYYY'
        });
      });
      $("#frmViewInterventionFU").modal("show");
    } else {
      var html = "";
      $("#div-cont-agg-intervention-fu").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<label for="description_user_id' + '">Cliente ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="description_user_id" id="description_user_id" placeholder="Cliente" value="' + '">';
      html += '<input type="hidden" class="form-control" name="user_id" id="user_id" value="' + '">';
      html += "</div>";
      html += '<div class="col-3">';
      html += '<label for="referent' + '">Tecnico ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="referent" id="referent" value="' + '">';
      html += "</div>";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-3">';
      html += '<label for="id' + '">Numero Riparazione ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="id" id="id" placeholder="Numero Riparazione" value="' + '">';
      html += "</div>";
      html += "</div>";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<label for="date' + '">Data ' + "</label>&nbsp;";
      '<input type="text" class="form-control" name="date" id="date" placeholder="Data" value="';
      //html += '<input type="text" class="form-control" name="date" id="date" value="'+ gg + mm + aaaa + ' ' + HH + ":" + MM + ":" + SS + '">';
      html += "</div>";
      html += '<div class="col-6">';
      html += '<label for="descriptionfast' + '">Descrizione ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="descriptionfast" id="descriptionfast" placeholder="Descrizione" value="' + '">';
      html += "</div>";
      html += "</div>";
      $("#div-cont-agg-intervention-fu").append(html);

      $('#div-cont-agg-intervention-fu #date').datetimepicker({
        format: 'DD/MM/YYYY'
      });

      $("#frmViewInterventionFU").modal("show");
    }
  });
}

function updateInterventionFU() {
  spinnerHelpers.show();
  var data = {
    object: $("#frmViewInterventionFU #id").val(),
    date: $("#frmViewInterventionFU #date").val(),
    description: $("#frmViewInterventionFU #descriptionfast").val(),
    user_id: $("#frmViewInterventionFU #user_id").val()
  };

  InterventionsServices.updateInterventionFU(data, function () {
    $("#frmViewInterventionFU #description").val("");
    notificationHelpers.success("Modifica avvenuta con successo");
    spinnerHelpers.hide();
    window.location.reload();
    $("#table_list_intervention").show();
    $("#frmViewQuotes").hide();
    $("#frmViewUser").hide();
    $("#frmUpdateIntervention").hide();
    $("#frmSearchIntervention").hide();
    $("#table_list_searchintervention").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#frmViewInterventionFU").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
  });
}

function getBySelectReferent(id) {
  InterventionsServices.getBySelectReferent(id, function (response) {

    var searchParam = "";
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmViewSelectReferent #" + i).html(val);
        } else {
          $("#frmViewSelectReferent #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmViewSelectReferent #imgPreview").attr("src", val);
          } else {
            $("#frmViewSelectReferent #imgName").remove();
          }
        } else {
          $("#frmViewSelectReferent #" + i).val(val);
          $("#frmViewSelectReferent #" + i).html(val);
        }
      }

      var AggReferent = response.data;
      $("#div-cont-agg-referent").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<label for="id' + '">Numero Riparazione ' + "</label>&nbsp;";
      html += '<input type="text" class="form-control" name="id" id="id" disabled="true" placeholder="Numero Riparazione" value="' + AggReferent.id + '">';
      html += "</div>";
      html += "</div>";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<label for="id_people' + '">Tecnico ' + "</label>&nbsp;";
      html += '<select class="form-control select-graphic custom-select id_people" id="id_people' + '" value="' + AggReferent.id_people + '" name="id_people' + '"></select>';
      html += "</div>";
      html += "</div>";
      $("#div-cont-agg-referent").append(html);

      $("#frmViewSelectReferenta #id_people").on("change", function () {
        if (AggReferent.rip_association != null) {
          alert("Attenzione, questa riparazione è associata alla riparazione numero " + AggReferent.rip_association);
        }
      });

      var searchParam = "";
      var initials = [];
      initials.push({
        id: AggReferent.id_people,
        text: AggReferent.Description_People
      });
      $("#id_people").select2({
        data: initials,
        ajax: {
          url: configData.wsRootServicesUrl + "/api/v1/people/select",
          data: function data(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function processResults(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var searchParam = "";
      $("#id_people").select2({
        ajax: {
          url: configData.wsRootServicesUrl + "/api/v1/people/select",
          data: function data(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function processResults(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });
    });
    $("#frmViewSelectReferent").modal("show");
    $("#frmViewExternalRepair").modal("hide");
    $("#table_list_searchintervention").hide();
    $("#table_list_intervention").show();
    $("#frmSearchIntervention").hide();
    $("#frmViewProcessingSheet").hide();
    $("#frmViewQuotes").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
  });
}

function getByViewQuotes(id) {
  InterventionsServices.getByViewQuotes(id, storageData.sUserId(), function (response) {
    var searchParam = "";
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmViewQuotes #" + i).html(val);
        } else {
          $("#frmViewQuotes #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmViewQuotes #imgPreview").attr("src", val);
          } else {
            $("#frmViewQuotes #imgName").remove();
          }
        } else {
          $("#frmViewQuotes #" + i).val(val);
          $("#frmViewQuotes #" + i).html(val);
        }
      }

      var AggOptions = response.data.ReferentEmailSendPreventivi;
      if (AggOptions.length > 0) {
        $("#emailReferent").html("");
        for (var j = 0; j < AggOptions.length; j++) {
          var html = "";
          if (j < 10) {
            html += '<div class="row" style="margin-bottom: 10px;margin-top: 7px;" data-id-row="' + '" id="divcont-' + ' " >';
            html += '<div class="col-10 text-left">';
            html += '<label class="labelclass">Email</label>';
            html += '<input type="hidden" class="form-control cost" id="user_id' + j + '" value="' + AggOptions[j].user_id + '" name="user_id' + j + '">';
            html += '<input type="hidden" class="form-control cost" id="people_id' + j + '" value="' + AggOptions[j].people_id + '" name="people_id' + j + '">';
            html += '<input type="text" class="form-control cost" id="emailreferente' + j + '" value="' + AggOptions[j].EmailReferente + '" name="emailreferente' + j + '">';
            html += "</div>";
            html += '<div class="col-2">';
            html += '<button type="button" style="margin-top: 30px;border-color: #fff;" class="btn btn-outline-danger del-img-agg" data-id-cont="' + AggOptions[j].people_id + '" id="btndelete' + AggOptions[j].people_id + '"><i class="fa fa-times"></i></button>';
            html += "</div>";
            html += "</div>";
            $("#emailReferent").append(html);
          }

          $("#frmViewQuotes .del-img-agg").on("click", function (e) {
            var idDel = $(this).attr('data-id-cont');
            var el = $(this).parent('div').parent('div').attr('data-id-row', idDel);

            AggOptions = AggOptions - 1;
            if (AggOptions < 0) {
              AggOptions = 0;
            }
            el.remove();
          });
        }
      }

      $('#div-cont-updated_id').html('');
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-5" style="text-align: left;">';
      html += '<span for="created_id' + '"><strong>Preventivo Creato Da: ' + '</strong></span>';
      html += '<input type="text" class="form-control" style="border: #fff;" name="created_id" id="created_id" value="' + response.data.created_id + '" readonly>';
      html += "</div>";
      // html += '<div class="col-4">';
      // html += '<span for="kind_attention' + '"><strong> Alla c.a: ' + '</strong></span>';
      //html += '<input type="text" class="form-control" style="border: #fff;" name="kind_attention" id="kind_attention" value="' + response.data.kind_attention + '">';
      //html += "</div>";
      html += "</div>";
      $('#div-cont-updated_id').append(html);

      /*  $('#div-cont-updated_id').html('');
        var html = "";
        html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
        html += '<div>';
        html += '<span for="created_id' + '"><strong>Preventivo Creato Da: ' + '</strong></span>';
        html += '<input type="text" class="form-control" style="border: #fff;" name="created_id" id="created_id" value="' + response.data.created_id + '" readonly>';
        html += "</div>";
       // html += '<div class="col-4">';
       // html += '<span for="kind_attention' + '"><strong> Alla c.a: ' + '</strong></span>';
        //html += '<input type="text" class="form-control" style="border: #fff;" name="kind_attention" id="kind_attention" value="' + response.data.kind_attention + '">';
        //html += "</div>";
        html += "</div>";
        $('#div-cont-updated_id').append(html);*/

      var AggNoteBeforeTheQuotes = response.data;
      $("#div-cont-note_before_the_quote").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-12">';
      html += '<textarea class="form-control" id="note_before_the_quote">' + AggNoteBeforeTheQuotes.note_before_the_quote + '</textarea>';
      /* html +=
         '<textarea class="form-control" id="note_before_the_quote">' +
         "In riferimento al Vs DDT n. " +
         AggNoteBeforeTheQuotes.nr_ddt +
         "  del " +
         AggNoteBeforeTheQuotes.date_ddt +
         " con la presente siamo ad inviarVi il seguente preventivo di riparazione relativo: " +
         AggNoteBeforeTheQuotes.description +
         "</textarea>";*/
      html += "</div>";
      html += "</div>";
      $("#div-cont-note_before_the_quote").append(html);

      var AggTotale = response.data;
      $("#div-cont-totale").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<label for="tot_quote' + '">Totale: ' + '</label>&nbsp;';
      //if(AggTotale.tot_quote==''){
      html += '<input type="text" class="form-control tot_quote" name="tot_quote" id="tot_quote" value="' + '€ ' + AggTotale.tot_quote + ' Netto + Iva" readonly>';
      // }else{
      // html += '<input type="text" class="form-control" name="tot_quote" id="tot_quote" value="'+ AggTotale.tot_quote + '">';
      //}
      html += "</div>";
      html += '<div class="col-6">';
      html += '<label for="discount' + '">Sconto €: ' + '</label>&nbsp;';
      html += '<input type="text" class="form-control discount" name="discount" id="discount" value="' + AggTotale.discount + '">';
      html += "</div>";
      html += '<div class="col-6">';
      html += '<label for="total_amount_with_discount' + '">Totale Scontato: ' + '</label>&nbsp;';
      html += '<input type="text" class="form-control total_amount_with_discount" name="total_amount_with_discount" id="total_amount_with_discount" value="' + '€ ' + AggTotale.total_amount_with_discount + ' Netto + Iva">';
      html += "</div>";
      html += "</div>";
      $("#div-cont-totale").append(html);

      var data = new Date();
      var gg, mm, aaaa;
      gg = data.getDate() + "/";
      mm = data.getMonth() + 1 + "/";
      aaaa = data.getFullYear();

      $('#div-cont-date_agg').html('');
      var html = "";
      html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<span for="date_agg' + '"><strong>Data: ' + '</strong></span><br>';
      html += '<input type="text" class="form-control" name="date_agg" id="date_agg" value="' + response.data.date_agg + '">';
      html += "</div>";
      html += "</div>";
      $('#div-cont-date_agg').append(html);

      var AggPaymentQuotes = response.data;
      $("#div-cont-payment_quote").html("");
      var html = "";
      html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-8">';
      html += '<label for="description_payment' + '">Descrizione Pagamento ' + "</label>&nbsp;";
      html += '<select class="form-control select-graphic custom-select description_payment" id="description_payment' + '" value="' + AggPaymentQuotes.description_payment + '" name="description_payment' + '"></select>';
      html += "</div>";
      html += "</div>";
      $("#div-cont-payment_quote").append(html);

      var searchParam = "";
      var initials = [];
      initials.push({
        id: AggPaymentQuotes.description_payment,
        text: AggPaymentQuotes.Description_payment
      });
      $("#description_payment").select2({
        data: initials,
        ajax: {
          url: configData.wsRootServicesUrl + "/api/v1/paymentGr/selectpaymentGr",
          data: function data(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function processResults(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var searchParam = "";
      $("#description_payment").select2({
        ajax: {
          url: configData.wsRootServicesUrl + "/api/v1/paymentGr/selectpaymentGr",
          data: function data(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function processResults(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var searchParam = "";
      var initials = [];
      initials.push({
        id: AggPaymentQuotes.id_states_quote,
        text: AggPaymentQuotes.States_Description
      });
      $("#id_states_quote").select2({
        data: initials,
        ajax: {
          url: configData.wsRootServicesUrl + "/api/v1/StatesQuotesGr/selectStatesQuotesGr",
          data: function data(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function processResults(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var AggArticlesQuotes = response.data.Articles;
      $("#div-cont-codegr_description-agg").html("");
      if (AggArticlesQuotes.length > 0) {
        for (var j = 0; j < AggArticlesQuotes.length; j++) {
          var html = "";
          html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
          html += '<div class="col-4">';
          //html += '<label for="description' + '">Descrizione: ' + '<span class="description' + '" id="description' + '" value="' + AggArticlesQuotes[j].description + '"</span></label>&nbsp;';
          //html += '<input type="text"  id="description' + '" value="' + AggArticlesQuotes[j].description + '" name="description' + '">';
          html += '<label for="description">Descrizione:<br><span class="description">' + AggArticlesQuotes[j].description + "</span></label>";
          html += "</div>";
          html += '<div class="col-4">';
          html += '<label for="code_gr">Codice Articolo:<br><span class="code_gr">' + AggArticlesQuotes[j].code_gr + "</span></label>";
          //html += '<label for="code_gr' + '">Codice Articolo ' + '</label>&nbsp;';
          //html += '<input type="text"  id="code_gr' + '" value="' + AggArticlesQuotes[j].code_gr + '" name="code_gr' + '">';
          html += "</div>";
          html += '<div class="col-4">';
          html += '<label for="qta">Quantità:<br><span class="qta">' + AggArticlesQuotes[j].qta + "</span></label>";
          //html += '<label for="code_gr' + '">Codice Articolo ' + '</label>&nbsp;';
          //html += '<input type="text"  id="code_gr' + '" value="' + AggArticlesQuotes[j].code_gr + '" name="code_gr' + '">';
          html += "</div>";
          html += "</div>";
          $("#div-cont-codegr_description-agg").append(html);
        }
      }

      var MessagesQuotes = response.data.Messages;
      $("#div-cont-messages").html("");
      if (MessagesQuotes.length > 0) {
        for (var j = 0; j < MessagesQuotes.length; j++) {
          if (storageData.sUserId() == MessagesQuotes[j].id_sender_id) {
            var html = "";
            html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
            html += '<div class="col-12">';
            html += '<div class="chat-history">';
            html += '<ul class="m-b-0">';
            html += '<li class="clearfix"style="    list-style: none;margin-bottom: 30px;">';
            html += '<div class="message-data text-right" style= "margin-bottom: 15px">';
            html += '<span class="message-data-time" style=" color: #434651;padding-left: 6px">' + MessagesQuotes[j].created_at + '</span>';
            html += '<img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="avatar" style="border-radius: 40px;width: 40px;">';
            html += '</div>';
            html += '<div class="message other-message float-right" style="background: #e8f1f3; color: #444;padding: 18px 20px;text-align: right;line-height: 26px;font-size: 16px;border-radius: 7px;display: inline-block; position: relative;"> ' + MessagesQuotes[j].messages + '</div>';
            html += '</li>';
            html += '</ul>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            $("#div-cont-messages").append(html);
          } else {
            if (MessagesQuotes[j].id_sender_id == response.data.id_user_quotes) {
              var html = "";
              html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
              html += '<div class="col-12">';
              html += '<div class="chat-history">';
              html += '<ul class="m-b-0">';
              html += '<li class="clearfix"style="list-style: none;margin-bottom: 30px;">';
              html += '<div class="message-data text-left" style= "margin-bottom: 15px">';
              html += '<span class="message-data-time">' + MessagesQuotes[j].created_at + '</span>';
              html += '</div>';
              html += '<div class="message my-message" style="background: #efefef;color: #444;padding: 18px 20px;line-height: 26px;font-size: 16px;border-radius: 7px;display: inline-block;position: relative;">' + MessagesQuotes[j].messages + '</div>';
              html += '</li>';
              html += '</ul>';
              html += '</div>';
              html += '</div>';
              html += '</div>';
              $("#div-cont-messages").append(html);
            } else {
              var html = "";
              html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
              html += '<div class="col-12">';
              html += '<div class="chat-history">';
              html += '<ul class="m-b-0">';
              html += '<li class="clearfix"style="    list-style: none;margin-bottom: 30px;">';
              html += '<div class="message-data text-right" style= "margin-bottom: 15px">';
              html += '<span class="message-data-time" style=" color: #434651;padding-left: 6px">' + MessagesQuotes[j].created_at + '</span>';
              html += '<img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="avatar" style="border-radius: 40px;width: 40px;">';
              html += '</div>';
              html += '<div class="message other-message float-right" style="background: #e8f1f3; color: #444;padding: 18px 20px;text-align: right;line-height: 26px;font-size: 16px;border-radius: 7px;display: inline-block; position: relative;"> ' + MessagesQuotes[j].messages + '</div>';
              html += '</li>';
              html += '</ul>';
              html += '</div>';
              html += '</div>';
              html += '</div>';
              $("#div-cont-messages").append(html);
            }
          }
        }
        /*var MessagesQuotes = response.data.Messages;
        $("#div-cont-messages").html("");
        if (MessagesQuotes.length > 0) {
          for (var j = 0; j < MessagesQuotes.length; j++) {
            var html = "";
            html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
            html += '<div class="col-3">';
            html += '<label for="id_receiver">Ricevente:<br><span class="id_receiver">' + MessagesQuotes[j].id_receiver + "</span></label>";
            html += "</div>";
            html += '<div class="col-3">';
            html += '<label for="id_sender">Io:<br><span class="id_sender">' + MessagesQuotes[j].id_sender + "</span></label>";
            html += "</div>";
            html += '<div class="col-3">';
            html += '<label for="created_at">Data:<br><span class="created_at">' + MessagesQuotes[j].created_at + "</span></label>";
            html += "</div>";
            html += '<div class="col-3">';
            html +=
              '<label for="messages">Messaggio:<br><span class="messages">' + MessagesQuotes[j].messages + "</span></label>";
            html += "</div>";
            html += "</>";
            $("#div-cont-messages").append(html);
           }
        }*/
      }
    });

    $("#frmViewQuotes .discount").on("keyup", function () {
      var discount = $(this).val();
      var str = $("#frmViewQuotes #tot_quote").val();
      var res = str.replace("€ ", "");
      var res1 = res.replace(" Netto + Iva", "");
      CalcolaTot(discount, res1);
    });
    function CalcolaTot(discount, tot_quote) {
      var totale = tot_quote - discount;
      $('#frmViewQuotes #total_amount_with_discount').val('€ ' + totale + ' Netto + Iva');
    }
    $("#frmViewQuotes").show();
    $("#table_list_searchintervention").hide();
    $("#table_list_intervention").hide();
    $("#frmSearchIntervention").hide();
    $("#frmViewProcessingSheet").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
    $("#row-table-fld-pre").hide();
    $("#button-searchintervention").hide();
  });
}

function getByViewValue(id) {
  //$('#frmViewProcessingSheet #id').val(id);
  InterventionsServices.getByViewValue(id, function (response) {
    var searchParam = "";
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmViewProcessingSheet #" + i).html(val);
        } else {
          $("#frmViewProcessingSheet #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmViewProcessingSheet #imgPreview").attr("src", val);
            $("#frmViewProcessingSheet #imgName").attr("src", val);
            $("#frmViewProcessingSheet #imgNameLink").attr("href", val);
          } else {
            $("#frmViewProcessingSheet #imgName").remove();
          }
        } else {
          $("#frmViewProcessingSheet #" + i).val(val);
          $("#frmViewProcessingSheet #" + i).html(val);
          $("#frmViewProcessingSheet #defect").prop("disabled", true);
          //$("#frmViewProcessingSheet #imgName" + i).val(val);
        }
      }

      var data = new Date();
      var gg, mm, aaaa;
      gg = data.getDate() + "/";
      mm = data.getMonth() + 1 + "/";
      aaaa = data.getFullYear();

      var HH = data.getHours();
      var MM = data.getMinutes();
      var SS = data.getSeconds();
      parseInt(MM) < 10 ? MM = "0" + MM : null;
      parseInt(SS) < 10 ? SS = "0" + SS : null;

      $("#div-cont-date_aggs").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-12">';
      html += '<label id="date_aggs" for="date_aggs' + '">Data: ' + response.data.created_at + "</label>&nbsp;";
      // html += '<label id="date_aggs" for="date_aggs' + '">Data: ' + gg + mm + aaaa + " " + HH + ":" + MM + ":" + SS + "</label>&nbsp;";
      //html += '<label for="date_aggs' + '">Data: ' + '</label>&nbsp;';
      //html += '<input type="text" class="form-control" name="date_aggs" id="date_aggs" value="' + gg + mm + aaaa + ' ' + HH + ":" + MM + ":" + SS + '">';
      html += "</div>";
      html += "</div>";
      $("#div-cont-date_aggs").html(html);

      var AggArticles = response.data.Articles;
      if (AggArticles.length > 0) {
        var cont = AggArticles.length;
        $("#div-cont-img-agg").html("");
        for (var j = 0; j < AggArticles.length; j++) {
          var html = "";
          if (j < 15) {
            //cont = cont + 1;
            html += '<div class="row" data-id-row="' + j + '" id="divcont-' + j + '" >';
            html += '<div class="col-2">';
            if (j == 0) {
              html += '<label for="code_gr' + j + '">Articolo</label>&nbsp;';
            }
            html += '<select class="custom-select code_gr_select" id="code_gr' + j + '" value="' + AggArticles[j].code_gr + '" name="code_gr' + j + '"></select>';
            html += "</div>";
            html += '<div class="col-4">';
            if (j == 0) {
              html += '<label for="description' + j + '">Descrizione</label>&nbsp;';
            }
            html += '<input type="text"  id="description' + j + '" value="' + AggArticles[j].description + '" name="description' + j + '" class="form-control description' + j + '">';
            html += "</div>";
            html += '<div class="col-2">';
            if (j == 0) {
              html += '<label class="col col-form-label nopadding" for="n_u' + j + '">N/U</label>';
            }
            // html += '<label class="switch switch-pill switch-primary" style="margin: 2px; vertical-align:bottom;"></label>&nbsp;';
            html += '<select class="form-select form-control" id="n_u' + j + '" value="' + AggArticles[j].n_u + '" name="n_u' + j + '"><option value="Nuovo">Nuovo</option><option value="Usato">Usato</option></select>';
            html += "</div>";
            html += '<div class="col-1">';
            if (j == 0) {
              html += '<label for="qta' + j + '">Quantita</label>&nbsp;';
            }
            html += '<input type="text" class="form-control qta" id="qta' + j + '" value="' + AggArticles[j].qta + '" name="qta' + j + '">';
            html += "</div>";
            html += '<div class="col-1">';
            if (j == 0) {
              html += '<label for="price_list' + j + '">Pr.Unitario</label>';
            }
            html += '<input type="text" class="form-control price_list" id="price_list' + j + '" value="' + AggArticles[j].price_list + '" name="price_list' + j + '">';
            html += "</div>";
            html += '<div class="col-1">';
            if (j == 0) {
              html += '<label for="tot' + j + '">Totale</label>&nbsp;';
            }
            html += '<input type="text" class="form-control tot" id="tot' + j + '" value="' + AggArticles[j].tot + '" name="tot' + j + '">';
            html += "</div>";
            html += '<div class="col-1 text-right">';
            html += '<i class="fa fa-times del-img-agg" data-id-cont="' + j + '"></i>';
            html += "</div>";
            html += "</div>";
            $("#div-cont-img-agg").append(html);
          }

          $("#frmViewProcessingSheet .tot").on("keyup", function () {
            var contRow = j;
            var tot = 0;

            $("#frmViewProcessingSheet .tot").each(function (index) {
              tot = parseFloat(tot) + parseFloat($(this).val());
            });
            var consumables = $("#frmViewProcessingSheet #consumables").val();
            var tot_value_config = $("#frmViewProcessingSheet #tot_value_config").val();
            //CalcolaTotale(tot, tot_value_config, consumables, contRow);

            var totaleFinale = parseFloat(tot_value_config) + parseFloat(consumables) + parseFloat(tot);
            $("#frmViewProcessingSheet .tot_final").val(totaleFinale);
          });

          // $.getScript("https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js").done(function(){
          var searchParam = "";
          var initials = [];
          if (AggArticles[j].code_gr != 'null' && AggArticles[j].code_gr != null && AggArticles[j].code_gr != '') {
            initials.push({
              id: AggArticles[j].code_gr,
              text: AggArticles[j].description_codeGr
            });
          }
          $("#code_gr" + j).select2({
            data: initials,
            ajax: {
              url: configData.wsRootServicesUrl + "/api/v1/itemGr/selectitemGr",
              data: function data(params) {
                if (params.term == "") {
                  searchParam = "*";
                } else {
                  searchParam = params.term;
                }
                var query = {
                  search: searchParam
                };
                return query;
              },
              processResults: function processResults(data) {
                var dataParse = JSON.parse(data);
                return {
                  results: dataParse.data
                };
              }
            }
          });
          //  });
        }
      }

      $("#frmViewProcessingSheet .qta").on("keyup", function () {
        var contRow = $(this).parent("div").parent("div").attr("data-id-row");
        var qta = $(this).val();
        var price_list = $("#frmViewProcessingSheet #price_list" + contRow).val();
        CalcolaTotaleRiga(qta, price_list, contRow);
      });
      $("#frmViewProcessingSheet .price_list").on("keyup", function () {
        var contRow = $(this).parent("div").parent("div").attr("data-id-row");
        var price_list = $(this).val();
        var qta = $("#frmViewProcessingSheet #qta" + contRow).val();

        CalcolaTotaleRiga(qta, price_list, contRow);
      });

      $("#frmViewProcessingSheet .code_gr_select").on("change", function () {
        var id = $(this).val();
        var contRow = $(this).parent("div").parent("div").attr("data-id-row");
        interventionHelpers.getDescriptionAndPriceProcessingSheet(id, contRow);
      });

      function CalcolaTotaleRiga(qta, price_list, contRow) {
        var tot = qta * price_list;
        $("#frmViewProcessingSheet #tot" + contRow).val(tot);
      }
    });

    $("#frmViewProcessingSheet").show();
    $("#table_list_searchintervention").hide();
    $("#table_list_intervention").hide();
    $("#frmSearchIntervention").hide();
    $("#frmViewQuotes").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#row-table-fld-pre").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
    $("#button-searchintervention").hide();
  });
}

function getById(id) {
  // spinnerHelpers.show();
  InterventionsServices.getById(id, function (response) {
    $("#frmUpdateIntervention #date_ddt_gr").on("change", function () {
      if (response.data.rip_association != null) {
        alert("Attenzione, questa riparazione è associata alla riparazione numero " + response.data.rip_association);
      }
    });
    $("#frmUpdateIntervention #nr_ddt_gr").on("change", function () {
      if (response.data.rip_association != null) {
        alert("Attenzione, questa riparazione è associata alla riparazione numero " + response.data.rip_association);
      }
    });

    var searchParam = "";
    /* SELECT states */
    var initials = [];
    if (response.data.states_id != 'null' && response.data.states_id != null && response.data.states_id != '') {
      initials.push({
        id: response.data.states_id,
        text: response.data.descriptionStates
      });
    }
    $("#states_id").select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + "/api/v1/States/select",
        data: function data(params) {
          if (params.term == "") {
            searchParam = "*";
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });

    /* SELECT ITEMS_ID */
    var initials = [];
    initials.push({
      id: response.data.items_id,
      text: response.data.descriptionItem
    });
    $("#items_id").select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + "/api/v1/itemGr/select",
        data: function data(params) {
          if (params.term == "") {
            searchParam = "*";
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });
    /* END SELECT ITEMS_ID */

    /* SELECT user_id */
    var initials = [];
    initials.push({
      id: response.data.user_id,
      text: response.data.descriptionUser
    });
    $("#user_id_intervention").select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + "/api/v1/user/select",
        data: function data(params) {
          if (params.term == "") {
            searchParam = "*";
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });
    /* END SELECT user_id */

    /* SELECT referent */
    var initials = [];
    initials.push({
      id: response.data.referent,
      text: response.data.descriptionPeople
    });
    $("#referent").select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + "/api/v1/people/select",
        data: function data(params) {
          if (params.term == "") {
            searchParam = "*";
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });
    /* END SELECT referent */

    /* SELECT external_referent_id_intervention */
    var initials = [];
    initials.push({
      id: response.data.external_referent_id_intervention,
      text: response.data.descriptionPeopleReferentExternal
    });
    $("#external_referent_id_intervention").select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + "/api/v1/people/getSelectExternalReferent",
        data: function data(params) {
          if (params.term == "") {
            searchParam = "*";
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });
    /* END SELECT external_referent_id_intervention */

    var searchParam = "";
    $('#user_id_intervention').select2({
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/user/select',
        data: function data(params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);
          return {
            results: dataParse.data
          };
        }
      }
    });

    /*SELECT INTERVENTION */
    jQuery.each(response.data, function (i, val) {

      if (val == true) {
        $("#frmUpdateIntervention #" + i).attr("checked", "checked");
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmUpdateIntervention #imgPreview").attr("src", val);
          } else {
            $("#frmUpdateIntervention #imgPreview").remove();
          }
        } else {
          $("#frmUpdateIntervention #" + i).val(val);
        }
      }
    });

    var cont = 0;
    var imgAggCaptures = response.data.captures;
    $("#snapShot").html("");
    if (imgAggCaptures.length > 0) {
      for (var j = 0; j < imgAggCaptures.length; j++) {
        var html = "";
        if (cont < 4) {
          cont = cont + 1;
          html += '<input type="hidden" id="imgBase64' + cont + '" value="' + imgAggCaptures[j].img + '" name="imgBase64' + cont + '">';
          html += '<a href="' + imgAggCaptures[j].img + '"><img src="' + imgAggCaptures[j].img + '" style="max-width:50px;"></a>';
          $("#snapShot").append(html);
        }
      }
    }

    lightGallery(document.getElementById("snapShot"));

    $.getScript("https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.js").done(function () {
      Webcam.set({
        width: 525,
        height: 410,
        image_format: "jpeg",
        jpeg_quality: 100
      });
      Webcam.attach("#camera");
    });

    $("#frmUpdateIntervention #btPic").on("click", function () {
      if (cont < 4) {
        cont = cont + 1;
        Webcam.snap(function (data_uri) {
          $("#snapShot").append('<img id="imgDel' + cont + '" src="' + data_uri + '" width="250px" height="200px" /><i class="fa fa-times del-img-agg" data-id-cont="' + cont + '"></i>');
          $("#snapShot").append('<input type="hidden" value="' + data_uri + '" id="imgBase64' + cont + '" name="imgBase64' + cont + '"/>');
        });
      }
      $("#frmUpdateIntervention .del-img-agg").on("click", function (e) {
        var idDel = $(this).attr('data-id-cont');

        var el = $('#imgDel' + idDel);
        var b64 = $('#imgBase64' + idDel);

        cont = cont - 1;
        if (cont < 0) {
          cont = 0;
        }
        el.remove();
        b64.remove();
        $(this).remove();
      });
    });

    $("#frmUpdateIntervention").show();
    $("#frmViewUser").hide();
    $("#table_list_intervention").hide();
    $("#frmSearchIntervention").hide();
    $("#table_list_searchintervention").hide();
    $("#frmViewQuotes").hide();
    $("#frmSearchIntervention").hide();
    $(".div-ricerca-intervention").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#row-table-fld-pre").hide();
    $("#table_list_repair_arriving").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
    $("#riparrivo").hide();
  });
}

function getByFaultModule(id) {
  InterventionsServices.getByFaultModule(id, function (response) {

    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        if (i == "id") {
          $("#FaultModule #" + i).html(val);
        } else {
          $("#FaultModule #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#FaultModule #imgPreview").attr("src", val);
          } else {
            $("#FaultModule #imgName").remove();
          }
        } else {
          $("#FaultModule #" + i).val(val);
          $("#FaultModule #" + i).html(val);
        }
      }
    });
    $("#FaultModule").show();
    $("#ModuleValidationGuarantee").hide();
    $("#frmViewUser").hide();
    $("#frmUpdateIntervention").hide();
    $("#table_list_intervention").hide();
    $("#frmSearchIntervention").hide();
    $("#table_list_searchintervention").hide();
    $("#frmViewQuotes").hide();
    $(".div-ricerca-intervention").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#row-table-fld-pre").hide();
    $("#table_list_repair_arriving").hide();
  });
}

function getByModuleGuarantee(id) {
  InterventionsServices.getByModuleGuarantee(id, function (response) {

    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        if (i == "id") {
          $("#ModuleValidationGuarantee #" + i).html(val);
        } else {
          $("#ModuleValidationGuarantee #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#ModuleValidationGuarantee #imgPreview").attr("src", val);
          } else {
            $("#ModuleValidationGuarantee #imgName").remove();
          }
        } else {
          $("#ModuleValidationGuarantee #" + i).val(val);
          $("#ModuleValidationGuarantee #" + i).html(val);
        }
      }
    });

    $("#ModuleValidationGuarantee").show();
    $("#frmViewUser").hide();
    $("#frmUpdateIntervention").hide();
    $("#table_list_intervention").hide();
    $("#frmSearchIntervention").hide();
    $("#table_list_searchintervention").hide();
    $("#frmViewQuotes").hide();
    $(".div-ricerca-intervention").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#row-table-fld-pre").hide();
    $("#table_list_repair_arriving").hide();
    $("#FaultModule").hide();
  });
}

function getByIdUser(user_id) {
  // spinnerHelpers.show();
  InterventionsServices.getByIdUser(user_id, function (response) {
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        $("#frmViewUser #" + i).attr("checked", "checked");
      } else {
        $("#frmViewUser #" + i).val(val);
      }
    });
    $("#frmViewUser").show();
    $("#frmUpdateIntervention").hide();
    $("#table_list_intervention").hide();
    $("#table_list_searchintervention").hide();
    $("#frmSearchIntervention").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#ModuleValidationGuarantee").hide();
    $(".div-ricerca-intervention").hide();
    $("#FaultModule").hide();
  });
}

//   ------ region get by -------  //

function updateCheck(id) {
  spinnerHelpers.show();
  var data = {
    id: id
  };
  InterventionsServices.updateCheck(data, function () {
    $("#table_list_intervention #ready").val("");
    notificationHelpers.success("Modifica avvenuta con successo");
    window.location.reload();
    spinnerHelpers.hide();
    $("#table_list_intervention").show();
    $("#frmViewQuotes").hide();
    $("#frmViewUser").hide();
    $("#frmUpdateIntervention").hide();
    $("#frmSearchIntervention").hide();
    $("#table_list_searchintervention").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
  });
}

function updateSelectReferent() {
  spinnerHelpers.show();
  var data = {
    id: $("#frmViewSelectReferent #id").val(),
    id_people: $("#frmViewSelectReferent #id_people").val()
  };

  InterventionsServices.updateSelectReferent(data, function () {
    $("#frmViewSelectReferent #id_people").val("");
    notificationHelpers.success("Modifica avvenuta con successo");
    spinnerHelpers.hide();
    window.location.reload();
    $("#table_list_intervention").show();
    $("#frmViewQuotes").hide();
    $("#frmViewUser").hide();
    $("#frmUpdateIntervention").hide();
    $("#frmSearchIntervention").hide();
    $("#table_list_searchintervention").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
  });
}
function updateExternalRepair() {
  //spinnerHelpers.show();

  var data = {
    id: $("#frmViewExternalRepair #id").val(),
    id_intervention: $("#frmViewExternalRepair #id_intervention").val(),
    id_librone_rip_esterna: $("#frmViewExternalRepair #id_librone_rip_esterna").val(),
    codice_intervento: $("#frmViewExternalRepair #codice_intervento").val(),
    date: $("#frmViewExternalRepair #date").val(),
    ddt_exit: $("#frmViewExternalRepair #ddt_exit").val(),
    id_business_name_supplier: $("#frmViewExternalRepair #id_business_name_supplier").val(),
    date_return_product_from_the_supplier: $("#frmViewExternalRepair #date_return_product_from_the_supplier").val(),
    ddt_supplier: $("#frmViewExternalRepair #ddt_supplier").val(),
    reference_supplier: $("#frmViewExternalRepair #reference_supplier").val(),
    notes_from_repairman: $("#frmViewExternalRepair #notes_from_repairman").val(),
    price: $("#frmViewExternalRepair #price").val()
  };

  if (data.id_business_name_supplier == "" || data.id_business_name_supplier == null || data.id_business_name_supplier == 'null') {
    notificationHelpers.error("Attenzione,non puoi salvare, selezionare il fornitore;");
  } else {
    InterventionsServices.updateExternalRepair(data, function () {
      $("#frmVieUpdatewRepair #date").val("");
      $("#frmVieUpdatewRepair #ddt_exit").val("");
      $("#frmVieUpdatewRepair #codice_intervento").val("");
      $("#frmVieUpdatewRepair #id_business_name_supplier").val("");
      $("#frmVieUpdatewRepair #date_return_product_from_the_supplier").val("");
      $("#frmVieUpdatewRepair #ddt_supplier").val("");
      $("#frmVieUpdatewRepair #reference_supplier").val("");
      $("#frmVieUpdatewRepair #notes_from_repairman").val("");
      $("#frmVieUpdatewRepair #price").val("");
      notificationHelpers.success("Modifica avvenuta con successo");
      spinnerHelpers.hide();
      window.location.reload();
      $("#table_list_intervention").show();
      $("#frmViewQuotes").hide();
      $("#frmViewUser").hide();
      $("#frmUpdateIntervention").hide();
      $("#frmSearchIntervention").hide();
      $("#table_list_searchintervention").hide();
      $("#frmViewExternalRepair").hide();
      $("#frmViewSelectReferent").hide();
      $("#ModuleValidationGuarantee").hide();
      $("#FaultModule").hide();
    });
  }
}

function updateInterventionUpdateViewQuotes() {
  // spinnerHelpers.show();
  var data = {
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmViewQuotes #id').val(),
    email: $('#frmViewQuotes #email').val(),
    id_states_quote: $('#frmViewQuotes #id_states_quote').val(),
    object: $('#frmViewQuotes #object').val(),
    date_agg: $('#frmViewQuotes #date_agg').val(),
    code_intervention_gr: $('#frmViewQuotes #code_intervention_gr').val(),
    note_before_the_quote: $('#frmViewQuotes #note_before_the_quote').val(),
    note: $('#frmViewQuotes #note').val(),
    description_payment: $('#frmViewQuotes #description_payment').val(),
    tot_quote: $('#frmViewQuotes #tot_quote').val(),
    discount: $('#frmViewQuotes #discount').val(),
    total_amount_with_discount: $('#frmViewQuotes #total_amount_with_discount').val(),
    note_customer: $('#frmViewQuotes #note_customer').val(),
    kind_attention: $('#frmViewQuotes #kind_attention').val(),
    description_and_items_agg: $('#frmViewQuotes #description_and_items_agg').val()
  };

  var arrayAggOptions = [];
  for (var j = 0; j < 10; j++) {
    var jsonAggOptions = {};
    var check = false;

    if (functionHelpers.isValued($('#emailreferente' + j).val())) {
      check = true;
      jsonAggOptions['emailreferente'] = $('#emailreferente' + j).val();
    }
    if (functionHelpers.isValued($('#people_id' + j).val())) {
      check = true;
      jsonAggOptions['people_id'] = $('#people_id' + j).val();
    }
    if (functionHelpers.isValued($('#user_id' + j).val())) {
      check = true;
      jsonAggOptions['user_id'] = $('#user_id' + j).val();
    }
    if (check) {
      arrayAggOptions.push(jsonAggOptions);
    }
  }

  data.AggOptions = arrayAggOptions;

  if (data.id_states_quote == "" || data.id_states_quote == null || data.id_states_quote == 'null') {
    notificationHelpers.error("Attenzione,non puoi salvare, seleziona lo stato del preventivo;");
  } else {
    InterventionsServices.updateInterventionUpdateViewQuotes(data, function () {
      $('#frmViewQuotes #email').val('');
      $('#frmViewQuotes #id_states_quote').val('');
      $('#frmViewQuotes #code_intervention_gr').val('');
      $('#frmViewQuotes #object').val('');
      $('#frmViewQuotes #date_agg').val('');
      $('#frmViewQuotes #id_states_quote').val('');
      $('#frmViewQuotes #note_before_the_quote').val('');
      $('#frmViewQuotes #description_payment').val('');
      $('#frmViewQuotes #note').val('');
      $('#frmViewQuotes #discount').val('');
      $('#frmViewQuotes #total_amount_with_discount').val('');
      $('#frmViewQuotes #tot_quote').val('');
      $('#frmViewQuotes #kind_attention').val('');
      $('#frmViewQuotes #description_and_items_agg').val('');
      notificationHelpers.success("Modifica avvenuta con successo");
      spinnerHelpers.hide();
      window.location.reload();
      $("#table_list_intervention").show();
      $("#frmViewQuotes").hide();
      $("#frmViewUser").hide();
      $("#frmUpdateIntervention").hide();
      $("#frmSearchIntervention").show();
      $("#table_list_searchintervention").hide();
      $("#frmViewSelectReferent").hide();
      $("#ModuleValidationGuarantee").hide();
      $("#FaultModule").hide();
    });
  }
}
function updateUser() {
  spinnerHelpers.show();
  var data = {
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $("#frmViewUser #id").val(),
    username: $("#frmViewUser #username").val(),
    email: $("#frmViewUser #email").val(),
    fiscal_code: $("#frmViewUser #fiscal_code").val(),
    business_name: $("#frmViewUser #business_name").val(),
    name: $("#frmViewUser #name").val(),
    surname: $("#frmViewUser #surname").val(),
    vat_number: $("#frmViewUser #vat_number").val(),
    address: $("#frmViewUser #address").val(),
    codepostal: $("#frmViewUser #codepostal").val(),
    country: $("#frmViewUser #country").val(),
    province: $("#frmViewUser #province").val(),
    website: $("#frmViewUser #website").val(),
    vat_type_id: $("#frmViewUser #vat_type_id").val(),
    telephone_number: $("#frmViewUser #telephone_number").val(),
    fax_number: $("#frmViewUser #fax_number").val(),
    mobile_number: $("#frmViewUser #mobile_number").val(),
    pec: $("#frmViewUser #pec").val()
  };
  InterventionsServices.updateUser(data, function () {
    $("#frmViewUser #username").val("");
    $("#frmViewUser #email").val("");
    $("#frmViewUser #fiscal_code").val("");
    $("#frmViewUser #business_name").val("");
    $("#frmViewUser #name").val("");
    $("#frmViewUser #surname").val("");
    $("#frmViewUser #vat_number").val("");
    $("#frmViewUser #address").val("");
    $("#frmViewUser #codepostal").val("");
    $("#frmViewUser #country").val("");
    $("#frmViewUser #province").val("");
    $("#frmViewUser #website").val("");
    $("#frmViewUser #vat_type_id").val("");
    $("#frmViewUser #telephone_number").val("");
    $("#frmViewUser #fax_number").val("");
    $("#frmViewUser #mobile_number").val("");
    $("#frmViewUser #pec").val("");
    notificationHelpers.success("Modifica avvenuta con successo");
    spinnerHelpers.hide();
    window.location.reload();
    $("#table_list_intervention").show();
    $("#frmViewQuotes").hide();
    $("#frmViewUser").hide();
    $("#frmUpdateIntervention").hide();
    $("#frmSearchIntervention").hide();
    $("#table_list_searchintervention").hide();
    $("#frmViewSelectReferent").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
  });
}

function updateInterventionUpdateProcessingSheet() {
  spinnerHelpers.show();
  var search_product = false;
  if ($("#frmViewProcessingSheet #search_product").is(":checked")) {
    search_product = true;
  } else {
    search_product = false;
  }
  var to_call = false;
  if ($("#frmViewProcessingSheet #to_call").is(":checked")) {
    to_call = true;
  } else {
    to_call = false;
  }
  var create_quote = false;
  if ($("#frmViewProcessingSheet #create_quote").is(":checked")) {
    create_quote = true;
  } else {
    create_quote = false;
  }
  var escape_from = false;
  if ($("#frmViewProcessingSheet #escape_from").is(":checked")) {
    escape_from = true;
  } else {
    escape_from = false;
  }
  var working = false;
  if ($("#frmViewProcessingSheet #working").is(":checked")) {
    working = true;
  } else {
    working = false;
  }

  var unrepairable = false;
  if ($("#frmViewProcessingSheet #unrepairable").is(":checked")) {
    unrepairable = true;
  } else {
    unrepairable = false;
  }

  var working_without_testing = false;
  if ($('#frmViewProcessingSheet #working_without_testing').is(':checked')) {
    working_without_testing = true;
  } else {
    working_without_testing = false;
  }
  var working_with_testing = false;
  if ($('#frmViewProcessingSheet #working_with_testing').is(':checked')) {
    working_with_testing = true;
  } else {
    working_with_testing = false;
  }
  var data = {
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $("#frmViewProcessingSheet #id").val(),
    flaw_detection: $("#frmViewProcessingSheet #flaw_detection").val(),
    done_works: $("#frmViewProcessingSheet #done_works").val(),
    hour: $("#frmViewProcessingSheet #hour").val(),
    value_config_hour: $("#frmViewProcessingSheet #value_config_hour").val(),
    code_intervention_gr: $("#frmViewProcessingSheet #code_intervention_gr").val(),
    tot_value_config: $("#frmViewProcessingSheet #tot_value_config").val(),
    consumables: $("#frmViewProcessingSheet #consumables").val(),
    tot_final: $("#frmViewProcessingSheet #tot_final").val(),
    date_aggs: $("#frmViewProcessingSheet #date_aggs").val(),
    total_calculated: $("#frmViewProcessingSheet #total_calculated").val(),
    n_u: $("#frmViewProcessingSheet #n_u").val(),
    unrepairable: unrepairable,
    to_call: to_call,
    working_with_testing: working_with_testing,
    working_without_testing: working_without_testing,
    search_product: search_product,
    create_quote: create_quote,
    escape_from: escape_from,
    working: working,
    id_intervention: $("#frmViewProcessingSheet #id").val(),
    description: $("#frmViewProcessingSheet #description").val(),
    qta: $("#frmViewProcessingSheet #qta").val(),
    id_gr_items: $("#frmViewProcessingSheet #code_gr").val(),
    price_list: $("#frmViewProcessingSheet #price_list").val(),
    tot: $("#frmViewProcessingSheet #tot").val()
  };

  var arrayArticles = [];
  for (var i = 0; i < 15; i++) {
    var jsonArticles = {};
    var check = false;
    if (functionHelpers.isValued($("#frmViewProcessingSheet #description" + i).val())) {
      check = true;
      jsonArticles["description"] = $("#frmViewProcessingSheet #description" + i).val();
    }
    if (functionHelpers.isValued($("#code_gr" + i).val())) {
      check = true;
      jsonArticles["code_gr"] = $("#code_gr" + i).val();
    }
    if (functionHelpers.isValued($("#qta" + i).val())) {
      check = true;
      jsonArticles["qta"] = $("#qta" + i).val();
    }
    if (functionHelpers.isValued($("#price_list" + i).val())) {
      check = true;
      jsonArticles["price_list"] = $("#price_list" + i).val();
    }
    if (functionHelpers.isValued($("#tot" + i).val())) {
      check = true;
      jsonArticles["tot"] = $("#tot" + i).val();
    }
    if (functionHelpers.isValued($("#n_u" + i).val())) {
      check = true;
      jsonArticles["n_u"] = $("#n_u" + i).val();
    }

    if (check) {
      arrayArticles.push(jsonArticles);
    }
  }
  data.Articles = arrayArticles;

  InterventionsServices.updateInterventionUpdateProcessingSheet(data, function () {
    $("#frmViewProcessingSheet #create_quote").val("");
    $("#frmViewProcessingSheet #escape_from").val("");
    $("#frmViewProcessingSheet #working").val("");
    $("#frmViewProcessingSheet #to_call").val("");
    $("#frmViewProcessingSheet #search_product").val("");
    $("#frmViewProcessingSheet #flaw_detection").val("");
    $("#frmViewProcessingSheet #done_works").val("");
    $("#frmViewProcessingSheet #hour").val("");
    $("#frmViewProcessingSheet #value_config_hour").val("");
    $("#frmViewProcessingSheet #tot_value_config").val("");
    $("#frmViewProcessingSheet #consumables").val("");
    $("#frmViewProcessingSheet #tot_final").val("");
    $("#frmViewProcessingSheet #date_aggs").val("");
    $("#frmViewProcessingSheet #total_calculated").val("");
    $("#frmViewProcessingSheet #n_u").val("");
    $("#frmViewProcessingSheet #description").val("");
    $("#frmViewProcessingSheet #qta").val("");
    $("#frmViewProcessingSheet #code_gr").val("");
    $("#frmViewProcessingSheet #price_list").val("");
    $("#frmViewProcessingSheet #tot").val("");
    notificationHelpers.success(dictionaryHelpers.getDictionary("ContactRequestCompleted"));
    spinnerHelpers.hide();
    window.location.reload();
    $("#table_list_intervention").show();
    $("#frmViewQuotes").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewProcessingSheet").hide();
    $("#frmViewSelectReferent").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
  });
}

function updateIntervention() {
  //spinnerHelpers.show();

  var unrepairable = false;
  if ($("#frmUpdateIntervention #unrepairable").is(":checked")) {
    unrepairable = true;
  } else {
    unrepairable = false;
  }

  var create_quote = false;
  if ($("#frmUpdateIntervention #create_quote").is(":checked")) {
    create_quote = true;
  } else {
    create_quote = false;
  }
  var send_to_customer = false;
  if ($("#frmUpdateIntervention #send_to_customer").is(":checked")) {
    send_to_customer = true;
  } else {
    send_to_customer = false;
  }
  var data = {
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $("#frmUpdateIntervention #id").val(),
    code_intervention_gr: $("#frmUpdateIntervention #code_intervention_gr").val(),
    states_id: $("#frmUpdateIntervention #states_id").val(),
    referent: $("#frmUpdateIntervention #referent").val(),
    external_referent_id_intervention: $("#frmUpdateIntervention #external_referent_id_intervention").val(),
    nr_ddt_gr: $("#frmUpdateIntervention #nr_ddt_gr").val(),
    date_ddt_gr: $("#frmUpdateIntervention #date_ddt_gr").val(),
    customers_id: $("#frmUpdateIntervention #customer_id").val(),
    user_id: $("#frmUpdateIntervention #user_id_intervention").val(),
    items_id: $("#frmUpdateIntervention #items_id").val(),
    //items_id: $("#frmUpdateIntervention #name_product").val(),
    plant_type: $("#frmUpdateIntervention #plant_type").val(),
    trolley_type: $("#frmUpdateIntervention #trolley_type").val(),
    description: $("#frmUpdateIntervention #description").val(),
    series: $("#frmUpdateIntervention #series").val(),
    nr_ddt: $("#frmUpdateIntervention #nr_ddt").val(),
    date_ddt: $("#frmUpdateIntervention #date_ddt").val(),
    defect: $("#frmUpdateIntervention #defect").val(),
    voltage: $("#frmUpdateIntervention #voltage").val(),
    note_internal_gr: $("#frmUpdateIntervention #note_internal_gr").val(),
    unrepairable: unrepairable,
    send_to_customer: send_to_customer,
    create_quote: create_quote,
    exit_notes: $("#frmUpdateIntervention #exit_notes").val(),
    imgmodule: $("#frmUpdateIntervention #imgBase64").val(),
    imgName: $("#frmUpdateIntervention #imgName").val(),
    code_tracking: $("#frmUpdateIntervention #code_tracking").val(),
    rip_association: $("#frmUpdateIntervention #rip_association").val()
  };

  var file_data = $('#imgmodule').prop('files')[0];
  var arrayImgAgg = [];
  for (var i = 0; i <= 4; i++) {
    if (functionHelpers.isValued($("#frmUpdateIntervention #imgBase64" + i).val())) {
      arrayImgAgg.push($("#frmUpdateIntervention #imgBase64" + i).val());
    }
  }
  data.captures = arrayImgAgg;

  if (data.user_id == "" || data.user_id == null || data.user_id == 'null') {
    notificationHelpers.error("Attenzione,non puoi salvare, selezionare il cliente;");
  } else {
    InterventionsServices.updateIntervention(data, function () {
      //$('#frmUpdateIntervention #id').val('');
      $("#frmUpdateIntervention #states_id").val("");
      $("#frmUpdateIntervention #note_internal_gr").val("");
      $("#frmUpdateIntervention #user_id_intervention").val("");
      $("#frmUpdateIntervention #referent").val("");
      $("#frmUpdateIntervention #nr_ddt_gr").val("");
      $("#frmUpdateIntervention #date_ddt_gr").val("");
      $("#frmUpdateIntervention #customer_id").val("");
      $("#frmUpdateIntervention #items_id").val("");
      $("#frmUpdateIntervention #plant_type").val("");
      $("#frmUpdateIntervention #trolley_type").val("");
      $("#frmUpdateIntervention #description").val("");
      $("#frmUpdateIntervention #series").val("");
      $("#frmUpdateIntervention #nr_ddt").val("");
      $("#frmUpdateIntervention #date_ddt").val("");
      $("#frmUpdateIntervention #defect").val("");
      $("#frmUpdateIntervention #voltage").val("");
      $("#frmUpdateIntervention #unrepairable").val("");
      $("#frmUpdateIntervention #send_to_customer").val("");
      $("#frmUpdateIntervention #exit_notes").val("");
      $("#frmUpdateIntervention #create_quote").val("");
      $("#frmUpdateIntervention #imgBase64").val("");
      $("#frmUpdateIntervention #code_tracking").val("");
      $("#frmUpdateIntervention #rip_association").val("");
      notificationHelpers.success(dictionaryHelpers.getDictionary("ContactRequestCompleted"));
      spinnerHelpers.hide();
      window.location.reload();
      $("#table_list_intervention").show();
      $("#frmUpdateIntervention").hide();
      $("#ModuleValidationGuarantee").hide();
      $("#FaultModule").hide();
      $("#table_list_repair_arriving").show();
    });
  }
}

function deleteRow(id) {
  if (confirm("Vuoi eliminare l'intervento?")) {
    spinnerHelpers.show();
    InterventionsServices.del(id, function (response) {
      notificationHelpers.success(dictionaryHelpers.getDictionary("DeleteCompleted"));
      spinnerHelpers.hide();
      window.location.reload();
    });
  }
}

function getDescriptionAndPriceProcessingSheet(id, contRow) {
  InterventionsServices.getDescriptionAndPriceProcessingSheet(id, function (response) {
    $("#frmViewProcessingSheet #description" + contRow).val(response.data.tipology + ' ' + response.data.note + ' ' + response.data.plant);
    $("#frmViewProcessingSheet #price_list" + contRow).val(response.data.price_list);
  });
}

/***   POST   ***/
function searchImplemented(idIntervention, nr_ddt, user_id, create_quote) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(idIntervention, nr_ddt, user_id, create_quote);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  searchInterventionServices.searchImplemented(data, function (response) {
    for (var i = 0; i < response.data.length; i++) {
      var newList = "";
      newList += "<tr>";
      newList += '<td><button class="btn btn-info" onclick="interventionHelpers.getById(' + response.data[i].id + ')"><i class="fa fa-pencil"/></button></td>';
      newList += "<td>" + response.data[i].id + "</td>";
      newList += "<td>" + response.data[i].date_ddt + "</td>";
      newList += "<td>" + response.data[i].nr_ddt + "</td>";
      newList += "<td>" + response.data[i].descriptionCustomer + '<button class="popup-button" data-popup="#popup-remove-account-main" onclick="interventionHelpers.getByIdUser(' + response.data[i].user_id + ')"><i class="fa fa-search-plus"/></button>' + "</td>";
      newList += "<td>" + response.data[i].description + "</td>";
      newList += "<td>" + response.data[i].defect + "<a href='" + response.data[i].imgmodule + "'</a><br>Anteprima</td>";
      newList += "<td>" + response.data[i].nr_ddt_gr + "</td>";
      newList += "<td>" + response.data[i].date_ddt_gr + "</td>";
      newList += "<td>" + response.data[i].create_quote + "</td>";
      newList += "<td>" + response.data[i].descriptionStates + "</td>";
      newList += "<td>" + response.data[i].codeTracking + "</td>";
      newList += '<td><button class="btn btn-danger" onclick="interventionHelpers.deleteRow(' + response.data[i].id + ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
      newList += "</tr>";

      $("#table_list_searchintervention").show();
      $("#table_list_searchintervention tbody").html(newList);
    }
  });

  spinnerHelpers.hide();
}

/***   END POST   ***/

function getAllRepairArrivedNumberMenuGlobal() {
  InterventionsServices.getAllRepairArrivedNumberMenuGlobal(storageData.sIdLanguage(), function (response) {
    $('#contanumeriid').html(response.data[0].contanumeriid);
  });
}

/***/ }),
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["getPeopleUser"] = getPeopleUser;
/* harmony export (immutable) */ __webpack_exports__["getByIdReferent"] = getByIdReferent;
/* harmony export (immutable) */ __webpack_exports__["updateReferent"] = updateReferent;
/* harmony export (immutable) */ __webpack_exports__["insertNoAuth"] = insertNoAuth;
/* harmony export (immutable) */ __webpack_exports__["deleteRow"] = deleteRow;
/** *   peopleHelpers   ***/

function insert() {
  spinnerHelpers.show();

  var data = {
    user_id: storageData.sUserId(),
    name: $('#people #name').val(),
    surname: $('#people #surname').val(),
    email: $('#people #email').val(),
    phone_number: $('#people #phone_number').val(),
    roles_id: $('#people #roles_id').val()
  };

  peopleServices.insert(data, function () {
    $('#people #name').val('');
    $('#people #surname').val('');
    $('#people #email').val('');
    $('#people #phone_number').val('');
    $('#people #roles_id').val('');
    notificationHelpers.success(dictionaryHelpers.getDictionary('ContactRequestCompleted'));
    spinnerHelpers.hide();
  });
}

function getPeopleUser(id) {
  spinnerHelpers.show();
  peopleServices.getPeopleUser(id, function (response) {
    for (var i = 0; i < response.data.length; i++) {
      var newList = '';
      newList += '<tr>';
      newList += '<td><button class="btn btn-info" onclick="peopleHelpers.getByIdReferent(' + response.data[i].id + ')"><i class="fa fa-pencil"/></button>';
      newList += '<button class="btn btn-danger" onclick="peopleHelpers.deleteRow(' + response.data[i].id + ')"><i class="fa fa-trash"/></button></td>';
      newList += '<td>' + response.data[i].id + '</td>';
      newList += '<td>' + response.data[i].name + '</td>';
      newList += '<td>' + response.data[i].surname + '</td>';
      newList += '<td>' + response.data[i].email + '</td>';
      newList += '<td>' + response.data[i].phone_number + '</td>';
      newList += '<td>' + response.data[i].roles_id + '</td>';
      newList += '</tr>';

      $('#table_list_people tbody').append(newList);
      $('#table_list_people').show();
      $('#frmUpdateReferent').hide();
    }
  });
  spinnerHelpers.hide();
}

function getByIdReferent(id) {
  // spinnerHelpers.show();
  peopleServices.getByIdReferent(id, function (response) {
    var searchParam = '';

    // select roles_id
    var initials = [];
    initials.push({
      id: response.data.roles_id,
      text: response.data.descriptionRoleId
    });
    $('#roles_id').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/roles/select',
        data: function data(params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });

    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        $('#frmUpdateReferent #' + i).attr('checked', 'checked');
      } else {
        $('#frmUpdateReferent #' + i).val(val);
      }
    });
    $('#frmUpdateReferent').show();
    $('#frmAddReferent').hide();
    $('#table_list_people').hide();
  });
}

function updateReferent() {
  spinnerHelpers.show();
  var data = {
    // id: storageData.sUserId(),
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmUpdateReferent #id').val(),
    name: $('#frmUpdateReferent #names').val(),
    surname: $('#frmUpdateReferent #surnames').val(),
    email: $('#frmUpdateReferent #emails').val(),
    phone_number: $('#frmUpdateReferent #phone_numbers').val(),
    roles_id: $('#frmUpdateReferent #roles_id').val()
  };
  peopleServices.updateReferent(data, function () {
    $('#frmUpdateReferent #id').val('');
    $('#frmUpdateReferent #names').val('');
    $('#frmUpdateReferent #surnames').val('');
    $('#frmUpdateReferent #emails').val('');
    $('#frmUpdateReferent #phone_numbers').val('');
    $('#frmUpdateReferent #roles_id').val('');

    notificationHelpers.success('Referente aggiornato correttamente!');
    spinnerHelpers.hide();
    $('#table_list_people').show();
    $('#frmUpdateReferent').hide();
  });
}

function insertNoAuth() {
  spinnerHelpers.show();

  var data = {
    created_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    // id: $('#frmAddReferent #id').val(),
    name: $('#frmAddReferent #name').val(),
    surname: $('#frmAddReferent #surname').val(),
    email: $('#frmAddReferent #email').val(),
    phone_number: $('#frmAddReferent #phone_number').val(),
    roles_id: $('#frmAddReferent #add_roles_id').val(),
    user_id: $('#frmAddReferent #user_id').val()
  };

  peopleServices.insertNoAuth(data, function () {
    // $('#frmAddReferent #id').val('');
    $('#frmAddReferent #name').val('');
    $('#frmAddReferent #surname').val('');
    $('#frmAddReferent #email').val('');
    $('#frmAddReferent #phone_number').val('');
    $('#frmAddReferent #add_roles_id').val('');
    $('#frmAddReferent #user_id').val('');

    $('#frmAddReferent').hide();

    $('#table_list_people').show();

    notificationHelpers.success('Referente inserito correttamente!');

    spinnerHelpers.hide();
  });
}

function deleteRow(id) {
  spinnerHelpers.show();
  peopleServices.del(id, function (response) {
    notificationHelpers.success(dictionaryHelpers.getDictionary('DeleteCompleted'));
    window.location.reload();
    $('#table_list_people').show();
    $('#table_list_user').hide();
  });
  spinnerHelpers.hide();
}

/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/** *   adsHelpers   ***/

function insert() {
  spinnerHelpers.show();
  var link = $('#ads #description').val();
  var a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;';
  var b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------';
  var p = new RegExp(a.split('').join('|'), 'g');

  var linkrewrite = link.toString().toLowerCase().replace(/\s+/g, '-') // Replace spaces with -
  .replace(p, function (c) {
    return b.charAt(a.indexOf(c));
  }) // Replace special characters
  .replace(/&/g, '-and-') // Replace & with 'and'
  .replace(/[^\w\-]+/g, '') // Remove all non-word characters
  .replace(/\-\-+/g, '-') // Replace multiple - with single -
  .replace(/^-+/, '') // Trim - from start of text
  .replace(/-+$/, '');

  var urlRewrite = '/' + linkrewrite + '-' + storageData.sUserId();

  var data = {
    customer_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    description: $('#ads #description').val(),
    meta_tag_link: urlRewrite,
    price: $('#ads #price').val().replace(',', '.'),
    available: $('#ads #available').val(),
    external_link: $('#ads #external_link').val(),
    new: $('#ads #new').val(),
    free_shipment: $('#ads #free_shipment').val(),
    shipping_price: $('#ads #shipping_price').val(),
    producer_id: $('#ads #producer_id').val(),
    meta_tag_characteristic: $('#ads #meta_tag_characteristic').val(),
    category_id: $('#ads #category_id').val(),
    img: $('#ads #imgBase64').val(),
    imgName: $('#ads #imgName').val()
  };

  var arrayImgAgg = [];
  for (var i = 0; i < 10; i++) {
    if (functionHelpers.isValued($('#imgBase64' + i).val()) && functionHelpers.isValued($('#imgName' + i).val())) {
      arrayImgAgg.push(JSON.parse('{"imgBase64' + i + '":"' + $('#imgBase64' + i).val() + '", "imgName' + i + '":"' + $('#imgName' + i).val() + '"}'));
    }
  }

  // if(arrayImgAgg.length>0){
  data.imgAgg = arrayImgAgg;
  // }

  itemServices.insert(data, function () {
    notificationHelpers.success('Annuncio caricato con successo!');
    setTimeout(function () {
      window.location = '/i-tuoi-annunci';
    }, 2000);
    spinnerHelpers.hide();
  });
}

/***/ }),
/* 19 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["select2"] = select2;
/** *   languageHelpers   ***/

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/language/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function select2(selector) {
  $(selector).select2(languageHelpers.getSelect2ForBootstrapTable());
}

/***/ }),
/* 20 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["login"] = login;
/* harmony export (immutable) */ __webpack_exports__["disconnect"] = disconnect;
/* harmony export (immutable) */ __webpack_exports__["refreshToken"] = refreshToken;
/* harmony export (immutable) */ __webpack_exports__["loginSalesman"] = loginSalesman;
/* harmony export (immutable) */ __webpack_exports__["disconnectUserBySalesman"] = disconnectUserBySalesman;
/* harmony export (immutable) */ __webpack_exports__["disconnectSalesman"] = disconnectSalesman;
/* harmony export (immutable) */ __webpack_exports__["insertLoginForGoogle"] = insertLoginForGoogle;
/* harmony export (immutable) */ __webpack_exports__["insertLoginForFacebook"] = insertLoginForFacebook;
/** *   loginHelpers   ***/

function login() {
  spinnerHelpers.show();
  $('#loginPasswordError').hide();
  var sSubmitFormButton = $('#btnLogin');
  var username = $('#loginUsername').val();
  var password = $('#loginPassword').val();

  if (functionHelpers.isValued(sSubmitFormButton)) {
    sSubmitFormButton.text('Login...');
    sSubmitFormButton.prop('disabled', true);
  }

  if ($('#chkSaveLogin').is(':checked')) {
    storageData.setCredetial({
      username: username,
      password: password
    });
  } else {
    storageData.removeCredetial();
  }

  loginServices.login(username, password, function (result) {
    console.log(result);
    if (result.code == '200') {
      storageData.setTokenKey('Bearer ' + result.access_token);
      storageData.setRefreshTokenKey(result.refresh_token);
      storageData.setExpiresTokenKey(result.expires_at);
      window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000);
      storageData.setUserName(result.username);
      storageData.setUserDescription(result.user_description);
      storageData.setUserId(result.id);
      storageData.setRole(result.role);

      storageData.removeIdLanguage();
      window.localStorage.setItem('sIdLanguage', result.idLanguage);
      storageData.setCookie('sIdLanguage', result.idLanguage, 365);

      storageData.setCartId(result.cart_id);

      if (pageHelpers.isAdminPage() || result.username == 'admin') {
        storageData.setIdLanguage(result.idLanguage);
        adminConfigurationServices.getMenu(function (result) {
          storageData.setMenu(result.data);
          if (functionHelpers.isValued(storageData.sHoldUrl())) {
            window.location = '/?' + storageData.sHoldUrl();
            storageData.removeHoldUrl();
          } else if (pageHelpers.isAdminPage()) {
            window.location = '/admin';
          } else {
            window.location = '/';
          }
        }, function (response) {
          if (functionHelpers.isValued(response)) {
            $('#loginPasswordError').text(response);
            $('#loginPasswordError').show();
            notificationHelpers.error(response);
          }

          var sSubmitFormButton = $('#btnLogin');
          sSubmitFormButton.text('Login');
          sSubmitFormButton.prop('disabled', false);
          spinnerHelpers.hide();
        });
      } else if (functionHelpers.isValued(storageData.sHoldUrl())) {
        window.location = '/?' + storageData.sHoldUrl();
        storageData.removeHoldUrl();
      } else {
        window.location = '/';
      }
    } else {
      if (result.code == '404') {
        confirm(result.message);
      } else {
        confirm('Credenziali errate');
      }
      var _sSubmitFormButton = $('#btnLogin');
      _sSubmitFormButton.text('Login');
      _sSubmitFormButton.prop('disabled', false);
      spinnerHelpers.hide();
    }
  }, function (response) {
    if (response.status == 0) {
      $('#loginPasswordError').text(dictionaryHelpers.getDictionary('UnableToConnectToServer'));
    } else if (functionHelpers.isValued(response.responseText) && jQuery.parseJSON(response.responseText).error_description != undefined) {
      $('#loginPasswordError').text(jQuery.parseJSON(response.responseText).error_description);
    }
    $('#loginPasswordError').show();
    var sSubmitFormButton = $('#btnLogin');
    sSubmitFormButton.text('Login');
    sSubmitFormButton.prop('disabled', false);
    spinnerHelpers.hide();
  });
}

function disconnect() {
  var credential = storageData.sCredetial();
  storageData.removeCredentialData();
  storageData.setCredetial(credential);
  storageData.setCartId('');
  if (window.location.hash != '#logout') {
    storageData.setHoldUrl(window.location.hash);
  }

  /*var auth2 = gapi.auth2.getAuthInstance();
  auth2.signOut().then(function () {
    console.log('User signed out.');
  });*/

  if (pageHelpers.isAdminPage()) {
    window.location = '/admin/login';
  } else {

    window.location = '/login';
  }
}

function refreshToken(refresh) {
  var diff = Math.round((new Date(storageData.sExpiresTokenKey()).getTime() - new Date().getTime()) / 1000 / 60);

  if (diff < 0) {
    loginHelpers.disconnect();
  } else if (diff <= 5) {
    loginServices.refreshToken(function (result) {
      storageData.setTokenKey('bearer ' + result.access_token);
      storageData.setRefreshTokenKey(result.refresh_token);
      storageData.setExpiresTokenKey(result['.expires']);
      window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000);
    });
  } else if (functionHelpers.isValued(refresh) && refresh) {
    window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000);
  }
}

function loginSalesman() {
  spinnerHelpers.show();
  $('#loginPasswordError').hide();
  var sSubmitFormButton = $('#btnLoginSalesman');
  var username = $('#loginUsernameSalesman').val();
  var password = $('#loginPasswordSalesman').val();

  if (functionHelpers.isValued(sSubmitFormButton)) {
    sSubmitFormButton.text('Login...');
    sSubmitFormButton.prop('disabled', true);
  }

  if ($('#chkSaveLogin').is(':checked')) {
    storageData.setCredetial({
      username: username,
      password: password
    });
  } else {
    storageData.removeCredetial();
  }

  loginServices.loginSalesman(username, password, function (result) {
    if (result.code == '200') {
      storageData.setSalesmanId(result.id);
      var _sSubmitFormButton2 = $('#btnLoginSalesman');
      _sSubmitFormButton2.text('Login');
      _sSubmitFormButton2.prop('disabled', false);
      spinnerHelpers.hide();
      window.location = '/area-agenti';
    } else {
      if (result.code == '404') {
        confirm(result.message);
      } else {
        confirm('Credenziali errate');
      }
      var _sSubmitFormButton3 = $('#btnLoginSalesman');
      _sSubmitFormButton3.text('Login');
      _sSubmitFormButton3.prop('disabled', false);
      spinnerHelpers.hide();
    }
  });
}

function disconnectUserBySalesman() {
  var credential = storageData.sCredetial();
  storageData.removeCredentialData();
  storageData.setCredetial(credential);
  storageData.setCartId('');
  if (window.location.hash != '#logout') {
    storageData.setHoldUrl(window.location.hash);
  }
  window.location.reload(true);
}

function disconnectSalesman() {
  window.localStorage.removeItem('sTokenKey');
  window.localStorage.removeItem('sRefreshTokenKey');
  window.localStorage.removeItem('sExpiresTokenKey');
  window.localStorage.removeItem('sCredetial');
  window.localStorage.removeItem('sUserName');
  window.localStorage.removeItem('sUserId');
  window.localStorage.removeItem('sSalesmanId');
  window.localStorage.removeItem('sUserDescription');
  window.localStorage.removeItem('sNotificationData');
  window.localStorage.removeItem('sRouteParameter');
  window.localStorage.removeItem('sFilterParameter');
  window.localStorage.removeItem('sMenu');

  storageData.delCookie('sUserId');
  storageData.delCookie('sUserDescription');
  storageData.delCookie('sSalesmanId');
  storageData.delCookie('sUserName');
  storageData.delCookie('sRole');
  storageData.delCookie('sCartId');
  window.location.reload(true);
}

/*
export function insertLoginForGoogle(profile) {
 spinnerHelpers.show(); 
  var data = {
   id_google: profile.getId(),
   name: profile.getGivenName(),
   surname: profile.getFamilyName(),
   name_and_surname: profile.getName(),
   email: profile.getEmail(),
 };
 loginServices.insertLoginForGoogle(data, function (result) {
   if (result.data.original.code == '200') {
     storageData.setTokenKey('Bearer ' + result.data.original.access_token)
     storageData.setRefreshTokenKey(result.data.original.refresh_token)
     storageData.setExpiresTokenKey(result.data.original.expires_at)
     window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000)
     storageData.setUserName(result.data.original.username)
     storageData.setUserDescription(result.data.original.user_description)
     storageData.setUserId(result.data.original.id)
     storageData.setRole(result.data.original.role)
     storageData.removeIdLanguage()
     window.localStorage.setItem('sIdLanguage', result.data.original.idLanguage)
     storageData.setCookie('sIdLanguage', result.data.original.idLanguage , 365);
     storageData.setCartId(result.data.original.cart_id)
      if (result.data.original.redirect == true) {
       window.location = '/modifica-i-tuoi-dati';
     }else{
       window.location = '/area-privata';
     }
   }else{
     if (result.data.original.code == '404') {
       confirm(result.data.original.message);
     } else {
       confirm('Credenziali errate');
     }
   }
 });
}
export function insertLoginForFacebook(response2) {
 spinnerHelpers.show(); 
  var data = {
   email: response2.email,
   first_name: response2.first_name,
   name: response2.name,
   id_facebook: response2.id,
   surname: response2.last_name,
   idUser: storageData.sUserId(),
 };
  loginServices.insertLoginForFacebook(data, function (result) {
   if (result.data.original.code == '200') {
     storageData.setTokenKey('Bearer ' + result.data.original.access_token)
     storageData.setRefreshTokenKey(result.data.original.refresh_token)
     storageData.setExpiresTokenKey(result.data.original.expires_at)
     window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000)
     storageData.setUserName(result.data.original.username)
     storageData.setUserDescription(result.data.original.user_description)
     storageData.setUserId(result.data.original.id)
     storageData.setRole(result.data.original.role)
     storageData.removeIdLanguage()
     window.localStorage.setItem('sIdLanguage', result.data.original.idLanguage)
     storageData.setCookie('sIdLanguage', result.data.original.idLanguage , 365);
     storageData.setCartId(result.data.original.cart_id)
      if (result.data.original.redirect == true) {
       window.location = '/modifica-i-tuoi-dati';
     }else{
       window.location = '/area-privata';
     }
   }else{
     if (result.data.original.code == '404') {
       confirm(result.data.original.message);
     } else {
       confirm('Credenziali errate');
     }
   }
 });
}*/

function insertLoginForGoogle(profile) {
  spinnerHelpers.show();

  var data = {
    id_google: profile.getId(),
    name: profile.getGivenName(),
    surname: profile.getFamilyName(),
    name_and_surname: profile.getName(),
    email: profile.getEmail()
  };

  //console.log(data);

  loginServices.insertLoginForGoogle(data, function (result) {
    if (result.data.original.code == '200') {
      storageData.setTokenKey('Bearer ' + result.data.original.access_token);
      storageData.setRefreshTokenKey(result.data.original.refresh_token);
      storageData.setExpiresTokenKey(result.data.original.expires_at);
      window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000);
      storageData.setUserName(result.data.original.username);
      storageData.setUserDescription(result.data.original.user_description);
      storageData.setUserId(result.data.original.id);
      storageData.setRole(result.data.original.role);
      storageData.removeIdLanguage();
      window.localStorage.setItem('sIdLanguage', result.data.original.idLanguage);
      storageData.setCookie('sIdLanguage', result.data.original.idLanguage, 365);
      storageData.setCartId(result.data.original.cart_id);

      if (result.data.original.redirect == true || result.data.original.redirect == 'true') {
        window.location = '/modifica-i-tuoi-dati';
      } else {
        window.location = '/area-privata';
      }
    }
  });
}

function insertLoginForFacebook(response2) {
  spinnerHelpers.show();

  var data = {
    email: response2.email,
    first_name: response2.first_name,
    name: response2.name,
    id_facebook: response2.id,
    surname: response2.last_name,
    idUser: storageData.sUserId()
  };

  loginServices.insertLoginForFacebook(data, function (result) {

    /* if (functionHelpers.isValued(result.data.message)){
      notificationHelpers.success("Accesso avvenuto con successo!");
      spinnerHelpers.hide();
     } else {
        window.location = '/completa-i-tuoi-dati';
     }*/
    if (result.data.original.code == '200') {
      storageData.setTokenKey('Bearer ' + result.data.original.access_token);
      storageData.setRefreshTokenKey(result.data.original.refresh_token);
      storageData.setExpiresTokenKey(result.data.original.expires_at);
      window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000);
      storageData.setUserName(result.data.original.username);
      storageData.setUserDescription(result.data.original.user_description);
      storageData.setUserId(result.data.original.id);
      storageData.setRole(result.data.original.role);
      storageData.removeIdLanguage();
      window.localStorage.setItem('sIdLanguage', result.data.original.idLanguage);
      storageData.setCookie('sIdLanguage', result.data.original.idLanguage, 365);
      storageData.setCartId(result.data.original.cart_id);

      if (result.data.original.redirect == true || result.data.original.redirect == 'true') {
        console.log(result);
        window.location = '/modifica-i-tuoi-dati';
      } else {
        window.location = '/area-privata';
      }
    }
  });
}

/***/ }),
/* 21 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getIdByAdminUrl"] = getIdByAdminUrl;
/* harmony export (immutable) */ __webpack_exports__["getIdFunctionByAdminUrl"] = getIdFunctionByAdminUrl;
/** *   menuHelpers   ***/

function getIdByAdminUrl() {
  var id = void 0;

  $.each(storageData.sMenu(), function () {
    if (this.url == pageHelpers.getUrl()) {
      id = this.id;
      return true;
    } else if (!functionHelpers.isValued(this.nextLevel) && this.nextLevel.length > 0) {
      $.each(this.nextLevel, function () {
        if (this.url == pageHelpers.getUrl()) {
          id = this.id;
          return true;
        }
      });

      if (id != 'undefined') {
        return true;
      }
    }
  });

  return id;
}

function getIdFunctionByAdminUrl(routeParameter) {
  var idFunction = void 0;

  $.each(storageData.sMenu(), function () {
    if (this.url == pageHelpers.getUrl(routeParameter)) {
      idFunction = this.idFunctionality;
      return true;
    } else if (functionHelpers.isValued(this.nextLevel) && this.nextLevel.length > 0) {
      $.each(this.nextLevel, function () {
        if (this.url == pageHelpers.getUrl(routeParameter)) {
          idFunction = this.idFunctionality;
          return true;
        }

        if (functionHelpers.isValued(this.nextLevel) && this.nextLevel.length > 0) {
          $.each(this.nextLevel, function () {
            if (this.url == pageHelpers.getUrl(routeParameter)) {
              idFunction = this.idFunctionality;
              return true;
            }
          });

          if (idFunction != 'undefined') {
            return true;
          }
        }
      });

      if (idFunction != 'undefined') {
        return true;
      }
    }
  });

  return idFunction;
}

/***/ }),
/* 22 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["insertDetail"] = insertDetail;
/* harmony export (immutable) */ __webpack_exports__["getByUser"] = getByUser;
/* harmony export (immutable) */ __webpack_exports__["getDetailById"] = getDetailById;
/* harmony export (immutable) */ __webpack_exports__["deleteRow"] = deleteRow;
/** *   negotiationHelpers   ***/

function insert(idForm) {
  spinnerHelpers.show();

  var data = functionHelpers.formToJson(idForm);

  negotiationServices.insert(data, function () {
    notificationHelpers.success('Messaggio inviato correttamente!');
    spinnerHelpers.hide();
    setTimeout(function () {
      window.location.href = '/trattative';
    }, 2000);
  });
}

function insertDetail(idForm) {
  spinnerHelpers.show();

  var data = functionHelpers.formToJson(idForm);

  negotiationServices.insertDetail(data, function (response) {
    if (response.message != '') {
      $('#description').val('');
      negotiationHelpers.getDetailById(response.message);
      notificationHelpers.success('Messaggio inviato correttamente!');
      spinnerHelpers.hide();
    }
  });
}

function getByUser() {
  spinnerHelpers.show();
  var userId = storageData.sUserId();
  $('#result-negotiation').html('');
  negotiationServices.getByUser(userId, function (response) {
    var html = '';
    for (var i = 0; i < response.data.length; i++) {
      html += '<tr>';
      html += "<td><button class='btn btn-success' onclick='negotiationHelpers.getDetailById(" + response.data[i].id + ");'><i class='fa fa-commenting-o' title='Visualizza il dettaglio'></i></button>";
      html += "<button class='btn btn-danger' onclick='negotiationHelpers.deleteRow(" + response.data[i].id + ")'><i class='fa fa-trash'></i></button>";
      html += '</td>';
      html += "<td><a target='_blank' href='" + response.data[i].link + "'><img class='img-negotiation' src='" + response.data[i].img + "'>" + response.data[i].item + '</a></td>';
      html += '<td>' + response.data[i].userNegotiatior + '<br>' + response.data[i].data + '</td>';
      if (response.data[i].toRead) {
        html += "<td><i class='fa fa-envelope'></i></td>";
      } else {
        html += "<td><i class='fa fa-envelope-open-o'></i></td>";
      }
      html += '</tr>';

      $('#result-negotiation').append(html);
    }
    spinnerHelpers.hide();
  });
}

function getDetailById(id) {
  spinnerHelpers.show();
  var idUser = storageData.sUserId();
  negotiationServices.getDetailById(id, idUser, function (response) {
    $('#id-item-detail').html(response.data.item);

    $('#div-cont-msg-negotiation').html('');
    $('#user_id').val('');
    $('#negotiation_id').val('');

    $('#user_id').val(storageData.sUserId());
    $('#negotiation_id').val(id);

    for (var i = 0; i < response.data.msg.length; i++) {
      var html = '';
      html += "<div class='row'>";
      if (idUser == response.data.msg[i].idUserMsg) {
        html += "<div class='col-12 text-right'>";
        // il messaggio estratto è scritto dall'utente che sta navigando
        html += "<div class='msg-user'><p>" + response.data.msg[i].description + '</p><span>' + response.data.msg[i].data + '</span></div>';
        html += '</div>';
      } else {
        html += "<div class='col-12 text-left'>";
        // il messaggio estratto è scritto da un utente diverso da quello che sta navigando
        html += "<div class='msg-user-negotiator text-left'><p>" + response.data.msg[i].description + '</p><span>' + response.data.msg[i].data + '</span></div>';
        html += '</div>';
      }
      html += '</div>';

      $('#div-cont-msg-negotiation').append(html);
      $('#tab-detail-negotiation').fadeIn('fast');
    }
    $('#div-cont-detail-negotiation').animate({
      scrollTop: $('#div-cont-msg-negotiation')[0].scrollHeight
    }, 'fast');
    negotiationHelpers.getByUser();
    spinnerHelpers.hide();
  });
}

function deleteRow(id) {
  if (confirm("Proseguendo con l'eliminazione anche l'altro utente non vedrà più la trattativa, confermi?")) {
    spinnerHelpers.show();
    negotiationServices.deleteRow(id, function (response) {
      negotiationHelpers.getByUser();
      notificationHelpers.success('Trattativa eliminata correttamente!');
      spinnerHelpers.hide();
    });
  }
}

/***/ }),
/* 23 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["error"] = error;
/* harmony export (immutable) */ __webpack_exports__["success"] = success;
/* harmony export (immutable) */ __webpack_exports__["warning"] = warning;
/* harmony export (immutable) */ __webpack_exports__["readNotification"] = readNotification;
/* harmony export (immutable) */ __webpack_exports__["readAll"] = readAll;
/** *   notificationHelpers   ***/

function error(message) {
  toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: 'toast-bottom-right',
    preventDuplicates: false,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '5000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'fadeIn',
    hideMethod: 'fadeOut'
  };
  toastr.error(message, dictionaryHelpers.getDictionary('Error'));
  if (typeof notificationData !== 'undefined') {
    notificationData.put('error', message);
    this.readNotification();
  }
}

function success(message, title) {
  toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: 'toast-bottom-right',
    preventDuplicates: false,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '5000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'fadeIn',
    hideMethod: 'fadeOut'
  };

  if (functionHelpers.isValued(title)) {
    toastr.success(message, title);
  } else {
    toastr.success(message);
  }

  notificationData.put('success', message);

  this.readNotification();
}

function warning(message) {
  toastr.options = {
    closeButton: false,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: 'toast-bottom-right',
    preventDuplicates: false,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '5000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'fadeIn',
    hideMethod: 'fadeOut'
  };
  toastr.warning(message, dictionaryHelpers.getDictionary('Warning'));
  notificationData.put('warning', message);
  this.readNotification();
}

function readNotification() {
  if (functionHelpers.isValued(notificationData.data())) {
    // Gestione notifiche non lette
    var notificationUnread = 0;
    $.each(notificationData.data(), function (i, item) {
      if (!item.read) {
        notificationUnread++;
      }
    });

    if (notificationUnread > 0) {
      $('#lblNumberNotification').text(notificationUnread);
    } else {
      $('#lblNumberNotification').text('');
    }

    $('#divNotification').html('');
    $('#divNotification').append('<div class="dropdown-header text-center"><strong>' + dictionaryHelpers.getDictionary('NotificationUnread').replace('{0}', notificationUnread) + '</strong></div>');

    var l = 10;

    if (notificationData.data().length < 10) {
      l = notificationData.data().length;
    }

    for (var i = 0; i < l; i++) {
      var notification = notificationData.data()[i];
      switch (notification.type) {
        case 'error':
          $('#divNotification').append('<a href="#" class="dropdown-item"><i class="fa fa-exclamation-triangle text-danger"></i> ' + notification.message + '</a>');
          break;
        case 'success':
          $('#divNotification').append('<a href="#" class="dropdown-item"><i class="fa fa-check text-success"></i> ' + notification.message + '</a>');
          break;
        case 'warning':
          $('#divNotification').append('<a href="#" class="dropdown-item"><i class="fa fa-exclamation-triangle text-warning"></i> ' + notification.message + '</a>');
          break;
        default:
          $('#divNotification').append('<a href="#" class="dropdown-item">' + notification.message + '</a>');
          break;
      }
    }
  }
}

function readAll() {
  notificationData.readAll();
}

/***/ }),
/* 24 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getUrl"] = getUrl;
/* harmony export (immutable) */ __webpack_exports__["isAdminPage"] = isAdminPage;
/* harmony export (immutable) */ __webpack_exports__["detail"] = detail;
/* harmony export (immutable) */ __webpack_exports__["detailFilter"] = detailFilter;
/* harmony export (immutable) */ __webpack_exports__["_addProperty"] = _addProperty;
/* harmony export (immutable) */ __webpack_exports__["renderInputType"] = renderInputType;
/* harmony export (immutable) */ __webpack_exports__["clearDetailValue"] = clearDetailValue;
/* harmony export (immutable) */ __webpack_exports__["setDetailValue"] = setDetailValue;
/* harmony export (immutable) */ __webpack_exports__["_setDetailValue"] = _setDetailValue;
/* harmony export (immutable) */ __webpack_exports__["getDetailValue"] = getDetailValue;
/* harmony export (immutable) */ __webpack_exports__["getDetailValueForFilter"] = getDetailValueForFilter;
/** *   pageHelpers   ***/

function getUrl(routeParameter) {
  if (!functionHelpers.isValued(routeParameter)) {
    return window.location.pathname;
  } else {
    var url = window.location.pathname;

    $.each(routeParameter, function (field, value) {
      url = url.replace('/' + value, '');
    });

    return url;
  }
}

function isAdminPage() {
  return window.location.pathname.indexOf('/admin') == 0;
}

function detail(selector, functions, configuration, jsonValue) {
  var idForm = 'frm' + functions;
  var html = '<form id="' + idForm + '" action="">';
  var lastRow = void 0;
  var lastColumn = void 0;
  var Colspan = void 0;
  var id = void 0;

  $.each(configuration, function (i, config) {
    id = config.inputType + '_' + functions + '_' + config.field.replace('.', '_');

    if (functionHelpers.isValued(config.posX) && functionHelpers.isValued(config.posY)) {
      // Creazione nuova riga
      if (lastRow != config.posY) {
        if (functionHelpers.isValued(lastRow)) {
          html += '</div>';
        }

        html += '<div class="form-group row">';
      }

      if (functionHelpers.isValued(config.colspan)) {
        Colspan = 3 * config.colspan;
      } else {
        Colspan = 3;
      }

      html += '<label class="col-sm-5 col-form-label" for="' + id + '">' + config.label + ':</label><div class="col-sm-6">';

      html += pageHelpers._renderInputType(id, config, jsonValue) + '</div>';

      lastRow = config.posY;
      lastColumn = config.posX;
      html += '</div>';
    } else if (config.inputType == 'hidden') {
      html += '<input type="hidden" id="' + id + '" name="' + id + '"';

      if (functionHelpers.isValued(jsonValue) && functionHelpers.isValued(jsonValue[config.field])) {
        html += ' value="' + functionHelpers.stringRemoveQuotes(jsonValue[config.field]) + '" ';
      }

      html += '>';
    }
  });

  html += '</div></form>';

  // Carico l'html generato
  $(html).appendTo(selector);

  validationHelpers.validateBase('#' + idForm);

  pageHelpers._addProperty(functions, configuration, jsonValue);
}

function detailFilter(selector, functions, configuration, jsonValue) {
  var idForm = 'frm' + functions;
  var html = '<form id="' + idForm + '" action="">';
  var lastRow = void 0;
  var lastColumn = void 0;
  var Colspan = void 0;
  var id = void 0;

  $.each(configuration, function (i, config) {
    id = config.inputType + '_' + functions + '_' + config.field.replace('.', '_');

    if (functionHelpers.isValued(config.posX) && functionHelpers.isValued(config.posY)) {
      // Creazione nuova riga
      if (lastRow != config.posY) {
        if (functionHelpers.isValued(lastRow)) {
          html += '</div>';
        }

        html += '<div class="row">';
      }

      if (functionHelpers.isValued(config.colspan)) {
        Colspan = 3 * config.colspan;
      } else {
        Colspan = 3;
      }

      html += '<div class="col-lg-' + Colspan + '"><label for="' + id + '">' + config.label + ':</label>';

      html += pageHelpers._renderInputType(id, config, jsonValue, true);

      lastRow = config.posY;
      lastColumn = config.posX;
      html += '</div>';
    } else if (config.inputType == 'hidden') {
      html += '<input type="hidden" id="' + id + '" name="' + id + '"';

      if (functionHelpers.isValued(jsonValue) && functionHelpers.isValued(jsonValue[config.field])) {
        html += ' value="' + functionHelpers.stringRemoveQuotes(jsonValue[config.field]) + '" ';
      }

      html += '>';
    }
  });

  html += '</div></form>';

  // Carico l'html generato
  $(html).appendTo(selector);

  validationHelpers.validateBase('#' + idForm);

  pageHelpers._addProperty(functions, configuration, jsonValue);
}

function _addProperty(functions, configuration, jsonValue) {
  $.each(configuration, function (i, config) {
    id = config.inputType + '_' + functions + '_' + config.field.replace('.', '_');

    switch (config.inputType) {
      case 'Select':
        if (functionHelpers.isValued(config.service)) {
          configurationHelpers.select(config.service, id, config.parameter);
        }

        if (functionHelpers.isValued(jsonValue) && functionHelpers.isValued(jsonValue[config.field])) {
          if ($('#' + id).find("option[value='" + jsonValue[config.field] + "']").length) {
            $('#' + id).val(jsonValue[config.field]).trigger('change');
          } else if (functionHelpers.isValued(jsonValue[config.field + 'Text'])) {
            $('#' + id).append(new Option(jsonValue[config.field + 'Text'], jsonValue[config.field], true, true)).trigger('change');
          } else {
            $('#' + id).val(jsonValue[config.field]).trigger('change');
          }
        } else if (functionHelpers.isValued(config.defaultValue)) {
          if ($('#' + id).find("option[value='" + config.defaultValue.ID + "']").length) {
            $('#' + id).val(config.defaultValue.ID).trigger('change');
          } else {
            $('#' + id).append(new Option(config.defaultValue.text, config.defaultValue.ID, true, true)).trigger('change');
          }
        }

        if (functionHelpers.isValued(config.onChange)) {
          $('#' + id).change(function () {
            window[config.onChange.split('.')[0]][config.onChange.split('.')[1]]();
          });
        }
        break;

      case 'DatePicker':
        if (functionHelpers.isValued(jsonValue) && functionHelpers.isValued(jsonValue[config.field])) {
          $('#' + id).datepicker('setDate', functionHelpers.dateFormatter(jsonValue[config.field]));
        }
        break;

      case 'EmailText':
        $('#' + id).rules('add', {
          email: true
        });
        break;

      case 'Numeric':
        $('#' + id).rules('add', {
          number: true
        });
        break;

      case 'Digits':
        $('#' + id).rules('add', {
          digits: true
        });
        break;
    }

    if (functionHelpers.isValued(config.minLength)) {
      $('#' + id).rules('add', {
        minlength: config.minLength
      });
    }

    if (functionHelpers.isValued(config.maxLength)) {
      $('#' + id).rules('add', {
        maxlength: config.maxLength
      });
    }
  });

  $('.input-group.date').datepicker({
    todayBtn: 'linked',
    clearBtn: true,
    format: storageData.sDatePickerFormat(),
    orientation: 'bottom auto',
    calendarWeeks: true,
    autoclose: true
  });

  $('.form-control.dateTime').datetimepicker({
    showClear: true,
    format: storageData.sDateTimePickerFormat()
  });
  $('.input-group-append.dateTime').on('click', function () {
    $(this).prev('.form-control.dateTime').data('DateTimePicker').toggle();
  });
}

function renderInputType(id, config, jsonValue, isFilter) {
  var htmlInpuType = '';

  switch (config.inputType) {
    case 'Input':
    case 'Numeric':
    case 'PhoneText':
    case 'EmailText':
      htmlInpuType += '<input type="text" id="' + id + '" name="' + id + '" class="form-control';
      if (functionHelpers.isValued(config.class)) {
        htmlInpuType += ' ' + config.class + ' ';
      }
      htmlInpuType += '"';
      if (functionHelpers.isValued(config.required) && config.required) {
        htmlInpuType += ' required ';
      }
      if (functionHelpers.isValued(config.disabled) && config.disabled) {
        htmlInpuType += ' disabled ';
      }
      if (functionHelpers.isValued(config.onChange)) {
        htmlInpuType += ' onchange="' + config.onChange + '(this)" ';
      }
      if (functionHelpers.isValued(jsonValue) && functionHelpers.isValued(jsonValue[config.field])) {
        htmlInpuType += ' value="' + functionHelpers.stringRemoveQuotes(jsonValue[config.field]) + '" ';
      } else if (functionHelpers.isValued(config.defaultValue)) {
        htmlInpuType += ' value="' + config.defaultValue + '" ';
      }

      htmlInpuType += '>';
      break;

    case 'TextArea':
      htmlInpuType += '<textarea id="' + id + '" name="' + id + '" rows="4" cols="50" class="form-control';
      if (functionHelpers.isValued(config.class)) {
        htmlInpuType += ' ' + config.class + ' ';
      }
      htmlInpuType += '"';
      if (functionHelpers.isValued(config.required) && config.required) {
        htmlInpuType += ' required ';
      }
      if (functionHelpers.isValued(config.disabled) && config.disabled) {
        htmlInpuType += ' disabled ';
      }
      htmlInpuType += '>';
      if (functionHelpers.isValued(jsonValue) && functionHelpers.isValued(jsonValue[config.field])) {
        htmlInpuType += functionHelpers.stringRemoveQuotes(jsonValue[config.field]);
      }
      htmlInpuType += '</textarea>';
      break;

    case 'Select':
      htmlInpuType += '<select id="' + id + '" name="' + id + '" class="form-control';
      if (functionHelpers.isValued(config.class)) {
        htmlInpuType += ' ' + config.class + ' ';
      }
      htmlInpuType += '" style="width:100%"';
      if (functionHelpers.isValued(config.required) && config.required) {
        htmlInpuType += ' required data-rule-minlength="1" ';
      }
      if (functionHelpers.isValued(config.disabled) && config.disabled) {
        htmlInpuType += ' disabled ';
      }
      htmlInpuType += '></select>';
      break;

    case 'DatePicker':
    case 'DateTimePicker':
      htmlInpuType += '<div class="input-group';
      if (config.inputType == 'DatePicker') {
        htmlInpuType += ' date';
      }
      htmlInpuType += '">';
      htmlInpuType += '<input type="text" class="form-control';
      if (config.inputType == 'DateTimePicker') {
        htmlInpuType += ' dateTime';
      }
      if (functionHelpers.isValued(config.class)) {
        htmlInpuType += ' ' + config.class + ' ';
      }
      htmlInpuType += '" id="' + id + '" name="' + id + '"';
      if (functionHelpers.isValued(config.disabled) && config.disabled) {
        htmlInpuType += ' disabled ';
      }
      htmlInpuType += '>';
      htmlInpuType += '<span class="input-group-append';
      if (config.inputType == 'DateTimePicker') {
        htmlInpuType += ' dateTime';
      }
      htmlInpuType += '">';
      htmlInpuType += '<span class="input-group-text input-group-addon"><i class="fa fa-calendar glyphicon glyphicon-th"></i></span>';
      htmlInpuType += '</span>';
      htmlInpuType += '</div>';
      break;

    case 'CheckBox':
      htmlInpuType += '<div class="input-group';
      if (functionHelpers.isValued(isFilter) && isFilter) {
        htmlInpuType += ' text-right';
      }
      htmlInpuType += '" style="display: block !important;">';
      htmlInpuType += '<label class="switch switch-pill switch-primary">';

      htmlInpuType += '<input type="checkbox" id="' + id + '" name="' + id + '" class="switch-input';
      if (functionHelpers.isValued(config.class)) {
        htmlInpuType += ' ' + config.class + ' ';
      }
      htmlInpuType += '" ';

      if (!functionHelpers.isValued(jsonValue)) {
        if (!functionHelpers.isValued(config.defaultValue) || functionHelpers.isValued(config.defaultValue) && config.defaultValue == '1') {
          htmlInpuType += ' checked="" ';
        }
      } else if (functionHelpers.isValued(jsonValue[config.field]) && jsonValue[config.field]) {
        htmlInpuType += ' checked="" ';
      }

      if (functionHelpers.isValued(config.required) && config.required) {
        htmlInpuType += ' required ';
      }

      if (functionHelpers.isValued(config.disabled) && config.disabled) {
        htmlInpuType += ' disabled ';
      }

      htmlInpuType += '>';

      if (functionHelpers.isValued(config.checkValue)) {
        htmlInpuType += '<span class="switch-slider" data-on="' + config.checkValue[0] + '" data-off="' + config.checkValue[1] + '"></span>';
      } else {
        htmlInpuType += '<span class="switch-slider" data-on="" data-off=""></span>';
      }

      htmlInpuType += '</label>';
      htmlInpuType += '</div>';

      break;

    case 'Radio':
      htmlInpuType += '<div class="col-form-label">';
      var isFirst = true;

      $.each(enumeratorHelpers.getValueFromConfiguration(config.radioEnumerator), function (i, value) {
        htmlInpuType += '<div class="form-check form-check-inline mr-1">';
        htmlInpuType += '<input class="form-check-input" id="' + id + '" type="radio" value="' + value.id + '" name="inline-radios_' + id + '"';
        if (isFirst) {
          htmlInpuType += ' checked ';
          isFirst = false;
        }
        htmlInpuType += '>';
        htmlInpuType += '<label class="form-check-label" for="' + id + '">' + value.text + '</label>';
        htmlInpuType += '</div>';
      });

      htmlInpuType += '</div>';
      break;

    case 'PhoneText':
      htmlInpuType += '<div class="input-group">';
      htmlInpuType += '<input type="text" id="' + id + '" name="' + id + '" class="form-control';
      if (functionHelpers.isValued(config.class)) {
        htmlInpuType += ' ' + config.class + ' ';
      }
      htmlInpuType += '"';
      if (functionHelpers.isValued(config.required) && config.required) {
        htmlInpuType += ' required ';
      }
      if (functionHelpers.isValued(config.disabled) && config.disabled) {
        htmlInpuType += ' disabled ';
      }
      if (functionHelpers.isValued(jsonValue[config.field])) {
        htmlInpuType += ' value="' + functionHelpers.stringRemoveQuotes(jsonValue[config.field]) + '" ';
      }
      htmlInpuType += '>';
      htmlInpuType += '<div class="input-group-append ">';
      htmlInpuType += '<span class="input-group-text">';
      htmlInpuType += '<i class="fa fa-phone"></i>';
      htmlInpuType += '</span>';
      htmlInpuType += '</div>';
      htmlInpuType += '</div>';
      break;

    case 'FaxText':
      htmlInpuType += '<div class="input-group">';
      htmlInpuType += '<input type="text" id="' + id + '" name="' + id + '" class="form-control';
      if (functionHelpers.isValued(config.class)) {
        htmlInpuType += ' ' + config.class + ' ';
      }
      htmlInpuType += '"';
      if (functionHelpers.isValued(config.required) && config.required) {
        htmlInpuType += ' required ';
      }
      if (functionHelpers.isValued(config.disabled) && config.disabled) {
        htmlInpuType += ' disabled ';
      }
      if (functionHelpers.isValued(jsonValue[config.field])) {
        htmlInpuType += ' value="' + functionHelpers.stringRemoveQuotes(jsonValue[config.field]) + '" ';
      }
      htmlInpuType += '>';
      htmlInpuType += '<div class="input-group-append ">';
      htmlInpuType += '<span class="input-group-text">';
      htmlInpuType += '<i class="fa fa-fax"></i>';
      htmlInpuType += '</span>';
      htmlInpuType += '</div>';
      htmlInpuType += '</div>';
      break;

    case 'EmailText':
      htmlInpuType += '<div class="input-group">';
      hthtmlInpuTypeml += '<input type="email" id="' + id + '" name="' + id + '" class="form-control';
      if (functionHelpers.isValued(config.class)) {
        htmlInpuType += ' ' + config.class + ' ';
      }
      htmlInpuType += '"';
      if (functionHelpers.isValued(config.required) && config.required) {
        htmlInpuType += ' required ';
      }
      if (functionHelpers.isValued(config.disabled) && config.disabled) {
        htmlInpuType += ' disabled ';
      }
      if (functionHelpers.isValued(jsonValue[config.field])) {
        htmlInpuType += ' value="' + functionHelpers.stringRemoveQuotes(jsonValue[config.field]) + '" ';
      }
      htmlInpuType += '>';
      htmlInpuType += '<div class="input-group-append ">';
      htmlInpuType += '<span class="input-group-text">';
      htmlInpuType += '<i class="fa fa-envelope"></i>';
      htmlInpuType += '</span>';
      htmlInpuType += '</div>';
      htmlInpuType += '</div>';
      break;
  }

  return htmlInpuType;
}

function clearDetailValue(functions, configuration) {
  $.each(configuration, function (i, config) {
    id = config.inputType + '_' + functions + '_' + config.field.replace('.', '_');

    switch (config.inputType) {
      case 'Select':
        $('#' + id).val(null).trigger('change');

        if (functionHelpers.isValued(config.defaultValue)) {
          if ($('#' + id).find("option[value='" + config.defaultValue.ID + "']").length) {
            $('#' + id).val(config.defaultValue.ID).trigger('change');
          } else {
            $('#' + id).append(new Option(config.defaultValue.text, config.defaultValue.ID, true, true)).trigger('change');
          }
        }
        break;
      case 'CheckBox':
        $('#' + id).prop('checked', true);
        break;
      default:
        $('#' + id).val(null);
        break;
    }
  });
}

function setDetailValue(functions, configuration, jsonValue, innerJsonField, innerJsonFieldKey, innerJsonValue) {
  var id = void 0;

  $.each(configuration, function (i, config) {
    id = config.inputType + '_' + functions + '_' + config.field.replace('.', '_');

    // If have a inner json by another field
    if (functionHelpers.isValued(innerJsonField) && config.field.indexOf(innerJsonField) == 0 && functionHelpers.isValued(innerJsonFieldKey)) {
      $.each(jsonValue[innerJsonField], function (j, jsonInnerValue) {
        if (jsonInnerValue[innerJsonFieldKey] == innerJsonValue) {
          pageHelpers._setDetailValue(id, config.inputType, jsonInnerValue[config.field.split('.')[1]]);
        }
      });
    } else {
      pageHelpers._setDetailValue(id, config.inputType, jsonValue[config.field]);
    }
  });
}

function _setDetailValue(id, inputType, value, valueText) {
  switch (inputType) {
    case 'DateTimePicker':
      if (functionHelpers.isValued(value)) {
        $('#' + id).data('DateTimePicker').date(functionHelpers.dateTimeFormatter(value));
      } else {
        $('#' + id).val('');
      }
      break;
    case 'Select':
      if (functionHelpers.isValued(valueText)) {
        $('#' + id).append(new Option(valueText, value, true, true)).trigger('change');
      } else {
        $('#' + id).val(value).trigger('change');
      }
      break;
    case 'CheckBox':
      $('#' + id).prop('checked', value);
      break;
    default:
      $('#' + id).val(value);
      break;
  }
}

function getDetailValue(functions, configuration, innerJsonField) {
  var jsonValue = {};
  var id = void 0;
  // If have a inner json by another field
  if (functionHelpers.isValued(innerJsonField)) {
    jsonValue[innerJsonField] = [{}];
  }

  $.each(configuration, function (i, config) {
    id = config.inputType + '_' + functions + '_' + config.field.replace('.', '_');

    switch (config.inputType) {
      case 'DatePicker':
      case 'DateTimePicker':
        // If have a inner json by another field
        if (functionHelpers.isValued(innerJsonField) && config.field.indexOf(innerJsonField) == 0) {
          jsonValue[innerJsonField][0][config.field.split('.')[1]] = functionHelpers.dateToJsonFormatter($('#' + id).val());
        } else {
          jsonValue[config.field] = functionHelpers.dateToJsonFormatter($('#' + id).val());
        }
        break;
      case 'Radio':
        // If have a inner json by another field
        if (functionHelpers.isValued(innerJsonField) && config.field.indexOf(innerJsonField) == 0) {
          jsonValue[innerJsonField][0][config.field.split('.')[1]] = $('#' + id + ':checked').val();
        } else {
          jsonValue[config.field] = $('#' + id + ':checked').val();
        }
        break;
      case 'CheckBox':
        // If have a inner json by another field
        if (functionHelpers.isValued(innerJsonField) && config.field.indexOf(innerJsonField) == 0) {
          jsonValue[innerJsonField][0][config.field.split('.')[1]] = $('#' + id + ':checked').length;
        } else {
          jsonValue[config.field] = $('#' + id + ':checked').length;
        }
        break;
      default:
        // If have a inner json by another field
        if (functionHelpers.isValued(innerJsonField) && config.field.indexOf(innerJsonField) == 0) {
          jsonValue[innerJsonField][0][config.field.split('.')[1]] = $('#' + id).val();
        } else {
          jsonValue[config.field] = $('#' + id).val();
        }
        break;
    }
  });

  return jsonValue;
}

function getDetailValueForFilter(functions, configuration) {
  var jsonValue = {};

  $.each(configuration, function (i, config) {
    id = config.inputType + '_' + functions + '_' + config.field.replace('.', '_');

    switch (config.inputType) {
      case 'Select':
        jsonValue[config.field] = $('#' + id).val();
        jsonValue[config.field + 'Text'] = $('#' + id).text();
        break;
      case 'Radio':
        jsonValue[config.field] = $('#' + id + ':checked').val();
        break;
      case 'CheckBox':
        jsonValue[config.field] = $('#' + id + ':checked').length;
        break;
      default:
        jsonValue[config.field] = $('#' + id).val();
        break;
    }
  });

  return jsonValue;
}

/***/ }),
/* 25 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAutenticate"] = getAutenticate;
/* harmony export (immutable) */ __webpack_exports__["getAutenticateWithData"] = getAutenticateWithData;
/* harmony export (immutable) */ __webpack_exports__["put"] = put;
/* harmony export (immutable) */ __webpack_exports__["putAutenticate"] = putAutenticate;
/* harmony export (immutable) */ __webpack_exports__["putAutenticateWithoutData"] = putAutenticateWithoutData;
/* harmony export (immutable) */ __webpack_exports__["post"] = post;
/* harmony export (immutable) */ __webpack_exports__["postWithoutData"] = postWithoutData;
/* harmony export (immutable) */ __webpack_exports__["postNoJson"] = postNoJson;
/* harmony export (immutable) */ __webpack_exports__["postAutenticate"] = postAutenticate;
/* harmony export (immutable) */ __webpack_exports__["postAutenticateWithoutData"] = postAutenticateWithoutData;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["delAutenticate"] = delAutenticate;
/** *   serviceHelpers   ***/

/** *   GET   ***/

function get(url, fnSuccess, fnError) {
  $.ajax({
    type: 'GET',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    contentType: 'application/json'
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

function getAutenticate(url, fnSuccess, fnError) {
  $.ajax({
    type: 'GET',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    // beforeSend: function (xhr) {
    //   xhr.setRequestHeader("Authorization", storageData.sTokenKey());
    // },
    headers: {
      Authorization: storageData.sTokenKey()
    },
    contentType: 'application/json'
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

function getAutenticateWithData(url, data, fnSuccess, fnError) {
  $.ajax({
    type: 'GET',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    // beforeSend: function (xhr) {
    //   xhr.setRequestHeader("Authorization", storageData.sTokenKey());
    // },
    headers: {
      Authorization: storageData.sTokenKey()
    },
    contentType: 'application/json',
    data: data
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

/** *   END GET   ***/

/** *   PUT   ***/

function put(url, data, fnSuccess, fnError) {
  $.ajax({
    type: 'PUT',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    contentType: 'application/json',
    data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

function putAutenticate(url, data, fnSuccess, fnError) {
  $.ajax({
    type: 'PUT',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    // beforeSend: function (xhr) {
    //   xhr.setRequestHeader("Authorization", storageData.sTokenKey());
    // },
    headers: {
      Authorization: storageData.sTokenKey()
    },
    contentType: 'application/json',
    data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

function putAutenticateWithoutData(url, fnSuccess, fnError) {
  $.ajax({
    type: 'PUT',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    // beforeSend: function (xhr) {
    //   xhr.setRequestHeader("Authorization", storageData.sTokenKey());
    // },
    headers: {
      Authorization: storageData.sTokenKey()
    },
    contentType: 'application/json'
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

/** *   END PUT   ***/

/** *   POST   ***/

function post(url, data, fnSuccess, fnError) {
  $.ajax({
    type: 'POST',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    contentType: 'application/json',
    data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

function postWithoutData(url, fnSuccess, fnError) {
  $.ajax({
    type: 'POST',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    contentType: 'application/json'
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

function postNoJson(url, data, fnSuccess, fnError) {
  $.ajax({
    type: 'POST',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    // headers: { "Accept": "application/json" },
    // contentType: 'application/json',
    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    data: data
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

function postAutenticate(url, data, fnSuccess, fnError) {
  $.ajax({
    type: 'POST',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    // beforeSend: function (xhr) {
    //   xhr.setRequestHeader("Authorization", storageData.sTokenKey());
    // },
    headers: {
      Authorization: storageData.sTokenKey()
    },
    contentType: 'application/json',
    data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

function postAutenticateWithoutData(url, fnSuccess, fnError) {
  $.ajax({
    type: 'POST',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    // beforeSend: function (xhr) {
    //   xhr.setRequestHeader("Authorization", storageData.sTokenKey());
    // },
    headers: {
      Authorization: storageData.sTokenKey()
    },
    contentType: 'application/json'
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(url, fnSuccess, fnError) {
  $.ajax({
    type: 'DELETE',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    contentType: 'application/json'
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

function delAutenticate(url, fnSuccess, fnError) {
  $.ajax({
    type: 'DELETE',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    // beforeSend: function (xhr) {
    //   xhr.setRequestHeader("Authorization", storageData.sTokenKey());
    // },
    headers: {
      Authorization: storageData.sTokenKey()
    },
    contentType: 'application/json'
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

/** *   END GET   ***/

/***/ }),
/* 26 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["search"] = search;
/* harmony export (immutable) */ __webpack_exports__["searchAutocomplete"] = searchAutocomplete;
/* harmony export (immutable) */ __webpack_exports__["searchImplemented"] = searchImplemented;
/* harmony export (immutable) */ __webpack_exports__["searchGeneral"] = searchGeneral;
/** *   searchHelpers   ***/

/** *   POST   ***/

function search(pageActive) {
  spinnerHelpers.show();
  var page = 1;
  if (pageActive != '' || pageActive != undefined) {
    page = pageActive;
  }
  var data = {
    search: $('#frmSearch #search').val(),
    idLanguage: storageData.sIdLanguage(),
    autocomplete: false,
    paginator: page
  };

  var html = '';

  searchServices.search(data, function (response) {
    var htmlPaginator = '';
    var minus10Page = 1;
    var plus10Page = 1;
    var minus1Page = 1;
    var plus1Page = 1;
    var pageSelected = parseInt(response.data.pageSelected);
    var maxPage = response.data.totalPages;
    window.history.pushState('ricerca', 'Ricerca', '/ricerca?r=' + $('#frmSearch #search').val() + '&p=' + page);

    html += "<div class='container div-cont-search'>";
    html += "<div class='row'>";
    html += "<div class='col-12'>";
    html += "<h1 class='div-title-search'>Ricerca</h1>";
    html += '</div>';
    html += '</div>';
    html += '<div class="row no-gutters div-paginator">';
    html += '<div class="col-12">';
    html += '<hr>';
    html += '</div>';
    html += '<div class="col-md-6 col-sm-12">';
    html += '<h5>Articoli presenti: <b class="totItems">&nbsp;</b></h5>';
    html += '</div>';
    html += '<div class="col-md-6 col-sm-12">';
    html += '<div class="paginationCont" style="float:right;">';
    html += '<br>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += "<div class='row'>";
    html += response.data.html;
    html += '</div>';
    html += '</div>';

    $('.mainPages').html(html);

    /* funzione gestione paginatore */
    if (response.data.paginationActive) {
      htmlPaginator += "<ul class='pagination'>";
      /* controllo freccia -10 pagine */
      if (pageSelected - 10 <= 1) {
        minus10Page = 1;
      } else {
        minus10Page = pageSelected - 10;
      }

      htmlPaginator += '<li class="page-item" data-page="' + minus10Page + '"><a><i class="fa fa-angle-double-left"></i></a></li>';
      /* end controllo freccia -10 pagine */
      /* controllo freccia -1 pagine */
      if (pageSelected - 1 <= 1) {
        minus1Page = 1;
      } else {
        minus1Page = pageSelected - 1;
      }

      htmlPaginator += '<li class="page-item" data-page="' + minus1Page + '"><a><i class="fa fa-angle-left"></i></a></li>';
      /* end controllo freccia -1 pagine */

      /* controllo per pagine intermedie */
      if (maxPage > 4) {
        if (pageSelected > 1) {
          if (pageSelected + 2 > maxPage) {
            if (pageSelected == maxPage) {
              for (var j = pageSelected - 3; j <= maxPage; j++) {
                if (pageSelected == j) {
                  htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
                } else {
                  htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
                }
              }
            } else {
              for (var j = pageSelected - 2; j <= maxPage; j++) {
                if (pageSelected == j) {
                  htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
                } else {
                  htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
                }
              }
            }
          } else {
            for (var j = pageSelected - 1; j <= pageSelected + 2; j++) {
              if (pageSelected == j) {
                htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
              } else {
                htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
              }
            }
          }
        } else {
          for (var j = 1; j <= 4; j++) {
            if (pageSelected == j) {
              htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
            } else {
              htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
            }
          }
        }
      } else {
        for (var j = 1; j <= maxPage; j++) {
          if (pageSelected == j) {
            htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
          } else {
            htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
          }
        }
      }
      /* end controllo per pagine intermedie */

      /* controllo freccia + 1 pagine */
      if (pageSelected + 1 >= maxPage) {
        plus1Page = maxPage;
      } else {
        plus1Page = pageSelected + 1;
      }

      htmlPaginator += '<li class="page-item" data-page="' + plus1Page + '"><a><i class="fa fa-angle-right"></i></a></li>';
      /* end controllo freccia + 1 pagine */
      /* controllo freccia + 10 pagine */
      if (pageSelected + 10 >= maxPage) {
        plus10Page = maxPage;
      } else {
        plus10Page = pageSelected + 10;
      }

      htmlPaginator += '<li class="page-item" data-page="' + plus10Page + '"><a><i class="fa fa-angle-double-right"></i></a></li>';
      /* end controllo freccia + 10 pagine */

      htmlPaginator += '</ul>';
      $('.paginationCont').html(htmlPaginator);
    } else {
      $('.paginationCont').html('');
    }
    $('.totItems').html(response.data.totItems);
    /* end funzione gestione paginatore */
    $('.pagination li').on('click', function () {
      searchHelpers.search($(this).attr('data-page'));
    });

    spinnerHelpers.hide();
    $('#searchAutocomplete').hide('fast');
  });
}

function searchAutocomplete() {
  var data = {
    search: $('#frmSearch #search').val(),
    idLanguage: storageData.sIdLanguage(),
    autocomplete: true
  };

  searchServices.search(data, function (response) {
    $('#searchAutocomplete').html("<i class='fa fa-times icon-close-search'></i><div class='row'>" + response.data.html + '</div>');
    $('#searchAutocomplete').show();
    $('#searchAutocomplete .icon-close-search').on('click', function () {
      $('#searchAutocomplete').hide();
    });
  });
}

function searchImplemented(idForm) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(idForm);
  data.idLanguage = storageData.sIdLanguage();
  var html = '';
  searchServices.searchImplemented(data, function (response) {
    // window.history.pushState('ricerca', 'Ricerca', '/ricerca-avanzata?r=' + $('#frmSearch #search').val());
    html += "<div class='container div-cont-search'>";
    html += "<div class='row'>";
    html += response.message;
    html += '</div>';
    html += '</div>';

    $('#result').html(html);
    spinnerHelpers.hide();
  });
}

function searchGeneral(data) {

  searchServices.searchGeneral(data, function (response) {
    console.log(response);
    $('#searchAutocomplete').html("<i class='fa fa-times icon-close-search'></i><div class='row'>" + response.message + "</div>");
    $('#searchAutocomplete').show();
    $('#searchAutocomplete .icon-close-search').on('click', function () {
      $('#searchAutocomplete').hide();
    });
  });
}

/** *   END POST   ***/

/***/ }),
/* 27 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["show"] = show;
/* harmony export (immutable) */ __webpack_exports__["hide"] = hide;
/** *   spinnerHelpers   ***/

function show() {
  $('.modal-backdrop').not('.fade').css('z-index', '9000');
  window.$('#spinnerView').modal('show');
}

function hide() {
  window.$('#spinnerView').modal('hide');
}

/***/ }),
/* 28 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["addInput"] = addInput;
/* harmony export (immutable) */ __webpack_exports__["onChangeEvent"] = onChangeEvent;
/* harmony export (immutable) */ __webpack_exports__["onChange"] = onChange;
/* harmony export (immutable) */ __webpack_exports__["onChangeCheckNumber"] = onChangeCheckNumber;
/** *   tableHelpers   ***/

function addInput(tableSelector, configurations, isDisabled) {
  $.each(configurations, function (i, configuration) {
    if (functionHelpers.isValued(configuration.visibleOnTable) && configuration.visibleOnTable) {
      if (functionHelpers.isValued(configuration.inputType) && functionHelpers.isValued(configuration.field)) {
        var editable = {
          mode: 'inline',
          emptytext: '-'
        };

        if (functionHelpers.isValued(isDisabled) && isDisabled) {
          editable.disabled = true;
        }

        switch (configuration.inputType) {
          case 'DatePicker':
            editable.type = 'date';
            editable.format = storageData.sDatePickerFormat();
            break;
          case 'Input':
          case 'EmailText':
            editable.type = 'text';
            break;
          case 'Password':
            editable.type = 'password';
            break;
          case 'Label':
            editable.disabled = true;
            break;
          case 'Select':
            editable.type = 'select';
            break;
        }

        if (functionHelpers.isValued(configuration.onChange)) {
          editable.onSave = configuration.onChange;
        } else {
          switch (configuration.inputType) {
            case 'Numeric':
              editable.onSave = 'tableHelpers.onChangeCheckNumber';
              break;
          }
        }

        editable.validate = function (value) {
          if (functionHelpers.isValued(configuration.required) && configuration.required && $.trim(value) == '') {
            return dictionaryHelpers.getDictionary('ThisFieldIsRequired');
          }

          if (functionHelpers.isValued(configuration.digits) && configuration.digits && !$.isNumeric(value) && functionHelpers.isValued(value)) {
            return dictionaryHelpers.getDictionary('OnlyNumbersAreAllowed');
          }

          if (functionHelpers.isValued(configuration.maxLength) && value.length > $(this).attr('data-maxlength')) {
            return dictionaryHelpers.getDictionary('MaxLenght').replace('{0}', $(this).attr('data-maxlength'));
          }

          if (configuration.inputType == 'EmailText' && !functionHelpers.validateEmail(value)) {
            return dictionaryHelpers.getDictionary('EmailNotValid');
          }

          // if (functionHelpers.isValued(configuration.onChange)) {
          //   var field = $(this).attr('field');
          //   var value = $(this).closest('td').find('.form-control').val();
          //   var rowIndex = $(this).data('rowindex');
          //   var id = $(this)[0].id;
          //   var rowId = id.substr(field.length + 4);
          //   return window[configuration.onChange.split('.')[0]][configuration.onChange.split('.')[1]](field,value,rowIndex,id,rowId);
          // }
        };

        $.each($(tableSelector).find('a[id^="ti_' + configuration.field + '_"]'), function (i, input) {
          if (functionHelpers.isValued(configuration.maxLength)) {
            $(tableSelector).find('#' + this.id).attr('data-maxlength', configuration.maxLength);
          }

          if (editable.type == 'select') {
            switch (configuration.service) {
              case 'languages':
                editable.select2 = languageHelpers.getSelect2ForBootstrapTable();
                break;

              // case 'templateEditorType':
              //   editable.select2 = templateEditorTypeServices.getSelect2ForBootstrapTable();
              //   break;

              default:
                editable.select2 = window[configuration.service + 'Services'].getSelect2ForBootstrapTable(configuration.dropDownParam);
                break;
            }

            editable.select2.width = 200;
            editable.tpl = '<select style="width:150px;">';

            editable.success = function (response, newValue) {
              var editable = $(this).data('editable');
              var option = editable.input.$input.find('option[value="VALUE"]'.replace('VALUE', newValue));
              var newText = option.text();
              $(this).attr('data-text', newText);
              $(this).attr('data-value', newValue);
            };

            $(tableSelector).find('#' + this.id).editable(editable);
          } else {
            $(tableSelector).find('#' + this.id).editable(editable);
          }

          $(tableSelector).find('#' + this.id).on('save', function (e, params) {
            if (!$(tableSelector).find('#' + this.id).hasClass('error')) {
              if ($(tableSelector).find('.bs-checkbox').length > 0) {
                $(tableSelector).bootstrapTable('check', $(this).attr('data-rowindex'));
              }
            }

            if (functionHelpers.isValued(configuration.onSelect2Save)) {
              eval(configuration.onSelect2Save)(this, e, params);
            }
          });
        });
      }
    }
  });

  // $(tableSelector).bootstrapTable('resetView');
  spinnerHelpers.hide();
}

/** *   ONCHANGE   ***/

function onChangeEvent(el, functionToCall) {
  var field = $('#' + $(el).closest('td').find('a')[0].id).attr('field');
  var value = $(el).closest('form').find('.form-control').val();
  var rowIndex = $(el).closest('td').find('a').data('rowindex');
  var id = $(el).closest('td').find('a')[0].id;
  var rowId = id.substr(field.length + 4);
  functionToCall(field, value, rowIndex, id, rowId);
}

function onChange(index, value, id, field) {
  switch (pageHelpers.getUrl()) {
    case '/admin/managementTechinicalSpecification':
      technicalSpecificationHelpers.onChangeTable(index, value, id, field);
      break;
    case '/admin/groupsTechinicalSpecification':
      groupTechnicalSpecificationHelpers.onChangeTable(index, value, id, field);
      break;
    case '/admin/groupsDisplayTechinicalSpecification':
      groupDisplayTechnicalSpecificationHelpers.onChangeTable(index, value, id, field);
      break;
  }
}

function onChangeCheckNumber(index, value, id, field) {
  var idTable = $('#' + id).closest('table')[0].id;
  if (functionHelpers.isValued(value)) {
    if (functionHelpers.isNumeric(value)) {
      if ($('#' + id).hasClass('error')) {
        $('#' + id).removeClass('error');
      }
    } else {
      if (!$('#' + id).hasClass('error')) {
        $('#' + id).addClass('error');
      }
      $('#' + idTable).bootstrapTable('uncheck', index);
      notificationHelpers.error(dictionaryHelpers.getDictionary('PleaseEnterAValidNumber'));
    }
  } else if ($('#' + id).hasClass('error')) {
    $('#' + id).removeClass('error');
  }
}

/** *   END ONCHANGE   ***/

/***/ }),
/* 29 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["onChangeTable"] = onChangeTable;
/** *   technicalSpecificationHelpers   ***/

function onChangeTable(index, value, id, field) {
  var idTable = $('#' + id).closest('table')[0].id;
  if (functionHelpers.isValued(value)) {
    technicalSpecificationServices.checkExists(value, field, $('#' + idTable).bootstrapTable('getData')[index].id, function (result) {
      if (result.data) {
        if (!$('#' + id).hasClass('error')) {
          $('#' + id).addClass('error');
        }
        $('#' + idTable).bootstrapTable('uncheck', index);
        notificationHelpers.error(dictionaryHelpers.getDictionary('TechnicalSpecificationAlreadyExists'));
      } else if ($('#' + id).hasClass('error')) {
        $('#' + id).removeClass('error');
      }
    });
  } else if (field == storageData.sIdLanguage()) {
    if (!$('#' + id).hasClass('error')) {
      $('#' + id).addClass('error');
    }
    $('#' + idTable).bootstrapTable('uncheck', index);
    notificationHelpers.error(dictionaryHelpers.getDictionary('ThisFieldIsRequired'));
  } else if ($('#' + id).hasClass('error')) {
    $('#' + id).removeClass('error');
  }
}

/***/ }),
/* 30 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["rescuePassword"] = rescuePassword;
/* harmony export (immutable) */ __webpack_exports__["changePassword"] = changePassword;
/* harmony export (immutable) */ __webpack_exports__["checkExistUsername"] = checkExistUsername;
/* harmony export (immutable) */ __webpack_exports__["checkExistEmail"] = checkExistEmail;
/* harmony export (immutable) */ __webpack_exports__["availableCheck"] = availableCheck;
/* harmony export (immutable) */ __webpack_exports__["insertNoAuth"] = insertNoAuth;
/* harmony export (immutable) */ __webpack_exports__["getAllUser"] = getAllUser;
/* harmony export (immutable) */ __webpack_exports__["getByIdResult"] = getByIdResult;
/* harmony export (immutable) */ __webpack_exports__["updateUser"] = updateUser;
/* harmony export (immutable) */ __webpack_exports__["insertUser"] = insertUser;
/* harmony export (immutable) */ __webpack_exports__["deleteRow"] = deleteRow;
/** *   userHelpers   ***/

function rescuePassword() {
  spinnerHelpers.show();
  var email = $('#inputEmail').val();
  var data = {
    email: email
  };
  if (functionHelpers.isValued(email)) {
    userServices.rescuePassword(data, function (result) {
      if (result.message == '') {
        notificationHelpers.error('Email non presente nel nostro sistema!');
      } else {
        notificationHelpers.success("Abbiamo inviato all'indirizzo email indicato il link per resettare la password.");
      }
      spinnerHelpers.hide();
    });
  }
}

function changePassword(id) {
  spinnerHelpers.show();
  var password = $('#password').val();
  var confirmPassword = $('#confirmPassword').val();
  var data = {
    password: password,
    confirmPassword: confirmPassword,
    id: id
  };
  if (password == confirmPassword) {
    userServices.changePasswordRescued(data, function (result) {
      if (result.message == '') {
        notificationHelpers.error('Attenzione la pagina non è più valida o la password è stata già resettata!');
      } else {
        notificationHelpers.success('Password cambiata correttamente!');
      }
      spinnerHelpers.hide();
    });
  } else {
    spinnerHelpers.hide();
    notificationHelpers.error('Le password non coincidono!');
  }
}

function checkExistUsername(field, value, rowIndex, id, rowId) {
  spinnerHelpers.show();

  if (functionHelpers.isValued(rowIndex)) {
    window.localStorage.setItem('userHelperId', id);
    if (value != 'admin') {
      userServices.checkExistUsernameNotForThisPage(rowId, value, function (result) {
        if (result.data) {
          validationHelpers.setTableError(window.localStorage.getItem('userHelperId'), dictionaryHelpers.getDictionary('UsernameAlreadyExist'));
        } else {
          $('#' + field).removeClass('error');
        }

        window.localStorage.setItem('userHelperId', '');
      });
    }
    spinnerHelpers.hide();
  } else {
    window.localStorage.setItem('userHelperId', field);
    if (value != 'admin') {
      userServices.checkExistUsername(value, function (result) {
        if (result.data) {
          validationHelpers.setDetailError(window.localStorage.getItem('userHelperId'), dictionaryHelpers.getDictionary('UsernameAlreadyExist'));
        } else {
          $('#' + field).removeClass('error');
        }

        window.localStorage.setItem('userHelperId', '');
      });
    }
    spinnerHelpers.hide();
  }
}

function checkExistEmail(field, value, rowIndex, id, rowId) {
  spinnerHelpers.show();

  if (functionHelpers.isValued(rowIndex)) {
    window.localStorage.setItem('userHelperId', id);
    userServices.checkExistEmailNotForThisPage(rowId, value, function (result) {
      if (result.data) {
        validationHelpers.setTableError(window.localStorage.getItem('userHelperId'), dictionaryHelpers.getDictionary('EmailAlreadyExist'));
      }

      window.localStorage.setItem('userHelperId', '');
      spinnerHelpers.hide();
    });
  } else {
    window.localStorage.setItem('userHelperId', field);
    userServices.checkExistEmail(value, function (result) {
      if (result.data) {
        validationHelpers.setDetailError(window.localStorage.getItem('userHelperId'), dictionaryHelpers.getDictionary('EmailAlreadyExist'));
      }

      window.localStorage.setItem('userHelperId', '');
      spinnerHelpers.hide();
    });
  }
}

function availableCheck(id) {
  // spinnerHelpers.show();
  userServices.availableCheck(id, function (result) {
    notificationHelpers.success('Registrazione completata con successo!');
  });
}

/* CREATE USER */
function insertNoAuth(idForm) {
  spinnerHelpers.show();

  var data = functionHelpers.formToJson(idForm);
  data["idLanguage"] = storageData.sIdLanguage();
  var arrayImgAgg = [];

  /* //format orari settimanali
  var html = "";
  html += '<table>'; 
  html += '<tbody>'; 
  html += '<tr>'; 
  html += '<th colspan="4"></th>';
  html += '<tr>'; 
  html += '<td>Lunedì</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#monday").val() + '</td>';
  html += '</tr>'; 
  html += '<tr>'; 
  html += '<td>Martedì</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'; 
  html += '<td>' + $("#tuesday").val() + '</td>';
  html += '</tr>'; 
  html += '<tr>'; 
  html += '<td>Mercoledì</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#wednesday").val() + '</td>';
  html += '</tr>';
  html += '<tr>'; 
  html += '<td>Giovedì</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#thursday").val() + '</td>';
  html += '</tr>';
  html += '<tr>'; 
  html += '<td>Venerdì</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#friday").val() + '</td>';
  html += '</tr>';
  html += '<tr>'; 
  html += '<td>Sabato</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#saturday").val() + '</td>';
  html += '</tr>';
  html += '<tr>'; 
  html += '<td>Domenica</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#sunday").val() + '</td>';
  html += '</tr>';
  html += '</tr>'; 
  html += '</tbody>'; 
  html += '</table>';  
    data.shop_time = html;
  //end orari settimanali */

  // ciclo img. aggiuntive
  for (var i = 0; i < 10; i++) {
    if (functionHelpers.isValued(data['imgBase64' + i]) && functionHelpers.isValued(data['imgName' + i])) {
      arrayImgAgg.push(JSON.parse('{"imgBase64' + i + '":"' + data['imgBase64' + i] + '", "imgName' + i + '":"' + data['imgName' + i] + '"}'));
    }
  }
  data.imgAgg = arrayImgAgg;
  // end ciclo img.aggiuntive

  // SEZIONE MAPS
  var arrayMapAgg = [];
  for (var i = 0; i < 5; i++) {
    var jsonMapAgg = {};
    var check = false;
    if (functionHelpers.isValued($('#user_id' + i).val())) {
      check = true;
      jsonMapAgg.user_id = $('#user_id' + i).val();
    }
    if (functionHelpers.isValued($('#maps' + i).val())) {
      check = true;
      jsonMapAgg.maps = $('#maps' + i).val();
    }
    if (functionHelpers.isValued($('#address' + i).val())) {
      check = true;
      jsonMapAgg.address = $('#address' + i).val();
    }
    if (functionHelpers.isValued($('#codepostal' + i).val())) {
      check = true;
      jsonMapAgg.codepostal = $('#codepostal' + i).val();
    }
    if (functionHelpers.isValued($('#country' + i).val())) {
      check = true;
      jsonMapAgg.country = $('#country' + i).val();
    }
    if (functionHelpers.isValued($('#province' + i).val())) {
      check = true;
      jsonMapAgg.province = $('#province' + i).val();
    }
    if (check) {
      arrayMapAgg.push(jsonMapAgg);
    }
  }
  data.MapAgg = arrayMapAgg;

  userServices.insertNoAuth(data, function (result) {
    notificationHelpers.success('Registrazione completata con successo!');
    setTimeout(function () {
      window.location = '/notification-of-receipt-of-user-confirmation-email';
    }, 2000);
    spinnerHelpers.hide();
  });
}
// END SEZIONE MAPS

/* END CREATE USER */

/* GET ALL USER */
function getAllUser() {
  spinnerHelpers.show();
  userServices.getAllUser(storageData.sIdLanguage(), function (response) {
    for (var i = 0; response.data.length; i++) {
      var newList = '';
      newList += '<response.data.leng<tr>';
      newList += '<td><button class="btn btn-info" onclick="userHelpers.getByIdResult(' + response.data[i].id + ')"><i class="fa fa-pencil"/></button></td>';
      newList += '<td>' + response.data[i].id + '</td>';
      newList += '<td>' + response.data[i].subscriber + '</td>';
      newList += '<td>' + response.data[i].expiration_date + '</td>';
      newList += '<td>' + response.data[i].subscription_date + '</td>';
      newList += '<td>' + response.data[i].business_name + '</td>';
      newList += '<td>' + response.data[i].name + '</td>';
      newList += '<td>' + response.data[i].telephone_number + '</td>';
      newList += '<td>' + response.data[i].mobile_number + '</td>';
      newList += '<td>' + response.data[i].fax_number + '</td>';
      newList += '<td>' + response.data[i].email + '</td>';

      // newList += "<td>" + response.data[i].username + "</td>";
      // newList += "<td>" + response.data[i].password + "</td>";
      // newList += "<td>" + response.data[i].language_id + "</td>";
      // newList += "<td>" + response.data[i].surname + "</td>";
      // newList += "<td>" + response.data[i].vat_number + "</td>";
      // newList += "<td>" + response.data[i].fiscal_code + "</td>";
      // newList += "<td>" + response.data[i].address + "</td>";
      // newList += "<td>" + response.data[i].codepostal + "</td>";
      // newList += "<td>" + response.data[i].country + "</td>";
      // newList += "<td>" + response.data[i].province + "</td>";
      // newList += "<td>" + response.data[i].website + "</td>";
      // newList += "<td>" + response.data[i].vat_type_id + "</td>";
      //  newList += "<td>" + response.data[i].pec + "</td>";
      newList += '<td><button class="btn btn-danger" onclick="userHelpers.deleteRow(' + response.data[i].id + ')"><i class="fa fa-trash"/></button></td>';
      newList += '</tr>';

      $('#table_list_user tbody').append(newList);
      $('#table_list_user').show();
      $('#frmAddReferent').hide();
      $('#table_list_searchuser').hide();
      $('#frmUpdateUser').hide();
      $('#frmUpdateReferent').hide();
      $('#table_list_people').hide();
      $('#nav-tab').hide();
      $('.title-modifica-utente').hide();
    }
  });
  spinnerHelpers.hide();
}

function getByIdResult(id) {
  // spinnerHelpers.show();
  userServices.getByIdResult(id, function (response) {
    $('#frmSearchUser').hide();
    $('#table_list_searchuser').hide();
    var searchParam = '';

    // select vattype
    var initials = [];
    initials.push({
      id: response.data.vat_type_id,
      text: response.data.descriptionVatType
    });
    $('#vat_type_id').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/vattype/select',
        data: function data(params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });
    // get all user
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        $('#frmUpdateUser #subscriber' + i).attr('checked', 'checked');
      } else {
        $('#frmUpdateUser #' + i).val(val);
      }
    });

    peopleHelpers.getPeopleUser(id);
    $('#frmAddReferent #user_id').val(id);
    $('#frmUpdateReferent #user_id').val(id);
    $('#frmUpdateUser').show();
    $('#frmAddReferent').hide();
    $('#nav-tab').show();
    $('#table_list_user').hide();
    $('#btnadd').hide();

    $('.title-modifica-utente').show();
    $('.title-modifica-cliente').hide();
    // $("#frmAddReferent").hide();
  });

  // peopleHelpers.getPeopleUser($("#id").val());
}

function updateUser() {
  spinnerHelpers.show();
  var data = {
    // id: storageData.sUserId(),
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmUpdateUser #id').val(),
    username: $('#frmUpdateUser #username').val(),
    email: $('#frmUpdateUser #email').val(),
    password: $('#frmUpdateUser #password').val(),
    business_name: $('#frmUpdateUser #business_name').val(),
    name: $('#frmUpdateUser #name').val(),
    surname: $('#frmUpdateUser #surname').val(),
    vat_number: $('#frmUpdateUser #vat_number').val(),
    fiscal_code: $('#frmUpdateUser #fiscal_code').val(),
    address: $('#frmUpdateUser #address').val(),
    codepostal: $('#frmUpdateUser #codepostal').val(),
    country: $('#frmUpdateUser #country').val(),
    province: $('#frmUpdateUser #province').val(),
    website: $('#frmUpdateUser #website').val(),
    vat_type_id: $('#frmUpdateUser #vat_type_id').val(),
    telephone_number: $('#frmUpdateUser #telephone_number').val(),
    fax_number: $('#frmUpdateUser #fax_number').val(),
    mobile_number: $('#frmUpdateUser #mobile_number').val(),
    pec: $('#frmUpdateUser #pec').val(),
    subscriber: $('#frmUpdateUser #subscriber').val(),
    subscription_date: $('#frmUpdateUser #subscription_date').val(),
    expiration_date: $('#frmUpdateUser #expiration_date').val()
  };

  userServices.updateUser(data, function () {
    notificationHelpers.success('Utente modificato con successo!');
    spinnerHelpers.hide();
    $('#table_list_user').show();
    $('#frmUpdateUser').hide();
    // window.location.reload();
  });
}

function insertUser() {
  spinnerHelpers.show();

  var data = {
    created_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmAddUser #id').val(),
    username: $('#frmAddUser #username').val(),
    email: $('#frmAddUser #email').val(),
    password: $('#frmAddUser #password').val(),
    business_name: $('#frmAddUser #business_name').val(),
    name: $('#frmAddUser #name').val(),
    surname: $('#frmAddUser #surname').val(),
    vat_number: $('#frmAddUser #vat_number').val(),
    fiscal_code: $('#frmAddUser #fiscal_code').val(),
    address: $('#frmAddUser #address').val(),
    codepostal: $('#frmAddUser #codepostal').val(),
    country: $('#frmAddUser #country').val(),
    province: $('#frmAddUser #province').val(),
    website: $('#frmAddUser #website').val(),
    vat_type_id: $('#frmAddUser #vat_type_id').val(),
    telephone_number: $('#frmAddUser #telephone_number').val(),
    fax_number: $('#frmAddUser #fax_number').val(),
    mobile_number: $('#frmAddUser #mobile_number').val(),
    pec: $('#frmAddUser #pec').val(),
    subscriber: $('#frmAddUser #subscriber').val(),
    subscription_date: $('#frmAddUser #subscription_date').val(),
    expiration_date: $('#frmAddUser #expiration_date').val()
  };

  userServices.insertUser(data, function (result) {
    $('#frmAddUser #id').val('');
    $('#frmAddUser #username').val('');
    $('#frmAddUser #email').val('');
    $('#frmAddUser #password').val('');
    $('#frmAddUser #business_name').val('');
    $('#frmAddUser #name').val('');
    $('#frmAddUser #surname').val('');
    $('#frmAddUser #vat_number').val('');
    $('#frmAddUser #fiscal_code').val('');
    $('#frmAddUser #address').val('');
    $('#frmAddUser #codepostal').val('');
    $('#frmAddUser #country').val('');
    $('#frmAddUser #province').val('');
    $('#frmAddUser #website').val('');
    $('#frmAddUser #vat_type_id').val('');
    $('#frmAddUser #telephone_number').val('');
    $('#frmAddUser #fax_number').val('');
    $('#frmAddUser #mobile_number').val('');
    $('#frmAddUser #pec').val('');
    $('#frmAddUser #subscriber').val('');
    $('#frmAddUser #subscription_date').val('');
    $('#frmAddUser #expiration_date').val('');
    notificationHelpers.success('Registrazione completata con successo!');
    spinnerHelpers.hide();
  });
}

function deleteRow(id) {
  spinnerHelpers.show();
  userServices.delet(id, function (response) {
    notificationHelpers.success(dictionaryHelpers.getDictionary('DeleteCompleted'));
    spinnerHelpers.hide();
    window.location.reload();
  });
}

/***/ }),
/* 31 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["validate"] = validate;
/* harmony export (immutable) */ __webpack_exports__["validateBase"] = validateBase;
/* harmony export (immutable) */ __webpack_exports__["setLanguage"] = setLanguage;
/* harmony export (immutable) */ __webpack_exports__["isValid"] = isValid;
/* harmony export (immutable) */ __webpack_exports__["clear"] = clear;
/* harmony export (immutable) */ __webpack_exports__["moveToInvalid"] = moveToInvalid;
/* harmony export (immutable) */ __webpack_exports__["setDetailError"] = setDetailError;
/* harmony export (immutable) */ __webpack_exports__["setTableError"] = setTableError;
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/** *   validationHelpers   ***/

function validate(formSelector, rulesValidation, fnSubmit) {
  validationHelpers.setLanguage();
  var _errorClass = 'is-invalid';
  window.$(formSelector).validate({
    rules: rulesValidation,
    errorClass: _errorClass,
    errorPlacement: function errorPlacement(error, element) {
      if (element.parent().is('td')) {
        // error.insertAfter(element);
        // $('<br />').insertAfter(element);
      } else if (element.parent().is('.input-group')) {
        error.insertAfter(element.parent());
      } else if (element.is('select')) {
        element.next().find('.select2-selection').addClass(_errorClass);
        error.insertAfter(element.next());
      } else {
        error.insertAfter(element);
      }
    },
    submitHandler: function submitHandler(form) {
      if (functionHelpers.isValued(fnSubmit) && $.isFunction(fnSubmit)) {
        fnSubmit();
      } else {
        form.submit();
      }
    }
  });

  $.each(JSON.parse(JSON.stringify(rulesValidation)), function (selector, value) {
    if ($('#' + selector).is('select')) {
      $('#' + selector).on('select2:close', function (e) {
        if ($(this).valid()) {
          $(this).next().find('.select2-selection').removeClass(_errorClass);
        }
      });
    }
  });
}

function validateBase(formSelector) {
  var _errorClass = 'is-invalid';
  validationHelpers.setLanguage();

  window.$(formSelector).validate({
    errorClass: _errorClass,
    errorPlacement: function errorPlacement(error, element) {
      if (element.parent().is('td')) {
        // error.insertAfter(element);
        // $('<br />').insertAfter(element);
      } else if (element.parent().is('.input-group')) {
        error.appendTo(element.parent().parent());
      } else if (element.is('select')) {
        element.next().find('.select2-selection').addClass(_errorClass);
        error.insertAfter(element.next());
      } else {
        error.insertAfter(element);
      }
    }
  });

  $.each($(formSelector + ' select'), function (value, selector) {
    $('#' + selector.id).on('select2:select', function (e) {
      if (window.$(this).valid()) {
        window.$(this).next().find('.select2-selection').removeClass(_errorClass);
      }
    });
  });
}

function setLanguage() {
  $.extend(window.$.validator.messages, {
    required: dictionaryHelpers.getDictionary('ThisFieldIsRequired'),
    remote: 'Please fix this field.',
    email: dictionaryHelpers.getDictionary('EmailNotValid'),
    url: 'Please enter a valid URL.',
    date: 'Please enter a valid date.',
    dateISO: 'Please enter a valid date (ISO).',
    number: dictionaryHelpers.getDictionary('PleaseEnterAValidNumber'),
    digits: 'Please enter only digits.',
    equalTo: 'Please enter the same value again.',
    maxlength: window.$.validator.format('Please enter no more than {0} characters.'),
    minlength: window.$.validator.format('Please enter at least {0} characters.'),
    rangelength: window.$.validator.format('Please enter a value between {0} and {1} characters long.'),
    range: window.$.validator.format('Please enter a value between {0} and {1}.'),
    max: window.$.validator.format('Please enter a value less than or equal to {0}.'),
    min: window.$.validator.format('Please enter a value greater than or equal to {0}.'),
    step: window.$.validator.format('Please enter a multiple of {0}.')
  });
}

function isValid(formSelector) {
  validationHelpers.setLanguage();

  if (window.$(formSelector).valid()) {
    return true;
  } else {
    notificationHelpers.error(dictionaryHelpers.getDictionary('RequiredFieldsNotCompleted'));
    validationHelpers.moveToInvalid();
    spinnerHelpers.hide();
    return false;
  }
}

function clear(formSelector) {
  var _errorClass = 'is-invalid';

  window.$(formSelector).validate({
    errorClass: _errorClass,
    errorPlacement: function errorPlacement(error, element) {
      if (element.parent().is('td')) {
        // error.insertAfter(element);
        // $('<br />').insertAfter(element);
      } else if (element.parent().is('.input-group')) {
        error.appendTo(element.parent().parent());
      } else if (element.is('select')) {
        element.next().find('.select2-selection').addClass(_errorClass);
        error.insertAfter(element.next());
      } else {
        error.insertAfter(element);
      }
    }
  }).resetForm();
}

function moveToInvalid() {
  if (functionHelpers.isValued($('.is-invalid').offset())) {
    $([document.documentElement, document.body]).animate({
      scrollTop: $('.is-invalid:visible').offset().top - 300
    }, 1000);
  }
}

function setDetailError(idField, error) {
  notificationHelpers.error(error);
  $('#' + idField).addClass('error');
  window.$('#' + idField).closest('form').validate().showErrors(_defineProperty({}, idField, error));
}

function setTableError(idField, error) {
  notificationHelpers.error(error);
  $('#' + idField).editable('show');
  setTimeout(function () {
    $('.editable-error-block').html(error);
    $('.editable-error-block').show();
  }, 100);
}

/***/ }),
/* 32 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getUserData"] = getUserData;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/** *   personaldataHelpers   ***/

function getUserData() {
  // spinnerHelpers.show();
  userServices.getUserData(storageData.sUserId(), function (response) {
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        $('#' + i).attr('checked', 'checked');
        // $("#" + i).val(val);
      } else if (i == 'img') {
        $('#img-preview').attr('src', val);
      } else {
        $('#' + i).val(val);
      }
    });

    var searchParam = '';
    var initials = [];
    initials.push({
      id: response.data.province_id,
      text: response.data.Province_id
    });
    window.$('#province_id').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/provinces/select',
        data: function data(params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);
          return {
            results: dataParse.data
          };
        }
      }
    });

    var searchParam = '';
    var initials = [];
    initials.push({
      id: response.data.region_id,
      text: response.data.Region_id
    });
    window.$('#region_id').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/regions/select',
        data: function data(params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);
          return {
            results: dataParse.data
          };
        }
      }
    });

    var searchParam = '';
    var initials = [];
    initials.push({
      id: response.data.nation_id,
      text: response.data.Nation_id
    });
    window.$('#nation_id').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/nation/select',
        data: function data(params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);
          return {
            results: dataParse.data
          };
        }
      }
    });

    // view result
    var cont = 0;
    var imgAggItem = response.data.imgAgg;
    if (imgAggItem !== undefined) {
      if (imgAggItem.length > 0) {
        for (var j = 0; j < imgAggItem.length; j++) {
          var html = '';
          if (cont < 10) {
            cont = cont + 1;
            html += '<div class="row" id="divcont-' + cont + '" data-id-img-agg="' + cont + '">';
            html += '<div class="col-8">';
            html += '<label for="img' + cont + '">Immagine Aggiuntiva ' + cont + '</label>&nbsp;';
            html += '<input type="hidden" id="imgBase64' + cont + '" name="imgBase64' + cont + '">';
            html += '<input type="hidden" id="imgName' + cont + '" name="imgName' + cont + '" value="' + imgAggItem[j].img + '">';
            html += '<input type="file" accept="image/jpeg, image/jpg, image/png, image/gif" id="img' + cont + '" class="imgAggInput" name="img' + cont + '">';
            html += '</div>';
            html += '<div class="col-2">';
            html += '<img src="' + imgAggItem[j].img + '" style="max-width:50px;">';
            html += '</div>';
            html += '<div class="col-2">';
            html += '<i class="fa fa-times del-img-agg" data-id-cont="' + cont + '"></i>';
            html += '</div>';
            html += '</div>';
            $('#div-cont-img-agg').append(html);
          }
        }
      }
    }

    $('#frmUpdateDate .del-img-agg').on('click', function (e) {
      var idDel = $(this).attr('data-id-cont');
      var el = $(this).parent('div').parent('div').attr('data-id-img-agg', idDel);

      cont = cont - 1;
      if (cont < 0) {
        cont = 0;
      }
      el.remove();
    });
    // end view result

    /* IMG AGG */
    $('#frmUpdateDate #add-img-agg').on('click', function (e) {
      var html = '';
      if (cont < 10) {
        cont = cont + 1;
        html += '<div class="row" id="divcont-' + cont + '" data-id-img-agg="' + cont + '">';
        html += '<div class="col-10">';
        html += '<label for="img' + cont + '">Immagine Aggiuntiva ' + cont + '</label>&nbsp;';
        html += '<input type="hidden" id="imgBase64' + cont + '" name="imgBase64' + cont + '">';
        html += '<input type="hidden" id="imgName' + cont + '" name="imgName' + cont + '">';
        html += '<input type="file" accept="image/jpeg, image/jpg, image/png, image/gif" id="img' + cont + '" class="imgAggInput" name="img' + cont + '">';
        html += '</div>';
        html += '<div class="col-2">';
        html += '<i class="fa fa-times del-img-agg" data-id-cont="' + cont + '"></i>';
        html += '</div>';
        html += '</div>';
        $('#div-cont-img-agg').append(html);
      }

      $('#frmUpdateDate .imgAggInput').on('change', function (e) {
        var el = $(this).parent('div').parent('div').attr('data-id-img-agg');
        var reader = new FileReader();
        var files = e.target.files || e.dataTransfer.files;
        var jsonImgAgg = {};
        if (!files.length) return;
        reader.onload = function (e) {
          $('#imgName' + el).val(files[0].name);
          $('#imgBase64' + el).val(e.target.result);
        };
        reader.readAsDataURL(files[0]);
      });

      $('#frmUpdateDate .del-img-agg').on('click', function (e) {
        var idDel = $(this).attr('data-id-cont');
        var el = $(this).parent('div').parent('div').attr('data-id-img-agg', idDel);

        cont = cont - 1;
        if (cont < 0) {
          cont = 0;
        }
        el.remove();
      });
    });
    /* END IMG AGG */

    // view result MAPS
    var cont = 0;
    var AggMaps = response.data.AggMaps;
    if (AggMaps !== undefined) {
      if (AggMaps.length > 0) {
        for (var j = 0; j < AggMaps.length; j++) {
          var html = '';
          if (cont < 10) {
            cont = cont + 1;
            html += '<div class="row" id="divcontmaps-' + cont + '" data-id-maps-agg="' + cont + '">';
            html += '<div class="col-5">';
            html += '<div class="form-group">';
            html += '<label for="maps_map' + cont + '">Mappe ' + cont + '</label>&nbsp;';
            html += '<textarea class="form-control" id="maps_map' + cont + '" name="maps_map' + cont + '">' + AggMaps[j].maps_map + '</textarea>';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-3">';
            html += '<div>';
            html += '<div class="form-group">';
            html += '<label for="address_maps' + cont + '">Indirizzo ' + cont + '</label>&nbsp;';
            html += '<input type="text" class="form-control" id="address_maps' + cont + '" name="address_maps' + cont + '" value="' + AggMaps[j].address_maps + '">';
            html += '</div>';
            html += '<div class="form-group">';
            html += '<label for="codepostal_maps' + cont + '">Codice Postale ' + cont + '</label>&nbsp;';
            html += '<input type="text" class="form-control" id="codepostal_maps' + cont + '" name="codepostal_maps' + cont + '" value="' + AggMaps[j].codepostal_maps + '">';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-3">';
            html += '<div>';
            html += '<div class="form-group">';
            html += '<label for="country_maps' + cont + '">Località ' + cont + '</label>&nbsp;';
            html += '<input type="text" class="form-control" id="country_maps' + cont + '" name="country_maps' + cont + '" value="' + AggMaps[j].country_maps + '">';
            html += '</div>';
            html += '<div class="form-group">';
            html += '<label for="province_maps' + cont + '">Provincia ' + cont + '</label>&nbsp;';
            html += '<input type="text" class="form-control" id="province_maps' + cont + '" name="province_maps' + cont + '" value="' + AggMaps[j].province_maps + '">';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '<div class="col-1">';
            html += '<div class="form-group">';
            html += '<i class="fa fa-times del-maps-agg" data-id-cont="' + cont + '"></i>';
            html += '</div>';
            html += '</div>';
            html += '<div class="form-group">';
            html += '<input type="hidden" class="form-control" id="id_map' + cont + '" name="id_map' + cont + '" value="' + AggMaps[j].id_map + '">';
            html += '</div>';
            html += '</div>';
            $('#div-cont-maps-agg').append(html);
          }
        }
      }
    }

    $('#frmUpdateDate .del-img-agg').on('click', function (e) {
      var idDel = $(this).attr('data-id-cont');
      var el = $(this).parent('div').parent('div').attr('data-id-img-agg', idDel);

      cont = cont - 1;
      if (cont < 0) {
        cont = 0;
      }
      el.remove();
    });
    // end view result MAPS

    /* AGG MAPS */
    $('#frmUpdateDate #add-img-maps').on('click', function (e) {
      var html = '';
      if (cont < 10) {
        cont = cont + 1;
        html += '<div class="row" id="divcontmaps-' + cont + '" data-id-maps-agg="' + cont + '">';
        html += '<div class="col-5">';
        html += '<div class="form-group">';
        html += '<label for="maps_map' + cont + '">Mappe ' + cont + '</label>&nbsp;';
        html += '<textarea class="form-control" id="maps_map' + cont + '" name="maps_map' + cont + '"></textarea>';
        html += '</div>';
        html += '</div>';
        html += '<div class="col-3">';
        html += '<div>';
        html += '<div class="form-group">';
        html += '<label for="address_maps' + cont + '">Indirizzo ' + cont + '</label>&nbsp;';
        html += '<input type="text" class="form-control" id="address_maps' + cont + '" name="address_maps' + cont + '">';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="codepostal_maps' + cont + '">Codice Postale ' + cont + '</label>&nbsp;';
        html += '<input type="text" class="form-control" id="codepostal_maps' + cont + '" name="codepostal_maps' + cont + '">';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '<div class="col-3">';
        html += '<div>';
        html += '<div class="form-group">';
        html += '<label for="country_maps' + cont + '">Località ' + cont + '</label>&nbsp;';
        html += '<input type="text" class="form-control" id="country_maps' + cont + '" name="country_maps' + cont + '">';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="province_maps' + cont + '">Provincia ' + cont + '</label>&nbsp;';
        html += '<input type="text" class="form-control" id="province_maps' + cont + '" name="province_maps' + cont + '">';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<input type="hidden" class="form-control" id="id_map' + cont + '" name="id_map' + cont + '">';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '<div class="col-1">';
        html += '<div class="form-group">';
        html += '<i class="fa fa-times del-maps-agg" data-id-cont="' + cont + '"></i>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('#div-cont-maps-agg').append(html);
      }

      $('#frmUpdateDate .del-maps-agg').on('click', function (e) {
        var idDel = $(this).attr('data-id-cont');
        var el = $(this).parent('div').parent('div').attr('data-id-maps-agg', idDel);

        cont = cont - 1;
        if (cont < 0) {
          cont = 0;
        }
        el.remove();
      });
    });
    /* END MAPS */
  });
}

function update() {
  spinnerHelpers.show();

  var data = {
    id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    email: $('#frmUpdateDate #email').val(),
    business_name: $('#frmUpdateDate #business_name').val(),
    name: $('#frmUpdateDate #name').val(),
    surname: $('#frmUpdateDate #surname').val(),
    vat_number: $('#frmUpdateDate #vat_number').val(),
    fiscal_code: $('#frmUpdateDate #fiscal_code').val(),
    address: $('#frmUpdateDate #address').val(),
    postal_code: $('#frmUpdateDate #postal_code').val(),
    telephone_number: $('#frmUpdateDate #telephone_number').val(),
    website: $('#frmUpdateDate #website').val(),
    vat_type_id: $('#frmUpdateDate #vat_type_id').val(),
    country: $('#frmUpdateDate #country').val(),
    province: $('#frmUpdateDate #province').val(),
    description: $('#frmUpdateDate #description').val(),
    accepted_payments: $('#frmUpdateDate #accepted_payments').val(),
    terms_of_sale: $('#frmUpdateDate #terms_of_sale').val(),
    shop_time: $('#frmUpdateDate #shop_time').val(),
    maps: $('#frmUpdateDate #maps').val(),
    img: $('#frmUpdateDate #imgBase64').val(),
    monday: $('#frmUpdateDate #monday').val(),
    tuesday: $('#frmUpdateDate #tuesday').val(),
    wednesday: $('#frmUpdateDate #wednesday').val(),
    thursday: $('#frmUpdateDate #thursday').val(),
    friday: $('#frmUpdateDate #friday').val(),
    saturday: $('#frmUpdateDate #saturday').val(),
    sunday: $('#frmUpdateDate #sunday').val(),
    province_id: $('#frmUpdateDate #province_id').val(),
    nation_id: $('#frmUpdateDate #nation_id').val(),
    region_id: $('#frmUpdateDate #region_id').val()
  };

  var arrayImgAgg = [];
  for (var i = 0; i < 10; i++) {
    if (functionHelpers.isValued($('#imgName' + i).val())) {
      arrayImgAgg.push(JSON.parse('{"imgBase64' + i + '":"' + $('#imgBase64' + i).val() + '", "imgName' + i + '":"' + $('#imgName' + i).val() + '"}'));
    }
  }
  data.imgAgg = arrayImgAgg;

  // SEZIONE MAPS
  var arrayMapAgg = [];
  for (var i = 0; i < 10; i++) {
    var jsonMapAgg = {};
    var check = false;
    if (functionHelpers.isValued($('#id_map' + i).val())) {
      check = true;
      jsonMapAgg.id_map = $('#id_map' + i).val();
    }
    if (functionHelpers.isValued($('#maps_map' + i).val())) {
      check = true;
      jsonMapAgg.maps_map = $('#maps_map' + i).val();
    }
    if (functionHelpers.isValued($('#address_maps' + i).val())) {
      check = true;
      jsonMapAgg.address_maps = $('#address_maps' + i).val();
    }
    if (functionHelpers.isValued($('#codepostal_maps' + i).val())) {
      check = true;
      jsonMapAgg.codepostal_maps = $('#codepostal_maps' + i).val();
    }
    if (functionHelpers.isValued($('#country_maps' + i).val())) {
      check = true;
      jsonMapAgg.country_maps = $('#country_maps' + i).val();
    }
    if (functionHelpers.isValued($('#province_maps' + i).val())) {
      check = true;
      jsonMapAgg.province_maps = $('#province_maps' + i).val();
    }
    if (check) {
      arrayMapAgg.push(jsonMapAgg);
    }
  }
  data.AggMaps = arrayMapAgg;

  userServices.updateNoAuth(data, function () {
    notificationHelpers.success('Modifica dati effettuata con successo!');
    setTimeout(function () {
      location.reload();
    }, 2000);
    spinnerHelpers.hide();
  });
}

/***/ }),
/* 33 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getListItems"] = getListItems;
/* harmony export (immutable) */ __webpack_exports__["getItemsUserShop"] = getItemsUserShop;
/* harmony export (immutable) */ __webpack_exports__["deleteRow"] = deleteRow;
/* harmony export (immutable) */ __webpack_exports__["getItemsCategoriesFilter"] = getItemsCategoriesFilter;
/* harmony export (immutable) */ __webpack_exports__["getItemById"] = getItemById;
/* harmony export (immutable) */ __webpack_exports__["updateNoAuth"] = updateNoAuth;
/* harmony export (immutable) */ __webpack_exports__["addFavoritesItem"] = addFavoritesItem;
/** *   itemsHelpers   ***/

function getListItems() {
  spinnerHelpers.show();
  itemServices.getListItems(storageData.sUserId(), function (response) {
    for (var i = 0; i < response.data.length; i++) {
      var newList = '';
      newList += '<tr>';
      newList += '<td><a class="btn btn-info" onclick="itemsHelpers.getItemById(' + response.data[i].id + ')"><i class="fa fa-pencil"></i></a>';
      newList += '<a class="btn btn-danger" onclick="itemsHelpers.deleteRow(' + response.data[i].id + ')"><i class="fa fa-trash"></i></a></td>';
      newList += "<td><img src='" + response.data[i].img + "'></td>";
      newList += '<td>' + response.data[i].description + '</td>';
      newList += '<td>' + response.data[i].producer_id + '</td>';
      newList += '<td>€ ' + response.data[i].price + '</td>';
      newList += '</tr>';

      $('#table_list_announcement').show();
      $('#frmUpdateAds').hide();
      $('#table_list_announcement tbody').append(newList);
    }
  });

  spinnerHelpers.hide();
}

function getItemsUserShop(data) {
  spinnerHelpers.show();

  itemServices.getItemsUserShop(data, function (response) {
    var html = '';
    var htmlPaginator = '';
    var minus10Page = 1;
    var plus10Page = 1;
    var minus1Page = 1;
    var plus1Page = 1;
    var pageSelected = parseInt(response.data.pageSelected);
    var maxPage = response.data.totalPages;
    var msgSearch = 'tutto il catalogo';
    if ($('#tabCategoryUserCont .nav-item .active').attr('data-id-category') != undefined) {
      msgSearch = $('#tabCategoryUserCont .nav-item .active').text();
    }
    if (response.data.html != '') {
      $('#resultItem').html('');
      html += '<div class="row"><div class="col-12"><h3>Stai cercando in: <b>' + msgSearch + '</b></h3></div></div>';
      html += '<div class="row">' + response.data.html + '</div>';
      $('#resultItem').html(html);

      /* funzione gestione paginatore */
      if (response.data.paginationActive) {
        htmlPaginator += "<ul class='pagination'>";
        /* controllo freccia -10 pagine */
        if (pageSelected - 10 <= 1) {
          minus10Page = 1;
        } else {
          minus10Page = pageSelected - 10;
        }

        htmlPaginator += '<li class="page-item" data-page="' + minus10Page + '"><a><i class="fa fa-angle-double-left"></i></a></li>';
        /* end controllo freccia -10 pagine */
        /* controllo freccia -1 pagine */
        if (pageSelected - 1 <= 1) {
          minus1Page = 1;
        } else {
          minus1Page = pageSelected - 1;
        }

        htmlPaginator += '<li class="page-item" data-page="' + minus1Page + '"><a><i class="fa fa-angle-left"></i></a></li>';
        /* end controllo freccia -1 pagine */

        /* controllo per pagine intermedie */
        if (maxPage > 4) {
          if (pageSelected > 1) {
            if (pageSelected + 2 > maxPage) {
              if (pageSelected == maxPage) {
                for (var j = pageSelected - 3; j <= maxPage; j++) {
                  if (pageSelected == j) {
                    htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
                  } else {
                    htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
                  }
                }
              } else {
                for (var j = pageSelected - 2; j <= maxPage; j++) {
                  if (pageSelected == j) {
                    htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
                  } else {
                    htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
                  }
                }
              }
            } else {
              for (var j = pageSelected - 1; j <= pageSelected + 2; j++) {
                if (pageSelected == j) {
                  htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
                } else {
                  htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
                }
              }
            }
          } else {
            for (var j = 1; j <= 4; j++) {
              if (pageSelected == j) {
                htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
              } else {
                htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
              }
            }
          }
        } else {
          for (var j = 1; j <= maxPage; j++) {
            if (pageSelected == j) {
              htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
            } else {
              htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
            }
          }
        }
        /* end controllo per pagine intermedie */

        /* controllo freccia + 1 pagine */
        if (pageSelected + 1 >= maxPage) {
          plus1Page = maxPage;
        } else {
          plus1Page = pageSelected + 1;
        }

        htmlPaginator += '<li class="page-item" data-page="' + plus1Page + '"><a><i class="fa fa-angle-right"></i></a></li>';
        /* end controllo freccia + 1 pagine */
        /* controllo freccia + 10 pagine */
        if (pageSelected + 10 >= maxPage) {
          plus10Page = maxPage;
        } else {
          plus10Page = pageSelected + 10;
        }

        htmlPaginator += '<li class="page-item" data-page="' + plus10Page + '"><a><i class="fa fa-angle-double-right"></i></a></li>';
        /* end controllo freccia + 10 pagine */

        htmlPaginator += '</ul>';
        $('html,body').animate({ scrollTop: $('.mainPages').offset().top }, 'slow');
        $('.paginationCont').html(htmlPaginator);
      } else {
        $('.paginationCont').html('');
      }
      $('.totItems').html(response.data.totItems);
      /* end funzione gestione paginatore */
      $('.pagination li').on('click', function () {
        var data = {
          url: window.location.pathname,
          description: $('#description').val(),
          priceFrom: $('#priceUserShopFrom').val(),
          priceTo: $('#priceUserShopTo').val(),
          condition: $('#conditionUserShop').val(),
          producer: $('#producerUserShop').val(),
          idCategory: $('#tabCategoryUserCont .nav-item .active').attr('data-id-category'),
          pageSelected: $(this).attr('data-page')
        };
        itemsHelpers.getItemsUserShop(data);
      });
    } else {
      $('#resultItem').html('');
      $('#resultItem').html('Nessun articolo trovato');
    }
  });

  spinnerHelpers.hide();
}

function deleteRow(id) {
  if (confirm("Sei sicuro di voler eliminare l'annuncio?")) {
    spinnerHelpers.show();
    itemServices.del(id, function (response) {
      notificationHelpers.success(dictionaryHelpers.getDictionary('DeleteCompleted'));
      setTimeout(function () {
        location.reload();
        spinnerHelpers.hide();
      }, 2000);
    });
  }
}

function getItemsCategoriesFilter(data) {
  spinnerHelpers.show();

  itemServices.getItemsCategoriesFilter(data, function (response) {
    var html = '';
    var htmlPaginator = '';
    var minus10Page = 1;
    var plus10Page = 1;
    var minus1Page = 1;
    var plus1Page = 1;
    var pageSelected = parseInt(response.data.pageSelected);
    var maxPage = response.data.totalPages;

    if (response.data.html != '') {
      $('#resultItem').html('');
      html += response.data.html;
      $('#resultItem').html(html);

      /* funzione gestione paginatore */
      if (response.data.paginationActive) {
        htmlPaginator += "<ul class='pagination'>";
        /* controllo freccia -10 pagine */
        if (pageSelected - 10 <= 1) {
          minus10Page = 1;
        } else {
          minus10Page = pageSelected - 10;
        }

        htmlPaginator += '<li class="page-item" data-page="' + minus10Page + '"><a><i class="fa fa-angle-double-left"></i></a></li>';
        /* end controllo freccia -10 pagine */
        /* controllo freccia -1 pagine */
        if (pageSelected - 1 <= 1) {
          minus1Page = 1;
        } else {
          minus1Page = pageSelected - 1;
        }

        htmlPaginator += '<li class="page-item" data-page="' + minus1Page + '"><a><i class="fa fa-angle-left"></i></a></li>';
        /* end controllo freccia -1 pagine */

        /* controllo per pagine intermedie */
        if (maxPage > 4) {
          if (pageSelected > 1) {
            if (pageSelected + 2 > maxPage) {
              if (pageSelected == maxPage) {
                for (var j = pageSelected - 3; j <= maxPage; j++) {
                  if (pageSelected == j) {
                    htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
                  } else {
                    htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
                  }
                }
              } else {
                for (var j = pageSelected - 2; j <= maxPage; j++) {
                  if (pageSelected == j) {
                    htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
                  } else {
                    htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
                  }
                }
              }
            } else {
              for (var j = pageSelected - 1; j <= pageSelected + 2; j++) {
                if (pageSelected == j) {
                  htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
                } else {
                  htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
                }
              }
            }
          } else {
            for (var j = 1; j <= 4; j++) {
              if (pageSelected == j) {
                htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
              } else {
                htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
              }
            }
          }
        } else {
          for (var j = 1; j <= maxPage; j++) {
            if (pageSelected == j) {
              htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
            } else {
              htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
            }
          }
        }
        /* end controllo per pagine intermedie */

        /* controllo freccia + 1 pagine */
        if (pageSelected + 1 >= maxPage) {
          plus1Page = maxPage;
        } else {
          plus1Page = pageSelected + 1;
        }

        htmlPaginator += '<li class="page-item" data-page="' + plus1Page + '"><a><i class="fa fa-angle-right"></i></a></li>';
        /* end controllo freccia + 1 pagine */
        /* controllo freccia + 10 pagine */
        if (pageSelected + 10 >= maxPage) {
          plus10Page = maxPage;
        } else {
          plus10Page = pageSelected + 10;
        }

        htmlPaginator += '<li class="page-item" data-page="' + plus10Page + '"><a><i class="fa fa-angle-double-right"></i></a></li>';
        /* end controllo freccia + 10 pagine */

        htmlPaginator += '</ul>';

        $('.paginationCont').html(htmlPaginator);
        $('html,body').animate({ scrollTop: $('.mainPages').offset().top }, 'slow');
      } else {
        $('.paginationCont').html('');
      }
      $('.totItems').html(response.data.totItems);
      /* end funzione gestione paginatore */
      $('.pagination li').on('click', function () {
        var data = {
          url: window.location.pathname,
          priceFrom: $('#priceCategoriesFilterFrom').val(),
          priceTo: $('#priceCategoriesFilterTo').val(),
          condition: $('#conditionCategoriesFilter').val(),
          producer: $('#producerCategoriesFilter').val(),
          order: $('#orderCategoriesFilter').val(),
          description: $('#searchItemsCategory').val(),
          pageSelected: $(this).attr('data-page')
        };
        itemsHelpers.getItemsCategoriesFilter(data);
      });
    } else {
      $('#resultItem').html('');
      $('#resultItem').html('Nessun articolo trovato');
    }
  });

  spinnerHelpers.hide();
}

function getItemById(id) {
  // spinnerHelpers.show();
  itemServices.getItemById(id, function (response) {
    var searchParam = '';

    /* SELECT PRODUCER */
    var initials = [];
    initials.push({
      id: response.data.producer_id,
      text: response.data.descriptionProducer
    });
    $('#producer_id').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/producer/select',
        data: function data(params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });
    /* END SELECT PRODUCER */

    /* SELECT CATEGORIES FATHER */
    var initialsCategoryFather = [];
    initialsCategoryFather.push({
      id: response.data.category_father_id,
      text: response.data.categoryFatherDescription
    });
    $('#category_father_id').select2({
      data: initialsCategoryFather,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/Categories/selectPrincipalCategories',
        data: function data(params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });

    $('#category_father_id').on('change', function () {
      $('#category_id').val('');
      var searchParam = '';
      var catFather = $(this).val();
      $('#category_id').select2({
        ajax: {
          url: configData.wsRootServicesUrl + '/api/v1/Categories/selectSubCategories',
          data: function data(params) {
            if (params.term == '') {
              searchParam = '*';
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam,
              idCatFather: catFather
            };
            return query;
          },
          processResults: function processResults(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });
    });
    /* END SELECT CATEGORIES FATHER */

    /* SELECT CATEGORIES */
    var initialsCategory = [];
    initialsCategory.push({
      id: response.data.category_id,
      text: response.data.categoryDescription
    });
    $('#category_id').select2({
      data: initialsCategory,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/Categories/selectSubCategories',
        data: function data(params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam,
            idCatFather: response.data.category_father_id
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);
          return {
            results: dataParse.data
          };
        }
      }
    });
    /* END SELECT CATEGORIES */

    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        $('#' + i).attr('checked', 'checked');
        // $("#" + i).val(val);
      } else if (i == 'img') {
        $('#img-preview').attr('src', val);
      } else {
        $('#' + i).val(val);
      }
    });

    var cont = 0;
    var imgAggItem = response.data.imgAgg;
    if (imgAggItem.length > 0) {
      for (var j = 0; j < imgAggItem.length; j++) {
        var html = '';
        if (cont < 10) {
          cont = cont + 1;
          html += '<div class="row" id="divcont-' + cont + '" data-id-img-agg="' + cont + '">';
          html += '<div class="col-8">';
          html += '<label for="img' + cont + '">Immagine Aggiuntiva ' + cont + '</label>&nbsp;';
          html += '<input type="hidden" id="imgBase64' + cont + '" name="imgBase64' + cont + '">';
          html += '<input type="hidden" id="imgName' + cont + '" name="imgName' + cont + '" value="' + imgAggItem[j].img + '">';
          html += '<input type="file" accept="image/jpeg, image/jpg, image/png, image/gif" id="img' + cont + '" class="imgAggInput" name="img' + cont + '">';
          html += '</div>';
          html += '<div class="col-2">';
          html += '<img src="' + imgAggItem[j].img + '" style="max-width:50px;">';
          html += '</div>';
          html += '<div class="col-2">';
          html += '<i class="fa fa-times del-img-agg" data-id-cont="' + cont + '"></i>';
          html += '</div>';
          html += '</div>';
          $('#div-cont-img-agg').append(html);
        }
      }
    }

    $('#frmUpdateAds .del-img-agg').on('click', function (e) {
      var idDel = $(this).attr('data-id-cont');
      var el = $(this).parent('div').parent('div').attr('data-id-img-agg', idDel);

      cont = cont - 1;
      if (cont < 0) {
        cont = 0;
      }
      el.remove();
    });

    /* IMG AGG */
    $('#frmUpdateAds #add-img-agg').on('click', function (e) {
      var html = '';
      if (cont < 10) {
        cont = cont + 1;
        html += '<div class="row" id="divcont-' + cont + '" data-id-img-agg="' + cont + '">';
        html += '<div class="col-10">';
        html += '<label for="img' + cont + '">Immagine Aggiuntiva ' + cont + '</label>&nbsp;';
        html += '<input type="hidden" id="imgBase64' + cont + '" name="imgBase64' + cont + '">';
        html += '<input type="hidden" id="imgName' + cont + '" name="imgName' + cont + '">';
        html += '<input type="file" accept="image/jpeg, image/jpg, image/png, image/gif" id="img' + cont + '" class="imgAggInput" name="img' + cont + '">';
        html += '</div>';
        html += '<div class="col-2">';
        html += '<i class="fa fa-times del-img-agg" data-id-cont="' + cont + '"></i>';
        html += '</div>';
        html += '</div>';
        $('#div-cont-img-agg').append(html);
      }

      $('#frmUpdateAds .imgAggInput').on('change', function (e) {
        var el = $(this).parent('div').parent('div').attr('data-id-img-agg');
        var reader = new FileReader();
        var files = e.target.files || e.dataTransfer.files;
        var jsonImgAgg = {};
        if (!files.length) return;
        reader.onload = function (e) {
          $('#imgName' + el).val(files[0].name);
          $('#imgBase64' + el).val(e.target.result);
        };
        reader.readAsDataURL(files[0]);
      });

      $('#frmUpdateAds .del-img-agg').on('click', function (e) {
        var idDel = $(this).attr('data-id-cont');
        var el = $(this).parent('div').parent('div').attr('data-id-img-agg', idDel);

        cont = cont - 1;
        if (cont < 0) {
          cont = 0;
        }
        el.remove();
      });
    });
    /* END IMG AGG */
  });

  $('#frmUpdateAds').show();
  $('#table_list_announcement').hide();
}

function updateNoAuth() {
  spinnerHelpers.show();
  var data = {
    user_id: storageData.sUserId(),
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmUpdateAds #id').val(),
    external_link: $('#frmUpdateAds #external_link').val(),
    description: $('#frmUpdateAds #description').val(),
    meta_tag_characteristic: $('#frmUpdateAds #meta_tag_characteristic').val(),
    price: $('#frmUpdateAds #price').val().replace(',', '.'),
    available: $('#frmUpdateAds #available').val(),
    producer_id: $('#frmUpdateAds #producer_id').val(),
    category_id: $('#frmUpdateAds #category_id').val(),
    free_shipment: $('#frmUpdateAds #free_shipment').val(),
    new: $('#frmUpdateAds #new').val(),
    img: $('#frmUpdateAds #imgBase64').val(),
    imgName: $('#frmUpdateAds #imgName').val()
  };

  var arrayImgAgg = [];
  for (var i = 0; i < 10; i++) {
    if (functionHelpers.isValued($('#imgName' + i).val())) {
      arrayImgAgg.push(JSON.parse('{"imgBase64' + i + '":"' + $('#imgBase64' + i).val() + '", "imgName' + i + '":"' + $('#imgName' + i).val() + '"}'));
    }
  }
  data.imgAgg = arrayImgAgg;

  itemServices.updateNoAuth(data, function () {
    notificationHelpers.success('Annuncio modificato correttamente!');
    setTimeout(function () {
      location.reload();
      spinnerHelpers.hide();
    }, 2000);
  });
}

function addFavoritesItem(id) {
  if (storageData.sUserId() == null || storageData.sUserId() == '') {
    notificationHelpers.error('Prima di inserire un articolo nei preferiti è necessario effettuare il login!');
  } else {
    spinnerHelpers.show();
    var data = {
      idItem: id,
      user_id: storageData.sUserId()
    };
    itemServices.addFavoritesItem(data, function (response) {
      if (response.data == '0') {
        // se articolo era già presente gestisco il fatto che è stato rimosso mostrando il cuore vuoto
        $("i[data-id-item-fav='" + id + "']").removeClass('fa-heart');
        $("i[data-id-item-fav='" + id + "']").addClass('fa-heart-o');
        notificationHelpers.success('Articolo rimosso dai preferiti!');
      } else {
        // altrimenti gestisco il fatto che è stato aggiunto ai preferiti
        $("i[data-id-item-fav='" + id + "']").addClass('fa-heart');
        $("i[data-id-item-fav='" + id + "']").removeClass('fa-heart-o');
        notificationHelpers.success('Articolo aggiunto ai preferiti!');
      }
      spinnerHelpers.hide();
    });
  }
}

/***/ }),
/* 34 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["searchImplemented"] = searchImplemented;
/** *   searchShopHelpers   ***/

/** *   POST   ***/

function searchImplemented(idForm) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(idForm);
  data.idLanguage = storageData.sIdLanguage();
  var html = '';
  searchShopServices.searchImplemented(data, function (response) {
    // window.history.pushState('ricerca', 'Ricerca', '/ricerca-avanzata?r=' + $('#frmSearch #search').val());
    html += "<div class='container div-cont-search'>";
    html += "<div class='row'>";
    html += response.message;
    html += '</div>';
    html += '</div>';

    $('#result').html(html);
    spinnerHelpers.hide();
  });
}

/** *   END POST   ***/

/***/ }),
/* 35 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["updateNoAuth"] = updateNoAuth;
/* harmony export (immutable) */ __webpack_exports__["deleteRow"] = deleteRow;
/* harmony export (immutable) */ __webpack_exports__["insertNoAuth"] = insertNoAuth;
/** *   statesHelpers   ***/
// statesServices

function getAll() {
  spinnerHelpers.show();
  statesServices.getAll(storageData.sIdLanguage(), function (response) {
    for (var i = 0; i < response.data.length; i++) {
      var newList = '';
      newList += '<tr>';
      newList += '<td><button class="btn btn-info" onclick="statesHelpers.getById(' + response.data[i].id + ')"><i class="fa fa-pencil"/></button>';
      newList += '<button class="btn btn-danger" onclick="statesHelpers.deleteRow(' + response.data[i].id + ')"><i class="fa fa-trash"/></button></td>';
      newList += '<td>' + response.data[i].id + '</td>';
      newList += '<td>' + response.data[i].description + '</td>';
      newList += '</tr>';

      $('#table_list_states').show();
      $('#frmUpdateStates').hide();
      $('#table_list_states tbody').append(newList);
    }
  });

  spinnerHelpers.hide();
}

function getById(id) {
  // spinnerHelpers.show();
  statesServices.getById(id, function (response) {
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        $('#' + i).attr('checked', 'checked');
      } else {
        $('#' + i).val(val);
      }
    });
  });

  $('#frmUpdateStates').show();
  $('#table_list_states').hide();
  $('#id').css('display', 'none');
  $('#description').css('display', 'block');
}

function updateNoAuth() {
  spinnerHelpers.show();
  var data = {
    // id: storageData.sUserId(),
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmUpdateStates #id').val(),
    description: $('#frmUpdateStates #description').val()
  };
  statesServices.updateNoAuth(data, function () {
    $('#frmUpdateStates #id').val('');
    $('#frmUpdateStates #description').val('');

    notificationHelpers.success(dictionaryHelpers.getDictionary('ContactRequestCompleted'));
    spinnerHelpers.hide();
    $('#table_list_states').show();
    $('#frmUpdateStates').hide();
    window.location.reload();
  });
}

function deleteRow(id) {
  if (confirm('Vuoi eliminare lo stato?')) {
    spinnerHelpers.show();
    statesServices.del(id, function (response) {
      notificationHelpers.success(dictionaryHelpers.getDictionary('DeleteCompleted'));
      spinnerHelpers.hide();
      window.location.reload();
    });
  }
}

function insertNoAuth() {
  spinnerHelpers.show();

  var data = {
    created_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmAddStates #id').val(),
    description: $('#frmAddStates #description').val()
  };

  statesServices.insertNoAuth(data, function () {
    $('#frmAddStates #id').val('');
    $('#frmAddStates #description').val('');
    notificationHelpers.success(dictionaryHelpers.getDictionary('ContactRequestCompleted'));
    spinnerHelpers.hide();
  });
}

/***/ }),
/* 36 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAllNoAuth"] = getAllNoAuth;
/* harmony export (immutable) */ __webpack_exports__["getByIdRoles"] = getByIdRoles;
/* harmony export (immutable) */ __webpack_exports__["updateNoAuth"] = updateNoAuth;
/* harmony export (immutable) */ __webpack_exports__["deleteRow"] = deleteRow;
/* harmony export (immutable) */ __webpack_exports__["insertNoAuth"] = insertNoAuth;
/** *   rolesHelpers   ***/

function getAllNoAuth() {
  spinnerHelpers.show();

  rolesServices.getAllNoAuth(function (response) {
    for (var i = 0; i < response.data.length; i++) {
      var newList = '';
      newList += '<tr>';
      newList += '<td><button class="btn btn-info" onclick="rolesHelpers.getByIdRoles(' + response.data[i].id + ')"><i class="fa fa-pencil"/></button>';
      newList += '<button class="btn btn-danger" onclick="rolesHelpers.deleteRow(' + response.data[i].id + ')"><i class="fa fa-trash"/></button></td>';
      newList += '<td>' + response.data[i].id + '</td>';
      newList += '<td>' + response.data[i].name + '</td>';
      newList += '<td>' + response.data[i].display_name + '</td>';
      newList += '<td>' + response.data[i].description + '</td>';
      newList += '</tr>';
      $('#table_list_roles tbody').append(newList);
      $('#table_list_roles').show();
      $('#frmUpdateRoles').hide();
    }
  });

  spinnerHelpers.hide();
}

function getByIdRoles(id) {
  // spinnerHelpers.show();
  rolesServices.getByIdRoles(id, function (response) {
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        $('#' + i).attr('checked', 'checked');
      } else {
        $('#' + i).val(val);
      }
    });
  });
  $('#btnadd').hide();
  $('#frmUpdateRoles').show();
  $('#table_list_roles').hide();
}

function updateNoAuth() {
  spinnerHelpers.show();
  var data = {
    // id: storageData.sUserId(),
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmUpdateRoles #id').val(),
    description: $('#frmUpdateRoles #description').val(),
    name: $('#frmUpdateRoles #name').val(),
    display_name: $('#frmUpdateRoles #display_name').val()
  };
  rolesServices.updateNoAuth(data, function () {
    $('#frmUpdateRoles #id').val('');
    $('#frmUpdateRoles #description').val('');
    $('#frmUpdateRoles #name').val('');
    $('#frmUpdateRoles #display_name').val('');

    notificationHelpers.success(dictionaryHelpers.getDictionary('ContactRequestCompleted'));
    spinnerHelpers.hide();
    $('#table_list_roles').show();
    $('#frmUpdateRoles').hide();

    window.location.reload();
  });
}

function deleteRow(id) {
  if (confirm('Vuoi eliminare il ruolo?')) {
    spinnerHelpers.show();
    rolesServices.del(id, function (response) {
      notificationHelpers.success(dictionaryHelpers.getDictionary('DeleteCompleted'));
      spinnerHelpers.hide();
      window.location.reload();
    });
  }
}

function insertNoAuth() {
  spinnerHelpers.show();

  var data = {
    created_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmAddRoles #id').val(),
    name: $('#frmAddRoles #name').val(),
    display_name: $('#frmAddRoles #display_name').val(),
    description: $('#frmAddRoles #description').val()
  };

  rolesServices.insertNoAuth(data, function () {
    $('#frmAddRoles #id').val('');
    $('#frmAddRoles #name').val('');
    $('#frmAddRoles #display_name').val('');
    $('#frmAddRoles #description').val('');
    notificationHelpers.success(dictionaryHelpers.getDictionary('ContactRequestCompleted'));
    spinnerHelpers.hide();
  });
}

/***/ }),
/* 37 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["deleteRow"] = deleteRow;
/* harmony export (immutable) */ __webpack_exports__["insertNoAuth"] = insertNoAuth;
function getAll(id) {
  spinnerHelpers.show();
  itemGrServices.getAll(id, function (response) {
    for (var i = 0; i < response.data.length; i++) {
      var newList = '';
      newList += '<tr>';
      newList += '<td><button class="btn btn-info" onclick="itemsGrHelpers.getById(' + response.data[i].id + ')"><i class="fa fa-pencil"/></button>';
      newList += '<button class="btn btn-danger" onclick="itemsGrHelpers.deleteRow(' + response.data[i].id + ')"><i class="fa fa-trash"/></button></td>';
      newList += '<td>' + response.data[i].id + '</td>';
      newList += '<td>' + response.data[i].tipology + '</td>';
      newList += '<td>' + response.data[i].note + '</td>';
      // newList += "<td><a href='" + response.data[i].img + "'</a>></td>";
      newList += '<div class="container"><div id="lightgallery" style="display:flex">';
      newList += "<td><a href='" + response.data[i].img + "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></td></div></div>";
      newList += '</td>';
      newList += '<td>' + response.data[i].original_code + '</td>';
      newList += '<td>' + response.data[i].code_product + '</td>';
      newList += '<td>' + response.data[i].code_gr + '</td>';
      newList += '<td>' + response.data[i].plant + '</td>';
      newList += '<td>' + response.data[i].brand + '</td>';
      newList += '<td>' + response.data[i].business_name_supplier + '</td>';
      newList += '<td>' + response.data[i].code_supplier + '</td>';
      newList += '<td>' + response.data[i].price_purchase + '</td>';
      newList += '<td>' + response.data[i].price_list + '</td>';
      newList += '<td>' + response.data[i].repair_price + '</td>';
      newList += '<td>' + response.data[i].price_new + '</td>';
      newList += '<td>' + response.data[i].update_date + '</td>';
      newList += '<td>' + response.data[i].usual_supplier + '</td>';
      newList += '<td>' + response.data[i].pr_rip_conc + '</td>';

      newList += '</tr>';

      $('#table_list_itemsGR tbody').append(newList);
      $('#frmUpdateItemsGr').hide();
      $('#table_list_searchitems').hide();
      $('#table_list_itemsGR').show();
    }
    lightGallery(document.getElementById('lightgallery'));
  });
  spinnerHelpers.hide();
}

function getById(id) {
  $('#frmUpdateItemsGr #id').val(id);
  // spinnerHelpers.show();
  itemGrServices.getById(id, function (response) {
    var searchParam = '';

    /* SELECT BRAND */
    var initials = [];
    initials.push({ id: response.data.brand, text: response.data.Brand });
    $('#brand').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/brand/select',
        data: function data(params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });

    var initials = [];
    $('#brand').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/brand/select',
        data: function data(params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });
    /* END SELECT brand */

    /* SELECT business_name_supplier */
    var initials = [];
    initials.push({
      id: response.data.business_name_supplier,
      text: response.data.BusinessName
    });
    $('#business_name_supplier').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/businessnamesupplier/select',
        data: function data(params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });

    var initials = [];
    $('#business_name_supplier').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/businessnamesupplier/select',
        data: function data(params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });
    /* END SELECT business_name_supplier */

    /* SELECT tipology */
    var initials = [];
    initials.push({ id: response.data.tipology, text: response.data.Tipology });
    $('#tipology').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/tipology/select',
        data: function data(params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });

    var initials = [];
    $('#tipology').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/tipology/select',
        data: function data(params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });
    /* END SELECT tipology */

    /*  SELECT ItemsGr */
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        $('#frmUpdateItemsGr #' + i).attr('checked', 'checked');
      } else if (i == 'imgName') {
        if (val != '') {
          $('#frmUpdateItemsGr #imgPreview').attr('src', val);
        } else {
          $('#frmUpdateItemsGr #imgPreview').remove();
        }
      } else {
        $('#frmUpdateItemsGr #' + i).val(val);
      }
    });
    $('#frmUpdateItemsGr').show();
    $('#table_list_itemsGR').hide();
    $('#frmSearchItems').hide();
    $('#table_list_searchitems').hide();
  });
}

function update() {
  spinnerHelpers.show();
  var data = {
    // id: storageData.sUserId(),
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmUpdateItemsGr #id').val(),
    tipology: $('#frmUpdateItemsGr #tipology').val(),
    note: $('#frmUpdateItemsGr #note').val(),
    original_code: $('#frmUpdateItemsGr #original_code').val(),
    code_product: $('#frmUpdateItemsGr #code_product').val(),
    code_gr: $('#frmUpdateItemsGr #code_gr').val(),
    plant: $('#frmUpdateItemsGr #plant').val(),
    brand: $('#frmUpdateItemsGr #brand').val(),
    business_name_supplier: $('#frmUpdateItemsGr #business_name_supplier').val(),
    code_supplier: $('#frmUpdateItemsGr #code_supplier').val(),
    price_purchase: $('#frmUpdateItemsGr #price_purchase').val(),
    price_list: $('#frmUpdateItemsGr #price_list').val(),
    repair_price: $('#frmUpdateItemsGr #repair_price').val(),
    price_new: $('#frmUpdateItemsGr #price_new').val(),
    update_date: $('#frmUpdateItemsGr #update_date').val(),
    usual_supplier: $('#frmUpdateItemsGr #usual_supplier').val(),
    pr_rip_conc: $('#frmUpdateItemsGr #pr_rip_conc').val(),
    img: $('#frmUpdateItemsGr #imgBase64').val(),
    imgName: $('#frmUpdateItemsGr #imgName').val()
  };
  itemGrServices.update(data, function () {
    $('#frmUpdateItemsGr #id').val('');
    $('#frmUpdateItemsGr #tipology').val('');
    $('#frmUpdateItemsGr #note').val('');
    $('#frmUpdateItemsGr #original_code').val('');
    $('#frmUpdateItemsGr #code_product').val('');
    $('#frmUpdateItemsGr #code_gr').val('');
    $('#frmUpdateItemsGr #plant').val('');
    $('#frmUpdateItemsGr #brand').val('');
    $('#frmUpdateItemsGr #business_name_supplier').val('');
    $('#frmUpdateItemsGr #code_supplier').val('');
    $('#frmUpdateItemsGr #price_purchase').val('');
    $('#frmUpdateItemsGr #price_list').val('');
    $('#frmUpdateItemsGr #repair_price').val('');
    $('#frmUpdateItemsGr #price_new').val('');
    $('#frmUpdateItemsGr #update_date').val('');
    $('#frmUpdateItemsGr #usual_supplier').val('');
    $('#frmUpdateItemsGr #imgBase64').val('');
    $('#frmUpdateItemsGr #pr_rip_conc').val('');

    notificationHelpers.success(dictionaryHelpers.getDictionary('ContactRequestCompleted'));
    spinnerHelpers.hide();
    window.location.reload();
    $('#table_list_itemsGR').show();
    $('#frmUpdateItemsGr').hide();
  });
}

function deleteRow(id) {
  if (confirm("Vuoi eliminare l'articolo?")) {
    spinnerHelpers.show();
    itemGrServices.del(id, function (response) {
      notificationHelpers.success(dictionaryHelpers.getDictionary('DeleteCompleted'));
      spinnerHelpers.hide();
      window.location.reload();
    });
  }
}

function insertNoAuth() {
  spinnerHelpers.show();

  var data = {
    created_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    img: $('#frmAddItemsGr #imgBase64').val(),
    imgName: $('#frmAddItemsGr #imgName').val(),
    tipology: $('#frmAddItemsGr #tipology').val(),
    note: $('#frmAddItemsGr #note').val(),
    original_code: $('#frmAddItemsGr #original_code').val(),
    code_product: $('#frmAddItemsGr #code_product').val(),
    code_gr: $('#frmAddItemsGr #code_gr').val(),
    plant: $('#frmAddItemsGr #plant').val(),
    brand: $('#frmAddItemsGr #brand').val(),
    business_name_supplier: $('#frmAddItemsGr #business_name_supplier').val(),
    code_supplier: $('#frmAddItemsGr #code_supplier').val(),
    price_purchase: $('#frmAddItemsGr #price_purchase').val(),
    price_list: $('#frmAddItemsGr #price_list').val(),
    repair_price: $('#frmAddItemsGr #repair_price').val(),
    price_new: $('#frmAddItemsGr #price_new').val(),
    update_date: $('#frmAddItemsGr #update_date').val(),
    usual_supplier: $('#frmAddItemsGr #usual_supplier').val(),
    pr_rip_conc: $('#frmAddItemsGr #pr_rip_conc').val()
  };

  itemGrServices.insertNoAuth(data, function () {
    $('#frmAddItemsGr #imgBase64').val('');
    $('#frmAddItemsGr #tipology').val('');
    $('#frmAddItemsGr #note').val('');
    $('#frmAddItemsGr #original_code').val('');
    $('#frmAddItemsGr #code_product').val('');
    $('#frmAddItemsGr #code_gr').val('');
    $('#frmAddItemsGr #plant').val('');
    $('#frmAddItemsGr #brand').val('');
    $('#frmAddItemsGr #business_name_supplier').val('');
    $('#frmAddItemsGr #code_supplier').val('');
    $('#frmAddItemsGr #price_purchase').val('');
    $('#frmAddItemsGr #price_list').val('');
    $('#frmAddItemsGr #repair_price').val('');
    $('#frmAddItemsGr #price_new').val('');
    $('#frmAddItemsGr #update_date').val('');
    $('#frmAddItemsGr #usual_supplier').val('');
    $('#frmAddItemsGr #pr_rip_conc').val('');
    notificationHelpers.success('Inserimento completato con successo!');
    spinnerHelpers.hide();
  });
}

/***/ }),
/* 38 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["searchImplemented"] = searchImplemented;
/** *   searchLiutaioHelpers   ***/

/** *   POST   ***/

function searchImplemented(idForm) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(idForm);
  data.idLanguage = storageData.sIdLanguage();
  var html = '';
  searchLiutaioServices.searchImplemented(data, function (response) {
    html += "<div class='container div-cont-search'>";
    html += "<div class='row'>";
    html += response.message;
    html += '</div>';
    html += '</div>';

    $('#result').html(html);
    spinnerHelpers.hide();
  });
}

/** *   END POST   ***/

/***/ }),
/* 39 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["searchImplemented"] = searchImplemented;
/** *   searchSalaRegistrazioneHelpers   ***/

/** *   POST   ***/

function searchImplemented(idForm) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(idForm);
  data.idLanguage = storageData.sIdLanguage();
  var html = '';

  searchSalaRegistrazioneServices.searchImplemented(data, function (response) {
    // window.history.pushState('ricerca', 'Ricerca', '/ricerca-avanzata?r=' + $('#frmSearch #search').val());
    html += "<div class='container div-cont-search'>";
    html += "<div class='row'>";
    html += response.message;
    html += '</div>';
    html += '</div>';

    $('#result').html(html);
    spinnerHelpers.hide();
  });
}

/** *   END POST   ***/

/***/ }),
/* 40 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["searchImplemented"] = searchImplemented;
/** *   searchInterventionHelpers   ***/
/** *   POST   ***/

function searchImplemented(create_quote, IdIntervention, nr_ddt, user_id, items) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(create_quote, IdIntervention, nr_ddt, user_id, items);
  data.idLanguage = storageData.sIdLanguage();
  var html = '';
  searchInterventionServices.searchImplemented(data, function (response) {
    $('#table_list_searchintervention tbody').html('');
    for (var i = 0; i < response.data.length; i++) {
      var newList = '';
      if (response.data[i].ready == true) {
        newList += "<tr style='background-color:#FFA500'>";
      } else {
        newList += "<tr style=''>";
      }

      if (response.data[i].nr_ddt_gr != null && response.data[i].nr_ddt_gr != '-') {
        newList += "<tr style='background-color:#fff'>";
      }

      if (response.data[i].ready == true) {
        newList += '<td><input type="checkbox"  id="form-control ready' + i + '" value="' + response.data[i].ready + '" name="ready' + i + '" class="switch-input' + i + '" checked="true' + i + '" onclick="interventionHelpers.updateCheck(' + response.data[i].id + ')"></td>';
      } else {
        newList += '<td><input type="checkbox" id="form-control ready' + i + '" value="' + response.data[i].ready + '" name="ready' + i + '" class="switch-input' + i + '" onclick="interventionHelpers.updateCheck(' + response.data[i].id + ')"></td>';
      }

      newList += '<td><button class="btn btn-info" onclick="interventionHelpers.getById(' + response.data[i].id + ')"><i class="fa fa-pencil"/></button></td>';
      newList += '<td><button class="btn btn-light" onclick="interventionHelpers.getByViewValue(' + response.data[i].id + ')"><i class="fa fa-circle-o"/></button></td>';
      newList += '<td><button class="btn btn-dark" onclick="interventionHelpers.getByViewQuotes(' + response.data[i].id + ')"><i class="fa fa-circle-o"/></button></td>';
      newList += '<td>' + response.data[i].id + '</td>';
      newList += '<td><a style="cursor:pointer;" onclick="interventionHelpers.getByIdUser(' + response.data[i].user_id + ')">' + response.data[i].descriptionCustomer + '</a></td>';
      newList += '<td>' + response.data[i].date_ddt + '</td>';
      newList += '<td>' + response.data[i].nr_ddt + '</td>';
      // newList += "<td><a href='" + response.data[i].imgmodule + "'</a>Visualizza</td>";
      newList += '<td>' + response.data[i].description + '</td>';
      newList += '<td>' + response.data[i].defect;
      if (response.data[i].imgmodule != '') {
        newList += '<div class="container"><div id="lightgallery" style="display:flex">';
        newList += "<a href='" + response.data[i].imgmodule + "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
      }
      newList += '</td>';
      newList += '<td>' + response.data[i].codeGr + '</td>';
      newList += '<td>' + response.data[i].nr_ddt_gr + '</td>';
      newList += '<td>' + response.data[i].date_ddt_gr + '</td>';
      if (response.data[i].idInterventionExternalRepair == null) {
        newList += '<td><button class="btn btn-green" style="color:#d7e7fe;font-size:38px;" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-truck"/></button></td>';
      }
      if (response.data[i].idInterventionExternalRepair != null) {
        newList += '<td><button class="btn btn-green" style="color:#02784d;font-size:38px;" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-truck"/></button></td>';
      }
      if (response.data[i].ddt_supplier != null && response.data[i].ddt_supplier != '-') {
        newList += '<button class="btn btn-green" style="color:#02784d;font-size:38px;" onclick="interventionHelpers.getByViewExternalRepair(' + response.data[i].id + ')"><i class="fa fa-reply" aria-hidden="true"></i></button></td>';
      }
      newList += '<td>' + response.data[i].create_quote + '</td>';
      newList += '<td>' + response.data[i].descriptionStates + '</td>';
      newList += '<td>' + response.data[i].codeTracking + '</td>';
      newList += '<td><button class="btn btn-danger" onclick="interventionHelpers.deleteRow(' + response.data[i].id + ')"><i class="fa fa-trash"/></button></td>';
      newList += '</tr>';

      $('#table_list_searchintervention').show();
      $('#table_list_searchintervention tbody').append(newList);
      $('#table_list_intervention').hide();
    }
  });
  spinnerHelpers.hide();
}

/** *   END POST   ***/

/***/ }),
/* 41 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["searchImplemented"] = searchImplemented;
/** *   searchUserHelpers   ***/
/** *   POST   ***/

function searchImplemented(name, business_name, email, country, province, telephone_number) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(name, business_name, email, country, province, telephone_number);
  data.idLanguage = storageData.sIdLanguage();
  var html = '';
  searchUserServices.searchImplemented(data, function (response) {
    $('#table_list_searchuser tbody').html('');
    for (var i = 0; i < response.data.length; i++) {
      var newList = '';
      newList += '<tr>';
      var newList = '';
      newList += '<tr>';
      newList += '<td><button class="btn btn-info" onclick="userHelpers.getByIdResult(' + response.data[i].id + ')"><i class="fa fa-pencil"/></button></td>';
      newList += '<td>' + response.data[i].id + '</td>';
      newList += '<td>' + response.data[i].subscriber + '</td>';
      newList += '<td>' + response.data[i].expiration_date + '</td>';
      newList += '<td>' + response.data[i].subscription_date + '</td>';
      newList += '<td>' + response.data[i].business_name + '</td>';
      newList += '<td>' + response.data[i].name + '</td>';
      newList += '<td>' + response.data[i].telephone_number + '</td>';
      newList += '<td>' + response.data[i].mobile_number + '</td>';
      newList += '<td>' + response.data[i].fax_number + '</td>';
      newList += '<td>' + response.data[i].email + '</td>';
      newList += '<td><button class="btn btn-danger" onclick="userHelpers.deleteRow(' + response.data[i].id + ')"><i class="fa fa-trash"/></button></td>';
      newList += '</tr>';

      $('#table_list_searchuser').show();
      $('#table_list_user').hide();
      $('#table_list_searchuser tbody').append(newList);
      // $("#table_list_intervention").hide();
    }
  });

  spinnerHelpers.hide();
}

/** *   END POST   ***/

/***/ }),
/* 42 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["searchImplemented"] = searchImplemented;
/** *   searchItemsHelpers   ***/
/** *   POST   ***/

function searchImplemented(tipology, note, original_code, code_product, code_gr, plant, brand, business_name_supplier, code_supplier, price_purchase, price_list, repair_price, price_new, update_date, usual_supplier, pr_rip_conc) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(tipology, note, original_code, code_product, code_gr, plant, brand, business_name_supplier, code_supplier, price_purchase, price_list, repair_price, price_new, update_date, usual_supplier, pr_rip_conc);
  data.idLanguage = storageData.sIdLanguage();
  var html = '';
  searchItemsServices.searchImplemented(data, function (response) {
    for (var i = 0; i < response.data.length; i++) {
      var newList = '';
      newList += '<tr>';
      newList += '<td><button class="btn btn-info" onclick="itemsGrHelpers.getById(' + response.data[i].id + ')"><i class="fa fa-pencil"/></button>';
      newList += '<button class="btn btn-danger" onclick="itemsGrHelpers.deleteRow(' + response.data[i].id + ')"><i class="fa fa-trash"/></button></td>';
      newList += '<td>' + response.data[i].id + '</td>';
      newList += '<td>' + response.data[i].tipology + '</td>';
      newList += '<td>' + response.data[i].note + '</td>';
      newList += '<div class="container"><div id="lightgallery" style="display:flex">';
      newList += "<td><a href='" + response.data[i].img + "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></td></div></div>";
      newList += '</td>';
      // newList += "<td><a href='" + response.data[i].img + "'</a>></td>";
      newList += '<td>' + response.data[i].original_code + '</td>';
      newList += '<td>' + response.data[i].code_product + '</td>';
      newList += '<td>' + response.data[i].code_gr + '</td>';
      newList += '<td>' + response.data[i].plant + '</td>';
      newList += '<td>' + response.data[i].brand + '</td>';
      newList += '<td>' + response.data[i].business_name_supplier + '</td>';
      newList += '<td>' + response.data[i].code_supplier + '</td>';
      newList += '<td>' + response.data[i].price_purchase + '</td>';
      newList += '<td>' + response.data[i].price_list + '</td>';
      newList += '<td>' + response.data[i].repair_price + '</td>';
      newList += '<td>' + response.data[i].price_new + '</td>';
      newList += '<td>' + response.data[i].update_date + '</td>';
      newList += '<td>' + response.data[i].usual_supplier + '</td>';
      newList += '<td>' + response.data[i].pr_rip_conc + '</td>';

      newList += '</tr>';

      $('#table_list_searchitems').show();
      $('#table_list_itemsGR').hide();
      $('#table_list_searchitems tbody').append(newList);
    }
    lightGallery(document.getElementById('lightgallery'));
  });

  spinnerHelpers.hide();
}

/** *   END POST   ***/

/***/ }),
/* 43 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["addFastUrgenze"] = addFastUrgenze;
/* harmony export (immutable) */ __webpack_exports__["getUserData"] = getUserData;
/* harmony export (immutable) */ __webpack_exports__["updateFast"] = updateFast;
//  fasturgenciesHelpers

// addFastUrgenze

function addFastUrgenze() {
  spinnerHelpers.show();

  var data = {
    user_id: $('#frmfasturgencies #user_id').val(),
    date: $('#frmfasturgencies #date').val(),
    object: $('#frmfasturgencies #object').val(),
    description: $('#frmfasturgencies #description').val()
  };

  var arrayfasturgencies = [];
  for (var i = 0; i < 15; i++) {
    var jsonfasturgencies = {};
    var check = false;

    if (functionHelpers.isValued($('#user_id' + i).val())) {
      check = true;
      jsonfasturgencies.user_id = $('#user_id' + i).val();
    }
    if (functionHelpers.isValued($('#date' + i).val())) {
      check = true;
      jsonfasturgencies.date = $('#date' + i).val();
    }
    if (functionHelpers.isValued($('#object' + i).val())) {
      check = true;
      jsonfasturgencies.object = $('#object' + i).val();
    }
    if (functionHelpers.isValued($('#description' + i).val())) {
      check = true;
      jsonfasturgencies.description = $('#description' + i).val();
    }
    if (check) {
      arrayfasturgencies.push(jsonfasturgencies);
    }
  }

  data.fasturgencies = arrayfasturgencies;

  fasturgenciesServices.insert(data, function () {
    $('#intervention #user_id').val('');
    $('#intervention #date').val('');
    $('#intervention #object').val('');
    $('#intervention #description').val('');
    notificationHelpers.success('Inserimento avvenuto correttamente!');
    spinnerHelpers.hide();
  });
}

function getUserData() {
  // spinnerHelpers.show();
  fasturgenciesServices.getUserData(storageData.sUserId(), function (response) {
    // view result FAST URGENZE
    var cont = 0;
    var Aggfasturgencies = response.data;
    if (Aggfasturgencies !== undefined) {
      if (Aggfasturgencies.length > 0) {
        for (var j = 0; j < Aggfasturgencies.length; j++) {
          var html = '';
          html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
          html += '<div class="col-3">';
          html += '<select class="custom-select user_id_select" id="user_id' + Aggfasturgencies[j].id + '" name="user_id' + '" value="' + Aggfasturgencies[j].user_id + '"></select>';
          html += '</div>';
          html += '<div class="col-2">';
          html += '<input type="text" id="date' + Aggfasturgencies[j].id + '" name="date' + '" class="form-control date' + '" value="' + Aggfasturgencies[j].date + '">';
          html += '</div>';
          html += '<div class="col-2">';
          html += '<input type="text" class="form-control object" id="object' + Aggfasturgencies[j].id + '" name="object' + '" value="' + Aggfasturgencies[j].object + '">';
          html += '</div>';
          html += '<div class="col-3">';
          html += '<input type="text" class="form-control description" id="description' + Aggfasturgencies[j].id + '" name="description' + '" value="' + Aggfasturgencies[j].description + '">';
          html += '</div>';
          html += '<div class="col-1">';
          html += '<button type="button" class="btn btn-outline-success" onclick="fasturgenciesHelpers.updateFast(' + Aggfasturgencies[j].id + ',false)">Salva</button>';
          html += '</div>';
          html += '<div class="col-1">';
          html += '<button type="button" class="btn btn-outline-danger" id="btndelete' + '" onclick="btndelete' + '">Cancella</button>';
          html += '</div>';
          html += '<div class="col-0">';
          html += '<input type="hidden" class="form-control id" id="id' + Aggfasturgencies[j].id + '" name="id' + '" value="' + Aggfasturgencies[j].id + '">';
          html += '</div>';
          html += '</div>';
          $('#div-cont-fast-urgenze').append(html);

          // $.getScript("https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js").done(function(){
          var searchParam = '';
          var initials = [];
          initials.push({
            id: Aggfasturgencies[j].user_id,
            text: Aggfasturgencies[j].description_user_id
          });
          $('#user_id' + Aggfasturgencies[j].id).select2({
            data: initials,
            ajax: {
              url: configData.wsRootServicesUrl + '/api/v1/user/select',
              data: function data(params) {
                if (params.term == '') {
                  searchParam = '*';
                } else {
                  searchParam = params.term;
                }
                var query = {
                  search: searchParam
                };
                return query;
              },
              processResults: function processResults(data) {
                var dataParse = JSON.parse(data);
                return {
                  results: dataParse.data
                };
              }
            }
          });
          // });
        }
      }
    }

    // end view FAST URGENZE

    /* AGG FAST URGENZE */
    $('#frmFastUrgenze #add-fast-urgenze').on('click', function (e) {
      var html = '';
      if (cont < 10) {
        cont = cont + 1;
        html += '<div class="row" data-id-row="' + cont + '" id="divcont-' + cont + '" >';
        html += '<div class="col-3">';
        html += '<select class="custom-select user_id_select" id="user_id' + cont + '" name="user_id' + cont + '"></select>';
        html += '</div>';
        html += '<div class="col-2">';
        html += '<input type="text" id="date' + cont + '" name="date' + cont + '" class="form-control date' + cont + '">';
        html += '</div>';
        html += '<div class="col-2">';
        html += '<input type="text" class="form-control object" id="object' + cont + '" name="object' + cont + '">';
        html += '</div>';
        html += '<div class="col-3">';
        html += '<input type="text" class="form-control description" id="description' + cont + '" name="description' + cont + '">';
        html += '</div>';
        html += '<div class="col-1">';
        html += '<button type="button" class="btn btn-outline-success" id="btnsave' + '" onclick="fasturgenciesHelpers.updateFast(' + cont + ',true)">Salva</button>';
        html += '</div>';
        html += '<div class="col-1">';
        html += '<button type="button" class="btn btn-outline-danger" id="btndelete' + '" onclick="btndelete' + '">Cancella</button>';
        html += '</div>';
        html += '<div class="col-0">';
        html += '<input type="hidden" class="form-control id" id="id' + cont + '" name="id' + '" value="' + cont + '">';
        html += '</div>';
        html += '</div>';
        $('#div-cont-fast-urgenze').append(html);

        $.getScript('https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js').done(function () {
          var searchParam = '';
          $('#user_id' + cont).select2({
            ajax: {
              url: configData.wsRootServicesUrl + '/api/v1/user/select',
              data: function data(params) {
                if (params.term == '') {
                  searchParam = '*';
                } else {
                  searchParam = params.term;
                }
                var query = {
                  search: searchParam
                };
                return query;
              },
              processResults: function processResults(data) {
                var dataParse = JSON.parse(data);
                return {
                  results: dataParse.data
                };
              }
            }
          });
        });
      }
    });
    /* END FAST URGENZE */
  });
}

function updateFast(id, isNew) {
  var data = {
    id: id,
    isNew: isNew
  };

  var arrayFast = [];
  var jsonFast = {};
  var check = false;
  if (functionHelpers.isValued($('#date' + id).val())) {
    check = true;
    jsonFast.date = $('#date' + id).val();
  }
  if (functionHelpers.isValued($('#id' + id).val())) {
    check = true;
    jsonFast.id = $('#id' + id).val();
  }
  jsonFast.isNew = isNew;

  if (functionHelpers.isValued($('#object' + id).val())) {
    check = true;
    jsonFast.object = $('#object' + id).val();
  }
  if (functionHelpers.isValued($('#description' + id).val())) {
    check = true;
    jsonFast.description = $('#description' + id).val();
  }
  if (functionHelpers.isValued($('#user_id' + id).val())) {
    check = true;
    jsonFast.user_id = $('#user_id' + id).val();
  }

  if (check) {
    arrayFast.push(jsonFast);
  }

  data = arrayFast;
  fasturgenciesServices.updateFast(data, function () {
    notificationHelpers.success('Modifica dati effettuata con successo!');
  });
}

/***/ }),
/* 44 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getLastId"] = getLastId;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["getAllInternalRepair"] = getAllInternalRepair;
/* harmony export (immutable) */ __webpack_exports__["getByInternalRepair"] = getByInternalRepair;
/* harmony export (immutable) */ __webpack_exports__["updateInternalRepair"] = updateInternalRepair;
/* harmony export (immutable) */ __webpack_exports__["deleteRow"] = deleteRow;
/** *   internalRepairHelpers   ***/

function getLastId() {
  InternalRepairServices.getLastId(function (response) {
    $('#frmViewInternalRepair #nr').val(response.data);
  });
}

function insert() {
  spinnerHelpers.show();

  var data = {
    customer_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    img: $('#frmViewInternalRepair #imgBase64').val(),
    imgName: $('#frmViewInternalRepair #imgName').val(),
    id: $('#frmViewInternalRepair #nr').val(),
    date: $('#frmViewInternalRepair #date').val(),
    ddt_exit: $('#frmViewInternalRepair #ddt_exit').val(),
    supplier: $('#frmViewInternalRepair #supplier').val(),
    description: $('#frmViewInternalRepair #description').val(),
    code_gr: $('#frmViewInternalRepair #code_gr').val()
  };

  var arrayInternalRepairImgAgg = [];
  for (var i = 0; i < 4; i++) {
    if (functionHelpers.isValued($('#imgBase64' + i).val())) {
      arrayInternalRepairImgAgg.push($('#imgBase64' + i).val());
    }
  }
  data.imagecaptures = arrayInternalRepairImgAgg;

  InternalRepairServices.insert(data, function () {
    $('#frmViewInternalRepair #imgName').val('');
    $('#frmViewInternalRepair #imgBase64').val('');
    $('#frmViewInternalRepair #nr').val('');
    $('#frmViewInternalRepair #date').val('');
    $('#frmViewInternalRepair #ddt_exit').val('');
    $('#frmViewInternalRepair #supplier').val('');
    $('#frmViewInternalRepair #description').val('');
    $('#frmViewInternalRepair #code_gr').val('');
    notificationHelpers.success('Inserimento avvenuto correttamente!');
    spinnerHelpers.hide();
  });
}

function getAllInternalRepair() {
  spinnerHelpers.show();
  InternalRepairServices.getAllInternalRepair(storageData.sIdLanguage(), function (response) {
    for (var i = 0; i < response.data.length; i++) {
      var newList = '';
      newList += '<tr>';
      newList += '<td><button class="btn btn-info" onclick="internalRepairHelpers.getByInternalRepair(' + response.data[i].id + ')"><i class="fa fa-pencil"/></button></td>';
      newList += '<td>' + response.data[i].id + '</td>';
      newList += '<td>' + response.data[i].date + '</td>';
      newList += '<td>' + response.data[i].ddt_exit + '</td>';
      newList += '<td>' + response.data[i].supplier + '</td>';
      newList += '<td>' + response.data[i].description + '</td>';
      newList += '<td>' + response.data[i].code_gr + '</td>';
      newList += '<td>';
      for (var j = 0; j < response.data[i].imagecaptures.length; j++) {
        newList += '<div class="container"><div id="lightgallery" style="display:flex">';
        newList += "<a href='" + response.data[i].imagecaptures[j].img + "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
      }
      newList += '</td>';
      newList += '<td>' + response.data[i].date_return_product_from_the_supplier + '</td>';
      newList += '<td>' + response.data[i].ddt_supplier + '</td>';
      newList += '<td>' + response.data[i].reference_supplier + '</td>';
      newList += '<td>' + response.data[i].notes_from_repairman + '</td>';
      newList += '<td><button class="btn btn-danger" onclick="internalRepairHelpers.deleteRow(' + response.data[i].id + ')"><i class="fa fa-trash"/></button></td>';
      newList += '</tr>';
      $('#table_list_internal_repair tbody').append(newList);
      $('#table_list_internal_repair').show();
      $('#frmViewUpdateInternalRepair').hide();
    }
    lightGallery(document.getElementById('lightgallery'));
  });

  spinnerHelpers.hide();
}

function getByInternalRepair(id) {
  // spinnerHelpers.show();
  InternalRepairServices.getByInternalRepair(id, function (response) {
    var searchParam = '';
    /* SELECT supplier */
    var initials = [];
    initials.push({
      id: response.data.supplier,
      text: response.data.Supplier
    });
    $('#supplier').select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/businessnamesupplier/select',
        data: function data(params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function processResults(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });
    /* END SELECT supplier */

    /* SELECT internal repair */
    jQuery.each(response.data, function (i, val) {

      if (val == true) {
        $('#frmViewUpdateInternalRepair #' + i).attr('checked', 'checked');
      } else if (i == 'imgName') {
        if (val != '') {
          $('#frmViewUpdateInternalRepair #imgPreview').attr('src', val);
        } else {
          $('#frmViewUpdateInternalRepair #imgPreview').remove();
        }
      } else {
        $('#frmViewUpdateInternalRepair #' + i).val(val);
      }
    });

    var cont = 0;
    var imgAggCaptures = response.data.imagecaptures;
    $('#snapShot').html('');
    if (imgAggCaptures.length > 0) {
      for (var j = 0; j < imgAggCaptures.length; j++) {
        var html = '';
        if (cont < 4) {
          cont = cont + 1;
          html += '<input type="hidden" id="imgBase64' + cont + '" value="' + imgAggCaptures[j].img + '" name="imgBase64' + cont + '">';
          html += '<a href="' + imgAggCaptures[j].img + '"><img src="' + imgAggCaptures[j].img + '" style="max-width:50px;"></a>';
          $('#snapShot').append(html);
        }
      }
    }

    lightGallery(document.getElementById('snapShot'));

    $.getScript('https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.js').done(function () {
      Webcam.set({
        width: 220,
        height: 190,
        image_format: 'jpeg',
        jpeg_quality: 100
      });
      Webcam.attach('#camera');
    });

    $('#frmViewUpdateInternalRepair #btPic').on('click', function () {
      if (cont < 4) {
        cont = cont + 1;
        Webcam.snap(function (data_uri) {
          $('#snapShot').append('<img src="' + data_uri + '" width="70px" height="50px" />');
          $('#snapShot').append('<input type="hidden" value="' + data_uri + '" id="imgBase64' + cont + '" name="imgBase64' + cont + '"/>');
        });
      }
    });
    $('#frmViewUpdateInternalRepair').show();
    $('#table_list_internal_repair').hide();
  });
}

function updateInternalRepair() {
  spinnerHelpers.show();
  var data = {
    id: $('#frmViewUpdateInternalRepair #id').val(),
    date: $('#frmViewUpdateInternalRepair #date').val(),
    ddt_exit: $('#frmViewUpdateInternalRepair #ddt_exit').val(),
    supplier: $('#frmViewUpdateInternalRepair #supplier').val(),
    description: $('#frmViewUpdateInternalRepair #description').val(),
    code_gr: $('#frmViewUpdateInternalRepair #code_gr').val(),
    date_return_product_from_the_supplier: $('#frmViewUpdateInternalRepair #date_return_product_from_the_supplier').val(),
    ddt_supplier: $('#frmViewUpdateInternalRepair #ddt_supplier').val(),
    reference_supplier: $('#frmViewUpdateInternalRepair #reference_supplier').val(),
    notes_from_repairman: $('#frmViewUpdateInternalRepair #notes_from_repairman').val()
  };

  var arrayimgAggCaptures = [];
  for (var i = 0; i <= 4; i++) {
    if (functionHelpers.isValued($('#frmViewUpdateInternalRepair #imgBase64' + i).val())) {
      arrayimgAggCaptures.push($('#frmViewUpdateInternalRepair #imgBase64' + i).val());
    }
  }
  data.imagecaptures = arrayimgAggCaptures;

  InternalRepairServices.updateInternalRepair(data, function () {
    $('#frmViewUpdateInternalRepair #date').val('');
    $('#frmViewUpdateInternalRepair #ddt_exit').val('');
    $('#frmViewUpdateInternalRepair #supplier').val('');
    $('#frmViewUpdateInternalRepair #description').val('');
    $('#frmViewUpdateInternalRepair #code_gr').val('');
    $('#frmViewUpdateInternalRepair #date_return_product_from_the_supplier').val('');
    $('#frmViewUpdateInternalRepair #ddt_supplier').val('');
    $('#frmViewUpdateInternalRepair #reference_supplier').val('');
    $('#frmViewUpdateInternalRepair #notes_from_repairman').val('');

    notificationHelpers.success('Modifica avvenuta con successo');
    spinnerHelpers.hide();
    $('#table_list_internal_repair').show();
    $('#frmViewUpdateInternalRepair').hide();
  });
}

function deleteRow(id) {
  if (confirm('Sei sicuro di voler eliminare la riparazione?')) {
    spinnerHelpers.show();
    InternalRepairServices.del(id, function (response) {
      notificationHelpers.success(dictionaryHelpers.getDictionary('DeleteCompleted'));
      spinnerHelpers.hide();
      window.location.reload();
    });
  }
}

/***/ }),
/* 45 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAllInterventionCustomer"] = getAllInterventionCustomer;
/* harmony export (immutable) */ __webpack_exports__["getByViewModuleCustomer"] = getByViewModuleCustomer;
/* harmony export (immutable) */ __webpack_exports__["getByViewQuotesCustomer"] = getByViewQuotesCustomer;
/* harmony export (immutable) */ __webpack_exports__["updateInterventionUpdateViewCustomerQuotes"] = updateInterventionUpdateViewCustomerQuotes;
/* harmony export (immutable) */ __webpack_exports__["updateInterventionUpdateViewCustomerQuotesAccept"] = updateInterventionUpdateViewCustomerQuotesAccept;
/** *   interventioncustomerHelpers   ***/

function getAllInterventionCustomer() {
  spinnerHelpers.show();
  interventionCustomerServices.getAllInterventionCustomer(storageData.sUserId(), function (response) {
    for (var i = 0; i < response.data.length; i++) {
      var newList = '';
      newList += '<tr>';
      newList += '<td>' + response.data[i].id + '</td>';
      newList += '<td>' + response.data[i].date_ddt + '</td>';
      newList += '<td>' + response.data[i].plant_type + '</td>';
      newList += '<td>' + response.data[i].defect;
      if (response.data[i].imgmodule != '') {
        newList += '<div class="container"><div id="lightgallery" style="display:flex">';
        newList += "<a href='" + response.data[i].imgmodule + "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
      }
      newList += '</td>';
      newList += '<td>' + response.data[i].states_quotes_description + '</td>';
      newList += '<td>' + response.data[i].states_description + '</td>';

      newList += '<td>';
      if (response.data[i].states_quotes_description != 'Accettato' && response.data[i].states_quotes_description != 'Rifiutato') {
        newList += '<button class="btn btn-link" onclick="interventioncustomerHelpers.getByViewQuotesCustomer(' + response.data[i].id + ')"><i class="fa fa-eur"/></button>';
      }
      if (response.data[i].states_quotes_description == 'Accettato') {
        newList += '<button class="btn btn-green" style="color:green;" onclick="interventioncustomerHelpers.getByViewQuotesCustomer(' + response.data[i].id + ')"><i class="fa fa-eur" style="color:green!important;"/></button>';
      }
      if (response.data[i].states_quotes_description == 'Rifiutato') {
        newList += '<button class="btn btn-green" style="color:red;" onclick="interventioncustomerHelpers.getByViewQuotesCustomer(' + response.data[i].id + ')"><i class="fa fa-eur" style="color:red!important;"/></button>';
      }
      ;'</td>';

      newList += '<td><button class="btn btn-link" onclick="interventioncustomerHelpers.getByViewModuleCustomer(' + response.data[i].id + ')"><i class="fa fa-wrench"/></button></td>';
      newList += '</tr>';
      $('#table_list_customerintervention tbody').append(newList);
      $('#table_list_customerintervention').show();
      $('#frmViewQuotesCustomer').hide();
      $('#ViewFaultModule').hide();
    }
    lightGallery(document.getElementById('lightgallery'));
  });
  spinnerHelpers.hide();
}

function getByViewModuleCustomer(id) {
  interventionCustomerServices.getByViewModuleCustomer(id, function (response) {
    var searchParam = '';
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        if (i == 'id') {
          $('#ViewFaultModule #' + i).html(val);
        } else {
          $('#ViewFaultModule #' + i).attr('checked', 'checked');
        }
      } else if (i == 'imgName') {
        if (val != '') {
          $('#ViewFaultModule #imgPreview').attr('src', val);
        } else {
          $('#ViewFaultModule #imgName').remove();
        }
      } else {
        $('#ViewFaultModule #' + i).val(val);
        $('#ViewFaultModule #' + i).html(val);
      }
    });
    $('#ViewFaultModule').show();
    $('#frmViewQuotesCustomer').hide();
    $('#table_list_customerintervention').hide();
  });
}

function getByViewQuotesCustomer(id) {
  interventionCustomerServices.getByViewQuotesCustomer(id, function (response) {
    var searchParam = '';
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        if (i == 'id') {
          $('#frmViewQuotesCustomer #' + i).html(val);
        } else {
          $('#frmViewQuotesCustomer #' + i).attr('checked', 'checked');
        }
      } else if (i == 'imgName') {
        if (val != '') {
          $('#frmViewQuotesCustomer #imgPreview').attr('src', val);
        } else {
          $('#frmViewQuotesCustomer #imgName').remove();
        }
      } else {
        $('#frmViewQuotesCustomer #' + i).val(val);
        $('#frmViewQuotesCustomer #' + i).html(val);
      }

      var searchParam = '';
      var initials = [];
      initials.push({
        id: response.data.id_states_quote,
        text: response.data.States_Description
      });
      $('#id_states_quote').select2({
        data: initials,
        ajax: {
          url: configData.wsRootServicesUrl + '/api/v1/StatesQuotesGr/selectStatesQuotesGr',
          data: function data(params) {
            if (params.term == '') {
              searchParam = '*';
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function processResults(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var AggArticlesQuotes = response.data.Articles;
      $('#div-cont-codegr_description-agg').html('');
      if (AggArticlesQuotes.length > 0) {
        for (var j = 0; j < AggArticlesQuotes.length; j++) {
          var html = '';
          html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
          html += '<div class="col-4">';
          html += '<label for="description">Descrizione:<br><span class="description">' + AggArticlesQuotes[j].description + '</span></label>';
          html += '</div>';
          html += '<div class="col-4">';
          html += '<label for="code_gr">Codice Articolo:<br><span class="code_gr">' + AggArticlesQuotes[j].code_gr + '</span></label>';
          html += '</div>';
          html += '</div>';
          $('#div-cont-codegr_description-agg').append(html);
        }
      }
    });
    $('#frmViewQuotesCustomer').show();
    $('#ViewFaultModule').hide();
    $('#table_list_customerintervention').hide();
  });
}

function updateInterventionUpdateViewCustomerQuotes() {
  spinnerHelpers.show();
  var data = {
    updated_id: storageData.sUserId(),
    // idLanguage: storageData.sIdLanguage(),
    id_intervention: $('#frmViewQuotesCustomer #id').val(),
    // id_states_quote: $('#frmViewQuotesCustomer #id_states_quote').val(),
    id_states_quote: $('#frmViewQuotesCustomer #rifiutato').val()
  };
  if (confirm('Vuoi confermare lo stato del tuo preventivo?')) {
    interventionCustomerServices.updateInterventionUpdateViewCustomerQuotes(data, function () {
      notificationHelpers.success('Modifica avvenuta con successo');
      spinnerHelpers.hide();
      $('#table_list_customerintervention').show();
      $('#frmViewQuotesCustomer').hide();
    });
  }
}
function updateInterventionUpdateViewCustomerQuotesAccept() {
  spinnerHelpers.show();
  var data = {
    updated_id: storageData.sUserId(),
    // idLanguage: storageData.sIdLanguage(),
    id_intervention: $('#frmViewQuotesCustomer #id').val(),
    // id_states_quote: $('#frmViewQuotesCustomer #id_states_quote').val(),
    id_states_quote: $('#frmViewQuotesCustomer #accettato').val()
  };
  if (confirm('Vuoi confermare lo stato del tuo preventivo?')) {
    interventionCustomerServices.updateInterventionUpdateViewCustomerQuotes(data, function () {
      notificationHelpers.success('Modifica avvenuta con successo');
      spinnerHelpers.hide();
      $('#table_list_customerintervention').show();
      $('#frmViewQuotesCustomer').hide();
    });
  }
}

/***/ }),
/* 46 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["getAllByUser"] = getAllByUser;
/** *   userAddressHelpers   ***/

function insert(idForm) {
  spinnerHelpers.show();

  if (functionHelpers.isValued(storageData.sUserId())) {
    var data = functionHelpers.formToJson(idForm);
    data.user_id = storageData.sUserId();
    data.cart_id = storageData.sCartId();

    userAddressServices.insert(data, function () {
      notificationHelpers.success('Indirizzo inserito correttamente!');
      window.$('#modal-add-address').modal('hide');
      spinnerHelpers.hide();
      setTimeout(function () {
        location.reload(true);
      }, 1000);
    });
  } else {
    window.location.href = '/login';
  }
}

function getAllByUser(idToAppend) {
  spinnerHelpers.show();
  if (functionHelpers.isValued(storageData.sUserId())) {
    userAddressServices.getAllByUser(storageData.sUserId(), function (result) {
      $('#' + idToAppend).html(result.message);

      /* gestione indirizzi di spedizione */
      $('#' + idToAppend + ' .fa-check').on('click', function () {
        var idAddressActive = $(this).attr('data-id-address');

        if (functionHelpers.isValued(idAddressActive)) {
          var data = {
            idAddress: idAddressActive,
            idCart: storageData.sCartId()
          };
          userAddressServices.activeAddressByUser(data, function (result) {
            window.$('#modal-list-address').modal('hide');
            if (result.data != '') {
              notificationHelpers.success('Indirizzo attivato correttamente!');
              spinnerHelpers.hide();
              setTimeout(function () {
                location.reload(true);
              }, 1000);
            } else {
              spinnerHelpers.hide();
              notificationHelpers.error('Ops, qualcosa è andato storto!');
            }
          });
        } else {
          notificationHelpers.error('Ops, qualcosa è andato storto!');
        }
      });

      $('#' + idToAppend + ' .fa-trash').on('click', function () {
        var idAddressActive = $(this).attr('data-id-address');
        if (confirm("Sei sicuro di voler eliminare l'indirizzo di spedizione?")) {
          if (functionHelpers.isValued(idAddressActive)) {
            userAddressServices.deleteAddressById(idAddressActive, function (result) {
              window.$('#modal-list-address').modal('hide');
              if (result.data != '') {
                notificationHelpers.success('Indirizzo eliminato correttamente!');
                spinnerHelpers.hide();
                setTimeout(function () {
                  location.reload(true);
                }, 1000);
              } else {
                spinnerHelpers.hide();
                notificationHelpers.error('Ops, qualcosa è andato storto!');
              }
            });
          } else {
            notificationHelpers.error('Ops, qualcosa è andato storto!');
          }
        }
      });

      /* end gestione indirizzi di spedizione */

      spinnerHelpers.hide();
    });
  } else {
    window.location.href = '/login';
  }
}

/***/ }),
/* 47 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAllBySales"] = getAllBySales;
/* harmony export (immutable) */ __webpack_exports__["getByDetailSaleOrder"] = getByDetailSaleOrder;
/* harmony export (immutable) */ __webpack_exports__["insertFileOrderSales"] = insertFileOrderSales;
/* harmony export (immutable) */ __webpack_exports__["sendEmailDetailSummaryOrder"] = sendEmailDetailSummaryOrder;
/* harmony export (immutable) */ __webpack_exports__["getAllBySalesmanId"] = getAllBySalesmanId;
/***   salesOrderHelpers   ***/

function getAllBySales(idToAppend) {
  spinnerHelpers.show();
  if (functionHelpers.isValued(storageData.sUserId())) {
    salesOrderServices.getAllBySales(storageData.sUserId(), storageData.sIdLanguage(), function (result) {
      $('#' + idToAppend).html(result.message);
      $('.list-sale-sorder').on('click', function () {
        salesOrderHelpers.getByDetailSaleOrder($(this).attr('data-id-list-sales-order'), 'body-modal-list-view-order-sales');
        window.$('#Modal-View-Order').modal('show');
      });
      spinnerHelpers.hide();
    });
  } else {
    window.location.href = '/login';
  }
}

function getByDetailSaleOrder(id, idToAppend) {
  spinnerHelpers.show();
  if (functionHelpers.isValued(id)) {
    salesOrderServices.getByDetailSaleOrder(id, storageData.sIdLanguage(), function (result) {
      $('#' + idToAppend).html(result.message);
      spinnerHelpers.hide();
    });
  } else {
    window.location.href = '/login';
  }
}

function insertFileOrderSales(data) {
  spinnerHelpers.show();

  if (data.imgName == "" || data.imgName == null) {
    notificationHelpers.error("Attenzione!Non hai inserito il file pdf!");
    spinnerHelpers.hide();
  } else {
    if (data.imgName.includes('jpg') || data.imgName.includes('png') || data.imgName.includes('gif')) {
      notificationHelpers.error("Attenzione!Puoi solo inserire file con estensione .pdf!");
      spinnerHelpers.hide();
    } else {
      salesOrderServices.insertFileOrderSales(data, function () {
        $("#upload-file-detail #created_id").val("");
        $("#upload-file-detail #imgName").val("");
        $("#upload-file-detail #sales_order_detail_id").val("");
        $("#upload-file-detail #created_id").val("");
        $("#upload-file-detail #imgBase64").val("");
        notificationHelpers.success("Inserimento avvenuto correttamente!");
        spinnerHelpers.hide();
      });
    }
  }
}
function sendEmailDetailSummaryOrder(data) {
  spinnerHelpers.show();

  salesOrderServices.sendEmailDetailSummaryOrder(data, function () {
    notificationHelpers.success("Email inviata con successo!");
    spinnerHelpers.hide();
  });
}

function getAllBySalesmanId(data) {
  spinnerHelpers.show();
  if (functionHelpers.isValued(data.salesmanId)) {
    salesOrderServices.getAllBySalesmanId(data, function (result) {
      $('#' + data.idToAppend).html(result.message);
      $('.list-sale-sorder').on('click', function () {
        salesOrderHelpers.getByDetailSaleOrder($(this).attr('data-id-list-sales-order'), 'body-modal-list-view-order-sales');
        window.$('#Modal-View-Order').modal('show');
      });
      spinnerHelpers.hide();
    });
  } else {
    window.location.href = '/login-agente';
  }
}

/***/ }),
/* 48 */
/***/ (function(module, exports) {



/***/ }),
/* 49 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["modifyQuantity"] = modifyQuantity;
function del(id) {
  spinnerHelpers.show();

  cartDetailServices.del(id, function (result) {
    spinnerHelpers.hide();
    window.location.reload();
  });
}

function modifyQuantity(id, newQuantity) {
  spinnerHelpers.show();
  var data = {
    id: id,
    newQuantity: newQuantity
  };

  cartDetailServices.modifyQuantity(data, function (result) {
    spinnerHelpers.hide();
    window.location.reload();
  });
}

/***/ }),
/* 50 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["search"] = search;
/* harmony export (immutable) */ __webpack_exports__["searchPaginatorDataSheet"] = searchPaginatorDataSheet;
/***   searchDataSheetHelpers   ***/

/***   POST   ***/
function search(description, categories_id, producer_id) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(description, categories_id, producer_id);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  searchDataSheetServices.search(data, function (response) {

    html += "<div class='container div-cont-search'>";
    html += "<div class='row'>";
    html += response.message;
    html += "</div>";
    html += "</div>";
    $('#result').html(html);
    spinnerHelpers.hide();
  });
}

function searchPaginatorDataSheet(pageActive) {
  spinnerHelpers.show();
  var page = pageActive;

  var data = {
    search: $('#frmSearchDatasSheet #search').val(),
    description: $('#frmSearchDatasSheet #description').val(),
    categories_id: $('#frmSearchDatasSheet #categories_id').val(),
    producer_id: $('#frmSearchDatasSheet #producer_id').val(),
    idLanguage: storageData.sIdLanguage(),
    autocomplete: false,
    orderby: $('#frmSearchDatasSheet #order').val(),
    paginator: page
  };
  var TextProd = "";
  if ($('#frmSearchDatasSheet #producer_id').val() != '' && $('#frmSearchDatasSheet #producer_id').val() != null && $('#frmSearchDatasSheet #producer_id').val() != undefined) {
    TextProd = window.$('#frmSearchDatasSheet #producer_id').select2('data')[0]['text'];
  }

  var TextCat = "";
  if ($('#frmSearchDatasSheet #categories_id').val() != '' && $('#frmSearchDatasSheet #categories_id').val() != null && $('#frmSearchDatasSheet #categories_id').val() != undefined) {
    TextCat = window.$('#frmSearchDatasSheet #categories_id').select2('data')[0]['text'];
  }

  var html = "";
  searchDataSheetServices.searchPaginatorDataSheet(data, function (response) {
    var htmlPaginator = "";
    var minus10Page = 1;
    var plus10Page = 1;
    var minus1Page = 1;
    var plus1Page = 1;
    var pageSelected = parseInt(response.data.pageSelected);
    var maxPage = response.data.totalPages;
    window.history.pushState('ricerca scheda tecnica', 'Ricerca', '/schede-tecniche?prod=' + $('#frmSearchDatasSheet #producer_id').val() + '&proddesc=' + TextProd + '&cat=' + $('#frmSearchDatasSheet #categories_id').val() + '&catdesc=' + TextCat + '&description=' + $('#frmSearchDatasSheet #description').val() + '&order=' + $('#frmSearchDatasSheet #order').val() + '&p=' + page);
    html += "<div class='container div-cont-search'>";
    html += "<div class='row'>";
    html += "<div class='col-12'>";
    html += "<h1 class='div-title-search'>Ricerca</h1>";
    html += "</div>";
    html += "</div>";
    html += '<div class="row no-gutters div-paginator">';
    html += '<div class="col-12">';
    html += '<hr>';
    html += '</div>';
    html += '<div class="col-md-6 col-sm-12">';
    html += '<h5>Articoli presenti: <b class="totItems">&nbsp;</b></h5>';
    html += '</div>';
    html += '<div class="col-md-6 col-sm-12">';
    html += '<div class="paginationCont" style="float:right;">';
    html += '<br>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += "<div class='row'>";
    html += response.data.html;
    html += "</div>";
    html += "</div>";
    $('#result').html(html);

    /* funzione gestione paginatore */
    if (response.data.paginationActive) {
      htmlPaginator += "<ul class='pagination'>";
      /* controllo freccia -10 pagine */
      if (pageSelected - 10 <= 1) {
        minus10Page = 1;
      } else {
        minus10Page = pageSelected - 10;
      }

      htmlPaginator += '<li class="page-item" data-page="' + minus10Page + '"><a><i class="fa fa-angle-double-left"></i></a></li>';
      /* end controllo freccia -10 pagine */
      /* controllo freccia -1 pagine */
      if (pageSelected - 1 <= 1) {
        minus1Page = 1;
      } else {
        minus1Page = pageSelected - 1;
      }

      htmlPaginator += '<li class="page-item" data-page="' + minus1Page + '"><a><i class="fa fa-angle-left"></i></a></li>';
      /* end controllo freccia -1 pagine */

      /* controllo per pagine intermedie */
      if (maxPage > 4) {
        if (pageSelected > 1) {
          if (pageSelected + 2 > maxPage) {
            if (pageSelected == maxPage) {
              for (var j = pageSelected - 3; j <= maxPage; j++) {
                if (pageSelected == j) {
                  htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
                } else {
                  htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
                }
              }
            } else {
              for (var j = pageSelected - 2; j <= maxPage; j++) {
                if (pageSelected == j) {
                  htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
                } else {
                  htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
                }
              }
            }
          } else {
            for (var j = pageSelected - 1; j <= pageSelected + 2; j++) {
              if (pageSelected == j) {
                htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
              } else {
                htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
              }
            }
          }
        } else {
          for (var j = 1; j <= 4; j++) {
            if (pageSelected == j) {
              htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
            } else {
              htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
            }
          }
        }
      } else {
        for (var j = 1; j <= maxPiage; j++) {
          if (pageSelected == j) {
            htmlPaginator += '<li class="page-item active" data-page="' + j + '"><a>' + j + '</a></li>';
          } else {
            htmlPaginator += '<li class="page-item" data-page="' + j + '"><a>' + j + '</a></li>';
          }
        }
      }
      /* end controllo per pagine intermedie */

      /* controllo freccia + 1 pagine */
      if (pageSelected + 1 >= maxPage) {
        plus1Page = maxPage;
      } else {
        plus1Page = pageSelected + 1;
      }

      htmlPaginator += '<li class="page-item" data-page="' + plus1Page + '"><a><i class="fa fa-angle-right"></i></a></li>';
      /* end controllo freccia + 1 pagine */
      /* controllo freccia + 10 pagine */
      if (pageSelected + 10 >= maxPage) {
        plus10Page = maxPage;
      } else {
        plus10Page = pageSelected + 10;
      }

      htmlPaginator += '<li class="page-item" data-page="' + plus10Page + '"><a><i class="fa fa-angle-double-right"></i></a></li>';
      /* end controllo freccia + 10 pagine */

      htmlPaginator += "</ul>";
      $('.paginationCont').html(htmlPaginator);
    } else {
      $('.paginationCont').html('');
    }
    $('.totItems').html(response.data.totItems);
    /* end funzione gestione paginatore */
    $('.pagination li').on('click', function () {
      searchDataSheetHelpers.searchPaginatorDataSheet($(this).attr('data-page'));
    });

    spinnerHelpers.hide();
    $('#searchAutocomplete').hide('fast');
  });
}

/***   END POST   ***/

/***/ }),
/* 51 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getMenu"] = getMenu;
/* harmony export (immutable) */ __webpack_exports__["getFunctionality"] = getFunctionality;
/** *   adminConfigurationServices   ***/

/** * GET ***/

function getMenu(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/adminConfiguration/getMenu', fnSuccess, fnError);
}

function getFunctionality(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/adminConfiguration/getFunctionality/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

/** * END GET ***/

/***/ }),
/* 52 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getByCustomer"] = getByCustomer;
/* harmony export (immutable) */ __webpack_exports__["getSelect"] = getSelect;
/** *  carriageServices   ***/

/** *   GET   ***/

function getByCustomer(idCustomer, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/carriage/getByCustomer/' + idCustomer + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

function getSelect(routeParams, fnSuccess, fnError) {
  var data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(routeParams)
  };

  serviceHelpers.getAutenticateWithData('/api/v1/carriage/select', data, fnSuccess, fnError);
}

/** *   END GET   ***/

/***/ }),
/* 53 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["getByCustomer"] = getByCustomer;
/* harmony export (immutable) */ __webpack_exports__["getSelect"] = getSelect;
/* harmony export (immutable) */ __webpack_exports__["getCarrierArea"] = getCarrierArea;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["insertCarrierArea"] = insertCarrierArea;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["updateCarrierRangeArea"] = updateCarrierRangeArea;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["deleteRangeDetailArea"] = deleteRangeDetailArea;
/* harmony export (immutable) */ __webpack_exports__["deleteZoneRangeDetailArea"] = deleteZoneRangeDetailArea;
/** *  carrierServices   ***/

/** *   GET   ***/

function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/carrier/getAll', fnSuccess, fnError);
}

function getByCustomer(idCustomer, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/carrier/getByCustomer/' + idCustomer + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

function getSelect(routeParams, fnSuccess, fnError) {
  var data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(routeParams)
  };

  serviceHelpers.getAutenticateWithData('/api/v1/carrier/select', data, fnSuccess, fnError);
}

function getCarrierArea(idCarrier, idZone, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/carrier/getCarrierArea/' + idCarrier + '/' + idZone, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   INSERT   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/carrier', data, fnSuccess, fnError);
}

function insertCarrierArea(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/carrier/insertCarrierArea', data, fnSuccess, fnError);
}

/** *   END INSERT   ***/

/** *   POST   ***/

function update(request, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/carrier', request, fnSuccess, fnError);
}

function updateCarrierRangeArea(request, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/carrier/updateCarrierRangeArea', request, fnSuccess, fnError);
}

/** *   END POST   ***/

/** * DELETE ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/carrier/' + id, fnSuccess, fnError);
}

function deleteRangeDetailArea(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/carrier/deleteRangeDetailArea/' + id, fnSuccess, fnError);
}

function deleteZoneRangeDetailArea(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/carrier/deleteZoneRangeDetailArea', data, fnSuccess, fnError);
}

/** * DELETE ***/

/***/ }),
/* 54 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["getList"] = getList;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["getAllListNavigation"] = getAllListNavigation;
/* harmony export (immutable) */ __webpack_exports__["getAllListNavigationFather"] = getAllListNavigationFather;
/* harmony export (immutable) */ __webpack_exports__["getAllDetailListCategories"] = getAllDetailListCategories;
/* harmony export (immutable) */ __webpack_exports__["getAllDetailListCategoriesFather"] = getAllDetailListCategoriesFather;
/* harmony export (immutable) */ __webpack_exports__["getImages"] = getImages;
/* harmony export (immutable) */ __webpack_exports__["getAllImagesInFolder"] = getAllImagesInFolder;
/* harmony export (immutable) */ __webpack_exports__["getCategoriesLanguages"] = getCategoriesLanguages;
/* harmony export (immutable) */ __webpack_exports__["getByTranslateDefault"] = getByTranslateDefault;
/* harmony export (immutable) */ __webpack_exports__["getByTranslateDefaultNew"] = getByTranslateDefaultNew;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["insertCategoriesFather"] = insertCategoriesFather;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["updateOrderCategoryItem"] = updateOrderCategoryItem;
/* harmony export (immutable) */ __webpack_exports__["updateOrderCategoryFather"] = updateOrderCategoryFather;
/* harmony export (immutable) */ __webpack_exports__["updateOnlineCategories"] = updateOnlineCategories;
/* harmony export (immutable) */ __webpack_exports__["updateImageName"] = updateImageName;
/* harmony export (immutable) */ __webpack_exports__["updateCategoriesImages"] = updateCategoriesImages;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["deleteCategoriesFather"] = deleteCategoriesFather;
/* harmony export (immutable) */ __webpack_exports__["clone"] = clone;
/** *   categoriesServices   ***/

/** *   GET   */

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/Categories/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Categories/select?search=' + search, fnSuccess, fnError);
}

function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Categories/' + id, fnSuccess, fnError);
}

function getList(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/Categories/getList', data, fnSuccess, fnError);
}

function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Categories/' + id, fnSuccess, fnError);
}

function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/Categories/all/' + idLanguage, fnSuccess, fnError);
}

function getAllListNavigation(idLanguage, idItem, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/Categories/getAllListNavigation/' + idLanguage + '/' + idItem, fnSuccess, fnError);
}

function getAllListNavigationFather(idLanguage, idItem, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/Categories/getAllListNavigationFather/' + idLanguage + '/' + idItem, fnSuccess, fnError);
}

function getAllDetailListCategories(idLanguage, idItem, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/Categories/getAllDetailListCategories/' + idLanguage + '/' + idItem, fnSuccess, fnError);
}

function getAllDetailListCategoriesFather(idLanguage, idCategories, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/Categories/getAllDetailListCategoriesFather/' + idLanguage + '/' + idCategories, fnSuccess, fnError);
}

function getImages(idCategories, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Categories/getImages/' + idCategories, fnSuccess, fnError);
}

function getAllImagesInFolder(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Categories/getAllImagesInFolder/' + id, fnSuccess, fnError);
}

function getCategoriesLanguages(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Categories/getCategoriesLanguages/' + id, fnSuccess, fnError);
}

function getByTranslateDefault(id, idItem, idLanguage, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Categories/getByTranslateDefault/' + id + '/' + idItem + '/' + idLanguage, fnSuccess, fnError);
}

function getByTranslateDefaultNew(idItem, idLanguage, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Categories/getByTranslateDefaultNew/' + idItem + '/' + idLanguage, fnSuccess, fnError);
}

/** *   END GET   */

/** *   PUT   ***/

function insert(DtoCategories, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/Categories', DtoCategories, fnSuccess, fnError);
}

function insertCategoriesFather(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/Categories/insertCategoriesFather', data, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(DtoCategories, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/Categories', DtoCategories, fnSuccess, fnError);
}

function updateOrderCategoryItem(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/Categories/updateOrderCategoryItem', data, fnSuccess, fnError);
}

function updateOrderCategoryFather(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/Categories/updateOrderCategoryFather', data, fnSuccess, fnError);
}

function updateOnlineCategories(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/Categories/updateOnlineCategories', data, fnSuccess, fnError);
}

function updateImageName(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/Categories/updateImageName', data, fnSuccess, fnError);
}

function updateCategoriesImages(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/Categories/updateCategoriesImages', data, fnSuccess, fnError);
}

/** *   END POST   ***/

/** * DELETE ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/Categories/' + id, fnSuccess, fnError);
}

function deleteCategoriesFather(idCategories, idCategoriesFather, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/Categories/deleteCategoriesFather/' + idCategories + '/' + idCategoriesFather, fnSuccess, fnError);
}

/** * END DELETE ***/

/** *   clone ***/
function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/Categories/clone/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), null, fnSuccess, fnError);
}
/** *   END  clone ***/

/***/ }),
/* 55 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   categoryTypeServices   ***/

/** *   GET   */

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/CategoryType/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/CategoryType/select?search=' + search, fnSuccess, fnError);
}
function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/CategoryType/' + id, fnSuccess, fnError);
}

function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/CategoryType/' + id, fnSuccess, fnError);
}

function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/CategoryType/all/' + idLanguage, fnSuccess, fnError);
}

/** *   END GET   */

/** *   PUT   ***/

function insert(DtoCategoryType, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/CategoryType', DtoCategoryType, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(DtoCategoryType, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/CategoryType', DtoCategoryType, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/CategoryType/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 56 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["importData"] = importData;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["subscribe"] = subscribe;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["unsubscribe"] = unsubscribe;
/* harmony export (immutable) */ __webpack_exports__["unsubscribeNewsletter"] = unsubscribeNewsletter;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   contactServices   ***/

/** *   GET   ***/

function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/contact/' + id, fnSuccess, fnError);
}

function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/contact/all', fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function importData(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/contact/importData', data, fnSuccess, fnError);
}

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/contact', data, fnSuccess, fnError);
}

function subscribe(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/contact/subscribe', data, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(dtoCustomer, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/contact', dtoCustomer, fnSuccess, fnError);
}

function unsubscribe(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/contact/unsubscribe', data, fnSuccess, fnError);
}
function unsubscribeNewsletter(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/contact/unsubscribeNewsletter', data, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/contact/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 57 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getIncludedByContactGroup"] = getIncludedByContactGroup;
/* harmony export (immutable) */ __webpack_exports__["getExcludedByContactGroup"] = getExcludedByContactGroup;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   contactGroupNewsletterServices   ***/

/** *   GET   ***/

function getIncludedByContactGroup(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/contactgroupnewsletter/includedgroup/' + id, fnSuccess, fnError);
}

function getExcludedByContactGroup(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/contactgroupnewsletter/excludedgroup/' + id, fnSuccess, fnError);
}

/** *   END GET   ***/

/** * PUT ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/contactgroupnewsletter', data, fnSuccess, fnError);
}

/** * END PUT ***/

/** * DELETE ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/contactgroupnewsletter/' + id, fnSuccess, fnError);
}

/** * END DELETE ***/

/***/ }),
/* 58 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getIncludedByCustomerGroup"] = getIncludedByCustomerGroup;
/* harmony export (immutable) */ __webpack_exports__["getExcludedByCustomerGroup"] = getExcludedByCustomerGroup;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   customerGroupNewsletterServices   ***/

/** *   GET   ***/

function getIncludedByCustomerGroup(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/customergroupnewsletter/includedgroup/' + id, fnSuccess, fnError);
}

function getExcludedByCustomerGroup(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/customergroupnewsletter/excludedgroup/' + id, fnSuccess, fnError);
}

/** *   END GET   ***/

/** * PUT ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/customergroupnewsletter', data, fnSuccess, fnError);
}

/** * END PUT ***/

/** * DELETE ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/customergroupnewsletter/' + id, fnSuccess, fnError);
}

/** * END DELETE ***/

/***/ }),
/* 59 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["insertTourAvailable"] = insertTourAvailable;
/* harmony export (immutable) */ __webpack_exports__["insertRequestLanguage"] = insertRequestLanguage;
/* harmony export (immutable) */ __webpack_exports__["insertWorkForUs"] = insertWorkForUs;
/* harmony export (immutable) */ __webpack_exports__["insertLandingeBookPcb"] = insertLandingeBookPcb;
/* harmony export (immutable) */ __webpack_exports__["insertLogsContacts"] = insertLogsContacts;
/***   contactRequestServices   ***/

/***   GET   ***/

function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/contactrequest/all', fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/contactrequest', data, fnSuccess, fnError);
}

function insertTourAvailable(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/contactrequest/insertTourAvailable', data, fnSuccess, fnError);
}

function insertRequestLanguage(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/contactrequest/insertRequestLanguage', data, fnSuccess, fnError);
}

function insertWorkForUs(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/contactrequest/insertworkforus', data, fnSuccess, fnError);
}

function insertLandingeBookPcb(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/contactrequest/insertLandingeBookPcb', data, fnSuccess, fnError);
}

function insertLogsContacts(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/contactrequest/insertLogsContacts', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***/ }),
/* 60 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["getByLanguage"] = getByLanguage;
/* harmony export (immutable) */ __webpack_exports__["getByCode"] = getByCode;
/* harmony export (immutable) */ __webpack_exports__["getByCodeRender"] = getByCodeRender;
/* harmony export (immutable) */ __webpack_exports__["getTagByType"] = getTagByType;
/* harmony export (immutable) */ __webpack_exports__["getBreadcrumbs"] = getBreadcrumbs;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["deleteLanguage"] = deleteLanguage;
/***   contentServices   ***/

/***   GET   ***/

function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate("/api/v1/content/getAll/" + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

function getByLanguage(idContent, idLanguage, fnSuccess, fnError) {
  serviceHelpers.getAutenticate("/api/v1/content/getByLanguage/" + idContent + "/" + idLanguage + "/" + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

function getByCode(code, idLanguage, fnSuccess, fnError) {
  var url = "/api/v1/content/getByCode/" + code;

  if (functionHelpers.isValued(idLanguage)) {
    url += "/" + idLanguage;
  }
  serviceHelpers.get(url, fnSuccess, fnError);
}

function getByCodeRender(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/content/getByCodeRender', data, fnSuccess, fnError);
}

function getTagByType(code, fnSuccess, fnError) {
  var url = "/api/v1/content/getTagByType/" + code;

  serviceHelpers.getAutenticate(url, fnSuccess, fnError);
}

function getBreadcrumbs(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/content/getBreadcrumbs', data, fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

function insert(code, idLanguage, content, fnSuccess, fnError) {
  var data = {
    code: code,
    idLanguage: idLanguage,
    content: content,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl()
  };

  serviceHelpers.putAutenticate("/api/v1/content", data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

function update(id, code, idLanguage, content, fnSuccess, fnError) {
  var data = {
    id: id,
    code: code,
    idLanguage: idLanguage,
    content: content,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl()
  };

  serviceHelpers.postAutenticate("/api/v1/content", data, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate("/api/v1/content/" + id + "/" + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

function deleteLanguage(idContent, idLanguage, fnSuccess, fnError) {
  serviceHelpers.delAutenticate("/api/v1/content/deleteLanguage/" + idContent + "/" + idLanguage + "/" + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

/***   END DELETE   ***/

/***/ }),
/* 61 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["selectBeforeDDTSixten"] = selectBeforeDDTSixten;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   customerServices   ***/

/** *   GET   ***/

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/customer/select?search' + search, fnSuccess, fnError);
}

function selectBeforeDDTSixten(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/customer/selectBeforeDDTSixten?search' + search, fnSuccess, fnError);
}

function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/customer/' + id, fnSuccess, fnError);
}

function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/customer/all', fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(dtoCustomer, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/customer', dtoCustomer, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(dtoCustomer, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/customer', dtoCustomer, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/customer/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 62 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["setByLanguage"] = setByLanguage;
/** *  dictionaryServices   ***/

/** *   GET   ***/

function setByLanguage(fnSuccess, fnError) {
  if (functionHelpers.isValued(storageData.sIdLanguage())) {
    serviceHelpers.get('/api/v1/dictionary/getByLanguage/' + storageData.sIdLanguage(), fnSuccess, fnError);
  }

  // $.ajax({
  //     type: 'GET',
  //     url: configData.wsRootServicesUrl + '/api/v1/dictionary/getByLanguage/' + storageData.sIdLanguage(),
  //     dataType: 'json',
  //     headers: { Accept: "application/json" },
  //     contentType: 'application/x-www-form-urlencoded; charset=UTF-8'
  // }).done(function (result) {
  //     /*** DICTIONARY */
  //     storageData.setDictionary(result.data);
  //     /*** MESSAGGE VALIDATION ***/
  //     if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //         fnSuccess();
  //     }
  // }).fail(function (response) {
  //     failServiceHelpers.fail(response);
  // });
}

/** *   END GET   ***/

/***/ }),
/* 63 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSixtenOrderConfirmed"] = getSixtenOrderConfirmed;
/* harmony export (immutable) */ __webpack_exports__["getSixtenOrderPicked"] = getSixtenOrderPicked;
/* harmony export (immutable) */ __webpack_exports__["getSelectMonthBeforeDDTSixten"] = getSelectMonthBeforeDDTSixten;
/* harmony export (immutable) */ __webpack_exports__["getMdMicrodetectorsShippingList"] = getMdMicrodetectorsShippingList;
/* harmony export (immutable) */ __webpack_exports__["confirmBeforeDDTSixten"] = confirmBeforeDDTSixten;
/** *  documentHeadServices   ***/

/** *   GET   ***/

function getSixtenOrderConfirmed(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/documenthead/getSixtenOrderConfirmed/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/documenthead/getSixtenOrderConfirmed/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function getSixtenOrderPicked(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/documenthead/getSixtenOrderPicked/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/documenthead/getSixtenOrderConfirmed/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function getSelectMonthBeforeDDTSixten(idCustomer, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/documenthead/selectMonthBeforeDDTSixten/' + idCustomer + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/documenthead/selectMonthBeforeDDTSixten/' + idCustomer + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function getMdMicrodetectorsShippingList(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/documenthead/getMdMicrodetectorsShippingList/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/documenthead/getSixtenOrderConfirmed/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END GET   ***/

/** *   UPDATE   ***/

function confirmBeforeDDTSixten(idCustomer, idMonth, idCarriage, idCarrier, idPackagingTypeSixten, idShipmentCodeSixten, weight, packagingNumber, routeParameter, fnSuccess, fnError) {
  var data = {
    idCustomer: idCustomer,
    idMonth: idMonth,
    idCarriage: idCarriage,
    idCarrier: idCarrier,
    idPackagingTypeSixten: idPackagingTypeSixten,
    idShipmentCodeSixten: idShipmentCodeSixten,
    weight: weight,
    packagingNumber: packagingNumber,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(routeParameter)
  };

  serviceHelpers.postAutenticate('/api/v1/documenthead/confirmBeforeDDTSixten', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'POST',
  //   url: configData.wsRootServicesUrl + '/api/v1/documenthead/confirmBeforeDDTSixten',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END UPDATE   ***/

/***/ }),
/* 64 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSixtenByHead"] = getSixtenByHead;
/* harmony export (immutable) */ __webpack_exports__["getMdMicrodetectorsByHead"] = getMdMicrodetectorsByHead;
/* harmony export (immutable) */ __webpack_exports__["getByErp"] = getByErp;
/* harmony export (immutable) */ __webpack_exports__["getNumberPackageDDT"] = getNumberPackageDDT;
/* harmony export (immutable) */ __webpack_exports__["insertBeforeDDT"] = insertBeforeDDT;
/* harmony export (immutable) */ __webpack_exports__["updateMdMicrodetectorsShippingRow"] = updateMdMicrodetectorsShippingRow;
/* harmony export (immutable) */ __webpack_exports__["updateMdMicrodetectorsResetShippingRow"] = updateMdMicrodetectorsResetShippingRow;
/** *  documentRowServices   ***/

/** *   GET   ***/

function getSixtenByHead(idHead, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/documentrow/getSixtenByHead/' + idHead + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //     type: "GET",
  //     url: configData.wsRootServicesUrl +
  //       "/api/v1/documentrow/getSixtenByHead/" +
  //       idHead +
  //       "/" +
  //       menuHelpers.getIdFunctionByAdminUrl(),
  //     dataType: "json",
  //     headers: {
  //       Authorization: storageData.sTokenKey()
  //     },
  //     contentType: "application/json"
  //   })
  //   .done(function (result) {
  //     if (
  //       functionHelpers.isValued(fnSuccess) &&
  //       functionHelpers.isFunction(fnSuccess)
  //     ) {
  //       fnSuccess(result);
  //     }
  //   })
  //   .fail(function (response) {
  //     failServiceHelpers.fail(response);
  //   });
}

function getMdMicrodetectorsByHead(idHead, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/documentrow/getMdMicrodetectorsByHead/' + idHead, fnSuccess, fnError);

  // $.ajax({
  //     type: "GET",
  //     url: configData.wsRootServicesUrl +
  //       "/api/v1/documentrow/getSixtenByHead/" +
  //       idHead +
  //       "/" +
  //       menuHelpers.getIdFunctionByAdminUrl(),
  //     dataType: "json",
  //     headers: {
  //       Authorization: storageData.sTokenKey()
  //     },
  //     contentType: "application/json"
  //   })
  //   .done(function (result) {
  //     if (
  //       functionHelpers.isValued(fnSuccess) &&
  //       functionHelpers.isFunction(fnSuccess)
  //     ) {
  //       fnSuccess(result);
  //     }
  //   })
  //   .fail(function (response) {
  //     failServiceHelpers.fail(response);
  //   });
}

function getByErp(idErp, routeParameter, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/documentrow/getByErp/' + idErp + '/' + menuHelpers.getIdFunctionByAdminUrl(routeParameter), fnSuccess, fnError);

  // $.ajax({
  //     type: "GET",
  //     url: configData.wsRootServicesUrl + "/api/v1/documentrow/getByErp/" + idErp + "/" + menuHelpers.getIdFunctionByAdminUrl(routeParameter),
  //     dataType: "json",
  //     headers: {
  //       Authorization: storageData.sTokenKey()
  //     },
  //     contentType: "application/json"
  //   })
  //   .done(function (result) {
  //     if (
  //       functionHelpers.isValued(fnSuccess) &&
  //       functionHelpers.isFunction(fnSuccess)
  //     ) {
  //       fnSuccess(result);
  //     }
  //   })
  //   .fail(function (response) {
  //     failServiceHelpers.fail(response);
  //   });
}

function getNumberPackageDDT(idCustomer, idMonth, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/documentrow/getNumberPackageDDT/' + idCustomer + '/' + idMonth + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //     type: "GET",
  //     url: configData.wsRootServicesUrl + "/api/v1/documentrow/getNumberPackageDDT/" + idCustomer + '/' + idMonth + "/" + menuHelpers.getIdFunctionByAdminUrl(),
  //     dataType: "json",
  //     headers: {
  //       Authorization: storageData.sTokenKey()
  //     },
  //     contentType: "application/json"
  //   })
  //   .done(function (result) {
  //     if (
  //       functionHelpers.isValued(fnSuccess) &&
  //       functionHelpers.isFunction(fnSuccess)
  //     ) {
  //       fnSuccess(result);
  //     }
  //   })
  //   .fail(function (response) {
  //     failServiceHelpers.fail(response);
  //   });
}

/** *   END GET   ***/

/** *   INSERT   ***/

function insertBeforeDDT(idErp, quantity, idPackage, routeParameter, fnSuccess, fnError) {
  var data = {
    idErp: idErp,
    idPackage: idPackage,
    quantity: quantity,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(routeParameter)
  };

  serviceHelpers.putAutenticate('/api/v1/documentrow/insertBeforeDDT', data, fnSuccess, fnError);

  // $.ajax({
  //     type: "PUT",
  //     url: configData.wsRootServicesUrl + "/api/v1/documentrow/insertBeforeDDT",
  //     dataType: "json",
  //     headers: {
  //       Authorization: storageData.sTokenKey()
  //     },
  //     contentType: "application/json",
  //     data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  //   })
  //   .done(function (result) {
  //     if (
  //       functionHelpers.isValued(fnSuccess) &&
  //       functionHelpers.isFunction(fnSuccess)
  //     ) {
  //       fnSuccess(result);
  //     }
  //   })
  //   .fail(function (response) {
  //     failServiceHelpers.fail(response);
  //   });
}

/** *   END INSERT   ***/

/** *   UPDATE   ***/

function updateMdMicrodetectorsShippingRow(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/documentrow/updateMdMicrodetectorsShippingRow', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'POST',
  //   url: configData.wsRootServicesUrl + '/api/v1/documenthead/confirmBeforeDDTSixten',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function updateMdMicrodetectorsResetShippingRow(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/documentrow/updateMdMicrodetectorsResetShippingRow', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'POST',
  //   url: configData.wsRootServicesUrl + '/api/v1/documenthead/confirmBeforeDDTSixten',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END UPDATE   ***/

/***/ }),
/* 65 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAllByNewsletter"] = getAllByNewsletter;
/* harmony export (immutable) */ __webpack_exports__["getChart"] = getChart;
/** *   feedbackNewsletterServices   ***/

/** *   GET   ***/

function getAllByNewsletter(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/reportnewsletter/allByNewsletter/' + id, fnSuccess, fnError);
}

function getChart(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/reportnewsletter/getChart/' + id, fnSuccess, fnError);
}

/** *   END GET   ***/

/***/ }),
/* 66 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getTotalUser"] = getTotalUser;
/* harmony export (immutable) */ __webpack_exports__["getTotalSession"] = getTotalSession;
/* harmony export (immutable) */ __webpack_exports__["getTotalPageView"] = getTotalPageView;
/* harmony export (immutable) */ __webpack_exports__["getPageViewPerSession"] = getPageViewPerSession;
/* harmony export (immutable) */ __webpack_exports__["getAvgSessionDuration"] = getAvgSessionDuration;
/* harmony export (immutable) */ __webpack_exports__["getBounceRate"] = getBounceRate;
/* harmony export (immutable) */ __webpack_exports__["getUserActiveRealTime"] = getUserActiveRealTime;
/* harmony export (immutable) */ __webpack_exports__["getMostVisitedPages"] = getMostVisitedPages;
/* harmony export (immutable) */ __webpack_exports__["getPagesViews"] = getPagesViews;
/* harmony export (immutable) */ __webpack_exports__["getVisitorsViews"] = getVisitorsViews;
/* harmony export (immutable) */ __webpack_exports__["getSourceSession"] = getSourceSession;
/* harmony export (immutable) */ __webpack_exports__["getReturningVisitors"] = getReturningVisitors;
/* harmony export (immutable) */ __webpack_exports__["getDeviceSession"] = getDeviceSession;
/** *   googleAnalyticsServices   ***/

/** *   GET   ***/

/** *   TOTALI ***/

function getTotalUser(startDate, endDate, fnSuccess, fnError) {
  var data = { start: startDate, end: endDate };
  serviceHelpers.postAutenticate('/api/v1/googleAnalytics/getTotalUser', data, fnSuccess, fnError);
}

function getTotalSession(startDate, endDate, fnSuccess, fnError) {
  var data = { start: startDate, end: endDate };
  serviceHelpers.postAutenticate('/api/v1/googleAnalytics/getTotalSession', data, fnSuccess, fnError);
}

function getTotalPageView(startDate, endDate, fnSuccess, fnError) {
  var data = { start: startDate, end: endDate };
  serviceHelpers.postAutenticate('/api/v1/googleAnalytics/getTotalPageView', data, fnSuccess, fnError);
}

function getPageViewPerSession(startDate, endDate, fnSuccess, fnError) {
  var data = { start: startDate, end: endDate };
  serviceHelpers.postAutenticate('/api/v1/googleAnalytics/getPageViewPerSession', data, fnSuccess, fnError);
}

function getAvgSessionDuration(startDate, endDate, fnSuccess, fnError) {
  var data = { start: startDate, end: endDate };
  serviceHelpers.postAutenticate('/api/v1/googleAnalytics/getAvgSessionDuration', data, fnSuccess, fnError);
}

function getBounceRate(startDate, endDate, fnSuccess, fnError) {
  var data = { start: startDate, end: endDate };
  serviceHelpers.postAutenticate('/api/v1/googleAnalytics/getBounceRate', data, fnSuccess, fnError);
}

function getUserActiveRealTime(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/googleAnalytics/getUserActiveRealTime', fnSuccess, fnError);
}

/** *   END TOTALI   ***/

function getMostVisitedPages(startDate, endDate, fnSuccess, fnError) {
  var data = { start: startDate, end: endDate };
  serviceHelpers.postAutenticate('/api/v1/googleAnalytics/getMostVisitedPages', data, fnSuccess, fnError);
}

function getPagesViews(startDate, endDate, fnSuccess, fnError) {
  var data = { start: startDate, end: endDate };

  serviceHelpers.postAutenticate('/api/v1/googleAnalytics/getPagesViews', data, fnSuccess, fnError);
}

function getVisitorsViews(startDate, endDate, fnSuccess, fnError) {
  var data = { start: startDate, end: endDate };
  serviceHelpers.postAutenticate('/api/v1/googleAnalytics/getVisitorsViews', data, fnSuccess, fnError);
}

function getSourceSession(startDate, endDate, fnSuccess, fnError) {
  var data = { start: startDate, end: endDate };
  serviceHelpers.postAutenticate('/api/v1/googleAnalytics/getSourceSession', data, fnSuccess, fnError);
}

function getReturningVisitors(startDate, endDate, fnSuccess, fnError) {
  var data = { start: startDate, end: endDate };
  serviceHelpers.postAutenticate('/api/v1/googleAnalytics/getReturningVisitors', data, fnSuccess, fnError);
}

function getDeviceSession(startDate, endDate, fnSuccess, fnError) {
  var data = { start: startDate, end: endDate };
  serviceHelpers.postAutenticate('/api/v1/googleAnalytics/getDeviceSession', data, fnSuccess, fnError);
}

/** *   END GET   ***/

/***/ }),
/* 67 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getDtoTable"] = getDtoTable;
/* harmony export (immutable) */ __webpack_exports__["getTechnicalSpecification"] = getTechnicalSpecification;
/* harmony export (immutable) */ __webpack_exports__["checkExists"] = checkExists;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["addTechnicalSpecification"] = addTechnicalSpecification;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["updateTechnicalSpecification"] = updateTechnicalSpecification;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["deleteGroupDisplayTechnicalSpecificationTechnicalSpecification"] = deleteGroupDisplayTechnicalSpecificationTechnicalSpecification;
/** *   groupDisplayTechnicalSpecificationServices   ***/

/** *   GET   ***/

function getDtoTable(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/groupdisplaytechnicalspecification/getDtoTable', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification/getDtoTable',
  //   dataType: 'json',
  //   headers: { Authorization: storageData.sTokenKey() },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function getTechnicalSpecification(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/groupdisplaytechnicalspecification/getTechnicalSpecification/' + id, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification/getTechnicalSpecification/' + id,
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function checkExists(description, idLanguage, id, fnSuccess, fnError) {
  var url = configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification/checkExists/' + description + '/' + idLanguage;

  if (functionHelpers.isValued(id)) {
    url = url + '/' + id;
  }
  serviceHelpers.getAutenticate(url, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: url,
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/groupdisplaytechnicalspecification', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(dtoGroupDisplayTechnicalSpecification))
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function addTechnicalSpecification(id, idTechnicalSpecification, fnSuccess, fnError) {
  serviceHelpers.putAutenticateWithoutData('/api/v1/groupdisplaytechnicalspecification/addTechnicalSpecification/' + id + '/' + idTechnicalSpecification + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification/addTechnicalSpecification/' + id + '/' + idTechnicalSpecification + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/groupdisplaytechnicalspecification', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'POST',
  //   url: configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(dtoGroupDisplayTechnicalSpecification))
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

function updateTechnicalSpecification(id, order, fnSuccess, fnError) {
  serviceHelpers.postAutenticateWithoutData('/api/v1/groupdisplaytechnicalspecification/updateTechnicalSpecification/' + id + '/' + order + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'POST',
  //   url: configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification/updateTechnicalSpecification/' + id + '/' + order + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/groupdisplaytechnicalspecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'DELETE',
  //   url: configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

function deleteGroupDisplayTechnicalSpecificationTechnicalSpecification(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/groupdisplaytechnicalspecification/deleteGroupDisplayTechnicalSpecificationTechnicalSpecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'DELETE',
  //   url: configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification/deleteGroupDisplayTechnicalSpecificationTechnicalSpecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

/** *   END DELETE   ***/

/***/ }),
/* 68 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getDtoTable"] = getDtoTable;
/* harmony export (immutable) */ __webpack_exports__["getItems"] = getItems;
/* harmony export (immutable) */ __webpack_exports__["getTechnicalSpecification"] = getTechnicalSpecification;
/* harmony export (immutable) */ __webpack_exports__["getGraphic"] = getGraphic;
/* harmony export (immutable) */ __webpack_exports__["checkExists"] = checkExists;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["addItem"] = addItem;
/* harmony export (immutable) */ __webpack_exports__["addTechnicalSpecification"] = addTechnicalSpecification;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["deleteItemGroupTechnicalSpecification"] = deleteItemGroupTechnicalSpecification;
/* harmony export (immutable) */ __webpack_exports__["deleteGroupTechnicalSpecificationTechnicalSpecification"] = deleteGroupTechnicalSpecificationTechnicalSpecification;
/** *   groupTechnicalSpecificationServices   ***/

/** *   GET   ***/

function getDtoTable(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/grouptechnicalspecification/getDtoTable', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/getDtoTable',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function getItems(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/grouptechnicalspecification/getItems/' + id, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/getItems/' + id,
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function getTechnicalSpecification(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/grouptechnicalspecification/getTechnicalSpecification/' + id, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/getTechnicalSpecification/' + id,
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function getGraphic(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/grouptechnicalspecification/getGraphic', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/getGraphic',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function checkExists(description, idLanguage, id, fnSuccess, fnError) {
  var url = configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/checkExists/' + description + '/' + idLanguage;

  if (functionHelpers.isValued(id)) {
    url = url + '/' + id;
  }

  serviceHelpers.getAutenticate(url, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: url,
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/grouptechnicalspecification', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(dtoGroupTechnicalSpecification))
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function addItem(id, idItem, fnSuccess, fnError) {
  serviceHelpers.putAutenticateWithoutData('/api/v1/grouptechnicalspecification/addItem/' + id + '/' + idItem + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/addItem/' + id + '/' + idItem + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function addTechnicalSpecification(id, idTechnicalSpecification, fnSuccess, fnError) {
  serviceHelpers.putAutenticateWithoutData('/api/v1/grouptechnicalspecification/addTechnicalSpecification/' + id + '/' + idTechnicalSpecification + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/addTechnicalSpecification/' + id + '/' + idTechnicalSpecification + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/grouptechnicalspecification/addTechnicalSpecification', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'POST',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(dtoGroupTechnicalSpecification))
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/grouptechnicalspecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'DELETE',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

function deleteItemGroupTechnicalSpecification(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/grouptechnicalspecification/deleteItemGroupTechnicalSpecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'DELETE',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/deleteItemGroupTechnicalSpecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

function deleteGroupTechnicalSpecificationTechnicalSpecification(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/grouptechnicalspecification/deleteGroupTechnicalSpecificationTechnicalSpecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'DELETE',
  //   url: configData.wsRootServicesUrl + '/api/v1/grouptechnicalspecification/deleteGroupTechnicalSpecificationTechnicalSpecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

/** *   END DELETE   ***/

/***/ }),
/* 69 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   groupNewsletterServices   ***/

/** *   GET   ***/

/* export function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET', 
      url: configData.wsRootServicesUrl + '/api/v1/postalcode/select',
      dataType: 'json',
      headers: {
        Accept: "application/json",
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function (params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function (result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/postalcode/select?search=' + search, fnSuccess, fnError);
} */

function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/groupnewsletter/' + id, fnSuccess, fnError);
}

function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/groupnewsletter/' + id, fnSuccess, fnError);
}

function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/groupnewsletter/all', fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(dtoVatType, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/groupnewsletter', dtoVatType, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(dtoVatType, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/groupnewsletter', dtoVatType, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/groupnewsletter/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 70 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/** *   iconServices   ***/

/** *   GET   ***/

function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/icon/' + id, fnSuccess, fnError);
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/icon/select?search' + search, fnSuccess, fnError);
}

/** *   END GET   ***/

/***/ }),
/* 71 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["getAllRelated"] = getAllRelated;
/* harmony export (immutable) */ __webpack_exports__["getAllRelatedSearch"] = getAllRelatedSearch;
/* harmony export (immutable) */ __webpack_exports__["getByImgAgg"] = getByImgAgg;
/* harmony export (immutable) */ __webpack_exports__["getByPriceList"] = getByPriceList;
/* harmony export (immutable) */ __webpack_exports__["getListItems"] = getListItems;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getWithGroupTechnicalSpecification"] = getWithGroupTechnicalSpecification;
/* harmony export (immutable) */ __webpack_exports__["getGraphic"] = getGraphic;
/* harmony export (immutable) */ __webpack_exports__["count"] = count;
/* harmony export (immutable) */ __webpack_exports__["getImages"] = getImages;
/* harmony export (immutable) */ __webpack_exports__["getAllImagesInFolder"] = getAllImagesInFolder;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["insertTranslate"] = insertTranslate;
/* harmony export (immutable) */ __webpack_exports__["insertRelated"] = insertRelated;
/* harmony export (immutable) */ __webpack_exports__["insertItemCategories"] = insertItemCategories;
/* harmony export (immutable) */ __webpack_exports__["insertItemImages"] = insertItemImages;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["addFavoritesItem"] = addFavoritesItem;
/* harmony export (immutable) */ __webpack_exports__["updateImageName"] = updateImageName;
/* harmony export (immutable) */ __webpack_exports__["updateOrderImamePhotos"] = updateOrderImamePhotos;
/* harmony export (immutable) */ __webpack_exports__["updateItemImages"] = updateItemImages;
/* harmony export (immutable) */ __webpack_exports__["updateItemCheckAssociation"] = updateItemCheckAssociation;
/* harmony export (immutable) */ __webpack_exports__["updateOnlineItem"] = updateOnlineItem;
/* harmony export (immutable) */ __webpack_exports__["updateOnlineItemCheck"] = updateOnlineItemCheck;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["deleteItemCategories"] = deleteItemCategories;
/* harmony export (immutable) */ __webpack_exports__["deleteItemsPhotos"] = deleteItemsPhotos;
/* harmony export (immutable) */ __webpack_exports__["getItemsUserShop"] = getItemsUserShop;
/* harmony export (immutable) */ __webpack_exports__["getItemsCategoriesFilter"] = getItemsCategoriesFilter;
/* harmony export (immutable) */ __webpack_exports__["getItemById"] = getItemById;
/* harmony export (immutable) */ __webpack_exports__["getByFile"] = getByFile;
/* harmony export (immutable) */ __webpack_exports__["getByFileNoId"] = getByFileNoId;
/* harmony export (immutable) */ __webpack_exports__["getItemLanguages"] = getItemLanguages;
/* harmony export (immutable) */ __webpack_exports__["getByTranslate"] = getByTranslate;
/* harmony export (immutable) */ __webpack_exports__["getByTranslateDefault"] = getByTranslateDefault;
/* harmony export (immutable) */ __webpack_exports__["getByTranslateDefaultNew"] = getByTranslateDefaultNew;
/* harmony export (immutable) */ __webpack_exports__["updateImages"] = updateImages;
/* harmony export (immutable) */ __webpack_exports__["updateTranslate"] = updateTranslate;
/* harmony export (immutable) */ __webpack_exports__["updateNoAuth"] = updateNoAuth;
/* harmony export (immutable) */ __webpack_exports__["RemoveFileDb"] = RemoveFileDb;
/* harmony export (immutable) */ __webpack_exports__["removeFile"] = removeFile;
/* harmony export (immutable) */ __webpack_exports__["getCancelTranslate"] = getCancelTranslate;
/* harmony export (immutable) */ __webpack_exports__["clone"] = clone;
/** *   itemServices   ***/

/** *   GET   ***/

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/item/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/select?search=' + search, fnSuccess, fnError);
}
function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/' + id, fnSuccess, fnError);
}

function getAllRelated(id, idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/item/getAllRelated/' + id + '/' + idLanguage, fnSuccess, fnError);
}

function getAllRelatedSearch(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/getAllRelatedSearch', data, fnSuccess, fnError);
}

function getByImgAgg(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getByImgAgg/' + id, fnSuccess, fnError);
}

function getByPriceList(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getByPriceList/' + id, fnSuccess, fnError);
}

function getListItems(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/item/getListItems/' + id, fnSuccess, fnError);
}

function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/item/all/' + idLanguage, fnSuccess, fnError);
}

function get(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/item',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function getWithGroupTechnicalSpecification(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/withGroupTechnicalSpecification', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/item/withGroupTechnicalSpecification',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function getGraphic(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getGraphic', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/item/getGraphic',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function count(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/count', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/item/count',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}
function getImages(idItem, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getImages/' + idItem, fnSuccess, fnError);
}

function getAllImagesInFolder(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getAllImagesInFolder/' + id, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(DtoItems, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/item', DtoItems, fnSuccess, fnError);
}

function insertTranslate(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/item/insertTranslate', data, fnSuccess, fnError);
}

function insertRelated(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/item/insertRelated', data, fnSuccess, fnError);
}
function insertItemCategories(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/item/insertItemCategories', data, fnSuccess, fnError);
}

function insertItemImages(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/item/insertItemImages', data, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(DtoItems, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item', DtoItems, fnSuccess, fnError);
}

function addFavoritesItem(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/item/addFavoritesItem', data, fnSuccess, fnError);
}

function updateImageName(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/updateImageName', data, fnSuccess, fnError);
}

function updateOrderImamePhotos(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/updateOrderImamePhotos', data, fnSuccess, fnError);
}

function updateItemImages(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/updateItemImages', data, fnSuccess, fnError);
}

function updateItemCheckAssociation(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/updateItemCheckAssociation', data, fnSuccess, fnError);
}

function updateOnlineItem(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/updateOnlineItem', data, fnSuccess, fnError);
}

function updateOnlineItemCheck(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/updateOnlineItemCheck', data, fnSuccess, fnError);
}
/** *   END POST   ***/

/** * DELETE ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/item/delete/' + id, fnSuccess, fnError);
}

function deleteItemCategories(idCategories, idItem, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/item/deleteItemCategories/' + idCategories + '/' + idItem, fnSuccess, fnError);
}

function deleteItemsPhotos(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/item/deleteItemsPhotos/' + id, fnSuccess, fnError);
}

/** * DELETE ***/

function getItemsUserShop(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/item/getItemsUserShop', data, fnSuccess, fnError);
}

function getItemsCategoriesFilter(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/item/getItemsCategoriesFilter', data, fnSuccess, fnError);
}

function getItemById(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/item/getItemById/' + id, fnSuccess, fnError);
}
function getByFile(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getByFile/' + id, fnSuccess, fnError);
}

function getByFileNoId(data, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getByFileNoId', data, fnSuccess, fnError);
}
function getItemLanguages(idItem, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getItemLanguages/' + idItem, fnSuccess, fnError);
}

function getByTranslate(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getByTranslate/' + id, fnSuccess, fnError);
}
function getByTranslateDefault(id, idItem, idLanguage, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/item/getByTranslateDefault/' + id + '/' + idItem + '/' + idLanguage, fnSuccess, fnError);
}
function getByTranslateDefaultNew(idItem, idLanguage, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/item/getByTranslateDefaultNew/' + idItem + '/' + idLanguage, fnSuccess, fnError);
}
function updateImages(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/item/updateImages', data, fnSuccess, fnError);
}

function updateTranslate(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/item/updateTranslate', data, fnSuccess, fnError);
}
function updateNoAuth(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/item/updateNoAuth', data, fnSuccess, fnError);
}
function RemoveFileDb(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/item/RemoveFileDb/' + id, fnSuccess, fnError);
}
function removeFile(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/item/removeFile/' + id, fnSuccess, fnError);
}

function getCancelTranslate(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/item/getCancelTranslate/' + id, fnSuccess, fnError);
}

/** *   clone ***/
function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/item/clone/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), null, fnSuccess, fnError);
}
/** *   END  clone ***/

/***/ }),
/* 72 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getItemWithoutGroupTechnicalSpecification"] = getItemWithoutGroupTechnicalSpecification;
/** *   itemGroupTechnicalSpecificationServices   ***/

/** *   GET   ***/

function getItemWithoutGroupTechnicalSpecification(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/itemgrouptechnicalspecification/getItemWithoutGroupTechnicalSpecification', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/itemgrouptechnicalspecification/getItemWithoutGroupTechnicalSpecification',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END GET   ***/

/***/ }),
/* 73 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getConfiguration"] = getConfiguration;
/* harmony export (immutable) */ __webpack_exports__["getVersionConfiguration"] = getVersionConfiguration;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getVersion"] = getVersion;
/* harmony export (immutable) */ __webpack_exports__["getCompletedStatByLanguage"] = getCompletedStatByLanguage;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["complete"] = complete;
/** *   itemTechnicalSpecificationServices   ***/

/** *   GET   ***/

function getConfiguration(idItem, idLanguage, fnSuccess, fnError) {
  var url = '/api/v1/itemtechnicalspecification/getConfiguration/' + idItem + '/' + menuHelpers.getIdFunctionByAdminUrl();

  if (functionHelpers.isValued(idLanguage)) {
    url = url + '/' + idLanguage;
  }

  serviceHelpers.getAutenticate(url, fnSuccess, fnError);
}

function getVersionConfiguration(idItem, version, idLanguage, fnSuccess, fnError) {
  var url = '/api/v1/itemtechnicalspecification/getVersionConfiguration/' + idItem + '/' + version + '/' + menuHelpers.getIdFunctionByAdminUrl();

  if (functionHelpers.isValued(idLanguage)) {
    url = url + '/' + idLanguage;
  }

  serviceHelpers.getAutenticate(url, fnSuccess, fnError);
}

function get(idItem, idLanguage, version, fnSuccess, fnError) {
  var url = '/api/v1/itemtechnicalspecification/' + idItem + '/' + menuHelpers.getIdFunctionByAdminUrl();

  if (functionHelpers.isValued(idLanguage)) {
    url = url + '/' + idLanguage;
  }
  if (functionHelpers.isValued(version) && version != 'Last') {
    if (!functionHelpers.isValued(idLanguage)) {
      url = url + '/' + storageData.sIdLanguage();
    }
    url = url + '/' + version;
  }

  serviceHelpers.getAutenticate(url, fnSuccess, fnError);
}

function getVersion(idItem, idLanguage, fnSuccess, fnError) {
  var url = '/api/v1/itemtechnicalspecification/getVersion/' + idItem + '/' + menuHelpers.getIdFunctionByAdminUrl();

  if (functionHelpers.isValued(idLanguage)) {
    url = url + '/' + idLanguage;
  }

  serviceHelpers.getAutenticate(url, fnSuccess, fnError);
}

function getCompletedStatByLanguage(idLanguage, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/itemtechnicalspecification/getCompletedStatByLanguage/' + idLanguage, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/itemtechnicalspecification', data, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function complete(id, complete, fnSuccess, fnError) {
  var dtoItem = {
    idItem: id,
    complete: complete
  };

  serviceHelpers.postAutenticate('/api/v1/itemtechnicalspecification/complete', dtoItem, fnSuccess, fnError);
}

/** *   END POST   ***/

/***/ }),
/* 74 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/** *   jsServices   ***/

/** *   GET   ***/

function get(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/js/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   POST   ***/

function update(css, fnSuccess, fnError) {
  var data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    css: css
  };

  serviceHelpers.postAutenticate('/api/v1/js', data, fnSuccess, fnError);
}

/** *   END POST   ***/

/***/ }),
/* 75 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getDefault"] = getDefault;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getConfiguration"] = getConfiguration;
/* harmony export (immutable) */ __webpack_exports__["all"] = all;
/* harmony export (immutable) */ __webpack_exports__["selectlanguagestranslate"] = selectlanguagestranslate;
/** *   languageServices   ***/

/** * GET ***/

function getDefault(fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/language/getDefault', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/language/getDefault',
  //   dataType: 'json',
  //   headers: {
  //     Accept: "application/json"
  //   },
  //   contentType: 'application/x-www-form-urlencoded; charset=UTF-8'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function get(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/language/get/' + id, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/language/get/' + id,
  //   dataType: 'json',
  //   headers: {
  //     Accept: "application/json"
  //   },
  //   contentType: 'application/x-www-form-urlencoded; charset=UTF-8'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/language/select?search=' + search, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/language/select?search=' + search,
  // }).done(function (result) {
  //   fnSuccess(JSON.parse(result));
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function getConfiguration(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/language/getConfiguration', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/language/getConfiguration',
  //   dataType: 'json',
  //   headers: {
  //     Accept: "application/json",
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function all(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/language/all', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/language/all',
  //   dataType: 'json',
  //   headers: {
  //     Accept: "application/json",
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function selectlanguagestranslate(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/language/selectlanguagestranslate?search' + search, fnSuccess, fnError);
}

/** * END GET ***/

/***/ }),
/* 76 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["login"] = login;
/* harmony export (immutable) */ __webpack_exports__["loginSalesman"] = loginSalesman;
/* harmony export (immutable) */ __webpack_exports__["refreshToken"] = refreshToken;
/* harmony export (immutable) */ __webpack_exports__["insertLoginForFacebook"] = insertLoginForFacebook;
/* harmony export (immutable) */ __webpack_exports__["insertLoginForGoogle"] = insertLoginForGoogle;
/** *   loginService   ***/

/** *   POST   ***/

function login(username, password, fnSuccess, fnError) {
  var session_token = storageData.sTokenKey();

  var data = {
    // grant_type: 'password',
    name: username,
    password: password,
    remember_me: true,
    session_token: session_token
  };

  serviceHelpers.postNoJson('/api/auth/login', data, fnSuccess, fnError);
}

function loginSalesman(username, password, fnSuccess, fnError) {
  var session_token = storageData.sTokenKey();

  var data = {
    // grant_type: 'password',
    name: username,
    password: password,
    remember_me: true,
    session_token: session_token
  };

  serviceHelpers.postNoJson('/api/auth/loginSalesman', data, fnSuccess, fnError);
}

function refreshToken(fnSuccess, fnError) {
  serviceHelpers.postWithoutData('/Token?grant_type=refresh_token&refresh_token=' + storageData.sRefreshTokenKey() + '&username=' + storageData.sUserName(), fnSuccess, fnError);
}

function insertLoginForFacebook(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/auth/insertLoginForFacebook', data, fnSuccess, fnError);
}

function insertLoginForGoogle(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/auth/insertLoginForGoogle', data, fnSuccess, fnError);
}

/** *   END POST   ***/

/***/ }),
/* 77 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/** *   manageStyleServices   ***/

/** *   GET   ***/

function get(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/managestyle/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   POST   ***/

function update(css, fnSuccess, fnError) {
  var data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    css: css
  };

  serviceHelpers.postAutenticate('/api/v1/managestyle', data, fnSuccess, fnError);
}

/** *   END POST   ***/

/***/ }),
/* 78 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSettings"] = getSettings;
/* harmony export (immutable) */ __webpack_exports__["insertLink"] = insertLink;
/* harmony export (immutable) */ __webpack_exports__["insertFolder"] = insertFolder;
/* harmony export (immutable) */ __webpack_exports__["updateLink"] = updateLink;
/* harmony export (immutable) */ __webpack_exports__["updateFolder"] = updateFolder;
/** *   menuServices   ***/

/** *   GET   ***/

function getSettings(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/menu/getSettings' + id, fnSuccess, fnError);

  // $.ajax({
  //     type: 'GET',
  //     url: configData.wsRootServicesUrl + '/api/v1/menu/getSettings/' + id,
  //     dataType: 'json',
  //     headers: { Accept: "application/json", Authorization: storageData.sTokenKey() },
  //     contentType: 'application/json',
  // }).done(function (result) {
  //     fnSuccess(result);
  // }).fail(function (response) {
  //     failServiceHelpers.fail(response);
  // });
}

/** *   END GET   ***/

/** *   PUT   ***/

function insertLink(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/menu/link', data, fnSuccess, fnError);

  //   $.ajax({
  //     type: 'PUT',
  //     url: configData.wsRootServicesUrl + '/api/v1/menu/link',
  //     dataType: 'json',
  //     headers: {
  //       Authorization: storageData.sTokenKey()
  //     },
  //     contentType: 'application/json',
  //     data: JSON.stringify(functionHelpers.clearNullOnJson(menu))
  //   }).done(function (result) {
  //     fnSuccess(result);
  //   }).fail(function (response) {
  //     failServiceHelpers.fail(response);
  //   });
}

function insertFolder(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/menu/folder', data, fnSuccess, fnError);

  //   $.ajax({
  //     type: 'PUT',
  //     url: configData.wsRootServicesUrl + '/api/v1/menu/folder',
  //     dataType: 'json',
  //     headers: {
  //       Authorization: storageData.sTokenKey()
  //     },
  //     contentType: 'application/json',
  //     data: JSON.stringify(functionHelpers.clearNullOnJson(menu))
  //   }).done(function (result) {
  //     fnSuccess(result);
  //   }).fail(function (response) {
  //     failServiceHelpers.fail(response);
  //   });
}

/** *   END PUT   ***/

/** *   POST   ***/

function updateLink(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/menu/link', data, fnSuccess, fnError);

  //   $.ajax({
  //     type: 'POST',
  //     url: configData.wsRootServicesUrl + '/api/v1/menu/link',
  //     dataType: 'json',
  //     headers: {
  //       Authorization: storageData.sTokenKey()
  //     },
  //     contentType: 'application/json',
  //     data: JSON.stringify(functionHelpers.clearNullOnJson(menu))
  //   }).done(function (result) {
  //     fnSuccess(result);
  //   }).fail(function (response) {
  //     failServiceHelpers.fail(response);
  //   });
}

function updateFolder(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/menu/folder', data, fnSuccess, fnError);

  //   $.ajax({
  //     type: 'POST',
  //     url: configData.wsRootServicesUrl + '/api/v1/menu/folder',
  //     dataType: 'json',
  //     headers: {
  //       Authorization: storageData.sTokenKey()
  //     },
  //     contentType: 'application/json',
  //     data: JSON.stringify(functionHelpers.clearNullOnJson(menu))
  //   }).done(function (result) {
  //     fnSuccess(result);
  //   }).fail(function (response) {
  //     failServiceHelpers.fail(response);
  //   });
}

/** *   END POST   ***/

/***/ }),
/* 79 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   nationServices   ***/

/** *   GET   ***/
function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/nation/select?search=' + search, fnSuccess, fnError);
}

function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/nation/all/' + idLanguage, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/nation', data, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(dtoNation, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/nation', dtoNation, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/nation/' + data, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 80 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getByUser"] = getByUser;
/* harmony export (immutable) */ __webpack_exports__["getDetailById"] = getDetailById;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["insertDetail"] = insertDetail;
/* harmony export (immutable) */ __webpack_exports__["deleteRow"] = deleteRow;
/** *   negotiationServices   ***/

/** *   GET   ***/

function getByUser(userId, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/negotiation/getByUser/' + userId, fnSuccess, fnError);
}

function getDetailById(id, idUser, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/negotiation/getDetailById/' + id + '/' + idUser, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/negotiation', data, fnSuccess, fnError);
}

function insertDetail(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/negotiation/insertDetail', data, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

/** *   END POST   ***/

/** * DELETE ***/

function deleteRow(id, fnSuccess, fnError) {
  serviceHelpers.del('/api/v1/negotiation/deleteRow/' + id, fnSuccess, fnError);
}

/** * DELETE ***/

/***/ }),
/* 81 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   newsletterServices   ***/

/** *   GET   ***/

function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletter/' + id, fnSuccess, fnError);
}

function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletter/all', fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(dtoNewsletter, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/newsletter', dtoNewsletter, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(dtoNewsletter, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/newsletter', dtoNewsletter, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/newsletter/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 82 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getContact"] = getContact;
/* harmony export (immutable) */ __webpack_exports__["getCustomer"] = getCustomer;
/* harmony export (immutable) */ __webpack_exports__["getGroupNewsletter"] = getGroupNewsletter;
/* harmony export (immutable) */ __webpack_exports__["getNumberRecipients"] = getNumberRecipients;
/* harmony export (immutable) */ __webpack_exports__["insertContact"] = insertContact;
/* harmony export (immutable) */ __webpack_exports__["insertCustomer"] = insertCustomer;
/* harmony export (immutable) */ __webpack_exports__["insertGroupNewsletter"] = insertGroupNewsletter;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["delGroupNewsletter"] = delGroupNewsletter;
/** *   newsletterRecipientServices   ***/

/** *   GET   ***/

function getContact(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletterrecipient/contact/' + id, fnSuccess, fnError);
}

function getCustomer(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletterrecipient/customer/' + id, fnSuccess, fnError);
}

function getGroupNewsletter(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletterrecipient/groupnewsletter/' + id, fnSuccess, fnError);
}

function getNumberRecipients(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletterrecipient/getNumberRecipients/' + id, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insertContact(dtoNewsletterRecipient, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/newsletterrecipient/contact', dtoNewsletterRecipient, fnSuccess, fnError);
}

function insertCustomer(dtoNewsletterRecipient, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/newsletterrecipient/customer', dtoNewsletterRecipient, fnSuccess, fnError);
}

function insertGroupNewsletter(dtoNewsletterRecipient, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/newsletterrecipient/groupnewsletter', dtoNewsletterRecipient, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/newsletterrecipient/' + id, fnSuccess, fnError);
}

function delGroupNewsletter(idNewsletter, idGroup, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/newsletterrecipient/groupnewsletter/' + idNewsletter + '/' + idGroup, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 83 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getMaxByErp"] = getMaxByErp;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["selectByErp"] = selectByErp;
/* harmony export (immutable) */ __webpack_exports__["createByErp"] = createByErp;
/** *   packageServices   ***/

/** *   GET   ***/

function getMaxByErp(idErp, routeParameter, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/package/getMaxByErp/' + idErp + '/' + menuHelpers.getIdFunctionByAdminUrl(routeParameter), fnSuccess, fnError);
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/package/select?search' + search, fnSuccess, fnError);
}

function selectByErp(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/pageCategory/selectByErp?search' + search, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   INSERT   ***/

function createByErp(idErp, description, routeParameter, fnSuccess, fnError) {
  var data = {
    idErp: idErp,
    description: description,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(routeParameter)
  };

  serviceHelpers.putAutenticate('/api/v1/package/createByErp', data, fnSuccess, fnError);
}

/** *   END INSERT   ***/

/***/ }),
/* 84 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect"] = getSelect;
/** *   packagingTypeSixtenServices   ***/

/** *   GET   ***/

function getSelect(idErp, routeParams, fnSuccess, fnError) {
  var data = {
    idErp: idErp,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(routeParams)
  };

  serviceHelpers.getAutenticateWithData('/api/v1/packagingTypeSixten/select', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/packagingTypeSixten/select',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: "application/x-www-form-urlencoded; charset=UTF-8",
  //   data: data
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END GET   ***/

/***/ }),
/* 85 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/** *   pageCategoryServices   ***/

/** *   GET   ***/

function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/pageCategory/' + id, fnSuccess, fnError);
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/pageCategory/select?search' + search, fnSuccess, fnError);
}

/** *   END GET   ***/

/***/ }),
/* 86 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getMenuList"] = getMenuList;
/* harmony export (immutable) */ __webpack_exports__["getSetting"] = getSetting;
/* harmony export (immutable) */ __webpack_exports__["getHome"] = getHome;
/* harmony export (immutable) */ __webpack_exports__["getLastActivity"] = getLastActivity;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["clone"] = clone;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["postMenuList"] = postMenuList;
/* harmony export (immutable) */ __webpack_exports__["postOnlinePage"] = postOnlinePage;
/* harmony export (immutable) */ __webpack_exports__["getRender"] = getRender;
/* harmony export (immutable) */ __webpack_exports__["checkExistUrl"] = checkExistUrl;
/* harmony export (immutable) */ __webpack_exports__["checkExistUrlNotForThisPage"] = checkExistUrlNotForThisPage;
/* harmony export (immutable) */ __webpack_exports__["checkExistUrlNotForThisNewsletter"] = checkExistUrlNotForThisNewsletter;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   pageServices   ***/

/** * GET ***/

function getMenuList(id, pageType, fnSuccess, fnError) {
  if (id == '' || id === undefined) {
    id = storageData.sIdLanguage();
  }
  if (pageType == '' || pageType === undefined) {
    pageType = 'Page';
  }

  serviceHelpers.getAutenticate('/api/v1/page/getMenuList/' + id + '/' + pageType, fnSuccess, fnError);
}

function getSetting(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/page/getSetting/' + id, fnSuccess, fnError);
}

function getHome(fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/page/getHome/' + storageData.sIdLanguage(), fnSuccess, fnError);
}

function getLastActivity(fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/page/getLastActivity/' + storageData.sIdLanguage(), fnSuccess, fnError);
}

/** * END GET ***/

/** * PUT ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/page', data, fnSuccess, fnError);
}

function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/page/clone/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), null, fnSuccess, fnError);
}

/** * END PUT ***/

/** * POST ***/

function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/page', data, fnSuccess, fnError);
}

function postMenuList(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/page/menuList', data, fnSuccess, fnError);
}

function postOnlinePage(valueOnline, id, fnSuccess, fnError) {
  var data = {
    online: valueOnline,
    id: id
  };

  serviceHelpers.postAutenticate('/api/v1/page/onlinePage', data, fnSuccess, fnError);
}

function getRender(url, fnSuccess, fnError) {
  var data = {
    url: url,
    userId: storageData.sUserId()
  };

  serviceHelpers.post('/api/v1/page/getRender', data, fnSuccess, fnError);
}

function checkExistUrl(url, fnSuccess, fnError) {
  var data = {
    url: url
  };

  serviceHelpers.postAutenticate('/api/v1/page/checkExistUrl', data, fnSuccess, fnError);
}

function checkExistUrlNotForThisPage(url, idPage, fnSuccess, fnError) {
  var data = {
    url: url,
    idPage: idPage
  };

  serviceHelpers.postAutenticate('/api/v1/page/checkExistUrl', data, fnSuccess, fnError);
}

function checkExistUrlNotForThisNewsletter(url, idNewsletter, fnSuccess, fnError) {
  var data = {
    url: url,
    idNewsletter: idNewsletter
  };

  serviceHelpers.postAutenticate('/api/v1/page/checkExistUrl', data, fnSuccess, fnError);
}

/** * END POST ***/

/** * DELETE ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/page/' + id, fnSuccess, fnError);
}

/** * END DELETE ***/

/***/ }),
/* 87 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/** *   pageShareTypeServices   ***/

/** *   GET   ***/

function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/pageShareType/' + id, fnSuccess, fnError);
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/pageShareType/select?search' + search, fnSuccess, fnError);
}

/** *   END GET   ***/

/***/ }),
/* 88 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   postalCodeServices   ***/

/** *   GET   ***/

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/postalcode/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/postalcode/select?search=' + search, fnSuccess, fnError);
}

function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/postalcode/' + id, fnSuccess, fnError);
}

function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/postalcode/' + id, fnSuccess, fnError);
}

function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/postalcode/all', fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(dtoVatType, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/postalcode', dtoVatType, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(dtoVatType, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/postalcode', dtoVatType, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/postalcode/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 89 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getByUser"] = getByUser;
/** *   roleServices   ***/

/** * GET ***/

function getByUser(idUser, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/role/getByUser/' + idUser + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/role/getByUser/' + idUser + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     "Accept": "application/json",
  //     "Authorization": storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** * END GET ***/

/***/ }),
/* 90 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   roleUserServices   ***/

/** * PUT ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/roleuser', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/roleuser/insert',
  //   dataType: 'json',
  //   headers: {
  //     "Accept": "application/json",
  //     "Authorization": storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** * END PUT ***/

/** * DELETE ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/roleuser/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'DELETE',
  //   url: configData.wsRootServicesUrl + '/api/v1/roleuser/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     "Accept": "application/json",
  //     "Authorization": storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** * END DELETE ***/

/***/ }),
/* 91 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["getLastSync"] = getLastSync;
/* harmony export (immutable) */ __webpack_exports__["getRenderMenu"] = getRenderMenu;
/* harmony export (immutable) */ __webpack_exports__["getRenderBlog"] = getRenderBlog;
/* harmony export (immutable) */ __webpack_exports__["getRenderNews"] = getRenderNews;
/* harmony export (immutable) */ __webpack_exports__["getByCode"] = getByCode;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   settingServices   ***/

/** *   GET   ***/

function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/setting/getAll', fnSuccess, fnError);
}

function getLastSync(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/setting/getLastSync', fnSuccess, fnError);
}

function getRenderMenu(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/setting/getRenderMenu', fnSuccess, fnError);
}

function getRenderBlog(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/setting/getRenderBlog', fnSuccess, fnError);
}

function getRenderNews(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/setting/getRenderNews', fnSuccess, fnError);
}

function getByCode(code, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/setting/getByCode/' + code, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/setting', data, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/setting', data, fnSuccess, fnError);
}

/** *   END POST    ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/setting/' + id, fnSuccess, fnError);
}

/** *   END DELETE    ***/

/***/ }),
/* 92 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect"] = getSelect;
/* harmony export (immutable) */ __webpack_exports__["getByCustomerSixten"] = getByCustomerSixten;
/** *   shipmentCodeSixtenServices   ***/

/** *   GET   ***/

function getSelect(routeParams, fnSuccess, fnError) {
  var data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(routeParams)
  };

  serviceHelpers.getAutenticateWithData('/api/v1/shipmentCodeSixten/select', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/shipmentCodeSixten/select',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: "application/x-www-form-urlencoded; charset=UTF-8",
  //   data: data
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function getByCustomerSixten(idCustomer, routeParams, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/shipmentCodeSixten/getByCustomerSixten/' + idCustomer + '/' + menuHelpers.getIdFunctionByAdminUrl(routeParams), fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/shipmentCodeSixten/getByCustomerSixten/' + idCustomer + '/' + menuHelpers.getIdFunctionByAdminUrl(routeParams),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END GET   ***/

/***/ }),
/* 93 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   selenaViewsServices   ***/

/** *   GET   ***/

function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/selenaview/all', fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/selenaview', data, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(dtoNation, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/selenaview', dtoNation, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/selenaview/' + data, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 94 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["getByCode"] = getByCode;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   messageServices   ***/

/** *   GET   ***/

function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/message/all/' + idLanguage, fnSuccess, fnError);
}

function getByCode(code, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/message/getByCode/' + code, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(dtoMessage, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/message', dtoMessage, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(dtoMessage, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/message', dtoMessage, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/message/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 95 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/** *   categoryTypeServices   ***/

/** *   GET   */

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/MessageType/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    }
  };
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/MessageType/select?search=' + search, fnSuccess, fnError);
}
function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/MessageType/' + id, fnSuccess, fnError);
}

function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/MessageType/' + id, fnSuccess, fnError);
}

function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/MessageType/all/' + idLanguage, fnSuccess, fnError);
}

/** *   END GET   */

/***/ }),
/* 96 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["search"] = search;
/* harmony export (immutable) */ __webpack_exports__["searchImplemented"] = searchImplemented;
/* harmony export (immutable) */ __webpack_exports__["searchGeneral"] = searchGeneral;
/** *   searchServices   ***/

/** *   POST   ***/

function search(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/search', search, fnSuccess, fnError);
}
function searchImplemented(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/search/searchImplemented', search, fnSuccess, fnError);
}

function searchGeneral(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/search/searchGeneral', search, fnSuccess, fnError);
}

/** *   END POST   ***/

/***/ }),
/* 97 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getDtoTable"] = getDtoTable;
/* harmony export (immutable) */ __webpack_exports__["getGroupDisplayTechnicalSpecification"] = getGroupDisplayTechnicalSpecification;
/* harmony export (immutable) */ __webpack_exports__["getGroupTechnicalSpecification"] = getGroupTechnicalSpecification;
/* harmony export (immutable) */ __webpack_exports__["checkExists"] = checkExists;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["addGroupDisplayTechnicalSpecification"] = addGroupDisplayTechnicalSpecification;
/* harmony export (immutable) */ __webpack_exports__["addGroupTechnicalSpecification"] = addGroupTechnicalSpecification;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   technicalSpecificationServices   ***/

/** *   GET   ***/

function getDtoTable(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/technicalSpecification/getDtoTable', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/technicalSpecification/getDtoTable',
  //   dataType: 'json',
  //   headers: { Authorization: storageData.sTokenKey() },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function getGroupDisplayTechnicalSpecification(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/technicalSpecification/getGroupDisplayTechnicalSpecification/' + id, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/technicalSpecification/getGroupDisplayTechnicalSpecification/' + id,
  //   dataType: 'json',
  //   headers: { Authorization: storageData.sTokenKey() },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function getGroupTechnicalSpecification(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/technicalSpecification/getGroupTechnicalSpecification/' + id, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/technicalSpecification/getGroupTechnicalSpecification/' + id,
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function checkExists(description, idLanguage, id, fnSuccess, fnError) {
  var url = configData.wsRootServicesUrl + '/api/v1/technicalSpecification/checkExists/' + description + '/' + idLanguage;

  if (functionHelpers.isValued(id)) {
    url = url + '/' + id;
  }

  serviceHelpers.getAutenticate(url, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: url,
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/technicalSpecification', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/technicalSpecification',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(dtoTechnicalSpecification))
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function addGroupDisplayTechnicalSpecification(id, idGroupDisplayTechnicalSpecification, fnSuccess, fnError) {
  serviceHelpers.putAutenticateWithoutData('/api/v1/technicalSpecification/addGroupDisplayTechnicalSpecification/' + id + '/' + idGroupDisplayTechnicalSpecification + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/technicalSpecification/addGroupDisplayTechnicalSpecification/' + id + '/' + idGroupDisplayTechnicalSpecification + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

function addGroupTechnicalSpecification(id, idGroupTechnicalSpecification, fnSuccess, fnError) {
  serviceHelpers.putAutenticateWithoutData('/api/v1/technicalSpecification/addGroupTechnicalSpecification/' + id + '/' + idGroupTechnicalSpecification + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/technicalSpecification/addGroupTechnicalSpecification/' + id + '/' + idGroupTechnicalSpecification + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/technicalSpecification', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'POST',
  //   url: configData.wsRootServicesUrl + '/api/v1/technicalSpecification',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(dtoTechnicalSpecification))
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/technicalSpecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'DELETE',
  //   url: configData.wsRootServicesUrl + '/api/v1/technicalSpecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

/** *   END DELETE   ***/

/***/ }),
/* 98 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getDto"] = getDto;
/* harmony export (immutable) */ __webpack_exports__["getTemplateById"] = getTemplateById;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["clone"] = clone;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["updateTable"] = updateTable;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   templateEditorServices   ***/

/** *   GET   ***/

function getDto(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/templateeditor/getDto/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

function getTemplateById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/templateeditor/getTemplateById/' + id, fnSuccess, fnError);
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/templateeditor/select?search' + search, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/templateeditor', data, fnSuccess, fnError);
}

function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/templateeditor/clone/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), null, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/templateeditor', data, fnSuccess, fnError);
}

function updateTable(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/templateeditor/table', data, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/templateeditor/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 99 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getDto"] = getDto;
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["selectGroupTemplate"] = selectGroupTemplate;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   templateEditorGroupServices   ***/

/** *   GET   ***/

function getDto(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/templateeditorgroup/getDto/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/templateeditorgroup/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/templateeditorgroup/' + id, fnSuccess, fnError);
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/templateeditorgroup/select?idFunctionality=' + menuHelpers.getIdFunctionByAdminUrl() + '&search' + search, fnSuccess, fnError);
}

function selectGroupTemplate(search, type, fnSuccess, fnError) {
  if (functionHelpers.isValued(type)) {
    serviceHelpers.getAutenticate('/api/v1/templateeditorgroup/selectGroupTemplate?idFunctionality=' + menuHelpers.getIdFunctionByAdminUrl() + '&type=' + type + '&search' + search, fnSuccess, fnError);
  } else {
    return {
      data: []
    };
  }
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/templateeditorgroup', data, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/templateeditorgroup', data, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/templateeditorgroup/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 100 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/** *   templateEditorTypeServices   ***/

/** * GET ***/

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/templateeditortype/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/templateeditortype/' + id, fnSuccess, fnError);
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/templateeditortype/select?idFunctionality=' + menuHelpers.getIdFunctionByAdminUrl() + '&search' + search, fnSuccess, fnError);
}

/** * END GET ***/

/***/ }),
/* 101 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   unitMeasuresServices   ***/

/** *   GET   */

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/UnitMeasure/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/UnitMeasure/select?search=' + search, fnSuccess, fnError);
}
function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/UnitMeasure/' + id, fnSuccess, fnError);
}

function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/UnitMeasure/' + id, fnSuccess, fnError);
}

function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/UnitMeasure/all/' + idLanguage, fnSuccess, fnError);
}

/** *   END GET   */

/** *   PUT   ***/

function insert(DtoUnitMeasure, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/UnitMeasure', DtoUnitMeasure, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(DtoUnitMeasure, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/UnitMeasure', DtoUnitMeasure, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/UnitMeasure/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 102 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getByIdResult"] = getByIdResult;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["availableCheck"] = availableCheck;
/* harmony export (immutable) */ __webpack_exports__["getUserData"] = getUserData;
/* harmony export (immutable) */ __webpack_exports__["getByUsername"] = getByUsername;
/* harmony export (immutable) */ __webpack_exports__["getGraphic"] = getGraphic;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["getAllUser"] = getAllUser;
/* harmony export (immutable) */ __webpack_exports__["count"] = count;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["insertNoAuth"] = insertNoAuth;
/* harmony export (immutable) */ __webpack_exports__["insertUser"] = insertUser;
/* harmony export (immutable) */ __webpack_exports__["changePassword"] = changePassword;
/* harmony export (immutable) */ __webpack_exports__["changePasswordRescued"] = changePasswordRescued;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["updateNoAuth"] = updateNoAuth;
/* harmony export (immutable) */ __webpack_exports__["updateUser"] = updateUser;
/* harmony export (immutable) */ __webpack_exports__["rescuePassword"] = rescuePassword;
/* harmony export (immutable) */ __webpack_exports__["checkExistUsername"] = checkExistUsername;
/* harmony export (immutable) */ __webpack_exports__["checkGroupUser"] = checkGroupUser;
/* harmony export (immutable) */ __webpack_exports__["checkExistUsernameNotForThisPage"] = checkExistUsernameNotForThisPage;
/* harmony export (immutable) */ __webpack_exports__["checkExistEmail"] = checkExistEmail;
/* harmony export (immutable) */ __webpack_exports__["checkExistEmailNotForThisPage"] = checkExistEmailNotForThisPage;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["delet"] = delet;
/** *   userServices   ***/

/** *   GET   ***/

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/select?search' + search, fnSuccess, fnError);
}

function getByIdResult(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/getByIdResult/' + id, fnSuccess, fnError);
}

function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/' + id, fnSuccess, fnError);
}
function availableCheck(id, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/user/availableCheck/' + id, fnSuccess, fnError);
}

function getUserData(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/user/getUserData/' + id, fnSuccess, fnError);
}

function getByUsername(username, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/user/getByUsername/' + username, fnSuccess, fnError);
}

function getGraphic(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/getGraphic', fnSuccess, fnError);
}

function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/all/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

function getAllUser(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/user/getAllUser/' + idLanguage, fnSuccess, fnError);
}

function count(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/count', fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/user', data, fnSuccess, fnError);
}

function insertNoAuth(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/user/insertNoAuth', data, fnSuccess, fnError);
}

function insertUser(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/user/insertUser', data, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function changePassword(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/user/changePassword', data, fnSuccess, fnError);
}

function changePasswordRescued(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/changePasswordRescued', data, fnSuccess, fnError);
}

function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/user', data, fnSuccess, fnError);
}

function updateNoAuth(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/updateNoAuth', data, fnSuccess, fnError);
}

function updateUser(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/updateUser', data, fnSuccess, fnError);
}

function rescuePassword(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/rescuePassword', data, fnSuccess, fnError);
}

function checkExistUsername(username, fnSuccess, fnError) {
  var data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    username: username
  };

  serviceHelpers.postAutenticate('/api/v1/user/checkExistUsername', data, fnSuccess, fnError);
}

function checkGroupUser(username, fnSuccess, fnError) {
  var data = {
    username: username
  };

  serviceHelpers.postAutenticate('/api/v1/user/checkGroupUser', data, fnSuccess, fnError);
}

function checkExistUsernameNotForThisPage(id, username, fnSuccess, fnError) {
  var data = {
    id: id,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    username: username
  };

  serviceHelpers.postAutenticate('/api/v1/user/checkExistUsername', data, fnSuccess, fnError);
}

function checkExistEmail(email, fnSuccess, fnError) {
  var data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    email: email
  };

  serviceHelpers.postAutenticate('/api/v1/user/checkExistEmail', data, fnSuccess, fnError);
}

function checkExistEmailNotForThisPage(id, email, fnSuccess, fnError) {
  var data = {
    id: id,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    email: email
  };

  serviceHelpers.postAutenticate('/api/v1/user/checkExistEmail', data, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/user/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

/** *   END DELETE   ***/

function delet(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/user/' + id, fnSuccess, fnError);
}

/***/ }),
/* 103 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   vatTypeServices   ***/

/** *   GET   */

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/vattype/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/vattype/select?search=' + search, fnSuccess, fnError);
}
function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/vattype/' + id, fnSuccess, fnError);
}

function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/vattype/' + id, fnSuccess, fnError);
}

function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/vattype/all', fnSuccess, fnError);
}

/** *   END GET   */

/** *   PUT   ***/

function insert(dtoVatType, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/vattype', dtoVatType, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(dtoVatType, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/vattype', dtoVatType, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/vattype/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 104 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["getList"] = getList;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["clone"] = clone;
/** *   faultModulesServices   ***/

/** *   GET   */

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/FaultModules/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/FaultModules/select?search=' + search, fnSuccess, fnError);
}

function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/FaultModules/' + id, fnSuccess, fnError);
}

function getList(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/FaultModules/getList', data, fnSuccess, fnError);
}

function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/FaultModules/' + id, fnSuccess, fnError);
}

function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/FaultModules/all/' + idLanguage, fnSuccess, fnError);
}

/** *   END GET   */

/** *   PUT   ***/

function insert(DtoInterventions, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/FaultModules', DtoInterventions, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(DtoInterventions, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/FaultModules', DtoInterventions, fnSuccess, fnError);
}

/** *   END POST   ***/

/** * DELETE ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/FaultModules/' + id, fnSuccess, fnError);
}

/** * END DELETE ***/

/** *   clone ***/
function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/FaultModules/clone/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), null, fnSuccess, fnError);
}
/** *   END  clone ***/

/***/ }),
/* 105 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["insertNoAuth"] = insertNoAuth;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["updateNoAuth"] = updateNoAuth;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   statesServices   ***/

/** *   GET   */

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/States/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/States/select?search=' + search, fnSuccess, fnError);
}
function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/States/' + id, fnSuccess, fnError);
}

function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/States/' + id, fnSuccess, fnError);
}

function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/States/all/' + idLanguage, fnSuccess, fnError);
}

/** *   END GET   */

/** *   PUT   ***/

function insert(DtoStates, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/States', DtoStates, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *    insert no Auth   ***/
function insertNoAuth(DtoStates, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/States', DtoStates, fnSuccess, fnError);
}

/** *  end  insert no Auth   ***/

/** *   POST   ***/

function update(DtoStates, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/States', DtoStates, fnSuccess, fnError);
}

function updateNoAuth(DtoStates, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/States', DtoStates, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/States/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 106 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["clone"] = clone;
/** *   producerServices   ***/

/** *   GET   ***/

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/producer/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/producer/select?search=' + search, fnSuccess, fnError);
}
function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/producer/' + id, fnSuccess, fnError);
}

function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/producer/all/' + idLanguage, fnSuccess, fnError);
}

function get(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/producer', fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(DtoProducer, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/producer', DtoProducer, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(DtoProducer, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/producer', DtoProducer, fnSuccess, fnError);
}

/** *   END POST   ***/

/** * DELETE ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/producer/' + id, fnSuccess, fnError);
}

/** * DELETE ***/

/** *   clone ***/
function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/producer/clone/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), null, fnSuccess, fnError);
}
/** *   END  clone ***/

/***/ }),
/* 107 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["getByIdRoles"] = getByIdRoles;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAllWithoutAdmin"] = getAllWithoutAdmin;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["getAllNoAuth"] = getAllNoAuth;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["insertNoAuth"] = insertNoAuth;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["updateNoAuth"] = updateNoAuth;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   rolesServices   ***/

/** *   GET   */

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/roles/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/roles/select?search=' + search, fnSuccess, fnError);
}
function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/roles/' + id, fnSuccess, fnError);
}

function getByIdRoles(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/roles/getByIdRoles/' + id, fnSuccess, fnError);
}
function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/roles/' + id, fnSuccess, fnError);
}

function getAllWithoutAdmin(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/roles/getAllWithoutAdmin', fnSuccess, fnError);
}

function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/roles/all', fnSuccess, fnError);
}

function getAllNoAuth(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/roles/allNoAuth', fnSuccess, fnError);
}

/** *   END GET   */

/** *   PUT   ***/

function insert(DtoRole, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/roles', DtoRole, fnSuccess, fnError);
}

function insertNoAuth(DtoRole, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/roles', DtoRole, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(DtoRole, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/roles', DtoRole, fnSuccess, fnError);
}

function updateNoAuth(DtoRole, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/roles', DtoRole, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/roles/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 108 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAllReferent"] = getAllReferent;
/* harmony export (immutable) */ __webpack_exports__["getPeopleUser"] = getPeopleUser;
/* harmony export (immutable) */ __webpack_exports__["getByIdReferent"] = getByIdReferent;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["insertNoAuth"] = insertNoAuth;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["updateReferent"] = updateReferent;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["clone"] = clone;
/** *   peopleServices   ***/

/** *   GET   ***/

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/people/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/people/select?search=' + search, fnSuccess, fnError);
}
function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/people/' + id, fnSuccess, fnError);
}

function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/people/all/' + idLanguage, fnSuccess, fnError);
}

function get(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/people', fnSuccess, fnError);
}

function getAllReferent(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/people/getAllReferent/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

function getPeopleUser(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/people/getPeopleUser/' + id, fnSuccess, fnError);
}

function getByIdReferent(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/people/getByIdReferent/' + id, fnSuccess, fnError);
}

/** *   END GET   ***/
/** *   PUT   ***/

function insert(DtoPeople, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/people', DtoPeople, fnSuccess, fnError);
}

function insertNoAuth(DtoPeople, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/people/insertNoAuth', DtoPeople, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(DtoPeople, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/people', DtoPeople, fnSuccess, fnError);
}

function updateReferent(DtoPeople, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/people/updateReferent', DtoPeople, fnSuccess, fnError);
}
/** *   END POST   ***/

/** * DELETE ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/people/' + id, fnSuccess, fnError);
}

/** * DELETE ***/

/** *   clone ***/
function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/people/clone/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), null, fnSuccess, fnError);
}
/** *   END  clone ***/

/***/ }),
/* 109 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["searchImplemented"] = searchImplemented;
/** *   searchShopServices   ***/

/** *   POST   ***/

function searchImplemented(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchshop/searchImplemented', search, fnSuccess, fnError);
}

/** *   END POST   ***/

/***/ }),
/* 110 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["insertNoAuth"] = insertNoAuth;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   itemGrServices   ***/

/** *   GET   ***/

function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/itemGr/all/' + idLanguage, fnSuccess, fnError);
}

function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/itemGr/' + id, fnSuccess, fnError);
}

function insertNoAuth(DtoItemGr, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/itemGr/insertNoAuth', DtoItemGr, fnSuccess, fnError);
}

function update(DtoItemGr, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/itemGr', DtoItemGr, fnSuccess, fnError);
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/itemGr/select?search' + search, fnSuccess, fnError);
}

/** * DELETE ***/
function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/itemGr/' + id, fnSuccess, fnError);
}
/** * DELETE ***/

/***/ }),
/* 111 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["searchImplemented"] = searchImplemented;
/** *   searchLiutaioServices   ***/

/** *   POST   ***/
function searchImplemented(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchliutaio/searchImplemented', search, fnSuccess, fnError);
}
/** *   END POST   ***/

/***/ }),
/* 112 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["searchImplemented"] = searchImplemented;
/** *   searchSalaRegistrazioneServices   ***/

/** *   POST   ***/
function searchImplemented(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchsalaregistrazione/searchImplemented', search, fnSuccess, fnError);
}
/** *   END POST   ***/

/***/ }),
/* 113 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["searchImplemented"] = searchImplemented;
/** *   searchShopServices   ***/

/** *   POST   ***/

function searchImplemented(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchintervention/searchImplemented', search, fnSuccess, fnError);
}
/** *   END POST   ***/

/***/ }),
/* 114 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["searchImplemented"] = searchImplemented;
/** *   searchUserServices   ***/

/** *   POST   ***/

function searchImplemented(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchuser/searchImplemented', search, fnSuccess, fnError);
}
/** *   END POST   ***/

/***/ }),
/* 115 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/** *   brandServices   ***/

/** *   GET   ***/

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/brand/select?search' + search, fnSuccess, fnError);
}

/***/ }),
/* 116 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/** *   tipologyServices   ***/

/** *   GET   ***/

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/tipology/select?search' + search, fnSuccess, fnError);
}

/***/ }),
/* 117 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["getAllTypeAndOptional"] = getAllTypeAndOptional;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["insertItemTypeOptional"] = insertItemTypeOptional;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["delItemTypeOptional"] = delItemTypeOptional;
/** *   typeOptionalServices   ***/

/** *   GET   ***/

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/typeoptional/select?search=' + search, fnSuccess, fnError);
}

function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/typeoptional/all/' + ln, fnSuccess, fnError);
}

function getAllTypeAndOptional(ln, idItem, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/typeoptional/getAllTypeAndOptional/' + ln + '/' + idItem, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/typeoptional', data, fnSuccess, fnError);
}

function insertItemTypeOptional(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/typeoptional/insertItemTypeOptional', data, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(dtoNation, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/typeoptional', dtoNation, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/typeoptional/' + data, fnSuccess, fnError);
}

function delItemTypeOptional(idTypeOptional, idItem, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/typeoptional/delItemTypeOptional/' + idTypeOptional + '/' + idItem, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 118 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["insertItemOptional"] = insertItemOptional;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["delItemOptional"] = delItemOptional;
/** *   optionalServices   ***/

/** *   GET   ***/

function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/optional/all/' + ln, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/optional', data, fnSuccess, fnError);
}

function insertItemOptional(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/optional/insertItemOptional', data, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(dtoNation, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/optional', dtoNation, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/optional/' + data, fnSuccess, fnError);
}

function delItemOptional(idOptional, idItem, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/optional/delItemOptional/' + idOptional + '/' + idItem, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 119 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/** *   businessnamesupplierServices   ***/

/** *   GET   ***/

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/businessnamesupplier/select?search' + search, fnSuccess, fnError);
}

/***/ }),
/* 120 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["searchImplemented"] = searchImplemented;
/** *   searchItemsServices   ***/

/** *   POST   ***/

function searchImplemented(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchitems/searchImplemented', search, fnSuccess, fnError);
}
/** *   END POST   ***/

/***/ }),
/* 121 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAllCustom"] = getAllCustom;
/* harmony export (immutable) */ __webpack_exports__["getByCode"] = getByCode;
/* harmony export (immutable) */ __webpack_exports__["getByRole"] = getByRole;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["disable"] = disable;
/* harmony export (immutable) */ __webpack_exports__["enable"] = enable;
/** *   MenuAdminService   ***/

/** *   GET   ***/

function getAllCustom(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }

  serviceHelpers.getAutenticate('/api/v1/menuAdmin/allCustom/' + idLanguage, fnSuccess, fnError);
}

function getByCode(code, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/menuAdmin/getByCode/' + code, fnSuccess, fnError);
}

function getByRole(idRole, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/menuAdmin/getByRole/' + idRole, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(dtoMenuAdmin, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/menuAdmin', dtoMenuAdmin, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(dtoMenuAdmin, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/menuAdmin', dtoMenuAdmin, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/menuAdmin/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

function disable(menu_admin_id, role_id, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/menuAdmin/disable/' + menu_admin_id + '/' + role_id, fnSuccess, fnError);
}

function enable(menu_admin_id, role_id, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/menuAdmin/enable/' + menu_admin_id + '/' + role_id, fnSuccess, fnError);
}

/***/ }),
/* 122 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getByViewValue"] = getByViewValue;
/* harmony export (immutable) */ __webpack_exports__["updateInterventionUpdateProcessingSheet"] = updateInterventionUpdateProcessingSheet;
/* harmony export (immutable) */ __webpack_exports__["getDescriptionAndPriceProcessingSheet"] = getDescriptionAndPriceProcessingSheet;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["getAllProcessingView"] = getAllProcessingView;
/* harmony export (immutable) */ __webpack_exports__["getAllProcessingSearch"] = getAllProcessingSearch;
/* harmony export (immutable) */ __webpack_exports__["getAllViewQuoteSearch"] = getAllViewQuoteSearch;
/** *   processingviewServices   ***/

/** *   GET   ***/

function getByViewValue(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/processingview/getByViewValue/' + id, fnSuccess, fnError);
}

function updateInterventionUpdateProcessingSheet(DtoInterventions, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/processingview/updateInterventionUpdateProcessingSheet', DtoInterventions, fnSuccess, fnError);
}

// export function getAllProcessingView(id, fnSuccess, fnError) {
// serviceHelpers.getAutenticate('/api/v1/processingview/getAllProcessingView/' + id, fnSuccess, fnError);
// }

function getDescriptionAndPriceProcessingSheet(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/processingview/getDescriptionAndPriceProcessingSheet/' + id, fnSuccess, fnError);
}

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/processingview/' + id, fnSuccess, fnError);
}

function getAllProcessingView(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/processingview/getAllProcessingView/' + idLanguage, fnSuccess, fnError);
}

/* ----- Gestione Materiali ----------- */

function getAllProcessingSearch(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/processingview/getAllProcessingSearch/' + idLanguage, fnSuccess, fnError);
}

function getAllViewQuoteSearch(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/processingview/getAllViewQuoteSearch/' + idLanguage, fnSuccess, fnError);
}

/***/ }),
/* 123 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["updateInterventionUpdateViewQuotes"] = updateInterventionUpdateViewQuotes;
/* harmony export (immutable) */ __webpack_exports__["getByViewQuotes"] = getByViewQuotes;
/* harmony export (immutable) */ __webpack_exports__["getAllViewQuote"] = getAllViewQuote;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   processingviewServices   ***/

/** *   GET   ***/

function updateInterventionUpdateViewQuotes(DtoQuotes, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/viewquotes/updateInterventionUpdateViewQuotes', DtoQuotes, fnSuccess, fnError);
}

function getByViewQuotes(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/viewquotes/getByViewQuotes/' + id, fnSuccess, fnError);
}

function getAllViewQuote(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/viewquotes/getAllViewQuote/' + idLanguage, fnSuccess, fnError);
}

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/viewquotes/' + id, fnSuccess, fnError);
}

/** *   END POST   ***/

/***/ }),
/* 124 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   listServices   ***/

/** *   GET   */

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/list/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/list/select?search=' + search, fnSuccess, fnError);
}
function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/list/' + id, fnSuccess, fnError);
}

function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/list/' + id, fnSuccess, fnError);
}

function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/list/all/' + idLanguage, fnSuccess, fnError);
}

/** *   END GET   */

/** *   PUT   ***/

function insert(DtoList, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/list', DtoList, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(DtoList, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/list', DtoList, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/list/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 125 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/** *  regionsServices   ***/

/** *   GET   ***/

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/regions/select?search' + search, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   INSERT   ***/

/** *   END INSERT   ***/

/** *   POST   ***/

/** *   END POST   ***/

/** * DELETE ***/

/** * DELETE ***/

/***/ }),
/* 126 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/** *  provincesServices   ***/

/** *   GET   ***/

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/provinces/select?search' + search, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   INSERT   ***/

/** *   END INSERT   ***/

/** *   POST   ***/

/** *   END POST   ***/

/** * DELETE ***/

/** * DELETE ***/

/***/ }),
/* 127 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["activeAddressByUser"] = activeAddressByUser;
/* harmony export (immutable) */ __webpack_exports__["getAllByUser"] = getAllByUser;
/* harmony export (immutable) */ __webpack_exports__["deleteAddressById"] = deleteAddressById;
/** *   userAddressServices   ***/

/** *   POST   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/useraddress', data, fnSuccess, fnError);
}

function activeAddressByUser(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/useraddress/activeAddressByUser', data, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   GET   ***/

function getAllByUser(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/useraddress/' + id, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   DELETE   ***/

function deleteAddressById(id, fnSuccess, fnError) {
  serviceHelpers.del('/api/v1/useraddress/deleteAddressById/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 128 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAllBySales"] = getAllBySales;
/* harmony export (immutable) */ __webpack_exports__["getAllBySalesmanId"] = getAllBySalesmanId;
/* harmony export (immutable) */ __webpack_exports__["getByDetailSaleOrder"] = getByDetailSaleOrder;
/* harmony export (immutable) */ __webpack_exports__["getAllSalesOrder"] = getAllSalesOrder;
/* harmony export (immutable) */ __webpack_exports__["getBySalesOrder"] = getBySalesOrder;
/* harmony export (immutable) */ __webpack_exports__["updateSalesOrderDetail"] = updateSalesOrderDetail;
/* harmony export (immutable) */ __webpack_exports__["updateTrackingShipment"] = updateTrackingShipment;
/* harmony export (immutable) */ __webpack_exports__["updateNote"] = updateNote;
/* harmony export (immutable) */ __webpack_exports__["insertAndSendMessages"] = insertAndSendMessages;
/* harmony export (immutable) */ __webpack_exports__["SendMailCustomer"] = SendMailCustomer;
/* harmony export (immutable) */ __webpack_exports__["SendShipment"] = SendShipment;
/* harmony export (immutable) */ __webpack_exports__["insertMessages"] = insertMessages;
/* harmony export (immutable) */ __webpack_exports__["insertFileOrderSales"] = insertFileOrderSales;
/* harmony export (immutable) */ __webpack_exports__["deleteOrderDetail"] = deleteOrderDetail;
/* harmony export (immutable) */ __webpack_exports__["sendEmailDetailSummaryOrder"] = sendEmailDetailSummaryOrder;
/***   salesOrderServices   ***/

/***   GET   ***/
function getAllBySales(id, idLanguage, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/salesorder/' + id + '/' + idLanguage, fnSuccess, fnError);
}

function getAllBySalesmanId(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/salesorder/getAllBySalesmanId/', data, fnSuccess, fnError);
}
/***   END GET   ***/

/***   GET   ***/
function getByDetailSaleOrder(id, idLanguage, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/salesorder/getByDetailSaleOrder/' + id + '/' + idLanguage, fnSuccess, fnError);
}
/***   END GET   ***/

function getAllSalesOrder(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/salesorder/getAllSalesOrder', fnSuccess, fnError);
}

function getBySalesOrder(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/salesorder/getBySalesOrder/' + id, fnSuccess, fnError);
}

function updateSalesOrderDetail(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesorder/updateSalesOrderDetail/', data, fnSuccess, fnError);
}

function updateTrackingShipment(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesorder/updateTrackingShipment/', data, fnSuccess, fnError);
}
function updateNote(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesorder/updateNote/', data, fnSuccess, fnError);
}

function insertAndSendMessages(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesorder/insertAndSendMessages/', data, fnSuccess, fnError);
}

function SendMailCustomer(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesorder/SendMailCustomer/', data, fnSuccess, fnError);
}
function SendShipment(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesorder/SendShipment/', data, fnSuccess, fnError);
}
function insertMessages(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/salesorder/insertMessages/', data, fnSuccess, fnError);
}
function insertFileOrderSales(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesorder/insertFileOrderSales/', data, fnSuccess, fnError);
}
function deleteOrderDetail(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/salesorder/deleteOrderDetail/' + id, fnSuccess, fnError);
}
function sendEmailDetailSummaryOrder(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesorder/sendEmailDetailSummaryOrder/', data, fnSuccess, fnError);
}

/***/ }),
/* 129 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["getAllByCart"] = getAllByCart;
/* harmony export (immutable) */ __webpack_exports__["insertRequest"] = insertRequest;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["modifyQuantity"] = modifyQuantity;
function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/CartDetail/' + id, fnSuccess, fnError);
}

function getAllByCart(idCart, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/CartDetail/getAllByCart/' + idCart, fnSuccess, fnError);
}

function insertRequest(cart_id, user_id, session_token, idlanguage, quantity, item_id, fnSuccess, fnError) {
  if (session_token == null || session_token == '') {
    session_token = 'Anonymous ' + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    storageData.setTokenKey(session_token);
  }

  var data = {
    cart_id: cart_id,
    user_id: user_id,
    session_token: session_token,
    idlanguage: idlanguage,
    quantity: quantity,
    item_id: item_id
  };

  serviceHelpers.putAutenticate('/api/v1/CartDetail/insertRequest', data, fnSuccess, fnError);
}

function update(DtoCategories, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/CartDetail', DtoCartDetail, fnSuccess, fnError);
}

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/CartDetail/' + id, fnSuccess, fnError);
}

function modifyQuantity(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/CartDetail/modifyQuantity', data, fnSuccess, fnError);
}

/***/ }),
/* 130 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["getTotalAmount"] = getTotalAmount;
/* harmony export (immutable) */ __webpack_exports__["getNrProductRows"] = getNrProductRows;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Cart/' + id, fnSuccess, fnError);
}

function getTotalAmount(idCart, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Cart/getTotalAmount/' + idCart, fnSuccess, fnError);
}

function getNrProductRows(idCart, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Cart/getNrProductRows/' + idCart, fnSuccess, fnError);
}

function insert(DtoCartDetail, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/Cart', DtoCartDetail, fnSuccess, fnError);
}

function update(DtoCategories, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/Cart', DtoCartDetail, fnSuccess, fnError);
}

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/Cart/' + id, fnSuccess, fnError);
}

/***/ }),
/* 131 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["getByCode"] = getByCode;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   voucherServices   ***/

/** *   GET   ***/

function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/voucher/all', fnSuccess, fnError);
}

function getByCode(code, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/voucher/getByCode/' + code, fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(dtoVoucher, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/voucher', dtoVoucher, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(dtoVoucher, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/voucher', dtoVoucher, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/voucher/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 132 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getSelect2ForBootstrapTable"] = getSelect2ForBootstrapTable;
/* harmony export (immutable) */ __webpack_exports__["select"] = select;
/* harmony export (immutable) */ __webpack_exports__["getById"] = getById;
/* harmony export (immutable) */ __webpack_exports__["get"] = get;
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   voucherTypeServices   ***/

/** *   GET   */

function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/VoucherType/select',
      dataType: 'json',
      headers: {
        Accept: 'application/json',
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function data(params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function processResults(result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/VoucherType/select?search=' + search, fnSuccess, fnError);
}
function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/VoucherType/' + id, fnSuccess, fnError);
}

function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/VoucherType/' + id, fnSuccess, fnError);
}

function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/VoucherType/all/' + idLanguage, fnSuccess, fnError);
}

/** *   END GET   */

/** *   PUT   ***/

function insert(dtoVoucherType, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/VoucherType', dtoVoucherType, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(dtoVoucherType, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/VoucherType', dtoVoucherType, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/VoucherType/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

/***/ }),
/* 133 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAll"] = getAll;
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["update"] = update;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["getGroupSelectedAndNot"] = getGroupSelectedAndNot;
/* harmony export (immutable) */ __webpack_exports__["getCarriersSelectedAndNot"] = getCarriersSelectedAndNot;
/* harmony export (immutable) */ __webpack_exports__["getUserSelectedAndNot"] = getUserSelectedAndNot;
/* harmony export (immutable) */ __webpack_exports__["getItemsSelectedAndNot"] = getItemsSelectedAndNot;
/* harmony export (immutable) */ __webpack_exports__["getCategoriesSelectedAndNot"] = getCategoriesSelectedAndNot;
/* harmony export (immutable) */ __webpack_exports__["insertCarriersCartRole"] = insertCarriersCartRole;
/* harmony export (immutable) */ __webpack_exports__["insertCategoriesCartRoleCategories"] = insertCategoriesCartRoleCategories;
/* harmony export (immutable) */ __webpack_exports__["insertUserCartRoleUser"] = insertUserCartRoleUser;
/* harmony export (immutable) */ __webpack_exports__["insertCartRoleGroup"] = insertCartRoleGroup;
/* harmony export (immutable) */ __webpack_exports__["insertItemCartRole"] = insertItemCartRole;
/* harmony export (immutable) */ __webpack_exports__["getUserCartRolesSearch"] = getUserCartRolesSearch;
/* harmony export (immutable) */ __webpack_exports__["getItemsCartRolesSearch"] = getItemsCartRolesSearch;
/* harmony export (immutable) */ __webpack_exports__["getItemsSearchDescriptionCartRuleItems"] = getItemsSearchDescriptionCartRuleItems;
/* harmony export (immutable) */ __webpack_exports__["getCarriersCartRolesSearch"] = getCarriersCartRolesSearch;
/* harmony export (immutable) */ __webpack_exports__["getUserSearchBusinessNameCartRuleUser"] = getUserSearchBusinessNameCartRuleUser;
/* harmony export (immutable) */ __webpack_exports__["getGroupCartRolesSearch"] = getGroupCartRolesSearch;
/* harmony export (immutable) */ __webpack_exports__["getCategoriesCartRolesSearch"] = getCategoriesCartRolesSearch;
/* harmony export (immutable) */ __webpack_exports__["getSearchDescriptionCartRuleCategories"] = getSearchDescriptionCartRuleCategories;
/* harmony export (immutable) */ __webpack_exports__["getCarrierSearchNameCartRuleCarriers"] = getCarrierSearchNameCartRuleCarriers;
/* harmony export (immutable) */ __webpack_exports__["getGroupSearchNameCartRuleGroup"] = getGroupSearchNameCartRuleGroup;
/* harmony export (immutable) */ __webpack_exports__["deleteUserCartRoleUser"] = deleteUserCartRoleUser;
/* harmony export (immutable) */ __webpack_exports__["deleteCategoriesCartRoleCategories"] = deleteCategoriesCartRoleCategories;
/* harmony export (immutable) */ __webpack_exports__["deleteCartRoleGroup"] = deleteCartRoleGroup;
/* harmony export (immutable) */ __webpack_exports__["deleteCartRoleCarriers"] = deleteCartRoleCarriers;
/* harmony export (immutable) */ __webpack_exports__["deleteCartRoleItem"] = deleteCartRoleItem;
/** *   voucherServices   ***/

/** *   GET   ***/

function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/cartRules/all', fnSuccess, fnError);
}

/** *   END GET   ***/

/** *   PUT   ***/

function insert(dtoVoucher, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/cartRules', dtoVoucher, fnSuccess, fnError);
}

/** *   END PUT   ***/

/** *   POST   ***/

function update(dtoVoucher, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/cartRules', dtoVoucher, fnSuccess, fnError);
}

/** *   END POST   ***/

/** *   DELETE   ***/

function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/cartRules/' + id, fnSuccess, fnError);
}

/** *   END DELETE   ***/

function getGroupSelectedAndNot(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/cartRules/getGroupSelectedAndNot/' + id, fnSuccess, fnError);
}

function getCarriersSelectedAndNot(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/cartRules/getCarriersSelectedAndNot/' + id, fnSuccess, fnError);
}

function getUserSelectedAndNot(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/cartRules/getUserSelectedAndNot/' + id, fnSuccess, fnError);
}
function getItemsSelectedAndNot(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/cartRules/getItemsSelectedAndNot/' + id, fnSuccess, fnError);
}

function getCategoriesSelectedAndNot(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/cartRules/getCategoriesSelectedAndNot/' + id, fnSuccess, fnError);
}

function insertCarriersCartRole(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/cartRules/insertCarriersCartRole', data, fnSuccess, fnError);
}
function insertCategoriesCartRoleCategories(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/cartRules/insertCategoriesCartRoleCategories', data, fnSuccess, fnError);
}

function insertUserCartRoleUser(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/cartRules/insertUserCartRoleUser', data, fnSuccess, fnError);
}

function insertCartRoleGroup(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/cartRules/insertCartRoleGroup', data, fnSuccess, fnError);
}

function insertItemCartRole(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/cartRules/insertItemCartRole', data, fnSuccess, fnError);
}

function getUserCartRolesSearch(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/cartRules/getUserCartRolesSearch', data, fnSuccess, fnError);
}
function getItemsCartRolesSearch(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/cartRules/getItemsCartRolesSearch', data, fnSuccess, fnError);
}

function getItemsSearchDescriptionCartRuleItems(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/cartRules/getItemsSearchDescriptionCartRuleItems', data, fnSuccess, fnError);
}

function getCarriersCartRolesSearch(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/cartRules/getCarriersCartRolesSearch', data, fnSuccess, fnError);
}

function getUserSearchBusinessNameCartRuleUser(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/cartRules/getUserSearchBusinessNameCartRuleUser', data, fnSuccess, fnError);
}
function getGroupCartRolesSearch(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/cartRules/getGroupCartRolesSearch', data, fnSuccess, fnError);
}
function getCategoriesCartRolesSearch(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/cartRules/getCategoriesCartRolesSearch', data, fnSuccess, fnError);
}

function getSearchDescriptionCartRuleCategories(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/cartRules/getSearchDescriptionCartRuleCategories', data, fnSuccess, fnError);
}

function getCarrierSearchNameCartRuleCarriers(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/cartRules/getCarrierSearchNameCartRuleCarriers', data, fnSuccess, fnError);
}
function getGroupSearchNameCartRuleGroup(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/cartRules/getGroupSearchNameCartRuleGroup', data, fnSuccess, fnError);
}

function deleteUserCartRoleUser(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/cartRules/deleteUserCartRoleUser/' + id, fnSuccess, fnError);
}
function deleteCategoriesCartRoleCategories(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/cartRules/deleteCategoriesCartRoleCategories/' + id, fnSuccess, fnError);
}

function deleteCartRoleGroup(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/cartRules/deleteCartRoleGroup/' + id, fnSuccess, fnError);
}
function deleteCartRoleCarriers(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/cartRules/deleteCartRoleCarriers/' + id, fnSuccess, fnError);
}

function deleteCartRoleItem(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/cartRules/deleteCartRoleItem/' + id, fnSuccess, fnError);
}

/***/ }),
/* 134 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAllexternalRepair"] = getAllexternalRepair;
/* harmony export (immutable) */ __webpack_exports__["getByRepair"] = getByRepair;
/* harmony export (immutable) */ __webpack_exports__["getbyFaultModule"] = getbyFaultModule;
/* harmony export (immutable) */ __webpack_exports__["updateRepairExternal"] = updateRepairExternal;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/** *   externalRepairServices   ***/

/** *   GET   ***/

function getAllexternalRepair(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/externalrepair/getAllexternalRepair/' + idLanguage, fnSuccess, fnError);
}
function getByRepair(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/externalrepair/getByRepair/' + id, fnSuccess, fnError);
}

function getbyFaultModule(id, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/externalrepair/getbyFaultModule/' + id, fnSuccess, fnError);
}
function updateRepairExternal(DtoExternalRepair, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/externalrepair/updateRepairExternal', DtoExternalRepair, fnSuccess, fnError);
}
/** * DELETE ***/
function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/externalrepair/' + id, fnSuccess, fnError);
}
/** * DELETE ***/

/***/ }),
/* 135 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["getUserData"] = getUserData;
/* harmony export (immutable) */ __webpack_exports__["updateFast"] = updateFast;
/** *   FasturgenciesServices   ***/

/** *   INSERT   ***/

function insert(DtoFast, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/fasturgencies', DtoFast, fnSuccess, fnError);
}

function getUserData(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/fasturgencies/getUserData/' + id, fnSuccess, fnError);
}

function updateFast(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/fasturgencies/updateFast', data, fnSuccess, fnError);
}

/***/ }),
/* 136 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["insert"] = insert;
/* harmony export (immutable) */ __webpack_exports__["getAllInternalRepair"] = getAllInternalRepair;
/* harmony export (immutable) */ __webpack_exports__["getByInternalRepair"] = getByInternalRepair;
/* harmony export (immutable) */ __webpack_exports__["updateInternalRepair"] = updateInternalRepair;
/* harmony export (immutable) */ __webpack_exports__["del"] = del;
/* harmony export (immutable) */ __webpack_exports__["getLastId"] = getLastId;
/** *   internalRepairServices   ***/

/** *   GET   ***/

function insert(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/internalrepair', data, fnSuccess, fnError);
}
function getAllInternalRepair(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/internalrepair/getAllInternalRepair/' + idLanguage, fnSuccess, fnError);
}
function getByInternalRepair(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/internalrepair/getByInternalRepair/' + id, fnSuccess, fnError);
}
function updateInternalRepair(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/internalrepair/updateInternalRepair', data, fnSuccess, fnError);
}
/** * DELETE ***/
function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/internalrepair/' + id, fnSuccess, fnError);
}

function getLastId(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/internalrepair/getLastId/', fnSuccess, fnError);
}

/***/ }),
/* 137 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["searchImplementedItemsGr"] = searchImplementedItemsGr;
/* harmony export (immutable) */ __webpack_exports__["getResultDetail"] = getResultDetail;
/* harmony export (immutable) */ __webpack_exports__["searchImplementedItemsGlobal"] = searchImplementedItemsGlobal;
/***   searchItemsGrServices   ***/

/***   POST   ***/

function searchImplementedItemsGr(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchitemsGr/searchImplementedItemsGr', search, fnSuccess, fnError);
}
/***   END POST   ***/

function getResultDetail(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/searchitemsGr/getResultDetail/' + id, fnSuccess, fnError);
}

function searchImplementedItemsGlobal(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchitemsGr/searchImplementedItemsGlobal', search, fnSuccess, fnError);
}

/***/ }),
/* 138 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["getAllInterventionCustomer"] = getAllInterventionCustomer;
/* harmony export (immutable) */ __webpack_exports__["getByViewQuotesCustomer"] = getByViewQuotesCustomer;
/* harmony export (immutable) */ __webpack_exports__["updateInterventionUpdateViewCustomerQuotes"] = updateInterventionUpdateViewCustomerQuotes;
/* harmony export (immutable) */ __webpack_exports__["getByViewModuleCustomer"] = getByViewModuleCustomer;
/** *   interventionCustomerServices   ***/

function getAllInterventionCustomer(idUser, fnSuccess, fnError) {
  if (idUser === undefined || idUser == '') {
    idUser = storageData.idUser();
  }
  serviceHelpers.getAutenticate('/api/v1/interventioncustomer/getAllInterventionCustomer/' + idUser, fnSuccess, fnError);
}

function getByViewQuotesCustomer(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/interventioncustomer/getByViewQuotesCustomer/' + id, fnSuccess, fnError);
}

function updateInterventionUpdateViewCustomerQuotes(DtoQuotes, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/interventioncustomer/updateInterventionUpdateViewCustomerQuotes', DtoQuotes, fnSuccess, fnError);
}

function getByViewModuleCustomer(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/interventioncustomer/getByViewModuleCustomer/' + id, fnSuccess, fnError);
}

/***/ }),
/* 139 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (immutable) */ __webpack_exports__["search"] = search;
/* harmony export (immutable) */ __webpack_exports__["searchPaginatorDataSheet"] = searchPaginatorDataSheet;
/***   searchDataSheetServices   ***/

/***   POST   ***/
function search(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchdatasheet/searchDataSheet', search, fnSuccess, fnError);
}

function searchPaginatorDataSheet(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchdatasheet/searchPaginatorDataSheet', search, fnSuccess, fnError);
}
/***   END POST   ***/

/***/ })
/******/ ]);
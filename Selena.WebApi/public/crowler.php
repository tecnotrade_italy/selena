<?php
// dichiarazioni variabili utilizzate per i metatag

$url = $_GET["url"];
$title = "";
$description = "";
$img = "";
$shareTitle = "";
$shareDescription = "";
$content = "";
// connessione db tramite file env
$db_connection = pg_connect("host=localhost dbname=selena user=selena password=!T3cD3v!");
$stat = pg_connection_status($db_connection);

if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] != "off") {
  $protocol = "https://";
} else {
  $protocol = "http://";
}

$server = $_SERVER["HTTP_HOST"];

// query per estrazione pagine
$result = pg_query($db_connection, "SELECT * FROM languages_pages WHERE url ='" . $url . "' LIMIT 1");
if($result){
  $arr = pg_fetch_array($result);
  $title = $arr['title'];
  $description = $arr['seo_description'];
  $img = $arr['url_cover_image'];
  $shareTitle = $arr['share_title'];
  $shareDescription = $arr['share_description'];
  $content = $arr['content'];
}

?>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title><?php echo $title; ?></title>
    <meta name="robots" content="noindex, nofollow">
    <meta name="description" content="<?php echo $description; ?>">
    <link rel="canonical" href="<?php echo $protocol . $server . $_GET["url"]; ?>" />
    <meta property="og:url" content="<?php echo $protocol . $server . $_GET["url"]; ?>">
    <meta property="og:type" content="article">
    <meta property="og:title" content="<?php echo $shareTitle; ?>">
    <meta property="og:description" content="<?php echo $shareDescription; ?>">
    <meta property="og:image:url" content="<?php echo $img;?>">
    <meta property="og:image:secure_url" content="<?php echo $img;?>">
    <meta property="og:image" content="<?php echo $img;?>">
  </head>
  <body>
  <?php echo $content ?>
  </body>
</html>
<?php
pg_close($db_connection);
?>

# Creazion DB
Creare i seguenti DB su postgres:
- selena
- selena_log
- selena_test
- selena_test_log

e i relativi utenti presenti nel file .env

# Creazione tabelle
php artisan migrate

# Installare passport
php artisan passport:install

# Inserire i dati presenti nei seed
php artisan db:seed

# Creare i dbLink tra i DB dei dati e dei log

- Su selena
CREATE EXTENSION dblink;
CREATE SERVER selena_log_remote FOREIGN DATA WRAPPER dblink_fdw OPTIONS (host 'localhost', dbname 'selena_log', port '5432');
CREATE USER MAPPING FOR selena SERVER selena_log_remote OPTIONS (user 'selena_log',password '!T3cD3v!');
GRANT USAGE ON FOREIGN SERVER selena_log_remote TO selena;

- Su selena_test
CREATE EXTENSION dblink;
CREATE SERVER selena_log_remote FOREIGN DATA WRAPPER dblink_fdw OPTIONS (host 'localhost', dbname 'selena_test_log', port '5432');
CREATE USER MAPPING FOR selena SERVER selena_log_remote OPTIONS (user 'selena_test_log',password '!T3cD3v!');
GRANT USAGE ON FOREIGN SERVER selena_log_remote TO selena;

# Creare le funzione per loggare
Eseguire i file sql presenti in /database/SQL/ sui DB dei dati ed eseguire:
- log_function.sql
- log_trigger.sql
- SELECT log_trigger();

per la creazione dei trigger dei log
#!/bin/bash
cd Selena.Web
npm install --quiet

cd ../Selena.Web/src/data/
echo " La seguente procedura andrà ad installare l'ambiente per il dominio: '$1'. "
cat config.js | sed -e "s|http://localhost|$1|g" >> configNew.js
rm config.js
mv configNew.js config.js
echo " Dominio: '$1' impostato correttamente per la sezione amministrativa del sito (Selena.Web) "

cd ../../
echo " Eseguo il build della sezione amministrativa (Selena.Web) "
npm run --quiet build
echo " Sezione amministrativa pronta (Selena.Web) "



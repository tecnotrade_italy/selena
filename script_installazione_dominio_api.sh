#!/bin/bash
cd Selena.WebApi
echo " Set up ambiente API (Selena.WebApi) "
composer install
echo " Composer installato (Selena.WebApi) "
php artisan migrate
echo " Migration effettuate (Selena.WebApi) "
php artisan passport:install
php artisan key:generate
composer global require “laravel/installer”
php artisan config:cache
php artisan cache:clear
php artisan storage:link
echo " Configurazioni ottimizzate e parametri generali aggiornati (Selena.WebApi) "

cd app
cat User.php | sed -e "s|use ObservantTrait;|//use ObservantTrait;|g" >> UserNew.php
rm User.php
mv UserNew.php User.php

cd ../
php artisan db:seed
php artisan db:seed --class=DefaultDataSeeder
php artisan db:seed --class=AggiornamentoVociMenuSeeder
php artisan db:seed --class=MenuAdminSelena
php artisan db:seed --class=DefaultDataSeeder
php artisan db:seed --class=AggiornamentoVociMenuSeeder
php artisan db:seed --class=MenuAdminSelena
php artisan db:seed --class=NewContentTemplateEditorSeeder

echo " Database popolato (Selena.WebApi) "

cd app
cat User.php | sed -e "s|//use ObservantTrait;|use ObservantTrait;|g" >> UserNew.php
rm User.php
mv UserNew.php User.php

cd ../
composer require intervention/image
php artisan vendor:publish --provider="Intervention\Image\ImageServiceProviderLaravelRecent"

cd storage/app/public/
echo " Creazione file e cartelle necessarie per la sezione amministrativa (Selena.WebApi) "
mkdir css js videos images files images/TemplateEditor images/TemplateEditor/Previews 
touch css/web.css js/site.js js/sitemin.js js/siteminhead.js css/webmin.css css/webminCssHead.css css/webCssEmail.css
echo " Creazione completata (Selena.WebApi) "

cd ../../../
echo " Installazione npm (Selena.WebApi) "
npm install

cd resources/assets/js/data/
cat config.js | sed -e "s|http://localhost|$1|g" >> configNew.js
rm config.js
mv configNew.js config.js
echo " Dominio: '$1' impostato correttamente nella carrella (Selena.WebApi)"

cd ../../../../
echo " Eseguo la generazione dei pacchetti aggiornati (Selena.WebApi) "
echo '7zaMGPw5Qk2pJJXDsQ6q' | sudo -S npm run prod

echo " Impostazione permessi alla cartella storage (Selena.WebApi) "
chmod -R 775 storage




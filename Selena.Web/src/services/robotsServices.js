/***   robotsServices   ***/

/***   GET   ***/

export function get(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/robots/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

/***   END GET   ***/

/***   POST   ***/

export function updateRobots(css, fnSuccess, fnError) {
  var data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    css: css
  };

  serviceHelpers.postAutenticate('/api/v1/robots/updateRobots', data, fnSuccess, fnError);
}

/***   END POST   ***/

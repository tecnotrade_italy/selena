/***   groupSmsServices   ***/
/***   GET   ***/
export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/groupSms/all', fnSuccess, fnError);
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/groupSms/select?search' + search, fnSuccess, fnError);
}

/***   END GET   ***/

/***   POST   ***/
export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/groupSms/insertGroupSms', data, fnSuccess, fnError);
}

export function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/groupSms/updateGroupSms', data, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/
export function del(id, fnSuccess, fnError) {
    serviceHelpers.delAutenticate('/api/v1/groupSms/' +  id, fnSuccess, fnError);
}
/***   END DELETE   ***/

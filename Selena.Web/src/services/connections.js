/***   connectionsServices   ***/

/***   GET   ***/
export function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/connections/select',
      dataType: 'json',
      headers: {
        Accept: "application/json",
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function (params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function (result) {
        return {
          results: result.data
        };
      }
    },
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/connections/select?search=' + search, fnSuccess, fnError);
}

export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/connections/' + id, fnSuccess, fnError);
}

export function getByEventResultForParameter(DtoConnections, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/connections/getByEventResultForParameter', DtoConnections, fnSuccess, fnError);
}

export function getAll(idLanguage, fnSuccess, fnError) {
  if(idLanguage === undefined || idLanguage==''){
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/connections/all/' + idLanguage, fnSuccess, fnError);
}
/***   END GET   ***/

/***   PUT   ***/
export function insert(DtoConnections, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/connections', DtoConnections, fnSuccess, fnError);
}
/***   END PUT   ***/

/***   POST   ***/
export function update(DtoConnections, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/connections', DtoConnections, fnSuccess, fnError);
}
/***   END POST   ***/

/*** DELETE ***/
export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/connections/' + id, fnSuccess, fnError);
}
/*** DELETE ***/

/***   clone ***/
export function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/connections/clone/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), null, fnSuccess, fnError);
}
/***   END  clone ***/


export function insertPrenotazioneForCliBooking(guida, audioGuida, RidottamobilitaNecessitaascensore, idCart, idCartDetail, idUser, disponibilita, connection_id,start_time, date_from, end_time, price,total_price, nr_place_selected,reduced_price_selected, fnSuccess, fnError) {
  
  var data = {
    guida:guida,
    audioGuida: audioGuida,
    RidottamobilitaNecessitaascensore: RidottamobilitaNecessitaascensore,
    cart_id: idCart,
    cart_detail_id: idCartDetail,
    user_id: idUser,
    disponibilita: disponibilita,
    connection_id: connection_id,
    start_time: start_time,
    date_from: date_from,
    end_time: end_time,
    price: price,
    total_price:total_price,
    nr_place_selected: nr_place_selected,
    reduced_price_selected:reduced_price_selected
  };
 
  serviceHelpers.putAutenticate('/api/v1/connections/insertPrenotazioneForCliBooking', data, fnSuccess, fnError);
}



/***  provincesServices   ***/

/***   GET   ***/

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/provinces/select?search' + search, fnSuccess, fnError);
}

export function getAll(idLanguage, fnSuccess, fnError) {
  if(idLanguage === undefined || idLanguage==''){
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/provinces/all/' + idLanguage, fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/provinces', data, fnSuccess, fnError);
}

export function getByIdProvince(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/provinces/getByIdProvince/' + id, fnSuccess, fnError);
}

/***   POST   ***/

export function update(dtoProvince, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/provinces', dtoProvince, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/provinces/' +  data, fnSuccess, fnError);
}

/***   END DELETE   ***/

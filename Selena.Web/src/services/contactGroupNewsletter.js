/***   contactGroupNewsletterServices   ***/

/***   GET   ***/

export function getIncludedByContactGroup(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/contactgroupnewsletter/includedgroup/' + id, fnSuccess, fnError);
}

export function getExcludedByContactGroup(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/contactgroupnewsletter/excludedgroup/' + id, fnSuccess, fnError);
}

/***   END GET   ***/

/*** PUT ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/contactgroupnewsletter', data, fnSuccess, fnError);
}

/*** END PUT ***/

/*** DELETE ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/contactgroupnewsletter/' + id, fnSuccess, fnError);
}

/*** END DELETE ***/


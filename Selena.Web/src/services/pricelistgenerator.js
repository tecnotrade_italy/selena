/***  pricelistgeneratorServices   ***/  

/***   GET   ***/

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/pricelistgenerator/select?search' + search, fnSuccess, fnError);
}

export function getAll(idLanguage, fnSuccess, fnError) {
  if(idLanguage === undefined || idLanguage==''){
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/pricelistgenerator/all/' + idLanguage, fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(dtoListGenerator, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/pricelistgenerator', dtoListGenerator, fnSuccess, fnError);
}

export function getAllByUser(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/pricelistgenerator/getAllByUser', data, fnSuccess, fnError);
}
export function getByExecuteResult(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/pricelistgenerator/getByExecuteResult', data, fnSuccess, fnError);
}

export function getAllByGroup(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/pricelistgenerator/getAllByGroup', data, fnSuccess, fnError);
}
export function getAllByCategories(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/pricelistgenerator/getAllByCategories', data, fnSuccess, fnError);
}
export function getAllByCategoriesNewList(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/pricelistgenerator/getAllByCategoriesNewList', data, fnSuccess, fnError);
}

export function getAllByDestinationUsers(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/pricelistgenerator/getAllByDestinationUsers', data, fnSuccess, fnError);
}
export function getAllByDestinationGroup(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/pricelistgenerator/getAllByDestinationGroup', data, fnSuccess, fnError);
}
export function getAllByDestinationCategories(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/pricelistgenerator/getAllByDestinationCategories', data, fnSuccess, fnError);
}
/***   END PUT   ***/
export function updateOnlineCheck(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/pricelistgenerator/updateOnlineCheck', data, fnSuccess, fnError);
}

/***   POST   ***/
export function update(dtoListGenerator, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/pricelistgenerator', dtoListGenerator, fnSuccess, fnError);
}
/***   END POST   ***/

/***   DELETE   ***/

export function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/pricelistgenerator/' +  data, fnSuccess, fnError);
}

/***   END DELETE   ***/

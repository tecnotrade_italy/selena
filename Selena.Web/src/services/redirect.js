/***   redirectServices   ***/

/***   GET   ***/
export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/redirect/select?search=' + search, fnSuccess, fnError);
}

export function getAll(fnSuccess, fnError) {

  serviceHelpers.getAutenticate('/api/v1/redirect/all', fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/redirect', data, fnSuccess, fnError);
}

export function insertRedirectCheck(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/redirect/insertRedirectCheck', data, fnSuccess, fnError);
}


/***   END PUT   ***/

/***   POST   ***/

export function update(dtoRedirect, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/redirect', dtoRedirect, fnSuccess, fnError);
}

export function updateTable(dtoRedirect, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/redirect/updateTable', dtoRedirect, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/redirect/' +  data, fnSuccess, fnError);
}

/***   END DELETE   ***/

/***   languageServices   ***/

/*** GET ***/

export function getDefault(fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/language/getDefault', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/language/getDefault',
  //   dataType: 'json',
  //   headers: {
  //     Accept: "application/json"
  //   },
  //   contentType: 'application/x-www-form-urlencoded; charset=UTF-8'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function get(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/language/get/' + id, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/language/get/' + id,
  //   dataType: 'json',
  //   headers: {
  //     Accept: "application/json"
  //   },
  //   contentType: 'application/x-www-form-urlencoded; charset=UTF-8'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}


export function select(search, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/language/select?search=' + search, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/language/select?search=' + search,
  // }).done(function (result) {
  //   fnSuccess(JSON.parse(result));
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function getConfiguration(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/language/getConfiguration', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/language/getConfiguration',
  //   dataType: 'json',
  //   headers: {
  //     Accept: "application/json",
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function all(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/language/all', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/language/all',
  //   dataType: 'json',
  //   headers: {
  //     Accept: "application/json",
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function selectlanguagestranslate(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/language/selectlanguagestranslate?search' + search, fnSuccess, fnError);
}

export function selectlanguagescategoriestranslate(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/language/selectlanguagescategoriestranslate?search' + search, fnSuccess, fnError);
}
export function selectlanguagesitemstranslate(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/language/selectlanguagesitemstranslate?search' + search, fnSuccess, fnError);
}

/*** END GET ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/language', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/language', data, fnSuccess, fnError);
}

/***   END POST   ***/

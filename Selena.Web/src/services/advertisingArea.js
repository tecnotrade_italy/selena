
  export function getById(id, fnSuccess, fnError) {
    serviceHelpers.getAutenticate('/api/v1/AdvertisingArea/' + id, fnSuccess, fnError);
  }
  
  export function getHtmlBanner(data, fnSuccess, fnError) {
    serviceHelpers.postAutenticate('/api/v1/AdvertisingArea/getHtmlBanner/', data, fnSuccess, fnError);
  }

  export function getAll(fnSuccess, fnError) {
    serviceHelpers.getAutenticate('/api/v1/AdvertisingArea/all/' , fnSuccess, fnError);
  }
  
  export function select(search, fnSuccess, fnError) {
    serviceHelpers.getAutenticate('/api/v1/AdvertisingArea/select?search=' + search, fnSuccess, fnError);
  }

  /***   END GET   */
  
  /***   PUT   ***/
  
  export function insert(dtoAdvertisingArea, fnSuccess, fnError) {
    serviceHelpers.putAutenticate('/api/v1/AdvertisingArea', dtoAdvertisingArea, fnSuccess, fnError);
  }
  
  /***   END PUT   ***/
  
  /***   POST   ***/
  
  export function update(dtoAdvertisingArea, fnSuccess, fnError) {
    serviceHelpers.postAutenticate('/api/v1/AdvertisingArea', dtoAdvertisingArea, fnSuccess, fnError);
  }
  
  /***   END POST   ***/
  
  /***   DELETE   ***/
  
  export function del(id, fnSuccess, fnError) {
    serviceHelpers.delAutenticate('/api/v1/AdvertisingArea/' + id, fnSuccess, fnError);
  }
  
  /***   END DELETE   ***/
  
  
  
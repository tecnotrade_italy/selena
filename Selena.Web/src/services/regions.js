/***  regionsServices   ***/  

/***   GET   ***/

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/regions/select?search' + search, fnSuccess, fnError);
}

export function getAll(idLanguage, fnSuccess, fnError) {
  if(idLanguage === undefined || idLanguage==''){
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/regions/all/' + idLanguage, fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/regions', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(dtoRegion, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/regions', dtoRegion, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/regions/' +  data, fnSuccess, fnError);
}

/***   END DELETE   ***/

/***   adminConfigurationServices   ***/

/*** GET ***/

export function getMenu(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/adminConfiguration/getMenu', fnSuccess, fnError);
}

export function getFunctionality(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/adminConfiguration/getFunctionality/' +
    menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

}

/*** END GET ***/

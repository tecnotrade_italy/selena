/***   settingServices   ***/

/***   GET   ***/

export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/setting/getAll', fnSuccess, fnError);
}

export function getLastSync(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/setting/getLastSync', fnSuccess, fnError);
}

export function getRenderMenu(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/setting/getRenderMenu', fnSuccess, fnError);
}

export function getRenderBlog(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/setting/getRenderBlog', fnSuccess, fnError);
}

export function getRenderNews(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/setting/getRenderNews', fnSuccess, fnError);
}

export function getByCode(code, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/setting/getByCode/' + code, fnSuccess, fnError);
}
/***   END GET   ***/



/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/setting', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/setting', data, fnSuccess, fnError);
}

export function updateImageNameFavicon(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/setting/updateImageNameFavicon', data, fnSuccess, fnError);
}

/***   END POST    ***/

/***   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/setting/' + id, fnSuccess, fnError);
}



/***   END DELETE    ***/

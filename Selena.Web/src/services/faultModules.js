/***   faultModulesServices   ***/

/***   GET   */

export function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/FaultModules/select',
      dataType: 'json',
      headers: {
        Accept: "application/json",
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function (params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function (result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/FaultModules/select?search=' + search, fnSuccess, fnError);
}

export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/FaultModules/' + id, fnSuccess, fnError);
}

export function getList(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/FaultModules/getList', data, fnSuccess, fnError);
}

export function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/FaultModules/' + id, fnSuccess, fnError);
}

export function getAll(idLanguage, fnSuccess, fnError) {
  if(idLanguage === undefined || idLanguage==''){
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/FaultModules/all/' + idLanguage, fnSuccess, fnError);
}


/***   END GET   */

/***   PUT   ***/

export function insert(DtoInterventions, fnSuccess, fnError) {
 
  serviceHelpers.putAutenticate('/api/v1/FaultModules', DtoInterventions, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(DtoInterventions, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/FaultModules', DtoInterventions, fnSuccess, fnError);
}

/***   END POST   ***/

/*** DELETE ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/FaultModules/' + id, fnSuccess, fnError);
}

/*** END DELETE ***/

/***   clone ***/
export function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/FaultModules/clone/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), null, fnSuccess, fnError);
}
/***   END  clone ***/

/***   supportsServices   ***/

/***   GET   ***/

export function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/supports/all/' + ln, fnSuccess, fnError);
}

export function getAllPricesSupports(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/supports/allPrices/' + id, fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/supports', data, fnSuccess, fnError);
}

export function insertSupportPrice(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/supports/insertSupportPrice', data, fnSuccess, fnError);
}

export function insertItemSupport(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/supports/insertItemSupport', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/supports', data, fnSuccess, fnError);
}

export function updateSupportPrice(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/supports/updateSupportPrice', data, fnSuccess, fnError);
}

export function updateUmSupportPrices(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/supports/updateUmSupportPrices', data, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/supports/' +  id, fnSuccess, fnError);
}
export function deleteRangeDetailSupportsPrice(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/supports/deleteRangeDetailSupportsPrice/' +  id, fnSuccess, fnError);
}

/***   END DELETE   ***/

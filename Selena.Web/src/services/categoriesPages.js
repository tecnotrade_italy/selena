/***   categoriesPagesServices   ***/

/***   GET   ***/
export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/categoriespages/select?search=' + search, fnSuccess, fnError);
}

export function getAll(idLanguage, fnSuccess, fnError) {
  if(idLanguage === undefined || idLanguage==''){
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/categoriespages/all/' + idLanguage, fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/
export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/categoriespages', data, fnSuccess, fnError);
}
/***   END PUT   ***/

/***   POST   ***/
export function update(dtoNation, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/categoriespages', dtoNation, fnSuccess, fnError);
}


/***   END POST   ***/

/***   DELETE   ***/
export function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/categoriespages/' +  data, fnSuccess, fnError);
}
/***   END DELETE   ***/

/***   customerGroupNewsletterServices   ***/

/***   GET   ***/

export function getIncludedByCustomerGroup(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/customergroupnewsletter/includedgroup/' + id, fnSuccess, fnError);
}

export function getExcludedByCustomerGroup(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/customergroupnewsletter/excludedgroup/' + id, fnSuccess, fnError);
}

/***   END GET   ***/

/*** PUT ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/customergroupnewsletter', data, fnSuccess, fnError);
}

/*** END PUT ***/

/*** DELETE ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/customergroupnewsletter/' + id, fnSuccess, fnError);
}

/*** END DELETE ***/


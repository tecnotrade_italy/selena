
  export function getById(id, fnSuccess, fnError) {
    serviceHelpers.getAutenticate('/api/v1/AmazonTemplate/' + id, fnSuccess, fnError);
  }
  

  export function getAll(fnSuccess, fnError) {
    serviceHelpers.getAutenticate('/api/v1/AmazonTemplate/all/' , fnSuccess, fnError);
  }
  
  export function select(search, fnSuccess, fnError) {
    serviceHelpers.getAutenticate('/api/v1/AmazonTemplate/select?search=' + search, fnSuccess, fnError);
  }

  /***   END GET   */
  
  /***   PUT   ***/
  
  export function insert(dtoAmazonTemplate, fnSuccess, fnError) {
    serviceHelpers.putAutenticate('/api/v1/AmazonTemplate', dtoAmazonTemplate, fnSuccess, fnError);
  }
  
  /***   END PUT   ***/
  
  /***   POST   ***/
  
  export function update(dtoAmazonTemplate, fnSuccess, fnError) {
    serviceHelpers.postAutenticate('/api/v1/AmazonTemplate', dtoAmazonTemplate, fnSuccess, fnError);
  }
  
  /***   END POST   ***/
  
  /***   DELETE   ***/
  
  export function del(id, fnSuccess, fnError) {
    serviceHelpers.delAutenticate('/api/v1/AmazonTemplate/' + id, fnSuccess, fnError);
  }
  
  /***   END DELETE   ***/
  
  
  
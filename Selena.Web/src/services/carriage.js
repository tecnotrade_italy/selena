/***  carriageServices   ***/

/***   GET   ***/

export function getByCustomer(idCustomer, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/carriage/getByCustomer/' + idCustomer + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

export function getSelect(routeParams, fnSuccess, fnError) {
  var data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(routeParams)
  };

  serviceHelpers.getAutenticateWithData('/api/v1/carriage/select', data, fnSuccess, fnError);
}

/***   END GET   ***/

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/carriage/select?search=' + search, fnSuccess, fnError);
}
export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/carriage/' + id, fnSuccess, fnError);
}

export function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/carriage/' + id, fnSuccess, fnError);
}

export function getAll(idLanguage, fnSuccess, fnError) {
  if(idLanguage === undefined || idLanguage==''){
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/carriage/all/' + idLanguage, fnSuccess, fnError);
}

/***   END GET   */

/***   PUT   ***/
export function insert(DtoCarriage, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/carriage', DtoCarriage, fnSuccess, fnError);
}
/***   END PUT   ***/

/***   POST   ***/
export function update(DtoCarriage, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/carriage', DtoCarriage, fnSuccess, fnError);
}
/***   POST   ***/
export function updateCheck(DtoCarriage, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/carriage/updateCheck', DtoCarriage, fnSuccess, fnError);
}
/***   END POST   ***/

/***   DELETE   ***/
export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/carriage/' + id, fnSuccess, fnError);
}

/***   processingviewServices   ***/

/***   GET   ***/

export function updateInterventionUpdateViewQuotes(DtoQuotes, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/viewquotes/updateInterventionUpdateViewQuotes', DtoQuotes, fnSuccess, fnError);
}

export function getByViewQuotes(id,idUser, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/viewquotes/getByViewQuotes/' + id + '/' + idUser, fnSuccess, fnError);
}

export function getAllViewQuote(idLanguage, fnSuccess, fnError) {
  if(idLanguage === undefined || idLanguage==''){
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/viewquotes/getAllViewQuote/' + idLanguage, fnSuccess, fnError);
}

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/viewquotes/' + id, fnSuccess, fnError);
}
export function getViewPdfQuotes(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/pdfviewquotes/' + id, fnSuccess, fnError);
}

export function updateandSendEmailQuotes(DtoQuotes, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/viewquotes/updateandSendEmailQuotes', DtoQuotes, fnSuccess, fnError);
}

export function insertMessageQuotes(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/viewquotes/insertMessageQuotes', data, fnSuccess, fnError);
}
export function insertReferent(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/viewquotes/insertReferent', data, fnSuccess, fnError);
}

export function SearchQuotes(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/viewquotes/SearchQuotes/', search, fnSuccess, fnError);
}

/***   END POST   ***/


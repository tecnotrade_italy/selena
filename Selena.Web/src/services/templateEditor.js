/***   templateEditorServices   ***/

/***   GET   ***/

export function getDto(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/templateeditor/getDto/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

export function getTemplateById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/templateeditor/getTemplateById/' + id, fnSuccess, fnError);
}

export function getTemplateDefaultValue(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/templateeditor/getTemplateDefaultValue', fnSuccess, fnError);
}


export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/templateeditor/select?search' + search, fnSuccess, fnError);
}

export function getTemplateDefaultValueSelect(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/templateeditor/getTemplateDefaultValueSelect', fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/templateeditor', data, fnSuccess, fnError);
}

export function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/templateeditor/clone/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), null, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/templateeditor', data, fnSuccess, fnError);
}

export function updateTable(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/templateeditor/table', data, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/templateeditor/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

/***   END DELETE   ***/

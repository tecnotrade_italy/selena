/***   formBuilderServices   ***/

/***   GET   ***/

export function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/formbuilder/all/' + ln, fnSuccess, fnError);
}

export function getAllRequest(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/formbuilder/all/getAllRequest', fnSuccess, fnError);
}

export function getAllById(ln, id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/formbuilder/getAllById/' + ln + '/' + id, fnSuccess, fnError);
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/formbuilder/select?search=' + search, fnSuccess, fnError);
}


/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/formbuilder', data, fnSuccess, fnError);
}

export function insertFormBuilderField(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/formbuilder/insertFormBuilderField', data, fnSuccess, fnError);
}

export function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/formbuilder/clone/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), null, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(dtoNation, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/formbuilder', dtoNation, fnSuccess, fnError);
}

export function updateFormBuilderField(dtoNation, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/formbuilder/updateFormBuilderField', dtoNation, fnSuccess, fnError);
}



/***   END POST   ***/

/***   DELETE   ***/

export function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/formbuilder/' +  data, fnSuccess, fnError);
}

export function delByContactAndFormId(id, idForm, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/formbuilder/delByContactAndFormId/' +  id + '/' + idForm, fnSuccess, fnError);
}

export function deleteFormBuilderField(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/formbuilder/deleteFormBuilderField/' +  data, fnSuccess, fnError);
}

export function deleteValueFormBuilderField(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/formbuilder/deleteValueFormBuilderField/' +  id, fnSuccess, fnError);
}

export function deleteValueFatherFormBuilderField(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/formbuilder/deleteValueFatherFormBuilderField/' +  id, fnSuccess, fnError);
}
/***   END DELETE   ***/

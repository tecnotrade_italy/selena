/***   itemGrServices   ***/

/***   GET   ***/

export function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/itemGr/all/' + idLanguage, fnSuccess, fnError);
}

export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/itemGr/' + id, fnSuccess, fnError);
}
export function getByCmp(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/itemGr/getByCmp/' + id, fnSuccess, fnError);
}


export function getBarcodeGrForTypology(idTipology, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/itemGr/getBarcodeGrForTypology/' + idTipology, fnSuccess, fnError);
}

export function getBarcodeGrForPlantId(idSelectplant_id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/itemGr/getBarcodeGrForPlantId/' + idSelectplant_id, fnSuccess, fnError);
}

export function insertNoAuth(DtoItemGr, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/itemGr/insertNoAuth', DtoItemGr, fnSuccess, fnError);
}

export function update(DtoItemGr, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/itemGr', DtoItemGr, fnSuccess, fnError);
}


export function updateExploded(DtoItemGr, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/itemGr/updateExploded', DtoItemGr, fnSuccess, fnError);
}


export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/itemGr/select?search' + search, fnSuccess, fnError);
}

export function getSelectListArticle(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/itemGr/getSelectListArticle?search' + search, fnSuccess, fnError);
}


export function getProduct(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/itemGr/getProduct/' + id, fnSuccess, fnError);
}


/*** DELETE ***/
export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/itemGr/' + id, fnSuccess, fnError);
}
export function deleteExploded(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/itemGr/deleteExploded/' + id, fnSuccess, fnError);
}

/*** DELETE ***/

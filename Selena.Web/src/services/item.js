/***   itemServices   ***/

/***   GET   ***/


export function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/item/select',
      dataType: 'json',
      headers: {
        Accept: "application/json",
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function (params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function (result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/select?search=' + search, fnSuccess, fnError);
}
export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/' + id, fnSuccess, fnError);
}

export function getAllRelated(id, idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/item/getAllRelated/' + id + '/' + idLanguage, fnSuccess, fnError);
}

export function getAllRelatedSearch(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/getAllRelatedSearch', data, fnSuccess, fnError);
}

export function getByImgAgg(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getByImgAgg/' + id, fnSuccess, fnError);
}

export function getDocumentInformationById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getDocumentInformationById/' + id, fnSuccess, fnError);
}

export function getByPriceList(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getByPriceList/' + id, fnSuccess, fnError);
}

export function getListItems(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/item/getListItems/' + id, fnSuccess, fnError);
}

export function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/item/all/' + idLanguage, fnSuccess, fnError);
}


export function get(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/item',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function getWithGroupTechnicalSpecification(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/withGroupTechnicalSpecification', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/item/withGroupTechnicalSpecification',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function getGraphic(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getGraphic', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/item/getGraphic',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function count(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/count', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/item/count',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}
export function getImages(idItem, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getImages/' + idItem, fnSuccess, fnError);
}

export function getAllImagesInFolder(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getAllImagesInFolder/' + id, fnSuccess, fnError);
}

export function getAllItemsWithoutCategory(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/getAllItemsWithoutCategory', data, fnSuccess, fnError);
}




/***   END GET   ***/

/***   PUT   ***/

export function insert(DtoItems, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/item', DtoItems, fnSuccess, fnError);
}

export function insertTranslate(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/item/insertTranslate', data, fnSuccess, fnError);
}

export function insertRelated(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/item/insertRelated', data, fnSuccess, fnError);
}
export function insertItemCategories(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/item/insertItemCategories', data, fnSuccess, fnError);
}

export function insertItemImages(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/item/insertItemImages', data, fnSuccess, fnError);
}
/***   END PUT   ***/


/***   POST   ***/

export function update(DtoItems, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item', DtoItems, fnSuccess, fnError);
}

export function addFavoritesItem(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/item/addFavoritesItem', data, fnSuccess, fnError);
}

export function updateImageName(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/updateImageName', data, fnSuccess, fnError);
}

export function updateOrderCategoriesItemTable(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/updateOrderCategoriesItemTable', data, fnSuccess, fnError);
}

export function updateOrderImamePhotos(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/updateOrderImamePhotos', data, fnSuccess, fnError);
}

export function updateItemImages(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/updateItemImages', data, fnSuccess, fnError);
}

export function updateItemCheckAssociation(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/updateItemCheckAssociation', data, fnSuccess, fnError);
}

export function updateOnlineItem(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/updateOnlineItem', data, fnSuccess, fnError);
}

export function updateOnlineItemCheck(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/updateOnlineItemCheck', data, fnSuccess, fnError);
}

export function updateAnnouncementsItemCheck(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/updateAnnouncementsItemCheck', data, fnSuccess, fnError);
}

export function updatePromotionItemCheck(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/item/updatePromotionItemCheck', data, fnSuccess, fnError);
}
/***   END POST   ***/

/*** DELETE ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/item/delete/' + id, fnSuccess, fnError);
}

export function deleteItemCategories(idCategories, idItem, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/item/deleteItemCategories/' + idCategories + '/' + idItem, fnSuccess, fnError);
}

export function deleteItemsPhotos(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/item/deleteItemsPhotos/' + id, fnSuccess, fnError);
}


/*** DELETE ***/

export function getItemsUserShop(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/item/getItemsUserShop', data, fnSuccess, fnError);
}

export function getItemsCategoriesFilter(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/item/getItemsCategoriesFilter', data, fnSuccess, fnError);
}

export function getItemById(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/item/getItemById/' + id, fnSuccess, fnError);
}
export function getByFile(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getByFile/' + id, fnSuccess, fnError);
}

export function getByFileNoId(data, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getByFileNoId', data, fnSuccess, fnError);
}
export function getItemLanguages(idItem, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getItemLanguages/' + idItem, fnSuccess, fnError);
}

export function getByTranslate(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/item/getByTranslate/' + id, fnSuccess, fnError);
}
export function getByTranslateDefault(id, idItem, idLanguage, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/item/getByTranslateDefault/' + id + '/' + idItem + '/' + idLanguage, fnSuccess, fnError);
}
export function getByTranslateDefaultNew(idItem, idLanguage, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/item/getByTranslateDefaultNew/' + idItem + '/' + idLanguage, fnSuccess, fnError);
}
export function updateImages(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/item/updateImages', data, fnSuccess, fnError);
}

export function updateTranslate(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/item/updateTranslate', data, fnSuccess, fnError);
}
export function updateNoAuth(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/item/updateNoAuth', data, fnSuccess, fnError);
}
export function updateImageAggFile(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/item/updateImageAggFile', data, fnSuccess, fnError);
}

export function RemoveFileDb(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/item/RemoveFileDb/' + id, fnSuccess, fnError);
}
export function removeFile(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/item/removeFile/' + id, fnSuccess, fnError);
}

export function getCancelTranslate(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/item/getCancelTranslate/' + id, fnSuccess, fnError);
}

/***   clone ***/
export function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/item/clone/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), null, fnSuccess, fnError);
}

  export function delRelated(idItems, rowIdSelected, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/item/delRelated/' + idItems + '/' + rowIdSelected, fnSuccess, fnError);
}
/***   END  deleterelated ***/

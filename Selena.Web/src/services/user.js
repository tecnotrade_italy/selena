/***   userServices   ***/

/***   GET   ***/

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/select?search' + search, fnSuccess, fnError);
}
export function getSelectUserPages(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/selectUserPages?search' + search, fnSuccess, fnError);
}

export function getByIdResult(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/getByIdResult/' + id, fnSuccess, fnError);
}

export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/' + id, fnSuccess, fnError);
}
export function availableCheck(id, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/user/availableCheck/' + id, fnSuccess, fnError);
}

export function getUserData(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/user/getUserData/' + id, fnSuccess, fnError);
}
export function getDetailUsers(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/user/getDetailUsers/' + id, fnSuccess, fnError);
}
export function getDifferentDestination(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/user/getDifferentDestination/' + id, fnSuccess, fnError);
}

export function getByUsername(username, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/user/getByUsername/' + username, fnSuccess, fnError);
}

export function getGraphic(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/getGraphic', fnSuccess, fnError);
}

export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/all/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

export function getAllUser(idLanguage, fnSuccess, fnError) {
  if(idLanguage === undefined || idLanguage==''){
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/user/getAllUser/' + idLanguage, fnSuccess, fnError);
}

export function getAllUserSales(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/getAllUserSales/' + id, fnSuccess, fnError);
}
export function getAllDetailValueSalesUser(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/getAllDetailValueSalesUser/' + id, fnSuccess, fnError);
}
export function getAllUserHistorical(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/getAllUserHistorical/' + id, fnSuccess, fnError);
}
export function getAllDetailTimeLine(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/getAllDetailTimeLine/' + id, fnSuccess, fnError);
}

export function getAllAddress(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/user/getAllAddress/' + id, fnSuccess, fnError);
}
export function getbyIdAddress(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/user/getbyIdAddress/' + id, fnSuccess, fnError);
}

export function count(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/user/count', fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/user', data, fnSuccess, fnError);
}

export function insertNoAuth(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/user/insertNoAuth', data, fnSuccess, fnError);
}

export function insertUser(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/user/insertUser', data, fnSuccess, fnError);
}


export function insertComment(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/user/insertComment', data, fnSuccess, fnError);
}

export function insertUserAddress(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/user/insertUserAddress', data, fnSuccess, fnError);
}

export function insertSalesUser(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/user/insertSalesUser', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function changePassword(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/user/changePassword', data, fnSuccess, fnError);
}
export function updateCheckOnlineAddress(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/user/updateCheckOnlineAddress', data, fnSuccess, fnError);
}
export function updateCheckDetailDefaultAddress(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/user/updateCheckDetailDefaultAddress', data, fnSuccess, fnError);
}

export function changePasswordRescued(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/changePasswordRescued', data, fnSuccess, fnError);
}

export function updateCheckOnlineReferentDetail(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/updateCheckOnlineReferentDetail', data, fnSuccess, fnError);
}
export function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/user', data, fnSuccess, fnError);
}

export function updateNoAuth(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/updateNoAuth', data, fnSuccess, fnError);
}
export function updateUserAddress(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/updateUserAddress', data, fnSuccess, fnError);
}
export function updateSalesUser(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/updateSalesUser', data, fnSuccess, fnError);
}

export function updateorcreateDestinationBusinessName(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/updateorcreateDestinationBusinessName', data, fnSuccess, fnError);
}

export function updateorcreateUsersAdresses(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/updateorcreateUsersAdresses', data, fnSuccess, fnError);
}

export function updateUser(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/updateUser', data, fnSuccess, fnError);
}
export function ResetConfermationEmail(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/ResetConfermationEmail', data, fnSuccess, fnError);
}

export function updateVatNumber(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/updateVatNumber', data, fnSuccess, fnError);
}

export function rescuePassword(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/user/rescuePassword', data, fnSuccess, fnError);
}

export function checkExistUsername(username, fnSuccess, fnError) {
  var data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    username: username
  };

  serviceHelpers.postAutenticate('/api/v1/user/checkExistUsername', data, fnSuccess, fnError);
}

export function checkGroupUser(username, fnSuccess, fnError) {
  var data = {
    username: username
  };

  serviceHelpers.postAutenticate('/api/v1/user/checkGroupUser', data, fnSuccess, fnError);
}

export function checkExistUsernameNotForThisPage(id, username, fnSuccess, fnError) {
  var data = {
    id: id,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    username: username
  };

  serviceHelpers.postAutenticate('/api/v1/user/checkExistUsername', data, fnSuccess, fnError);
}

export function checkExistEmail(email, fnSuccess, fnError) {
  var data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    email: email
  };

  serviceHelpers.postAutenticate('/api/v1/user/checkExistEmail', data, fnSuccess, fnError);
}

export function checkExistEmailNotForThisPage(id, email, fnSuccess, fnError) {
  var data = {
    id: id,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    email: email
  };

  serviceHelpers.postAutenticate('/api/v1/user/checkExistEmail', data, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/user/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

/***   END DELETE   ***/


export function deleteAddress(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/user/deleteAddress/' + id, fnSuccess, fnError);
}

export function delet(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/user/' + id, fnSuccess, fnError);
}

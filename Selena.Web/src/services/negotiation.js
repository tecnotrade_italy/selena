/***   negotiationServices   ***/

/***   GET   ***/

export function getByUser(userId, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/negotiation/getByUser/' + userId, fnSuccess, fnError);
}

export function getDetailById(id, idUser, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/negotiation/getDetailById/' + id + "/" + idUser, fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/negotiation', data, fnSuccess, fnError);
}

export function insertDetail(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/negotiation/insertDetail', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

/***   END POST   ***/

/*** DELETE ***/

export function deleteRow(id, fnSuccess, fnError) {
  serviceHelpers.del('/api/v1/negotiation/deleteRow/' + id, fnSuccess, fnError);
}

/*** DELETE ***/
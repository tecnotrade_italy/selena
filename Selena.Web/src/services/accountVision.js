/***   accountVisionServices   ***/

/***   GET   ***/

export function getAllAccountVision(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/accountvision/getAllAccountVision/' + idLanguage, fnSuccess, fnError);
}
export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/accountvision/getById/' + id, fnSuccess, fnError);
}
export function getLastId(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/accountvision/getLastId/', fnSuccess, fnError);
}
export function getLastIdContoVisione(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/accountvision/getLastIdContoVisione/', fnSuccess, fnError);
}

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/accountvision', data, fnSuccess, fnError);
}

export function SearchAccountVision(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/accountvision/SearchAccountVision', search, fnSuccess, fnError);
}

export function SearchAccountVisionOpen(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/accountvision/SearchAccountVisionOpen', search, fnSuccess, fnError);
}

export function SearchAccountVisionClose(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/accountvision/SearchAccountVisionClose', search, fnSuccess, fnError);
}

export function update(DtoAccountVision, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/accountvision/update', DtoAccountVision, fnSuccess, fnError);
}
/*** DELETE ***/
export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/accountvision/' + id, fnSuccess, fnError);
}
/*** DELETE ***/


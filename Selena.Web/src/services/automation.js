/***   automationServices   ***/

/***   GET   ***/

export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/automation/getAll', fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/automation', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/automation', data, fnSuccess, fnError);
}

/***   END POST   ***/

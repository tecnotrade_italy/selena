/***   interventionCustomerServices   ***/

export function getAllInterventionCustomer(idUser, fnSuccess, fnError) {
  if (idUser === undefined || idUser == '') {
    idUser = storageData.idUser();
  }
    serviceHelpers.getAutenticate('/api/v1/interventioncustomer/getAllInterventionCustomer/' + idUser, fnSuccess, fnError);
}

export function getByViewQuotesCustomer(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/interventioncustomer/getByViewQuotesCustomer/' + id, fnSuccess, fnError);
}

export function updateInterventionUpdateViewCustomerQuotes(DtoQuotes, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/interventioncustomer/updateInterventionUpdateViewCustomerQuotes', DtoQuotes, fnSuccess, fnError);
}

export function updateModuleValidationGuarantee(DtoModuleValidationGuarantee, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/interventioncustomer/updateModuleValidationGuarantee', DtoModuleValidationGuarantee, fnSuccess, fnError);
}
export function updateSendFaultModule(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/interventioncustomer/updateSendFaultModule', data, fnSuccess, fnError);
}
export function insertMessageCustomerQuotes(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/interventioncustomer/insertMessageCustomerQuotes', data, fnSuccess, fnError);
}

export function getByViewModuleCustomer(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/interventioncustomer/getByViewModuleCustomer/' + id, fnSuccess, fnError);
}

export function getByViewModuleValidationGuarantee(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/interventioncustomer/getByViewModuleValidationGuarantee/' + id, fnSuccess, fnError);
}

export function getAllInterventionCustomerSearch(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/interventioncustomer/getAllInterventionCustomerSearch', search, fnSuccess, fnError);
}
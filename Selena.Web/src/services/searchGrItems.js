/***   searchItemsGrServices   ***/

/***   POST   ***/

export function searchImplementedItemsGr(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchitemsGr/searchImplementedItemsGr', search, fnSuccess, fnError);
}
/***   END POST   ***/

export function getResultDetail(id, fnSuccess, fnError) {
    serviceHelpers.getAutenticate('/api/v1/searchitemsGr/getResultDetail/' + id, fnSuccess, fnError);
}

export function searchImplementedItemsGlobal(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchitemsGr/searchImplementedItemsGlobal', search, fnSuccess, fnError);
}


/***   searchSchoolMusicServices   ***/

/***   POST   ***/
export function searchImplemented(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchschoolmusic/searchImplemented', search, fnSuccess, fnError);
}
/***   END POST   ***/

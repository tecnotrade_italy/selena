/***   packagingTypeSixtenServices   ***/

/***   GET   ***/

export function getSelect(idErp, routeParams, fnSuccess, fnError) {
  var data = {
    idErp: idErp,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(routeParams)
  };

  serviceHelpers.getAutenticateWithData('/api/v1/packagingTypeSixten/select', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/packagingTypeSixten/select',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: "application/x-www-form-urlencoded; charset=UTF-8",
  //   data: data
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/***   END GET   ***/

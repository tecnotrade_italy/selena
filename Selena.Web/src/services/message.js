/***   messageServices   ***/

/***   GET   ***/

export function getAll(idLanguage, fnSuccess, fnError) {
    if(idLanguage === undefined || idLanguage==''){
      idLanguage = storageData.sIdLanguage();
    }
    serviceHelpers.getAutenticate('/api/v1/message/all/' + idLanguage, fnSuccess, fnError);
  }
  
  export function getByCode(code, fnSuccess, fnError) {
    serviceHelpers.get('/api/v1/message/getByCode/' + code, fnSuccess, fnError);
  }

  export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/message/select?search=' + search, fnSuccess, fnError);
  }
  /***   END GET   ***/
  
  /***   PUT   ***/
  
  export function insert(dtoMessage, fnSuccess, fnError) {
    serviceHelpers.putAutenticate('/api/v1/message', dtoMessage, fnSuccess, fnError);
  }
  
  /***   END PUT   ***/
  
  /***   POST   ***/
  
  export function update(dtoMessage, fnSuccess, fnError) {
    serviceHelpers.postAutenticate('/api/v1/message', dtoMessage, fnSuccess, fnError);
  }
  
  /***   END POST   ***/
  
  /***   DELETE   ***/
  
  export function del(id, fnSuccess, fnError) {
    serviceHelpers.delAutenticate('/api/v1/message/' +  id, fnSuccess, fnError);
  }
  
  /***   END DELETE   ***/
  
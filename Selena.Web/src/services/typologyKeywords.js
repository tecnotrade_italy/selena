/***   typologyKeywordsServices   ***/

/***   GET   ***/

export function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/typologyKeywords/all/' + ln, fnSuccess, fnError);
}


export function getAllSelect(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/typologyKeywords/getAllSelect?search=' + search, fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/typologyKeywords', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(dtoNation, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/typologyKeywords', dtoNation, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/typologyKeywords/' +  data, fnSuccess, fnError);
}

/***   END DELETE   ***/

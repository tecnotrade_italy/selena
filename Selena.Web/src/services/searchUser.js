/***   searchUserServices   ***/

/***   POST   ***/

export function searchImplemented(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchuser/searchImplemented', search, fnSuccess, fnError);
}

export function SearchViewPrivates(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchuser/SearchViewPrivates', search, fnSuccess, fnError);
}
export function SearchViewTrueSubscriber(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchuser/SearchViewTrueSubscriber', search, fnSuccess, fnError);
}
export function SearchViewFalseSubscriber(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchuser/SearchViewFalseSubscriber', search, fnSuccess, fnError);
}
/***   END POST   ***/


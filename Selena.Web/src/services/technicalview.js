/***   technicalviewServices   ***/

/***   GET   ***/

/*----- Gestione Materiali Tecnico -----------*/

export function getAllProcessingTechnical(idUser, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/technicalviewgr/getAllProcessingTechnical/' + idUser, fnSuccess, fnError);
}

export function getAllQuotesTechnical(idUser, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/technicalviewgr/getAllQuotesTechnical/' + idUser, fnSuccess, fnError);
}

/***   formBuilderTypeServices   ***/

/***   GET   ***/

export function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/formbuildertype/all/' + ln, fnSuccess, fnError);
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/formbuildertype/select?search=' + search, fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/formbuildertype', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(dtoNation, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/formbuildertype', dtoNation, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/formbuildertype/' +  data, fnSuccess, fnError);
}

/***   END DELETE   ***/

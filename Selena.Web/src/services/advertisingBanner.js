
  export function getById(id, fnSuccess, fnError) {
    serviceHelpers.getAutenticate('/api/v1/AdvertisingBanner/' + id, fnSuccess, fnError);
  }
  

  export function getAll(fnSuccess, fnError) {
    serviceHelpers.getAutenticate('/api/v1/AdvertisingBanner/all/' , fnSuccess, fnError);
  }
  

  /***   END GET   */
  
  /***   PUT   ***/
  
  export function insert(dtoAdvertisingBanner, fnSuccess, fnError) {
    serviceHelpers.putAutenticate('/api/v1/AdvertisingBanner', dtoAdvertisingBanner, fnSuccess, fnError);
  }
  
  /***   END PUT   ***/
  
  /***   POST   ***/
  
  export function update(dtoAdvertisingBanner, fnSuccess, fnError) {
    serviceHelpers.postAutenticate('/api/v1/AdvertisingBanner', dtoAdvertisingBanner, fnSuccess, fnError);
  }
  
  /***   END POST   ***/
  
  /***   DELETE   ***/
  
  export function del(id, fnSuccess, fnError) {
    serviceHelpers.delAutenticate('/api/v1/AdvertisingBanner/' + id, fnSuccess, fnError);
  }
  
  /***   END DELETE   ***/
  
  
  
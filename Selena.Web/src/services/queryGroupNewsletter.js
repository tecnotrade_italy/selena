/***   QueryGroupNewsletterServices   ***/

/***   GET   ***/

export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/querygroupnewsletter/all', fnSuccess, fnError);
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/querygroupnewsletter/select?search=' + search, fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/querygroupnewsletter', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/querygroupnewsletter', data, fnSuccess, fnError);
}
export function executequeryresult(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/querygroupnewsletter/executequeryresult', data, fnSuccess, fnError);
}
/***   END POST   ***/

/***   DELETE   ***/
export function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/querygroupnewsletter/' +  data, fnSuccess, fnError);
}

export function deleteRow(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/querygroupnewsletter/deleteRow/' + id, fnSuccess, fnError);
}

/***   END DELETE   ***/

/***   packageServices   ***/

/***   GET   ***/

export function getMaxByErp(idErp, routeParameter, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/package/getMaxByErp/' + idErp + '/' + menuHelpers.getIdFunctionByAdminUrl(routeParameter), fnSuccess, fnError);
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/package/select?search' + search, fnSuccess, fnError);
}

export function selectByErp(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/pageCategory/selectByErp?search' + search, fnSuccess, fnError);
}

/***   END GET   ***/

/***   INSERT   ***/

export function createByErp(idErp, description, routeParameter, fnSuccess, fnError) {
  var data = {
    idErp: idErp,
    description: description,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(routeParameter)
  };

  serviceHelpers.putAutenticate('/api/v1/package/createByErp', data, fnSuccess, fnError);
}

/***   END INSERT   ***/

/***   pageCategoryServices   ***/

/***   GET   ***/

export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/pageCategory/' + id, fnSuccess, fnError);
}

export function getAllByPageId(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/pageCategory/getAllByPageId/' + id, fnSuccess, fnError);
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/pageCategory/select?search' + search, fnSuccess, fnError);
}


export function getAllListCategories(idLanguage, idPages, fnSuccess, fnError) {
  if(idLanguage === undefined || idLanguage==''){
    idLanguage = storageData.sIdLanguage();
    }
  serviceHelpers.getAutenticate('/api/v1/pageCategory/getAllListCategories/' + idLanguage + '/' + idPages, fnSuccess, fnError);
}

/***   END GET   ***/

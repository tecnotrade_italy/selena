/***   appointmentsServices   ***/

/***   GET   ***/
export function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/appointments/select',
      dataType: 'json',
      headers: {
        Accept: "application/json",
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function (params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function (result) {
        return {
          results: result.data
        };
      }
    },
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/appointments/select?search=' + search, fnSuccess, fnError);
}


export function getAll(idLanguage, fnSuccess, fnError) {
  if(idLanguage === undefined || idLanguage==''){
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/appointments/all/' + idLanguage, fnSuccess, fnError);
}
/***   END GET   ***/

export function getAllResultCalendar(idLanguage, fnSuccess, fnError) {
  if(idLanguage === undefined || idLanguage==''){
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/appointments/getAllResultCalendar/' + idLanguage, fnSuccess, fnError);
}
/***   END GET   ***/

export function getAllAppointmentsSearch(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/appointments/getAllAppointmentsSearch', data, fnSuccess, fnError);
}


export function getByIdEvent(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/appointments/getByIdEvent/' + id, fnSuccess, fnError);
}



export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Cart/' + id, fnSuccess, fnError);
}

export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Cart/getAll/', fnSuccess, fnError);
}


export function getTotalAmount(idCart, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Cart/getTotalAmount/' + idCart, fnSuccess, fnError);
}

export function getNrProductRows(idCart, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/Cart/getNrProductRows/' + idCart, fnSuccess, fnError);
}

export function insert(DtoCartDetail, fnSuccess, fnError) {
  
  serviceHelpers.putAutenticate('/api/v1/Cart', DtoCartDetail, fnSuccess, fnError);
}


export function update(DtoCategories, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/Cart', DtoCartDetail, fnSuccess, fnError);
}


export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/Cart/' + id, fnSuccess, fnError);
}

  

/***   itemTechnicalSpecificationServices   ***/

/***   GET   ***/

export function getConfiguration(idItem, idLanguage, fnSuccess, fnError) {
  var url = '/api/v1/itemtechnicalspecification/getConfiguration/' + idItem + '/' + menuHelpers.getIdFunctionByAdminUrl();

  if (functionHelpers.isValued(idLanguage)) {
    url = url + '/' + idLanguage;
  }

  serviceHelpers.getAutenticate(url, fnSuccess, fnError);
}

export function getVersionConfiguration(idItem, version, idLanguage, fnSuccess, fnError) {
  var url = '/api/v1/itemtechnicalspecification/getVersionConfiguration/' + idItem + '/' + version + '/' + menuHelpers.getIdFunctionByAdminUrl();

  if (functionHelpers.isValued(idLanguage)) {
    url = url + '/' + idLanguage;
  }

  serviceHelpers.getAutenticate(url, fnSuccess, fnError);
}

export function get(idItem, idLanguage, version, fnSuccess, fnError) {
  var url = '/api/v1/itemtechnicalspecification/' + idItem + '/' + menuHelpers.getIdFunctionByAdminUrl();

  if (functionHelpers.isValued(idLanguage)) {
    url = url + '/' + idLanguage;
  }
  if (functionHelpers.isValued(version) && version != 'Last') {
    if (!functionHelpers.isValued(idLanguage)) {
      url = url + '/' + storageData.sIdLanguage();
    }
    url = url + '/' + version;
  }

  serviceHelpers.getAutenticate(url, fnSuccess, fnError);
}

export function getVersion(idItem, idLanguage, fnSuccess, fnError) {
  var url = '/api/v1/itemtechnicalspecification/getVersion/' + idItem + '/' + menuHelpers.getIdFunctionByAdminUrl();

  if (functionHelpers.isValued(idLanguage)) {
    url = url + '/' + idLanguage;
  }

  serviceHelpers.getAutenticate(url, fnSuccess, fnError);
}

export function getCompletedStatByLanguage(idLanguage, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/itemtechnicalspecification/getCompletedStatByLanguage/' + idLanguage, fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/itemtechnicalspecification', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function complete(id, complete, fnSuccess, fnError) {
  var dtoItem = {
    idItem: id,
    complete: complete
  };

  serviceHelpers.postAutenticate('/api/v1/itemtechnicalspecification/complete', dtoItem, fnSuccess, fnError);
}

/***   END POST   ***/

/***   roleServices   ***/

/*** GET ***/

export function getByUser(idUser, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/role/getByUser/' + idUser + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/role/getByUser/' + idUser + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     "Accept": "application/json",
  //     "Authorization": storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function getAllByRole(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/role/getAllByRole', data, fnSuccess, fnError);
}
export function getUserName(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/role/getUserName', data, fnSuccess, fnError);
}

export function insertRole(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/role/insertRole', data, fnSuccess, fnError);
}

export function delRoleUser(idRoleUser, user_id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/role/delRoleUser/' + idRoleUser + '/' + user_id, fnSuccess, fnError);
}

/*** END GET ***/

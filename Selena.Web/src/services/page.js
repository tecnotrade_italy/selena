/***   pageServices   ***/

/*** GET ***/

export function getMenuList(id, pageType, fnSuccess, fnError) {
  if (id == '' || id === undefined) {
    id = storageData.sIdLanguage();
  }
  if (pageType == '' || pageType === undefined) {
    pageType = "Page"
  }

  serviceHelpers.getAutenticate('/api/v1/page/getMenuList/' + id + '/' + pageType, fnSuccess, fnError);
}

export function getSetting(id, sUserId, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/page/getSetting/' + id + '/' + sUserId, fnSuccess, fnError);
}

export function getByUserDescription(sUserId, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/page/getByUserDescription/' + sUserId, fnSuccess, fnError);
}


export function getByFile(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/page/getByFile', data, fnSuccess, fnError);
}

export function getHome(fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/page/getHome/' + storageData.sIdLanguage(), fnSuccess, fnError);
}

export function getLastActivity(fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/page/getLastActivity/' + storageData.sIdLanguage(), fnSuccess, fnError);
}

/*** END GET ***/

/*** PUT ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/page', data, fnSuccess, fnError);
}

export function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/page/clone/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), null, fnSuccess, fnError);
}


export function insertImage(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/page/insertImage', data, fnSuccess, fnError);
}

export function insertItemCategoriesPages(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/page/insertItemCategoriesPages', data, fnSuccess, fnError);
}

export function deleteItemCategoriesPages(idCategories, idPages, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/page/deleteItemCategoriesPages/' + idCategories + '/' + idPages, fnSuccess, fnError);
}
/*** END PUT ***/

/*** POST ***/

export function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/page', data, fnSuccess, fnError);
}

export function postMenuList(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/page/menuList', data, fnSuccess, fnError);
}

export function checkUploadFileEditor(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/file/checkUploadFileEditor', data, fnSuccess, fnError);
}

export function postOnlinePage(valueOnline, id, fnSuccess, fnError) {
  var data = {
    'online': valueOnline,
    'id': id
  };

  serviceHelpers.postAutenticate('/api/v1/page/onlinePage', data, fnSuccess, fnError);
}

export function getRender(url, fnSuccess, fnError) {
  var data = {
    url: url,
    userId: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    cartId: storageData.sCartId()
  };

  serviceHelpers.post('/api/v1/page/getRender', data, fnSuccess, fnError);
}

export function checkExistUrl(url, fnSuccess, fnError) {
  var data = {
    url: url
  };

  serviceHelpers.postAutenticate('/api/v1/page/checkExistUrl', data, fnSuccess, fnError);
}

export function checkExistUrlNotForThisPage(url, idPage, fnSuccess, fnError) {
  var data = {
    url: url,
    idPage: idPage
  };

  serviceHelpers.postAutenticate('/api/v1/page/checkExistUrl', data, fnSuccess, fnError);
}

export function checkExistUrlNotForThisNewsletter(url, idNewsletter, fnSuccess, fnError) {
  var data = {
    url: url,
    idNewsletter: idNewsletter
  };

  serviceHelpers.postAutenticate('/api/v1/page/checkExistUrl', data, fnSuccess, fnError);
}

export function convertImageToWebp(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/page/convertImageToWebp', data, fnSuccess, fnError);
}

/*** END POST ***/

/*** DELETE ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/page/' + id, fnSuccess, fnError);
}

export function deleteImage(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/page/deleteImage', data, fnSuccess, fnError);
}


/*** END DELETE ***/

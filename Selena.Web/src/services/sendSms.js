/***   sendSmsServices   ***/
export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/sendSms/all', fnSuccess, fnError);
}

/***   GET   ***/
export function insertSms(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/sendSms/insertSms', data, fnSuccess, fnError);
}

export function getnumbertelephone(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/sendSms/getnumbertelephone/' + id, fnSuccess, fnError);
}

export function getListContactByGroup(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/sendSms/getListContactByGroup/' + id, fnSuccess, fnError);
}
export function getListContactByUser(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/sendSms/getListContactByUser/' + id, fnSuccess, fnError);
}
export function getListTelephoneQueryGroup(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/sendSms/getListTelephoneQueryGroup/' + id, fnSuccess, fnError);
}

/***   END GET   ***/

/***   DELETE   ***/
export function del(id, fnSuccess, fnError) {
    serviceHelpers.delAutenticate('/api/v1/sendSms/' +  id, fnSuccess, fnError);
}
/***   END DELETE   ***/

/***   searchDataSheetServices   ***/


/***   POST   ***/
export function search(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchdatasheet/searchDataSheet', search, fnSuccess, fnError);
}

export function searchPaginatorDataSheet(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchdatasheet/searchPaginatorDataSheet', search, fnSuccess, fnError);
}
/***   END POST   ***/


/***   templateEditorTypeServices   ***/

/*** GET ***/

export function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/templateeditortype/select',
      dataType: 'json',
      headers: {
        Accept: "application/json",
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function (params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function (result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/templateeditortype/' + id, fnSuccess, fnError);
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/templateeditortype/select?idFunctionality=' + menuHelpers.getIdFunctionByAdminUrl() + '&search' + search, fnSuccess, fnError);
}

/*** END GET ***/

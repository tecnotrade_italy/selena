/***   peopleServices   ***/

/***   GET   ***/

export function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/people/select',
      dataType: 'json',
      headers: {
        Accept: "application/json",
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function (params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function (result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/people/select?search=' + search, fnSuccess, fnError);
}


export function getSelectExternalReferent(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/people/getSelectExternalReferent?search=' + search, fnSuccess, fnError);
}

export function getPeopleUserWeb(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/people/getPeopleUserWeb/' + id, fnSuccess, fnError);
}

export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/people/' + id, fnSuccess, fnError);
}

export function getAll(idLanguage, fnSuccess, fnError) {
  if(idLanguage === undefined || idLanguage==''){
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/people/all/' + idLanguage, fnSuccess, fnError);
}


export function get(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/people', fnSuccess, fnError);

}

export function getAllReferent(id,fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/people/getAllReferent/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

export function getPeopleUser(id,fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/people/getPeopleUser/' + id, fnSuccess, fnError);
}

export function getByIdReferent(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/people/getByIdReferent/' + id, fnSuccess, fnError);
}

/***   END GET   ***/
/***   PUT   ***/

export function insert(DtoPeople, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/people', DtoPeople, fnSuccess, fnError);
}

export function insertReferentWeb(DtoPeople, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/people/insertReferentWeb', DtoPeople, fnSuccess, fnError);
}
export function insertNoAuth(DtoPeople, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/people/insertNoAuth', DtoPeople, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(DtoPeople, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/people', DtoPeople, fnSuccess, fnError);
}

export function updateReferentWeb(DtoPeople, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/people/updateReferentWeb', DtoPeople, fnSuccess, fnError);
}

export function updateReferent(DtoPeople, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/people/updateReferent', DtoPeople, fnSuccess, fnError);
}
/***   END POST   ***/

/*** DELETE ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/people/' + id, fnSuccess, fnError);
}

/*** DELETE ***/


/***   clone ***/
export function clone(id, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/people/clone/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), null, fnSuccess, fnError);
}
/***   END  clone ***/

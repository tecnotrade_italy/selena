
  
  export function getById(id, fnSuccess, fnError) {
    serviceHelpers.getAutenticate('/api/v1/CommunityImage/' + id, fnSuccess, fnError);
  }
  
  export function getAll(fnSuccess, fnError) {
    serviceHelpers.getAutenticate('/api/v1/CommunityImage/all/' , fnSuccess, fnError);
  }
  
  export function getGallery(type, fnSuccess, fnError) {
    serviceHelpers.getAutenticate('/api/v1/CommunityImage/getGallery/' + type , fnSuccess, fnError);
  }

  /***   END GET   */
  
  /***   PUT   ***/
  
  export function insert(dtoCommunityImage, fnSuccess, fnError) {
    serviceHelpers.putAutenticate('/api/v1/CommunityImage', dtoCommunityImage, fnSuccess, fnError);
  }
  export function insertNewImages(dtoCommunityImage, fnSuccess, fnError) {
    serviceHelpers.putAutenticate('/api/v1/CommunityImage/insertNewImages', dtoCommunityImage, fnSuccess, fnError);
  }

  export function insertToWebP(dtoCommunityImage, fnSuccess, fnError) {
    serviceHelpers.putAutenticate('/api/v1/CommunityImage/insertToWebP', dtoCommunityImage, fnSuccess, fnError);
  }
  
    /***   END PUT   ***/
  
  /***   POST   ***/
  
  export function update(dtoCommunityImage, fnSuccess, fnError) {
    serviceHelpers.postAutenticate('/api/v1/CommunityImage', dtoCommunityImage, fnSuccess, fnError);
  }
  
  export function updateOnlineCheckImages(dtoCommunityImage, fnSuccess, fnError) {
    serviceHelpers.postAutenticate('/api/v1/CommunityImage/updateOnlineCheckImages', dtoCommunityImage, fnSuccess, fnError);
  }
  
  /***   END POST   ***/
  
  /***   DELETE   ***/
  
  export function del(id, fnSuccess, fnError) {
    serviceHelpers.delAutenticate('/api/v1/CommunityImage/' + id, fnSuccess, fnError);
  }
  
  /***   END DELETE   ***/
  
  
  
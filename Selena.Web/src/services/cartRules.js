/***   voucherServices   ***/

/***   GET   ***/

export function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/cartRules/all', fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(dtoVoucher, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/cartRules', dtoVoucher, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(dtoVoucher, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/cartRules', dtoVoucher, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/cartRules/' +  id, fnSuccess, fnError);
}

/***   END DELETE   ***/

export function getGroupSelectedAndNot(id, fnSuccess, fnError) {
serviceHelpers.getAutenticate('/api/v1/cartRules/getGroupSelectedAndNot/' + id, fnSuccess, fnError);
}

export function getCarriersSelectedAndNot(id, fnSuccess, fnError) {
serviceHelpers.getAutenticate('/api/v1/cartRules/getCarriersSelectedAndNot/' + id, fnSuccess, fnError);
}

export function getUserSelectedAndNot(id, fnSuccess, fnError) {
serviceHelpers.getAutenticate('/api/v1/cartRules/getUserSelectedAndNot/' + id, fnSuccess, fnError);
}
export function getItemsSelectedAndNot(id, fnSuccess, fnError) {
serviceHelpers.getAutenticate('/api/v1/cartRules/getItemsSelectedAndNot/' + id, fnSuccess, fnError);
}

export function getCategoriesSelectedAndNot(id, fnSuccess, fnError) {
serviceHelpers.getAutenticate('/api/v1/cartRules/getCategoriesSelectedAndNot/' + id, fnSuccess, fnError);
}

export function getProducersSelectedAndNot(id, fnSuccess, fnError) {
serviceHelpers.getAutenticate('/api/v1/cartRules/getProducersSelectedAndNot/' + id, fnSuccess, fnError);
}


export function getSuppliersSelectedAndNot(id, fnSuccess, fnError) {
serviceHelpers.getAutenticate('/api/v1/cartRules/getSuppliersSelectedAndNot/' + id, fnSuccess, fnError);
}

export function getSpecificProductsSelectedAndNot(id, fnSuccess, fnError) {
serviceHelpers.getAutenticate('/api/v1/cartRules/getSpecificProductsSelectedAndNot/' + id, fnSuccess, fnError);
}

export function insertCarriersCartRole(data, fnSuccess, fnError) {
serviceHelpers.putAutenticate('/api/v1/cartRules/insertCarriersCartRole', data, fnSuccess, fnError);
}
export function insertCategoriesCartRoleCategories(data, fnSuccess, fnError) {
serviceHelpers.putAutenticate('/api/v1/cartRules/insertCategoriesCartRoleCategories', data, fnSuccess, fnError);
}

export function insertProducersCartRoleProducers(data, fnSuccess, fnError) {
serviceHelpers.putAutenticate('/api/v1/cartRules/insertProducersCartRoleProducers', data, fnSuccess, fnError);
}

export function insertSuppliersCartRoleSuppliers(data, fnSuccess, fnError) {
serviceHelpers.putAutenticate('/api/v1/cartRules/insertSuppliersCartRoleSuppliers', data, fnSuccess, fnError);
}

export function insertSpecificItemsCartRole(data, fnSuccess, fnError) {
serviceHelpers.putAutenticate('/api/v1/cartRules/insertSpecificItemsCartRole', data, fnSuccess, fnError);
}

export function updateSpecificProductsValue(data, fnSuccess, fnError) {
serviceHelpers.putAutenticate('/api/v1/cartRules/updateSpecificProductsValue', data, fnSuccess, fnError);
}

export function insertUserCartRoleUser(data, fnSuccess, fnError) {
serviceHelpers.putAutenticate('/api/v1/cartRules/insertUserCartRoleUser', data, fnSuccess, fnError);
}

export function insertCartRoleGroup(data, fnSuccess, fnError) {
serviceHelpers.putAutenticate('/api/v1/cartRules/insertCartRoleGroup', data, fnSuccess, fnError);
}

export function insertItemCartRole(data, fnSuccess, fnError) {
serviceHelpers.putAutenticate('/api/v1/cartRules/insertItemCartRole', data, fnSuccess, fnError);
}

export function getUserCartRolesSearch(data, fnSuccess, fnError) {
serviceHelpers.postAutenticate('/api/v1/cartRules/getUserCartRolesSearch', data, fnSuccess, fnError);
}
export function getItemsCartRolesSearch(data, fnSuccess, fnError) {
serviceHelpers.postAutenticate('/api/v1/cartRules/getItemsCartRolesSearch', data, fnSuccess, fnError);
}

export function getItemsSearchDescriptionCartRuleItems(data, fnSuccess, fnError) {
serviceHelpers.postAutenticate('/api/v1/cartRules/getItemsSearchDescriptionCartRuleItems', data, fnSuccess, fnError);
}

export function getCarriersCartRolesSearch(data, fnSuccess, fnError) {
serviceHelpers.postAutenticate('/api/v1/cartRules/getCarriersCartRolesSearch', data, fnSuccess, fnError);
}

export function getUserSearchBusinessNameCartRuleUser(data, fnSuccess, fnError) {
serviceHelpers.postAutenticate('/api/v1/cartRules/getUserSearchBusinessNameCartRuleUser', data, fnSuccess, fnError);
}
export function getGroupCartRolesSearch(data, fnSuccess, fnError) {
serviceHelpers.postAutenticate('/api/v1/cartRules/getGroupCartRolesSearch', data, fnSuccess, fnError);
}
export function getCategoriesCartRolesSearch(data, fnSuccess, fnError) {
serviceHelpers.postAutenticate('/api/v1/cartRules/getCategoriesCartRolesSearch', data, fnSuccess, fnError);
}

export function getProducersCartRolesSearch(data, fnSuccess, fnError) {
serviceHelpers.postAutenticate('/api/v1/cartRules/getProducersCartRolesSearch', data, fnSuccess, fnError);
}

export function getSuppliersCartRolesSearch(data, fnSuccess, fnError) {
serviceHelpers.postAutenticate('/api/v1/cartRules/getSuppliersCartRolesSearch', data, fnSuccess, fnError);
}

export function getSpecificProductCartRolesSearch(data, fnSuccess, fnError) {
serviceHelpers.postAutenticate('/api/v1/cartRules/getSpecificProductCartRolesSearch', data, fnSuccess, fnError);
}

export function getSearchSpecificProductsSelectedCartRule(data, fnSuccess, fnError) {
serviceHelpers.postAutenticate('/api/v1/cartRules/getSearchSpecificProductsSelectedCartRule', data, fnSuccess, fnError);
}

export function getSearchDescriptionCartRuleCategories(data, fnSuccess, fnError) {
serviceHelpers.postAutenticate('/api/v1/cartRules/getSearchDescriptionCartRuleCategories', data, fnSuccess, fnError);
}

export function getSearchDescriptionCartRuleProducers(data, fnSuccess, fnError) {
serviceHelpers.postAutenticate('/api/v1/cartRules/getSearchDescriptionCartRuleProducers', data, fnSuccess, fnError);
}

export function getSearchDescriptionCartRuleSuppliers(data, fnSuccess, fnError) {
serviceHelpers.postAutenticate('/api/v1/cartRules/getSearchDescriptionCartRuleSuppliers', data, fnSuccess, fnError);
}



export function getCarrierSearchNameCartRuleCarriers(data, fnSuccess, fnError) {
serviceHelpers.postAutenticate('/api/v1/cartRules/getCarrierSearchNameCartRuleCarriers', data, fnSuccess, fnError);
}
export function getGroupSearchNameCartRuleGroup(data, fnSuccess, fnError) {
serviceHelpers.postAutenticate('/api/v1/cartRules/getGroupSearchNameCartRuleGroup', data, fnSuccess, fnError);
}

export function deleteUserCartRoleUser(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/cartRules/deleteUserCartRoleUser/' +  id, fnSuccess, fnError);
}
export function deleteCategoriesCartRoleCategories(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/cartRules/deleteCategoriesCartRoleCategories/' +  id, fnSuccess, fnError);
}

export function deleteProducersCartRoleProducers(id, fnSuccess, fnError) {
serviceHelpers.delAutenticate('/api/v1/cartRules/deleteProducersCartRoleProducers/' +  id, fnSuccess, fnError);
}

export function deleteSuppliersCartRoleSuppliers(id, fnSuccess, fnError) {
serviceHelpers.delAutenticate('/api/v1/cartRules/deleteSuppliersCartRoleSuppliers/' +  id, fnSuccess, fnError);
}

export function deleteSpecificProductsCartRole(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/cartRules/deleteSpecificProductsCartRole/' +  id, fnSuccess, fnError);
}

export function deleteCartRoleGroup(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/cartRules/deleteCartRoleGroup/' +  id, fnSuccess, fnError);
}
 export function deleteCartRoleCarriers(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/cartRules/deleteCartRoleCarriers/' +  id, fnSuccess, fnError);
}

 export function deleteCartRoleItem(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/cartRules/deleteCartRoleItem/' +  id, fnSuccess, fnError);
 }
 
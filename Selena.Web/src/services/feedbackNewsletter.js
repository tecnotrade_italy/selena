/***   feedbackNewsletterServices   ***/

/***   GET   ***/

export function getAllByNewsletter(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/reportnewsletter/allByNewsletter/' + id, fnSuccess, fnError);
}
export function getChart(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/reportnewsletter/getChart/' + id, fnSuccess, fnError);
}
export function getChartGraphics(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/reportnewsletter/getChartGraphics/' + id, fnSuccess, fnError);
}
export function getChartGraphicsforTime(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/reportnewsletter/getChartGraphicsforTime/' + id, fnSuccess, fnError);
}

/***   END GET   ***/

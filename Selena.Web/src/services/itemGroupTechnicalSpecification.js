/***   itemGroupTechnicalSpecificationServices   ***/

/***   GET   ***/

export function getItemWithoutGroupTechnicalSpecification(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/itemgrouptechnicalspecification/getItemWithoutGroupTechnicalSpecification', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/itemgrouptechnicalspecification/getItemWithoutGroupTechnicalSpecification',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/***   END GET   ***/

/***   searchServices   ***/

/***   POST   ***/

export function search(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/search', search, fnSuccess, fnError);
}
export function searchImplemented(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/search/searchImplemented', search, fnSuccess, fnError);
}
export function searchGeneral(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/search/searchGeneral', search, fnSuccess, fnError);
}



/***   END POST   ***/

/***   schedulerServices   ***/

/***   GET   ***/

export function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/scheduler/all/' + ln, fnSuccess, fnError);
}

export function typeScheduler(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/scheduler/typeScheduler?search' + search, fnSuccess, fnError);
}

export function repeatHourScheduler(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/scheduler/repeatHourScheduler?search' + search, fnSuccess, fnError);
}

export function repeatMinutesScheduler(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/scheduler/repeatMinutesScheduler?search' + search, fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/scheduler', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(dtoNation, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/scheduler', dtoNation, fnSuccess, fnError);
}

export function updateCheckActive(request, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/scheduler/updateCheckActive', request, fnSuccess, fnError);
}

export function updateCheckNotification(request, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/scheduler/updateCheckNotification', request, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/scheduler/' +  data, fnSuccess, fnError);
}


/***   END DELETE   ***/

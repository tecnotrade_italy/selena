/***   contactServices   ***/

/***   GET   ***/

export function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/contact/' + id, fnSuccess, fnError);
}

export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/contact/all', fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function importData(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/contact/importData', data, fnSuccess, fnError);
}

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/contact', data, fnSuccess, fnError);
}

export function subscribe(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/contact/subscribe', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(dtoCustomer, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/contact', dtoCustomer, fnSuccess, fnError);
}

export function unsubscribe(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/contact/unsubscribe', data, fnSuccess, fnError);
}
export function unsubscribeNewsletter(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/contact/unsubscribeNewsletter', data, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/contact/' + id, fnSuccess, fnError);
}

/***   END DELETE   ***/

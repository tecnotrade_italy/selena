/***   jsServices   ***/

/***   GET   ***/

export function get(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/js/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

/***   END GET   ***/

/***   POST   ***/

export function update(css, fnSuccess, fnError) {
  var data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    css: css
  };

  serviceHelpers.postAutenticate('/api/v1/js', data, fnSuccess, fnError);
}

export function updateHead(css, fnSuccess, fnError) {
  var data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    css: css
  };

  serviceHelpers.postAutenticate('/api/v1/js/updateHead', data, fnSuccess, fnError);
}

/***   END POST   ***/

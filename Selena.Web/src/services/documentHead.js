/***  documentHeadServices   ***/

/***   GET   ***/

export function getSixtenOrderConfirmed(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/documenthead/getSixtenOrderConfirmed/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/documenthead/getSixtenOrderConfirmed/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function getSixtenOrderPicked(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/documenthead/getSixtenOrderPicked/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/documenthead/getSixtenOrderConfirmed/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function getSelectMonthBeforeDDTSixten(idCustomer, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/documenthead/selectMonthBeforeDDTSixten/' + idCustomer + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/documenthead/selectMonthBeforeDDTSixten/' + idCustomer + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function getMdMicrodetectorsShippingList(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/documenthead/getMdMicrodetectorsShippingList/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/documenthead/getSixtenOrderConfirmed/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/***   END GET   ***/

/***   UPDATE   ***/

export function confirmBeforeDDTSixten(idCustomer, idMonth, idCarriage, idCarrier, idPackagingTypeSixten, idShipmentCodeSixten, weight, packagingNumber, routeParameter, fnSuccess, fnError) {
  var data = {
    idCustomer: idCustomer,
    idMonth: idMonth,
    idCarriage: idCarriage,
    idCarrier: idCarrier,
    idPackagingTypeSixten: idPackagingTypeSixten,
    idShipmentCodeSixten: idShipmentCodeSixten,
    weight: weight,
    packagingNumber: packagingNumber,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(routeParameter)
  };

  serviceHelpers.postAutenticate('/api/v1/documenthead/confirmBeforeDDTSixten', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'POST',
  //   url: configData.wsRootServicesUrl + '/api/v1/documenthead/confirmBeforeDDTSixten',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  // }).done(function (result) {
  //   if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
  //     fnSuccess(result);
  //   }
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/***   END UPDATE   ***/

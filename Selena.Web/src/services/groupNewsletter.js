/***   groupNewsletterServices   ***/

/***   GET   ***/

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/groupnewsletter/select?search' + search, fnSuccess, fnError);
}

export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/groupnewsletter/' + id, fnSuccess, fnError);
}

export function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/groupnewsletter/' + id, fnSuccess, fnError);
}

export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/groupnewsletter/all', fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(dtoVatType, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/groupnewsletter', dtoVatType, fnSuccess, fnError);
}
/***   END PUT   ***/

/***   POST   ***/
export function update(dtoVatType, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/groupnewsletter', dtoVatType, fnSuccess, fnError);
}
/***   END POST   ***/

/***   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/groupnewsletter/' + id, fnSuccess, fnError);
}
export function insertUser(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/groupnewsletter/insertUser', data, fnSuccess, fnError);
}
export function delUser(IdCustomer, rowIdSelected, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/groupnewsletter/delUser/' + IdCustomer + '/' + rowIdSelected, fnSuccess, fnError);
}

export function getAllByUser(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/groupnewsletter/getAllByUser', data, fnSuccess, fnError);
}

export function getAllByUserGroup(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/groupnewsletter/getAllByUserGroup', data, fnSuccess, fnError);
}

export function getAllByContact(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/groupnewsletter/getAllByContact', data, fnSuccess, fnError);
}

export function insertContact(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/groupnewsletter/insertContact', data, fnSuccess, fnError);
}
export function insertGroupUser(data, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/groupnewsletter/insertGroupUser', data, fnSuccess, fnError);
}

export function delGroupUser(idGroupUser, rowIdSelected, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/groupnewsletter/delGroupUser/' + idGroupUser + '/' + rowIdSelected, fnSuccess, fnError);
}

export function delContact(IdContact, rowIdSelected, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/groupnewsletter/delContact/' + IdContact + '/' + rowIdSelected, fnSuccess, fnError);
}
export function getUserSearch(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/groupnewsletter/getUserSearch', data, fnSuccess, fnError);
}

/***   END DELETE   ***/

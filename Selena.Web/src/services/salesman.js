/***   salesmanServices   ***/

/***   GET   ***/

export function getAll(ln, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/salesman/all/' + ln, fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/salesman', data, fnSuccess, fnError);
}



/***   END PUT   ***/

/***   POST   ***/

export function insertSalesmanUsers(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesman/insertSalesmanUsers', data, fnSuccess, fnError);
}

export function removeSalesmanUsers(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesman/removeSalesmanUsers', data, fnSuccess, fnError);
}

export function update(dtoNation, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/salesman', dtoNation, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/salesman/' +  data, fnSuccess, fnError);
}

/***   END DELETE   ***/

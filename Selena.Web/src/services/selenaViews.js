/***   selenaViewsServices   ***/

/***   GET   ***/

export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/selenaview/all', fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/selenaview', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(dtoNation, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/selenaview', dtoNation, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(data, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/selenaview/' +  data, fnSuccess, fnError);
}

/***   END DELETE   ***/

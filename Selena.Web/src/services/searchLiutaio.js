/***   searchLiutaioServices   ***/

/***   POST   ***/
export function searchImplemented(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/searchliutaio/searchImplemented', search, fnSuccess, fnError);
}
/***   END POST   ***/

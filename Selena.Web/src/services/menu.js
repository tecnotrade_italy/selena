/***   menuServices   ***/

/***   GET   ***/

export function getSettings(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/menu/getSettings' + id, fnSuccess, fnError);

  // $.ajax({
  //     type: 'GET',
  //     url: configData.wsRootServicesUrl + '/api/v1/menu/getSettings/' + id,
  //     dataType: 'json',
  //     headers: { Accept: "application/json", Authorization: storageData.sTokenKey() },
  //     contentType: 'application/json',
  // }).done(function (result) {
  //     fnSuccess(result);
  // }).fail(function (response) {
  //     failServiceHelpers.fail(response);
  // });
}

/***   END GET   ***/

/***   PUT   ***/

export function insertLink(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/menu/link', data, fnSuccess, fnError);
}

export function insertNewLink(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/menu/newlink', data, fnSuccess, fnError);
}

export function insertFolder(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/menu/folder', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function updateLink(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/menu/link', data, fnSuccess, fnError);
}

export function updateNewLink(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/menu/updateNewlink', data, fnSuccess, fnError);
}



export function updateFolder(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/menu/folder', data, fnSuccess, fnError);

  //   $.ajax({
  //     type: 'POST',
  //     url: configData.wsRootServicesUrl + '/api/v1/menu/folder',
  //     dataType: 'json',
  //     headers: {
  //       Authorization: storageData.sTokenKey()
  //     },
  //     contentType: 'application/json',
  //     data: JSON.stringify(functionHelpers.clearNullOnJson(menu))
  //   }).done(function (result) {
  //     fnSuccess(result);
  //   }).fail(function (response) {
  //     failServiceHelpers.fail(response);
  //   });
}

/***   END POST   ***/

/***   rolesServices   ***/

/***   GET   */

export function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/roles/select',
      dataType: 'json',
      headers: {
        Accept: "application/json",
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function (params) {
        return {
          idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
          search: $.trim(params.term)
        };
      },
      processResults: function (result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/roles/select?search=' + search, fnSuccess, fnError);
}
export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/roles/' + id, fnSuccess, fnError);
}

export function getByIdRoles(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/roles/getByIdRoles/' + id, fnSuccess, fnError);
}
export function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/roles/' + id, fnSuccess, fnError);
}

export function getAllWithoutAdmin(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/roles/getAllWithoutAdmin', fnSuccess, fnError);
}

export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/roles/all', fnSuccess, fnError);
}

export function getAllNoAuth(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/roles/allNoAuth', fnSuccess, fnError);
}

/***   END GET   */

/***   PUT   ***/

export function insert(DtoRole, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/roles', DtoRole, fnSuccess, fnError);
}

export function insertNoAuth(DtoRole, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/roles', DtoRole, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(DtoRole, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/roles', DtoRole, fnSuccess, fnError);
}

export function updateNoAuth(DtoRole, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/roles', DtoRole, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/roles/' + id, fnSuccess, fnError);
}

/***   END DELETE   ***/

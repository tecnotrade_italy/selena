/***   contentServices   ***/

/***   GET   ***/

export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    "/api/v1/content/getAll/" + menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  );
}

export function getByLanguage(idContent, idLanguage, fnSuccess, fnError) {
  serviceHelpers.getAutenticate(
    "/api/v1/content/getByLanguage/" +
      idContent +
      "/" +
      idLanguage +
      "/" +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  );
}



export function getByCode(code, idLanguage, fnSuccess, fnError) {
  var url = "/api/v1/content/getByCode/" + code;

  if (functionHelpers.isValued(idLanguage)) {
    url += "/" + idLanguage;
  }
  serviceHelpers.get(url, fnSuccess, fnError);
}


export function getByCodeRender(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/content/getByCodeRender', data, fnSuccess, fnError);
}

export function getByCodeUser(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/content/getByCodeUser', data, fnSuccess, fnError);
}

export function getTagByType(code, fnSuccess, fnError) {
  var url = "/api/v1/content/getTagByType/" + code;

  serviceHelpers.getAutenticate(url, fnSuccess, fnError);
}

export function getBreadcrumbs(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/content/getBreadcrumbs', data, fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(code, idLanguage, content, fnSuccess, fnError) {
  var data = {
    code: code,
    idLanguage: idLanguage,
    content: content,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl()
  };

  serviceHelpers.putAutenticate("/api/v1/content", data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(id, code, idLanguage, content, description, fnSuccess, fnError) {
  var data = {
    id: id,
    code: code,
    idLanguage: idLanguage,
    content: content,
    description: description,
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl()
  };

  serviceHelpers.postAutenticate("/api/v1/content", data, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate(
    "/api/v1/content/" + id + "/" + menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  );
}

export function deleteLanguage(idContent, idLanguage, fnSuccess, fnError) {
  serviceHelpers.delAutenticate(
    "/api/v1/content/deleteLanguage/" +
      idContent +
      "/" +
      idLanguage +
      "/" +
      menuHelpers.getIdFunctionByAdminUrl(),
    fnSuccess,
    fnError
  );
}

/***   END DELETE   ***/

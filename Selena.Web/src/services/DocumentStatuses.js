/***   DocumentStatusesServices   ***/

/***   GET   */

export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/documentStatuses/' + id, fnSuccess, fnError);
}

export function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/documentStatuses/' + id, fnSuccess, fnError);
}

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/documentStatuses/select?search=' + search, fnSuccess, fnError);
}

export function getAll(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/documentStatuses/all/' + idLanguage, fnSuccess, fnError);
}
/***   END GET   */

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/documentStatuses', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/documentStatuses', data, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/documentStatuses/' + id, fnSuccess, fnError);
}

/***   END DELETE   ***/



/***   customerServices   ***/

/***   GET   ***/

export function select(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/customer/select?search' + search, fnSuccess, fnError);
}

export function selectBeforeDDTSixten(search, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/customer/selectBeforeDDTSixten?search' + search, fnSuccess, fnError);
}

export function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/customer/' + id, fnSuccess, fnError);
}

export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/customer/all', fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(dtoCustomer, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/customer', dtoCustomer, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(dtoCustomer, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/customer', dtoCustomer, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/customer/' + id, fnSuccess, fnError);
}

/***   END DELETE   ***/

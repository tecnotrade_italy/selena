/***   newsletterServices   ***/

/***   GET   ***/

export function get(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletter/' + id, fnSuccess, fnError);
}

export function getAll(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletter/all', fnSuccess, fnError);
}

export function getAllCampaign(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletter/getAllCampaign', fnSuccess, fnError);
}

export function getemail(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletter/getemail/' + id, fnSuccess, fnError);
}

export function getemailusers(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletter/getemailusers/' + id, fnSuccess, fnError);
}
export function getListEmailByGroup(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletter/getListEmailByGroup/' + id, fnSuccess, fnError);
}
export function getByListDateForId(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletter/getByListDateForId/' + id, fnSuccess, fnError);
}
export function getListEmailQueryGroupNewsletter(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletter/getListEmailQueryGroupNewsletter/' + id, fnSuccess, fnError);
}
export function unsubscribeallfornewsletter(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/newsletter/unsubscribeallfornewsletter', data, fnSuccess, fnError);
}
export function confirmSendMailTestSender(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/newsletter/confirmSendMailTestSender', data, fnSuccess, fnError);
}

export function updateOnlineNewsletterCheck(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/newsletter/updateOnlineNewsletterCheck', data, fnSuccess, fnError);
}

export function updateemailnewsletter(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/newsletter/updateemailnewsletter', data, fnSuccess, fnError);
}

export function getAllviewlistmailerror(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletter/getAllviewlistmailerror/' + id, fnSuccess, fnError);
}
export function getAllviewlistmailopen(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/newsletter/getAllviewlistmailopen/' + id, fnSuccess, fnError);
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(dtoNewsletter, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/newsletter', dtoNewsletter, fnSuccess, fnError);
}
export function clone(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/newsletter/clone', data, fnSuccess, fnError);
}

export function insertCampaign(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/newsletter/insertCampaign', data, fnSuccess, fnError);
}

/***   END PUT   ***/

/***   POST   ***/

export function update(dtoNewsletter, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/newsletter', dtoNewsletter, fnSuccess, fnError);
}

export function updateCampaign(dtoNewsletter, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/newsletter/updateCampaign', dtoNewsletter, fnSuccess, fnError);
}
export function unsubscribefornewsletter(dtoNewsletter, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/newsletter/unsubscribefornewsletter', dtoNewsletter, fnSuccess, fnError);
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/newsletter/' + id, fnSuccess, fnError);
}

/***   END DELETE   ***/

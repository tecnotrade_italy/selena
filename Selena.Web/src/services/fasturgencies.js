/***   FasturgenciesServices   ***/

/***   INSERT   ***/

export function insert(DtoFast, fnSuccess, fnError) {
  serviceHelpers.put('/api/v1/fasturgencies', DtoFast, fnSuccess, fnError);
  
}

export function getUserData(id, fnSuccess, fnError) {
  serviceHelpers.get('/api/v1/fasturgencies/getUserData/' + id, fnSuccess, fnError);
}

export function updateFast(data, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/fasturgencies/updateFast', data, fnSuccess, fnError);
}
export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/fasturgencies/' + id, fnSuccess, fnError);
}



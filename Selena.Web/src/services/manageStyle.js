/***   manageStyleServices   ***/

/***   GET   ***/

export function get(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/managestyle/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}
export function getCssEmail(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/managestyle/getCssEmail/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

export function getCssHead(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/managestyle/getCssHead/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);
}

/***   END GET   ***/

/***   POST   ***/

export function update(css, fnSuccess, fnError) {
  var data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    css: css
  };

  serviceHelpers.postAutenticate('/api/v1/managestyle', data, fnSuccess, fnError);
}

export function updateCssEmail(cssEmail, fnSuccess, fnError) {
  var data = {
    idFunctionality: menuHelpers.getIdFunctionByAdminUrl(),
    cssEmail: cssEmail
  };
  serviceHelpers.postAutenticate('/api/v1/managestyle/updateCssEmail', data, fnSuccess, fnError);
}

export function updateSaveGlobal(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/managestyle/updateSaveGlobal', data, fnSuccess, fnError);
}


export function updateSaveHead(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/managestyle/updateSaveHead', data, fnSuccess, fnError);
}


 
/***   END POST   ***/

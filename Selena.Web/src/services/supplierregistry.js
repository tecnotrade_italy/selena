/***   supplierregistryServices   ***/

/***   GET   ***/

export function getAllSupplierRegistrySearch(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/businessnamesupplier/getAllSupplierRegistrySearch', search, fnSuccess, fnError);
}


export function getById(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/businessnamesupplier/getById/' + id, fnSuccess, fnError);
}


export function getByIdUp(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/businessnamesupplier/getByIdUp/' + id, fnSuccess, fnError);
}

export function updatedetailsupplierregistry(DtoBusinessName, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/businessnamesupplier/updatedetailsupplierregistry/', DtoBusinessName, fnSuccess, fnError);
}

export function insertsupplierregistry(DtoBusinessName, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/businessnamesupplier/insertsupplierregistry/', DtoBusinessName, fnSuccess, fnError);
}
export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/businessnamesupplier/' + id, fnSuccess, fnError);
}




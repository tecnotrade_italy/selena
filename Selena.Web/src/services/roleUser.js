/***   roleUserServices   ***/

/*** PUT ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/roleuser', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/roleuser/insert',
  //   dataType: 'json',
  //   headers: {
  //     "Accept": "application/json",
  //     "Authorization": storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/*** END PUT ***/

/*** DELETE ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/roleuser/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'DELETE',
  //   url: configData.wsRootServicesUrl + '/api/v1/roleuser/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     "Accept": "application/json",
  //     "Authorization": storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}



/*** END DELETE ***/

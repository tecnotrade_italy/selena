/***   externalRepairServices   ***/

/***   GET   ***/

export function getAllexternalRepair(idLanguage, fnSuccess, fnError) {
  if (idLanguage === undefined || idLanguage == '') {
    idLanguage = storageData.sIdLanguage();
  }
  serviceHelpers.getAutenticate('/api/v1/externalrepair/getAllexternalRepair/' + idLanguage, fnSuccess, fnError);
}
export function getByRepair(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/externalrepair/getByRepair/' + id, fnSuccess, fnError);
}

export function getbyFaultModule(id, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/externalrepair/getbyFaultModule/' + id, fnSuccess, fnError);
}
export function updateRepairExternal(DtoExternalRepair, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/externalrepair/updateRepairExternal', DtoExternalRepair, fnSuccess, fnError);
}
/*** DELETE ***/
export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/externalrepair/' + id, fnSuccess, fnError);
}

export function getAllexternalRepairSearch(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/externalrepair/getAllexternalRepairSearch', search, fnSuccess, fnError);
}

export function getexternalRepairSearchViewOpen(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/externalrepair/getexternalRepairSearchViewOpen', search, fnSuccess, fnError);
}
export function getexternalRepairSearchViewClose(search, fnSuccess, fnError) {
  serviceHelpers.post('/api/v1/externalrepair/getexternalRepairSearchViewClose', search, fnSuccess, fnError);
}
/*** DELETE ***/


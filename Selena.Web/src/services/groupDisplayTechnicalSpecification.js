/***   groupDisplayTechnicalSpecificationServices   ***/

/***   GET   ***/

export function getDtoTable(fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/groupdisplaytechnicalspecification/getDtoTable', fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification/getDtoTable',
  //   dataType: 'json',
  //   headers: { Authorization: storageData.sTokenKey() },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function getTechnicalSpecification(id, fnSuccess, fnError) {
  serviceHelpers.getAutenticate('/api/v1/groupdisplaytechnicalspecification/getTechnicalSpecification/' + id, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification/getTechnicalSpecification/' + id,
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function checkExists(description, idLanguage, id, fnSuccess, fnError) {
  var url = configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification/checkExists/' + description + '/' + idLanguage;

  if (functionHelpers.isValued(id)) {
    url = url + '/' + id;
  }
  serviceHelpers.getAutenticate(url, fnSuccess, fnError);

  // $.ajax({
  //   type: 'GET',
  //   url: url,
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/***   END GET   ***/

/***   PUT   ***/

export function insert(data, fnSuccess, fnError) {
  serviceHelpers.putAutenticate('/api/v1/groupdisplaytechnicalspecification', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(dtoGroupDisplayTechnicalSpecification))
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

export function addTechnicalSpecification(id, idTechnicalSpecification, fnSuccess, fnError) {
  serviceHelpers.putAutenticateWithoutData('/api/v1/groupdisplaytechnicalspecification/addTechnicalSpecification/' + id + '/' + idTechnicalSpecification + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'PUT',
  //   url: configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification/addTechnicalSpecification/' + id + '/' + idTechnicalSpecification + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/***   END PUT   ***/

/***   POST   ***/

export function update(data, fnSuccess, fnError) {
  serviceHelpers.postAutenticate('/api/v1/groupdisplaytechnicalspecification', data, fnSuccess, fnError);

  // $.ajax({
  //   type: 'POST',
  //   url: configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification',
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json',
  //   data: JSON.stringify(functionHelpers.clearNullOnJson(dtoGroupDisplayTechnicalSpecification))
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

export function updateTechnicalSpecification(id, order, fnSuccess, fnError) {
  serviceHelpers.postAutenticateWithoutData('/api/v1/groupdisplaytechnicalspecification/updateTechnicalSpecification/' + id + '/' + order + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'POST',  
  //   url: configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification/updateTechnicalSpecification/' + id + '/' + order + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response);
  // });
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/groupdisplaytechnicalspecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'DELETE',
  //   url: configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

export function deleteGroupDisplayTechnicalSpecificationTechnicalSpecification(id, fnSuccess, fnError) {
  serviceHelpers.delAutenticate('/api/v1/groupdisplaytechnicalspecification/deleteGroupDisplayTechnicalSpecificationTechnicalSpecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(), fnSuccess, fnError);

  // $.ajax({
  //   type: 'DELETE',
  //   url: configData.wsRootServicesUrl + '/api/v1/groupdisplaytechnicalspecification/deleteGroupDisplayTechnicalSpecificationTechnicalSpecification/' + id + '/' + menuHelpers.getIdFunctionByAdminUrl(),
  //   dataType: 'json',
  //   headers: {
  //     Authorization: storageData.sTokenKey()
  //   },
  //   contentType: 'application/json'
  // }).done(function (result) {
  //   fnSuccess(result);
  // }).fail(function (response) {
  //   failServiceHelpers.fail(response, fnError);
  // });
}

/***   END DELETE   ***/

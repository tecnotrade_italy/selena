/* eslint-disable import/first */
/* eslint-disable spaced-comment */
import Vue from "vue";
import App from "./App.vue";
import Aut from "./Aut.vue";
import router from "./router";
import VueVisible from "vue-visible";
import Select2 from "v-select2-component";
import datePicker from "vue-bootstrap-datetimepicker";
//import draggable from "vuedraggable";
import VueFroala from "vue-froala-wysiwyg";
import VueMeta from "vue-meta";
import WebCam from "vue-web-cam";
Vue.use(WebCam);
Vue.use(VueVisible);
Vue.use(VueMeta);
Vue.component("Select2", Select2);
Vue.component("datePicker", datePicker);
//Vue.component("draggable", draggable);
Vue.use(VueFroala);

Vue.config.productionTip = false;


//19-11-2020 Selena Automation
// Vuetify
/*
import vuetify from "@/plugins/automations-module/vuetify";

// Flowy
import FlowyPlugin from "@hipsjs/flowy-vue";
import "@hipsjs/flowy-vue/dist/lib/flowy-vue.css";
Vue.use(FlowyPlugin);

// Automations
import AutomationsModule from "./plugins/automations-module";
Vue.use(AutomationsModule, {
  endpoint: "https://automation.tecnotrade.com",
  timePicker12Hr: false
});*/

//END 19-11-2020 Selena Automation

import "bootstrap";
import "ionicons";
import "pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css";
import "@/assets/css/vendors/bootstrap-editable.css";
import "@/assets/css/vendors/bootstrap-table/bootstrap-table.css";
import "@/assets/css/vendors/bootstrap-table/extensions/bootstrap-table-filter-control.css";
import "@/assets/css/vendors/flag-icon-css/css/flag-icon.min.css";
import "@/assets/css/vendors/font-awesome/css/font-awesome.css";
import "@/assets/css/vendors/simple-line-icons/css/simple-line-icons.css";
import "@/assets/css/vendors/toastr.min.css";
import "@/assets/css/vendors/spinkit.css";
import "@/assets/css/style.css";
import "@/assets/css/admin.css";

import 'codemirror/lib/codemirror.css';
import 'codemirror/lib/codemirror.js';
import 'codemirror/mode/css/css.js';
import 'codemirror/mode/xml/xml.js';


// import '@/assets/css/bormac.css';
// import '@/assets/css/sixten.css';

/***   JS   ***/
window.$ = require("jquery");
window.jQuery = require("jquery");
import jqueryValidation from "jquery-validation";
window.moment = require("moment");
window.toastr = require("toastr");
import dateFormat from "@/assets/js/jquery-dateFormat/jquery-dateFormat.js";

/***   Data   ***/
window.configData = require("@/data/config.js");
window.notificationData = require("@/data/notification.js");
window.storageData = require("@/data/storage.js");

/***   Helpers   ***/
window.calendarHelpers = require("@/helpers/calendar.js");
window.configurationHelpers = require("@/helpers/configuration.js");
window.contactHelpers = require("@/helpers/contact.js");
window.dataHelpers = require("@/helpers/data.js");
window.dictionaryHelpers = require("@/helpers/dictionary.js");
window.failServiceHelpers = require("@/helpers/failService.js");
window.functionHelpers = require("@/helpers/function.js");
window.groupDisplayTechnicalSpecificationHelpers = require("@/helpers/groupDisplayTechnicalSpecification.js");
window.groupTechnicalSpecificationHelpers = require("@/helpers/groupTechnicalSpecification.js");
window.interventionHelpers = require("@/helpers/intervention.js");
window.peopleHelpers = require("@/helpers/people.js");
window.adsHelpers = require("@/helpers/ads.js");
window.languageHelpers = require("@/helpers/language.js");
window.loginHelpers = require("@/helpers/login.js");
window.menuHelpers = require("@/helpers/menu.js");
window.negotiationHelpers = require("@/helpers/negotiation.js");
window.notificationHelpers = require("@/helpers/notification.js");
window.pageHelpers = require("@/helpers/page.js");
window.serviceHelpers = require("@/helpers/service.js");
window.searchHelpers = require("@/helpers/search.js");
window.spinnerHelpers = require("@/helpers/spinner.js");
window.tableHelpers = require("@/helpers/table.js");
window.technicalSpecificationHelpers = require("@/helpers/technicalSpecification.js");
window.userHelpers = require("@/helpers/user.js");
window.validationHelpers = require("@/helpers/validation.js");
window.personaldataHelpers = require("@/helpers/personaldata.js");
window.itemsHelpers = require("@/helpers/items.js");
window.searchShopHelpers = require("@/helpers/searchShop.js");
window.interventionHelpers = require("@/helpers/intervention.js");
window.statesHelpers = require("@/helpers/states.js");
window.rolesHelpers = require("@/helpers/roles.js");
window.itemsGrHelpers = require("@/helpers/itemsGr.js");
window.searchLiutaioHelpers = require("@/helpers/searchLiutaio.js");
window.searchSalaRegistrazioneHelpers = require("@/helpers/searchSalaRegistrazione.js");
window.searchInterventionHelpers = require("@/helpers/searchIntervention.js");
window.searchUserHelpers = require("@/helpers/searchUser.js");
window.searchItemsHelpers = require("@/helpers/searchItems.js");
window.processingviewHelpers = require("@/helpers/processingview.js");
window.quotesHelpers = require("@/helpers/quotes.js");
window.externalRepairHelpers = require("@/helpers/externalRepair.js");
window.fasturgenciesHelpers = require("@/helpers/fasturgencies.js");
window.internalRepairHelpers = require("@/helpers/internalRepair.js");
window.interventioncustomerHelpers = require("@/helpers/interventioncustomer.js");
window.userAddressHelpers = require("@/helpers/userAddress.js");
window.salesOrderHelpers = require("@/helpers/salesOrder.js");
window.accountVisionHelpers = require("@/helpers/accountVision.js");
window.searchGrItemsHelpers = require("@/helpers/searchGrItems.js");
window.supplierregistryHelpers = require("@/helpers/supplierregistry.js");
window.technicalviewHelpers = require("@/helpers/technicalview.js");
window.searchSchoolMusicHelpers = require("@/helpers/searchSchoolMusic.js");
window.searchDataSheetHelpers = require("@/helpers/searchDataSheet.js");
window.downloadHelpers = require("@/helpers/download.js");
window.attachmentFileUserHelpers = require("@/helpers/attachmentFileUser.js");





/***   Services   ***/
window.adminConfigurationServices = require("@/services/adminConfiguration.js");
window.automationServices = require("@/services/automation.js");
window.carriageServices = require("@/services/carriage.js");
window.carrierServices = require("@/services/carrier.js");
window.categoriesServices = require("@/services/categories.js");
window.categoryTypeServices = require("@/services/categoryType.js");
window.contactServices = require("@/services/contact.js");
window.contactGroupNewsletterServices = require("@/services/contactGroupNewsletter.js");
window.customerGroupNewsletterServices = require("@/services/customerGroupNewsletter.js");
window.contactRequestServices = require("@/services/contactRequest.js");
window.contentServices = require("@/services/content.js");
window.customerServices = require("@/services/customer.js");
window.dictionaryServices = require("@/services/dictionary.js");
window.documentHeadServices = require("@/services/documentHead.js");
window.documentRowServices = require("@/services/documentRow.js");
window.feedbackNewsletterServices = require("@/services/feedbackNewsletter.js");
window.googleAnalyticsServices = require("@/services/googleAnalytics.js");
window.groupDisplayTechnicalSpecificationServices = require("@/services/groupDisplayTechnicalSpecification.js");
window.groupTechnicalSpecificationServices = require("@/services/groupTechnicalSpecification.js");
window.groupNewsletterServices = require("@/services/groupNewsletter.js");
window.iconServices = require("@/services/icon.js");
window.itemServices = require("@/services/item.js");
window.itemGroupTechnicalSpecificationServices = require("@/services/itemGroupTechnicalSpecification.js");
window.itemTechnicalSpecificationServices = require("@/services/itemTechnicalSpecification.js");
window.jsServices = require("@/services/js.js");
window.languageServices = require("@/services/language.js");
window.loginServices = require("@/services/login.js");
window.manageStyleServices = require("@/services/manageStyle.js");
window.menuServices = require("@/services/menu.js");
window.nationServices = require("@/services/nation.js");
window.negotiationServices = require("@/services/negotiation.js");
window.newsletterServices = require("@/services/newsletter.js");
window.newsletterRecipientServices = require("@/services/newsletterRecipient.js");
window.packageServices = require("@/services/package.js");
window.packagingTypeSixtenServices = require("@/services/packagingTypeSixten.js");
window.pageCategoryServices = require("@/services/pageCategory.js");
window.pageServices = require("@/services/page.js");
window.pageShareTypeServices = require("@/services/pageShareType.js");
window.postalCodeServices = require("@/services/postalCode.js");
window.roleServices = require("@/services/role.js");
window.roleUserServices = require("@/services/roleUser.js");
window.settingServices = require("@/services/setting.js");
window.shipmentCodeSixtenServices = require("@/services/shipmentCodeSixten.js");
window.selenaViewsServices = require("@/services/selenaViews.js");
window.messageServices = require("@/services/message.js");
window.messageTypeServices = require("@/services/messageType.js");
window.searchServices = require("@/services/search.js");
window.technicalSpecificationServices = require("@/services/technicalSpecification.js");
window.templateEditorServices = require("@/services/templateEditor.js");
window.templateEditorGroupServices = require("@/services/templateEditorGroup.js");
window.templateEditorTypeServices = require("@/services/templateEditorType.js");
window.unitMeasuresServices = require("@/services/UnitMeasure.js");
window.userServices = require("@/services/user.js");
window.vatTypeServices = require("@/services/vatType.js");
window.InterventionsServices = require("@/services/interventions.js");
window.faultModulesServices = require("@/services/faultModules.js");
window.statesServices = require("@/services/states.js");
window.producerServices = require("@/services/producer.js");
window.rolesServices = require("@/services/roles.js");
window.peopleServices = require("@/services/people.js");
window.searchShopServices = require("@/services/searchShop.js");
window.itemGrServices = require("@/services/itemGr.js");
window.searchLiutaioServices = require("@/services/searchLiutaio.js");
window.searchSalaRegistrazioneServices = require("@/services/searchSalaRegistrazione.js");
window.searchInterventionServices = require("@/services/searchIntervention.js");
window.searchUserServices = require("@/services/searchUser.js");
window.brandServices = require("@/services/brand.js");
window.tipologyServices = require("@/services/tipology.js");
window.typeOptionalServices = require("@/services/typeOptional.js");
window.optionalServices = require("@/services/optional.js");
window.salesmanServices = require("@/services/salesman.js");
window.itemAttachmentTypeServices = require("@/services/itemAttachmentType.js");
window.itemAttachmentFileServices = require("@/services/itemAttachmentFile.js");
window.businessnamesupplierServices = require("@/services/businessnamesupplier.js");
window.searchItemsServices = require("@/services/searchItems.js");
window.menuAdminCustomServices = require("@/services/menuAdmin.js");
window.processingviewServices = require("@/services/processingview.js");
window.quotesServices = require("@/services/quotes.js");
window.listServices = require("@/services/list.js");
window.regionsServices = require("@/services/regions.js");
window.provincesServices = require("@/services/provinces.js");
window.userAddressServices = require("@/services/userAddress.js");
window.salesOrderServices = require("@/services/salesOrder.js");
window.DocumentStatusesServices = require("@/services/DocumentStatuses.js");
window.cartDetailHelpers = require("@/helpers/cartDetail.js");
window.cartDetailServices = require("@/services/cartDetail.js");
window.cartServices = require("@/services/cart.js");
window.cartHelpers = require("@/helpers/cart.js");
window.voucherServices = require("@/services/voucher.js");
window.voucherTypeServices = require("@/services/voucherType.js");
window.cartRulesServices = require("@/services/cartRules.js");
window.externalRepairServices = require("@/services/externalRepair.js");
window.fasturgenciesServices = require("@/services/fasturgencies.js");
window.InternalRepairServices = require("@/services/internalRepair.js");
window.interventionCustomerServices = require("@/services/interventionCustomer.js");
window.AccountVisionServices = require("@/services/accountVision.js");
window.searchItemsGrServices = require("@/services/searchGrItems.js");
window.supplierregistryServices = require("@/services/supplierregistry.js");
window.technicalviewServices = require("@/services/technicalview.js");
window.dealerServices = require("@/services/dealer.js");
window.sendSmsServices = require("@/services/sendSms.js");
window.supportsServices = require("@/services/supports.js");
window.groupSmsServices = require("@/services/groupSms.js");
window.regionsServices = require("@/services/regions.js");
window.provincesServices = require("@/services/provinces.js");
window.citiesServices = require("@/services/cities.js");
window.zoneServices = require("@/services/zone.js");
window.searchSchoolMusicServices = require("@/services/searchSchoolMusic.js");
window.QueryGroupNewsletterServices = require("@/services/queryGroupNewsletter.js");
window.placesServices = require("@/services/places.js");
window.BookingServiceServices = require("@/services/bookingServices.js");
window.bookingsettingsServices = require("@/services/bookingsettings.js");
window.employeesServices = require("@/services/employees.js");
window.connectionsServices = require("@/services/connections.js");
window.appointmentsServices = require("@/services/appointments.js");
window.bookingStatesServices = require("@/services/bookingStates.js");
window.imageResizeServices = require("@/services/imageResize.js");
window.RobotsSettingServices = require("@/services/robotsServices.js");
window.formBuilderServices = require("@/services/formBuilder.js");
window.formBuilderTypeServices = require("@/services/formBuilderType.js");
window.searchDataSheetServices = require("@/services/searchDataSheet.js");
window.downloadServices = require("@/services/download.js");
window.communityImageReportTypeServices = require("@/services/communityImageReportType.js");
window.communityImageServices = require("@/services/communityImage.js");
window.communityImageCategoryServices = require("@/services/communityImageCategory.js");
window.ExceptionsServices = require("@/services/exceptions.js");
window.paymenttypeServices = require("@/services/paymenttype.js");
window.carrierstypeServices = require("@/services/carrierstype.js");
window.advertisingAreaServices = require("@/services/advertisingArea.js");
window.advertisingBannerServices = require("@/services/advertisingBanner.js");
window.amazonTemplateServices = require("@/services/amazonTemplate.js");
window.amazonServices = require("@/services/amazon.js");
window.CategoriesPagesServices = require("@/services/categoriesPages.js");
window.plantServices = require("@/services/plant.js");
window.redirectServices = require("@/services/redirect.js");
window.faqServices = require("@/services/faq.js");
window.pricelistgeneratorServices = require("@/services/pricelistgenerator.js");
window.schedulerServices = require("@/services/scheduler.js");
window.typologyKeywordsServices = require("@/services/typologyKeywords.js");
window.listKeywordsServices = require("@/services/listKeywords.js");

var page = App;
// This callback runs before every route change, including on page load.
router.beforeEach((to, from, next) => {
  
  if(to.path == '/admin/crm/automation'){
    page = Aut; 
  }
  else{
    page = App; 
  }
  var path = to.path;
  if (path == undefined || path == "/") {
    pageServices.getHome(function (result) {
      if (functionHelpers.isValued(result.data)) {

        var hideSearchEngine = "index, follow";
        if (result.data.hideSearchEngine) {
          //hideSearchEngine = "noindex, nofollow";
        }
        var ajaxTag = [{
          meta: {
            /* title: result.data.title,
             metaTags: [
               {name: 'robots', content: hideSearchEngine},
               {name: 'description', content: result.data.seoDescription},
               {name: 'canonical', content: path},
               {name: 'og:url', content: path},
               {property: 'og:title', content: result.data.shareTitle},
               {property: 'og:description', content: result.data.shareDescription},
               {property: 'og:image:url', content: result.data.urlCoverImage},
               {property: 'og:image:secure_url', content: result.data.urlCoverImage}
             ]*/
          }
        }];

        // This goes through the matched routes from last to first, finding the closest route with a title.
        // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
        const nearestWithTitle = ajaxTag.find(r => r.meta && r.meta.title);

        // Find the nearest route element with meta tags.
        const nearestWithMeta = ajaxTag.find(r => r.meta && r.meta.metaTags);
        //const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

        // If a route with a title was found, set the document (page) title to that value.
        if (nearestWithTitle) document.title = nearestWithTitle.meta.title;

        // Remove any stale meta tags from the document using the key attribute we set below.
        Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

        // Skip rendering meta tags if there are none.
        if (!nearestWithMeta) return next();

        // Turn the meta tag definitions into actual elements in the head.
        nearestWithMeta.meta.metaTags.map(tagDef => {
            const tag = document.createElement('meta');

            Object.keys(tagDef).forEach(key => {
              tag.setAttribute(key, tagDef[key]);
            });

            // We use this to track which meta tags we create, so we don't interfere with other ones.
            tag.setAttribute('data-vue-router-controlled', '');

            return tag;
          })
          // Add the meta tags to the document head.
          .forEach(tag => document.head.appendChild(tag));
      }
    });
  } else {
    pageServices.getRender(path, function (result) {
      if (functionHelpers.isValued(result.data.content)) {
        var hideSearchEngine = "index, follow";
        var title = result.data.title;
        if (result.data.hideSearchEngine) {
          //hideSearchEngine = "noindex, nofollow";
        }

        if (path.indexOf('admin') >= 0 || path.indexOf('Admin') >= 0) {
          title = configData.customer + ' | Sezione amministrativa';
        }

        var ajaxTag = [{
          meta: {
            title: title,
            metaTags: [{
                name: 'robots',
                content: hideSearchEngine
              },
              {
                name: 'description',
                content: result.data.seoDescription
              },
              {
                name: 'canonical',
                content: window.location.href
              },
              {
                property: 'og:url',
                content: window.location.href
              },
              {
                property: 'og:type',
                content: 'article'
              },
              {
                property: 'og:title',
                content: result.data.shareTitle
              },
              {
                property: 'og:description',
                content: result.data.shareDescription
              },
              {
                property: 'og:image:url',
                content: result.data.urlCoverImage
              },
              {
                property: 'og:image:secure_url',
                content: result.data.urlCoverImage
              }
            ]
          }
        }];

        // This goes through the matched routes from last to first, finding the closest route with a title.
        // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
        const nearestWithTitle = ajaxTag.find(r => r.meta && r.meta.title);

        // Find the nearest route element with meta tags.
        const nearestWithMeta = ajaxTag.find(r => r.meta && r.meta.metaTags);
        //const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

        // If a route with a title was found, set the document (page) title to that value.
        if (nearestWithTitle) document.title = nearestWithTitle.meta.title;

        // Remove any stale meta tags from the document using the key attribute we set below.
        Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

        // Skip rendering meta tags if there are none.
        if (!nearestWithMeta) return next();

        // Turn the meta tag definitions into actual elements in the head.
        nearestWithMeta.meta.metaTags.map(tagDef => {
            const tag = document.createElement('meta');

            Object.keys(tagDef).forEach(key => {
              tag.setAttribute(key, tagDef[key]);
            });

            // We use this to track which meta tags we create, so we don't interfere with other ones.
            tag.setAttribute('data-vue-router-controlled', '');

            return tag;
          })
          // Add the meta tags to the document head.
          .forEach(tag => document.head.appendChild(tag));
      } else {
        pageServices.getRender("/404", function (result) {
          if (functionHelpers.isValued(result.data.content)) {
            var hideSearchEngine = "index, follow";
            if (result.data.hideSearchEngine) {
              //hideSearchEngine = "noindex, nofollow";
            }
            var ajaxTag = [{
              meta: {
                title: result.data.title,
                metaTags: [{
                    name: 'robots',
                    content: hideSearchEngine
                  },
                  {
                    name: 'description',
                    content: result.data.seoDescription
                  },
                  {
                    name: 'canonical',
                    content: path
                  },
                  {
                    name: 'og:url',
                    content: path
                  },
                  {
                    property: 'og:title',
                    content: result.data.shareTitle
                  },
                  {
                    property: 'og:description',
                    content: result.data.shareDescription
                  },
                  {
                    property: 'og:image:url',
                    content: result.data.urlCoverImage
                  },
                  {
                    property: 'og:image:secure_url',
                    content: result.data.urlCoverImage
                  }
                ]
              }
            }];

            // This goes through the matched routes from last to first, finding the closest route with a title.
            // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
            const nearestWithTitle = ajaxTag.find(r => r.meta && r.meta.title);

            // Find the nearest route element with meta tags.
            const nearestWithMeta = ajaxTag.find(r => r.meta && r.meta.metaTags);
            //const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

            // If a route with a title was found, set the document (page) title to that value.
            if (nearestWithTitle) document.title = nearestWithTitle.meta.title;

            // Remove any stale meta tags from the document using the key attribute we set below.
            Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

            // Skip rendering meta tags if there are none.
            if (!nearestWithMeta) return next();

            // Turn the meta tag definitions into actual elements in the head.
            nearestWithMeta.meta.metaTags.map(tagDef => {
                const tag = document.createElement('meta');

                Object.keys(tagDef).forEach(key => {
                  tag.setAttribute(key, tagDef[key]);
                });

                // We use this to track which meta tags we create, so we don't interfere with other ones.
                tag.setAttribute('data-vue-router-controlled', '');

                return tag;
              })
              // Add the meta tags to the document head.
              .forEach(tag => document.head.appendChild(tag));
          }
        });
      }
    });
  }


  // This goes through the matched routes from last to first, finding the closest route with a title.
  // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
  /*const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
  const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

  // If a route with a title was found, set the document (page) title to that value.
  if(nearestWithTitle) document.title = nearestWithTitle.meta.title;

  // Remove any stale meta tags from the document using the key attribute we set below.
  Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

  // Skip rendering meta tags if there are none.
  if(!nearestWithMeta) return next();

  // Turn the meta tag definitions into actual elements in the head.
  nearestWithMeta.meta.metaTags.map(tagDef => {
    const tag = document.createElement('meta');

    Object.keys(tagDef).forEach(key => {
      tag.setAttribute(key, tagDef[key]);
    });

    // We use this to track which meta tags we create, so we don't interfere with other ones.
    tag.setAttribute('data-vue-router-controlled', '');

    return tag;
  })
  // Add the meta tags to the document head.
  .forEach(tag => document.head.appendChild(tag));*/

  next();
});

//***************************************************** */
// Ciao FIlippo.
// Ho circosritto il problema ad una incompatibilità con:
// * Il plugin vuedraggable che usate
// * il foglio di style /assets/css/style.css
// sto lavorando per capire come aggirare il problema
//***************************************************** */


/* end test route  */




// Before load page, load dictionary
$.when(storageData.sDictionary()).done(
  new Vue({
    router,
    //vuetify,
    render: h => h(page)
  }).$mount("#app")
);

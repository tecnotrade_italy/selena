const ENUMS = {
  NODE_TYPES: {
    TRIGGER: "trigger",
    TIME_DISPATCHER: "time_dispatcher",
    DELAY: "delay",
    OPERATION: "operation",
  },
  TRIGGER_TYPES: {
    //NEWSLETTER_SUBSCRIPTION: "newsletter_subscription",
    READ_RECEIPT: "received_read_receipt",
    DB_WATCHER: "db_watcher",
  },
  DAYS_OF_WEEK: [
    {
      value: 1,
      human: 'Lunedì',
      short: 'L',
    },
    {
      value: 2,
      human: 'Martedì',
      short: 'M',
    },
    {
      value: 3,
      human: 'Mercoledì',
      short: 'M',
    },
    {
      value: 4,
      human: 'Giovedì',
      short: 'G',
    },
    {
      value: 5,
      human: 'Venerdì',
      short: 'V',
    },
    {
      value: 6,
      human: 'Sabato',
      short: 'S',
    },
    {
      value: 0,
      human: 'Domenica',
      short: 'D',
    },
  ],
  OPERATION_TYPES: {
    EMAIL: "email",
    DB: "db",
  },
  PERIODS: {
    WEEK: "week",
    MONTH: "month",
    YEAR: "year",
  },
};

const STRUCTURES = {
  DEFAULT_NODE: {
    id: 0,
    parent_node_id: -1,
    type: ENUMS.NODE_TYPES.TRIGGER,
    trigger: {
      type: ENUMS.TRIGGER_TYPES.READ_RECEIPT,
      db_watcher: {
        table_name: "",
        conditions: [{
          operand: 'and',
          table_key: 'Id',
          comparer: '=',
          key: ''
        }],
        column_name: "",
        from_value: "*",
        to_value: "*",
      },
    },
    time_dispatcher: {
      repeat_on: [],
      repeat_each: ENUMS.PERIODS.WEEK,
      time_of_day: "09:00",
      repeat_until: null,
      max_occurrencies: 5,
      limit_occurrencies: true
    },
    delay: {
      interval: "00:00:00.00",
    },
    operation: {
      type: ENUMS.OPERATION_TYPES.EMAIL,
      email: {
        template_id: 0,
        group_id: 0,
        recipient: "",
      },
      db: {
        table_name: "",
        conditions: [{
          operand: 'and',
          table_key: 'Id',
          comparer: '=',
          key: ''
        }],
        column_name: "",
        value: "",
      },
    },
  },
};

export default { ENUMS: ENUMS, STRUCTURES: STRUCTURES };

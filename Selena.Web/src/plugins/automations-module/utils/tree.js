import constants from "./constants";

const TREE = {
  canAdd: (node, from, to, hierarchy) => {
    let allowed = true;

    const type = node["default-node-type"];
    const toType = to.data["default-node-type"] || to.data.type;

    console.debug("#### canAdd", type);
    console.debug("#### node:", node);
    console.debug("#### from:", from);
    console.debug("#### to:", to);
    console.debug("#### toType:", toType);

    // Can I drag the root node? Nope
    if (from && from.parentId <= 0) {
      allowed = false;
    }

    // Can I add a TimeDispatcher?
    // - Only if no other TimeDispacther are present in the hierarchy
    if (type === constants.ENUMS.NODE_TYPES.TIME_DISPATCHER) {
      allowed = !hierarchy.find(
        (x) => x.data.type === constants.ENUMS.NODE_TYPES.TIME_DISPATCHER
      );
    }

    // Can I add a Delay??
    // - Only if I'm not attaching it directly to another delay
    //   because it would make more sense to have a cumulative sleep
    // - And, it doesn't make sense to attach a delay to TimeDispatcher,
    //   because the TimeDispatcher already defines the time of day
    if (type === constants.ENUMS.NODE_TYPES.DELAY) {
      allowed =
        toType !== constants.ENUMS.NODE_TYPES.DELAY &&
        toType !== constants.ENUMS.NODE_TYPES.TIME_DISPATCHER;
    }

    // Can I add an Operation??
    // - Only if I'm not attaching it directly to another Operation
    //   because it would make more sense to have a cumulative sleep
    if (type === constants.ENUMS.NODE_TYPES.OPERATION) {
      allowed = toType !== constants.ENUMS.NODE_TYPES.OPEARTION;
    }

    console.log("allowed:", allowed);

    return allowed;
  },
};

export default TREE;

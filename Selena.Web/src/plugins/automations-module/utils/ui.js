const ICONS = {
  trigger: "mdi-flash",
  operation: "mdi-email",
  time_dispatcher: "mdi-calendar",
  delay: "mdi-timer",
};

const TEXTS = {
  NODE_TYPES: {
    trigger: "Trigger",
    operation: "Operazione",
    time_dispatcher: "Ripetizione",
    delay: "Delay",
  },
  TR: (label) => {
    if (label === "NEWSLETTER_SUBSCRIPTION")
      return "Iscrizione alla newsletter";
    else if (label === "READ_RECEIPT") return "Lettura E-Mail";
    else if (label === "DB_WATCHER") return "Monitor database";
    else if (label === "EMAIL") return "Invio E-Mail";
    else if (label === "DB") return "Scrittura su database";
    else if (label.toUpperCase() === "WEEK") return "Settimana";
    else if (label.toUpperCase() === "MONTH") return "Mese";
    else if (label.toUpperCase() === "YEAR") return "Anno";
    return label;
  },
};

const COLORS = {
  trigger: "#00b894",
  operation: "#0984e3",
  time_dispatcher: "#6c5ce7",
  delay: "#f7b731",
};

export default { ICONS: ICONS, TEXTS: TEXTS, COLORS: COLORS };

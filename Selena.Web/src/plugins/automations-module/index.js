// Components
import NodePeek from "./components/NodePeek.vue";
import NodeBlock from "./components/NodeBlock.vue";
import Root from "./Root.vue";

//import Vuetify from "vuetify/lib";
//import colors from "vuetify/lib/util/colors";

// Icons for dropdown
import "material-design-icons-iconfont/dist/material-design-icons.css";

// Utils
import constants from "./utils/constants";
import ui from "./utils/ui";
import tree from "./utils/tree";

// Vuetify a-la-carte
import {
  VApp,
  VCard,
  VCardTitle,
  VCardSubtitle,
  VCardText,
  VCardActions,
  VSpacer,
  VIcon,
  VBtn,
  VDivider,
  VExpandTransition,
  VChip,
  VBadge,
  VToolbar,
  VToolbarTitle,
  VAppBarNavIcon,
  VSelect,
  VSubheader,
  VListItemGroup,
  VTextField,
  VRow,
  VCol,
  VProgressCircular,
  VSlider,
  VCheckbox,
  VMenu,
  VTimePicker,
  VTimePickerClock,
  VPicker,
  VDialog,
  VRadioGroup,
  VRadio,
  VDatePicker
} from "vuetify/lib";

// Axios
import axios from "axios";

// Base options, to be merged with the provided ones
const baseOptions = {
  endpoint: "localhost:5000",
  useAmPmInTimePicker: false
};

export default {
  install(Vue, options) {
    options = { ...baseOptions, ...options };
    console.debug("Options:", options);

    Vue.component("NodePeek", NodePeek);
    Vue.component("NodeBlock", NodeBlock);
    Vue.component("AutomationsModuleRoot", Root);

    Vue.component("VApp", VApp);
    Vue.component("VCard", VCard);
    Vue.component("VCardTitle", VCardTitle);
    Vue.component("VCardSubtitle", VCardSubtitle);
    Vue.component("VCardText", VCardText);
    Vue.component("VCardActions", VCardActions);
    Vue.component("VSpacer", VSpacer);
    Vue.component("VIcon", VIcon);
    Vue.component("VBtn", VBtn);
    Vue.component("VDivider", VDivider);
    Vue.component("VExpandTransition", VExpandTransition);
    Vue.component("VChip", VChip);
    Vue.component("VBadge", VBadge);
    Vue.component("VToolbar", VToolbar);
    Vue.component("VToolbarTitle", VToolbarTitle);
    Vue.component("VAppBarNavIcon", VAppBarNavIcon);
    Vue.component("VSelect", VSelect);
    Vue.component("VSubheader", VSubheader);
    Vue.component("VListItemGroup", VListItemGroup);
    Vue.component("VTextField", VTextField);
    Vue.component("VRow", VRow);
    Vue.component("VCol", VCol);
    Vue.component("VProgressCircular", VProgressCircular);
    Vue.component("VSlider", VSlider);
    Vue.component("VCheckbox", VCheckbox);
    Vue.component("VMenu", VMenu);
    Vue.component("VTimePicker", VTimePicker);
    Vue.component("VTimePickerClock", VTimePickerClock);
    Vue.component("VDatePicker", VDatePicker);
    Vue.component("VPicker", VPicker);
    Vue.component("VDialog", VDialog);
    Vue.component("VRadioGroup", VRadioGroup);
    Vue.component("VRadio", VRadio);

    // Utils as prototypes
    Vue.prototype.$k = constants;
    Vue.prototype.$ui = ui;
    Vue.prototype.$tree = tree;

    // Axios prototype
    Vue.prototype.$http = axios;

    Vue.prototype.$automations = options;
  },
};

/* eslint-disable indent */
/* eslint-disable spaced-comment */
import Vue from 'vue';
import Router from 'vue-router';
const Empty = () => import('./views/Empty');


/***   FRONTEND   ***/

import Home from './views/Home.vue';
import Test from './views/Test.vue';
import Catalog from './views/Catalog.vue';

/***   END FRONTEND   ***/

/***   ADMIN   ***/

const AdminDashboard = () => import('./views/admin/Dashboard');
const AdminHome = () => import('./views/admin/Home');
const AdminLogin = () => import('./views/admin/Login');
const AdminLogout = () => import('./views/admin/Logout');

/***   CRM   ***/
const AdminRequestContact = () => import('./views/admin/crm/RequestContact');
const AdminRequestContactFormBuilder = () => import('./views/admin/crm/RequestContactFormBuilder');
const AdminNations = () => import('./views/admin/crm/Nations');
const AdminCrmCustomer = () => import('./views/admin/crm/Customer');
const adminCrmContact = () => import('./views/admin/crm/Contact');
const adminCrmVatType = () => import('./views/admin/crm/VatType');
const adminCrmPostalCode = () => import('./views/admin/crm/PostalCode');
const adminCrmUnitMeasure = () => import('./views/admin/crm/UnitMeasure');
const adminCrmStates = () => import('./views/admin/crm/States');
const adminCrmCategoryType = () => import('./views/admin/crm/CategoryType');
const adminCrmCategories = () => import('./views/admin/crm/Categories');
const adminCrmItems = () => import('./views/admin/crm/Items');
const adminCrmCategoriesPages = () => import('./views/admin/crm/categoriesPages');


const AdminTypologyKeywords = () => import('./views/admin/crm/TypologyKeywords');
const AdminListKeywords = () => import('./views/admin/crm/ListKeywords');

const adminCrmInterventions = () => import('./views/admin/crm/Interventions');
const adminCrmList = () => import('./views/admin/crm/List');
const adminTypeOptional = () => import('./views/admin/crm/TypeOptional');
const adminOptional = () => import('./views/admin/crm/Optional');
const adminSalesman = () => import('./views/admin/crm/Salesman');
const adminCarriers = () => import('./views/admin/crm/Carriers');
const adminAutomation = () => import('./views/admin/crm/Automation');
//const adminCrmFaultModules = () => import('./views/admin/crm/FaultModule');
const AdminCrmRegions = () => import('./views/admin/crm/Regions');
const AdminCrmProvinces = () => import('./views/admin/crm/Provinces');
const AdminCrmCities = () => import('./views/admin/crm/Cities');
const AdminCrmZone = () => import('./views/admin/crm/Zone');
const AdminCrmSupports = () => import('./views/admin/crm/Supports');
const adminCrmCarriage = () => import('./views/admin/crm/Carriage');
const adminCrmItemAttachmentType = () => import('./views/admin/crm/ItemAttachmentType');
const adminCrmItemAttachmentFile = () => import('./views/admin/crm/ItemAttachmentFile');
const adminCrmMappaCatalogo = () => import('./views/admin/crm/MappaCatalogo');
const AdminCrmPriceListGenerator = () => import('./views/admin/crm/pricelistgenerator');


/***   END CRM   ***/

/***   REGISTRIES   ***/
const adminCrmProducer = () => import('./views/admin/registries/Producer');
const adminCrmRoles = () => import('./views/admin/registries/Roles');
const adminCrmPeople = () => import('./views/admin/registries/People');
/***  END  REGISTRIES   ***/

/***  BOOKING ***/
const adminCrmPlaces = () => import('./views/admin/booking/Places');
const adminCrmServices = () => import('./views/admin/booking/Services');
const adminCrmBookingSettings = () => import('./views/admin/booking/Settings');
const adminCrmEmployees = () => import('./views/admin/booking/Employees');
const adminCrmExceptions = () => import('./views/admin/booking/Exceptions');
const adminCrmConnections = () => import('./views/admin/booking/Connections');
const adminCrmAppointments = () => import('./views/admin/booking/Appointments');
/***  END  BOOKING ***/

/***   ITEM   ***/
const AdminItemTechnicalSpecification = () => import('./views/admin/item/TechnicalSpecification');
/***   END ITEM   ***/

/***   ITEM SETTING   ***/
const AdminGroupsDisplayTechinicalSpecification = () => import('./views/admin/itemSetting/GroupsDisplayTechnicalsSpecifications');
const AdminGroupsTechinicalsSpecifications = () => import('./views/admin/itemSetting/GroupsTechnicalsSpecifications');
const AdminManagementsTechinicalsSpecifications = () => import('./views/admin/itemSetting/ManagementsTechinicalsSpecifications');
/***   END ITEM SETTING   ***/

/***   NEWSLETTER   ***/
//const AdminNewsletterCampaign = () => import('./views/admin/newsletter/Campaign');
const AdminNewsletterCampaign1 = () => import('./views/admin/newsletter/Campaign');
const AdminQueryGroupNewsletter = () => import('./views/admin/newsletter/querygroupnewsletter');
const AdminGroupNewsletter = () => import('./views/admin/newsletter/GroupNewsletter');
/***   END NEWSLETTER   ***/

/***   PICKING   ***/
const AdminPickingOCListSixten = () => import('./views/admin/picking/OCListSixten');
const AdminPickingReadingSixten = () => import('./views/admin/picking/ReadingSixten');
const AdminShippingListMdMicrodetectors = () => import('./views/admin/picking/ShippingListMdMicrodetectors');
const AdminShippingReadingMd = () => import('./views/admin/picking/ReadingMd');
/***   END PICKING   ***/

/***   SETTING   ***/
const AdminSettingConfiguration = () => import('./views/admin/setting/Configuration');
const AdminSettingJS = () => import('./views/admin/setting/JS');
const AdminSettingManageStyle = () => import('./views/admin/setting/ManageStyle');
const AdminSettingTemplateEditor = () => import('./views/admin/setting/TemplateEditor');
const AdminSettingTemplateEditorGroup = () => import('./views/admin/setting/TemplateEditorGroup');
const AdminSettingSelenaSqlView = () => import('./views/admin/setting/SelenaViews');
const AdminSettingLanguages = () => import('./views/admin/setting/Languages');
const AdminSettingUser = () => import('./views/admin/setting/User');
const AdminSettingMessage = () => import('./views/admin/setting/Message');
const AdminSettingMenuAdmin = () => import('./views/admin/setting/MenuAdminCustom');
const AdminImageResizeAdmin = () => import('./views/admin/setting/ImageResize');
const AdminRobotsSettingAdmin = () => import('./views/admin/setting/RobotsSetting');
const AdminFormBuilderSettingAdmin = () => import('./views/admin/setting/FormBuilder');
const AdminFormBuilderTypeSettingAdmin = () => import('./views/admin/setting/FormBuilderType');
const AdminCrmRedirect = () => import('./views/admin/setting/Redirect');
const AdminCrmSchedulatore = () => import('./views/admin/setting/Scheduler');
const AdminCrmFaq = () => import('./views/admin/setting/Faq');

/***   END SETTING   ***/

/*** SALES ***/
const AdminSalesVoucher = () => import('./views/admin/sales/Voucher');
const AdminVoucherType = () => import('./views/admin/sales/VoucherType');
const AdminCartRules = () => import('./views/admin/sales/CartRules');
const adminCrmDocumentStatuses = () => import('./views/admin/sales/DocumentStatuses');
const adminCrmSalesOrders = () => import('./views/admin/sales/SalesOrders');
const adminCrmSalesCarts = () => import('./views/admin/sales/SalesCarts');

/*** END SALES ***/

/*** COMMUNITY ***/
const AdminImageReportType = () => import('./views/admin/community/ImageReportType');
const AdminImageCategory = () => import('./views/admin/community/ImageCategory');
const AdminImage = () => import('./views/admin/community/Image');

/*** END COMMUNITY ***/

/*** ADVERTISING ***/
const AdminAdvertisingArea = () => import('./views/admin/advertising/AdvertisingArea');
const AdminAdvertisingBanner = () => import('./views/admin/advertising/AdvertisingBanner');
const AdminAmazonTemplate = () => import('./views/admin/advertising/AmazonTemplate');
/*** END ADVERTISING ***/

/***   SHIPMENT   ***/
const AdminShipmentSentSixten = () => import('./views/admin/shipment/SentSixten');
/***   END SHIPMENT   ***/

/***   SendSms   ***/
const AdminSms = () => import('./views/admin/sms/sendSms');
const AdminGroupSms = () => import('./views/admin/sms/GroupSms');
/***   END SendSms   ***/


/***   WEB PAGE   ***/
const AdminContents = () => import('./views/admin/webPage/Contents');
const AdminNews = () => import('./views/admin/webPage/News');
const AdminPages = () => import('./views/admin/webPage/Pages');
const AdminBlog = () => import('./views/admin/webPage/Blog');
/***   WEB PAGE   ***/

/***   END ADMIN   ***/
Vue.use(Router)

export default new Router({
  mode: 'history',
  linkActiveClass: 'open active',
  base: process.env.BASE_URL,
  routes: [{
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/test',
      name: 'test',
      component: Test
    },
    {
      path: '/catalogo/:id',
      name: 'catalog',
      component: Catalog
    },

    /***   ADMIN   ***/

    {
      path: '/admin/login',
      name: 'adminLogin',
      component: AdminLogin
    },
    {
      path: '/admin/logout',
      name: 'adminLogout',
      component: AdminLogout
    },
    {
      path: '/admin',
      component: AdminHome,
      children: [{
          path: '/',
          name: 'AdminDashboard',
          name: 'admin',
          component: AdminDashboard
        },

        /***   CRM   ***/

        {
          path: 'crm',
          name: 'adminCrm',
          component: Empty,
          children: [{
              path: 'requestcontact',
              name: 'adminRequestContact',
              component: AdminRequestContact
            },
            {
              path: 'requestcontactformbuilder',
              name: 'adminRequestContactFormBuilder',
              component: AdminRequestContactFormBuilder,
            },
            {
              path: 'nations',
              name: 'adminNations',
              component: AdminNations

            }, {
              path: 'customer',
              name: 'adminCrmCustomer',
              component: AdminCrmCustomer
            }, {
              path: 'vatType',
              name: 'adminCrmVatType',
              component: adminCrmVatType
            }, {
              path: 'postalCode',
              name: 'adminCrmPostalCode',
              component: adminCrmPostalCode
            }, {
              path: 'unitMeasure',
              name: 'adminCrmUnitMeasure',
              component: adminCrmUnitMeasure
            }, {
              path: 'categoryType',
              name: 'adminCrmCategoryType',
              component: adminCrmCategoryType
            }, {
              path: 'categories',
              name: 'adminCrmCategories',
              component: adminCrmCategories
            }, {
              path: 'items',
              name: 'adminCrmItems',
              component: adminCrmItems
            },
            {
              path: 'interventions',
              name: 'adminCrmInterventions',
              component: adminCrmInterventions
            }, {
              path: 'list',
              name: 'adminCrmList',
              component: adminCrmList
            },

            {
              path: 'States',
              name: 'adminCrmStates',
              component: adminCrmStates
            },
               {
              path: 'regions',
              name: 'AdminCrmRegions',
              component: AdminCrmRegions
            },
            {
              path: 'provinces',
              name: 'AdminCrmProvinces',
              component: AdminCrmProvinces
            },
            {
              path: 'cities',
              name: 'AdminCrmCities',
              component: AdminCrmCities
            },
             {
              path: 'zone',
              name: 'AdminCrmZone',
              component: AdminCrmZone
            },
            {
              path: 'supports',
              name: 'AdminCrmSupports',
              component: AdminCrmSupports
            },
            {
              path: 'pricelistgenerator',
              name: 'AdminCrmPriceListGenerator',
              component: AdminCrmPriceListGenerator
            },

            /*{
              path: 'FaultModule',
              name: 'adminCrmFaultModules',
              component: adminCrmFaultModules
            }, */
            {
              path: 'categoriespages',
              name: 'adminCrmCategoriesPages',
              component: adminCrmCategoriesPages
            },
            {
              path: 'typologykeywords',
              name: 'adminTypologyKeywords',
              component: AdminTypologyKeywords
            },
            {
              path: 'listkeywords',
              name: 'adminListKeywords',
              component: AdminListKeywords
            },
            {
              path: 'contact',
              name: 'adminCrmContact',
              component: adminCrmContact
            },
            {
              path: 'typeoptional',
              name: 'adminTypeOptional',
              component: adminTypeOptional
            },
            {
              path: 'optional',
              name: 'adminOptional',
              component: adminOptional
            },
            {
              path: 'salesman',
              name: 'adminSalesman',
              component: adminSalesman
            },
            {
              path: 'carriers',
              name: 'adminCarriers',
              component: adminCarriers
            },
            {
              path: 'carriage',
              name: 'adminCrmCarriage',
              component: adminCrmCarriage
            },
            {
              path: 'itemattachmenttype',
              name: 'adminCrmItemAttachmentType',
              component: adminCrmItemAttachmentType
            },
            {
              path: 'itemattachmentfile',
              name: 'adminCrmItemAttachmentFile',
              component: adminCrmItemAttachmentFile
            },
            {
              path: 'mappacatalogo',
              name: 'adminCrmMappaCatalogo',
              component: adminCrmMappaCatalogo
            },            
            {
              path: 'automation',
              name: 'adminAutomation',
              component: adminAutomation
            }            
          ]
        },

        /***   END CRM   ***/

        /***   ITEM   ***/

        {
          path: 'techinicalSpecification',
          name: 'adminItemTechnicalSpecification',
          component: AdminItemTechnicalSpecification
        },

        /***   END ITEM   ***/

        /***   ITEM SETTING   ***/

        {
          path: 'groupsDisplayTechinicalSpecification',
          name: 'adminGroupsDisplayTechinicalSpecification',
          component: AdminGroupsDisplayTechinicalSpecification
        },
        {
          path: 'groupsTechinicalSpecification',
          name: 'adminGroupsTechinicalsSpecifications',
          component: AdminGroupsTechinicalsSpecifications
        },
        {
          path: 'managementTechinicalSpecification',
          name: 'adminManagementTechinicalSpecification',
          component: AdminManagementsTechinicalsSpecifications
        },

        /***   REGISTRIES   ***/
        {
          path: 'registries',
          name: 'Producer',
          component: Empty,
          children: [{
            path: 'Producer',
            name: 'adminCrmProducer',
            component: adminCrmProducer
          }, {
            path: 'People',
            name: 'adminCrmPeople',
            component: adminCrmPeople
          }, {
            path: 'Roles',
            name: 'adminCrmRoles',
            component: adminCrmRoles
          }]
        },

      /***   END REGISTRIES   ***/
        
  
        /***   END ITEM SETTING   ***/

        /***   NEWSLETTER   ***/
        {
          path: 'newsletter',
          name: 'adminNewletter',
          component: Empty,
          children: [{
            path: 'campaign',
            name: 'adminNewsletterCampaign1',
            component: AdminNewsletterCampaign1
          },{
            path: 'automaticgroups',
            name: 'adminQueryGroupNewsletter',
            component: AdminQueryGroupNewsletter
          },
            {
            path: 'groups',
            name: 'adminGroupNewsletter',
            component: AdminGroupNewsletter
          }]
        },
        /***   END NEWSLETTER   ***/

          /***   SALES   ***/
          {
            path: 'sales',
            name: 'adminSales',
            component: Empty,
            children: [{
              path: 'voucher',
              name: 'adminSalesVoucher',
              component: AdminSalesVoucher
            },
            {
              path: 'voucherType',
              name: 'adminVoucherType',
              component: AdminVoucherType
              },
            {
              path: 'statesdocuments',
              name: 'adminCrmDocumentStatuses',
              component: adminCrmDocumentStatuses
              },
            {
              path: 'salesorders',
              name: 'adminCrmSalesOrders',
              component: adminCrmSalesOrders
              },
              {
                path: 'salescarts',
                name: 'adminCrmSalesCarts',
                component: adminCrmSalesCarts
                },
              {
              path: 'cartRules',
              name: 'adminCartRules',
              component: AdminCartRules
            }
          ]
          },
  
      /***   END SALES   ***/
          
  
        /***   PICKING   ***/

        {
          path: 'picking',
          name: 'adminPicking',
          component: Empty,
          children: [{
            path: 'OClist',
            name: 'adminPickingOCListSixten',
            component: AdminPickingOCListSixten
          }, {
            path: 'oclist/:id',
            name: 'adminPickingOCListSixtenId',
            component: AdminPickingOCListSixten
          }, {
            path: 'reading',
            name: 'adminPickingReadingSixtenSixten',
            component: AdminPickingReadingSixten
          }, {
            path: 'reading/:id',
            name: 'adminPickingReadingSixtenSixtenId',
            component: AdminPickingReadingSixten
          }, {
            path: 'Shipping',
            name: 'adminShippingListMdMicrodetectors',
            component: AdminShippingListMdMicrodetectors
          }, {
            path: 'shipping/:id',
            name: 'adminShippingListMdMicrodetectorsId',
            component: AdminShippingListMdMicrodetectors
          }, {
            path: 'readingmd',
            name: 'adminShippingReadingMd',
            component: AdminShippingReadingMd
          }],
        },

        /***   END PICKING   ***/

        /***   SETTING   ***/

        {
          path: 'setting',
          name: 'adminSetting',
          component: Empty,
          children: [{
              path: 'configuration',
              name: 'adminSettingConfiguration',
              component: AdminSettingConfiguration
            },
            {
              path: 'JS',
              name: 'adminSettingJS',
              component: AdminSettingJS
            }, {
              path: 'manageStyle',
              name: 'adminSettingManageStyle',
              component: AdminSettingManageStyle
            }, {
              path: 'templateEditor',
              name: 'adminSettingTemplateEditor',
              component: AdminSettingTemplateEditor
            }, {
              path: 'templateEditorGroup',
              name: 'adminSettingTemplateEditorGroup',
              component: AdminSettingTemplateEditorGroup
            }, {
              path: 'user',
              name: 'adminSettingUser',
              component: AdminSettingUser
            }, {
              path: 'selenaViews',
              name: 'adminSettingSelenaSqlView',
              component: AdminSettingSelenaSqlView
            }, {
              path: 'languages',
              name: 'adminSettingLanguages',
              component: AdminSettingLanguages
            }, {
              path: 'messages',
              name: 'adminSettingMessage',
              component: AdminSettingMessage
            }, {
              path: 'menuadmincustom',
              name: 'adminSettingMenuAdmin',
              component: AdminSettingMenuAdmin
            }, {
              path: 'imageresize',
              name: 'adminImageResizeAdmin',
              component: AdminImageResizeAdmin
            },
            {
              path: 'robotssetting',
              name: 'adminRobotsSettingAdmin',
              component: AdminRobotsSettingAdmin
            },
            {
              path: 'redirecturl',
              name: 'adminCrmRedirect',
              component: AdminCrmRedirect
            },
            {
              path: 'faq',
              name: 'adminCrmFaq',
              component: AdminCrmFaq
            },
            {
              path: 'scheduler',
              name: 'adminCrmSchedulatore',
              component: AdminCrmSchedulatore
            },
            {
              path: 'formbuilder',
              name: 'adminFormBuilderAdmin',
              component: AdminFormBuilderSettingAdmin
            },
            {
              path: 'formbuildertype',
              name: 'adminFormBuilderTypeAdmin',
              component: AdminFormBuilderTypeSettingAdmin
            },
          ],
        },
        /***   END SETTING   ***/

        /***   SHIPMENT   ***/
        {
          path: 'shipment',
          name: 'adminShipment',
          component: Empty,
          children: [{
            path: 'sentSixten',
            name: 'adminShipmentSentSixten',
            component: AdminShipmentSentSixten
          }],
        },

      /***   END SHIPMENT   ***/
        

        /***   SMS   ***/
        {
          path: 'sms',
          name: 'adminSendSms',
          component: Empty,
          children: [{
            path: 'sendSms',
            name: 'AdminSms',
            component: AdminSms
          },{
            path: 'GroupSms',
            name: 'AdminGroupSms',
            component: AdminGroupSms
          }],
        },
      /***   END SMS   ***/  
 

        /***   COMMUNITY   ***/

        {
          path: 'community',
          name: 'adminCommunity',
          component: Empty,
          children: [{
            path: 'imageReportType',
            name: 'adminImageReportType',
            component: AdminImageReportType
          },
          {
            path: 'imageCategory',
            name: 'adminImageCategory',
            component: AdminImageCategory
          },
          {
            path: 'images',
            name: 'adminImage',
            component: AdminImage
          }
        ]
        },

        /***   END COMMUNITY   ***/

 

        /***   ADVERTISING   ***/

        {
          path: 'advertising',
          name: 'adminAdvertising',
          component: Empty,
          children: [{
            path: 'advertisingArea',
            name: 'adminAdvertisingArea',
            component: AdminAdvertisingArea
          },
          {
            path: 'advertisingBanner',
            name: 'adminAdvertisingBanner',
            component: AdminAdvertisingBanner
          },
          {
            path: 'amazonTemplate',
            name: 'adminAmazonTemplate',
            component: AdminAmazonTemplate
          }
        ]
        },
      /***   END ADVERTISING   ***/
        
        /***   BOOKING   ***/
        {
          path: 'booking',
          name: 'adminPlaces',
          component: Empty,
          children: [{
            path: 'place',
            name: 'adminCrmPlaces',
            component: adminCrmPlaces
          },
            {
            path: 'employee',
            name: 'adminCrmEmployees',
            component: adminCrmEmployees
            },
            {
              path: 'connections',
              name: 'adminCrmConnections',
              component: adminCrmConnections
            },
            {
            path: 'bookingsettings',
            name: 'adminCrmBookingSettings',
            component: adminCrmBookingSettings
            },
            {
            path: 'exceptions',
            name: 'adminCrmExceptions',
            component: adminCrmExceptions
            },
            {
              path: 'Appointments',
              name: 'adminCrmAppointments',
              component: adminCrmAppointments
              },
          {
            path: 'services',
            name: 'adminCrmServices',
            component: adminCrmServices
          }
        ]
      },
      /***   END BOOKING   ***/  
        

        /***   WEB PAGE   ***/

        {
          path: 'contents',
          name: 'adminContents',
          component: AdminContents
        },
        {
          path: 'news',
          name: 'adminNews',
          component: AdminNews
        },
        {
          path: 'blog',
          name: 'adminBlog',
          component: AdminBlog
        },
        {
          path: 'pages',
          name: 'adminPages',
          component: AdminPages
        },

        /***   END WEB PAGE   ***/

        {
          path: '*',
          name: 'genericAdmin'
        }
      ]
    },

    /***   END ADMIN   ***/

    {
      path: '*',
      name: 'generic',
      component: Home
    }
  ]
})

/***   configData   ***/

/*** STATIC ***/

/***   DEV   ***/
export const customer = "Selena";
export const wsRootServicesUrl = "http://localhost";

//***   SELENA   ***/
// export const customer = "Selena";
// export const wsRootServicesUrl = "https://selena.tecnotrade.com/api";

/***   BORMAC   ***/
// export const customer = "Bormac";
// export const wsRootServicesUrl = "http://192.168.0.54:8080"; // Production

/***   SIXTEN   ***/
// export const customer = "Sixten";
// export const wsRootServicesUrl = "http://10.0.1.52:588"; //Sixten dev
// export const wsRootServicesUrl = "https://sede.tecnotrade.com:5543"; //Sixten release
// export const wsRootServicesUrl = "https://192.168.2.186:8443"; // Production

/*** END STATIC ***/

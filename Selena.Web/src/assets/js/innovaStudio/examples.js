
innovaExamples = {
    data_examples: function () {
        return {
            'designs': [

                {
                    'thumbnail': 'preview/header-01.png',
                    'category': '101',
                    'contentCss': 'type-fugazone-librefranklin.css',
                    'contentClass': 'type-fugazone-librefranklin',
                    id: 36,
                    'html':
                        '<div class="is-section is-section-100 is-box is-light-text is-align-right type-fugazone-librefranklin">' +
                        innovaBasic._tabs(1) + '<div class="is-overlay">' +
                        innovaBasic._tabs(2) + '<div class="is-overlay-bg" style="background-image: url(\'[%IMAGE_PATH%]images/QGojv23425.jpg\');"></div>' +
                        innovaBasic._tabs(2) + '<div class="is-overlay-color" style="opacity: 0.1;"></div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        innovaBasic._tabs(1) + '<div class="is-boxes">' +
                        innovaBasic._tabs(2) + '<div class="is-box-centered is-opacity-80">' +
                        innovaBasic._tabs(3) + '<div class="is-container is-builder container is-content-right is-content-640">' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full">' +
                        innovaBasic._tabs(6) + '<div class="display">' +
                        innovaBasic._tabs(7) + '<h1>A New Point of View</h1>' +
                        innovaBasic._tabs(7) + '<p>Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>' +
                        innovaBasic._tabs(6) + '</div>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full">' +
                        innovaBasic._tabs(6) + '<a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 is-btn-small edit">Contact Us</a>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(3) + '</div>' +
                        innovaBasic._tabs(2) + '</div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        '\n</div>'
                },

                {
                    'thumbnail': 'preview/header-02.png',
                    'category': '101',
                    'contentCss': 'type-quicksand-kalam.css',
                    'contentClass': 'type-quicksand-kalam',
                    id: 37,
                    'html':
                        '<div class="is-section is-section-100 is-box is-light-text type-quicksand-kalam">' +
                        innovaBasic._tabs(1) + '<div class="is-overlay">' +
                        innovaBasic._tabs(2) + '<div class="is-overlay-bg" style="background-image: url(\'[%IMAGE_PATH%]images/2lgyE21187.jpg\');"></div>' +
                        innovaBasic._tabs(2) + '<div class="is-overlay-color" style="opacity: 0.1;"></div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        innovaBasic._tabs(1) + '<div class="is-boxes">' +
                        innovaBasic._tabs(2) + '<div class="is-box-centered is-opacity-95">' +
                        innovaBasic._tabs(3) + '<div class="is-container is-builder container is-content-500">' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full center">' +
                        innovaBasic._tabs(6) + '<h1 class="size-48 is-title4-48" style="letter-spacing:5px">STUNNING</h1>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(3) + '</div>' +
                        innovaBasic._tabs(2) + '</div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        '\n</div>'
                },

                {
                    'thumbnail': 'preview/header-03.png',
                    'category': '101',
                    'contentCss': 'type-adventpro-opensans.css',
                    'contentClass': 'type-adventpro-opensans',
                    id: 38,
                    'html':
                        '<div class="is-section is-section-100 is-box is-dark-text type-adventpro-opensans">' +
                        innovaBasic._tabs(1) + '<div class="is-overlay">' +
                        innovaBasic._tabs(2) + '<div class="is-overlay-bg" style="background-image: url(\'[%IMAGE_PATH%]images/ewpaI23916.jpg\');"></div>' +
                        innovaBasic._tabs(2) + '<div class="is-overlay-color" style="opacity: 0.1;"></div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        innovaBasic._tabs(1) + '<div class="is-boxes">' +
                        innovaBasic._tabs(2) + '<div class="is-box-centered">' +
                        innovaBasic._tabs(3) + '<div class="is-container is-builder container is-content-left is-content-640">' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full">' +
                        innovaBasic._tabs(6) + '<h1 class="size-64">Mark\'s Photo Studio</h1>' +
                        innovaBasic._tabs(6) + '<p style="font-weight: 300">Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full">' +
                        innovaBasic._tabs(6) + '<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Read More</a>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(3) + '</div>' +
                        innovaBasic._tabs(2) + '</div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        '\n</div>'
                },

                {
                    'thumbnail': 'preview/header-04.png',
                    'category': '101',
                    'contentCss': 'type-macondo-mukta.css',
                    'contentClass': 'type-macondo-mukta',
                    id: 39,
                    'html':
                        '<div class="is-section is-section-100 is-box is-light-text type-macondo-mukta">' +
                        innovaBasic._tabs(1) + '<div class="is-overlay">' +
                        innovaBasic._tabs(2) + '<div class="is-overlay-bg" style="background-image: url(\'[%IMAGE_PATH%]images/Uridi01739.jpg\');"></div>' +
                        innovaBasic._tabs(2) + '<div class="is-overlay-color" style="opacity: 0.1;"></div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        innovaBasic._tabs(1) + '<div class="is-boxes">' +
                        innovaBasic._tabs(2) + '<div class="is-box-centered">' +
                        innovaBasic._tabs(3) + '<div class="is-container is-builder container is-content-640 is-content-left">' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full">' +
                        innovaBasic._tabs(6) + '<h1 class="size-64 is-title1-64 is-title-lite">Dream. Explore. Discover.</h1>' +
                        innovaBasic._tabs(6) + '<p class="size-24">Lorem Ipsum is simply dummy text of the printing industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.</p>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full">' +
                        innovaBasic._tabs(6) + '<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Travel Packages</a>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(3) + '</div>' +
                        innovaBasic._tabs(2) + '</div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        '\n</div>'
                },

                {
                    'thumbnail': 'preview/header-05.png',
                    'category': '101',
                    'contentCss': 'type-forum-nanumgothiccoding.css',
                    'contentClass': 'type-forum-nanumgothiccoding',
                    id: 40,
                    'html':
                        '<div class="is-section is-section-100 is-box is-dark-text type-forum-nanumgothiccoding">' +
                        innovaBasic._tabs(1) + '<div class="is-overlay">' +
                        innovaBasic._tabs(2) + '<div class="is-overlay-bg" style="background-image: url(\'[%IMAGE_PATH%]images/H5etB35240.jpg\');"></div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        innovaBasic._tabs(1) + '<div class="is-boxes">' +
                        innovaBasic._tabs(2) + '<div class="is-box-centered edge-y-5 is-content-top">' +
                        innovaBasic._tabs(3) + '<div class="is-container is-builder is-content-800 container">' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full center">' +
                        innovaBasic._tabs(6) + '<div class="display">' +
                        innovaBasic._tabs(7) + '<h1 class="size-68">FUNCTIONAL, CLEAR & CLEAN</h1>' +
                        innovaBasic._tabs(7) + '<p>Lorem Ipsum is dummy text of the printing industry</p>' +
                        innovaBasic._tabs(6) + '</div>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full center">' +
                        innovaBasic._tabs(6) + '<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Get A Quote</a>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(3) + '</div>' +
                        innovaBasic._tabs(2) + '</div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        '\n</div>'
                },

                {
                    'thumbnail': 'preview/header-06.png',
                    'category': '101',
                    'contentCss': 'type-spectralsc-karma.css',
                    'contentClass': 'type-spectralsc-karma',
                    id: 41,
                    'html':
                        '<div class="is-section is-section-100 is-box is-light-text type-spectralsc-karma">' +
                        innovaBasic._tabs(1) + '<div class="is-overlay">' +
                        innovaBasic._tabs(2) + '<div class="is-overlay-bg" style="background-image: url(\'[%IMAGE_PATH%]images/Jau3220598.jpg\');"></div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        innovaBasic._tabs(1) + '<div class="is-boxes">' +
                        innovaBasic._tabs(2) + '<div class="is-box-centered is-content-top is-opacity-95">' +
                        innovaBasic._tabs(3) + '<div class="is-container is-builder container is-content-980">' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full center">' +
                        innovaBasic._tabs(6) + '<div class="display">' +
                        innovaBasic._tabs(7) + '<h1>Modern Home Decor</h1>' +
                        innovaBasic._tabs(7) + '<p>Create a Stylish Space With Our Products</p>' +
                        innovaBasic._tabs(6) + '</div>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full center">' +
                        innovaBasic._tabs(6) + '<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Browse Collection</a>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(3) + '</div>' +
                        innovaBasic._tabs(2) + '</div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        '\n</div>'
                },

                {
                    'thumbnail': 'preview/header-07.png',
                    'category': '101',
                    'contentCss': 'type-tenaram-overpassmono.css',
                    'contentClass': 'type-tenaram-overpassmono',
                    id: 42,
                    'html':
                        '<div class="is-section is-section-100 is-box is-dark-text type-tenaram-overpassmono">' +
                        innovaBasic._tabs(1) + '<div class="is-overlay">' +
                        innovaBasic._tabs(2) + '<div class="is-overlay-bg" style="background-image: url(\'[%IMAGE_PATH%]images/0YgyL34026.jpg\');"></div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        innovaBasic._tabs(1) + '<div class="is-boxes">' +
                        innovaBasic._tabs(2) + '<div class="is-box-centered is-content-top">' +
                        innovaBasic._tabs(3) + '<div class="is-container is-builder is-content-800 container">' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full center">' +
                        innovaBasic._tabs(6) + '<div class="display">' +
                        innovaBasic._tabs(7) + '<h1 class="size-108" style="margin-bottom:0;">Outstanding</h1>' +
                        innovaBasic._tabs(7) + '<p style="margin-top:0">Lorem Ipsum is dummy text of the printing and typesetting industry</p>' +
                        innovaBasic._tabs(6) + '</div>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full center">' +
                        innovaBasic._tabs(6) + '<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">Contact Us</a>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(3) + '</div>' +
                        innovaBasic._tabs(2) + '</div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        '\n</div>'
                },

                {
                    'thumbnail': 'preview/header-08.png',
                    'category': '101',
                    'contentCss': 'type-pacifico-nanumgothiccod.css',
                    'contentClass': 'type-pacifico-nanumgothiccod',
                    id: 43,
                    'html':
                        '<div class="is-section is-box is-light-text is-section-100 type-pacifico-nanumgothiccod">' +
                        innovaBasic._tabs(1) + '<div class="is-overlay">' +
                        innovaBasic._tabs(2) + '<div class="is-overlay-bg" style="background-image: url(\'[%IMAGE_PATH%]images/uoyts22105.jpg\');"></div>' +
                        innovaBasic._tabs(2) + '<div class="is-overlay-color" style="opacity: 0.25;"></div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        innovaBasic._tabs(1) + '<div class="is-boxes">' +
                        innovaBasic._tabs(2) + '<div class="is-box-centered">' +
                        innovaBasic._tabs(3) + '<div class="is-container is-builder container">' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full center">' +
                        innovaBasic._tabs(6) + '<i class="icon ion-coffee size-80" style="line-height:1"></i>' +
                        innovaBasic._tabs(6) + '<div class="display">' +
                        innovaBasic._tabs(7) + '<h1 class="size-80">Café & Bistro</h1>' +
                        innovaBasic._tabs(6) + '</div>' +
                        innovaBasic._tabs(6) + '<p><i>Lorem Ipsum is simply dummy text</i></p>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(3) + '</div>' +
                        innovaBasic._tabs(2) + '</div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        '\n</div>'
                },

                {
                    'thumbnail': 'preview/header-09.png',
                    'category': '101',
                    'contentCss': 'type-amaticsc-josepfinsans.css',
                    'contentClass': 'type-amaticsc-josepfinsans',
                    id: 44,
                    'html':
                        '<div class="is-section is-section-100 is-box is-dark-text type-amaticsc-josepfinsans">' +
                        innovaBasic._tabs(1) + '<div class="is-overlay">' +
                        innovaBasic._tabs(2) + '<div class="is-overlay-bg" style="background-image: url(\'[%IMAGE_PATH%]images/knrny205553.jpg\');"></div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        innovaBasic._tabs(1) + '<div class="is-boxes">' +
                        innovaBasic._tabs(2) + '<div class="is-box-centered is-content-top is-opacity-95">' +
                        innovaBasic._tabs(3) + '<div class="is-container is-builder is-content-800 container">' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full center">' +
                        innovaBasic._tabs(6) + '<div class="display">' +
                        innovaBasic._tabs(7) + '<h1>Calm, Pure, and lovely</h1>' +
                        innovaBasic._tabs(7) + '<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s</p>' +
                        innovaBasic._tabs(6) + '</div>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full center">' +
                        innovaBasic._tabs(6) + '<a href="#" class="is-btn is-btn-ghost1 is-upper is-rounded-30 is-btn-small edit">Shop Now</a>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(3) + '</div>' +
                        innovaBasic._tabs(2) + '</div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        '\n</div>'
                },

                {
                    'thumbnail': 'preview/header-10.png',
                    'category': '101',
                    'contentCss': 'type-frankruhllibre-lato.css',
                    'contentClass': 'type-frankruhllibre-lato',
                    id: 45,
                    'html':
                        '<div class="is-section is-section-100 is-box is-light-text type-frankruhllibre-lato">' +
                        innovaBasic._tabs(1) + '<div class="is-overlay">' +
                        innovaBasic._tabs(2) + '<div class="is-overlay-bg" style="background-image: url(\'[%IMAGE_PATH%]images/dNkJx2105.jpg\');"></div>' +
                        innovaBasic._tabs(2) + '<div class="is-overlay-color" style="opacity: 0.25;"></div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        innovaBasic._tabs(1) + '<div class="is-boxes">' +
                        innovaBasic._tabs(2) + '<div class="is-box-centered edge-y-4 is-opacity-85">' +
                        innovaBasic._tabs(3) + '<div class="is-container is-builder container is-content-640 is-content-left">' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full">' +
                        innovaBasic._tabs(6) + '<div class="display">' +
                        innovaBasic._tabs(7) + '<h1>It suits you perfectly</h1>' +
                        innovaBasic._tabs(6) + '</div>' +
                        innovaBasic._tabs(6) + '<p style="text-transform:uppercase">Lorem Ipsum is dummy text of the printing and typesetting industry</p>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(4) + '<div class="row clearfix">' +
                        innovaBasic._tabs(5) + '<div class="column full">' +
                        innovaBasic._tabs(6) + '<a href="#" class="is-btn is-btn-ghost1 is-upper is-btn-small edit">View Gallery</a>' +
                        innovaBasic._tabs(5) + '</div>' +
                        innovaBasic._tabs(4) + '</div>' +
                        innovaBasic._tabs(3) + '</div>' +
                        innovaBasic._tabs(2) + '</div>' +
                        innovaBasic._tabs(1) + '</div>' +
                        '\n</div>'
                }
            ]
        };
    }
}
/***   interventionHelpers   ***/
export function getLastId() {
  InterventionsServices.getLastId(function(response) {
    $("#id").val(response.data);
  });
}

export function getLastIdLib() {
  InterventionsServices.getLastIdLib(function(response) {
    $("#code_intervention_gr").val(response.data);
  });
}


export function getdescription(id) {
  InterventionsServices.getdescription(id, function(response) {
    $("#frmUpdateIntervention #description").val(response.data.tipology + ' ' + response.data.note + ' ' + response.data.plant);
  });
}

export function getdescriptionforminsert(id) {
  InterventionsServices.getdescription(id, function(response) {
    $("#frmAddIntervention #description").val(response.data.tipology + ' ' + response.data.note + ' ' + response.data.plant);
  });
}

export function getnumbertelephone(id) {
  InterventionsServices.getnumbertelephone(id, function(response) {
    $("#intervention #phone_number").val(response.data.phone_number);
    $("#ViewFaultModule #phone_number").val(response.data.phone_number);
  });
}

export function insertModule() {
  spinnerHelpers.show();

var create_quote = false;
  if ($("#intervention #create_quote").is(":checked")) {
    create_quote = true;
  } else {
    create_quote = false;
  }
var data = {
    customer_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    imgmodule: $("#intervention #imgBase64").val(),
    imgName: $("#intervention #imgName").val(),
    imgNames: $("#intervention #imgNames").val(),
    external_referent_id: $("#intervention #referent").val(),
    nr_ddt: $("#intervention #nr_ddt").val(),
    date_ddt: $("#intervention #date_ddt").val(),
    series: $("#intervention #series").val(),
    trolley_type: $("#intervention #trolley_type").val(),
    plant_type: $("#intervention #plant_type").val(),
    defect: $("#intervention #defect").val(),
    voltage: $("#intervention #voltage").val(),
    exit_notes: $("#intervention #exit_notes").val(),
    gridCheck: $("#intervention #gridCheck").val(),
    fileddt: $("#intervention #imgBase64").val(), 
    create_quote: create_quote,
};
    //var file_data = $('#imgmodule').prop('files')[0];
    var file_data = $('#fileddt').prop('files')[0];

    InterventionsServices.insertModule(data, function() {
    $("#intervention #referent").val("");
    $("#intervention #imgBase64").val("");
    $("#intervention #nr_ddt").val("");
    $("#intervention #date_ddt").val("");
    $("#intervention #series").val("");
    $("#intervention #trolley_type").val("");
    $("#intervention #plant_type").val("");
    $("#intervention #defect").val("");
    $("#intervention #voltage").val("");
    $("#intervention #exit_notes").val("");
    $("#intervention #gridCheck").val(""); 
    $("#intervention #create_quote").val("");
    notificationHelpers.success("Inserimento avvenuto correttamente!");
    spinnerHelpers.hide();
    window.location.reload();
    window.location.href = 'https://grsrl.net/pannello-cliente';
  });
}

export function insert() {
  spinnerHelpers.show();

  var unrepairable = false;
  if ($("#frmAddIntervention #unrepairable").is(":checked")) {
    unrepairable = true;
  } else {
    unrepairable = false;
  }

  var create_quote = false;
  if ($("#frmAddIntervention #create_quote").is(":checked")) {
    create_quote = true;
  } else {
    create_quote = false;
  }

  var send_to_customer = false;
  if ($("#frmAddIntervention #send_to_customer").is(":checked")) {
    send_to_customer = true;
  } else {
    send_to_customer = false;
  }

  var data = {
    customer_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    // ADD frmAddIntervention
    id: $("#frmAddIntervention #id").val(),
    imgmodule: $("#frmAddIntervention #imgBase64").val(),
    imgName: $("#frmAddIntervention #imgName").val(),
    referent: $("#frmAddIntervention #referent").val(),
    nr_ddt: $("#frmAddIntervention #nr_ddt").val(),
    date_ddt: $("#frmAddIntervention #date_ddt").val(),
    series: $("#frmAddIntervention #series").val(),
    trolley_type: $("#frmAddIntervention #trolley_type").val(),
    code_intervention_gr: $("#frmAddIntervention #code_intervention_gr").val(),
    plant_type: $("#frmAddIntervention #plant_type").val(),
    defect: $("#frmAddIntervention #defect").val(),
    voltage: $("#frmAddIntervention #voltage").val(),
    exit_notes: $("#frmAddIntervention #exit_notes").val(),
    gridCheck: $("#frmAddIntervention #gridCheck").val(),
    states_id: $("#frmAddIntervention #states_id").val(),
    nr_ddt_gr: $("#frmAddIntervention #nr_ddt_gr").val(),
    date_ddt_gr: $("#frmAddIntervention #date_ddt_gr").val(),
    note_internal_gr: $("#frmAddIntervention #note_internal_gr").val(),
    user_id: $("#frmAddIntervention #user_new_id").val(),
    items_id: $("#frmAddIntervention #items_id").val(),
    description: $("#frmAddIntervention #description").val(),
    code_tracking: $("#frmAddIntervention #code_tracking").val(),
    rip_association: $("#frmAddIntervention #rip_association").val(),
    external_referent_id_intervention: $("#frmAddIntervention #external_referent_id_intervention").val(),
    unrepairable: unrepairable,
    create_quote: create_quote,
    send_to_customer: send_to_customer
  };


  var file_data = $('#imgmodule').prop('files')[0];

  var arrayImgAgg = [];
  for (var i = 0; i < 4; i++) {
    if (functionHelpers.isValued($("#imgBase64" + i).val())) {
      arrayImgAgg.push($("#imgBase64" + i).val());
    }
  }
  data.captures = arrayImgAgg;

  InterventionsServices.insert(data, function() {
    //ADD frmAddIntervention
    $("#frmAddIntervention #id").val("");
    $("#frmAddIntervention #referent").val("");
    $("#frmAddIntervention #code_intervention_gr").val("");
    $("#frmAddIntervention #imgBase64").val("");
    $("#frmAddIntervention #nr_ddt").val("");
    $("#frmAddIntervention #date_ddt").val("");
    $("#frmAddIntervention #series").val("");
    $("#frmAddIntervention #trolley_type").val("");
    $("#frmAddIntervention #plant_type").val("");
    $("#frmAddIntervention #defect").val("");
    $("#frmAddIntervention #voltage").val("");
    $("#frmAddIntervention #exit_notes").val("");
    $("#frmAddIntervention #gridCheck").val("");
    $("#frmAddIntervention #states_id").val("");
    $("#frmAddIntervention #nr_ddt_gr").val("");
    $("#frmAddIntervention #date_ddt_gr").val("");
    $("#frmAddIntervention #user_new_id").val("");
    $('#frmAddIntervention #note_internal_gr').val('');
    $("#frmAddIntervention #items_id").val("");
    $("#frmAddIntervention #states_id").val("");
    $("#frmAddIntervention #description").val("");
    $("#frmAddIntervention #unrepairable").val("");
    $("#frmAddIntervention #send_to_customer").val("");
    $("#frmAddIntervention #create_quote").val("");
    $("#frmAddIntervention #code_tracking").val("");
    $("#frmAddIntervention #rip_association").val("");
    notificationHelpers.success("Inserimento avvenuto correttamente!");
    spinnerHelpers.hide();
    window.location.reload();
    window.location.href = 'https://grsrl.net/gestione-materiali';
  });
}

export function updateCheckRepairArrived(id) {
  spinnerHelpers.show();
  var data = {
    id:id 
  };
InterventionsServices.updateCheckRepairArrived(data, function() {
    $("#table_list_repair_arriving #repaired_arrived").val("");
  notificationHelpers.success("Modifica avvenuta con successo");
  window.location.reload();
    spinnerHelpers.hide();
    $("#table_list_repair_arriving").show();
  });
}

export function getAllRepairArriving() {
  spinnerHelpers.show();
  InterventionsServices.getAllRepairArriving(storageData.sIdLanguage(), function (response) {

    for (var i = 0; i < response.data.length; i++) {
      var newList = "";
      newList += "<tr>"; 
       newList +=
        '<td><button class="btn btn-info" onclick="interventionHelpers.getById(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil"/></button></td>';
        if (response.data[i].repaired_arrived == true) {
      newList +=
     '<td><input type="checkbox"  id="form-control ready' + i + '" value="' + response.data[i].repaired_arrived +
     '" name="ready' + i + '" class="switch-input' + i + '" checked="true' + i + '" onclick="interventionHelpers.updateCheckRepairArrived(' +
        response.data[i].id +
        ')"></td>'; 
      } else{
      newList +=
     '<td><input type="checkbox" id="form-control ready' + i + '" value="' + response.data[i].repaired_arrived +
       '" name="ready' + i + '" class="switch-input' + i + '" onclick="interventionHelpers.updateCheckRepairArrived(' +
        response.data[i].id +
        ')"></td>'; 
      }
      newList += "<td>" + response.data[i].id + "</td>";
      newList += "<td>" + response.data[i].user_id + "</td>";
      newList += "<td>" + response.data[i].date_ddt + "</td>";
      newList += "<td>" + response.data[i].nr_ddt + "</td>";
      newList += "<td>" + response.data[i].series + "</td>";
      newList += "<td>" + response.data[i].plant_type + "</td>";
      newList += "<td>" + response.data[i].trolley_type + "</td>";
      newList += "<td>" + response.data[i].defect + "</td>";
      newList += "<td>" + response.data[i].voltage + "</td>";
      newList += "<td>" + response.data[i].exit_notes + "</td>";
      newList += "<td>" + response.data[i].description + "</td>";
      newList += "<td>" + response.data[i].nr_ddt_gr + "</td>";
      newList += "<td>" + response.data[i].date_ddt_gr + "</td>";
      newList += "</tr>";
      $("#table_list_repair_arriving tbody").append(newList);
      $("#table_list_repair_arriving").show();
      $("#frmUpdateIntervention").hide();
    }
    lightGallery(document.getElementById('lightgallery'));
  });

  spinnerHelpers.hide();
}

export function SearchTrue(id) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(id);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  InterventionsServices.SearchTrue(data, function (response) {
    $("#table_list_intervention tbody").html('');
    
    if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_intervention tbody").append(newList);
      $("#table_list_intervention").show();
      $("#table_list_searchintervention").hide();
      $("#table_list_repair_arriving").hide();
      $("#frmUpdateIntervention").hide();
      $("#frmViewUser").hide();
      $("#frmViewProcessingSheet").hide();
      $("#frmViewQuotes").hide();
      $("#frmViewExternalRepair").hide();
      $("#frmViewSelectReferent").hide();
      $("#ModuleValidationGuarantee").hide();
      $("#FaultModule").hide();
      
    } else {
     for (var i = 0; i < response.data.length; i++) { 
     var newList = "";
      if (response.data[i].ready) {
        if((response.data[i].nr_ddt_gr == null || response.data[i].nr_ddt_gr == "") || (response.data[i].date_ddt_gr == null ||
          response.data[i].date_ddt_gr == "") && (response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable)) {
          
            newList += "<tr class='color-orange'>";
          
        } else {
          newList += "<tr class='color-white'>";
        }
      } else {
        if((response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable)) {
          newList += "<tr class='color-orange'>";
          
        } else {
          newList += "<tr class='color-white'>";
        }
      }

      if (response.data[i].ready == true) {
      newList +=
     '<td><input type="checkbox"  id="form-control ready' + i + '" value="' + response.data[i].ready +
     '" name="ready' + i + '" class="switch-input' + i + '" checked="true' + i + '" onclick="interventionHelpers.updateCheck(' +
        response.data[i].id +
        ')"></td>'; 
      } else{
      newList +=
     '<td><input type="checkbox" id="form-control ready' + i + '" value="' + response.data[i].ready +
       '" name="ready' + i + '" class="switch-input' + i + '" onclick="interventionHelpers.updateCheck(' +
        response.data[i].id +
        ')"></td>'; 
      }

       newList +=
        '<td><button class="btn btn-info" style="background-color: #AEC4F1;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Compila Fast Urgenza!" onclick="interventionHelpers.getByInterventionFU(' +
        response.data[i].id +
        ')"><i class="fa fa-bolt" style="color:#fff;"/></button></td>';

      if (response.data[i].checkProcessingExist == true) {
        newList +=
            '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' +
            response.data[i].id +
            ')"><i class="fa fa-circle-o" style="color:#007fff!important"/></button></td>';
      }else{
        newList +=
                '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' +
                response.data[i].id +
                ')"><i class="fa fa-pencil-square-o" style="color:#007fff!important"/></button></td>';
      }
       newList += "<td>"
     if (response.data[i].id_states_quote != 'Accettato' && response.data[i].id_states_quote != 'Rifiutato' && response.data[i].id_states_quote != 'In Attesa') {
        newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:navi!important;" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag" style="color:navi!important;"/></button>';
     }
       if (response.data[i].id_states_quote == 'In Attesa') {
        newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:#f5ea74!important;" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag" style="color:#f5ea74!important;"/></button>';
       }
      
     if (response.data[i].id_states_quote == 'Accettato') {
        newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:green!important;" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag" style="color:green!important;"/></button>';
     }
     if (response.data[i].id_states_quote == 'Rifiutato') {
        newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:red!important;" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag" style="color:red!important;"/></button>';
     }
    "</td>";

      /*newList +=
        '<td><button class="btn btn-link" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag"/></button></td>';*/
       newList += "<td style='white-space:nowrap;'>" + response.data[i].id + "</td>";
       newList += "<td style='white-space:nowrap;'>" + response.data[i].code_intervention_gr + "</td>";
      newList +=
       '<td><a  style="cursor:pointer;white-space: nowrap; color:#787878 !important;" data-toggle="tooltip" data-placement="top" title="Visualizza dettaglio Utente!" onclick="interventionHelpers.getByIdUser(' +
        response.data[i].user_id +
        ')">' +  response.data[i].descriptionCustomer + '</a></td>';
      newList += "<td>" + response.data[i].date_ddt + "</td>";
      newList += "<td>" + response.data[i].nr_ddt + "</td>";
     
      //newList += "<td><a href='" + response.data[i].imgmodule + "'</a>Visualizza</td>";
      newList += "<td>" + response.data[i].description + response.data[i].rip_association + "</td>";
      //newList += "<td>" + response.data[i].defect + "</td>";
       newList += "<td>";
       for (var j = 0; j < response.data[i].captures.length; j++) { 
      newList +=
       '<div class="container"><div id="lightgallery" style="display:flex">';
        newList +=
          "<a href='" +
         response.data[i].captures[j].img  +
         "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
       }
      newList += "</td>";
      newList += "<td>"
    if (response.data[i].imgmodule != "" && response.data[i].imgmodule != '-' ) {
        newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">';
      newList +=
          "<a href='" +
          response.data[i].imgmodule +
          "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a> " + "'</div></div>";
    } 
    if (response.data[i].imgmodule == "") {
        newList += "<div>" + response.data[i].defect + "</div>";
    } 
 //
       if (response.data[i].checkModuleFault == true) {
      newList +=
        '<button class="btn btn-green" style="color:#007FFF!important;" data-toggle="tooltip" data-placement="top" title="Visualizza Modulo Guasti!"  onclick="interventionHelpers.getByFaultModule(' +
        response.data[i].id +
      ')"><i class="fa fa-file-pdf-o" style="color:#007FFF!important;"/></button>'
      } 
      if (response.data[i].checkModuleFault == false) {
         newList += "<div>" + "</div>";
      } 
//
      "</td>"
      newList += "<td>" + response.data[i].codeGr + "</td>";
      newList += "<td>" + response.data[i].date_ddt_gr + "</td>";
      newList += "<td>" + response.data[i].nr_ddt_gr + "</td>";

      if (response.data[i].idInterventionExternalRepair == null) {
        newList +=
          '<td><button class="btn btn-green" style="color:#d7e7fe !important;font-size:28px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' +
          response.data[i].id +
          ')"><i class="fa fa-truck" style="color:#d7e7fe !important"/></button></td>';
      }
      if (response.data[i].idInterventionExternalRepair != null) {
        newList +=
          '<td><button class="btn btn-green" style="color:#02784d !important;font-size:26px;margin-top: 21px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' +
          response.data[i].id +
          ')"><i class="fa fa-truck" style="color:#02784d !important;"/></button>';
      }
       if (response.data[i].ddt_supplier != null && response.data[i].ddt_supplier != "") {
        newList +=
          '<button class="btn btn-green" style="color:#02784d !important;font-size:27px;margin-top: -100px!important;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' +
          response.data[i].id +
          ')"><i class="fa fa-reply" style="color:#02784d !important;>" aria-hidden="true"></i></button></td>';
      }
       newList +=
        '<td><button class="btn btn-info" style="background-color: #ff337b;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Sezione gestione del tecnico" onclick="interventionHelpers.getBySelectReferent(' +
        response.data[i].id +
        ')"><i class="fa fa-circle-o" style="color:#fff;"/></button></td>';
        if (response.data[i].create_quote == "Si") {
          newList += "<td style='color:green;'>" + response.data[i].create_quote + "</td>";
       }else{
          newList += "<td style='color:red;'>" + response.data[i].create_quote + "</td>";
       }
     // newList += "<td>" + response.data[i].descriptionStates + "</td>";
      newList += "<td>" + response.data[i].code_tracking + "</td>";
      newList += "<td>" + response.data[i].note_internal_gr + "</td>";
      newList += "<td>"
      if (response.data[i].fileddt != "" && response.data[i].fileddt != '-' ) {
        newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">';
      newList +=
          "<a href='" +
          response.data[i].fileddt +
          "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:red;float:right;'></i></a></div></div>";
    } 
    if (response.data[i].fileddt == "") {
      newList += "<div>" + "</div>";
    } 
    newList += "</td>"
       newList +=
        '<td><button class="btn btn-info" onclick="interventionHelpers.getById(' +
        response.data[i].id +
         ')"><i class="fa fa-pencil"style="color:#fff"/></button></td>';
      
       newList += "<td>"
      if (response.data[i]. checkModuleValidationGuarantee == true) {
      newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Modulo Garanzie" style="color:#007FFF!important;" onclick="interventionHelpers.getByModuleGuarantee(' +
        response.data[i].id +
      ')"><i class="fa fa-external-link" style="color:#007FFF!important;"/></button>'
} 
      if (response.data[i].checkModuleValidationGuarantee == false) {
         newList += "<div>" + "</div>";
      } 
      for (var j = 0; j < response.data[i].captures.length; j++) { 
        newList +=
         '<div class="container"><div id="lightgallery" style="display:flex">';
          newList +=
            "<a href='" + response.data[i].captures[j].img_rep_test  +  "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
      }
      "</td>"
      
      newList +=
        '<td><button class="btn btn-danger" onclick="interventionHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
      newList += "</tr>";
      $("#table_list_intervention tbody").append(newList);
      $("#table_list_intervention").show();
      $("#table_list_searchintervention").hide();
      $("#table_list_repair_arriving").hide();
      $("#frmUpdateIntervention").hide();
      $("#frmViewUser").hide();
      $("#frmViewProcessingSheet").hide();
      $("#frmViewQuotes").hide();
      $("#frmViewExternalRepair").hide();
      $("#frmViewSelectReferent").hide();
      $("#ModuleValidationGuarantee").hide();
      $("#FaultModule").hide();
   } 
    }
    lightGallery(document.getElementById("lightgallery"));
  });

  spinnerHelpers.hide();
}

export function SearchOpen(id) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(id);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  InterventionsServices.SearchOpen(data, function (response) {
    $("#table_list_intervention tbody").html('');
    if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_intervention tbody").append(newList);
      $("#table_list_intervention").show();
      $("#table_list_searchintervention").hide();
      $("#table_list_repair_arriving").hide();
      $("#frmUpdateIntervention").hide();
      $("#frmViewUser").hide();
      $("#frmViewProcessingSheet").hide();
      $("#frmViewQuotes").hide();
      $("#frmViewExternalRepair").hide();
      $("#frmViewSelectReferent").hide();
      $("#ModuleValidationGuarantee").hide();
      $("#FaultModule").hide();
    } else {
       for (var i = 0; i < response.data.length; i++) { 
     var newList = "";
      if (response.data[i].ready) {
        if((response.data[i].nr_ddt_gr == null || response.data[i].nr_ddt_gr == "") || (response.data[i].date_ddt_gr == null ||
          response.data[i].date_ddt_gr == "") && (response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable)) {
          
            newList += "<tr class='color-orange'>";
          
        } else {
          newList += "<tr class='color-white'>";
        }
      } else {
        if((response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable)) {
          newList += "<tr class='color-orange'>";
          
        } else {
          newList += "<tr class='color-white'>";
        }
      }

      if (response.data[i].ready == true) {
      newList +=
     '<td><input type="checkbox"  id="form-control ready' + i + '" value="' + response.data[i].ready +
     '" name="ready' + i + '" class="switch-input' + i + '" checked="true' + i + '" onclick="interventionHelpers.updateCheck(' +
        response.data[i].id +
        ')"></td>'; 
      } else{
      newList +=
     '<td><input type="checkbox" id="form-control ready' + i + '" value="' + response.data[i].ready +
       '" name="ready' + i + '" class="switch-input' + i + '" onclick="interventionHelpers.updateCheck(' +
        response.data[i].id +
        ')"></td>'; 
      }

       newList +=
        '<td><button class="btn btn-info" style="background-color: #AEC4F1;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Compila Fast Urgenza!" onclick="interventionHelpers.getByInterventionFU(' +
        response.data[i].id +
        ')"><i class="fa fa-bolt" style="color:#fff;"/></button></td>';

       if (response.data[i].checkProcessingExist == true) {
      newList +=
          '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' +
          response.data[i].id +
          ')"><i class="fa fa-circle-o" style="color:#007fff!important"/></button></td>';
    }else{
      newList +=
              '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' +
              response.data[i].id +
              ')"><i class="fa fa-pencil-square-o" style="color:#007fff!important"/></button></td>';
    }
       newList += "<td>"
     if (response.data[i].id_states_quote != 'Accettato' && response.data[i].id_states_quote != 'Rifiutato' && response.data[i].id_states_quote != 'In Attesa') {
        newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:navi!important;" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag" style="color:navi!important;"/></button>';
     }
       if (response.data[i].id_states_quote == 'In Attesa') {
        newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:#f5ea74!important;" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag" style="color:#f5ea74!important;"/></button>';
       }
      
     if (response.data[i].id_states_quote == 'Accettato') {
        newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:green!important;" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag" style="color:green!important;"/></button>';
     }
     if (response.data[i].id_states_quote == 'Rifiutato') {
        newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:red!important;" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag" style="color:red!important;"/></button>';
     }
    "</td>";

      /*newList +=
        '<td><button class="btn btn-link" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag"/></button></td>';*/
      newList += "<td style='white-space:nowrap;'>" + response.data[i].id + "</td>";
      newList += "<td style='white-space:nowrap;'>" + response.data[i].code_intervention_gr + "</td>";
      newList +=
       '<td><a  style="cursor:pointer;white-space: nowrap; color:#787878 !important;" data-toggle="tooltip" data-placement="top" title="Visualizza dettaglio Utente!" onclick="interventionHelpers.getByIdUser(' +
        response.data[i].user_id +
        ')">' +  response.data[i].descriptionCustomer + '</a></td>';
      newList += "<td>" + response.data[i].date_ddt + "</td>";
      newList += "<td>" + response.data[i].nr_ddt + "</td>";
     
      //newList += "<td><a href='" + response.data[i].imgmodule + "'</a>Visualizza</td>";
      newList += "<td>" + response.data[i].description + response.data[i].rip_association + "</td>";
      //newList += "<td>" + response.data[i].defect + "</td>";
       newList += "<td>";
       for (var j = 0; j < response.data[i].captures.length; j++) { 
      newList +=
       '<div class="container"><div id="lightgallery" style="display:flex">';
        newList +=
          "<a href='" +
         response.data[i].captures[j].img  +
         "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
       }
      newList += "</td>";
      newList += "<td>"
    if (response.data[i].imgmodule != "" && response.data[i].imgmodule != '-' ) {
        newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">';
      newList +=
          "<a href='" +
          response.data[i].imgmodule +
          "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a> " + "'</div></div>";
    } 
    if (response.data[i].imgmodule == "") {
        newList += "<div>" + response.data[i].defect + "</div>";
    } 
      
 //
       if (response.data[i].checkModuleFault == true) {
      newList +=
        '<button class="btn btn-green" style="color:#007FFF!important;" data-toggle="tooltip" data-placement="top" title="Visualizza Modulo Guasti!"  onclick="interventionHelpers.getByFaultModule(' +
        response.data[i].id +
      ')"><i class="fa fa-file-pdf-o" style="color:#007FFF!important;"/></button>'
      } 
      if (response.data[i].checkModuleFault == false) {
         newList += "<div>" + "</div>";
      } 
//
      "</td>"
      newList += "<td>" + response.data[i].codeGr + "</td>";
      newList += "<td>" + response.data[i].date_ddt_gr + "</td>";
      newList += "<td>" + response.data[i].nr_ddt_gr + "</td>";

      if (response.data[i].idInterventionExternalRepair == null) {
        newList +=
          '<td><button class="btn btn-green" style="color:#d7e7fe !important;font-size:28px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' +
          response.data[i].id +
          ')"><i class="fa fa-truck" style="color:#d7e7fe !important"/></button></td>';
      }
      if (response.data[i].idInterventionExternalRepair != null) {
        newList +=
          '<td><button class="btn btn-green" style="color:#02784d !important;font-size:26px;margin-top: 21px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' +
          response.data[i].id +
          ')"><i class="fa fa-truck" style="color:#02784d !important;"/></button>';
      }
       if (response.data[i].ddt_supplier != null && response.data[i].ddt_supplier != "") {
        newList +=
          '<button class="btn btn-green" style="color:#02784d !important;font-size:27px;margin-top: -100px!important;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' +
          response.data[i].id +
          ')"><i class="fa fa-reply" style="color:#02784d !important;>" aria-hidden="true"></i></button></td>';
      }
       newList +=
        '<td><button class="btn btn-info" style="background-color: #ff337b;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Sezione gestione del tecnico" onclick="interventionHelpers.getBySelectReferent(' +
        response.data[i].id +
        ')"><i class="fa fa-circle-o" style="color:#fff;"/></button></td>';
        if (response.data[i].create_quote == "Si") {
          newList += "<td style='color:green;'>" + response.data[i].create_quote + "</td>";
       }else{
          newList += "<td style='color:red;'>" + response.data[i].create_quote + "</td>";
       }
     // newList += "<td>" + response.data[i].descriptionStates + "</td>";
      newList += "<td>" + response.data[i].code_tracking + "</td>";
      newList += "<td>" + response.data[i].note_internal_gr + "</td>";
      newList += "<td>"
      if (response.data[i].fileddt != "" && response.data[i].fileddt != '-' ) {
        newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">';
      newList +=
          "<a href='" +
          response.data[i].fileddt +
          "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:red;float:right;'></i></a></div></div>";
    } 
    if (response.data[i].fileddt == "") {
      newList += "<div>" + "</div>";
    } 
    newList += "</td>"
       newList +=
        '<td><button class="btn btn-info" onclick="interventionHelpers.getById(' +
        response.data[i].id +
         ')"><i class="fa fa-pencil"style="color:#fff"/></button></td>';
      
       newList += "<td>"
      if (response.data[i]. checkModuleValidationGuarantee == true) {
      newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Modulo Garanzie" style="color:#007FFF!important;" onclick="interventionHelpers.getByModuleGuarantee(' +
        response.data[i].id +
      ')"><i class="fa fa-external-link" style="color:#007FFF!important;"/></button>'
} 
      if (response.data[i].checkModuleValidationGuarantee == false) {
         newList += "<div>" + "</div>";
      } 
      for (var j = 0; j < response.data[i].captures.length; j++) { 
        newList +=
         '<div class="container"><div id="lightgallery" style="display:flex">';
          newList +=
            "<a href='" + response.data[i].captures[j].img_rep_test  +  "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
      }
      "</td>"
      
      newList +=
        '<td><button class="btn btn-danger" onclick="interventionHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
      newList += "</tr>";
        $("#table_list_intervention tbody").append(newList);
        $("#table_list_intervention").show();
        $("#table_list_searchintervention").hide();
        $("#table_list_repair_arriving").hide();
        $("#frmUpdateIntervention").hide();
        $("#frmViewUser").hide();
        $("#frmViewProcessingSheet").hide();
        $("#frmViewQuotes").hide();
        $("#frmViewExternalRepair").hide();
        $("#frmViewSelectReferent").hide();
        $("#ModuleValidationGuarantee").hide();
        $("#FaultModule").hide();
      }
    }
    lightGallery(document.getElementById("lightgallery"));
  });

  spinnerHelpers.hide();
}


export function SearchClose(id) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(id);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  InterventionsServices.SearchClose(data, function (response) {
    $("#table_list_intervention tbody").html('');
        if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_intervention tbody").append(newList);
      $("#table_list_intervention").show();
      $("#table_list_searchintervention").hide();
      $("#table_list_repair_arriving").hide();
      $("#frmUpdateIntervention").hide();
      $("#frmViewUser").hide();
      $("#frmViewProcessingSheet").hide();
      $("#frmViewQuotes").hide();
      $("#frmViewExternalRepair").hide();
      $("#frmViewSelectReferent").hide();
      $("#ModuleValidationGuarantee").hide();
      $("#FaultModule").hide();
      
    } else {
          for (var i = 0; i < response.data.length; i++) { 
     var newList = "";
      if (response.data[i].ready) {
        if((response.data[i].nr_ddt_gr == null || response.data[i].nr_ddt_gr == "") || (response.data[i].date_ddt_gr == null ||
          response.data[i].date_ddt_gr == "") && (response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable)) {
          
            newList += "<tr class='color-orange'>";
          
        } else {
          newList += "<tr class='color-white'>";
        }
      } else {
        if((response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable)) {
          newList += "<tr class='color-orange'>";
          
        } else {
          newList += "<tr class='color-white'>";
        }
      }

      if (response.data[i].ready == true) {
      newList +=
     '<td><input type="checkbox"  id="form-control ready' + i + '" value="' + response.data[i].ready +
     '" name="ready' + i + '" class="switch-input' + i + '" checked="true' + i + '" onclick="interventionHelpers.updateCheck(' +
        response.data[i].id +
        ')"></td>'; 
      } else{
      newList +=
     '<td><input type="checkbox" id="form-control ready' + i + '" value="' + response.data[i].ready +
       '" name="ready' + i + '" class="switch-input' + i + '" onclick="interventionHelpers.updateCheck(' +
        response.data[i].id +
        ')"></td>'; 
      }

       newList +=
        '<td><button class="btn btn-info" style="background-color: #AEC4F1;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Compila Fast Urgenza!" onclick="interventionHelpers.getByInterventionFU(' +
        response.data[i].id +
        ')"><i class="fa fa-bolt" style="color:#fff;"/></button></td>';

 if (response.data[i].checkProcessingExist == true) {
      newList +=
          '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' +
          response.data[i].id +
          ')"><i class="fa fa-circle-o" style="color:#007fff!important"/></button></td>';
    }else{
      newList +=
              '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' +
              response.data[i].id +
              ')"><i class="fa fa-pencil-square-o" style="color:#007fff!important"/></button></td>';
    }

       newList += "<td>"
     if (response.data[i].id_states_quote != 'Accettato' && response.data[i].id_states_quote != 'Rifiutato' && response.data[i].id_states_quote != 'In Attesa') {
        newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:navi!important;" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag" style="color:navi!important;"/></button>';
     }
       if (response.data[i].id_states_quote == 'In Attesa') {
        newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:#f5ea74!important;" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag" style="color:#f5ea74!important;"/></button>';
       }
      
     if (response.data[i].id_states_quote == 'Accettato') {
        newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:green!important;" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag" style="color:green!important;"/></button>';
     }
     if (response.data[i].id_states_quote == 'Rifiutato') {
        newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:red!important;" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag" style="color:red!important;"/></button>';
     }
    "</td>";

      /*newList +=
        '<td><button class="btn btn-link" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag"/></button></td>';*/
            newList += "<td style='white-space:nowrap;'>" + response.data[i].id + "</td>";
            newList += "<td style='white-space:nowrap;'>" + response.data[i].code_intervention_gr + "</td>";
      newList +=
       '<td><a  style="cursor:pointer;white-space: nowrap; color:#787878 !important;" data-toggle="tooltip" data-placement="top" title="Visualizza dettaglio Utente!" onclick="interventionHelpers.getByIdUser(' +
        response.data[i].user_id +
        ')">' +  response.data[i].descriptionCustomer + '</a></td>';
      newList += "<td>" + response.data[i].date_ddt + "</td>";
      newList += "<td>" + response.data[i].nr_ddt + "</td>";
     
      //newList += "<td><a href='" + response.data[i].imgmodule + "'</a>Visualizza</td>";
      newList += "<td>" + response.data[i].description + response.data[i].rip_association + "</td>";
      //newList += "<td>" + response.data[i].defect + "</td>";
       newList += "<td>";
       for (var j = 0; j < response.data[i].captures.length; j++) { 
      newList +=
       '<div class="container"><div id="lightgallery" style="display:flex">';
        newList +=
          "<a href='" +
         response.data[i].captures[j].img  +
         "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
       }
      newList += "</td>";
      newList += "<td>"
    if (response.data[i].imgmodule != "" && response.data[i].imgmodule != '-' ) {
        newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">';
      newList +=
          "<a href='" +
          response.data[i].imgmodule +
          "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a> " + "'</div></div>";
    } 
    if (response.data[i].imgmodule == "") {
        newList += "<div>" + response.data[i].defect + "</div>";
    } 
      
 //
       if (response.data[i].checkModuleFault == true) {
      newList +=
        '<button class="btn btn-green" style="color:#007FFF!important;" data-toggle="tooltip" data-placement="top" title="Visualizza Modulo Guasti!"  onclick="interventionHelpers.getByFaultModule(' +
        response.data[i].id +
      ')"><i class="fa fa-file-pdf-o" style="color:#007FFF!important;"/></button>'
      } 
      if (response.data[i].checkModuleFault == false) {
         newList += "<div>" + "</div>";
      } 
//
      "</td>"
      newList += "<td>" + response.data[i].codeGr + "</td>";
      newList += "<td>" + response.data[i].date_ddt_gr + "</td>";
      newList += "<td>" + response.data[i].nr_ddt_gr + "</td>";

      if (response.data[i].idInterventionExternalRepair == null) {
        newList +=
          '<td><button class="btn btn-green" style="color:#d7e7fe !important;font-size:28px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' +
          response.data[i].id +
          ')"><i class="fa fa-truck" style="color:#d7e7fe !important"/></button></td>';
      }
      if (response.data[i].idInterventionExternalRepair != null) {
        newList +=
          '<td><button class="btn btn-green" style="color:#02784d !important;font-size:26px;margin-top: 21px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' +
          response.data[i].id +
          ')"><i class="fa fa-truck" style="color:#02784d !important;"/></button>';
      }
       if (response.data[i].ddt_supplier != null && response.data[i].ddt_supplier != "") {
        newList +=
          '<button class="btn btn-green" style="color:#02784d !important;font-size:27px;margin-top: -100px!important;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' +
          response.data[i].id +
          ')"><i class="fa fa-reply" style="color:#02784d !important;>" aria-hidden="true"></i></button></td>';
      }
       newList +=
        '<td><button class="btn btn-info" style="background-color: #ff337b;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Sezione gestione del tecnico" onclick="interventionHelpers.getBySelectReferent(' +
        response.data[i].id +
        ')"><i class="fa fa-circle-o" style="color:#fff;"/></button></td>';
        if (response.data[i].create_quote == "Si") {
          newList += "<td style='color:green;'>" + response.data[i].create_quote + "</td>";
       }else{
          newList += "<td style='color:red;'>" + response.data[i].create_quote + "</td>";
       }
     // newList += "<td>" + response.data[i].descriptionStates + "</td>";
      newList += "<td>" + response.data[i].code_tracking + "</td>";
      newList += "<td>" + response.data[i].note_internal_gr + "</td>";
      newList += "<td>"
      if (response.data[i].fileddt != "" && response.data[i].fileddt != '-' ) {
        newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">';
      newList +=
          "<a href='" +
          response.data[i].fileddt +
          "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:red;float:right;'></i></a></div></div>";
    } 
    if (response.data[i].fileddt == "") {
      newList += "<div>" + "</div>";
    } 
    newList += "</td>"
       newList +=
        '<td><button class="btn btn-info" onclick="interventionHelpers.getById(' +
        response.data[i].id +
         ')"><i class="fa fa-pencil"style="color:#fff"/></button></td>';
      
       newList += "<td>"
      if (response.data[i]. checkModuleValidationGuarantee == true) {
      newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Modulo Garanzie" style="color:#007FFF!important;" onclick="interventionHelpers.getByModuleGuarantee(' +
        response.data[i].id +
      ')"><i class="fa fa-external-link" style="color:#007FFF!important;"/></button>'
} 
      if (response.data[i].checkModuleValidationGuarantee == false) {
         newList += "<div>" + "</div>";
      } 
      for (var j = 0; j < response.data[i].captures.length; j++) { 
        newList +=
         '<div class="container"><div id="lightgallery" style="display:flex">';
          newList +=
            "<a href='" + response.data[i].captures[j].img_rep_test  +  "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
      }
      "</td>"
      
      newList +=
        '<td><button class="btn btn-danger" onclick="interventionHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
      newList += "</tr>";
            $("#table_list_intervention tbody").append(newList);
            $("#table_list_intervention").show();
            $("#table_list_searchintervention").hide();
            $("#table_list_repair_arriving").hide();
            $("#frmUpdateIntervention").hide();
            $("#frmViewUser").hide();
            $("#frmViewProcessingSheet").hide();
            $("#frmViewQuotes").hide();
            $("#frmViewExternalRepair").hide();
            $("#frmViewSelectReferent").hide();
            $("#ModuleValidationGuarantee").hide();
            $("#FaultModule").hide();
          }
    }
    lightGallery(document.getElementById("lightgallery"));
  });

  spinnerHelpers.hide();
}

export function getAllIntervention() {
  spinnerHelpers.show();
  InterventionsServices.getAllIntervention(storageData.sIdLanguage(), function(response) {
    for (var i = 0; i < response.data.length; i++) { 
     var newList = "";
      if (response.data[i].ready) {
        if((response.data[i].nr_ddt_gr == null || response.data[i].nr_ddt_gr == "") || (response.data[i].date_ddt_gr == null ||
          response.data[i].date_ddt_gr == "") && (response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable)) {
            newList += "<tr class='color-orange'>";
        } else {
          newList += "<tr class='color-white'>";
        }
      } else {
        if ((response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable) &&
        (response.data[i].nr_ddt_gr == null || response.data[i].nr_ddt_gr == '' || response.data[i].date_ddt_gr == null || response.data[i].date_ddt_gr == '')) {
          newList += "<tr class='color-orange'>";    
        } else {
          newList += "<tr class='color-white'>";
        }
      }
      if (response.data[i].ready == true) {
      newList +=
     '<td><input type="checkbox"  id="form-control ready' + i + '" value="' + response.data[i].ready +
     '" name="ready' + i + '" class="switch-input' + i + '" checked="true' + i + '" onclick="interventionHelpers.updateCheck(' +
        response.data[i].id +
        ')"></td>'; 
      } else{
      newList +=
     '<td><input type="checkbox" id="form-control ready' + i + '" value="' + response.data[i].ready +
       '" name="ready' + i + '" class="switch-input' + i + '" onclick="interventionHelpers.updateCheck(' +
        response.data[i].id +
        ')"></td>'; 
      }
       newList +=
        '<td><button class="btn btn-info" style="background-color: #AEC4F1;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Compila Fast Urgenza!" onclick="interventionHelpers.getByInterventionFU(' +
        response.data[i].id +
        ')"><i class="fa fa-bolt" style="color:#fff;"/></button></td>';
 
    if (response.data[i].checkProcessingExist == true) {
      newList +=
          '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' +
          response.data[i].id +
          ')"><i class="fa fa-circle-o" style="color:#007fff!important"/></button></td>';
    }else{
      newList +=
              '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' +
              response.data[i].id +
              ')"><i class="fa fa-pencil-square-o" style="color:#007fff!important"/></button></td>';
    }

       newList += "<td>"
     if (response.data[i].id_states_quote != 'Accettato' && response.data[i].id_states_quote != 'Rifiutato' && response.data[i].id_states_quote != 'In Attesa') {
        newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:navi!important;" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag" style="color:navi!important;"/></button>';
     }
       if (response.data[i].id_states_quote == 'In Attesa') {
        newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:#f5ea74!important;" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag" style="color:#f5ea74!important;"/></button>';
       }
      
     if (response.data[i].id_states_quote == 'Accettato') {
        newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:green!important;" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag" style="color:green!important;"/></button>';
     }
     if (response.data[i].id_states_quote == 'Rifiutato') {
        newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:red!important;" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag" style="color:red!important;"/></button>';
     }
    "</td>";

      /*newList +=
        '<td><button class="btn btn-link" onclick="interventionHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-flag"/></button></td>';*/
      newList += "<td>" + response.data[i].id + "</td>";
      newList += "<td>" + response.data[i].code_intervention_gr + "</td>";
      newList +=
       '<td><a  style="cursor:pointer;white-space: nowrap; color:#787878 !important;" data-toggle="tooltip" data-placement="top" title="Visualizza dettaglio Utente!" onclick="interventionHelpers.getByIdUser(' +
        response.data[i].user_id +
        ')">' +  response.data[i].descriptionCustomer + '</a></td>';
      newList += "<td>" + response.data[i].date_ddt + "</td>";
      newList += "<td>" + response.data[i].nr_ddt + "</td>";

      //newList += "<td><a href='" + response.data[i].imgmodule + "'</a>Visualizza</td>";
      newList += "<td>" + response.data[i].description + response.data[i].rip_association + "</td>";
      //newList += "<td>" + response.data[i].defect + "</td>";
       newList += "<td>";
       for (var j = 0; j < response.data[i].captures.length; j++) { 
      newList +=
       '<div class="container"><div id="lightgallery" style="display:flex">';
        newList +=
          "<a href='" + response.data[i].captures[j].img  +  "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
       }
      newList += "</td>";
      newList += "<td>"
    if (response.data[i].imgmodule != "" && response.data[i].imgmodule != '-' ) {
        newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">';
    /*  newList +=
          "<a href='" +
          response.data[i].imgmodule +
          "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a> " + response.data[i].defect + "'</div></div>";*/
          newList +=
          "<a href='" +
          response.data[i].imgmodule +
          "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a> " + "'</div></div>";
    } 
    if (response.data[i].imgmodule == "") {
        newList += "<div>" + response.data[i].defect + "</div>";
    } 
 //
       if (response.data[i].checkModuleFault == true) {
      newList +=
        '<button class="btn btn-green" style="color:#007FFF!important;" data-toggle="tooltip" data-placement="top" title="Visualizza Modulo Guasti!"  onclick="interventionHelpers.getByFaultModule(' +
        response.data[i].id +
      ')"><i class="fa fa-file-pdf-o" style="color:#007FFF!important;"/></button>'
      } 
      if (response.data[i].checkModuleFault == false) {
         newList += "<div>" + "</div>";
      } 
//
      newList += "<td>" + response.data[i].codeGr + "</td>";
      newList += "<td>" + response.data[i].date_ddt_gr + "</td>";
      newList += "<td>" + response.data[i].nr_ddt_gr + "</td>";
      if (response.data[i].idInterventionExternalRepair == null) {
        newList +=
          '<td><button class="btn btn-green" style="color:#d7e7fe !important;font-size:28px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' +
          response.data[i].id +
          ')"><i class="fa fa-truck" style="color:#d7e7fe !important"/></button></td>';
      }
      if (response.data[i].idInterventionExternalRepair != null) {
        newList +=
          '<td><button class="btn btn-green" style="color:#02784d !important;font-size:26px;margin-top: 21px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' +
          response.data[i].id +
          ')"><i class="fa fa-truck" style="color:#02784d !important;"/></button>';
      }
       if (response.data[i].ddt_supplier != null && response.data[i].ddt_supplier != "") {
        newList +=
          '<button class="btn btn-green" style="color:#02784d !important;font-size:27px;margin-top: -100px!important;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' +
          response.data[i].id +
          ')"><i class="fa fa-reply" style="color:#02784d !important;>" aria-hidden="true"></i></button></td>';
      }
       newList +=
        '<td><button class="btn btn-info" style="background-color: #ff337b;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Sezione gestione del tecnico" onclick="interventionHelpers.getBySelectReferent(' +
        response.data[i].id +
        ')"><i class="fa fa-circle-o" style="color:#fff;"/></button></td>';
        if (response.data[i].create_quote == "Si") {
          newList += "<td style='color:green;'>" + response.data[i].create_quote + "</td>";
       }else{
          newList += "<td style='color:red;'>" + response.data[i].create_quote + "</td>";
       }
     // newList += "<td>" + response.data[i].descriptionStates + "</td>";
      newList += "<td>" + response.data[i].code_tracking + "</td>";
      newList += "<td>" + response.data[i].note_internal_gr + "</td>";

      newList += "<td>"
      if (response.data[i].fileddt != "" && response.data[i].fileddt != '-' ) {
        newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">';
      newList +=
          "<a href='" +
          response.data[i].fileddt +
          "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:red;float:right;'></i></a></div></div>";
    } 
    if (response.data[i].fileddt == "") {
      newList += "<div>" + "</div>";
    } 
    newList += "</td>"

       newList +=
        '<td><button class="btn btn-info" onclick="interventionHelpers.getById(' +
        response.data[i].id +
         ')"><i class="fa fa-pencil"style="color:#fff"/></button></td>';
      
       newList += "<td>"
      if (response.data[i]. checkModuleValidationGuarantee == true) {
      newList +=
        '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Modulo Garanzie" style="color:#007FFF!important;" onclick="interventionHelpers.getByModuleGuarantee(' +
        response.data[i].id +
      ')"><i class="fa fa-external-link" style="color:#007FFF!important;"/></button>'
} 
      if (response.data[i].checkModuleValidationGuarantee == false) {
         newList += "<div>" + "</div>";
      } 

     for (var j = 0; j < response.data[i].captures.length; j++) { 
        newList +=
         '<div class="container"><div id="lightgallery" style="display:flex">';
          newList +=
            "<a href='" + response.data[i].captures[j].img_rep_test  +  "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
      }
      "</td>"
      newList +=
        '<td><button class="btn btn-danger" onclick="interventionHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
      newList += "</tr>";
      $("#table_list_intervention tbody").append(newList);

      $("#table_list_intervention").show();
      $("#table_list_searchintervention").hide();
      $("#table_list_repair_arriving").hide();
      $("#frmUpdateIntervention").hide();
      $("#frmViewUser").hide();
      $("#frmViewProcessingSheet").hide();
      $("#frmViewQuotes").hide();
      $("#frmViewExternalRepair").hide();
      $("#frmViewSelectReferent").hide();
      $("#ModuleValidationGuarantee").hide();
      $("#FaultModule").hide();
    }
    lightGallery(document.getElementById("lightgallery"));
  });
  spinnerHelpers.hide();
}
//   ------ region get by -------  //
export function getByViewExternalRepair(id) {
  InterventionsServices.getByViewExternalRepair(id, function(response) {
    var searchParam = "";
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmViewExternalRepair #" + i).html(val);
        } else {
          $("#frmViewExternalRepair #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmViewExternalRepair #imgPreview").attr("src", val);
          } else {
            $("#frmViewExternalRepair #imgName").remove();
          }
        } else {
          $("#frmViewExternalRepair #" + i).val(val);
          $("#frmViewExternalRepair #" + i).html(val);
        }
      }
  var selectFaultModule = response.data.selectFaultModule[0];
      $("#div-cont-module").html("");
      var html = "";
      html += '<h3 '+ '">Anteprima Modulo Guasti ' + "</h3>&nbsp;";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-3">';
      html += '<label for="external_referent_id' + '">Referente ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="external_referent_id" id="external_referent_id" placeholder="Referente" readonly value="' +
        selectFaultModule.external_referent_id +
        '">';
      html += "</div>";
      html += '<div class="col-3">';
      html += '<label for="nr_ddt' + '">Numero DDT ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="nr_ddt" id="nr_ddt" placeholder="Numero DDT" readonly value="' +
        selectFaultModule.nr_ddt +
        '">';
      html += "</div>";
       html += '<div class="col-3">';
      html += '<label for="date_ddt' + '">Data DDT ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="referent" id="date_ddt" placeholder="Data DDT" readonly value="' +
        selectFaultModule.date_ddt +
        '">';
      html += "</div>";
      html += "</div>";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-3">';
      html += '<label for="trolley_type' + '">Modello Carrello ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="trolley_type" id="trolley_type" placeholder="Modello Carrello" readonly value="' +
        selectFaultModule.trolley_type +
        '">';
      html += "</div>";
      html += '<div class="col-3">';
      html += '<label for="series' + '">Matricola ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="series" id="series" placeholder="Matricola" readonly value="' +
        selectFaultModule.series +
        '">';
      html += "</div>";
       html += '<div class="col-3">';
      html += '<label for="voltage' + '">Voltaggio ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="voltage" id="voltage" placeholder="Voltaggio" readonly value="' +
        selectFaultModule.voltage +
        '">';
      html += "</div>";
      html += '<div class="col-3">';
      html += '<label for="plant_type' + '">Tipo Impianto ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="plant_type" id="plant_type" placeholder="Tipo Impianto" readonly value="' +
        selectFaultModule.plant_type +
        '">';
      html += "</div>";
      html += "</div>";

      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<label for="defect' + '">Difetto ' + "</label>&nbsp;";
      html += '<textarea class="form-control" id="defect" readonly>' + selectFaultModule.defect + '</textarea>';
      html += "</div>";
      html += '<div class="col-6">';
      html += '<label for="exit_notes' + '">Note ' + "</label>&nbsp;";
      html += '<textarea class="form-control" id="exit_notes" readonly>' + selectFaultModule.exit_notes + '</textarea>';
      html += "</div>";
      html += "</div>";
      $("#div-cont-module").append(html);

      var AggExternalRepair = response.data;
      console.log(response.data);
      $("#div-cont-agg-external-repair").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-4">';
      html += '<label for="id_intervention' + '">Numero Rip.Esterna ' + "</label>&nbsp;";
      html +=
      '<input type="text" class="form-control" name="id_librone_rip_esterna" id="id_librone_rip_esterna" placeholder="Numero Riparazione Esterna" value="' + AggExternalRepair.id_librone_rip_esterna + '">';
      html +=
      '<input type="hidden" class="form-control" name="id_intervention" id="id_intervention" placeholder="Numero Riparazione" value="' + AggExternalRepair.id_intervention + '">';
      html += "</div>";
      html += '<div class="col-4">';
      html += '<label for="date' + '">Data ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="date" id="date" placeholder="Data" value="' +
        AggExternalRepair.date +
        '">';
      html += "</div>";
      html += '<div class="col-4">';
      html += '<label for="ddt_exit' + '">DDT Uscita ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="ddt_exit" id="ddt_exit" placeholder="DDT Uscita" value="' +
        AggExternalRepair.ddt_exit +
        '">';
      html += "</div>";
      html += "</div>";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html +=
        '<label for="id_business_name_supplier' +
        '">Fornitore ' +
        "</label>&nbsp;";
      html +=
        '<select class="form-control select-graphic custom-select id_business_name_supplier" id="id_business_name_supplier' +
        '" value="' +
        AggExternalRepair.id_business_name_supplier +
        '" name="id_business_name_supplier' +
        '"></select>';
      html += "</div>";
      html += "</div>";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-11">';
      html += '<label for="description' + '">Descrizione ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="description" id="description" placeholder="Descrizione" value="' +
        AggExternalRepair.description +
        '">';
      html += "</div>";
      html += "</div>";
      html += '<div class="row">';
      html += '<div class="col-6">';
      html += '<label for="code_gr' + '">Codice GR ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="code_gr" id="code_gr" placeholder="Codice GR" value="' +
        AggExternalRepair.code_gr +
        '">';
      html += "</div>";
      html += '<div class="col-6">';
      html += '<label for="price' + '">Prezzo ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="price" id="price" placeholder="Prezzo" value="' +
        AggExternalRepair.price +
        '">';
      html +=
        '<input type="hidden" class="form-control" name="id" id="id" placeholder="id" value="' +
        AggExternalRepair.id +
        '">';
      html += "</div>";
       html +=
        '<input type="hidden" class="form-control" name="codice_intervento" id="codice_intervento" placeholder="codice_intervento" value="' + AggExternalRepair.codice_intervento + '">';
      html += "</div>";
      html += "</div>";
      $("#div-cont-agg-external-repair").append(html);

      $('#div-cont-agg-external-repair #date').datetimepicker({
        format: 'DD/MM/YYYY'
        });


    var AggExternalRepair = response.data;
          $("#div-cont-upd-rientro-external-repair").html("");
          var html = "";
          html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
          html += '<div class="col-4">';
          html += '<label for="date_return_product_from_the_supplier' + '">Data Entrata ' + "</label>&nbsp;";
          html +=
            '<input type="text" class="form-control" name="date_return_product_from_the_supplier" id="date_return_product_from_the_supplier" placeholder="Data Entrata" value="' +
            AggExternalRepair.date_return_product_from_the_supplier +
            '">';
          html += "</div>";
          html += '<div class="col-4">';
          html += '<label for="ddt_supplier' + '">DDT Fornitore ' + "</label>&nbsp;";
          html +=
            '<input type="text" class="form-control" name="ddt_supplier" id="ddt_supplier" placeholder="DDT Fornitore" value="' +
            AggExternalRepair.ddt_supplier +
            '">';
          html += "</div>";
          html += '<div class="col-4">';
          html += '<label for="reference_supplier' + '">Rif Fornitore ' + "</label>&nbsp;";
          html +=
            '<input type="text" class="form-control" name="reference_supplier" id="reference_supplier" placeholder="Rif Fornitore" value="' +
            AggExternalRepair.reference_supplier +
            '">';
          html += "</div>";
          html += "</div>";
          html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
          html += '<div class="col-12">';
          html += '<label for="notes_from_repairman' + '">Note Fornitore ' + "</label>&nbsp;";
          html += '<textarea class="form-control" id="notes_from_repairman" placeholder="Note Fornitore">' + AggExternalRepair.notes_from_repairman + '</textarea>';

          html += "</div>";
          html += "</div>";
          $("#div-cont-upd-rientro-external-repair").append(html); 

          $('#div-cont-upd-rientro-external-repair #date_return_product_from_the_supplier').datetimepicker({
            format: 'DD/MM/YYYY'
            });

      var searchParam = "";
      var initials = [];
      initials.push({
        id: AggExternalRepair.id_business_name_supplier,
        text: AggExternalRepair.Description_business_name_supplier
      });
      $("#id_business_name_supplier").select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl +
            "/api/v1/businessnamesupplier/select",
          data: function(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var searchParam = "";
      $("#id_business_name_supplier").select2({
        ajax: {
          url:
            configData.wsRootServicesUrl +
            "/api/v1/businessnamesupplier/select",
          data: function(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });
    });
    $("#frmViewExternalRepair").modal("show");
    $("#table_list_searchintervention").hide();
    $("#table_list_intervention").show();
    $("#frmSearchIntervention").hide();
    $("#frmViewProcessingSheet").hide();
    $("#frmViewQuotes").hide();
    $("#frmViewSelectReferent").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();


  });
}


export function getByInterventionFU(id) {
  InterventionsServices.getByInterventionFU(id, function(response) {
 var searchParam = "";
   if (!$.isEmptyObject(response.data)) {
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmViewInterventionFU #" + i).html(val);
        } else {
          $("#frmViewInterventionFU #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmViewInterventionFU #imgPreview").attr("src", val);
          } else {
            $("#frmViewInterventionFU #imgName").remove();
          }
        } else {
          $("#frmViewInterventionFU #" + i).val(val);
          $("#frmViewInterventionFU #" + i).html(val);
        }
      }

     var data = new Date();
      var gg, mm, aaaa;
      gg = data.getDate() + "/";
      mm = data.getMonth() + 1 + "/";
      aaaa = data.getFullYear();

      var HH = data.getHours();
      var MM = data.getMinutes();
      var SS = data.getSeconds();
      parseInt(MM) < 10 ? MM = "0" + MM : null;
      parseInt(SS) < 10 ? SS = "0" + SS : null;
     var AggInterventionFU = response.data;
      
      $("#div-cont-agg-intervention-fu").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<label for="description_user_id' + '">Cliente ' + "</label>&nbsp;";
      html +=
        '<input type="text" style="background-color:fff!important;" class="form-control" name="description_user_id" id="description_user_id" placeholder=" Cliente" value="' +
        AggInterventionFU.description_user_id +
        '" readonly>';
      html +=
        '<input type="hidden" class="form-control" name="user_id" id="user_id" value="' +
        AggInterventionFU.user_id +
        '">';
      html += "</div>";

        html += '<div class="col-3">';
        html += '<label for="referent' + '">Tecnico ' + "</label>&nbsp;";
       html += '<input type="text" style="background-color:fff!important;" class="form-control" name="referent" id="referent" value="' +
        AggInterventionFU.referent +
        '" readonly>';
    html += "</div>";
    html += '<div class="col-3">';
    html += '<label for="id' + '">Numero Riparazione ' + "</label>&nbsp;";
    html +=
        '<input type="text" style="background-color:fff!important;" class="form-control" name="id" id="id" value="' +
        AggInterventionFU.id +
        '" readonly>';
      html += "</div>";
      html += "</div>";

    html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
    html += '<div class="col-6">';
      html += '<label for="date' + '">Data ' + "</label>&nbsp;";

    html +=
        '<input type="text" class="form-control" name="date" id="date" value="' +
        AggInterventionFU.date +
        '">';
      html += "</div>";
      html += '<div class="col-6">';
      html += '<label for="descriptionfast' + '">Descrizione ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="descriptionfast" id="descriptionfast" placeholder=" Descrizione" value="' +
        AggInterventionFU.descriptionfast +
        '">';
      html += "</div>";
      html += "</div>";
      $("#div-cont-agg-intervention-fu").append(html);
   
      $('#div-cont-agg-intervention-fu #date').datetimepicker({
        format: 'DD/MM/YYYY'
        });

    });
     $("#frmViewInterventionFU").modal("show");
   } else { 
       var html = "";
      $("#div-cont-agg-intervention-fu").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<label for="description_user_id' + '">Cliente ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="description_user_id" id="description_user_id" placeholder="Cliente" value="' 
         +
        '">';
     html +=
        '<input type="hidden" class="form-control" name="user_id" id="user_id" value="' 
         +
        '">';
      html += "</div>";
    html += '<div class="col-3">';
    html += '<label for="referent' + '">Tecnico ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="referent" id="referent" value="' 
        +
        '">';
      html += "</div>";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-3">';
      html += '<label for="id' + '">Numero Riparazione ' + "</label>&nbsp;";
      html +=
      '<input type="text" class="form-control" name="id" id="id" placeholder="Numero Riparazione" value="' 
       +
        '">';
     html += "</div>";
     html += "</div>";
     html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
     html += '<div class="col-6">';
     html += '<label for="date' + '">Data ' + "</label>&nbsp;";
     '<input type="text" class="form-control" name="date" id="date" placeholder="Data" value="' 
     //html += '<input type="text" class="form-control" name="date" id="date" value="'+ gg + mm + aaaa + ' ' + HH + ":" + MM + ":" + SS + '">';
      html += "</div>";
      html += '<div class="col-6">';
      html += '<label for="descriptionfast' + '">Descrizione ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="descriptionfast" id="descriptionfast" placeholder="Descrizione" value="' +
        '">';
      html += "</div>";
      html += "</div>";
      $("#div-cont-agg-intervention-fu").append(html);

      $('#div-cont-agg-intervention-fu #date').datetimepicker({
        format: 'DD/MM/YYYY'
        });

     $("#frmViewInterventionFU").modal("show");
  }
});
}


export function updateInterventionFU() {
  spinnerHelpers.show();
  var data = {
    object: $("#frmViewInterventionFU #id").val(),
    date: $("#frmViewInterventionFU #date").val(),
    description: $("#frmViewInterventionFU #descriptionfast").val(),
    user_id: $("#frmViewInterventionFU #user_id").val(),
};

 InterventionsServices.updateInterventionFU(data, function() {
    $("#frmViewInterventionFU #description").val("");
    notificationHelpers.success("Modifica avvenuta con successo");
    spinnerHelpers.hide();
    window.location.reload();
    $("#table_list_intervention").show();
    $("#frmViewQuotes").hide();
    $("#frmViewUser").hide();
    $("#frmUpdateIntervention").hide();
    $("#frmSearchIntervention").hide();
    $("#table_list_searchintervention").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#frmViewInterventionFU").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
   
  });
}



export function getBySelectReferent(id) {
  InterventionsServices.getBySelectReferent(id, function(response) {

    var searchParam = "";
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmViewSelectReferent #" + i).html(val);
        } else {
          $("#frmViewSelectReferent #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmViewSelectReferent #imgPreview").attr("src", val);
          } else {
            $("#frmViewSelectReferent #imgName").remove();
          }
        } else {
          $("#frmViewSelectReferent #" + i).val(val);
          $("#frmViewSelectReferent #" + i).html(val);
        }
      }

      var AggReferent = response.data;
      $("#div-cont-agg-referent").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<label for="id' + '">Numero Riparazione ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="id" id="id" disabled="true" placeholder="Numero Riparazione" value="' +
        AggReferent.id +
        '">';
      html += "</div>";
      html += "</div>";
         html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html +=
        '<label for="id_people' +
        '">Tecnico ' +
        "</label>&nbsp;";
      html +=
        '<select class="form-control select-graphic custom-select id_people" id="id_people' +
        '" value="' +
        AggReferent.id_people +
        '" name="id_people' +
        '"></select>';
      html += "</div>";
      html += "</div>";
      $("#div-cont-agg-referent").append(html);

    $("#frmViewSelectReferenta #id_people").on("change", function() {
         if (AggReferent.rip_association != null) {
alert("Attenzione, questa riparazione è associata alla riparazione numero " +  AggReferent.rip_association); 
 } 
    });

   
      var searchParam = "";
      var initials = [];
      initials.push({
        id: AggReferent.id_people,
        text: AggReferent.Description_People
      });
      $("#id_people").select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl +
            "/api/v1/people/select",
          data: function(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var searchParam = "";
      $("#id_people").select2({
        ajax: {
          url:
            configData.wsRootServicesUrl +
            "/api/v1/people/select",
          data: function(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });
    });
    $("#frmViewSelectReferent").modal("show");
    $("#frmViewExternalRepair").modal("hide");
    $("#table_list_searchintervention").hide();
    $("#table_list_intervention").show();
    $("#frmSearchIntervention").hide();
    $("#frmViewProcessingSheet").hide();
    $("#frmViewQuotes").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
  });
}

export function getByViewQuotes(id) {
  InterventionsServices.getByViewQuotes(id, storageData.sUserId(), function (response) {
    var searchParam = "";
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmViewQuotes #" + i).html(val);
        } else {
          $("#frmViewQuotes #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmViewQuotes #imgPreview").attr("src", val);
          } else {
            $("#frmViewQuotes #imgName").remove();
          }
        } else {
          $("#frmViewQuotes #" + i).val(val);
          $("#frmViewQuotes #" + i).html(val);
        }
      }

      var AggOptions = response.data.ReferentEmailSendPreventivi;
      if (AggOptions.length > 0) {
              $("#emailReferent").html("");
              for (var j = 0; j < AggOptions.length; j++) {
                var html = "";
                if (j < 10) {
            html += '<div class="row" style="margin-bottom: 10px;margin-top: 7px;" data-id-row="' + '" id="divcont-' +' " >';
            html += '<div class="col-10 text-left">';
            html += '<label class="labelclass">Email</label>';
            html += '<input type="hidden" class="form-control cost" id="user_id' + j + '" value="' + AggOptions[j].user_id + '" name="user_id' + j + '">';
            html += '<input type="hidden" class="form-control cost" id="people_id' + j + '" value="' + AggOptions[j].people_id + '" name="people_id' + j + '">';
            html += '<input type="text" class="form-control cost" id="emailreferente' + j + '" value="' + AggOptions[j].EmailReferente + '" name="emailreferente' + j + '">';
            html += "</div>";          
            html += '<div class="col-2">';
            html += '<button type="button" style="margin-top: 30px;border-color: #fff;" class="btn btn-outline-danger del-img-agg" data-id-cont="' + AggOptions[j].people_id + '" id="btndelete' + AggOptions[j].people_id + '"><i class="fa fa-times"></i></button>'
            html += "</div>";
            html += "</div>";
            $("#emailReferent").append(html);
          }

          $("#frmViewQuotes .del-img-agg").on("click", function(e) {
            var idDel = $(this).attr('data-id-cont');
            var el = $(this).parent('div').parent('div').attr('data-id-row', idDel);
            
            AggOptions = AggOptions - 1;
            if(AggOptions<0){
              AggOptions = 0;
            }
            el.remove();
          });

        }
      }

      $('#div-cont-updated_id').html('');
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-5" style="text-align: left;">';
      html += '<span for="created_id' + '"><strong>Preventivo Creato Da: ' + '</strong></span>';
      html += '<input type="text" class="form-control" style="border: #fff;" name="created_id" id="created_id" value="' + response.data.created_id + '" readonly>';
      html += "</div>";
     // html += '<div class="col-4">';
     // html += '<span for="kind_attention' + '"><strong> Alla c.a: ' + '</strong></span>';
      //html += '<input type="text" class="form-control" style="border: #fff;" name="kind_attention" id="kind_attention" value="' + response.data.kind_attention + '">';
      //html += "</div>";
      html += "</div>";
      $('#div-cont-updated_id').append(html);


    /*  $('#div-cont-updated_id').html('');
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div>';
      html += '<span for="created_id' + '"><strong>Preventivo Creato Da: ' + '</strong></span>';
      html += '<input type="text" class="form-control" style="border: #fff;" name="created_id" id="created_id" value="' + response.data.created_id + '" readonly>';
      html += "</div>";
     // html += '<div class="col-4">';
     // html += '<span for="kind_attention' + '"><strong> Alla c.a: ' + '</strong></span>';
      //html += '<input type="text" class="form-control" style="border: #fff;" name="kind_attention" id="kind_attention" value="' + response.data.kind_attention + '">';
      //html += "</div>";
      html += "</div>";
      $('#div-cont-updated_id').append(html);*/
      
      var AggNoteBeforeTheQuotes = response.data;
      $("#div-cont-note_before_the_quote").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-12">';
      html += '<textarea class="form-control" id="note_before_the_quote">' + AggNoteBeforeTheQuotes.note_before_the_quote + '</textarea>';
     /* html +=
        '<textarea class="form-control" id="note_before_the_quote">' +
        "In riferimento al Vs DDT n. " +
        AggNoteBeforeTheQuotes.nr_ddt +
        "  del " +
        AggNoteBeforeTheQuotes.date_ddt +
        " con la presente siamo ad inviarVi il seguente preventivo di riparazione relativo: " +
        AggNoteBeforeTheQuotes.description +
        "</textarea>";*/
      html += "</div>";
      html += "</div>";
      $("#div-cont-note_before_the_quote").append(html);

      var AggTotale = response.data;
      $("#div-cont-totale").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<label for="tot_quote' + '">Totale: ' + '</label>&nbsp;';
      //if(AggTotale.tot_quote==''){
      html += '<input type="text" class="form-control tot_quote" name="tot_quote" id="tot_quote" value="' + '€ ' + AggTotale.tot_quote + ' Netto + Iva" readonly>';
      // }else{
      // html += '<input type="text" class="form-control" name="tot_quote" id="tot_quote" value="'+ AggTotale.tot_quote + '">';
      //}
      html += "</div>";
      html += '<div class="col-6">';
      html += '<label for="discount' + '">Sconto €: ' + '</label>&nbsp;';
      html += '<input type="text" class="form-control discount" name="discount" id="discount" value="' + AggTotale.discount + '">';
      html += "</div>";
      html += '<div class="col-6">';
      html += '<label for="total_amount_with_discount' + '">Totale Scontato: ' + '</label>&nbsp;';
      html += '<input type="text" class="form-control total_amount_with_discount" name="total_amount_with_discount" id="total_amount_with_discount" value="' + '€ ' + AggTotale.total_amount_with_discount + ' Netto + Iva">';
      html += "</div>";
      html += "</div>";
      $("#div-cont-totale").append(html);

      var data = new Date();
      var gg, mm, aaaa;
      gg = data.getDate() + "/";
      mm = data.getMonth() + 1 + "/";
      aaaa = data.getFullYear();

      $('#div-cont-date_agg').html('');
      var html = "";
      html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<span for="date_agg' + '"><strong>Data: ' + '</strong></span><br>';
      html += '<input type="text" class="form-control" name="date_agg" id="date_agg" value="' + response.data.date_agg + '">';
      html += "</div>";
      html += "</div>";
      $('#div-cont-date_agg').append(html);

      var AggPaymentQuotes = response.data;
      $("#div-cont-payment_quote").html("");
      var html = "";
      html +=
        '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-8">';
      html +=
        '<label for="description_payment' +
        '">Descrizione Pagamento ' +
        "</label>&nbsp;";
      html +=
        '<select class="form-control select-graphic custom-select description_payment" id="description_payment' +
        '" value="' +
        AggPaymentQuotes.description_payment +
        '" name="description_payment' +
        '"></select>';
      html += "</div>";
      html += "</div>";
      $("#div-cont-payment_quote").append(html);

      var searchParam = "";
      var initials = [];
      initials.push({
        id: AggPaymentQuotes.description_payment,
        text: AggPaymentQuotes.Description_payment
      });
      $("#description_payment").select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl + "/api/v1/paymentGr/selectpaymentGr",
          data: function(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var searchParam = "";
      $("#description_payment").select2({
        ajax: {
          url:
            configData.wsRootServicesUrl + "/api/v1/paymentGr/selectpaymentGr",
          data: function(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var searchParam = "";
      var initials = [];
      initials.push({
        id: AggPaymentQuotes.id_states_quote,
        text: AggPaymentQuotes.States_Description
      });
      $("#id_states_quote").select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl +
            "/api/v1/StatesQuotesGr/selectStatesQuotesGr",
          data: function(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      

      var AggArticlesQuotes = response.data.Articles;
      $("#div-cont-codegr_description-agg").html("");
      if (AggArticlesQuotes.length > 0) {
        for (var j = 0; j < AggArticlesQuotes.length; j++) {
          var html = "";
          html +=
            '<div class="row no-gutters" data-id-row="' +
            '" id="divcont-' +
            '" >';
          html += '<div class="col-4">';
          //html += '<label for="description' + '">Descrizione: ' + '<span class="description' + '" id="description' + '" value="' + AggArticlesQuotes[j].description + '"</span></label>&nbsp;';
          //html += '<input type="text"  id="description' + '" value="' + AggArticlesQuotes[j].description + '" name="description' + '">';
          html +=
            '<label for="description">Descrizione:<br><span class="description">' +
            AggArticlesQuotes[j].description +
            "</span></label>";
          html += "</div>";
          html += '<div class="col-4">';
          html +=
            '<label for="code_gr">Codice Articolo:<br><span class="code_gr">' +
            AggArticlesQuotes[j].code_gr +
            "</span></label>";
          //html += '<label for="code_gr' + '">Codice Articolo ' + '</label>&nbsp;';
          //html += '<input type="text"  id="code_gr' + '" value="' + AggArticlesQuotes[j].code_gr + '" name="code_gr' + '">';
          html += "</div>";
          html += '<div class="col-4">';
          html +=
            '<label for="qta">Quantità:<br><span class="qta">' +
            AggArticlesQuotes[j].qta +
            "</span></label>";
          //html += '<label for="code_gr' + '">Codice Articolo ' + '</label>&nbsp;';
          //html += '<input type="text"  id="code_gr' + '" value="' + AggArticlesQuotes[j].code_gr + '" name="code_gr' + '">';
          html += "</div>";
          html += "</div>";
          $("#div-cont-codegr_description-agg").append(html);
        }
      }

      var MessagesQuotes = response.data.Messages;
      $("#div-cont-messages").html("");
      if (MessagesQuotes.length > 0) {
        for (var j = 0; j < MessagesQuotes.length; j++) {
          if (storageData.sUserId() == MessagesQuotes[j].id_sender_id) {
            var html = "";
            html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
            html += '<div class="col-12">';
            html += '<div class="chat-history">';
            html += '<ul class="m-b-0">';
            html += '<li class="clearfix"style="    list-style: none;margin-bottom: 30px;">';
            html += '<div class="message-data text-right" style= "margin-bottom: 15px">';
            html += '<span class="message-data-time" style=" color: #434651;padding-left: 6px">' + MessagesQuotes[j].created_at + '</span>';
            html += '<img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="avatar" style="border-radius: 40px;width: 40px;">';
            html += '</div>';
            html += '<div class="message other-message float-right" style="background: #e8f1f3; color: #444;padding: 18px 20px;text-align: right;line-height: 26px;font-size: 16px;border-radius: 7px;display: inline-block; position: relative;"> ' + MessagesQuotes[j].messages + '</div>';
            html += '</li>';
            html += '</ul>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            $("#div-cont-messages").append(html);
          }else{
            if (MessagesQuotes[j].id_sender_id == response.data.id_user_quotes) {
            var html = "";
            html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
            html += '<div class="col-12">';
            html += '<div class="chat-history">';
            html += '<ul class="m-b-0">';
            html += '<li class="clearfix"style="list-style: none;margin-bottom: 30px;">';
            html += '<div class="message-data text-left" style= "margin-bottom: 15px">';
            html += '<span class="message-data-time">' + MessagesQuotes[j].created_at + '</span>';
            html += '</div>';
            html += '<div class="message my-message" style="background: #efefef;color: #444;padding: 18px 20px;line-height: 26px;font-size: 16px;border-radius: 7px;display: inline-block;position: relative;">' + MessagesQuotes[j].messages + '</div>';
            html += '</li>';
            html += '</ul>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
          $("#div-cont-messages").append(html);
         }else{
            var html = "";
            html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
            html += '<div class="col-12">';
            html += '<div class="chat-history">';
            html += '<ul class="m-b-0">';
            html += '<li class="clearfix"style="    list-style: none;margin-bottom: 30px;">';
            html += '<div class="message-data text-right" style= "margin-bottom: 15px">';
            html += '<span class="message-data-time" style=" color: #434651;padding-left: 6px">' + MessagesQuotes[j].created_at + '</span>';
            html += '<img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="avatar" style="border-radius: 40px;width: 40px;">';
            html += '</div>';
            html += '<div class="message other-message float-right" style="background: #e8f1f3; color: #444;padding: 18px 20px;text-align: right;line-height: 26px;font-size: 16px;border-radius: 7px;display: inline-block; position: relative;"> ' + MessagesQuotes[j].messages + '</div>';
            html += '</li>';
            html += '</ul>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            $("#div-cont-messages").append(html);
         }
         }
        }
      /*var MessagesQuotes = response.data.Messages;
      $("#div-cont-messages").html("");
      if (MessagesQuotes.length > 0) {
        for (var j = 0; j < MessagesQuotes.length; j++) {
          var html = "";
          html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
          html += '<div class="col-3">';
          html += '<label for="id_receiver">Ricevente:<br><span class="id_receiver">' + MessagesQuotes[j].id_receiver + "</span></label>";
          html += "</div>";
          html += '<div class="col-3">';
          html += '<label for="id_sender">Io:<br><span class="id_sender">' + MessagesQuotes[j].id_sender + "</span></label>";
          html += "</div>";
          html += '<div class="col-3">';
          html += '<label for="created_at">Data:<br><span class="created_at">' + MessagesQuotes[j].created_at + "</span></label>";
          html += "</div>";
          html += '<div class="col-3">';
          html +=
            '<label for="messages">Messaggio:<br><span class="messages">' + MessagesQuotes[j].messages + "</span></label>";
          html += "</div>";
          html += "</>";
          $("#div-cont-messages").append(html);
         }
      }*/
  }
    });

     $("#frmViewQuotes .discount").on("keyup", function () {
        var discount = $(this).val();
        var str = $("#frmViewQuotes #tot_quote").val();
        var res = str.replace("€ ", "");
        var res1 = res.replace(" Netto + Iva", "");
        CalcolaTot(discount, res1);
      });
      function CalcolaTot(discount, tot_quote) {
        var totale = tot_quote - discount;
        $('#frmViewQuotes #total_amount_with_discount').val('€ '+ totale + ' Netto + Iva');
      }
    $("#frmViewQuotes").show();
    $("#table_list_searchintervention").hide();
    $("#table_list_intervention").hide();
    $("#frmSearchIntervention").hide();
    $("#frmViewProcessingSheet").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
    $("#row-table-fld-pre").hide();
    $("#button-searchintervention").hide();    
  });
}

export function getByViewValue(id) {
  //$('#frmViewProcessingSheet #id').val(id);
  InterventionsServices.getByViewValue(id, function(response) {
    var searchParam = "";
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmViewProcessingSheet #" + i).html(val);
        } else {
          $("#frmViewProcessingSheet #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmViewProcessingSheet #imgPreview").attr("src", val);
            $("#frmViewProcessingSheet #imgName").attr("src", val);
            $("#frmViewProcessingSheet #imgNameLink").attr("href", val);
          } else {
            $("#frmViewProcessingSheet #imgName").remove();
          }
        } else {
          $("#frmViewProcessingSheet #" + i).val(val);
          $("#frmViewProcessingSheet #" + i).html(val);
          $("#frmViewProcessingSheet #defect").prop("disabled", true);
          //$("#frmViewProcessingSheet #imgName" + i).val(val);
        }
      }

      var data = new Date();
      var gg, mm, aaaa;
      gg = data.getDate() + "/";
      mm = data.getMonth() + 1 + "/";
      aaaa = data.getFullYear();

      var HH = data.getHours();
      var MM = data.getMinutes();
      var SS = data.getSeconds();
      parseInt(MM) < 10 ? (MM = "0" + MM) : null;
      parseInt(SS) < 10 ? (SS = "0" + SS) : null;

      $("#div-cont-date_aggs").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-12">';
      html += '<label id="date_aggs" for="date_aggs' + '">Data: ' + response.data.created_at + "</label>&nbsp;";
     // html += '<label id="date_aggs" for="date_aggs' + '">Data: ' + gg + mm + aaaa + " " + HH + ":" + MM + ":" + SS + "</label>&nbsp;";
      //html += '<label for="date_aggs' + '">Data: ' + '</label>&nbsp;';
      //html += '<input type="text" class="form-control" name="date_aggs" id="date_aggs" value="' + gg + mm + aaaa + ' ' + HH + ":" + MM + ":" + SS + '">';
      html += "</div>";
      html += "</div>";
      $("#div-cont-date_aggs").html(html);

      var AggArticles = response.data.Articles;
      if (AggArticles.length > 0) {
        var cont = AggArticles.length;
        $("#div-cont-img-agg").html("");
        for (var j = 0; j < AggArticles.length; j++) {
          var html = "";
          if (j < 15) {
            //cont = cont + 1;
            html +=
              '<div class="row" data-id-row="' +
              j +
              '" id="divcont-' +
              j +
              '" >';
            html += '<div class="col-2">';
            if (j == 0) {
              html += '<label for="code_gr' + j + '">Articolo</label>&nbsp;';
            }
            html +=
              '<select class="custom-select code_gr_select" id="code_gr' +
              j +
              '" value="' +
              AggArticles[j].code_gr +
              '" name="code_gr' +
              j +
              '"></select>';
            html += "</div>";
            html += '<div class="col-4">';
            if (j == 0) {
              html +=
                '<label for="description' + j + '">Descrizione</label>&nbsp;';
            }
            html +=
              '<input type="text"  id="description' +
              j +
              '" value="' +
              AggArticles[j].description +
              '" name="description' +
              j +
              '" class="form-control description' +
              j +
              '">';
            html += "</div>";
         html += '<div class="col-2">';
            if (j == 0) {
              html += '<label class="col col-form-label nopadding" for="n_u' + j + '">N/U</label>';
            }
           // html += '<label class="switch switch-pill switch-primary" style="margin: 2px; vertical-align:bottom;"></label>&nbsp;';
            html += '<select class="form-select form-control" id="n_u' + j + '" value="' + AggArticles[j].n_u + '" name="n_u' + j + '"><option value="Nuovo">Nuovo</option><option value="Usato">Usato</option></select>';
            html += "</div>";
            html += '<div class="col-1">';
            if (j == 0) {
              html += '<label for="qta' + j + '">Quantita</label>&nbsp;';
            }
            html +=
              '<input type="text" class="form-control qta" id="qta' +
              j +
              '" value="' +
              AggArticles[j].qta +
              '" name="qta' +
              j +
              '">';
            html += "</div>";
            html += '<div class="col-1">';
            if (j == 0) {
              html += '<label for="price_list' + j + '">Pr.Unitario</label>';
            }
            html +=
              '<input type="text" class="form-control price_list" id="price_list' +
              j +
              '" value="' +
              AggArticles[j].price_list +
              '" name="price_list' +
              j +
              '">';
            html += "</div>";
            html += '<div class="col-1">';
            if (j == 0) {
              html += '<label for="tot' + j + '">Totale</label>&nbsp;';
            }
            html +=
              '<input type="text" class="form-control tot" id="tot' +
              j +
              '" value="' +
              AggArticles[j].tot +
              '" name="tot' +
              j +
              '">';
            html += "</div>";
            html += '<div class="col-1 text-right">';
            html +=
              '<i class="fa fa-times del-img-agg" data-id-cont="' +
              j +
              '"></i>';
            html += "</div>";
            html += "</div>";
            $("#div-cont-img-agg").append(html);
          }

          $("#frmViewProcessingSheet .tot").on("keyup", function() {
            var contRow = j;
            var tot = 0;

            $("#frmViewProcessingSheet .tot").each(function(index) {
              tot = parseFloat(tot) + parseFloat($(this).val());
            });
            var consumables = $("#frmViewProcessingSheet #consumables").val();
            var tot_value_config = $(
              "#frmViewProcessingSheet #tot_value_config"
            ).val();
            //CalcolaTotale(tot, tot_value_config, consumables, contRow);

            var totaleFinale =
              parseFloat(tot_value_config) +
              parseFloat(consumables) +
              parseFloat(tot);
            $("#frmViewProcessingSheet .tot_final").val(totaleFinale);
          });

          // $.getScript("https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js").done(function(){
          var searchParam = "";
          var initials = [];
           if (AggArticles[j].code_gr != 'null' && AggArticles[j].code_gr != null && AggArticles[j].code_gr != '') { 
            initials.push({
            id: AggArticles[j].code_gr,
            text: AggArticles[j].description_codeGr
          }); 
          }
          $("#code_gr" + j).select2({
            data: initials,
            ajax: {
              url: configData.wsRootServicesUrl + "/api/v1/itemGr/selectitemGr",
              data: function(params) {
                if (params.term == "") {
                  searchParam = "*";
                } else {
                  searchParam = params.term;
                }
                var query = {
                  search: searchParam
                };
                return query;
              },
              processResults: function(data) {
                var dataParse = JSON.parse(data);
                return {
                  results: dataParse.data
                };
              }
            }
          });
          //  });
        }
      }

      $("#frmViewProcessingSheet .qta").on("keyup", function() {
        var contRow = $(this)
          .parent("div")
          .parent("div")
          .attr("data-id-row");
        var qta = $(this).val();
        var price_list = $(
          "#frmViewProcessingSheet #price_list" + contRow
        ).val();
        CalcolaTotaleRiga(qta, price_list, contRow);
      });
      $("#frmViewProcessingSheet .price_list").on("keyup", function() {
        var contRow = $(this)
          .parent("div")
          .parent("div")
          .attr("data-id-row");
        var price_list = $(this).val();
        var qta = $("#frmViewProcessingSheet #qta" + contRow).val();

        CalcolaTotaleRiga(qta, price_list, contRow);
      });

      $("#frmViewProcessingSheet .code_gr_select").on("change", function() {
        var id = $(this).val();
        var contRow = $(this)
          .parent("div")
          .parent("div")
          .attr("data-id-row");
        interventionHelpers.getDescriptionAndPriceProcessingSheet(id, contRow);
      });

      function CalcolaTotaleRiga(qta, price_list, contRow) {
        var tot = qta * price_list;
        $("#frmViewProcessingSheet #tot" + contRow).val(tot);
      }
    });

    $("#frmViewProcessingSheet").show();
    $("#table_list_searchintervention").hide();
    $("#table_list_intervention").hide();
    $("#frmSearchIntervention").hide();
    $("#frmViewQuotes").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#row-table-fld-pre").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
    $("#button-searchintervention").hide();    

  });
}

export function getById(id) {
  // spinnerHelpers.show();
  InterventionsServices.getById(id, function(response) {
 $("#frmUpdateIntervention #date_ddt_gr").on("change", function() {
    if (response.data.rip_association != null) {
  alert("Attenzione, questa riparazione è associata alla riparazione numero "
   +  response.data.rip_association); 
 } 
});
 $("#frmUpdateIntervention #nr_ddt_gr").on("change", function() {
    if (response.data.rip_association != null) {
  alert("Attenzione, questa riparazione è associata alla riparazione numero "
    + response.data.rip_association); 
 } 
});

    var searchParam = "";
    /* SELECT states */
    var initials = [];
    if (response.data.states_id != 'null' && response.data.states_id != null && response.data.states_id != '') { 
    initials.push({
      id: response.data.states_id,
      text: response.data.descriptionStates
    });
  }
    $("#states_id").select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + "/api/v1/States/select",
        data: function(params) {
          if (params.term == "") {
            searchParam = "*";
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });

    /* SELECT ITEMS_ID */
    var initials = [];
    initials.push({
      id: response.data.items_id,
      text: response.data.descriptionItem
    });
    $("#items_id").select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + "/api/v1/itemGr/select",
        data: function(params) {
          if (params.term == "") {
            searchParam = "*";
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });
    /* END SELECT ITEMS_ID */

    /* SELECT user_id */
    var initials = [];
    initials.push({
      id: response.data.user_id,
      text: response.data.descriptionUser
    });
    $("#user_id_intervention").select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + "/api/v1/user/select",
        data: function(params) {
          if (params.term == "") {
            searchParam = "*";
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });
    /* END SELECT user_id */

    /* SELECT referent */
    var initials = [];
    initials.push({
      id: response.data.referent,
      text: response.data.descriptionPeople
    });
    $("#referent").select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + "/api/v1/people/select",
        data: function(params) {
          if (params.term == "") {
            searchParam = "*";
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });
    /* END SELECT referent */

     /* SELECT external_referent_id_intervention */
    var initials = [];
    initials.push({
      id: response.data.external_referent_id_intervention,
      text: response.data.descriptionPeopleReferentExternal
    });
    $("#external_referent_id_intervention").select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl + "/api/v1/people/getSelectExternalReferent",
        data: function(params) {
          if (params.term == "") {
            searchParam = "*";
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });
    /* END SELECT external_referent_id_intervention */


    var searchParam = "";
    $('#user_id_intervention').select2({
      ajax: {
        url: configData.wsRootServicesUrl + '/api/v1/user/select',
        data: function (params) {
          if (params.term == '') {
            searchParam = '*';
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam,
          }
          return query;
        },
        processResults: function (data) {
          var dataParse = JSON.parse(data);
          return {
            results: dataParse.data
          };
        }
      }
    });




    /*SELECT INTERVENTION */
    jQuery.each(response.data, function(i, val) {

      if (val == true) {
        $("#frmUpdateIntervention #" + i).attr("checked", "checked");
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmUpdateIntervention #imgPreview").attr("src", val);
          } else {
            $("#frmUpdateIntervention #imgPreview").remove();
          }
        } else {
          $("#frmUpdateIntervention #" + i).val(val);
        }
      }
    });

    var cont = 0;
    var imgAggCaptures = response.data.captures;
    $("#snapShot").html("");
    if (imgAggCaptures.length > 0) {
      for (var j = 0; j < imgAggCaptures.length; j++) {
        var html = "";
        if (cont < 4) {
          cont = cont + 1;
          html +=
            '<input type="hidden" id="imgBase64' +
            cont +
            '" value="' +
            imgAggCaptures[j].img +
            '" name="imgBase64' +
            cont +
            '">';
          html +=
            '<a href="' +
            imgAggCaptures[j].img +
            '"><img src="' +
            imgAggCaptures[j].img +
            '" style="max-width:50px;"></a>';
          $("#snapShot").append(html);
        }
      }
    }

    lightGallery(document.getElementById("snapShot"));

    $.getScript(
      "https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.js"
    ).done(function() {
      Webcam.set({
        width: 525,
        height: 410,
        image_format: "jpeg",
        jpeg_quality: 100
      });
      Webcam.attach("#camera");
    });

    $("#frmUpdateIntervention #btPic").on("click", function() {
      if (cont < 4) {
        cont = cont + 1;
        Webcam.snap(function(data_uri) {
          $("#snapShot").append(
            '<img id="imgDel' + cont + '" src="' + data_uri + '" width="250px" height="200px" /><i class="fa fa-times del-img-agg" data-id-cont="' + cont + '"></i>'
          );
          $("#snapShot").append(
            '<input type="hidden" value="' + data_uri + '" id="imgBase64' + cont + '" name="imgBase64' + cont + '"/>'
          );
        });
      }
      $("#frmUpdateIntervention .del-img-agg").on("click", function(e) {
	 var idDel = $(this).attr('data-id-cont');
	  
	 var el = $('#imgDel' + idDel);
	 var b64 = $('#imgBase64' + idDel);
	 
	cont = cont - 1;
	 if(cont<0){
	  cont = 0;
	  }
	  el.remove();
	  b64.remove();
	  $(this).remove();
  });
    });


    $("#frmUpdateIntervention").show();
    $("#frmViewUser").hide();
    $("#table_list_intervention").hide();
    $("#frmSearchIntervention").hide();
    $("#table_list_searchintervention").hide();
    $("#frmViewQuotes").hide();
    $("#frmSearchIntervention").hide();
    $(".div-ricerca-intervention").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#row-table-fld-pre").hide();
    $("#table_list_repair_arriving").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
    $("#riparrivo").hide();
  });
}

export function getByFaultModule(id) {
  InterventionsServices.getByFaultModule(id, function (response) {
  
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        if (i == "id") {
          $("#FaultModule #" + i).html(val);
        } else {
          $("#FaultModule #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#FaultModule #imgPreview").attr("src", val);
          } else {
            $("#FaultModule #imgName").remove();
          }
        } else {
          $("#FaultModule #" + i).val(val);
          $("#FaultModule #" + i).html(val);
          }
        } 
    });
    $("#FaultModule").show();
    $("#ModuleValidationGuarantee").hide();
    $("#frmViewUser").hide();
    $("#frmUpdateIntervention").hide();
    $("#table_list_intervention").hide();
    $("#frmSearchIntervention").hide();
    $("#table_list_searchintervention").hide();
    $("#frmViewQuotes").hide();
    $(".div-ricerca-intervention").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#row-table-fld-pre").hide();
     $("#table_list_repair_arriving").hide();
  });
} 

export function getByModuleGuarantee(id) {
  InterventionsServices.getByModuleGuarantee(id, function (response) {
  
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        if (i == "id") {
          $("#ModuleValidationGuarantee #" + i).html(val);
        } else {
          $("#ModuleValidationGuarantee #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#ModuleValidationGuarantee #imgPreview").attr("src", val);
          } else {
            $("#ModuleValidationGuarantee #imgName").remove();
          }
        } else {
          $("#ModuleValidationGuarantee #" + i).val(val);
          $("#ModuleValidationGuarantee #" + i).html(val);
          }
        } 
    });
   
    $("#ModuleValidationGuarantee").show();
    $("#frmViewUser").hide();
    $("#frmUpdateIntervention").hide();
    $("#table_list_intervention").hide();
    $("#frmSearchIntervention").hide();
    $("#table_list_searchintervention").hide();
    $("#frmViewQuotes").hide();
    $(".div-ricerca-intervention").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#row-table-fld-pre").hide();
    $("#table_list_repair_arriving").hide();
    $("#FaultModule").hide();

  });
} 

export function getByIdUser(user_id) {
  // spinnerHelpers.show();
  InterventionsServices.getByIdUser(user_id, function(response) {
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        $("#frmViewUser #" + i).attr("checked", "checked");
      } else {
        $("#frmViewUser #" + i).val(val);
      }
    });
    $("#frmViewUser").show();
    $("#frmUpdateIntervention").hide();
    $("#table_list_intervention").hide();
    $("#table_list_searchintervention").hide();
    $("#frmSearchIntervention").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#ModuleValidationGuarantee").hide();
    $(".div-ricerca-intervention").hide();
    $("#FaultModule").hide();
  });
}

//   ------ region get by -------  //

export function updateCheck(id) {
  spinnerHelpers.show();
  var data = {
    id:id 
  };
    InterventionsServices.updateCheck(data, function() {
    $("#table_list_intervention #ready").val("");
    notificationHelpers.success("Modifica avvenuta con successo");
    window.location.reload();
    spinnerHelpers.hide();
    $("#table_list_intervention").show();
    $("#frmViewQuotes").hide();
    $("#frmViewUser").hide();
    $("#frmUpdateIntervention").hide();
    $("#frmSearchIntervention").hide();
    $("#table_list_searchintervention").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
  });
}


export function updateSelectReferent() {
  spinnerHelpers.show();
  var data = {
    id: $("#frmViewSelectReferent #id").val(),
    id_people: $("#frmViewSelectReferent #id_people").val(),
};

 InterventionsServices.updateSelectReferent(data, function() {
    $("#frmViewSelectReferent #id_people").val("");
    notificationHelpers.success("Modifica avvenuta con successo");
    spinnerHelpers.hide();
    window.location.reload();
    $("#table_list_intervention").show();
    $("#frmViewQuotes").hide();
    $("#frmViewUser").hide();
    $("#frmUpdateIntervention").hide();
    $("#frmSearchIntervention").hide();
    $("#table_list_searchintervention").hide();
    $("#frmViewExternalRepair").hide();
    $("#frmViewSelectReferent").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
  });
}
export function updateExternalRepair() {
  //spinnerHelpers.show();

  var data = {
    id: $("#frmViewExternalRepair #id").val(),
    id_intervention: $("#frmViewExternalRepair #id_intervention").val(),
    id_librone_rip_esterna: $("#frmViewExternalRepair #id_librone_rip_esterna").val(),
    codice_intervento: $("#frmViewExternalRepair #codice_intervento").val(),
    date: $("#frmViewExternalRepair #date").val(),
    ddt_exit: $("#frmViewExternalRepair #ddt_exit").val(),
    id_business_name_supplier: $("#frmViewExternalRepair #id_business_name_supplier").val(),
    date_return_product_from_the_supplier: $("#frmViewExternalRepair #date_return_product_from_the_supplier").val(),
    ddt_supplier: $("#frmViewExternalRepair #ddt_supplier").val(),
    reference_supplier: $("#frmViewExternalRepair #reference_supplier").val(),
    notes_from_repairman: $("#frmViewExternalRepair #notes_from_repairman").val(),
    price: $("#frmViewExternalRepair #price").val(),
  };

 
if (data.id_business_name_supplier == "" || data.id_business_name_supplier == null || data.id_business_name_supplier == 'null') {
    notificationHelpers.error("Attenzione,non puoi salvare, selezionare il fornitore;");
}else{
    InterventionsServices.updateExternalRepair(data, function() {
      $("#frmVieUpdatewRepair #date").val("");
      $("#frmVieUpdatewRepair #ddt_exit").val("");
      $("#frmVieUpdatewRepair #codice_intervento").val("");
      $("#frmVieUpdatewRepair #id_business_name_supplier").val("");
      $("#frmVieUpdatewRepair #date_return_product_from_the_supplier").val("");
      $("#frmVieUpdatewRepair #ddt_supplier").val("");
      $("#frmVieUpdatewRepair #reference_supplier").val("");
      $("#frmVieUpdatewRepair #notes_from_repairman").val("");
      $("#frmVieUpdatewRepair #price").val("");
      notificationHelpers.success("Modifica avvenuta con successo");
      spinnerHelpers.hide();
      window.location.reload();
      $("#table_list_intervention").show();
      $("#frmViewQuotes").hide();
      $("#frmViewUser").hide();
      $("#frmUpdateIntervention").hide();
      $("#frmSearchIntervention").hide();
      $("#table_list_searchintervention").hide();
      $("#frmViewExternalRepair").hide();
      $("#frmViewSelectReferent").hide();
      $("#ModuleValidationGuarantee").hide();
      $("#FaultModule").hide();
    });
  }

}

export function updateInterventionUpdateViewQuotes() {
 // spinnerHelpers.show();
  var data = {
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmViewQuotes #id').val(),
    email: $('#frmViewQuotes #email').val(),
    id_states_quote: $('#frmViewQuotes #id_states_quote').val(),
    object: $('#frmViewQuotes #object').val(),
    date_agg: $('#frmViewQuotes #date_agg').val(),
    code_intervention_gr: $('#frmViewQuotes #code_intervention_gr').val(),
    note_before_the_quote: $('#frmViewQuotes #note_before_the_quote').val(),
    note: $('#frmViewQuotes #note').val(),
    description_payment: $('#frmViewQuotes #description_payment').val(),
    tot_quote: $('#frmViewQuotes #tot_quote').val(),
    discount: $('#frmViewQuotes #discount').val(),
    total_amount_with_discount: $('#frmViewQuotes #total_amount_with_discount').val(),
    note_customer: $('#frmViewQuotes #note_customer').val(), 
    kind_attention: $('#frmViewQuotes #kind_attention').val(), 
    description_and_items_agg: $('#frmViewQuotes #description_and_items_agg').val(), 
  };

    var arrayAggOptions = [];
    for (var j = 0; j < 10; j++) {
      var jsonAggOptions = {};
      var check = false;

      if (functionHelpers.isValued($('#emailreferente' + j).val())) {
        check = true;
        jsonAggOptions['emailreferente'] = $('#emailreferente' + j).val();
      }
      if (functionHelpers.isValued($('#people_id' + j).val())) {
        check = true;
        jsonAggOptions['people_id'] = $('#people_id' + j).val();
      }
      if (functionHelpers.isValued($('#user_id' + j).val())) {
        check = true;
        jsonAggOptions['user_id'] = $('#user_id' + j).val();
      }
      if (check) {
        arrayAggOptions.push(jsonAggOptions);
      }
    }

    data.AggOptions = arrayAggOptions;

  if (data.id_states_quote == "" || data.id_states_quote == null || data.id_states_quote == 'null') {
    notificationHelpers.error("Attenzione,non puoi salvare, seleziona lo stato del preventivo;");
  }else{
  InterventionsServices.updateInterventionUpdateViewQuotes(data, function() {
    $('#frmViewQuotes #email').val('');
    $('#frmViewQuotes #id_states_quote').val('');
    $('#frmViewQuotes #code_intervention_gr').val('');
    $('#frmViewQuotes #object').val('');
    $('#frmViewQuotes #date_agg').val('');
    $('#frmViewQuotes #id_states_quote').val('');
    $('#frmViewQuotes #note_before_the_quote').val('');
    $('#frmViewQuotes #description_payment').val('');
    $('#frmViewQuotes #note').val('');
    $('#frmViewQuotes #discount').val('');
    $('#frmViewQuotes #total_amount_with_discount').val('');
    $('#frmViewQuotes #tot_quote').val('');
    $('#frmViewQuotes #kind_attention').val('');
    $('#frmViewQuotes #description_and_items_agg').val('');
    notificationHelpers.success("Modifica avvenuta con successo");
    spinnerHelpers.hide();
    window.location.reload();
    $("#table_list_intervention").show();
    $("#frmViewQuotes").hide();
    $("#frmViewUser").hide();
    $("#frmUpdateIntervention").hide();
    $("#frmSearchIntervention").show();
    $("#table_list_searchintervention").hide();
    $("#frmViewSelectReferent").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
  });
}
}
export function updateUser() {
  spinnerHelpers.show();
  var data = {
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $("#frmViewUser #id").val(),
    username: $("#frmViewUser #username").val(),
    email: $("#frmViewUser #email").val(),
    fiscal_code: $("#frmViewUser #fiscal_code").val(),
    business_name: $("#frmViewUser #business_name").val(),
    name: $("#frmViewUser #name").val(),
    surname: $("#frmViewUser #surname").val(),
    vat_number: $("#frmViewUser #vat_number").val(),
    address: $("#frmViewUser #address").val(),
    codepostal: $("#frmViewUser #codepostal").val(),
    country: $("#frmViewUser #country").val(),
    province: $("#frmViewUser #province").val(),
    website: $("#frmViewUser #website").val(),
    vat_type_id: $("#frmViewUser #vat_type_id").val(),
    telephone_number: $("#frmViewUser #telephone_number").val(),
    fax_number: $("#frmViewUser #fax_number").val(),
    mobile_number: $("#frmViewUser #mobile_number").val(),
    pec: $("#frmViewUser #pec").val()
  };
  InterventionsServices.updateUser(data, function() {
    $("#frmViewUser #username").val("");
    $("#frmViewUser #email").val("");
    $("#frmViewUser #fiscal_code").val("");
    $("#frmViewUser #business_name").val("");
    $("#frmViewUser #name").val("");
    $("#frmViewUser #surname").val("");
    $("#frmViewUser #vat_number").val("");
    $("#frmViewUser #address").val("");
    $("#frmViewUser #codepostal").val("");
    $("#frmViewUser #country").val("");
    $("#frmViewUser #province").val("");
    $("#frmViewUser #website").val("");
    $("#frmViewUser #vat_type_id").val("");
    $("#frmViewUser #telephone_number").val("");
    $("#frmViewUser #fax_number").val("");
    $("#frmViewUser #mobile_number").val("");
    $("#frmViewUser #pec").val("");
    notificationHelpers.success("Modifica avvenuta con successo");
    spinnerHelpers.hide();
    window.location.reload();
    $("#table_list_intervention").show();
    $("#frmViewQuotes").hide();
    $("#frmViewUser").hide();
    $("#frmUpdateIntervention").hide();
    $("#frmSearchIntervention").hide();
    $("#table_list_searchintervention").hide();
    $("#frmViewSelectReferent").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
  });
}

export function updateInterventionUpdateProcessingSheet() {
  spinnerHelpers.show();
  var search_product = false;
  if ($("#frmViewProcessingSheet #search_product").is(":checked")) {
    search_product = true;
  } else {
    search_product = false;
  }
  var to_call = false;
  if ($("#frmViewProcessingSheet #to_call").is(":checked")) {
    to_call = true;
  } else {
    to_call = false;
  }
  var create_quote = false;
  if ($("#frmViewProcessingSheet #create_quote").is(":checked")) {
    create_quote = true;
  } else {
    create_quote = false;
  }
  var escape_from = false;
  if ($("#frmViewProcessingSheet #escape_from").is(":checked")) {
    escape_from = true;
  } else {
    escape_from = false;
  }
  var working = false;
  if ($("#frmViewProcessingSheet #working").is(":checked")) {
    working = true;
  } else {
    working = false;
  }
  
   var unrepairable = false;
  if ($("#frmViewProcessingSheet #unrepairable").is(":checked")) {
    unrepairable = true;
  } else {
    unrepairable = false;
  }

    var working_without_testing = false;
  if ($('#frmViewProcessingSheet #working_without_testing').is(':checked')) {
    working_without_testing = true;
  } else {
    working_without_testing = false;
  }
  var working_with_testing = false;
  if ($('#frmViewProcessingSheet #working_with_testing').is(':checked')) {
    working_with_testing = true;
  } else {
    working_with_testing = false;
  }
  var data = {
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $("#frmViewProcessingSheet #id").val(),
    flaw_detection: $("#frmViewProcessingSheet #flaw_detection").val(),
    done_works: $("#frmViewProcessingSheet #done_works").val(),
    hour: $("#frmViewProcessingSheet #hour").val(),
    value_config_hour: $("#frmViewProcessingSheet #value_config_hour").val(),
    code_intervention_gr: $("#frmViewProcessingSheet #code_intervention_gr").val(),
    tot_value_config: $("#frmViewProcessingSheet #tot_value_config").val(),
    consumables: $("#frmViewProcessingSheet #consumables").val(),
    tot_final: $("#frmViewProcessingSheet #tot_final").val(),
    date_aggs: $("#frmViewProcessingSheet #date_aggs").val(),
    total_calculated: $("#frmViewProcessingSheet #total_calculated").val(),
    n_u: $("#frmViewProcessingSheet #n_u").val(),
    unrepairable: unrepairable,
    to_call: to_call,
    working_with_testing: working_with_testing,
    working_without_testing: working_without_testing,
    search_product: search_product,
    create_quote: create_quote,
    escape_from: escape_from,
    working: working,
    id_intervention: $("#frmViewProcessingSheet #id").val(),
    description: $("#frmViewProcessingSheet #description").val(),
    qta: $("#frmViewProcessingSheet #qta").val(),
    id_gr_items: $("#frmViewProcessingSheet #code_gr").val(),
    price_list: $("#frmViewProcessingSheet #price_list").val(),
    tot: $("#frmViewProcessingSheet #tot").val()
  };

  var arrayArticles = [];
  for (var i = 0; i < 15; i++) {
    var jsonArticles = {};
    var check = false;
    if (functionHelpers.isValued($("#frmViewProcessingSheet #description" + i).val())) {
      check = true;
      jsonArticles["description"] = $("#frmViewProcessingSheet #description" + i).val();
    }
    if (functionHelpers.isValued($("#code_gr" + i).val())) {
      check = true;
      jsonArticles["code_gr"] = $("#code_gr" + i).val();
    }
    if (functionHelpers.isValued($("#qta" + i).val())) {
      check = true;
      jsonArticles["qta"] = $("#qta" + i).val();
    }
    if (functionHelpers.isValued($("#price_list" + i).val())) {
      check = true;
      jsonArticles["price_list"] = $("#price_list" + i).val();
    }
    if (functionHelpers.isValued($("#tot" + i).val())) {
      check = true;
      jsonArticles["tot"] = $("#tot" + i).val();
    }
     if (functionHelpers.isValued($("#n_u" + i).val())) {
      check = true;
      jsonArticles["n_u"] = $("#n_u" + i).val();
     }
    
    if (check) {
      arrayArticles.push(jsonArticles);
    }
  }
  data.Articles = arrayArticles;
 
  InterventionsServices.updateInterventionUpdateProcessingSheet(
    data,
    function() {
      $("#frmViewProcessingSheet #create_quote").val("");
      $("#frmViewProcessingSheet #escape_from").val("");
      $("#frmViewProcessingSheet #working").val("");
      $("#frmViewProcessingSheet #to_call").val("");
      $("#frmViewProcessingSheet #search_product").val("");      
      $("#frmViewProcessingSheet #flaw_detection").val("");
      $("#frmViewProcessingSheet #done_works").val("");
      $("#frmViewProcessingSheet #hour").val("");
      $("#frmViewProcessingSheet #value_config_hour").val("");
      $("#frmViewProcessingSheet #tot_value_config").val("");
      $("#frmViewProcessingSheet #consumables").val("");
      $("#frmViewProcessingSheet #tot_final").val("");
      $("#frmViewProcessingSheet #date_aggs").val("");
      $("#frmViewProcessingSheet #total_calculated").val("");
      $("#frmViewProcessingSheet #n_u").val("");
      $("#frmViewProcessingSheet #description").val("");
      $("#frmViewProcessingSheet #qta").val("");
      $("#frmViewProcessingSheet #code_gr").val("");
      $("#frmViewProcessingSheet #price_list").val("");
      $("#frmViewProcessingSheet #tot").val("");
      notificationHelpers.success(
        dictionaryHelpers.getDictionary("ContactRequestCompleted")
      );
     spinnerHelpers.hide();
     window.location.reload();
      $("#table_list_intervention").show();
      $("#frmViewQuotes").hide();
      $("#frmViewExternalRepair").hide();
      $("#frmViewProcessingSheet").hide();
      $("#frmViewSelectReferent").hide();
      $("#ModuleValidationGuarantee").hide();
      $("#FaultModule").hide();
    }
  );
}

export function updateIntervention() {
  //spinnerHelpers.show();

  var unrepairable = false;
  if ($("#frmUpdateIntervention #unrepairable").is(":checked")) {
    unrepairable = true;
  } else {
    unrepairable = false;
  }

  var create_quote = false;
  if ($("#frmUpdateIntervention #create_quote").is(":checked")) {
    create_quote = true;
  } else {
    create_quote = false;
  }
  var send_to_customer = false;
  if ($("#frmUpdateIntervention #send_to_customer").is(":checked")) {
    send_to_customer = true;
  } else {
    send_to_customer = false;
  }
  var data = {
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $("#frmUpdateIntervention #id").val(),
    code_intervention_gr: $("#frmUpdateIntervention #code_intervention_gr").val(),
    states_id: $("#frmUpdateIntervention #states_id").val(),
    referent: $("#frmUpdateIntervention #referent").val(),
    external_referent_id_intervention: $("#frmUpdateIntervention #external_referent_id_intervention").val(),
    nr_ddt_gr: $("#frmUpdateIntervention #nr_ddt_gr").val(),
    date_ddt_gr: $("#frmUpdateIntervention #date_ddt_gr").val(),
    customers_id: $("#frmUpdateIntervention #customer_id").val(),
    user_id: $("#frmUpdateIntervention #user_id_intervention").val(),
    items_id: $("#frmUpdateIntervention #items_id").val(),
    //items_id: $("#frmUpdateIntervention #name_product").val(),
    plant_type: $("#frmUpdateIntervention #plant_type").val(),
    trolley_type: $("#frmUpdateIntervention #trolley_type").val(),
    description: $("#frmUpdateIntervention #description").val(),
    series: $("#frmUpdateIntervention #series").val(),
    nr_ddt: $("#frmUpdateIntervention #nr_ddt").val(),
    date_ddt: $("#frmUpdateIntervention #date_ddt").val(),
    defect: $("#frmUpdateIntervention #defect").val(),
    voltage: $("#frmUpdateIntervention #voltage").val(),
    note_internal_gr: $("#frmUpdateIntervention #note_internal_gr").val(),
    unrepairable: unrepairable,
    send_to_customer: send_to_customer,
    create_quote: create_quote,
    exit_notes: $("#frmUpdateIntervention #exit_notes").val(),
    imgmodule: $("#frmUpdateIntervention #imgBase64").val(),
    imgName: $("#frmUpdateIntervention #imgName").val(),
    code_tracking: $("#frmUpdateIntervention #code_tracking").val(),
    rip_association: $("#frmUpdateIntervention #rip_association").val(),
  };

  var file_data = $('#imgmodule').prop('files')[0];
  var arrayImgAgg = [];
  for (var i = 0; i <= 4; i++) {
    if (
      functionHelpers.isValued($("#frmUpdateIntervention #imgBase64" + i).val())
    ) {
      arrayImgAgg.push($("#frmUpdateIntervention #imgBase64" + i).val());
    }
  }
  data.captures = arrayImgAgg;

  if (data.user_id == "" || data.user_id == null || data.user_id == 'null') {
    notificationHelpers.error("Attenzione,non puoi salvare, selezionare il cliente;");
}else{
  InterventionsServices.updateIntervention(data, function() {
    //$('#frmUpdateIntervention #id').val('');
    $("#frmUpdateIntervention #states_id").val("");
    $("#frmUpdateIntervention #note_internal_gr").val("");
    $("#frmUpdateIntervention #user_id_intervention").val("");
    $("#frmUpdateIntervention #referent").val("");
    $("#frmUpdateIntervention #nr_ddt_gr").val("");
    $("#frmUpdateIntervention #date_ddt_gr").val("");
    $("#frmUpdateIntervention #customer_id").val("");
    $("#frmUpdateIntervention #items_id").val("");
    $("#frmUpdateIntervention #plant_type").val("");
    $("#frmUpdateIntervention #trolley_type").val("");
    $("#frmUpdateIntervention #description").val("");
    $("#frmUpdateIntervention #series").val("");
    $("#frmUpdateIntervention #nr_ddt").val("");
    $("#frmUpdateIntervention #date_ddt").val("");
    $("#frmUpdateIntervention #defect").val("");
    $("#frmUpdateIntervention #voltage").val("");
    $("#frmUpdateIntervention #unrepairable").val("");
    $("#frmUpdateIntervention #send_to_customer").val("");
    $("#frmUpdateIntervention #exit_notes").val("");
    $("#frmUpdateIntervention #create_quote").val("");
    $("#frmUpdateIntervention #imgBase64").val("");
    $("#frmUpdateIntervention #code_tracking").val("");
    $("#frmUpdateIntervention #rip_association").val(""); 
    notificationHelpers.success(
      dictionaryHelpers.getDictionary("ContactRequestCompleted")
    );
    spinnerHelpers.hide();
    window.location.reload();
    $("#table_list_intervention").show();
    $("#frmUpdateIntervention").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
    $("#table_list_repair_arriving").show();
  });
}
}

export function deleteRow(id) {
  if (confirm("Vuoi eliminare l'intervento?")) {
    spinnerHelpers.show();
    InterventionsServices.del(id, function(response) {
      notificationHelpers.success(
        dictionaryHelpers.getDictionary("DeleteCompleted")
      );
      spinnerHelpers.hide();
      window.location.reload();
    });
  }
}

export function getDescriptionAndPriceProcessingSheet(id, contRow) {
  InterventionsServices.getDescriptionAndPriceProcessingSheet(id, function(
    response
  ) {
    $("#frmViewProcessingSheet #description" + contRow).val(
      response.data.tipology + ' ' + response.data.note + ' ' + response.data.plant
    );
    $("#frmViewProcessingSheet #price_list" + contRow).val(
      response.data.price_list
    );
  });
}


/***   POST   ***/
export function searchImplemented(
  idIntervention,
  nr_ddt,
  user_id,
  create_quote
) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(
    idIntervention,
    nr_ddt,
    user_id,
    create_quote
  );
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  searchInterventionServices.searchImplemented(data, function(response) {
    for (var i = 0; i < response.data.length; i++) {
      var newList = "";
      newList += "<tr>";
      newList +=
        '<td><button class="btn btn-info" onclick="interventionHelpers.getById(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil"/></button></td>';
      newList += "<td>" + response.data[i].id + "</td>";
      newList += "<td>" + response.data[i].date_ddt + "</td>";
      newList += "<td>" + response.data[i].nr_ddt + "</td>";
      newList +=
        "<td>" +
        response.data[i].descriptionCustomer +
        '<button class="popup-button" data-popup="#popup-remove-account-main" onclick="interventionHelpers.getByIdUser(' +
        response.data[i].user_id +
        ')"><i class="fa fa-search-plus"/></button>' +
        "</td>";
      newList += "<td>" + response.data[i].description + "</td>";
      newList +=
        "<td>" + response.data[i].defect +
        "<a href='" +
        response.data[i].imgmodule +
        "'</a><br>Anteprima</td>";
      newList += "<td>" + response.data[i].nr_ddt_gr + "</td>";
      newList += "<td>" + response.data[i].date_ddt_gr + "</td>";
      newList += "<td>" + response.data[i].create_quote + "</td>";
      newList += "<td>" + response.data[i].descriptionStates + "</td>";
      newList += "<td>" + response.data[i].codeTracking + "</td>";
      newList +=
        '<td><button class="btn btn-danger" onclick="interventionHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
      newList += "</tr>";

      $("#table_list_searchintervention").show();
      $("#table_list_searchintervention tbody").html(newList);
    }
  });

  spinnerHelpers.hide();
}

/***   END POST   ***/

export function getAllRepairArrivedNumberMenuGlobal() {
  InterventionsServices.getAllRepairArrivedNumberMenuGlobal(storageData.sIdLanguage(), function (response) {
    $('#contanumeriid').html(response.data[0].contanumeriid);
  });
}



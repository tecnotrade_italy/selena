/***   userAddressHelpers   ***/

export function insert(idForm) {
  spinnerHelpers.show();
  
  if(functionHelpers.isValued(storageData.sUserId())){
    var data = functionHelpers.formToJson(idForm);
    data.user_id = storageData.sUserId();
    data.cart_id = storageData.sCartId();

    userAddressServices.insert(data, function () {
      notificationHelpers.success('Indirizzo inserito correttamente!');
      $('#modal-add-address').modal('hide');
      spinnerHelpers.hide();
      setTimeout(function(){
        location.reload(true);
      }, 1000);
    });
  }else{
    window.location.href = '/login';
  }
}

export function getAllByUser(idToAppend){
  spinnerHelpers.show();
  if(functionHelpers.isValued(storageData.sUserId())){
    userAddressServices.getAllByUser(storageData.sUserId(), function (result) {
      $('#' + idToAppend).html(result.message);

      /* gestione indirizzi di spedizione */
      $('#' + idToAppend + ' .fa-check').on('click',function(){
        var idAddressActive = $(this).attr('data-id-address');

        if(functionHelpers.isValued(idAddressActive)){
          var data = {
            idAddress: idAddressActive,
            idCart: storageData.sCartId()
          }
          userAddressServices.activeAddressByUser(data, function (result) {
            $('#modal-list-address').modal('hide');
            if(result.data != ""){
              notificationHelpers.success('Indirizzo attivato correttamente!');
              spinnerHelpers.hide();
              setTimeout(function(){
                location.reload(true);
              }, 1000);
            }else{
              spinnerHelpers.hide();
              notificationHelpers.error('Ops, qualcosa è andato storto!');
            }
          });
        }else{
          notificationHelpers.error('Ops, qualcosa è andato storto!');
        }
      });

      $('#' + idToAppend + ' .fa-trash').on('click',function(){
        var idAddressActive = $(this).attr('data-id-address');
        if(confirm("Sei sicuro di voler eliminare l'indirizzo di spedizione?")){
          if(functionHelpers.isValued(idAddressActive)){
            userAddressServices.deleteAddressById(idAddressActive, function (result) {
              $('#modal-list-address').modal('hide');
              if(result.data != ""){
                notificationHelpers.success('Indirizzo eliminato correttamente!');
                spinnerHelpers.hide();
                setTimeout(function(){
                  location.reload(true);
                }, 1000);
              }else{
                spinnerHelpers.hide();
                notificationHelpers.error('Ops, qualcosa è andato storto!');
              }
            });
          }else{
            notificationHelpers.error('Ops, qualcosa è andato storto!');
          }
        }
      });
      
      /* end gestione indirizzi di spedizione */

      spinnerHelpers.hide();
    });
  }else{
    window.location.href = '/login';
  }
}

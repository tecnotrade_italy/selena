/***   configurationHelpers   ***/

export function getValueToSave(value, config) {
  switch (config) {
    case 'CheckBox':
      if (!functionHelpers.isValued(value)) {
        return false
      } else {
        return value
      }
      case 'Select':
        if (functionHelpers.isNumeric(value) && Number(value) > 0) {
          return Number(value);
        } else {
          return value;
        }
        default:
          return value
  }
}

export function getValueForSelect(el) {
  switch (el.service) {
    case 'icons':
      iconServices.getById(el.value, function (result) {
        if (functionHelpers.isValued(result.data)) {
          $(el.$el).append(new Option(result.data.description, result.data.id, false, false)).trigger('change');
        }
      });
      break

    case 'languages':
      this.url = 'language';
      break

    case 'pageShareType':
      pageShareTypeServices.getById(el.value, function (result) {
        if (functionHelpers.isValued(result.data)) {
          $(el.$el).append(new Option(result.data.description, result.data.id, false, false)).trigger('change');
        }
      });
      break

    case 'pageCategory':
      pageCategoryServices.getById(el.value, function (result) {
        if (functionHelpers.isValued(result.data)) {
          $(el.$el).append(new Option(result.data.description, result.data.id, false, false)).trigger('change');
        }
      });
      break

    case 'postalCode':
      postalCodeServices.getById(el.value, function (result) {
        if (functionHelpers.isValued(result.data)) {
          $(el.$el).append(new Option(result.data.description, result.data.id, false, false)).trigger('change');
        }
      });
      break

    case "templateEditorGroup":
      templateEditorGroupServices.getById(el.value, function (result) {
        if (functionHelpers.isValued(result.data)) {
          $(el.$el).append(new Option(result.data.description, result.data.id, false, false)).trigger('change');
        }
      });
      break

    case "templateEditorType":
      templateEditorTypeServices.getById(el.value, function (result) {
        if (functionHelpers.isValued(result.data)) {
          $(el.$el).append(new Option(result.data.description, result.data.id, false, false)).trigger('change');
        }
      });
      break

    case 'users':
      userServices.getById(el.value, function (result) {
        if (functionHelpers.isValued(result.data)) {
          $(el.$el).append(new Option(result.data.name, result.data.id, false, false)).trigger('change');
        }
      });
      break

    case 'vatType':
      vatTypeServices.getById(el.value, function (result) {
        if (functionHelpers.isValued(result.data)) {
          $(el.$el).append(new Option(result.data.name, result.data.id, false, false)).trigger('change');
        }
      });
      // this.url = 'vattype';
      break

    default:
      this.url = '';
      break
  }
}

/***   loginHelpers   ***/

export function login()
{
  spinnerHelpers.show();
  $('#loginPasswordError').hide();

  var sSubmitFormButton = $('#btnLogin');
  var username = $('#loginUsername').val();
  var password = $('#loginPassword').val();

  if (functionHelpers.isValued(sSubmitFormButton)) {
    sSubmitFormButton.text('Login...');
    sSubmitFormButton.prop('disabled', true);
  }

  if ($('#chkSaveLogin').is(':checked')) {
    storageData.setCredetial({
      username: username,
      password: password
    });
  } else {
    storageData.removeCredetial();
  }

  loginServices.login(username, password, function (result) {
      if(result.code=="200"){
        storageData.setTokenKey('Bearer ' + result.access_token);
        storageData.setRefreshTokenKey(result.refresh_token);
        storageData.setExpiresTokenKey(result.expires_at);
        window.setTimeout("loginHelpers.refreshToken(true)", 5 * 60 * 1000);

        storageData.setUserName(result.username);
        storageData.setUserId(result.id);
        storageData.setRole(result.role);

        storageData.removeIdLanguage();
        storageData.setIdLanguage(result.idLanguage);

        storageData.setCartId(result.cart_id);

        if (pageHelpers.isAdminPage() || result.username== 'admin') {
          adminConfigurationServices.getMenu(function (result) {
              storageData.setMenu(result.data);
              if (functionHelpers.isValued(storageData.sHoldUrl())) {
                window.location = '/?' + storageData.sHoldUrl();
                storageData.removeHoldUrl();
              } else {
                if (pageHelpers.isAdminPage()) {
                  window.location = '/admin';
                }else{
                  window.location = '/';
                }
              }
            },
            function (response) {
              if (functionHelpers.isValued(response)) {
                $('#loginPasswordError').text(response);
                $('#loginPasswordError').show();
                notificationHelpers.error(response);
              }

              var sSubmitFormButton = $('#btnLogin');
              sSubmitFormButton.text('Login');
              sSubmitFormButton.prop('disabled', false);
              spinnerHelpers.hide();
            });
        } else {
          if (functionHelpers.isValued(storageData.sHoldUrl())) {
            window.location = '/?' + storageData.sHoldUrl();
            storageData.removeHoldUrl();
          } else {
            window.location = '/';
          }
        }
      } else {
        if (result.code == "404") {
          confirm(result.message);
          } else {
          confirm('Credenziali errate');
          }
        var sSubmitFormButton = $('#btnLogin');
        sSubmitFormButton.text('Login');
        sSubmitFormButton.prop('disabled', false);
        spinnerHelpers.hide();
      }
    },
    function (response) {
      if (response.status == 0) {
        $('#loginPasswordError').text(dictionaryHelpers.getDictionary('UnableToConnectToServer'));
      } else {
        if (functionHelpers.isValued(response.responseText) && jQuery.parseJSON(response.responseText).error_description != undefined) {
          $('#loginPasswordError').text(jQuery.parseJSON(response.responseText).error_description);
        }
      }
      $('#loginPasswordError').show();
    
      var sSubmitFormButton = $('#btnLogin');
      sSubmitFormButton.text('Login');
      sSubmitFormButton.prop('disabled', false);
      spinnerHelpers.hide();
    });
}

export function disconnect() {
  var credential = storageData.sCredetial();
  storageData.removeCredentialData();
  storageData.setCredetial(credential);
  storageData.setCartId('');
  if (window.location.hash != '#logout') {
    storageData.setHoldUrl(window.location.hash);
  }

  if (pageHelpers.isAdminPage()) {
    window.location = '/admin/login';
  } else {
    window.location = '/login';
  }
}

export function insertLoginForGoogle(profile) {
  spinnerHelpers.show(); 

  var data = {
    id_google: profile.getId(),
    name: profile.getGivenName(),
    surname: profile.getFamilyName(),
    name_and_surname: profile.getName(),
    email: profile.getEmail(),
  };

  loginServices.insertLoginForGoogle(data, function (result) {

    if (result.data.original.code == '200') {
      storageData.setTokenKey('Bearer ' + result.data.original.access_token)
      storageData.setRefreshTokenKey(result.data.original.refresh_token)
      storageData.setExpiresTokenKey(result.data.original.expires_at)
      window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000)
      storageData.setUserName(result.data.original.username)
      storageData.setUserDescription(result.data.original.user_description)
      storageData.setUserId(result.data.original.id)
      storageData.setRole(result.data.original.role)
      storageData.removeIdLanguage()
      window.localStorage.setItem('sIdLanguage', result.data.original.idLanguage)
      storageData.setCookie('sIdLanguage', result.data.original.idLanguage , 365);
      storageData.setCartId(result.data.original.cart_id)

      if (result.data.original.redirect == true) {
        window.location = '/modifica-i-tuoi-dati';
      }else{
        window.location = '/area-privata';
      }
    }else{
      if (result.data.original.code == '404') {
        confirm(result.data.original.message);
      } else {
        confirm('Credenziali errate');
      }
    }
  });
}

export function insertLoginForFacebook(response2) {
  spinnerHelpers.show(); 

  var data = {
    email: response2.email,
    first_name: response2.first_name,
    name: response2.name,
    id_facebook: response2.id,
    surname: response2.last_name,
    idUser: storageData.sUserId(),
  };

  loginServices.insertLoginForFacebook(data, function (result) {
    if (result.data.original.code == '200') {
      storageData.setTokenKey('Bearer ' + result.data.original.access_token)
      storageData.setRefreshTokenKey(result.data.original.refresh_token)
      storageData.setExpiresTokenKey(result.data.original.expires_at)
      window.setTimeout('loginHelpers.refreshToken(true)', 5 * 60 * 1000)
      storageData.setUserName(result.data.original.username)
      storageData.setUserDescription(result.data.original.user_description)
      storageData.setUserId(result.data.original.id)
      storageData.setRole(result.data.original.role)
      storageData.removeIdLanguage()
      window.localStorage.setItem('sIdLanguage', result.data.original.idLanguage)
      storageData.setCookie('sIdLanguage', result.data.original.idLanguage , 365);
      storageData.setCartId(result.data.original.cart_id)

      if (result.data.original.redirect == true) {
        window.location = '/modifica-i-tuoi-dati';
      }else{
        window.location = '/area-privata';
      }
    }else{
      if (result.data.original.code == '404') {
        confirm(result.data.original.message);
      } else {
        confirm('Credenziali errate');
      }
    }
  });

}
/***   quotesHelpers   ***/
export function pdfquotes(id) {
  quotesServices.getViewPdfQuotes(id, function (response) {
  });
}
export function SearchQuotes(code_intervention_gr,states_quotes,cliente_quotes){
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(code_intervention_gr,states_quotes,cliente_quotes);

  var html = "";
  quotesServices.SearchQuotes(data, function (response) {
    $("#table_list_quotes tbody").html('');
    if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_quotes tbody").append(newList);
      $("#table_list_quotes").show();
      $("#frmViewQuotes").hide();
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        newList += "<tr>";
       
        newList += "<td>" + response.data[i].code_intervention_gr + "</td>";
        newList += "<td>" + response.data[i].descriptionCustomer + "</td>";
        newList += "<td>" + response.data[i].date_aggs + "</td>";
        newList += "<td>" + response.data[i].description + "</td>";
        newList += "<td>" + response.data[i].note + "</td>";
       // newList += "<td>" + response.data[i].States_Description + "</td>";
        newList += "<td>" + response.data[i].checksendmail + "</td>";
        newList += "</td>";
         newList +=
          '<td><button class="btn btn-info" onclick="quotesHelpers.getByViewQuotes(' +
          response.data[i].id +
          ')"><i class="fa fa-pencil" style="color: #fff!important;"/></button></td>';
        newList +=
          '<td><button class="btn btn-danger" onclick="quotesHelpers.deleteRow(' +
          response.data[i].id +
          ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
        newList += "</tr>";
        $("#table_list_quotes").show();
        $("#frmViewQuotes").hide();
        $("#table_list_quotes tbody").append(newList);
      }
    }
    lightGallery(document.getElementById('lightgallery'));
  });
     spinnerHelpers.hide();
}

export function getAllViewQuote() {
  spinnerHelpers.show();
  quotesServices.getAllViewQuote(storageData.sIdLanguage(), function (response) {
    for (var i = 0; i < response.data.length; i++) {
      var newList = "";
      newList += "<tr>";
     
      newList += "<td>" + response.data[i].code_intervention_gr + "</td>";
      newList += "<td>" + response.data[i].descriptionCustomer + "</td>";
      newList += "<td>" + response.data[i].date_aggs + "</td>";
      newList += "<td>" + response.data[i].description + "</td>";
      newList += "<td>" + response.data[i].note + "</td>";
     // newList += "<td>" + response.data[i].States_Description + "</td>";
      newList += "<td>" + response.data[i].checksendmail + "</td>";
      newList += "</td>";
       newList +=
        '<td><button class="btn btn-info" onclick="quotesHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil" style="color: #fff!important;"/></button></td>';
      newList +=
        '<td><button class="btn btn-danger" onclick="quotesHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
      newList += "</tr>";
      $("#table_list_quotes").show();
      $("#frmViewQuotes").hide();
      $("#table_list_quotes tbody").append(newList);
    }
    lightGallery(document.getElementById('lightgallery'));
  });

  spinnerHelpers.hide();

}

export function getByViewQuotes(id) {
  quotesServices.getByViewQuotes(id,storageData.sUserId(), function (response) {
    console.log(response);
    var searchParam = "";
    var AggNoteBeforeTheQuotes = [];
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmViewQuotes #" + i).html(val);
        } else {
          $('#frmViewQuotes #' + i).attr('checked', 'checked');
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmViewQuotes #imgPreview").attr('src', val);
          } else {
            $("#frmViewQuotes #imgName").remove();
          }
        } else {
          $("#frmViewQuotes #" + i).val(val);
          $("#frmViewQuotes #" + i).html(val);
        }
      }

      var AggOptions = response.data.ReferentEmailSendPreventivi;
      if (AggOptions.length > 0) {
              $("#emailReferent").html("");
              for (var j = 0; j < AggOptions.length; j++) {
                var html = "";
                if (j < 10) {
            html += '<div class="row" style="margin-bottom: 10px;margin-top: 7px;" data-id-row="' + '" id="divcont-' +' " >';
            html += '<div class="col-10 text-left">';
            html += '<label class="labelclass">Email</label>';
            html += '<input type="hidden" class="form-control cost" id="user_id' + j + '" value="' + AggOptions[j].user_id + '" name="user_id' + j + '">';
            html += '<input type="hidden" class="form-control cost" id="people_id' + j + '" value="' + AggOptions[j].people_id + '" name="people_id' + j + '">';
            html += '<input type="text" class="form-control cost" id="emailreferente' + j + '" value="' + AggOptions[j].EmailReferente + '" name="emailreferente' + j + '">';
            html += "</div>";          
            html += '<div class="col-2">';
            html += '<button type="button" style="margin-top: 30px;border-color: #fff;" class="btn btn-outline-danger del-img-agg" data-id-cont="' + AggOptions[j].people_id + '" id="btndelete' + AggOptions[j].people_id + '"><i class="fa fa-times"></i></button>'
            html += "</div>";
            html += "</div>";
            $("#emailReferent").append(html);
          }

          $("#frmViewQuotes .del-img-agg").on("click", function(e) {
            var idDel = $(this).attr('data-id-cont');
            var el = $(this).parent('div').parent('div').attr('data-id-row', idDel);
            
            AggOptions = AggOptions - 1;
            if(AggOptions<0){
              AggOptions = 0;
            }
            el.remove();
          });

        }
      }

      $('#div-cont-updated_id').html('');
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-5" style="text-align: left;">';
      html += '<span for="created_id' + '"><strong>Preventivo Creato Da: ' + '</strong></span>';
      html += '<input type="text" class="form-control" style="border: #fff;" name="created_id" id="created_id" value="' + response.data.created_id + '" readonly>';
      html += "</div>";
     // html += '<div class="col-4">';
     // html += '<span for="kind_attention' + '"><strong> Alla c.a: ' + '</strong></span>';
      //html += '<input type="text" class="form-control" style="border: #fff;" name="kind_attention" id="kind_attention" value="' + response.data.kind_attention + '">';
      //html += "</div>";
      html += "</div>";
      $('#div-cont-updated_id').append(html);

      AggNoteBeforeTheQuotes = response.data;
      $('#div-cont-note_before_the_quote').html('');
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-12">';
      html += '<textarea class="form-control" id="note_before_the_quote">' + AggNoteBeforeTheQuotes.note_before_the_quote + '</textarea>';
     // html += '<textarea class="form-control" id="note_before_the_quote">' + "In riferimento al Vs DDT n. " + AggNoteBeforeTheQuotes.nr_ddt + "  del " + AggNoteBeforeTheQuotes.date_ddt + " con la presente siamo ad inviarVi il seguente preventivo di riparazione relativo: " /*+ AggNoteBeforeTheQuotes.description*/ + AggNoteBeforeTheQuotes.note_before_the_quotes_agg + '</textarea>';
      html += "</div>";
      html += "</div>";
      $('#div-cont-note_before_the_quote').append(html);


      var AggTotale = response.data;
      var a = AggTotale.tot_quote;
      var b = AggTotale.discount;
      var c = AggTotale.tot_quote - AggTotale.discount;
      $('#div-cont-totale').html('');
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<span for="tot_quote' + '"><strong>Totale: ' + '</strong></span>&nbsp;';
      //if(AggTotale.tot_quote==''){
      html += '<input type="text" class="form-control tot_quote" name="tot_quote" id="tot_quote" value="' + '€ ' + AggTotale.tot_quote + ' Netto + Iva" readonly>';
      // }else{
      // html += '<input type="text" class="form-control" name="tot_quote" id="tot_quote" value="'+ AggTotale.tot_quote + '">';
      //}
      html += "</div>";
      html += '<div class="col-6">';
      html += '<span for="discount' + '"><strong>Sconto € : ' + '</strong></span>&nbsp;';
      html += '<input type="text" class="form-control discount" name="discount" id="discount" value="' + AggTotale.discount + '">';
      html += "</div>";
      html += '<div class="col-6">';
      html += '<span for="total_amount_with_discount' + '"><strong>Totale Scontato: ' + '</strong></span>&nbsp;';
      html += '<input type="text" class="form-control total_amount_with_discount" name="total_amount_with_discount" id="total_amount_with_discount" value="' + '€ ' + AggTotale.total_amount_with_discount + ' Netto + Iva">';
      html += "</div>";
      html += "</div>";
      $('#div-cont-totale').append(html);

      var data = new Date();
      var gg, mm, aaaa;
      gg = data.getDate() + "/";
      mm = data.getMonth() + 1 + "/";
      aaaa = data.getFullYear();

      $('#div-cont-date_agg').html('');
      var html = "";
      html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<span for="date_agg' + '"><strong>Data: ' + '</strong></span><br>';
      html += '<input type="text" class="form-control" name="date_agg" id="date_agg" value="' + response.data.date_agg + '">';
      html += "</div>";
      html += "</div>";
      $('#div-cont-date_agg').append(html);

      /*$('#date_agg').datetimepicker({
      format: 'DD/MM/YYYY HH:mm'
      });*/

      var AggPaymentQuotes = response.data;
      $('#div-cont-payment_quote').html('');
      var html = "";
      html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-8">';
      html += '<span for="description_payment' + '"><strong>Descrizione Pagamento ' + '</strong></span>&nbsp;';
      html += '<select class="form-control select-graphic custom-select code_gr_select" id="description_payment' + '" value="' + AggPaymentQuotes.description_payment + '" name="description_payment' + '"></select>';
      html += "</div>";
      html += "</div>";
      $('#div-cont-payment_quote').append(html);

      var searchParam = "";
      var initials = [];
      initials.push({id: AggPaymentQuotes.description_payment, text: AggPaymentQuotes.Description_payment});
      $('#description_payment').select2({
        data: initials,
        ajax: {
          url: configData.wsRootServicesUrl + '/api/v1/paymentGr/selectpaymentGr',
          data: function (params) {
            if (params.term == '') {
              searchParam = '*';
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam,
            }
            return query;
          },
          processResults: function (data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var searchParam = "";
      $('#description_payment').select2({
        ajax: {
          url: configData.wsRootServicesUrl + '/api/v1/paymentGr/selectpaymentGr',
          data: function (params) {
            if (params.term == '') {
              searchParam = '*';
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam,
            }
            return query;
          },
          processResults: function (data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var searchParam = "";
      var initials = [];
      initials.push({
        id: AggPaymentQuotes.id_states_quote,
        text: AggPaymentQuotes.States_Description
      });
      $('#id_states_quote').select2({
        data: initials,
        ajax: {
          url: configData.wsRootServicesUrl + '/api/v1/StatesQuotesGr/selectStatesQuotesGr',
          data: function (params) {
            if (params.term == '') {
              searchParam = '*';
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam,
            }
            return query;
          },
          processResults: function (data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var AggArticlesQuotes = response.data.Articles;
      $('#div-cont-codegr_description-agg').html('');
      if (AggArticlesQuotes.length > 0) {
        for (var j = 0; j < AggArticlesQuotes.length; j++) {
          var html = "";
          html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
          html += '<div class="col-4">';
          //html += '<label for="description' + '">Descrizione: ' + '<span class="description' + '" id="description' + '" value="' + AggArticlesQuotes[j].description + '"</span></label>&nbsp;';
          //html += '<input type="text"  id="description' + '" value="' + AggArticlesQuotes[j].description + '" name="description' + '">';
          html += '<span for="description"><strong>Descrizione:<br><span class="description">' + AggArticlesQuotes[j].description + '</strong></span></span>';
          html += "</div>";
          html += '<div class="col-4">';
          html += '<span for="code_gr"><strong>Codice Articolo:<br><span class="code_gr">' + AggArticlesQuotes[j].code_gr + '</strong></span></span>';
          //html += '<label for="code_gr' + '">Codice Articolo ' + '</label>&nbsp;';
          //html += '<input type="text"  id="code_gr' + '" value="' + AggArticlesQuotes[j].code_gr + '" name="code_gr' + '">';
          html += "</div>";
          html += '<div class="col-4">';
          html += '<label for="qta">Quantità:<br><span class="qta">' + AggArticlesQuotes[j].qta + '</span></label>';
          html += "</div>";
          html += "</div>";
          $('#div-cont-codegr_description-agg').append(html);
        }
      }

      var MessagesQuotes = response.data.Messages;
      $("#div-cont-messages").html("");
      if (MessagesQuotes.length > 0) {
        for (var j = 0; j < MessagesQuotes.length; j++) {
          if (storageData.sUserId() == MessagesQuotes[j].id_sender_id) {
            var html = "";
            html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
            html += '<div class="col-12">';
            html += '<div class="chat-history">';
            html += '<ul class="m-b-0">';
            html += '<li class="clearfix"style="    list-style: none;margin-bottom: 30px;">';
            html += '<div class="message-data text-right" style= "margin-bottom: 15px">';
            html += '<span class="message-data-time" style=" color: #434651;padding-left: 6px">' + MessagesQuotes[j].created_at + '</span>';
            html += '<img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="avatar" style="border-radius: 40px;width: 40px;">';
            html += '</div>';
            html += '<div class="message other-message float-right" style="background: #e8f1f3; color: #444;padding: 18px 20px;text-align: right;line-height: 26px;font-size: 16px;border-radius: 7px;display: inline-block; position: relative;"> ' + MessagesQuotes[j].messages + '</div>';
            html += '</li>';
            html += '</ul>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            $("#div-cont-messages").append(html);
          }else{
            if (MessagesQuotes[j].id_sender_id == response.data.id_user_quotes) {
            var html = "";
            html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
            html += '<div class="col-12">';
            html += '<div class="chat-history">';
            html += '<ul class="m-b-0">';
            html += '<li class="clearfix"style="list-style: none;margin-bottom: 30px;">';
            html += '<div class="message-data text-left" style= "margin-bottom: 15px">';
            html += '<span class="message-data-time">' + MessagesQuotes[j].created_at + '</span>';
            html += '</div>';
            html += '<div class="message my-message" style="background: #efefef;color: #444;padding: 18px 20px;line-height: 26px;font-size: 16px;border-radius: 7px;display: inline-block;position: relative;">' + MessagesQuotes[j].messages + '</div>';
            html += '</li>';
            html += '</ul>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
          $("#div-cont-messages").append(html);
         }else{
            var html = "";
            html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
            html += '<div class="col-12">';
            html += '<div class="chat-history">';
            html += '<ul class="m-b-0">';
            html += '<li class="clearfix"style="    list-style: none;margin-bottom: 30px;">';
            html += '<div class="message-data text-right" style= "margin-bottom: 15px">';
            html += '<span class="message-data-time" style=" color: #434651;padding-left: 6px">' + MessagesQuotes[j].created_at + '</span>';
            html += '<img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="avatar" style="border-radius: 40px;width: 40px;">';
            html += '</div>';
            html += '<div class="message other-message float-right" style="background: #e8f1f3; color: #444;padding: 18px 20px;text-align: right;line-height: 26px;font-size: 16px;border-radius: 7px;display: inline-block; position: relative;"> ' + MessagesQuotes[j].messages + '</div>';
            html += '</li>';
            html += '</ul>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            $("#div-cont-messages").append(html);
         }
         }
        }
      /*var MessagesQuotes = response.data.Messages;
      $("#div-cont-messages").html("");
      if (MessagesQuotes.length > 0) {
        for (var j = 0; j < MessagesQuotes.length; j++) {
          var html = "";
          html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
          html += '<div class="col-3">';
          html += '<label for="id_receiver">Ricevente:<br><span class="id_receiver">' + MessagesQuotes[j].id_receiver + "</span></label>";
          html += "</div>";
          html += '<div class="col-3">';
          html += '<label for="id_sender">Io:<br><span class="id_sender">' + MessagesQuotes[j].id_sender + "</span></label>";
          html += "</div>";
          html += '<div class="col-3">';
          html += '<label for="created_at">Data:<br><span class="created_at">' + MessagesQuotes[j].created_at + "</span></label>";
          html += "</div>";
          html += '<div class="col-3">';
          html +=
            '<label for="messages">Messaggio:<br><span class="messages">' + MessagesQuotes[j].messages + "</span></label>";
          html += "</div>";
          html += "</>";
          $("#div-cont-messages").append(html);
         }
      }*/
  }
    });
      $("#frmViewQuotes .discount").on("keyup", function () {
        var discount = $(this).val();

        var str = $("#frmViewQuotes #tot_quote").val();
        var res = str.replace("€ ", "");
        var res1 = res.replace(" Netto + Iva", "");
        CalcolaTot(discount, res1);
      });
      function CalcolaTot(discount, tot_quote) {
        var totale = tot_quote - discount;
        $('#frmViewQuotes #total_amount_with_discount').val('€ '+ totale + ' Netto + Iva');
      }
    $("#frmViewQuotes").show();
    $("#table_list_quotes").hide();
    $("#preventivo").hide();
    $("#preventivohr").hide();
    $("#frmSearchQuotes").hide();
  
    
  });
}

export function updateInterventionUpdateViewQuotes() {

  var data = {
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmViewQuotes #id').val(),
    email: $('#frmViewQuotes #email').val(),
    id_states_quote: $('#frmViewQuotes #id_states_quote').val(),
    code_intervention_gr: $('#frmViewQuotes #code_intervention_gr').val(),
    object: $('#frmViewQuotes #object').val(),
    date_agg: $('#frmViewQuotes #date_agg').val(),
    note_before_the_quote: $('#frmViewQuotes #note_before_the_quote').val(),
    note: $('#frmViewQuotes #note').val(),
    description_payment: $('#frmViewQuotes #description_payment').val(),
    tot_quote: $('#frmViewQuotes #tot_quote').val(),
    discount: $('#frmViewQuotes #discount').val(),
    total_amount_with_discount: $('#frmViewQuotes #total_amount_with_discount').val(),
    note_customer: $('#frmViewQuotes #note_customer').val(), 
    kind_attention: $('#frmViewQuotes #kind_attention').val(), 
    description_and_items_agg: $('#frmViewQuotes #description_and_items_agg').val(), 
  };

  var arrayAggOptions = [];
  for (var j = 0; j < 10; j++) {
    var jsonAggOptions = {};
    var check = false;

    if (functionHelpers.isValued($('#emailreferente' + j).val())) {
      check = true;
      jsonAggOptions['emailreferente'] = $('#emailreferente' + j).val();
    }
    if (functionHelpers.isValued($('#people_id' + j).val())) {
      check = true;
      jsonAggOptions['people_id'] = $('#people_id' + j).val();
    }
    if (functionHelpers.isValued($('#user_id' + j).val())) {
      check = true;
      jsonAggOptions['user_id'] = $('#user_id' + j).val();
    }
    if (check) {
      arrayAggOptions.push(jsonAggOptions);
    }
  }

  data.AggOptions = arrayAggOptions;


  if (data.id_states_quote == "" || data.id_states_quote == null || data.id_states_quote == 'null') {
    notificationHelpers.error("Attenzione,non puoi salvare, seleziona lo stato del preventivo;");
}else{
  quotesServices.updateInterventionUpdateViewQuotes(data, function () {
    $('#frmViewQuotes #email').val('');
    $('#frmViewQuotes #id_states_quote').val('');
    $('#frmViewQuotes #object').val('');
    $('#frmViewQuotes #date_agg').val('');
    $('#frmViewQuotes #id_states_quote').val('');
    $('#frmViewQuotes #note_before_the_quote').val('');
    $('#frmViewQuotes #description_payment').val('');
    $('#frmViewQuotes #note').val('');
    $('#frmViewQuotes #discount').val('');
    $('#frmViewQuotes #total_amount_with_discount').val('');
    $('#frmViewQuotes #tot_quote').val('');
    $('#frmViewQuotes #kind_attention').val('');
    $('#frmViewQuotes #description_and_items_agg').val('');
    notificationHelpers.success("Modifica avvenuta con successo");
    spinnerHelpers.hide();
     window.location.reload();
    $("#table_list_quotes").show();
    $("#frmViewQuotes").hide();
  });
}
}


export function updateandSendEmailQuotes() {
  spinnerHelpers.show();
  var data = {
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmViewQuotes #id').val(),
    email: $('#frmViewQuotes #email').val(),
    id_states_quote: $('#frmViewQuotes #id_states_quote').val(),
    object: $('#frmViewQuotes #object').val(),
    date_agg: $('#frmViewQuotes #date_agg').val(),
    note_before_the_quote: $('#frmViewQuotes #note_before_the_quote').val(),
    note: $('#frmViewQuotes #note').val(),
    description_payment: $('#frmViewQuotes #description_payment').val(),
    tot_quote: $('#frmViewQuotes #tot_quote').val(),
    discount: $('#frmViewQuotes #discount').val(),
    total_amount_with_discount: $('#frmViewQuotes #total_amount_with_discount').val(),
    note_customer: $('#frmViewQuotes #note_customer').val(), 
    kind_attention: $('#frmViewQuotes #kind_attention').val(), 
    //params for send mail
    description: $('#frmViewQuotes #description').val(),
    id_customer: $('#frmViewQuotes #id_customer').val(),
    code_gr: $('#frmViewQuotes #code_gr').val(),
    //end params for send mail
  };

  var arrayAggOptions = [];
  for (var j = 0; j < 10; j++) {
    var jsonAggOptions = {};
    var check = false;

    if (functionHelpers.isValued($('#emailreferente' + j).val())) {
      check = true;
      jsonAggOptions['emailreferente'] = $('#emailreferente' + j).val();
    }
    if (functionHelpers.isValued($('#people_id' + j).val())) {
      check = true;
      jsonAggOptions['people_id'] = $('#people_id' + j).val();
    }
    if (functionHelpers.isValued($('#user_id' + j).val())) {
      check = true;
      jsonAggOptions['user_id'] = $('#user_id' + j).val();
    }
    if (check) {
      arrayAggOptions.push(jsonAggOptions);
    }
  }
  data.AggOptions = arrayAggOptions;

  quotesServices.updateandSendEmailQuotes(data, function () {
    $('#frmViewQuotes #email').val('');
    $('#frmViewQuotes #id_states_quote').val('');
    $('#frmViewQuotes #object').val('');
    $('#frmViewQuotes #date_agg').val('');
    $('#frmViewQuotes #id_states_quote').val('');
    $('#frmViewQuotes #note_before_the_quote').val('');
    $('#frmViewQuotes #description_payment').val('');
    $('#frmViewQuotes #note').val('');
    $('#frmViewQuotes #discount').val('');
    $('#frmViewQuotes #total_amount_with_discount').val('');
    $('#frmViewQuotes #tot_quote').val('');
    $('#frmViewQuotes #kind_attention').val('');
    notificationHelpers.success("Modifica avvenuta con successo");
    spinnerHelpers.hide();
     window.location.reload();
    $("#table_list_quotes").show();
    $("#frmViewQuotes").hide();
  });
}

export function deleteRow(id) {
  if (confirm("Vuoi eliminare il preventivo?")) {
    spinnerHelpers.show();
    quotesServices.del(id, function (response) {
      notificationHelpers.success(dictionaryHelpers.getDictionary("DeleteCompleted"));
      spinnerHelpers.hide();
      window.location.reload();
    });
  }
}


export function insertReferent() {
  spinnerHelpers.show(); 

  var data = {
    idUserConnection: storageData.sUserId(),
    id_user: $('#frmViewQuotes #id_user_quotes').val(),
    name: $('#frmInsertReferent #name').val(),
    surname: $('#frmInsertReferent #surname').val(),
    email: $('#frmInsertReferent #email').val(),
    password: $('#frmInsertReferent #password').val(),
    phone_number: $('#frmInsertReferent #phone_number').val(),
    ref: $('#frmInsertReferent #ref').val(),
  };
    console.log(data);

    if (confirm("Vuoi confermarel'inserimento del referente?")) {
        quotesServices.insertReferent(data, function () {
        $('#frmInsertReferent #name').val('');
        $('#frmInsertReferent #surname').val('');
        $('#frmInsertReferent #email').val('');
        $('#frmInsertReferent #password').val('');
        $('#frmInsertReferent #phone_number').val('');
        $('#frmInsertReferent #ref').val('');
        quotesHelpers.getByViewQuotes($('#frmViewQuotes #id').val());
        notificationHelpers.success("Inserimento del referente eseguito con successo!");
        spinnerHelpers.hide();
        $("#frmInsertReferent").modal('hide');
        $("#frmViewQuotes").show();
      });
    }

}




export function insertMessageQuotes() {
  spinnerHelpers.show(); 
  var data = {
    id_sender: storageData.sUserId(),
    id_intervention: $('#frmViewQuotes #id').val(),
    messages: $('#frmViewQuotes #messages').val(),
    id_customer: $('#frmViewQuotes #id_customer').val(),
  };

  if (confirm("Vuoi confermare l'invio del messaggio?")) {
    quotesServices.insertMessageQuotes(data, function () {
      $('#frmViewQuotes #messages').val('');
      quotesHelpers.getByViewQuotes($('#frmViewQuotes #id').val());
      notificationHelpers.success("Invio del messaggio al destinatario eseguito con successo!");
      spinnerHelpers.hide();
      $("#frmViewQuotes").show();
    });
  }
}


/***   END POST   ***/

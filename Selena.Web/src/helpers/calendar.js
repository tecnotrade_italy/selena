/***   calendarHelpers   ***/

export function selectMonth() {
  return [{
    id: 1,
    text: dictionaryHelpers.getDictionary("January")
  }, {
    id: 2,
    text: dictionaryHelpers.getDictionary("February")
  }, {
    id: 3,
    text: dictionaryHelpers.getDictionary("March")
  }, {
    id: 4,
    text: dictionaryHelpers.getDictionary("April")
  }, {
    id: 5,
    text: dictionaryHelpers.getDictionary("May")
  }, {
    id: 6,
    text: dictionaryHelpers.getDictionary("June")
  }, {
    id: 7,
    text: dictionaryHelpers.getDictionary("July")
  }, {
    id: 8,
    text: dictionaryHelpers.getDictionary("August")
  }, {
    id: 9,
    text: dictionaryHelpers.getDictionary("September")
  }, {
    id: 10,
    text: dictionaryHelpers.getDictionary("October")
  }, {
    id: 11,
    text: dictionaryHelpers.getDictionary("November")
  }, {
    id: 12,
    text: dictionaryHelpers.getDictionary("December")
  }];
}

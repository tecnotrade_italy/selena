/***   languageHelpers   ***/

export function getSelect2ForBootstrapTable() {
  return {
    ajax: {
      type: 'GET',
      url: configData.wsRootServicesUrl + '/api/v1/language/select',
      dataType: 'json',
      headers: {
        Accept: "application/json",
        Authorization: storageData.sTokenKey()
      },
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      data: function (params) {
        return {
          search: $.trim(params.term)
        };
      },
      processResults: function (result) {
        return {
          results: result.data
        };
      }
    },
    // allowClear: true,
    minimumInputLength: 3,
    placeholder: dictionaryHelpers.getDictionary('Select2Empty')
  };
}

export function select2(selector) {
  $(selector).select2(languageHelpers.getSelect2ForBootstrapTable());
}

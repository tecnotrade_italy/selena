//  supplierregistryjsHelpers
//
export function getAllSupplierRegistrySearch(business_name,telephone_number,mobile_phone) {
  spinnerHelpers.show();
    var data = functionHelpers.formToJson(business_name,telephone_number,mobile_phone);
  data["idLanguage"] = storageData.sIdLanguage();
  supplierregistryServices.getAllSupplierRegistrySearch(data, function (response) {
      $( "#table_list_supplier_registry tbody").html('');
     if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
       newList += '<td></td>';
       newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_supplier_registry tbody").append(newList);
      $("#table_list_supplier_registry").show();
      $("#frmViewSupplierRegistry").hide();
      $("#frmUpdateSupplierRegistry").hide();
    } else {
       for (var i = 0; i < response.data.length; i++) {
         var newList = "";
         newList += "<tr>";
         newList +=
           '<td><button class="btn btn-info" onclick="supplierregistryHelpers.getByIdUp(' +
           response.data[i].id +
           ')"><i class="fa fa-pencil" style="color:#fff!important;"/></button></td>';
         //newList += "<td>" + response.data[i].id + "</td>";
         newList += '<td><a  style="cursor:pointer;white-space: nowrap; color:#20a8d8 !important;" onclick="supplierregistryHelpers.getById(' +
        response.data[i].id +
           ')">' + response.data[i].business_name + '</a></td>';
 
         newList += "<td>" + response.data[i].telephone_number + "</td>";
         newList += "<td>" + response.data[i].mobile_phone + "</td>";
         newList += "<td>" + response.data[i].fax_number + "</td>";
         newList += "<td>" + response.data[i].email + "</td>";
      
         newList +=
           '<td><button class="btn btn-danger" onclick="supplierregistryHelpers.deleteRow(' +
           response.data[i].id +
           ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
         newList += "</tr>";
         $("#table_list_supplier_registry tbody").append(newList);
         $("#table_list_supplier_registry").show();
         $("#frmViewSupplierRegistry").hide();
         $("#frmUpdateSupplierRegistry").hide();
       }
    }
    lightGallery(document.getElementById('lightgallery'));
  });

  spinnerHelpers.hide();
}

export function getById(id) {
  supplierregistryServices.getById(id, function (response) {
  var searchParam = "";
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmViewSupplierRegistry #" + i).html(val);
        } else {
          $("#frmViewSupplierRegistry #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmViewSupplierRegistry #imgPreview").attr("src", val);
          } else {
            $("#frmViewSupplierRegistry #imgName").remove();
          }
        } else {
          $("#frmViewSupplierRegistry #" + i).val(val);
          $("#frmViewSupplierRegistry #" + i).html(val);
          }
        } 
     });
     $("#frmViewSupplierRegistry").show();
     $("#table_list_supplier_registry").hide();
     $("#frmUpdateSupplierRegistry").hide();
     $("#frmSearchSupplier").hide();
     $("#anagraficafornit").hide();

    
  });
}
export function getByIdUp(id) {
  supplierregistryServices.getByIdUp(id, function (response) {
  var searchParam = "";
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmUpdateSupplierRegistry #" + i).html(val);
        } else {
          $("#frmUpdateSupplierRegistry #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmUpdateSupplierRegistry #imgPreview").attr("src", val);
          } else {
            $("#frmUpdateSupplierRegistry #imgName").remove();
          }
        } else {
          $("#frmUpdateSupplierRegistry #" + i).val(val);
          $("#frmUpdateSupplierRegistry #" + i).html(val);
          }
        } 
    });
     $("#frmUpdateSupplierRegistry").show();
     $("#frmViewSupplierRegistry").hide();
     $("#frmSearchSupplier").hide();
     $("#table_list_supplier_registry").hide();
     $("#anagraficafornit").hide();
  });
}

export function updatedetailsupplierregistry() {
  spinnerHelpers.show();
  var data = {
    id: $("#frmUpdateSupplierRegistry #id").val(),
    business_name: $("#frmUpdateSupplierRegistry #business_name").val(),
    name: $("#frmUpdateSupplierRegistry #name").val(),
    surname: $("#frmUpdateSupplierRegistry #surname").val(),
    address: $("#frmUpdateSupplierRegistry #address").val(),
    code_postal: $("#frmUpdateSupplierRegistry #code_postal").val(),
    country: $("#frmUpdateSupplierRegistry #country").val(),
    province: $("#frmUpdateSupplierRegistry #province").val(),
    telephone_number: $("#frmUpdateSupplierRegistry #telephone_number").val(),
    mobile_phone: $("#frmUpdateSupplierRegistry #mobile_phone").val(),
    fax_number: $("#frmUpdateSupplierRegistry #fax_number").val(),
    email: $("#frmUpdateSupplierRegistry #email").val(),
};

 supplierregistryServices.updatedetailsupplierregistry(data, function() {
   $("#frmUpdateSupplierRegistry #id").val("");
    notificationHelpers.success("Modifica avvenuta con successo");

    spinnerHelpers.hide();
    window.location.reload();
    $("#table_list_supplier_registry").show();
    $("#frmViewSupplierRegistry").hide();
    $("#frmUpdateSupplierRegistry").hide();
  });
}


  export function insertsupplierregistry() {
  spinnerHelpers.show();
  
  var data = {
    created_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmAddSupplierRegistry #id').val(),
    business_name: $("#frmAddSupplierRegistry #business_name").val(),
    name: $("#frmAddSupplierRegistry #name").val(),
    surname: $("#frmAddSupplierRegistry #surname").val(),
    address: $("#frmAddSupplierRegistry #address").val(),
    code_postal: $("#frmAddSupplierRegistry #code_postal").val(),
    country: $("#frmAddSupplierRegistry #country").val(),
    province: $("#frmAddSupplierRegistry #province").val(),
    telephone_number: $("#frmAddSupplierRegistry #telephone_number").val(),
    mobile_phone: $("#frmAddSupplierRegistry #mobile_phone").val(),
    fax_number: $("#frmAddSupplierRegistry #fax_number").val(),
    email: $("#frmAddSupplierRegistry #email").val(),   
  };

  supplierregistryServices.insertsupplierregistry(data, function () {
    $('#frmAddSupplierRegistry #id').val('');
    $('#frmAddSupplierRegistry #business_name').val('');
    $('#frmAddSupplierRegistry #name').val('');
    $('#frmAddSupplierRegistry #surname').val('');
    $('#frmAddSupplierRegistry #address').val('');
    $('#frmAddSupplierRegistry #code_postal').val('');
    $('#frmAddSupplierRegistry #country').val('');
    $('#frmAddSupplierRegistry #province').val('');
    $('#frmAddSupplierRegistry #telephone_number').val('');
    $('#frmAddSupplierRegistry #mobile_phone').val('');
    $('#frmAddSupplierRegistry #fax_number').val('');
    $('#frmAddSupplierRegistry #email').val('');
    notificationHelpers.success(dictionaryHelpers.getDictionary("Inserimento avvenuto con successo"));
    spinnerHelpers.hide();
  });
}

export function deleteRow(id) {
  if (confirm("sei sicuro di voler eliminare?")) {
  spinnerHelpers.show();
  supplierregistryServices.del(id, function (response) {
  notificationHelpers.success(dictionaryHelpers.getDictionary("DeleteCompleted"));
  spinnerHelpers.hide();
  window.location.reload();
  });
  } 
  }

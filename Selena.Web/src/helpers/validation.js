/***   validationHelpers   ***/

export function validate(formSelector, rulesValidation, fnSubmit) {
  validationHelpers.setLanguage();
  var _errorClass = 'is-invalid';
  $(formSelector).validate({
    rules: rulesValidation,
    errorClass: _errorClass,
    errorPlacement: function (error, element) {
      if (element.parent().is('td')) {
        // error.insertAfter(element);
        // $('<br />').insertAfter(element);
      } else {
        if (element.parent().is('.input-group')) {
          error.insertAfter(element.parent());
        } else {
          if (element.is('select')) {
            element.next().find('.select2-selection').addClass(_errorClass);
            error.insertAfter(element.next());
          } else {
            error.insertAfter(element);
          }
        }
      }
    },
    submitHandler: function (form) {
      if (functionHelpers.isValued(fnSubmit) && $.isFunction(fnSubmit)) {
        fnSubmit();
      } else {
        form.submit();
      }
    }
  });

  $.each(JSON.parse(JSON.stringify(rulesValidation)), function (selector, value) {
    if ($('#' + selector).is('select')) {
      $('#' + selector).on("select2:close", function (e) {
        if ($(this).valid()) {
          $(this).next().find('.select2-selection').removeClass(_errorClass);
        }
      });
    }
  });
}

export function validateBase(formSelector) {
  var _errorClass = 'is-invalid';
  validationHelpers.setLanguage();

  $(formSelector).validate({
    errorClass: _errorClass,
    errorPlacement: function (error, element) {
      if (element.parent().is('td')) {
        // error.insertAfter(element);
        // $('<br />').insertAfter(element);
      } else {
        if (element.parent().is('.input-group')) {
          error.appendTo(element.parent().parent());
        } else {
          if (element.is('select')) {
            element.next().find('.select2-selection').addClass(_errorClass);
            error.insertAfter(element.next());
          } else {
            error.insertAfter(element);
          }
        }
      }
    }
  });

  $.each($(formSelector + ' select'), function (value, selector) {
    $('#' + selector.id).on("select2:select", function (e) {
      if ($(this).valid()) {
        $(this).next().find('.select2-selection').removeClass(_errorClass);
      }
    });
  });
}

export function setLanguage() {
  $.extend($.validator.messages, {
    required: dictionaryHelpers.getDictionary('ThisFieldIsRequired'),
    remote: "Please fix this field.",
    email: dictionaryHelpers.getDictionary('EmailNotValid'),
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: dictionaryHelpers.getDictionary('PleaseEnterAValidNumber'),
    digits: "Please enter only digits.",
    equalTo: "Please enter the same value again.",
    maxlength: $.validator.format("Please enter no more than {0} characters."),
    minlength: $.validator.format("Please enter at least {0} characters."),
    rangelength: $.validator.format("Please enter a value between {0} and {1} characters long."),
    range: $.validator.format("Please enter a value between {0} and {1}."),
    max: $.validator.format("Please enter a value less than or equal to {0}."),
    min: $.validator.format("Please enter a value greater than or equal to {0}."),
    step: $.validator.format("Please enter a multiple of {0}.")
  });
}

export function isValid(formSelector) {
  validationHelpers.setLanguage();

  if ($(formSelector).valid()) {
    return true;
  } else {
    notificationHelpers.error(dictionaryHelpers.getDictionary('RequiredFieldsNotCompleted'));
    validationHelpers.moveToInvalid();
    spinnerHelpers.hide();
    return false;
  }
}

export function clear(formSelector) {
  var _errorClass = 'is-invalid';

  $(formSelector).validate({
    errorClass: _errorClass,
    errorPlacement: function (error, element) {
      if (element.parent().is('td')) {
        // error.insertAfter(element);
        // $('<br />').insertAfter(element);
      } else {
        if (element.parent().is('.input-group')) {
          error.appendTo(element.parent().parent());
        } else {
          if (element.is('select')) {
            element.next().find('.select2-selection').addClass(_errorClass);
            error.insertAfter(element.next());
          } else {
            error.insertAfter(element);
          }
        }
      }
    }
  }).resetForm();
}

export function moveToInvalid() {
  if (functionHelpers.isValued($('.is-invalid').offset())) {
    $([document.documentElement, document.body]).animate({
      scrollTop: $('.is-invalid:visible').offset().top - 300
    }, 1000);
  }
}

export function setDetailError(idField, error) {
  notificationHelpers.error(error);
  $('#' + idField).addClass('error');
  $('#' + idField).closest('form').validate().showErrors({
    [idField]: error
  });
}

export function setTableError(idField, error) {
  notificationHelpers.error(error);
  $('#' + idField).editable('show');
  setTimeout(() => {
    $(".editable-error-block").html(error);
    $(".editable-error-block").show();
  }, 100);
}


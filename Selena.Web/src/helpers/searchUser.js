/***   searchUserHelpers   ***/
/***   POST   ***/

export function searchImplemented(name,business_name,email,country,province,telephone_number) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(name,business_name,email,country,province,telephone_number);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  searchUserServices.searchImplemented(data, function (response) {
    console.log(response);
    $("#table_list_searchuser tbody").html('');
    if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_searchuser").show();
      $("#table_list_user").hide();
      $("#table_list_searchuser tbody").append(newList);
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        newList += "<tr>";
        var newList = "";
        newList += "<tr>";
        newList +=
          '<td><button class="btn btn-info" onclick="userHelpers.getByIdResult(' +
          response.data[i].id +
          ')"><i class="fa fa-pencil" style="color:#ƒff!important;"/></button></td>';
        newList += "<td>" + response.data[i].id + "</td>";
        newList += "<td>" + response.data[i].subscriber + "</td>";
        newList += "<td>" + response.data[i].expiration_date + "</td>";
        newList += "<td>" + response.data[i].subscription_date + "</td>";
        if (response.data[i].vat_number == "" || response.data[i].vat_number == null) {
          newList += "<td>" +
            '<input type="text" class="form-control" style="border: #fff;width:200px;" name="vat_number_id" id="vat_number_id' + response.data[i].id + '" onchange="searchUserHelpers.updateVatNumber(' +
            response.data[i].id +
            ')" > ';
          + "</td>";
        } else {
          // newList += "<td>" + response.data[i].vat_number + "</td>";
          newList += "<td>" +
            '<input type="text" class="form-control" style="border: #fff;width:200px;" name="vat_number_id" id="vat_number_id' + response.data[i].id + '" onchange="searchUserHelpers.updateVatNumber(' +
            response.data[i].id +
            ')" value=" ' + response.data[i].vat_number + '"> ';
          + "</td>";
        }
        newList += "<td>" + response.data[i].business_name + "</td>";
        newList += "<td>" + response.data[i].name + "</td>";
        newList += "<td>" + response.data[i].telephone_number + "</td>";
        newList += "<td>" + response.data[i].mobile_number + "</td>";
        newList += "<td>" + response.data[i].fax_number + "</td>";
        newList += "<td>" + response.data[i].email + "</td>";
        newList +=
          '<td><button class="btn btn-danger" onclick="userHelpers.deleteRow(' +
          response.data[i].id +
          ')"><i class="fa fa-trash" style="color:#ƒff!important;" /></button></td>';
        newList += "</tr>";
    
        $("#table_list_searchuser").show();
        $("#table_list_user").hide();
        $("#table_list_searchuser tbody").append(newList);
        //$("#table_list_intervention").hide();
      }
    }
    });
    
    spinnerHelpers.hide(); 

}
    
export function updateVatNumber(id) {
  spinnerHelpers.show();
  var data = {
      id: id,
      vat_number: $('#table_list_searchuser #vat_number_id' + id).val(),
  };

  userServices.updateVatNumber(data, function (result) {
    notificationHelpers.success("Aggiornamento avvenuto correttamente");
    spinnerHelpers.hide();
  });
}

/***   END POST   ***/

  export function SearchViewPrivates(id) {
    spinnerHelpers.show();
    var data = functionHelpers.formToJson(id);
    data["idLanguage"] = storageData.sIdLanguage();
    var html = "";
    searchUserServices.SearchViewPrivates(data, function (response) {
      console.log(response.data);
      $("#table_list_searchuser tbody").html('');
if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_searchuser").show();
      $("#table_list_user").hide();
      $("#table_list_searchuser tbody").append(newList);
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        newList += "<tr>";
        var newList = "";
        newList += "<tr>";
        newList +=
          '<td><button class="btn btn-info" onclick="userHelpers.getByIdResult(' +
          response.data[i].id +
          ')"><i class="fa fa-pencil" style="color:#ƒff!important;"/></button></td>';
        newList += "<td>" + response.data[i].id + "</td>";
        newList += "<td>" + response.data[i].subscriber + "</td>";
        newList += "<td>" + response.data[i].expiration_date + "</td>";
        newList += "<td>" + response.data[i].subscription_date + "</td>";
        if (response.data[i].vat_number == "" || response.data[i].vat_number == null) {
          newList += "<td>" +
            '<input type="text" class="form-control" style="border: #fff;width:200px;" name="vat_number_id" id="vat_number_id' + response.data[i].id + '" onchange="searchUserHelpers.updateVatNumber(' +
            response.data[i].id +
            ')" > ';
          + "</td>";
        } else {
          // newList += "<td>" + response.data[i].vat_number + "</td>";
          newList += "<td>" +
            '<input type="text" class="form-control" style="border: #fff;width:200px;" name="vat_number_id" id="vat_number_id' + response.data[i].id + '" onchange="searchUserHelpers.updateVatNumber(' +
            response.data[i].id +
            ')" value=" ' + response.data[i].vat_number + '"> ';
          + "</td>";
        }
        newList += "<td>" + response.data[i].business_name + "</td>";
        newList += "<td>" + response.data[i].name + "</td>";
        newList += "<td>" + response.data[i].telephone_number + "</td>";
        newList += "<td>" + response.data[i].mobile_number + "</td>";
        newList += "<td>" + response.data[i].fax_number + "</td>";
        newList += "<td>" + response.data[i].email + "</td>";
        newList +=
          '<td><button class="btn btn-danger" onclick="userHelpers.deleteRow(' +
          response.data[i].id +
          ')"><i class="fa fa-trash" style="color:#ƒff!important;" /></button></td>';
        newList += "</tr>";
    
        $("#table_list_searchuser").show();
        $("#table_list_user").hide();
        $("#table_list_searchuser tbody").append(newList);
        //$("#table_list_intervention").hide();
      }
    }
    });
    
    spinnerHelpers.hide(); 

}


    export function SearchViewTrueSubscriber(id) {
    spinnerHelpers.show();
    var data = functionHelpers.formToJson(id);
    data["idLanguage"] = storageData.sIdLanguage();
    var html = "";
    searchUserServices.SearchViewTrueSubscriber(data, function (response) {
      console.log(response.data);
      $("#table_list_searchuser tbody").html('');
if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_searchuser").show();
      $("#table_list_user").hide();
      $("#table_list_searchuser tbody").append(newList);
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        newList += "<tr>";
        var newList = "";
        newList += "<tr>";
        newList +=
          '<td><button class="btn btn-info" onclick="userHelpers.getByIdResult(' +
          response.data[i].id +
          ')"><i class="fa fa-pencil" style="color:#ƒff!important;"/></button></td>';
        newList += "<td>" + response.data[i].id + "</td>";
        newList += "<td>" + response.data[i].subscriber + "</td>";
        newList += "<td>" + response.data[i].expiration_date + "</td>";
        newList += "<td>" + response.data[i].subscription_date + "</td>";
        if (response.data[i].vat_number == "" || response.data[i].vat_number == null) {
          newList += "<td>" +
            '<input type="text" class="form-control" style="border: #fff;width:200px;" name="vat_number_id" id="vat_number_id' + response.data[i].id + '" onchange="searchUserHelpers.updateVatNumber(' +
            response.data[i].id +
            ')" > ';
          + "</td>";
        } else {
          // newList += "<td>" + response.data[i].vat_number + "</td>";
          newList += "<td>" +
            '<input type="text" class="form-control" style="border: #fff;width:200px;" name="vat_number_id" id="vat_number_id' + response.data[i].id + '" onchange="searchUserHelpers.updateVatNumber(' +
            response.data[i].id +
            ')" value=" ' + response.data[i].vat_number + '"> ';
          + "</td>";
        }
        newList += "<td>" + response.data[i].business_name + "</td>";
        newList += "<td>" + response.data[i].name + "</td>";
        newList += "<td>" + response.data[i].telephone_number + "</td>";
        newList += "<td>" + response.data[i].mobile_number + "</td>";
        newList += "<td>" + response.data[i].fax_number + "</td>";
        newList += "<td>" + response.data[i].email + "</td>";
        newList +=
          '<td><button class="btn btn-danger" onclick="userHelpers.deleteRow(' +
          response.data[i].id +
          ')"><i class="fa fa-trash" style="color:#ƒff!important;" /></button></td>';
        newList += "</tr>";
  
        $("#table_list_searchuser").show();
        $("#table_list_user").hide();
        $("#table_list_searchuser tbody").append(newList);
        //$("#table_list_intervention").hide();
      }
    }
    });
    spinnerHelpers.hide(); 
}

    export function SearchViewFalseSubscriber(id) {
    spinnerHelpers.show();
    var data = functionHelpers.formToJson(id);
    data["idLanguage"] = storageData.sIdLanguage();
    var html = "";
    searchUserServices.SearchViewFalseSubscriber(data, function (response) {
    console.log(response.data);
    $("#table_list_searchuser tbody").html('');
  if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_searchuser").show();
      $("#table_list_user").hide();
      $("#table_list_searchuser tbody").append(newList);
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        newList += "<tr>";
        var newList = "";
        newList += "<tr>";
        newList +=
          '<td><button class="btn btn-info" onclick="userHelpers.getByIdResult(' +
          response.data[i].id +
          ')"><i class="fa fa-pencil" style="color:#ƒff!important;"/></button></td>';
        newList += "<td>" + response.data[i].id + "</td>";
        newList += "<td>" + response.data[i].subscriber + "</td>";
        newList += "<td>" + response.data[i].expiration_date + "</td>";
        newList += "<td>" + response.data[i].subscription_date + "</td>";
        if (response.data[i].vat_number == "" || response.data[i].vat_number == null) {
          newList += "<td>" +
            '<input type="text" class="form-control" style="border: #fff;width:200px;" name="vat_number_id" id="vat_number_id' + response.data[i].id + '" onchange="searchUserHelpers.updateVatNumber(' +
            response.data[i].id +
            ')" > ';
          + "</td>";
        } else {
          // newList += "<td>" + response.data[i].vat_number + "</td>";
          newList += "<td>" +
            '<input type="text" class="form-control" style="border: #fff;width:200px;" name="vat_number_id" id="vat_number_id' + response.data[i].id + '" onchange="searchUserHelpers.updateVatNumber(' +
            response.data[i].id +
            ')" value=" ' + response.data[i].vat_number + '"> ';
          + "</td>";
        }
        newList += "<td>" + response.data[i].business_name + "</td>";
        newList += "<td>" + response.data[i].name + "</td>";
        newList += "<td>" + response.data[i].telephone_number + "</td>";
        newList += "<td>" + response.data[i].mobile_number + "</td>";
        newList += "<td>" + response.data[i].fax_number + "</td>";
        newList += "<td>" + response.data[i].email + "</td>";
        newList +=
          '<td><button class="btn btn-danger" onclick="userHelpers.deleteRow(' +
          response.data[i].id +
          ')"><i class="fa fa-trash" style="color:#ƒff!important;" /></button></td>';
        newList += "</tr>";
    
        $("#table_list_searchuser").show();
        $("#table_list_user").hide();
        $("#table_list_searchuser tbody").append(newList);
        //$("#table_list_intervention").hide();
      }
    }
    });
    
    spinnerHelpers.hide(); 

}

  


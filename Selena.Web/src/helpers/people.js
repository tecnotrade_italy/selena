/***   peopleHelpers   ***/

export function insertReferentWeb() {
  spinnerHelpers.show();
  var data = {
    user_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    name: $('#referentAdd #name').val(),
    surname: $('#referentAdd #surname').val(),
    email: $('#referentAdd #email').val(),
    phone_number: $('#referentAdd #phone_number').val(),
    password: $('#referentAdd #password').val(),
    roles_id:$('#referentAdd #roles').val(),
    ref:$('#referentAdd #ref').val(),
  };
  peopleServices.insertReferentWeb(data, function () {
    $('#referentAdd #name').val('');
    $('#referentAdd #surname').val('');
    $('#referentAdd #email').val('');
    $('#referentAdd #phone_number').val('');
    $('#referentAdd #roles').val('');
    $('#referentAdd #password').val('');
    $('#referentAdd #ref').val('');
    notificationHelpers.success(dictionaryHelpers.getDictionary("ContactRequestCompleted"));
    spinnerHelpers.hide();
  });
}


export function insert() {
  spinnerHelpers.show();

  var data = {
    user_id: storageData.sUserId(),
    name: $('#people #name').val(),
    surname: $('#people #surname').val(),
    email: $('#people #email').val(),
    phone_number: $('#people #phone_number').val(),
    roles_id:$('#people #roles').val(),
  };

  peopleServices.insert(data, function () {
    $('#people #name').val('');
    $('#people #surname').val('');
    $('#people #email').val('');
    $('#people #phone_number').val('');
    $('#people #roles_id').val('');
    notificationHelpers.success(dictionaryHelpers.getDictionary("ContactRequestCompleted"));
    spinnerHelpers.hide();
  });
}

// sezione dati personali referente gr -- web cliente -- //
export function getPeopleUserWeb() {
   spinnerHelpers.show();
  peopleServices.getPeopleUserWeb(storageData.sUserId(), function (response) {   
     for(var i = 0; i<response.data.length;i++){
       var newList = "";
       newList += "<tr>";
       newList +=
       '<td><button class="btn btn-link" onclick="peopleHelpers.getByIdReferentWeb(' +
       response.data[i].id +
       ')"><i class="fa fa-pencil"/></button>';
         newList +=
         '<button class="btn btn-link" onclick="peopleHelpers.deleteRow(' +
         response.data[i].id +
           ')"><i class="fa fa-trash"/></button></td>';
       //newList += "<td>" + response.data[i].id + "</td>";
       newList += "<td>" + response.data[i].name + "</td>";
       newList += "<td>" + response.data[i].surname + "</td>";
       newList += "<td>" + response.data[i].email + "</td>";
       newList += "<td>" + response.data[i].phone_number + "</td>";
       newList += "<td>" + response.data[i].roles_id + "</td>";
       newList += "</tr>";
 
      $("#table_list_people_userweb tbody").append(newList);
      $("#table_list_people_userweb").show();
      $("#frmUpdateReferentWeb").hide();     
     }
   });
   spinnerHelpers.hide(); 
}
 
 export function getByIdReferentWeb(id) {
  // spinnerHelpers.show();
  peopleServices.getByIdReferent(id, function (response) {
  var searchParam = "";
 //select roles_id
var initials = [];
initials.push({id: response.data.roles_id, text: response.data.descriptionRoleId});
$('#roles_id').select2({
  data: initials,
  ajax: {
    url: configData.wsRootServicesUrl + '/api/v1/roles/select',
    data: function (params) {
      if(params.term==''){
        searchParam = '*';
      }else{
      searchParam = params.term;
      }
      var query = {
        search: searchParam,
      }
      return query;
    },
    processResults: function (data) {
      var dataParse = JSON.parse(data);

      return {
        results: dataParse.data
      };
    }
  }
})

   jQuery.each(response.data, function(i, val) {
    if (val == true){
      $('#frmUpdateReferentWeb #' + i).attr('checked','checked');
    }else{
      $("#frmUpdateReferentWeb #" + i).val(val);
    }
   }); 
     $("#table_list_people_userweb").show();
     $("#frmUpdateReferentWeb").modal("show");
   }); 
 }

 export function updateReferentWeb() {
  spinnerHelpers.show();
  var data = {
    UserId: storageData.sUserId(),
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmUpdateReferentWeb #id').val(),
    name: $('#frmUpdateReferentWeb #names').val(),
    surname: $('#frmUpdateReferentWeb #surnames').val(),
    email: $('#frmUpdateReferentWeb #emails').val(),
    phone_number: $('#frmUpdateReferentWeb #phone_numbers').val(),
    roles_id: $('#frmUpdateReferentWeb #roles_id').val(),
  };
  peopleServices.updateReferentWeb(data, function () {
    $('#frmUpdateReferentWeb #id').val('');
    $('#frmUpdateReferentWeb #names').val('');
    $('#frmUpdateReferentWeb #surnames').val('');
    $('#frmUpdateReferentWeb #emails').val('');
    $('#frmUpdateReferentWeb #phone_numbers').val('');
    $('#frmUpdateReferentWeb #roles_id').val('');

   notificationHelpers.success("Referente aggiornato correttamente!");
    spinnerHelpers.hide();
    $("#frmUpdateReferentWeb").modal("hide"); 
    $("#table_list_people_userweb").show();
  });
}
// end sezione dati personali referente gr -- web cliente -- //
 
export function getPeopleUser(id) {
  spinnerHelpers.show();
  peopleServices.getPeopleUser(id, function (response) {   
    $( "#table_list_people tbody").html('');
     for(var i = 0; i<response.data.length;i++){
       var newList = "";
       newList += "<tr>";
       newList +=
       '<td><button class="btn btn-info" onclick="peopleHelpers.getByIdReferent(' +
       response.data[i].id +
       ')"><i class="fa fa-pencil" style="color:#fff!important;"/></button></td>';
       newList += "<td>" + response.data[i].id + "</td>";
       newList += "<td>" + response.data[i].name + "</td>";
       newList += "<td>" + response.data[i].surname + "</td>";
       newList += "<td>" + response.data[i].email + "</td>";
       newList += "<td>" + response.data[i].phone_number + "</td>";
       newList += "<td>" + response.data[i].roles_id + "</td>";

       newList +=
         '<td><button class="btn btn-danger" onclick="peopleHelpers.deleteRow(' +
         response.data[i].id +
         ')"><i class="fa fa-trash" style="color:#fff!important;"/></button</td>';
       newList += "</tr>";
      $( "#table_list_people tbody").append(newList);
      $("#table_list_people").show();
      $("#frmUpdateReferent").hide();     
     }
   });
   spinnerHelpers.hide(); 
 }

 export function getByIdReferent(id) {
  // spinnerHelpers.show();
  peopleServices.getByIdReferent(id, function (response) {

  var searchParam = "";

 //select roles_id
var initials = [];
initials.push({id: response.data.roles_id, text: response.data.descriptionRoleId});
$('#roles_id').select2({
  data: initials,
  ajax: {
    url: configData.wsRootServicesUrl + '/api/v1/roles/select',
    data: function (params) {
      if(params.term==''){
        searchParam = '*';
      }else{
      searchParam = params.term;
      }
      var query = {
        search: searchParam,
      }
      return query;
    },
    processResults: function (data) {
      var dataParse = JSON.parse(data);

      return {
        results: dataParse.data
      };
    }
  }
})

   jQuery.each(response.data, function(i, val) {
    if (val == true){
      $('#frmUpdateReferent #' + i).attr('checked','checked');
    }else{
      $("#frmUpdateReferent #" + i).val(val);
    }
   }); 
   $("#frmUpdateReferent").show();
   $("#frmAddReferent").hide();
   $("#table_list_people").hide();
 });
  
 }

 export function updateReferent() {

  spinnerHelpers.show();
  var data = {
    UserId: storageData.sUserId(),
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmUpdateReferent #id').val(),
    name: $('#frmUpdateReferent #names').val(),
    surname: $('#frmUpdateReferent #surnames').val(),
    email: $('#frmUpdateReferent #emails').val(),
    phone_number: $('#frmUpdateReferent #phone_numbers').val(),
    roles_id: $('#frmUpdateReferent #roles_id').val(),
  };
  peopleServices.updateReferent(data, function () {
    $('#frmUpdateReferent #id').val('');
    $('#frmUpdateReferent #names').val('');
    $('#frmUpdateReferent #surnames').val('');
    $('#frmUpdateReferent #emails').val('');
    $('#frmUpdateReferent #phone_numbers').val('');
    $('#frmUpdateReferent #roles_id').val('');

   notificationHelpers.success("Referente aggiornato correttamente!");
    spinnerHelpers.hide();
    $("#table_list_people").show();
    $("#frmUpdateReferent").hide(); 
  });
}

export function insertNoAuth() {
  spinnerHelpers.show();
  var user_id = $('#frmAddReferent #user_id').val();
  var data = {
    created_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    //id: $('#frmAddReferent #id').val(),
    name: $('#frmAddReferent #name').val(),
    ref: $('#frmAddReferent #ref').val(),
    surname: $('#frmAddReferent #surname').val(),
    email: $('#frmAddReferent #email').val(), 
    phone_number: $('#frmAddReferent #phone_number').val(), 
    roles_id: $('#frmAddReferent #add_roles_id').val(),
    user_id: $('#frmAddReferent #user_id').val(),
    password: $('#frmAddReferent #password').val(), 
  };

  peopleServices.insertNoAuth(data, function () {
    //$('#frmAddReferent #id').val('');
    $('#frmAddReferent #name').val('');
    $('#frmAddReferent #surname').val('');
    $('#frmAddReferent #ref').val('');
    $('#frmAddReferent #email').val('');
    $('#frmAddReferent #phone_number').val('');
    $('#frmAddReferent #add_roles_id').val('');
    //$('#frmAddReferent #user_id').val('');
    $('#frmAddReferent #password').val('');
    //  window.location.reload();
    $('#frmAddReferent').hide();
    peopleHelpers.getPeopleUser(user_id);
     //window.location.reload();
    $('#table_list_people').show();
   // $('#frmUpdateReferent').hide();
    notificationHelpers.success("Referente inserito correttamente!");
    spinnerHelpers.hide();
  });
}

export function deleteRow(id) {
  spinnerHelpers.show();
  peopleServices.del(id, function (response) {
  notificationHelpers.success(dictionaryHelpers.getDictionary("DeleteCompleted"));
  window.location.reload();
  $("#table_list_people").show();
  $("#table_list_user").hide();
    });
    spinnerHelpers.hide();
  }

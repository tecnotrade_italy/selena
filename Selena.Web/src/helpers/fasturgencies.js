//  fasturgenciesHelpers

//addFastUrgenze

export function addFastUrgenze() {
  spinnerHelpers.show();

  var data = {  
    users_id: $("#frmfasturgencies #users_id").val(),
    date: $("#frmfasturgencies #date").val(),
    object: $("#frmfasturgencies #object").val(),
    description: $("#frmfasturgencies #description").val(),
  };


var arrayfasturgencies = [];
  for (var i = 0; i < 15; i++) {
    var jsonfasturgencies = {};
     var check = false;
   
    if (functionHelpers.isValued($('#users_id' + i).val())) {
       check = true;
      jsonfasturgencies['users_id'] = $('#users_id' + i).val();
    }
    if (functionHelpers.isValued($('#date' + i).val())) {
       check = true;
      jsonfasturgencies['date'] = $('#date' + i).val();
    }
    if (functionHelpers.isValued($('#object' + i).val())) {
       check = true;
      jsonfasturgencies['object'] = $('#object' + i).val();
    }
    if (functionHelpers.isValued($('#description' + i).val())) {
       check = true;
      jsonfasturgencies['description'] = $('#description' + i).val();
    }
  if (check) {
        arrayfasturgencies.push(jsonfasturgencies);
    }
    
  }

  data.fasturgencies = arrayfasturgencies;

  fasturgenciesServices.insert(data, function() {
    $("#intervention #users_id").val("");
    $("#intervention #date").val("");
    $("#intervention #object").val("");
    $("#intervention #description").val("");
    notificationHelpers.success("Inserimento avvenuto correttamente!");
    spinnerHelpers.hide();
  });
}

export function getUserData() {
 // spinnerHelpers.show();
  fasturgenciesServices.getUserData(storageData.sUserId(), function (response) {
  //view result FAST URGENZE
  var cont = 0;
    var Aggfasturgencies = response.data;
    if(Aggfasturgencies !== undefined){
     if(Aggfasturgencies.length>0){
       for(var j=0;j<Aggfasturgencies.length;j++){
        var html = "";
         html += "<tr>";
         html += "<td>" + '<input type="hidden" class="form-control id" id="id'  + Aggfasturgencies[j].id + '" name="id' + '" value="' + Aggfasturgencies[j].id + '"><select class="custom-select users_id_select" data-toggle="tooltip" data-placement="top" title="'+ Aggfasturgencies[j].users_id + '" id="users_id'  + Aggfasturgencies[j].id + '" name="users_id' + '" value="' + Aggfasturgencies[j].users_id + '"></select></td>';
         html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ Aggfasturgencies[j].id +'" id="datetext'+ Aggfasturgencies[j].id +'"> ' + Aggfasturgencies[j].date + '</label><input type="date" data-toggle="tooltip" style="display:none;" data-placement="top" title="'+ Aggfasturgencies[j].date + '" id="date' + Aggfasturgencies[j].id + '" name="date' + '" class="form-control date' + '" value="' + Aggfasturgencies[j].date + '"></td>';
         html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ Aggfasturgencies[j].id +'" id="objecttext'+ Aggfasturgencies[j].id +'"> ' + Aggfasturgencies[j].object + '</label><input type="text" class="form-control object" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ Aggfasturgencies[j].object + '"  id="object' +  Aggfasturgencies[j].id + '" name="object' + '" value="' + Aggfasturgencies[j].object + '"></td>';
         html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ Aggfasturgencies[j].id +'" id="descriptiontext'+ Aggfasturgencies[j].id +'"> ' + Aggfasturgencies[j].description + '</label><input type="text" class="form-control description" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ Aggfasturgencies[j].description + '" id="description'  + Aggfasturgencies[j].id + '" name="description' + '" value="' + Aggfasturgencies[j].description + '"></td>';
         html += "<td>" + '<button type="button" class="btn btn-outline-success" onclick="fasturgenciesHelpers.updateFast(' + Aggfasturgencies[j].id + ',false)"><i class="fa fa-floppy-o" aria-hidden="true"></i></button></td>';
         html += "<td>" + '<button type="button" class="btn btn-outline-danger" id="btndelete' + '" onclick="fasturgenciesHelpers.deleteRow(' +
         Aggfasturgencies[j].id + ',false)"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>';
         html += "</tr>"; 
         $('#div-cont-fast-urgenze').append(html);
           
         //section nascondi fast urgenze 
          $("#div-cont-fast-urgenze #description_user_idtext" + Aggfasturgencies[j].id).on("click", function() {
            var id= $(this).attr('data-attr-id');
            $("#description_user_idtext" + id).hide();
            $("#users_id" + id).show(); 
        }); 
          
          $("#div-cont-fast-urgenze #datetext" + Aggfasturgencies[j].id + "").on("click", function() {
            var id= $(this).attr('data-attr-id');
            $("#datetext" + id).hide();
            $("#date" + id).show();
        }); 

          $("#div-cont-fast-urgenze #objecttext" + Aggfasturgencies[j].id + "").on("click", function() {
            var id= $(this).attr('data-attr-id');
            $("#objecttext" + id).hide();
            $("#object" + id).show();
        }); 

          $("#div-cont-fast-urgenze #descriptiontext" + Aggfasturgencies[j].id + "").on("click", function() {
            var id= $(this).attr('data-attr-id');
            $("#descriptiontext" + id).hide();
            $("#description" + id).show();
        }); 


   // $.getScript("https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js").done(function(){
       var searchParam = "";
         var initials = [];
         initials.push({    
           id: Aggfasturgencies[j].users_id,
            text: Aggfasturgencies[j].description_user_id  
         });
          $('#users_id'+ Aggfasturgencies[j].id).select2({
            data: initials,
            ajax: {
              url: configData.wsRootServicesUrl + '/api/v1/user/select',
              data: function (params) {
                if (params.term == '') {
                  searchParam = '*';
                } else {
                  searchParam = params.term;
                }
                var query = {
                  search: searchParam,
                }
                return query;
              },
              processResults: function (data) {
                var dataParse = JSON.parse(data);
                return {
                  results: dataParse.data
                };
              }
            }
          });
         // });
         }
     }
  }

//end view FAST URGENZE
    
    /* AGG FAST URGENZE*/
    $("#frmFastUrgenze #add-fast-urgenze").on("click", function(e) {
      var html = "";
      if(cont < 10){
      cont = cont + 1;
      var html = "";
         html += "<tr>";
         html += "<td>" + '<input type="hidden" class="form-control id" id="id'  + cont + '" name="id' + '" value="' + cont + '"><select class="custom-select users_id_select" id="users_id' + cont + '" name="users_id' + cont + '"></select></td>';
         html += "<td>" + '<input type="date" id="date' + cont + '" name="date' + cont + '" class="form-control date' + cont + '"></td>';
         html += "<td>" + '<input type="text" class="form-control object" id="object' + cont + '" name="object' + cont + '"></td>';
         html += "<td>" + '<input type="text" class="form-control description" id="description' + cont + '" name="description' + cont + '"></td>';
         html += "<td>" + '<button type="button" class="btn btn-outline-success" id="btnsave' + '" onclick="fasturgenciesHelpers.updateFast('+ cont +',true)"><i class="fa fa-floppy-o" aria-hidden="true"></i></button></td>';
         html += "<td>" + '<button type="button" class="btn btn-outline-danger" id="btndelete' + '" onclick="btndelete' + '"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>';
         html += "</tr>"; 
         $('#div-cont-fast-urgenze').append(html);

      /* html += '<div class="row" data-id-row="' + cont + '" id="divcont-' + cont + '" >';
	  	html += '<div class="col-5">';
      html += '<label>Cliente</label><select class="custom-select users_id_select" id="users_id' + cont + '" name="users_id' + cont + '"></select>';
      html += "</div>";
      html += '<div class="col-5">';
	  	html += '<label>Data</label><input type="date" id="date' + cont + '" name="date' + cont + '" class="form-control date' + cont + '">';
		  html += "</div>";	
      html += '<div class="col-2">';
	  	html += '<button type="button" class="btn btn-outline-success" id="btnsave' + '" onclick="fasturgenciesHelpers.updateFast('+ cont +',true)"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>';  
		  html += "</div>";
      html += "</div>";

      html += '<div class="row" data-id-row="' + cont + '" id="divcont-' + cont + '" >';
      html += '<div class="col-5">';
	  	html += '<label>Oggetto</label><input type="text" class="form-control object" id="object' + cont + '" name="object' + cont + '">';
	    html += "</div>";
	    html += '<div class="col-5">';
	  	html += '<label>Descrizione</label><input type="text" class="form-control description" id="description' + cont + '" name="description' + cont + '">';	  
      html += "</div>";	
	    html += '<div class="col-2">';
	  	html += '<button type="button" class="btn btn-outline-danger" id="btndelete' + '" onclick="btndelete' + '"><i class="fa fa-trash-o" aria-hidden="true"></i></button>';	  
      html += "</div>";
      html += '<div class="col-0">'; 
      html += '<input type="hidden" class="form-control id" id="id'  + cont + '" name="id' + '" value="' + cont + '">'; 
      html += "</div>";
	  	html += "</div>";
      html += "<hr style='border-top: 4px solid #e8b70d'>";
      $('#div-cont-fast-urgenze').append(html); */

      $.getScript("https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js").done(function(){ 
	    var searchParam = "";
	   $('#users_id' + cont).select2({
      ajax: {
      url: configData.wsRootServicesUrl + '/api/v1/user/select',
      data: function (params) {
		  if(params.term==''){
		  	searchParam = '*';
		  }else{
			searchParam = params.term;
		  }
		  var query = {
			search: searchParam,
		  }
		  return query;
		},
		processResults: function (data) {
		  var dataParse = JSON.parse(data);
		  return {
			results: dataParse.data
		  };
		}
	  }
	});  
  });
      }
});
     /* END FAST URGENZE */
  
});
}

export function updateFast(id,isNew) {
  var data = {
  id: id,
  isNew:isNew,
  };

    var arrayFast = [];
    var jsonFast = {};
    var check = false;

    if (functionHelpers.isValued($('#date' + id).val())) {
       check = true;
      jsonFast['date'] = $('#date' + id).val();
    }

   if (functionHelpers.isValued($('#id' + id).val())) {
       check = true;
      jsonFast['id'] = $('#id' + id).val();
   } 
      jsonFast['isNew'] = isNew;
  
    if (functionHelpers.isValued($('#object' + id).val())) {
       check = true;
      jsonFast['object'] = $('#object' + id).val();
    }
    if (functionHelpers.isValued($('#description' + id).val())) {
       check = true;
      jsonFast['description'] = $('#description' + id).val();
    }
    if (functionHelpers.isValued($('#users_id' + id).val())) {
       check = true;
     jsonFast['users_id'] = $('#users_id' + id).val();
    }
  
     if (check) {
      arrayFast.push(jsonFast);
     }
   
  data = arrayFast;
  fasturgenciesServices.updateFast(data, function () {
    notificationHelpers.success("Modifica dati effettuata con successo!");
  });
}

export function deleteRow(id) {
  if (confirm("sei sicuro di voler eliminare?")) {
    spinnerHelpers.show();
    fasturgenciesServices.del(id, function (response) {
    notificationHelpers.success(dictionaryHelpers.getDictionary("DeleteCompleted"));
    spinnerHelpers.hide();
    window.location.reload();
      });
    } 
  }




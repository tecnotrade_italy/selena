//  accountVisionHelpers

export function getLastId() {
  AccountVisionServices.getLastId(function(response) {
    $("#frmInsertAccountVision #id").val(response.data);
  });
}

export function getLastIdContoVisione() {
  AccountVisionServices.getLastIdContoVisione(function(response) {
    $("#frmInsertAccountVision #id_contovisione").val(response.data);
  });
}


export function insert() {
  spinnerHelpers.show();

  var data = {
    created_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $("#frmInsertAccountVision #id").val(),
    id_contovisione: $("#frmInsertAccountVision #id_contovisione").val(),
    date: $("#frmInsertAccountVision #date").val(),
    ddt_gr: $("#frmInsertAccountVision #ddt_gr").val(),
    code_gr: $("#frmInsertAccountVision #code_gr").val(),
    user_id: $("#frmInsertAccountVision #usernew_id").val(),
    description: $("#frmInsertAccountVision #description").val(),  
    note: $("#frmInsertAccountVision #note").val(),
    date_return: $("#frmInsertAccountVision #date_return").val(),
    ddt_return: $("#frmInsertAccountVision #ddt_return").val(),
    soll: $("#frmInsertAccountVision #soll").val(),
    note_return: $("#frmInsertAccountVision #note_return").val(),
    img: $("#frmInsertAccountVision #imgBase64").val(),
    imgName: $("#frmInsertAccountVision #imgName").val(),
  };

   var arrayimgAccountVision = [];
    for (var i = 0; i < 4; i++) {
    if (functionHelpers.isValued($("#imgBase64" + i).val())) {
      arrayimgAccountVision.push($("#imgBase64" + i).val());
    }
  }
    data.imageaccountvision = arrayimgAccountVision;

  AccountVisionServices.insert(data, function() {
    $("#frmInsertAccountVision #id").val("");
    $("#frmInsertAccountVision #id_contovisione").val("");
    $("#frmInsertAccountVision #date").val("");
    $("#frmInsertAccountVision #note").val("");
    $("#frmInsertAccountVision #ddt_gr").val("");
    $("#frmInsertAccountVision #code_gr").val("");
    $("#frmInsertAccountVision #usernew_id").val("");
    $("#frmInsertAccountVision #description").val("");
    $("#frmInsertAccountVision #date_return").val("");
    $("#frmInsertAccountVision #ddt_return").val("");
    $("#frmInsertAccountVision #soll").val("");
    $("#frmInsertAccountVision #note_return").val("");    
    notificationHelpers.success("Inserimento avvenuto correttamente!");
    spinnerHelpers.hide();
    window.location.href = 'https://www.grsrl.net/Gestione-Conto-Visione';
  });
}
export function getAllAccountVision() {
  spinnerHelpers.show();
  AccountVisionServices.getAllAccountVision(storageData.sIdLanguage(), function (response) {
    for (var i = 0; i < response.data.length; i++) {
      var newList = "";
      newList += "<tr>";
      newList += "<td>" + response.data[i].id + "</td>";
      newList += "<td>" + response.data[i].id_contovisione + "</td>";
      newList += "<td>" + response.data[i].date + "</td>";
      newList += "<td>" + response.data[i].ddt_gr + "</td>";
      newList += "<td>" + response.data[i].user_id + "</td>";
      newList += "<td>" + response.data[i].description + "</td>";
      newList += "<td>";
      for (var j = 0; j < response.data[i].imageaccountvision.length; j++) { 
      newList +=
       '<div class="container"><div id="lightgallery" style="display:flex">';
        newList +=
          "<a href='" +
         response.data[i].imageaccountvision[j].img  +
         "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
     }
      newList += "</td>";
      newList += "<td>" + response.data[i].note + "</td>";
      newList += "<td>" + response.data[i].code_gr + "</td>";
      newList += "<td>" + response.data[i].date_return + "</td>";
      newList += "<td>" + response.data[i].ddt_return + "</td>";
      newList += "<td>" + response.data[i].note_return + "</td>";
      newList += "<td>" + response.data[i].soll + "</td>";
      newList +=
      '<td><button class="btn btn-info" onclick="accountVisionHelpers.getById(' +
      response.data[i].id +
      ')"><i class="fa fa-pencil"style="color:#fff!important;" /></button></td>';
      newList +=
        '<td><button class="btn btn-danger" onclick="accountVisionHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
      newList += "</tr>";
      $("#table_list_account_vision tbody").append(newList);
      $("#table_list_account_vision").show();
      $("#frmViewAccountVision").hide();
    }
    lightGallery(document.getElementById('lightgallery'));
  });

  spinnerHelpers.hide();
}


export function SearchAccountVisionOpen(id) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(id);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  AccountVisionServices.SearchAccountVisionOpen(data, function (response) {
    $("#table_list_account_vision tbody").html('');
    if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_account_vision tbody").append(newList);
      $("#table_list_account_vision").show();
      $("#frmViewAccountVision").hide();
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        newList += "<tr>";
        newList += "<td>" + response.data[i].id + "</td>";
        newList += "<td>" + response.data[i].id_contovisione + "</td>";
        newList += "<td>" + response.data[i].date + "</td>";
        newList += "<td>" + response.data[i].ddt_gr + "</td>";
        newList += "<td>" + response.data[i].user_id + "</td>";
        newList += "<td>" + response.data[i].description + "</td>";
        newList += "<td>";
        for (var j = 0; j < response.data[i].imageaccountvision.length; j++) { 
        newList +=
         '<div class="container"><div id="lightgallery" style="display:flex">';
          newList +=
            "<a href='" +
           response.data[i].imageaccountvision[j].img  +
           "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
       }
        newList += "</td>";
        newList += "<td>" + response.data[i].note + "</td>";
        newList += "<td>" + response.data[i].code_gr + "</td>";
        newList += "<td>" + response.data[i].date_return + "</td>";
        newList += "<td>" + response.data[i].ddt_return + "</td>";
        newList += "<td>" + response.data[i].note_return + "</td>";
        newList += "<td>" + response.data[i].soll + "</td>";
        newList +=
        '<td><button class="btn btn-info" onclick="accountVisionHelpers.getById(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil"style="color:#fff!important;" /></button></td>';
        newList +=
          '<td><button class="btn btn-danger" onclick="accountVisionHelpers.deleteRow(' +
          response.data[i].id +
          ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
        newList += "</tr>";
        $("#table_list_account_vision tbody").append(newList);
        $("#table_list_account_vision").show();
        $("#frmViewAccountVision").hide();
      }
    }
    lightGallery(document.getElementById('lightgallery'));
    });
     spinnerHelpers.hide();
    } 

export function SearchAccountVisionClose(id) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(id);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  AccountVisionServices.SearchAccountVisionClose(data, function (response) {
    $("#table_list_account_vision tbody").html('');
    if (response.data.length == 0) {
      var newList = "";
     newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_account_vision tbody").append(newList);
      $("#table_list_account_vision").show();
      $("#frmViewAccountVision").hide();
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        newList += "<tr>";
        newList += "<td>" + response.data[i].id + "</td>";
        newList += "<td>" + response.data[i].id_contovisione + "</td>";
        newList += "<td>" + response.data[i].date + "</td>";
        newList += "<td>" + response.data[i].ddt_gr + "</td>";
        newList += "<td>" + response.data[i].user_id + "</td>";
        newList += "<td>" + response.data[i].description + "</td>";
        newList += "<td>";
        for (var j = 0; j < response.data[i].imageaccountvision.length; j++) { 
        newList +=
         '<div class="container"><div id="lightgallery" style="display:flex">';
          newList +=
            "<a href='" +
           response.data[i].imageaccountvision[j].img  +
           "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
       }
        newList += "</td>";
        newList += "<td>" + response.data[i].note + "</td>";
        newList += "<td>" + response.data[i].code_gr + "</td>";
        newList += "<td>" + response.data[i].date_return + "</td>";
        newList += "<td>" + response.data[i].ddt_return + "</td>";
        newList += "<td>" + response.data[i].note_return + "</td>";
        newList += "<td>" + response.data[i].soll + "</td>";
        newList +=
        '<td><button class="btn btn-info" onclick="accountVisionHelpers.getById(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil"style="color:#fff!important;" /></button></td>';
        newList +=
          '<td><button class="btn btn-danger" onclick="accountVisionHelpers.deleteRow(' +
          response.data[i].id +
          ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
        newList += "</tr>";
        $("#table_list_account_vision tbody").append(newList);
        $("#table_list_account_vision").show();
        $("#frmViewAccountVision").hide();
      }
    }
    lightGallery(document.getElementById('lightgallery'));
    });
     spinnerHelpers.hide();
    } 

export function SearchAccountVision(id_contovisione,code_gr,user_id_cv,note) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(id_contovisione,code_gr,user_id_cv,note);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  AccountVisionServices.SearchAccountVision(data, function (response) {
    $("#table_list_account_vision tbody").html('');
    if (response.data.length == 0) {
      var newList = "";
     newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_account_vision tbody").append(newList);
      $("#table_list_account_vision").show();
      $("#frmViewAccountVision").hide();
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        newList += "<tr>";
        newList += "<td>" + response.data[i].id + "</td>";
        newList += "<td>" + response.data[i].id_contovisione + "</td>";
        newList += "<td>" + response.data[i].date + "</td>";
        newList += "<td>" + response.data[i].ddt_gr + "</td>";
        newList += "<td>" + response.data[i].user_id + "</td>";
        newList += "<td>" + response.data[i].description + "</td>";
       newList += "<td>";
      for (var j = 0; j < response.data[i].imageaccountvision.length; j++) { 
      newList +=
       '<div class="container"><div id="lightgallery" style="display:flex">';
        newList +=
          "<a href='" +
         response.data[i].imageaccountvision[j].img  +
         "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
     }
      newList += "</td>";
        newList += "<td>" + response.data[i].note + "</td>";
        newList += "<td>" + response.data[i].code_gr + "</td>";
        newList += "<td>" + response.data[i].date_return + "</td>";
        newList += "<td>" + response.data[i].ddt_return + "</td>";
        newList += "<td>" + response.data[i].note_return + "</td>";
        newList += "<td>" + response.data[i].soll + "</td>";
        newList +=
        '<td><button class="btn btn-info" onclick="accountVisionHelpers.getById(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil"style="color:#fff!important;" /></button></td>';
        newList +=
          '<td><button class="btn btn-danger" onclick="accountVisionHelpers.deleteRow(' +
          response.data[i].id +
          ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
        newList += "</tr>";
        $("#table_list_account_vision tbody").append(newList);
        $("#table_list_account_vision").show();
        $("#frmViewAccountVision").hide();
      }
    }
    lightGallery(document.getElementById('lightgallery'));
  });
     spinnerHelpers.hide();
}

export function getById(id) {
  AccountVisionServices.getById(id, function(response) {
    var searchParam = "";
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmViewAccountVision #" + i).html(val);
        } else {
          $("#frmViewAccountVision #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
           $("#frmViewAccountVision #imgPreview").attr('src', val);
            $("#frmViewAccountVision #imgName").attr('src', val);
            $("#frmViewAccountVision #imgNameLink").attr('href', val);
          } else {
             $("#frmViewAccountVision #imgName").remove();
          }
        } else {
          $("#frmViewAccountVision #" + i).val(val);
          $("#frmViewAccountVision #" + i).html(val);
        }
      }

      var searchParam = "";
      var initials = [];
      initials.push({
        id: response.data.user_id,
        text: response.data.DescriptionUser_id
      });
      $("#frmViewAccountVision #user_id").select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl
            + "/api/v1/user/select",
          data: function(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var searchParam = "";
      $("#frmViewAccountVision #user_id").select2({
        ajax: {
          url:
            configData.wsRootServicesUrl  + "/api/v1/user/select",
          data: function(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });
    });

 var cont = 0;
    var imgAccountVision = response.data.imageaccountvision;
    $("#snapShot").html("");
    if (imgAccountVision.length > 0) {
      for (var j = 0; j < imgAccountVision.length; j++) {
        var html = "";
        if (cont < 4) {
          cont = cont + 1;
          html +=
            '<input type="hidden" id="imgBase64' +
            cont +
            '" value="' +
            imgAccountVision[j].img +
            '" name="imgBase64' +
            cont +
            '">';
          html +=
            '<a href="' +
            imgAccountVision[j].img +
            '"><img src="' +
            imgAccountVision[j].img +
            '" style="max-width:50px;"></a>';
          $("#snapShot").append(html);
        }
      }
    }

    lightGallery(document.getElementById("snapShot"));

    $.getScript(
      "https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.js"
    ).done(function() {
      Webcam.set({
        width: 525,
        height: 410,
        image_format: "jpeg",
        jpeg_quality: 100
      });
      Webcam.attach("#camera");
    });

    $("#frmViewAccountVision #btPic").on("click", function() {
      if (cont < 4) {
        cont = cont + 1;
        Webcam.snap(function(data_uri) {
          $("#snapShot").append(
            '<img src="' + data_uri + '" width="250px" height="200px" />'
          );
          $("#snapShot").append(
            '<input type="hidden" value="' +
              data_uri +
              '" id="imgBase64' +
              cont +
              '" name="imgBase64' +
              cont +
              '"/>'
          );
        });
      }
    });

     $("#frmViewAccountVision").show();
    $("#table_list_account_vision").hide();
    $("#frmSearchAccountVision").hide();
    $("#contovisione").hide();
  });
}

export function update() {
  spinnerHelpers.show();
  var data = {
    id: $("#frmViewAccountVision #id").val(),
    id_contovisione: $("#frmViewAccountVision #id_contovisione").val(),
    date: $("#frmViewAccountVision #date").val(),
    ddt_gr: $("#frmViewAccountVision #ddt_gr").val(),
    code_gr: $("#frmViewAccountVision #code_gr").val(),
    user_id: $("#frmViewAccountVision #user_id").val(),
    description: $("#frmViewAccountVision #description").val(),
    note: $("#frmViewAccountVision #note").val(),
    date_return: $("#frmViewAccountVision #date_return").val(),
    ddt_return: $("#frmViewAccountVision #ddt_return").val(),
    note_return: $("#frmViewAccountVision #note_return").val(),
    soll: $("#frmViewAccountVision #soll").val(), 
  };

var arrayimgAccountVision = [];
  for (var i = 0; i <= 4; i++) {
    if (
      functionHelpers.isValued($("#frmViewAccountVision #imgBase64" + i).val())
    ) {
      arrayimgAccountVision.push($("#frmViewAccountVision #imgBase64" + i).val());
    }
  }
  data.imageaccountvision = arrayimgAccountVision;

  AccountVisionServices.update(data, function() {
   /* $("#frmViewAccountVision #date").val("");
    $("#frmViewAccountVision #ddt_gr").val("");
    $("#frmViewAccountVision #code_gr").val("");
    $("#frmViewAccountVision #user_id").val("");
    $("#frmViewAccountVision #description").val("");
    $("#frmViewAccountVision #note").val("");
    $("#frmViewAccountVision #date_return").val("");
    $("#frmViewAccountVision #ddt_return").val("");
    $("#frmViewAccountVision #note_return").val("");
    $("#frmViewAccountVision #soll").val("");*/

    notificationHelpers.success("Modifica avvenuta con successo");
    spinnerHelpers.hide();
    window.location.reload();
    $("#frmViewAccountVision").show();
    $("#table_list_account_vision").hide(); 
    $("#frmSearchAccountVision").hide(); 
  });
}

export function deleteRow(id) {
  if (confirm("Sei sicuro di voler eliminare?")) {
    spinnerHelpers.show();
    AccountVisionServices.del(id, function(response) {
      notificationHelpers.success(
        dictionaryHelpers.getDictionary("DeleteCompleted")
      );
      spinnerHelpers.hide();
      window.location.reload();
    });
  }
}
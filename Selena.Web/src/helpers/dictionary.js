/***   dictionaryHelpers   ***/

export function getDictionary(property) {

  var value = _getDictionary(property);

  if (!functionHelpers.isValued(value)) {
    dictionaryServices.setByLanguage(function (result) {
      storageData.setDictionary(result.data);

      value = _getDictionary(property);
    });
  }

  return value;
}

export function _getDictionary(property) {
  var propertySplit = property.split('.');
  var propertyLength = propertySplit.length;
  var value;

  $.each(propertySplit, function (i, item) {
    if (i == propertyLength - 1) {
      $.each(storageData.sDictionary(), function (x, dictionary) {
        if (functionHelpers.isValued(dictionary[item])) {
          value = dictionary[item];
          return true;
        }
      });
      return true;
    }
  });

  return value;
}

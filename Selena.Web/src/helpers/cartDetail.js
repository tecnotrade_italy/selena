export function del(id) {
    spinnerHelpers.show();
    
    cartDetailServices.del(id, function (result) {
      spinnerHelpers.hide();
      window.location.reload();
    });
  }

  export function modifyQuantity(id, newQuantity) {
    spinnerHelpers.show();
    var data = {
      id : id,
      newQuantity: newQuantity
    };
    
    cartDetailServices.modifyQuantity(data, function (result) {
      spinnerHelpers.hide();
      window.location.reload();
    });

    
  }
//downloadHelpers

//downloadServices

export function downloadfile(user, token, items, order) {
  spinnerHelpers.show();
  
  var sUserId = storageData.sUserId();
  if (user == sUserId) {
    if (functionHelpers.isValued(token, items, order)) {
      var data = {
        user: user,
        token: token,
        items: items,
        order: order,
      };

      downloadServices.downloadfile(data, function (result) {
      $("#div-cont-img-digital").html("");
      for(var i = 0; i< result.data.length;i++){
        var html = "";
        if (result.data[i].img_digital == 'Link non valido') {
          html += '<div class="row justify-content-center" data-id-row="' + '" id="divcont-' + '" >';
          html += '<div class="col-12" style="text-align: center;">';
          html += "<span> " + result.data[i].img_digital + "<br>Attenzione, non puoi scaricare questo corso. Clicca qui per procedere all'acquisto:<br><a href='/corso-base-di-fotografia-digitale-online'>Clicca qui</a></span>";
          html += "</div>";
          html += "</div>";
        } else {
          html += '<div class="row justify-content-center" data-id-row="' + '" id="divcont-' + '" >';
          html += '<div class="col-12" style="text-align: center;">';
          html += '<div class="container"><div id="lightgallery">';
          html += "<span><br>Congratulazioni, adesso puoi scaricare il corso cliccando sul bottone qui sotto:<br></span>";
          html += "<div><a href='" + result.data[i].img_digital + "'><button class='btn btn-success' id='btnsucc' class='fr-fic fr-dii' style='color:#fff;'>Scarica corso</button></a></div></div></div>";          
          html += "</div>";
          html += "</div>";
          html += "<br>"; 
        }

      $("#div-cont-img-digital").append(html);   
     } 
      spinnerHelpers.hide();
      });
    }
  } else {
    notificationHelpers.error('Mi dispiace, non puoi scaricare questo corso!');
    window.location = '/login';  
  }
}
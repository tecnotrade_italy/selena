/***   internalRepairHelpers   ***/

export function getLastId() {
  InternalRepairServices.getLastId(function(response) {
    $("#frmViewInternalRepair #nr").val(response.data);
  });
}

export function getLastIdIntRepair() {
  InternalRepairServices.getLastIdIntRepair(function(response) {
    $("#frmViewInternalRepair #nr_rip_interna").val(response.data);
  });
}

export function getdescription(code_gr) {
  var data = {
    code_gr:code_gr,
  }
  InternalRepairServices.getdescription(data, function (response) {
      console.log(response.data);
    $("#frmViewUpdateInternalRepair #description").val(response.data.tipology + ' ' + response.data.note + ' ' + response.data.plant);
    console.log($("#frmViewUpdateInternalRepair #description").val(response.data.tipology + ' ' + response.data.note + ' ' + response.data.plant));
  });
}

export function getdescriptioninsert(code_gr) {

  var data = {
    code_gr:code_gr,
  }
    InternalRepairServices.getdescriptioninsert(data, function (response) {
    console.log(response.data);
    $("#frmViewInternalRepair #description").val(response.data.tipology + ' ' + response.data.note + ' ' + response.data.plant);
  });
}

export function insert() {
  spinnerHelpers.show();
  var data = {
    customer_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    img: $("#frmViewInternalRepair #imgBase64").val(),
    imgName: $("#frmViewInternalRepair #imgName").val(),
    nr_rip_interna: $("#frmViewInternalRepair #nr_rip_interna").val(),
    id: $("#frmViewInternalRepair #nr").val(),
    date: $("#frmViewInternalRepair #date").val(),
    ddt_exit: $("#frmViewInternalRepair #ddt_exit").val(),
    supplier: $("#frmViewInternalRepair #supplier").val(),  
    description: $("#frmViewInternalRepair #description").val(),
    code_gr: $("#frmViewInternalRepair #code_gr").val(),
    price: $("#frmViewInternalRepair #price").val(),
  };

  var arrayInternalRepairImgAgg = [];
  for (var i = 0; i < 4; i++) {
    if (functionHelpers.isValued($("#imgBase64" + i).val())) {
      arrayInternalRepairImgAgg.push($("#imgBase64" + i).val());
    }
  }
  data.imagecaptures = arrayInternalRepairImgAgg;

  InternalRepairServices.insert(data, function() {
    $("#frmViewInternalRepair #imgName").val("");
    $("#frmViewInternalRepair #imgBase64").val("");
    $("#frmViewInternalRepair #nr").val("");
    $("#frmViewInternalRepair #date").val("");
    $("#frmViewInternalRepair #nr_rip_interna").val("");
    $("#frmViewInternalRepair #ddt_exit").val("");
    $("#frmViewInternalRepair #supplier").val("");
    $("#frmViewInternalRepair #description").val("");
    $("#frmViewInternalRepair #code_gr").val("");
    $("#frmViewInternalRepair #price").val("");
    notificationHelpers.success("Inserimento avvenuto correttamente!");
    spinnerHelpers.hide();
    window.location.href = 'https://www.grsrl.net/gestione-riparazione-interna';
  });
}

export function getinternalRepairSearchViewOpen() {
  spinnerHelpers.show();
  InternalRepairServices.getinternalRepairSearchViewOpen(data, function (response) {
  $("#table_list_internal_repair tbody").html('');
  if (response.data.length == 0) {
    var newList = "";
    newList += "<tr>";
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += "</tr>";
    $("#table_list_internal_repair tbody").append(newList);
    $("#table_list_internal_repair").show();
    $("#frmViewUpdateInternalRepair").hide();
  } else {
    for (var i = 0; i < response.data.length; i++) {
      var newList = "";
      newList += "<tr>";
      newList += "<td>" + response.data[i].id + "</td>";
      newList += "<td>" + response.data[i].numero + "</td>";
      newList += "<td style='white-space: nowrap;'>" + response.data[i].date + "</td>";
      newList += "<td>" + response.data[i].ddt_exit + "</td>";
      newList += "<td>" + response.data[i].supplier + "</td>";
      newList += "<td>";
      for (var j = 0; j < response.data[i].imagecaptures.length; j++) { 
     newList +=
      '<div class="container"><div id="lightgallery" style="display:flex">';
       newList +=
         "<a href='" +
        response.data[i].imagecaptures[j].img  +
        "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
    }
      newList += "</td>";
      newList += "<td>" + response.data[i].description + "</td>";
      newList += "<td>" + response.data[i].code_gr + "</td>";
      newList += "<td style='white-space: nowrap;'>" + response.data[i].date_return_product_from_the_supplier + "</td>";
      newList += "<td>" + response.data[i].ddt_supplier + "</td>";
      newList += "<td>" + response.data[i].reference_supplier + "</td>";
      newList += "<td>" + response.data[i].notes_from_repairman + "</td>";
      newList += "<td>" + response.data[i].price + "</td>";
      newList +=
      '<td><button class="btn btn-info" onclick="internalRepairHelpers.getByInternalRepair(' +
      response.data[i].id +
      ')"><i class="fa fa-pencil" style="color:#fff!important;"/></button></td>';
      newList +=
        '<td><button class="btn btn-danger" onclick="internalRepairHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash"style="color:#fff!important;"/></button></td>';
      newList += "</tr>";
      $("#table_list_internal_repair tbody").append(newList);
      $("#table_list_internal_repair").show();
      $("#frmViewUpdateInternalRepair").hide();
    }
  }
  lightGallery(document.getElementById('lightgallery'));
});
  spinnerHelpers.hide();
}

export function getinternalRepairSearchViewClose() {
  spinnerHelpers.show();
  InternalRepairServices.getinternalRepairSearchViewClose(data, function (response) {
  $("#table_list_internal_repair tbody").html('');
  if (response.data.length == 0) {
    var newList = "";
    newList += "<tr>";
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += "</tr>";
    $("#table_list_internal_repair tbody").append(newList);
    $("#table_list_internal_repair").show();
    $("#frmViewUpdateInternalRepair").hide();
  } else {
    for (var i = 0; i < response.data.length; i++) {
      var newList = "";
      newList += "<tr>";
      newList += "<td>" + response.data[i].id + "</td>";
      newList += "<td>" + response.data[i].numero + "</td>";
      newList += "<td style='white-space: nowrap;'>" + response.data[i].date + "</td>";
      newList += "<td>" + response.data[i].ddt_exit + "</td>";
      newList += "<td>" + response.data[i].supplier + "</td>";
      newList += "<td>";
      for (var j = 0; j < response.data[i].imagecaptures.length; j++) { 
     newList +=
      '<div class="container"><div id="lightgallery" style="display:flex">';
       newList +=
         "<a href='" +
        response.data[i].imagecaptures[j].img  +
        "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
    }
      newList += "</td>";
      newList += "<td>" + response.data[i].description + "</td>";
      newList += "<td>" + response.data[i].code_gr + "</td>";
      newList += "<td style='white-space: nowrap;'>" + response.data[i].date_return_product_from_the_supplier + "</td>";
      newList += "<td>" + response.data[i].ddt_supplier + "</td>";
      newList += "<td>" + response.data[i].reference_supplier + "</td>";
      newList += "<td>" + response.data[i].notes_from_repairman + "</td>";
      newList += "<td>" + response.data[i].price + "</td>";
      newList +=
      '<td><button class="btn btn-info" onclick="internalRepairHelpers.getByInternalRepair(' +
      response.data[i].id +
      ')"><i class="fa fa-pencil" style="color:#fff!important;"/></button></td>';
      newList +=
        '<td><button class="btn btn-danger" onclick="internalRepairHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash"style="color:#fff!important;"/></button></td>';
      newList += "</tr>";
      $("#table_list_internal_repair tbody").append(newList);
      $("#table_list_internal_repair").show();
      $("#frmViewUpdateInternalRepair").hide();
    }
  }
  lightGallery(document.getElementById('lightgallery'));
});
  spinnerHelpers.hide();
}


export function getAllInternalRepairSearch(business_name,code_gr,code_intervention_gr,date) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(business_name,code_gr,code_intervention_gr,date);
InternalRepairServices.getAllInternalRepairSearch(data, function (response) {
  $("#table_list_internal_repair tbody").html('');
  if (response.data.length == 0) {
    var newList = "";
    newList += "<tr>";
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += "</tr>";
    $("#table_list_internal_repair tbody").append(newList);
    $("#table_list_internal_repair").show();
    $("#frmViewUpdateInternalRepair").hide();
  } else {
    for (var i = 0; i < response.data.length; i++) {
      var newList = "";
      newList += "<tr>";
      newList += "<td>" + response.data[i].id + "</td>";
      newList += "<td>" + response.data[i].numero + "</td>";
      newList += "<td style='white-space: nowrap;'>" + response.data[i].date + "</td>";
      newList += "<td>" + response.data[i].ddt_exit + "</td>";
      newList += "<td>" + response.data[i].supplier + "</td>";
      newList += "<td>";
      for (var j = 0; j < response.data[i].imagecaptures.length; j++) { 
      newList +=
       '<div class="container"><div id="lightgallery" style="display:flex">';
        newList +=
          "<a href='" +
         response.data[i].imagecaptures[j].img  +
         "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
     }
      newList += "</td>";
      newList += "<td>" + response.data[i].description + "</td>";
      newList += "<td>" + response.data[i].code_gr + "</td>";
      newList += "<td style='white-space: nowrap;'>" + response.data[i].date_return_product_from_the_supplier + "</td>";
      newList += "<td>" + response.data[i].ddt_supplier + "</td>";
      newList += "<td>" + response.data[i].reference_supplier + "</td>";
      newList += "<td>" + response.data[i].notes_from_repairman + "</td>";
      newList += "<td>" + response.data[i].price + "</td>";
      newList +=
      '<td><button class="btn btn-info" onclick="internalRepairHelpers.getByInternalRepair(' +
      response.data[i].id +
      ')"><i class="fa fa-pencil" style="color:#fff!important;"/></button></td>';
      newList +=
        '<td><button class="btn btn-danger" onclick="internalRepairHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash"style="color:#fff!important;"/></button></td>';
      newList += "</tr>";
      $("#table_list_internal_repair tbody").append(newList);
      $("#table_list_internal_repair").show();
      $("#frmViewUpdateInternalRepair").hide();
    }
  }
  lightGallery(document.getElementById('lightgallery'));
});
  spinnerHelpers.hide();
}

export function getAllInternalRepair() {
  spinnerHelpers.show();
  InternalRepairServices.getAllInternalRepair(storageData.sIdLanguage(), function (response) {  
    for (var i = 0; i < response.data.length; i++) {
      var newList = "";
      newList += "<tr>";
      newList += "<td>" + response.data[i].id + "</td>";
      newList += "<td>" + response.data[i].numero + "</td>";
      newList += "<td style='white-space: nowrap;'>" + response.data[i].date + "</td>";
      newList += "<td>" + response.data[i].ddt_exit + "</td>";
      newList += "<td>" + response.data[i].supplier + "</td>";
      newList += "<td>";
      for (var j = 0; j < response.data[i].imagecaptures.length; j++) { 
      newList +=
       '<div class="container"><div id="lightgallery" style="display:flex">';
        newList +=
          "<a href='" +
         response.data[i].imagecaptures[j].img  +
         "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
     }
      newList += "</td>";
      newList += "<td>" + response.data[i].description + "</td>";
      newList += "<td>" + response.data[i].code_gr + "</td>";
      newList += "<td style='white-space: nowrap;'>" + response.data[i].date_return_product_from_the_supplier + "</td>";
      newList += "<td>" + response.data[i].ddt_supplier + "</td>";
      newList += "<td>" + response.data[i].reference_supplier + "</td>";
      newList += "<td>" + response.data[i].notes_from_repairman + "</td>";
      newList += "<td>" + response.data[i].price + "</td>";
      newList +=
      '<td><button class="btn btn-info" onclick="internalRepairHelpers.getByInternalRepair(' +
      response.data[i].id +
      ')"><i class="fa fa-pencil" style="color:#fff!important;"/></button></td>';
      newList +=
        '<td><button class="btn btn-danger" onclick="internalRepairHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash"style="color:#fff!important;"/></button></td>';
      newList += "</tr>";
      $("#table_list_internal_repair tbody").append(newList);
      $("#table_list_internal_repair").show();
      $("#frmViewUpdateInternalRepair").hide();
    }
    lightGallery(document.getElementById('lightgallery'));
  });
  spinnerHelpers.hide();
}

export function getByInternalRepair(id) {
  // spinnerHelpers.show();
  InternalRepairServices.getByInternalRepair(id, function (response) {
    var searchParam = "";
    /* SELECT supplier */
    var initials = [];
    initials.push({
      id: response.data.supplier,
      text: response.data.Supplier
    });
    $("#supplier").select2({
      data: initials,
      ajax: {
        url: configData.wsRootServicesUrl  + '/api/v1/businessnamesupplier/select',
        data: function(params) {
          if (params.term == "") {
            searchParam = "*";
          } else {
            searchParam = params.term;
          }
          var query = {
            search: searchParam
          };
          return query;
        },
        processResults: function(data) {
          var dataParse = JSON.parse(data);

          return {
            results: dataParse.data
          };
        }
      }
    });
    /* END SELECT supplier */

    /*SELECT internal repair */
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        $("#frmViewUpdateInternalRepair #" + i).attr("checked", "checked");
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmViewUpdateInternalRepair #imgPreview").attr("src", val);
          } else {
            $("#frmViewUpdateInternalRepair #imgPreview").remove();
          }
        } else {
          $("#frmViewUpdateInternalRepair #" + i).val(val);
        }
      }
    });

    var cont = 0;
    var imgAggCaptures = response.data.imagecaptures;
    $("#snapShot").html("");
    if (imgAggCaptures.length > 0) {
      for (var j = 0; j < imgAggCaptures.length; j++) {
        var html = "";
        if (cont < 4) {
          cont = cont + 1;
          html +=
            '<input type="hidden" id="imgBase64' +
            cont +
            '" value="' +
            imgAggCaptures[j].img +
            '" name="imgBase64' +
            cont +
            '">';
          html +=
            '<a href="' +
            imgAggCaptures[j].img +
            '"><img src="' +
            imgAggCaptures[j].img +
            '" style="max-width:50px;"></a>';
          $("#snapShot").append(html);
        }
      }
    }

    lightGallery(document.getElementById("snapShot"));

    $.getScript(
      "https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.js"
    ).done(function() {
      Webcam.set({
        width: 525,
        height: 410,
        image_format: "jpeg",
        jpeg_quality: 100
      });
      Webcam.attach("#camera");
    });

    $("#frmViewUpdateInternalRepair #btPic").on("click", function() {
      if (cont < 4) {
        cont = cont + 1;
        Webcam.snap(function(data_uri) {
          $("#snapShot").append(
            '<img src="' + data_uri + '" width="250px" height="200px" />'
          );
          $("#snapShot").append(
            '<input type="hidden" value="' +
              data_uri +
              '" id="imgBase64' +
              cont +
              '" name="imgBase64' +
              cont +
              '"/>'
          );
        });
      }
    });
    $("#frmViewUpdateInternalRepair").show();
    $("#table_list_internal_repair").hide(); 
    $("#rip_interna").hide();
    $("#btnadd").hide();
  });
}

export function updateInternalRepair() {
  spinnerHelpers.show();
  var data = {
    id: $("#frmViewUpdateInternalRepair #id").val(),
    date: $("#frmViewUpdateInternalRepair #date").val(),
    ddt_exit: $("#frmViewUpdateInternalRepair #ddt_exit").val(),
    nr_rip_interna: $("#frmViewUpdateInternalRepair #nr_rip_interna").val(),
    supplier: $("#frmViewUpdateInternalRepair #supplier").val(),
    description: $("#frmViewUpdateInternalRepair #description").val(),
    code_gr: $("#frmViewUpdateInternalRepair #code_gr").val(),
    date_return_product_from_the_supplier: $("#frmViewUpdateInternalRepair #date_return_product_from_the_supplier").val(),
    ddt_supplier: $("#frmViewUpdateInternalRepair #ddt_supplier").val(),
    reference_supplier: $("#frmViewUpdateInternalRepair #reference_supplier").val(),
    notes_from_repairman: $("#frmViewUpdateInternalRepair #notes_from_repairman").val(),
    price: $("#frmViewUpdateInternalRepair #price").val(),
  };

 var arrayimgAggCaptures = [];
  for (var i = 0; i <= 4; i++) {
    if (
      functionHelpers.isValued($("#frmViewUpdateInternalRepair #imgBase64" + i).val())
    ) {
      arrayimgAggCaptures.push($("#frmViewUpdateInternalRepair #imgBase64" + i).val());
    }
  }
  data.imagecaptures = arrayimgAggCaptures;
  InternalRepairServices.updateInternalRepair(data, function() {
    $("#frmViewUpdateInternalRepair #date").val("");
    $("#frmViewUpdateInternalRepair #ddt_exit").val("");
    $("#frmViewUpdateInternalRepair #supplier").val("");
    $("#frmViewUpdateInternalRepair #nr_rip_interna").val("");
    $("#frmViewUpdateInternalRepair #description").val("");
    $("#frmViewUpdateInternalRepair #code_gr").val("");
    $("#frmViewUpdateInternalRepair #date_return_product_from_the_supplier").val("");
    $("#frmViewUpdateInternalRepair #ddt_supplier").val("");
    $("#frmViewUpdateInternalRepair #reference_supplier").val("");
    $("#frmViewUpdateInternalRepair #notes_from_repairman").val("");
    $("#frmViewUpdateInternalRepair #price").val("");
    notificationHelpers.success("Modifica avvenuta con successo");
    spinnerHelpers.hide();
    window.location.reload();
    $("#table_list_internal_repair").show();
    $("#frmViewUpdateInternalRepair").hide();
  });
}
export function deleteRow(id) {
  if (confirm("Sei sicuro di voler eliminare la riparazione?")) {
    spinnerHelpers.show();
    InternalRepairServices.del(id, function(response) {
      notificationHelpers.success(
        dictionaryHelpers.getDictionary("DeleteCompleted")
      );
      spinnerHelpers.hide();
      window.location.reload();
    });
  }
}

/* -------- SECTION RICERCA GLOBALE RIPARAZIONE INTERNA ED ESTERNA UNICA PAGINA RICERCA GLOBALE ----------- */

export function getAllInternalExternalRepairSearch(business_name_rip_int_est,code_gr_rip_int_est) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(business_name_rip_int_est,code_gr_rip_int_est);
  InternalRepairServices.getAllInternalExternalRepairSearch(data, function (response) {
  $("#table_list_internal_external_repair tbody").html('');
  if (response.data.length == 0) {
    var newList = "";
    newList += "<tr>";
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += "</tr>";
    $("#table_list_internal_external_repair tbody").append(newList);
    $("#table_list_internal_external_repair").show();
  } else {
    for (var i = 0; i < response.data.length; i++) {
      var newList = "";
      newList += "<tr>";
      newList += "<td>" + response.data[i].id + "</td>";
      newList += "<td>" + response.data[i].numero + "</td>";
      newList += "<td style='white-space: nowrap;'>" + response.data[i].date + "</td>";
      newList += "<td>" + response.data[i].ddt_exit + "</td>";
      newList += "<td>" + response.data[i].supplier + "</td>";
      newList += "<td>";
      for (var j = 0; j < response.data[i].imagecaptures.length; j++) { 
      newList +=
       '<div class="container"><div id="lightgallery" style="display:flex">';
        newList +=
          "<a href='" +
         response.data[i].imagecaptures[j].img  +
         "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
     }
     newList += "</td>";
     newList += "<td>" + response.data[i].description + "</td>";
     newList += "<td>" + response.data[i].code_gr + "</td>";
     newList += "<td style='white-space: nowrap;'>" + response.data[i].date_return_product_from_the_supplier + "</td>";
     newList += "<td>" + response.data[i].ddt_supplier + "</td>";
     newList += "<td>" + response.data[i].reference_supplier + "</td>";
     newList += "<td>" + response.data[i].notes_from_repairman + "</td>";
    //newList += "<td>" + response.data[i].price + "</td>";
      newList += "</tr>";
      $("#table_list_internal_external_repair tbody").append(newList);
      $("#table_list_internal_external_repair").show();
    }
  }
  lightGallery(document.getElementById('lightgallery'));
});
  spinnerHelpers.hide();
}

/* -------- END SECTION RICERCA GLOBALE RIPARAZIONE INTERNA ED ESTERNA UNICA PAGINA RICERCA GLOBALE ----------- */
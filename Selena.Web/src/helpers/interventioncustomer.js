/***   interventioncustomerHelpers   ***/

export function getAllInterventionCustomerSearch(id,nr_ddt,plant_type) {
  spinnerHelpers.show();
    var data = functionHelpers.formToJson(id,nr_ddt,plant_type);
  data["idUser"] = storageData.sUserId();
  interventionCustomerServices.getAllInterventionCustomerSearch(data, function (response) {
      $( "#table_list_customerintervention tbody").html('');
 for (var i = 0; i < response.data.length; i++) {
      var newList = "";
   newList += "<tr>";
      newList += "<td>" + response.data[i].code_intervention_gr + "</td>";
      newList += "<td>" + response.data[i].date_ddt + "</td>";
      newList += "<td>" + response.data[i].nr_ddt + "</td>";
      //newList += "<td>" + response.data[i].plant_type + "</td>";
   newList += "<td style='white-space: nowrap;font-size: 13px;'>" + response.data[i].description + "</td>";
      newList += "<td>"
     /* if (response.data[i].imgmodule != "") {
        newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">';
        newList +=
          "<a href='" +
          response.data[i].imgmodule +
          "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
      }*/

      for (var j = 0; j < response.data[i].captures.length; j++) { 
        newList +=
        '<div class="container"><div id="lightgallery" style="display:flex">';
          newList +=
            "<a href='" + response.data[i].captures[j].img  +  "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        } 
      newList += "</td>";
      newList += "<td>" + response.data[i].states_quotes_description + "</td>";
      newList += "<td>" + response.data[i].states_description + "</td>";
    newList += "<td>"
     if (response.data[i].states_quotes_description != 'Accettato' && response.data[i].states_quotes_description != 'Rifiutato' && response.data[i].checkQuotes == true) {
        newList +=
        '<button class="btn btn-link" onclick="interventioncustomerHelpers.getByViewQuotesCustomer(' +
        response.data[i].id +
        ')"><i class="fa fa-eur"/></button>';
     } 
 
        if (response.data[i].checkQuotes == false) {
            newList += "<div>" + "</div>";
        } 
        /*if (response.data[i].checkQuotes == true) {
            newList +=
          '<button class="btn btn-link" onclick="interventioncustomerHelpers.getByViewQuotesCustomer(' +
          response.data[i].id +
          ')"><i class="fa fa-eur"/></button>'
        } */

     if (response.data[i].states_quotes_description == 'Accettato' && response.data[i].checkQuotes == true) {
        newList +=
        '<button class="btn btn-green" style="color:green;" onclick="interventioncustomerHelpers.getByViewQuotesCustomer(' +
        response.data[i].id +
        ')"><i class="fa fa-eur" style="color:green!important;"/></button>';
     }
     if (response.data[i].states_quotes_description == 'Rifiutato' && response.data[i].checkQuotes == true) {
        newList +=
        '<button class="btn btn-green" style="color:red;" onclick="interventioncustomerHelpers.getByViewQuotesCustomer(' +
        response.data[i].id +
        ')"><i class="fa fa-eur" style="color:red!important;"/></button>';
     }
   "</td>";
   
      newList += "<td>"
      if (response.data[i]. checkModuleFault == true) {
      newList +=
        '<button class="btn btn-link" onclick="interventioncustomerHelpers.getByViewModuleCustomer(' +
        response.data[i].id +
     ')"><i class="fa fa-wrench"/></button>'
    } 
      if (response.data[i].checkModuleFault == false) {
        newList +=
            '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Attenzione:il tuo modulo guasti non è compilato, premere sul pulsante per eseguire la compilazione dei campi mancanti!" style="color:#ff7514!important;" onclick="interventioncustomerHelpers.getByViewModuleCustomer(' +
        response.data[i].id +
     ')"><i class="fa fa-exclamation-triangle" style="color:red!important;"/></button>'    
      } 
      "</td>"

   newList += "<td>"
  if (response.data[i]. checkModuleValidationGuarantee == true) {
    newList +=
  '<button class="btn btn-link" onclick="interventioncustomerHelpers.getByViewModuleValidationGuarantee(' +
        response.data[i].id +
    ')"><i class="fa fa-certificate"/></button>'
    }
   
   if (response.data[i].checkModuleValidationGuarantee == false) {
    newList +=
  '<button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Attenzione:il tuo modulo garanzia non è compilato, premere sul pulsante per eseguire la compilazione dei campi mancanti!" style="color:#ff7514!important;" onclick="interventioncustomerHelpers.getByViewModuleValidationGuarantee(' +
        response.data[i].id +
    ')"><i class="fa fa-exclamation-triangle" style="color:red!important;"/></button>'
    }
   "</td>"

   //   newList +=
        //'<td><button class="btn btn-link" onclick="interventioncustomerHelpers.getByViewModuleCustomer(' +
       // response.data[i].id +
    // ')"><i class="fa fa-wrench"/></button></td>';   

 
   /* newList +=
        '<td><button class="btn btn-link" onclick="interventioncustomerHelpers.getByViewModuleValidationGuarantee(' +
        response.data[i].id +
     ')"><i class="fa fa-certificate"/></button></td>'; */
    
      newList += "<td>" + response.data[i].nr_ddt_gr + "</td>";
      newList += "<td>" + response.data[i].date_ddt_gr + "</td>";
      newList += "</tr>"; 
      $("#table_list_customerintervention tbody").append(newList);
      $("#table_list_customerintervention").show();
      $("#frmViewQuotesCustomer").hide(); 
      $("#ViewFaultModule").hide();
      $("#ModuleValidationGuarantee").hide();

}
    lightGallery(document.getElementById("lightgallery"));
  });
  spinnerHelpers.hide();
}

export function getAllInterventionCustomer() {
  spinnerHelpers.show();
 interventionCustomerServices.getAllInterventionCustomer(storageData.sUserId(), function(response) {
  $("#table_list_customerintervention tbody").html('');
if (response.data.length == 0) {
    var newList = "";
    newList += "<tr>";
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td><h2 style="white-space: nowrap;">Nessuna Riparazione Presente!</h2></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += '<td></td>';
    newList += "</tr>";
    $("#table_list_customerintervention tbody").append(newList);
    $("#table_list_customerintervention").show();
    $("#frmViewQuotesCustomer").hide(); 
    $("#ViewFaultModule").hide();
    $("#ModuleValidationGuarantee").hide();
}else{
 for (var i = 0; i < response.data.length; i++) {
   var newList = "";
   newList += "<tr>";
      newList += "<td>" + response.data[i].code_intervention_gr + "</td>";
      newList += "<td>" + response.data[i].date_ddt + "</td>";
      newList += "<td>" + response.data[i].nr_ddt + "</td>";
      newList += "<td style='white-space: nowrap;font-size: 13px;'>" + response.data[i].description + "</td>";
      newList += "<td>" 
        for (var j = 0; j < response.data[i].captures.length; j++) { 
          newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">';
            newList +=
              "<a href='" + response.data[i].captures[j].img  +  "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
          } 
      newList += "</td>";
      newList += "<td>" + response.data[i].states_quotes_description + "</td>";
      newList += "<td>" + response.data[i].states_description + "</td>";
      newList += "<td>"
     if (response.data[i].states_quotes_description != 'Accettato' && response.data[i].states_quotes_description != 'Rifiutato' && response.data[i].checkQuotes == true) {
        newList +=
        '<button class="btn btn-link" onclick="interventioncustomerHelpers.getByViewQuotesCustomer(' +
        response.data[i].id +
        ')"><i class="fa fa-eur"/></button>';
     } 
        if (response.data[i].checkQuotes == false) {
            newList += "<div>" + "</div>";
        } 
     if (response.data[i].states_quotes_description == 'Accettato' && response.data[i].checkQuotes == true) {
        newList +=
        '<button class="btn btn-green" style="color:green;" onclick="interventioncustomerHelpers.getByViewQuotesCustomer(' +
        response.data[i].id +
        ')"><i class="fa fa-eur" style="color:green!important;"/></button>';
     }
     if (response.data[i].states_quotes_description == 'Rifiutato' && response.data[i].checkQuotes == true) {
        newList +=
        '<button class="btn btn-green" style="color:red;" onclick="interventioncustomerHelpers.getByViewQuotesCustomer(' +
        response.data[i].id +
        ')"><i class="fa fa-eur" style="color:red!important;"/></button>';
     }
   "</td>";
   
      newList += "<td>"
      if (response.data[i]. checkModuleFault == true) {
      newList +=
        '<button class="btn btn-link" onclick="interventioncustomerHelpers.getByViewModuleCustomer(' +
        response.data[i].id +
     ')"><i class="fa fa-wrench"/></button>'
    } 
      if (response.data[i].checkModuleFault == false) {
        newList +=
            '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Attenzione:il tuo modulo guasti non è compilato, premere sul pulsante per eseguire la compilazione dei campi mancanti!" style="color:#ff7514!important;" onclick="interventioncustomerHelpers.getByViewModuleCustomer(' +
        response.data[i].id +
     ')"><i class="fa fa-exclamation-triangle" style="color:red!important;"/></button>'    
      } 
      "</td>"

   newList += "<td>"
  if (response.data[i]. checkModuleValidationGuarantee == true) {
    newList +=
  '<button class="btn btn-link" onclick="interventioncustomerHelpers.getByViewModuleValidationGuarantee(' +
        response.data[i].id +
    ')"><i class="fa fa-certificate"/></button>'
    }
   
   if (response.data[i].checkModuleValidationGuarantee == false) {
    newList +=
  '<button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Attenzione:il tuo modulo garanzia non è compilato, premere sul pulsante per eseguire la compilazione dei campi mancanti!" style="color:#ff7514!important;" onclick="interventioncustomerHelpers.getByViewModuleValidationGuarantee(' +
        response.data[i].id +
    ')"><i class="fa fa-exclamation-triangle" style="color:red!important;"/></button>'
    }
   "</td>"
      newList += "<td>" + response.data[i].nr_ddt_gr + "</td>";
      newList += "<td>" + response.data[i].date_ddt_gr + "</td>";
      newList += "</tr>"; 
      $("#table_list_customerintervention tbody").append(newList);
      $("#table_list_customerintervention").show();
      $("#frmViewQuotesCustomer").hide(); 
      $("#ViewFaultModule").hide();
      $("#ModuleValidationGuarantee").hide();
  }
}
    lightGallery(document.getElementById("lightgallery"));
  });
  spinnerHelpers.hide();
}
   
 

export function getByViewModuleValidationGuarantee(id) {
  interventionCustomerServices.getByViewModuleValidationGuarantee(id, function (response) {
  
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        if (i == "id") {
          $("#ModuleValidationGuarantee #" + i).html(val);
        } else {
          $("#ModuleValidationGuarantee #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#ModuleValidationGuarantee #imgPreview").attr("src", val);
          } else {
            $("#ModuleValidationGuarantee #imgName").remove();
          }
        } else {
          $("#ModuleValidationGuarantee #" + i).val(val);
          $("#ModuleValidationGuarantee #" + i).html(val);
          }
      } 
 
      var searchParam = "";
      var initials = [];
      initials.push({ 
        id: response.data.external_referent_id,
        text: response.data.DescriptionReferent
      });
      $('#ModuleValidationGuarantee #external_referent_id').select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl +
            "/api/v1/people/getSelectExternalReferent",
          data: function(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });
  });
   
      $("#ModuleValidationGuarantee").show();
      $("#ViewFaultModule").hide();
      $("#frmViewQuotesCustomer").hide();
      $("#table_list_customerintervention").hide();
    $("#frmSearchCustomerPanel").hide();
    $("#pannclienti").hide();
    
  
if (response.data.insulation_value_tewards_machine_frame_positive != null) {
    $('#insulation_value_tewards_machine_frame_positive').prop("disabled", true);
      $('#note').show();
} else {
      $('#note').hide();
 } 
if (response.data.insulation_value_tewards_machine_frame_negative != null) {
    $('#insulation_value_tewards_machine_frame_negative').prop("disabled",true);
   $('#note').show();
} else {
      $('#note').hide();
 } 
if (response.data.single_motor_with_traction_motor_range != null) {
    $('#single_motor_with_traction_motor_range').prop("disabled",true);
   $('#note').show();
} else {
      $('#note').hide();
 }  
if (response.data.single_motor_with_brush_traction_motor != null) {
    $('#single_motor_with_brush_traction_motor').prop("disabled",true);
  $('#note').show();
} else {
      $('#note').hide();
 } 
if (response.data.single_motor_u_v_w_traction_phase_motor != null) {
    $('#single_motor_u_v_w_traction_phase_motor').prop("disabled",true);
    $('#note').show();
} else {
      $('#note').hide();
 } 
if (response.data.single_motor_u_v_w_motor_lifting_phase != null) {
    $('#single_motor_u_v_w_motor_lifting_phase').prop("disabled",true);
   $('#note').show();
} else {
      $('#note').hide();
 } 
if (response.data.traction != null) {
    $('#traction').prop("disabled",true);
   $('#note').show();
} else {
      $('#note').hide();
 } 
if (response.data.voltage != null) {
    $('#voltage').prop("disabled",true);
   $('#note').show();
} else {
      $('#note').hide();
 }  
if (response.data.plant_type != null) {
    $('#plant_type').prop("disabled",true);
    $('#note').show();
} else {
      $('#note').hide();
 } 
if (response.data.trolley_type != null) {
    $('#trolley_type').prop("disabled",true);
    $('#note').show();
} else {
      $('#note').hide();
 } 
if (response.data.date_mount != null) {
    $('#date_mount').prop("disabled",true);
   $('#note').show();
} else {
      $('#note').hide();
 }  
if (response.data.external_referent_id != null) {
    $('#external_referent_id').prop("disabled",true);
    $('#note').show();
} else {
      $('#note').hide();
 } 
if (response.data.bi_engine_with_traction_motor_range != null) {
    $('#bi_engine_with_traction_motor_range').prop("disabled",true);
   $('#note').show();
} else {
      $('#note').hide();
 } 
if (response.data.bi_engine_with_brush_traction_motor != null) {
    $('#bi_engine_with_brush_traction_motor').prop("disabled",true);
  $('#note').show();
} else {
      $('#note').hide();
 } 
if (response.data.bi_engine_u_v_w_traction_phase_motor != null) {
    $('#bi_engine_u_v_w_traction_phase_motor').prop("disabled",true);
   $('#note').show();
} else {
      $('#note').hide();
 } 
if (response.data.bi_engine_u_v_w_motor_lifting_phase != null) {
    $('#bi_engine_u_v_w_motor_lifting_phase').prop("disabled",true);
   $('#note').show();
} else {
      $('#note').hide();
 } 
if (response.data.lift_with_field_brush_motor_raised != null) {
    $('#lift_with_field_brush_motor_raised').prop("disabled",true);
   $('#note').show();
} else {
      $('#note').hide();
 } 
if (response.data.battery_voltage != null) {
    $('#battery_voltage').prop("disabled",true);
  $('#note').show();
} else {
      $('#note').hide();
 }   
if (response.data.traction_limit_current_consumption != null) {
    $('#traction_limit_current_consumption').prop("disabled",true);
  $('#note').show();
} else {
      $('#note').hide();
 } 
if (response.data.lift_limit_current_absorption != null) {
    $('#lift_limit_current_absorption').prop("disabled",true);
    $('#note').show();
} else {
      $('#note').hide();
 } 
    if (response.data.lift_with_u_v_w_motor_lift_pharse != null) {
    $('#lift_with_u_v_w_motor_lift_pharse').prop("disabled",true);
    $('#note').show();
} else {
      $('#note').hide();
 }  
  });
} 

export function getByViewModuleCustomer(id) {
  interventionCustomerServices.getByViewModuleCustomer(id, function (response) {
  var searchParam = "";
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        if (i == "id") {
          $("#ViewFaultModule #" + i).html(val);
        } else {
          $("#ViewFaultModule #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#ViewFaultModule #imgPreview").attr("src", val);
          } else {
            $("#ViewFaultModule #imgName").remove();
          }
        } else {
          $("#ViewFaultModule #" + i).val(val);
          $("#ViewFaultModule #" + i).html(val);
          }
      } 
      
      var searchParam = "";
      var initials = [];
      initials.push({ 
        id: response.data.external_referent_id,
        text: response.data.DescriptionReferent
      });
      $('#ViewFaultModule #external_referent_id').select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl +
            "/api/v1/people/getSelectExternalReferent",
          data: function(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });
     });
      $("#ViewFaultModule").show();
      $("#frmViewQuotesCustomer").hide();
      $("#table_list_customerintervention").hide();
      $("#ModuleValidationGuarantee").hide();
      $("#frmSearchCustomerPanel").hide();
      $("#pannclienti").hide();

    if (response.data.external_referent_id != null) {
      $('#ViewFaultModule #external_referent_id').prop("disabled", true);
      $('#ViewFaultModule #additional_notes').show();
    } else {
      $('#ViewFaultModule #external_referent_id').prop("disabled", false);
      $('#ViewFaultModule #additional_notes').hide();
    } 

    if (response.data.date_ddt != null) {
      $('#ViewFaultModule #date_ddt').prop("disabled", true);
      $('#ViewFaultModule #additional_notes').show();
    } else {
      $('#ViewFaultModule #date_ddt').prop("disabled", false);
      $('#ViewFaultModule #additional_notes').hide();
    } 
    if (response.data.trolley_type != null) {
      $('#ViewFaultModule #trolley_type').prop("disabled", true);
      $('#ViewFaultModule #additional_notes').show();
    } else {
      $('#ViewFaultModule #trolley_type').prop("disabled", false);
      $('#ViewFaultModule #additional_notes').hide();
    } 
    if (response.data.series != null) {
      $('#ViewFaultModule #series').prop("disabled", true);
      $('#ViewFaultModule #additional_notes').show();
    } else {
      $('#ViewFaultModule #series').prop("disabled", false);
      $('#ViewFaultModule #additional_notes').hide();
    } 
    if (response.data.phone_number != null) {
      $('#ViewFaultModule #phone_number').prop("disabled", true);
      $('#ViewFaultModule #additional_notes').show();
    } else {
      $('#ViewFaultModule #phone_number').prop("disabled", false);
      $('#ViewFaultModule #additional_notes').hide();
    } 
    if (response.data.voltage != null) {
      $('#ViewFaultModule #voltage').prop("disabled", true);
      $('#ViewFaultModule #additional_notes').show();
    } else {
      $('#ViewFaultModule #voltage').prop("disabled", false);
      $('#ViewFaultModule #additional_notes').hide();
    }   
    if (response.data.plant_type != null) {
      $('#ViewFaultModule #plant_type').prop("disabled", true);
      $('#ViewFaultModule #additional_notes').show();
    } else {
      $('#ViewFaultModule #plant_type').prop("disabled", false);
      $('#ViewFaultModule #additional_notes').hide();
    } 
    if (response.data.defect != null) {
      $('#ViewFaultModule #defect').prop("disabled", true);
      $('#ViewFaultModule #additional_notes').show();
    } else {
      $('#ViewFaultModule #defect').prop("disabled", false);
      $('#ViewFaultModule #additional_notes').hide();
    } 
    if (response.data.exit_notes != null) {
      $('#ViewFaultModule #exit_notes').prop("disabled", true);
      $('#ViewFaultModule #additional_notes').show();
    } else {
      $('#ViewFaultModule #exit_notes').prop("disabled", false);
      $('#ViewFaultModule #additional_notes').hide();
    } 
    if (response.data.nr_ddt != null) {
        $('#ViewFaultModule #nr_ddt').prop("disabled", true);
        $('#ViewFaultModule #additional_notes').show();
    } else {
        $('#ViewFaultModule #nr_ddt').prop("disabled", false);
        $('#ViewFaultModule #additional_notes').hide();
    }
    if (response.data.additional_notes != null) {
        $('#ViewFaultModule #additional_notes').prop("disabled", true);  
    } else {
        $('#ViewFaultModule #additional_notes').prop("disabled", false);
    }
  });
} 

  export function getByViewQuotesCustomer(id) {
    interventionCustomerServices.getByViewQuotesCustomer(id, function (response) {
    var searchParam = "";
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmViewQuotesCustomer #" + i).html(val);
        } else {
          $("#frmViewQuotesCustomer #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmViewQuotesCustomer #imgPreview").attr("src", val);
          } else {
            $("#frmViewQuotesCustomer #imgName").remove();
          }
        } else {
          $("#frmViewQuotesCustomer #" + i).val(val);
          $("#frmViewQuotesCustomer #" + i).html(val);
     }
      }

      var searchParam = "";
      var initials = [];
      initials.push({
        id: response.data.id_states_quote,
        text: response.data.States_Description
      });
      $("#id_states_quote").select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl +
            "/api/v1/StatesQuotesGr/selectStatesQuotesGr",
          data: function(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var AggArticlesQuotes = response.data.Articles;
      $("#div-cont-codegr_description-agg").html("");
      if (AggArticlesQuotes.length > 0) {
        for (var j = 0; j < AggArticlesQuotes.length; j++) {
          var html = "";
          html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
          html += '<div class="col-4">';
          html +=
            '<label for="description">Descrizione:<br><span class="description">' +
            AggArticlesQuotes[j].description +
            "</span></label>";
          html += "</div>";
          html += '<div class="col-4">';
          html +=
            '<label for="code_gr">Codice Articolo:<br><span class="code_gr">' +
            AggArticlesQuotes[j].code_gr +
            "</span></label>";
          html += "</div>";
          html += "</div>";
          $("#div-cont-codegr_description-agg").append(html);
         }
      }

      var MessagesQuotes = response.data.Messages;
      console.log(response.data.Messages);
      $("#div-cont-messages").html("");
      if (MessagesQuotes.length > 0) {
        for (var j = 0; j < MessagesQuotes.length; j++) {
          if (storageData.sUserId() == MessagesQuotes[j].id_sender_id) {
            var html = "";
            html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
            html += '<div class="col-12">';
            html += '<div class="chat-history">';
            html += '<ul class="m-b-0">';
            html += '<li class="clearfix"style="    list-style: none;margin-bottom: 30px;">';
            html += '<div class="message-data text-right" style= "margin-bottom: 15px">';
            html += '<span class="message-data-time" style=" color: #434651;padding-left: 6px">' + MessagesQuotes[j].created_at + '</span>';
            html += '<img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="avatar" style="border-radius: 40px;width: 40px;">';
            html += '</div>';
            html += '<div class="message other-message float-right" style="background: #e8f1f3; color: #444;padding: 18px 20px;text-align: right;line-height: 26px;font-size: 16px;border-radius: 7px;display: inline-block; position: relative;"> ' + MessagesQuotes[j].messages + '</div>';
            html += '</li>';
            html += '</ul>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            $("#div-cont-messages").append(html);

           /* if (storageData.sUserId() == MessagesQuotes[j].id_sender_id) {
            }else{

            }*/

          }else{
            var html = "";
            html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
            html += '<div class="col-12">';
            html += '<div class="chat-history">';
            html += '<ul class="m-b-0">';
            html += '<li class="clearfix"style="list-style: none;margin-bottom: 30px;">';
            html += '<div class="message-data" style= "margin-bottom: 15px">';
            html += '<span class="message-data-time">' + MessagesQuotes[j].created_at + '</span>';
            html += '</div>';
            html += '<div class="message my-message" style="background: #efefef;color: #444;padding: 18px 20px;line-height: 26px;font-size: 16px;border-radius: 7px;display: inline-block;position: relative;">' + MessagesQuotes[j].messages + '</div>';
            html += '</li>';
            html += '</ul>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            $("#div-cont-messages").append(html);
          }


        /*  if (storageData.sUserId() == MessagesQuotes[j].id_sender_id) {
            var html = "";
            html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
            html += '<div class="col-12">';
            html += '<div class="chat-history">';
            html += '<ul class="m-b-0">';
            html += '<li class="clearfix"style="    list-style: none;margin-bottom: 30px;">';
            html += '<div class="message-data text-right" style= "margin-bottom: 15px">';
            html += '<span class="message-data-time" style=" color: #434651;padding-left: 6px">' + MessagesQuotes[j].created_at + '</span>';
            html += '<img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="avatar" style="border-radius: 40px;width: 40px;">';
            html += '</div>';
            html += '<div class="message other-message float-right" style="background: #e8f1f3; color: #444;padding: 18px 20px;text-align: right;line-height: 26px;font-size: 16px;border-radius: 7px;display: inline-block; position: relative;"> ' + MessagesQuotes[j].messages + '</div>';
            html += '</li>';
            html += '</ul>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            $("#div-cont-messages").append(html);
          }else{
            var html = "";
            html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
            html += '<div class="col-12">';
            html += '<div class="chat-history">';
            html += '<ul class="m-b-0">';
            html += '<li class="clearfix"style="list-style: none;margin-bottom: 30px;">';
            html += '<div class="message-data" style= "margin-bottom: 15px">';
            html += '<span class="message-data-time">' + MessagesQuotes[j].created_at + '</span>';
            html += '</div>';
            html += '<div class="message my-message" style="background: #efefef;color: #444;padding: 18px 20px;line-height: 26px;font-size: 16px;border-radius: 7px;display: inline-block;position: relative;">' + MessagesQuotes[j].messages + '</div>';
            html += '</li>';
            html += '</ul>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            $("#div-cont-messages").append(html);
          }*/


          /*var html = "";
          html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
          html += '<div class="col-3">';
          html += '<label for="id_receiver">Ricevente:<br><span class="id_receiver">' + MessagesQuotes[j].id_receiver + "</span></label>";
          html += "</div>";
          html += '<div class="col-3">';
          html += '<label for="id_sender">Io:<br><span class="id_sender">' + MessagesQuotes[j].id_sender + "</span></label>";
          html += "</div>";
          html += '<div class="col-3">';
          html += '<label for="created_at">Data:<br><span class="created_at">' + MessagesQuotes[j].created_at + "</span></label>";
          html += "</div>";
          html += '<div class="col-3">';
          html +=
            '<label for="messages">Messaggio:<br><span class="messages">' + MessagesQuotes[j].messages + "</span></label>";
          html += "</div>"
          html += "</div>";
          $("#div-cont-messages").append(html);;*/
         }
      }
      



    });
    $("#frmViewQuotesCustomer").show();
    $("#ViewFaultModule").hide();
    $("#table_list_customerintervention").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#frmSearchCustomerPanel").hide();
    $("#pannclienti").hide();
     
      if (response.data.note_customer != null) {
        $('#note_customer').prop("disabled", true);
        $('#btnsaveupdatestatequotesAccRis').prop("disabled", true);
        $('#btnsaveupdatestatequotesRifRis').prop("disabled", true);
        $('#note_customer').show();
       } else {
         $('#note_customer').prop("disabled", false);
         $('#btnsaveupdatestatequotesAccRis').prop("disabled", false);
         $('#btnsaveupdatestatequotesRifRis').prop("disabled", false);
      } 
      if (response.data.States_Description == 'Accettato') {
        $('#btnsaveupdatestatequotesAccRis').prop("disabled", true);
        $('#btnsaveupdatestatequotesRifRis').prop("disabled", true);
        $('#note_customer').prop("disabled", true); 
      }  
       if (response.data.States_Description == 'Rifiutato') {
        $('#btnsaveupdatestatequotesAccRis').prop("disabled", true);
         $('#btnsaveupdatestatequotesRifRis').prop("disabled", true);
         $('#note_customer').prop("disabled", true); 
      }  

  });
}
 
export function updateInterventionUpdateViewCustomerQuotes() {
  spinnerHelpers.show();  
  var data = {
    updated_id: storageData.sUserId(),
    //idLanguage: storageData.sIdLanguage(),
    id_intervention: $('#frmViewQuotesCustomer #id').val(),
   // id_states_quote: $('#frmViewQuotesCustomer #id_states_quote').val(),
    id_states_quote: $('#frmViewQuotesCustomer #rifiutato').val(),
  };
  if (confirm("Vuoi confermare lo stato del tuo preventivo?")) {
  interventionCustomerServices.updateInterventionUpdateViewCustomerQuotes(data, function () {
    notificationHelpers.success("Modifica avvenuta con successo");
    spinnerHelpers.hide();
    $("#table_list_customerintervention").show();
    $("#frmViewQuotesCustomer").hide();
  });
}
}
export function updateInterventionUpdateViewCustomerQuotesAccept() {
  spinnerHelpers.show();  
  var data = {
    updated_id: storageData.sUserId(),
    //idLanguage: storageData.sIdLanguage(),
    id_intervention: $('#frmViewQuotesCustomer #id').val(),
   // id_states_quote: $('#frmViewQuotesCustomer #id_states_quote').val(),
  id_states_quote: $('#frmViewQuotesCustomer #accettato').val(),
  };
  if (confirm("Vuoi confermare lo stato del tuo preventivo?")) {
  interventionCustomerServices.updateInterventionUpdateViewCustomerQuotes(data, function () {
    notificationHelpers.success("Modifica avvenuta con successo");
    spinnerHelpers.hide();
    $("#table_list_customerintervention").show();
    $("#frmViewQuotesCustomer").hide();
  });
}
}
export function updateInterventionUpdateViewCustomerQuotesAcceptRiserva() {
  spinnerHelpers.show();  
  var data = {
    updated_id: storageData.sUserId(),
    id_intervention: $('#frmViewQuotesCustomer #id').val(),
    id_states_quote: $('#frmViewQuotesCustomer #accettatoconriserva').val(),
    note_customer: $('#frmViewQuotesCustomer #note_customer').val(),

  };
  if (confirm("Vuoi confermare lo stato del tuo preventivo?")) {
  interventionCustomerServices.updateInterventionUpdateViewCustomerQuotes(data, function () {
    notificationHelpers.success("Modifica e invio della nota aggiuntiva al destinatario eseguita con successo!");
    spinnerHelpers.hide();
    $("#table_list_customerintervention").show();
    $("#frmViewQuotesCustomer").hide();
  });
}
}
export function updateInterventionUpdateViewCustomerQuotesRifRiserva() {
  spinnerHelpers.show();  
  var data = {
    updated_id: storageData.sUserId(),
    id_intervention: $('#frmViewQuotesCustomer #id').val(),
    id_states_quote: $('#frmViewQuotesCustomer #rifiutatoconriserva').val(),
    note_customer: $('#frmViewQuotesCustomer #note_customer').val(),

  };
  if (confirm("Vuoi confermare lo stato del tuo preventivo?")) {
  interventionCustomerServices.updateInterventionUpdateViewCustomerQuotes(data, function () {
    notificationHelpers.success("Modifica e invio della nota aggiuntiva al destinatario eseguita con successo!");
    spinnerHelpers.hide();
    $("#table_list_customerintervention").show();
    $("#frmViewQuotesCustomer").hide();
  });
}
}


export function insertMessageCustomerQuotes() {
  spinnerHelpers.show(); 
  var data = {
    id_sender: storageData.sUserId(),
    id_intervention: $('#frmViewQuotesCustomer #id').val(),
    messages: $('#frmViewQuotesCustomer #messages').val(),
  };

  if (confirm("Vuoi confermare l'invio del messaggio?")) {
     interventionCustomerServices.insertMessageCustomerQuotes(data, function () {
      $('#frmViewQuotesCustomer #messages').val('');
      interventioncustomerHelpers.getByViewQuotesCustomer($('#frmViewQuotesCustomer #id').val());
      $('#frmViewQuotesCustomer').show();
      notificationHelpers.success("Invio del messaggio al destinatario eseguito con successo!");
      spinnerHelpers.hide();

   });
  }

}



export function updateSendFaultModule() {
  spinnerHelpers.show(); 

  var data = {
    updated_id: storageData.sUserId(),
    id: $('#ViewFaultModule #id').val(),    
    additional_notes: $('#ViewFaultModule #additional_notes').val(),
    nr_ddt: $('#ViewFaultModule #nr_ddt').val(),
    date_ddt: $('#ViewFaultModule #date_ddt').val(),
    trolley_type: $('#ViewFaultModule #trolley_type').val(),
    series: $('#ViewFaultModule #series').val(),
    voltage: $('#ViewFaultModule #voltage').val(),
    plant_type: $('#ViewFaultModule #plant_type').val(),
    defect: $('#ViewFaultModule #defect').val(),
    exit_notes: $('#ViewFaultModule #exit_notes').val(),
    external_referent_id: $('#ViewFaultModule #external_referent_id').val(),
  };
   interventionCustomerServices.updateSendFaultModule(data, function () {
    notificationHelpers.success("Modifica avvenuta con successo");
    spinnerHelpers.hide();
    $("#table_list_customerintervention").show();
    $("#ModuleValidationGuarantee").hide();
    $("#ViewFaultModule").hide(); 
  });
}



export function updateModuleValidationGuarantee() {
  spinnerHelpers.show();  
  var data = {
    updated_id: storageData.sUserId(),
    id_user: storageData.sUserId(),
    id: $('#ModuleValidationGuarantee #id').val(),
    id_intervention: $('#ModuleValidationGuarantee #id_intervention').val(),
    external_referent_id: $('#ModuleValidationGuarantee #external_referent_id').val(),
    date_mount: $('#ModuleValidationGuarantee #date_mount').val(),
    trolley_type: $('#ModuleValidationGuarantee #trolley_type').val(),
    plant_type: $('#ModuleValidationGuarantee #plant_type').val(),
    voltage: $('#ModuleValidationGuarantee #voltage').val(),
    traction: $('#ModuleValidationGuarantee #traction').val(),
    insulation_value_tewards_machine_frame_positive: $('#ModuleValidationGuarantee #insulation_value_tewards_machine_frame_positive').val(),
    insulation_value_tewards_machine_frame_negative: $('#ModuleValidationGuarantee #insulation_value_tewards_machine_frame_negative').val(),
    single_motor_with_traction_motor_range: $('#ModuleValidationGuarantee #single_motor_with_traction_motor_range').val(),
    single_motor_with_brush_traction_motor: $('#ModuleValidationGuarantee #single_motor_with_brush_traction_motor').val(),
    single_motor_u_v_w_traction_phase_motor: $('#ModuleValidationGuarantee #single_motor_u_v_w_traction_phase_motor').val(),
    single_motor_u_v_w_motor_lifting_phase: $('#ModuleValidationGuarantee #single_motor_u_v_w_motor_lifting_phase').val(),
    bi_engine_with_traction_motor_range: $('#ModuleValidationGuarantee #bi_engine_with_traction_motor_range').val(),
    bi_engine_with_brush_traction_motor: $('#ModuleValidationGuarantee #bi_engine_with_brush_traction_motor').val(),
    bi_engine_u_v_w_traction_phase_motor: $('#ModuleValidationGuarantee #bi_engine_u_v_w_traction_phase_motor').val(),
    bi_engine_u_v_w_motor_lifting_phase: $('#ModuleValidationGuarantee #bi_engine_u_v_w_motor_lifting_phase').val(),
    lift_with_field_brush_motor_raised: $('#ModuleValidationGuarantee #lift_with_field_brush_motor_raised').val(),
    lift_with_u_v_w_motor_lift_pharse: $('#ModuleValidationGuarantee #lift_with_u_v_w_motor_lift_pharse').val(),
    battery_voltage: $('#ModuleValidationGuarantee #battery_voltage').val(),
    traction_limit_current_consumption: $('#ModuleValidationGuarantee #traction_limit_current_consumption').val(),
    lift_limit_current_absorption: $('#ModuleValidationGuarantee #lift_limit_current_absorption').val(),
    note: $('#ModuleValidationGuarantee #note').val(),
  };
 
 
  interventionCustomerServices.updateModuleValidationGuarantee(data, function () {
    notificationHelpers.success("Modifica avvenuta con successo");
    spinnerHelpers.hide();
    $("#table_list_customerintervention").show();
    $("#ModuleValidationGuarantee").hide();
  });

}



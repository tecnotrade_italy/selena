/***   negotiationHelpers   ***/

export function insert(idForm) {
  spinnerHelpers.show();
  
  var data = functionHelpers.formToJson(idForm);
  
  negotiationServices.insert(data, function () {
    notificationHelpers.success('Messaggio inviato correttamente!');
    spinnerHelpers.hide();
    setTimeout(function(){
      window.location.href = "/trattative";
    }, 2000);
  });
}

export function insertDetail(idForm) {
  spinnerHelpers.show();
  
  var data = functionHelpers.formToJson(idForm);
  
  negotiationServices.insertDetail(data, function (response) {
    if(response.message!=''){
      $('#description').val('');
      negotiationHelpers.getDetailById(response.message);
      notificationHelpers.success('Messaggio inviato correttamente!');
      spinnerHelpers.hide();
    }
  });
}

export function getByUser(){
  spinnerHelpers.show();
  var userId = storageData.sUserId();
  $('#result-negotiation').html('');
  negotiationServices.getByUser(userId,function (response) {
    var html = "";
    for(var i=0;i<response.data.length;i++){
      html += "<tr>";
      html += "<td><button class='btn btn-success' onclick='negotiationHelpers.getDetailById(" + response.data[i].id + ");'><i class='fa fa-commenting-o' title='Visualizza il dettaglio'></i></button>";
      html += "<button class='btn btn-danger' onclick='negotiationHelpers.deleteRow(" + response.data[i].id + ")'><i class='fa fa-trash'></i></button>";
      html += "</td>";
      html += "<td><a target='_blank' href='" + response.data[i].link + "'><img class='img-negotiation' src='" + response.data[i].img + "'>" + response.data[i].item + "</a></td>";
      html += "<td>" + response.data[i].userNegotiatior + "<br>" + response.data[i].data + "</td>";
      if(response.data[i].toRead){
        html += "<td><i class='fa fa-envelope'></i></td>";
      }else{
        html += "<td><i class='fa fa-envelope-open-o'></i></td>";
      }
      html += "</tr>";
      
      $('#result-negotiation').append(html);
    }
    spinnerHelpers.hide();
  });
}

export function getDetailById(id){
  spinnerHelpers.show();
  var idUser = storageData.sUserId();
  negotiationServices.getDetailById(id,idUser,function (response) {
    $('#id-item-detail').html(response.data.item);

    $('#div-cont-msg-negotiation').html('');
    $('#user_id').val('');
    $('#negotiation_id').val('');

    $('#user_id').val(storageData.sUserId());
    $('#negotiation_id').val(id);
    
    for(var i=0;i<response.data.msg.length;i++){
      var html = "";
      html += "<div class='row'>";
      if(idUser == response.data.msg[i].idUserMsg){
        html += "<div class='col-12 text-right'>";
        //il messaggio estratto è scritto dall'utente che sta navigando
        html += "<div class='msg-user'><p>" + response.data.msg[i].description + "</p><span>" + response.data.msg[i].data + "</span></div>";
        html += "</div>";
      }else{
        html += "<div class='col-12 text-left'>";
        //il messaggio estratto è scritto da un utente diverso da quello che sta navigando
        html += "<div class='msg-user-negotiator text-left'><p>" + response.data.msg[i].description + "</p><span>" + response.data.msg[i].data + "</span></div>";
        html += "</div>";
      }
      html += "</div>";
      
      $('#div-cont-msg-negotiation').append(html);
      $('#tab-detail-negotiation').fadeIn('fast');
      
    }
    $('#div-cont-detail-negotiation').animate({
        scrollTop: $("#div-cont-msg-negotiation")[0].scrollHeight
    }, 'fast');
    negotiationHelpers.getByUser();
    spinnerHelpers.hide();
  });
}

export function deleteRow(id){
  if (confirm('Proseguendo con l\'eliminazione anche l\'altro utente non vedrà più la trattativa, confermi?')) {
    spinnerHelpers.show();
    negotiationServices.deleteRow(id,function (response) {
      negotiationHelpers.getByUser();
      notificationHelpers.success('Trattativa eliminata correttamente!');
      spinnerHelpers.hide();
    });
  }
}



/***   notificationHelpers   ***/

export function error(message) {
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "4000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };
  toastr.error(message,dictionaryHelpers.getDictionary('Error'));
  if (typeof notificationData !== "undefined") {
    notificationData.put('error',message);
    this.readNotification();
  }
}

export function success(message,title) {
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "4000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };

  if (functionHelpers.isValued(title)) {
    toastr.success(message,title);
  }
  else {
    toastr.success(message);
  }

  notificationData.put('success',message);

  this.readNotification();
}

export function warning(message) {
  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "4000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  };
  toastr.warning(message,dictionaryHelpers.getDictionary('Warning'));
  notificationData.put('warning',message);
  this.readNotification();
}

export function readNotification() {
  if (functionHelpers.isValued(notificationData.data())) {
    // Gestione notifiche non lette
    var notificationUnread = 0;
    $.each(notificationData.data(),function (i,item) {
      if (!item.read) {
        notificationUnread++;
      }
    });

    if (notificationUnread > 0) {
      $('#lblNumberNotification').text(notificationUnread);
    }
    else {
      $('#lblNumberNotification').text("");
    }

    $('#divNotification').html('');
    $('#divNotification').append('<div class="dropdown-header text-center"><strong>' + dictionaryHelpers.getDictionary('NotificationUnread').replace('{0}',notificationUnread) + '</strong></div>');

    var l = 10;

    if (notificationData.data().length < 10) {
      l = notificationData.data().length;
    }

    for (var i = 0; i < l; i++) {
      var notification = notificationData.data()[i];
      switch (notification.type) {
        case 'error':
          $('#divNotification').append('<a href="#" class="dropdown-item"><i class="fa fa-exclamation-triangle text-danger"></i> ' + notification.message + '</a>');
          break;
        case 'success':
          $('#divNotification').append('<a href="#" class="dropdown-item"><i class="fa fa-check text-success"></i> ' + notification.message + '</a>');
          break;
        case 'warning':
          $('#divNotification').append('<a href="#" class="dropdown-item"><i class="fa fa-exclamation-triangle text-warning"></i> ' + notification.message + '</a>');
          break;
        default:
          $('#divNotification').append('<a href="#" class="dropdown-item">' + notification.message + '</a>');
          break;
      }
    }
  }
}

export function readAll() {
  notificationData.readAll();
}

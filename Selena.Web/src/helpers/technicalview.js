//technicalviewHelpers //

  export function getAllProcessingTechnical() {
  spinnerHelpers.show();
  technicalviewServices.getAllProcessingTechnical(storageData.sUserId(), function (response) {
    for (var i = 0; i < response.data.length; i++) {
      var newList = "";
      if (response.data[i].ready == true) {
        newList += "<tr class='color-orange'>";
      } else {
        newList += "<tr style=''>";
      }   
        if (response.data[i].working_without_testing == true) {
        newList += "<tr class='color-orange'>";
      }
      if (response.data[i].working_with_testing == true) {
        newList += "<tr class='color-orange'>";
      } 
      if (response.data[i].nr_ddt_gr != null && response.data[i].nr_ddt_gr != '-' && response.data[i].nr_ddt_gr != "" && response.data[i].ready == true) {
        newList += "<tr class='color-white'>";
      };
      if (response.data[i].date_ddt_gr != null && response.data[i].date_ddt_gr != '-' && response.data[i].date_ddt_gr != "" && response.data[i].ready == true) {
        newList += "<tr class='color-white'>";
      };
      newList += "<td>";

      if (response.data[i].escape_from == true) {
        newList +=
          '<button class="btn btn-green" style="color:#8f00ff;font-size:15px;"><i class="fa fa-circle" style="color:#8f00ff!important;"/></button>';
      }
      if (response.data[i].to_call == true) {
        newList +=
          '<button class="btn btn-green" style="color:#03876e;font-size:15px;"><i class="fa fa-circle" style="color:#03876e!important;"/></button>';
      }
       if (response.data[i].search_product == true) {
        newList +=
          '<button class="btn btn-green" style="color:#00bfff;font-size:15px;"><i class="fa fa-circle" style="color:#00bfff!important;"/></button>';
      }
      newList +=
        '</td><td><button class="btn btn-primary-quotes" onclick="processingviewHelpers.getByViewValue(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil-square-o"/></button></td>';
      newList += "<td>" + response.data[i].id + "</td>";
      newList += "<td>" + response.data[i].descriptionCustomer + "</td>";
      newList += "<td>" + response.data[i].defect;
      newList += "</td>";
      newList += "<td>"
      if (response.data[i].imgmodule != '') {
        newList += '<div class="container"><div id="lightgallery" style="display:flex">';
        newList += "<a href='" + response.data[i].imgmodule + "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
      }
      newList += "</td>"
      newList += "<td>" + response.data[i].referent;
      newList += "</td>";

      newList +=
        '<td><button class="btn btn-primary-quotes" style="color:black" onclick="quotesHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-file-archive-o"/></button></td>';
      newList += "</tr>";

      $("#row-table-fld-pre").show();
      $("#frmViewProcessingSheet").hide();
      $("#table_list_processingSearch tbody").append(newList);
    }
    lightGallery(document.getElementById('lightgallery'));
  });
  spinnerHelpers.hide();
}

/* Tabella Preventivi gestione materiali Tecnico */
export function getAllQuotesTechnical() {
  spinnerHelpers.show();
  technicalviewServices.getAllQuotesTechnical(storageData.sUserId(), function (response) {
    for (var i = 0; i < response.data.length; i++) {
      var newList = "";
      if (response.data[i].ready == true) {
        newList += "<tr class='color-orange'>";
      } else {
        newList += "<tr style=''>";
      }     
      if (response.data[i].nr_ddt_gr != null && response.data[i].nr_ddt_gr != '-' && response.data[i].nr_ddt_gr != "" && response.data[i].ready == true) {
        newList += "<tr class='color-white'>";
      };
      if (response.data[i].date_ddt_gr != null && response.data[i].date_ddt_gr != '-' && response.data[i].date_ddt_gr != "" && response.data[i].ready == true) {
        newList += "<tr class='color-white'>";
      };

      newList +=
        '<td><button class="btn btn-primary-quotes" onclick="quotesHelpers.getByViewQuotes(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil-square-o"/></button></td>';
      newList += "<td>" + response.data[i].id + "</td>";
      /*newList += "<td>"
      if (response.data[i].imgmodule != '') {
        newList += '<div class="container"><div id="lightgallery" style="display:flex">';
        newList += "<a href='" + response.data[i].imgmodule + "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
      } +
        "</td>";*/
        newList += "<td>"
    if (response.data[i].imgmodule != "" && response.data[i].imgmodule != '-' ) {
        newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">';
              newList += "<a href='" + response.data[i].imgmodule + "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
    } 
          if (response.data[i].imgmodule == "") {
      newList += "<div>" + "</div>";
           }    
      "</td>"
      newList += "<td>" + response.data[i].descriptionCustomer + "</td>";
      newList += "<td>" + response.data[i].description + "</td>";
      newList += "<td>"
       if (response.data[i].id_states_quote == 1) {
        newList +=
          '<button class="btn btn-green" style="color:#ffd700!important;font-size:15px;"><i class="fa fa-flag" style="color:#ffd700!important;font-size:15px;"/></button>';
      } 
         if (response.data[i].id_states_quote == 7) {
        newList +=
          '<button class="btn btn-green" style="color:green!important;font-size:15px;"><i class="fa fa-flag" style="color:green!important;font-size:15px;"/></button>';
      }  
       if (response.data[i].id_states_quote == 5) {
        newList +=
          '<button class="btn btn-green" style="color:red!important;font-size:15px;"><i class="fa fa-flag" style="color:red!important;font-size:15px;"/></button>';
      } 
      "</td>";  
       if (response.data[i].id_states_quote == 8) {
        newList +=
          '<button class="btn btn-green" style="color:green!important;font-size:15px;"><i class="fa fa-bell" style="color:green!important;font-size:15px;"/></button>';
      }  
       if (response.data[i].id_states_quote == 9) {
        newList +=
          '<button class="btn btn-green" style="color:red!important;font-size:15px;"><i class="fa fa-bell" style="color:red!important;font-size:15px;"/></button>';
      } 
      "</td>";   
      newList += "</tr>";
      $("#row-table-fld-pre").show();
      $("#table_list_quotesSearch").show();
      $("#frmViewQuotes").hide();
      $("#table_list_quotesSearch tbody").append(newList);
    }
    lightGallery(document.getElementById('lightgallery'));
  });
  spinnerHelpers.hide();
}


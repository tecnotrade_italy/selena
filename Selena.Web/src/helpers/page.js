/***   pageHelpers   ***/

export function getUrl(routeParameter) {
  if (!functionHelpers.isValued(routeParameter)) {
    return window.location.pathname;
  } else {
    var url = window.location.pathname;

    $.each(routeParameter, function (field, value) {
      url = url.replace('/' + value, '');
    });

    return url;
  }
}

export function isAdminPage() {
  return window.location.pathname.indexOf('/admin') == 0;
}

export function detail(selector, functions, configuration, jsonValue) {
  var idForm = 'frm' + functions;
  var html = '<form id="' + idForm + '" action="">';
  var lastRow;
  var lastColumn;
  var Colspan;
  var id;

  $.each(configuration, function (i, config) {
    id = config.inputType + '_' + functions + '_' + config.field.replace('.', '_');

    if (functionHelpers.isValued(config.posX) && functionHelpers.isValued(config.posY)) {
      // Creazione nuova riga
      if (lastRow != config.posY) {
        if (functionHelpers.isValued(lastRow)) {
          html += '</div>';
        }

        html += '<div class="form-group row">';
      }

      if (functionHelpers.isValued(config.colspan)) {
        Colspan = 3 * config.colspan;
      } else {
        Colspan = 3;
      }

      html += '<label class="col-sm-5 col-form-label" for="' + id + '">' + config.label + ':</label><div class="col-sm-6">';

      html += pageHelpers._renderInputType(id, config, jsonValue) + '</div>';

      lastRow = config.posY;
      lastColumn = config.posX;
      html += '</div>';
    } else {
      if (config.inputType == 'hidden') {
        html += '<input type="hidden" id="' + id + '" name="' + id + '"';

        if (functionHelpers.isValued(jsonValue) && functionHelpers.isValued(jsonValue[config.field])) {
          html += ' value="' + functionHelpers.stringRemoveQuotes(jsonValue[config.field]) + '" ';
        }

        html += '>';
      }
    }
  });

  html += '</div></form>';

  // Carico l'html generato
  $(html).appendTo(selector);

  validationHelpers.validateBase('#' + idForm);

  pageHelpers._addProperty(functions, configuration, jsonValue);
}

export function detailFilter(selector, functions, configuration, jsonValue) {
  var idForm = 'frm' + functions;
  var html = '<form id="' + idForm + '" action="">';
  var lastRow;
  var lastColumn;
  var Colspan;
  var id;

  $.each(configuration, function (i, config) {
    id = config.inputType + '_' + functions + '_' + config.field.replace('.', '_');

    if (functionHelpers.isValued(config.posX) && functionHelpers.isValued(config.posY)) {
      // Creazione nuova riga
      if (lastRow != config.posY) {
        if (functionHelpers.isValued(lastRow)) {
          html += '</div>';
        }

        html += '<div class="row">';
      }

      if (functionHelpers.isValued(config.colspan)) {
        Colspan = 3 * config.colspan;
      } else {
        Colspan = 3;
      }

      html += '<div class="col-lg-' + Colspan + '"><label for="' + id + '">' + config.label + ':</label>';

      html += pageHelpers._renderInputType(id, config, jsonValue, true);

      lastRow = config.posY;
      lastColumn = config.posX;
      html += '</div>';
    } else {
      if (config.inputType == 'hidden') {
        html += '<input type="hidden" id="' + id + '" name="' + id + '"';

        if (functionHelpers.isValued(jsonValue) && functionHelpers.isValued(jsonValue[config.field])) {
          html += ' value="' + functionHelpers.stringRemoveQuotes(jsonValue[config.field]) + '" ';
        }

        html += '>';
      }
    }
  });

  html += '</div></form>';

  // Carico l'html generato
  $(html).appendTo(selector);

  validationHelpers.validateBase('#' + idForm);

  pageHelpers._addProperty(functions, configuration, jsonValue);
}

export function _addProperty(functions, configuration, jsonValue) {
  $.each(configuration, function (i, config) {
    id = config.inputType + '_' + functions + '_' + config.field.replace('.', '_');

    switch (config.inputType) {
      case 'Select':
        if (functionHelpers.isValued(config.service)) {
          configurationHelpers.select(config.service, id, config.parameter);
        }

        if (functionHelpers.isValued(jsonValue) && functionHelpers.isValued(jsonValue[config.field])) {
          if ($('#' + id).find("option[value='" + jsonValue[config.field] + "']").length) {
            $('#' + id).val(jsonValue[config.field]).trigger('change');
          } else {
            if (functionHelpers.isValued(jsonValue[config.field + 'Text'])) {
              $('#' + id).append(new Option(jsonValue[config.field + 'Text'], jsonValue[config.field], true, true)).trigger('change');
            } else {
              $('#' + id).val(jsonValue[config.field]).trigger('change');
            }
          }
        } else {
          if (functionHelpers.isValued(config.defaultValue)) {
            if ($('#' + id).find("option[value='" + config.defaultValue.ID + "']").length) {
              $('#' + id).val(config.defaultValue.ID).trigger('change');
            } else {
              $('#' + id).append(new Option(config.defaultValue.text, config.defaultValue.ID, true, true)).trigger('change');
            }
          }
        }

        if (functionHelpers.isValued(config.onChange)) {
          $('#' + id).change(function () {
            window[config.onChange.split('.')[0]][config.onChange.split('.')[1]]();
          });
        }
        break;

      case 'DatePicker':
        if (functionHelpers.isValued(jsonValue) && functionHelpers.isValued(jsonValue[config.field])) {
          $('#' + id).datepicker('setDate', functionHelpers.dateFormatter(jsonValue[config.field]));
        }
        break;

      case 'EmailText':
        $('#' + id).rules("add", {
          email: true
        });
        break;

      case 'Numeric':
        $('#' + id).rules("add", {
          number: true
        });
        break;

      case 'Digits':
        $('#' + id).rules("add", {
          digits: true
        });
        break;
    }

    if (functionHelpers.isValued(config.minLength)) {
      $('#' + id).rules("add", {
        minlength: config.minLength
      });
    }

    if (functionHelpers.isValued(config.maxLength)) {
      $('#' + id).rules("add", {
        maxlength: config.maxLength
      });
    }
  });

  $('.input-group.date').datepicker({
    todayBtn: "linked",
    clearBtn: true,
    format: storageData.sDatePickerFormat(),
    orientation: "bottom auto",
    calendarWeeks: true,
    autoclose: true
  });

  $('.form-control.dateTime').datetimepicker({
    showClear: true,
    format: storageData.sDateTimePickerFormat()
  });
  $('.input-group-append.dateTime').on('click', function () {
    $(this).prev('.form-control.dateTime').data('DateTimePicker').toggle();
  });
}

export function renderInputType(id, config, jsonValue, isFilter) {

  var htmlInpuType = '';

  switch (config.inputType) {
    case 'Input':
    case 'Numeric':
    case 'PhoneText':
    case 'EmailText':
      htmlInpuType += '<input type="text" id="' + id + '" name="' + id + '" class="form-control';
      if (functionHelpers.isValued(config.class)) {
        htmlInpuType += ' ' + config.class + ' ';
      }
      htmlInpuType += '"';
      if (functionHelpers.isValued(config.required) && config.required) {
        htmlInpuType += ' required ';
      }
      if (functionHelpers.isValued(config.disabled) && config.disabled) {
        htmlInpuType += ' disabled ';
      }
      if (functionHelpers.isValued(config.onChange)) {
        htmlInpuType += ' onchange="' + config.onChange + '(this)" ';
      }
      if (functionHelpers.isValued(jsonValue) && functionHelpers.isValued(jsonValue[config.field])) {
        htmlInpuType += ' value="' + functionHelpers.stringRemoveQuotes(jsonValue[config.field]) + '" ';
      } else {
        if (functionHelpers.isValued(config.defaultValue)) {
          htmlInpuType += ' value="' + config.defaultValue + '" ';
        }
      }

      htmlInpuType += '>';
      break;

    case 'TextArea':
      htmlInpuType += '<textarea id="' + id + '" name="' + id + '" rows="4" cols="50" class="form-control';
      if (functionHelpers.isValued(config.class)) {
        htmlInpuType += ' ' + config.class + ' ';
      }
      htmlInpuType += '"';
      if (functionHelpers.isValued(config.required) && config.required) {
        htmlInpuType += ' required ';
      }
      if (functionHelpers.isValued(config.disabled) && config.disabled) {
        htmlInpuType += ' disabled ';
      }
      htmlInpuType += '>';
      if (functionHelpers.isValued(jsonValue) && functionHelpers.isValued(jsonValue[config.field])) {
        htmlInpuType += functionHelpers.stringRemoveQuotes(jsonValue[config.field]);
      }
      htmlInpuType += '</textarea>'
      break;

    case 'Select':
      htmlInpuType += '<select id="' + id + '" name="' + id + '" class="form-control';
      if (functionHelpers.isValued(config.class)) {
        htmlInpuType += ' ' + config.class + ' ';
      }
      htmlInpuType += '" style="width:100%"';
      if (functionHelpers.isValued(config.required) && config.required) {
        htmlInpuType += ' required data-rule-minlength="1" ';
      }
      if (functionHelpers.isValued(config.disabled) && config.disabled) {
        htmlInpuType += ' disabled ';
      }
      htmlInpuType += '></select>';
      break;

    case 'DatePicker':
    case 'DateTimePicker':
      htmlInpuType += '<div class="input-group';
      if (config.inputType == 'DatePicker') {
        htmlInpuType += ' date';
      }
      htmlInpuType += '">';
      htmlInpuType += '<input type="text" class="form-control';
      if (config.inputType == 'DateTimePicker') {
        htmlInpuType += ' dateTime';
      }
      if (functionHelpers.isValued(config.class)) {
        htmlInpuType += ' ' + config.class + ' ';
      }
      htmlInpuType += '" id="' + id + '" name="' + id + '"';
      if (functionHelpers.isValued(config.disabled) && config.disabled) {
        htmlInpuType += ' disabled ';
      }
      htmlInpuType += '>';
      htmlInpuType += '<span class="input-group-append';
      if (config.inputType == 'DateTimePicker') {
        htmlInpuType += ' dateTime';
      }
      htmlInpuType += '">';
      htmlInpuType += '<span class="input-group-text input-group-addon"><i class="fa fa-calendar glyphicon glyphicon-th"></i></span>';
      htmlInpuType += '</span>';
      htmlInpuType += '</div>';
      break;

    case 'CheckBox':
      htmlInpuType += '<div class="input-group';
      if (functionHelpers.isValued(isFilter) && isFilter) {
        htmlInpuType += ' text-right';
      }
      htmlInpuType += '" style="display: block !important;">';
      htmlInpuType += '<label class="switch switch-pill switch-primary">';

      htmlInpuType += '<input type="checkbox" id="' + id + '" name="' + id + '" class="switch-input';
      if (functionHelpers.isValued(config.class)) {
        htmlInpuType += ' ' + config.class + ' ';
      }
      htmlInpuType += '" ';

      if (!functionHelpers.isValued(jsonValue)) {
        if (!functionHelpers.isValued(config.defaultValue) || ((functionHelpers.isValued(config.defaultValue) && config.defaultValue == "1"))) {
          htmlInpuType += ' checked="" ';
        }
      } else {
        if (functionHelpers.isValued(jsonValue[config.field]) && jsonValue[config.field]) {
          htmlInpuType += ' checked="" ';
        }
      }

      if (functionHelpers.isValued(config.required) && config.required) {
        htmlInpuType += ' required ';
      }

      if (functionHelpers.isValued(config.disabled) && config.disabled) {
        htmlInpuType += ' disabled ';
      }

      htmlInpuType += '>';

      if (functionHelpers.isValued(config.checkValue)) {
        htmlInpuType += '<span class="switch-slider" data-on="' + config.checkValue[0] + '" data-off="' + config.checkValue[1] + '"></span>';
      } else {
        htmlInpuType += '<span class="switch-slider" data-on="" data-off=""></span>';
      }

      htmlInpuType += '</label>';
      htmlInpuType += '</div>';

      break;

    case 'Radio':
      htmlInpuType += '<div class="col-form-label">';
      var isFirst = true;

      $.each(enumeratorHelpers.getValueFromConfiguration(config.radioEnumerator), function (i, value) {
        htmlInpuType += '<div class="form-check form-check-inline mr-1">';
        htmlInpuType += '<input class="form-check-input" id="' + id + '" type="radio" value="' + value.id + '" name="inline-radios_' + id + '"';
        if (isFirst) {
          htmlInpuType += ' checked ';
          isFirst = false;
        }
        htmlInpuType += '>';
        htmlInpuType += '<label class="form-check-label" for="' + id + '">' + value.text + '</label>';
        htmlInpuType += '</div>';
      });

      htmlInpuType += '</div>';
      break;

    case 'PhoneText':
      htmlInpuType += '<div class="input-group">';
      htmlInpuType += '<input type="text" id="' + id + '" name="' + id + '" class="form-control';
      if (functionHelpers.isValued(config.class)) {
        htmlInpuType += ' ' + config.class + ' ';
      }
      htmlInpuType += '"';
      if (functionHelpers.isValued(config.required) && config.required) {
        htmlInpuType += ' required ';
      }
      if (functionHelpers.isValued(config.disabled) && config.disabled) {
        htmlInpuType += ' disabled ';
      }
      if (functionHelpers.isValued(jsonValue[config.field])) {
        htmlInpuType += ' value="' + functionHelpers.stringRemoveQuotes(jsonValue[config.field]) + '" ';
      }
      htmlInpuType += '>';
      htmlInpuType += '<div class="input-group-append ">';
      htmlInpuType += '<span class="input-group-text">';
      htmlInpuType += '<i class="fa fa-phone"></i>';
      htmlInpuType += '</span>';
      htmlInpuType += '</div>';
      htmlInpuType += '</div>';
      break;

    case 'FaxText':
      htmlInpuType += '<div class="input-group">';
      htmlInpuType += '<input type="text" id="' + id + '" name="' + id + '" class="form-control';
      if (functionHelpers.isValued(config.class)) {
        htmlInpuType += ' ' + config.class + ' ';
      }
      htmlInpuType += '"';
      if (functionHelpers.isValued(config.required) && config.required) {
        htmlInpuType += ' required ';
      }
      if (functionHelpers.isValued(config.disabled) && config.disabled) {
        htmlInpuType += ' disabled ';
      }
      if (functionHelpers.isValued(jsonValue[config.field])) {
        htmlInpuType += ' value="' + functionHelpers.stringRemoveQuotes(jsonValue[config.field]) + '" ';
      }
      htmlInpuType += '>';
      htmlInpuType += '<div class="input-group-append ">';
      htmlInpuType += '<span class="input-group-text">';
      htmlInpuType += '<i class="fa fa-fax"></i>';
      htmlInpuType += '</span>';
      htmlInpuType += '</div>';
      htmlInpuType += '</div>';
      break;

    case 'EmailText':
      htmlInpuType += '<div class="input-group">';
      hthtmlInpuTypeml += '<input type="email" id="' + id + '" name="' + id + '" class="form-control';
      if (functionHelpers.isValued(config.class)) {
        htmlInpuType += ' ' + config.class + ' ';
      }
      htmlInpuType += '"';
      if (functionHelpers.isValued(config.required) && config.required) {
        htmlInpuType += ' required ';
      }
      if (functionHelpers.isValued(config.disabled) && config.disabled) {
        htmlInpuType += ' disabled ';
      }
      if (functionHelpers.isValued(jsonValue[config.field])) {
        htmlInpuType += ' value="' + functionHelpers.stringRemoveQuotes(jsonValue[config.field]) + '" ';
      }
      htmlInpuType += '>';
      htmlInpuType += '<div class="input-group-append ">';
      htmlInpuType += '<span class="input-group-text">';
      htmlInpuType += '<i class="fa fa-envelope"></i>';
      htmlInpuType += '</span>';
      htmlInpuType += '</div>';
      htmlInpuType += '</div>';
      break;
  }

  return htmlInpuType;
}

export function clearDetailValue(functions, configuration) {
  $.each(configuration, function (i, config) {
    id = config.inputType + '_' + functions + '_' + config.field.replace('.', '_');

    switch (config.inputType) {
      case 'Select':
        $('#' + id).val(null).trigger('change');

        if (functionHelpers.isValued(config.defaultValue)) {
          if ($('#' + id).find("option[value='" + config.defaultValue.ID + "']").length) {
            $('#' + id).val(config.defaultValue.ID).trigger('change');
          } else {
            $('#' + id).append(new Option(config.defaultValue.text, config.defaultValue.ID, true, true)).trigger('change');
          }
        }
        break;
      case 'CheckBox':
        $('#' + id).prop('checked', true);
        break;
      default:
        $('#' + id).val(null);
        break;
    }
  });
}

export function setDetailValue(functions, configuration, jsonValue, innerJsonField, innerJsonFieldKey, innerJsonValue) {
  var id = undefined;

  $.each(configuration, function (i, config) {
    id = config.inputType + '_' + functions + '_' + config.field.replace('.', '_');

    // If have a inner json by another field
    if (functionHelpers.isValued(innerJsonField) && config.field.indexOf(innerJsonField) == 0 && functionHelpers.isValued(innerJsonFieldKey)) {
      $.each(jsonValue[innerJsonField], function (j, jsonInnerValue) {
        if (jsonInnerValue[innerJsonFieldKey] == innerJsonValue) {
          pageHelpers._setDetailValue(id, config.inputType, jsonInnerValue[config.field.split('.')[1]]);
          return;
        }
      });
    } else {
      pageHelpers._setDetailValue(id, config.inputType, jsonValue[config.field]);
    }
  });
}

export function _setDetailValue(id, inputType, value, valueText) {
  switch (inputType) {
    case 'DateTimePicker':
      if (functionHelpers.isValued(value)) {
        $('#' + id).data("DateTimePicker").date(functionHelpers.dateTimeFormatter(value));
      } else {
        $('#' + id).val('');
      }
      break;
    case 'Select':
      if (functionHelpers.isValued(valueText)) {
        $('#' + id).append(new Option(valueText, value, true, true)).trigger('change');
      } else {
        $('#' + id).val(value).trigger('change');
      }
      break;
    case 'CheckBox':
      $('#' + id).prop('checked', value);
      break;
    default:
      $('#' + id).val(value);
      break;
  }
}

export function getDetailValue(functions, configuration, innerJsonField) {
  var jsonValue = {};
  var id = undefined;
  // If have a inner json by another field
  if (functionHelpers.isValued(innerJsonField)) {
    jsonValue[innerJsonField] = [{}];
  }

  $.each(configuration, function (i, config) {
    id = config.inputType + '_' + functions + '_' + config.field.replace('.', '_');

    switch (config.inputType) {
      case 'DatePicker':
      case 'DateTimePicker':
        // If have a inner json by another field
        if (functionHelpers.isValued(innerJsonField) && config.field.indexOf(innerJsonField) == 0) {
          jsonValue[innerJsonField][0][config.field.split('.')[1]] = functionHelpers.dateToJsonFormatter($('#' + id).val());
        } else {
          jsonValue[config.field] = functionHelpers.dateToJsonFormatter($('#' + id).val());
        }
        break;
      case 'Radio':
        // If have a inner json by another field
        if (functionHelpers.isValued(innerJsonField) && config.field.indexOf(innerJsonField) == 0) {
          jsonValue[innerJsonField][0][config.field.split('.')[1]] = $('#' + id + ':checked').val();
        } else {
          jsonValue[config.field] = $('#' + id + ':checked').val();
        }
        break;
      case 'CheckBox':
        // If have a inner json by another field
        if (functionHelpers.isValued(innerJsonField) && config.field.indexOf(innerJsonField) == 0) {
          jsonValue[innerJsonField][0][config.field.split('.')[1]] = $('#' + id + ':checked').length;
        } else {
          jsonValue[config.field] = $('#' + id + ':checked').length;
        }
        break;
      default:
        // If have a inner json by another field
        if (functionHelpers.isValued(innerJsonField) && config.field.indexOf(innerJsonField) == 0) {
          jsonValue[innerJsonField][0][config.field.split('.')[1]] = $('#' + id).val();
        } else {
          jsonValue[config.field] = $('#' + id).val();
        }
        break;
    }
  });

  return jsonValue;
}

export function getDetailValueForFilter(functions, configuration) {
  var jsonValue = {};

  $.each(configuration, function (i, config) {
    id = config.inputType + '_' + functions + '_' + config.field.replace('.', '_');

    switch (config.inputType) {
      case 'Select':
        jsonValue[config.field] = $('#' + id).val();
        jsonValue[config.field + 'Text'] = $('#' + id).text();
        break;
      case 'Radio':
        jsonValue[config.field] = $('#' + id + ':checked').val();
        break;
      case 'CheckBox':
        jsonValue[config.field] = $('#' + id + ':checked').length;
        break;
      default:
        jsonValue[config.field] = $('#' + id).val();
        break;
    }
  });

  return jsonValue;
}

/***   searchItemsHelpers   ***/
/***   POST   ***/

export function searchImplemented(tipology,note,original_code,code_product,code_gr,plant,brand,business_name_supplier,code_supplier,price_purchase,price_list,repair_price,price_new,update_date,usual_supplier,pr_rip_conc) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(tipology,note,original_code,code_product,code_gr,plant,brand,business_name_supplier,code_supplier,price_purchase,price_list,repair_price,price_new,update_date,usual_supplier,pr_rip_conc);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  searchItemsServices.searchImplemented(data, function (response) {
$("#table_list_searchitems tbody").html('');
if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
     $("#table_list_searchitems").show();
     $("#table_list_itemsGR").hide();
     $( "#table_list_searchitems tbody").append(newList);

     $('#div-length').html('');
     var html = "";
     html += '<div class="row">';
     html += '<div class="col-12" style="text-align: center;font-size: 18px;">';
     html += '<span style="color:#787878;">Numero di Articoli Totali Trovati ' + response.data.length + '</span>&nbsp;';
     html += "</div>";
     $('#div-length').append(html);

  } else {
    for(var i = 0; i<response.data.length;i++){
        var newList = "";
         newList += "<tr>";
         newList += "<td>" + response.data[i].id + "</td>";
         newList += "<td>" + response.data[i].tipology + "</td>";
         newList += "<td>" + response.data[i].note + "</td>";
         //newList += "<td><a href='" + response.data[i].img + "'</a>></td>";
         newList += '<div class="container"><div id="lightgallery" style="display:flex">';
         newList += "<td><a href='" + response.data[i].img + "'><i class='i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style=float:right;'></i></a></td></div></div>";
          newList +=  "</td>";
         newList += "<td>" + response.data[i].original_code + "</td>";
      newList += "<td>" + response.data[i].code_product + "</td>";
       newList += "<td><a href='" + response.data[i].imageitems + "'>"+ response.data[i].code_gr +"</a></td>";
        // newList += "<td>" + response.data[i].code_gr + "</td>";
         newList += "<td>" + response.data[i].plant + "</td>";
         newList += "<td>" + response.data[i].brand + "</td>";
         newList += "<td>" + response.data[i].business_name_supplier + "</td>";
         newList += "<td>" + response.data[i].code_supplier + "</td>";
         newList += "<td>" + response.data[i].price_purchase + "</td>";
         newList += "<td>" + response.data[i].price_list + "</td>";
         newList += "<td>" + response.data[i].repair_price + "</td>";
         newList += "<td>" + response.data[i].price_new + "</td>";
         newList += "<td>" + response.data[i].update_date + "</td>";
         newList += "<td>" + response.data[i].usual_supplier + "</td>";
         newList +=
         '<td><button class="btn btn-info" onclick="itemsGrHelpers.getById(' +
         response.data[i].id +
         ')"><i class="fa fa-pencil" style="color:#fff!important;"/></button></td>';
         newList +=
            '<td><button class="btn btn-danger" onclick="itemsGrHelpers.deleteRow(' +
           response.data[i].id +
             ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
         newList += "</tr>";  
    
     $("#table_list_searchitems").show();
     $("#table_list_itemsGR").hide();
     $( "#table_list_searchitems tbody").append(newList);

     $('#div-length').html('');
     var html = "";
     html += '<div class="row">';
     html += '<div class="col-12" style="text-align: center;font-size: 18px;">';
     html += '<span class="length" style="color:#787878;">Numero di Articoli Totali Trovati ' + response.data.length + '</span>&nbsp;';
     html += "</div>";
     $('#div-length').append(html);

    }
  }
    lightGallery(document.getElementById('lightgallery'));
    });
    
    spinnerHelpers.hide(); 
    }

/***   END POST   ***/


  
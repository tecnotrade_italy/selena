/***   userHelpers   ***/

export function rescuePassword() {
  spinnerHelpers.show();
  var email = $('#inputEmail').val();
  var data = {
    email: email
  };
  if (functionHelpers.isValued(email)) {
    userServices.rescuePassword(data, function (result) {
      if(result.message==""){
        notificationHelpers.error('Email non presente nel nostro sistema!');
      }else{
        notificationHelpers.success('Abbiamo inviato all\'indirizzo email indicato il link per resettare la password.');
      }
      spinnerHelpers.hide();
    });
  }
}

export function changePassword(id) {
  spinnerHelpers.show();
  var password = $('#password').val();
  var confirmPassword = $('#confirmPassword').val();
  var data = {
    password: password,
    confirmPassword: confirmPassword,
    id: id
  };
  if (password == confirmPassword) {
    userServices.changePasswordRescued(data, function (result) {
      if(result.message==""){
        notificationHelpers.error('Attenzione la pagina non è più valida o la password è stata già resettata!');
      }else{
        notificationHelpers.success('Password cambiata correttamente!');
      }
      spinnerHelpers.hide();
    });
  }else{
    spinnerHelpers.hide();
    notificationHelpers.error('Le password non coincidono!');
  }
}


export function checkExistUsername(field,value,rowIndex,id,rowId) {
  
  spinnerHelpers.show();

  if (functionHelpers.isValued(rowIndex)) {
    window.localStorage.setItem('userHelperId',id);
    if(value!='admin'){
      userServices.checkExistUsernameNotForThisPage(rowId,value,function (result) {
        if (result.data) {
          validationHelpers.setTableError(window.localStorage.getItem('userHelperId'),dictionaryHelpers.getDictionary("UsernameAlreadyExist"));
        }else{
          $('#' + field).removeClass('error');
        }

        window.localStorage.setItem('userHelperId','');
        
      });
    }
    spinnerHelpers.hide();
  }
  else {
    window.localStorage.setItem('userHelperId',field);
    if(value!='admin'){
      userServices.checkExistUsername(value,function (result) {
        
        if (result.data) {
          validationHelpers.setDetailError(window.localStorage.getItem('userHelperId'),dictionaryHelpers.getDictionary("UsernameAlreadyExist"));
        }else{
          $('#' + field).removeClass('error');
        }

        window.localStorage.setItem('userHelperId','');
        
      });
    }
    spinnerHelpers.hide();
  }
}


export function checkExistEmail(field,value,rowIndex,id,rowId) {
  spinnerHelpers.show();

  if (functionHelpers.isValued(rowIndex)) {
    window.localStorage.setItem('userHelperId',id);
    userServices.checkExistEmailNotForThisPage(rowId,value,function (result) {
      if (result.data) {
        validationHelpers.setTableError(window.localStorage.getItem('userHelperId'),dictionaryHelpers.getDictionary("EmailAlreadyExist"));
      }

      window.localStorage.setItem('userHelperId','');
      spinnerHelpers.hide();
    });
  }
  else {
    window.localStorage.setItem('userHelperId',field);
    userServices.checkExistEmail(value,function (result) {
      if (result.data) {
        validationHelpers.setDetailError(window.localStorage.getItem('userHelperId'),dictionaryHelpers.getDictionary("EmailAlreadyExist"));
      }

      window.localStorage.setItem('userHelperId','');
      spinnerHelpers.hide();
    });
  }
}

export function availableCheck(id) {
  //spinnerHelpers.show();
  userServices.availableCheck(id, function (result) {
    notificationHelpers.success('Registrazione completata con successo!');
  });  
 }


/* CREATE USER */
export function insertNoAuth(idForm) {
  spinnerHelpers.show();


  var data = functionHelpers.formToJson(idForm);
  
  data["idLanguage"] = storageData.sIdLanguage();

  var arrayImgAgg = [];

/*//format orari settimanali
  var html = "";
  html += '<table>'; 
  html += '<tbody>'; 
  html += '<tr>'; 
  html += '<th colspan="4"></th>';
  html += '<tr>'; 
  html += '<td>Lunedì</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#monday").val() + '</td>';
  html += '</tr>'; 
  html += '<tr>'; 
  html += '<td>Martedì</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>'; 
  html += '<td>' + $("#tuesday").val() + '</td>';
  html += '</tr>'; 
  html += '<tr>'; 
  html += '<td>Mercoledì</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#wednesday").val() + '</td>';
  html += '</tr>';
  html += '<tr>'; 
  html += '<td>Giovedì</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#thursday").val() + '</td>';
  html += '</tr>';
  html += '<tr>'; 
  html += '<td>Venerdì</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#friday").val() + '</td>';
  html += '</tr>';
  html += '<tr>'; 
  html += '<td>Sabato</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#saturday").val() + '</td>';
  html += '</tr>';
  html += '<tr>'; 
  html += '<td>Domenica</td>';
  html += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>';
  html += '<td>' + $("#sunday").val() + '</td>';
  html += '</tr>';
  html += '</tr>'; 
  html += '</tbody>'; 
  html += '</table>';  

   data.shop_time = html;
  //end orari settimanali */

//ciclo img. aggiuntive
  for(var i = 0; i<10; i ++){
    if(functionHelpers.isValued(data["imgBase64" + i]) && functionHelpers.isValued(data["imgName" + i])){
      arrayImgAgg.push(JSON.parse('{"imgBase64' + i + '":"' + data["imgBase64" + i] + '", "imgName' + i + '":"' + data["imgName" + i] + '"}'));
    }
  }
  data.imgAgg = arrayImgAgg;
  // end ciclo img.aggiuntive 

//SEZIONE MAPS 
  var arrayMapAgg = [];
  for (var i = 0; i < 5; i++) {
    var jsonMapAgg = {};
    var check = false;
    if (functionHelpers.isValued($('#user_id' + i).val())) {
       check = true;
      jsonMapAgg['user_id'] = $('#user_id' + i).val();
    }
    if (functionHelpers.isValued($('#maps' + i).val())) {
       check = true;
      jsonMapAgg['maps'] = $('#maps' + i).val();
    }
    if (functionHelpers.isValued($('#address' + i).val())) {
       check = true;
      jsonMapAgg['address'] = $('#address' + i).val();
    }
    if (functionHelpers.isValued($('#codepostal' + i).val())) {
       check = true;
     jsonMapAgg['codepostal'] = $('#codepostal' + i).val();
    }
    if (functionHelpers.isValued($('#country' + i).val())) {
       check = true;
      jsonMapAgg['country'] = $('#country' + i).val();
    }
    if (functionHelpers.isValued($('#province' + i).val())) {
        check = true;
      jsonMapAgg['province'] = $('#province' + i).val();
    }
     if (check) {
      arrayMapAgg.push(jsonMapAgg);
     }
    }
  data.MapAgg = arrayMapAgg;
  
  userServices.insertNoAuth(data, function (result) {
     
  /*  if ($('#vat_number').val() == result.data.vat_number && $('#email').val() == result.data.email) {
      notificationHelpers.success('Attenzione, Email e Partita Iva presenti già sul nostro sistema!Clicca qui per recuperare la password!');
      window.location = '/notification-resetpassword';
    } else { */
        notificationHelpers.success('Registrazione completata con successo!');
        setTimeout(function(){  
        window.location = '/notification-of-receipt-of-user-confirmation-email';
      }, 2000);
    //}
    spinnerHelpers.hide();
  });
}
// END SEZIONE MAPS 

/* END CREATE USER */


/* GET ALL USER */
export function getAllUser() {
  spinnerHelpers.show();

  userServices.getAllUser(storageData.sIdLanguage(), function (response) {   
    for (var i = 0; i < response.data.length; i++) {
      console.log(response.data);
       var newList = "";
       newList += "<tr>";
       newList +=
       '<td><button class="btn btn-info" onclick="userHelpers.getByIdResult(' +
       response.data[i].id +
       ')"><i class="fa fa-pencil" style="color:white!important;"/></button></td>';
          newList += "<td>" + response.data[i].id + "</td>";
        if (response.data[i].subscriber == '' || response.data[i].subscriber == null || response.data[i].subscriber == false) {
             newList +=
              "<td style='background-color:#FF5050!important;'>" + 'No' + "</td>";
        } else {
            newList +=
              "<td style='background-color:green!important;'>" + 'Si' + "</td>";
      } 

        if (response.data[i].basic == true) {
             newList +=
              "<td style='background-color:orange!important;'>" + 'BASIC' + "</td>";
        }
        /*else {
             newList += "<td style='background-color:red!important;'>" + '' + "</td>";
      } */
      
        if (response.data[i].plus == true) {
                  newList += "<td style='background-color:blue!important;'>" + 'PLUS' + "</td>";
        }
        /*else {
                  newList += "<td style='background-color:red!important;'>" + '' + "</td>";
        }*/
      
          newList += "<td>" + response.data[i].expiration_date + "</td>";
          newList += "<td>" + response.data[i].subscription_date + "</td>";

         if (response.data[i].vat_number == "" || response.data[i].vat_number == null) {
            newList += "<td>" +
              '<input type="text" class="form-control" style="border: #fff;width:200px;" name="vat_number_id" id="vat_number_id' + response.data[i].id  +'" onchange="userHelpers.updateVatNumber(' +
              response.data[i].id +
              ')" > ';
            + "</td>";
         } else {
           // newList += "<td>" + response.data[i].vat_number + "</td>";
            newList += "<td>" +
              '<input type="text" class="form-control" style="border: #fff;width:200px;" name="vat_number_id" id="vat_number_id' + response.data[i].id  +'" onchange="userHelpers.updateVatNumber(' +
              response.data[i].id +
              ')" value=" ' + response.data[i].vat_number + '"> ';
            + "</td>";
         }
       newList += "<td>" + response.data[i].business_name + "</td>";
       newList += "<td>" + response.data[i].name + "</td>";
       newList += "<td>" + response.data[i].telephone_number + "</td>";
       newList += "<td>" + response.data[i].mobile_number + "</td>";
       newList += "<td>" + response.data[i].fax_number + "</td>"; 
       newList += "<td>" + response.data[i].email + "</td>";   
     newList +=
     '<td><button class="btn btn-danger" onclick="userHelpers.deleteRow(' +
     response.data[i].id +
       ')"><i class="fa fa-trash" style="color:white!important;"/></button></td>';
       newList +=
       '<td><button class="btn btn-info" onclick="userHelpers.getDetailUsers(' +
       response.data[i].id +
         ')"><i class="fa fa-info" style="color:white!important;"/></button></td>'; 
       newList += "</tr>";   
      $( "#table_list_user tbody").append(newList);
      $("#table_list_user").show();
      $("#frmAddReferent").hide();
      $("#table_list_searchuser").hide();
      $("#frmUpdateUser").hide();
      $("#frmUpdateReferent").hide();
      $("#table_list_people").hide();
      $("#nav-tab").hide();
      $(".title-modifica-utente").hide();
      
     }
   });
   spinnerHelpers.hide(); 
 }


 export function getDetailUsers(id) {
  console.log(id);
  //frmDetailAggUsers
  //frmDetailAggUsers

  userServices.getDetailUsers(id, function (response) {
    console.log(response);
    var searchParam = "";
    if (!$.isEmptyObject(response.data)) {
     jQuery.each(response.data, function(i, val) {
       if (val == true) {
         if (i == "id") {
           $("#frmDetailAggUsers #" + i).html(val);
         } else {
           $("#frmDetailAggUsers #" + i).attr("checked", "checked");
         }
       } else {
         if (i == "imgName") {
           if (val != "") {
             $("#frmDetailAggUsers #imgPreview").attr("src", val);
           } else {
             $("#frmDetailAggUsers #imgName").remove();
           }
         } else {
           $("#frmDetailAggUsers #" + i).val(val);
           $("#frmDetailAggUsers #" + i).html(val);
         }
       }
       var AggDestinationDifferent = response.data; 
       
     
       $("#div-cont-agg-detailUsers").html("");
       var html = "";
       html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
       html += '<div class="col-12">';
       html += '<h3>Dati Anagrafici Utente</h3>';
       html += '</div>';

       html += '<div class="col-6">';
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Ragione Sociale: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Business_name + '' + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="business_name_plus' + '">Nome e Cognome: ' + "</label>&nbsp;";
       html += '<label for="name' + '"> ' + AggDestinationDifferent.Name + ' ' + AggDestinationDifferent.Surname + ''  + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="Address' + '">Indirizzo: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Address + '' + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Cap: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Code_postal + '' + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Località: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Country + '' + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Provincia: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Province + '' + "</label>&nbsp;";
       html += "</div>";
       html += '</div>';

       html += '<div class="col-6">';
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Telefono: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Telephone_number + '' + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Cellulare: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Mobile_number + '' + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Email: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Email + '' + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Partita IVA: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Vat_number + '' + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Codice Fiscale: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Fiscal_code + '' + "</label>&nbsp;";
       html += "</div>";
       html += "</div>";
       html += "</div>";
     

        html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
        html += '<div class="col-12">';
        html += '<h3>Indirizzo di Spedizione Aggiuntivo</h3>';
        html += "</div>";

        html += '<div class="col-4">';
        html += '<label for="business_name' + '">Ragione Sociale ' + "</label>&nbsp;";
        html +=
          '<input type="text" class="form-control" name="business_name" id="business_name" placeholder=" Ragione Sociale" value="' + AggDestinationDifferent.business_name + '">';
          html +=
          '<input type="hidden" class="form-control" name="user_id" id="user_id" value="' + AggDestinationDifferent.user_id + '">';
          html +=
          '<input type="hidden" class="form-control" name="id_useraddress" id="id_useraddress" value="' + AggDestinationDifferent.id_useraddress + '">';
        html += "</div>";
        html += '<div class="col-4">';
        html += '<label for="name' + '">Nome ' + "</label>&nbsp;";
        html +=
          '<input type="text" class="form-control" name="name" id="name" placeholder=" Nome" value="' + AggDestinationDifferent.name + '">';
        html += "</div>";
        html += '<div class="col-4">';
        html += '<label for="name' + '">Cognome ' + "</label>&nbsp;";
        html +=
          '<input type="text" class="form-control" name="surname" id="surname" placeholder=" Cognome" value="' + AggDestinationDifferent.surname + '">';
        html += "</div>";
        html += "</div>";

        html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
        html += '<div class="col-3">';
        html += '<label for="name' + '">Indirizzo ' + "</label>&nbsp;";
        html +=
          '<input type="text" class="form-control" name="address" id="address" placeholder=" Indirizzo" value="' + AggDestinationDifferent.address + '">';
        html += "</div>";
        html += '<div class="col-3">';
        html += '<label for="name' + '">Cap ' + "</label>&nbsp;";
        html +=
          '<input type="text" class="form-control" name="postal_code" id="postal_code" placeholder=" Cap" value="' + AggDestinationDifferent.postal_code + '">';
        html += "</div>";
        html += '<div class="col-3">';
        html += '<label for="name' + '">Località ' + "</label>&nbsp;";
        html +=
          '<input type="text" class="form-control" name="locality" id="locality" placeholder=" Località" value="' + AggDestinationDifferent.locality + '">';
        html += "</div>";
        html += '<div class="col-3">';
        html += '<label for="name' + '">Provincia ' + "</label>&nbsp;";
      /*  html +=
          '<input type="text" class="form-control" name="province_id" id="province_id" placeholder=" Provincia" value="' + AggDestinationDifferent.province_id + '">';*/
          html +=
          '<select class="custom-select form-control" id="province_id" name="province_id" required="required" style="width:100% !important;">&nbsp;</select>';
        html += "</div>";
        html += "</div>";

        html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
        html += '<div class="col-4">';
        html += '<label for="name' + '">Telefono ' + "</label>&nbsp;";
        html +=
          '<input type="text" class="form-control" name="phone_number" id="phone_number" placeholder=" Indirizzo" value="' + AggDestinationDifferent.phone_number + '">';
        html += "</div>";
        html += "</div>";
        $("#div-cont-agg-detailUsers").append(html);
     });

     var initials = [];
      initials.push({id: response.data.province_id, text: response.data.descriptionProvince});
      $('#province_id').select2({
        data: initials,
        ajax: {
          url: configData.wsRootServicesUrl + '/api/v1/provinces/select',
          data: function (params) {
            if(params.term==''){
              searchParam = '*';
            }else{
            searchParam = params.term;
            }
            var query = {
              search: searchParam,
            }
            return query;
          },
          processResults: function (data) {
            var dataParse = JSON.parse(data);

            return {
              results: dataParse.data
            };
          }
        }
      })

      var initials = [];
      $('#province_id').select2({
        data: initials,
        ajax: {
          url: configData.wsRootServicesUrl + '/api/v1/provinces/select',
          data: function (params) {
            if(params.term==''){
              searchParam = '*';
            }else{
            searchParam = params.term;
            }
            var query = {
              search: searchParam,
            }
            return query;
          },
          processResults: function (data) {
            var dataParse = JSON.parse(data);

            return {
              results: dataParse.data
            };
          }
        }
      })


      $("#frmDetailAggUsers").modal("show");
   } else { 
       var html = "";
       $("#div-cont-agg-detailUsers").html("");
       var html = "";
       html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
       html += '<div class="col-12">';
       html += '<h3>Dati Anagrafici Utente</h3>';
       html += '</div>';

       html += '<div class="col-6">';
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Ragione Sociale: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Business_name + '' + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="business_name_plus' + '">Nome e Cognome: ' + "</label>&nbsp;";
       html += '<label for="name' + '"> ' + AggDestinationDifferent.Name + ' ' + AggDestinationDifferent.Surname + ''  + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="Address' + '">Indirizzo: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Address + '' + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Cap: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Code_postal + '' + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Località: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Country + '' + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Provincia: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Province + '' + "</label>&nbsp;";
       html += "</div>";
       html += '</div>';

       html += '<div class="col-6">';
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Telefono: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Telephone_number + '' + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Cellulare: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Mobile_number + '' + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Email: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Email + '' + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Partita IVA: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Vat_number + '' + "</label>&nbsp;";
       html += "</div>";
       html += '<div class="col-12">';
       html += '<label for="Businessname' + '">Codice Fiscale: ' + "</label>&nbsp;";
       html += '<label for="Businessname' + '"> ' + AggDestinationDifferent.Fiscal_code + '' + "</label>&nbsp;";
       html += "</div>";
       html += "</div>";
       html += "</div>";
     

        html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
        html += '<div class="col-12">';
        html += '<h3>Indirizzo di Spedizione Aggiuntivo</h3>';
        html += "</div>";
        html += '<div class="col-4">';
        html += '<label for="business_name' + '">Ragione Sociale ' + "</label>&nbsp;";
        html +=
          '<input type="text" class="form-control" name="business_name" id="business_name" placeholder=" Ragione Sociale" value="' + '">';
        html += "</div>";
        html += '<div class="col-4">';
        html += '<label for="name' + '">Nome ' + "</label>&nbsp;";
        html +=
          '<input type="text" class="form-control" name="name" id="name" placeholder=" Nome" value="' +  '">';
        html += "</div>";
        html += '<div class="col-4">';
        html += '<label for="name' + '">Cognome ' + "</label>&nbsp;";
        html +=
          '<input type="text" class="form-control" name="surname" id="surname" placeholder=" Cognome" value="' + '">';
        html += "</div>";
        html += "</div>";

        html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
        html += '<div class="col-3">';
        html += '<label for="name' + '">Indirizzo ' + "</label>&nbsp;";
        html +=
          '<input type="text" class="form-control" name="address" id="address" placeholder=" Indirizzo" value="' + '">';
        html += "</div>";
        html += '<div class="col-3">';
        html += '<label for="name' + '">Cap ' + "</label>&nbsp;";
        html +=
          '<input type="text" class="form-control" name="postal_code" id="postal_code" placeholder=" Cap" value="' + '">';
        html += "</div>";
        html += '<div class="col-3">';
        html += '<label for="name' + '">Località ' + "</label>&nbsp;";
        html +=
          '<input type="text" class="form-control" name="locality" id="locality" placeholder=" Località" value="' + '">';
        html += "</div>";
        html += '<div class="col-3">';
        html += '<label for="name' + '">Provincia ' + "</label>&nbsp;";
        html +=
          '<input type="text" class="form-control" name="province_id" id="province_id" placeholder=" Provincia" value="' + '">';
        html += "</div>";
        html += "</div>";

        html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
        html += '<div class="col-4">';
        html += '<label for="name' + '">Telefono ' + "</label>&nbsp;";
        html +=
          '<input type="text" class="form-control" name="phone_number" id="phone_number" placeholder=" Indirizzo" value="' + '">';
        html += "</div>";
        html += "</div>";
        $("#div-cont-agg-detailUsers").append(html);
        $("#frmDetailAggUsers").modal("show");

        var initials = [];
        $('#province_id').select2({
          data: initials,
          ajax: {
            url: configData.wsRootServicesUrl + '/api/v1/provinces/select',
            data: function (params) {
              if(params.term==''){
                searchParam = '*';
              }else{
              searchParam = params.term;
              }
              var query = {
                search: searchParam,
              }
              return query;
            },
            processResults: function (data) {
              var dataParse = JSON.parse(data);

              return {
                results: dataParse.data
              };
            }
          }
        })

   }
        //$("#frmDetailAggUsers").modal("show");
 });


 }

export function getByIdResult(id) {
  // spinnerHelpers.show();
  userServices.getByIdResult(id, function (response) {
    var searchParam = "";
    jQuery.each(response.data, function (i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmUpdateUser #" + i).html(val);
        } else {
          $('#frmUpdateUser #' + i).attr('checked', 'checked');
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmUpdateUser #imgPreview").attr('src', val);
          } else {
            $("#frmUpdateUser #imgName").remove();
          }
        } else {
          $("#frmUpdateUser #" + i).val(val);
          $("#frmUpdateUser #" + i).html(val);
        }
      }
  });
     $("#frmSearchUser").hide();
     $("#table_list_searchuser").hide();
     $("#table_list_people").hide(); 
     $("#frmUpdateUser").show();

//select vattype
var initials = [];
initials.push({id: response.data.vat_type_id, text: response.data.descriptionVatType});
$('#vat_type_id').select2({
  data: initials,
  ajax: {
    url: configData.wsRootServicesUrl + '/api/v1/vattype/select',
    data: function (params) {
      if(params.term==''){
        searchParam = '*';
      }else{
      searchParam = params.term;
      }
      var query = {
        search: searchParam,
      }
      return query;
    },
    processResults: function (data) {
      var dataParse = JSON.parse(data);

      return {
        results: dataParse.data
      };
    }
  }
})
     //select carrier_id
var initials = [];
initials.push({id: response.data.carrier_id, text: response.data.descriptionCarrier});
$('#carrier_id').select2({
  data: initials,
  ajax: {
    url: configData.wsRootServicesUrl + '/api/v1/carrier/selectCarrier',
    data: function (params) {
      if(params.term==''){
        searchParam = '*';
      }else{
      searchParam = params.term;
      }
      var query = {
        search: searchParam,
      }
      return query;
    },
    processResults: function (data) {
      var dataParse = JSON.parse(data);

      return {
        results: dataParse.data
      };
    }
  }
})

//select id_dealer
var initials = [];
initials.push({ id: response.data.id_dealer, text: response.data.descriptionDealer});
$('#id_dealer').select2({
  data: initials,
  ajax: {
    url: configData.wsRootServicesUrl + '/api/v1/dealer/select',
    data: function (params) {
      if(params.term==''){
        searchParam = '*';
      }else{
      searchParam = params.term;
      }
      var query = {
        search: searchParam,
      }
      return query;
    },
    processResults: function (data) {
      var dataParse = JSON.parse(data);

      return {
        results: dataParse.data
      };
    }
  }
})
var initials = [];
$('#id_dealer').select2({
  data: initials,
  ajax: {
    url: configData.wsRootServicesUrl + '/api/v1/dealer/select',
    data: function (params) {
      if(params.term==''){
        searchParam = '*';
      }else{
      searchParam = params.term;
      }
      var query = {
        search: searchParam,
      }
      return query;
    },
    processResults: function (data) {
      var dataParse = JSON.parse(data);

      return {
        results: dataParse.data
      };
    }
  }
})
      
       //select paymentGr
var initials = [];
initials.push({id: response.data.id_payments, text: response.data.descriptionPayments});
$('#id_payments').select2({
  data: initials,
  ajax: {
    url: configData.wsRootServicesUrl + '/api/v1/paymentGr/selectpaymentGr',
    data: function (params) {
      if(params.term==''){
        searchParam = '*';
      }else{
      searchParam = params.term;
      }
      var query = {
        search: searchParam,
      }
      return query;
    },
    processResults: function (data) {
      var dataParse = JSON.parse(data);

      return {
        results: dataParse.data
      };
    }
  }
})   
    //get all user
   jQuery.each(response.data, function(i, val) {
    if (val == true){
      $('#frmUpdateUser #subscriber' + i).attr('checked','checked');
    }else{
      $("#frmUpdateUser #" + i).val(val);
    }
   });    
    $("#frmUpdateUser #id").val(id);
   
   peopleHelpers.getPeopleUser(id);
   $("#frmAddReferent #user_id").val(id);
   $("#frmUpdateReferent #user_id").val(id);
   $("#frmUpdateUser").show();
   $("#frmAddReferent").hide();
   $("#nav-tab").show();
   $("#table_list_user").hide();
   $("#btnadd").hide();
   $(".title-modifica-utente").show();
   $(".title-modifica-cliente").hide();
   $("#titlemodificaref").hide();
 });
// peopleHelpers.getPeopleUser($("#id").val());
 }


 export function updateUser() {
   spinnerHelpers.show();
   var privates = false;
  if ($("#frmUpdateUser #privates").is(":checked")) {
    privates = true;
  } else {
    privates = false;
  }
   
  var subscriber = false;
  if ($("#frmUpdateUser #subscriber").is(":checked")) {
    subscriber = true;
  } else {
    subscriber = false;
  }
   var create_quote = false;
  if ($("#frmUpdateUser #create_quote").is(":checked")) {
    create_quote = true;
  } else {
    create_quote = false;
  }
  var final = false;
  if ($("#frmUpdateUser #final").is(":checked")) {
    final = true;
  } else {
    final = false;
  }
    var basic = false;
  if ($("#frmUpdateUser #basic").is(":checked")) {
    basic = true;
  } else {
    basic = false;
  }
    var plus = false;
  if ($("#frmUpdateUser #plus").is(":checked")) {
    plus = true;
  } else {
    plus = false;
  }
   
 
  
  var data = {
    //id: storageData.sUserId(),
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmUpdateUser #id').val(),
    username: $('#frmUpdateUser #username').val(),
    email: $('#frmUpdateUser #email').val(),
    password: $('#frmUpdateUser #password').val(),
    business_name: $('#frmUpdateUser #business_name').val(),
    name: $('#frmUpdateUser #name').val(),
    surname: $('#frmUpdateUser #surname').val(),
    vat_number: $('#frmUpdateUser #vat_number').val(),
    fiscal_code: $('#frmUpdateUser #fiscal_code').val(),
    address: $('#frmUpdateUser #address').val(),
    codepostal: $('#frmUpdateUser #codepostal').val(),
    country: $('#frmUpdateUser #country').val(),
    province: $('#frmUpdateUser #province').val(),
    website: $('#frmUpdateUser #website').val(),
    vat_type_id: $('#frmUpdateUser #vat_type_id').val(),
    telephone_number: $('#frmUpdateUser #telephone_number').val(),
    fax_number: $('#frmUpdateUser #fax_number').val(),
    mobile_number: $('#frmUpdateUser #mobile_number').val(),
    pec: $('#frmUpdateUser #pec').val(),
    subscriber: subscriber, 
    privates: privates,
    create_quote: create_quote,
    final: final,
    basic: basic,
    plus: plus,
    port: $('#frmUpdateUser #port').val(),
    id_dealer: $('#frmUpdateUser #id_dealer').val(),
    id_sdi: $('#frmUpdateUser #id_sdi').val(),
    carrier_id: $('#frmUpdateUser #carrier_id').val(),
    id_payments: $('#frmUpdateUser #id_payments').val(),
    subscription_date: $('#frmUpdateUser #subscription_date').val(),
    expiration_date: $('#frmUpdateUser #expiration_date').val(),
  };

  userServices.updateUser(data, function () {
    notificationHelpers.success("Utente modificato con successo!");
    spinnerHelpers.hide();
    //window.location.reload();
    //$("#table_list_user").show();
    $("#frmUpdateUser").show();
  });
 }


export function updateVatNumber(id) {
  
  spinnerHelpers.show();
  var data = {
      id: id,
      vat_number: $('#table_list_user #vat_number_id' + id).val(),
  };

  userServices.updateVatNumber(data, function (result) {
  //  $('#table_list_user #id').val('');
    //$('#table_list_user #vat_number').val('');
    notificationHelpers.success("Aggiornamento avvenuto correttamente");
    spinnerHelpers.hide();
   // window.location.reload();

  });
}

export function insertUser() {
  spinnerHelpers.show();

      var privates = false;
      if ($("#frmAddUser #privates").is(":checked")) {
        privates = true;
      } else {
        privates = false;
      }
      
      var subscriber = false;
      if ($("#frmAddUser #subscriber").is(":checked")) {
        subscriber = true;
      } else {
        subscriber = false;
      }
      var create_quote = false;
      if ($("#frmAddUser #create_quote").is(":checked")) {
        create_quote = true;
      } else {
        create_quote = false;
      }
      var final = false;
      if ($("#frmAddUser #final").is(":checked")) {
        final = true;
      } else {
        final = false;
      }
     var basic = false;
      if ($("#frmAddUser #basic").is(":checked")) {
        basic = true;
      } else {
        basic = false;
      }
      var plus = false;
      if ($("#frmAddUser #plus").is(":checked")) {
        plus = true;
      } else {
        plus = false;
      }

  var data = {  
    created_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmAddUser #id').val(),
    username: $('#frmAddUser #username').val(),
    email: $('#frmAddUser #email').val(),
    password: $('#frmAddUser #password').val(),
    business_name: $('#frmAddUser #business_name').val(), 
    name: $('#frmAddUser #name').val(),
    surname: $('#frmAddUser #surname').val(),
    vat_number: $('#frmAddUser #vat_number').val(),
    fiscal_code: $('#frmAddUser #fiscal_code').val(),
    address: $('#frmAddUser #address').val(),
    codepostal: $('#frmAddUser #codepostal').val(),
    country: $('#frmAddUser #country').val(),
    province: $('#frmAddUser #province').val(),
    website: $('#frmAddUser #website').val(),
    vat_type_id: $('#frmAddUser #vat_type_id').val(),
    telephone_number: $('#frmAddUser #telephone_number').val(),
    fax_number: $('#frmAddUser #fax_number').val(),
    mobile_number: $('#frmAddUser #mobile_number').val(),
    pec: $('#frmAddUser #pec').val(),
    id_dealer: $('#frmAddUser #id_dealer').val(),
    //subscriber: $('#frmAddUser #subscriber').val(),
    subscription_date: $('#frmAddUser #subscription_date').val(),
    expiration_date: $('#frmAddUser #expiration_date').val(),
    carrier_id: $('#frmAddUser #carrier_id').val(),
    port: $('#frmAddUser #port').val(),
    id_payments: $('#frmAddUser #id_payments').val(),
    id_sdi: $('#frmAddUser #id_sdi').val(),
    create_quote: create_quote,
    privates: privates,
    subscriber: subscriber,
    final: final,
    basic: basic,
    plus: plus,
  };
  
  userServices.insertUser(data, function (result) {
    $('#frmAddUser #id').val('');
    $('#frmAddUser #username').val('');
    $('#frmAddUser #email').val('');
    $('#frmAddUser #password').val('');
    $('#frmAddUser #business_name').val('');
    $('#frmAddUser #name').val('');
    $('#frmAddUser #surname').val('');
    $('#frmAddUser #vat_number').val('');
    $('#frmAddUser #fiscal_code').val('');
    $('#frmAddUser #address').val('');
    $('#frmAddUser #codepostal').val('');
    $('#frmAddUser #country').val('');
    $('#frmAddUser #province').val('');
    $('#frmAddUser #website').val('');
    $('#frmAddUser #vat_type_id').val('');
    $('#frmAddUser #telephone_number').val('');
    $('#frmAddUser #fax_number').val('');
    $('#frmAddUser #mobile_number').val('');
    $('#frmAddUser #pec').val('');
    $('#frmAddUser #subscriber').val('');
    $('#frmAddUser #subscription_date').val('');
    $('#frmAddUser #expiration_date').val('');
    $('#frmAddUser #id_dealer').val('');
    $('#frmAddUser #port').val('');
    $('#frmAddUser #final').val('');
    notificationHelpers.success('Registrazione completata con successo!');
    spinnerHelpers.hide();
  });
}

export function deleteRow(id) {
spinnerHelpers.show();
userServices.delet(id, function (response) {
notificationHelpers.success(dictionaryHelpers.getDictionary("DeleteCompleted"));
spinnerHelpers.hide();
 window.location.reload();
  });
}


export function getDifferentDestination(id) {
 userServices.getDifferentDestination(id, function (response) {
   var searchParam = "";
   if (!$.isEmptyObject(response.data)) {
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmDifferentDestinationUser #" + i).html(val);
        } else {
          $("#frmDifferentDestinationUser #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmDifferentDestinationUser #imgPreview").attr("src", val);
          } else {
            $("#frmDifferentDestinationUser #imgName").remove();
          }
        } else {
          $("#frmDifferentDestinationUser #" + i).val(val);
          $("#frmDifferentDestinationUser #" + i).html(val);
        }
      }
      var AggDestinationDifferent = response.data; 
      
      $("#div-cont-agg-destinationDifferent").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<label for="business_name_plus' + '">Sede Legale ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="business_name_plus" id="business_name_plus" placeholder=" Ragione Sociale Sede Legale Azienda" value="' +
        AggDestinationDifferent.business_name_plus +
        '">';
      html += "</div>";

      html += '<div class="col-3">';
      html +=
        '<input type="hidden" class="form-control" name="user_id" id="user_id" value="' +
        AggDestinationDifferent.user_id +
        '">';
      html += "</div>";
      html += '<div class="col-3">';
      html +=
        '<input type="hidden" class="form-control" name="id" id="id" value="' +
        AggDestinationDifferent.id +
        '">';
      html += "</div>";
      html += "</div>";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-3">';
      html += '<label for="address' + '">Indirizzo Sede Legale ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="address" id="address" placeholder=" Indirizzo Sede Legale Azienda" value="' +
        AggDestinationDifferent.address +
        '">';
      html += "</div>";
      html += '<div class="col-2">';
      html += '<label for="codepostal' + '">CAP ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="codepostal" id="codepostal" placeholder="CAP" value="' +
        AggDestinationDifferent.codepostal +
        '">';
      html += "</div>";
      html += '<div class="col-2">';
      html += '<label for="country' + '">Località ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="country" id="country" placeholder="Località" value="' +
        AggDestinationDifferent.country +
        '">';
      html += "</div>";
      html += '<div class="col-2">';
      html += '<label for="province' + '">Provincia ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="province" id="province" placeholder="Provincia" value="' +
        AggDestinationDifferent.province +
        '">';
      html += "</div>";
      html += '<div class="col-3">';
      html += '<label for="telephone' + '">Telefono ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="telephone" id="telephone" placeholder="Telefono" value="' +
        AggDestinationDifferent.telephone +
        '">';
      html += "</div>";

      html += "</div>";
      $("#div-cont-agg-destinationDifferent").append(html);
   
    });
     $("#frmDifferentDestinationUser").modal("show");
  } else { 
      var html = "";
      $("#div-cont-agg-destinationDifferent").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<label for="business_name_plus' + '">Sede Legale ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="business_name_plus" id="business_name_plus" placeholder=" Ragione Sociale Sede Legale Azienda" value="' 
         +
        '">';
      html += "</div>";

      html += '<div class="col-3">';
      html +=
        '<input type="hidden" class="form-control" name="user_id" id="user_id" value="'
         +
        '">';
      html += "</div>";
      html += '<div class="col-3">';
      html +=
        '<input type="hidden" class="form-control" name="id" id="id" value="' 
        +
        '">';
      html += "</div>";
      html += "</div>";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-3">';
      html += '<label for="address' + '">Indirizzo Sede Legale ' + "</label>&nbsp;";
      html +=
      '<input type="text" class="form-control" name="address" id="address" placeholder=" Indirizzo Sede Legale Azienda" value="' 
       +
        '">';
      html += "</div>";
      html += '<div class="col-2">';
      html += '<label for="codepostal' + '">CAP ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="codepostal" id="codepostal" placeholder="CAP" value="' +
        '">';
      html += "</div>";
      html += '<div class="col-2">';
      html += '<label for="country' + '">Località ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="country" id="country" placeholder="Località" value="' +
        '">';
      html += "</div>";
      html += '<div class="col-2">';
      html += '<label for="province' + '">Provincia ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="province" id="province" placeholder="Provincia" value="' +
        '">';
      html += "</div>";
      html += '<div class="col-3">';
      html += '<label for="telephone' + '">Telefono ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="telephone" id="telephone" placeholder="Telefono" value="' +
        '">';
      html += "</div>";
      html += "</div>";
     $("#div-cont-agg-destinationDifferent").append(html);
     $("#frmDifferentDestinationUser").modal("show");
  }
});
}



export function updateorcreateUsersAdresses() {
 // spinnerHelpers.show();
  var data = {
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id_useraddress: $('#frmDetailAggUsers #id_useraddress').val(),
    business_name: $('#frmDetailAggUsers #business_name').val(),
    user_id: $('#frmDetailAggUsers #user_id').val(),
    address: $('#frmDetailAggUsers #address').val(),
    postal_code: $('#frmDetailAggUsers #postal_code').val(),
    locality: $('#frmDetailAggUsers #locality').val(),
    province_id: $('#frmDetailAggUsers #province_id').val(),
    phone_number: $('#frmDetailAggUsers #phone_number').val(),
    name: $('#frmDetailAggUsers #name').val(),
    surname: $('#frmDetailAggUsers #surname').val(),
    region_id: $('#frmDetailAggUsers #region_id').val(),

  };

  userServices.updateorcreateUsersAdresses(data, function () {
    notificationHelpers.success("Indirizzo di spedizione modificato o inserito con successo!");
   // spinnerHelpers.hide();
    //window.location.reload();
   // $("#table_list_user").show();
  //  $("#frmDetailAggAUsers").hide();
  });
}

export function updateorcreateDestinationBusinessName() {
  spinnerHelpers.show();
  var data = {
    //id: storageData.sUserId(),
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmDifferentDestinationAUser #id').val(),
    business_name_plus: $('#frmDifferentDestinationAUser #business_name_plus').val(),
    user_id: $('#frmUpdateUser #id').val(),
    address: $('#frmDifferentDestinationAUser #address').val(),
    codepostal: $('#frmDifferentDestinationAUser #codepostal').val(),
    country: $('#frmDifferentDestinationAUser #country').val(),
    province: $('#frmDifferentDestinationAUser #province').val(),
    telephone: $('#frmDifferentDestinationAUser #telephone').val(),
  };

  userServices.updateorcreateDestinationBusinessName(data, function () {
    notificationHelpers.success("Sede Legale modificata o inserita con successo!");
    spinnerHelpers.hide();
    window.location.reload();
    $("#frmUpdateUser").show();
    $("#frmDifferentDestinationAUser").hide();
    //window.location.reload();
  });
}




/*export function getBySelectReferent(id) {
  InterventionsServices.getBySelectReferent(id, function(response) {

    var searchParam = "";
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmViewSelectReferent #" + i).html(val);
        } else {
          $("#frmViewSelectReferent #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
            $("#frmViewSelectReferent #imgPreview").attr("src", val);
          } else {
            $("#frmViewSelectReferent #imgName").remove();
          }
        } else {
          $("#frmViewSelectReferent #" + i).val(val);
          $("#frmViewSelectReferent #" + i).html(val);
        }
      }

      var AggReferent = response.data;
      $("#div-cont-agg-referent").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<label for="id' + '">Numero Riparazione ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="id" id="id" disabled="true" placeholder="Numero Riparazione" value="' +
        AggReferent.id +
        '">';
      html += "</div>";
      html += "</div>";
         html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html +=
        '<label for="id_people' +
        '">Tecnico ' +
        "</label>&nbsp;";
      html +=
        '<select class="form-control select-graphic custom-select id_people" id="id_people' +
        '" value="' +
        AggReferent.id_people +
        '" name="id_people' +
        '"></select>';
      html += "</div>";
      html += "</div>";
      $("#div-cont-agg-referent").append(html);

    $("#frmViewSelectReferenta #id_people").on("change", function() {
         if (AggReferent.rip_association != null) {
alert("Attenzione, questa riparazione è associata alla riparazione numero " +  AggReferent.rip_association); 
 } 
    });

   
      var searchParam = "";
      var initials = [];
      initials.push({
        id: AggReferent.id_people,
        text: AggReferent.Description_People
      });
      $("#id_people").select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl +
            "/api/v1/people/select",
          data: function(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var searchParam = "";
      $("#id_people").select2({
        ajax: {
          url:
            configData.wsRootServicesUrl +
            "/api/v1/people/select",
          data: function(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });
    });
    $("#frmViewSelectReferent").modal("show");
    $("#frmViewExternalRepair").modal("hide");
    $("#table_list_searchintervention").hide();
    $("#table_list_intervention").show();
    $("#frmSearchIntervention").hide();
    $("#frmViewProcessingSheet").hide();
    $("#frmViewQuotes").hide();
    $("#ModuleValidationGuarantee").hide();
    $("#FaultModule").hide();
  });
}*/

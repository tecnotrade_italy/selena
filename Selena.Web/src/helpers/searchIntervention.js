/***   searchInterventionHelpers   ***/
/***   POST   ***/

export function searchImplemented(create_quote,IdIntervention,nr_ddt,user,items,code_intervention_gr) {
  spinnerHelpers.show();
  var data = functionHelpers.formToJson(create_quote,IdIntervention,nr_ddt,user,items,code_intervention_gr);
  data["idLanguage"] = storageData.sIdLanguage();
  var html = "";
  console.log(data);
  searchInterventionServices.searchImplemented(data, function (response) {
   console.log(response.data);
    $("#table_list_searchintervention tbody").html('');
    if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
    $("#table_list_searchintervention").show();
    $("#table_list_searchintervention tbody").append(newList);
    $("#table_list_intervention").hide();
    } else {
      for (var i = 0; i < response.data.length; i++) { 
        var newList = "";
         if (response.data[i].ready) {
           if((response.data[i].nr_ddt_gr == null || response.data[i].nr_ddt_gr == "") || (response.data[i].date_ddt_gr == null ||
             response.data[i].date_ddt_gr == "") && (response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable)) {
               newList += "<tr class='color-orange'>";
           } else {
             newList += "<tr class='color-white'>";
           }
         } else {
           if ((response.data[i].working_without_testing || response.data[i].working_with_testing || response.data[i].unrepairable) &&
           (response.data[i].nr_ddt_gr == null || response.data[i].nr_ddt_gr == '' || response.data[i].date_ddt_gr == null || response.data[i].date_ddt_gr == '')) {
             newList += "<tr class='color-orange'>";    
           } else {
             newList += "<tr class='color-white'>";
           }
         }
         if (response.data[i].ready == true) {
         newList +=
        '<td><input type="checkbox"  id="form-control ready' + i + '" value="' + response.data[i].ready +
        '" name="ready' + i + '" class="switch-input' + i + '" checked="true' + i + '" onclick="interventionHelpers.updateCheck(' +
           response.data[i].id +
           ')"></td>'; 
         } else{
         newList +=
        '<td><input type="checkbox" id="form-control ready' + i + '" value="' + response.data[i].ready +
          '" name="ready' + i + '" class="switch-input' + i + '" onclick="interventionHelpers.updateCheck(' +
           response.data[i].id +
           ')"></td>'; 
         }
          newList +=
           '<td><button class="btn btn-info" style="background-color: #AEC4F1;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Compila Fast Urgenza!" onclick="interventionHelpers.getByInterventionFU(' +
           response.data[i].id +
           ')"><i class="fa fa-bolt" style="color:#fff;"/></button></td>';
   
          if (response.data[i].checkProcessingExist == true) {
      newList +=
          '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' +
          response.data[i].id +
          ')"><i class="fa fa-circle-o" style="color:#007fff!important"/></button></td>';
    }else{
      newList +=
              '<td><button class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Apri FDL!" onclick="interventionHelpers.getByViewValue(' +
              response.data[i].id +
              ')"><i class="fa fa-pencil-square-o" style="color:#007fff!important"/></button></td>';
    }
    
          newList += "<td>"
        if (response.data[i].id_states_quote != 'Accettato' && response.data[i].id_states_quote != 'Rifiutato' && response.data[i].id_states_quote != 'In Attesa') {
           newList +=
           '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:navi!important;" onclick="interventionHelpers.getByViewQuotes(' +
           response.data[i].id +
           ')"><i class="fa fa-flag" style="color:navi!important;"/></button>';
        }
          if (response.data[i].id_states_quote == 'In Attesa') {
           newList +=
           '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:#f5ea74!important;" onclick="interventionHelpers.getByViewQuotes(' +
           response.data[i].id +
           ')"><i class="fa fa-flag" style="color:#f5ea74!important;"/></button>';
          }
         
        if (response.data[i].id_states_quote == 'Accettato') {
           newList +=
           '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:green!important;" onclick="interventionHelpers.getByViewQuotes(' +
           response.data[i].id +
           ')"><i class="fa fa-flag" style="color:green!important;"/></button>';
        }
        if (response.data[i].id_states_quote == 'Rifiutato') {
           newList +=
           '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Preventivo!" style="color:red!important;" onclick="interventionHelpers.getByViewQuotes(' +
           response.data[i].id +
           ')"><i class="fa fa-flag" style="color:red!important;"/></button>';
        }
       "</td>";
   
         /*newList +=
           '<td><button class="btn btn-link" onclick="interventionHelpers.getByViewQuotes(' +
           response.data[i].id +
           ')"><i class="fa fa-flag"/></button></td>';*/
         newList += "<td>" + response.data[i].id + "</td>";
         newList += "<td>" + response.data[i].code_intervention_gr + "</td>";
         newList +=
          '<td><a  style="cursor:pointer;white-space: nowrap; color:#787878 !important;" data-toggle="tooltip" data-placement="top" title="Visualizza dettaglio Utente!" onclick="interventionHelpers.getByIdUser(' +
           response.data[i].user_id +
           ')">' +  response.data[i].descriptionCustomer + '</a></td>';
         newList += "<td>" + response.data[i].date_ddt + "</td>";
         newList += "<td>" + response.data[i].nr_ddt + "</td>";
   
         //newList += "<td><a href='" + response.data[i].imgmodule + "'</a>Visualizza</td>";
         newList += "<td>" + response.data[i].description + response.data[i].rip_association + "</td>";
         //newList += "<td>" + response.data[i].defect + "</td>";
          newList += "<td>";
          for (var j = 0; j < response.data[i].captures.length; j++) { 
         newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">';
           newList +=
             "<a href='" + response.data[i].captures[j].img  +  "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
          }
         newList += "</td>";
         newList += "<td>"
      if (response.data[i].imgmodule != "" && response.data[i].imgmodule != '-' ) {
          newList +=
            '<div class="container"><div id="lightgallery" style="display:flex">';
        newList +=
            "<a href='" +
            response.data[i].imgmodule +
            "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a> " + "'</div></div>";
      } 
      if (response.data[i].imgmodule == "") {
          newList += "<div>" + response.data[i].defect + "</div>";
      } 
    //
          if (response.data[i].checkModuleFault == true) {
         newList +=
           '<button class="btn btn-green" style="color:#007FFF!important;" data-toggle="tooltip" data-placement="top" title="Visualizza Modulo Guasti!"  onclick="interventionHelpers.getByFaultModule(' +
           response.data[i].id +
         ')"><i class="fa fa-file-pdf-o" style="color:#007FFF!important;"/></button>'
         } 
         if (response.data[i].checkModuleFault == false) {
            newList += "<div>" + "</div>";
         } 
   //
         newList += "<td>" + response.data[i].codeGr + "</td>";
         newList += "<td>" + response.data[i].date_ddt_gr + "</td>";
         newList += "<td>" + response.data[i].nr_ddt_gr + "</td>";
         if (response.data[i].idInterventionExternalRepair == null) {
           newList +=
             '<td><button class="btn btn-green" style="color:#d7e7fe !important;font-size:28px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' +
             response.data[i].id +
             ')"><i class="fa fa-truck" style="color:#d7e7fe !important"/></button></td>';
         }
         if (response.data[i].idInterventionExternalRepair != null) {
           newList +=
             '<td><button class="btn btn-green" style="color:#02784d !important;font-size:26px;margin-top: 21px;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' +
             response.data[i].id +
             ')"><i class="fa fa-truck" style="color:#02784d !important;"/></button>';
         }
          if (response.data[i].ddt_supplier != null && response.data[i].ddt_supplier != "") {
           newList +=
             '<button class="btn btn-green" style="color:#02784d !important;font-size:27px;margin-top: -100px!important;" data-toggle="tooltip" data-placement="top" title="Apri Riparazione Esterna" onclick="interventionHelpers.getByViewExternalRepair(' +
             response.data[i].id +
             ')"><i class="fa fa-reply" style="color:#02784d !important;>" aria-hidden="true"></i></button></td>';
         }
          newList +=
           '<td><button class="btn btn-info" style="background-color: #ff337b;border-color: white;color: white;" data-toggle="tooltip" data-placement="top" title="Sezione gestione del tecnico" onclick="interventionHelpers.getBySelectReferent(' +
           response.data[i].id +
           ')"><i class="fa fa-circle-o" style="color:#fff;"/></button></td>';

         if (response.data[i].create_quote == "Si") {
            newList += "<td style='color:green;'>" + response.data[i].create_quote + "</td>";
         }else{
            newList += "<td style='color:red;'>" + response.data[i].create_quote + "</td>";
         }
        // newList += "<td>" + response.data[i].descriptionStates + "</td>";
         newList += "<td>" + response.data[i].code_tracking + "</td>";
         newList += "<td>" + response.data[i].note_internal_gr + "</td>";
   
         newList += "<td>"
         if (response.data[i].fileddt != "" && response.data[i].fileddt != '-' ) {
           newList +=
             '<div class="container"><div id="lightgallery" style="display:flex">';
         newList +=
             "<a href='" +
             response.data[i].fileddt +
             "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='color:red;float:right;'></i></a></div></div>";
       } 
       if (response.data[i].fileddt == "") {
         newList += "<div>" + "</div>";
       } 
       newList += "</td>"
   
          newList +=
           '<td><button class="btn btn-info" onclick="interventionHelpers.getById(' +
           response.data[i].id +
            ')"><i class="fa fa-pencil"style="color:#fff"/></button></td>';
         
          newList += "<td>"
         if (response.data[i]. checkModuleValidationGuarantee == true) {
         newList +=
           '<button class="btn btn-green" data-toggle="tooltip" data-placement="top" title="Apri Modulo Garanzie" style="color:#007FFF!important;" onclick="interventionHelpers.getByModuleGuarantee(' +
           response.data[i].id +
         ')"><i class="fa fa-external-link" style="color:#007FFF!important;"/></button>'
   } 
         if (response.data[i].checkModuleValidationGuarantee == false) {
            newList += "<div>" + "</div>";
         } 
   
        for (var j = 0; j < response.data[i].captures.length; j++) { 
           newList +=
            '<div class="container"><div id="lightgallery" style="display:flex">';
             newList +=
               "<a href='" + response.data[i].captures[j].img_rep_test  +  "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
         }
         "</td>"
         newList +=
           '<td><button class="btn btn-danger" onclick="interventionHelpers.deleteRow(' +
           response.data[i].id +
           ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
         newList += "</tr>";
        $("#table_list_searchintervention").show();
        $("#table_list_searchintervention tbody").append(newList);
        $("#table_list_intervention").hide();
       }
  }
     });
    spinnerHelpers.hide();
    }

/***   END POST   ***/


  
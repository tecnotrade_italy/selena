export function getAll(id) {
    spinnerHelpers.show();
    itemGrServices.getAll(id, function (response) {   
       for(var i = 0; i<response.data.length;i++){
         var newList = "";
         newList += "<tr>";
         newList += "<td>" + response.data[i].id + "</td>";
         newList += "<td>" + response.data[i].tipology + "</td>";
         newList += "<td>" + response.data[i].note + "</td>";
         //newList += "<td><a href='" + response.data[i].img + "'</a>></td>";
         newList += '<div class="container"><div id="lightgallery" style="display:flex">';

     newList += "<td>"
    if (response.data[i].img != "" && response.data[i].img != '-' ) {
        newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">';
      newList +=
          "<a href='" +
          response.data[i].img +
        "'><a href='" + response.data[i].img + "'><i class='fa fa-file-pdf-o' class='fr-fic fr-dii' style='float:right;'></i></a></div></div > ";
    } 
    if (response.data[i].img == "") {
        newList += "<div>" + "</div>";
           }    
        "</td>"
      //newList += "<td><a href='" + response.data[i].img + "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></td></div></div>";
      //newList +=  "</td>";
         newList += "<td>" + response.data[i].original_code + "</td>";
         newList += "<td>" + response.data[i].code_product + "</td>";
         newList += "<td><a href='" + response.data[i].imageitems + "'>"+ response.data[i].code_gr +"</a></td>";
         //newList += "<td>" + response.data[i].code_gr + "</td>";
         newList += "<td>" + response.data[i].plant + "</td>";
         newList += "<td>" + response.data[i].brand + "</td>";
         newList += "<td>" + response.data[i].business_name_supplier + "</td>";
         newList += "<td>" + response.data[i].code_supplier + "</td>";
         newList += "<td>" + response.data[i].price_purchase + "</td>";
         newList += "<td>" + response.data[i].price_list + "</td>";
         newList += "<td>" + response.data[i].repair_price + "</td>";
         newList += "<td>" + response.data[i].price_new + "</td>";
         newList += "<td>" + response.data[i].update_date + "</td>";
         newList += "<td>" + response.data[i].usual_supplier + "</td>";
         //newList += "<td>" + response.data[i].pr_rip_conc + "</td>";
         newList +=
         '<td><button class="btn btn-info" onclick="itemsGrHelpers.getById(' +
         response.data[i].id +
         ')"><i class="fa fa-pencil" style="color:#fff!important;"/></button></td>';
         newList +=
         '<td><button class="btn btn-light" onclick="itemsGrHelpers.getByCmp(' +
         response.data[i].id +
         ')"><i class="fa fa-bars" style="color:#fff!important;"/></button></td>';
         newList +=
            '<td><button class="btn btn-danger" onclick="itemsGrHelpers.deleteRow(' +
           response.data[i].id +
             ')"><i class="fa fa-trash" style="color:#fff!important;"/></button></td>';
         newList += "</tr>";
   
        $("#table_list_itemsGR tbody").append(newList);
        $("#frmUpdateItemsGr").hide();
        $("#table_list_searchitems").hide();
        $("#frmCmpItemsGr").hide();
        $("#rr").hide();
        $("#table_list_itemsGR").show();   

        $('#div-length').html('');
        var html = "";
        html += '<div class="row">';
        html += '<div class="col-12" style="text-align: center;font-size: 18px;">';
        html += '<span style="color:#787878;">Numero di Articoli Totali ' + response.data[i].totcolumn[0].count + '</span>&nbsp;';
        html += "</div>";
        $('#div-length').append(html); 
       }
       lightGallery(document.getElementById('lightgallery'));
     });
       spinnerHelpers.hide(); 
   }
   

  export function changetipologyforRangeBarcodeGR(idSelectTipology) {

  itemGrServices.getBarcodeGrForTypology(idSelectTipology, function (result) {
      $("#code_gr").val(result.data);
  });
    notificationHelpers.success('Range Tipologia inserito con successo su Codice GR!');

}

export function changeplantidforRangeBarcodeGR(idSelectplant_id) {
  itemGrServices.getBarcodeGrForPlantId(idSelectplant_id, function (result) {
      $("#code_gr").val(result.data);
  });
    notificationHelpers.success('Range Impianto inserito con successo su Codice GR!');
}

 export function getById(id) {
    $('#frmUpdateItemsGr #id').val(id);
   // spinnerHelpers.show();
   itemGrServices.getById(id, function (response) {
    var searchParam = "";
     /* SELECT BRAND*/
  var initials = [];
  initials.push({id: response.data.brand, text: response.data.Brand});
  $('#brand').select2({
    data: initials,
    ajax: {
      url: configData.wsRootServicesUrl + '/api/v1/brand/select',
      data: function (params) {
        if(params.term==''){
          searchParam = '*';
        }else{
        searchParam = params.term;
        }
        var query = {
          search: searchParam,
        }
        return query;
      },
      processResults: function (data) {
        var dataParse = JSON.parse(data);

        return {
          results: dataParse.data
        };
      }
    }
  })
     
  var initials = [];
  $('#brand').select2({
    data: initials,
    ajax: {
      url: configData.wsRootServicesUrl + '/api/v1/brand/select',
      data: function (params) {
        if(params.term==''){
          searchParam = '*';
        }else{
        searchParam = params.term;
        }
        var query = {
          search: searchParam,
        }
        return query;
      },
      processResults: function (data) {
        var dataParse = JSON.parse(data);

        return {
          results: dataParse.data
        };
      }
    }
  })
  /* END SELECT brand */

  /* SELECT business_name_supplier */
  var initials = [];
  initials.push({id: response.data.business_name_supplier, text: response.data.BusinessName});
  $('#business_name_supplier').select2({
    data: initials,
    ajax: {
      url: configData.wsRootServicesUrl + '/api/v1/businessnamesupplier/select',
      data: function (params) {
        if(params.term==''){
          searchParam = '*';
        }else{
        searchParam = params.term;
        }
        var query = {
          search: searchParam,
        }
        return query;
      },
      processResults: function (data) {
        var dataParse = JSON.parse(data);

        return {
          results: dataParse.data
        };
      }
    }
  })
     
     var initials = [];
  $('#business_name_supplier').select2({
    data: initials,
    ajax: {
      url: configData.wsRootServicesUrl + '/api/v1/businessnamesupplier/select',
      data: function (params) {
        if(params.term==''){
          searchParam = '*';
        }else{
        searchParam = params.term;
        }
        var query = {
          search: searchParam,
        }
        return query;
      },
      processResults: function (data) {
        var dataParse = JSON.parse(data);

        return {
          results: dataParse.data
        };
      }
    }
  })
  /* END SELECT business_name_supplier */

  /* SELECT tipology */
  var initials = [];
  initials.push({id: response.data.tipology, text: response.data.Tipology});
  $('#tipology').select2({
    data: initials,
    ajax: {
      url: configData.wsRootServicesUrl + '/api/v1/tipology/select',
      data: function (params) {
        if(params.term==''){
          searchParam = '*';
        }else{
        searchParam = params.term;
        }
        var query = {
          search: searchParam,
        }
        return query;
      },
      processResults: function (data) {
        var dataParse = JSON.parse(data);

        return {
          results: dataParse.data
        };
      }
    }
  })
     
      var initials = [];
  $('#tipology').select2({
    data: initials,
    ajax: {
      url: configData.wsRootServicesUrl + '/api/v1/tipology/select',
      data: function (params) {
        if(params.term==''){
          searchParam = '*';
        }else{
        searchParam = params.term;
        }
        var query = {
          search: searchParam,
        }
        return query;
      },
      processResults: function (data) {
        var dataParse = JSON.parse(data);

        return {
          results: dataParse.data
        };
      }
    }
  })
  /* END SELECT tipology */


 /* SELECT plant_id */
 var initials = [];
 initials.push({id: response.data.plant_id, text: response.data.PlantId});
 $('#plant_id').select2({
   data: initials,
   ajax: {
     url: configData.wsRootServicesUrl + '/api/v1/plant/select',
     data: function (params) {
       if(params.term==''){
         searchParam = '*';
       }else{
       searchParam = params.term;
       }
       var query = {
         search: searchParam,
       }
       return query;
     },
     processResults: function (data) {
       var dataParse = JSON.parse(data);

       return {
         results: dataParse.data
       };
     }
   }
 })
    
     var initials = [];
 $('#plant_id').select2({
   data: initials,
   ajax: {
     url: configData.wsRootServicesUrl + '/api/v1/plant/select',
     data: function (params) {
       if(params.term==''){
         searchParam = '*';
       }else{
       searchParam = params.term;
       }
       var query = {
         search: searchParam,
       }
       return query;
     },
     processResults: function (data) {
       var dataParse = JSON.parse(data);

       return {
         results: dataParse.data
       };
     }
   }
 })
 /* END SELECT plant_id */



   /*  SELECT ItemsGr */
    jQuery.each(response.data, function(i, val) {
     if (val == true){
       $('#frmUpdateItemsGr #' + i).attr('checked','checked');
     }else{
        if (i=="imgName"){
            if (val!=""){
                $("#frmUpdateItemsGr #imgPreview").attr('src',val);  
            }else{
                $("#frmUpdateItemsGr #imgPreview").remove();   
            }    
        }else{
            $("#frmUpdateItemsGr #" + i).val(val);
        }
     }
    }); 
     
        //view result Items Agg 
  var cont = 0;
    var AggItems = response.data.AggItems;
    if(AggItems !== undefined){
     if(AggItems.length>0){
       for(var j=0;j<AggItems.length;j++){
        var html = "";
        if(cont < 10){
          cont = cont + 1;
         html += '<div class="row" id="divcontmaps-'+ cont +'" data-id-maps-agg="' + cont + '">';
        html += '<div class="col-3">';
        html += '<div class="form-group">';  
        html += '<label for="business_name_suppliers' + cont + '">Ragione Sociale Fornitore ' + cont + '</label>&nbsp;';
        html += '<select class="form-control select-graphic custom-select business_name_suppliers_select"  id="business_name_suppliers' + cont + '" name="business_name_suppliers' + cont + '"></select>';
        html += "</div>";
        html += "</div>";
        html += '<div class="col-2">';
        html += '<div class="form-group">';   
        html += '<label for="code_supplier' + cont + '">Codice Fornitore ' + cont + '</label>&nbsp;';
        html += '<input type="text" class="form-control" id="code_supplier' + cont + '" name="code_supplier' + cont + '" value="' + AggItems[j].code_supplier + '">';
        html += "</div>";
        html += "</div>";
        html += '<div class="col-2">';
        html += '<div class="form-group">';
        html += '<label for="price_purchase' + cont + '">Prezzo Acquisto ' + cont + '</label>&nbsp;';
        html += '<input type="text" class="form-control" id="price_purchase' + cont + '" name="price_purchase' + cont + '" value="' + AggItems[j].price_purchase + '">';
        html += "</div>";
        html += "</div>";  
        html += '<div class="col-2">';
        html += '<div class="form-group">';
        html += '<label for="update_date' + cont + '">Data ' + cont + '</label>&nbsp;';
        html += '<input type="text" class="form-control" id="update_date' + cont + '" name="update_date' + cont + '" value="' + AggItems[j].update_date + '">';
        html += "</div>";
        html += "</div>";  
        html += '<div class="col-2">';
        html += '<div class="form-group">';
        html += '<label for="usual_supplier' + cont + '">Fornitore Abituale ' + cont + '</label>&nbsp;';
        html += '<input type="text" class="form-control" id="usual_supplier' + cont + '" name="usual_supplier' + cont + '" value="' + AggItems[j].usual_supplier + '">';
        html += "</div>";
        html += "</div>";    
        html += '<div class="form-group">';        
        html += '<input type="hidden" class="form-control" id="id' + cont + '" name="id' + cont + '" value="' + AggItems[j].id + '">';
        html += "</div>";
        html += '<div class="form-group">';        
        html += '<input type="hidden" class="form-control" id="id' + cont + '" name="id_gritems' + cont + '" value="' + AggItems[j].id_gritems + '">';
        html += "</div>";
        html += '<div class="col-1">';
        html += '<div class="form-group">';
        html += '<i class="fa fa-times del-maps-agg" data-id-cont="' + cont + '"></i>';
        html += "</div>";
        html += "</div>";
        html += "</div>";
		    $('#div-cont-maps-agg').append(html);

       // $.getScript("https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js").done(function(){ 
         /*select  */
      var searchParam = "";
        var initials = [];
        initials.push({id: AggItems[j].business_name_suppliers, text: AggItems[j].BusinessName});
        $('#business_name_suppliers' + cont).select2({
          data: initials,
          ajax: {
            url: configData.wsRootServicesUrl + '/api/v1/businessnamesupplier/select',
            data: function (params) {
              if (params.term == '') {
                searchParam = '*';
              } else {
                searchParam = params.term;
              }
              var query = {
                search: searchParam,
              }
              return query;
            },
            processResults: function (data) {
              var dataParse = JSON.parse(data);
              return {
                results: dataParse.data
              };
            }
          }
        });
     // });
        /* end select  */

}
}
}
}

    $("#frmUpdateItemsGr .del-maps-agg").on("click", function(e) {
      var idDel = $(this).attr('data-id-cont');
     // var el = $(this).parent('div').parent('div').attr('data-id-maps-agg', idDel);
      var el = $('#divcontmaps-' + idDel);
      
      cont = cont - 1;
      if(cont<0){
        cont = 0;
      }
      el.remove();
    });
//end view result MAPS
    
    /* AGG MAPS*/
    $("#frmUpdateItemsGr #add-img-maps").on("click", function(e) {
        var html = "";
        if(cont < 10){
          cont = cont + 1;
        html += '<div class="row" id="divcontmaps-'+ cont +'" data-id-maps-agg="' + cont + '">';
        html += '<div class="col-3">';
        html += '<div class="form-group">';  
        html += '<label for="business_name_supplier' + cont + '">Ragione Sociale Fornitore ' + cont + '</label>&nbsp;';
        html += '<select class="custom-select users_id_select" data-toggle="tooltip" data-placement="top" id="business_name_suppliers' + cont + '" name="business_name_suppliers' + cont + '"></select>';
        html += "</div>";
        html += "</div>";
        html += '<div class="col-2">';
        html += '<div class="form-group">';   
        html += '<label for="code_supplier' + cont + '">Codice Fornitore ' + cont + '</label>&nbsp;';
        html += '<input type="text" class="form-control" id="code_supplier' + cont + '" name="code_supplier' + cont + '">';
        html += "</div>";
        html += "</div>";
        html += '<div class="col-2">';
        html += '<div class="form-group">';
        html += '<label for="price_purchase' + cont + '">Prezzo Acquisto ' + cont + '</label>&nbsp;';
        html += '<input type="text" class="form-control" id="price_purchase' + cont + '" name="price_purchase' + cont + '">';
        html += "</div>";
        html += "</div>";  
        html += '<div class="col-2">';
        html += '<div class="form-group">';
        html += '<label for="update_date' + cont + '">Data ' + cont + '</label>&nbsp;';
        html += '<input type="text" class="form-control" id="update_date' + cont + '" name="update_date' + cont + '">';
        html += "</div>";
        html += "</div>";  
        html += '<div class="col-2">';
        html += '<div class="form-group">';
        html += '<label for="usual_supplier' + cont + '">Fornitore Abituale ' + cont + '</label>&nbsp;';
        html += '<input type="text" class="form-control" id="usual_supplier' + cont + '" name="usual_supplier' + cont + '">';
        html += "</div>";
        html += '<div class="form-group">';        
        html += '<input type="hidden" class="form-control" id="id' + cont + '" name="id_gritems' + cont + '">';
        html += "</div>";
        html += "</div>";    
        html += '<div class="col-1">';
        html += '<div class="form-group">';
        html += '<i class="fa fa-times del-maps-agg" data-id-cont="' + cont + '"></i>';
        html += "</div>";
        html += "</div>";
        html += "</div>";
		    $('#div-cont-maps-agg').append(html);

        $.getScript("https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js").done(function(){ 
          var searchParam = "";
         $('#business_name_suppliers' + cont).select2({
          ajax: {
          url: configData.wsRootServicesUrl + '/api/v1/businessnamesupplier/select',
          data: function (params) {
          if(params.term==''){
            searchParam = '*';
          }else{
          searchParam = params.term;
          }
          var query = {
          search: searchParam,
          }
          return query;
        },
        processResults: function (data) {
          var dataParse = JSON.parse(data);
          return {
          results: dataParse.data
            };
          }
         }
        });  
      });
      }   
      $("#frmUpdateItemsGr .del-maps-agg").on("click", function(e) {
        var idDel = $(this).attr('data-id-cont');
        var el = $('#divcontmaps-' + idDel);

        cont = cont - 1;
        if(cont<0){
          cont = 0;
        }
        el.remove();
      });
    });     
    $("#frmUpdateItemsGr").show();
    $("#table_list_itemsGR").hide(); 
    $("#frmSearchItems").hide();
    $("#table_list_searchitems").hide();
    $("#listino").hide();
    $("#listinohr").hide();
  }); 
}

export function getByCmp(id) {
  $('#frmCmpItemsGr #id').val(id);
 // spinnerHelpers.show();
 itemGrServices.getByCmp(id, function (response) {
  jQuery.each(response.data, function(i, val) {
   if (val == true){
     $('#frmCmpItemsGr #' + i).attr('checked','checked');
   }else{
      if (i=="imgName"){
          if (val!=""){
              $("#frmCmpItemsGr #imgPreview").attr('src',val);  
          }else{
              $("#frmCmpItemsGr #imgPreview").remove();   
          }    
      }else{
              $("#frmCmpItemsGr #" + i).val(val);
      }
   }
  });  
      $("#div-Cont-Detail-Cod_List_Article").html("");
            var html = "";
            html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
            html += '<div class="col-4">';
            html += '<label style="font-weight: bold;">Tipologia: </label><span class="Tipology">' + response.data.Tipology + '</span><br>';
            html += '<label style="font-weight: bold;">Nota: </label><span class="note">' + response.data.note + '</span><br>';
            html += '<label style="font-weight: bold;">Codice Prodotto: </label><span class="CodProdotto">' + response.data.code_gr + '</span><br>';
            html += '<label style="font-weight: bold;">Codice GR: </label><span class="CodGr">' + response.data.Tipology + '</span><br>';
            html += '</div>';

            html += '<div class="col-4">';
            html += '<label style="font-weight: bold;">Marchio: </label><span class="plant">' + response.data.plant + '</span><br>';
            html += '<label style="font-weight: bold;">Ragione Sociale Fornitore: </label><span class="BusinessName">' + response.data.BusinessName + '</span><br>';
            html += '<label style="font-weight: bold;">Codice Fornitore:</label><span class="code_supplier">' + response.data.code_supplier + '</span><br>';
            html += '</div>';

            html += '<div class="col-4">';
            html += '<label style="font-weight: bold;">Prezzo Acquisto: </label><span class="pr_acq">' + response.data.price_purchase + '</span><br>';
            html += '<label style="font-weight: bold;">Prezzo Listino: </label><span class="pr_listino">' + response.data.price_list + '</span><br>';
            html += '<label style="font-weight: bold;">Prezzo Riparazione: </label><span class="pr_riparaz">' + response.data.repair_price + '</span><br>';
            html += '<label style="font-weight: bold;">Prezzo Nuovo:</label><span class="pr_nuovo">' + response.data.price_new + '</span>';
            html += '</div>';
            html += '</div>';
      $("#div-Cont-Detail-Cod_List_Article").append(html);

            var AggExploded = response.data.AggExploded;
            if (AggExploded.length > 0) {
             for (var j = 0; j < AggExploded.length; j++) {
              var html = "";
              html += "<tr>";
              html += '<input type="hidden" class="form-control id" id="id'  + AggExploded[j].id + '" name="id' + '" value="' + AggExploded[j].id + '"><input type="hidden"  data-toggle="tooltip" data-placement="top" title="'+ AggExploded[j].id + '" id="id'  + AggExploded[j].id + '" name="id' + '" value="' + AggExploded[j].id + '"></input></td>';
              html += '<input type="hidden" class="form-control id" id="id'  + AggExploded[j].id + '" name="id' + '" value="' + AggExploded[j].id + '"><input type="hidden"  data-toggle="tooltip" data-placement="top" title="'+ AggExploded[j].id_items + '" id="id_items'  + AggExploded[j].id + '" name="id_items' + '" value="' + AggExploded[j].id_items + '"></input></td>';
              html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ AggExploded[j].id +'" id="descriptiontext'+ AggExploded[j].id +'"> ' + AggExploded[j].description + '</label><input type="text" data-toggle="tooltip" style="display:none;" data-placement="top" title="'+ AggExploded[j].description + '" id="description' + AggExploded[j].id + '" name="description' + '" class="form-control description' + '" value="' + AggExploded[j].description + '"></td>';
              html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ AggExploded[j].id +'" id="codegrtext'+ AggExploded[j].id +'"> ' + AggExploded[j].code_gr + '</label><input type="text" class="form-control code_gr" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ AggExploded[j].code_gr + '"  id="code_gr' +  AggExploded[j].id + '" name="code_gr' + '" value="' + AggExploded[j].code_gr + '"></td>';
              html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ AggExploded[j].id +'" id="supplier'+ AggExploded[j].id +'"> ' + AggExploded[j].supplier + '</label><input type="text" class="form-control supplier" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ AggExploded[j].supplier + '"  id="supplier' +  AggExploded[j].id + '" name="supplier' + '" value="' + AggExploded[j].supplier + '"></td>';
              html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ AggExploded[j].id +'" id="code_supplier'+ AggExploded[j].id +'"> ' + AggExploded[j].code_supplier + '</label><input type="text" class="form-control code_supplier" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ AggExploded[j].code_supplier + '"  id="code_supplier' +  AggExploded[j].id + '" name="code_supplier' + '" value="' + AggExploded[j].code_supplier + '"></td>';
              html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ AggExploded[j].id +'" id="qta'+ AggExploded[j].id +'"> ' + AggExploded[j].qta + '</label><input type="text" class="form-control qta" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ AggExploded[j].qta + '"  id="qta' +  AggExploded[j].id + '" name="qta' + '" value="' + AggExploded[j].qta + '"></td>';
              html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ AggExploded[j].id +'" id="unit_price'+ AggExploded[j].id +'"> ' + AggExploded[j].unit_price + '</label><input type="text" class="form-control unit_price" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ AggExploded[j].unit_price + '"  id="unit_price' +  AggExploded[j].id + '" name="unit_price' + '" value="' + AggExploded[j].unit_price + '"></td>';
              html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ AggExploded[j].id +'" id="total_price'+ AggExploded[j].id +'"> ' + AggExploded[j].total_price + '</label><input type="text" class="form-control total_price" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ AggExploded[j].total_price + '"  id="total_price' +  AggExploded[j].id + '" name="total_price' + '" value="' + AggExploded[j].total_price + '"></td>';
              html += "<td>" + '<button type="button" class="btn btn-outline-success" onclick="itemsGrHelpers.updateExploded('+ AggExploded[j].id_items + ',' + AggExploded[j].id + ',false)"><i class="fa fa-floppy-o" aria-hidden="true"></i></button></td>';
              html += "<td>" + '<button type="button" class="btn btn-outline-danger" id="btndelete' + '" onclick="itemsGrHelpers.deleteExploded(' +
              AggExploded[j].id + ',false)"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>';
              html += "</tr>"; 
              $("#div-cont-result_exploded tbody").append(html);  
            }
          }

          var cont = $('#div-cont-result_exploded tbody tr').length;
          $("#add-img-agg").on("click", function(e) {
            var html = "";
            if(cont < 10){
            cont = cont + 1;
            var html = "";
               html += "<tr>";
               html += '<input type="hidden" class="form-control id" id="id'  + cont + '" name="id' + '" value="' + cont + '">';
               html += '<input type="hidden" class="form-control id_items" id="id_items'  + cont + '" name="id_items' + '" value="' + cont + '">';
               html += "<td>" + '<input type="text" id="description' + cont + '" name="description' + cont + '" class="form-control description' + cont + '"></td>';
               html += "<td>" + '<input type="text" class="form-control code_gr" id="code_gr' + cont + '" name="code_gr' + cont + '"></td>';
               html += "<td>" + '<input type="text" class="form-control supplier" id="supplier' + cont + '" name="supplier' + cont + '"></td>';
               html += "<td>" + '<input type="text" class="form-control code_supplier" id="code_supplier' + cont + '" name="code_supplier' + cont + '"></td>';
               html += "<td>" + '<input type="text" class="form-control qta" id="qta' + cont + '" name="qta' + cont + '"></td>';
               html += "<td>" + '<input type="text" class="form-control unit_price" id="unit_price' + cont + '" name="unit_price' + cont + '"></td>';
               html += "<td>" + '<input type="text" class="form-control total_price" id="total_price' + cont + '" name="total_price' + cont + '"></td>';
               html += "<td>" + '<button type="button" class="btn btn-outline-success" id="btnsave' + '" onclick="itemsGrHelpers.updateExploded('+ response.data.id +  ','+ cont +',true)"><i class="fa fa-floppy-o" aria-hidden="true"></i></button></td>';
               html += "<td>" + '<button type="button" class="btn btn-outline-danger btndeletes" id="btndelete' + '" onclick="btndelete' + '"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>';
               html += "</tr>";
               $('#div-cont-result_exploded tbody').append(html);
            } 

            $("#div-cont-result_exploded tbody .btndeletes").on("click", function(e) {
                  var el = $(this).parent('td').parent('tr');
                  AggExploded = AggExploded - 1;
                  if(AggExploded<0){
                    AggExploded = 0;
                  }
                  el.remove();
                });

                $("#div-cont-result_exploded tbody .unit_price").on("keyup", function () {
                  var contRow = j  
                  var unit_price = $("#div-cont-result_exploded tbody #unit_price" + cont).val();
                  var qta = $("#div-cont-result_exploded tbody #qta" + cont).val();
      
                  var totaleFinale = parseFloat(qta) * parseFloat(unit_price);
                  $('#div-cont-result_exploded tbody .total_price').val(totaleFinale);
              });
            });

            $("#div-CodGR_List_Article").html("");
            var html = "";
            html += '<div class="row no-gutters" data-id-row="' + '" id="divcont-' + '" >';
            html += '<div class="col-6">';
            html += '<label>Codice GR</label><select class="custom-select code_list_gr_select" id="code_list_gr' + cont + '" name="code_list_gr' + cont + '"></select>';
            html += '</div>';
            html += '</div>';
            $('#div-CodGR_List_Article').append(html); 

            $.getScript("https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js").done(function(){ 
              var searchParam = "";
             $('#code_list_gr' + cont).select2({
              ajax: {
              url: configData.wsRootServicesUrl + '/api/v1/itemGr/getSelectListArticle',
              data: function (params) {
              if(params.term==''){
                searchParam = '*';
              }else{
              searchParam = params.term;
              }
              var query = {
              search: searchParam,
              }
              return query;
            },
            processResults: function (data) {
              var dataParse = JSON.parse(data);
              return {
              results: dataParse.data
              };
            }
            }
            });  
          });           

          var idItem=  response.data.id;
          $("#code_list_gr" + cont).on("change", function () {
            var id = $(this).val();
            itemGrServices.getProduct(id, function (response) {
              var getProduct = response.data.AggProduct;
              console.log(response.data.AggProduct);
              if (getProduct.length > 0) {
               for (var j = 0; j < getProduct.length; j++) {
               // $("#div-Result-listArticle").html("");
                var html = "";
                html += "<tr>";
                html += '<input type="hidden" class="form-control id" id="id'  + getProduct[j].id + '" name="id' + '" value="' + getProduct[j].id + '"><input type="hidden"  data-toggle="tooltip" data-placement="top" title="'+ getProduct[j].id + '" id="id'  + getProduct[j].id + '" name="id' + '" value="' + getProduct[j].id + '"></input></td>';
                //html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ getProduct[j].id +'" id="code_suppliertext'+ AggExploded[j].id +'"> ' + AggExploded[j].code_supplier + '</label><input type="text" data-toggle="tooltip" style="display:none;" data-placement="top" title="'+ AggExploded[j].code_supplier + '" id="code_supplier' + AggExploded[j].id + '" name="code_supplier' + '" class="form-control code_supplier' + '" value="' + AggExploded[j].code_supplier + '"></td>';
                html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ getProduct[j].id +'" id="notetext'+ getProduct[j].id +'"> ' + getProduct[j].note + '</label><input type="text" class="form-control note" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ getProduct[j].note + '"  id="note' +  getProduct[j].id + '" name="note' + '" value="' + getProduct[j].note + '"></td>';
                html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ getProduct[j].id +'" id="codegrtext'+ getProduct[j].id +'"> ' + getProduct[j].code_gr + '</label><input type="text" class="form-control code_gr" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ getProduct[j].code_gr + '"  id="code_gr' +  getProduct[j].id + '" name="code_gr' + '" value="' + getProduct[j].code_gr + '"></td>';
                html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ getProduct[j].id +'" id="supplier'+ getProduct[j].id +'"> ' + getProduct[j].supplier + '</label><input type="text" class="form-control supplier" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ getProduct[j].supplier + '"  id="supplier' +  getProduct[j].id + '" name="supplier' + '" value="' + getProduct[j].supplier + '"></td>';
                html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ getProduct[j].id +'" id="code_supplier'+ getProduct[j].id +'"> ' + getProduct[j].code_supplier + '</label><input type="text" class="form-control code_supplier" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ getProduct[j].code_supplier + '"  id="code_supplier' +  getProduct[j].id + '" name="code_supplier' + '" value="' + getProduct[j].code_supplier + '"></td>';
                //html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ getProduct[j].id +'" id="qta'+ AggExploded[j].id +'"> ' + AggExploded[j].qta + '</label><input type="text" class="form-control qta" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ AggExploded[j].qta + '"  id="qta' +  AggExploded[j].id + '" name="qta' + '" value="' + AggExploded[j].qta + '"></td>';
                html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ getProduct[j].id +'" id="price_purchase'+ getProduct[j].id +'"> ' + getProduct[j].price_purchase + '</label><input type="text" class="form-control price_purchase" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ getProduct[j].price_purchase + '"  id="price_purchase' +  getProduct[j].id + '" name="price_purchase' + '" value="' + getProduct[j].price_purchase + '"></td>';
                html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ getProduct[j].id +'" id="update_date'+ getProduct[j].id +'"> ' + getProduct[j].update_date + '</label><input type="text" class="form-control update_date" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ getProduct[j].update_date + '"  id="update_date' +  getProduct[j].id + '" name="update_date' + '" value="' + getProduct[j].update_date + '"></td>';
                //html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ getProduct[j].id +'" id="unit_price'+ AggExploded[j].id +'"> ' + AggExploded[j].unit_price + '</label><input type="text" class="form-control unit_price" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ AggExploded[j].unit_price + '"  id="unit_price' +  AggExploded[j].id + '" name="unit_price' + '" value="' + AggExploded[j].unit_price + '"></td>';
                //html += "<td>" + '<label style="cursor:pointer;" data-attr-id="'+ getProduct[j].id +'" id="total_price'+ AggExploded[j].id +'"> ' + AggExploded[j].total_price + '</label><input type="text" class="form-control total_price" style="display:none;" data-toggle="tooltip" data-placement="top" title="'+ AggExploded[j].total_price + '"  id="total_price' +  AggExploded[j].id + '" name="total_price' + '" value="' + AggExploded[j].total_price + '"></td>';
               // html += "<td>" + '<button type="button" class="btn btn-outline-success" onclick="itemsGrHelpers.insertExploded(' + getProduct[j].id + ',' + getProduct[j].code_gr + ',' + getProduct[j].code_supplier + ',' + getProduct[j].price_purchase + ')">Aggiungi</button></td>';
                html += '<td><button type="button" class="btn btn-outline-success" onclick="itemsGrHelpers.insertExploded(' + getProduct[j].id + ',' + idItem + ')">Aggiungi</button></td>';
                html += "</tr>"; 
                $('#div-Result-listArticle tbody').append(html);  
              }
            }
            });                                    
          });

            $("#frmCmpItemsGr").show();
            $("#rr").show();
            $("#frmUpdateItemsGr").hide();
            $("#table_list_itemsGR").hide(); 
            $("#frmSearchItems").hide();
            $("#table_list_searchitems").hide();
            $("#listino").hide();
            $("#listinohr").hide();
}); 
}

export function insertExploded(id,idInsertExp) {
console.log(id);
console.log(idInsertExp);
console.log($('#codegrtext' + id).text());
console.log($('#supplier' + id).text());
console.log($('#code_supplier' + id).text());
console.log($('#price_purchase' + id).text());


var cont = $('#div-cont-result_exploded tbody tr').length +1;
    var html = "";
        html += "<tr>";
        html += '<input type="hidden" class="form-control id" id="id'  + cont + '" name="id' + '" value="' + cont + '">';
        html += '<input type="hidden" class="form-control id_items" id="id_items'  + cont + '" name="id_items' + '" value="' + cont + '">';
        html += "<td>" + '<input type="text" id="description' + cont + '" name="description' + cont + '" class="form-control description' + cont + '" value="' + $('#notetext' + id).text()+ '"></td>';
        html += "<td>" + '<input type="text" class="form-control code_gr" id="code_gr' + cont + '" name="code_gr' + cont + '" value="' + $('#codegrtext' + id).text()+ '"></td>';
        html += "<td>" + '<input type="text" class="form-control supplier" id="supplier' + cont + '" name="supplier' + cont + '" value="' + $('#supplier' + id).text()+ '"></td>';
        html += "<td>" + '<input type="text" class="form-control code_supplier" id="code_supplier' + cont + '" name="code_supplier' + cont + '" value="' + $('#code_supplier' + id).text()+ '"></td>';
        html += "<td>" + '<input type="text" class="form-control qta" id="qta' + cont + '" name="qta' + cont + '"></td>';
        html += "<td>" + '<input type="text" class="form-control unit_price" id="unit_price' + cont + '" name="unit_price' + cont + '" value="' + $('#price_purchase' + id).text()+ '"></td>';
        html += "<td>" + '<input type="text" class="form-control total_price" id="total_price' + cont + '" name="total_price' + cont + '"></td>';
        html += "<td>" + '<button type="button" class="btn btn-outline-success" id="btnsave' + '" onclick="itemsGrHelpers.updateExploded('+ idInsertExp +  ','+ cont +',true)"><i class="fa fa-floppy-o" aria-hidden="true"></i></button></td>';
        html += "<td>" + '<button type="button" class="btn btn-outline-danger btndeletes" id="btndelete' + '" onclick="btndelete' + '"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>';
        html += "</tr>";
        $('#div-cont-result_exploded tbody').append(html);
    

      $("#div-cont-result_exploded tbody .btndeletes").on("click", function(e) {
          var el = $(this).parent('td').parent('tr');
          AggExploded = AggExploded - 1;
          if(AggExploded<0){
            AggExploded = 0;
          }
          el.remove();
        });

        $("#div-cont-result_exploded tbody .unit_price").on("keyup", function () {   
          var unit_price = $("#div-cont-result_exploded tbody #unit_price" + cont).val();
          var qta = $("#div-cont-result_exploded tbody #qta" + cont).val();

          var totaleFinale = parseFloat(qta) * parseFloat(unit_price);
          $('#div-cont-result_exploded tbody .total_price').val(totaleFinale);
      });
}



export function updateExploded(id_items, id, isNew) {
  spinnerHelpers.show();

  var data = {
    id_items:id_items,
    id: id,
    isNew:isNew,
    };
  
  //var arrayAggItemsExplodes = [];

 /* for (var j = 0; j < 10; j++) {
    var jsonAggItemsExplodes = {};
    var check = false;
    if (functionHelpers.isValued($('#description' + j).val())) {
       check = true;
       jsonAggItemsExplodes['description'] = $('#description' + j).val();
    }
    if (functionHelpers.isValued($('#code_gr' + j).val())) {
      check = true;
       jsonAggItemsExplodes['code_gr'] = $('#code_gr' + j).val();
   }
    if (functionHelpers.isValued($('#supplier' + j).val())) {
        check = true;
       jsonAggItemsExplodes['supplier'] = $('#supplier' + j).val();
    }
    if (functionHelpers.isValued($('#code_supplier' + j).val())) {
        check = true;
       jsonAggItemsExplodes['code_supplier'] = $('#code_supplier' + j).val();
    }
    if (functionHelpers.isValued($('#qta' + j).val())) {
        check = true;
       jsonAggItemsExplodes['qta'] = $('#qta' + j).val();
    }
    if (functionHelpers.isValued($('#unit_price' + j).val())) {
        check = true;
       jsonAggItemsExplodes['unit_price'] = $('#unit_price' + j).val();
    }
    if (functionHelpers.isValued($('#total_price' + j).val())) {
        check = true;
       jsonAggItemsExplodes['total_price'] = $('#total_price' + j).val();
    }
    if (functionHelpers.isValued($('#id_items' + j).val())) {
        check = true;
      jsonAggItemsExplodes['id_items'] = $('#id_items' + j).val();
  }
  if (functionHelpers.isValued($('#id' + j).val())) {
        check = true;
      jsonAggItemsExplodes['id'] = $('#id' + j).val();
}
     if (check) {
      arrayAggItemsExplodes.push(jsonAggItemsExplodes);
     }
    }
    data.AggExploded = arrayAggItemsExplodes;*/

    var arrayAggItemsExplodes = [];
    var jsonAggItemsExplodes = {};
    var check = false;

    if (functionHelpers.isValued($('#description' + id).val())) {
       check = true;
       jsonAggItemsExplodes['description'] = $('#description' + id).val();
    }
     if (functionHelpers.isValued($('#id' + id).val())) {
        check = true;
        jsonAggItemsExplodes['id'] = $('#id' + id).val();
    } 
    if (functionHelpers.isValued($('#code_gr' + id).val())) {
       check = true;
       jsonAggItemsExplodes['code_gr'] = $('#code_gr' + id).val();
    }
    jsonAggItemsExplodes['isNew'] = isNew;
    jsonAggItemsExplodes['id_items'] = id_items;

    if (functionHelpers.isValued($('#supplier' + id).val())) {
       check = true;
       jsonAggItemsExplodes['supplier'] = $('#supplier' + id).val();
    }
    if (functionHelpers.isValued($('#code_supplier' + id).val())) {
      check = true;
      jsonAggItemsExplodes['code_supplier'] = $('#code_supplier' + id).val();
   }
    if (functionHelpers.isValued($('#qta' + id).val())) {
       check = true;
       jsonAggItemsExplodes['qta'] = $('#qta' + id).val();
    }
    if (functionHelpers.isValued($('#unit_price' + id).val())) {
      check = true;
      jsonAggItemsExplodes['unit_price'] = $('#unit_price' + id).val();
   }
   if (functionHelpers.isValued($('#total_price' + id).val())) {
    check = true;
    jsonAggItemsExplodes['total_price'] = $('#total_price' + id).val();
 }
 /*if (functionHelpers.isValued($('#id_items' + id).val())) {
  check = true;
  jsonAggItemsExplodes['id_items'] = $('#id_items' + id).val();
}*/
  
     if (check) {
      arrayAggItemsExplodes.push(jsonAggItemsExplodes);
     }
     data = arrayAggItemsExplodes;

    itemGrServices.updateExploded(data, function () {
    notificationHelpers.success(dictionaryHelpers.getDictionary("ContactRequestCompleted"));
    spinnerHelpers.hide();
    //window.location.reload();
     //$("#table_list_itemsGR").hide();
    //  $("#frmUpdateItemsGr").hide(); 
    });
}



  export function update() {
    spinnerHelpers.show();
    var data = {
      //id: storageData.sUserId(),
      updated_id: storageData.sUserId(),
      idLanguage: storageData.sIdLanguage(),
      id: $('#frmUpdateItemsGr #id').val(),
      tipology: $('#frmUpdateItemsGr #tipology').val(),
      note: $('#frmUpdateItemsGr #note').val(),
      plant_id: $('#frmUpdateItemsGr #plant_id').val(),
      original_code: $('#frmUpdateItemsGr #original_code').val(),
      code_product: $('#frmUpdateItemsGr #code_product').val(),
      code_gr: $('#frmUpdateItemsGr #code_gr').val(),
      plant: $('#frmUpdateItemsGr #plant').val(),
      brand: $('#frmUpdateItemsGr #brand').val(),
      business_name_supplier: $('#frmUpdateItemsGr #business_name_supplier').val(),
      code_supplier: $('#frmUpdateItemsGr #code_supplier').val(),
      price_purchase: $('#frmUpdateItemsGr #price_purchase').val(),
      price_list: $('#frmUpdateItemsGr #price_list').val(),
      repair_price: $('#frmUpdateItemsGr #repair_price').val(),
      price_new: $('#frmUpdateItemsGr #price_new').val(),
      update_date: $('#frmUpdateItemsGr #update_date').val(),
      usual_supplier: $('#frmUpdateItemsGr #usual_supplier').val(),
      pr_rip_conc: $('#frmUpdateItemsGr #pr_rip_conc').val(),
      img: $('#frmUpdateItemsGr #imgBase64').val(),
      imgName: $('#frmUpdateItemsGr #imgName').val(),
    };

     var arrayAggItems = [];
  for (var i = 0; i < 10; i++) {
    var jsonAggItems = {};
    var check = false;
    if (functionHelpers.isValued($('#business_name_supplier' + i).val())) {
       check = true;
      jsonAggItems['business_name_supplier'] = $('#business_name_supplier' + i).val();
    }
    if (functionHelpers.isValued($('#business_name_suppliers' + i).val())) {
      check = true;
     jsonAggItems['business_name_suppliers'] = $('#business_name_suppliers' + i).val();
   }
    if (functionHelpers.isValued($('#code_supplier' + i).val())) {
       check = true;
      jsonAggItems['code_supplier'] = $('#code_supplier' + i).val();
    }
    if (functionHelpers.isValued($('#price_purchase' + i).val())) {
       check = true;
      jsonAggItems['price_purchase'] = $('#price_purchase' + i).val();
    }
    if (functionHelpers.isValued($('#update_date' + i).val())) {
       check = true;
     jsonAggItems['update_date'] = $('#update_date' + i).val();
    }
    if (functionHelpers.isValued($('#usual_supplier' + i).val())) {
       check = true;
      jsonAggItems['usual_supplier'] = $('#usual_supplier' + i).val();
    }
    if (functionHelpers.isValued($('#id_gritems' + i).val())) {
        check = true;
      jsonAggItems['id_gritems'] = $('#id_gritems' + i).val();
    }
     if (check) {
      arrayAggItems.push(jsonAggItems);
     }
    }
    data.AggItems = arrayAggItems;
  
    itemGrServices.update(data, function () {
      $('#frmUpdateItemsGr #id').val('');
      $('#frmUpdateItemsGr #tipology').val('');
      $('#frmUpdateItemsGr #note').val('');
      $('#frmUpdateItemsGr #original_code').val('');
      $('#frmUpdateItemsGr #code_product').val('');
      $('#frmUpdateItemsGr #code_gr').val('');
      $('#frmUpdateItemsGr #plant').val('');
      $('#frmUpdateItemsGr #brand').val('');
      $('#frmUpdateItemsGr #business_name_supplier').val('');
      $('#frmUpdateItemsGr #business_name_suppliers').val('');
      $('#frmUpdateItemsGr #code_supplier').val('');
      $('#frmUpdateItemsGr #price_purchase').val('');
      $('#frmUpdateItemsGr #price_list').val('');
      $('#frmUpdateItemsGr #repair_price').val('');
      $('#frmUpdateItemsGr #plant_id').val('');
      $('#frmUpdateItemsGr #price_new').val('');
      $('#frmUpdateItemsGr #update_date').val('');
      $('#frmUpdateItemsGr #usual_supplier').val('');
      $('#frmUpdateItemsGr #imgBase64').val('');
      $('#frmUpdateItemsGr #pr_rip_conc').val('');
   
     notificationHelpers.success(dictionaryHelpers.getDictionary("ContactRequestCompleted"));
      spinnerHelpers.hide();
      window.location.reload();
      $("#table_list_itemsGR").show();
      $("#frmUpdateItemsGr").hide(); 
    });
  }

  export function deleteRow(id) {
  if (confirm("Vuoi eliminare l\'articolo?")) {
  spinnerHelpers.show();
  itemGrServices.del(id, function (response) {
  notificationHelpers.success(dictionaryHelpers.getDictionary("DeleteCompleted"));
  spinnerHelpers.hide();
  window.location.reload();
    });
  }
  }

  export function deleteExploded(id) {
    if (confirm("sei sicuro di voler eliminare?")) {
      spinnerHelpers.show();
      itemGrServices.deleteExploded(id, function (response) {
      notificationHelpers.success(dictionaryHelpers.getDictionary("DeleteCompleted"));
      spinnerHelpers.hide();
      window.location.reload();
        });
      } 
    }



  export function insertNoAuth() {
    spinnerHelpers.show();
    
    var data = {
      created_id: storageData.sUserId(),
      idLanguage: storageData.sIdLanguage(),
      img: $('#frmAddItemsGr #imgBase64').val(),
      imgName: $('#frmAddItemsGr #imgName').val(),
      tipology: $('#frmAddItemsGr #tipology').val(),
      note: $('#frmAddItemsGr #note').val(),
      plant_id: $('#frmAddItemsGr #plant_id').val(),
      original_code: $('#frmAddItemsGr #original_code').val(),
      code_product: $('#frmAddItemsGr #code_product').val(),
      code_gr: $('#frmAddItemsGr #code_gr').val(),
      plant: $('#frmAddItemsGr #plant').val(),
      brand: $('#frmAddItemsGr #brand').val(),
      business_name_supplier: $('#frmAddItemsGr #business_name_supplier').val(),
      code_supplier: $('#frmAddItemsGr #code_supplier').val(),
      price_purchase: $('#frmAddItemsGr #price_purchase').val(),
      price_list: $('#frmAddItemsGr #price_list').val(),
      repair_price: $('#frmAddItemsGr #repair_price').val(),
      price_new: $('#frmAddItemsGr #price_new').val(),
      update_date: $('#frmAddItemsGr #update_date').val(),
      usual_supplier: $('#frmAddItemsGr #usual_supplier').val(),
      pr_rip_conc: $('#frmAddItemsGr #pr_rip_conc').val(),
    };
  
      itemGrServices.insertNoAuth(data, function () {
      $('#frmAddItemsGr #imgBase64').val('');
      $('#frmAddItemsGr #tipology').val('');
      $('#frmAddItemsGr #note').val('');
      $('#frmAddItemsGr #original_code').val('');
      $('#frmAddItemsGr #plant_id').val('');
      $('#frmAddItemsGr #code_product').val('');
      $('#frmAddItemsGr #code_gr').val('');
      $('#frmAddItemsGr #plant').val('');
      $('#frmAddItemsGr #brand').val('');
      $('#frmAddItemsGr #business_name_supplier').val('');
      $('#frmAddItemsGr #code_supplier').val('');
      $('#frmAddItemsGr #price_purchase').val('');
      $('#frmAddItemsGr #price_list').val('');
      $('#frmAddItemsGr #repair_price').val('');
      $('#frmAddItemsGr #price_new').val('');
      $('#frmAddItemsGr #update_date').val('');
      $('#frmAddItemsGr #usual_supplier').val('');
      $('#frmAddItemsGr #pr_rip_conc').val('');
      notificationHelpers.success('Inserimento completato con successo!');
      spinnerHelpers.hide();
    });
  }
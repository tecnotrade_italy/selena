/***   spinnerHelpers   ***/

export function show() {
    $('.modal-backdrop').not('.fade').css('z-index', '9000');
    $('#spinnerView').modal('show');
}

export function hide() {
    $('#spinnerView').modal('hide');
}
/***   rolesHelpers   ***/

export function getAllNoAuth() {
  
  spinnerHelpers.show();

  rolesServices.getAllNoAuth(function (response) {   
     for(var i = 0; i<response.data.length;i++){
       var newList = "";
       newList += "<tr>";
       newList +=
       '<td><button class="btn btn-info" onclick="rolesHelpers.getByIdRoles(' +
       response.data[i].id +
       ')"><i class="fa fa-pencil"/></button>';
         newList +=
         '<button class="btn btn-danger" onclick="rolesHelpers.deleteRow(' +
         response.data[i].id +
           ')"><i class="fa fa-trash"/></button></td>';
       newList += "<td>" + response.data[i].id + "</td>";
       newList += "<td>" + response.data[i].name + "</td>";
       newList += "<td>" + response.data[i].display_name + "</td>"; 
       newList += "<td>" + response.data[i].description + "</td>";
       newList += "</tr>";
      $( "#table_list_roles tbody").append(newList);
      $("#table_list_roles").show();
      $("#frmUpdateRoles").hide();
     }
   });

   spinnerHelpers.hide();
 }

 export function getByIdRoles(id) {
  // spinnerHelpers.show();
  rolesServices.getByIdRoles(id, function (response) {
   jQuery.each(response.data, function(i, val) {
    if (val == true){
      $('#' + i).attr('checked','checked');
    }else{
      $("#" + i).val(val);
    }
   }); 
 });
 $("#btnadd").hide();
 $("#frmUpdateRoles").show();
 $("#table_list_roles").hide();
 }


 export function updateNoAuth() {

  spinnerHelpers.show();
  var data = {
    //id: storageData.sUserId(),
    updated_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmUpdateRoles #id').val(),
    description: $('#frmUpdateRoles #description').val(),
    name: $('#frmUpdateRoles #name').val(),
    display_name: $('#frmUpdateRoles #display_name').val(),
  };
  rolesServices.updateNoAuth(data, function () {
    $('#frmUpdateRoles #id').val('');
    $('#frmUpdateRoles #description').val('');
    $('#frmUpdateRoles #name').val('');
    $('#frmUpdateRoles #display_name').val('');

   notificationHelpers.success(dictionaryHelpers.getDictionary("ContactRequestCompleted"));
    spinnerHelpers.hide();
    $("#table_list_roles").show();
    $("#frmUpdateRoles").hide();
    
    window.location.reload();
  });
}

export function deleteRow(id) {
if (confirm("Vuoi eliminare il ruolo?")) {
spinnerHelpers.show();
rolesServices.del(id, function (response) {
notificationHelpers.success(dictionaryHelpers.getDictionary("DeleteCompleted"));
spinnerHelpers.hide();
window.location.reload();
  });
} 
}

export function insertNoAuth() {
  spinnerHelpers.show();
  
  var data = {
    created_id: storageData.sUserId(),
    idLanguage: storageData.sIdLanguage(),
    id: $('#frmAddRoles #id').val(),
    name: $('#frmAddRoles #name').val(),
    display_name: $('#frmAddRoles #display_name').val(),
    description: $('#frmAddRoles #description').val(),
  };

  rolesServices.insertNoAuth(data, function () {
    $('#frmAddRoles #id').val('');
    $('#frmAddRoles #name').val('');
    $('#frmAddRoles #display_name').val('');
    $('#frmAddRoles #description').val('');
    notificationHelpers.success(dictionaryHelpers.getDictionary("ContactRequestCompleted"));
    spinnerHelpers.hide();
  });
}
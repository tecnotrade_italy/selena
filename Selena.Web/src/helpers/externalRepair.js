//  externalRepairHelpers
export function getAllexternalRepairSearch(business_name,code_gr,code_intervention_gr,date) {
  spinnerHelpers.show();
    var data = functionHelpers.formToJson(business_name,code_gr,code_intervention_gr,date);
  //data["idLanguage"] = storageData.sIdLanguage();
  externalRepairServices.getAllexternalRepairSearch(data, function (response) {
    $("#table_list_external_repair tbody").html('');
    if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_external_repair tbody").append(newList);
      $("#table_list_external_repair").show();
      $("#frmVieUpdatewRepair").hide();
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        newList += "<tr>";
        newList += "<td>" + response.data[i].id_librone_rip_esterna + "</td>";
       // newList += "<td>" + response.data[i].id + "</td>";
        newList += "<td>" + response.data[i].numero + "</td>";
        newList += "<td>" + response.data[i].date + "</td>";
        newList += "<td>" + response.data[i].ddt_exit + "</td>";
        newList += "<td>" + response.data[i].id_business_name_supplier + "</td>";
        newList += "<td>";
        if (response.data[i].img != "") {
          newList +=
            '<div class="container"><div id="lightgallery" style="display:flex">';
          newList +=
            "<a href='" +
            response.data[i].img +
            "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        newList += "</td>";
        newList += "<td>" + response.data[i].description + "</td>";
    newList += "<td>";
        if (response.data[i].imgmodule != "") {
          newList +=
            '<div class="container"><div id="lightgallery" style="display:flex">';
          newList +=
            "<a href='" +
            response.data[i].imgmodule +
            "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        newList += "</td>";
        newList += "<td>" + response.data[i].code_gr + "</td>";
        newList += "<td>" + response.data[i].date_return_product_from_the_supplier + "</td>";
        newList += "<td>" + response.data[i].ddt_supplier + "</td>";
        newList += "<td>" + response.data[i].reference_supplier + "</td>";
        newList += "<td>" + response.data[i].notes_from_repairman + "</td>";
        newList += "<td>" + response.data[i].price + "</td>";
        newList += "<td>";
        if (response.data[i].img_rep_test != "") {
          newList +=
            '<div class="container"><div id="lightgallery" style="display:flex">';
          newList +=
            "<a href='" +
            response.data[i].img_rep_test +
            "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        newList += "</td>";
        newList +=
        '<td><button class="btn btn-info" onclick="externalRepairHelpers.getByRepair(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil"style="color:#fff!important;"/></button></td>';
        newList +=
          '<td><button class="btn btn-danger" onclick="externalRepairHelpers.deleteRow(' +
          response.data[i].id +
          ')"><i class="fa fa-trash"style="color:#fff!important;"/></button></td>';
        newList += "</tr>";
        $("#table_list_external_repair tbody").append(newList);
        $("#table_list_external_repair").show();
        $("#frmVieUpdatewRepair").hide();
      }
    }
    lightGallery(document.getElementById('lightgallery'));
  });
  spinnerHelpers.hide();
}

export function getexternalRepairSearchViewOpen() {
  spinnerHelpers.show();
  //data["idLanguage"] = storageData.sIdLanguage();
  externalRepairServices.getexternalRepairSearchViewOpen(data, function (response) {
    $("#table_list_external_repair tbody").html('');
    if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_external_repair tbody").append(newList);
      $("#table_list_external_repair").show();
      $("#frmVieUpdatewRepair").hide();
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        newList += "<tr>";
        newList += "<td>" + response.data[i].id_librone_rip_esterna + "</td>";
       // newList += "<td>" + response.data[i].id + "</td>";
        newList += "<td>" + response.data[i].numero + "</td>";
        newList += "<td>" + response.data[i].date + "</td>";
        newList += "<td>" + response.data[i].ddt_exit + "</td>";
        newList += "<td>" + response.data[i].id_business_name_supplier + "</td>";
        newList += "<td>";
        if (response.data[i].img != "") {
          newList +=
            '<div class="container"><div id="lightgallery" style="display:flex">';
          newList +=
            "<a href='" +
            response.data[i].img +
            "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        newList += "</td>";
        newList += "<td>" + response.data[i].description + "</td>";
    newList += "<td>";
        if (response.data[i].imgmodule != "") {
          newList +=
            '<div class="container"><div id="lightgallery" style="display:flex">';
          newList +=
            "<a href='" +
            response.data[i].imgmodule +
            "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        newList += "</td>";
        newList += "<td>" + response.data[i].code_gr + "</td>";
        newList += "<td>" + response.data[i].date_return_product_from_the_supplier + "</td>";
        newList += "<td>" + response.data[i].ddt_supplier + "</td>";
        newList += "<td>" + response.data[i].reference_supplier + "</td>";
        newList += "<td>" + response.data[i].notes_from_repairman + "</td>";
        newList += "<td>" + response.data[i].price + "</td>";
        newList += "<td>";
        if (response.data[i].img_rep_test != "") {
          newList +=
            '<div class="container"><div id="lightgallery" style="display:flex">';
          newList +=
            "<a href='" +
            response.data[i].img_rep_test +
            "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        newList += "</td>";
        newList +=
        '<td><button class="btn btn-info" onclick="externalRepairHelpers.getByRepair(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil"style="color:#fff!important;"/></button></td>';
        newList +=
          '<td><button class="btn btn-danger" onclick="externalRepairHelpers.deleteRow(' +
          response.data[i].id +
          ')"><i class="fa fa-trash"style="color:#fff!important;"/></button></td>';
        newList += "</tr>";
        $("#table_list_external_repair tbody").append(newList);
        $("#table_list_external_repair").show();
        $("#frmVieUpdatewRepair").hide();
      }
    }
    lightGallery(document.getElementById('lightgallery'));
  });
  spinnerHelpers.hide();
}

export function getexternalRepairSearchViewClose() {
  spinnerHelpers.show();
  //data["idLanguage"] = storageData.sIdLanguage();
  externalRepairServices.getexternalRepairSearchViewClose(data, function (response) {
    $("#table_list_external_repair tbody").html('');
    if (response.data.length == 0) {
      var newList = "";
      newList += "<tr>";
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td style="width: 100%;"><div class="col-12" style="text-align: center;"><h1>Nessuna informazione trovata</h1></div></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += '<td></td>';
      newList += "</tr>";
      $("#table_list_external_repair tbody").append(newList);
      $("#table_list_external_repair").show();
      $("#frmVieUpdatewRepair").hide();
    } else {
      for (var i = 0; i < response.data.length; i++) {
        var newList = "";
        newList += "<tr>";
        newList += "<td>" + response.data[i].id_librone_rip_esterna + "</td>";
       // newList += "<td>" + response.data[i].id + "</td>";
        newList += "<td>" + response.data[i].numero + "</td>";
        newList += "<td>" + response.data[i].date + "</td>";
        newList += "<td>" + response.data[i].ddt_exit + "</td>";
        newList += "<td>" + response.data[i].id_business_name_supplier + "</td>";
        newList += "<td>";
        if (response.data[i].img != "") {
          newList +=
            '<div class="container"><div id="lightgallery" style="display:flex">';
          newList +=
            "<a href='" +
            response.data[i].img +
            "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        newList += "</td>";
        newList += "<td>" + response.data[i].description + "</td>";
    newList += "<td>";
        if (response.data[i].imgmodule != "") {
          newList +=
            '<div class="container"><div id="lightgallery" style="display:flex">';
          newList +=
            "<a href='" +
            response.data[i].imgmodule +
            "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        newList += "</td>";
        newList += "<td>" + response.data[i].code_gr + "</td>";
        newList += "<td>" + response.data[i].date_return_product_from_the_supplier + "</td>";
        newList += "<td>" + response.data[i].ddt_supplier + "</td>";
        newList += "<td>" + response.data[i].reference_supplier + "</td>";
        newList += "<td>" + response.data[i].notes_from_repairman + "</td>";
        newList += "<td>" + response.data[i].price + "</td>";
        newList += "<td>";
        if (response.data[i].img_rep_test != "") {
          newList +=
            '<div class="container"><div id="lightgallery" style="display:flex">';
          newList +=
            "<a href='" +
            response.data[i].img_rep_test +
            "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
        }
        newList += "</td>";
        newList +=
        '<td><button class="btn btn-info" onclick="externalRepairHelpers.getByRepair(' +
        response.data[i].id +
        ')"><i class="fa fa-pencil"style="color:#fff!important;"/></button></td>';
        newList +=
          '<td><button class="btn btn-danger" onclick="externalRepairHelpers.deleteRow(' +
          response.data[i].id +
          ')"><i class="fa fa-trash"style="color:#fff!important;"/></button></td>';
        newList += "</tr>";
        $("#table_list_external_repair tbody").append(newList);
        $("#table_list_external_repair").show();
        $("#frmVieUpdatewRepair").hide();
      }
    }
    lightGallery(document.getElementById('lightgallery'));
  });
  spinnerHelpers.hide();
}



export function getAllexternalRepair() {
  spinnerHelpers.show();
  externalRepairServices.getAllexternalRepair(storageData.sIdLanguage(), function (response) {
    console.log(response.data);
    for (var i = 0; i < response.data.length; i++) {
      var newList = "";
      newList += "<tr>";
      newList += "<td>" + response.data[i].id_librone_rip_esterna + "</td>";
      // newList += "<td>" + response.data[i].id + "</td>";
      newList += "<td>" + response.data[i].numero + "</td>";
      newList += "<td>" + response.data[i].date + "</td>";
      newList += "<td>" + response.data[i].ddt_exit + "</td>";
      newList += "<td>" + response.data[i].id_business_name_supplier + "</td>";
      newList += "<td>";
      if (response.data[i].img != "") {
        newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">';
        newList +=
          "<a href='" +
          response.data[i].img +
          "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
      }
      newList += "</td>";
      newList += "<td>" + response.data[i].description + "</td>";
  newList += "<td>";
      if (response.data[i].imgmodule != "") {
        newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">';
        newList +=
          "<a href='" +
          response.data[i].imgmodule +
          "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
      }
      newList += "</td>";
      newList += "<td>" + response.data[i].code_gr + "</td>";
      newList += "<td>" + response.data[i].date_return_product_from_the_supplier + "</td>";
      newList += "<td>" + response.data[i].ddt_supplier + "</td>";
      newList += "<td>" + response.data[i].reference_supplier + "</td>";
      newList += "<td>" + response.data[i].notes_from_repairman + "</td>";
      newList += "<td>" + response.data[i].price + "</td>";
      newList += "<td>";
      if (response.data[i].img_rep_test != "") {
        newList +=
          '<div class="container"><div id="lightgallery" style="display:flex">';
        newList +=
          "<a href='" +
          response.data[i].img_rep_test +
          "'><i class='fa fa-camera' class='fr-fic fr-dii' style='color:#000;float:right;'></i></a></div></div>";
      }
      newList += "</td>";
      newList +=
      '<td><button class="btn btn-info" onclick="externalRepairHelpers.getByRepair(' +
      response.data[i].id +
      ')"><i class="fa fa-pencil"style="color:#fff!important;"/></button></td>';
      newList +=
        '<td><button class="btn btn-danger" onclick="externalRepairHelpers.deleteRow(' +
        response.data[i].id +
        ')"><i class="fa fa-trash"style="color:#fff!important;"/></button></td>';
      newList += "</tr>";
      $("#table_list_external_repair tbody").append(newList);
      $("#table_list_external_repair").show();
      $("#frmVieUpdatewRepair").hide();
    }
    lightGallery(document.getElementById('lightgallery'));
  });

  spinnerHelpers.hide();
}

export function getByRepair(id) {
  externalRepairServices.getByRepair(id, function(response) {
    var searchParam = "";
    jQuery.each(response.data, function(i, val) {
      if (val == true) {
        if (i == "id") {
          $("#frmVieUpdatewRepair #" + i).html(val);
        } else {
          $("#frmVieUpdatewRepair #" + i).attr("checked", "checked");
        }
      } else {
        if (i == "imgName") {
          if (val != "") {
           $("#frmVieUpdatewRepair #imgPreview").attr('src', val);
            $("#frmVieUpdatewRepair #imgName").attr('src', val);
            $("#frmVieUpdatewRepair #imgNameLink").attr('href', val);
          } else {
             $("#frmVieUpdatewRepair #imgName").remove();
          }
        } else {
          $("#frmVieUpdatewRepair #" + i).val(val);
          $("#frmVieUpdatewRepair #" + i).html(val);
        }
      }

      var selectFaultModule = response.data.selectFaultModule[0];
      $("#div-cont-module").html("");
      var html = "";
      html += '<h3 '+ '">Anteprima Modulo Guasti ' + "</h3>&nbsp;";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-3">';
      html += '<label for="external_referent_id' + '">Referente ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="external_referent_id" id="external_referent_id" placeholder="Referente" readonly value="' +
        selectFaultModule.external_referent_id +
        '">';
      html += "</div>";
      html += '<div class="col-3">';
      html += '<label for="nr_ddt' + '">Numero DDT ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="nr_ddt" id="nr_ddt" placeholder="Numero DDT" readonly value="' +
        selectFaultModule.nr_ddt +
        '">';
      html += "</div>";
       html += '<div class="col-3">';
      html += '<label for="date_ddt' + '">Data DDT ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="referent" id="date_ddt" placeholder="Data DDT" readonly value="' +
        selectFaultModule.date_ddt +
        '">';
      html += "</div>";
      html += "</div>";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-3">';
      html += '<label for="trolley_type' + '">Modello Carrello ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="trolley_type" id="trolley_type" placeholder="Modello Carrello" readonly value="' +
        selectFaultModule.trolley_type +
        '">';
      html += "</div>";
      html += '<div class="col-3">';
      html += '<label for="series' + '">Matricola ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="series" id="series" placeholder="Matricola" readonly value="' +
        selectFaultModule.series +
        '">';
      html += "</div>";
       html += '<div class="col-3">';
      html += '<label for="voltage' + '">Voltaggio ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="voltage" id="voltage" placeholder="Voltaggio" readonly value="' +
        selectFaultModule.voltage +
        '">';
      html += "</div>";
      html += '<div class="col-3">';
      html += '<label for="plant_type' + '">Tipo Impianto ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="plant_type" id="plant_type" placeholder="Tipo Impianto" readonly value="' +
        selectFaultModule.plant_type +
        '">';
      html += "</div>";
      html += "</div>";

      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html += '<label for="defect' + '">Difetto ' + "</label>&nbsp;";
      html += '<textarea class="form-control" id="defect" readonly>' + selectFaultModule.defect + '</textarea>';
      html += "</div>";
      html += '<div class="col-6">';
      html += '<label for="exit_notes' + '">Note ' + "</label>&nbsp;";
      html += '<textarea class="form-control" id="exit_notes" readonly>' + selectFaultModule.exit_notes + '</textarea>';
      html += "</div>";
      html += "</div>";
      $("#div-cont-module").append(html);

      var AggExternalRepair = response.data;
      $("#div-cont-upd-external-repair").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-4">';
      html += '<label for="id_intervention' + '">Numero ' + "</label>&nbsp;";
      html +=
      '<input type="text" class="form-control" name="id_librone_rip_esterna" id="id_librone_rip_esterna" placeholder="Numero Riparazione Esterna" value="' +
      AggExternalRepair.id_librone_rip_esterna +
      '">';
      html +=
        '<input type="hidden" class="form-control" name="id_intervention" id="id_intervention" placeholder="Numero" value="' +
        AggExternalRepair.id_intervention +
        '">';
      html += "</div>";
      html += '<div class="col-4">';
      html += '<label for="date' + '">Data ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="date" id="date" placeholder="Data" value="' +
        AggExternalRepair.date +
        '">';
      html += "</div>";
      html += '<div class="col-4">';
      html += '<label for="ddt_exit' + '">DDT Uscita ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="ddt_exit" id="ddt_exit" placeholder="DDT Uscita" value="' +
        AggExternalRepair.ddt_exit +
        '">';
      html += "</div>";
      html += "</div>";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-6">';
      html +=
        '<label for="id_business_name_supplier' +
        '">Fornitore ' +
        "</label>&nbsp;";
      html +=
        '<select class="form-control select-graphic custom-select id_business_name_supplier" id="id_business_name_supplier' +
        '" value="' +
        AggExternalRepair.id_business_name_supplier +
        '" name="id_business_name_supplier' +
        '"></select>';
      html += "</div>";
      html += "</div>";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col">';
      html += '<label for="description' + '">Descrizione ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="description" id="description" placeholder="Descrizione" value="' +
        AggExternalRepair.description +
        '">';
      html += "</div>";
      html += "</div>";
      html += '<div class="row">';
      html += '<div class="col-6">';
      html += '<label for="code_gr' + '">Codice GR ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="code_gr" id="code_gr" placeholder="Codice GR" value="' +
        AggExternalRepair.code_gr +
        '">';
      html += "</div>";
     
      html += '<div class="col-6">';
      html += '<label for="price' + '">Prezzo ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="price" id="price" placeholder="Prezzo" value="' +
        AggExternalRepair.price +
        '">';
      html +=
        '<input type="hidden" class="form-control" name="id" id="id" placeholder="id" value="' +
        AggExternalRepair.id +
        '">';
      html += "</div>";
      html += "</div>";
      $("#div-cont-upd-external-repair").append(html);

    $('#div-cont-upd-external-repair #date').datetimepicker({
    format: 'DD/MM/YYYY'
    });
      var AggExternalRepair = response.data;
      $("#div-cont-upd-rientro-external-repair").html("");
      var html = "";
      html += '<div class="row" data-id-row="' + '" id="divcont-' + '" >';
      html += '<div class="col-4">';
      html += '<label for="date_return_product_from_the_supplier' + '">Data Entrata ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="date_return_product_from_the_supplier" id="date_return_product_from_the_supplier" placeholder="Data Entrata" value="' +
        AggExternalRepair.date_return_product_from_the_supplier +
        '">';
      html += "</div>";
      html += '<div class="col-4">';
      html += '<label for="ddt_supplier' + '">DDT Fornitore ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="ddt_supplier" id="ddt_supplier" placeholder="DDT Fornitore" value="' +
        AggExternalRepair.ddt_supplier +
        '">';
      html += "</div>";
      html += '<div class="col-4">';
      html += '<label for="reference_supplier' + '">Rif Fornitore ' + "</label>&nbsp;";
      html +=
        '<input type="text" class="form-control" name="reference_supplier" id="reference_supplier" placeholder="Rif Fornitore" value="' +
        AggExternalRepair.reference_supplier +
        '">';
      html += "</div>";
      html += "</div>";
      html += "</div>";
      html += '<div class="row">';
      html += '<div class="col">';
      html += '<label for="notes_from_repairman' + '">Note Fornitore ' + "</label>&nbsp;";
      html += '<textarea class="form-control" id="notes_from_repairman" placeholder="Note Fornitore">' + AggExternalRepair.notes_from_repairman + '</textarea>';
      html += "</div>";
      $("#div-cont-upd-rientro-external-repair").append(html); 

       $('#date_return_product_from_the_supplier').datetimepicker({
          format: 'DD/MM/YYYY'
       });
      
      var searchParam = "";
      var initials = [];
      initials.push({
        id: AggExternalRepair.id_business_name_supplier,
        text: AggExternalRepair.Description_business_name_supplier
      });
      $("#id_business_name_supplier").select2({
        data: initials,
        ajax: {
          url:
            configData.wsRootServicesUrl +
            "/api/v1/businessnamesupplier/select",
          data: function(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });

      var searchParam = "";
      $("#id_business_name_supplier").select2({
        ajax: {
          url:
            configData.wsRootServicesUrl +
            "/api/v1/businessnamesupplier/select",
          data: function(params) {
            if (params.term == "") {
              searchParam = "*";
            } else {
              searchParam = params.term;
            }
            var query = {
              search: searchParam
            };
            return query;
          },
          processResults: function(data) {
            var dataParse = JSON.parse(data);
            return {
              results: dataParse.data
            };
          }
        }
      });
    });
    $("#table_list_external_repair").show();
    $("#frmVieUpdatewRepair").modal("show");

  });
}


export function updateRepairExternal() {
  spinnerHelpers.show();
  var data = {
    id: $("#frmVieUpdatewRepair #id").val(),
    id_librone_rip_esterna: $("#frmVieUpdatewRepair #id_librone_rip_esterna").val(),
    id_intervention: $("#frmVieUpdatewRepair #id_intervention").val(),
    date: $("#frmVieUpdatewRepair #date").val(),
    ddt_exit: $("#frmVieUpdatewRepair #ddt_exit").val(),
    id_business_name_supplier: $("#frmVieUpdatewRepair #id_business_name_supplier").val(),
    date_return_product_from_the_supplier: $("#frmVieUpdatewRepair #date_return_product_from_the_supplier").val(),
    ddt_supplier: $("#frmVieUpdatewRepair #ddt_supplier").val(),
    reference_supplier: $("#frmVieUpdatewRepair #reference_supplier").val(),
    notes_from_repairman: $("#frmVieUpdatewRepair #notes_from_repairman").val(),
    price: $("#frmVieUpdatewRepair #price").val(),
  };

if (data.id_business_name_supplier == "" || data.id_business_name_supplier == null || data.id_business_name_supplier == 'null') {
      notificationHelpers.error("Attenzione,non puoi salvare, selezionare il fornitore;");
}else{
    externalRepairServices.updateRepairExternal(data, function() {
      $("#frmVieUpdatewRepair #date").val("");
      $("#frmVieUpdatewRepair #ddt_exit").val("");
      $("#frmVieUpdatewRepair #id_business_name_supplier").val("");
      $("#frmVieUpdatewRepair #date_return_product_from_the_supplier").val("");
      $("#frmVieUpdatewRepair #ddt_supplier").val("");
      $("#frmVieUpdatewRepair #reference_supplier").val("");
      $("#frmVieUpdatewRepair #notes_from_repairman").val("");
      $("#frmVieUpdatewRepair #price").val("");
      notificationHelpers.success("Modifica avvenuta con successo");
      spinnerHelpers.hide();
      window.location.reload();
      $("#table_list_external_repair").show();
      $("#frmVieUpdatewRepair").modal("hide");
    });
}
}

export function deleteRow(id) {
  if (confirm("Sei sicuro di voler eliminare la riparazione esterna?")) {
    spinnerHelpers.show();
    externalRepairServices.del(id, function(response) {
      notificationHelpers.success(
        dictionaryHelpers.getDictionary("DeleteCompleted")
      );
      spinnerHelpers.hide();
      window.location.reload();
    });
  }
}
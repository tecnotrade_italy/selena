/***   serviceHelpers   ***/

/***   GET   ***/

export function get(url, fnSuccess, fnError) {
  $.ajax({
    type: 'GET',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    contentType: 'application/json'
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

export function getAutenticate(url, fnSuccess, fnError) {
  $.ajax({
    type: 'GET',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    // beforeSend: function (xhr) {
    //   xhr.setRequestHeader("Authorization", storageData.sTokenKey());
    // },
    headers: {
      Authorization: storageData.sTokenKey()
    },
    contentType: 'application/json'
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

export function getAutenticateWithData(url, data, fnSuccess, fnError) {
  $.ajax({
    type: 'GET',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    // beforeSend: function (xhr) {
    //   xhr.setRequestHeader("Authorization", storageData.sTokenKey());
    // },
    headers: {
      Authorization: storageData.sTokenKey()
    },
    contentType: 'application/json',
    data: data
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

/***   END GET   ***/

/***   PUT   ***/

export function put(url, data, fnSuccess, fnError) {
  $.ajax({
    type: 'PUT',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    contentType: 'application/json',
    data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

export function putAutenticate(url, data, fnSuccess, fnError) {
  $.ajax({
    type: 'PUT',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    // beforeSend: function (xhr) {
    //   xhr.setRequestHeader("Authorization", storageData.sTokenKey());
    // },
    headers: {
      Authorization: storageData.sTokenKey()
    },
    contentType: 'application/json',
    data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

export function putAutenticateWithoutData(url, fnSuccess, fnError) {
  $.ajax({
    type: 'PUT',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    // beforeSend: function (xhr) {
    //   xhr.setRequestHeader("Authorization", storageData.sTokenKey());
    // },
    headers: {
      Authorization: storageData.sTokenKey()
    },
    contentType: 'application/json'
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

/***   END PUT   ***/

/***   POST   ***/

export function post(url, data, fnSuccess, fnError) {
  $.ajax({
    type: 'POST',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    contentType: 'application/json',
    data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

export function postWithoutData(url, fnSuccess, fnError) {
  $.ajax({
    type: 'POST',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    contentType: 'application/json'
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

export function postNoJson(url, data, fnSuccess, fnError) {
  $.ajax({
    type: 'POST',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    //headers: { "Accept": "application/json" },
    //contentType: 'application/json',
    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    data: data
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

export function postAutenticate(url, data, fnSuccess, fnError) {
  $.ajax({
    type: 'POST',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    // beforeSend: function (xhr) {
    //   xhr.setRequestHeader("Authorization", storageData.sTokenKey());
    // },
    headers: {
      Authorization: storageData.sTokenKey()
    },
    contentType: 'application/json',
    data: JSON.stringify(functionHelpers.clearNullOnJson(data))
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

export function postAutenticateWithoutData(url, fnSuccess, fnError) {
  $.ajax({
    type: 'POST',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    // beforeSend: function (xhr) {
    //   xhr.setRequestHeader("Authorization", storageData.sTokenKey());
    // },
    headers: {
      Authorization: storageData.sTokenKey()
    },
    contentType: 'application/json'
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

/***   END POST   ***/

/***   DELETE   ***/

export function del(url, fnSuccess, fnError) {
  $.ajax({
    type: 'DELETE',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    contentType: 'application/json'
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

export function delAutenticate(url, fnSuccess, fnError) {
  $.ajax({
    type: 'DELETE',
    url: configData.wsRootServicesUrl + url,
    dataType: 'json',
    // beforeSend: function (xhr) {
    //   xhr.setRequestHeader("Authorization", storageData.sTokenKey());
    // },
    headers: {
      Authorization: storageData.sTokenKey()
    },
    contentType: 'application/json'
  }).done(function (result) {
    if (functionHelpers.isValued(fnSuccess) && functionHelpers.isFunction(fnSuccess)) {
      fnSuccess(result);
    }
  }).fail(function (response) {
    failServiceHelpers.fail(response, fnError);
  });
}

/***   END GET   ***/

#!/bin/bash
cd Selena.Web/src/data/
echo " La seguente procedura andrà a sostituire il dominio: '$1' con il seguente dominio: '$2' all'interno dei file configurazioni del sito."
cat config.js | sed -e "s|$1|$2|g" >> configNew.js
rm config.js
mv configNew.js config.js
echo " Dominio: '$1' sostituito correttamente nella carrella Selena.Web"

cd ../../
echo " Eseguo il build della sezione amministrativa "
npm run build

cd ../Selena.WebApi/resources/assets/js/data/
cat config.js | sed -e "s|$1|$2|g" >> configNew.js
rm config.js
mv configNew.js config.js
echo " Dominio: '$1' sostituito correttamente nella carrella Selena.WebApi"

cd ../../../../
echo " Eseguo la generazione dei pacchetti aggiornati "
npm run prod

